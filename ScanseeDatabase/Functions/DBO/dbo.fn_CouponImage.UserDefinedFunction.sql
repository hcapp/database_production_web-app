USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CouponImage]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_CouponImage
Purpose			: Function returns Coupon, Loyalty Deal and Rebate Info of the product and retailer

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th Oct 2011	SPAN S	Initial Version
---------------------------------------------------------------
*/


CREATE FUNCTION [dbo].[fn_CouponImage]
(
	@CouponID int
)

RETURNS varchar(1000)
AS
BEGIN
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	  
	Declare @image varchar(1000)
	Select @image= ProductImagePath
	FROM (SELECT Top 1 ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
											ELSE ProductImagePath END 
	FROM Coupon c
			INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID
			INNER JOIN Product P ON P.ProductID=CP.ProductID
		WHERE C.CouponID=@CouponID
		Order By ProductName) Coupon
	
	RETURN @image
END


GO
