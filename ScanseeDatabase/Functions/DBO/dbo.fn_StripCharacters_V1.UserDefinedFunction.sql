USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_StripCharacters_V1]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_StripCharacters_V1]
(
    @String NVARCHAR(MAX)
    
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @regex INT
--SET @string='!@#$%^&*()<>?l'
SET @regex = PATINDEX('%[^a-zA-Z0-9 ]%', @string)
WHILE @regex > 0
BEGIN
SET @string = STUFF(@string, @regex, 1, '' )
SET @regex = PATINDEX('%[^a-zA-Z0-9 ]%', @string)
END
    RETURN @String

END



GO
