USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CouponDetails]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Function name : fn_CouponDetails    
Purpose   : Function returns Coupon Info of the product and retailer    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   7th June 2011 SPAN Initial Version    
---------------------------------------------------------------    
*/    
    
    
CREATE FUNCTION [dbo].[fn_CouponDetails]    
(    
 @ProductID int,    
 @RetailID int    
)    
    
RETURNS @Coupon TABLE (Coupon INT)    
AS    
BEGIN    
 INSERT INTO @Coupon    
 --To fetch available coupon.    
 SELECT C.CouponID     
 FROM Coupon C  
 INNER JOIN CouponProduct CP ON  C.CouponID=CP.CouponID 
 LEFT OUTER JOIN CouponRetailer CR ON CR.CouponID=C.CouponID  
 WHERE CP.ProductID = @ProductID     
  AND (((ISNULL(@RetailID, '') = '') AND 1=1)  
  OR   
   ((ISNULL(@RetailID, '') <> '') AND CR.RetailID = @RetailID))    
  AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate  
      
     
 RETURN    
END


GO
