USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ConfigvalueforScreencontent]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_ConfigvalueforScreencontent]
(
    @String NVARCHAR(MAX)
    
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

		SELECT @String=ScreenContent
			FROM AppConfiguration 
				WHERE ConfigurationType=@String	AND Active = 1 

		RETURN @String

END



GO
