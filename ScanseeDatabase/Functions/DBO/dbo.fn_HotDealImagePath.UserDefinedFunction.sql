USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_HotDealImagePath]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_HotDealImagePath  
Purpose   : Function returns HotDealImagePath 
  
History  
Version  Date           Author   Change Description  
---------------------------------------------------------------   
1.0   17th jun 2013     SPAN           Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [dbo].[fn_HotDealImagePath]  
(  
 @ProductHotDealID int  
)  
  
RETURNS VARCHAR(500)  
AS  
	BEGIN  
	  DECLARE @HotDealImagePath varchar(500)
	  DECLARE @ManufConfig varchar(50) 
      SELECT @ManufConfig=(SELECT ScreenContent  
                           FROM AppConfiguration   
                           WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
      FROM AppConfiguration  
	  SELECT @HotDealImagePath = CASE WHEN P.WebsiteSourceFlag = 1 
									THEN @ManufConfig +CONVERT(VARCHAR(30),PHD.ManufacturerID)+'/' +HotDealImagePath
							    ELSE P.ProductImagePath
							    END
	 FROM ProductHotDeal PHD
     INNER JOIN HotDealProduct  HDP ON PHD.ProductHotDealID = HDP.ProductHotDealID
     INNER JOIN Product p ON HDP.ProductID = P.ProductID
     WHERE PHD.ProductHotDealID = @ProductHotDealID
     AND P.ProductImagePath IS NOT NULL
										 
	 RETURN (@HotDealImagePath)  
	END


GO
