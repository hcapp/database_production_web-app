USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SplitParamMultiDelimiter]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_SplitParam
Purpose			: Function will split the comma seperated values in a array and give it in a tabular result set.

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10-03-2014		Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/


CREATE  FUNCTION [dbo].[fn_SplitParamMultiDelimiter](@RepParam nvarchar(MAX), @Delim char(1)= ',')
RETURNS @Values TABLE (Param nvarchar(MAX))

AS

BEGIN          

  DECLARE @chrind INT
  DECLARE @Piece nvarchar(MAX)
  SELECT @chrind = 1 
  WHILE @chrind > 0
    BEGIN
      SELECT @chrind = PATINDEX('%'+@Delim+'%',@RepParam)
      IF @chrind  > 0
        SELECT @Piece = LEFT(@RepParam,@chrind - 1)

      ELSE

        SELECT @Piece = @RepParam
        
      INSERT  @Values(Param) VALUES(Cast(@Piece AS varchar(MAX)))	 
      SELECT @RepParam = RIGHT(@RepParam,ABS(LEN(@RepParam) - (@chrind + LEN(@Delim)-1)))
 
      IF LEN(@RepParam) = 0 BREAK
	END
	DELETE FROM @Values WHERE Param = '~~'
  RETURN

END

GO
