USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SplitParam]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_SplitParam]

   (@RepParam nvarchar(max), @Delim char(1)= ',')

RETURNS @Values TABLE (Param nvarchar(max))AS

  BEGIN            

  DECLARE @chrind INT

  DECLARE @Piece nvarchar(max)

  SELECT @chrind = 1 

  WHILE @chrind > 0

    BEGIN

      SELECT @chrind = CHARINDEX(@Delim,@RepParam)

      IF @chrind  > 0

        SELECT @Piece = LEFT(@RepParam,@chrind - 1)

      ELSE

        SELECT @Piece = @RepParam

      INSERT  @Values(Param) VALUES(Cast(@Piece AS nvarchar(max)))

	 

      SELECT @RepParam = RIGHT(@RepParam,LEN(@RepParam) - @chrind)

--          SELECT @RepParam = @RepParam

      IF LEN(@RepParam) = 0 BREAK

    END

  RETURN

  END

GO
