USE [scansee]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_MSLCLRButtonDisplay]    Script Date: 4/6/2017 2:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_MSLCLRButtonDisplay
Purpose			: Function returns a flag to display CLR Button in MSL based on the user preference.

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24th Aug 2011	SPAN.S	Initial Version
---------------------------------------------------------------
*/


CREATE FUNCTION [dbo].[fn_MSLCLRButtonDisplay]
(
	@UserID int
	, @ProductID int
)

RETURNS int
AS
BEGIN
	DECLARE @CLR int
	--to check if the product has any CLR associated with it. 
		IF EXISTS (SELECT 1 
					FROM Coupon c 
					INNER JOIN CouponProduct CP ON CP.couponid=c.CouponID 
					FULL JOIN LoyaltyDeal l	ON cp.ProductID=l.ProductID
					FULL JOIN RebateProduct R ON cp.ProductID=r.ProductID
					WHERE cp.ProductID=@productid)
		BEGIN
			--If a product has a CLR associated, then checking for the User Preferences for the CLR info.
			IF EXISTS (SELECT 1 
						FROM UserPreference 
						WHERE userid=@userid 
							AND (DisplayCoupons=1 OR DisplayRebates=1 OR DisplayLoyaltyRewards=1))
			BEGIN
				--If user has opted for any of the CLR in user preferences then enable the CLR button in the Screen.
				SET @CLR = 1
			END
			ELSE 
				--If user have not opted for any of the CLR in user preferences then disable the CLR button in the Screen.
				SET @CLR = -1
		END
		ELSE 
		 --If the produtc is not associated with any CLR, then CLR button is not displayed.
		 SET @CLR = 0
	RETURN (@CLR)
END


GO
