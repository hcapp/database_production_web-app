USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiWeb].[fn_SplitParam]    Script Date: 4/6/2017 2:33:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [HubCitiWeb].[fn_SplitParam]

   (@RepParam nvarchar(MAX), @Delim char(1)= ',')

RETURNS @Values TABLE (Param nvarchar(4000))AS

  BEGIN            

  DECLARE @chrind INT

  DECLARE @Piece nvarchar(1000)

  SELECT @chrind = 1 

  WHILE @chrind > 0

    BEGIN

      SELECT @chrind = CHARINDEX(@Delim,@RepParam)

      IF @chrind  > 0

        SELECT @Piece = LEFT(@RepParam,@chrind - 1)

      ELSE

        SELECT @Piece = @RepParam

      INSERT  @Values(Param) VALUES(Cast(@Piece AS varchar(4000)))

      SELECT @RepParam = RIGHT(@RepParam,LEN(@RepParam) - @chrind)

--          SELECT @RepParam = @RepParam

      IF LEN(@RepParam) = 0 BREAK

    END

  RETURN

  END



GO
