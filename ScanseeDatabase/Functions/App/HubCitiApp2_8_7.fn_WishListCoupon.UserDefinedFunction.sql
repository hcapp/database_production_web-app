USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_7].[fn_WishListCoupon]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_WishListCoupon
Purpose			: Function returns a flag to display CLR Button in MSL based on the user preference.

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24th Aug 2011	SPAN.S	Initial Version
---------------------------------------------------------------
*/


CREATE FUNCTION [HubCitiApp2_8_7].[fn_WishListCoupon]
(
	@ProductID int
	, @RetailID int
)

RETURNS int
AS
BEGIN
	DECLARE @HD int  
	--To fetch available coupon.  
	 SELECT @HD = CASE WHEN COUNT(C.CouponID) > 0 THEN 1 ELSE 0 END   
	 FROM Coupon C
	 INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID
	 INNER JOIN CouponRetailer CR ON CR.CouponID=C.CouponID   
	 WHERE CP.ProductID = @ProductID   
	  AND (((ISNULL(@RetailID, '') = '') AND 1=1)
			OR 
		  ((ISNULL(@RetailID, '') <> '') AND CR.RetailID = @RetailID))  
	  AND CouponExpireDate >= GETDATE() 
  
	--SELECT 
	--FROM Coupon 
	--WHERE ProductID = @ProductID 
	
	RETURN (@HD)
END





































GO
