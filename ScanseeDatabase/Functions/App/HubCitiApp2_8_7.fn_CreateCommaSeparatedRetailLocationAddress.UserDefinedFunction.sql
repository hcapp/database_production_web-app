USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_7].[fn_CreateCommaSeparatedRetailLocationAddress]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_CreateCommaSeparatedRetailLocation  
Purpose   : To return the comma separated Retail Location addresses associated with the given hot deal.  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   24th Aug 2011 SPAN Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [HubCitiApp2_8_7].[fn_CreateCommaSeparatedRetailLocationAddress]
(
    @ProductHotDealID AS INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @RetailLocationID VARCHAR(MAX)

    SELECT @RetailLocationID = COALESCE(@RetailLocationID + ' | ','') + RL.Address1 + ', ' +  RL.city + ', ' +  RL.[State] + ', ' +  RL.PostalCode
    FROM ProductHotDealRetailLocation PRL
    INNER JOIN RetailLocation RL ON RL.RetailLocationID = PRL.RetailLocationID
    WHERE ProductHotDealID = @ProductHotDealID


    RETURN @RetailLocationID

END





































GO
