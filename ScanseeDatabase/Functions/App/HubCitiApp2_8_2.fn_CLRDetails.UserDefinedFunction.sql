USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_2].[fn_CLRDetails]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_CLRDetails
Purpose			: Function returns Coupon, Loyalty Deal and Rebate Info of the product and retailer

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN	Initial Version
---------------------------------------------------------------
*/


CREATE FUNCTION [HubCitiApp2_8_2].[fn_CLRDetails]
(
	@ProductID int,
	@RetailLocationID int
)

RETURNS @CLR TABLE (Coupon INT, Loyalty INT, Rebate INT)
AS
BEGIN
	--To get Retailer Id
	DECLARE @RetailID INT
	SELECT @RetailID = RetailID 
	FROM RetailLocation 
	WHERE RetailLocationID =  @RetailLocationID
	
	DECLARE @Coupon bit = 0
	DECLARE @Loyalty bit = 0
	DECLARE @Rebate bit = 0 
	--To fetch available coupon.
	SELECT @Coupon = C.CouponID 
	FROM Coupon C
	INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID
	WHERE CP.ProductID = @ProductID 
		AND RetailID = @RetailID 
		AND CouponExpireDate >= GETDATE()
	
	--To fetch available Loyalty Deals.	
	SELECT @Loyalty = LD.LoyaltyDealID
	FROM LoyaltyDeal LD 
	INNER JOIN LoyaltyDealProduct LP ON LD.LoyaltyDealID = LP.LoyaltyDealID
	WHERE LP.ProductID = @ProductID 
		AND RetailLocationID = @RetailLocationID 
		AND LoyaltyDealExpireDate >= GETDATE()
		
	--To fetch available Rebate.
	SELECT @Rebate = RP.RebateID 
	FROM Rebate R
		INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID 
		INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
	WHERE RR.RetailID = @RetailID 
		AND RP.ProductID = @ProductID 
		AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate 
		
	INSERT INTO @CLR SELECT @Coupon, @Loyalty, @Rebate 
	
	RETURN
END



































GO
