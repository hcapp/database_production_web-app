USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_3].[fn_CreateCommaSeparatedRetailLocation]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_CreateCommaSeparatedRetailLocation  
Purpose   : To return the comma separated Retail Location IDs.  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   24th Aug 2011 SPAN Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [HubCitiApp2_8_3].[fn_CreateCommaSeparatedRetailLocation]
(
    @ProductHotDealID AS INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @RetailLocationIDs VARCHAR(MAX)

    SELECT @RetailLocationIDs = ISNULL(@RetailLocationIDs+',','') + CAST(RetailLocationID AS VARCHAR(100))
    FROM ProductHotDealRetailLocation 
    WHERE ProductHotDealID = @ProductHotDealID


    RETURN @RetailLocationIDs

END




































GO
