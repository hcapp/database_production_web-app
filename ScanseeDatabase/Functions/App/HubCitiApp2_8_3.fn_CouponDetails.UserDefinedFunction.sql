USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_3].[fn_CouponDetails]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Function name : fn_CouponDetails    
Purpose   : Function returns Coupon Info of the product and retailer    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   7th June 2011 SPAN Initial Version    
---------------------------------------------------------------    
*/    
    
    
CREATE FUNCTION [HubCitiApp2_8_3].[fn_CouponDetails]    
(    
 @ProductID int,    
 @RetailID int    
)    
    
RETURNS @Coupon TABLE (Coupon INT)    
AS    
BEGIN    
 INSERT INTO @Coupon    
 --To fetch available coupon.    
 SELECT C.CouponID     
 FROM Coupon C  
 INNER JOIN CouponProduct CP ON  C.CouponID=CP.CouponID 
 LEFT OUTER JOIN CouponRetailer CR ON CR.CouponID=C.CouponID
 LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID  
 WHERE CP.ProductID = @ProductID     
  AND (((ISNULL(@RetailID, '') = '') AND 1=1)  
  OR   
   ((ISNULL(@RetailID, '') <> '') AND CR.RetailID = @RetailID))    
  AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate 
GROUP BY C.CouponID
		, NoOfCouponsToIssue
HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
			ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0) 
      
     
 RETURN    
END




































GO
