USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_3].[fn_RebateImage]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_CouponImage
Purpose			: Function returns productImagePath.

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th Oct 2011	SPAN	Initial Version
---------------------------------------------------------------
*/


CREATE FUNCTION [HubCitiApp2_8_3].[fn_RebateImage]
(
	@RebateID int
)

RETURNS varchar(1000)
AS
BEGIN
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
  
	Declare @image varchar(1000)
	Select @image= ProductImagePath
	FROM (SELECT Top 1 ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END 
	FROM Rebate R
			INNER JOIN RebateProduct RP ON R.RebateID=RP.RebateID
			INNER JOIN Product P ON P.ProductID=RP.ProductID
		WHERE R.RebateID=@RebateID
		Order By ProductName) Rebate
	
	RETURN @image
END




































GO
