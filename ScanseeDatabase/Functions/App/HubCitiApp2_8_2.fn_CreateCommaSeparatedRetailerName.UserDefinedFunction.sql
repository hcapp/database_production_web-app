USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_2].[fn_CreateCommaSeparatedRetailerName]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_CreateCommaSeparatedRetailerName  
Purpose   : To return the comma separated Retail Names associated with the given hot deal.  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   24th Aug 2011 SPAN Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [HubCitiApp2_8_2].[fn_CreateCommaSeparatedRetailerName]
(
    @ProductHotDealID AS INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @RetailName VARCHAR(MAX)

    SELECT @RetailName = COALESCE(@RetailName + ', ','') + CAST(R.RetailName AS VARCHAR(100))
    FROM ProductHotDealRetailLocation PRL
    INNER JOIN RetailLocation RL ON RL.RetailLocationID = PRL.RetailLocationID
    INNER JOIN Retailer R ON R.RetailID = RL.RetailID
    WHERE ProductHotDealID = @ProductHotDealID
    GROUP BY R.RetailName


    RETURN @RetailName

END



































GO
