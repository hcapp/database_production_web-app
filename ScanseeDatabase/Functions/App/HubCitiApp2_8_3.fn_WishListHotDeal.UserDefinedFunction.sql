USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_3].[fn_WishListHotDeal]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_WishListHotDeal  
Purpose   : Function returns a flag to display CLR Button in MSL based on the user preference.  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   24th Aug 2011 SPAN.S Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [HubCitiApp2_8_3].[fn_WishListHotDeal]  
(  
 @ProductID int  
)  
  
RETURNS int  
AS  
BEGIN  
 DECLARE @HD int    
 SELECT @HD = CASE WHEN COUNT(HotDealProductID) > 0 THEN 1 ELSE 0 END  
 FROM HotDealProduct HP
 INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = HP.ProductHotDealID   
 WHERE HP.ProductID = @ProductID  AND (GETDATE() BETWEEN PH.HotDealStartDate AND PH.HotDealEndDate)
 RETURN (@HD)  
END




































GO
