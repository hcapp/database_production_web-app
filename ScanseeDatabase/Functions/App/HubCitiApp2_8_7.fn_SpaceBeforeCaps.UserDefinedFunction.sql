USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_7].[fn_SpaceBeforeCaps]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Function name	: fn_CLRDetails
Purpose			: To insert spaces before the Capital Letters

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th April 2014	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/


CREATE FUNCTION [HubCitiApp2_8_7].[fn_SpaceBeforeCaps]
(
	@StringWithoutSpaces varchar(2000)
)

RETURNS VARCHAR(100)
AS
BEGIN
    DECLARE @return VARCHAR(100);
    SET @return = LEFT(@StringWithoutSpaces,1);
    DECLARE @i int;
    SET @i = 2;
    WHILE @i <= LEN(@StringWithoutSpaces)
    BEGIN
        IF ASCII(SUBSTRING(@StringWithoutSpaces,@i,1)) BETWEEN ASCII('A') AND ASCII('Z')
            SET @return = @return + ' ' + SUBSTRING(@StringWithoutSpaces,@i,1)
        ELSE
            SET @return = @return + SUBSTRING(@StringWithoutSpaces,@i,1)
        SET @i = @i + 1;
    END;
    RETURN @return;
END;




































GO
