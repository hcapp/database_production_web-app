USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_3].[fn_RebateDetails]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_RebateDetails  
Purpose   : Function returns Rebate Info of the product and retailer  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   7th June 2011 SPAN Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [HubCitiApp2_8_3].[fn_RebateDetails]  
(  
 @ProductID int,  
 @RetailID int  
)  
  
RETURNS @Rebate TABLE (Rebate INT)  
AS  
BEGIN  
 INSERT INTO @Rebate  
 --To fetch available Rebate.  
 SELECT DISTINCT RP.RebateID   
 FROM Rebate R  
  INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID  
  LEFT OUTER JOIN RebateRetailer RR ON RR.RebateID = R.RebateID 
 WHERE RP.ProductID = @ProductID   
  AND (((ISNULL(@RetailID, '') = '') AND 1=1)
		OR 
	  ((ISNULL(@RetailID, '') <> '') AND RR.RetailID = @RetailID))  
  AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate   
   
 RETURN  
END




































GO
