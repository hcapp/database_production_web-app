USE [scansee]
GO
/****** Object:  UserDefinedFunction [HubCitiApp2_8_7].[fn_LoyaltyDetails]    Script Date: 4/6/2017 2:29:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Function name : fn_LoyaltyDetails  
Purpose   : Function returns Coupon Info of the product and retailer  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   7th June 2011 SPAN Initial Version  
---------------------------------------------------------------  
*/  
  
  
CREATE FUNCTION [HubCitiApp2_8_7].[fn_LoyaltyDetails]  
(  
 @ProductID int,  
 @RetailID int  
)  
  
RETURNS @Loyalty TABLE (Loyalty INT)  
AS  
BEGIN  
 INSERT INTO @Loyalty  
 --To fetch available Loyalty Deals.   
 SELECT LD.LoyaltyDealID  
 FROM LoyaltyDeal LD  
  INNER JOIN LoyaltyProgram LP ON LP.LoyaltyProgramID = LD.LoyaltyProgramID   
  INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = LD.LoyaltyDealID
 WHERE LDP.ProductID = @ProductID   
  AND (((ISNULL(@RetailID, '') = '') AND 1=1)
		OR 
	  ((ISNULL(@RetailID, '') <> '') AND RetailID = @RetailID))  
  AND LoyaltyDealExpireDate >= GETDATE()  
   
 RETURN  
END





































GO
