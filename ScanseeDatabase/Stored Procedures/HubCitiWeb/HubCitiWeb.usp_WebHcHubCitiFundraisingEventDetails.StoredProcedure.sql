USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingEventDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : usp_WebHcHubCitiFundraisingEventDetails
Purpose                                  : To display Fundraising Event Details.
Example                                  : usp_WebHcHubCitiFundraisingEventDetails

History
Version              Date                 Author               Change Description
--------------------------------------------------------------- 
1.0                  26 Aug 2014       Mohith H R             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingEventDetails]
(   
		--Input variable.        
          @HcFundraisingID Int
		, @HcHubCitiID int
  
       --Output Variable 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
                     
                     DECLARE @HcFundraisingCategoryID Int
                     DECLARE @AppSiteID Int
					 DECLARE @DepartmentId Int
					 DECLARE @AppSiteFlag Bit
                     DECLARE @EventID Varchar(2000)
                     DECLARE @Config Varchar(500)
                     

					 SELECT @Config = ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'
                     
                     SELECT @HcFundraisingCategoryID = HcFundraisingCategoryID
                     FROM HcFundraisingCategoryAssociation 
                     WHERE HcFundraisingID  = @HcFundraisingID 

					 SELECT @DepartmentId = HcFundraisingDepartmentID
                     FROM HcFundraisingCategoryAssociation 
                     WHERE HcFundraisingID  = @HcFundraisingID

                     SELECT @AppSiteID = HcAppsiteID 
                     FROM HcFundraisingAppsiteAssociation 
                     WHERE HcFundraisingID  = @HcFundraisingID 

					 SELECT @AppSiteFlag = FundraisingAppsiteFlag
                     FROM HcFundraising
                     WHERE HcFundraisingID  = @HcFundraisingID 

                     --Select Event if Exist
                     SELECT @EventID = COALESCE(@EventID+',', '')+CAST(HcEventID AS VARCHAR)
                     FROM HcFundraisingEventsAssociation 
                     WHERE HcFundraisingID = @HcFundraisingID 
              
                     --To display Event Details.
                     SELECT DISTINCT FundraisingName hcEventName 
                                            ,IIF(@AppSiteFlag = 1,NULL,FundraisingOrganizationName) organizationHosting
                                            ,eventImagePath = @Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath
											,FundraisingOrganizationImagePath eventImageName    
											,ShortDescription shortDescription
											,LongDescription longDescription                                                   
                                            ,E.HcHubCitiID                                                    
                                            ,FundraisingGoal fundraisingGoal
                                            ,CurrentLevel currentLevel                                                      
                                            ,FundraisingAppsiteFlag isEventAppsite
                                            ,FundraisingEventFlag isEventTied
                                            ,MoreInformationURL moreInfoURL
                                            ,PurchaseProductURL purchaseProducts                                                                                                                                                         
                                            ,eventDate =CAST(StartDate AS DATE)
                                            ,eventEDate =ISNULL(CAST(EndDate AS DATE),NULL)
                                            --,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
                                            --,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)                                                                                                                   
                                            ,MenuItemExist =CASE WHEN(SELECT COUNT(HcMenuItemID)
                                                                                    FROM HcMenuItem MI 
                                                                                    INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                         
                                                                                    WHERE LinkTypeName = 'Fundraisers' 
                                                                                    AND MI.LinkID = E.HcFundraisingID)>0 THEN 1 ELSE 0 END          
                                           ,@HcFundraisingCategoryID  eventCategory                                                        
                                           ,@AppSiteID appsiteIDs
										   --,@DepartmentId departmentId
                                           ,eventTiedIds = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(HcEventID AS VARCHAR(10)))
                                                                                         FROM  HcFundraisingEventsAssociation HP
                                                                                         WHERE HP.HcFundraisingID = E.HcFundraisingID
                                                                                      FOR XML PATH( '' )
                                                                                    ), 3, 1000 ), ' ', '')
                                           ,Active
										   ,FL.Address address
										   ,FL.City city
										   ,FL.State state
										   ,FL.PostalCode postalCode
										   ,FL.Latitude latitude
										   ,FL.Longitude logitude  
										   ,ISNULL(FL.GeoErrorFlag,0) geoError                                                                                                                                                                              
                     FROM HcFundraising E 
					 LEFT JOIN HcFundraisingLocation FL ON E.HcFundraisingID = FL.HcFundraisingID AND FL.HcHubCitiID = @HcHubCitiID
                     --LEFT JOIN HcFundraisingEventsAssociation FE ON E.HcFundraisingID = FE.HcFundraisingID         
                     WHERE E.HcFundraisingID  = @HcFundraisingID AND E.HcHubCitiID = @HcHubCitiID
                     AND E.Active = 1 
                           
                     
                     --Confirmation of Success
                     SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingEventDetails.'        
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;











GO
