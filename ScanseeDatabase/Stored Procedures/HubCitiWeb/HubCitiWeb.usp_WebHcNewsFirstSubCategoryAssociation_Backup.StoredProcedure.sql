USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation_Backup]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]
Purpose					: To retrive Category Details
Example					: [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]
----------------------------------------------------------------------------------------------
*/
--exec [HubCitiWeb].[usp_WebHcNewsFirstCategoryUpdation&Deletion] 1,null,null,null,null,null,null,null
create PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation_Backup]
(   
	--Input variable
	  @HcHubCitiID int
	, @NewsFirstSubCategoryURL varchar(1000)
	, @NewsCategoryID INT
	, @NewsSubCategoryID varchar(1000)



	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			SELECT   Rownum=Identity(Int,1,1)
					,Param NewsFirstSubCategoryURL
			INTO #NewsFirstSubCategoryURL
			FROM fn_SplitParam(@NewsFirstSubCategoryURL,',')

			 SELECT   Rownum=Identity(Int,1,1)
					,Param NewsSubCategoryID
			INTO #NewsSubCategoryID
			FROM fn_SplitParam(@NewsSubCategoryID,',')

			DELETE FROM NewsFirstSubCategorySettings
			WHERE HcHubCitiID= @HcHubCitiID AND NewsCategoryID = @NewsCategoryID
	

			INSERT INTO NewsFirstSubCategorySettings(
													 NewsCategoryID
													,NewsSubCategoryID
													,HcHubCitiID
													,NewsFirstSubCategoryURL
													,DateCreated)
			SELECT @NewsCategoryID
				  ,NewsSubCategoryID
				  ,@HcHubCitiID
				  ,NewsFirstSubCategoryURL
				  ,GETDATE()
			FROM #NewsFirstSubCategoryURL N
			INNER JOIN #NewsSubCategoryID S ON N.Rownum=S.Rownum
		



	

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		throw;
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
