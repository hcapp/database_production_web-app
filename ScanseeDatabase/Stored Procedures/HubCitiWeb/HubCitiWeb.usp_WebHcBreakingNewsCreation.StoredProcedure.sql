USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBreakingNewsCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBreakingNewsCreation]
Purpose					: To Create news for the push Notification.
Example					: [usp_WebHcBreakingNewsCreation]

History
Version		Date			Author	             Change Description
--------------------------------------------------------------- 
1.0			04/09/2015	    SAGAR BYALI               1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBreakingNewsCreation]
(
    --Input variable
	 
	  @Title Varchar(2000)
	, @Link Varchar(2000)
	, @PublishedDate  Varchar(2000)
	, @Type Varchar(2000)
	, @Description Varchar(Max)
	, @ImagePath Varchar(2000)	
	
	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			--Loading Breaking news into main table from staging data 
			INSERT INTO HcNews(Title
							,PublishedDate
							,URL
							,HcNewsTypeID
							,Description
							,ImagePath
							,NotificationSentDate
							,DateCreated
							,CreatedUserID)
			SELECT   Title
					,PublishedDate
					,URL
					,HcNewsTypeID
					,Description
					,ImagePath
					,DateCreated
					,DateCreated
					,CreatedUserID
			FROM HcNewsStaging 

			--Clearing the staging table data 
			DELETE FROM HcNewsStaging

	        --Splitting the input data  
	        SELECT Rownum=Identity(Int,1,1)
			      ,Param Title
			INTO #Title
			FROM fn_SplitParam(@Title,'|~~|')

			SELECT Rownum=Identity(Int,1,1)
			      ,Param Link
			INTO #Link
			FROM fn_SplitParam(@Link,'|~~|')

			--SELECT *FROM #Link

			SELECT Rownum=Identity(Int,1,1)
			      ,Param PublishedDate
			INTO #PublishedDate
			FROM fn_SplitParam(@PublishedDate,'|~~|')
	
			--SELECT *FROM #PublishedDate

			SELECT Rownum=Identity(Int,1,1)
			      ,Param Type
			INTO #Type
			FROM fn_SplitParam(@Type,'|~~|') 

			--SELECT *FROM #Type

			SELECT Rownum=Identity(Int,1,1)
			      ,Param Description
			INTO #Description
			FROM fn_SplitParam(@Description,'|~~|')

			--SELECT *FROM #Description

			SELECT Rownum=Identity(Int,1,1)
			      ,Param ImagePath
			INTO #ImagePath
			FROM fn_SplitParam(@ImagePath,'|~~|')

			--SELECT *FROM #ImagePath

			SELECT DISTINCT T.Rownum 
			               ,NT.HcNewsTypeID 
			INTO #newsTypes
			FROM #Type T
			INNER JOIN HCnewsType NT ON NT.NewsType=T.Type 

		   --SELECT *FROM #newsTypes


			--Inserting the recieved input data into Newslist 
			SELECT DISTINCT T.Rownum
			      ,T.Title
				  ,L.Link 
				  ,P.PublishedDate 
				  ,NT.HcNewsTypeID
				  ,D.Description  
				  ,I.ImagePath
			INTO #NewsList
			FROM #Title T
			INNER JOIN #Link L ON L.Rownum =T.Rownum
			INNER JOIN #PublishedDate P ON P.Rownum =L.Rownum
			INNER JOIN #newsTypes NT ON NT.Rownum =T.Rownum
			INNER JOIN #Description D ON D.Rownum =T.Rownum
			INNER JOIN #ImagePath I ON I.Rownum =T.Rownum
			

			--SELECT *FROM #NewsList

			
			--Inserting data into staging table
			INSERT INTO HcNewsStaging(Title
									,PublishedDate
									,URL
									,HcNewsTypeID
									,Description
									,ImagePath
									,DateCreated)
			SELECT DISTINCT T.Title 
			      ,T.PublishedDate
				  ,T.Link
				  ,T.HcNewsTypeID 
				  ,T.Description 
				  ,T.ImagePath 
				  ,GETDATE()	  
			FROM #NewsList T
			LEFT JOIN HcNews N ON N.Title =T.Title AND T.Link =N.URL AND T.PublishedDate =N.PublishedDate 
			WHERE N.HcNewsID IS NULL


			--select  * from HcNewsStaging

			--Confirmation of Success
			SELECT @Status = 0
	
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
		
		   select ERROR_LINE()	 
			PRINT 'Error occured in Stored Procedure [usp_WebHcBreakingNewsCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
