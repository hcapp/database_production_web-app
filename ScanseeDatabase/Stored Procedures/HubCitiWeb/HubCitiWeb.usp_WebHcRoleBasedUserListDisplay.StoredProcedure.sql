USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRoleBasedUserListDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcRoleBasedUserListDisplay
Purpose					: Display list of Role Based User List.
Example					: usp_WebHcRoleBasedUserListDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/07/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRoleBasedUserListDisplay]
(
   
    --Input variable. 	
	  @HcHubcitiID Int
	, @SearchKey Varchar(1000)  
	, @HubcitiUserID Int
	, @LowerLimit int  
	, @ScreenName varchar(50)
	  
	  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	            
				DECLARE @UpperLimit int				

				--To get the row count for pagination.	 
				SELECT @UpperLimit = ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
						
				   
				--Display list of Role Based User List
								
				SELECT  ROW_NUMBER() OVER (Order BY U.userID) AS RowNum			
					  ,U.UserID	
				      ,U.UserName 
				      ,FirstName 
					  --,Lastname
					  --,RoleBasedModuleName module
					  --,IIF(RU.UserID = @HubcitiUserID AND F.RoleBasedUserID = @HubcitiUserID,'Events / Fundraisers','Events') module
					    , module=  REPLACE(STUFF((SELECT '/ ' + CAST(R.RoleBasedModuleName AS VARCHAR(2000))
                                                                                                     FROM HcUserRole F
																									 INNER JOIN HcRoleBasedModuleList R ON R.HcRoleBasedModuleListID =F.HcRoleBasedModuleListID
                                                                                                     WHERE HcAdminUserID=U.Userid
																									 --F.HcMenuItemID = MI.HcMenuItemID AND F.BusinessCategoryID =FI.BusinessCategoryID 
                          --                                                                           ORDER BY F.HcMenuFindRetailerBusinessCategoryID
                                                                                                     FOR XML PATH(''))
                                                                                                     , 1, 2, ''), ' ', '')
					 
					 
					 
					 
					 
					 
					  --, CASE WHEN (RU.UserID IS NOT NULL AND F.RoleBasedUserID IS NOT NULL) THEN 'Events / Fundraisers'
					  --       WHEN (RU.UserID IS NOT NULL OR F.RoleBasedUserID IS NULL) THEN 'Events' 
							-- WHEN (RU.UserID IS NULL OR F.RoleBasedUserID IS NOT NULL) THEN 'Fundraisers' END module
					  --,Usertype=IIF(RU.UserID IS NULL,'super','normal')
					  ,CASE WHEN (RU.UserID IS NOT NULL OR F.RoleBasedUserID IS NOT NULL) THEN 'Normal'
					         WHEN (RU.UserID IS NULL) THEN 'Super'   
							 WHEN (F.RoleBasedUserID IS NULL) THEN 'Super' END Usertype
					  ,U.Enabled 
				INTO #Temp
				FROM Users U
				INNER JOIN HcUserRole UR ON UR.HcAdminUserID =U.UserID
				INNER JOIN HcRoleBasedModuleAssociation RM ON RM.HcRoleID =UR.HcRoleID  
				INNER JOIN HcRoleBasedModuleList ML ON RM.HcRoleBasedModuleListID = ML.HcRoleBasedModuleListID
				LEFT JOIN HcRoleBasedUserEventCategoryAssociation RU ON RU.UserID =UR.HcAdminUserID 
				LEFT JOIN HcFundraisingRoleBasedUserCategoryAssociation F ON F.RoleBasedUserID = UR.HcAdminUserID
				LEFT JOIN UserType UT ON UT.UserTypeID =U.UserTypeID 
				WHERE (U.HcHubcitiID  =@HcHubcitiID OR U.CreatedUserID =@HubcitiUserID)
				AND UT.UserType IN ('Super User','Normal User')	
				AND (@SearchKey IS NULL OR (@SearchKey IS NOT NULL AND U.UserName LIKE '%'+@SearchKey+'%'))
				GROUP BY U.userID,U.UserName,FirstName,Enabled,RU.UserID,F.RoleBasedUserID


				--To capture max row number.  
				SELECT @MaxCnt = ISNULL(MAX(RowNum),0) FROM #Temp

				IF @LowerLimit IS NULL
				BEGIN
					SET @LowerLimit=0
					SET @UpperLimit=@MaxCnt					
				END

				--this flag is a indicator to enable "More" button in the UI.   
				--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@UpperLimit+@LowerLimit)) > 0 THEN 1 ELSE 0 END 

				SELECT  RowNum	
					  ,UserID roleUserId			
				      ,UserName 
				      ,FirstName 
					  ,module
					  ,Usertype
					  ,Enabled userStatus
				FROM #Temp 
				Order By Rownum 
                OFFSET @lowerlimit Rows
                FETCH NEXT @UpperLimit ROWS ONLY
							
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcRoleBasedUserListDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
