USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HcBandSummaryDetail]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[usp_HcBandSummaryDetail]]
Purpose					: To Display the basic infomration about Band Summary
Example					: [[usp_HcBandSummaryDetail]]

History
Version		Date				Author				Change Description
--------------------------------------------------------------- 
1.0			29th July 2016		Sagar Byali			Initial Version
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiWeb].[usp_HcBandSummaryDetail]
(
	  
	  @BandID int
	, @HubCitiID int 
    
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
	, @IOSAppID varchar(1000) output
	, @AndroidAppID varchar(1000) output
)
AS
BEGIN
	

	BEGIN TRY
	
		BEGIN TRANSACTION


			SELECT @IOSAppID = IOSAppID
				  ,@AndroidAppID = AndroidAppID
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID 
				
				--To declare and set URL
				DECLARE  @RibbonAdImagePath VARCHAR(1000)
						,@SSQRURL VARCHAR(1000)= 'http://10.122.61.145:8080/'
						,@ImagePath VARCHAR(1000)= 'http://10.122.61.146:8080/'
						,@RetailConfig varchar(1000)='http://10.122.61.145:8080/Images/retailer/'
						,@RibbonAdURL varchar(1000)
						,@BannerAdID varchar(1000)
						,@RetailerPageQRURL VARCHAR(1000)
						,@BandConfig VARCHAR(1000)
						,@Config VARCHAR(1000)
						,@DefaultBandBannerImage VARCHAR(500)= 'http://10.122.61.146:8080/Images/band/banner-default.jpg'

            

				--Get the associated Banner Ad Details.
				SELECT DISTINCT   @RibbonAdImagePath = @RetailConfig  + CAST(@BandID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
								, @RibbonAdURL = RLA.BannerAdURL  --Web set Banner URL
								, @BannerAdID = RLA.bandAdvertisementBannerID
				FROM bandAdvertisementBanner RLA
				WHERE RLA.bandID = @bandID AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1) AND RLA.StartDate = (SELECT MAX(StartDate) FROM bandAdvertisementBanner ASP WHERE BandID = @bandID)					 
				
			

				--Get the associated QRPage URL.
				SELECT DISTINCT @RetailerPageQRURL = CASE WHEN QR.QRBandCustomPageID IS NOT NULL THEN @Config  + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(@BandID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(QR.QRBandCustomPageID AS VARCHAR(10)) ELSE NULL END			
				FROM QRBandCustomPage QR
				LEFT JOIN QRBandCustomPageAssociation QRRCPA ON QR.QRBandCustomPageID = QRRCPA.QRBandCustomPageID
				INNER JOIN QRTypes Q ON QR.QRTypeID = Q.QRTypeID
				WHERE QR.BandID = @BandID AND Q.QRTypeName = 'Main Menu Page'



				SELECT DISTINCT bandID = R.BandId 
					 , bandName = R.BandName 
					 , bandaddress = R.City+','+R.State+','+R.PostalCode  
					 , bandURL = R.BandURL 

					  --Band Banner image
					 , bandBannerImg = ISNULL(@RibbonAdImagePath,@DefaultBandBannerImage)

					 --Web set Banner URL
					 , bannerImgURL = @RibbonAdURL  

					 --Band web, phone, mail image
					 , bandURLImgPath = @ImagePath + 'Images/band/web.png' 
					 , phoneImg = @ImagePath + 'Images/band/contact.png'
					 , mailImg = @ImagePath + 'Images/band/mail.png'

					 --Band phone & Email
					 , phoneNo = CASE WHEN LEN(ISNULL(R.CorporatePhoneNo,'')) = 0 THEN '('+STUFF(STUFF('9999999999',4,0,') '),8,0,'-') ELSE '(' + STUFF(STUFF(R.CorporatePhoneNo,4,0,') '),8,0,'-') END   
					 , bandEmail = Email  
					 
					 --Flag to set band is associated to an event
					 , isShowFlag = CASE WHEN EE.BandID IS NOT NULL THEN '1' ELSE '0' END
					 
					 --Band event image path
					 , bandEventImgPath = @ImagePath + 'Images/band/web.png' 
					 , bandEventName= 'Shows'

					 --SSQR Link
					 , QRURL = @SSQRURL + 'SSQR/qr/3002.htm?bandId='+cast(R.BandId as varchar)+'&hubcitiId=' + cast(@HubCitiID as varchar)		
				FROM Band R
				LEFT JOIN HcBandEvents EE ON EE.BandID = R.BandID AND R.BandId = @BandID
				WHERE R.BandId = @BandID	
				
				
			
		   --Confirmation of Success.
		   SELECT @Status = 0	


	   COMMIT TRANSACTION
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			THROW;
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;


GO
