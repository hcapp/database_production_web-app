USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventMarkerDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventMarkerDetails
Purpose					: Display  Event Marker Details.
Example					: usp_WebHcHubCitiEventMarkerDetails

History
Version		Date			    Author			Change Description
--------------------------------------------------------------- 
1.0			26thMarch2015	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventMarkerDetails]
(
   
    --Input variable. 	  
	  @EventMarkerID Int	
	, @UserID Int
	, @HubCitiID int
	, @HcEventsID Int
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

				DECLARE @EventConfig Varchar(500) 
				SELECT @EventConfig =ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'

	               
				--Display  Event Marker Details.
								
				SELECT DISTINCT HcEventsOverLayMarkerDetailID evtMarkerId
							   ,HcEventID hcEventID
							   ,EventMarkerName evtMarkerName
							   ,EventMarkerImagePath evtMarkerImgPath
							   ,@EventConfig+CAST(@HubCitiID AS VARCHAR)+ '/events/'+CAST(@HcEventsID AS VARCHAR)+'/'+EventMarkerImagePath eventImageName 
							   ,Latitude latitude
							   ,Longitude logitude
				FROM HcEventsOverLayMarkerDetails
				WHERE HcEventsOverLayMarkerDetailID=@EventMarkerID 						
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventMarkerDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
