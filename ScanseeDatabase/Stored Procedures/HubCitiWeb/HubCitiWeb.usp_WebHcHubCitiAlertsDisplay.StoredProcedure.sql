USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsDisplay
Purpose					: To display List fo Alerts Severity.
Example					: usp_WebHcHubCitiAlertsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @UserID Int
	, @SearchParameter Varchar(2000)
	, @LowerLimit int  
	, @ScreenName varchar(50)
  
	--Output Variable
	, @MaxCnt int  output
	, @NxtPageFlag bit output   
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   
				

			--To display List fo Alerts Severity
			SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY A.HCAlertID) AS Rownum			
			               ,A.HcAlertID 			               
			               ,HcAlertName 
			               ,ShortDescription  						
						   ,A.HcHubCitiID 
						   ,HcSeverity  
						   ,StartDate =CAST(StartDate AS DATE)
						   ,EndDate =CAST(EndDate AS DATE)
						   ,StartTime =CAST(StartDate AS Time)
						   ,EndTime =CAST(EndDate AS Time)
						   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
											  FROM HcMenuItem MI 
											  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
											  WHERE LinkTypeName = 'Alerts' 
											  AND MI.LinkID = A.HcAlertID)>0 THEN 1 ELSE 0 END	
						  ,AA.HcAlertCategoryName 					  											
			INTO #Alerts
			FROM HcAlerts A
			INNER JOIN HcSeverity S ON S.HcSeverityID =A.HcSeverityID  		
			INNER JOIN HcAlertCategoryAssociation AC ON AC.HcAlertID =A.HcAlertID
			INNER JOIN HcAlertCategory AA ON AA.HcAlertCategoryID =AC.HcAlertCategoryID  	
			WHERE A.HcHubCitiID=@HubCitiID AND (( @SearchParameter IS NOT NULL AND HcAlertName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
			ORDER BY EndDate DESC	
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Alerts
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			SELECT DISTINCT RowNum
			               ,HcAlertID alertId		               
			               ,HcAlertName title
			               ,ShortDescription description						
						   ,HcHubCitiID 
						   ,HcSeverity Severity
						   ,StartDate
						   ,EndDate
						   ,StartTime
						   ,EndTime
						   ,MenuItemExist
						   ,HcAlertCategoryName Category					  											
			
			FROM #Alerts 		
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY RowNum 
					
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
