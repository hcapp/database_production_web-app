USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsListDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNewsListDisplay]
Purpose					: To Display List of news.
Example					: [usp_WebHcNewsListDisplay]

History
Version		Date			Author	              Change Description
--------------------------------------------------------------- 
1.0			01/19/2015	    Dhananjaya TR		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsListDisplay]
(
    --Input variable	
	  @HcNewsTypeId int	
	
	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
	
		    --To get Media Server Configuration.  
			DECLARE @Config varchar(500)    
			SELECT @Config=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='WebRssFeedDefaultImage'   		
			
			SELECT Rownum=identity(int,1,1)
				,Title 
				,PublishedDate
				,URL
				,HcNewsTypeID 
				,Description 
				,ImagePath
			INTO #News
			FROM
			(
			SELECT DISTINCT Title 
						  ,PublishedDate
						  ,URL
						  ,HcNewsTypeID 
						  ,Description 
						  ,IIF(ImagePath='' OR ImagePath='null',@Config,ImagePath) ImagePath					 		  
			FROM HcNewsStaging 
			WHERE HcNewsTypeID = @HcNewsTypeId)A

			SELECT Rownum id
				,Title title
				,PublishedDate date
				,URL link
				,HcNewsTypeID 
				,Description description
				,ImagePath image
			FROM #News



			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsListDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
