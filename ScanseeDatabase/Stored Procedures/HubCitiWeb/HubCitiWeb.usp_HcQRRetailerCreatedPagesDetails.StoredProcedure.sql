USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HcQRRetailerCreatedPagesDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcQRRetailerCreatedPagesDetails
Purpose					: To Display the Retailer Created Page Details.
Example					: usp_HcQRRetailerCreatedPagesDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th May 2012	SPAN	Initial Version
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiWeb].[usp_HcQRRetailerCreatedPagesDetails]
(	  
	  @RetailID int
    , @RetailLocationID int
    , @PageID int
	, @HubCitiID int
    
    --User Tracking Imputs
    , @ScanTypeID bit
    , @MainMenuID int
       
	--Output Variable  
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output  
	, @ExternalLinkFlag bit output
	, @ExternalLink varchar(1000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		
		     
		
			 DECLARE @RetailerConfig varchar(50)
			 DECLARE @MediaTypes VARCHAR(1000) = ''
			 DECLARE @MediaPath VARCHAR(MAX) = ''
			 DECLARE @ExternalFlag varchar(100) = ''
			 DECLARE @RetailLocations VARCHAR(1000) = ''
			 DECLARE @ScanType int

			 SELECT @IOSAppID = IOSAppID
					  ,@AndroidAppID = AndroidAppID
			 FROM HcHubCiti
			 WHERE HcHubCitiID = @HubCitiID
							 
			 SELECT @ScanType = ScanTypeID
		     FROM HubCitiReportingDatabase..ScanType
		     WHERE ScanType = 'Retailer Anything Page QR Code'

			 DECLARE @RetailerConfig2 varchar(50)
			 SELECT @RetailerConfig2= ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='QR Code Configuration'
			 AND Active = 1
		     
		     CREATE TABLE #AnythingPages(pageID INT
									  , Pagetitle VARCHAR(1000)
									  , PageDescription VARCHAR(Max)
									  , ShortDescription VARCHAR(Max)
									  , LongDescription VARCHAR(Max)
									  , ImageName VARCHAR(1000)
									  , ImagePath VARCHAR(1000)
									  , MediaType VARCHAR(10)
									  , MediaPath VARCHAR(1000)
									  , ExternalFlag BIT
									  , RetailID INT	 
									  , StartDate DATETIME
									  , EndDate DATETIME
									  , Expired BIT
									  , RetailName VARCHAR(500)
									  , Logo VARCHAR(1000)
									  , QRType VARCHAR(100)
									  , AppDownloadLink VARCHAR(1000)
									  , QRUrl VARCHAR(1000))
		     
		     
			 --User Tracking
			 DECLARE @Rowcount int
			
			 --Get the server configuration.
			 SELECT @RetailerConfig= ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'

			 IF (@RetailID IS NOT NULL AND @RetailLocationID IS NOT NULL)
			 BEGIN

			  SELECT @ExternalLinkFlag = CASE WHEN (B.QRRetailerCustomPageID IS NOT NULL AND B.MediaPath LIKE '%pdf%' OR B.MediaPath LIKE '%png%' OR B.MediaPath LIKE '%html%') OR (A.URL IS NOT NULL) THEN 1
										 ELSE 0 
									END
		      FROM QRRetailerCustomPage A
		      LEFT JOIN QRRetailerCustomPageMedia B ON A.QRRetailerCustomPageID = B.QRRetailerCustomPageID
		      WHERE A.QRRetailerCustomPageID = @PageID

			  SELECT 
				   QRTypeCode
				 , QRRCP.RetailID		       
				 , QRRCP.QRRetailerCustomPageID  
				 , COUNT(QRRCPA.RetailLocationID) Counts
			 INTO #Temp
			 FROM QRRetailerCustomPage QRRCP  
			 INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
			 INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCP.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
			 WHERE (QRRCPA.QRRetailerCustomPageID = @PageID
			 AND QRRCP.RetailID = @RetailID) 
			 GROUP BY QRTypeCode
					, QRRCP.RetailID		     
					, QRRCP.QRRetailerCustomPageID
			 
			 
			 --Send the details of the anything page if the page is found to be an internal(Make your own).
			 IF @ExternalLinkFlag = 0
			 BEGIN
				--Get Associated Media Details
				 SELECT  @MediaTypes = COALESCE(@MediaTypes+',','') + CAST(PMT.ProductMediaType AS VARCHAR(10))
					   , @MediaPath = COALESCE(@MediaPath+',','') + @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + CAST(QR.MediaPath AS VARCHAR(100))
				 FROM QRRetailerCustomPageMedia QR
				 INNER JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = QR.MediaTypeID
				 WHERE QRRetailerCustomPageID = @PageID	 
				
				--If the Custom Page is associated with multiple locations.
				IF @RetailLocationID = 0
				BEGIN 
							--Get Page Details 
							INSERT INTO #AnythingPages(pageID  
									  , Pagetitle  
									  , PageDescription 
									  , ShortDescription  
									  , LongDescription  
									  , ImageName  
									  , ImagePath  
									  , MediaType  
									  , MediaPath  
									  , ExternalFlag 
									  , RetailID  	 
									  , StartDate  
									  , EndDate  
									  , Expired  
									  , RetailName  
									  , Logo  
									  , QRType  
									  , AppDownloadLink
									  , QRUrl)
							SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
								 , QRRCP.Pagetitle
								 , QRRCP.PageDescription
								 , QRRCP.ShortDescription
								 , QRRCP.LongDescription
								 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID)
									ELSE QRRCP.Image END  ImageName
								 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT @RetailerConfig + CAST(QRRetailerCustomPageIconImagePath AS VARCHAR(100)) FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID) 
									ELSE @RetailerConfig + CAST(QRRCP.RetailID AS VARCHAR(10)) + '/'+ CAST(Image AS VARCHAR(100)) END	ImagePath
								 , SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
								 , SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
								 , SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
								 , QRRCP.RetailID			 
								 , QRRCP.StartDate
								 , QRRCP.EndDate
								 , Expired = CASE WHEN CAST(GETDATE() AS DATE) > ISNULL(EndDate, GETDATE()+1) THEN 1
											 ELSE 0 END
								 , R.RetailName
								 , @RetailerConfig + CAST(R.RetailID AS VARCHAR(10))+ '/' + R.RetailerImagePath Logo
								 , QRT.QRTypeName QRType
								 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
								 , QRUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL									 
									 THEN CASE WHEN Counts > 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10)) +'&source=HubCiti&EL=true'
											   WHEN COUNTS = 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti&EL=true'
										  END 
									 ELSE			
									 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
										  CASE WHEN Counts > 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+ '&EL=false'
											   WHEN Counts = 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti&EL=false'
										  END
									 END		      
							FROM QRRetailerCustomPage QRRCP
							INNER JOIN #Temp T ON QRRCP.QRRetailerCustomPageID = T.QRRetailerCustomPageID
							INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID	
							INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
							INNER JOIN Retailer R ON R.RetailID = QRRCP.RetailID 
							LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID
							WHERE (QRRCP.RetailID = @RetailID
							AND QRRCP.QRRetailerCustomPageID = @PageID) 
							
							SELECT @Rowcount=@@ROWCOUNT 		 
					   END
					   
					   --If Custom page is associated with single location.
					   IF @RetailLocationID <> 0
					   BEGIN
						--Get Page Details 
						INSERT INTO #AnythingPages(pageID  
									  , Pagetitle  
									  , PageDescription 
									  , ShortDescription  
									  , LongDescription  
									  , ImageName  
									  , ImagePath  
									  , MediaType  
									  , MediaPath  
									  , ExternalFlag 
									  , RetailID  	 
									  , StartDate  
									  , EndDate  
									  , Expired  
									  , RetailName  
									  , Logo  
									  , QRType  
									  , AppDownloadLink
									  , QRUrl)
							SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
								 , QRRCP.Pagetitle
								 , QRRCP.PageDescription
								 , QRRCP.ShortDescription
								 , QRRCP.LongDescription
								 , QRRCP.Image ImageName
								 , @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + QRRCP.Image	ImagePath
								 , SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
								 , SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
								 , SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
								 , QRRCP.RetailID			 
								 , QRRCP.StartDate
								 , QRRCP.EndDate
								 , Expired = CASE WHEN GETDATE() > EndDate THEN 1
											 ELSE 0 END
								 , R.RetailName
								 , @RetailerConfig + CAST(R.RetailID AS VARCHAR(10))+ '/' + R.RetailerImagePath Logo
								 , QRT.QRTypeName QRType
								 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
								 , QRUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL									 
									 THEN CASE WHEN Counts > 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10)) +'&source=HubCiti&EL=true'
											   WHEN COUNTS = 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=true'
										  END 
									 ELSE			
									 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
										  CASE WHEN Counts > 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+ '&source=HubCiti&EL=false'
											   WHEN Counts = 1 THEN @RetailerConfig2 + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti&EL=false'
										  END
									 END	
							FROM QRRetailerCustomPage QRRCP
							INNER JOIN #Temp T ON QRRCP.QRRetailerCustomPageID = T.QRRetailerCustomPageID
							INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID	
							INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
							INNER JOIN Retailer R ON R.RetailID = QRRCP.RetailID 
							LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID 
							WHERE (QRRCP.RetailID = @RetailID
							AND QRRCP.QRRetailerCustomPageID = @PageID
							AND QRRCPA.RetailLocationID = @RetailLocationID)
							
							--User Tracking
							SELECT @Rowcount=@@ROWCOUNT
					   END
		   END
		   
		   --send the link if the page is found to be an external one(Link to existing or Upload PDF or image).
		   ELSE IF @ExternalLinkFlag = 1
		   BEGIN
				 INSERT INTO #AnythingPages(pageID)
				 VALUES(@PageID)
				 
				 SELECT @ExternalLink = CASE WHEN B.QRRetailerCustomPageID IS NOT NULL AND B.MediaPath LIKE '%pdf%' OR B.MediaPath LIKE '%png%'  OR B.MediaPath LIKE '%html%' 
												THEN @RetailerConfig + CAST(A.RetailID AS VARCHAR(10)) + '/' + B.MediaPath
											 ELSE A.URL
										 END
				 FROM QRRetailerCustomPage A
				 LEFT JOIN QRRetailerCustomPageMedia B ON A.QRRetailerCustomPageID = B.QRRetailerCustomPageID
				 WHERE A.QRRetailerCustomPageID = @PageID
				 
				 --User Tracking
				  SELECT @Rowcount=@@ROWCOUNT
		   END
		   
		   
		   --User Tracking
			
		   --Check if the user has scanned the page and capture in the scan related User Tracking tables.
		   IF @ScanTypeID  = 1
		   BEGIN
			   INSERT INTO HubCitiReportingDatabase..scan(MainMenuID
		     											 ,ScanTypeID
														 ,Success
														 ,CreatedDate
														 ,ID)
					SELECT @MainMenuID 
						  ,@ScanType 
						  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
						  ,GETDATE()
						  , pageID
					FROM #AnythingPages	
					
					INSERT INTO HubCitiReportingDatabase..AnythingPageList(AnythingPageID, MainMenuID, AnythingPageClick, DateCreated)
					SELECT pageID
						 , @MainMenuID
						 , 1
						 , GETDATE()
					FROM #AnythingPages					
		    END 		  
		    --Capture the click if the user has come through retailer summary.  
		    IF @ScanTypeID = 0
		    BEGIN
				UPDATE HubCitiReportingDatabase..AnythingPageList SET AnythingPageClick = 1
				WHERE MainMenuID = @MainMenuID AND AnythingPageID = @PageID
		    END
		   --Display the result set if it is an internal page.
		   IF @ExternalLinkFlag = 0
		   BEGIN
			   SELECT  pageID
					 , Pagetitle
					 , PageDescription
					 , ShortDescription
					 , LongDescription
					 , ImageName
					 , ImagePath
					 , MediaType
					 , MediaPath	
					 , ExternalFlag 
					 , RetailID			 
					 , StartDate
					 , EndDate
					 , Expired 
					 , RetailName
					 , Logo
					 , QRType
					 , AppDownloadLink 
					 , QRUrl
			FROM #AnythingPages
		END
	  END
	  ELSE
	  BEGIN

		DECLARE @HubCitiConfig VARCHAR(1000)
		SELECT @HubCitiConfig = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration' 

		DECLARE @RetailerConfig1 varchar(50)
		SELECT @RetailerConfig1= ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='QR Code Configuration'
		AND Active = 1

		SELECT @ExternalLinkFlag = CASE WHEN (B.HcAnythingPageID IS NOT NULL AND B.MediaPath LIKE '%pdf%' OR B.MediaPath LIKE '%png%' OR B.MediaPath LIKE '%html%') OR (A.URL IS NOT NULL) THEN 1
										 ELSE 0 
									END
		FROM HcAnythingPage A
		LEFT JOIN HcAnythingPageMedia B ON A.HcAnythingPageID = B.HcAnythingPageID
		WHERE A.HcAnythingPageID = @PageID

		--Send the details of the anything page if the page is found to be an internal(Make your own).
		IF @ExternalLinkFlag = 0
		BEGIN
		--Get Associated Media Details
			SELECT  @MediaTypes = COALESCE(@MediaTypes+',','') + CAST(PMT.HcAnyThingPageMediaType AS VARCHAR(10))
				  , @MediaPath = COALESCE(@MediaPath+',','') + @HubCitiConfig + CAST(@HubCitiID AS VARCHAR(10))+ '/' + CAST(QR.MediaPath AS VARCHAR(100))				
			FROM HcAnythingPageMedia QR
			INNER JOIN HcAnyThingPageMediaType PMT ON PMT.HcAnyThingPageMediaTypeID = QR.MediaTypeID
			WHERE HcAnythingPageID = @PageID
		END
		--send the link if the page is found to be an external one(Link to existing or Upload PDF or image).
		ELSE IF @ExternalLinkFlag = 1
		BEGIN
				INSERT INTO #AnythingPages(pageID)
				VALUES(@PageID)
				 
				SELECT @ExternalLink = CASE WHEN B.HcAnythingPageID IS NOT NULL AND B.MediaPath LIKE '%pdf%' OR B.MediaPath LIKE '%png%' OR B.MediaPath LIKE '%html%'
											THEN @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(10)) + '/' + B.MediaPath
											ELSE A.URL
										END
				FROM HcAnythingPage A
				LEFT JOIN HcAnythingPageMedia B ON A.HcAnythingPageID = B.HcAnythingPageID
				WHERE A.HcAnythingPageID = @PageID
				 
				--User Tracking
				SELECT @Rowcount=@@ROWCOUNT
		END
		 --User Tracking
			
		   --Check if the user has scanned the page and capture in the scan related User Tracking tables.
		   IF @ScanTypeID  = 1
		   BEGIN
			   INSERT INTO HubCitiReportingDatabase..scan(MainMenuID
		     											 ,ScanTypeID
														 ,Success
														 ,CreatedDate
														 ,ID)
					SELECT @MainMenuID 
						  ,@ScanType 
						  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
						  ,GETDATE()
						  , pageID
					FROM #AnythingPages	
					
					INSERT INTO HubCitiReportingDatabase..AnythingPageList(AnythingPageID, MainMenuID, AnythingPageClick, DateCreated)
					SELECT pageID
						 , @MainMenuID
						 , 1
						 , GETDATE()
					FROM #AnythingPages					
		    END 		  
		    --Capture the click if the user has come through retailer summary.  
		    IF @ScanTypeID = 0
		    BEGIN
				UPDATE HubCitiReportingDatabase..AnythingPageList SET AnythingPageClick = 1
				WHERE MainMenuID = @MainMenuID AND AnythingPageID = @PageID
		    END
		   		 

		INSERT INTO #AnythingPages(PageID
										 ,PageTitle
										 ,ImagePath
										 ,QRUrl
										 ,ShortDescription
										 ,LongDescription
										 ,ImageName
										 ,StartDate
										 ,EndDate
										 ,Expired
										 ,MediaType  
									     ,MediaPath  
									     ,ExternalFlag
										 ,QRType)
			SELECT   pageID 
			        ,pagetitle
					,retImage
					,QRUrl	
					,ShortDescription
					,LongDescription
					,ImageName
				    ,StartDate
				    ,EndDate
					,Expired
					,MediaType  
				    ,MediaPath  
					,ExternalFlag 
					,QRType
					
			FROM
			(SELECT A.HcanythingpageID  pageID
				  ,AnythingPageName pageTitle					
				  ,retImage = IIF(A.ImageIcon IS NOT NULL, @RetailerConfig + QRRetailerCustomPageIconImagePath, @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + ImagePath)					
				  --,QRUrl = @RetailerConfig +'2100.htm?key1='+ CAST(A.HcAnythingPageID AS VARCHAR(10))
				  --, PageLink = @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') as varchar(10)) + '.htm?retId=' + CAST(QRCPA.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))+ CASE WHEN QR.URL IS NOT NULL OR M.MediaPath LIKE '%pdf' OR M.MediaPath LIKE '%png' THEN '&EL=true' ELSE '&EL=false'	END
		
				  ,QRUrl = @RetailerConfig1 +'2100.htm?retId=null'+ '&retlocId=null&pageId='+ CAST(@PageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti'+ CASE WHEN A.URL IS NOT NULL OR B.MediaPath LIKE '%pdf' OR B.MediaPath LIKE '%png' OR B.MediaPath LIKE '%html%' THEN '&EL=true' ELSE '&EL=false'	END	
				  ,ShortDescription
				  ,LongDescription
				  ,ImagePath ImageName
				  ,StartDate
				  ,EndDate
				  ,Expired = CASE WHEN GETDATE() > EndDate THEN 1
											 ELSE 0 END
				  ,SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
				  ,SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
				  ,SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
				  ,QRType = 'Anything Page'
				  --,ImageIcon imageIconId
				  --,URL pageLink
				  --,MediaPath = @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + MediaPath
				  --,Mediapath pathName
			      --,CASE WHEN URL IS NULL OR URL = '' THEN  HcAnyThingPageMediaTypeID ELSE (SELECT HcAnyThingPageMediaTypeID FROM HcAnyThingPageMediaType WHERE HcAnyThingPageMediaType = 'Website') END pageType				
				  --,CASE WHEN MediaPath IS NOT NULL THEN (SELECT @HubCitiConfig + CAST(@HubCitiID AS VARCHAR(10))+ '/' + MediaPath FROM HcAnythingPageMedia WHERE HcAnythingPageMediaID = B.HcAnythingPageMediaID) END pageTempLink				  
			FROM HcAnythingPage A
			LEFT JOIN HcAnythingPageMedia B ON A.HcAnythingPageID = B.HcAnythingPageID
			LEFT JOIN QRRetailerCustomPageIcons D ON D.QRRetailerCustomPageIconID = A.ImageIcon
			LEFT JOIN HcAnyThingPageMediaType C ON C.HcAnyThingPageMediaTypeID = B.MediaTypeID		
			WHERE A.HcAnythingPageID = @PageID AND HcHubCitiID = @HubCitiID) A

			 --Display the result set if it is an internal page.
		   IF @ExternalLinkFlag = 0
		   BEGIN
		   SELECT * FROM #AnythingPages
		   END
	  END	
	  		 
		   
		  --Confirmation of Success.
		  SELECT @Status = 0
		COMMIT TRANSACTION   
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


--------------













GO
