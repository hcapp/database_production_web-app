USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcGeneralSettingsDisply]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcGeneralSettingsDisply
Purpose					: To display menu genaral setting details.
Example					: usp_WebHcGeneralSettingsDisply

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/11/2013	    Dhananjaya TR		1.0
1.1			11/15/2016		Bindu T A			1.1 - News Ticker Changes
1.2			13/12/2016		Sagar Byali			Claim changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcGeneralSettingsDisply]
(   
    --Input variable.	  
	  @HubCitiID int
	, @SettingsType varchar(500)
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config VARCHAR(500)
					,@NewsFirstFlag BIT
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'			
			
			------------- Banner Image Display for News -------------------
			
			DECLARE  @NewsBannerImage Varchar(500) = ''
			SELECT   @NewsBannerImage = NewsBannerImage, @NewsFirstFlag = NewsFirstFlag FROM HcHubCiti WHERE hchubcitiid = @HubCitiID
		
			---------------------------------------------------------------

			IF @SettingsType = 'TabBarLogo'
			BEGIN
				--To display List fo AppSites
				SELECT @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+SmallLogo LogoPath
				      ,SmallLogo LogoImageName
				      ,HU.HcBottomButtonTypeID bottomBtnId
				      ,HB.ButtonType bottomBtnName
				      ,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HC.AppIcon imagePath
				      ,HC.AppIcon bannerImageName
					  ,HU.homeIconName homeIconName
					  ,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName homeIconPath
					  ,HU.backButtonIconName  backButtonIconName
					  ,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName backButtonIconPath
					  ,HU.backGroundColor backGroundColor
					  ,HU.titleColor titleColor
					  --Claim Changes
					  ,HC.HubCitiClaimURL hubCitiClaimURL
					  ,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+ClaimImage claimImagePath
					  ,HU.ClaimImage claimImage 
					  ,HU.ClaimText claimText
					,ClaimBannerText1,
					ClaimBannerText2,
					ClaimBannerText3,
					ClaimBannerText4
				FROM HcHubCiti HC
				LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
				LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
				WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					
			END

			IF @SettingsType = 'News'
			BEGIN
				SELECT newsFeedText,
					   tickerBackground,
					   @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+NewsHamburgerIcon  sidemenuImage,
				       NewsHamburgerIcon sidemenuImageName
					  ,ISNULL(@NewsFirstFlag,'0') newsFirstFlag
				FROM HcMenuCustomUI
				WHERE HubCitiID = @HubCitiID
			END
			
			IF @SettingsType = 'MainMenu'
			BEGIN
				--Display Main Menu genaral setting details
				SELECT  MenuBackgroundColor BgColor
					   ,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+MenuBackgroundImage LogoPath
					   ,MenuBackgroundImage LogoImageName
					   ,MenuButtonColor btnColor
					   ,MenuButtonFontColor BtnFontColor
					   ,HC.HcBottomButtonTypeID
					   ,HB.ButtonType
					   ,MenuGroupBackgroundColor grpColor
					   ,MenuGroupFontColor grpFontColor
					   ,MenuIconicFontColor iconsFontColor	
					   ,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+@NewsBannerImage newsBanner
					   ,ISNULL(HC.MenuPgntdColorActive,'#000000') PgntdColorActive 
					   ,ISNULL(HC.MenuPgntdColorInActive,'#808080') PgntdColorInActive 	   
				FROM HcMenuCustomUI HC
				LEFT JOIN HcBottomButtonTypes HB ON HC.HcBottomButtonTypeID = HB.HcBottomButtonTypeId
				WHERE HubCitiId = @HubCitiID AND (MenuBackgroundImage IS NOT NULL OR MenuButtonColor IS NOT NULL OR MenuButtonFontColor IS NOT NULL OR MenuBackgroundColor IS NOT NULL OR MenuGroupBackgroundColor IS NOT NULL OR MenuGroupFontColor IS NOT NULL)
			END
			
			IF @SettingsType = 'SubMenu'
			BEGIN
				--Display Sub Menu genaral setting details
				SELECT  SubMenuBackgroundColor BgColor
						,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+SubMenuBackgroundImage LogoPath
						,SubMenuBackgroundImage LogoImageName
						,SubMenuButtonColor btnColor
						,SubMenuButtonFontColor BtnFontColor	
						,HC.HcBottomButtonTypeID
					    ,HB.ButtonType	 
						,SubMenuGroupBackgroundColor grpColor
					    ,SubMenuGroupFontColor grpFontColor	
						,SubMenuIconicFontColor iconsFontColor
						,ISNULL(HC.SubMenuPgntdColorActive,'#000000') PgntdColorActive 
					    ,ISNULL(HC.SubMenuPgntdColorInActive,'#808080') PgntdColorInActive 
				FROM HcMenuCustomUI HC
				LEFT JOIN HcBottomButtonTypes HB ON HC.HcBottomButtonTypeID = HB.HcBottomButtonTypeId
				WHERE HubCitiId = @HubCitiID AND (SubMenuBackgroundImage IS NOT NULL OR SubMenuButtonColor IS NOT NULL OR SubMenuButtonFontColor IS NOT NULL OR SubMenuBackgroundColor IS NOT NULL OR SubMenuGroupBackgroundColor IS NOT NULL OR SubMenuGroupFontColor IS NOT NULL)
			END
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcGeneralSettingsDisply.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
