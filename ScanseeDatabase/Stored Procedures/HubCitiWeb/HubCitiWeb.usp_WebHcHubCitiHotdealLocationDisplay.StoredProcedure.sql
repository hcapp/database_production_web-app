USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiHotdealLocationDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiHotdealLocationDisplay
Purpose					: To display list of Location Details.
Example					: usp_WebHcHubCitiHotdealLocationDisplay

History
Version		Date			Author			   Change Description
--------------------------------------------------------------- 
1.0			30April2015 	Dhananjaya TR		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiHotdealLocationDisplay]
(   
    --Input variable.	  
	  @ProductHotdealID int	
  
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		   
	
			DECLARE @RetailConfig Varchar(100)
			DECLARE @Config Varchar(100)
			DECLARE @RetailerEvent int

						
			SELECT @RetailConfig = ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'  
			
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'  
			
			--To display List fo appsites associated to events.
			
				SELECT RL.RetailLocationID appSiteId					 			  
					  ,appSiteImg = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
					  ,RL.Address1 Address
					  ,RL.City 
					  ,RL.[State] 
					  ,RL.PostalCode 				   	   
				FROM ProductHotDealRetailLocation  PR				
				INNER JOIN RetailLocation RL ON PR.RetailLocationID = RL.RetailLocationID 
				INNER JOIN Retailer R ON RL.RetailID = R.RetailID
				WHERE ProductHotDealID  = @ProductHotdealID AND (RL.Active=1 AND R.RetailerActive=1)
			
									
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiHotdealLocationDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
