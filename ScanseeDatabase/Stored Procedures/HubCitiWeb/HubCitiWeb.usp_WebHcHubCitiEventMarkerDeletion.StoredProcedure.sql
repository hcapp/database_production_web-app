USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventMarkerDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcHubCitiEventMarkerDeletion]
Purpose					: To delete Marker detaile for a hubciti.
Example					: [usp_WebHcHubCitiEventMarkerDeletion] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14thNov2013     Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventMarkerDeletion]
(
	  
	  @EventMarkerID int
	
	--Output Variable	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
					 
		    --Swipe delete from Scan history table.

					DELETE FROM HcEventsOverLayMarkerDetails
					WHERE HcEventsOverLayMarkerDetailID=@EventMarkerID  
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcHubCitiEventMarkerDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





















GO
