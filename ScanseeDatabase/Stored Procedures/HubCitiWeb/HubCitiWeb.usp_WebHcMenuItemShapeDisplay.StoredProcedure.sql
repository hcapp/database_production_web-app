USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcMenuItemShapeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcMenuItemShapeDisplay]
Purpose					: To display list of BMenuItem Shapes.
Example					: [usp_WebHcMenuItemShapeDisplay]

History
Version		Date			Author			  Change Description
--------------------------------------------------------------- 
1.0			5thFeb2014	    Dhananjaya TR	  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcMenuItemShapeDisplay]
(
    --Input variable.    
      
	--Output Variable 
      @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN


	BEGIN TRY
	 BEGIN TRANSACTION       
		
		--To display list of BMenuItem Shapes.
		SELECT DISTINCT HcMenuItemShapeID comboBtnTypeId
				        ,HcMenuItemShape comboBtnType
		FROM HcMenuItemShape					
		ORDER BY HcMenuItemShape ASC
		
								
	    --Confirmation of Success.
		SELECT @Status = 0

		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcMenuItemShapeDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
