USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[[usp_WebHcNewsCategoryUpdation&Deletion]]]
Purpose					: To delete news settings
Example					: [[[usp_WebHcNewsCategoryUpdation&Deletion]]]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[usp_WebHcFindCategoryimagesList]
----------------------------------------------------------------------------------------------
*/
--exec [HubCitiWeb].[usp_WebHcNewsFirstCategoryUpdation&Deletion] 1,null,null,null,null,null,null,null
CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryDisplay]
(   
	--Input variable
	  @HcHubCitiID int
	, @NewsCategoryID INT


	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			SELECT NewsSubCategoryID
			INTO #Selected
			FROM NewsFirstSettings N
			INNER JOIN NewsfirstSubCategorySettings T ON T.NewsCategoryID = N.NewsCategoryID
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID= N.NewsCategoryID AND A.SubCategoryID = T.NewsSubCategoryID AND A.HcHubcitiID = @HcHubCitiID 
			WHERE N.NewsCategoryID = @NewsCategoryID AND N.HcHubCitiID = @HcHubCitiID AND T.HcHubCitiID = @HcHubCitiID
			
			SELECT NewsSubCategoryID	  
			INTO #NotSeleted
			FROM NewsFirstCategory N
			INNER JOIN NewsFirstSubCategoryType T ON T.NewsCategoryID = N.NewsCategoryID
			INNER JOIN NewsFirstSubCategory S ON S.NewsSubCategoryTypeID = T.NewsSubCategoryTypeID
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = N.NewsCategoryID AND A.SubCategoryID = S.NewsSubCategoryID AND A.hchubcitiID = @HcHubCitiID
			WHERE N.NewsCategoryID = @NewsCategoryID AND S.NewsSubCategoryID NOT IN (SELECT NewsSubCategoryID FROM #Selected)
		

			SELECT distinct S.NewsSubCategoryID subCatIds
				  ,SS.NewsSubCategoryDisplayValue subCatName
				  ,CAST(S.NewsFirstSubCategoryURL AS VARCHAR(1000)) subCatURL
			FROM #Selected T
			INNER JOIN NewsFirstSubCategorySettings S ON S.NewsSubCategoryID = T.NewsSubCategoryID ANd HcHubCitiID = @HcHubCitiID
			INNER JOIN NewsFirstSubCategory SS ON SS.NewsSubCategoryID = S.NewsSubCategoryID

			UNION
					
			SELECT distinct S.NewsSubCategoryID subCatIds
				  ,S.NewsSubCategoryDisplayValue subCatName
				  ,NULL subCatURL
			FROM #NotSeleted T
			INNER JOIN NewsFirstSubCategory S ON S.NewsSubCategoryID = T.NewsSubCategoryID

			

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
