USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDealoftheDayList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDelaoftheDayList]
Purpose					: To Display Filters for given HubCiti.
Example					: [usp_WebHcDelaoftheDayList]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
            15/12/2015      Sagar Byali   Added 2 parameters in the Resultset (sStartDate, sEndDate)
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDealoftheDayList]
(
    --Input variable
	  @UserID int
	, @HcHubCitiID Int
	, @DealsName Varchar(100)
	, @SearchKey Varchar(500)
	, @LowerLimit Int
	, @ScreenName Varchar(25)
	
	
	--Output Variable 
	, @DealName Varchar(100) OUTPUT
	, @DealID Int Output
	, @MaxCnt int  output
    , @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		   DECLARE @QRTypeID INT
				   , @DealFlagName Varchar(255)		  
				   , @UpperLimit int 
				   , @ScheduledFlag BIT = 0

		  --To get the row count for pagination.
		   SELECT @UpperLimit = @LowerLimit + ScreenContent      
		   FROM AppConfiguration       
		   WHERE ScreenName LIKE @ScreenName AND Active = 1  		 
		  

		   SET @DealID = Null

		   IF EXISTS(SELECT 1 FROM HcDealOfTheDayStaging WHERE HcHubCitiID=@HcHubCitiID AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL)
		   BEGIN
				SET @ScheduledFlag = 1
				
				SELECT @DealFlagName=D.DealDescription 
				      ,@DealID =DT.DealOfTheDayID 
					  ,@DealName=D.DealDescription
				FROM HcDealOfTheDayStaging DT 
				INNER JOIN HcDeals D ON D.HcDealsID =DT.HcDealsID  
				WHERE HcHubCitiID=@HcHubCitiID AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL

		   END
		   

	       CREATE TABLE #Deals(Rownum Int Identity(1,1)
							  ,DealID int
		                      ,DealName Varchar(1000)
							  ,DealDescription Varchar(max)
							  ,DealStartDate DateTime
							  ,DealEndDate DateTime
							  ,Price Money
							  ,SalePrice Money
							  ,DealScheduleStartDate DateTime
					          ,DealScheduleEndDate DateTime
							  ,Featured BIT 
							  ,IsEnded bit
							  )

          

				
			IF @DealsName = 'Hotdeals' OR (@DealFlagName = 'Hotdeals' AND @DealsName IS NULL)
			BEGIN
				SET @DealName='Hotdeals'

				INSERT INTO #Deals(DealID 
								  ,DealName 
								  ,DealDescription
								  ,DealStartDate 
								  ,DealEndDate
								  ,Price
								  ,SalePrice
								  ,DealScheduleStartDate
								  ,DealScheduleEndDate 
								  ,Featured 
								  ,IsEnded 
								  )
				SELECT DealID 
					  ,DealName 
					  ,DealDescription
					  ,DealStartDate 
					  ,DealEndDate	
					  ,Price 
					  ,SalePrice
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate	
					  ,0
					  ,IsEnded	
					  	
				FROM (
				SELECT DISTINCT DealOfTheDayID DealID
					  ,DealName=PH.HotDealName 
					  ,DealDescription=ISNULL(HotDealShortDescription,HotDeaLonglDescription)
					  ,DealStartDate=HotDealStartDate 
					  ,DealEndDate=HotDealEndDate 
					  ,DealFlag =0
					  ,Price=PH.Price
					  ,SalePrice=PH.SalePrice 
					  ,S.DealScheduleStartDate
					  ,S.DealScheduleEndDate 
					  ,IsEnded = 0
				FROM HcDealOfTheDayStaging S
				INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID =S.DealOfTheDayID
				INNER JOIN HcDeals D ON S.HcDealsID=D.HcDealsID AND D.DealDescription ='Hotdeals'
				WHERE HcHubCitiID =@HcHubCitiID AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND S.DealName LIKE '%'+@SearchKey+'%')) 
				AND GETDATE() BETWEEN ISNULL(HotDealStartDate,GETDATE()-1) AND COALESCE(HotDealExpirationDate,HotDealEndDate,GETDATE()+1)
				AND s.DealScheduleStartDate IS NOT NULL AND s.DealScheduleEndDate IS NOT NULL AND @ScheduledFlag = 1

				UNION ALL

				SELECT DISTINCT DealOfTheDayID DealID
					  ,DealName=PH.HotDealName 
					  ,DealDescription=ISNULL(HotDealShortDescription,HotDeaLonglDescription)
					  ,DealStartDate=HotDealStartDate 
					  ,DealEndDate=HotDealEndDate 
					  ,DealFlag =0
					  ,Price=PH.Price
					  ,SalePrice=PH.SalePrice 
					  ,S.DealScheduleStartDate
					  ,S.DealScheduleEndDate  
					  ,IsEnded= 0 
				FROM HcDealOfTheDayStaging S
				INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID =S.DealOfTheDayID
				INNER JOIN HcDeals D ON S.HcDealsID=D.HcDealsID AND D.DealDescription ='Hotdeals'
				WHERE HcHubCitiID =@HcHubCitiID AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND S.DealName LIKE '%'+@SearchKey+'%')) 
				AND GETDATE() BETWEEN ISNULL(HotDealStartDate,GETDATE()-1) AND  COALESCE(HotDealExpirationDate,HotDealEndDate,GETDATE()+1)
				AND s.DealScheduleStartDate IS NULL AND s.DealScheduleEndDate IS NULL 

				UNION ALL
					
				SELECT DISTINCT ProductHotDealID DealID
				      ,HotDealName DealName
					  ,ISNULL(HotDealShortDescription,HotDeaLonglDescription) DealDescription
					  ,HotDealStartDate DealStartDate
					  ,HotDealEndDate DealEndDate
					  ,DealFlag=1
					  ,P.Price 
					  ,P.SalePrice 
					  ,DealScheduleStartDate = null
					  ,DealScheduleEndDate = null
					  ,IsEnded = 0
				FROM ProductHotDeal P 
				INNER JOIN Retailer PHD ON P.RetailID=PHD.RetailID AND PHD.RetailerActive = 1
				INNER JOIN HcRetailerAssociation HRA ON HRA.Retailid = PHD.Retailid AND HRA.Associated = 1 AND HRA.HcHubCitiID = @HcHubCitiID
				INNER JOIN HcLocationAssociation HLA ON HLA.HcHubCitiID = @HcHubCitiID
				LEFT JOIN HcDealOfTheDayStaging D ON D.DealOfTheDayID=P.ProductHotDealID AND D.HcHubCitiID = @HcHubCitiID
				WHERE ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND HotDealName LIKE '%'+@SearchKey+'%'))
				AND GETDATE() BETWEEN ISNULL(HotDealStartDate,GETDATE()-1) AND  COALESCE(HotDealExpirationDate,HotDealEndDate,GETDATE()+1)
				AND D.DealOfTheDayID IS NULL 
				
			
			
				)A
				ORDER BY DealFlag,DealStartDate DESC,DealName

			END			

			ELSE IF @DealsName = 'SpecialOffers' OR (@DealFlagName ='SpecialOffers' AND @DealsName IS NULL)
			BEGIN
			    SET @DealName = 'SpecialOffers'

			    SELECT @QRTypeID =QRTypeID 
				FROM QRTypes 
				WHERE QRTypeName LIKE 'Special Offer Page' 

				
				INSERT INTO #Deals(DealID 
								  ,DealName 
								  ,DealDescription
								  ,DealStartDate 
								  ,DealEndDate
								  ,Price
								  ,SalePrice
								  ,DealScheduleStartDate
								  ,DealScheduleEndDate 
								  ,Featured 
								  ,ISEnded
								  )
				SELECT DealID 
					  ,DealName 
					  ,DealDescription
					  ,DealStartDate 
					  ,DealEndDate	
					  ,Price
					  ,SalePrice 
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate	
					  ,0	
					  ,IsEnded	
				FROM (
				SELECT DISTINCT DealOfTheDayID DealID
					  ,DealName=Pagetitle
					  ,DealDescription=ISNULL(ShortDescription,LongDescription) 
					  ,DealStartDate=StartDate
					  ,DealEndDate=EndDate
					  ,DealFlag =0
					  ,Price
					  ,SalePrice
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate
					  ,IsEnded = 0
				FROM HcDealOfTheDayStaging S
				INNER JOIN QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID =S.DealOfTheDayID 
				INNER JOIN HcDeals D ON S.HcDealsID=D.HcDealsID AND D.DealDescription ='SpecialOffers'
				WHERE HcHubCitiID =@HcHubCitiID AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND S.DealName LIKE '%'+@SearchKey+'%')) 
				AND  GETDATE() BETWEEN ISNULL(StartDate,GETDATE()-1) AND ISNULL(EndDate,GETDATE()+1)
				AND s.DealScheduleStartDate IS NOT NULL AND s.DealScheduleEndDate IS NOT NULL AND @ScheduledFlag = 1

				UNION ALL

				SELECT DISTINCT DealOfTheDayID DealID
					  ,DealName=Pagetitle
					  ,DealDescription=ISNULL(ShortDescription,LongDescription) 
					  ,DealStartDate=StartDate
					  ,DealEndDate=EndDate
					  ,DealFlag =0
					  ,Price
					  ,SalePrice
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate
					  ,IsEnded = 0
				FROM HcDealOfTheDayStaging S
				INNER JOIN QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID =S.DealOfTheDayID 
				INNER JOIN HcDeals D ON S.HcDealsID=D.HcDealsID AND D.DealDescription ='SpecialOffers'
				WHERE HcHubCitiID =@HcHubCitiID AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND S.DealName LIKE '%'+@SearchKey+'%')) 
				AND  GETDATE() BETWEEN ISNULL(StartDate,GETDATE()-1) AND ISNULL(EndDate,GETDATE()+1)
				AND DealScheduleStartDate IS NULL AND DealScheduleEndDate IS NULL 

				UNION ALL
					
				SELECT DISTINCT Q.QRRetailerCustomPageID  
				      ,Pagetitle DealName
					  ,DealDescription=ISNULL(ShortDescription,LongDescription) 
					  ,StartDate DealStartDate
					  ,EndDate DealEndDate
					  ,DealFlag=1
					  ,Price 
					  ,SalePrice 
					  ,DealScheduleStartDate = null
					  ,DealScheduleEndDate = null
					  ,IsEnded = 0
				FROM QRRetailerCustomPage Q
				INNER JOIN QRRetailerCustomPageAssociation QRA ON QRA.QRRetailerCustomPageID = Q.QRRetailerCustomPageID AND Q.QRTypeID = 4
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = QRA.RetailLocationID AND RL.Active=1
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID
				INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
				INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND R.RetailerActive = 1
				LEFT JOIN HcDealOfTheDayStaging D ON D.DealOfTheDayID=Q.QRRetailerCustomPageID AND D.HcHubCitiID =@HcHubCitiID		
				WHERE ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND Pagetitle LIKE '%'+@SearchKey+'%'))
				AND GETDATE() BETWEEN ISNULL(StartDate,GETDATE()-1) AND ISNULL(EndDate,GETDATE()+1)
				
				AND D.DealOfTheDayID IS NULL 

				)A
				ORDER BY DealFlag,DealStartDate DESC,DealName

			END

			ELSE IF @DealsName = 'Coupons' OR @SearchKey IS NULL OR @DealFlagName= 'Coupons'
			BEGIN

				SET @DealName='Coupons'
				
				INSERT INTO #Deals(DealID 
								  ,DealName 
								  ,DealDescription
								  ,DealStartDate 
								  ,DealEndDate
								  ,Price
								  ,SalePrice
								  ,DealScheduleStartDate
							      ,DealScheduleEndDate 
								  ,Featured
								  ,IsEnded
								  )
				SELECT DealID 
					  ,DealName 
					  ,DealDescription
					  ,DealStartDate 
					  ,DealEndDate	
					  ,Price
					  ,SalePrice 
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate	
					  ,Featured	
					  ,isended	
				FROM (
				SELECT DISTINCT DealOfTheDayID DealID
					  ,DealName=CouponName
					  ,DealDescription=ISNULL(CouponShortDescription,CouponLongDescription) 
					  ,DealStartDate=CouponStartDate
					  ,DealEndDate=CouponExpireDate
					  ,DealFlag =0
					  ,Price
					  ,SalePrice =CouponDiscountAmount
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate
					  ,ISNULL(FC.Featured,0) Featured
					  ,IsEnded = CASE WHEN CAST(C.CouponExpireDate as smalldatetime) < cast(getdate() as smalldatetime) then 1 else 0 end
				FROM HcDealOfTheDayStaging S
				INNER JOIN Coupon C ON C.CouponID =S.DealOfTheDayID 
				LEFT JOIN HubCitiFeaturedCoupon FC ON FC.CouponID = C.CouponID  AND FC.HubCitiID = @HcHubCitiID
				INNER JOIN HcDeals D ON S.HcDealsID=D.HcDealsID AND D.DealDescription ='Coupons'
				WHERE S.HcHubCitiID =@HcHubCitiID AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND S.DealName LIKE '%'+@SearchKey+'%')) 
				AND GETDATE() BETWEEN ISNULL(CouponStartDate,GETDATE()-1) AND ISNULL(ActualCouponExpirationDate,GETDATE()+1)
				AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL AND @ScheduledFlag = 1 AND C.HcHubCitiID is null

				UNION ALL

				SELECT DISTINCT DealOfTheDayID DealID
					  ,DealName=CouponName
					  ,DealDescription=ISNULL(CouponShortDescription,CouponLongDescription) 
					  ,DealStartDate=CouponStartDate
					  ,DealEndDate=CouponExpireDate
					  ,DealFlag =0
					  ,Price
					  ,SalePrice =CouponDiscountAmount
					  ,DealScheduleStartDate
					  ,DealScheduleEndDate
					  ,ISNULL(FC.Featured,0) Featured
					  ,IsEnded = CASE WHEN CAST(C.CouponExpireDate as smalldatetime) < cast(getdate() as smalldatetime) then 1 else 0 end
				FROM HcDealOfTheDayStaging S
				INNER JOIN Coupon C ON C.CouponID =S.DealOfTheDayID 
				LEFT JOIN HubCitiFeaturedCoupon FC ON FC.CouponID = C.CouponID  AND FC.HubCitiID = @HcHubCitiID
				INNER JOIN HcDeals D ON S.HcDealsID=D.HcDealsID AND D.DealDescription ='Coupons'
				WHERE S.HcHubCitiID =@HcHubCitiID AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND S.DealName LIKE '%'+@SearchKey+'%')) 
				AND GETDATE() BETWEEN ISNULL(CouponStartDate,GETDATE()-1) AND ISNULL(ActualCouponExpirationDate,GETDATE()+1)
				AND DealScheduleStartDate IS  NULL AND DealScheduleEndDate IS NULL and C.HcHubCitiID is null

				UNION ALL
					
				SELECT DISTINCT C.CouponID 
				      ,CouponName DealName
					  ,DealDescription=ISNULL(CouponShortDescription,CouponLongDescription) 
					  ,CouponStartDate DealStartDate
					  ,CouponExpireDate DealEndDate
					  ,DealFlag=1
					  ,Price
					  ,SalePrice=CouponDiscountAmount 
					  ,DealScheduleStartDate = null
					  ,DealScheduleEndDate = null
					  ,ISNULL(FC.Featured,0) Featured
					  ,IsEnded = CASE WHEN CAST(C.CouponExpireDate as smalldatetime) < cast(getdate() as smalldatetime) then 1 else 0 end
				FROM Coupon C
				LEFT JOIN HubCitiFeaturedCoupon FC ON FC.CouponID = C.CouponID AND FC.HubCitiID = @HcHubCitiID
				INNER JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = CR.RetailLocationID AND RL.Active=1
				INNER JOIN HcLocationAssociation HL ON HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID
				INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
				LEFT JOIN HcDealOfTheDayStaging D ON D.DealOfTheDayID=C.CouponID AND D.HcHubCitiID =@HcHubCitiID
				WHERE ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND CouponName LIKE '%'+@SearchKey+'%'))
				 AND GETDATE() BETWEEN ISNULL(CouponStartDate,GETDATE()-1) AND ISNULL(ActualCouponExpirationDate,GETDATE()+1)
				 AND D.DealOfTheDayID IS NULL and c.HcHubCitiID is null
	
				)A
				ORDER BY DealFlag,DealStartDate DESC,DealName

			 END

			 SELECT @MaxCnt = MAX(RowNum) FROM #Deals
             
			 SET @MaxCnt =ISNULL(@MaxCnt,0)             
             --this flag is a indicator to enable "More" button in the UI.   
             --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
             SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END


			 SELECT RowNum
			       ,DealID 
				   ,DealName 
				   ,DealDescription dealDescription
				   ,DealStartDate startDate
				   ,DealEndDate endDate
				   ,Price
				   ,SalePrice
				   ,DealScheduleStartDate sStartDate
				   ,DealScheduleEndDate sEndDate
				   ,Featured isFeatured
				   ,IsEnded isEnded
			 FROM #Deals
			 WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  
             ORDER BY RowNum
	
			 
			
		


			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDelaoftheDayList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
