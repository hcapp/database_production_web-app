USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceRetailerList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceRetailerList
Purpose					: To display CityExperience RetailLocations.
Example					: usp_WebHcCityExperienceRetailerList

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceRetailerList]
(
    --Input variable
      @HubCitiID int   
    , @SearchKey varchar(1000)
    , @LowerLimit int  
	, @ScreenName varchar(50)
	--, @SearchKey Varchar(500)
  
	--Output Variable
	, @CityExperienceName Varchar(500) Output 
	, @HcCityExperienceID Int Output
	, @MaxCnt int  output
	, @NxtPageFlag bit output   
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To get the row count for pagination.  
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1
			
			SELECT @HcCityExperienceID=HcCityExperienceID  
			      ,@CityExperienceName=CityExperienceName
			FROM HcCityExperience WHERE HcHubCitiID =@HubCitiID

			--To display list of RetailLocations for given CityExperience
			SELECT DISTINCT RLC.RetailLocationID
			               --, 1 Associated
						   ,RLC.Associated		   
			INTO #Associated   
			FROM HcCityExperience HC
			INNER JOIN HcCityExperienceRetailLocation CER ON HC.HcCityExperienceID = CER.HcCityExperienceID 
			INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =CER.RetailLocationID -- AND RLC.Associated = 1 
			WHERE RLC.HcHubCitiID = @HubCitiID AND RLC.HcHubCitiID = @HubCitiID AND CER.HcCityExperienceID = @HcCityExperienceID AND RLC.HcHubCitiID = @HubCitiID
			
			SELECT DISTINCT RL.RetailLocationID
						  , 0 Associated
			INTO #Search
			FROM Retailer R
			INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0 AND Active = 1
			INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HubCitiID 
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID --AND RLC.Associated = 0
			WHERE R.RetailName LIKE '%'+@SearchKey+'%'
			AND R.RetailerActive=1 
			
			--To match the associated records.
			SELECT DISTINCT RetailLocationID
				 , Associated
			INTO #T 
			FROM ( 
					SELECT RetailLocationID
						, Associated
					FROM #Associated 
					
					UNION
					
					SELECT RetailLocationID
						, Associated
					FROM #Search)A
			
			SELECT RetailLocationID
				 , MAX(Associated) Associated
			INTO #Temp
			FROM #T 
			GROUP BY RetailLocationID
			
			--Logic to sort based on the relevance to the search key.
			SELECT DISTINCT R.RetailID
				 , RetailName
				 , RL.RetailLocationID
				 , RL.Address1
				 , RL.City
				 , RL.State
				 , RL.PostalCode
				 , A.Associated
				 , Relevant = CASE WHEN RetailName LIKE '%'+@SearchKey+'%' THEN 1 ELSE 0 END
			INTO #Relevance
			FROM #Temp A
			INNER JOIN RetailLocation RL ON RL.RetailLocationID = A.RetailLocationID AND RL.Headquarters = 0 AND Active = 1
			INNER JOIN HcLocationAssociation HLA ON HLA.PostalCode = RL.PostalCode		
			INNER JOIN Retailer R ON R.RetailID = RL.RetailID			
			WHERE RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%'	END
			AND R.RetailerActive=1
			ORDER BY Associated DESC, RetailName ASC
			
			SELECT Row_num = IDENTITY(INT, 1, 1)
				 , RetailID
				 , RetailName
				 , RetailLocationID
				 , Address1
				 , City
				 , State
				 , PostalCode
				 , Associated
				 , Relevant
			INTO #Temp1
			FROM #Relevance
			ORDER BY Relevant DESC, RetailName ASC			
			
			--To capture Max Row Number.  
			SELECT @MaxCnt = COUNT(Row_num) FROM #Temp1
					 
			--This flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			SELECT  Row_num
				    --, CityExperienceName
					, RetailID retId
					, RetailName retName
					, RetailLocationID retLocId
					, Address1 address
					, City city
					, State state
					, PostalCode postalCode
					, Associated associate
			FROM #Temp1
			WHERE Row_num BETWEEN (@LowerLimit+1) AND @UpperLimit												
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceRetailerList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
