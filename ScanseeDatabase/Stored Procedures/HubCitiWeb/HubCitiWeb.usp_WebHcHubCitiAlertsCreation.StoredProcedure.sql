USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsCreation
Purpose					: Creating Alerts.
Example					: usp_WebHcHubCitiAlertsCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsCreation]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int
	, @HcAlertName Varchar(1000)
	, @Description Varchar(2000)
	, @HcAlertCategoryID Int
	, @HcSeverityID int
	, @StartDate VARCHAR(1000)
	, @EndDate VARCHAR(1000)	
	, @StartTime VARCHAR(1000)
	, @EndTime VARCHAR(1000)	
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
								
			DECLARE @HcAlertsID INT	
					
			INSERT INTO HcAlerts(HcAlertName 
			                    ,ShortDescription
                                ,LongDescription 								
								,HcHubCitiID 
								,HcSeverityID 
								,StartDate 
								,EndDate 
								,DateCreated  
								,CreatedUserID)	
			SELECT @HcAlertName
			     , @Description
			     , @Description 			      
				 , @HcHubCitiID 					
			     , @HcSeverityID 
				 , CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
				 , CAST(@EndDate AS DATETIME)+' '+CAST(@EndTime AS DATETIME)
				 , GETDATE()
				 , @UserID			
					
				
			SET @HcAlertsID=SCOPE_IDENTITY()
			
			INSERT INTO HcAlertCategoryAssociation(HcAlertCategoryID
												  ,HcAlertID
												  ,HcHubCitiID
												  ,DateCreated
												  ,CreatedUserID)	
			SELECT [Param]
			      ,@HcAlertsID
			      ,@HcHubCitiID
			      ,GETDATE()
			      ,@UserID
			FROM dbo.fn_SplitParam(@HcAlertCategoryID, ',') 					
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
