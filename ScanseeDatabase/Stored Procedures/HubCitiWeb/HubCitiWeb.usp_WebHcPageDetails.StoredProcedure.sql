USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcPageDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcPageDetails
Purpose					: To view the page details of a particular HubCiti.
Example					: usp_WebHcPageDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23/09/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcPageDetails]
(
   ----Input variable.
      @HubCitiID int
      
	--Output Variable 
	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @LoginPage int  
        DECLARE @RegistrationPage int  
		DECLARE @PrivacyPage int  
		DECLARE @AboutUsPage int  
		DECLARE @WelcomePage varchar(50)  
		DECLARE @GeneralSettingPage varchar(50)  
		DECLARE @MainMenuPage varchar(50)   
	
			--SELECT @LoginPage = CASE WHEN COUNT(PC.HcPageConfigurationID) > 0 AND PT.PageType = 'Login Page' THEN 1 ELSE 0 END 
			--	 , @RegistrationPage = CASE WHEN COUNT(PC.HcPageConfigurationID) > 0 AND PT.PageType = 'Registration Page' THEN 1 ELSE 0 END 
			--	 , @PrivacyPage = CASE WHEN COUNT(PC.HcPageConfigurationID) > 0 AND PT.PageType = 'Privacy Policy' THEN 1 ELSE 0 END 
			--	 , @AboutUsPage = CASE WHEN COUNT(PC.HcPageConfigurationID) > 0 AND PT.PageType = 'About Us Page' THEN 1 ELSE 0 END 
			--FROM HcPageConfiguration PC
			--INNER JOIN HcPageType PT ON PC.HcPageTypeID = PT.HcPageTypeID
			--WHERE HcHubCitiID = @HubCitiID
			--GROUP BY PT.PageType, HcPageConfigurationID
			
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcPageTypeID =1 AND HcHubCitiID = @HubCitiID)
			BEGIN
				SELECT @LoginPage = 1				  
			END
			ELSE
			BEGIN
			    SELECT @LoginPage = 0
			END
			    
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcPageTypeID =2 AND HcHubCitiID = @HubCitiID)
			BEGIN
				SELECT @RegistrationPage = 1						  
			END
			ELSE
			BEGIN
			    SELECT @RegistrationPage = 0
			END
			
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcPageTypeID =3 AND HcHubCitiID = @HubCitiID)
			BEGIN
				SELECT @PrivacyPage = 1					  
			END
			ELSE
			BEGIN
			    SELECT @PrivacyPage = 0
			END
			
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcPageTypeID =4 AND HcHubCitiID = @HubCitiID)
			BEGIN
				SELECT @AboutUsPage = 1
			END
			ELSE
			BEGIN
			    SELECT @AboutUsPage = 0
			END			
			
			IF EXISTS(SELECT 1 FROM HcHubCiti WHERE SplashImage IS NOT NULL AND HcHubCitiID = @HubCitiID)
			BEGIN
				SELECT @WelcomePage = 1				  
			END
			ELSE
			BEGIN
			    SELECT @WelcomePage = 0
			END
			    
			IF EXISTS(SELECT 1 FROM HcHubCiti WHERE SmallLogo IS NOT NULL AND HcHubCitiID = @HubCitiID)
			BEGIN
				SELECT @GeneralSettingPage = 1						  
			END
			ELSE
			BEGIN
			    SELECT @GeneralSettingPage = 0
			END
			
			SELECT LoginPage  = @LoginPage    
				 , RegistrationPage = @RegistrationPage    
				 , PrivacyPage = @PrivacyPage    
				 , AboutUsPage = @AboutUsPage    
				 , WelcomePage = @WelcomePage   
				 , MainMenuPage = @MainMenuPage  
				 , GeneralSettingPage = @GeneralSettingPage
			INTO #Temp
			
			SELECT LoginPage
				 , RegistrationPage
				 , PrivacyPage
				 , AboutUsPage
				 , WelcomePage
				 , MainMenuPage
				 , GeneralSettingPage
			FROM #Temp
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
		SELECT ERROR_LINE()	 
			PRINT 'Error occured in Stored Procedure usp_WebHcPageDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
