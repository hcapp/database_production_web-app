USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRoleBasedUserDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcRoleBasedUserDeletion
Purpose					: To Deactivate Role Based User.
Example					: usp_WebHcRoleBasedUserDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/07/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRoleBasedUserDeletion]
(
   
    --Input variable. 	
	 
	  @RoleBasedUserID Int

	  
	  
	--Output Variable 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	            
				
				--To Deactivate Role Based User

				UPDATE USERS SET Enabled =IIF(Enabled=0 OR Enabled IS NULL,1,0) 
				WHERE UserID =@RoleBasedUserID
						
							
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcRoleBasedUserDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
