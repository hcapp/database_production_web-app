USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiRetailerFundraisingEventDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name					 : usp_WebHcHubCitiRetailerFundraisingEventDetails
Purpose                                  : To display HubCitit/Retailer Fundraising Event Details.
Example                                  : usp_WebHcHubCitiRetailerFundraisingEventDetails

History
Version              Date                 Author               Change Description
--------------------------------------------------------------- 
1.0                  19 Feb 2014		  Mohith H R             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiRetailerFundraisingEventDetails]
(   
    --Input variable.        
         @HcFundraisingID Int
       , @HubCitiID int
  
       --Output Variable 
	   , @IOSAppID varchar(500) output
	   , @AndroidAppID varchar(500) output
       , @Status int output	  
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY

		
			SELECT @IOSAppID = IOSAppID
				  ,@AndroidAppID = AndroidAppID
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID

			--Retrieve the server configuration.
			DECLARE @QRConfig varchar(500)		
			SELECT @QRConfig = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'QR Code Configuration'
			AND Active = 1 


			DECLARE @RetailerFundraiserID int

			SELECT @RetailerFundraiserID = RetailID
			FROM HcFundraising
			WHERE HcFundraisingID = @HcFundraisingID

			IF @RetailerFundraiserID IS NULL
			BEGIN
                     
                     DECLARE @HcFundraisingCategoryID Int
                     DECLARE @AppSiteID Int
					 DECLARE @DepartmentId Int
					 DECLARE @AppSiteFlag Bit
                     DECLARE @EventID Varchar(2000)
                     DECLARE @Config Varchar(500)
                     

					 SELECT @Config = ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'
                     
                     SELECT @HcFundraisingCategoryID = HcFundraisingCategoryID
                     FROM HcFundraisingCategoryAssociation 
                     WHERE HcFundraisingID  = @HcFundraisingID 

					 SELECT @DepartmentId = HcFundraisingDepartmentID
                     FROM HcFundraisingCategoryAssociation 
                     WHERE HcFundraisingID  = @HcFundraisingID

                     SELECT @AppSiteID = HcAppsiteID 
                     FROM HcFundraisingAppsiteAssociation 
                     WHERE HcFundraisingID  = @HcFundraisingID 

					 SELECT @AppSiteFlag = FundraisingAppsiteFlag
                     FROM HcFundraising
                     WHERE HcFundraisingID  = @HcFundraisingID 

                     --Select Event if Exist
                     SELECT @EventID = COALESCE(@EventID+',', '')+CAST(HcEventID AS VARCHAR)
                     FROM HcFundraisingEventsAssociation 
                     WHERE HcFundraisingID = @HcFundraisingID 
              
                     --To display Event Details.
                     SELECT DISTINCT FundraisingName hcEventName 
                                            ,IIF(@AppSiteFlag = 1,NULL,FundraisingOrganizationName) organizationHosting
                                            ,eventImagePath = @Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath
											--,FundraisingOrganizationImagePath eventImageName    
											,ShortDescription shortDescription
											,LongDescription longDescription                                                   
                                            --,E.HcHubCitiID                                                    
                                            ,FundraisingGoal fundraisingGoal
                                            ,CurrentLevel currentLevel                                                      
                                            --,FundraisingAppsiteFlag isEventAppsite
                                            --,FundraisingEventFlag isEventTied
                                            ,MoreInformationURL moreInfoURL
										    ,PurchaseProductURL purchaseProducts                                                                                                                                                           
                                            ,eventDate =CAST(StartDate AS DATE)
                                            ,eventEDate =ISNULL(CAST(EndDate AS DATE),NULL)
                                            --,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
                                            --,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)                                                                                                                   
                                            --,MenuItemExist =CASE WHEN(SELECT COUNT(HcMenuItemID)
                                            --                                        FROM HcMenuItem MI 
                                            --                                        INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                         
                                            --                                        WHERE LinkTypeName = 'Fundraising' 
                                            --                                        AND MI.LinkID = E.HcFundraisingID)>0 THEN 1 ELSE 0 END          
                                           --,@HcFundraisingCategoryID  eventCategory                                                        
                                           --,@AppSiteID appsiteIDs
										   --,@DepartmentId departmentId
                                           ,eventTiedIds = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(HcEventID AS VARCHAR(10)))
                                                                                         FROM  HcFundraisingEventsAssociation HP
                                                                                         WHERE HP.HcFundraisingID = E.HcFundraisingID
                                                                                      FOR XML PATH( '' )
                                                                                    ), 3, 1000 ), ' ', '')
										   ,QRUrl = @QRConfig+'2500.htm?fundEvtId='+ CAST(@HcFundraisingID AS VARCHAR(10)) +'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10)) 
                                           --,Active                                                                                                                                                                                 
                     FROM HcFundraising E 
                     --LEFT JOIN HcFundraisingEventsAssociation FE ON E.HcFundraisingID = FE.HcFundraisingID         
                     WHERE HcFundraisingID  = @HcFundraisingID 
                     AND E.Active = 1 
                           
                     
                     --Confirmation of Success
                     SELECT @Status = 0  
			END	
			ELSE
			BEGIN 
					DECLARE @RetFundraisingCategoryID Int
					DECLARE @HcFundraisingDepartmentID Int
					DECLARE @RetailLocationIDs VArchar(1000)
					DECLARE @RetAppSiteFlag Bit
					DECLARE @RetEventID Varchar(2000)
					DECLARE @RetConfig Varchar(500)
                     

					SELECT @RetConfig = ScreenContent
					FROM AppConfiguration
					WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
                     
					SELECT @RetFundraisingCategoryID = HcFundraisingCategoryID
							, @HcFundraisingDepartmentID = HcFundraisingDepartmentID
					FROM HcFundraisingCategoryAssociation 
					WHERE HcFundraisingID  = @HcFundraisingID 

					SELECT @RetailLocationIDs = COALESCE(@RetailLocationIDs+',','')+ CAST(RetailLocationID AS VARCHAR)
					FROM HcRetailerFundraisingAssociation 
					WHERE HcFundraisingID  = @HcFundraisingID 

					SELECT @RetAppSiteFlag = FundraisingAppsiteFlag
					FROM HcFundraising
					WHERE HcFundraisingID = @HcFundraisingID 

					--Select Event if Exist
					SELECT @RetEventID = COALESCE(@RetEventID+',', '')+CAST(HcEventID AS VARCHAR)
					FROM HcFundraisingEventsAssociation 
					WHERE HcFundraisingID = @HcFundraisingID 
              
					--To display Event Details
					SELECT DISTINCT FundraisingName hcEventName 
										,FundraisingOrganizationName organizationHosting
										,eventImagePath = @RetConfig + CAST(E.RetailID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath
										,FundraisingOrganizationImagePath eventImageName    
										,ShortDescription shortDescription
										,LongDescription longDescription                                                   
										,E.RetailID                                                  
										,FundraisingGoal fundraisingGoal
										,CurrentLevel currentLevel                                                      
										--,FundraisingAppsiteFlag isRetLoc
										--,FundraisingEventFlag isEventTied
										,MoreInformationURL moreInfoURL
										,PurchaseProductURL purchaseProducts                                                                                                                                                         
										,eventDate =CAST(StartDate AS DATE)
										,eventEDate =ISNULL(CAST(EndDate AS DATE),NULL)
										--,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
										--,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)                                                                                                                   
										--,@RetFundraisingCategoryID  eventCategory                                                        
										--,@RetailLocationIDs retLocId
										,eventTiedIds = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(HcEventID AS VARCHAR(10)))
																						FROM  HcFundraisingEventsAssociation HP
																						WHERE HP.HcFundraisingID = E.HcFundraisingID
																					FOR XML PATH( '' )
																				), 3, 1000 ), ' ', '')
										,QRUrl = @QRConfig+'2500.htm?fundEvtId='+ CAST(@HcFundraisingID AS VARCHAR(10)) +'&hubcitiId=null'--+ CAST(@HubCitiId AS VARCHAR(10)) 
										--,Active
										--,@HcFundraisingDepartmentID department
					FROM HcFundraising E 
					--LEFT JOIN HcFundraisingEventsAssociation FE ON E.HcFundraisingID = FE.HcFundraisingID         
					WHERE HcFundraisingID  = @HcFundraisingID  AND RetailID = @RetailerFundraiserID
					AND E.Active = 1 
			
					--Confirmation of Success
					SELECT @Status = 0	
			END	        
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiRetailerFundraisingEventDetails.'        
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;








GO
