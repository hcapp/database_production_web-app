USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventsDisplay
Purpose					: To display List fo Events.
Example					: usp_WebHcHubCitiEventsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventsDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @UserID Int
	, @Searchparameter Varchar(2000)
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @Fundraising bit
	--, @SortColumn Varchar(200)
	--, @SortOrder Varchar(100)
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int
			
			SET @Fundraising = ISNULL(@Fundraising,0)	
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To get user info i.e Super/Normal user.
			DECLARE @RoleName VARCHAR(100)
			SELECT @RoleName = HcRoleName
			FROM HcUserRole UR
			INNER JOIN HcRole R ON UR.HcRoleID = R.HcRoleID
			WHERE HcAdminUserID = @UserID AND UR.HcRoleBasedModuleListID=1

			
			--To display List fo Events

			CREATE TABLE #Events(RowNum INT
								,HcEventID INT
								,HcEventName VARCHAR(100)
								,ShortDescription VARCHAR(1000)
								,LongDescription VARCHAR(2000)
								,HcHubCitiID INT
								,ImagePath VARCHAR(1000)
								,BussinessEvent INT
								,PackageEvent INT
								,HotelEvent INT
								,StartDate Date
								,EndDate Date
								,StartTime Time
								,EndTime Time
								,MenuItemExist INT
								,Address VARCHAR(500)
								,City VARCHAR(50)
								,State VARCHAR(10)
								,PostalCode VARCHAR(10)
								,GeoErrorFlag INT
								,IsEventOverLayFlag Bit) 

  
			IF @RoleName = 'ROLE_EVENT_USER'
			BEGIN

				INSERT INTO #Events(RowNum 
								,HcEventID 
								,HcEventName 
								,ShortDescription 
								,LongDescription 
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,PackageEvent 
								,HotelEvent
								,StartDate
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist 
								,Address
								,City 
								,State 
								,PostalCode 
								,GeoErrorFlag
								,IsEventOverLayFlag)

				SELECT			RowNum 
								,HcEventID 
								,HcEventName 
								,ShortDescription 
								,LongDescription 
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,PackageEvent 
								,HotelEvent
								,StartDate
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist 
								,Address
								,City 
								,State 
								,PostalCode 
								,GeoErrorFlag
								,IsEventOverLayFlag
				FROM(
				SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY E.HcEventID) Rownum
							   ,E.HcEventID 
							   ,HcEventName  
							   ,ShortDescription
							   ,LongDescription 								
							   ,E.HcHubCitiID 
							   ,ImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
							   ,BussinessEvent 
							   ,PackageEvent
							   ,HotelEvent								
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   ,EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
							   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
												  FROM HcMenuItem MI 
												  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
												  WHERE LinkTypeName = 'Events' 
												  AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END	
							  --,EC.HcEventCategoryID 
							  --,EC.HcEventCategoryName 			
							  ,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.Address 
										 END
							  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.City 
									  END
							  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.State 
									  END
							  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.PostalCode 
									  END  	
							  ,GeoErrorFlag = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE HE.GeoErrorFlag END
							  ,IsEventOverLayFlag=IIF(EO.HcEventID IS NOT NULL,1,0)
							  --,MoreInformationURL moreInfoURL								
				--INTO #Events
				FROM HcEvents E 
				--INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
				--INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  	
				--LEFT JOIN HcEventLocation HE on HE.HcEventID = E.HcEventID		
				--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON E.HcHubCitiID = UE.HcHubcitiID 
				LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON E.CreatedUserID = UE.UserID 		
				INNER JOIN HcEventsCategoryAssociation EA ON UE.HcEventCategoryID = EA.HcEventCategoryID 			
				--INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  	
				INNER JOIN HcEventLocation HE on HE.HcEventID = E.HcEventID
				LEFT JOIN HcEventsLogistic  EO ON EO.HcEventID =E.HcEventID AND EventsIsOverLayFlag =1
				WHERE (( @Searchparameter IS NOT NULL AND HcEventName LIKE '%'+@Searchparameter+'%') OR (@Searchparameter IS NULL AND 1=1))
				AND  ((@Fundraising = 1 AND GETDATE() <= ISNULL(EndDate, GETDATE()+1)) OR @Fundraising = 0)				
				AND E.HcHubCitiID = @HubCitiID AND E.Active = 1 AND E.CreatedUserID = @UserID 
				GROUP BY E.HcEventID
					    ,HcEventName
						,ShortDescription
						,LongDescription
						,E.HcHubCitiID
						,ImagePath
						,BussinessEvent
						,PackageEvent
						,HotelEvent
						,StartDate
						,EndDate
						,Address
						,City
						,State
						,PostalCode
						,GeoErrorFlag	
						,EO.HcEventID					
				--ORDER BY E.HcEventName
				) Events 
			END
			ELSE
			BEGIN
				INSERT INTO #Events(RowNum 
								,HcEventID 
								,HcEventName 
								,ShortDescription 
								,LongDescription 
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,PackageEvent 
								,HotelEvent
								,StartDate
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist 
								,Address
								,City 
								,State 
								,PostalCode 
								,GeoErrorFlag
								,IsEventOverLayFlag)

				SELECT			RowNum 
								,HcEventID 
								,HcEventName 
								,ShortDescription 
								,LongDescription 
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,PackageEvent 
								,HotelEvent
								,StartDate
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist 
								,Address
								,City 
								,State 
								,PostalCode 
								,GeoErrorFlag
								,IsEventOverLayFlag
				FROM(
				SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY E.HcEventID) Rownum
							   ,E.HcEventID 
							   ,HcEventName  
							   ,ShortDescription
							   ,LongDescription 								
							   ,E.HcHubCitiID 
							   ,ImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
							   ,BussinessEvent 
							   ,PackageEvent
							   ,HotelEvent								
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   ,EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
							   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
												  FROM HcMenuItem MI 
												  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
												  WHERE LinkTypeName = 'Events' 
												  AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END	
							  --,EC.HcEventCategoryID 
							  --,EC.HcEventCategoryName 			
							  ,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.Address 
										 END
							  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.City 
									  END
							  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.State 
									  END
							  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE HE.PostalCode 
									  END  	
							  ,GeoErrorFlag = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE HE.GeoErrorFlag END
							  ,IsEventOverLayFlag=IIF(EO.HcEventID IS NOT NULL,1,0)
							  --,MoreInformationURL moreInfoURL								
				--INTO #Events
				FROM HcEvents E 
				--INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
				--INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  	
				--LEFT JOIN HcEventLocation HE on HE.HcEventID = E.HcEventID		
				--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON E.HcHubCitiID = UE.HcHubcitiID 
				--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON E.HcHubCitiID = UE.HcHubcitiID 		
				--INNER JOIN HcEventsCategoryAssociation EA ON UE.HcEventCategoryID = EA.HcEventCategoryID 			
				--INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  	
				LEFT JOIN HcEventLocation HE on HE.HcEventID = E.HcEventID
			    LEFT JOIN HcEventsLogistic  EO ON EO.HcEventID =E.HcEventID AND EventsIsOverLayFlag =1
				WHERE (( @Searchparameter IS NOT NULL AND HcEventName LIKE '%'+@Searchparameter+'%') OR (@Searchparameter IS NULL AND 1=1))			
				AND  ((@Fundraising = 1 AND GETDATE() <= ISNULL(EndDate, GETDATE()+1)) OR @Fundraising = 0)	
				AND E.HcHubCitiID = @HubCitiID 
				--AND ((@HubCitiID IS NOT NULL AND E.HcHubCitiID = @HubCitiID) OR (@HubCitiID IS NULL AND 1=1))
				AND E.Active = 1 
				--ORDER BY E.HcEventName
				) Events 
			END	               

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Events

			IF @LowerLimit IS NULL
				BEGIN
					SET @LowerLimit=0
					SET @UpperLimit=@MaxCnt					
				END
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SELECT Rownum	
			      ,HcEventID 
			      ,HcEventName  
			      ,ShortDescription
                  ,LongDescription 								
				  ,HcHubCitiID 
				  ,ImagePath
				  ,BussinessEvent 
				  ,PackageEvent
				  ,HotelEvent								
				  ,StartDate eventStartDate
				  ,EndDate eventEndDate
				  ,StartTime eventTime
				  ,EndTime				
				  ,MenuItemExist
				  --,HcEventCategoryID 
				  --,HcEventCategoryName 	
				  ,Address
				  ,City
				  ,State
				  ,PostalCode	
				  ,GeoErrorFlag	
				  ,IsEventOverLayFlag isNewLogisticsImg	
				  --,MoreInformationURL	  				  											
			FROM #Events
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY HcEventName 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
