USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingEventsCreationAndUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingEventsCreationAndUpdation
Purpose					: Creating/Updating New Fundraising Events.
Example					: usp_WebHcHubCitiFundraisingEventsCreationAndUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Aug 2014   Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingEventsCreationAndUpdation]
(
   --Input variable. 	
      @UserID Int
	, @FundraisingID Int
	, @FundraisingOrganizationName Varchar(1000)
	, @FundraisingOrganizationImagePath Varchar(1000)
	, @FundraisingName Varchar(1000)
	, @ShortDescription Varchar(1000)
	, @LongDescription nVarchar(max)
	, @HcFundraisingCategoryID Varchar(100) 
	--, @HcFundraisingDepartmentID Varchar(100) 
	, @HcHubCitiID Int
	, @StartDate Date
	--, @StartTime Time
	, @EndDate Date
	--,@ EndTime Time
	, @FundraisingAppsiteFlag Bit
	, @AppsiteID Int
	, @FundraisingEventFlag Bit
	, @EventID Varchar(max)	
	, @MoreInformationURL Varchar(1000)
	, @PurchaseProductURL Varchar(1000)
	, @FundraisingGoal Varchar(1000)
	, @CurrentLevel Money
	, @Address Varchar(1000)
	, @City Varchar(100)
	, @State Varchar(100)
	, @PostalCode Varchar(20)
	, @Latitude float
	, @Longitude float
	, @GeoErrorFlag bit
	
	--Output Variable 		
    , @Status Int output        
	, @ErrorNumber Int output
	, @ErrorMessage Varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           													

			IF @FundraisingID IS NULL
			BEGIN
				--Creating New Event		
						INSERT INTO HcFundraising(FundraisingName
													,FundraisingOrganizationName
													,FundraisingOrganizationImagePath
													,ShortDescription
													,LongDescription
													,HcHubCitiID
													,StartDate
													,EndDate
													,FundraisingGoal
													,CurrentLevel										
													,FundraisingAppsiteFlag
													,FundraisingEventFlag
													,MoreInformationURL
													,PurchaseProductURL
													,CreatedUserID										
													,DateCreated
													,Active)	
																				
										SELECT LTRIM(RTRIM(@FundraisingName))
											 , @FundraisingOrganizationName
											 --, IIF(@AppsiteID IS NULL,@FundraisingOrganizationName,(SELECT HcAppSiteName FROM HcAppSite WHERE HcAppSiteID = @AppsiteID))
											 , @FundraisingOrganizationImagePath
											 , @ShortDescription 
											 , @LongDescription 			      
											 , @HcHubCitiID 
											 , CAST(@StartDate AS DATETIME)--+' '+CAST(@StartTime AS DATETIME) 													 								
											 , CAST(@EndDate AS DATETIME)--+' '+CAST(@EndTime AS DATETIME)	
											 , @FundraisingGoal
											 , @CurrentLevel
											 , @FundraisingAppsiteFlag		
											 , @FundraisingEventFlag					
											 , @MoreInformationURL
											 , @PurchaseProductURL
											 , @UserID
											 , GETDATE()
											 , 1
					
				
						SET @FundraisingID =SCOPE_IDENTITY()

						INSERT INTO HcFundraisingCategoryAssociation(HcFundraisingID
																	,HcFundraisingCategoryID
																	--,HcFundraisingDepartmentID
																	,HcHubCitiID
																	,CreatedUserID															
																	,DateCreated)
												 
														 SELECT @FundraisingID
															   ,@HcFundraisingCategoryID
															   --,@HcFundraisingDepartmentID
															   ,@HcHubCitiID
															   ,@UserID
															   ,GETDATE()
															

						--If Appsite associated, insert into Fundraising Appsite table
						IF @FundraisingAppsiteFlag = 1
						BEGIN

							INSERT INTO HcFundraisingAppsiteAssociation(HcFundraisingID
																	,HcAppsiteID
																	,HcHubCitiID
																	,CreatedUserID
																	,DateCreated)
													
														SELECT @FundraisingID
															  ,@AppsiteID
															  ,@HcHubCitiID											  
															  ,@UserID 
															  ,GETDATE()
						END
						
						--If Fundraising is Organization hosting, insert into HcFundraisingLocation table
						IF @FundraisingAppsiteFlag = 0
						BEGIN
							INSERT INTO HcFundraisingLocation(HcFundraisingID
															, HcHubCitiID
															, Address
															, City
															, State
															, PostalCode
															, Latitude
															, Longitude
															, DateCreated
															, CreatedUserID
															, GeoErrorFlag
															)
													SELECT @FundraisingID
															, @HcHubCitiID
															, @Address
															, @City
															, @State
															, @PostalCode
															, @Latitude
															, @Longitude
															, GETDATE()
															, @UserID
															, @GeoErrorFlag
						END

						--If Events associated, insert into Fundraising Event Association table
						IF @FundraisingEventFlag = 1
						BEGIN
							INSERT INTO HcFundraisingEventsAssociation(HcFundraisingID
																		,HcEventID
																		,HcHubCitiID
																		,CreatedUserID															
																		,DateCreated)
																
															SELECT @FundraisingID
																  ,Param 
																  ,@HcHubCitiID
																  ,@UserID
																  ,GETDATE()					  
															FROM dbo.fn_SplitParam(@EventID, ',') 
						END		
			END

			ELSE IF @FundraisingID IS NOT NULL
			BEGIN 
				UPDATE HcFundraising SET FundraisingName = @FundraisingName
										,FundraisingOrganizationName = @FundraisingOrganizationName
										--,FundraisingOrganizationName = IIF((@AppsiteID IS NULL AND @FundraisingAppsiteFlag = 0),@FundraisingOrganizationName,(SELECT HcAppSiteName FROM HcAppSite WHERE HcAppSiteID = @AppsiteID))
										,FundraisingOrganizationImagePath = @FundraisingOrganizationImagePath
										,ShortDescription = @ShortDescription
										,LongDescription = @LongDescription
										,HcHubCitiID = @HcHubCitiID
										,StartDate = CAST(@StartDate AS DATETIME)--+' '+CAST(@StartTime AS DATETIME) 
										,EndDate = CAST(@EndDate AS DATETIME)--+' '+CAST(@EndTime AS DATETIME)
										,FundraisingGoal = @FundraisingGoal
										,CurrentLevel = @CurrentLevel										
										,FundraisingAppsiteFlag = @FundraisingAppsiteFlag 
										,FundraisingEventFlag = @FundraisingEventFlag
										,MoreInformationURL = @MoreInformationURL
										,PurchaseProductURL = @PurchaseProductURL
										,ModifiedUserID	= @UserID									
										,DateModified = GETDATE()
										,Active = 1
               WHERE HcFundraisingID = @FundraisingID

			   --UPDATE HcFundraisingCategoryAssociation SET HcFundraisingCategoryID = @HcFundraisingCategoryID
						--								  ,HcFundraisingDepartmentID = @HcFundraisingDepartmentID
						--								  ,HcHubCitiID = @HcHubCitiID
						--								  ,ModifiedUserID = @UserID															
						--								  ,DateModified = GETDATE()
		       --WHERE HcFundraisingID = @FundraisingID

				DELETE FROM HcFundraisingCategoryAssociation WHERE HcFundraisingID = @FundraisingID 

				INSERT INTO HcFundraisingCategoryAssociation(HcFundraisingID
																,HcFundraisingCategoryID
																--,HcFundraisingDepartmentID
																,HcHubCitiID
																,CreatedUserID															
																,DateCreated)
												 
													 SELECT @FundraisingID
														   ,@HcFundraisingCategoryID
														   --,@HcFundraisingDepartmentID
														   ,@HcHubCitiID
														   ,@UserID
														   ,GETDATE()


			    --If Appsite associated, insert into Fundraising Appsite table
				IF @FundraisingAppsiteFlag = 1
				BEGIN	
					
					DELETE FROM HcFundraisingAppsiteAssociation WHERE HcFundraisingID = @FundraisingID
					DELETE FROM HcFundraisingLocation WHERE HcFundraisingID = @FundraisingID

					INSERT INTO HcFundraisingAppsiteAssociation(HcFundraisingID
																,HcAppsiteID
																,HcHubCitiID
																,ModifiedUserID
																,DateModified)
													
														SELECT @FundraisingID
															  ,@AppsiteID
															  ,@HcHubCitiID											  
															  ,@UserID 
															  ,GETDATE()
				END
				
				--If Fundraising is Organization hosting, insert into HcFundraisingLocation table
				IF @FundraisingAppsiteFlag = 0
				BEGIN

				DELETE FROM HcFundraisingAppsiteAssociation WHERE HcFundraisingID = @FundraisingID
				DELETE FROM HcFundraisingLocation WHERE HcFundraisingID = @FundraisingID

					INSERT INTO HcFundraisingLocation(HcFundraisingID
													, HcHubCitiID
													, Address
													, City
													, State
													, PostalCode
													, Latitude
													, Longitude
													, DateModified
													, ModifiedUserID
													, GeoErrorFlag
													)
											SELECT @FundraisingID
													, @HcHubCitiID
													, @Address
													, @City
													, @State
													, @PostalCode
													, @Latitude
													, @Longitude
													, GETDATE()
													, @UserID
													, @GeoErrorFlag
				END

				--If Events associated, insert into Fundraising Event Association table			
				IF @FundraisingEventFlag = 0
				BEGIN

					DELETE FROM HcFundraisingEventsAssociation WHERE HcFundraisingID = @FundraisingID
                END
				ELSE
				BEGIN
					DELETE FROM HcFundraisingEventsAssociation WHERE HcFundraisingID = @FundraisingID
					INSERT INTO HcFundraisingEventsAssociation(HcFundraisingID
																,HcEventID
																,HcHubCitiID
																,ModifiedUserID
																,DateModified)
																
													SELECT @FundraisingID
														  ,Param 
														  ,@HcHubCitiID
														  ,@UserID
														  ,GETDATE()					  
													FROM dbo.fn_SplitParam(@EventID, ',') 
				END		
			END		
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingEventsCreationAndUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
