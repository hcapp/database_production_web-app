USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceFilterDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceFilterDeletion
Purpose					: To delete Filter in Hubciti.
Example					: usp_WebHcCityExperienceFilterDeletion

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			26/11/2013	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceFilterDeletion]
(
    --Input variable
      @HubCitiID int
    , @FilterID int
  
	--Output Variable 
	, @Associated bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION

			DECLARE @Count INT
			SELECT @Associated = IIF(COUNT(1)>0, 1, 0)
			FROM HcBottomButton A
			INNER JOIN HcBottomButtonLinkType B ON A.BottomButtonLinkTypeID = B.HcBottomButtonLinkTypeID
			WHERE B.BottomButtonLinkTypeName = 'Filters'
			AND HcHubCitiID = @HubCitiID

			SELECT @Count = COUNT(1)
			FROM HcFilter 
			WHERE HcHubCitiID = @HubCitiID		

			IF (@Associated = 0 OR @Count <> 1)
			BEGIN					
				DELETE FROM HcFilterRetailLocation 
				WHERE HcFilterID = @FilterID
			
				--To delete Filter
				DELETE FROM HcFilter 
				WHERE HcFilterID = @FilterID
				AND HcHubCitiID = @HubCitiID
			END

			SELECT @Associated = IIF(@Count = 1 AND @Associated = 1, 1 ,0)
			
			--Confirmation of Success
			SELECT @Status = 0	
			
		COMMIT TRANSACTION      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceFilterDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
			ROLLBACK TRANSACTION
		END;
		 
	END CATCH;
END;









GO
