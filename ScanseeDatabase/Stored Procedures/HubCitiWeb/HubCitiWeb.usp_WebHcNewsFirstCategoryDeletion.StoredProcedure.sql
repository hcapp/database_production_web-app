USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstCategoryDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[[usp_WebHcNewsCategoryUpdation&Deletion]]]
Purpose					: To delete news settings
Example					: [[[usp_WebHcNewsCategoryUpdation&Deletion]]]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[usp_WebHcFindCategoryimagesList]
----------------------------------------------------------------------------------------------
*/


--exec [HubCitiWeb].[usp_WebHcNewsFirstCategoryUpdation&Deletion] 1,null,null,null,null,null,null,null
CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstCategoryDeletion]
(   
	--Input variable
	  @HcHubCitiID int
	, @NewsSettingsID VARCHAR(1000)

	--Output Variable 
	, @isDeleted BIT output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @SortOrder INT
				   ,@NewsCategoryID INT
				   ,@NewsCategoryName varchar(1000)
				   ,@Sort INT
				   ,@NewsCategoryName1 varchar(1000)
				   ,@SortOrder1 int

				  
			SELECT @SortOrder = N.SortOrder
				 , @NewsCategoryID = N.NewsCategoryID
				 , @NewsCategoryName = ISNULL(NewsCategoryName,NewsLinkCategoryName)
			FROM NewsFirstSettings N
			INNER JOIN NewsFirstCategory NC ON NC.NewsCategoryID = N.NewsCategoryID
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.categoryID = NC.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID
			WHERE HcNewsSettingsID = @NewsSettingsID AND Active = 1

			SELECT @SortOrder1 = SortOrder
				 , @NewsCategoryName1 = NewsLinkCategoryName
			FROM NewsFirstSettings 
			WHERE HcNewsSettingsID = @NewsSettingsID 

			SELECT @Sort = SortOrder
			FROM HcDefaultNewsFirstBookmarkedCategories
			WHERE HcHubCitiID = @HcHubCitiID AND (NewsCategoryID = @NewsCategoryID OR NewsLinkCategoryName = @NewsCategoryName1)

			IF EXISTS(SELECT * FROM HcMenuItem M 
					 INNER JOIN HcMenu MM ON MM.hcmenuid = M.hcmenuid  WHERE RTRIM(LTRIM(MenuItemName)) = @NewsCategoryName AND HcHubCitiID = @HcHubCitiID)
			BEGIN
					--Delete Unsuccesfull
					SET @isDeleted = 0
			END
			ELSE IF NOT EXISTS(SELECT * FROM HcMenuItem M 
					 INNER JOIN HcMenu MM ON MM.hcmenuid = M.hcmenuid  WHERE MenuItemName = @NewsCategoryName AND HcHubCitiID = @HcHubCitiID)
			BEGIN
					--To delete Subcategories from default, user and settings table
					DELETE FROM NewsfirstSubCategorySettings 
					WHERE NewsCategoryID = @NewsCategoryID  AND HcHubCitiID = @HcHubCitiID

					DELETE FROM HcUserNewsFirstSideNavigationSubCategories 
					WHERE NewsFirstCategoryID = @NewsCategoryID  AND HcHubCitiID = @HcHubCitiID

					DELETE FROM NewsfirstSubCategorySettings 
					WHERE NewsCategoryID = @NewsCategoryID AND HcHubCitiID = @HcHubCitiID

					DELETE FROM HcDefaultNewsFirstBookmarkedSubCategories 
					WHERE NewsFirstCategoryID = @NewsCategoryID AND HcHubCitiID = @HcHubCitiID
			

					--To delete Categories from default, user and settings table
					DELETE FROM NewsFirstSettings 
					WHERE HcNewsSettingsID = @NewsSettingsID 
			
					DELETE FROM HcUserNewsFirstSideNavigation 
					WHERE NewsSideNavigationID = @NewsCategoryID AND HcHubCitiID = @HcHubCitiID

					DELETE FROM HcUserNewsFirstBookmarkedCategories 
					WHERE(NewsCategoryID = @NewsCategoryID OR NewsLinkCategoryName = @NewsCategoryName1) AND HcHubCitiID = @HcHubCitiID

					DELETE FROM HcDefaultNewsFirstBookmarkedCategories
					WHERE (NewsCategoryID = @NewsCategoryID OR NewsLinkCategoryName = @NewsCategoryName1) AND HcHubCitiID = @HcHubCitiID

					--To update Sort order
					UPDATE NewsFirstSettings
					SET SortOrder = SortOrder - 1 
					FROM NewsFirstSettings 
					WHERE SortOrder > ISNULL(@SortOrder,@SortOrder1) AND HcHubCitiID = @HcHubCitiID 

					UPDATE HcDefaultNewsFirstBookmarkedCategories
					SET SortOrder = SortOrder - 1 
					FROM HcDefaultNewsFirstBookmarkedCategories 
					WHERE SortOrder > @Sort AND HcHubCitiID = @HcHubCitiID 

					--Deleted succesfully
					SET @isDeleted = 1
			END

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
