USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFAQCategoryDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFAQCategoryDeletion
Purpose					: Delete selected FAQ Category.
Example					: usp_WebHcFAQCategoryDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFAQCategoryDeletion]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int	
	, @HcFAQCategoryID Int
	  
	--Output Variable 
	, @Associateflag Bit output		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
				--Check if any FAQ  associated to the selected Category
								
				SELECT @Associateflag=(CASE WHEN F.HcFAQCategoryID IS NULL THEN 0 ELSE 1 END)
				FROM HcFAQCategory C
				LEFT JOIN HcFAQ F ON F.HcFAQCategoryID =C.HcFAQCategoryID 
				WHERE C.HcHubCitiID=@HcHubcitiID AND C.HcFAQCategoryID =@HcFAQCategoryID 	 				
				
				--Delete category from 	HcFAQCategory	
				DELETE FROM HcFAQCategory 
				WHERE HcFAQCategoryID=@HcFAQCategoryID AND HcHubCitiID =@HcHubcitiID 

	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQCategoryDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
