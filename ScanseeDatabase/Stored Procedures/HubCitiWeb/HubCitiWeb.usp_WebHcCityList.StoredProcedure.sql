USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityList
Purpose					: To display list of Cities.
Example					: usp_WebHcCityList

History
Version		    Date		   Author		    Change Description
------------------------------------------------------------------- 
1.0			    16thSept2014   Dhananjaya TR  	1.1
-------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityList]
(
   
    --Input variable 	  
	  @HcHubcitiID int
	, @UserID int
  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

	     --To display List of assciated cities to a Region App
		 SELECT DISTINCT  C.HcCityID cityExpId
						   ,C.CityName cityExpName						 
		 FROM HcHubCiti H
		 LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID =RA.HcHubcitiID OR (H.HcHubCitiID =RA.HcRegionAppID AND RA.HcRegionAppID =@HcHubcitiID)
		 INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID
		 INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID		
		 WHERE H.HcHubCitiID  = @HcHubcitiID
	     ORDER BY C.CityName     

	    --Confirmation of Success.
		SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [HubCitiWeb].[usp_WebHcCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
