USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRetailerAssociationDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcRetailerAssociationDeletion]
Purpose					: To DeAssociate Retailers to Hubciti
Example					: [usp_WebHcRetailerAssociationDeletion]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			1/20/2014	    SPAN		    1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcRetailerAssociationDeletion]
(
    --Input variable
      @HubCitiID int
   -- , @RetailID int
    , @RetailLocationIDs varchar(500)  --Comma separated RetailLocationID's

	--Output Variable 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION

			--To DeAssociate Retailers for given HubCiti
			UPDATE HcRetailerAssociation
			SET Associated = 0
			FROM HcRetailerAssociation R
			INNER JOIN fn_SplitParam (@RetailLocationIDs,',') P ON P.Param =R.RetailLocationID AND HcHubCitiID = @HubCitiID 	
			
			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		    ------------------------------------------------
										
			--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION

	END TRY

BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcRetailerAssociationDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
END CATCH;

END;









GO
