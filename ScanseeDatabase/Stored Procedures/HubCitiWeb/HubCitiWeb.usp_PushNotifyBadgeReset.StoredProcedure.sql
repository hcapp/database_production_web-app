USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_PushNotifyBadgeReset]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_PushNotifyBatchCount]
Purpose					: To Deal Of the Day for a Hubciti.
Example					: [usp_PushNotifyBatchCount]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
			15/12/2015      Sagar Byali   
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_PushNotifyBadgeReset]
(
    --Input variable
	  @UserID int
	, @HcHubCitiID Int
	, @DeviceID VARCHAR(100)

    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	

	       
		   UPDATE HcUserToken
		   SET PushNotifyBadgeCount = 0 
		   WHERE DeviceID = @DeviceID AND HcHubCitiID= @HcHubCitiID
	

	--Confirmation of Success
			SELECT @Status = 0
	
	END TRY

	
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDelaoftheDayCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
