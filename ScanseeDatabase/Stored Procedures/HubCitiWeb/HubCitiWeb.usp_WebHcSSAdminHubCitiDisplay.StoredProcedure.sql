USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminHubCitiDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcSSAdminHubCitiCreation
Purpose					: To disply the list of the Hub Cities that are created by the SS admin.
Example					: usp_WebHcSSAdminHubCitiCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcSSAdminHubCitiDisplay]
(
   ----Input variable.
	  @ScanSeeAdminUserID int
	, @ShowDeactivated bit
	, @SearchKey varchar(100)
	, @LowerLimit int
	, @AppRole varchar(100)
	  
	--Output Variable 
	, @FindClearCacheURL varchar(1000) output
    , @Status int output
    , @MaxCnt int output
    , @NextPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
			DECLARE @ScreenContent int
			
			SELECT @ScreenContent = ScreenContent       
		    FROM AppConfiguration       
		    WHERE ScreenName = 'All'     
		    AND ConfigurationType = 'HubCiti - Website Pagination'    
		    AND Active = 1  
		    
		    SELECT @UpperLimit = @ScreenContent + @LowerLimit
		    
		    --Display the hub cities created by the SS admin.
			SELECT DISTINCT HC.HcHubCitiID
				 , HubCitiName				
				 , UserName
				 , Active
			INTO #Temp1
			FROM HcHubCiti HC
			--LEFT JOIN HcLocationAssociation HL ON HL.HcHubCitiID = HC.HcHubCitiID 
			LEFT JOIN Users HCA ON HCA.HcHubCitiID = HC.HcHubCitiID 
			LEFT JOIN UserType T ON T.UserTypeID =HCA.UserTypeID 
			INNER JOIN HcAppList HA ON HC.HcAppListID = HA.HcAppListID
			--LEFT JOIN State S ON S.Stateabbrevation = HL.State
			--LEFT JOIN GeoPosition G ON G.State = S.Stateabbrevation AND G.PostalCode = HL.PostalCode AND G.State = HL.State
			WHERE ((@ShowDeactivated = 1) OR (@ShowDeactivated = 0 AND Active = 1))	
			AND T.UserType LIKE 'HubCiti Admin'	AND HA.HcAppListName = @AppRole
		    AND ((@SearchKey IS NOT NULL AND HubCitiName LIKE '%'+@SearchKey+'%') OR @SearchKey IS NULL)
		    GROUP BY HC.HcHubCitiID, HubCitiName, UserName, Active
			
			
			--AND HC.HubCitiName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END
				
			
			
			SELECT RowNum = ROW_NUMBER() OVER (ORDER BY HubCitiName ASC)
				 , HcHubCitiID
				 , HubCitiName				 			
				 , UserName
				 , Active
			INTO #Temp
			FROM #Temp1 			
			
			--Get the total no. of records for qualifying the search criteria.
			SELECT @MaxCnt = COUNT(1) FROM #Temp
			
			--Check if there are more records for next page.
			SELECT @NextPageFlag = (CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END) 
			
			--Display the list of hub cities.
			SELECT RowNum
			     , HcHubCitiID HubCitiID
				 , HubCitiName						
				 , UserName
				 , Active
			FROM #Temp
			WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit   
			

			-------Find Clear Cache URL---03/04/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcSSAdminHubCitiCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
