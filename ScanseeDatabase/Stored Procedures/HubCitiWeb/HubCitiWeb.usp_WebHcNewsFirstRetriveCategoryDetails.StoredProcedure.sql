USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstRetriveCategoryDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNewsFirstRetriveCategoryDetails]
Purpose					: To retrive Category Details
Example					: [usp_WebHcNewsFirstRetriveCategoryDetails]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[usp_WebHcNewsFirstRetriveCategoryDetails]
2.0         17/9/2016        Shilpashree            Adding Default Category flag
3.0			28/9/2016		 Sagar Byali			Adding News ticker 
3.1			12/15/2016		 Bindu T A				Back Button Changes
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstRetriveCategoryDetails]
(   
	--Input variable
	  @HcHubCitiID int
	, @NewsSettingsID INT

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
			
		  SELECT DISTINCT S.HcNewsSettingsID newsSettingsID
						  ,N.NewsCategoryID catId 
						  ,ISNULL(N.NewsCategoryDisplayValue,S.NewsLinkCategoryName) catName
						  ,TT.NewsCategoryTempleteTypeDisplayValue displayTypeName
						  ,TT.NewsCategoryTempleteTypeID displayTypeId
						  ,S.NewsCategoryColor catColor
						  ,ISNULL(S.BackButtonColor,'#ffffff') backButtonColor
						  ,S.NewsFeedURL catfeedURL
						 -- ,SS.NewsSubCategoryID newsSubCatID
						 -- ,SS.NewsSubCategoryDisplayValue newsSubCatName
						  ,isSubCatFlag = CASE WHEN SS.NewsSubCategoryID IS NOT NULL THEN 1 ELSE 0 END
						  ,SP.NewsFirstSubPageID subPageId
						  ,SP.NewsFirstSubPageDisplayName subPageName 
						  ,DefaultCategory As IsDefault
						  ,S.NewsCategoryFontColor catFontColor
						  --Adding News ticker
						  ,S.NewsTicker as isNewsTicker
						  ,S.NewsStories as noStories
						  ,IsNewsFeed isFeed
			FROM NewsFirstSettings S
			LEFT JOIN NewsFirstCategory N ON N.NewsCategoryID = S.NewsCategoryID AND N.Active = 1 
			LEFT JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = N.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID
			LEFT JOIN NewsFirstSubPage SP ON SP.NewsFirstSubPageID = S.NewsFirstSubPageID
			LEFT JOIN NewsFirstCategoryTempleteType TT ON TT.NewsCategoryTempleteTypeID = S.NewsFirstCategoryTempleteTypeID
			LEFT JOIN NewsFirstSubCategoryType NC ON NC.NewsCategoryID = N.NewsCategoryID
			LEFT JOIN NewsFirstSubCategory SS ON NC.NewsSubCategoryTypeID = SS.NewsSubCategoryTypeID
			WHERE S.HcHubCitiID = @HcHubCitiID AND S.HcNewsSettingsID = @NewsSettingsID
			--

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsFirstRetriveCategoryDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
