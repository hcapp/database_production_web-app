USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceFilterDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceFilterDetails
Purpose					: To display Filter Details.
Example					: usp_WebHcCityExperienceFilterDetails

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceFilterDetails]
(
    --Input variable
      @HubCitiID int
    , @FilterID int
     
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			
			
			DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To display Filter Details
			SELECT  HF.FilterName
					,@Config+cast (@HubCitiID as varchar)+'/'++ButtonImagePath ImagePath
					,ButtonImagePath logoImageName
					,FilterDescription ShortDescription            -- New changes for Filter description				
			FROM HcFilter HF			
			WHERE HF.HcFilterID = @FilterID
			
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceFilterDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
