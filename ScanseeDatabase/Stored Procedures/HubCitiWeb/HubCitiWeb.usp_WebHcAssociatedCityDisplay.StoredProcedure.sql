USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAssociatedCityDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAssociatedCityDisplay]
Purpose					: To disply list of associated cities for given HubCiti and state.
Example					: [usp_WebHcAssociatedCityDisplay]

History
Version		Date			Author		 Change Description
--------------------------------------------------------------- 
1.0			1/21/2014	    SPAN		       1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAssociatedCityDisplay]
(
    
    --Input variable
      @HubCitiID int
	, @State varchar(100)  
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT DISTINCT City
			FROM HcLocationAssociation
			WHERE HcHubCitiID = @HubCitiID
				AND [State] = @State
			ORDER BY City
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAssociatedCityDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
