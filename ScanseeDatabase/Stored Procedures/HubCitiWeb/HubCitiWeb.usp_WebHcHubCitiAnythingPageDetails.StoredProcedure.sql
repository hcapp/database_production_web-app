USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAnythingPageDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAnythingPageDetails
Purpose					: Update Any thing Page.
Example					: usp_WebHcHubCitiAnythingPageDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAnythingPageDetails]
(
   ----Input variable.	
   	  @AnythingPageID int
	, @HcHubCitiID Int
	  
	--Output Variable 	
	, @MenuItemExist bit output			
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
		DECLARE @HubCitiConfig VARCHAR(1000)
		
		SELECT @HubCitiConfig = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration' 
		
		SET @MenuItemExist = 0
		
			IF EXISTS (SELECT HcMenuItemID
							FROM HcMenuItem MI 
							INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID
							WHERE LinkTypeName = 'AnythingPage' AND LinkID = @AnythingPageID)
			BEGIN 
				SET @MenuItemExist = 1
			END
				
			SELECT AnythingPageName pageTitle
				 , ShortDescription
				 , LongDescription
				 , StartDate
				 , EndDate
				 , logoImageName = @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + ImagePath
				 , ImagePath ImageName
				 , ImageIcon imageIconID
				 , URL pageAttachLink
				 , MediaPath = @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + MediaPath
				 , Mediapath pathName
				 , CASE WHEN URL IS NULL OR URL = '' THEN  HcAnyThingPageMediaTypeID ELSE (SELECT HcAnyThingPageMediaTypeID FROM HcAnyThingPageMediaType WHERE HcAnyThingPageMediaType = 'Website') END pageType
			FROM HcAnythingPage A
			LEFT JOIN HcAnythingPageMedia B ON A.HcAnythingPageID = B.HcAnythingPageID
			LEFT JOIN HcAnyThingPageMediaType C ON C.HcAnyThingPageMediaTypeID = B.MediaTypeID
			WHERE A.HcAnythingPageID = @AnythingPageID
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAnythingPageDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
