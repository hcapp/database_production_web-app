USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceRetailerListCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceRetailerListCreation
Purpose					: To Associate RetailLocations to CityExperience in Hubciti.
Example					: usp_WebHcCityExperienceRetailerListCreation

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceRetailerListCreation]
(
    --Input variable
	  @UserID int
    , @HubCitiID int
    , @CityExperienceID int
    , @RetailLocationID varchar(Max) --Comma separated ID's
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION
			
			--To Delete previously associated RetailLocations
			DELETE FROM HcCityExperienceRetailLocation
			WHERE HcCityExperienceID = @CityExperienceID
			
			--To Associate RetailLocations to given CityExperience	
			INSERT INTO HcCityExperienceRetailLocation( HcCityExperienceID
													   , RetailLocationID
													   , CreatedUserID
													   , DateCreated
													   )
												SELECT @CityExperienceID
													   , P.Param
													   , @UserID
													   , GETDATE()
												FROM dbo.fn_SplitParam(@RetailLocationID,',')P
																								
			
			--Confirmation of Success
			SELECT @Status = 0	
			      
		COMMIT TRANSACTION
		
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceRetailerListCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
			ROLLBACK TRANSACTION
		END;
		 
	END CATCH;
END;







GO
