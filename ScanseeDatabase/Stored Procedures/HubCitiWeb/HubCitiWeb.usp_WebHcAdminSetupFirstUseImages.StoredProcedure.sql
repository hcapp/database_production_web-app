USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminSetupFirstUseImages]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminSetupFirstUseImages
Purpose					: To set up and update First use images for the HubCiti App.
Example					: usp_WebHcAdminSetupFirstUseImages

History
Version		Date			Author			Change Description
--------------------------------------------------------------------------------------------
1.0			18/8/2016	    Shilpashree		       1.0
2.0			17/11/2016		Sagar Byali		Adding Weblink / anything pages with in an image
-------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminSetupFirstUseImages]
(
   --Input variable
      @HCAdminUserID int
	, @HubCitiID int
	, @ScreenName Varchar(50)
	, @Type Varchar(10)
	, @FirstUseImages varchar(1000)
	, @AssociatePageURL varchar(100)
	, @AssociatePageURLTypeID varchar(100)
	, @AssociatePageURLValue varchar(1000)

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			IF (@ScreenName = '' AND @FirstUseImages = '')
			BEGIN
				 SET @ScreenName = NULL
				 SET @FirstUseImages = NULL
			END
			
			DELETE FROM FirstUseLoginImage WHERE HcHubCitiID = @HubCitiID

			IF (@ScreenName IS NOT NULL AND @FirstUseImages IS NOT NULL)
			BEGIN
				
				SELECT RowNumber = IDENTITY(INT,1,1)
					 , ScreenName = Param
					INTO #Screens
						FROM fn_SplitParam(@ScreenName,',')
				
				SELECT RowNumber = IDENTITY(INT,1,1)
					 , Type = Param
					INTO #Types
						FROM fn_SplitParam(@Type,',')

				SELECT RowNumber = IDENTITY(INT,1,1)
					 , FirstUseImages = Param
					INTO #Images
						FROM fn_SplitParam(@FirstUseImages,',')

				SELECT RowNumber = IDENTITY(INT,1,1)
					 , AssociatePageURL = Param
					INTO #AssociatePageURL
						FROM fn_SplitParam(@AssociatePageURL,',')


				SELECT RowNumber = IDENTITY(INT,1,1)
					 , AssociatePageURLTypeID = Param
					INTO #AssociatePageURLTypeID
						FROM fn_SplitParam(@AssociatePageURLTypeID,',')	

				SELECT RowNumber = IDENTITY(INT,1,1)
					 , AssociatePageURLValue = Param 
					INTO #AssociatePageURLValue
						FROM fn_SplitParam(@AssociatePageURLValue,',')


				INSERT INTO FirstUseLoginImage (HcHubCitiID
												, ScreenName
												, Type
												, FirstUseImage
												, DateCreated
												, CreatedUserID
												, AssociatePageURL
												, AssociatePageURLTypeID
												, AssociatePageURLValue)

										SELECT    @HubCitiID
												, S.ScreenName
												, T.Type
												, I.FirstUseImages
												, GETDATE()
												, @HCAdminUserID
												, U.AssociatePageURL
												, CASE WHEN TT.AssociatePageURLTypeID = 'NULL' THEN NULL ELSE TT.AssociatePageURLTypeID END AssociatePageURLTypeID
												, CASE WHEN YY.AssociatePageURLValue = 'NULL' THEN NULL ELSE YY.AssociatePageURLValue END AssociatePageURLValue
												
											FROM #Screens S
												INNER JOIN #Types T on S.RowNumber = T.RowNumber
												INNER JOIN #Images I on T.RowNumber = I.RowNumber
												INNER JOIN #AssociatePageURL U ON U.RowNumber = I.RowNumber
												INNER JOIN #AssociatePageURLTypeID TT ON TT.RowNumber = U.RowNumber
												INNER JOIN #AssociatePageURLValue YY ON YY.RowNumber = TT.RowNumber		
				
			END
			   --Confirmation of Success.
			   SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
		SET @ErrorNumber = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @Status = 1 
	END CATCH;
	
END;
GO
