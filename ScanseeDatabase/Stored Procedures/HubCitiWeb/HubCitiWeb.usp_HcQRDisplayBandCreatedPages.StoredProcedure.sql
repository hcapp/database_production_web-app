USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HcQRDisplayBandCreatedPages]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*  
Stored Procedure name	: [usp_HcQRDisplayBandCreatedPages]
Purpose					: To Retrieve the list of Custom pages(Anything Page) associated with Band.
Example					: [usp_HcQRDisplayBandCreatedPages]

History
Version			Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22th Apr 2016	Sagar Byali	        Initial version : usp_HcQRDisplayRetailerCreatedPages
---------------------------------------------------------------------------------------------------*/

 CREATE PROCEDURE [HubCitiWeb].[usp_HcQRDisplayBandCreatedPages]
(
	  @BandID int
	, @HcHubCitiID int
    
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Configuration VARCHAR(1000)
		DECLARE @QRType INT
		DECLARE @RetailerConfig varchar(500) =  'http://10.122.61.145:8080/Images/retailer/'
		DECLARE @ScanType int
		DECLARE	@RowCount int		
		DECLARE @RetailEventsIconLogo Varchar(1000)
		DECLARE @RetailFundRaisingIconLogo Varchar(1000)
		DECLARE @EventExists bit
		DECLARE @FundRaisingExists bit

		SET @EventExists = 0
		SET @FundRaisingExists = 0
	
		SELECT @EventExists = 1 
		FROM HcBandEvents RE 
		WHERE BandID = @BandID AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND RE.Active=1 
		
		
		--Get the Server Configuration.
		SELECT  @Configuration= ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'QR Code Configuration' AND Active = 1		
				 
		SELECT @QRType = QRTypeID 
		FROM QRTypes 
		WHERE QRTypeName LIKE 'Anything Page'
		
		 
		SELECT @RetailEventsIconLogo = @RetailerConfig + ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Retailer Events Icon Logo'

		
		--Fetch all the Created Pages for the given Retailer Location		
		SELECT  DISTINCT QR.QRBandCustomPageID 
					   , QR.Pagetitle 
					   , pageImg = CASE WHEN [Image] IS NULL 
										  THEN (SELECT @RetailerConfig + QRbandCustomPageIconImagePath FROM QRBandCustomPageIcons WHERE QRbandCustomPageIconID = QR.QRbandCustomPageIconID) 
										  ELSE @RetailerConfig + CAST(ISNULL(@BandID, (SELECT BandID FROM Band RL WHERE RL.BandID = H.BandID AND rl.bandActive=1)) AS VARCHAR(10))+ '/' + [Image] END
					  , PageLink = CASE WHEN MediaPath IS NOT NULL THEN  @RetailerConfig + CAST(QR.BandID AS VARCHAR(10)) + '/'+ MediaPath ELSE QR.url END   
					  , ISNULL(SortOrder, 100000) SortOrder	
					  
		INTO #AnythingPagess	  
		FROM QRbandCustomPage QR
		LEFT JOIN QRBandCustomPageMedia M ON M.QRBandCustomPageID = QR.QRBandCustomPageID	
		LEFT JOIN HcbandAppSite H ON H.BandID = QR.BandID	
		WHERE (ISNULL(@BandID, 0) <> 0 AND QR.BandID = @BandID) 
		AND QR.QRTypeID = @QRType
		AND ((StartDate IS NULL AND EndDate IS NULL) OR (CAST(GETDATE() AS DATE) BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)))
		ORDER BY ISNULL(SortOrder, 100000) ,Pagetitle 
		
		--Check for the existance of the Anything Pages. 
		SELECT @RowCount =@@ROWCOUNT 

		SELECT QRBandCustomPageID pageID
			  ,Pagetitle pageTitle 
			  ,pageImg
			  ,PageLink
			  ,SortOrder
		FROM #AnythingPagess

	
				
				select @status = 0

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcQRDisplayRetailerCreatedPages.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;













GO
