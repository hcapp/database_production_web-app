USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcUserRegistrationFormsCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcUserRegistrationFormsCreation
Purpose					: Creating User Registration Forms For Hubciti.
Example					: usp_WebHcUserRegistrationFormsCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17/12/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcUserRegistrationFormsCreation]
(
  	  @HubCitiID Int	
	, @RegistrationFormFields Varchar(max)
	, @RequiredFlag Varchar(max)
	, @HcUserID Int
	, @ImagePath Varchar(1000)	
	  
	--Output Variable	
	--, @DuplicateFlag Bit OutPut
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION    	      
	
			SELECT Position = IDENTITY(INT, 1, 1)
				 , RegistrationFormFields = Param 
				 --, FieldID=U.HcUserRegistrationFormFieldsID 
			INTO #Split
			FROM dbo.fn_SplitParam(@RegistrationFormFields, ',') P
														
			SELECT ReqPosition = IDENTITY(INT, 1, 1)
				 , [Required] = Param
			INTO #Field
			FROM dbo.fn_SplitParam(@RequiredFlag, ',')Q
				
			SELECT T.Position
				 , T.RegistrationFormFields
				 , FieldID = U.HcUserRegistrationFormFieldsID
				 , RequiredFields = F.[Required] 
			INTO #RegistrationFields
			FROM #Split T
			INNER JOIN #Field F ON F.ReqPosition=T.Position    
			INNER JOIN HcUserRegistrationFormFields U ON U.RegistrationFieldsDisplayName = T.RegistrationFormFields
			ORDER BY T.Position
			
			DELETE FROM HcHubcitiUserRegistrationForms 
			Where HchubcitiID =@HubCitiID 	
			
			SELECT Position
				 , R.FieldID 
				 , R.RequiredFields					
			INTO #Temp
			FROM #RegistrationFields R	
			
			INSERT INTO HcHubcitiUserRegistrationForms (HchubcitiID
														,HcUserRegistrationFormFieldsID
														,Position
														,CreatedUserID
													    ,CreatedDate
														,[Required])
												SELECT @HubCitiID
													 , FieldID
													 , Position
													 , @HcUserID
													 , GETDATE()
													 , RequiredFields
												FROM #Temp
			
				
			UPDATE HcMenuCustomUI SET UserSettingsImagePath=@ImagePath 
			WHERE HubCitiId =@HubCitiID		
			
	       --Confirmation of Success.
	    SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcUserRegistrationFormsCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
