USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcQRDisplayRetailerCreatedPages]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcQRDisplayRetailerCreatedPages
Purpose					: To Retrieve the list of Custom pages(Anything Page) associated with the retailer location.
Example					: usp_WebHcQRDisplayRetailerCreatedPages

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Feb 2015	Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcQRDisplayRetailerCreatedPages]
(
	  
	  @HubCitiID int
	, @RetailID int
    , @RetailLocationID int
	--, @AppsiteID int
    
   
	--Output Variable 
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output 
	, @EventExists bit output
	, @FundRaisingExists bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Configuration VARCHAR(100)
		DECLARE @QRType INT
		DECLARE @RetailerConfig varchar(50)
		DECLARE @ScanType int
		DECLARE	@RowCount int		
		DECLARE @RetailEventsIconLogo Varchar(1000)
		DECLARE @RetailFundRaisingIconLogo Varchar(1000)

		SELECT @IOSAppID = IOSAppID
			  ,@AndroidAppID = AndroidAppID
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID

		SET @EventExists = 0
		SET @FundRaisingExists = 0
		
		--SELECT @RetailLocationID = ISNULL(@RetailLocationID , A.RetailLocationID)
		--	   ,@RetailID = ISNULL(@RetailID, RL.RetailID)
		--FROM HcAppSite A
		--INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
		--WHERE HcAppSiteID = @AppsiteID

		
		SELECT @EventExists = 1 
		FROM HcRetailerEventsAssociation RE 
		INNER  JOIN HcEvents E ON RE.HcEventID = E.HcEventID
		WHERE RE.RetailID = @RetailID AND RetailLocationID = @RetailLocationID
		--AND E.EndDate > GETDATE()
		AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active=1

		--Verify If FundRaising Exist For A retailer
		SELECT @FundRaisingExists = 1 
		FROM HcRetailerFundraisingAssociation RF 
		INNER  JOIN HcFundraising F ON F.HcFundraisingID =RF.HcFundraisingID 
		WHERE RF.RetailID = @RetailID AND RetailLocationID = @RetailLocationID
		--AND E.EndDate > GETDATE()
		AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active=1

		SELECT @ScanType = ScanTypeID
		FROM HubCitiReportingDatabase..ScanType
		WHERE ScanType = 'Retailer Summary QR Code'
			   
		SELECT @RetailerConfig= ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		--Get the Server Configuration.
		SELECT @Configuration = ScreenContent FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'QR Code Configuration'
		AND Active = 1		
				 
		SELECT @QRType = QRTypeID 
		FROM QRTypes 
		WHERE QRTypeName LIKE 'Anything Page' 

		SELECT @RetailEventsIconLogo=@RetailerConfig+ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Retailer Events Icon Logo'

		SELECT @RetailFundRaisingIconLogo=@RetailerConfig+ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Retailer Fundraiser Icon Logo'

		--Fetch all the Created Pages for the given Retailer Location		
		SELECT  DISTINCT QR.QRRetailerCustomPageID pageID
					   , QR.Pagetitle pageTitle
					   , CASE WHEN [Image] IS NULL THEN (SELECT @RetailerConfig + QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QR.QRRetailerCustomPageIconID) 
								ELSE @RetailerConfig + CAST(ISNULL(@RetailID, (SELECT RetailID FROM RetailLocation RL WHERE RL.RetailLocationID = @RetailLocationID AND RL.Active=1)) AS VARCHAR(10))+ '/' + [Image] END pageImage		 
				--	   , PageLink = CASE WHEN M.QRRetailerCustomPageID IS NOT NULL AND M.MediaPath LIKE '%pdf%' OR M.MediaPath LIKE '%png%'       H.RetailLocationID
				--		  THEN @RetailerConfig + CAST(QR.RetailID AS VARCHAR(10)) + '/' + M.MediaPath
				--		  ELSE 
				--			  CASE WHEN QR.URL IS NULL 
				--				   THEN @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') AS VARCHAR(10)) + '.htm?key1=' + CAST(QRCPA.RetailID AS VARCHAR(10)) + '&key2=' + CAST(QRCPA.RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10)) 
				--			  ELSE QR.URL END
				--		  END
					  , PageLink = @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') as varchar(10)) + '.htm?retId=' + CAST(QRCPA.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))++'&hubcitiId='+ CAST(ISNULL(@HubCitiId,'null') AS VARCHAR(10)) + CASE WHEN QR.URL IS NOT NULL OR M.MediaPath LIKE '%pdf' OR M.MediaPath LIKE '%png' THEN '&EL=true' ELSE '&EL=false'	END
					  , ISNULL(SortOrder, 100000) SortOrder	
					  , CASE WHEN ExternalFlag = 1 THEN MediaPath ELSE (SELECT @RetailerConfig + CAST(QR.RetailID AS VARCHAR(10))+ '/' + MediaPath FROM QRRetailerCustomPageMedia WHERE QRRetailerCustomPageMediaID = M.QRRetailerCustomPageMediaID) END pageTempLink		
		FROM QRRetailerCustomPage QR
		INNER JOIN QRRetailerCustomPageAssociation QRCPA ON QR.QRRetailerCustomPageID = QRCPA.QRRetailerCustomPageID
		LEFT JOIN QRRetailerCustomPageMedia M ON M.QRRetailerCustomPageID = QR.QRRetailerCustomPageID	
		--LEFT JOIN HcAppSite H ON H.RetailLocationID = QRCPA.RetailLocationID	
		WHERE ((ISNULL(@RetailLocationID, 0) <> 0 AND QRCPA.RetailLocationID = @RetailLocationID))
				--OR
			 --(ISNULL(@AppsiteID, 0) <> 0 AND H.HcAppSiteID = @AppsiteID))
		AND QR.QRTypeID = @QRType
		AND ((StartDate IS NULL AND EndDate IS NULL) 
		      OR 
		     (CAST(GETDATE() AS DATE) BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1))
		    )
		ORDER BY ISNULL(SortOrder, 100000) ,Pagetitle  

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcQRDisplayRetailerCreatedPages.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;








GO
