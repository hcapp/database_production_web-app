USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminHubCitiCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcSSAdminHubCitiCreation
Purpose					: To create the hub citi admin users by ScanSee admin.
Example					: usp_WebHcSSAdminHubCitiCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			11/9/2013	    Pavan Sharma K		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminHubCitiCreation]
(
   ----Input variable.
      @HcHubCitiID Int
	, @ScanSeeAdminUserID INT
	, @HubCitiName varchar(255)
	, @CityExperience varchar(255)	
	, @UserName varchar(255)
	, @Password varchar(255)
	, @EmailID varchar(255)
	, @AppRole varchar(100)
	, @HcHubCitiIDs varchar(max)
	, @DefaultPostalCode varchar(10)
	, @SalesPersonEmail varchar(1000)
	, @EmailIDs varchar(1000)
	  
	--Output Variable 
	, @FindClearCacheURL VARCHAR(1000) OUTPUT
    , @Status int output
	, @HubCitiID Int output
    , @DuplicateEmail bit output
    , @DuplicateHubCitiName bit output
    , @DuplicateUserName bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
			--DECLARE @HubCitiID INT
			DECLARE @HcAdminUserID INT
			SET @DuplicateEmail = 0
			SET @DuplicateUserName = 0
			SET @DuplicateHubCitiName = 0

			SET @EmailIDs = LTRIM(RTRIM(@EmailIDs))
			
			IF SUBSTRING(@EmailIDs,LEN(@EmailIDs),LEN(@EmailIDs)-1) = ';' 
			BEGIN
			SET @EmailIDs = LEFT(@EmailIDs,LEN(@EmailIDs) - 1)
			END

			select @EmailIDs
			
					
			--Check for User Name duplication.
			IF EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName AND @HcHubCitiID IS NULL) --NOT NULL AND HcHubCitiID <> @HcHubCitiID )
			BEGIN
				SET @DuplicateUserName = 1
			END

			--Check if the Hub Citi Name duplication.
			ELSE IF EXISTS(SELECT 1 FROM HcHubCiti WHERE HubCitiName = @HubCitiName AND (@HcHubCitiID IS NULL or @HcHubCitiID <> HcHubCitiID))
			BEGIN			    
				SET @DuplicateHubCitiName = 1
			END

			--Previous implementation

			--New implementation
			ELSE IF EXISTS (SELECT 1 FROM HcHubCiti WHERE @HcHubCitiID = HcHubCitiID) AND @HcHubCitiID IS NOT NULL AND @HubCitiName IS NOT NULL
			BEGIN
				--SELECT 'A'
				UPDATE HcHubCiti SET HubCitiName = @HubCitiName 
									,Active = 1								
									,DateModified = GETDATE()
									,ModifiedUserID = @ScanSeeAdminUserID
									,DefaultPostalCode = @DefaultPostalCode
									,SalesPersonEmail = @SalesPersonEmail
									,AdditionalEmailID = @EmailIDs
				WHERE HcHubCitiID = @HcHubCitiID 

				--Update Citi Experience.
				Update HcCityExperience SET CityExperienceName=@CityExperience
				WHERE HcHubCitiID = @HcHubCitiID 
				
				--Update EmailID (New requirement)
				UPDATE Users SET Email=@EmailID
				WHERE HcHubCitiID=@HcHubCitiID
				

				DELETE FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HcHubCitiID
				
				
				IF @HcHubCitiIDs IS NOT NULL 
				BEGIN					

					INSERT INTO HcRegionAppHubcitiAssociation(HcRegionAppID
															,HcHubcitiID
															,ModifiedUserID																
															,DateModified)
																
											SELECT    @HcHubCitiID
													, Param
													, @ScanSeeAdminUserID
													, GETDATE()														 
											FROM dbo.fn_SplitParam(@HcHubCitiIDs, ',')	
				END		
						
							--SELECT 'B'
			END

			ELSE			
				--Creation of Hub Citi.
				BEGIN					
				  
					INSERT INTO HcHubCiti(HubCitiName
										, Active
										, DateCreated
										, HcHubCitiKey
										, CreatedUserID
										, HcAppListID
										, DefaultPostalCode
										, SalesPersonEmail
										, AdditionalEmailID)
								SELECT @HubCitiName
									 , 1
									 , GETDATE()
									 , @HubCitiName
									 , @ScanSeeAdminUserID
									 , IIF(@AppRole='RegionApp',(SELECT HcAppListID FROM HcAppList WHERE HcAppListName='RegionApp'),(SELECT HcAppListID FROM HcAppList WHERE HcAppListName='HubCitiApp'))
									 , @DefaultPostalCode
									 , @SalesPersonEmail
									 , @EmailIDs
									 
					--Capture the last inserted identity value.
					SELECT @HubCitiID = SCOPE_IDENTITY()
					
					--Update HubCitiKey with Unique Value
					Update HcHubCiti SET HcHubCitiKey=@HubCitiName+CAST(@HubCitiID AS Varchar(10))
					WHERE HcHubCitiID = @HubCitiID 				
					
					--Create Default FAQ Category when hubciti is created
					INSERT INTO HcFAQCategory(CategoryName
											,HcHubCitiID
											,DateCreated										
											,CreatedUserID)
					VALUES('General',@HubCitiID,GETDATE(),@ScanSeeAdminUserID)


					--Created the user and associate to the Hub Citi.
					INSERT INTO Users(
											 Email
											,UserName
											,Password
											,HcHubCitiID
											,DateCreated
											,CreatedUserID
											,UserTypeID
											,Enabled)
								SELECT @EmailID
									 , @UserName
									 , @Password
									 , @HubCitiID
									 , GETDATE()
									 , @ScanSeeAdminUserID
									 , (SELECT UserTypeID FROM UserType WHERE UserType = 'HubCiti Admin')
									 , 1
									 
					SELECT @HcAdminUserID = SCOPE_IDENTITY()	
					
					--Associate the user created to the admin role			
					INSERT INTO HcUserRole(HcAdminUserID
										 , HcRoleID
										 , DateCreated
										 , CreatedUserID)
								SELECT @HcAdminUserID
									 , (SELECT HcRoleID FROM HcRole WHERE HcRoleName = 'ROLE_ADMIN')
									 , GETDATE()
									 , @ScanSeeAdminUserID
					
					--Associate City Experience to the Hub Citi.
					INSERT INTO HcCityExperience(CityExperienceName
											   , HcHubCitiID
											   , DateCreated
											   , CreatedUserID)
										SELECT @CityExperience
											 , @HubCitiID
											 , GETDATE()
											 , @HcAdminUserID
											 
					--Insert default colors to HubCiti
											 
					INSERT INTO HcMenuCustomUI(HubCitiId
												,MenuBackgroundColor
												--,MenuBackgroundImage
												,MenuButtonColor
												,MenuButtonFontColor
												,SubMenuBackgroundColor
												--,SubMenuBackgroundImage
												,SubMenuButtonColor
												,SubMenuButtonFontColor
												,MenuGroupBackgroundColor
												,MenuGroupFontColor
												,SubMenuGroupBackgroundColor
												,SubMenuGroupFontColor
												,MenuIconicFontColor
												,SubMenuIconicFontColor
												,backGroundColor
												,titleColor)
												--,HcTabbarButtonStateID)		
										SELECT	@HubCitiID	
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuBGClr' AND ScreenName = 'HubCitiMainMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuBtnClr' AND ScreenName = 'HubCitiMainMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuFntClr' AND ScreenName = 'HubCitiMainMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultSubMenuBGClr' AND ScreenName = 'HubCitiSubMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultSubMenuBtnClr' AND ScreenName = 'HubCitiSubMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultSubMenuFntClr' AND ScreenName = 'HubCitiSubMenu')
												,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuGrpBGClr' AND ScreenName = 'HubCitiMainMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuGrpFntClr' AND ScreenName = 'HubCitiMainMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultSubMenuGrpBGClr' AND ScreenName = 'HubCitiSubMenu')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultSubMenuGrpFntClr' AND ScreenName = 'HubCitiSubMenu')
												,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuSubMenuIconicFntClr' AND ScreenName = 'HubCitiMainMenuSubMenu')
												,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultMainMenuSubMenuIconicFntClr' AND ScreenName = 'HubCitiMainMenuSubMenu')
												,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultBackgroundClr' AND ScreenName = 'Others')
										        ,(SELECT ScreenContent From AppConfiguration where ConfigurationType = 'DefaultTitleClr' AND ScreenName = 'Others')
			
			          	--To fix the no records found issue if the user has not set up the Settings page.
						SELECT RowNum = ROW_NUMBER() OVER (ORDER BY HcUserRegistrationFormFieldsID)
								, HcUserRegistrationFormFieldsID
						INTO #Temp
						FROM HcUserRegistrationFormFields
						WHERE RegistrationFormFieldName IN ('FirstName','ZipCode')
										        
						INSERT INTO HcHubcitiUserRegistrationForms(HchubcitiID
																, HcUserRegistrationFormFieldsID
																, Position
																, CreatedDate
																, Required)
						SELECT @HubCitiID
								, HcUserRegistrationFormFieldsID
								, RowNum
								, GETDATE()
								, 0
						FROM #Temp	
						
						--Associating HubCities to Region App.
						IF @HcHubCitiIDs IS NOT NULL
						BEGIN

							--DELETE FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HcHubCitiID

							INSERT INTO HcRegionAppHubcitiAssociation(HcRegionAppID
																	,HcHubcitiID
																	,CreatedUserID																
																	,DateCreated)
																
													SELECT @HubCitiID
														 , Param
														 , @ScanSeeAdminUserID
														 , GETDATE()														 
													FROM dbo.fn_SplitParam(@HcHubCitiIDs, ',')	
						END							 		
			
			END

			-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------

			--To create standard bottom buttons

			--EXEC [HubCitiWeb].[usp_WebHcCreateBottomButtons] @HubcitiID, @ScanSeeAdminUserID, @Status = @Status OUTPUT ,@ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcSSAdminHubCitiCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













GO
