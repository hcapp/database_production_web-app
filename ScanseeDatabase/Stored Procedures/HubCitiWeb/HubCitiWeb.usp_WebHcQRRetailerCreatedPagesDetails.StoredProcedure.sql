USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcQRRetailerCreatedPagesDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcQRRetailerCreatedPagesDetails
Purpose					: To Display the Retailer Created Page Details.
Example					: usp_WebHcQRRetailerCreatedPagesDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th Jan 2015		Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcQRRetailerCreatedPagesDetails]
(	  
	  @RetailID int
    , @RetailLocationID int
    , @PageID int
	, @UserID int
	, @HubCitiID int
    
    --User Tracking Inputs
    , @ScanTypeID bit
    , @MainMenuID int
       
	--Output Variable 
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output  
	, @ExternalLinkFlag bit output
	, @ExternalLink varchar(1000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		
		     
		
			 DECLARE @RetailerConfig varchar(50)
			 DECLARE @MediaTypes VARCHAR(1000) = ''
			 DECLARE @MediaPath VARCHAR(MAX) = ''
			 DECLARE @ExternalFlag varchar(100) = ''
			 DECLARE @RetailLocations VARCHAR(1000) = ''
			 DECLARE @ScanType int

			 SELECT @IOSAppID = IOSAppID
					  ,@AndroidAppID = AndroidAppID
			 FROM HcHubCiti
			 WHERE HcHubCitiID = @HubCitiID
				
			 SELECT @ScanType = ScanTypeID
		     FROM ScanSeeReportingDatabase..ScanType
		     WHERE ScanType = 'Retailer Anything Page QR Code'
		     
		     SELECT @ExternalLinkFlag = CASE WHEN (B.QRRetailerCustomPageID IS NOT NULL AND B.MediaPath LIKE '%pdf%' OR B.MediaPath LIKE '%png%') OR (A.URL IS NOT NULL) THEN 1
										 ELSE 0 
									END
		     FROM QRRetailerCustomPage A
		     LEFT JOIN QRRetailerCustomPageMedia B ON A.QRRetailerCustomPageID = B.QRRetailerCustomPageID
		     WHERE A.QRRetailerCustomPageID = @PageID
		     
		     
		     CREATE TABLE #AnythingPages(pageID INT
									  , Pagetitle VARCHAR(1000)
									  , PageDescription VARCHAR(500)
									  , ShortDescription VARCHAR(500)
									  , LongDescription VARCHAR(1000)
									  , ImageName VARCHAR(1000)
									  , ImagePath VARCHAR(1000)
									  , MediaType VARCHAR(10)
									  , MediaPath VARCHAR(1000)
									  , ExternalFlag BIT
									  , RetailID INT	 
									  , StartDate DATETIME
									  , EndDate DATETIME
									  , Expired BIT
									  , RetailName VARCHAR(500)
									  , Logo VARCHAR(1000)
									  , QRType VARCHAR(100)
									  , AppDownloadLink VARCHAR(1000)
									  , QRUrl VARCHAR(1000))
		     
		     
			 --User Tracking
			 DECLARE @Rowcount int
			
			 --Get the server configuration.
			 SELECT @RetailerConfig= ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'

			 SELECT 
				   QRTypeCode
				 , QRRCP.RetailID		       
				 , QRRCP.QRRetailerCustomPageID  
				 , COUNT(QRRCPA.RetailLocationID) Counts
			 INTO #Temp
			 FROM QRRetailerCustomPage QRRCP  
			 INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
			 INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCP.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
			 WHERE QRRCPA.QRRetailerCustomPageID = @PageID
			 AND QRRCP.RetailID = @RetailID
			 GROUP BY QRTypeCode
					, QRRCP.RetailID		     
					, QRRCP.QRRetailerCustomPageID
			 
			 --Send the details of the anything page if the page is found to be an internal(Make your own).
			 IF @ExternalLinkFlag = 0
			 BEGIN
				--Get Associated Media Details
				 SELECT  @MediaTypes = COALESCE(@MediaTypes+',','') + CAST(PMT.ProductMediaType AS VARCHAR(10))
					   , @MediaPath = COALESCE(@MediaPath+',','') + @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + CAST(QR.MediaPath AS VARCHAR(100))
				 FROM QRRetailerCustomPageMedia QR
				 INNER JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = QR.MediaTypeID
				 WHERE QRRetailerCustomPageID = @PageID	 
				
				--If the Custom Page is associated with multiple locations.
				IF @RetailLocationID = 0
				BEGIN 
							--Get Page Details 
							INSERT INTO #AnythingPages(pageID  
									  , Pagetitle  
									  , PageDescription 
									  , ShortDescription  
									  , LongDescription  
									  , ImageName  
									  , ImagePath  
									  , MediaType  
									  , MediaPath  
									  , ExternalFlag 
									  , RetailID  	 
									  , StartDate  
									  , EndDate  
									  , Expired  
									  , RetailName  
									  , Logo  
									  , QRType  
									  , AppDownloadLink
									  , QRUrl)
							SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
								 , QRRCP.Pagetitle
								 , QRRCP.PageDescription
								 , QRRCP.ShortDescription
								 , QRRCP.LongDescription
								 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID)
									ELSE QRRCP.Image END  ImageName
								 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT @RetailerConfig + CAST(QRRetailerCustomPageIconImagePath AS VARCHAR(100)) FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID) 
									ELSE @RetailerConfig + CAST(QRRCP.RetailID AS VARCHAR(10)) + '/'+ CAST(Image AS VARCHAR(100)) END	ImagePath
								 , SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
								 , SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
								 , SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
								 , QRRCP.RetailID			 
								 , QRRCP.StartDate
								 , QRRCP.EndDate
								 , Expired = CASE WHEN CAST(GETDATE() AS DATE) > ISNULL(EndDate, GETDATE()+1) THEN 1
											 ELSE 0 END
								 , R.RetailName
								 , @RetailerConfig + CAST(R.RetailID AS VARCHAR(10))+ '/' + R.RetailerImagePath Logo
								 , QRT.QRTypeName QRType
								 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
								 , QRUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL									 
									 THEN CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10)) +'&EL=true'
											   WHEN COUNTS = 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=true'
										  END 
									 ELSE			
									 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
										  CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+ '&EL=false'
											   WHEN Counts = 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=false'
										  END
									 END		               
							FROM QRRetailerCustomPage QRRCP
							INNER JOIN #Temp T ON QRRCP.QRRetailerCustomPageID = T.QRRetailerCustomPageID
							INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID	
							INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
							INNER JOIN Retailer R ON R.RetailID = QRRCP.RetailID 
							LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID
							WHERE QRRCP.RetailID = @RetailID
							AND QRRCP.QRRetailerCustomPageID = @PageID
							AND R.RetailerActive=1	
							
							SELECT @Rowcount=@@ROWCOUNT 		 
					   END
					   
					   --If Custom page is associated with single location.
					   IF @RetailLocationID <> 0
					   BEGIN
						--Get Page Details 
						INSERT INTO #AnythingPages(pageID  
									  , Pagetitle  
									  , PageDescription 
									  , ShortDescription  
									  , LongDescription  
									  , ImageName  
									  , ImagePath  
									  , MediaType  
									  , MediaPath  
									  , ExternalFlag 
									  , RetailID  	 
									  , StartDate  
									  , EndDate  
									  , Expired  
									  , RetailName  
									  , Logo  
									  , QRType  
									  , AppDownloadLink
									  , QRUrl)
							SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
								 , QRRCP.Pagetitle
								 , QRRCP.PageDescription
								 , QRRCP.ShortDescription
								 , QRRCP.LongDescription
								 , QRRCP.Image ImageName
								 , @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + QRRCP.Image	ImagePath
								 , SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
								 , SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
								 , SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
								 , QRRCP.RetailID			 
								 , QRRCP.StartDate
								 , QRRCP.EndDate
								 , Expired = CASE WHEN GETDATE() > EndDate THEN 1
											 ELSE 0 END
								 , R.RetailName
								 , @RetailerConfig + CAST(R.RetailID AS VARCHAR(10))+ '/' + R.RetailerImagePath Logo
								 , QRT.QRTypeName QRType
								 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
								 , QRUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL									 
									 THEN CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10)) +'&EL=true'
											   WHEN COUNTS = 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=true'
										  END 
									 ELSE			
									 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
										  CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+ '&EL=false'
											   WHEN Counts = 1 THEN @RetailerConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=false'
										  END
									 END	
							FROM QRRetailerCustomPage QRRCP
							INNER JOIN #Temp T ON QRRCP.QRRetailerCustomPageID = T.QRRetailerCustomPageID
							INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID	
							INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
							INNER JOIN Retailer R ON R.RetailID = QRRCP.RetailID
							LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID 
							WHERE QRRCP.RetailID = @RetailID
							AND QRRCP.QRRetailerCustomPageID = @PageID
							AND QRRCPA.RetailLocationID = @RetailLocationID
							AND R.RetailerActive=1	
							
							--User Tracking
							SELECT @Rowcount=@@ROWCOUNT
					   END
		   END
		   
		   --send the link if the page is found to be an external one(Link to existing or Upload PDF or image).
		   ELSE IF @ExternalLinkFlag = 1
		   BEGIN
				 INSERT INTO #AnythingPages(pageID)
				 VALUES(@PageID)
				 
				 SELECT @ExternalLink = CASE WHEN B.QRRetailerCustomPageID IS NOT NULL AND B.MediaPath LIKE '%pdf%' OR B.MediaPath LIKE '%png%'
												THEN @RetailerConfig + CAST(A.RetailID AS VARCHAR(10)) + '/' + B.MediaPath
											 ELSE A.URL
										 END
				 FROM QRRetailerCustomPage A
				 LEFT JOIN QRRetailerCustomPageMedia B ON A.QRRetailerCustomPageID = B.QRRetailerCustomPageID
				 WHERE A.QRRetailerCustomPageID = @PageID
				 
				 --User Tracking
				  SELECT @Rowcount=@@ROWCOUNT
		   END
		   
		   
		   --User Tracking
			
		   --Check if the user has scanned the page and capture in the scan related User Tracking tables.
		   IF @ScanTypeID  = 1
		   BEGIN
			   INSERT INTO HubCitiReportingDatabase..scan(MainMenuID
		     											 ,ScanTypeID
														 ,Success
														 ,CreatedDate
														 ,ID)
					SELECT @MainMenuID 
						  ,@ScanType 
						  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
						  ,GETDATE()
						  , pageID
					FROM #AnythingPages	
					
					INSERT INTO HubCitiReportingDatabase..AnythingPageList(AnythingPageID, MainMenuID, AnythingPageClick, CreatedDate)
					SELECT pageID
						 , @MainMenuID
						 , 1
						 , GETDATE()
					FROM #AnythingPages					
		    END 		  
		    --Capture the click if the user has come through retailer summary.  
		    IF @ScanTypeID = 0
		    BEGIN
				UPDATE HubCitiReportingDatabase..AnythingPageList SET AnythingPageClick = 1
				WHERE MainMenuID = @MainMenuID AND AnythingPageID = @PageID
		    END
		   --Display the result set if it is an internal page.
		   IF @ExternalLinkFlag = 0
		   BEGIN
			   SELECT  pageID
					 , Pagetitle
					 , PageDescription
					 , ShortDescription
					 , LongDescription
					 , ImageName
					 , ImagePath
					 , MediaType
					 , MediaPath	
					 , ExternalFlag 
					 , RetailID			 
					 , StartDate
					 , EndDate
					 , Expired 
					 , RetailName
					 , Logo
					 , QRType
					 , AppDownloadLink 
					 , QRUrl
			FROM #AnythingPages
		END
		   
		  --Confirmation of Success.
		  SELECT @Status = 0
		COMMIT TRANSACTION   
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
