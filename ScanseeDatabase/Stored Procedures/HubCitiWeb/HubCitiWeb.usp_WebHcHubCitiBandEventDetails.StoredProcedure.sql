USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiBandEventDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventDetails
Purpose					: To display Event Details.
Example					: usp_WebHcHubCitiEventDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiBandEventDetails]
(   
    --Input variable.	  
	  @HcEventID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @HcEventCategoryID Varchar(2000)
			DECLARE @RetailLocationID Varchar(2000)
			DECLARE @HotelFlag Bit
			DECLARE @Config VARCHAR(500)
			DECLARE @WeeklyDays VARCHAR(1000)

	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT @HcEventCategoryID=COALESCE(@HcEventCategoryID+',', '')+CAST(HcBandEventCategoryID AS VARCHAR)
			FROM HcBandEventsCategoryAssociation 
			WHERE HcBandEventID =@HcEventID 


			
			SELECT @WeeklyDays=COALESCE(@WeeklyDays+',', '')+CAST([DayName] AS VARCHAR)
			FROM HcBandEventInterval			
			WHERE HcBandEventID =@HcEventID
			
			SET @WeeklyDays=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@WeeklyDays, 'Sunday', '1'), 'Monday', '2'), 'Tuesday', '3'), 'Wednesday', '4'), 'Thursday', '5'), 'Friday', '6'), 'Saturday', '7')

			--To display Event Details.
			SELECT DISTINCT HcBandEventName hcEventName 
			               ,ShortDescription shortDescription
                           ,LongDescription longDescription 								
						   ,E.HcHubCitiID 
						   ,eventImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
						   ,ImagePath eventImageName
						   ,eventListingImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+EventListingImagePath 
						   ,eventListingImagePath eventListingImageName
						   ,BussinessEvent bsnsLoc
						   ,ISNULL(EL.GeoErrorFlag,0) geoError
				
						   ,eventStartDate =CAST(StartDate AS DATE)
						   ,eventEndDate =ISNULL(CAST(EndDate AS DATE),NULL)
						   ,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
						   ,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)
						   ,El.Address
						   ,EL.City
						   ,EL.State
						   ,EL.PostalCode
						   ,Latitude
						   ,Longitude logitude						
					
						  ,@HcEventCategoryID  eventCategory		

						 , RL.RetailID retID
						 , RetailName retName
						 , RL.RetailLocationID retailLocationID	
						 , A.Address1 +', '+ a.City + ', '+a.state +', '+a.postalcode retaddress
						 , RL.RetailLocationID rLocId	
						 ,MoreInformationURL moreInfoURL
						 ,OnGoingEvent isOnGoing	 	
						 ,E.HcEventRecurrencePatternID recurrencePatternID
						 ,HR.RecurrencePattern recurrencePatternName
						 ,[isWeekDay] = CASE WHEN HR.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
						 ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
						 ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
						 ,ByDayNumber = CASE WHEN HR.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
						 ,DayNumber
						 --,MonthInterval	
						 ,EventFrequency AS EndAfter	
						 ,BandTicketURL buyTicketURL
					
						 ,EL.EventLocationTitle locationTitle	
						 							
			FROM HcBandEvents E
			LEFT JOIN HcBandEventLocation EL ON E.HcBandEventID =EL.HcBandEventID 
			LEFT JOIN HcEventRecurrencePattern HR ON HR.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
			LEFT JOIN HcBandEventInterval EI ON E.HcBandEventID = EI.HcbandEventID
			LEFT JOIN HcBandRetailerEventsAssociation RL ON RL.HcBandEventID = E.HcBandEventID
			LEFT JOIN Retaillocation A ON RL.RetailLocationID = A.RetailLocationID --AND t.RetailID= A.RetailID 
			LEFT JOIN Retailer R ON R.Retailid = RL.RetailID --AND-- A.RetailID = R.RetailID
			WHERE E.HcBandEventID =@HcEventID
			AND E.Active = 1 

	

				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
