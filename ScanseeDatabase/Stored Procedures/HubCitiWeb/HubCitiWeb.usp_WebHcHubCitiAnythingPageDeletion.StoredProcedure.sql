USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAnythingPageDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcAnythingPageDeletion
Purpose					: To Delete Anything Pages.
Example					: usp_HcAnythingPageDeletion

History
Version		Date		  Author	  Change Description
--------------------------------------------------------------- 
1.0			6/11/2013	   SPAN	           1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAnythingPageDeletion]
(
    --Input variable.	
   	  @AnythingPageID varchar(100) --Comma separated AnythingPageID's
	, @HcHubCitiID int
	, @UserID int

	--Output Variable 
	--, @BottomButtonExist bit output	
	--, @MenuItemExist bit output	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION
						
			DELETE FROM HcAnythingPageMedia
			WHERE HcAnythingPageID IN (SELECT Param FROM dbo.fn_SplitParam(@AnythingPageID, ',')) 			
			
			
			DELETE FROM HcAnythingPage
			WHERE HcAnythingPageID IN (SELECT Param FROM dbo.fn_SplitParam(@AnythingPageID, ','))
			AND HcHubCitiID = @HcHubCitiID
	
					
		--Confirmation of Success.
		SELECT @Status = 0
		
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcAnythingPageDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
