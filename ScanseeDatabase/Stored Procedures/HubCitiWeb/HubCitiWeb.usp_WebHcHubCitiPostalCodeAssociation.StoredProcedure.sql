USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiPostalCodeAssociation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiPostalCodeAssociation
Purpose					: To disply the list of the Hub Cities that are created by the SS admin.
Example					: usp_WebHcHubCitiPostalCodeAssociation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20/1/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiPostalCodeAssociation]
(
   ----Input variable.
	  @HcHubcitiID Int  
	, @State varchar(100)
	, @City varchar(100)
	, @PostalCode varchar(max) --Comma separated zip codes. 
	, @ScanSeeAdminUserID Int
	 
	--Output Variable 
    , @Status int output   
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		
		    --Delete all postalcode association from HcLocationAssociation table.
			INSERT INTO HcLocationAssociation(HcHubCitiID
						, City
						, State
						, PostalCode 
						, DateCreated
						, CreatedUserID)
			SELECT @HcHubcitiID
					, @City 													 
					, @State
					, P.Param 
					, GETDATE()
					, @ScanSeeAdminUserID
			FROM dbo.fn_SplitParam(@PostalCode, ',') P  
			LEFT JOIN HcLocationAssociation L ON HcHubCitiID =@HcHubcitiID AND City =@City AND State=@State AND L.PostalCode =@PostalCode
			WHERE L.HcLocationAssociationID IS NULL	
			
			
			--Update CityId and StateId
			UPDATE HcLocationAssociation
			SET HcCityID =C. HcCityID
			FROM HcLocationAssociation L
			INNER JOIN HcCity C ON L.City = C.CityName
			where L.HcHubCitiID= @HcHubcitiID

			UPDATE HcLocationAssociation
			SET StateID =C.StateID
			FROM HcLocationAssociation L
			INNER JOIN state C ON L.State = C.Stateabbrevation	
			where L.HcHubCitiID= @HcHubcitiID
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiPostalCodeAssociation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
