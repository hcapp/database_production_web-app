USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_RetailerSignup]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerSignup
Purpose					: To disply the PROD Retailer SignUp Link.

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29/01/2016	    Bindu T A	1.0
---------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiWeb].[usp_RetailerSignup]
(
	@ScreenCont VARCHAR(500) OUTPUT,
	@Status int OUTPUT,
	@ErrorNumber int OUTPUT,
	@ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY

		SELECT @ScreenCont=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='RetailerSignup'

		SET @Status= 0

	END TRY
	BEGIN CATCH

		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			PRINT 'Error occured in Stored Procedure [usp_RetailerSignup].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;

	END CATCH
END

GO
