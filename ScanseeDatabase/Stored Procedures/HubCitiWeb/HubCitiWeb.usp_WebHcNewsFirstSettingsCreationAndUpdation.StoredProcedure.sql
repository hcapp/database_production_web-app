USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstSettingsCreationAndUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[HubCitiWeb].[usp_WebHcNewsSettingsCreationAndUpdation]]
Purpose					: To create and update news settings.
Example					: [[HubCitiWeb].[usp_WebHcNewsSettingsCreationAndUpdation]]

History
Version		Date			Author			Change Description
------------------------------------------------------------------------ 
1.0			17/5/2016	   Sagar Byali		1.0
2.0         17/9/2016      Shilpashree      Adding Default Category flag
3.0			28/9/2016	   Sagar Byali	    Adding News ticker 
4.0         20/10/2016	   Sagar Byali		Removed subcatgeory association
4.1			12/15/2016	   Bindu T A		Back Button Changes
4.2			01/Jan/2017	   Sagar Byali		Adding Non-Feed news 
4.3			12/13/2016	   Bindu T A		Side Navigation Changes	
------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstSettingsCreationAndUpdation]
(
   --Input variable.
      @HcAdminUserID int
	, @HubCitiID int
	, @NewsCategoryID int  --All templetes
	, @NewsCategoryColor VARCHAR(1000) --All templetes
	, @DisplayTypeName VARCHAR(1000) 
	, @NewsFeedURL VARCHAR(1000)
	, @NewsSettingsID INT -- Manage Category Updation
	, @NewsSubPageName VARCHAR(1000)
	, @IsDefault Bit
	, @NewsTicker BIT
	, @NewsStories INT
	, @NewsCategoryFontColor VARCHAR(10)
	, @BackButtonColor VARCHAR(10)
	, @IsNewsFeed BIT  --(0 means Only link, 1 means Batch process News)
	, @NewsLinkCategoryName VARCHAR(1000)

	--Output Variable
	, @isCategoryAdded bit output
	, @IsSubCategoryAdded bit output
	, @ClearCacheURL Varchar(1000) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION	
			
			 UPDATE HcMenuItem SET DateCreated = GETDATE()
			 WHERE HcMenuID IN (SELECT HcMenuID FROM HcMenu WHERE HcHubCitiID = @HubCitiID)

			 DECLARE @NewsCategoryCombinationTempleteTypeID INT
			 DECLARE @NewsSubPageID INT
			 DECLARE @NewsCategoryDisplayValue VARCHAR(100)
			 DECLARE @NewsCategory TABLE(SortOrder INT, NewsCategoryID INT)
			 DECLARE @CategoryID INT
			 DECLARE @CategoryName varchar(1000)


			 SELECT @NewsCategoryCombinationTempleteTypeID = NewsCategoryTempleteTypeID
			 FROM NewsFirstCategoryTempleteType
			 WHERE NewsCategoryTempleteTypeDisplayValue =  @DisplayTypeName

			SET @NewsLinkCategoryName = CASE WHEN RTRIM(LTRIM(@NewsLinkCategoryName)) = ' ' THEN NULL ELSE @NewsLinkCategoryName END
			SET @isCategoryAdded = 1

			IF (@NewsSettingsID IS NOT NULL) AND NOT EXISTS (SELECT 1 FROM NewsFirstSettings WHERE HcHubCitiID=@HubCitiID AND RTRIM(LTRIM(NewsLinkCategoryName)) = @NewsLinkCategoryName AND HcNewsSettingsID <> @NewsSettingsID) 
			BEGIN   
			        SET @isCategoryAdded = 0


					--To delete old news if URL is updated
					DECLARE @CatName varchar(500)
					SELECT @CatName = CC.NewsCategoryDisplayValue
					FROM NewsFirstSettings S
					INNER JOIN NewsFirstCategory CC ON CC.NewsCategoryID = S.NewsCategoryID 
					WHERE HcNewsSettingsID = @NewsSettingsID AND CC.Active = 1

					IF NOT EXISTS(SELECT 1 FROM NewsFirstSettings WHERE HcNewsSettingsID = @NewsSettingsID AND NewsFeedURL = @NewsFeedURL)
					BEGIN
							DELETE FROM RssNewsFirstFeedNews WHERE HcHubCitiID = @HubCitiID AND NewsType = @CatName AND subcategory IS NULL
							DELETE FROM RssNewsFirstFeedNewsStagingTable WHERE HcHubCitiID = @HubCitiID AND NewsType = @CatName AND subcategory IS NULL
					END


					UPDATE NewsFirstSettings
					SET  NewsFeedURL = @NewsFeedURL
						,NewsCategoryColor = @NewsCategoryColor
						,NewsFirstCategoryTempleteTypeID = @NewsCategoryCombinationTempleteTypeID
						,NewsFirstSubPageID = @NewsSubPageID
						,DefaultCategory = @IsDefault
						,NewsTicker = @NewsTicker
						,NewsStories = @NewsStories
						,NewsCategoryFontColor = @NewsCategoryFontColor
						,BackButtonColor= @BackButtonColor
						,IsNewsFeed = @IsNewsFeed
						,NewsLinkCategoryName = @NewsLinkCategoryName 
					WHERE HcNewsSettingsID = @NewsSettingsID

					UPDATE HcUserNewsFirstBookmarkedCategories
					SET NewsLinkCategoryName = @NewsLinkCategoryName
					WHERE HcHubCitiID = @HubCitiID AND NewsCategoryDisplayValue = @CategoryName 

					DELETE FROM HcDefaultNewsFirstBookmarkedCategories
					WHERE HcHubCitiID = @HubCitiID

					;WITH CTE AS(
					SELECT DISTINCT  c.NewsCategoryID NewsCategoryID
									,NewsCategoryDisplayValue NewsCategoryDisplayValue									
									,GETDATE() as DateCreated
									,1 CreatedBy
									,1 Active
									,s.NewsCategoryColor
									,s.hcHubCitiID
									,IsNewsFeed
									,s.NewsLinkCategoryName
									,s.SortOrder 								 
					FROM NewsFirstSettings s
					INNER JOIN NewsFirstCategory c on c.NewsCategoryID = s.NewsCategoryID and active =1
					INNER JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = C.NewsCategoryID AND A.HchubcitiID = @HubCitiID
					WHERE S.HcHubCitiID = @HubCitiID and DefaultCategory = 1
		
					UNION ALL
		 
					SELECT DISTINCT  null NewsCategoryID
									,null NewsCategoryDisplayValue						
									,GETDATE() as DateCreated
									,1 CreatedBy
									,1 Active
									,s.NewsCategoryColor
									,s.hcHubCitiID
									,IsNewsFeed
									,NewsLinkCategoryName
									,s.SortOrder 
					FROM NewsFirstSettings s
					WHERE HcHubCitiID = @HubCitiID  and IsNewsFeed = 0 and DefaultCategory = 1
		
					)SELECT    NewsCategoryID
								,NewsCategoryDisplayValue 
								,DateCreated
								,CreatedBy
								,Active
								,NewsCategoryColor
								,hcHubCitiID
								,IsNewsFeed
								,NewsLinkCategoryName
								,NewOrder = IDENTITY(INT,1,1)
						INTO #DefaultCategories
						FROM CTE
						ORDER BY SortOrder

						INSERT INTO HcDefaultNewsFirstBookmarkedCategories(NewsCategoryID
																			,NewsCategoryDisplayValue
																			,DateCreated
																			,CreatedBy
																			,Active
																			,CategoryColor
																			,HcHubCitiID
																			,IsNewsFeed
																			,NewsLinkCategoryName
																			,SortOrder)

						SELECT * 
						FROM #DefaultCategories
				END
				ELSE IF (@NewsSettingsID IS NULL) AND NOT EXISTS(SELECT 1 FROM NewsFirstSettings WHERE RTRIM(LTRIM(NewsLinkCategoryName)) = @NewsLinkCategoryName AND HcHubCitiID = @HubCitiID) AND NOT EXISTS(SELECT 1 FROM NewsFirstSettings WHERE NewsCategoryID = @NewsCategoryID AND HcHubCitiID = @HubCitiID)
				BEGIN
						SET @isCategoryAdded = 0

						INSERT INTO NewsFirstSettings(HcHubCitiID
								,NewsCategoryID
								,DateCreated
								,NewsCategoryColor
								,NewsFeedURL
								,NewsFirstCategoryTempleteTypeID
								,NewsFirstSubPageID
								,DefaultCategory
								,NewsTicker
								,NewsStories
								,SortOrder
								,NewsCategoryFontColor
								,BackButtonColor
								,IsNewsFeed
								,NewsLinkCategoryName)
						OUTPUT  inserted.SortOrder,inserted.NewsCategoryID INTO @NewsCategory
						SELECT   @HubCitiID
								,@NewsCategoryID
								,GETDATE()
								,@NewsCategoryColor
								,@NewsFeedURL
								,@NewsCategoryCombinationTempleteTypeID
								,@NewsSubPageID
								,@IsDefault
								,@NewsTicker
								,@NewsStories
								,ISNULL(MAX(SortOrder) + 1,1)
								,@NewsCategoryFontColor
								,@BackButtonColor
								,@IsNewsFeed
								,@NewsLinkCategoryName
						FROM NewsFirstSettings 
						WHERE HcHubCitiID = @HubCitiID

						--Adding news category to default bookmark based on flag
						IF (@IsDefault = 1)
						BEGIN

						DECLARE @SortOrder INT
						SELECT @SortOrder = MAX(SortOrder)
						FROM HcDefaultNewsFirstBookmarkedCategories
						WHERE HcHubCitiID = @HubCitiID

						INSERT INTO HcDefaultNewsFirstBookmarkedCategories(NewsCategoryID
																,NewsCategoryDisplayValue
																,SortOrder
																,DateCreated
																,CreatedBy
																,Active
																,CategoryColor
																,HcHubCitiID
																,IsNewsFeed
																,NewsLinkCategoryName)
														SELECT   NewsCategoryID
																,@NewsCategoryDisplayValue
																,ISNULL(@SortOrder + 1,1)
																,GETDATE()
																,@HcAdminUserID
																,1
																,@NewsCategoryColor
																,@HubCitiID
																,@IsNewsFeed
																,@NewsLinkCategoryName
														FROM @NewsCategory
											
						END
				END

							


------------------------------------------------MAIN MENU CACHING CHANGES---------------------------------------------------------------------------------------------
--------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------

		IF EXISTS (SELECT 1 FROM HcHubCiti WHERE HcAppListID = 1 AND HcHubCitiID = @HubCitiID)		
		BEGIN

				DECLARE @ClearCacheURL1 varchar(1000),@ClearCacheURL2 varchar(1000),@ClearCacheURL3 varchar(1000)
				SELECT @ClearCacheURL1= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema3'
				SELECT @ClearCacheURL2= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema2'
				SELECT @ClearCacheURL3= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema1'
				SET @ClearCacheURL = @ClearCacheURL1 +','+ @ClearCacheURL2 +','+ @ClearCacheURL3

		END

		ELSE 
		BEGIN

				DECLARE @ClearCacheURL11 varchar(1000),@ClearCacheURL22 varchar(1000),@ClearCacheURL33 varchar(1000)
				SELECT @ClearCacheURL11= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema3'
				SELECT @ClearCacheURL22= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema2'
				SELECT @ClearCacheURL33= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema1'
				SET @ClearCacheURL = @ClearCacheURL11 +','+ @ClearCacheURL22 +','+ @ClearCacheURL33

		END
------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------



	       --Confirmation of Success.
		   SELECT @Status = 0

	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsFirstSettingsCreationAndUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
