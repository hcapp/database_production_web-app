USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventMarkerDetailsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventMarkerDetailsDisplay
Purpose					: To display List of Evnt marker Details.
Example					: usp_WebHcHubCitiEventMarkerDetailsDisplay

History
Version		Date			    Author			Change Description
--------------------------------------------------------------- 
1.0			24thMarch2015	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventMarkerDetailsDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @HcEventsID Int
  
	--Output Variable 
	, @MapTylerImagePath Varchar(2000) Output
	, @EventLogisticLatitude Float Output	
	, @EventLogisticLongitude Float Output	
	, @EventLogisticTopLeftLat Float Output	
	, @EventLogisticTopLeftLong Float Output	
	, @EventLogisticBottomRightLat Float Output	
	, @EventLogisticBottomRightLong Float Output	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY 
	
	        DECLARE @EventConfig Varchar(500)   
			
			--SELECT @EventLogisticLatitude =ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType LIKE 'Addison Event Logistic Map Latitude'

			--SELECT @EventLogisticLongitude =ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType LIKE 'Addison Event Logistic Map Longitude'


			SELECT @EventConfig =ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'

			SELECT @EventLogisticLatitude = Latitude
				  ,@EventLogisticLongitude = Longitude
				  ,@EventLogisticTopLeftLat = TopLeftLatitude
				  ,@EventLogisticTopLeftLong = TopLeftLongitude
				  ,@EventLogisticBottomRightLat = BottomRightLatitude
				  ,@EventLogisticBottomRightLong = BottomRightLongitude
			FROM HcEventsOverLayCoOrdinates
			WHERE HcEventID = @HcEventsID 

			SET @MapTylerImagePath=@EventConfig+CAST(@HubCitiID AS VARCHAR)+ '/events/'+CAST(@HcEventsID AS VARCHAR)+'/'

			--To display List of Evnt marker Details.
			SELECT DISTINCT HcEventsOverLayMarkerDetailID evtMarkerId
			               ,HcEventID hcEventID
						   ,EventMarkerName evtMarkerName
						   ,@EventConfig+CAST(@HubCitiID AS VARCHAR)+ '/events/'+CAST(@HcEventsID AS VARCHAR)+'/'+EventMarkerImagePath eventImageName
						   ,Latitude latitude
						   ,Longitude logitude
			FROM HcEventsOverLayMarkerDetails
			WHERE HcEventID =@HcEventsID 
			ORDER BY EventMarkerName
			
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventMarkerDetailsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
