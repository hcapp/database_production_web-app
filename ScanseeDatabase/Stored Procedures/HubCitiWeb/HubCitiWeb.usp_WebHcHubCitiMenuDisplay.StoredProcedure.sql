USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiMenuDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiMenuDisplay
Purpose					: To disply the list of the Main Menu Button Details associated to the Hubciti.
Example					: usp_WebHcHubCitiMenuDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    Dhananjaya TR	1.0
2.0			11/15/2016		Bindu T A		2.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiMenuDisplay]
(
    --Input variable.
      @HcMenuID Int
    , @HubCitiID VARCHAR(1000)
    , @LevelID Int  
    , @UserID Int
	  
	--Output Variable 
	, @MenuID int output
	, @MenuName varchar(255) output
	, @TemplateName varchar(255) output
	, @isDefault bit output
	, @Status int output
	, @BannerImage Varchar(2000) output
	, @BannerImageName Varchar(255) output
	, @HcDepartmentFlag bit output
	, @HcTypeFlag bit output
	, @Level int output
	, @NoOfColumns int output	
	, @TemplateBackgroundColor Varchar(100) output
	, @TemplateBackgroundImage Varchar(2000) output
	, @TemplateBackgroundImageName Varchar(255) output
	, @DisplayLabel bit output
	, @LabelBckGndColor Varchar(250) output
	, @LabelFontColor Varchar(250) output

	, @isBannerOrTicker INT OutPut  -- News Ticker
	, @HcNewsTickerModeID Int OutPut
	, @HcNewsTickerDirectionID VARCHAR(100) OUTPUT  -- ENd News Ticker
	
    
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 



)
AS
BEGIN

	BEGIN TRY




			
			DECLARE @Config Varchar(500)
			DECLARE @TemplateID	Int		
			DECLARE @HcLinkTypeID Int			

			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To set default Hubcitit sidemenu flag
			SELECT @isDefault = IsDefaultHubCitiSideMenu
			FROM Hcmenu 
			WHERE Hcmenuid = @HcMenuID AND HubCitiSideMenu = 1

			
			SELECT @HcDepartmentFlag = ISNULL(HcDepartmentFlag, 0)
				  ,@HcTypeFlag = ISNULL(HcTypeFlag, 0)
			FROM HcMenu M
			--INNER JOIN #HubcitiIDs H ON H.IntIDs = M.HcHubCitiID
			WHERE M.HcHubCitiID=@HubCitiID AND (HcMenuID = @HcMenuID OR (@HcMenuID = 0 AND Level = 1))
			
			
			SELECT @TemplateID=HcTemplateID
			FROM HcTemplate Where TemplateName ='Two Column Tab with Banner Ad'		
			
			SELECT @TemplateName = T.TemplateName
				 , @MenuID = HCMenuID
				 , @MenuName = MenuName
				 , @Level = @LevelID
				 , @BannerImage=@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HCMenuBannerImage 
				 , @BannerImageName=HCMenuBannerImage
				 , @NoOfColumns = NoOfColumns
				 , @TemplateBackgroundColor = TemplateBackgroundColor
				 , @TemplateBackgroundImageName = TemplateBackgroundImage
				 , @TemplateBackgroundImage = @Config + CAST(HM.HCHubcitiID AS VARCHAR(100)) + '/' + TemplateBackgroundImage
				 , @DisplayLabel = DisplayLabel
				 , @LabelBckGndColor= HM.LabelBackGroundColor
				 , @LabelFontColor = HM.LabelFontColor
			FROM HcMenu HM
			INNER JOIN HcTemplate T ON T.HcTemplateID = HM.HcTemplateID
			WHERE (HM.HcMenuID = @HcMenuID OR (@HcMenuID = 0 AND Level = 1))
			AND HM.HCHubcitiID=@HubCitiID
			
			
			SELECT  Rownum= identity(INT,1,1)
					                , MI.HcMenuItemID
                                    ,FI.BusinessCategoryID 
                             
                            
                            
                                                                       
                                   , HcBusinessSubCategoryID = REPLACE(STUFF((SELECT ', ' + CAST(F.HcBusinessSubCategoryID AS VARCHAR(2000))
                                                                                                     FROM HcMenuFindRetailerBusinessCategories F
                                                                                                     WHERE F.HcMenuItemID = MI.HcMenuItemID AND F.BusinessCategoryID =FI.BusinessCategoryID 
                                                                                                     ORDER BY F.HcMenuFindRetailerBusinessCategoryID
                                                                                                     FOR XML PATH(''))
                                                                                                     , 1, 2, ''), ' ', '')

									
                     
				INTO #Temp1
				FROM HcMenuItem MI              
				INNER JOIN HcMenuFindRetailerBusinessCategories FI ON FI.HcMenuItemID = MI.HcMenuItemID
				INNER JOIN HcMenu M ON M.HcMenuID =MI.HcMenuID 
				WHERE M.HCHubcitiID=@HubCitiID AND M.HcMenuID = @MenuID
				GROUP BY MI.HcMenuItemID,FI.BusinessCategoryID
				ORDER BY min(FI.HcMenuFindRetailerBusinessCategoryID)

				--select * from #Temp1


				---------NEWS----------------
			SELECT  Rownum= identity(INT,1,1)
					, MI.HcMenuItemID
                    , FII.NewsCategoryID 
                             
                            
                      
                                                                       
                    , NewsSubCategoryID = REPLACE(STUFF((SELECT ', ' + CAST(NewsSubCategoryID AS VARCHAR(2000))
                                                                                        FROM [HcMenuItemNewsCategories] FF
                                                                                        WHERE FF.HcMenuItemID = MI.HcMenuItemID AND FF.NewscategoryID =FII.NewscategoryID 
                                                                                        ORDER BY FF.HcMenuItemNewsCategoriesID
                                                                                        FOR XML PATH(''))
                                                                                        , 1, 2, ''), ' ', '')
															
                        
				INTO #Temp111
				FROM HcMenuItem MI              
				INNER JOIN HcMenuItemNewsCategories FII ON FII.HcMenuItemID = MI.HcMenuItemID
				INNER JOIN HcMenu M ON M.HcMenuID =MI.HcMenuID 
				WHERE M.HCHubcitiID=@HubCitiID AND M.HcMenuID = @MenuID
				GROUP BY MI.HcMenuItemID,FII.NewsCategoryID
				ORDER BY min(FII.HcMenuItemNewsCategoriesID)

				--select  *from #Temp111

				--select * from [HcMenuItemNewsCategories]


				-----------------------------------------------------------------


							---------BAND----------------

			SELECT  Rownum= identity(INT,1,1)
					, MI.HcMenuItemID
                    ,FI.BandCategoryID 
                             
                            
                      
                                                                       
                    , HcBandSubCategoryID = REPLACE(STUFF((SELECT ', ' + CAST(BandSubCategoryID AS VARCHAR(2000))
                                                                                        FROM HcMenuItemBandCategories F
                                                                                        WHERE F.HcMenuItemID = MI.HcMenuItemID AND F.bandcategoryID =FI.bandcategoryID 
                                                                                        ORDER BY F.HcMenuItemBandCategoriesID
                                                                                        FOR XML PATH(''))
                                                                                        , 1, 2, ''), ' ', '')
				

                        
				INTO #Temp11
				FROM HcMenuItem MI              
				INNER JOIN HcMenuItemBandCategories FI ON FI.HcMenuItemID = MI.HcMenuItemID
				INNER JOIN HcMenu M ON M.HcMenuID =MI.HcMenuID 
				WHERE M.HCHubcitiID=@HubCitiID AND M.HcMenuID = @MenuID
				GROUP BY MI.HcMenuItemID,FI.BandCategoryID
				ORDER BY min(FI.HcMenuItemBandCategoriesID)


				--select  *from #Temp11


				-----------------------------------------------------------------


			SELECT DISTINCT MI.HcMenuItemID
			      ,MenuItemName	btnName			
				  ,btnAction = CASE WHEN LinkTypeName = 'Dining' THEN (SELECT HcLinkTypeID FROM HcLinkType WHERE LinkTypeName = 'Find') ELSE MI.HcLinkTypeID END 
				  ,linkId = CASE WHEN LinkTypeName NOT IN('Find', 'Events','Fundraisers','Filters','Band','Dining','News')
								 THEN CAST(LinkID AS VARCHAR(100))
								 ELSE CASE WHEN LinkTypeName = 'Events' THEN REPLACE(STUFF((SELECT distinct ', ' + CAST(F.HcEventCategoryID AS VARCHAR(100))
											 FROM HcMenuItemEventCategoryAssociation F
											 WHERE F.HcMenuItemID = MI.HcMenuItemID
											 FOR XML PATH(''))
											, 1, 2, ''), ' ', '') 
								 ELSE CASE WHEN LinkTypeName = 'Fundraisers' THEN REPLACE(STUFF((SELECT distinct ', ' + CAST(F.HcFundraisingCategoryID AS VARCHAR(100))
											 FROM HcMenuItemFundraisingCategoryAssociation F
											 WHERE F.HcMenuItemID = MI.HcMenuItemID
											 FOR XML PATH(''))
											, 1, 2, ''), ' ', '') 
								 ELSE CASE WHEN LinkTypeName = 'Filters' THEN REPLACE(STUFF((SELECT distinct ', ' + CAST(F.HcFilterID AS VARCHAR(100))
											 FROM HcMenuItemFilterAssociation F
											 WHERE F.HcMenuItemID = MI.HcMenuItemID
											 FOR XML PATH(''))
											, 1, 2, ''), ' ', '') 
						

									ELSE CASE WHEN LinkTypeName = 'Find' OR LinkTypeName = 'Dining' THEN REPLACE(STUFF((SELECT  ', ' + CAST(F.BusinessCategoryID AS VARCHAR(100))
											FROM #Temp1  F
											WHERE F.HcMenuItemID = MI.HcMenuItemID                                                                          
											FOR XML PATH(''))
											, 1, 2, ''), ' ', '')

										
									ELSE CASE WHEN LinkTypeName = 'Band' THEN REPLACE(STUFF((SELECT  ', ' + CAST(F.BandCategoryID AS VARCHAR(100))
											FROM #Temp11  F
											WHERE F.HcMenuItemID = MI.HcMenuItemID                                                                          
											FOR XML PATH(''))
											, 1, 2, ''), ' ', '')

									ELSE CASE WHEN LinkTypeName = 'News' THEN REPLACE(STUFF((SELECT  ', ' + CAST(FFF.NewsCategoryID AS VARCHAR(100))
											FROM #Temp111  FFF
											WHERE FFF.HcMenuItemID = MI.HcMenuItemID                                                                          
											FOR XML PATH(''))
											, 1, 2, ''), ' ', '')


								 
                                 END
								 END
								 END
								 END
								 END
								 END
							END	
				  , Position	btnPosition 
				  , btnImage=CASE WHEN HM.Hctemplateid=@TemplateID THEN HCMenuBannerImage  ELSE (@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath)	END
				  , btniPadImage=CASE WHEN HM.Hctemplateid=@TemplateID THEN HCMenuBannerImage  ELSE (@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemIpadImagePath)	END  --New IpadImagePath implementation
				  , imageName = HcMenuItemImagePath	
				  , ipadImageName = MI.HcMenuItemIpadImagePath --New IpadImagePath implementation
				  , functnName=CASE WHEN L.LinkTypeName ='Text' THEN 'Text' WHEN L.LinkTypeName ='Label' THEN 'Label' END	
				  , MI.HcDepartmentID
				  , MI.HcMenuItemTypeID
				  , D.HcDepartmentName btnDept
				  , MT.HcMenuItemTypeName btnType
				  , S.HcMenuItemShapeID comboBtnTypeId
				  , S.HcMenuItemShape comboBtnType					 		  
				 
				 , findSubCatIds = REPLACE(REPLACE(REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.HcBusinessSubCategoryID IS NULL,'asd',F.HcBusinessSubCategoryID) AS NVARCHAR(2000))
                                                                           FROM #Temp1 F
                                                                           WHERE F.HcMenuItemID = MI.HcMenuItemID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', ''),'asd,',''),',asd',''),'asd','')

				 
				 
				 
				  , svdSubCate = REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.HcBusinessSubCategoryID IS NULL,'asd',F.HcBusinessSubCategoryID) AS NVARCHAR(2000))
                                                                           FROM #Temp1 F
                                                                           WHERE F.HcMenuItemID = MI.HcMenuItemID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', '|'),'asd','null')),',|','!~~!')),',','|')


			---------------------------------BAND---------------------------													   				 
				 , bandSubCatIds = REPLACE(REPLACE(REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.HcBandSubCategoryID IS NULL,'asd',F.HcBandSubCategoryID) AS NVARCHAR(2000))
                                                                           FROM #Temp11 F
                                                                           WHERE F.HcMenuItemID = MI.HcMenuItemID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', ''),'asd,',''),',asd',''),'asd','')

				 
				 
				 
				  , bndSubCate = REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.HcBandSubCategoryID IS NULL,'asd',F.HcBandSubCategoryID) AS NVARCHAR(2000))
                                                                           FROM #Temp11 F
                                                                           WHERE F.HcMenuItemID = MI.HcMenuItemID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', '|'),'asd','null')),',|','!~~!')),',','|')


				-------------------------------------------------------------------------------------------------------------------------------------


							---------------------------------News---------------------------													   				 
				 , newsSubCatIds = REPLACE(REPLACE(REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(FFF.NewsSubCategoryID IS NULL,'asd',FFF.NewsSubCategoryID) AS NVARCHAR(2000))
                                                                           FROM #Temp111 FFF
                                                                           WHERE FFF.HcMenuItemID = MI.HcMenuItemID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', ''),'asd,',''),',asd',''),'asd','')

				 
				 
				 
				  , nwsSubCate = REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(FFF.NewsSubCategoryID IS NULL,'asd',FFF.NewsSubCategoryID) AS NVARCHAR(2000))
                                                                           FROM #Temp111 FFF
                                                                           WHERE FFF.HcMenuItemID = MI.HcMenuItemID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', '|'),'asd','null')),',|','!~~!')),',','|')


				-------------------------------------------------------------------------------------------------------------------------------------
				 
				  , L.LinkTypeDisplayName funType    
				  , CitiID=IIF(MI.HcMenuItemID =RPM.HcMenuItemID,REPLACE(STUFF((SELECT  ', ' + CAST(R.HcCityID AS VARCHAR(100))
                                                                           FROM HcRegionAppMenuItemCityAssociation R
                                                                           WHERE R.HcMenuItemID = MI.HcMenuItemID                                                                          
                                                                           FOR XML PATH(''))
                                                                      , 1, 2, ''), ' ', ''),null)  

				 , isNewsTemp = CASE WHEN L.LinkTypeDisplayName = 'News' THEN 1 ELSE 0 END
				 , MI.linkID SubMenuLinkID
				
		    INTO #Display                                                                    
			FROM HcMenuItem MI			 
			INNER JOIN HcMenu HM ON MI.HCMenuID=HM.HCMenuID
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = MI.HcLinkTypeID
			LEFT JOIN HcDepartments D ON D.HcDepartmentID = MI.HcDepartmentID
			LEFT JOIN HcMenuItemType MT ON MT.HcMenuItemTypeID = MI.HcMenuItemTypeID
			LEFT JOIN HcMenuFindRetailerBusinessCategories F ON F.HcMenuItemID = MI.HcMenuItemID
			LEFT JOIN HcMenuItemBandCategories FF ON FF.HcMenuItemID = MI.HcMenuItemID
			LEFT JOIN [HcMenuItemNewsCategories] FFF ON FFF.HcMenuItemID = MI.HcMenuItemID
			LEFT JOIN HcRegionAppMenuItemCityAssociation RPM ON RPM.HcMenuItemID =MI.HcMenuItemID 
			LEFT JOIN HcMenuItemType T ON T.HcMenuItemTypeID = MI.HcMenuItemTypeID
			LEFT JOIN HcMenuItemShape S ON S.HcMenuItemShapeID =MI.HcMenuItemShapeID 
			WHERE HM.HCHubcitiID=@HubCitiID AND (MI.HcMenuID = @HcMenuID OR (@HcMenuID = 0 AND Level = 1))  
			ORDER BY Position	



			UPDATE #Display
			SET isNewsTemp = 1
			FROM #Display D
			INNER JOIN HcMenu M ON M.HcMenuID = D.SubMenuLinkID
			INNER JOIN HcTemplate T ON T.HcTemplateID = M.HcTemplateID
			WHERE T.IsNews = 1


			select  DISTINCT HcMenuItemID
			      ,btnName			
				  ,btnAction 
				  ,linkId
				  , btnPosition 
				  , btnImage
				  , btniPadImage 
				  , imageName 
				  , ipadImageName  
				  , functnName	
				  , HcDepartmentID
				  , HcMenuItemTypeID
				  , btnDept
				  , btnType
				  , comboBtnTypeId
				  , comboBtnType					 		  
				  , findSubCatIds 
				  , svdSubCate 

			---------------------------------BAND---------------------------													   				 
				 , bandSubCatIds 
				 
				 
				  , bndSubCate 
				-------------------------------------------------------------------------------------------------------------------------------------


							---------------------------------News---------------------------													   				 
				 , newsSubCatIds
				 
				 
				  , nwsSubCate 
				-------------------------------------------------------------------------------------------------------------------------------------
				 
				  , funType    
				  , CitiID
				  , isNewsTemp
				 
				
			from #Display


			-- News Ticker --

			SELECT @isBannerOrTicker = CASE WHEN IsNewsTicker = 0 THEN 1
											WHEN IsNewsTicker = 1 THEN 0
											WHEN IsNewsTicker IS NULL THEN NULL END,
				   @hcNewsTickerModeID = HcNewsTickerModeID,
				   @hcNewsTickerDirectionID = CASE WHEN HcNewsTickerDirectionID = 1 THEN 'Left'
													WHEN HcNewsTickerDirectionID = 2 THEN 'Right'
													WHEN HcNewsTickerDirectionID = 3 THEN 'Up'
													WHEN HcNewsTickerDirectionID = 4 THEN 'Down' END
			FROM HcMenu
			WHERE HcHubCitiID=@HubCitiID AND (HcMenuID = @HcMenuID OR (@HcMenuID = 0 AND Level = 1))
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiMenuDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
