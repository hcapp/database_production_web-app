USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_GeneralImagesGetScreenContent]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GeneralImagesGetScreenContent]
Purpose					: To get Image of Product Summary page.
Example					: [usp_GeneralImagesGetScreenContent]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			08 June 2012	Span	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_GeneralImagesGetScreenContent]
(
	@ConfigurationType varchar(50)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			--Get the Configuration of the server for general images.
			 SELECT ScreenName
				  , ScreenContent
			 FROM AppConfiguration
			 WHERE ConfigurationType = @ConfigurationType
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GeneralImagesGetScreenContent].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;







GO
