USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiDepartmentAndTypeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiDepartmentAndTypeDisplay
Purpose					: To display list of departments and types associated to given HubCiti.
Example					: usp_WebHcHubCitiDepartmentAndTypeDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiDepartmentAndTypeDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	  
	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT HcDepartmentName deptName
			FROM HcDepartments
			WHERE HcHubCitiID = @HubCitiID
			
			SELECT HcMenuItemTypeName typeName
			FROM HcMenuItemType
			WHERE HcHubCitiID = @HubCitiID
								
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiDepartmentAndTypeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
