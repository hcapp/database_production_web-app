USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingCategoryCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingCategoryCreation
Purpose					: Creating Fundraising Categories.
Example					: usp_WebHcHubCitiFundraisingCategoryCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Aug 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingCategoryCreation]
(
   
    --Input variable. 	  
	  @ScanSeeAdminUserID Int
	, @HcFundraisingCategoryName Varchar(1000)
	, @CategoryImagePath Varchar(1000)
	  
	--Output Variable 	
	, @DuplicateFlag Bit Output
	, @FundraisingCategoryID INT OUTPUT
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
								
				IF EXISTS(SELECT 1 FROM HcFundraisingCategory WHERE FundraisingCategoryName =@HcFundraisingCategoryName)
				BEGIN
				
					SET @DuplicateFlag=1
										
				END
				
				ELSE

				BEGIN
					
					INSERT INTO HcFundraisingCategory(FundraisingCategoryName 
												,CategoryImagePath
												,DateCreated 												
												,CreatedUserID)	
					SELECT @HcFundraisingCategoryName 
						 , @CategoryImagePath 					   
						 , GETDATE()
						 , @ScanSeeAdminUserID							 
						 
					SET @DuplicateFlag=0	
					
					--SELECT HcFundraisingCategoryID = SCOPE_IDENTITY()
					
				END
				
				
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingCategoryCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
