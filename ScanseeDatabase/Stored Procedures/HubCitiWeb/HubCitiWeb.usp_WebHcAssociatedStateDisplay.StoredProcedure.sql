USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAssociatedStateDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAssociatedStateDisplay]
Purpose					: To disply list of associated states for given HubCiti.
Example					: [usp_WebHcAssociatedStateDisplay]

History
Version		Date			Author		 Change Description
--------------------------------------------------------------- 
1.0			1/21/2014	    SPAN		       1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAssociatedStateDisplay]
(
    
    --Input variable
      @HubCitiID int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT DISTINCT S.StateName stateName
			              , HL.State
			FROM HcLocationAssociation HL
			INNER JOIN [State] S ON HL.[State] = S.Stateabbrevation
			WHERE HcHubCitiID = @HubCitiID
			ORDER BY StateName
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAssociatedStateDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
