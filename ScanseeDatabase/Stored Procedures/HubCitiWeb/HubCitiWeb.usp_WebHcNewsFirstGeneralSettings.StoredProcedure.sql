USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstGeneralSettings]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[[usp_WebHcNewsFirstGeneralSettings]]]
Purpose					: To display General News Settings.
Example					: [[[usp_WebHcNewsFirstGeneralSettings]]]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[[[usp_WebHcNewsFirstGeneralSettings]]]
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstGeneralSettings]
(   
	--Input variable
	   @HcHubCitiID INT
	 , @HcMenuBannerImage VARCHAR(1000)
	 , @HcNewsImage VARCHAR(1000)
	 , @NewsThumbNailPosition VARCHAR(1000)
	 , @WeatherURL VARCHAR(1000)

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY


			DECLARE @NewsBannerImage varchar(1000)
				   ,@NewsImagePath varchar(1000)	

			SELECT @NewsBannerImage = ScreenContent + CONVERT(VARCHAR,@HcHubCitiID)+ '/'
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		

			IF @NewsThumbNailPosition IS  NULL AND @HcNewsImage IS  NULL AND @HcMenuBannerImage IS  NULL AND @WeatherURL IS NULL
			BEGIN
			--To display list of News defaults
			SELECT DISTINCT @NewsBannerImage + NewsBannerImage bannerImgPath
						  , @NewsBannerImage + NewsCategoryImage newsImgPath
						  , NewsBannerImage bannerImg
						  , NewsCategoryImage newsImage
						  , WeatherURL weatherURL
						  , NewsThumbNailPosition ThumnailPos
			FROM HcHubCiti 
			WHERE HcHubCitiID = @HcHubCitiID
			END
				
			ELSE IF @NewsThumbNailPosition IS NOT NULL AND @HcNewsImage IS NOT NULL AND @HcMenuBannerImage IS NOT NULL AND @WeatherURL IS NOT NULL
			BEGIN
			UPDATE HcHubCiti
			SET NewsCategoryImage = @HcNewsImage
			   ,NewsThumbnailPosition = @NewsThumbNailPosition
			   ,NewsBannerImage = @HcMenuBannerImage
			   ,WeatherURL = @WeatherURL
            WHERE HcHubCitiID = @HcHubCitiID 
			END		
						
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
