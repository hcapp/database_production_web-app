USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HcUserDealPushNotify]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_4_3].[usp_HcUserDealPushNotify]
Purpose					: To send Deal push notifications to the respective users registered for a Hubciti.
Example					: [HubCitiApp2_4_3].[usp_HcUserDealPushNotify]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			02 Feb 2016		Sagar Byali		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_HcUserDealPushNotify]
(
	  @HcHubCitiID INT

	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			
			
		BEGIN TRANSACTION	

		--QT.QRTypeName = 'AnyThing Page' AND

			--To fetch deals information.
			SELECT  TOP 1 S.DealOfTheDayID dealId
						  , S.DealName dealName
						  , D.DealDescription type
						  , S.SpecialsQRURL splUrl
						  , H.HubCitiName hcName
						  ,(CASE WHEN ( S.SpecialsQRURL IS NULL AND QA.URL IS NULL AND D.DealDescription = 'SpecialOffers') THEN QR.RetailID END) AS retailerId   
						  ,(CASE WHEN ( S.SpecialsQRURL IS NULL AND QA.URL IS NULL AND D.DealDescription = 'SpecialOffers') THEN QR.RetailLocationID END)  retailLocationId
						  , CONVERT(DATE,DealExpirationDate) endDate
						  , CONVERT(VARCHAR(5), DealExpirationDate, 108) endTime
			FROM HcDeals D 
			INNER JOIN HcDealOfTheDayStaging S ON D.HcDealsID = S.HcDealsID
			INNER JOIN HcHubCiti H ON S.HcHubCitiID = H.HcHubCitiID
			LEFT JOIN QRRetailerCustomPageAssociation QR ON QR.QRRetailerCustomPageID = S.DealOfTheDayID
			LEFT JOIN QRRetailerCustomPage QA ON QR.QRRetailerCustomPageID=QA.QRRetailerCustomPageID
			LEFT JOIN QRTypes QT ON QT.QRTypeID=QA.QRTypeID
			LEFT JOIN RetailLocation RL ON RL.Retailid = QR.RetailID AND RL.RetailLocationID = QR.RetailLocationID
			WHERE H.HcHubCitiID =@HcHubCitiID AND CAST(Getdate() as date) BETWEEN CAST(S.DealScheduleStartDate as date) AND CAST(S.DealScheduleEndDate as date)
			AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL
			ORDER BY s.DateCreated desc


			 INSERT INTO HcDealOfTheDay(HcHubCitiID
										,DealOfTheDayID
										,DealName
										,DealDescription
										,Price
										,SalePrice
										,DealStartDate
										,DealEndDate
										,HcDealsID
										,DateCreated
										,DateModified
										,CreatedUserID
										,ModifiedUserID)

								SELECT	 HcHubCitiID
										,DealOfTheDayID
										,DealName
										,DealDescription
										,Price
										,SalePrice
										,DealStartDate
										,DealEndDate
										,HcDealsID
										,DateCreated
										,DateModified
										,CreatedUserID
										,ModifiedUserID
							FROM HcDealOfTheDayStaging 
							WHERE HcHubCitiID = @HcHubCitiID
							AND cast(DealScheduleEndDate as date) < cast(GETDATE() as date)
							

			--To send pushnotifications to respective devices.
			--To notify Android and iphone devices


							UPDATE HcUserToken
							SET PushNotifyBadgeCount =PushNotifyBadgeCount + 1 
							WHERE HcHubCitiID = @HcHubCitiID
			
						SELECT DISTINCT UserToken deviceId
							  ,'Android' platform					  		
							  ,HubCitiName hcName
							  ,PushNotifyBadgeCount badgeCount
						FROM HcUserToken U
						INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
						--INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID
						WHERE PushNotify = 1 AND H.HcHubCitiID = @HcHubCitiID AND U.HcRequestPlatformID=2
						--AND (U.DeviceID = 'a55e948e0c71dac5') -- OR DeviceID = '29a3832895c3c01')
			 
						UNION ALL

						SELECT DISTINCT UserToken deviceId
							  ,'IOS' platform					  		
							  ,HubCitiName hcName
							--,D.AppVersion
							 ,PushNotifyBadgeCount badgeCount
						FROM HcUserToken U
						INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
						--INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID
						WHERE PushNotify = 1 AND H.HcHubCitiID = @HcHubCitiID AND U.HcRequestPlatformID=1 --AND D.AppVersion = '2.3.2'
						--AND ((U.DeviceID = '750F56A4-F843-4893-AD04-762C2095D91A'))-- OR (U.DeviceID= '8d1f7917bc2e9e91f0036554b2eebb30ab99999d')) 


	--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserPushNotify.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;















GO
