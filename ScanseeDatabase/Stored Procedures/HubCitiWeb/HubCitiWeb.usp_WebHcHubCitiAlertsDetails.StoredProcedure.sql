USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsDetails
Purpose					: To display Alert Details.
Example					: usp_WebHcHubCitiAlertsDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsDetails]
(   
    --Input variable.	  
	  @HcAlertID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @HcAlertCategoryID Varchar(2000)
			
			SELECT @HcAlertCategoryID=COALESCE(@HcAlertCategoryID+',', '')+CAST(HcAlertCategoryID AS VARCHAR)
			FROM HcAlertCategoryAssociation
			WHERE HcAlertID=@HcAlertID 
			
			--To display Alert Details
			SELECT DISTINCT HcAlertID alertId			               
			               ,HcAlertName title
			               ,ShortDescription Description						
						   ,HcHubCitiID 
						   ,HcSeverityID SeverityID
						   ,StartDate =CAST(StartDate AS DATE)
						   ,EndDate =CAST(EndDate AS DATE)
						   ,StartTime =CAST(StartDate AS Time)
						   ,EndTime =CAST(EndDate AS Time)
						   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
											  FROM HcMenuItem MI 
											  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
											  WHERE LinkTypeName = 'Alerts' 
											  AND MI.LinkID = HcAlertID)>0 THEN 1 ELSE 0 END
						  ,@HcAlertCategoryID CategoryID					  											
			FROM HcAlerts 
			WHERE HcAlertID=@HcAlertID
				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
