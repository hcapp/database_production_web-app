USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDataExportHubCitiListUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDataExportHubCitiListUpdation]
Purpose					: To display list of Hubcities selected by Admin for Data Export.
Example					: [usp_WebHcDataExportHubCitiListUpdation]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			13 Oct 2015	    Span		1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDataExportHubCitiListUpdation]
(   
    --Input variable
	  @CheckedHcHubCitiID VARCHAR(500) --Selected Hubcities passed as input parameter
	 ,@UncheckedHcHubCitiID VARCHAR(500)  

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			--To Set Data export flag to 1 when Admin selects the hubcities
			UPDATE HcDataExportHubCitiList
			SET HubCitiDataExportFlag = 0 
			WHERE HcHubCitiID IN (SELECT Param FROM fn_SplitParam(@UncheckedHcHubCitiID,',')) 

			UPDATE HcDataExportHubCitiList
			SET HubCitiDataExportFlag = 1 
			WHERE HcHubCitiID IN (SELECT Param FROM fn_SplitParam(@CheckedHcHubCitiID,','))

			--Confirmation of Success
			SELECT @Status = 0	 
    
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDataExportHubCitiListUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
