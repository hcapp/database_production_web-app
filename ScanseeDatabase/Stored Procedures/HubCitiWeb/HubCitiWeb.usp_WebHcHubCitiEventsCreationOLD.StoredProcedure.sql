USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventsCreationOLD]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventsCreation
Purpose					: Creating New Events.
Example					: usp_WebHcHubCitiEventsCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4th Feb 2014    Pavan Sharma K	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventsCreationOLD]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int
	, @HcEventName Varchar(1000)
	, @ShortDescription Varchar(2000)
    , @LongDescription Varchar(2000) 
	, @HcEventCategoryID Varchar(4000) 
	, @ImagePath Varchar(2000) 
	, @BussinessEvent Bit 
	, @PackageEvent Bit
	, @HotelEvent Bit
	, @StartDate DATE
	, @StartTime TIME
	, @PackageDescription Varchar(2000)
	, @PackageTicketURL Varchar(2000) 
	, @PackagePrice Money
	, @Address Varchar(2000)
    , @City varchar(200)
    , @State Varchar(100)
    , @PostalCode VARCHAR(10)
    , @Latitude Float
    , @Longitude Float
    , @GeoErrorFlag BIT
	, @HcAppsiteID Varchar(max)
	, @RetailLocationID Varchar(max)
	, @HotelPrice VARCHAR(MAX)
	, @DiscountCode VARCHAR(MAX)
	, @DiscountAmount VARCHAR(MAX)
	, @Rating VARCHAR(MAX)
	, @RoomAvailabilityCheckURL VARCHAR(MAX)
	, @RoomBookingURL VARCHAR(MAX)	
	, @MoreInformationURL Varchar(2000)
	, @OngoingEvent bit 
	--, @Duration float
	, @EndTime time
	, @EndDate date
	, @RecurrencePatternID int
	, @RecurrenceInterval int
	, @EveryWeekday bit
	, @Days varchar(1000)
	, @EndAfter int
	, @DayNumber int
	

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
								
			DECLARE @HcEventID INT	
			DECLARE @RecurrencePattern varchar(10)
			SELECT @EveryWeekday = ISNULL(@EveryWeekday, 0)


			SELECT @RetailLocationID = REPLACE(@RetailLocationID, 'NULL', '0')
			SELECT @HotelPrice = REPLACE(@HotelPrice, 'NULL', '0')
			SELECT @DiscountCode = REPLACE(@DiscountCode, 'NULL', '0')
			SELECT @DiscountAmount = REPLACE(@DiscountAmount, 'NULL', '0')
			SELECT @Rating = REPLACE(@Rating, 'NULL', '0')
			SELECT @RoomAvailabilityCheckURL = REPLACE(@RoomAvailabilityCheckURL, 'NULL', '0')
		    SELECT @RoomBookingURL = REPLACE(@RoomBookingURL, 'NULL', '0')

			SELECT @Days = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Days, '1', 'Sunday'), '2', 'Monday'), '3', 'Tuesday'), '4', 'Wednesday'), '5', 'Thursday'), '6', 'Friday'), '7', 'Saturday')

			SELECT @RecurrencePattern = RecurrencePattern FROM HcEventRecurrencePattern WHERE HcEventRecurrencePatternID = @RecurrencePatternID

			IF @EveryWeekday = 1
			BEGIN
				SELECT @Days = 'Monday,Tuesday,Wednesday,Thursday,Friday'				
			END
		    
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , RetailLocationID = Param
			INTO #RetailLocationID
			FROM dbo.fn_SplitParam(@RetailLocationID, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HotelPrice = Param
			INTO #HotelPrice
			FROM dbo.fn_SplitParam(@HotelPrice, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , DiscountCode = Param
			INTO #DiscountCode
			FROM dbo.fn_SplitParam(@DiscountCode, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , DiscountAmount = Param
			INTO #DiscountAmount
			FROM dbo.fn_SplitParam(@DiscountAmount, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , Rating = Param
			INTO #Rating
			FROM dbo.fn_SplitParam(@Rating, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , RoomAvailabilityCheckURL = Param
			INTO #RoomAvailabilityCheckURL
			FROM dbo.fn_SplitParam(@RoomAvailabilityCheckURL, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , RoomBookingURL = Param
			INTO #RoomBookingURL
			FROM dbo.fn_SplitParam(@RoomBookingURL, ',')


			--Creating New Event		
			INSERT INTO HcEvents(HcEventName  
			                    ,ShortDescription
                                ,LongDescription 								
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,PackageEvent								
								,StartDate 
								,EndDate 
								,DateCreated  
								,CreatedUserID
								,HotelEvent
								,MoreInformationURL
								,OnGoingEvent
								,HcEventRecurrencePatternID
								,RecurrenceInterval
								,EventFrequency
								)	
			SELECT @HcEventName 
			     , @ShortDescription 
			     , @LongDescription 			      
				 , @HcHubCitiID 					
			     , @ImagePath 
				 , @BussinessEvent 
				 , @PackageEvent 
				 , CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
				 , IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @StartDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, '23:59:00.000') AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
				 , GETDATE()
				 , @UserID 
				 , @HotelEvent 	
				 , @MoreInformationURL	
				 , @OngoingEvent
				 , @RecurrencePatternID
				 , CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
							THEN @RecurrenceInterval 
						ELSE NULL 
				   END
				 , @EndAfter
					
				
			SET @HcEventID =SCOPE_IDENTITY()

			---Insert Event Details to Event Location Table
			INSERT INTO HcEventLocation(HcEventID
									   ,HcHubCitiID
									   ,Address
									   ,City
									   ,State
									   ,PostalCode
									   ,Latitude
									   ,Longuitude
									   ,GeoErrorFlag
									   ,DateCreated
									   ,CreatedUserID)
			SELECT @HcEventID 
			      ,@HcHubCitiID 
				  ,@Address 
				  ,@City
				  ,@State 
				  ,@PostalCode 
				  ,@Latitude
				  ,@Longitude 
				  ,@GeoErrorFlag
				  ,GETDATE()
				  ,@UserID 
			
			INSERT INTO HcEventsCategoryAssociation(HcEventCategoryID 
												   ,HcEventID 
												   ,HcHubCitiID
												   ,DateCreated
												   ,CreatedUserID)	
			SELECT [Param]
			      ,@HcEventID 
			      ,@HcHubCitiID
			      ,GETDATE()
			      ,@UserID
			FROM dbo.fn_SplitParam(@HcEventCategoryID, ',') 	
			
			IF @PackageEvent = 1
			BEGIN
				UPDATE HcEvents SET	PackageDescription = @PackageDescription
								  , PackageTicketURL = @PackageTicketURL 
								  ,	PackagePrice= @PackagePrice
				WHERE HcEventID = @HcEventID
			END				
			
			IF @BussinessEvent=1
			BEGIN
			   INSERT INTO HcEventAppsite(HcEventID
										 ,HcHubCitiID
										 ,HcAppsiteID
										 ,DateCreated										
										 ,CreatedUserID)
				SELECT @HcEventID 
				      ,@HcHubCitiID 
					  ,[param] 
					  ,GETDATE()
					  ,@UserID 
				FROM fn_SplitParam(@HcAppsiteID,',')
			END

			IF @HotelEvent =1
			BEGIN
				INSERT INTO HcEventPackage(HcEventID
										  ,HcHubCitiID
										  ,RetailLocationID
										  ,DateCreated										
										  ,CreatedUserID
										  ,HotelPrice 
										  ,DiscountCode 
										  ,DiscountAmount 
										  ,Rating 
										  ,RoomAvailabilityCheckURL
										  ,RoomBookingURL)
				SELECT @HcEventID 
				      ,@HcHubCitiID 
					  ,CASE WHEN A.RetailLocationID = '0' THEN NULL ELSE A.RetailLocationID END 
					  ,GETDATE()
					  ,@UserID
					  ,CASE WHEN B.HotelPrice = '0' THEN NULL ELSE B.HotelPrice END 
					  ,CASE WHEN C.DiscountCode = '0' THEN NULL ELSE C.DiscountCode END  
				      ,CASE WHEN D.DiscountAmount = '0' THEN NULL ELSE D.DiscountAmount END  
					  ,CASE WHEN E.Rating = '0' THEN NULL ELSE E.Rating END  
					  ,CASE WHEN F.RoomAvailabilityCheckURL = '0' THEN NULL ELSE F.RoomAvailabilityCheckURL END  
					  ,CASE WHEN G.RoomBookingURL = '0' THEN NULL ELSE G.RoomBookingURL END  
				FROM #RetailLocationID A
				INNER JOIN #HotelPrice B ON A.RowNum =B.RowNum
				INNER JOIN #DiscountCode C ON C.RowNum =B.RowNum
				INNER JOIN #DiscountAmount D ON C.RowNum =D.RowNum 
				INNER JOIN #Rating E ON E.RowNum =D.RowNum
				INNER JOIN #RoomAvailabilityCheckURL F ON F.RowNum =E.RowNum
				INNER JOIN #RoomBookingURL G ON G.RowNum =F.RowNum
				
			END		

			--For On Going Events insert the ongoing pattern into the association tables.
			IF @OngoingEvent = 1
			BEGIN
				--If the event is created as Daily with a inteval value then do not insert record into HcEventInterval table.
				--IF ((@RecurrencePattern <> 'Daily' AND @EveryWeekday <> 0) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				IF ((@RecurrencePattern = 'Weekly' OR @RecurrencePattern = 'Monthly' ) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				BEGIN
					INSERT INTO HcEventInterval(HcEventID
											  , DayNumber
											  , MonthInterval
											  , DayName
											  , DateCreated
											  , CreatedUserID)
									SELECT @HcEventID
										 , @DayNumber
										 , IIF(@RecurrencePattern ='Monthly', @RecurrenceInterval, NULL)
										 , Param
										 , GETDATE()
										 , @UserID
									FROM [HubCitiWeb].fn_SplitParam(@Days, ',') A
				END
			END
			
			
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventsCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
