USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminRetailLocationDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminRetailLocationDisplay
Purpose					: To disply the list of the RetailLocations.
Example					: usp_WebHcAdminRetailLocationDisplay

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminRetailLocationDisplay]
(
    --Input variable
	  @HubCitiID int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--List of associated RetailLocations.			
			SELECT R.RetailID
					,R.RetailName
					,RL.RetailLocationID
					,RL.Address1
					,RL.City
					,RL.[State]
					,RL.PostalCode
			FROM HcLocationAssociation LA
			INNER JOIN RetailLocation RL ON LA.PostalCode = RL.PostalCode
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
			INNER JOIN Retailer R ON RL.RetailID = R.RetailID
			WHERE LA.HcHubCitiID = @HubCitiID AND (RL.Active=1 AND R.RetailerActive=1)
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminRetailLocationDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
