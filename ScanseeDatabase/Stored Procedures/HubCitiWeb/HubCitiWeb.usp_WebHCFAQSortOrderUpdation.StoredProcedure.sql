USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHCFAQSortOrderUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebHCFAQSortOrderUpdation]
Purpose                  : To Sort FAQ CAtegory based on sort order.
Example                  : [usp_WebHCFAQSortOrderUpdation]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               26thJune2014        Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHCFAQSortOrderUpdation]
(

      --Input Input Parameter(s)--  
      
        @HcHubcitiID INT
      , @FAQCategoryId Varchar(MAX) --Comma separated Anythingpageids
      , @FAQId Varchar(2000)
	  , @SortOrder VARCHAR (255)
      
      --Output Variable--
	  , @Status INT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN
  
      BEGIN TRY
      
      BEGIN TRANSACTION
      
             
				   
					  --DECLARE @HcHubcitiID INT='47'
				--      DECLARE @FAQCategoryId VARCHAR(2000)='1!~~!2!~~!3'
				--      DECLARE @FAQId Varchar(MAX) ='1|2|3!~~!1|2|3!~~!1|2|3'
				--      DECLARE @SortOrder VARCHAR (2000)='1|2|3!~~!1|2|3!~~!1|2|3'

                    SELECT @FAQCategoryId = REPLACE(@FAQCategoryId, 'NULL', '0')
                    SELECT @FAQId = REPLACE(@FAQId, 'NULL', '0')                  
                    SELECT @SortOrder = REPLACE(@SortOrder, 'NULL', '0')

					CREATE TABLE #FAQCA(Rownum int identity(1,1)
						   			    ,HCHUBCITIID INT
										,FAQCategoryID INT
										,FAQID INT
										, SORTORDER INT)


					CREATE TABLE #FAQSort( Rownum int identity(1,1)
						                    ,HCHUBCITIID INT
                                            ,FAQCategoryID INT
                                            , SORTORDER INT)

                      
                     SELECT RowNum = IDENTITY(INT, 1, 1)
                           , FAQCategoryID = Param
                     INTO #FAQCategoryId
                     FROM dbo.fn_SplitParamMultiDelimiter(@FAQCategoryId, '!~~!')                     
               
                     SELECT RowNum = IDENTITY(INT, 1, 1)
                           , FAQId = Param
                     INTO #FAQId
                     FROM dbo.fn_SplitParamMultiDelimiter(@FAQId, '!~~!')  
                     
                     SELECT RowNum = IDENTITY(INT, 1, 1)
                           , SortOrder = Param
                     INTO #SortOrder
                     FROM dbo.fn_SplitParamMultiDelimiter(@SortOrder, '!~~!')  

                     SELECT F.FAQCategoryID 
                           ,Q.FAQId 
                           ,S.SortOrder 
					 INTO #FAQC
                     FROM #FAQCategoryId F 
                     INNER JOIN #FAQId Q ON Q.RowNum =F.RowNum 
                     INNER JOIN #SortOrder S ON S.RowNum =F.RowNum                      
                           
                    --Logic to associate the pipe separated business categories to menu item.  
                    IF CURSOR_STATUS('global','FAQCategoryList')>=-1
                    BEGIN
                    DEALLOCATE FAQCategoryList
                    END
                                                
                    DECLARE @FAQsCategoryID int
                    DECLARE @FAQsID varchar(1000)
                    DECLARE @Sort VARCHAR(1000)
                    DECLARE FAQCategoryList CURSOR STATIC FOR
                    SELECT FAQCategoryID
                            , FAQId --Pipe separated
                            --,SortOrder 
                    FROM #FAQC 
                           
                    OPEN FAQCategoryList
                           
                    FETCH NEXT FROM FAQCategoryList INTO @FAQsCategoryID, @FAQsID--,@Sort
                           
                    CREATE TABLE #TempEventBiz(FAQCategoryID INT)

					Declare @Sortso int=0
                    --Check if there are rows present in the cursor
                    WHILE @@FETCH_STATUS = 0
                    BEGIN   
                            
							INSERT INTO #FAQCA(HCHUBCITIID
											  ,FAQCategoryID 
											  ,FAQID 
											  , SORTORDER)                                  
							OUTPUT inserted.FAQCategoryID INTO #TempEventBiz(FAQCategoryID)
							SELECT @HCHubCitiID 
								 , @FAQsCategoryID
								 , F.[Param]
								 , null       
							FROM dbo.fn_SplitParam(@FAQsID, ',') F
                             Print 'Dhaya'            
                                  
                                  
							TRUNCATE TABLE #TempEventBiz
							--SET @Sortso=0
							FETCH NEXT FROM FAQCategoryList INTO @FAQsCategoryID, @FAQsID--,@Sort
                                  
                    END
                           
                    --Deallocate the memory allocated after the execution.
                    CLOSE FAQCategoryList
                    DEALLOCATE FAQCategoryList

					DECLARE @FAQCID Int 
					DECLARE @SortFAQ Varchar(1000)

					         
                    --Logic to associate the pipe separated business categories to menu item.  
                    IF CURSOR_STATUS('global','Sortorders')>=-1
                    BEGIN
                    DEALLOCATE Sortorders
                    END

                    Declare Sortorders Cursor Static for
					Select  FAQCategoryID 
						    ,SortOrder --Pipe saperated
                    From #FAQC
						   
					OPEN  Sortorders
					Fetch next from Sortorders INTO @FAQCID,@SortFAQ
						                            
					CREATE TABLE #TempSortOrder(FAQCategoryID INT)

					WHILE @@FETCH_STATUS =0
					BEGIN

						INSERT INTO #FAQSort( HCHUBCITIID 
											 ,FAQCategoryID 
											 ,SORTORDER )
						SELECT @HcHubcitiID 
								,@FAQCID
								,Param 								    
						FROM dbo.fn_SplitParam(@SortFAQ,',') 
                                  
                        TRUNCATE TABLE #TempSortOrder
                        FETCH NEXT FROM Sortorders INTO @FAQCID,@SortFAQ--,@Sort
                                  
                    END
                           
                    --Deallocate the memory allocated after the execution.
                    CLOSE Sortorders
                    DEALLOCATE Sortorders

					UPDATE #FAQCA SET SORTORDER =S.SORTORDER 
					FROM #FAQCA F
					INNER JOIN #FAQSort S ON S.Rownum=F.rownum

		   
		    --Update FAQ Category based on sorting.
		    UPDATE HcFAQ SET SortOrder = T.SORTORDER  
		    FROM HcFAQ F
	        INNER JOIN #FAQCA T ON T.FAQCategoryID =F.HcFAQCategoryID AND F.HcFAQID =T.FAQID AND F.HcHubCitiID =@HcHubcitiID   
	        
	   --Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION      
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebHCFAQSortOrderUpdation.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  --Confirmation of Failure.
                  SELECT @Status =1
                  ROLLBACK TRANSACTION;              
                  
            END;
            
      END CATCH;
END;




GO
