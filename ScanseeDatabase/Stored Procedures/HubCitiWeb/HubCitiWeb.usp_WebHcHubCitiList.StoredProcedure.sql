USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcHubCitiList]
Purpose					: To list all active HubCities
Example					: [usp_WebHcHubCitiList] 

History
Version		    Date		  Author				Change Description
--------------------------------------------------------------- 
1.0				11 Sep 2014	  Mohith H R	        1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiList]
(
		

    --OutPut Variable
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		
			--List of all active HubCities.			

			SELECT HcHubCitiID regHubcitiIds
			      ,HubCitiName
			FROM HcHubCiti HC
			WHERE Active = 1 AND HcAppListID IN (SELECT A.HcAppListID FROM HcHubCiti H
												INNER JOIN HcAppList A ON H.HcAppListID = A.HcAppListID WHERE A.HcAppListName = 'HubCitiApp')
		    ORDER BY HubCitiName
			
			
			--Confirmation of Success
			SELECT @Status = 0
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcHubCitiList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
