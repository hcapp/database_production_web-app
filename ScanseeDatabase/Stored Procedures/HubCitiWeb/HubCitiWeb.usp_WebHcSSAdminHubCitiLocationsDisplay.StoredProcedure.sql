USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminHubCitiLocationsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcSSAdminHubCitiLocationsDisplay
Purpose					: To disply the list of the associated Cities or Postal Codes.
Example					: usp_WebHcSSAdminHubCitiLocationsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminHubCitiLocationsDisplay]
(
   ----Input variable.
	  @ScanSeeAdminUserID int
	, @HubCitiID int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--List out the associated locations(cities or postal codes).			
			SELECT [State]
			     , City 
				 , Location = PostalCode
			FROM HcLocationAssociation
			WHERE HcHubCitiID = @HubCitiID
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcSSAdminHubCitiLocationsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
