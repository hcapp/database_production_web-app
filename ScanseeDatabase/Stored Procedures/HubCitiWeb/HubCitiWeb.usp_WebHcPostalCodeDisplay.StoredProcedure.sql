USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcPostalCodeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcPostalCodeDisplay
Purpose					: To disply the list of the postal codes associated to the state.
Example					: usp_WebHcPostalCodeDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcPostalCodeDisplay]
(
   ----Input variable.
      @City Varchar(100)
     ,@State char(2)
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT PostalCode
			FROM GeoPosition 
			WHERE City = @City AND [State] = @State
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcPostalCodeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
