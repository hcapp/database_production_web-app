USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiBandEventsAssociation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcHubCitiBandEventsAssociation]
Purpose					: Associate bands to an event
Example					: [usp_WebHcHubCitiBandEventsAssociation]

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			12th Sept 2016     Sagar Byali		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiBandEventsAssociation]
(
   ----Input variable. 	  
	  @HcHubCitiID Int
	, @HcEventID int 

	, @AssociatedBandID VARCHAR(MAX)
	, @UnAssociatedBandID VARCHAR(MAX)
	
	, @BandStartDate varchar(MAX)
	, @BandEndDate varchar(MAX)
	, @BandStartTime varchar(MAX)
	, @BandEndTime varchar(MAX)
	, @BandStage varchar(MAX) 

	--Output Variable 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(MAX) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION    
	 
			       
			--To collect un-associated bands					
			SELECT Rownum=Identity(Int,1,1)
			      ,Param UnAssociatedBandID
			INTO #UnAssociatedBandID
			FROM fn_SplitParam(@UnAssociatedBandID,',')

			--To collect & associate bands
			SELECT Rownum=Identity(Int,1,1)
			      ,Param AssociatedBandID
			INTO #AssociatedBandID
			FROM fn_SplitParam(@AssociatedBandID,',')

			--To delete un-associated bands & associated bands from HcBandEventsAssociation
			DELETE FROM HcBandEventsAssociation 
			WHERE BandID IN (SELECT UnAssociatedBandID FROM #UnAssociatedBandID) AND HcBandEventID = @HcEventID
			DELETE FROM HcBandEventsAssociation 
			WHERE BandID IN (SELECT AssociatedBandID FROM #AssociatedBandID) AND HcBandEventID = @HcEventID
	
			SELECT Rownum=Identity(Int,1,1)
			      ,Param BandStartDate
			INTO #BandStartDate
			FROM fn_SplitParam(@BandStartDate,',')
			
			SELECT Rownum=Identity(Int,1,1)
			      ,Param BandEndDate
			INTO #BandEndDate
			FROM fn_SplitParam(@BandEndDate,',')
			
			SELECT Rownum=Identity(Int,1,1)
			      ,Param BandStartTime
			INTO #BandStartTime
			FROM fn_SplitParam(@BandStartTime,',')
			
			SELECT Rownum=Identity(Int,1,1)
			      ,Param BandEndTime
			INTO #BandEndTime
			FROM fn_SplitParam(@BandEndTime,',')
			
			SELECT Rownum=Identity(Int,1,1)
			      ,Param BandStage
			INTO #BandStage
			FROM dbo.fn_SplitParamMultiDelimiter(@BandStage, '!~~!')
			

			--cast(month(getdate()) as varchar) + '/' + cast(day(getdate()) as varchar)  + '/' + cast(year(getdate()) as varchar)
			--To update end date , band stage when they are null
			UPDATE #BandEndDate
			SET BandEndDate = BandStartDate 
			from #BandEndDate E
			inner join #BandStartDate S on S.Rownum = E.Rownum
			WHERE BandEndDate = 'NULL' OR BandEndDate = ' '

			UPDATE #BandStage
			SET BandStage = NULL
			WHERE BandStage = 'NULL' 


			--IF EXISTS (SELECT 1 FROM #AssociatedBandID WHERE AssociatedBandID IS NOT NULL AND AssociatedBandID <> 'NULL')
			--BEGIN
			--	IF EXISTS (SELECT 1 FROM #BandStartDate WHERE BandStartDate IS NOT NULL AND BandStartDate <> 'NULL' AND BandStartDate <> ' ')
			--	BEGIN

			--To associate bands to an event
					INSERT INTO HcBandEventsAssociation(HcBandEventID,
														BandID,
														DateCreated,
														DateModified,
														CreatedUserID,
														ModifiedUserID,
														BandStage,
														BandStartTime,
														BandEndTime,
														BandEndDate,
														BandStartDate
														)
					SELECT  @HcEventID,
							AssociatedBandID,
							getdate(),
							null,
							3,
							3,
							DD.BandStage,
							cast(BandStartTime as time), 
							cast(BandEndTime as time),
							cast(BandEndDate as date),
							cast(BandStartDate as date)

					FROM #AssociatedBandID T
					INNER JOIN #BandStartDate L ON L.Rownum =T.Rownum
					INNER JOIN #BandEndDate P ON P.Rownum =L.Rownum
					INNER JOIN #BandStartTime NT ON NT.Rownum =P.Rownum
					INNER JOIN #BandEndTime D ON D.Rownum =NT.Rownum 
					LEFT JOIN #BandStage DD ON DD.Rownum =T.Rownum 
					WHERE T.AssociatedBandID IS NOT NULL
			--	END
			--END
	
	

	
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 

		THROW;
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiBandEventsCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
