USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HubCitiWebCouponDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HubCitiWebCouponDeletion    
Purpose				  : To delete a coupon.   
Example				  : usp_HubCitiWebCouponDeletion    
    
History
Version		Date			Author			Change Description
-----------------------------------------------------------------
1.0        12/6/2016	  Shilpashree	    Initial version
-----------------------------------------------------------------  
*/    
    
CREATE PROCEDURE [HubCitiWeb].[usp_HubCitiWebCouponDeletion]    
(    
	  @CouponID int

	--Output Variable 
	, @Status int output  
	, @ErrorNumber int output    
	, @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
	BEGIN TRY    
		BEGIN TRAN 
			
			DELETE FROM UserCouponGallery WHERE CouponID = @CouponID
			DELETE FROM CouponProduct WHERE CouponID = @CouponID	
			DELETE FROM CouponRetailer WHERE CouponID = @CouponID
			DELETE FROM CouponProductCategoryAssociation WHERE CouponID = @CouponID
			DELETE FROM HubCitiFeaturedCoupon where Couponid = @CouponID
			DELETE FROM Coupon WHERE CouponID = @CouponID
		
		--Confirmation of Success.
		SELECT @Status = 0
		
		COMMIT

	 END TRY    
      
	 BEGIN CATCH    
     
	  --Check whether the Transaction is uncommitable.    
	  IF @@ERROR <> 0    
	  BEGIN    
	   PRINT 'Error occured in Stored Procedure usp_HubCitiWebCouponDeletion.'      
	   --- Execute retrieval of Error info.    
	   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       ROLLBACK TRAN

	   --Confirmation of failure.
	   SELECT @Status = 1  

	  END;    
       
	 END CATCH;    
END;





GO
