USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiRetailerEventDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiRetailerEventDetails
Purpose					: To display HubCiti/Retailer Event Details.
Example					: usp_WebHcHubCitiRetailerEventDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19 Feb 2015	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiRetailerEventDetails]
(   
    --Input variable.	  
	  @HcEventID Int
    , @HubCitiID int
  
	--Output Variable
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output 
    , @Status int output	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		
		SELECT @IOSAppID = IOSAppID
			  ,@AndroidAppID = AndroidAppID
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID

		--Retrieve the server configuration.
		DECLARE @QRConfig varchar(500)		
        SELECT @QRConfig = ScreenContent 
        FROM AppConfiguration 
        WHERE ConfigurationType LIKE 'QR Code Configuration'
        AND Active = 1 

		DECLARE @RetailerEventID int

			SELECT @RetailerEventID = RetailID
			FROM HcEvents
			WHERE HcEventID = @HcEventID

			IF @RetailerEventID IS NULL
			BEGIN
			
				DECLARE @HcEventCategoryID Varchar(2000)
				DECLARE @AppSiteID Int
				DECLARE @RetailLocationID Varchar(2000)
				DECLARE @HotelFlag Bit
				DECLARE @Config VARCHAR(500)
				DECLARE @WeeklyDays VARCHAR(1000)

				SELECT @Config = ScreenContent
				FROM AppConfiguration
				WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
				SELECT @HcEventCategoryID=COALESCE(@HcEventCategoryID+',', '')+CAST(HcEventCategoryID AS VARCHAR)
				FROM HcEventsCategoryAssociation 
				WHERE HcEventID =@HcEventID 

				SELECT @AppSiteID=HcAppsiteID 
				FROM HcEventAppsite 
				WHERE HcEventID =@HcEventID 

				--SELECT @AppSiteID=HcAppsiteID 
				--FROM HcEventAppsite 
				--WHERE @HcEventID =@HcEventID 
            
				--Select Event Package if Exist
				SELECT @RetailLocationID=COALESCE(@RetailLocationID+',', '')+CAST(RetailLocationID AS VARCHAR)
				FROM HcEventPackage 
				WHERE HcEventID =@HcEventID
			
				SELECT @HotelFlag=CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
				FROM HcEventPackage 
				WHERE HcEventID =@HcEventID  
			
				SELECT @WeeklyDays=COALESCE(@WeeklyDays+',', '')+CAST([DayName] AS VARCHAR)
				FROM HcEventInterval			
				WHERE HcEventID =@HcEventID
			
				SET @WeeklyDays=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@WeeklyDays, 'Sunday', '1'), 'Monday', '2'), 'Tuesday', '3'), 'Wednesday', '4'), 'Thursday', '5'), 'Friday', '6'), 'Saturday', '7')

				--To display Event Details.
				SELECT DISTINCT HcEventName hcEventName 
							   ,ShortDescription shortDescription
							   ,LongDescription longDescription 								
							   --,E.HcHubCitiID 
							   ,eventImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
							   --,ImagePath eventImageName
							   --,BussinessEvent bsnsLoc
							   --,PackageEvent evntPckg								
							   ,eventStartDate =CAST(StartDate AS DATE)
							   ,eventEndDate =ISNULL(CAST(EndDate AS DATE),NULL)
							   --,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   --,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)
							   ,Address
							   ,City
							   ,State
							   ,PostalCode
							   --,Latitude
							   --,Longuitude logitude						
							  -- ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
									--			  FROM HcMenuItem MI 
									--			  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
									--			  WHERE LinkTypeName = 'Events' 
									--			  AND MI.LinkID = @HcEventID )>0 THEN 1 ELSE 0 END
							  --,@HcEventCategoryID  eventCategory		
							  --,PackageDescription packageDescription 
							  --,PackageTicketURL packageTicketURL
							  --,PackagePrice packagePrice
							  --,@HotelFlag evntHotel	
							  , AppsiteIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(HcAppsiteID AS VARCHAR(10)))
														FROM HcEventAppsite HA
														WHERE HA.HcEventID = E.HcEventID
														FOR XML PATH( '' )
													  ), 3, 1000 ), ' ', '')
							 --, RetailLocationIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(RetailLocationID AS VARCHAR(10)))
								--						FROM HcEventPackage HP
								--						WHERE HP.HcEventID = E.HcEventID
								--						FOR XML PATH( '' )
								--					  ), 3, 1000 ), ' ', '')		
							 --, EL.GeoErrorFlag geoError	
							 , MoreInformationURL moreInfoURL
							 , QRUrl = @QRConfig+'2400.htm?eventId='+ CAST(@HcEventID AS VARCHAR(10)) +'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10))    
							 --,OnGoingEvent isOnGoing	 	
							 --,E.HcEventRecurrencePatternID recurrencePatternID
							 --,HR.RecurrencePattern recurrencePatternName
							 --,[isWeekDay] = CASE WHEN HR.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
							 --,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
							 --,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
							 --,ByDayNumber = CASE WHEN HR.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
							 --,DayNumber
							 --,MonthInterval	
							 --,EventFrequency AS EndAfter									
				FROM HcEvents E
				LEFT JOIN HcEventLocation EL ON E.HcEventID =EL.HcEventID 
				LEFT JOIN HcEventRecurrencePattern HR ON HR.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
				LEFT JOIN HcEventInterval EI ON EL.HcEventID = EI.HcEventID
				WHERE E.HcEventID =@HcEventID
				AND E.Active = 1 
				
			
				--Confirmation of Success
				SELECT @Status = 0	
		END
		ELSE
		BEGIN
				DECLARE @EventCategoryID Varchar(2000)
				--DECLARE @RetailLocationID Varchar(2000)
				DECLARE @RetConfig VARCHAR(500)
				DECLARE @WeeklyDayss VARCHAR(1000)

				SELECT @RetConfig=ScreenContent
				FROM AppConfiguration
				WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
			
				SELECT @EventCategoryID = COALESCE(@EventCategoryID+',', '') + CAST(HcEventCategoryID AS VARCHAR)
				FROM HcEventsCategoryAssociation 
				WHERE HcEventID = @HcEventID 
			
				SELECT @WeeklyDayss = COALESCE(@WeeklyDayss+',', '') + CAST([DayName] AS VARCHAR)
				FROM HcEventInterval			
				WHERE HcEventID = @HcEventID
			
				SET @WeeklyDayss=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@WeeklyDayss, 'Sunday', '1'), 'Monday', '2'), 'Tuesday', '3'), 'Wednesday', '4'), 'Thursday', '5'), 'Friday', '6'), 'Saturday', '7')

				--To display Event Details.
				SELECT DISTINCT 
				--E.HcEventID EventID
							    HcEventName hcEventName 
							   ,ShortDescription shortDescription
							   ,LongDescription longDescription 								
							   ,eventImagePath=@RetConfig + CAST(E.RetailID AS VARCHAR(100))+'/'+ImagePath 
							   --,ImagePath eventImageName
							   --,BussinessEvent bsnsLoc
							   ,eventStartDate =CAST(StartDate AS DATE)
							   ,eventEndDate =ISNULL(CAST(EndDate AS DATE),NULL)
							   --,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   --,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)
								,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcRetailerEventsAssociation A 
																				  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																				  WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																			 ELSE EL.Address 
											 END
								  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcRetailerEventsAssociation A 
																				INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																				WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																			 ELSE EL.City 
										  END
								  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcRetailerEventsAssociation A 
																				INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																				WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																			 ELSE EL.State 
										  END
								  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcRetailerEventsAssociation A 
																					INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																					WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																			  ELSE EL.PostalCode 
										  END  	
						
								 --,Latitude = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.RetailLocationLatitude FROM HcRetailerEventsAssociation A 
									--												INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
									--												WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
									--										  ELSE EL.Latitude 
									--	  END  	
								 --,Longitude = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.RetailLocationLongitude FROM HcRetailerEventsAssociation A 
									--												INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
									--												WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
									--										  ELSE EL.Longuitude 
									--	  END  		
							  -- ,@EventCategoryID  EventCategory		
							   ,AppsiteIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(RetailLocationID AS VARCHAR(10)))
														FROM HcRetailerEventsAssociation REA
														WHERE REA.HcEventID = E.HcEventID
														FOR XML PATH( '' )
													  ), 3, 1000 ), ' ', '')
							  --,geoError = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE EL.GeoErrorFlag END 		
							  ,MoreInformationURL moreInfoURL
							  ,QRUrl = @QRConfig+'2400.htm?eventId='+ CAST(@HcEventID AS VARCHAR(10)) +'&hubcitiId=null'--+ CAST(@HubCitiId AS VARCHAR(10)) 
							  --,OnGoingEvent isOnGoing	 	
							  --,E.HcEventRecurrencePatternID recurrencePatternID
							  --,HR.RecurrencePattern recurrencePatternName
							  --,[isWeekDay] = CASE WHEN HR.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
							  --,REPLACE(REPLACE(@WeeklyDayss, '[', ''), ']', '') Days	
							  --,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
							  --,ByDayNumber = CASE WHEN HR.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
							  --,DayNumber
							  --,MonthInterval	
							  ,EventFrequency AS EndAfter									
				FROM HcEvents E
				LEFT JOIN HcRetailerEventsAssociation RE ON RE.HcEventID = E.HcEventID
				LEFT JOIN RetailLocation RL ON RE.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
				LEFT JOIN HcEventLocation EL ON E.HcEventID =EL.HcEventID 
				LEFT JOIN HcEventRecurrencePattern HR ON HR.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
				LEFT JOIN HcEventInterval EI ON EL.HcEventID = EI.HcEventID
				WHERE E.HcEventID = @HcEventID 
				AND E.RetailID IS NOT NULL
				AND E.Active = 1
			
			--Confirmation of Success
			SELECT @Status = 0	      
		END		      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiRetailerEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
