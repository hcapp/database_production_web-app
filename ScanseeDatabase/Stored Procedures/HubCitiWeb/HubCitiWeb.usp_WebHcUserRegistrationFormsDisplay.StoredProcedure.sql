USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcUserRegistrationFormsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcUserRegistrationFormsDisplay
Purpose					: Creating User Registration Forms For Hubciti.
Example					: usp_WebHcUserRegistrationFormsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17/12/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcUserRegistrationFormsDisplay]
(
  	  @HubCitiID Int	
	  
	--Output Variable	
	--, @DuplicateFlag Bit OutPut
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
	      
		    DECLARE @ImagePath Varchar(1000)
			DECLARE @Config VARCHAR(500)
			DECLARE @Fields VARCHAR(MAX)
			DECLARE @RequiredFlag VARCHAR(MAX)
			
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

	        SELECT @ImagePath=UserSettingsImagePath
			FROM HcMenuCustomUI WHERE HubCitiId =@HubCitiID	

			SELECT @Fields = COALESCE(@Fields + ',', '')+RegistrationFieldsDisplayName				   
		    FROM HcHubcitiUserRegistrationForms F
			INNER JOIN HcUserRegistrationFormFields UF ON F.HcUserRegistrationFormFieldsID =UF.HcUserRegistrationFormFieldsID AND F.HchubcitiID =@HubCitiID					   			
			ORDER BY Position
						
			SELECT @RequiredFlag = COALESCE(@RequiredFlag+ ',', '')+F.[Required]
		    FROM HcHubcitiUserRegistrationForms F
			WHERE F.HchubcitiID =@HubCitiID	
			ORDER BY F.Position
			
			--Display selected Hubciti user Registration form details			
			SELECT DISTINCT @Fields userSettingsFields
				  ,@RequiredFlag RequiredFields
				  ,@ImagePath logoImageName
				  ,CASE WHEN @ImagePath IS NOT NULL THEN @Config+CAST(@HubCitiID AS VARCHAR(100))+'/'+@ImagePath END logoPath
			FROM HcHubcitiUserRegistrationForms F
			INNER JOIN HcUserRegistrationFormFields UF ON F.HcUserRegistrationFormFieldsID =UF.HcUserRegistrationFormFieldsID AND F.HchubcitiID =@HubCitiID		
			
			
				   			
			--ORDER BY Position
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcUserRegistrationFormsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
