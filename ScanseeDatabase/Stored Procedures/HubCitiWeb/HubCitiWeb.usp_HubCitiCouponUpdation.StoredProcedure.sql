USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HubCitiCouponUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HubCitiCouponUpdation
Purpose					: To update existing coupon in HubCiti.

History
Version		   Date			Author			Change Description
------------------------------------------------------------------------------- 
1.0        5th Dec 2016	  Shilpashree	    Initial version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_HubCitiCouponUpdation]
(
	--Input Variables
	  @CouponID int 
	, @UserID int
	, @RetailID int
	, @RetailLocationID VARCHAR(Max)
	, @CouponName varchar(200)
	, @CouponBannerTitle Varchar(1000)
	, @NoOfCouponsToIssue int
	, @CouponDescription Nvarchar(MAX)
	, @CouponTermsAndConditions varchar(MAX)
	, @CouponImagePath varchar(1000)
	, @CouponDetailImage varchar(1000)
	--, @ProductCategoryIDs Varchar(Max)
	, @CouponStartDate date
	, @CouponEndDate date
	, @CouponStartTime time
	, @CouponEndTime time
	, @CouponExpirationDate date
	, @CouponExpirationTime time
	, @KeyWords varchar(1000)
	, @HcHubCitiID int
	
	--Output Variable--
	, @FindClearCacheURL varchar(1000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY 

		BEGIN TRANSACTION
		
		UPDATE Coupon 
		SET CouponName	= @CouponName
			,BannerTitle = @CouponBannerTitle
			,CouponShortDescription = @CouponDescription
			,CouponTermsAndConditions = @CouponTermsAndConditions
			,NoOfCouponsToIssue = @NoOfCouponsToIssue
			,CouponImagePath = @CouponImagePath
			,CouponDetailImage = @CouponDetailImage
			,CouponStartDate = CAST(@CouponStartDate AS DATETIME) + CAST(ISNULL(@CouponStartTime,'') AS DATETIME)
			,CouponExpireDate = CAST(@CouponEndDate AS DATETIME) + CAST(ISNULL(@CouponEndTime,'') AS DATETIME)
			,ActualCouponExpirationDate = CAST(ISNULL(@CouponExpirationDate,NULL) AS DATETIME) + CAST(ISNULL(@CouponExpirationTime,'') AS DATETIME)
			,ModifiedUserID = @UserID
			,KeyWords = @KeyWords
			,HcHubCitiID = @HcHubCitiID
		WHERE CouponID = @CouponID

		
		--DELETE FROM CouponProductCategoryAssociation WHERE CouponID = @CouponID
				
		--IF(@ProductCategoryIDs IS NOT NULL) --Associate the Coupon to the given Product Categories
		--BEGIN						
		
		--INSERT INTO CouponProductCategoryAssociation(CouponID
		--												,ProductCategoryID
		--												,DateCreated
		--												,CreatedUserID
		--												)
		--										SELECT @CouponID
		--											, P.Param
		--											, GETDATE()
		--											, @UserID
		--										FROM dbo.fn_SplitParam(@ProductCategoryIDs,',') P
			
		--END
		
		DELETE FROM CouponRetailer WHERE CouponID = @CouponID
			
		IF(@RetailLocationID IS NOT NULL) --Associate the Coupon to the given Retail Locations
		BEGIN

		INSERT INTO CouponRetailer (CouponID
									, RetailID 
									, RetailLocationID
									, DateCreated	
									)
                            SELECT @CouponID
                                , @RetailID
                                , RL.Param
                                , GETDATE()
                            FROM dbo.fn_SplitParam(@RetailLocationID, ',') RL									
		END

		-------Find Clear Cache URL-----------

		DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

		SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
		SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
		SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

		SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
		FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		------------------------------------------------
		
		--Confirmation of Success.
		SELECT @Status = 0

		COMMIT TRANSACTION
	  END TRY
            
	BEGIN CATCH
			
			INSERT  INTO SCANSEEVALIDATIONERRORS(ERRORCODE,ERRORLINE,ERRORDESCRIPTION,ERRORPROCEDURE,Inputon)
			VALUES(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE(),getdate())
			SET @ErrorMessage=ERROR_MESSAGE() 	SET @ErrorNumber=ERROR_NUMBER()  
					
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			
			--Confirmation of failure.
			SELECT @Status = 1  
	 END CATCH;
END;









GO
