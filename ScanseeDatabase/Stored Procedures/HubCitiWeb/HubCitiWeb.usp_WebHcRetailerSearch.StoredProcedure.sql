USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRetailerSearch]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcRetailerSearch]
Purpose					: To capture HubCiti Retailer information
Example					: [usp_WebHcRetailerSearch] 

History
Version		     Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			24th oct 2013	  SPAN	        1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRetailerSearch]
(
	
	--Input Variable
	  @HubCitiID int
	, @SearchKey varchar(100)
    
    --OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		
			SELECT DISTINCT R.RetailID retailId
				  , R.RetailName retName
			FROM HcLocationAssociation LA
			INNER JOIN RetailLocation RL ON LA.PostalCode = RL.PostalCode
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND RLC.Associated = 1
			INNER JOIN Retailer R ON RL.RetailID = R.RetailID
			WHERE LA.HcHubCitiID = @HubCitiID
			AND R.RetailName LIKE '%' + @SearchKey + '%' AND RL.Headquarters <> 1 AND Active = 1 AND R.RetailerActive=1	
			
			--Confirmation of Success
			SELECT @Status = 0
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcRetailerSearch].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
