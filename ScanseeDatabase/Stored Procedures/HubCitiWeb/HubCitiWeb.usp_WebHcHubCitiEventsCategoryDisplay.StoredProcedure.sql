USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventsCategoryDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventsCategoryDisplay
Purpose					: To display List of Events Categories.
Example					: usp_WebHcHubCitiEventsCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventsCategoryDisplay]
(   
    --Input variable.
	  @UserID int	  
    , @CategoryName varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(200)
	, @EventCategoryID int
	, @RoleBasedUserID int
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   

			DECLARE @Config Varchar(500)
				
			----To get the Configuration Details.	 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			--To get user info i.e Super/Normal user.

			DECLARE @RoleName VARCHAR(100)
			SELECT @RoleName = HcRoleName
			FROM HcUserRole UR
			INNER JOIN HcRole R ON UR.HcRoleID = R.HcRoleID
			WHERE HcAdminUserID = @UserID
			AND (HcRoleName LIKE '%EVENT%'  OR HcRoleName LIKE '%ROLE_ADMIN%')

			--To display List fo Events Categories

			CREATE TABLE #EventsCat(RowNum INT
									,HcEventsCategory INT
									,HcEventCategoryName VARCHAR(1000)
									,AssociateCate INT
									,CategoryImageName VARCHAR(1000)
									,CategoryImagePath VARCHAR(1000))

			IF @UserID IS NOT NULL AND @RoleBasedUserID IS NOT NULL 
			BEGIN
			print 'b'
				INSERT INTO #EventsCat(RowNum 
									,HcEventsCategory 
									,HcEventCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(								
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY HcEventCategoryName) As Rownum
										,P.HcEventCategoryID catId
										,HcEventCategoryName catName
										,associateCate = IIF(F.CreatedUserID=@RoleBasedUserID,'1','0')
										,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
										,CategoryImagePath cateImg
								FROM HcEventsCategory P 
								LEFT JOIN HcEventsCategoryAssociation UE ON P.HcEventCategoryID = UE.HcEventCategoryID AND UE.CreatedUserID = @RoleBasedUserID	
								LEFT JOIN HcEvents F ON UE.HcEventID = F.HcEventID AND F.Active = 1	
								WHERE ((@CategoryName IS NOT NULL AND HcEventCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@EventCategoryID IS NOT NULL AND P.HcEventCategoryID = @EventCategoryID) OR (@EventCategoryID IS NULL and 1=1))
								) EventsCat			
																			
								
			END
			ELSE IF @RoleName = 'ROLE_EVENT_USER' AND @RoleBasedUserID IS NULL
			BEGIN
			print 'aaaaa'
				INSERT INTO #EventsCat(RowNum 
									,HcEventsCategory 
									,HcEventCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY HcEventCategoryName) As Rownum
									  ,P.HcEventCategoryID catId
									  ,HcEventCategoryName catName
									  ,associateCate = CASE WHEN (SELECT COUNT(1) FROM HcEventsCategoryAssociation WHERE HcEventCategoryID=P.HcEventCategoryID) >0 THEN 1 ELSE 0 END
									  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
									  ,CategoryImagePath cateImg
								--INTO #EventsCat
								FROM HcEventsCategory P 
								LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON P.HcEventCategoryID = UE.HcEventCategoryID
								WHERE ((@CategoryName IS NOT NULL AND HcEventCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@Eventcategoryid IS NOT NULL AND P.HcEventCategoryID = @EventCategoryID) OR (@Eventcategoryid IS NULL and 1=1))
								AND UE.UserID = @UserID) EventsCat
			END
			ELSE IF @RoleName <> 'ROLE_EVENT_USER' AND @RoleBasedUserID IS NULL		
			BEGIN
						print 'bbb'
				INSERT INTO #EventsCat(RowNum 
									,HcEventsCategory 
									,HcEventCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY HcEventCategoryName) As Rownum
									  ,P.HcEventCategoryID catId
									  ,HcEventCategoryName catName
									  ,associateCate = 0--CASE WHEN (SELECT COUNT(1) FROM HcEventsCategoryAssociation WHERE HcEventCategoryID=P.HcEventCategoryID) >0 THEN 1 ELSE 0 END
									  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
									  ,CategoryImagePath cateImg
								--INTO #EventsCat
								FROM HcEventsCategory P 
								--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON P.HcEventsCategory = UE.HcEventsCategory
								WHERE ((@CategoryName IS NOT NULL AND HcEventCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@Eventcategoryid IS NOT NULL AND P.HcEventCategoryID = @EventCategoryID) OR (@Eventcategoryid IS NULL and 1=1)))EventsCat

			END
			ELSE IF @UserID IS NOT NULL AND @RoleBasedUserID IS NULL			
			BEGIN
						print 'bdre'
				INSERT INTO #EventsCat(RowNum 
									,HcEventsCategory 
									,HcEventCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(			
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY HcEventCategoryName) As Rownum
									  ,P.HcEventCategoryID catId
									  ,HcEventCategoryName catName
									  ,associateCate = CASE WHEN (SELECT COUNT(1) FROM HcEventsCategoryAssociation WHERE HcEventCategoryID=P.HcEventCategoryID) >0 THEN 1 ELSE 0 END
									  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
									  ,CategoryImagePath cateImg
								--INTO #EventsCat
								FROM HcEventsCategory P 
								--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON P.HcEventsCategory = UE.HcEventsCategory
								WHERE ((@CategoryName IS NOT NULL AND HcEventCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@EventCategoryID IS NOT NULL AND P.HcEventCategoryID = @EventCategoryID) OR (@EventCategoryID IS NULL and 1=1))
								AND P.CreatedUserID = @UserID)EventsCat
			
			END
		
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #EventsCat
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			SELECT   Rownum 
					,HcEventsCategory catId
					,HcEventCategoryName catName
					,AssociateCate associateCate
					,CategoryImageName cateImgName
					,CategoryImagePath cateImg
			FROM #EventsCat 
			WHERE ((@LowerLimit IS NOT NULL AND RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit) OR @LowerLimit IS NULL) 	
		    ORDER BY RowNum 									
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventsCategoryDisplay.'				
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
