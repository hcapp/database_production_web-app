USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDataExportDashboardDataDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDataExportDashboardDataDisplay]
Purpose					: To display Dashboard data for given module.
Example					: [usp_WebHcDataExportDashboardDataDisplay]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			21 Apr 2015	     SPAN			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDataExportDashboardDataDisplay]
(   
	 --Input Variable
	  @HcDataExportTypeID int
	, @HcHubCitiID int
	, @HcDataExportModuleID Varchar(1000)  --Comma Separated ModuleIDs
	, @StartDate datetime
	, @EndDate datetime	 

	--Output Variable 
	, @ExportFilePath varchar(1000) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			

			--To get export file path

			DECLARE @Date INT
			DECLARE @Month INT
			DECLARE @Year INT
			DECLARE @HH INT
			DECLARE @MM INT

			SELECT @Month = FORMAT(GETDATE(), 'MM')
			SELECT @Date = FORMAT(GETDATE(), 'dd')
			SELECT @Year = FORMAT(GETDATE(), 'yyyy')
			SELECT @HH = FORMAT(GETDATE(), 'HH')
			SELECT @MM = FORMAT(GETDATE(), 'mm')

			SELECT @ExportFilePath = 'Dashboard_' + CAST(@Month AS varchar(100)) + '-' +CAST(@Date AS varchar(100)) + '-' + CAST(@Year AS varchar(100))
									+ '_' + CAST(@HH AS Varchar(100)) + '-' + CAST(@MM AS Varchar(100)) + '.xls'


			DECLARE @ColumnList varchar(1000)
			DECLARE @SqlCommand varchar(1000)

			SELECT ID = Param
			INTO #ModuleID
			FROM dbo.fn_SplitParamMultiDelimiter(@HcDataExportModuleID, ',')  

			----To fetch all modules Clicks and Impressions
			SELECT DISTINCT HubCitiName
					,Retailname, R.RetailID, RetailerImagePath, RL.RetailLocationID ,BC.BusinessCategoryName ,BSC.BusinessSubCategoryName
			INTO #Temp1
			FROM HcHubCiti H  with(nolock)
			INNER JOIN HcRetailerAssociation RA with(nolock) ON RA.HcHubCitiID = h.HcHubCitiID AND Associated = 1 AND H.HcHubCitiID = @HcHubCitiID
			INNER JOIN Retailer R with(nolock) ON R.Retailid = RA.RetailID 
			INNER JOIN RetailLocation RL with(nolock) ON RL.RetailLocationID = RA.RetailLocationID 
			INNER JOIN HcLocationAssociation LA with(nolock) ON LA.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID = @HcHubCitiID AND LA.PostalCode=RL.PostalCode 
			LEFT JOIN RetailerBusinessCategory RBC  with(nolock)ON RBC.RetailerID = R.RetailID 
			LEFT JOIN BusinessCategory BC with(nolock) ON BC.BusinessCategoryID = RBC.BusinessCategoryID 
			LEFT JOIN HcRetailerSubCategory RS with(nolock) ON RS.RetailLocationID = RL.RetailLocationID 
			LEFT JOIN HcBusinessSubCategory BSC with(nolock) ON BSC.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID 
			WHERE RL.Active = 1 AND R.RetailerActive=1 AND R.Retailid = RL.RetailID and R.Retailname <> 'SPANqa%'


			SELECT DISTINCT HubCitiName
					,Retailname, RetailID, RetailerImagePath, RetailLocationID --,BC.BusinessCategoryName ,BSC.BusinessSubCategoryName
			INTO #Temp12
			FROM #Temp1

			----RetailLocationList
			SELECT HubCitiName
				  ,Retailname
				  ,R.RetailID  
				  ,R.RetailLocationID
				  ,RetailLocationImpression = COUNT(DISTINCT RetailerListID)
				  ,RetailLocationClicks =  ISNULL(SUM(RetailLocationClick) , 0 )   
			INTO #RetailLocationList
			FROM #Temp12 R
			LEFT JOIN HubCitiReportingDatabase..RetailerList RL with(nolock) ON R.RetailLocationID=RL.RetailLocationID 
			AND CAST(@StartDate AS DATE) <= CAST(RL.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(RL.DateCreated AS DATE)
			GROUP BY HubCitiName,RetailName,R.RetailID,R.RetailLocationID--,RetailLocationClick
			ORDER BY RetailName

			

			----DealsList
			SELECT DISTINCT HR.RetailID,HR.RetailName,HR.RetailLocationID,HR.HubCitiName 
										,DealImpression = COUNT(DISTINCT SaleListID)
										,DealClicks = COUNT( CASE WHEN RetailLocationDealClick =1 THEN SaleListID END)
			INTO #Dealslist
			FROM #RetailLocationList HR
			LEFT JOIN RetailLocationDeal RLD with(nolock) ON RLD.RetailLocationID = HR.RetailLocationID 
			LEFT JOIN HubCitiReportingDatabase..SaleList SL with(nolock) ON SL.RetailLocationDealID =RLD.RetailLocationDealID
			AND CAST(@StartDate AS DATE) <= CAST(SL.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(SL.DateCreated AS DATE)
			GROUP BY HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName

			----HotDealList
			SELECT DISTINCT HR.RetailID,HR.RetailName ,HR.RetailLocationID ,HR.HubCitiName 
										,HotdealImpression = COUNT(DISTINCT HotDealListID)
										,HotdealClicks = COUNT( CASE WHEN HD.ProductHotDealClick =1 THEN HotDealListID END )
			INTO #HotDealList
			FROM #RetailLocationList HR
			LEFT JOIN ProductHotDealRetailLocation PRL with(nolock) ON PRL.RetailLocationID =HR.RetailLocationID 
			LEFT JOIN HubCitiReportingDatabase..HotDealList HD ON HD.ProductHotDealID =PRL.ProductHotDealID 
			AND CAST(@StartDate AS DATE) <= CAST(HD.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(HD.DateCreated AS DATE)
			GROUP BY HR.RetailID,HR.RetailName,HR.RetailLocationID,HR.HubCitiName

			----CouponList
			SELECT DISTINCT HR.RetailID,HR.RetailName ,HR.RetailLocationID ,HR.HubCitiName 
										,CouponImpression = COUNT(DISTINCT CouponsListID)
										,CouponClicks = COUNT( CASE WHEN CouponClick = 1 THEN CouponsListID END)
			INTO #CouponsList
			FROM #RetailLocationList HR
			LEFT JOIN CouponRetailer CR with(nolock) ON CR.RetailLocationID = HR.RetailLocationID 
			LEFT JOIN HubCitiReportingDatabase..CouponsList CL ON CL.CouponID = CR.CouponID 
			AND CAST(@StartDate AS DATE) <= CAST(CL.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(CL.DateCreated AS DATE)
			GROUP BY HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName

			----AppsiteList
			SELECT DISTINCT HR.RetailID,HR.RetailName ,HR.RetailLocationID ,HR.HubCitiName 
										,AppsiteImpression = COUNT(Distinct AppsiteListID)
										,AppsiteClicks = COUNT(CASE WHEN AppsiteClick =1 THEN AppsiteListID END)
			INTO #AppsiteList
			FROM #RetailLocationList HR
			LEFT JOIN HcAppSite A with(nolock) ON A.RetailLocationID =HR.RetailLocationID
			LEFT JOIN HubCitiReportingDatabase..AppsiteList AL ON AL.AppsiteID =A.HcAppSiteID
			AND CAST(@StartDate AS DATE) <= CAST(AL.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(AL.DateCreated AS DATE) 
			GROUP BY HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName

			----AnuthingPageList
			SELECT DISTINCT HR.RetailID,HR.RetailName ,HR.RetailLocationID ,HR.HubCitiName 
										,AnythingPageImpression = COUNT(DISTINCT AnythingPageListID) 
										,AnythingPageClicks = COUNT( CASE WHEN Ap.AnythingPageClick =1 THEN AnythingPageListID END) 
			INTO #AnythingPageList
			FROM #RetailLocationList HR
			LEFT JOIN QRRetailerCustomPageAssociation QR with(nolock) ON QR.RetailLocationID =HR.RetailLocationID 
			LEFT JOIN HubCitiReportingDatabase..AnythingPageList AP ON AP.AnythingPageID =QR.QRRetailerCustomPageID
			AND CAST(@StartDate AS DATE) <= CAST(AP.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(AP.DateCreated AS DATE)
			GROUP BY HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName

			----AnythingPageList1
			SELECT DISTINCT HR.RetailID,HR.RetailName ,HR.RetailLocationID ,HR.HubCitiName 
										,AnythingPageImpression = COUNT(DISTINCT AnythingPageListID) 
										,AnythingPageClicks = COUNT( CASE WHEN Ap.AnythingPageClick =1 THEN AnythingPageListID END) 
			INTO #AnythingPageList1
			FROM #RetailLocationList HR
			LEFT JOIN HcAppSite A  with(nolock) ON A.RetailLocationID =HR.RetailLocationID
			LEFT JOIN HubCitiReportingDatabase..AnythingPageList AP ON AP.AppsiteID =A.HcAppSiteID
			AND CAST(@StartDate AS DATE) <= CAST(AP.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(AP.DateCreated AS DATE)
			GROUP BY HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName

			----SpecialsList
			SELECT DISTINCT HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName 
										,RetailerSpecialsImpression = COUNT(DISTINCT SpecialsListID) 
										,RetailerSpecialsClicks = COUNT( CASE WHEN SPL.SpecialOfferClick =1 THEN SpecialsListID END)
			INTO #SpecialsList
			FROM #RetailLocationList HR
			LEFT JOIN QRRetailerCustomPageAssociation QR ON QR.RetailLocationID =HR.RetailLocationID 
			LEFT JOIN HubCitiReportingDatabase..SpecialsList SPL ON SPL.SpecialOfferID =QR.QRRetailerCustomPageID 
			AND CAST(@StartDate AS DATE) <= CAST(SPL.CreatedDate AS DATE) AND CAST(@EndDate AS DATE) >= CAST(SPL.CreatedDate AS DATE)
			GROUP BY HR.RetailID,HR.RetailName ,HR.RetailLocationID,HR.HubCitiName


		

			----Final Result
			SELECT DISTINCT HR.RetailID,HR.RetailName, HR.RetailLocationID,HR.HubCitiName    
										,IIF(T.BusinessCategoryName = '' , 'N/A',ISNULL(T.BusinessCategoryName,'N/A')) AS CategoryName
										,IIF(T.BusinessSubCategoryName = '' , 'N/A',ISNULL(T.BusinessSubCategoryName,'N/A')) AS SubCategoryName
										,RetailLocationImpression
										,RetailLocationClicks
										,AppsiteImpression
										,AppsiteClicks
										,AnythingPageImpression= A.AnythingPageImpression+AA.AnythingPageImpression
										,AnythingPageClicks =A.AnythingPageClicks+AA.AnythingPageClicks
										,RetailerSpecialsImpression
										,RetailerSpecialsClicks 
										,HotdealImpression
										,HotdealClicks
										,CouponImpression
										,CouponClicks
										,DealImpression
										,DealClicks
			INTO #AllColumns
			FROM #Temp1 T
			INNER JOIN #RetailLocationList HR ON T.RetailID = HR.RetailID
			INNER JOIN #AnythingPageList A ON A.RetailLocationID = HR.RetailLocationID 
			INNER JOIN #AnythingPageList1 AA ON AA.RetailLocationID = HR.RetailLocationID 
			INNER JOIN #HotdealList B ON B.RetailLocationID = HR.RetailLocationID 
			INNER JOIN #AppsiteList C ON C.RetailLocationID = HR.RetailLocationID 
			INNER JOIN #SpecialsList D ON D.RetailLocationID = HR.RetailLocationID 
			INNER JOIN #CouponsList E ON E.RetailLocationID = HR.RetailLocationID 
			INNER JOIN #Dealslist F ON F.RetailLocationID = HR.RetailLocationID
			--AND HR.retailid =1133749 
			ORDER BY RetailName

			SELECT @ColumnList = COALESCE(@ColumnList + ', ','') + HcDataExportModuleInfo   
			FROM #ModuleID M
			INNER JOIN HcDataExportModule HM ON M.ID = HM.HcDataExportModuleID
			INNER JOIN HcDataExportModuleInformation I ON HM.HcDataExportModuleID = I.HcDataExportModuleID


			SET @SqlCommand = 'SELECT ' + @ColumnList + ', RetailID, RetailName, RetailLocationID,HubCitiName FROM #AllColumns ' 
			SET @SqlCommand = 'SELECT DISTINCT RetailID, RetailName, RetailLocationID, HubCitiName, ' + @ColumnList + ' FROM #AllColumns ' 
			EXEC (@SqlCommand)
			


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 

		

		SELECT ERROR_LINE()
			PRINT 'Error occured in Stored Procedure [usp_WebHcDataExportDashboardDataDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
