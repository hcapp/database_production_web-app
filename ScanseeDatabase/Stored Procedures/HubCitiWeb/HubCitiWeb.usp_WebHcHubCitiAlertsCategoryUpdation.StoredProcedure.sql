USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsCategoryUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsCategoryUpdation
Purpose					: Update Selected  Alerts Category.
Example					: usp_WebHcHubCitiAlertsCategoryUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsCategoryUpdation]
(   
    --Input variable.	  
	 @HubCitiID Int
    ,@UserID Int
    ,@HcAlertCategoryID Int
	,@HcAlertCategoryName Varchar(1000)
  
	--Output Variable 
	, @DuplicateCategory Bit Output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Update Selected  Alerts Category.
			
			IF  (SELECT COUNT(1) FROM HcAlertCategory WHERE HcAlertCategoryName =@HcAlertCategoryName AND HcAlertCategoryID <> @HcAlertCategoryID AND HcHubCitiID =@HubCitiID)=0
			BEGIN
				UPDATE HcAlertCategory SET HcAlertCategoryName=@HcAlertCategoryName
										  ,DateModified =GETDATE()
										  ,ModifiedUserID =@UserID
				WHERE HcAlertCategoryID =@HcAlertCategoryID
		       
			    SET @DuplicateCategory=0
			END
			ELSE
			BEGIN
				SET @DuplicateCategory=1	
			END 
												
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsCategoryUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
