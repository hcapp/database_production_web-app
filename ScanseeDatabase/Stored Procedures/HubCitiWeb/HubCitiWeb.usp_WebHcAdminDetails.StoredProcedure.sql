USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminDetails
Purpose					: To view the details of the Hub Citi admin once logged in.
Example					: usp_WebHcAdminDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminDetails]
(
   ----Input variable.
      @UserName varchar(100)
      
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @AdminUserID INT
			
			SELECT @AdminUserID = UserID
			FROM Users 
			WHERE HcHubCitiID IS NOT NULL
			AND UserName = @UserName
		
			SELECT HC.HcHubCitiID
				 , HC.HubCitiName
				 , HU.UserID
				 , HU.UserName
			FROM HcHubCiti HC
			INNER JOIN Users HU ON HU.HcHubCitiID = HC.HcHubCitiID
			WHERE HU.UserID = @AdminUserID
				
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
		SELECT ERROR_LINE()	 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
