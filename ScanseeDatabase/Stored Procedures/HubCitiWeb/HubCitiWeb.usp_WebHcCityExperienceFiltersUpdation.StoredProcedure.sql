USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceFiltersUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceFiltersUpdation
Purpose					: Update Selected  City Experience Filter.
Example					: usp_WebHcCityExperienceFiltersUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceFiltersUpdation]
(   
    --Input variable.	  
	  @UserID Int
	, @HcHubCitiID Int
	, @HcCityExperienceID Int
	, @HcFilterID Int
	, @FilteryName Varchar(1000)
	, @ButtonImagePath  Varchar(2000)
	, @BottomButtonImagePath_ON Bit
	, @BottomButtonImagePath_OFF Bit
  
	--Output Variable 
	, @DuplicateFlag Bit Output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Update Selected  City Experience Filter.
			
		    IF  (SELECT COUNT(1) FROM HcFilter WHERE HcHubCitiID =@HcHubCitiID AND HcCityExperienceID =@HcCityExperienceID   AND FilterName =@FilteryName AND HcFilterID <> @HcFilterID)=0
			BEGIN
				Update HcFilter SET FilterName=@FilteryName
								   ,HcCityExperienceID=@HcCityExperienceID
											 ,HcHubCitiID=@HcHubCitiID
											 ,DateModified=GETDATE()
											 ,ModifiedUserID=@UserID
											 ,BottomButtonImagePath_ON=@BottomButtonImagePath_ON
											 ,BottomButtonImagePath_OFF=@BottomButtonImagePath_OFF
											 ,ButtonImagePath=@ButtonImagePath	
				WHERE HcFilterID =@HcFilterID

				SET @DuplicateFlag=0
			END
			ELSE
			BEGIN
				SET @DuplicateFlag=1	
			END 
												
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceFiltersUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
