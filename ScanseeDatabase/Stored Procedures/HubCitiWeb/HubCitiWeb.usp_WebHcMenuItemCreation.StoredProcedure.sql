USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcMenuItemCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcMenuItemCreation
Purpose					: Creating Menu Items for a hubciti.
Example					: usp_WebHcMenuItemCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Dhananjaya TR		1.0
2.0			30/09/2016		Bindu T A			2.0 - Changes for News Ticker Implimentation
2.1			27/12/2016		Bindu T A			Side Navigation Changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcMenuItemCreation]
(
   ----Input variable.   	
	  @HcMenuID Int
	, @MenuName varchar(100)
	, @Level int
	, @HCMenuItemName Varchar(max) 
	, @HCLinkTypeID Varchar(max)
	, @HCLinkID Varchar(max)	
	, @Position Varchar(max)
	, @MenuItemImgPath Varchar(Max)
	, @MenuItemIpadImagePath Varchar(Max)  -- IpadImagePath implementation 
	, @UserID INT
	, @BottomButtonID Varchar(Max)
	, @HCHubCitiID INT
	, @HCMenuBannerImage Varchar(2000)
	, @HCTemplateName Varchar(100)
	, @HcTypeFlag bit
	, @HcDepartmentFlag bit
	, @HcDepartmentName varchar(max)
	, @HcTypeName varchar(max)
	, @HcMenuItemShapeID Varchar(max)
	, @NoOfColumns int
	, @HcBusinessSubCategoryID Varchar(max)
	, @HcBandSubCategoryID VARCHAR(MAX)
	, @HcNewsSubCategoryID VARCHAR(MAX)
	, @HcCityID Varchar(max)
	, @TemplateBackgroundColor Varchar(100)
	, @GroupIds Varchar(max)
	, @TemplateBackgroundImage Varchar(2000) 
	, @DisplayLabel bit  
	, @LabelBckGndColor Varchar(250)
	, @LabelFontColor Varchar(250)
---------Hubciti Side Menu changes-------START
	, @HubCitiSideMenu bit
	, @IsDefaultHubCitiSideMenu bit
---------Hubciti Side Menu changes-------END
	, @isBannerOrTicker INT        -- NewsTicker Changes 
	, @hcNewsTickerModeID INT
	, @hcNewsTickerDirectionID VarChar(100)  -- END NewsTicker

	  
	--Output Variable 
		
	, @DuplicateMenuItems Varchar(max) output
	, @ClearCacheURL Varchar(1000) output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

			
			
			   DECLARE @HcnewsLinkID int
		 	   SELECT TOP 1 @HcnewsLinkID=HcLinkTypeID  
			   from HcLinkType WHERE LinkTypeName IN ('News')

			   ---------Hubciti Side Menu changes-------START
			   SELECT @HubCitiSideMenu = ISNULL(@HubCitiSideMenu,'0'), @IsDefaultHubCitiSideMenu = ISNULL(@IsDefaultHubCitiSideMenu,'0')
			   ---------Hubciti Side Menu changes-------END

			--To Verify Subcat contain non null value
			DECLARE @SubCatValueCheck Varchar(2000) 
			DECLARE @SubCatValueCheck1 Varchar(2000)
			DECLARE @SubCatValueCheck11 Varchar(2000)

			DECLARE @AppConfig Varchar(2000)
			DECLARE @CurrentAppVersion Varchar(2000)
			DECLARE @YetToReleaseAppVersion Varchar(2000)
			DECLARE @AdroidCurrentAppVersion Varchar(2000)
			DECLARE @AndroidYetToReleaseVersion Varchar(2000)

			select @MenuItemImgPath
			SELECT @HCMenuItemName = REPLACE(@HCMenuItemName, 'NULL', '0')
			SELECT @HCLinkTypeID = REPLACE(@HCLinkTypeID, 'NULL', '0')
			SELECT @HCLinkID = REPLACE(@HCLinkID, 'NULL', '0')
			SELECT @Position = REPLACE(@Position, 'NULL', '0')
			SELECT @MenuItemImgPath = REPLACE(@MenuItemImgPath, 'NULL', '0')
			SELECT @MenuItemIpadImagePath = REPLACE(@MenuItemIpadImagePath, 'NULL', '0')  --IpadImagePath implementation
			SELECT @BottomButtonID = REPLACE(@BottomButtonID, 'NULL', '0')
			SELECT @HcDepartmentName = REPLACE(@HcDepartmentName, 'NULL', '0')
			SELECT @HcTypeName = REPLACE(@HcTypeName, 'NULL', '0')
            SELECT @HcMenuItemShapeID  = REPLACE(@HcMenuItemShapeID, 'NULL', '0')
			SELECT @HcBusinessSubCategoryID  = REPLACE(@HcBusinessSubCategoryID, 'NULL', '0')			
			SELECT @HcCityID  = REPLACE(@HcCityID, 'NULL', '0')
			SELECT @GroupIds = REPLACE(@GroupIds, 'NULL', '0')

			SELECT @AppConfig = ScreenContent from AppConfiguration where ScreenName like 'Web Email'
			SELECT @CurrentAppVersion = ScreenContent from AppConfiguration where ScreenName like 'CurrentAppVersion'
			SELECT @YetToReleaseAppVersion = ScreenContent from AppConfiguration where ScreenName like 'Yet to Release Version'

			set @SubCatValueCheck =@HcBusinessSubCategoryID		

			Set @SubCatValueCheck =REPLACE(REPLACE(@SubCatValueCheck,'!~~!',','),'|',',')						

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , SubCatValue = Param
			INTO #SubCatValueCheck
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCatValueCheck, ',')

			set @SubCatValueCheck1 =@HcBandSubCategoryID		

			Set @SubCatValueCheck1 =REPLACE(REPLACE(@SubCatValueCheck1,'!~~!',','),'|',',')						

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , SubCatValue = Param
			INTO #SubCatValueCheck1
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCatValueCheck11, ',')

			--Split all the comma separated parameters.	
		    UPDATE HcMenu SET HCMenuBannerImage=@HCMenuBannerImage
							, HcDepartmentFlag = @HcDepartmentFlag
							, HcTypeFlag = @HcTypeFlag
							, DateCreated = GETDATE()
		    WHERE HCMenuID=@HCMenuID 	

			---------News Ticker Implimentation-----------------

			IF  ( @isBannerOrTicker = 0 )
			 BEGIN 
				UPDATE HcMenu 
				SET IsNewsTicker = 1
					,HcNewsTickerModeID = @hcNewsTickerModeID
					,HcNewsTickerDirectionID = CASE WHEN @hcNewsTickerDirectionID = 'Left' THEN 1
													WHEN @hcNewsTickerDirectionID = 'Right' THEN 2
													WHEN @hcNewsTickerDirectionID = 'Up' THEN 3
													WHEN @hcNewsTickerDirectionID = 'Down' THEN 4 
											  END
					,HCMenuBannerImage = NULL
				WHERE HcMenuID = @HcMenuID
			 END
			ELSE IF (@isBannerOrTicker = 1 )
			  BEGIN 
				UPDATE HcMenu
				SET IsNewsTicker = 0,
					HcNewsTickerModeID = NULL,
					HcNewsTickerDirectionID =NULL
				WHERE HcMenuID = @HcMenuID
			 END
			 ELSE IF (@isBannerOrTicker IS NULL )
			  BEGIN 
				UPDATE HcMenu
				SET IsNewsTicker = NULL
					,HcNewsTickerModeID = NULL
					,HcNewsTickerDirectionID =NULL
					,HCMenuBannerImage = NULL
				WHERE HcMenuID = @HcMenuID
			 END
			--------------------------------

			--------NEWS-------------
			set @SubCatValueCheck11 =@HcNewsSubCategoryID		

			Set @SubCatValueCheck11 =REPLACE(REPLACE(@SubCatValueCheck11,'!~~!',','),'|',',')						

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , SubCatValue = Param
			INTO #SubCatValueCheck11
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCatValueCheck11, ',')

			------News-----------------

			IF @HcTypeFlag = 0
			BEGIN
				SET @HcTypeName = NULL
			END
			IF @HcDepartmentFlag = 0
			BEGIN	
				SET @HcDepartmentName = NULL
			END
			

			DECLARE @ButtonPosition Varchar(max)='1!~~!2!~~!3!~~!4'
			DECLARE @RetailerBusinessCategories VARCHAR(100)
			
				
		     
		        
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HCLinkTypeID = Param
			INTO #HCLinkTypeID
			FROM dbo.fn_SplitParamMultiDelimiter(@HCLinkTypeID, '!~~!')
			
			--Error while saving templete fix---START
			create table #HCLinkID (RowNum int identity (1,1),HCLinkID varchar(max))

			IF @HCLinkID IS NOT NULL
			BEGIN
					SET @HCLinkID = REPLACE(@HCLinkID,'!~~!',';')
					insert into #HCLinkID
					SELECT splitdata
					FROM dbo.[fnSplitString_Custom](@HCLinkID,';')
			END
			ELSE 
			BEGIN
					insert into #HCLinkID
					SELECT param
					FROM dbo.fn_SplitParamMultiDelimiter(@HCLinkID,'!~~!')
			END
			--Error while saving templete fix---END
			
			--Capture all the menu items of the type Find.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HCLinkID
			INTO #FindRetBizCat
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName = 'Find'

--------------Capture all the menu items of the type BAND-----------------23rd April 2016-------
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HCLinkID
			INTO #BandRetBizCat
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName = 'Band'
---------------------------------------------------------------------------------


--------------Capture all the menu items of the type BAND-----------------23rd April 2016-------
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HCLinkID
			INTO #NewsRetBizCat
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName = 'News'
---------------------------------------------------------------------------------

			--SELECT '#FindRetBizCat',* FROM #FindRetBizCat

			--12th Jan 2016 - Filter changes
			--Capture all the menu items of the type Filters.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				  ,HCLinkID
			INTO #FiltersList
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName = 'Filters'

			--SELECT '#FiltersList'
			--select * from #FiltersList

			--Capture All bussiness Subcategoies
			
			SELECT RowNum = IDENTITY(INT, 1, 1)
					,HCSubCatID = Param
			INTO #SubCat
			FROM dbo.fn_SplitParamMultiDelimiter(@HcBusinessSubCategoryID, '!~~!') 
			
---------------Capture All BAND Subcategoies--------------------23rd April 2016----------------	
			SELECT RowNum = IDENTITY(INT, 1, 1)
					, HCSubCatID = CASE WHEN Param ='NULL' THEN NULL ELSE Param END
			INTO #BandSubCat
			FROM dbo.fn_SplitParamMultiDelimiter(@HcBandSubCategoryID, '!~~!')
------------------------------------------------------------------------------------------------



---------------Capture All News Subcategoies--------------------23rd April 2016----------------	
			SELECT RowNum = IDENTITY(INT, 1, 1)
					, HCSubCatID = CASE WHEN Param ='NULL' THEN NULL ELSE Param END
			INTO #NewsSubCat
			FROM dbo.fn_SplitParamMultiDelimiter(@HcNewsSubCategoryID, '!~~!')
------------------------------------------------------------------------------------------------

			
			--Capture all the menu items of the type Events.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HCLinkID
			INTO #EventsRetBizCat
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName = 'Events'

			--Capture all the menu items of the type Fundraisers.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HCLinkID
			INTO #FundraisersRetBizCat
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName = 'Fundraisers'


			--Remove the pipe symbols to remove the assoiations
			UPDATE #HCLinkID SET HCLinkID = NULL
			FROM #HCLinkID A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
			WHERE LinkTypeName in('Find','Events','Fundraisers','Filters','Band','News')
						
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , MenuItemName = Param
			INTO #HCMenuItemName
			FROM dbo.fn_SplitParamMultiDelimiter(@HCMenuItemName, '!~~!')
			
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , Position = Param
			INTO #Position
			FROM dbo.fn_SplitParamMultiDelimiter(@Position, '!~~!')	
				
				
				


			SET @MenuItemImgPath = REPLACE(@MenuItemImgPath,'!~~!',';')
	        SELECT RowNum = IDENTITY(INT, 1, 1)
				 , MenuItemImgPath = Param
			INTO #MenuItemImgPath
			FROM dbo.fn_SplitParamMultiDelimiter(@MenuItemImgPath, ';')



			-- New implementation
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , MenuItemIpadImagePath = Param
			INTO #MenuItemIpadImagePath
			FROM dbo.fn_SplitParamMultiDelimiter(@MenuItemIpadImagePath, '!~~!')
			
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , BottomButtonID = Param
			INTO #BottomButton
			FROM dbo.fn_SplitParamMultiDelimiter(@BottomButtonID, ',')
			
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , ButtonPosition = Param
			INTO #ButtonPosition
			FROM dbo.fn_SplitParamMultiDelimiter(@ButtonPosition, '!~~!')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , GroupIds = Param
			INTO #GroupIds
			FROM dbo.fn_SplitParamMultiDelimiter(@GroupIds, '!~~!')	

			--Phase 2 Changes related adding new template
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , MenuItemShapeId = Param
			INTO #HcMenuItemShape
			FROM dbo.fn_SplitParamMultiDelimiter(@HcMenuItemShapeID, '!~~!')

			
			--Logic to split the Comma separated dept names, insert new ones and capture the IDs.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , Param
			INTO #TempDep
			FROM dbo.fn_SplitParamMultiDelimiter(@HcDepartmentName, '!~~!') 
			
			UPDATE #TempDep 
			SET [Param] = IIF(C.LinkTypeName = 'Text','Text','Label')
			FROM #TempDep A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType C ON B.HcLinkTypeID = C.HcLinkTypeID
			WHERE C.LinkTypeName = 'Text' OR C.LinkTypeName = 'Label'
			
			INSERT INTO HcDepartments(HcDepartmentName
									 ,HcHubCitiID
									 ,DateCreated
									 ,CreatedUserID)
			SELECT DISTINCT Param
				 , @HCHubCitiID
				 , GETDATE()
				 , @UserID
			FROM #TempDep F
			LEFT JOIN HcDepartments D ON D.HcDepartmentName = F.Param AND D.HcHubCitiID = @HCHubCitiID
			WHERE D.HcDepartmentID IS NULL
			AND Param IS NOT NULL	
			AND Param <> '0'		
			AND Param <> 'Text' --Logic to accomodate the text values in the grouped template.
			AND Param <> 'Label'--Logic to accomodate the text values in the grouped template.
			
			--Logic to split the Comma separated Type names, insert new ones and capture the IDs.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , A.HcDepartmentID
				 , A.HcDepartmentName
			INTO #Departments
			FROM #TempDep F 
			LEFT JOIN HcDepartments A ON A.HcDepartmentName = F.Param AND A.HcHubCitiID = @HCHubCitiID
			ORDER BY F.RowNum ASC			
			
			--Logic for Type association for the Menu Item.
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , Param
			INTO #TempType
			FROM dbo.fn_SplitParamMultiDelimiter(@HcTypeName, '!~~!') F
			
			--select '#TempType'
			--select * from #TempType

			UPDATE #TempType 
			SET [Param] = IIF(C.LinkTypeName = 'Text','Text','Label')
			FROM #TempType A
			INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
			INNER JOIN HcLinkType C ON B.HcLinkTypeID = C.HcLinkTypeID
			WHERE C.LinkTypeName = 'Text' OR C.LinkTypeName = 'Label'
			
			INSERT INTO HcMenuItemType(HcMenuItemTypeName
									 ,HcHubCitiID
									 ,DateCreated
									 ,CreatedUserID)
			SELECT DISTINCT Param
				 , @HCHubCitiID
				 , GETDATE()
				 , @UserID
			FROM #TempType F
			LEFT JOIN HcMenuItemType D ON D.HcMenuItemTypeName = F.Param AND D.HcHubCitiID = @HCHubCitiID
			WHERE D.HcMenuItemTypeID IS NULL 
			AND Param IS NOT NULL
			AND Param <> '0'
			AND Param <> 'Text' --Logic to accomodate the text values in the grouped template.
			AND Param <> 'Label' --Logic to accomodate the text values in the grouped template.
			
			
			--select 'HcMenuItemType'
			--select * from HcMenuItemType

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , A.HcMenuItemTypeID
				 , A.HcMenuItemTypeName
			INTO #Types 
			FROM #TempType F
			LEFT JOIN HcMenuItemType A ON A.HcMenuItemTypeName = F.Param AND A.HcHubCitiID = @HCHubCitiID
			ORDER BY F.RowNum ASC

			--select '#Types'
			--select * from #Types
					
			--Update the menu name.
			UPDATE HCMenu SET MenuName = @MenuName							
			WHERE HcMenuID = @HCMenuID 	
			AND @Level IS NULL   
			
			--Update the template name.
			DECLARE @HCTemplateID INT
			SELECT @HCTemplateID = HCTemplateId
			FROM HcTemplate
			WHERE TemplateName = @HCTemplateName
			
			UPDATE HCMenu SET HcTemplateID = (SELECT HcTemplateID FROM HcTemplate WHERE TemplateName = @HCTemplateName)
				  ,NoOfColumns = @NoOfColumns
				  ,TemplateBackgroundColor = @TemplateBackgroundColor
				  ,TemplateBackgroundImage = @TemplateBackgroundImage
				  ,DisplayLabel = @DisplayLabel
				  ,LabelBackGroundColor = @LabelBckGndColor
				  ,LabelFontColor = @LabelFontColor
			WHERE HcMenuID = @HCMenuID
	
				--12th Jan 2016 - Filter changes	
				--Delete the Filtermenuitem association from HcMenuItemFilterAssociation table
				DELETE FROM HcMenuItemFilterAssociation 
				FROM HcMenuItemFilterAssociation H
				INNER JOIN HcMenuItem MI ON MI.HcMenuItemID = H.HcMenuItemID
				WHERE MI.HcMenuItemID =H.HcMenuItemID AND MI.HcMenuID = @HCMenuID
				
				--Delete the business category association for the menu if it was associated to the find.	
				DELETE FROM HcMenuFindRetailerBusinessCategories 
				FROM HcMenuFindRetailerBusinessCategories H
				INNER JOIN HCMenuItem MI ON MI.HCMenuItemID = H.HCMenuItemID
				WHERE MI.HcMenuID = @HCMenuID


---------------Delete the BAND category association for the menu if it was associated to the BAND-------23rd April 2016----	
				DELETE FROM HcMenuItemBandCategories 
				FROM HcMenuItemBandCategories H
				INNER JOIN HCMenuItem MI ON MI.HCMenuItemID = H.HCMenuItemID
				WHERE MI.HcMenuID = @HCMenuID
---------------------------------------------------------------------------------------------------------------------------



---------------Delete the News category association for the menu if it was associated to the News-------23rd April 2016----	
				DELETE FROM HcMenuItemNewsCategories 
				FROM HcMenuItemNewsCategories H
				INNER JOIN HCMenuItem MI ON MI.HCMenuItemID = H.HCMenuItemID
				WHERE MI.HcMenuID = @HCMenuID
---------------------------------------------------------------------------------------------------------------------------

				--Delete Menuitem association from HcMenuItemEventCategoryAssociation table
				DELETE FROM HcMenuItemEventCategoryAssociation
				FROM HcMenuItemEventCategoryAssociation M
				INNER JOIN HCMenuItem I ON I.HcMenuItemID =M.HcMenuItemID AND I.HcMenuID = @HCMenuID

				--Delete Menuitem association from HcMenuItemFundraisingCategoryAssociation table
				DELETE FROM HcMenuItemFundraisingCategoryAssociation
				FROM HcMenuItemFundraisingCategoryAssociation M
				INNER JOIN HCMenuItem I ON I.HcMenuItemID =M.HcMenuItemID AND I.HcMenuID = @HCMenuID

				--Delete Menuitem association from HcRegionAppMenuItemCityAssociation table
				DELETE FROM HcRegionAppMenuItemCityAssociation
				FROM HcRegionAppMenuItemCityAssociation R
				INNER JOIN HCMenuItem I ON I.HcMenuItemID =R.HcMenuItemID AND I.HcMenuID = @HCMenuID

				--Refresh the menu before saving the new set.			
				DELETE FROM HCMenuItem FROM HCMenuItem H INNER JOIN HCMenu M ON H.HcMenuID = M.HcMenuID AND M.HcMenuID = @HCMenuID
				
				--SELECT '#HCLinkID',* FROM #HCLinkID

			

				CREATE TABLE #Temp(HcMenuItemID INT, LinkID INT, HcLinkTypeID INT)
				--Creation of Menu Item for a Menu.
				INSERT INTO HCMenuItem(HcMenuID
									 , MenuItemName
									 , HcLinkTypeID
									 , LinkID
									 , Position
									 , HcMenuItemImagePath
									 , HcMenuItemIpadImagePath
									 , DateCreated
									 , CreatedUserID
									 , HcDepartmentID
									 , HcMenuItemTypeID
									 , HcMenuItemShapeID
									 , GroupIDs
									)
				OUTPUT inserted.HcMenuItemID, inserted.LinkID, inserted.HcLinkTypeID INTO #Temp(HcMenuItemID, LinkID, HcLinkTypeID)
				SELECT @HCMenuID
					 , CASE WHEN A.MenuItemName = '0' THEN NULL ELSE A.MenuItemName END
					 , CASE WHEN B.HCLinkTypeID = '0' THEN NULL ELSE B.HCLinkTypeID END
					 , CASE WHEN C.HCLinkID = '0' THEN NULL
							WHEN E.LinkTypeName = 'City Experience' THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HCHubCitiID)
							ELSE C.HCLinkID 
					   END
					 , CASE WHEN D.Position = '0' THEN NULL ELSE D.Position END
					 , CASE WHEN MI.MenuItemImgPath = '0' THEN NULL ELSE MI.MenuItemImgPath END
					 , CASE WHEN MII.MenuItemIpadImagePath = '0' THEN NULL ELSE MII.MenuItemIpadImagePath END
					 , GETDATE()
					 , @UserID
					 , CASE WHEN F.HcDepartmentName = '0' THEN NULL ELSE F.HcDepartmentID END 
					 , CASE WHEN T.HcMenuItemTypeName = '0' THEN NULL ELSE T.HcMenuItemTypeID END
					 , CASE WHEN S.MenuItemShapeID = '0' THEN NULL ELSE S.MenuItemShapeID END
					,  CASE WHEN G.GroupIds = '0' THEN NULL ELSE G.GroupIds END
				FROM #HCMenuItemName A
				INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
				INNER JOIN #HCLinkID C ON C.RowNum = B.RowNum
				INNER JOIN #Position D ON D.RowNum = C.RowNum
				INNER JOIN #MenuItemImgPath MI ON MI.RowNum =D.RowNum
				INNER JOIN #MenuItemIpadImagePath MII ON MII.RowNum =D.RowNum
				LEFT JOIN #Departments F ON F.RowNum = MI.RowNum
				LEFT JOIN #Types T ON T.RowNum = A.RowNum
				LEFT JOIN HcLinkType E ON E.HcLinkTypeID = B.HcLinkTypeID
				LEFT JOIN #HcMenuItemShape S ON S.RowNum =A.RowNum 
				LEFT JOIN #GroupIds G ON G.RowNum = A.RowNum



--------------------------Band--------------23rd April 2016--------------------------------

				SELECT RowNum = IDENTITY(INT, 1, 1)
					 , T.HcMenuItemID
				INTO #Band
				FROM #Temp T
				INNER JOIN HcLinkType H ON H.HcLinkTypeID = T.HcLinkTypeID
				WHERE LinkTypeName = 'Band'
				AND T.LinkID IS NULL
												
				SELECT A.RowNum
					 , A.HcMenuItemID
					 , B.HCLinkID
				INTO #BC
				FROM #Band A
				INNER JOIN #BandRetBizCat B ON A.RowNum = B.RowNum 
				
				
				CREATE Table #BandCatRec(Rownum int identity(1,1)
									,HcMenuItemID Int
									,CatID INT )
				SELECT RowNum = IDENTITY(INT, 1, 1)
						, HcMenuItemID
						, HCLinkID
				INTO #BandCATRE
				FROM #BC   
				
				IF CURSOR_STATUS('global','BandCategoryBiz')>=-1
				BEGIN
					DEALLOCATE BandCategoryBiz
				END

				DECLARE @HcMenuItemID11 int
				DECLARE @Link11 varchar(1000)
				DECLARE BandCategoryBiz CURSOR STATIC FOR
				SELECT HcMenuItemID
						, HCLinkID --Pipe separated
				FROM #BandCATRE
				
				OPEN BandCategoryBiz
				
				FETCH NEXT FROM BandCategoryBiz INTO @HcMenuItemID11, @Link11
				
				
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO #BandCatRec(HcMenuItemID
										   ,CatID)
				
					SELECT @HcMenuItemID11
							, [Param]
						 
					FROM dbo.fn_SplitParam(@Link11, '|')
					
					DECLARE @Cnt11 INT 
					SELECT @Cnt11 = 0
					SELECT @Cnt11 = COUNT(Param)
					FROM dbo.fn_SplitParam(@Link11, '|')									
					
									
					FETCH NEXT FROM BandCategoryBiz INTO @HcMenuItemID11, @Link11
					
				END
				
				--Deallocate the memory allocated after the execution.
				CLOSE BandCategoryBiz
				DEALLOCATE BandCategoryBiz


				SELECT DISTINCT T.Rownum 
								,T.HcMenuItemID 
								,T.CatID 
								,CASE WHEN S.HCSubCatID='0' THEN null ELSE S.HCSubCatID END HCSubCatID
				INTO #BandCatCombination
				From #BandCatRec T
				INNER JOIN #BandSubCat S ON S.RowNum =T.Rownum 
				ORDER BY T.Rownum 
									                                

				IF CURSOR_STATUS('global','BandSubCategoryUpdate')>=-1
				BEGIN
					DEALLOCATE BandSubCategoryUpdate
				END

				DECLARE @BandCatListID int
				DECLARE @MenuItemBandListID Int
				DECLARE @BandSubcateGories varchar(1000)

				DECLARE BandSubCategoryUpdate CURSOR STATIC FOR
				SELECT	 HcMenuItemID 
						,CatID 
						,HCSubCatID 
				FROM #BandCatCombination

				OPEN BandSubCategoryUpdate

				FETCH NEXT FROM BandSubCategoryUpdate INTO @MenuItemBandListID,@BandCatListID,@BandSubcateGories
				WHILE @@FETCH_STATUS =0
				BEGIN															
																												

				INSERT INTO HcMenuItemBandCategories(HcMenuItemID
													,BandCategoryID
													,DateCreated
													,CreatedUserID
													,BandSubCategoryID)
				SELECT    @MenuItemBandListID
						, @BandCatListID 
						, GETDATE()									
						, @UserID
						, [Param]
				FROM dbo.fn_SplitParam(@BandSubcateGories, '|')															

				DECLARE @BandCatCnt INT 
				SELECT @BandCatCnt = 0
				SELECT @BandCatCnt = COUNT(Param)
				FROM dbo.fn_SplitParam(@BandSubcateGories, '|')

									
								
				FETCH NEXT FROM BandSubCategoryUpdate INTO @MenuItemBandListID,@BandCatListID,@BandSubcateGories
					
			END
				
			--Deallocate the memory allocated after the execution.
			CLOSE BandSubCategoryUpdate
			DEALLOCATE BandSubCategoryUpdate
	
-----------------------------------------------------------------------------------



--------------------------News--------------23rd April 2016--------------------------------

				SELECT RowNum = IDENTITY(INT, 1, 1)
					 , T.HcMenuItemID
				INTO #News
				FROM #Temp T
				INNER JOIN HcLinkType H ON H.HcLinkTypeID = T.HcLinkTypeID
				WHERE LinkTypeName = 'News'
				AND T.LinkID IS NULL
												
				SELECT A.RowNum
					 , A.HcMenuItemID
					 , B.HCLinkID
				INTO #BCNews
				FROM #News A
				INNER JOIN #NewsRetBizCat B ON A.RowNum = B.RowNum 
				
				
				CREATE Table #NewsCatRec(Rownum int identity(1,1)
									,HcMenuItemID Int
									,CatID INT )
				SELECT RowNum = IDENTITY(INT, 1, 1)
						, HcMenuItemID
						, HCLinkID
				INTO #NewsCATRE
				FROM #BCNews   
				
				IF CURSOR_STATUS('global','NewsCategoryBiz')>=-1
				BEGIN
					DEALLOCATE BandCategoryBiz
				END

				DECLARE @HcMenuItemID111 int
				DECLARE @Link111 varchar(1000)
				DECLARE NewsCategoryBiz CURSOR STATIC FOR
				SELECT HcMenuItemID
						, HCLinkID --Pipe separated
				FROM #NewsCATRE
				
				OPEN NewsCategoryBiz
				
				FETCH NEXT FROM NewsCategoryBiz INTO @HcMenuItemID111, @Link111
				
				
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO #NewsCatRec(HcMenuItemID
										   ,CatID)
				
					SELECT @HcMenuItemID111
							, [Param]
						 
					FROM dbo.fn_SplitParam(@Link111, '|')
					
					DECLARE @Cnt111 INT 
					SELECT @Cnt111 = 0
					SELECT @Cnt111 = COUNT(Param)
					FROM dbo.fn_SplitParam(@Link111, '|')									
					
									
					FETCH NEXT FROM NewsCategoryBiz INTO @HcMenuItemID111, @Link111
					
				END
				
				--Deallocate the memory allocated after the execution.
				CLOSE NewsCategoryBiz
				DEALLOCATE NewsCategoryBiz


				SELECT DISTINCT T.Rownum 
								,T.HcMenuItemID 
								,T.CatID 
								,CASE WHEN S.HcSubCatID='0' THEN null ELSE S.HCSubCatID END HCSubCatID
				INTO #NewsCatCombination
				From #NewsCatRec T
				INNER JOIN #NewsSubCat S ON S.RowNum =T.Rownum 
				ORDER BY T.Rownum 
									                                

				IF CURSOR_STATUS('global','NewsSubCategoryUpdate')>=-1
				BEGIN
					DEALLOCATE NewsSubCategoryUpdate
				END

				DECLARE @NewsCatListID int
				DECLARE @MenuItemNewsListID Int
				DECLARE @NewsSubcateGories varchar(1000)

				DECLARE NewsSubCategoryUpdate CURSOR STATIC FOR
				SELECT	 HcMenuItemID 
						,CatID 
						,HCSubCatID 
				FROM #NewsCatCombination

				OPEN NewsSubCategoryUpdate

				FETCH NEXT FROM NewsSubCategoryUpdate INTO @MenuItemNewsListID,@NewsCatListID,@NewsSubcateGories
				WHILE @@FETCH_STATUS =0
				BEGIN															
																												

				INSERT INTO HcMenuItemNewsCategories(HcMenuItemID
													,NewsCategoryID
													,DateCreated
													,CreatedUserID
													,NewsSubCategoryID)
				SELECT    @MenuItemNewsListID
						, @NewsCatListID 
						, GETDATE()									
						, @UserID
						, [Param]
				FROM dbo.fn_SplitParam(@NewsSubcateGories, '|')															

				DECLARE @NewsCatCnt INT 
				SELECT @NewsCatCnt = 0
				SELECT @NewsCatCnt = COUNT(Param)
				FROM dbo.fn_SplitParam(@NewsSubcateGories, '|')

									
								
				FETCH NEXT FROM NewsSubCategoryUpdate INTO @MenuItemNewsListID,@NewsCatListID,@NewsSubcateGories
					
			END
				
			--Deallocate the memory allocated after the execution.
			CLOSE NewsSubCategoryUpdate
			DEALLOCATE NewsSubCategoryUpdate
	
-----------------------------------------------------------------------------------



				--select '#Temp'
				--select * from #Temp

				SELECT RowNum = IDENTITY(INT, 1, 1)
					 , T.HcMenuItemID
				INTO #Find
				FROM #Temp T
				INNER JOIN HcLinkType H ON H.HcLinkTypeID = T.HcLinkTypeID
				WHERE LinkTypeName = 'Find'
				AND T.LinkID IS NULL
												
				SELECT A.RowNum
					 , A.HcMenuItemID
					 , B.HCLinkID
				INTO #RBC
				FROM #Find A
				INNER JOIN #FindRetBizCat B ON A.RowNum = B.RowNum 		

			
				----RetaiklerBusiness Subcategory

				CREATE Table #CatRec(Rownum int identity(1,1)
									,HcMenuItemID Int
									,CatID INT )


				SELECT RowNum = IDENTITY(INT, 1, 1)
						, HcMenuItemID
						, HCLinkID
				INTO #CATRE
				FROM #RBC   
							
				IF CURSOR_STATUS('global','CategoryBiz')>=-1
				BEGIN
					DEALLOCATE CategoryBiz
				END

				DECLARE @HcMenuItemID1 int
				DECLARE @Link1 varchar(1000)
				DECLARE CategoryBiz CURSOR STATIC FOR
				SELECT HcMenuItemID
						, HCLinkID --Pipe separated
				FROM #CATRE
				
				OPEN CategoryBiz
				
				FETCH NEXT FROM CategoryBiz INTO @HcMenuItemID1, @Link1
				
				--CREATE TABLE #TempBiz(BusinessCategoryID INT)
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO #CatRec(HcMenuItemID
										,CatID)
					--OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinessCategoryID)
					SELECT @HcMenuItemID1
							, [Param]
						 
					FROM dbo.fn_SplitParam(@Link1, '|')
					
					DECLARE @Cnt1 INT 
					SELECT @Cnt1 = 0
					SELECT @Cnt1 = COUNT(Param)
					FROM dbo.fn_SplitParam(@Link1, '|')									
					
									
					FETCH NEXT FROM CategoryBiz INTO @HcMenuItemID1, @Link1
					
				END
				
				--Deallocate the memory allocated after the execution.
				CLOSE CategoryBiz
				DEALLOCATE CategoryBiz


				SELECT DISTINCT T.Rownum 
						, T.HcMenuItemID 
						,T.CatID 
						,CASE WHEN S.HCSubCatID='0' THEN null ELSE S.HCSubCatID END  HCSubCatID
				INTO #CatCombination
				From #CatRec T
				INNER JOIN #SubCat S ON S.RowNum =T.Rownum 
				ORDER BY T.Rownum 
									                                

							IF CURSOR_STATUS('global','SubCategoryUpdate')>=-1
							BEGIN
								DEALLOCATE SubCategoryUpdate
							END

							DECLARE @CatListID int
							DECLARE @MenuItemListID Int
							DECLARE @SubcateGories varchar(1000)

							DECLARE SubCategoryUpdate CURSOR STATIC FOR
							SELECT HcMenuItemID 
									,CatID 
									,HCSubCatID 
							FROM #CatCombination

							OPEN SubCategoryUpdate

							FETCH NEXT FROM SubCategoryUpdate INTO @MenuItemListID,@CatListID,@SubcateGories
							WHILE @@FETCH_STATUS =0
							BEGIN															
																												

							INSERT INTO HcMenuFindRetailerBusinessCategories(HcMenuItemID
																			,BusinessCategoryID
																			,DateCreated
																			,CreatedUserID
																			,HcBusinessSubCategoryID)
							SELECT @MenuItemListID
									, @CatListID 
									, GETDATE()									
									, @UserID
									, [Param]
							FROM dbo.fn_SplitParam(@SubcateGories, '|')															

							DECLARE @CatCnt INT 
							SELECT @CatCnt = 0
							SELECT @CatCnt = COUNT(Param)
							FROM dbo.fn_SplitParam(@SubcateGories, '|')

									
								
							FETCH NEXT FROM SubCategoryUpdate INTO @MenuItemListID,@CatListID,@SubcateGories
					
						END
				
						--Deallocate the memory allocated after the execution.
						CLOSE SubCategoryUpdate
						DEALLOCATE SubCategoryUpdate


            IF 1=2       
			BEGIN 

				--Logic to associate the pipe separated business categories to menu item.
				IF CURSOR_STATUS('global','RetBizCat')>=-1
				BEGIN
					DEALLOCATE RetBizCat
				END

				DECLARE @HcMenuItemID int
				DECLARE @Link varchar(1000)
				DECLARE RetBizCat CURSOR STATIC FOR
				SELECT HcMenuItemID
						, HCLinkID --Pipe separated
				FROM #RBC
				
				OPEN RetBizCat
				
				FETCH NEXT FROM RetBizCat INTO @HcMenuItemID, @Link
				
				CREATE TABLE #TempBiz(BusinessCategoryID INT)
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO HcMenuFindRetailerBusinessCategories(HcMenuItemID
																	,BusinessCategoryID
																	,DateCreated
																	,CreatedUserID)
					OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinessCategoryID)
					SELECT @HcMenuItemID
							, [Param]
							, GETDATE()
							, @UserID
					FROM dbo.fn_SplitParam(@Link, '|')
					
					DECLARE @Cnt INT 
					SELECT @Cnt = 0
					SELECT @Cnt = COUNT(Param)
					FROM dbo.fn_SplitParam(@Link, '|')
										
					
					TRUNCATE TABLE #TempBiz
					FETCH NEXT FROM RetBizCat INTO @HcMenuItemID, @Link
					
				END
				
					--Deallocate the memory allocated after the execution.
					CLOSE RetBizCat
					DEALLOCATE RetBizCat
				
			   END

---------------------------------------------------BAND----------------------------------------23rdApril2016
			   IF 1=2
			   BEGIN
			   	--Logic to associate the pipe separated BAND categories to menu item.
				IF CURSOR_STATUS('global','BandBizCat')>=-1
				BEGIN
					DEALLOCATE BandBizCat
				END

				DECLARE @HcBandMenuItemID int
				DECLARE @BandLink varchar(1000)
				DECLARE BandBizCat CURSOR STATIC FOR
				SELECT HcMenuItemID
						, HCLinkID --Pipe separated
				FROM #BC
				
				OPEN BandBizCat
				
				FETCH NEXT FROM BandBizCat INTO @HcMenuItemID, @Link
				
				CREATE TABLE #TempBandBiz(BandCategoryID INT)
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO HcMenuItemBandCategories(HcMenuItemID
																	,BandCategoryID
																	,DateCreated
																	,CreatedUserID)
					OUTPUT inserted.BandCategoryID INTO #TempBandBiz(BandCategoryID)
					SELECT @HcBandMenuItemID
							, [Param]
							, GETDATE()
							, @UserID
					FROM dbo.fn_SplitParam(@BandLink, '|')
					
					DECLARE @BandCnt1 INT 
					SELECT @BandCnt1 = 0
					SELECT @BandCnt1 = COUNT(Param)
					FROM dbo.fn_SplitParam(@BandLink, '|')
										
					
					TRUNCATE TABLE #TempBandBiz
					FETCH NEXT FROM BandBizCat INTO @HcBandMenuItemID, @BandLink


				END
				
					--Deallocate the memory allocated after the execution.
					CLOSE BandBizCat
					DEALLOCATE BandBizCat
				
			   END

---------------------------------------------------------------------------------------------------------



---------------------------------------------------NEWS----------------------------------------23rdApril2016
			   IF 1=2
			   BEGIN
			   	--Logic to associate the pipe separated BAND categories to menu item.
				IF CURSOR_STATUS('global','NewsBizCat')>=-1
				BEGIN
					DEALLOCATE NewsBizCat
				END

				DECLARE @HcNewsMenuItemID int
				DECLARE @NewsLink varchar(1000)
				DECLARE NewsBizCat CURSOR STATIC FOR
				SELECT HcMenuItemID
						, HCLinkID --Pipe separated
				FROM #BCNews
				
				OPEN NewsBizCat
				
				FETCH NEXT FROM NewsBizCat INTO @HcMenuItemID1, @Link1
				
				CREATE TABLE #TempNewsBiz(NewsCategoryID INT)
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO HcMenuItemNewsCategories(HcMenuItemID
																	,NewsCategoryID
																	,DateCreated
																	,CreatedUserID)
					OUTPUT inserted.NewsCategoryID INTO #TempNewsBiz(NewsCategoryID)
					SELECT @HcNewsMenuItemID
							, [Param]
							, GETDATE()
							, @UserID
					FROM dbo.fn_SplitParam(@NewsLink, '|')
					
					DECLARE @NewsCnt1 INT 
					SELECT @NewsCnt1 = 0
					SELECT @NewsCnt1 = COUNT(Param)
					FROM dbo.fn_SplitParam(@NewsLink, '|')
										
					
					TRUNCATE TABLE #TempNewsBiz
					FETCH NEXT FROM NewsBizCat INTO @HcNewsMenuItemID, @NewsLink


				END
				
					--Deallocate the memory allocated after the execution.
					CLOSE NewsBizCat
					DEALLOCATE NewsBizCat
				
			   END

---------------------------------------------------------------------------------------------------------

						
				
				-----Events Start

				SELECT RowNum = IDENTITY(INT, 1, 1)
					 , T.HcMenuItemID
				INTO #Events
				FROM #Temp T
				INNER JOIN HcLinkType H ON H.HcLinkTypeID = T.HcLinkTypeID
				WHERE LinkTypeName = 'Events'
				AND T.LinkID IS NULL
				
												
				SELECT A.RowNum
					 , A.HcMenuItemID
					 , B.HCLinkID
				INTO #ERBC
				FROM #Events A
				INNER JOIN #EventsRetBizCat B ON A.RowNum = B.RowNum 		
				
				--Logic to associate the pipe separated business categories to menu item.	
				IF CURSOR_STATUS('global','EventsRetBizCat')>=-1
				BEGIN
				 DEALLOCATE EventsRetBizCat
				END
							
				DECLARE @HcEventsMenuItemID int
				DECLARE @EventLink varchar(1000)
				DECLARE EventsRetBizCat CURSOR STATIC FOR
				SELECT HcMenuItemID
					 , HCLinkID --Pipe separated
				FROM #ERBC
				
				OPEN EventsRetBizCat
				
				FETCH NEXT FROM EventsRetBizCat INTO @HcEventsMenuItemID, @EventLink
				
				CREATE TABLE #TempEventBiz(HcEventCategoryID INT)
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO HcMenuItemEventCategoryAssociation(HcHubCitiID
																  ,HcMenuItemID
																  ,HcEventCategoryID
																  ,DateCreated
																  ,CreatedUserID)
					OUTPUT inserted.HcEventCategoryID INTO #TempEventBiz(HcEventCategoryID)
					SELECT @HCHubCitiID 
					     , @HcEventsMenuItemID
						 , [Param]
						 , GETDATE()
						 , @UserID
					FROM dbo.fn_SplitParam(@EventLink, '|')
					
					DECLARE @EventCnt INT 
					SELECT @EventCnt = 0
					SELECT @EventCnt = COUNT(Param)
					FROM dbo.fn_SplitParam(@EventLink, '|')			
					
					
					TRUNCATE TABLE #TempEventBiz
					FETCH NEXT FROM EventsRetBizCat INTO @HcEventsMenuItemID, @EventLink
					
				END
				
				--Deallocate the memory allocated after the execution.
				CLOSE EventsRetBizCat
				DEALLOCATE EventsRetBizCat


				-----Events End

				-----Fundraisers Start

				SELECT RowNum = IDENTITY(INT, 1, 1)
					 , T.HcMenuItemID
				INTO #Fundraisers
				FROM #Temp T
				INNER JOIN HcLinkType H ON H.HcLinkTypeID = T.HcLinkTypeID
				WHERE LinkTypeName = 'Fundraisers'
				AND T.LinkID IS NULL


												
				SELECT A.RowNum
					 , A.HcMenuItemID
					 , B.HCLinkID
				INTO #FinalFund
				FROM #Fundraisers A
				INNER JOIN #FundraisersRetBizCat B ON A.RowNum = B.RowNum 		
				
				--Logic to associate the pipe separated business categories to menu item.	
				IF CURSOR_STATUS('global','FundraisersRetBizCat')>=-1
				BEGIN
				 DEALLOCATE FundraisersRetBizCat
				END
							
				DECLARE @HcFundraisersMenuItemID int
				DECLARE @FundraiserLink varchar(1000)
				DECLARE FundraisersRetBizCat CURSOR STATIC FOR
				SELECT HcMenuItemID
					 , HCLinkID --Pipe separated
				FROM #FinalFund
				
				OPEN FundraisersRetBizCat
				
				FETCH NEXT FROM FundraisersRetBizCat INTO @HcFundraisersMenuItemID, @FundraiserLink
				
				CREATE TABLE #TempFundraiserBiz(HcFundraisingCategoryID INT)
				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO HcMenuItemFundraisingCategoryAssociation(HcHubCitiID
																  ,HcMenuItemID
																  ,HcFundraisingCategoryID
																  ,DateCreated
																  ,CreatedUserID)
					OUTPUT inserted.HcFundraisingCategoryID INTO #TempFundraiserBiz(HcFundraisingCategoryID)
					SELECT @HCHubCitiID 
					     , @HcFundraisersMenuItemID
						 , [Param]
						 , GETDATE()
						 , @UserID
					FROM dbo.fn_SplitParam(@FundraiserLink, '|')
					
					DECLARE @FundraiserCnt INT 
					SELECT @FundraiserCnt = 0
					SELECT @FundraiserCnt = COUNT(Param)
					FROM dbo.fn_SplitParam(@FundraiserLink, '|')			
					
					
					TRUNCATE TABLE #TempFundraiserBiz
					FETCH NEXT FROM FundraisersRetBizCat INTO @HcFundraisersMenuItemID, @FundraiserLink
					
				END
				
				--Deallocate the memory allocated after the execution.
				CLOSE FundraisersRetBizCat
				DEALLOCATE FundraisersRetBizCat


				-----Fundraisers End

				--12th Jan 2016 - Filter changes
				--Filters Start

				-- SELECT RowNum = IDENTITY(INT, 1, 1)
				--		, HCLinkID
				-- INTO #FiltersList
				-- FROM #HCLinkID A
				-- INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum
				-- INNER JOIN HcLinkType L ON L.HcLinkTypeID = B.HCLinkTypeID
				-- WHERE LinkTypeName = 'Filters'

				--To collect Menuitems of Filter
				SELECT RowNum = IDENTITY(INT, 1, 1)
					  ,T.HcMenuItemID
				INTO #Filters
				FROM #Temp T
				INNER JOIN HcLinkType H ON H.HcLinkTypeID = T.HcLinkTypeID
				WHERE LinkTypeName = 'Filters'
				AND T.LinkID IS NULL

				--SELECT 'Cursor - Filters'	
				--SELECT * FROM #Filters	
								
				SELECT A.RowNum
					 , A.HcMenuItemID
					 , B.HCLinkID
				INTO #FList
				FROM #Filters A
				INNER JOIN #FiltersList B ON A.RowNum = B.RowNum 		
				
				--select 'FList'
				--select * from #FList
				--Logic to associate the pipe separated FilterID's to menu item.	
				IF CURSOR_STATUS('global','FiltersList')>=-1
				BEGIN
				 DEALLOCATE FiltersList
				END
							
				DECLARE @HcFilterMenuItemID int
				DECLARE @FilterLink varchar(1000)
				DECLARE FiltersList CURSOR STATIC FOR
				
				SELECT HcMenuItemID
					 , HCLinkID --Pipe separated
				FROM #FList
				
				OPEN FiltersList
				
				FETCH NEXT FROM FiltersList INTO @HcFilterMenuItemID, @FilterLink
				
				CREATE TABLE #TempFiltersList(HcFilterID INT)

				--Check if there are rows present in the cursor
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO HcMenuItemFilterAssociation (HcHubCitiID
															,HcMenuItemID
															,HcFilterID
															,DateCreated
															,CreatedUserID)
					OUTPUT inserted.HcFilterID INTO #TempFiltersList(HcFilterID)
					SELECT @HCHubCitiID 
					     , @HcFilterMenuItemID
						 , [Param]
						 , GETDATE()
						 , @UserID
					FROM dbo.fn_SplitParam(@FilterLink, '|')
					
					--select 'HcMenuItemFilterAssociation'
					--select  *From HcMenuItemFilterAssociation
					
					DECLARE @FilterCnt INT 
					SELECT @FilterCnt = 0
					SELECT @FilterCnt = COUNT(Param)
					FROM dbo.fn_SplitParam(@FilterLink, '|')			
					
					
					TRUNCATE TABLE #TempFiltersList
					FETCH NEXT FROM FiltersList INTO @HcFilterMenuItemID, @FilterLink
					
				END
				
				--Deallocate the memory allocated after the execution.
				CLOSE FiltersList
				DEALLOCATE FiltersList

				--12th Jan 2016 - Filter changes 
				-----Filters End

				
				print 'b'
				DELETE FROM HcMenuBottomButton 
				FROM HcMenuBottomButton H 
				INNER JOIN HcBottomButton C ON C.HcBottomButtonID = H.HcBottomButtonID
				INNER JOIN HcMenu M ON M.HcMenuID = H.HcMenuID
				WHERE M.HcHubCitiID = @HCHubCitiID AND ((@HCMenuID =0 AND Level =1) OR H.HcMenuID =@HCMenuID)
				
				INSERT INTO HcMenuBottomButton(HcMenuID
											  ,HcBottomButtonID
											  ,DateCreated
											  ,CreatedUserID
											  ,Position)
									SELECT @HCMenuID
										 , BottomButtonID
										 , GETDATE()
										 , @UserID
										 , ButtonPosition
									FROM #BottomButton BN
									INNER JOIN #ButtonPosition B ON BN.RowNum = B.RowNum
			--END
				        
	        --Region App related Changes.

			--Capture all the Citis of the Menuitems.
			
			SET @HcCityID = REPLACE(@HcCityID,'!~~!',';')
			
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , CityID = Param
			INTO #CommaSeparatedCityList
			FROM dbo.fn_SplitParamMultiDelimiter(@HcCityID, ';') 			

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , T.HcMenuItemID 
				 , T.HcLinkTypeID 
			INTO #CityList
			FROM #Temp T
			INNER JOIN HcMenuItem M ON M.HcMenuItemID =T.HcMenuItemID
			INNER JOIN HcLinkType L ON L.HcLinkTypeID =T.HcLinkTypeID 
			--WHERE LinkTypeName NOT IN ('Text','Label')
			ORDER BY T.HcMenuItemID ASC			

			SELECT Rownum=IDENTITY(INT,1,1)
			      ,C.HcMenuItemID 
				  ,T.CityID 
			INTO #CityCombwithMenuitem
			FROM #CityList C
			INNER JOIN #CommaSeparatedCityList T ON C.RowNum =T.RowNum 
			

            --Region App With City Implementation

			IF CURSOR_STATUS('GLOBAL','CitywithMenuitem')>=-1
			BEGIN
			DEALLOCATE CitywithMenuitem
			END

			DECLARE @CityMenuItemID INT
			DECLARE @CityComblists Varchar(1000)

			DECLARE CitywithMenuitem CURSOR STATIC FOR

			SELECT HcMenuItemID 
			      ,CityID 
			FROM #CityCombwithMenuitem 

			OPEN CitywithMenuitem 

			FETCH NEXT FROM CitywithMenuitem INTO @CityMenuItemID,@CityComblists

			WHILE(@@FETCH_STATUS=0)
			BEGIN
			INSERT INTO HcRegionAppMenuItemCityAssociation(HcHubcitiID
														  ,HcMenuItemID
														  ,HcCityID
														  ,CreatedUserID
														  ,DateCreated)

			 SELECT @HCHubCitiID 
			       ,@CityMenuItemID 
				   ,IIF(Param = 0,NULL,Param)
				   ,@UserID 
				   ,GETDATE()
			 FROM fn_SplitParam(@CityComblists,'|')

			 FETCH NEXT FROM CitywithMenuitem INTO @CityMenuItemID,@CityComblists
			 
			END

			CLOSE  CitywithMenuitem
			DEALLOCATE CitywithMenuitem

--------------------------------NEWS TEMPLATE 5/13/2016------------------------------------------------------------

IF  EXISTS(SELECT 1 FROM HcTemplate WHERE HcTemplateID= @HCTemplateID AND IsNews=1)
BEGIN
	UPDATE HcMenu 
	SET IsAssociateNews=1
	WHERE Hcmenuid = @HCMenuID
END
else 
UPDATE HcMenu 
	SET IsAssociateNews=0
	WHERE Hcmenuid = @HCMenuID

-------------------------------------------------------------------------------------------------------------------



------------------------------------START -------------------HubCiti Side menu 23rd Aug 2016---------------------------
			
			UPDATE HcMenu 
			SET HubCitiSideMenu = @HubCitiSideMenu
			WHERE HcMenuID = @HCMenuID

			IF @IsDefaultHubCitiSideMenu = 1 
			BEGIN
					UPDATE HcMenu 
					SET IsDefaultHubCitiSideMenu = 0
					WHERE HcHubCitiID = @HCHubCitiID AND @HubCitiSideMenu = 1

					UPDATE HcMenu 
					SET IsDefaultHubCitiSideMenu = 1
					WHERE HcMenuID = @HCMenuID AND HcHubCitiID = @HCHubCitiID AND @HubCitiSideMenu = 1
			END  
			ELSE IF @IsDefaultHubCitiSideMenu = 0 OR @IsDefaultHubCitiSideMenu IS NULL
			BEGIN
					UPDATE HcMenu 
					SET IsDefaultHubCitiSideMenu = 0
					WHERE HcMenuID = @HCMenuID AND HcHubCitiID = @HCHubCitiID AND @HubCitiSideMenu = 1
			END

------------------------------------END----------------------HubCiti Side menu 23rd Aug 2016---------------------------

UPDATE HcUserNewsFirstSingleSideNavigation
SET IsSideNavigationChanged = 1
WHERE HchubcitiID= @HCHubCitiID

DELETE FROM HcUserNewsFirstSingleSideNavigation WHERE HchubcitiID= @HCHubCitiID

---------------------------------------------MAIN MENU CACHING CHANGES--------------------------------------------------------------------------------------------
		--HUBCITI Clear Cache
		IF @HcCityID is null
		BEGIN

		DECLARE @ClearCacheURL1 varchar(1000),@ClearCacheURL2 varchar(1000),@ClearCacheURL3 varchar(1000)
		SELECT @ClearCacheURL1= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema3'
		SELECT @ClearCacheURL2= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema2'
		SELECT @ClearCacheURL3= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema1'
		SET @ClearCacheURL = @ClearCacheURL1 +','+ @ClearCacheURL2 +','+ @ClearCacheURL3

		END

		ELSE IF @HcCityID is NOT null
		BEGIN

		DECLARE @ClearCacheURL11 varchar(1000),@ClearCacheURL22 varchar(1000),@ClearCacheURL33 varchar(1000)
		SELECT @ClearCacheURL11= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema3'
		SELECT @ClearCacheURL22= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema2'
		SELECT @ClearCacheURL33= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema1'
		SET @ClearCacheURL = @ClearCacheURL11 +','+ @ClearCacheURL22 +','+ @ClearCacheURL33

		END


	------------------------------------------------MAIN MENU CACHING CHANGES--------------------------------------------------------------------------------------------------------------------	   
	

if exists( select 1  from #HCMenuItemName A INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum where HCLinkTypeID= @HcnewsLinkID)
begin




insert  into HcDefaultNewsFirstBookmarkedCategories
(NewsCategoryID,NewsCategoryDisplayValue,HchubcitiID,SortOrder,DateCreated,DateModified,CreatedBy,ModifiedBy,Active,
CategoryColor,
Issubmenu)
--isnull(cat.NewsCategoryName,NewsLinkCategoryName)
select distinct cat.NewsCategoryID,cat.NewsCategoryName, @HCHubCitiID,null,getdate(),null,1,1,1,'#fffff',1 
from  #HCMenuItemName A
inner join NewsFirstCategory cat on cat.NewsCategoryName=MenuItemName
LEFT JOIN NewsFirstCatSubCatAssociation S ON S.CategoryID = Cat.NewsCategoryID AND S.hchubcitiID = @HCHubCitiID
INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum where HCLinkTypeID= @HcnewsLinkID
and not exists (select MenuItemName  
from HcDefaultNewsFirstBookmarkedCategories def where A.MenuItemName=isnull(def.NewsCategoryDisplayValue,NewsLinkCategoryName)
and def.HchubcitiID=@HCHubCitiID)

delete  cat 
from HcDefaultNewsFirstBookmarkedCategories   cat
 inner join #HCMenuItemName  A on A.MenuItemName=ISNULL(NewsCategoryDisplayValue,NewsLinkCategoryName)
INNER JOIN #HCLinkTypeID B ON A.RowNum = B.RowNum where HCLinkTypeID= @HcnewsLinkID
AND  isnull(Issubmenu,0)<>1 and HchubcitiID=@HCHubCitiID

select row_number() over(order  by  HcDefaultBookmarkedCategoriesID asc) as row,HcDefaultBookmarkedCategoriesID into #update
  from HcDefaultNewsFirstBookmarkedCategories  cat 
where  HchubcitiID=@HCHubCitiID

update  c set sortorder=row from  HcDefaultNewsFirstBookmarkedCategories  c inner join #update on #update.HcDefaultBookmarkedCategoriesID=c.HcDefaultBookmarkedCategoriesID

end

update a set NewsSideNavigationID=b.HcMenuItemID  from HcUserNewsFirstSideNavigation a  inner join HcMenuItem b on b.MenuItemName=a.NewsSideNavigationDisplayValue
inner join  HcMenu  on HcMenu.HcMenuID=b.HcMenuID
where a.Flag=1 and a.NewsSideNavigationID<>b.HcMenuItemID
and   a.HchubcitiID=@HCHubCitiID and HcMenu.HchubcitiID=@HCHubCitiID
AND HcMenu.HcMenuID= @HcMenuID




	       --Confirmation of Success.
		   SELECT @Status = 0


COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	

		THROW;

		    Select ERROR_LINE()
			IF (SELECT CURSOR_STATUS('global','RetBizCat')) >=0 
			BEGIN
				DEALLOCATE RetBizCat
			END

			IF (SELECT CURSOR_STATUS('global','BandBizCat')) >=0 
			BEGIN
				DEALLOCATE BandBizCat
			END


			IF (SELECT CURSOR_STATUS('global','NewsBizCat')) >=0 
			BEGIN
				DEALLOCATE NewsBizCat
			END

			
			PRINT 'Error occured in Stored Procedure usp_WebHcMenuItemCreation.'		
			--- Execute retrieval of Error info.
			--EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












GO
