USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminFiltersCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAdminFiltersCreation]
Purpose					: To Create Filters for given HubCiti.
Example					: [usp_WebHcAdminFiltersCreation]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminFiltersCreation]
(
    --Input variable
	  @HubCitiID int
	, @CityExperienceID int 
	, @FilterName varchar(100)
	, @RetailLocationIDs varchar(500) --Comma Seperated ID's
	, @BottomButtonImagePath_ON varchar(1000)
	, @BottomButtonImagePath_OFF varchar(1000)  
	  
	--Output Variable
	, @DuplicateName int output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			BEGIN TRANSACTION
			
			DECLARE @UserID INT
			DECLARE @UserType INT
			
			SELECT @UserType = UserTypeID 
			FROM UserType
			WHERE UserType = 'HubCiti Admin'
			
			SELECT @UserID = UserID
			FROM Users
			WHERE UserTypeID = @UserType
			AND HcHubCitiID = @HubCitiID
			
			--Check if given FilterName is already existed.
			IF EXISTS (SELECT 1 FROM HcFilter WHERE FilterName = @FilterName)
			BEGIN
				
				SET @DuplicateName = 1
				
			END
			ELSE 
			BEGIN
				INSERT INTO HcFilter ( FilterName
									   , HcCityExperienceID
									   , HcHubCitiID
									   , DateCreated
									   , CreatedUserID
									   , BottomButtonImagePath_ON
									   , BottomButtonImagePath_OFF
									   )
								Values (  @FilterName
										, @CityExperienceID
										, @HubCitiID
										, GETDATE()
										, @UserID
										, @BottomButtonImagePath_ON
										, @BottomButtonImagePath_OFF
										)
				
				--Capture the last inserted identity value.
				DECLARE @FilterID int
				SELECT @FilterID = @@IDENTITY
																			
				INSERT INTO HcFilterRetailLocation ( HcFilterID
													, RetailLocationID
													, DateCreated
													, CreatedUserID
													)
											SELECT  @FilterID
													, R.Param
													, GETDATE()
													, @UserID
											FROM dbo.fn_SplitParam (@RetailLocationIDs,',') R 
				SET @DuplicateName = 0
				
			END
			
			
			--Confirmation of Success
			SELECT @Status = 0
			
	        COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminFiltersCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
