USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminForgotPassword]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminForgotPassword
Purpose					: To display the password when the HC admin selects the reset [password ling.
Example					: usp_WebHcAdminForgotPassword

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminForgotPassword]
(
   ----Input variable.
	  @UserName varchar(255)
	, @NewPassword varchar(100)
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN
			
			--Update the Password of the User
			UPDATE Users SET Password = @NewPassword
							, ResetPassword = 0 
			WHERE HcHubCitiID > 0
			AND UserName = @UserName   
			
			--Send the details of the User
			SELECT HcHubCitiID
				 , UserID
				 , Email emailId
				 , Password password
				 , UserName
				 , ResetPassword
			FROM Users
			WHERE HcHubCitiID > 0
			AND UserName = @UserName   
			
			--Confirmation of Success
			SELECT @Status = 0
			
	    COMMIT TRAN  
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminForgotPassword.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
			ROLLBACK TRAN
		END;
		 
	END CATCH;
END;







GO
