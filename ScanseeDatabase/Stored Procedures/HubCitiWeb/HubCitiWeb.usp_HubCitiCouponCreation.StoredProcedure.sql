USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HubCitiCouponCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HubCitiCouponCreation
Purpose					: To create a new coupon in HubCiti.

History
Version		   Date			Author			Change Description
------------------------------------------------------------------------------- 
1.0        28th Nov 2016	Shilpashree	    Initial version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_HubCitiCouponCreation]
(
	--Input Variables 
	  @UserID int
	, @RetailID int
	, @RetailLocationID VARCHAR(Max)
	, @CouponName varchar(200)
	, @CouponBannerTitle Varchar(1000)
	, @NoOfCouponsToIssue int
	, @CouponDescription NVarchar(MAX)
	, @CouponTermsAndConditions varchar(MAX)
	, @CouponImagePath varchar(1000)
	, @CouponDetailImage Varchar(1000)
	--, @ProductCategoryIDs Varchar(Max)
	, @CouponStartDate date
	, @CouponEndDate date
	, @CouponStartTime time
	, @CouponEndTime time
	, @CouponExpirationDate date
	, @CouponExpirationTime time
	, @KeyWords varchar(1000)
	, @HcHubCitiID int
	
	--Output Variable--
	, @FindClearCacheURL varchar(1000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY 

		BEGIN TRANSACTION
		
			DECLARE @CouponID int
		
			INSERT INTO Coupon ( CouponName	
								,BannerTitle
								,CouponShortDescription 
								,CouponTermsAndConditions
								,NoOfCouponsToIssue
								,CouponImagePath
								,CouponDetailImage
								,CouponDateAdded
								,CouponStartDate
								,CouponExpireDate 
								,ActualCouponExpirationDate 
								,CreatedUserID
								,KeyWords
								,HcHubCitiID)
						 VALUES(@CouponName
								,@CouponBannerTitle
								,@CouponDescription
								,@CouponTermsAndConditions
								,@NoOfCouponsToIssue
								,@CouponImagePath
								,@CouponDetailImage
								,GETDATE()
								,CAST(@CouponStartDate AS DATETIME) + CAST(ISNULL(@CouponStartTime,'') AS DATETIME)
								,CAST(@CouponEndDate AS DATETIME) + CAST(ISNULL(@CouponEndTime,'') AS DATETIME)
								,CAST(ISNULL(@CouponExpirationDate,NULL) AS DATETIME) + CAST(ISNULL(@CouponExpirationTime,'') AS DATETIME)
								,@UserID
								,@KeyWords
								,@HcHubCitiID
								)
								
				SET @CouponID = SCOPE_IDENTITY();	
				
				--IF(@ProductCategoryIDs IS NOT NULL) --Associate the Coupon to the given Product Categories
				--BEGIN						
			
				--INSERT INTO CouponProductCategoryAssociation(CouponID
				--											 ,ProductCategoryID
				--											 ,DateCreated
				--											 ,CreatedUserID
				--											 )
				--										SELECT @CouponID
				--											, P.Param
				--											, GETDATE()
				--											, @UserID
				--										FROM dbo.fn_SplitParam(@ProductCategoryIDs,',') P
			
				--END
			
				IF(@RetailLocationID IS NOT NULL) --Associate the Coupon to the given Retail Locations
				BEGIN
				INSERT INTO CouponRetailer (CouponID
											, RetailID 
											, RetailLocationID
											, DateCreated	
											)
                                   SELECT @CouponID
                                        , @RetailID
                                        , RL.Param
                                        , GETDATE()
                                   FROM dbo.fn_SplitParam(@RetailLocationID, ',') RL									
			    END

			-------Find Clear Cache URL-----------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
		
			--Confirmation of Success.
			SELECT @Status = 0

		COMMIT TRANSACTION
	  END TRY
            
	BEGIN CATCH
			
			INSERT  INTO SCANSEEVALIDATIONERRORS(ERRORCODE,ERRORLINE,ERRORDESCRIPTION,ERRORPROCEDURE,Inputon)
			VALUES(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE(),getdate())
			SET @ErrorMessage=ERROR_MESSAGE() 	SET @ErrorNumber=ERROR_NUMBER()  
					
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			
			--Confirmation of failure.
			SELECT @Status = 1  
	 END CATCH;
END;









GO
