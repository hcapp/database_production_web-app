USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcGeneralSettings]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminSetupAboutUsPage
Purpose					: To set up the small logo image path of the Hub Citi.
Example					: usp_WebHcAdminSetupAboutUsPage

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13/9/2013	    Pavan Sharma K		1.0
2.0			21/9/2016		Bindu T A			1.1 - Adding Pagination Colors From the Web
-------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcGeneralSettings]
(
   --Input variable.
      @HCAdminUserID int
	, @HubCitiID int
	, @SmallLogo varchar(255)
	, @MenuBackgroundColor varchar(255)
    , @MenuBackgroundImage varchar(255)
    , @MenuButtonColor varchar(255)
    , @MenuButtonFontColor varchar(255)
    , @SubMenuBackgroundColor varchar(255)
    , @SubMenuBackgroundImage varchar(255)
    , @SubMenuButtonColor varchar(255)
    , @SubMenuButtonFontColor varchar(255)
    , @SettingsType varchar(255)
    , @BottomButtonTypeID int
    , @AppIcon varchar(255)
	, @MenuGroupBackgroundColor varchar(255)
	, @MenuGroupFontColor varchar(255)
	, @SubMenuGroupBackgroundColor varchar(255)
	, @SubMenuGroupFontColor varchar(255)
	, @MenuIconicFontColor varchar(255)
	, @SubMenuIconicFontColor varchar(255)
	, @homeIconName varchar(255)
	, @backButtonIconName varchar(255)
	, @backGroundColor varchar(255)
	, @titleColor varchar(255)
	, @MenuPgntdColorActive varchar(10)
	, @SubMenuPgntdColorActive varchar(10)
	, @MenuPgntdColorInActive varchar(10)
	, @SubMenuPgntdColorInActive varchar(10)
	, @newsFeedText varchar(10)
	, @tickerBackground varchar(10)
	, @HubCitiClaimURL varchar(max)
	, @NewsHamburgerIcon varchar(5000)
	, @ClaimImage VARCHAR(1000)
	, @ClaimText varchar(max)
	--News first flag to populate news data
	, @NewsFirstFlag BIT

	, @ClaimBannerText1 varchar(100)
	, @ClaimBannerText2 varchar(100)
	, @ClaimBannerText3 varchar(100)
	, @ClaimBannerText4 varchar(100)


	
	--Output Variable
	, @ClearCacheURL VARCHAR(1000) OUTPUT  
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	

	 IF EXISTS(SELECT 1 FROM HcMenuCustomUI WHERE HubCitiId = @HubCitiID)
	 BEGIN

		UPDATE HcMenuItem 
		SET DateCreated = GETDATE()
		FROM HcMenu H
		INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
		WHERE H.HcHubCitiID = @HubCitiID
		
		IF @SettingsType = 'TabBarLogo'
		BEGIN
				UPDATE HcHubCiti
				SET SmallLogo = @SmallLogo
				   ,AppIcon = @AppIcon
				WHERE HcHubCitiID = @HubCitiID
				
				UPDATE HcMenuCustomUI 
					SET HcBottomButtonTypeID = @BottomButtonTypeID, 
						homeIconName = @homeIconName,
						backButtonIconName = @backButtonIconName,
						backGroundColor = @backGroundColor,
						titleColor = @titleColor,
						ClaimImage = @ClaimImage,
						ClaimText = @ClaimText 
					, ClaimBannerText1  = rtrim(ltrim(@ClaimBannerText1))
					, ClaimBannerText2  = rtrim(ltrim(@ClaimBannerText2)) 
					, ClaimBannerText3  = rtrim(ltrim(@ClaimBannerText3))
					, ClaimBannerText4  = rtrim(ltrim(@ClaimBannerText4))
				WHERE HubCitiId = @HubCitiID

					--To update Cliam url for a hubciti
					UPDATE HcHubCiti 
					SET HubCitiClaimURL = @HubCitiClaimURL 
					WHERE hchubcitiid = @HubCitiID
				
		END
		ELSE IF @SettingsType = 'News'
		BEGIN
				UPDATE HcMenuCustomUI
				SET newsFeedText = @newsFeedText,
				    tickerBackground = @tickerBackground,
					NewsHamburgerIcon = @NewsHamburgerIcon
				WHERE HubCitiID = @HubCitiID

				UPDATE HcHubCiti
				SET NewsFirstFlag = @NewsFirstFlag
				WHERE HcHubCitiID = @HubCitiID
		END
		ELSE IF EXISTS(SELECT 1 FROM HcMenuCustomUI WHERE HubCitiId = @HubCitiID) AND @SettingsType = 'MainMenu'	
		BEGIN
				UPDATE HcMenuCustomUI 
				SET  MenuBackgroundColor = @MenuBackgroundColor
					,MenuBackgroundImage = @MenuBackgroundImage
					,MenuButtonColor = @MenuButtonColor
					,MenuButtonFontColor = @MenuButtonFontColor
					,MenuGroupBackgroundColor = @MenuGroupBackgroundColor
					,MenuGroupFontColor = @MenuGroupFontColor
					,MenuIconicFontColor = @MenuIconicFontColor,
					MenuPgntdColorActive = @MenuPgntdColorActive,
					MenuPgntdColorInActive = @MenuPgntdColorInActive
				
				WHERE HubCitiId = @HubCitiID		
		END
		ELSE IF EXISTS(SELECT 1 FROM HcMenuCustomUI WHERE HubCitiId = @HubCitiID) AND @SettingsType = 'SubMenu'	
		BEGIN
				UPDATE HcMenuCustomUI 
				SET  SubMenuBackgroundColor = @SubMenuBackgroundColor
					,SubMenuBackgroundImage = @SubMenuBackgroundImage
					,SubMenuButtonColor = @SubMenuButtonColor
					,SubMenuButtonFontColor = @SubMenuButtonFontColor
					,SubMenuGroupBackgroundColor = @SubMenuGroupBackgroundColor
					,SubMenuGroupFontColor = @SubMenuGroupFontColor
					,SubMenuIconicFontColor = @SubMenuIconicFontColor,
					SubMenuPgntdColorActive = @SubMenuPgntdColorActive,
					SubMenuPgntdColorInActive= @SubMenuPgntdColorInActive
					
				WHERE HubCitiId = @HubCitiID		
		END
			
	END
	ELSE IF NOT EXISTS(SELECT 1 FROM HcMenuCustomUI WHERE HubCitiId = @HubCitiID)
	BEGIN
			INSERT INTO HcMenuCustomUI(HubCitiId
									,MenuBackgroundColor
									,MenuBackgroundImage
									,MenuButtonColor
									,MenuButtonFontColor
									,SubMenuBackgroundColor
									,SubMenuBackgroundImage
									,SubMenuButtonColor
									,SubMenuButtonFontColor
									,MenuGroupBackgroundColor
									,MenuGroupFontColor
									,SubMenuGroupBackgroundColor
									,SubMenuGroupFontColor
									,MenuIconicFontColor
									,SubMenuIconicFontColor
									,MenuPgntdColorActive
									,SubMenuPgntdColorActive
									,MenuPgntdColorInActive
									,SubMenuPgntdColorInActive
									,newsFeedText
									,tickerBackground
									,ClaimImage
									,ClaimText
									, ClaimBannerText1 
									, ClaimBannerText2 
									, ClaimBannerText3 
									, ClaimBannerText4 )
							SELECT   @HubCitiID
									,@MenuBackgroundColor
									,@MenuBackgroundImage
									,@MenuButtonColor
									,@MenuButtonFontColor
									,@SubMenuBackgroundColor
									,@SubMenuBackgroundImage
									,@SubMenuButtonColor
									,@SubMenuButtonFontColor
									,@MenuGroupBackgroundColor 
									,@MenuGroupFontColor 
									,@SubMenuGroupBackgroundColor 
									,@SubMenuGroupFontColor	
									,@MenuIconicFontColor
									,@SubMenuIconicFontColor
									,@MenuPgntdColorActive
									,@SubMenuPgntdColorActive
									,@MenuPgntdColorInActive
									,@SubMenuPgntdColorInActive
									,@newsFeedText
									,@tickerBackground
									,@ClaimImage
									,@ClaimText
									, rtrim(ltrim(@ClaimBannerText1)) 
									, rtrim(ltrim(@ClaimBannerText2)) 
									, rtrim(ltrim(@ClaimBannerText3)) 
									, rtrim(ltrim(@ClaimBannerText4)) 
	END	

------------------------------------------------MAIN MENU CACHING CHANGES---------------------------------------------------------------------------------------------
--------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------

		IF EXISTS (SELECT 1 FROM HcHubCiti WHERE HcAppListID = 1 AND HcHubCitiID = @HubCitiID)		
		BEGIN

				DECLARE @ClearCacheURL1 varchar(1000),@ClearCacheURL2 varchar(1000),@ClearCacheURL3 varchar(1000)
				SELECT @ClearCacheURL1= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema3'
				SELECT @ClearCacheURL2= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema2'
				SELECT @ClearCacheURL3= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema1'
				SET @ClearCacheURL = @ClearCacheURL1 +','+ @ClearCacheURL2 +','+ @ClearCacheURL3

		END

		ELSE 
		BEGIN

				DECLARE @ClearCacheURL11 varchar(1000),@ClearCacheURL22 varchar(1000),@ClearCacheURL33 varchar(1000)
				SELECT @ClearCacheURL11= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema3'
				SELECT @ClearCacheURL22= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema2'
				SELECT @ClearCacheURL33= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema1'
				SET @ClearCacheURL = @ClearCacheURL11 +','+ @ClearCacheURL22 +','+ @ClearCacheURL33

		END


		UPDATE HcMenuCustomUI
		SET ClaimBannerText1 = CASE WHEN ClaimBannerText1 = ' ' THEN NULL ELSE ClaimBannerText1 END
		where hubcitiid = @HubCitiID
		
		UPDATE HcMenuCustomUI
		SET ClaimBannerText2 = CASE WHEN ClaimBannerText2 = ' ' THEN NULL ELSE ClaimBannerText2 END
		where hubcitiid = @HubCitiID
		
		UPDATE HcMenuCustomUI
		SET ClaimBannerText3 = CASE WHEN ClaimBannerText3 = ' ' THEN NULL ELSE ClaimBannerText3 END
		where hubcitiid = @HubCitiID
		
		UPDATE HcMenuCustomUI
		SET ClaimBannerText4 = CASE WHEN ClaimBannerText4 = ' ' THEN NULL ELSE ClaimBannerText4 END
		where hubcitiid = @HubCitiID

		 
		 
		          
------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------

					-------Find Clear Cache URL---29/2/2015--------

			  DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

              SELECT @ClearCacheURL = @ClearCacheURL + ',' + @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------	

	 --Confirmation of Success.
	 SELECT @Status = 0

	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminSetupAboutUsPage.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
