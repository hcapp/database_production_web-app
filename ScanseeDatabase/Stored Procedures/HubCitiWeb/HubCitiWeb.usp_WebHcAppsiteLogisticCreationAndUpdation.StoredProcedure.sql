USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAppsiteLogisticCreationAndUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteLogisticCreationAndUpdation]
Purpose					: To create and update Appsite logistic map.
Example					: [usp_WebHcAppsiteLogisticCreationAndUpdation] 

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAppsiteLogisticCreationAndUpdation]
(
	
	--Input Variable
	  @UserID int
	, @HubCitiID int
	, @HcAppsiteLogisticID int
	, @LogisticName varchar(1000)
	, @LogisticImage varchar(1000)
	, @RetailID int
	, @RetailLocationID int
	, @StartDate datetime
	, @EndDate Datetime
	, @isPortrtOrLandscp int
    
    --Output Variables
	, @LogisticID int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN


		
			--To Create Appsite Logistic map
			IF (@HcAppsiteLogisticID IS NULL)
			BEGIN

				INSERT INTO HcAppsiteLogistic( HcHubCitiID
										, LogisticName
										, LogisticImage
										, RetailID
										, RetailLocationID
										, StartDate
										, EndDate
										, DateCreated
										, CreatedUserID
										, MapDisplayTypeID
										)
								SELECT @HubCitiID
								      , @LogisticName
									  , @LogisticImage
									  , @RetailID
									  , @RetailLocationID
									  , @StartDate
									  , @EndDate
									  , GETDATE()
									  , @UserID
									  , @isPortrtOrLandscp

				SELECT @LogisticID = SCOPE_IDENTITY() 

			END
			ELSE 
			--To Update Appsite Logistic map
			BEGIN

				UPDATE HcAppsiteLogistic SET LogisticName = @LogisticName
											, LogisticImage = @LogisticImage
											, RetailID = @RetailID
											, RetailLocationID = @RetailLocationID
											, StartDate = @StartDate
											, EndDate = @EndDate
											, DateModified = GETDATE()
											, ModifiedUserID = @UserID
											, MapDisplayTypeID = @isPortrtOrLandscp
				WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID
				AND HcHubCitiID = @HubCitiID

				SELECT @LogisticID = @HcAppsiteLogisticID

			END
			
			--Confirmation of Success
			SELECT @Status = 0
			COMMIT TRAN

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteLogisticCreationAndUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRAN;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
