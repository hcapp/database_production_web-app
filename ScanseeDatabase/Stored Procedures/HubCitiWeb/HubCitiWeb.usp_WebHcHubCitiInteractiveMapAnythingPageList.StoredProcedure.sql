USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiInteractiveMapAnythingPageList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiWeb].[usp_WebHcHubCitiInteractiveMapAnythingPageList]
Purpose					: To display List of Anything Page for interactive Map
Example					: [HubCitiWeb].[usp_WebHcHubCitiInteractiveMapAnythingPageList]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/31/2016	    Sagar Byali			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiInteractiveMapAnythingPageList]
(  
     --Input variable.
      @HubCitiID Int

  
	--Output Variable 
	, @MaxCnt int output	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
	
		  			   
			--To display List fo Anything Page
			SELECT RowNum = IDENTITY(int, 1, 1)
				  ,HP.HcAnythingPageID
				  ,AnythingPageName AnythingPageTitle
				  ,ShortDescription
				  ,LongDescription
				  ,StartDate
				  ,EndDate
				  ,ImagePath
				  ,ImageIcon
				  ,HcHubCitiID	
				  ,CASE WHEN URL IS NULL OR URL = '' THEN  HT.HcAnyThingPageMediaType ELSE 'Website' END PageType
				  ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
											  FROM HcMenuItem MI 
											  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
											  WHERE LinkTypeName = 'AnythingPage' 
											  AND MI.LinkID = HP.HcAnythingPageID)>0 THEN 1 ELSE 0 END	
				  ,CASE WHEN (URL IS NULL OR URL = '') AND (MediaPath IS NULL OR MediaPath = '')   THEN 'Make Your Own' ELSE 'EXISTING' END PageView									  
											  						  
			INTO #Temp	   
			FROM HcAnythingPage HP
			LEFT JOIN HcAnythingPageMedia HM ON HP.HcAnythingPageID = HM.HcAnythingPageID
			LEFT JOIN HcAnythingPageMediaType HT ON HM.MediaTypeID = HT.HcAnyThingPageMediaTypeID  			
			WHERE HcHubCitiID = @HubCitiID 
			
			
			--To capture max row number.        
			SELECT @MaxCnt = COUNT(RowNum) FROM #Temp
			
			
			
			
			SELECT RowNum
				 , HcAnythingPageID
				 , AnythingPageTitle
				 , ShortDescription
				 , LongDescription
				 , StartDate
			     , EndDate
			     , ImagePath
			     , ImageIcon
			     , HcHubCitiID
			     , PageType 
			     , MenuItemExist
			     , PageView
			FROM #Temp
			WHERE PageType NOT IN ('Audio','Video')
			
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [HubCitiWeb].[usp_WebHcHubCitiInteractiveMapAnythingPageList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
