USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceDisplay
Purpose					: To display CityExperienceName.
Example					: usp_WebHcCityExperienceDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceDisplay]
(
    --Input variable.
      @HubCitiID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config VARCHAR(100)
			
			SELECT @Config = ScreenContent       
		    FROM AppConfiguration       
		    WHERE --ScreenName = 'All'     
		    ConfigurationType = 'Hubciti Media Server Configuration'    
		    AND Active = 1 
			
			--To display CityExperience List
			SELECT HcCityExperienceID
				  ,CityExperienceName
			      ,HcHubCitiID
			      ,@Config + CAST(@HubCitiID AS VARCHAR(100))+'/'+ CityExperienceImage 				 		   
			FROM HcCityExperience 
			WHERE HcHubCitiID=@HubCitiID 
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
