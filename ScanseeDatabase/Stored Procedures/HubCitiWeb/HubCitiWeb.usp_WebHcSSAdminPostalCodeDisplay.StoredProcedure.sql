USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminPostalCodeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcSSAdminPostalCodeDisplay]
Purpose					: To disply the list of postal code for given Hubciti.
Example					: [usp_WebHcSSAdminPostalCodeDisplay]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			17/1/2014	    SPAN			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminPostalCodeDisplay]
(
    --Input variable
	  @City VARCHAR(100)
	, @State VARCHAR(100)
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		SELECT DISTINCT G.PostalCode
			 , Location = G.PostalCode + ',' + G.City + ',' + G.State
		FROM GeoPosition G
		--LEFT JOIN HcLocationAssociation H ON G.PostalCode = H.PostalCode AND G.State = H.State AND G.City = H.City 	
		--AND ((@HubCitiID IS NULL AND HcHubCitiID = 0) OR HcHubCitiID = @HubCitiID)	
		WHERE G.City = @City AND G.[State] = @State 		
		--AND H.HcLocationAssociationID IS NULL
		--EXCEPT
		--SELECT PostalCode
		--	 , Location = PostalCode + ' ' + City + ' ' + State
		--FROM HcLocationAssociation
		--WHERE HcHubCitiID = @HubCitiID AND City = @City AND [State] = @State

		--Confirmation of Success
		SELECT @Status = 0
			
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcSSAdminPostalCodeDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
