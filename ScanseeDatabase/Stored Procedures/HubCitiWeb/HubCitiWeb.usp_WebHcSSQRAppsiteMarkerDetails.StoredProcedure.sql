USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSQRAppsiteMarkerDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcSSQRAppsiteMarkerDetails]
Purpose					: To edit Appsite logistic map marker details.
Example					: [usp_WebHcSSQRAppsiteMarkerDetails]

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSQRAppsiteMarkerDetails]
(
	
	--Input Variable
	  @HubCitiID int
	, @HcAppsiteLogisticID int
    
    --Output Variables
	, @AppsiteMapImagePath Varchar(2000) Output
	, @AppsiteLogisticLatitude Float Output	
	, @AppsiteLogisticLongitude Float Output	
	, @AppsiteLogisticTopLeftLat Float Output	
	, @AppsiteLogisticTopLeftLong Float Output	
	, @AppsiteLogisticBottomRightLat Float Output	
	, @AppsiteLogisticBottomRightLong Float Output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @AppsiteConfig Varchar(500) 

			SELECT @AppsiteConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'

			SELECT @AppsiteLogisticLatitude = Latitude
				  ,@AppsiteLogisticLongitude = Longitude
				  ,@AppsiteLogisticTopLeftLat = TopLeftLatitude
				  ,@AppsiteLogisticTopLeftLong = TopLeftLongitude
				  ,@AppsiteLogisticBottomRightLat = BottomRightLatitude
				  ,@AppsiteLogisticBottomRightLong = BottomRightLongitude
			FROM HcAppsiteOverLayCoordinates
			WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID 
			AND HcHubCitiID = @HubCitiID

			SET @AppsiteMapImagePath = @AppsiteConfig + CAST(@HubCitiID AS VARCHAR)+ '/logistics/'+CAST(@HcAppsiteLogisticID AS VARCHAR)+'/'

			--To display details of Appsite Logistic map marker points.
			 SELECT L.HcAppsiteLogisticID
					, MarkerName
					, markerImage = @AppsiteConfig + CAST(@HubCitiID AS VARCHAR)+ '/logistics/'+CAST(@HcAppsiteLogisticID AS VARCHAR)+'/' + MarkerImage
					, Latitude AS markerLatitude
					, Longitude AS markerLongitude
			FROM HcAppsiteLogistic L
			INNER JOIN HcAppsiteMarker M ON L.HcAppsiteLogisticID = M.HcAppsiteLogisticID
			WHERE L.HcAppsiteLogisticID = @HcAppsiteLogisticID AND M.HcAppsiteLogisticID = @HcAppsiteLogisticID
			AND HcHubCitiID = @HubCitiID
			ORDER BY MarkerName									
			
			--Confirmation of Success
			SELECT @Status = 0

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcSSQRAppsiteMarkerDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
