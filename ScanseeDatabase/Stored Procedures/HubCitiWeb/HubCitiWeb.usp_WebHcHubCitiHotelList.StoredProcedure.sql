USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiHotelList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiHotelList
Purpose					: To display List fo Retaillocation Details.
Example					: usp_WebHcHubCitiHotelList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiHotelList]
(   
    --Input variable.	  
	  @HubCitiID int
	, @SearchKey Varchar(max)
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT DISTINCT TOP (CASE WHEN @SearchKey IS NULL THEN 200 ELSE 5000 END) 
				            RL.RetailID
			               ,R.RetailName  
			               ,RL.RetailLocationID
						   ,RL.Address1 
						   ,RL.City
						   ,RL.State 
						   ,RL.PostalCode 
			FROM BusinessCategory B
			INNER JOIN RetailerBusinessCategory RB ON B.BusinessCategoryID =RB.BusinessCategoryID AND B.BusinessCategoryName ='Lodging'
			INNER JOIN Retailer R ON RB.RetailerID =R.RetailID
			INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID 
			INNER JOIN HcLocationAssociation HL ON  HL.HcHubCitiID =@HubCitiID AND RL.PostalCode  =HL.PostalCode 
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND RLC.Associated = 1
			WHERE ((@SearchKey IS NOT NULL AND R.RetailName LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL))	
				  AND (R.RetailerActive=1 AND RL.Active=1)
			
						
			--HcHubCiti H
			
			--INNER JOIN RetailLocation RL ON RL.PostalCode =HL.PostalCode AND RL.city =HL.City AND RL.State =HL.State 
			--INNER JOIN Retailer R ON R.RetailID=RL.RetailID
			--INNER JOIN RetailerBusinessCategory RB ON RB.RetailerID =RL.RetailID 
			--INNER JOIN BusinessCategory B ON 
											
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiHotelList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
