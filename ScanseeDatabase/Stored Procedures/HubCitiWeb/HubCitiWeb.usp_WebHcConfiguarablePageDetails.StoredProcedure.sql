USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcConfiguarablePageDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcConfiguarablePageDetails
Purpose					: To disply the list of the Hub Cities that are created by the SS admin.
Example					: usp_WebHcConfiguarablePageDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16/9/2013	    Span   1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcConfiguarablePageDetails]
(
   ----Input variable.
	  @HCAdminUserID int
	, @HubCitiID varchar(100)
	, @PageTypeName varchar(100)
	  
	--Output Variable 
	, @VersionNumber varchar(20) output
    , @Status int output       
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config VARCHAR(100)
			
			SELECT @Config = ScreenContent       
		    FROM AppConfiguration       
		    WHERE --ScreenName = 'All'     
		    ConfigurationType = 'Hubciti Media Server Configuration'    
		    AND Active = 1  
			

			DECLARE @AppListName VARCHAR(100)
			SELECT @AppListName = HcAppListName
			FROM HcHubCiti H 
			INNER JOIN HcAppList A ON H.HcAppListID = A.HcAppListID
			WHERE HcHubCitiID = @HubCitiID 
		    
			IF @AppListName = 'HubCitiApp'
				--Get the latest version of the Hub Citi app.
				SELECT @VersionNumber = 'Version ' + ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'YetToReleaseHubCitiAppVersionIOS' AND ScreenName = 'YetToReleaseHubCitiAppVersionIOS'
            ELSE
				--Get the latest version of the Hub Citi app.
				SELECT @VersionNumber = 'Version ' + ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'YetToReleaseRegionAppVersionIOS' AND ScreenName = 'YetToReleaseRegionAppVersionIOS'

		     
		   	
		   	--Select Login Page Details for selected Hubciti if exist  
		    IF (@PageTypeName='Login Page') 
		    BEGIN
		       SELECT @Config + CAST(@HubCitiID AS VARCHAR(100))+'/'+ SmallLogo smallLogoPath
				     ,@Config+ CAST(@HubCitiID AS VARCHAR(100))+'/'+LogoImage logoPath
				     ,LogoImage LogoImageName
		             ,BackgroundColor bgColor
		             ,FontColor 
		             ,ButtonColor btnColor
		             ,ButtonFontColor btnFontColor
		       FROM HcPageConfiguration HCP
		       INNER JOIN HcPageType HC ON HCP.HcPageTypeID =HC.HcPageTypeID  
		       INNER JOIN HcHubCiti H ON H.HcHubCitiID = HCP.HcHubCitiID
		       WHERE H.HcHubCitiID =@HubCitiID AND HC.PageType='Login Page'
		    
		    END
		    
		    --Select Registration Page Details for selected Hubciti if exist
		    IF (@PageTypeName='Registration Page') 
		    BEGIN
		       SELECT @Config + CAST(@HubCitiID AS VARCHAR(100))+'/'+ SmallLogo SmallLogo
					 ,BackgroundColor bgColor		           
		             ,FontColor 
		             ,Title pageTitle
		             ,[Description] pageContent
		             ,ButtonColor btnColor
		             ,ButtonFontColor btnFontColor
		       FROM HcPageConfiguration HCP
		       INNER JOIN HcPageType HC ON HCP.HcPageTypeID =HC.HcPageTypeID  
		       INNER JOIN HcHubCiti H ON H.HcHubCitiID = HCP.HcHubCitiID
		       WHERE HCP.HcHubCitiID =@HubCitiID AND HC.PageType='Registration Page' 		    
		    END
		    
		    --Select Privacy Policy Details for selected Hubciti if exist
		    IF (@PageTypeName='Privacy Policy') 
		    BEGIN
		       SELECT @Config + CAST(@HubCitiID AS VARCHAR(100))+'/'+ SmallLogo SmallLogo
				     ,BackgroundColor bgColor
		             ,FontColor 
		             ,Title pageTitle
		             ,[Description] pageContent		             
		        FROM HcPageConfiguration HCP
		       INNER JOIN HcPageType HC ON HCP.HcPageTypeID =HC.HcPageTypeID 
		       INNER JOIN HcHubCiti H ON H.HcHubCitiID = HCP.HcHubCitiID 
		       WHERE HCP.HcHubCitiID =@HubCitiID AND HC.PageType='Privacy Policy'		    
		    END
		   
		    --Select About Us Page Details for selected Hubciti if exist
		    IF (@PageTypeName='About Us Page') 
		    BEGIN
		       SELECT smallLogoPath = @Config+CAST(@HubCitiID AS VARCHAR(100))+'/'+SmallLogo 
					 ,SmallLogo smallLogoImageName
		             ,[logoPath] = @Config+CAST(@HubCitiID AS VARCHAR(100))+'/'+[Image]
		             , [Image] [logoImageName]		            
		             ,[Description] pageContent		             
		       FROM HcPageConfiguration HCP
		       INNER JOIN HcPageType HC ON HCP.HcPageTypeID =HC.HcPageTypeID 
		       INNER JOIN HcHubCiti H ON H.HcHubCitiID = HCP.HcHubCitiID  
		       WHERE HCP.HcHubCitiID =@HubCitiID AND HC.PageType='About Us Page'		    
		    END	
			
			--Select Splash Image Details for selected Hubciti if exist
			IF (@PageTypeName='Splash Image') 
		    BEGIN
		       SELECT [logoPath] = @Config+CAST(@HubCitiID AS VARCHAR(100))+'/'+SplashImage 
		             , SplashImage [logoImageName]	             	             
		       FROM HcHubCiti H  
		       WHERE HcHubCitiID = @HubCitiID
		       AND SplashImage IS NOT NULL		    
		    END	    
		    --Get the general settings page.
		    IF @PageTypeName = 'General Settings'
		    BEGIN
				SELECT [logoPath] = @Config+CAST(@HubCitiID AS VARCHAR(100))+'/'+SmallLogo
					 , SmallLogo [logoImageName]
				FROM HcHubCiti H  
		        WHERE HcHubCitiID = @HubCitiID
		        AND SmallLogo IS NOT NULL
		    END
		   
		    			
			--Confirmation of Success
			SELECT @Status = 0	
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcConfiguarablePageDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
