USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HubCitiWebSetFeaturedCoupon]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HubCitiWebSetFeaturedCoupon    
Purpose				  : To set Coupon as Featured.   
Example				  : usp_HubCitiWebSetFeaturedCoupon    
    
History
Version		Date			Author			Change Description
-----------------------------------------------------------------
1.0        12/6/2016	  Shilpashree	    Initial version
-----------------------------------------------------------------  
*/    
    
CREATE PROCEDURE [HubCitiWeb].[usp_HubCitiWebSetFeaturedCoupon]
(    
	   @CouponID int   
	 , @Featured bit
	 , @HcHubCitiID int
     
	 --Output Variable   
	  , @FeaturedLimit bit output
	  --, @CannotBeFeatured bit output
	  , @Status int output    
	  , @ErrorNumber int output    
	  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
	BEGIN TRY    
		BEGIN TRAN
			
			SET @FeaturedLimit = 0
			--SET @CannotBeFeatured = 0 

			--IF EXISTS(SELECT 1 FROM Coupon WHERE ISNULL(ActualCouponExpirationDate,GETDATE()+1) < GETDATE() AND CouponID = @CouponID)
			--	SET @CannotBeFeatured  = 1 

			DECLARE @Counts int
			SELECT @Counts = Count(FC.Featured)
			FROM Coupon C
			INNER JOIN HubCitiFeaturedCoupon FC ON FC.CouponID = C.CouponID
			WHERE FC.HubCitiID = @HcHubCitiID AND FC.Featured = 1

			IF @Featured = 1 AND @Counts >= 10
			BEGIN

				SET @FeaturedLimit = 1
				
			END
			ELSE IF @Featured = 1 AND @Counts < 10
			BEGIN

				INSERT INTO HubCitiFeaturedCoupon( CouponID
												  ,HubCitiID
												  ,Featured
												  ,DateCreated
												  ,DateModified
												  ,CreatedUserID
												  ,ModifiedUserID) 
							VALUES(@CouponID,@HcHubCitiID,@Featured,GETDATE(),GETDATE(),3,3)
			END
			ELSE IF @Featured = 0
			BEGIN
				
				DELETE FROM HubCitiFeaturedCoupon
				WHERE CouponID = @CouponID AND HubCitiID= @HcHubCitiID

		    END
		
		COMMIT

		--Confirmation of Success
		SELECT @Status = 0	 
        
	END TRY    
      
	BEGIN CATCH    
     
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure usp_HubCitiWebSetFeaturedCoupon.'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
			ROLLBACK TRAN
			
			--Confirmation of Failure
			SELECT @Status = 1
		END;    
       
	END CATCH;    
END;






GO
