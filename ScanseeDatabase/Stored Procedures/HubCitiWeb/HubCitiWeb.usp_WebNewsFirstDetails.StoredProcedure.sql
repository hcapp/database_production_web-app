USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebNewsFirstDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [HubCitiWeb].[usp_WebNewsFirstDetails]
( 
		
		  @NewsID int  

		--Output Variable 
		--Output Variable 	
	    , @IOSAppID varchar(500) output
	    , @AndroidAppID varchar(500) output
		, @Status bit Output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY  


				SELECT @IOSAppID = IOSAppID
					  ,@AndroidAppID = AndroidAppID
				FROM HcHubCiti
				WHERE HcHubCitiID = (SELECT HcHubCitiID FROM RssNewsFirstFeedNews WHERE RssNewsFirstFeedNewsID = @NewsID)

	--

		
						SELECT distinct  RssNewsFirstFeedNewsID Id
										,NewsType
										,Title
										,PublishedDate PubStrtDate
										,ImagePath = CASE WHEN CHARINDEX(',', ImagePath) > 0 THEN LEFT(ImagePath, CHARINDEX(',', ImagePath)-1) ELSE ImagePath END
										,ShortDescription shortDesc
										,LongDescription description
										,Link
										,PublishedTime PubTime
										,videoLink
										,(SELECT screencontent FROM AppConfiguration where ConfigurationType ='QR Code Configuration') + '4000.htm?newsId='+ CAST(RssNewsFirstFeedNewsID AS VARCHAR(10)) qrURL
							FROM RssNewsFirstFeedNews RFN
							WHERE RssNewsFirstFeedNewsID = @NewsID
			
	 SET @Status=0  

	END TRY
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.' 
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		SET @Status=1 
		END;
 
	END CATCH;
END;









GO
