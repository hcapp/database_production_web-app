USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcMenuDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcMenuDeletion
Purpose					: Delete selected Menu Details.
Example					: usp_WebHcMenuDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6/03/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcMenuDeletion]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int	
	, @HcMenuID Int
	  
	--Output Variable 	
	, @SubMenuAssociateFlag int OUTPUT
	, @SubMenuNames varchar(500) OUTPUT
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	             				
				
				--Check if the selected menuid associated to any submenu
				IF NOT EXISTS(SELECT 1 
				              FROM HcMenuItem M
				              INNER JOIN HcLinkType L ON L.HcLinkTypeID =M.HcLinkTypeID AND L.LinkTypeName ='SubMenu' AND LinkID =@HcMenuID )

                BEGIN

				   
				    SET @SubMenuAssociateFlag=0

					
					DELETE FROM HcMenuItemFilterAssociation
					FROM HcMenuItemFilterAssociation E
					INNER JOIN HcMenuItem M ON M.HcMenuItemID = E.HcMenuItemID AND M.HcMenuID =@HcMenuID 

					DELETE FROM HcMenuItemFundraisingCategoryAssociation
					FROM HcMenuItemFundraisingCategoryAssociation E
					INNER JOIN HcMenuItem M ON M.HcMenuItemID = E.HcMenuItemID AND M.HcMenuID =@HcMenuID 

					DELETE FROM HcRegionAppMenuItemCityAssociation
					FROM HcRegionAppMenuItemCityAssociation E
					INNER JOIN HcMenuItem M ON M.HcMenuItemID = E.HcMenuItemID AND M.HcMenuID =@HcMenuID 

					DELETE FROM HcMenuItemEventCategoryAssociation
					FROM HcMenuItemEventCategoryAssociation E
					INNER JOIN HcMenuItem M ON M.HcMenuItemID = E.HcMenuItemID AND M.HcMenuID =@HcMenuID 					

					DELETE FROM HcMenuItemBandCategories
					FROM HcMenuItemBandCategories F
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID 

					DELETE FROM HcMenuItemNewsCategories
					FROM HcMenuItemNewsCategories F
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID

					DELETE FROM HcMenuFindRetailerBusinessCategories
					FROM HcMenuFindRetailerBusinessCategories F
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID

					DELETE FROM HcFormContent
					FROM HcFormContent FC
					INNER JOIN HcForm F ON F.HcFormID =FC.HcFormID 
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID 

					DELETE FROM HcFormEmailRecipient
					FROM HcFormEmailRecipient FC
					INNER JOIN HcForm F ON F.HcFormID =FC.HcFormID 
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID 
				
					DELETE FROM HcUserHubCitiForm
					FROM HcUserHubCitiForm FC
					INNER JOIN HcForm F ON F.HcFormID =FC.HcFormID 
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID 

					DELETE FROM HcForm
					FROM HcForm F
					INNER JOIN HcMenuItem M ON M.HcMenuItemID =F.HcMenuItemID AND M.HcMenuID =@HcMenuID 
				
					DELETE FROM HcMenuItem WHERE HcMenuID =@HcMenuID 

					DELETE FROM HcMenuBottomButton  WHERE HcMenuID =@HcMenuID
				
					DELETE FROM HcMenu where HcMenuID =@HcMenuID 
				END

				ELSE
				BEGIN

					SELECT @SubMenuNames = 'SubMenu has been associated to ' + STUFF((SELECT ', ' + [MenuItemName]
					FROM HcMenuItem M			
					INNER JOIN HcLinkType L ON L.HcLinkTypeID =M.HcLinkTypeID AND L.LinkTypeName ='SubMenu' AND LinkID = @HcMenuID																																						
					FOR XML PATH('')), 1, 2, '') + ' .Please deassociate and delete'

					SET @SubMenuAssociateFlag=1

				END
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcMenuDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
