USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCouponDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCouponDetails
Purpose					: 
Example					: usp_WebHcCouponDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30 Apr 2015     Mohith H R	    Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCouponDetails]
(
	  @CouponID int
    , @HcHubcitiID int 	
	
	--Output Variable 
	--, @CouponExpired bit Output
	--, @ShareText varchar(500) output
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output  
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		    --To get Server Configuration 
			 DECLARE @RetailConfig varchar(50)
			 DECLARE @Config varchar(100)
			  DECLARE @Config1 varchar(100)
		  
			 SELECT @Config1 = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Hubciti Media Server Configuration' 

			  SELECT @RetailConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'

		   --Retrieve the server configuration.
            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 

			SELECT @IOSAppID = IOSAppID
			  ,@AndroidAppID = AndroidAppID
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID

			--To get the text used for share.         
            --SELECT @ShareText = ScreenContent
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti Coupon Share Text'    
			 
		    --SET @CouponExpired = 0

			SELECT DISTINCT CouponName
					,BannerTitle
			       ,CouponShortDescription
				   ,CouponLongDescription
				   ,CouponURL
				   ,ExternalCoupon
				   ,CouponStartDate
				   ,CouponExpireDate
				   ,CouponImagePath = CASE WHEN C.HcHubCitiID IS NOT NULL THEN @Config1 + CAST(@HcHubCitiID as varchar(10))+'/' + CouponDetailImage 
											 WHEN C.RetailID IS NOT NULL AND C.HcHubCitiID IS NULL AND CouponDetailImage  = 'uploadIconSqr.png' THEN @RetailConfig + CouponDetailImage ELSE @RetailConfig + CAST(C.RetailID as varchar(10))+'/' + CouponDetailImage 
											 END
				   --,CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
							--																			CASE WHEN WebsiteSourceFlag = 1 
							--																				THEN @RetailConfig
							--																				+CONVERT(VARCHAR(30),CR.RetailID)+'/'
							--																				+CouponImagePath 
							--																		    ELSE CouponImagePath 
							--																		    END
							--																	 END 
									 --END  				   
				   --,QRUrl = @Config+'2700.htm?key1='+ CAST(@CouponID AS VARCHAR(10)) 
				   ,QRUrl = @Config+'2700.htm?couponId='+ CAST(@CouponID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HcHubCitiId AS VARCHAR(10))  			
			FROM Coupon C
			LEFT JOIN CouponRetailer CR on C.CouponID = CR.CouponID
			LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID
			WHERE c.CouponID=@CouponID --AND GETDATE() Between CouponStartDate AND CouponExpireDate 

			SELECT DISTINCT RL.RetailLocationID
						,Address = Rl.Address1
						,City = RL.City
						,State = RL.State
						,PostalCode = RL.PostalCode
						,Imagepath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RL.RetailLocationImagePath) 	
			FROM CouponRetailer CR
			INNER JOIN Retailer R ON CR.RetailID = R.RetailID
			INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID
			WHERE CR.CouponID = @CouponID 

			--SELECT @CouponExpired=CASE WHEN GETDATE()>CouponExpireDate Then 1 else 0 END 
			--FROM Coupon
			--WHERE CouponID=@CouponID		
			
			SET @Status = 0 
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcCouponDetails.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status = 1
			
		END;
		 
	END CATCH;
END;

GO
