USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventsCategoryDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsCategoryDeletion
Purpose					: Deleting Selected  Event Category.
Example					: usp_WebHcHubCitiAlertsCategoryDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventsCategoryDeletion]
(   
    --Input variable.	  
      @HcEventCategoryID Int
	
  	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			--Deleting Selected  Event Category.
			
			IF NOT EXISTS(SELECT 1 FROM HcEventsCategoryAssociation HC
					  INNER JOIN HcEventsCategory HE ON HC.HcEventCategoryID = HE.HcEventCategoryID
					  WHERE HC.HcEventCategoryID = @HcEventCategoryID)
			BEGIN		  	
				DELETE FROM HcEventsCategoryAssociation 
				WHERE HcEventCategoryID = @HcEventCategoryID 
			
				DELETE FROM HcEventsCategory 
				WHERE HcEventCategoryID = @HcEventCategoryID  

				SELECT @Status = 0
			END
			ELSE
			BEGIN
				SELECT @Status = 1
			END	 
												
			
	       --Confirmation of Success.
		   --SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventsCategoryDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
