USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonsAssociationDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBottomButtonsAssociationDeletion]
Purpose					: To delete associated bottom buttons from selected module..
Example					: [usp_WebHcBottomButtonsAssociationDeletion] 

History
Version		     Date				Author				    Change Description
-------------------------------------------------------------------------- 
1.0				 9 July 2014		Dhananjaya TR	        1.0
--------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonsAssociationDeletion]
(
	
	--Input Variable
	  @HcHubCitiID int
	, @ModuleID int	
    , @UserID int
    
    --OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY		
			
			--Delete bottom Button association from selected module
			DELETE FROM HcFunctionalityBottomButton 
			WHERE HcHubCitiID = @HcHubCitiID 
			AND HcFunctionalityID = @ModuleID

			
			--Confirmation of Success
			SELECT @Status = 0
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcBottomButtonsAssociationDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
