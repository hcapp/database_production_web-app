USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDisplayRetailerCustomPageURL]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcDisplayRetailerCustomPageURL
Purpose					: To Display the Retailer Custom Page URLs for HubCiti.
Example					: usp_WebHcDisplayRetailerCustomPageURL

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			18 Nov 2013      SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDisplayRetailerCustomPageURL]
(
	  
	  @UserID int
	, @RetailID int  
	, @PageID int  
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	     --To Get App Media Server Configuration 
		 DECLARE @Config varchar(50)    
		 SELECT @Config=ScreenContent    
		 FROM AppConfiguration     
		 WHERE ConfigurationType='App Media Server Configuration'
		 
		 DECLARE @RetailerConfig varchar(50)
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='QR Code Configuration'
		 AND Active = 1
		 		 	 
		SELECT QRTypeCode
		     , QRRCP.RetailID		       
		     , QRRCP.QRRetailerCustomPageID  
		     , COUNT(QRRCPA.RetailLocationID) Counts
		INTO #Temp
		FROM QRRetailerCustomPage QRRCP  
		INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
		INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCP.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
		WHERE QRRCPA.QRRetailerCustomPageID = @PageID
		AND QRRCP.RetailID = @RetailID
		GROUP BY QRTypeCode
			   , QRRCP.RetailID		     
			   , QRRCP.QRRetailerCustomPageID  		
		  
		SELECT DISTINCT  QRRCP.Pagetitle AS retPageTitle
					   , QRRCP.PageDescription AS retPageDescription
					   , QRRCP.ShortDescription AS retShortDescription
					   , QRRCP.LongDescription AS retLongDescription  
					   --If the user has selected the upload pdf or images or the link to existing then follow the SSQR template to construct the URL.
					   , qrUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL
								 THEN CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10)) + '&EL=true'
										   WHEN COUNTS = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10)) + '&EL=true'
									  END 
								 ELSE			
								 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
									  CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10)) + '&EL=false'
										   WHEN Counts = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10)) + '&EL=false'
									  END
								 END
		               ,@Config+'mailer_facebook.png' facebookimg
					   ,@Config+'mailer_scansee.png' scanseeimg
					   ,@Config+'mailer_twitter.png' twitterimg
					   ,@Config+'mailer_arrowscansee.png' arrowscanseeimg
		FROM #Temp T
		INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON T.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
		INNER JOIN QRRetailerCustomPage QRRCP ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID
		LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = T.QRRetailerCustomPageID
				
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebHcDisplayRetailerCustomPageURL>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END; 






GO
