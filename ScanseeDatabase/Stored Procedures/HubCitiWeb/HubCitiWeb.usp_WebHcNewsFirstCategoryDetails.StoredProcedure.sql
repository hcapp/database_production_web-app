USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[usp_WebHcNewsCategoryDetails]]
Purpose					: To display list of News categories along with images for Combinational Templete.
Example					: [[usp_WebHcNewsCategoryDetails]]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali		 [usp_WebHcNewsFirstCategoryDetails]
2.0         17/9/2016        Shilpashree         Added Default Category flag
3.0			28/9/2016		 Sagar Byali	     Added News ticker 
4.0			20/10/2016		 Sagar Byali	     Added Subcategory flag 
5.0         11/09/2016		 Sagar Byali		 Sort Order changes & Pagination removal
5.1			12/15/2016		 Bindu T A			 Back Button Changes
----------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails]
(   
	--Input variable
	  @HcHubCitiID int
	, @UserID int
	, @SearchKey VARCHAR(1000)
	, @NewsSettingsID VARCHAR(1000)

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
			

			;with cte as(

			--To display list of News categories along with Color.
			SELECT DISTINCT  I.SortOrder  sortOder
							,I.HcNewsSettingsID newsSettingsID
							,I.NewsCategoryID AS catId
							,ISNULL(B.NewsCategoryDisplayValue,NewsLinkCategoryName) AS catName
							,NewsFeedURL catfeedURL
							,ISNULL(I.NewsCategoryColor,B.NewsCategoryColor) catColor
							,ISNULL(I.BackButtonColor,'#ffffff') backButtonColor
							,NewsCategoryTempleteTypeDisplayValue  displayType
							,T.NewsCategoryTempleteTypeId newsTempleteTypeId	
							
							,DefaultCategory As isDefault
							,NewsTicker isNewsTicker
							,NewsStories newsStories

							--Added subcategory Flag ---20thOct2016
							,isSubCatFlag = CASE WHEN A.SubCategoryID IS NOT NULL THEN 1 ELSE 0 END
							,NewsCategoryFontColor catFontColor

							,IsNewsFeed isFeed
			FROM NewsFirstSettings I
			INNER JOIN NewsFirstCategory B ON I.NewsCategoryID = B.NewsCategoryID AND B.Active = 1
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.categoryID = B.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID
			INNER JOIN NewsFirstCategoryTempleteType T ON T.NewsCategoryTempleteTypeID = I.NewsFirstCategoryTempleteTypeID
			LEFT JOIN NewsFirstSubCategoryType CT ON CT.NewsCategoryID = B.NewsCategoryID
			LEFT JOIN NewsFirstSubCategory CC ON CC.NewsSubCategoryTypeID = CT.NewsSubCategoryTypeID
			WHERE I.HcHubCitiID = @HcHubCitiID 
			
			UNION

			SELECT DISTINCT  SortOrder  sortOder
							,HcNewsSettingsID newsSettingsID
							,NewsCategoryID AS catId
							,NewsLinkCategoryName AS catName
							,NewsFeedURL catfeedURL
							,NewsCategoryColor catColor
							,BackButtonColor backButtonColor
							,null  displayType
							,null newsTempleteTypeId	
							
							,DefaultCategory As isDefault
							,NewsTicker isNewsTicker
							,null newsStories

							--Added subcategory Flag ---20thOct2016
							,isSubCatFlag = 0 
							,NewsCategoryFontColor catFontColor

							,IsNewsFeed isFeed
			FROM NewsFirstSettings 
			WHERE HcHubCitiID = @HcHubCitiID and IsNewsFeed = 0
			)
			
			select  *
			from cte 
			where (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (catName LIKE '%'+@SearchKey+'%')) OR (@SearchKey IS NULL))
			ORDER BY sortOder 
			 
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		--THROW;
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsFirstCategoryDetails].'		
			--- Execute retrieval of Error info.
			DECLARE @ErrorLine INT
			       ,@ErrorCode INT
				   ,@ErrorProcedure varchar(1000)
				   
			SET @ErrorLine = ERROR_LINE()
			SET @ErrorCode = ERROR_NUMBER() 
			SET @ErrorProcedure = '[HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails]'

			EXEC [usp_GetErrorInfoOutPut]  @ErrorProcedure, @Errorline, @ErrorCode , @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
	
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
