USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFAQCategoryCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFAQCategoryCreation
Purpose					: Creating FAQ Category.
Example					: usp_WebHcFAQCategoryCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFAQCategoryCreation]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int
	, @CategoryName Varchar(1000)
	, @FAQCategoryID Int
	, @ScanSeeAdminUserID Int
	  
	--Output Variable 	
	, @DuplicateFlag Bit Output
	, @HcFAQCategoryID INT OUTPUT
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
								
				IF EXISTS(SELECT 1 FROM HcFAQCategory WHERE CategoryName =@CategoryName AND HcHubCitiID =@HcHubcitiID 
				AND (@FAQCategoryID IS NULL OR (@FAQCategoryID IS NOT NULL AND @FAQCategoryID <>HcFAQCategoryID )))
				BEGIN
				
					SET @DuplicateFlag=1
										
				END
				
				ELSE IF @FAQCategoryID IS NULL
				BEGIN
					
					INSERT INTO HcFAQCategory ( CategoryName
											   ,HcHubCitiID
											   ,DateCreated
											   ,CreatedUserID )	
					SELECT @CategoryName
					     , @HcHubcitiID  					   
						 , GETDATE()
						 , @ScanSeeAdminUserID	
						 
					SELECT @HcFAQCategoryID = SCOPE_IDENTITY()	 	
						 
					SET @DuplicateFlag=0					
					
				END
				
				ELSE
				BEGIN
                    UPDATE HcFAQCategory SET CategoryName=@CategoryName 
										    ,HcHubCitiID=@HcHubcitiID
										    ,DateModified=GETDATE()
										    ,ModifiedUserID=@ScanSeeAdminUserID
					WHERE HcFAQCategoryID =@FAQCategoryID 
					
					SET @DuplicateFlag=0	  
				END			
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQCategoryCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
