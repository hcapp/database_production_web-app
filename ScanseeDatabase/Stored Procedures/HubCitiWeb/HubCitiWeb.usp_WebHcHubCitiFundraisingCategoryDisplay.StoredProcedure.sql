USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingCategoryDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingCategoryDisplay
Purpose					: To display list of Fundraising Categories.
Example					: usp_WebHcHubCitiFundraisingCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Aug 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingCategoryDisplay]
(   
    --Input variable.
	  @UserID Int	  
    , @CategoryName Varchar(1000)
	, @LowerLimit Int  
	, @ScreenName Varchar(200)
	, @FundraisingCategoryID Int
	, @RoleBasedUserID Int
  
	--Output Variable 
	, @MaxCnt Int output
	, @NxtPageFlag Bit output 
    , @Status Int output
	, @ErrorNumber Int output
	, @ErrorMessage Varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit Int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   

			DECLARE @Config Varchar(500)
				
			----To get the Configuration Details.	 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			--To get user info i.e Super/Normal user.

			DECLARE @RoleName VARCHAR(100)
			SELECT @RoleName = HcRoleName
			FROM HcUserRole UR
			INNER JOIN HcRole R ON UR.HcRoleID = R.HcRoleID
			WHERE HcAdminUserID = @UserID
			AND (HcRoleName LIKE '%FUNDRAISING%' OR HcRoleName LIKE '%ROLE_ADMIN%')

			--To display List fo Events Categories

			CREATE TABLE #FundraisingCat(RowNum Int
									,HcFundraisingCategoryID Int
									,HcFundraisingCategoryName Varchar(1000)
									,AssociateCate Int
									,CategoryImageName Varchar(1000)
									,CategoryImagePath Varchar(1000))


			IF @UserID IS NOT NULL AND @RoleBasedUserID IS NOT NULL 
			BEGIN

				INSERT INTO #FundraisingCat(RowNum 
									,HcFundraisingCategoryID 
									,HcFundraisingCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(								
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY FundraisingCategoryName) As Rownum
										,P.HcFundraisingCategoryID catId
										,FundraisingCategoryName catName
										,associateCate = IIF(F.CreatedUserID=@RoleBasedUserID,'1','0')
										,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
										,CategoryImagePath cateImg
								FROM HcFundraisingCategory P 
								LEFT JOIN HcFundraisingCategoryAssociation UE ON P.HcFundraisingCategoryID = UE.HcFundraisingCategoryID AND UE.CreatedUserID = @RoleBasedUserID	
								LEFT JOIN HcFundraising F ON UE.HcFundraisingID = F.HcFundraisingID AND F.Active = 1	
								WHERE ((@CategoryName IS NOT NULL AND FundraisingCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@FundraisingCategoryID IS NOT NULL AND P.HcFundraisingCategoryID = @FundraisingCategoryID) OR (@FundraisingCategoryID IS NULL and 1=1))
								) EventsCat			
																			
								
			END
			ELSE IF @RoleName = 'ROLE_FUNDRAISING_USER' AND @RoleBasedUserID IS NULL
			BEGIN

				INSERT INTO #FundraisingCat(RowNum 
									,HcFundraisingCategoryID 
									,HcFundraisingCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(			
								--SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY HcEventCategoryName) As Rownum
								--	  ,P.HcEventCategoryID catId
								--	  ,HcEventCategoryName catName
								--	  ,associateCate = IIF(UE.CreatedUserID=@RoleBasedUserID,'1','0')
								--	  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
								--	  ,CategoryImagePath cateImg
								----INTO #EventsCat
								--FROM HcEventsCategory P 
								--inner JOIN HcEventsCategoryAssociation UE ON P.HcEventCategoryID = UE.HcEventCategoryID AND UE.CreatedUserID = @RoleBasedUserID
								----LEFT JOIN HcEventsCategoryAssociation CA ON P.HcEventCategoryID = Ca.HcEventCategoryID
								--WHERE ((@CategoryName IS NOT NULL AND HcEventCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								--AND ((@Eventcategoryid IS NOT NULL AND P.HcEventCategoryID = @EventCategoryID) OR (@Eventcategoryid IS NULL and 1=1))
								--) EventsCat

								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY FundraisingCategoryName) As Rownum
									  ,P.HcFundraisingCategoryID catId
									  ,FundraisingCategoryName catName
									  ,associateCate = IIF(EC.CreatedUserID=@UserID,'1','0') --CASE WHEN (SELECT COUNT(1) FROM HcEventsCategoryAssociation WHERE HcEventCategoryID=P.HcEventCategoryID) >0 THEN 1 ELSE 0 END
									  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
									  ,CategoryImagePath cateImg
								--INTO #EventsCat
								FROM HcFundraisingCategory P 
								LEFT JOIN HcFundraisingRoleBasedUserCategoryAssociation UE ON P.HcFundraisingCategoryID = UE.HcFundraisingCategoryID
								LEFT JOIN HcFundraisingCategoryAssociation EC ON EC.HcFundraisingCategoryID =P.HcFundraisingCategoryID AND EC.CreatedUserID =@UserID 
								WHERE ((@CategoryName IS NOT NULL AND FundraisingCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@FundraisingCategoryID IS NOT NULL AND P.HcFundraisingCategoryID = @FundraisingCategoryID) OR (@FundraisingCategoryID IS NULL and 1=1))
								AND UE.RoleBasedUserID = @UserID) EventsCat
			END
			ELSE IF @RoleName <> 'ROLE_FUNDRAISING_USER' AND @RoleBasedUserID IS NULL			
			BEGIN
				
				INSERT INTO #FundraisingCat(RowNum 
									,HcFundraisingCategoryID 
									,HcFundraisingCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(			
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY FundraisingCategoryName) As Rownum
									  ,P.HcFundraisingCategoryID catId
									  ,FundraisingCategoryName catName
									  ,associateCate = 0 --CASE WHEN (SELECT COUNT(1) FROM HcFundraisingCategoryAssociation WHERE HcFundraisingCategoryID=P.HcFundraisingCategoryID) >0 THEN 1 ELSE 0 END
									  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
									  ,CategoryImagePath cateImg
								--INTO #EventsCat
								FROM HcFundraisingCategory P 
								--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON P.HcEventCategoryID = UE.HcEventCategoryID
								WHERE ((@CategoryName IS NOT NULL AND FundraisingCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@FundraisingCategoryID IS NOT NULL AND P.HcFundraisingCategoryID = @FundraisingCategoryID) OR (@FundraisingCategoryID IS NULL and 1=1)))EventsCat			
			END
			ELSE IF @UserID IS NOT NULL AND @RoleBasedUserID IS NULL			
			BEGIN
				
				INSERT INTO #FundraisingCat(RowNum 
									,HcFundraisingCategoryID 
									,HcFundraisingCategoryName 
									,AssociateCate 
									,CategoryImageName
									,CategoryImagePath)
							
							SELECT  RowNum 
									,catId 
									,catName 
									,associateCate 
									,cateImgName
									,cateImg
						
							FROM(			
								SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY FundraisingCategoryName) As Rownum
									  ,P.HcFundraisingCategoryID catId
									  ,FundraisingCategoryName catName
									  ,associateCate = CASE WHEN (SELECT COUNT(1) FROM HcFundraisingCategoryAssociation WHERE HcFundraisingCategoryID=P.HcFundraisingCategoryID) >0 THEN 1 ELSE 0 END
									  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
									  ,CategoryImagePath cateImg
								--INTO #EventsCat
								FROM HcFundraisingCategory P 
								--LEFT JOIN HcRoleBasedUserEventCategoryAssociation UE ON P.HcEventCategoryID = UE.HcEventCategoryID
								WHERE ((@CategoryName IS NOT NULL AND FundraisingCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
								AND ((@FundraisingCategoryID IS NOT NULL AND P.HcFundraisingCategoryID = @FundraisingCategoryID) OR (@FundraisingCategoryID IS NULL and 1=1))
								AND P.CreatedUserID = @UserID)EventsCat
			
			END
		
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #FundraisingCat
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			SELECT   Rownum 
					,HcFundraisingCategoryID catId
					,HcFundraisingCategoryName catName
					,AssociateCate associateCate
					,CategoryImageName cateImgName
					,CategoryImagePath cateImg
			FROM #FundraisingCat 
			WHERE ((@LowerLimit IS NOT NULL AND RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit) OR @LowerLimit IS NULL) 	
		    ORDER BY RowNum 									
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingCategoryDisplay.'				
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
