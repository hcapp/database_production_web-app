USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceUpdation
Purpose					: Update Selected  City Experience.
Example					: usp_WebHcCityExperienceUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcCityExperienceUpdation]
(   
    --Input variable.	  
	 @HubCitiID Int
    ,@UserID Int
	,@CityExperienceName Varchar(500)
	--,@CityExperienceImage Varchar(2000)
	--,@ButtonImagePath  Varchar(2000)
	--,@BottomButtonImagePath_ON Bit
	--,@BottomButtonImagePath_OFF Bit
	, @RetailLocationID varchar(Max) --Comma separated ID's
  
	--Output Variable
	, @FindClearCacheURL VARCHAR(500) OUTPUT 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Update Selected  City Experience.

			DECLARE @CityExperienceID int
			
			SELECT @CityExperienceID =HcCityExperienceID 
			FROM HcCityExperience WHERE HcHubCitiID =@HubCitiID
			
			UPDATE HcCityExperience SET CityExperienceName=@CityExperienceName 
										,HcHubCitiID=@HubCitiID
										,DateModified=GETDATE()						
										,ModifiedUserID=@UserID
										--,CityExperienceImage=@CityExperienceImage
										--,ButtonImagePath=@ButtonImagePath
										--,BottomButtonImagePath_ON=@BottomButtonImagePath_ON
										--,BottomButtonImagePath_OFF=@BottomButtonImagePath_OFF
			WHERE HcHubCitiID =@HubCitiID --AND HcCityExperienceID =@HcCityExperienceID
			
			----To Delete previously associated RetailLocations
			--DELETE FROM HcCityExperienceRetailLocation
			--WHERE HcCityExperienceID = @CityExperienceID
			
			--To Associate RetailLocations to given CityExperience	
			INSERT INTO HcCityExperienceRetailLocation( HcCityExperienceID
													   , RetailLocationID
													   , CreatedUserID
													   , DateCreated
													   )
												SELECT @CityExperienceID
													   , P.Param
													   , @UserID
													   , GETDATE()
												FROM dbo.fn_SplitParam(@RetailLocationID,',')P
												LEFT JOIN HcCityExperienceRetailLocation C ON C.RetailLocationID = P.Param AND C.HcCityExperienceID = @CityExperienceID 
												WHERE C.HcCityExperienceRetailLocationID IS NULL
			
			UPDATE HcRetailerAssociation 
			SET Associated = 1 
				, DateModified = GETDATE()
				,  ModifiedUserID = @UserID
			WHERE HcHubCitiID = @HubCitiID
			AND RetailLocationID IN (SELECT Param FROM fn_SplitParam(@RetailLocationID,','))

			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		    ------------------------------------------------

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
