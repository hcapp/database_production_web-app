USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNewsCreation]
Purpose					: To Create news for the push Notification.
Example					: [usp_WebHcNewsCreation]

History
Version		Date			Author	             Change Description
--------------------------------------------------------------- 
1.0			01/19/2015	    Dhananjaya TR		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsCreation]
(
    --Input variable
	 
	  @Title Varchar(max)
	, @Link Varchar(max)
	, @PublishedDate  Varchar(2000)
	, @Type Varchar(2000)
	, @Description Varchar(Max)
	, @ImagePath Varchar(max)	
	
	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DELETE FROM HcNewsStaging 
			FROM HcNewsStaging N
			INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
			WHERE NewsType <> 'Breaking News' AND @Type = 'Trending' AND NewsType <> 'tyler test Breaking News' 

			DELETE FROM HcNewsStaging 
			FROM HcNewsStaging N
			INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
			WHERE Title  like '%~~%' AND PublishedDate like '%~~%'

			DELETE FROM HcNews 
			FROM HcNews N
			INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
			WHERE Title  like '%~~%' AND PublishedDate like '%~~%'


			if not exists(SELECT *  FROM HcNewsStaging  N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
						  WHERE convert(datetime,convert(varchar(100), DateCreated,101))=convert(datetime,convert(varchar(100),getdate(),101)) AND NewsType = 'Breaking News') 

			BEGIN
					UPDATE HcUserToken 	
					SET NewsTypeBreaking = 0
			END

			if not exists(SELECT *  FROM HcNewsStaging  N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
						  WHERE convert(datetime,convert(varchar(100), DateCreated,101))=convert(datetime,convert(varchar(100),getdate(),101)) AND NewsType = 'tyler test Breaking News') 

			BEGIN
					UPDATE HcUserToken 	
					SET NewsTypeTaylorBreakingNews = 0
			END

			if not exists(SELECT * FROM HcNewsStaging  N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
						  WHERE convert(datetime,convert(varchar(100), DateCreated,101))=convert(datetime,convert(varchar(100),getdate(),101)) AND NewsType = 'Trending') 


			BEGIN
					UPDATE HcUserToken 	
					SET NewsTypeTrending = 0
			END

			
			SET @Title = REPLACE(@Title,'|~~|','¶')
	
	        SELECT Rownum=Identity(Int,1,1)
			      ,item Title
			INTO #Title
			FROM SplitString_Description(@Title,'¶')

			--select  *From #Title

			SET @Link = REPLACE(@Link,'|~~|','¶')

			SELECT Rownum=Identity(Int,1,1)
			      ,param Link
			INTO #Link
			FROM fn_SplitParam(@Link,'¶')

			--select  *From #Link

			SET @PublishedDate = REPLACE(@PublishedDate,'|~~|','¶')

			SELECT Rownum=Identity(Int,1,1)
			      ,Param PublishedDate
			INTO #PublishedDate
			FROM fn_SplitParam(@PublishedDate,'¶')

			--select * from #PublishedDate

			SET @Type = REPLACE(@Type,'|~~|','¶')

			SELECT Rownum=Identity(Int,1,1)
			      ,PARAM Type
			INTO #Type
			FROM fn_SplitParam(@Type,'¶')
			
	

			SET @Description = REPLACE(@Description,'|~~|','~')

			SELECT Rownum=Identity(Int,1,1)
			      ,item Description
			INTO #Description
			FROM SplitString_Description(@Description,'~')

			SET @ImagePath = REPLACE(@ImagePath,'|~~|','¶')

			SELECT Rownum=Identity(Int,1,1)
			      ,param ImagePath
			INTO #ImagePath
			FROM fn_SplitParam(@ImagePath,'¶')

		

			SELECT DISTINCT T.Rownum 
			               ,NT.HcNewsTypeID 
			INTO #newsTypes
			FROM #Type T
			INNER JOIN HCnewsType NT ON NT.Newstype=T.Type 

			--select *From #newsTypes


			SELECT DISTINCT T.Rownum
			      ,T.Title
				  ,L.Link 
				  ,P.PublishedDate 
				  ,NT.HcNewsTypeID
				  ,D.Description  
				  ,I.ImagePath
			INTO #NewsList
			FROM #Title T
			INNER JOIN #Link L ON L.Rownum =T.Rownum
			INNER JOIN #PublishedDate P ON P.Rownum =L.Rownum
			INNER JOIN #newsTypes NT ON NT.Rownum =T.Rownum
			INNER JOIN #Description D ON D.Rownum =T.Rownum
			INNER JOIN #ImagePath I ON I.Rownum =T.Rownum
			
			--select  *From #NewsList
				
			INSERT INTO HcNewsStaging(Title
									 ,PublishedDate
									 ,URL
									 ,HcNewsTypeID
									 ,Description
									 ,ImagePath
									 ,DateCreated)
			SELECT DISTINCT T.Title 
			      ,T.PublishedDate
				  ,T.Link
				  ,T.HcNewsTypeID 
				  ,T.Description 
				  ,T.ImagePath 
				  ,GETDATE()
				  
			FROM #NewsList T
			LEFT JOIN HcNews N ON isnull(N.Title,'') =isnull(T.Title,'') AND isnull(T.Link,'') =isnull(N.URL,'') 
			AND CONVERT(DATE,PARSE(T.PublishedDate AS DATETIME))  =CONVERT(DATE,PARSE(N.PublishedDate AS DATETIME))  AND T.HcNewsTypeID = N.HcNewsTypeID
			WHERE N.HcNewsID IS NULL 

			DELETE FROM HcNewsStaging 
			FROM HcNewsStaging N
			INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
			WHERE Title  like '%~~%' AND PublishedDate like '%~~%'

			DELETE FROM HcNews 
			FROM HcNews N
			INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
			WHERE Title  like '%~~%' AND PublishedDate like '%~~%'


				INSERT INTO HcNews(Title
								,PublishedDate
								,URL
								,HcNewsTypeID
								,Description
								,ImagePath
								,NotificationSentDate
								,DateCreated
								,CreatedUserID)
			SELECT T.Title
					,T.PublishedDate
					,T.link
					,T.HcNewsTypeID
					,T.Description
					,T.ImagePath
					,getdate()
					,getdate()
					,CreatedUserID
			FROM #NewsList T
			LEFT JOIN HcNews N ON isnull(N.Title,'') =isnull(T.Title,'') AND isnull(T.Link,'') =isnull(N.URL,'') 
			AND CONVERT(DATE,PARSE(T.PublishedDate AS DATETIME))  =CONVERT(DATE,PARSE(N.PublishedDate AS DATETIME))  AND T.HcNewsTypeID = N.HcNewsTypeID
			WHERE N.HcNewsID IS NULL 

			if exists(SELECT * FROM HcNewsStaging N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID WHERE (convert(date,convert(varchar(100),N.DateCreated))  =  convert(date,convert(varchar(100),getdate()))) AND NewsType = 'Breaking News') 
			begin
			
					DELETE FROM HcNewsStaging 
					FROM HcNewsStaging N
					INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
					WHERE NewsType = 'Breaking News' 
					AND (convert(date,convert(varchar(100),N.DateCreated))  <>  convert(date,convert(varchar(100),getdate())))

			END

			if exists(SELECT * FROM HcNewsStaging N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID WHERE (convert(date,convert(varchar(100),N.DateCreated))  =  convert(date,convert(varchar(100),getdate()))) AND NewsType = 'tyler test Breaking News') 
			begin
			
					DELETE FROM HcNewsStaging 
					FROM HcNewsStaging N
					INNER JOIN HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
					WHERE NewsType = 'tyler test Breaking News' 
					AND (convert(date,convert(varchar(100),N.DateCreated))  <>  convert(date,convert(varchar(100),getdate())))

			END

				select isnull(Title,'') as Title,CONVERT(VARCHAR(1000),CONVERT(DATETIME,CONVERT(DATE,PARSE(PublishedDate AS DATETIME)))) 
				as PublishedDate,url,max(HcNewsID) as max,min(HcNewsID) as min  into #temp1 from HcNews
				group by isnull(Title,''),CONVERT(DATE,PARSE(PublishedDate AS DATETIME)),url
				having count(*)>1


				DELETE N  from #temp1 T inner JOIN HcNews N ON isnull(N.Title,'') =isnull(T.Title,'') 
				AND ISNULL(T.url,'') =ISNULL(N.URL,'') 
				--AND T.PublishedDate =N.PublishedDate 
				AND CONVERT(DATETIME,CONVERT(DATE,PARSE(t.PublishedDate AS DATETIME)))  =CONVERT(DATETIME,CONVERT(DATE,PARSE(n.PublishedDate AS DATETIME)))
				where  N.HcNewsID  NOT IN(SELECT MAX  FROM #temp1)

				select isnull(Title,'') as Title,CONVERT(VARCHAR(1000),CONVERT(DATETIME,CONVERT(DATE,PARSE(PublishedDate AS DATETIME)))) 
				as PublishedDate,url,max(HcNewsID) as max,min(HcNewsID) as min  into #temp2 from HcNewsStaging
				group by isnull(Title,''),CONVERT(DATE,PARSE(PublishedDate AS DATETIME)),url
				having count(*)>1

				DELETE N  from #temp2 T inner JOIN HcNewsStaging N ON isnull(N.Title,'') =isnull(T.Title,'') 
				AND ISNULL(T.url,'') =ISNULL(N.URL,'')  
				AND CONVERT(DATETIME,CONVERT(DATE,PARSE(t.PublishedDate AS DATETIME)))  =CONVERT(DATETIME,CONVERT(DATE,PARSE(n.PublishedDate AS DATETIME)))
				where  N.HcNewsID  NOT IN(SELECT MAX  FROM #temp2)

				DROP TABLE #temp1


			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
