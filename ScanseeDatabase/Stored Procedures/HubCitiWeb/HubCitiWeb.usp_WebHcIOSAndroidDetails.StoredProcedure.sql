USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcIOSAndroidDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcIOSAndroidDetails
Purpose					: To fetch IOS and Android details.
Example					: usp_WebHcIOSAndroidDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19 Jan 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcIOSAndroidDetails]
(	
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION	

		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Host'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Port'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Certificate Path'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Certificate Password'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='GCM URL'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='GCM API KEY'
		
			
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcIOSAndroidDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;























GO
