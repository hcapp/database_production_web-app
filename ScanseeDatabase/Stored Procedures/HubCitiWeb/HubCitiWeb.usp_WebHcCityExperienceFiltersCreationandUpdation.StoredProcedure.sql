USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceFiltersCreationandUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceFiltersCreationandUpdation
Purpose					: Creating new Filters.
Example					: usp_WebHcCityExperienceFiltersCreationandUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcCityExperienceFiltersCreationandUpdation]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int
	, @HcFilterID Int
	, @HcCityExperienceID Int
	, @FilterName Varchar(1000)
	, @ButtonImagePath  Varchar(2000)
	--, @BottomButtonImagePath_ON Bit
	--, @BottomButtonImagePath_OFF Bit
	, @RetailLocationID varchar(Max) --Comma separated ID's
	, @FilterDescription varchar(2000)  -- New parameter added for filter description
	  
	--Output Variable 
	, @FindClearCacheURL VARCHAR(1000) Output
	, @DuplicateFlag Bit Output	
	, @FilterID int output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	            
				--DECLARE @FilterID Int   
	               
				--Creating new Filters.				
				IF NOT EXISTS(SELECT 1 FROM HcFilter WHERE FilterName=@FilterName AND HcHubCitiID=@HcHubCitiID AND HcCityExperienceID =@HcCityExperienceID) AND @HcFilterID IS NULL
				BEGIN
					
					INSERT INTO HcFilter( FilterName
										 ,HcCityExperienceID
										 ,HcHubCitiID
										 ,DateCreated
										 ,CreatedUserID										
										 ,ButtonImagePath 
										 ,FilterDescription )	
					SELECT @FilterName
					     , @HcCityExperienceID
					     , @HcHubCitiID 
						 , GETDATE()
						 , @UserID					 
						 , @ButtonImagePath
						 , @FilterDescription   	
					
					Set @FilterID =SCOPE_IDENTITY()
					
					--SELECT @FilterID
			
					--To Associate RetailLocations to given Filter	
					INSERT INTO HcFilterRetailLocation( HcFilterID
													   , RetailLocationID
													   , CreatedUserID
													   , DateCreated
													   )
					SELECT @FilterID
							, P.Param
							, @UserID
							, GETDATE()
					FROM dbo.fn_SplitParam(@RetailLocationID,',')P				
						 
					SET @DuplicateFlag=0						
				END
				
				ELSE IF EXISTS(SELECT 1 FROM HcFilter WHERE FilterName=@FilterName AND HcHubCitiID=@HcHubCitiID AND HcCityExperienceID =@HcCityExperienceID) AND @HcFilterID IS NULL 
				BEGIN
					SET @DuplicateFlag=1
				END

				ELSE IF NOT EXISTS(SELECT 1 FROM HcFilter WHERE FilterName=@FilterName AND HcFilterID <>@HcFilterID AND HcHubCitiID=@HcHubCitiID AND HcCityExperienceID =@HcCityExperienceID) AND @HcFilterID IS NOT NULL 
				--ELSE IF @HcFilterID IS NOT NULL
				BEGIN
				   
				   Update HcFilter SET FilterName=@FilterName
								      ,HcCityExperienceID=@HcCityExperienceID
									  ,HcHubCitiID=@HcHubCitiID
									  ,DateModified=GETDATE()
									  ,ModifiedUserID=@UserID											
									  ,ButtonImagePath=@ButtonImagePath	
									  ,FilterDescription=@FilterDescription
				   WHERE HcFilterID =@HcFilterID

					--To Delete previously associated RetailLocations
					--DELETE FROM HcFilterRetailLocation
					--WHERE HcFilterID = @FilterID
			
					--To Associate RetailLocations to given Filter	
					INSERT INTO HcFilterRetailLocation( HcFilterID
													   , RetailLocationID
													   , CreatedUserID
													   , DateCreated
													   )
												SELECT @HcFilterID
													   , P.Param
													   , @UserID
													   , GETDATE()
												FROM dbo.fn_SplitParam(@RetailLocationID,',')P
												LEFT JOIN HcFilterRetailLocation F ON F.RetailLocationID =P.Param AND F.HcFilterID=@HcFilterID 
												WHERE F.RetailLocationID IS NULL

					

					SET @DuplicateFlag=0										
				END

				ELSE IF EXISTS(SELECT 1 FROM HcFilter WHERE FilterName=@FilterName AND HcFilterID <>@HcFilterID  AND HcHubCitiID=@HcHubCitiID AND HcCityExperienceID =@HcCityExperienceID) AND @HcFilterID IS NOT NULL 
				BEGIN
					SET @DuplicateFlag=1
				END
				
				UPDATE HcRetailerAssociation 
				SET Associated = 1 
					, DateModified = GETDATE()
					,  ModifiedUserID = @UserID
				WHERE HcHubCitiID = @HcHubCitiID
				AND RetailLocationID IN (SELECT Param FROM fn_SplitParam(@RetailLocationID,','))
				
				-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceFiltersCreationandUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
