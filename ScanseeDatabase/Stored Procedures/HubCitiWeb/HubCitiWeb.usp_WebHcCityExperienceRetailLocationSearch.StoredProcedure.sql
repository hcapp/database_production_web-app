USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceRetailLocationSearch]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcCityExperienceRetailLocationSearch]
Purpose					: List of RetailLocations.	
Example					: [usp_WebHcCityExperienceRetailLocationSearch] 

History
Version		     Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			26th Nov 2013	  Span	        1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceRetailLocationSearch]
(
	
	--Input Variable
	  @HubCitiID int
    , @FilterID Int
	, @SearchKey Varchar(2000)
	, @LowerLimit int  
	, @ScreenName varchar(50)

    --OutPut Variable
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		    
			--To get the row count for pagination.  
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

			--List of RetailLocations.			 		
		    CREATE Table #RL( Rownum Int IDENTITY(1,1)
			            ,RetailName Varchar(2000)
		                ,RetailLocationID Int
						,Address1 Varchar(2000)
						,City Varchar(100)
						,[State] Varchar(10)
						,PostalCode Varchar(10)
						,AssociateFlag Bit)
		
				

				SELECT DISTINCT 
				          R.RetailName 
						,RL.RetailLocationID 
						,RL.Address1 
						,RL.City 
						,RL.[State] 
						,RL.PostalCode 
						,AssociateFlag=0											
			    INTO #RL1
				FROM HcLocationAssociation LA
				INNER JOIN RetailLocation RL ON LA.PostalCode = RL.PostalCode AND LA.StateID =RL.StateID AND RL.HcCityID=LA.HcCityID AND RL.Headquarters = 0 AND Active = 1
				INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID 
				INNER JOIN Retailer R ON RL.RetailID = R.RetailID
				LEFT JOIN HcFilterRetailLocation FRL ON FRL.RetailLocationID =RLC.RetailLocationID AND (@FilterID IS NOT NULL AND FRL.HcFilterID =@FilterID )        
				WHERE LA.HcHubCitiID = @HubCitiID 
				AND FRL.RetailLocationID IS NULL
				AND R.RetailerActive=1
				AND ((@SearchKey IS NULL AND @FilterID IS NULL) OR (@SearchKey IS NOT NULL AND RetailName LIKE '%'+@SearchKey+'%' AND @FilterID IS NULL)
				OR (@FilterID IS NOT NULL AND ((@SearchKey IS NULL AND 1=2) OR (@SearchKey IS NOT NULL AND RetailName LIKE '%'+@SearchKey+'%'))))

				
				 ORDER BY RetailName

				INSERT INTO #RL(RetailName 
		                ,RetailLocationID 
						,Address1 
						,City 
						,[State] 
						,PostalCode 
						,AssociateFlag)

                SELECT  RetailName 
					    ,RetailLocationID 
						,Address1 
						,City 
						,[State] 
						,PostalCode
						,AssociateFlag
				FROM 
					(SELECT RetailName 
						   ,R.RetailLocationID 
							,Address1 
							,City 
							,[State] 
							,PostalCode
							,AssociateFlag					
					FROM #RL1 R
					

					UNION ALL

					SELECT DISTINCT R.RetailName 
							,RL.RetailLocationID 
							,RL.Address1 
							,RL.City 
							,RL.[State] 
							,RL.PostalCode	
							,AssociateFlag=1    
					FROM HcFilterRetailLocation LA
					INNER JOIN RetailLocation RL ON LA.RetailLocationID = RL.RetailLocationID AND RL.Headquarters = 0 AND LA.HcFilterID =@FilterID AND Active = 1
					INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID --AND Associated =0
					INNER JOIN Retailer R ON RL.RetailID = R.RetailID
					WHERE ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND R.RetailName LIKE '%'+@SearchKey+'%'))
					AND R.RetailerActive=1
				)A ORDER BY AssociateFlag DESC			
			
			

			--To capture Max Row Number.  
			SELECT @MaxCnt = COUNT(Rownum) FROM #RL
					 
			--This flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SELECT   Rownum 
			        ,RetailName retName
					,RetailLocationID retLocId
					,Address1 address
					,City city
					,[State] state
					,PostalCode postalCode
					,AssociateFlag
			FROM #RL
			WHERE Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit

			--Confirmation of Success
			SELECT @Status = 0
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcCityExperienceRetailLocationSearch].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
