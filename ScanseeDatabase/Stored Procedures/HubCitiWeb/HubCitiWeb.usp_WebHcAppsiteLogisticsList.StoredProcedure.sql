USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAppsiteLogisticsList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteLogisticsList]
Purpose					: To display list of Appsite logistic maps.
Example					: [usp_WebHcAppsiteLogisticsList] 

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE  PROCEDURE [HubCitiWeb].[usp_WebHcAppsiteLogisticsList]
(
	
	--Input Variable
	  @HubCitiID int
	, @SearchKey Varchar(500)
	, @LowerLimit int  
	, @ScreenName varchar(50)
    
    --Output Variables
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			
			--To get the row count for pagination.  
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

			DECLARE @AppsiteConfig Varchar(500) 

			SELECT @AppsiteConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'QR Code Configuration'
		
			--To display list of Appsite Logistic maps for given HubcitiID OR SearchKey
			SELECT Row_num = IDENTITY(INT, 1, 1)
					, L.HcAppsiteLogisticID AS logisticId
					, LogisticName
					, LogisticImage
					, MapLink = @AppsiteConfig + '3000.htm?logisticsId=' + CAST(L.HcAppsiteLogisticID AS VARCHAR) + '&hubcitiId=' + CAST(@HubCitiID AS VARCHAR) + '&oType=' + MM.MapDisplayTypeName
					, RetailName
					, location = RL.Address1 + ','+ RL.City +','+ RL.[State] +','+ RL.PostalCode
					, StartDate
					, EndDate
			INTO #Logistics
			FROM HcAppsiteLogistic L
			INNER JOIN MapDisplayType MM ON MM.MapDisplayTypeID = L.MapDisplayTypeID
			LEFT JOIN Retailer R ON L.RetailID = R.RetailID
			LEFT JOIN RetailLocation RL ON L.RetailLocationID = RL.RetailLocationID
			LEFT JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HubCitiID AND Associated = 1
			WHERE L.HcHubCitiID = @HubCitiID									
			AND ((@SearchKey IS NULL AND 1=1) OR (@SearchKey IS NOT NULL AND LogisticName LIKE '%' + @SearchKey + '%'))

			--To capture Max Row Number.  
			SELECT @MaxCnt = COUNT(Row_num) FROM #Logistics
					 
			--This flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
			
			SELECT Row_num
				, logisticId 
				, LogisticName
				, LogisticImage
				, MapLink
				, location
				, StartDate
				, EndDate
			FROM #Logistics
			WHERE Row_num BETWEEN (@LowerLimit + 1)AND @UpperLimit

			--Confirmation of Success
			SELECT @Status = 0

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteLogisticsList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
