USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAppSiteDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAppSiteDisplay
Purpose					: To display List of appsites.
Example					: usp_WebHcHubCitiAppSiteDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAppSiteDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @LowerLimit int  
	, @ScreenName varchar(200)
	, @SearchKey Varchar (MAx)
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	        DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1  
			
			--To display List fo AppSites
			SELECT Row_Number() OVER (ORDER BY HcAppSiteID) Rownum
			      , HcAppSiteID 
				  ,HcAppSiteName 
				  ,R.RetailID 
				  ,R.RetailName 
				  ,HcHubCitiID 
				  ,AP.RetailLocationID 
				  ,RL.Address1 
				  ,RL.City 
				  ,RL.[State] 
				  ,RL.PostalCode 		
		    INTO #Appsite	   
			FROM HcAppSite AP
			INNER JOIN RetailLocation RL ON AP.RetailLocationID = RL.RetailLocationID AND Active = 1
			INNER JOIN Retailer R ON RL.RetailID = R.RetailID
			WHERE HcHubCitiID= @HubCitiID AND ((@SearchKey IS NOT NULL AND AP.HcAppSiteName LIKE '%'+@SearchKey+'%') OR @SearchKey IS NULL)
			AND (RL.Active=1 AND R.RetailerActive=1)
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Appsite
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			
			SELECT RowNum
			      ,HcAppSiteID appSiteId
				  ,HcAppSiteName appSiteName
				  ,RetailID retailId
				  ,RetailName retName
				  ,HcHubCitiID hubCityId
				  ,RetailLocationID retLocId
				  ,Address1 address
				  ,City city
				  ,[State] state
				  ,PostalCode postalCode		
		    FROM #Appsite
			WHERE ((@LowerLimit IS NOT NULL AND RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit) OR @LowerLimit IS NULL) 	
		    ORDER BY RowNum 			
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAppSiteDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
