USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRoleBasedUserDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcRoleBasedUserListDisplay
Purpose					: Display list of Role Based User List.
Example					: usp_WebHcRoleBasedUserListDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/07/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRoleBasedUserDetails]
(
   
    --Input variable. 	
	 
	  @RoleBasedUserID Int

	  
	  
	--Output Variable 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	            
				
				--Display list of Role Based User List

				DECLARE @catID VARCHAR(1000)
								
				SELECT DISTINCT U.UserID roleUserId 				
				      ,U.UserName 
					  ,Email emailId
				      ,FirstName 
					  ,Lastname
					  --,R.HcRoleName userType
					  , module=  REPLACE(STUFF((SELECT ', ' + CAST(F.HcRoleBasedModuleListID AS VARCHAR(2000))
                                                                                                     FROM HcUserRole F
                                                                                                     WHERE HcAdminUserID=@RoleBasedUserID
																									 --F.HcMenuItemID = MI.HcMenuItemID AND F.BusinessCategoryID =FI.BusinessCategoryID 
                          --                                                                           ORDER BY F.HcMenuFindRetailerBusinessCategoryID
                                                                                                     FOR XML PATH(''))
                                                                                                     , 1, 2, ''), ' ', '')







					  ,U.Enabled userStatus
					  --,EvenTCategories=IIF(RU.UserID IS NULL,Null, (Select  DISTINCT COALESCE(HcEventCategoryID,@catID,',') From HcRoleBasedUserEventCategoryAssociation WHERE UserID =@RoleBasedUserID))
				
				    , EventCategory=REPLACE(STUFF((SELECT distinct ', ' + CAST(F.HcEventCategoryID AS VARCHAR(100))
											 FROM HcRoleBasedUserEventCategoryAssociation  F
											 WHERE F.UserID = @RoleBasedUserID 
											 FOR XML PATH(''))
											, 1, 2, ''), ' ', '') 
                    
				    , fundraiserCategory=REPLACE(STUFF((SELECT distinct ', ' + CAST(F.HcFundraisingCategoryID AS VARCHAR(100))
											 FROM HcFundraisingRoleBasedUserCategoryAssociation  F
											 WHERE F.RoleBasedUserID = @RoleBasedUserID 
											 FOR XML PATH(''))
											, 1, 2, ''), ' ', '') 
                   --, RML.HcRoleBasedModuleListID module
				FROM Users U
				INNER JOIN HcUserRole UR ON UR.HcAdminUserID =U.UserID
				INNER JOIN HcRole R ON R.HcRoleID=UR.HcRoleID
				--INNER JOIN HcRoleBasedModuleAssociation RM ON RM.HcRoleID =UR.HcRoleID  	
				--INNER JOIN HcRoleBasedModuleList RML ON RML.HcRoleBasedModuleListID =RM.HcRoleBasedModuleListID 			
				LEFT JOIN HcRoleBasedUserEventCategoryAssociation RU ON RU.UserID =UR.HcAdminUserID 
				LEFT JOIN HcFundraisingRoleBasedUserCategoryAssociation F ON F.RoleBasedUserID = UR.HcAdminUserID
				WHERE U.UserID  =@RoleBasedUserID			
							
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcRoleBasedUserListDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
