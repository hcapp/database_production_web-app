USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstCategorySubCategoryList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNewsCategoryList]
Purpose					: To display list of News categories.
Example					: [usp_WebHcNewsCategoryList]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[usp_WebHcFindCategoryList]
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstCategorySubCategoryList]
(   
	--Input variable
	 @HcHubCitiID int 

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			;WITH CTE AS(

			SELECT DISTINCT  I.NewsCategoryID AS catID
							,I.NewsCategoryDisplayValue AS catName
						    ,NULL as subCatId
							,NULl AS subCatName
							
			FROM NewsFirstCategory I
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = I.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID
			INNER JOIN NewsFirstSettings NN ON NN.NewsCategoryID = I.NewsCategoryID AND NN.HcHubCitiID = @HcHubCitiID
			LEFT JOIN NewsFirstSubCategorySettings T ON T.NewsCategoryID = I.NewsCategoryID  AND T.HcHubCitiID = @HcHubCitiID 
			WHERE T.NewsCategoryID IS NULL AND I.Active =1

			UNION

						
			SELECT DISTINCT  I.NewsCategoryID AS catID
							,I.NewsCategoryDisplayValue AS catName
						    ,CC.NewsSubCategoryID as subCatId
							,CC.NewsSubCategoryDisplayValue AS subCatName
							
			FROM NewsFirstCategory I
			INNER JOIN NewsFirstSettings NN ON NN.NewsCategoryID = I.NewsCategoryID AND NN.HcHubCitiID = @HcHubCitiID
			INNER JOIN NewsFirstSubCategoryType T ON T.NewsCategoryID = I.NewsCategoryID
			INNER JOIN NewsFirstSubCategory CC ON CC.NewsSubCategoryTypeID = T.NewsSubCategoryTypeID
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = I.NewsCategoryID  AND A.hchubcitiID = @HcHubCitiID
			INNER JOIN NewsFirstSubCategorySettings SS ON SS.NewsSubCategoryID  = CC.NewsSubCategoryID AND SS.HcHubCitiID = @HcHubCitiID 
			WHERE I.Active = 1

			--UNION

			--SELECT DISTINCT  NULL AS catID
			--				,NewsLinkCategoryName AS catName
			--			    ,NULL as subCatId
			--				,NULL AS subCatName
							
			--FROM NewsFirstSettings  
			--WHERE HcHubCitiID = @HcHubCitiID AND IsNewsFeed =  0
								
			) select * from CTE ORDER BY catName,subCatName


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
