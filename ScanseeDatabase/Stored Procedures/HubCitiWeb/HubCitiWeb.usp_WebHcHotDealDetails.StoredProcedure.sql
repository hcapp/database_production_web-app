USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHotDealDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHotDealDetails
Purpose					: 
Example					: usp_WebHcHotDealDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30 april 2015   Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHotDealDetails]
(
	  @HotDealID int
	, @HcHubcitiID int 	
	
	--Output Variable 
	--, @HotDealExpired bit output

	--, @ShareText varchar(500) output
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output  
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			--Retrieve the server configuration.
			DECLARE @Config varchar(100)
			DECLARE @RetailConfig varchar(50)
			DECLARE @ManufConfig varchar(50)

            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 

			--To get the text used for share.         
            --SELECT @ShareText = ScreenContent
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti HotDeal Share Text'  
			
			--To get server Configuration.
            SELECT @RetailConfig= ScreenContent  
            FROM AppConfiguration   
            WHERE ConfigurationType='Web Retailer Media Server Configuration'  

			SELECT @ManufConfig=(select ScreenContent  
						 FROM AppConfiguration   
						 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')

			SELECT @IOSAppID = IOSAppID
			  ,@AndroidAppID = AndroidAppID
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID

			--SET @HotDealExpired = 0

			SELECT DISTINCT HotDealName hdName
			       ,HotDealShortDescription hdShortDesc
				   ,HotDeaLonglDescription hdLongDesc
				   --,HotDealURL hdURL
				   ,HotDealStartDate hDStartDate
				   ,HotDealEndDate hDEndDate
				   ,hdImgPath = CASE WHEN HotDealImagePath IS NOT NULL THEN   
														CASE WHEN WebsiteSourceFlag = 1 THEN 
																	CASE WHEN ManufacturerID IS NOT NULL THEN @ManufConfig  
																		+CONVERT(VARCHAR(30),ManufacturerID)+'/'  
																	   +HotDealImagePath  
																	ELSE @RetailConfig  
																	+CONVERT(VARCHAR(30),RetailID)+'/'  
																	+HotDealImagePath  
																	END  					                         
														ELSE HotDealImagePath  
														END  
								END 
				  --,QRUrl = @Config+'2600.htm?key1='+ CAST(@HotDealID AS VARCHAR(10))
				  , qrUrl = @Config+'2600.htm?hotdealId='+ CAST(@HotDealID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HcHubCitiId AS VARCHAR(10))				  
				  , HotDealURL hdUrl
				  , hdLocFlag=CASE WHEN PR.ProductHotDealID IS NOT NULL THEN 1 ELSE 0 END
			FROM ProductHotDeal P
			LEFT JOIN ProductHotDealRetailLocation PR ON P.ProductHotDealID =PR.ProductHotDealID  
			WHERE P.ProductHotDealID=@HotDealID
			--AND GETDATE() Between HotDealStartDate AND HotDealEndDate
			
			--SELECT @HotDealExpired=ISNULL(CASE WHEN GETDATE()>HotDealEndDate Then 0 
			--							WHEN GETDATE()<=HotDealEndDate THEN 0 END, 1) FROM ProductHotDeal
			--WHERE ProductHotDealID=@HotDealID

			SET @Status = 0
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcHotDealDetails.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status = 1
			
		END;
		 
	END CATCH;
END;




















GO
