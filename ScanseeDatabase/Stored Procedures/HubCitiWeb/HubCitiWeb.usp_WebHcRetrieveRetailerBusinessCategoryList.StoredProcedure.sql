USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRetrieveRetailerBusinessCategoryList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcRetrieveRetailerBusinessCategoryList
Purpose					: To display list of BusinessCategories for Find Module.
Example					: usp_WebHcRetrieveRetailerBusinessCategoryList

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			15 Oct 2013	     SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRetrieveRetailerBusinessCategoryList]
(   
  
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--SELECT BusinessCategoryID catId
			--	  ,BusinessCategoryName catName
			--FROM BusinessCategory
			--WHERE Active = 1
			--AND BusinessCategoryName <> 'Other'
			--ORDER BY BusinessCategoryName

			SELECT B.BusinessCategoryID catId
				  ,B.BusinessCategoryName catName
				  ,SB.HcBusinessSubCategoryID subCatId
				  ,SB.BusinessSubCategoryName subCatName
			FROM BusinessCategory B
			LEFT JOIN HcBusinessSubCategoryType BST on B.BusinessCategoryID = BST.BusinessCategoryID
			LEFT JOIN HcBusinessSubCategory SB on BST.HcBusinessSubCategoryTypeID = SB.HcBusinessSubCategoryTypeID
			WHERE Active = 1
			AND BusinessCategoryName <> 'Other'
			ORDER BY BusinessCategoryName,BusinessSubCategoryName
			
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcRetrieveRetailerBusinessCategoryList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
