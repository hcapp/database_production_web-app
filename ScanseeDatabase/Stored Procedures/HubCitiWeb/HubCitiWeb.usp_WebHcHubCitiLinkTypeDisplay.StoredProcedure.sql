USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiLinkTypeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiLinkTypeDisplay
Purpose					: To display all existing link types.
Example					: usp_WebHcHubCitiLinkTypeDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span			1.0
1.1			25/12/2016		Bindu T A		Side Navigation Changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiLinkTypeDisplay]
(
  
   	  @HubCitiID int
	 ,@HubCitiSideMenu bit 

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @HubCitiName varchar(100)

			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID

			SET @HubCitiSideMenu = ISNULL(@HubCitiSideMenu,'0')


			IF (@HubCitiName = 'spanqa.regionsapp' OR @HubCitiName = 'AddisonTX') AND @HubCitiSideMenu = 0 
			BEGIN
				SELECT HcLinkTypeID menuTypeId
				  ,LinkTypeName menuTypeVal
				  ,LinkTypeDisplayName menuTypeName
				FROM HcLinkType 
				WHERE LinkTypeName NOT IN ('Dining' ,'Feedback','Login/Logout')
				AND Active = 1
				ORDER BY menuTypeName ASC 	
			END
			ELSE IF (@HubCitiName <> 'spanqa.regionsapp' OR @HubCitiName <> 'AddisonTX') AND @HubCitiSideMenu = 0 
			BEGIN
				--To display all existing link types

				--Hubregion/Hubciti check 
				IF EXISTS(SELECT 1 FROM HcHubCiti H
				          INNER JOIN HcAppList A ON H.HcAppListID=A.HcAppListID AND A.HcAppListName='RegionApp' AND H.HcHubCitiID=@HubCitiID)  

                BEGIN
				--To display Link types of HubRegion
				SELECT HcLinkTypeID menuTypeId
					  ,LinkTypeName menuTypeVal
					  ,LinkTypeDisplayName menuTypeName
				FROM HcLinkType 
				WHERE LinkTypeName NOT IN('Dining' ,'GovQA','Feedback','Login/Logout')
				AND Active = 1
				ORDER BY menuTypeName ASC 
				END
				
				ELSE

				BEGIN
				--To display Link types of Hubciti
				SELECT HcLinkTypeID menuTypeId
					  ,LinkTypeName menuTypeVal
					  ,LinkTypeDisplayName menuTypeName
				FROM HcLinkType 
				WHERE LinkTypeName NOT IN('Dining' ,'GovQA', 'City Favorites','Feedback','Login/Logout')
				AND Active = 1
				ORDER BY menuTypeName ASC 
				END	
			END		
			

			IF @HubCitiSideMenu = 1
			BEGIN


				--Hubregion/Hubciti check 
				IF EXISTS(SELECT 1 FROM HcHubCiti H
				          INNER JOIN HcAppList A ON H.HcAppListID=A.HcAppListID AND A.HcAppListName='RegionApp' AND H.HcHubCitiID=@HubCitiID)  

				BEGIN
							--To display Link types of HubRegion
							SELECT HcLinkTypeID menuTypeId
								  ,LinkTypeName menuTypeVal
								  ,LinkTypeDisplayName menuTypeName
							FROM HcLinkType 
							WHERE LinkTypeName NOT IN('Dining' ,'GovQA', 'Settings','Feedback','Login/Logout','Share','My Accounts')
							AND Active = 1
							ORDER BY menuTypeName ASC 
				END
				
				ELSE

						BEGIN
						--To display Link types of Hubciti
						SELECT HcLinkTypeID menuTypeId
							  ,LinkTypeName menuTypeVal
							  ,LinkTypeDisplayName menuTypeName
						FROM HcLinkType 
						WHERE LinkTypeName NOT IN('Dining' ,'GovQA', 'City Favorites','Settings','Feedback','Login/Logout','Share')
						AND Active = 1
						ORDER BY menuTypeName ASC 
						END	
			END		
	
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiLinkTypeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
