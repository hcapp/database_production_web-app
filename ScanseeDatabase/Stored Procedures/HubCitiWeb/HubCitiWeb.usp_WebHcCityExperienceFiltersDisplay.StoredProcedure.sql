USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceFiltersDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceFiltersDisplay
Purpose					: To display List fo Filters.
Example					: usp_WebHcCityExperienceFiltersDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceFiltersDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @HcCityExperienceID Int
	, @FilterName Varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(200)
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		    DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   
			
			DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To display List fo Filters
			SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(P.SortOrder,10000), P.HcFilterID ASC) RowNum
			              ,HcFilterID
			              ,FilterName
						  ,HcCityExperienceID
						  ,HcHubCitiID						 
						  ,@Config+cast (@HubCitiID as varchar)+'/'++ButtonImagePath ButtonImagePath				 		
			INTO #Filters
			FROM HcFilter P 
			WHERE HcCityExperienceID=@HcCityExperienceID AND ((@FilterName IS NOT NULL AND FilterName like '%'+@FilterName+'%') OR (@FilterName IS NULL AND 1=1)) 
			
			
			--To capture max row number.  
			SELECT @MaxCnt = COUNT(RowNum) FROM #Filters		
			
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
													
			
			SELECT Rownum
			       ,HcFilterID FilterID
			       ,FilterName
				   ,HcCityExperienceID
				   ,HcHubCitiID					
				   ,ButtonImagePath logoImageName
			FROM #Filters 
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY RowNum 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceFiltersDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
