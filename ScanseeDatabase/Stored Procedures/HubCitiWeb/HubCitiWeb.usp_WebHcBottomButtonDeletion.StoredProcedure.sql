USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcBottomButtonDeletion
Purpose					: Deleting Bottom Button For Hubciti.
Example					: usp_WebHcBottomButtonDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21/10/2013	    SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonDeletion]
(
   ----Input variable.
	  @HcBottomButtonID int
	  
	--Output Variable 
	, @IsAssociated bit output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
		DECLARE @Cnt INT
		DECLARE @Cnt1 INT

		SELECT @Cnt = COUNT(1) 
	    FROM HcMenuBottomButton
        WHERE HcBottomButtonID = @HcBottomButtonID	
		
		SELECT @Cnt1 = COUNT(1) 
	    FROM HcFunctionalityBottomButton 
        WHERE HcBottomButtonID = @HcBottomButtonID	

		IF (@Cnt > 0) OR (@Cnt1 > 0)		
		BEGIN
			SET @IsAssociated = 1
		END
		ELSE
		BEGIN	
			
			--Delete bottom Button association from selected module
			DELETE FROM HcFunctionalityBottomButton 
			WHERE HcBottomButtonID  = @HcBottomButtonID 

			DELETE FROM HcBottomButtonFindRetailerBusinessCategories 
			WHERE HcBottomButonID = @HcBottomButtonID

			DELETE FROM HcBottomButtonEventCategoryAssociation
			WHERE HcBottomButtonID = @HcBottomButtonID

			DELETE FROM HcFundraisingBottomButtonAssociation
			WHERE HcBottomButtonID = @HcBottomButtonID

			DELETE FROM HcBottomButtonFilterAssociation
			WHERE HcBottomButtonID = @HcBottomButtonID
			
			DELETE FROM HcBottomButton
			WHERE HcBottomButtonID = @HcBottomButtonID	
			
			SET @IsAssociated = 0
		END		
		
	   					
	    --Confirmation of Success.
		SELECT @Status = 0
	COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcBottomButtonDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
