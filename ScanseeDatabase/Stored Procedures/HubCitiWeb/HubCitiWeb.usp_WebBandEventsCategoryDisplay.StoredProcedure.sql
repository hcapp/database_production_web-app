USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebBandEventsCategoryDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebBandEventsCategoryDisplay]
Purpose					: To display list of Band Event categories.
Example					: [usp_WebBandEventsCategoryDisplay]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			4/28/2016		Bindu T A           1.0 - usp_WebBandEventsCategoryDisplay
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebBandEventsCategoryDisplay]
(   
    
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To get the Configuration Details.
			DECLARE @Config Varchar(500)	 
			SELECT @Config = 
			ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			--To display List fo Events Categories
			SELECT DISTINCT HcBandEventCategoryID As catId
						  ,HcBandEventCategoryName As catName
						  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
						  ,CategoryImagePath cateImg
			FROM HcBandEventsCategory  
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebBandEventsCategoryDisplay].'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
