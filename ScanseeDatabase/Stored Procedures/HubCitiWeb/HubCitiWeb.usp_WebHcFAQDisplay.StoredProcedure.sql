USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFAQDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFAQDisplay
Purpose					: Display list of FAQs.
Example					: usp_WebHcFAQDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFAQDisplay]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int		
    , @CategoryName Varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @FAQCategoryId Varchar(MAX) --Comma separated Anythingpageids
    , @FAQId Varchar(2000)
	, @SortOrder VARCHAR (255)
	
	  
	--Output Variable 		
	, @MaxCnt int  output
	, @NxtPageFlag bit output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

	            DECLARE @UpperLimit int

				--To get the row count for pagination.	 
				SELECT @UpperLimit = ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
	            

				IF @SortOrder IS NOT NULL
				BEGIN 
				EXEC [HubCitiWeb].[usp_WebHCFAQSortOrderUpdation] @HcHubcitiID,@FAQCategoryId,@FAQId,@SortOrder, @Status = @Status OUTPUT,@ErrorNumber=@ErrorNumber OUTPUT,@ErrorMessage=@ErrorMessage OUTPUT

				END

				SELECT ROW_NUMBER() OVER (ORDER BY ISNULL(C.sortorder,10000),ISNULL(F.sortorder,10000),C.HcFAQCategoryID, C.CategoryName,Question ASC) As Rownum
				      ,C.HcFAQCategoryID
				      ,C.CategoryName 
				      ,F.HcFAQID
					  ,Question
					  ,Answer
					  ,F.SortOrder
				INTO #FAQ	  				
				FROM HcFAQ F   
				INNER JOIN HcFAQCategory C ON C.HcFAQCategoryID =F.HcFAQCategoryID AND F.HcHubCitiID=@HcHubcitiID
				WHERE (@CategoryName IS NULL OR (@CategoryName IS NOT NULL 
				AND (CategoryName LIKE '%'+@CategoryName+'%' OR Question LIKE '%'+@CategoryName+'%' OR Answer LIKE '%'+@CategoryName+'%')))
				--ORDER BY C.CategoryName,Question			
				
				--To capture max row number.  
				SELECT @MaxCnt = MAX(RowNum) FROM #FAQ
				 
				--this flag is a indicator to enable "More" button in the UI.   
				--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@UpperLimit+@LowerLimit)) > 0 THEN 1 ELSE 0 END 


				SELECT Rownum 
				      ,HcFAQCategoryID faqCatId
				      ,CategoryName faqCatName
				      ,HcFAQID FAQID
					  ,Question
					  ,Answer
					  ,SortOrder sortOrder
				FROM #FAQ 
				Order By Rownum 
                OFFSET @lowerlimit Rows
                FETCH NEXT @UpperLimit ROWS ONLY


	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
