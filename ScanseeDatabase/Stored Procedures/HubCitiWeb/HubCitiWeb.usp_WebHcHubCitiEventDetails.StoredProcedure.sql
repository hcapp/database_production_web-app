USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventDetails
Purpose					: To display Event Details.
Example					: usp_WebHcHubCitiEventDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventDetails]
(   
    --Input variable.	  
	  @HcEventID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @HcEventCategoryID Varchar(2000)
			DECLARE @AppSiteID Int
			DECLARE @RetailLocationID Varchar(2000)
			DECLARE @HotelFlag Bit
			DECLARE @Config VARCHAR(500)
			DECLARE @WeeklyDays VARCHAR(1000)

	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT @HcEventCategoryID=COALESCE(@HcEventCategoryID+',', '')+CAST(HcEventCategoryID AS VARCHAR)
			FROM HcEventsCategoryAssociation 
			WHERE HcEventID =@HcEventID 

			SELECT @AppSiteID=HcAppsiteID 
			FROM HcEventAppsite 
			WHERE HcEventID =@HcEventID 

			--SELECT @AppSiteID=HcAppsiteID 
			--FROM HcEventAppsite 
			--WHERE @HcEventID =@HcEventID 
            
			--Select Event Package if Exist
			SELECT @RetailLocationID=COALESCE(@RetailLocationID+',', '')+CAST(RetailLocationID AS VARCHAR)
			FROM HcEventPackage 
			WHERE HcEventID =@HcEventID
			
			SELECT @HotelFlag=CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
			FROM HcEventPackage 
			WHERE HcEventID =@HcEventID  
			
			SELECT @WeeklyDays=COALESCE(@WeeklyDays+',', '')+CAST([DayName] AS VARCHAR)
			FROM HcEventInterval			
			WHERE HcEventID =@HcEventID
			
			SET @WeeklyDays=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@WeeklyDays, 'Sunday', '1'), 'Monday', '2'), 'Tuesday', '3'), 'Wednesday', '4'), 'Thursday', '5'), 'Friday', '6'), 'Saturday', '7')

			--To display Event Details.
			SELECT DISTINCT HcEventName hcEventName 
			               ,ShortDescription shortDescription
                           ,LongDescription longDescription 								
						   ,E.HcHubCitiID 
						   ,eventImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
						   ,ImagePath eventImageName
						   ,eventListingImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+EventListingImagePath 
						   ,eventListingImagePath eventListingImageName
						   ,BussinessEvent bsnsLoc
						   ,PackageEvent evntPckg								
						   ,eventStartDate =CAST(StartDate AS DATE)
						   ,eventEndDate =ISNULL(CAST(EndDate AS DATE),NULL)
						   ,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
						   ,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)
						   ,Address
						   ,City
						   ,State
						   ,PostalCode
						   ,Latitude
						   ,Longuitude logitude						
						   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
											  FROM HcMenuItem MI 
											  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
											  WHERE LinkTypeName = 'Events' 
											  AND MI.LinkID = @HcEventID )>0 THEN 1 ELSE 0 END
						  ,@HcEventCategoryID  eventCategory		
						  ,PackageDescription packageDescription 
						  ,PackageTicketURL packageTicketURL
						  ,PackagePrice packagePrice
						  ,@HotelFlag evntHotel	
						  , AppsiteIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(HcAppsiteID AS VARCHAR(10)))
													FROM HcEventAppsite HA
													WHERE HA.HcEventID = E.HcEventID
												    FOR XML PATH( '' )
												  ), 3, 1000 ), ' ', '')
						 , RetailLocationIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(RetailLocationID AS VARCHAR(10)))
													FROM HcEventPackage HP
													WHERE HP.HcEventID = E.HcEventID
												    FOR XML PATH( '' )
												  ), 3, 1000 ), ' ', '')		
						 ,ISNULL(EL.GeoErrorFlag,0) geoError	
						 , MoreInformationURL moreInfoURL
						 ,OnGoingEvent isOnGoing	 	
						 ,E.HcEventRecurrencePatternID recurrencePatternID
						 ,HR.RecurrencePattern recurrencePatternName
						 ,[isWeekDay] = CASE WHEN HR.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
						 ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
						 ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
						 ,ByDayNumber = CASE WHEN HR.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
						 ,DayNumber
						 --,MonthInterval	
						 ,EventFrequency AS EndAfter	
						 ,EventsLogisticFlag isEventLogistics
						 ,EL.EventLocationTitle locationTitle	
						 ,MapDisplayTypeID isPortrtOrLandscp							
			FROM HcEvents E
			LEFT JOIN HcEventLocation EL ON E.HcEventID =EL.HcEventID 
			LEFT JOIN HcEventRecurrencePattern HR ON HR.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
			LEFT JOIN HcEventInterval EI ON E.HcEventID = EI.HcEventID
			LEFT JOIN HcEventsLogistic LL ON LL.HcEventID = E.HcEventID
			WHERE E.HcEventID =@HcEventID
			AND E.Active = 1 
				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
