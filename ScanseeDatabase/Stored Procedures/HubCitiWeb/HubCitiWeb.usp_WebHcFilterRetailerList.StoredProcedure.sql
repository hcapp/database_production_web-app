USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFilterRetailerList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFilterRetailerList
Purpose					: To display Filter RetailLocations.
Example					: usp_WebHcFilterRetailerList

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFilterRetailerList]
(
    --Input variable
      @HubCitiID int
    , @FilterID int
    , @LowerLimit int  
	, @ScreenName varchar(50)
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To get the row count for pagination.  
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1
			
			--To display list of RetailLocations for given Filter
			SELECT Row_num = IDENTITY(INT,1,1)
					, HF.FilterName
					, R.RetailID
					, R.RetailName
					, RL.RetailLocationID
					, RL.Address1
					, RL.City
					, RL.State
					, RL.PostalCode
			INTO #Filter
			FROM HcFilter HF
			INNER JOIN HcFilterRetailLocation FRL ON HF.HcFilterID = FRL.HcFilterID
			INNER JOIN RetailLocation RL ON FRL.RetailLocationID = RL.RetailLocationID AND Active = 1
			INNER JOIN HcLocationAssociation HLA ON HLA.PostalCode = RL.PostalCode
			INNER JOIN Retailer R ON RL.RetailID = R.RetailID
			WHERE HF.HcHubCitiID = @HubCitiID
			AND HF.HcFilterID = @FilterID
			AND (R.RetailerActive=1 AND RL.Active=1)
			ORDER BY RetailName																									
			
			--To capture Max Row Number.  
			SELECT @MaxCnt = COUNT(Row_num) FROM #Filter
					 
			--This flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			SELECT  Row_num
				    , FilterName
					, RetailID
					, RetailName
					, RetailLocationID
					, Address1
					, City
					, State
					, PostalCode 
			FROM #Filter
			WHERE Row_num BETWEEN (@LowerLimit+1) AND @UpperLimit
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFilterRetailerList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
