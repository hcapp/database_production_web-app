USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFAQDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFAQDeletion
Purpose					: Delete selected FAQ.
Example					: usp_WebHcFAQDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFAQDeletion]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int	
	, @HcFAQID Int
	  
	--Output Variable 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	             				
				
				--Delete FAQ from 	HcFAQ	
				DELETE FROM HcFAQ 
				WHERE HcFAQID=@HcFAQID AND HcHubCitiID =@HcHubcitiID

	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
