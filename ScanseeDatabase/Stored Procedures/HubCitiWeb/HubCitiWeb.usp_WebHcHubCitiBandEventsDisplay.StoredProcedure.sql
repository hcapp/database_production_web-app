USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiBandEventsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventsDisplay
Purpose					: To display List fo Events.
Example					: usp_WebHcHubCitiEventsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiBandEventsDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @UserID Int
	, @Searchparameter Varchar(max)
	, @LowerLimit int  
	, @ScreenName varchar(max)


  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int	
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent  
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'


			
			--To display List fo Events

			CREATE TABLE #Events(RowNum INT
								,HcEventID INT
								,HcEventName VARCHAR(max)
								,ShortDescription VARCHAR(max)
								,LongDescription VARCHAR(max)
								,HcHubCitiID INT
								,ImagePath VARCHAR(max)
								,BussinessEvent INT
								,PackageEvent INT
								,HotelEvent INT
								,StartDate Date
								,EndDate Date
								,StartTime Time
								,EndTime Time
								,MenuItemExist INT
								,Address VARCHAR(max)
								,City VARCHAR(50)
								,State VARCHAR(10)
								,PostalCode VARCHAR(10)
								,isBandAssociated bit
							) 


				INSERT INTO #Events(RowNum 
								,HcEventID 
								,HcEventName 
								,ShortDescription 
								,LongDescription 
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,StartDate
								,EndDate 
								,StartTime
								,EndTime 							
								,Address
								,City 
								,State 
								,PostalCode 
								,isBandAssociated
								)

				SELECT			RowNum 
								,HcBandEventID 
								,HcBandEventName 
								,ShortDescription 
								,LongDescription 
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent 
								,StartDate
								,EndDate 
								,StartTime
								,EndTime 
								,Address
								,City 
								,State 
								,PostalCode 
								,isBandAssociated
								
				FROM(
				SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY E.HcbandEventID) Rownum
							   ,E.HcBandEventID 
							   ,HcBandEventName  
							   ,ShortDescription
							   ,LongDescription 								
							   ,E.HcHubCitiID 
							   ,ImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
							   ,BussinessEvent 								
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =isnull(CAST(StartDate AS Time),'00:00')
							   ,EndTime =isnull(CAST(EndDate AS Time),'00:00')
					
							  ,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcBandRetailerEventsAssociation A 
																			  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcBandEventID = E.HcBandEventID)
																		 ELSE HE.Address 
										 END
							  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcBandRetailerEventsAssociation A 
																			  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcBandEventID = E.HcBandEventID)
																		 ELSE HE.City 
									  END
							  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcBandRetailerEventsAssociation A 
																			  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcBandEventID = E.HcBandEventID)
																		 ELSE HE.State 
									  END
							  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcBandRetailerEventsAssociation A 
																			  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcBandEventID = E.HcBandEventID)
																		 ELSE HE.PostalCode 
									  END  	
							  ,GeoErrorFlag = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(HE.GeoErrorFlag,0) END
							  ,MoreInformationURL moreInfoURL	
							  ,isBandAssociated = CASE WHEN	aa.BandID is not null then '1' else '0' end						
				FROM HcBandEvents E 		
				INNER JOIN HcBandEventsCategoryAssociation EA ON E.HcBandEventID = EA.HcBandEventID 				
				LEFT JOIN HcBandEventLocation HE on HE.HcBandEventID = E.HcBandEventID
				LEFT JOIN HcBandEventsAssociation AA ON AA.bandid = e.bandid
				WHERE (( @Searchparameter IS NOT NULL AND HcBandEventName LIKE '%'+@Searchparameter+'%') OR (@Searchparameter IS NULL AND 1=1))			
				AND E.HcHubCitiID = @HubCitiID AND E.Active = 1 --AND E.CreatedUserID = @UserID 
				GROUP BY E.HcbandEventID
					    ,HcbandEventName
						,ShortDescription
						,LongDescription
						,E.HcHubCitiID
						,ImagePath
						,BussinessEvent
						,StartDate
						,EndDate
						,Address
						,City
						,State
						,PostalCode
						,GeoErrorFlag	
						,MoreInformationURL	
						,aa.BandID			
			) as a
			
			
              
			


			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Events

			IF @LowerLimit IS NULL
				BEGIN
					SET @LowerLimit=0
					SET @UpperLimit=@MaxCnt					
				END
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SELECT Rownum	
			      ,HcEventID hcEventID
			      ,HcEventName hcEventName					
				  ,HcHubCitiID 		
				  ,StartDate eventStartDate
				  ,EndDate eventEndDate
				  ,left(StartTime,5) eventStartTime
				  ,left(EndTime,5)	eventEndTime			
				  ,Address
				  ,City
				  ,State
				  ,PostalCode	  
				  ,isBandAssociated				  											
			FROM #Events
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY rownum 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












GO
