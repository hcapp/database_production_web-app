USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiBandEventsUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcHubCitiBandEventsCreation]
Purpose					: Create Band Events.
Example					: [usp_WebHcHubCitiBandEventsCreation]

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			12th Sept 2016    Sagar Byali		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiBandEventsUpdation]
(
   ----Input variable. 
   
   	  @HcEventID int 
	, @UserID Int
	, @HcHubCitiID Int

	, @HcEventName Varchar(MAX)
	, @ShortDescription Varchar(MAX)
    , @LongDescription Varchar(MAX) 
	, @HcEventCategoryID int
	, @ImagePath Varchar(MAX) 

	, @BussinessEvent Bit 
	, @StartDate DATE
	, @StartTime TIME

	, @Address Varchar(MAX)
    , @City varchar(MAX)
    , @State Varchar(MAX)
    , @PostalCode VARCHAR(MAX)
    , @Latitude Float
    , @Longitude Float

	, @RetailID int
	, @RetailLocationID VARCHAR(MAX)

	, @OngoingEvent bit 
	, @EventListingImagePath Varchar(MAX) 	
	, @EndTime time
	, @EndDate date
	, @RecurrencePatternID int
	, @RecurrenceInterval int
	, @EveryWeekday bit
	, @Days varchar(MAX)
	, @EndAfter int
	, @DayNumber int
	, @EventLocationTitle Varchar(MAX)
	, @MoreInformationURL varchar(max)
	, @BandTicketURL varchar(max)
	, @GeoErrorFlag bit

	--Output Variable 
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(MAX) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
								

			SELECT @RetailLocationID = RetailLocationID 
			FROM RetailLocation 
			WHERE Address1 = @RetailLocationID   
		
			DECLARE @RecurrencePattern varchar(10)
			SELECT @EveryWeekday = ISNULL(@EveryWeekday, 0)

			SELECT @RetailID = REPLACE(@RetailID, 'NULL', '0')
			SELECT @RetailLocationID = REPLACE(@RetailLocationID, 'NULL', '0')

			SELECT @Days = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Days, '1', 'Sunday'), '2', 'Monday'), '3', 'Tuesday'), '4', 'Wednesday'), '5', 'Thursday'), '6', 'Friday'), '7', 'Saturday')

			SELECT @RecurrencePattern = RecurrencePattern FROM HcEventRecurrencePattern WHERE HcEventRecurrencePatternID = @RecurrencePatternID

			IF @EveryWeekday = 1
			BEGIN
				SELECT @Days = 'Monday,Tuesday,Wednesday,Thursday,Friday'				
			END
		    
		
			--Updating New Event		
			UPDATE HcBandEvents SET HcBandEventName = LTRIM(RTRIM(@HcEventName))  
			                    ,ShortDescription = @ShortDescription
                                ,LongDescription = @LongDescription								
								,HcHubCitiID = @HcHubCitiID
								,ImagePath = @ImagePath 
								,EventListingImagePath = @EventListingImagePath
								,BussinessEvent = @BussinessEvent							
								,StartDate = CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
								,EndDate = IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + ISNULL(CAST(@EndTime AS DATETIME),'00:00:00'))
								,DateModified = GETDATE() 
								,ModifiedUserID = @UserID
								,MoreInformationURL= @MoreInformationURL
								,OnGoingEvent = @OngoingEvent
								,HcEventRecurrencePatternID = IIF(@OngoingEvent = 1, @RecurrencePatternID, NULL) 
								,RecurrenceInterval = IIF(@OngoingEvent = 1, CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
																THEN @RecurrenceInterval 
															ELSE NULL 
													   END, NULL)
								,EventFrequency =  IIF(@OngoingEvent = 1, @EndAfter, NULL)
								,BandTicketURL = @BandTicketURL
			WHERE HcBandEventID = @HcEventID				
			
			DELETE FROM HcbandEventsCategoryAssociation 
			WHERE HcbandEventID =@HcEventID 

			INSERT INTO HcbandEventsCategoryAssociation(HcbandEventCategoryID 
												   ,HcbandEventID 
												   ,HcHubCitiID
												   ,DateCreated
												   ,CreatedUserID)	
			SELECT @HcEventCategoryID
			      ,@HcEventID 
			      ,@HcHubCitiID
			      ,GETDATE()
			      ,1
				
			
			DELETE FROM HcBandEventLocation 
			WHERE HcbandEventID = @HcEventID 

			DELETE FROM HcBandRetailerEventsAssociation
			WHERE HcbandEventID = @HcEventID 
			
			---Insert Event Details to Event Location Table
			IF @BussinessEvent = 0
			BEGIN

			 
				INSERT INTO HcBandEventLocation(HcBandEventID
											   ,HcHubCitiID
											   ,Address
											   ,City
											   ,State
											   ,PostalCode
											   ,Latitude
											   ,Longitude
											   ,DateCreated
											   ,CreatedUserID
											   ,EventLocationTitle
											   ,GeoErrorFlag)
				SELECT @HcEventID 
					  ,@HcHubCitiID 
					  ,@Address 
					  ,@City
					  ,@State 
					  ,@PostalCode 
					  ,@Latitude
					  ,@Longitude 
					  ,GETDATE()
					  ,1
					  ,@EventLocationTitle 
					  ,@GeoErrorFlag
			END



			

			IF @BussinessEvent=1
			BEGIN
			
			INSERT INTO HcBandRetailerEventsAssociation(HcBandEventID,
														RetailID,
														RetailLocationID,
														DateCreated,
														DateModified,
														CreatedUserID)
														
				SELECT @HcEventID 
					  ,@RetailID
					  ,@RetailLocationID
					  ,GETDATE()
					  ,GETDATE()
					  ,1 
			END
				
		
		
			--For On Going Events insert the ongoing pattern into the association tables.
			IF @OngoingEvent = 1
			BEGIN
				--If the event is created as Daily with a inteval value then do not insert record into HcEventInterval table.
				--IF ((@RecurrencePattern <> 'Daily' AND @EveryWeekday <> 0) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				IF ((@RecurrencePattern = 'Weekly' OR @RecurrencePattern = 'Monthly' ) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				BEGIN


					DELETE FROM HcBandEventInterval WHERE HcBandEventID = @HcEventID
					INSERT INTO HcBandEventInterval(HcBandEventID
											  , DayNumber
											  , MonthInterval
											  , DayName
											  , DateCreated
											  , CreatedUserID)
									SELECT @HcEventID
										 , @DayNumber
										 , IIF(@RecurrencePattern ='Monthly', @RecurrenceInterval, NULL)
										 , LTRIM(RTRIM(REPLACE(REPLACE(Param, '[', ''), ']', '')))
										 , GETDATE()
										 , 1
									FROM [HubCitiWeb].fn_SplitParam(@Days, ',') A
				END
			END
			

	
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 

		THROW;
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiBandEventsCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
