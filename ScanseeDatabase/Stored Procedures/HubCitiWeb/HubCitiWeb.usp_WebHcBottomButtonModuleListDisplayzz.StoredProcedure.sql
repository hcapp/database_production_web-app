USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonModuleListDisplayzz]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBottomButtonModuleListDisplay]
Purpose					: To Display list of Modules for associating BottomButtons.
Example					: [usp_WebHcBottomButtonModuleListDisplay]

History
Version		  Date			Author	   Change Description
--------------------------------------------------------------- 
1.0			04 jul 2014	     SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonModuleListDisplayzz]
(

	--Input Variable 
	  @HcHubCitiID int
    , @UserID int 
	    
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @RegionApp BIT

			IF EXISTS(SELECT 1 FROM HcHubCiti H
					  INNER JOIN HcAppList A ON H.HcAppListID=A.HcAppListID AND A.HcAppListName='RegionApp' AND H.HcHubCitiID=@HcHubCitiID)
			SET @RegionApp = 1
			ELSE
			SET @RegionApp = 0
				

			--To Display list of Modules
			IF @RegionApp = 1

			BEGIN	
			SELECT HcLinkTypeID bottomBtnId
				  , LinkTypeDisplayName bottomBtnName
			FROM HcLinkType
			WHERE ActiveBottomButton = 1 AND LinkTypeDisplayName <>'My Deals'
			AND ((LinkTypeName = 'Filters' AND (SELECT COUNT(1) FROM HcFilter WHERE HcHubCitiID = @HcHubCitiID)>0)
			OR (LinkTypeName <> 'Filters')) AND LinkTypeName NOT IN ('City Favorites','Location Preferences','User Information','Category Favorites','GovQA','news')				
			ORDER BY LinkTypeDisplayName 
			END

			ELSE IF @RegionApp = 0

			BEGIN
			SELECT HcLinkTypeID bottomBtnId
				  , LinkTypeDisplayName bottomBtnName
			FROM HcLinkType
			WHERE ActiveBottomButton = 1 AND LinkTypeDisplayName <>'My Deals'
			AND ((LinkTypeName = 'Filters'  AND (SELECT COUNT(1) FROM HcFilter WHERE HcHubCitiID = @HcHubCitiID)>0)
			OR (LinkTypeName <> 'Filters')) AND LinkTypeName NOT IN ('City Favorites','Location Preferences','User Information','Category Favorites','GovQA','news')		
			ORDER BY LinkTypeDisplayName
			END


			
			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcBottomButtonModuleListDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
