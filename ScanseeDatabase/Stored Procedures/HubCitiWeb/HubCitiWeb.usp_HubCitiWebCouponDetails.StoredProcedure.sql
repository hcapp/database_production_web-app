USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HubCitiWebCouponDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HubCitiWebCouponDetails
Purpose					: To Display coupon details in HubCiti.
Example					: usp_HubCitiWebCouponDetails

History
Version		Date			Author			Change Description
-----------------------------------------------------------------
1.0        12/6/2016	  Shilpashree	    Initial version
-----------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_HubCitiWebCouponDetails]
(
	  @CouponID int

	--Output Variable 
	, @Status int output  
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--To get Media Server Configuration.
		 DECLARE @Config varchar(50)  
		 DECLARE @RetailerLocations VARCHAR(Max)
		 --DECLARE @ProductCategoryIDs Varchar(Max)

		 SELECT @Config=ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Hubciti Media Server Configuration' 

		 SELECT @RetailerLocations = COALESCE(@RetailerLocations + ',','') + CAST(RetailLocationID AS VARCHAR(1000)) 
		 FROM CouponRetailer 
		 WHERE CouponID = @CouponID

		 --SELECT @ProductCategoryIDs = COALESCE(@ProductCategoryIDs + ',','') + CAST(ProductCategoryID AS VARCHAR(1000))
		 --FROM CouponProductCategoryAssociation
		 --WHERE CouponID = @CouponID

		SELECT DISTINCT C.CouponID couponId
			   ,CouponName AS coupTitle
			   ,CR.RetailID AS retailerId	
			   ,R.RetailName AS retailerName		   
			   ,BannerTitle coupBanTitle
			   ,coupImgName = @Config + CAST(HcHubCitiID as varchar(10))+'/' + CouponImagePath 
			   ,dealLstImgPath = @Config + CAST(HcHubCitiID as varchar(10))+'/' + CouponDetailImage 
			   ,CouponImagePath AS coupImg
			   ,CouponDetailImage AS dealLstImgName
			   ,CouponShortDescription AS coupDescription
			   ,CouponTermsAndConditions AS coupTermsConds
			   ,NoOfCouponsToIssue AS coupNum
			   ,CAST(CouponStartDate AS DATE) AS coupStartDate
			   ,CAST(CouponExpireDate AS DATE) AS coupEndDate
			   ,CAST(ActualCouponExpirationDate AS DATE) AS coupExpiryDate
			   ,LEFT(CAST(CouponStartDate AS TIME),5) AS coupStaTime
			   ,LEFT(CAST(CouponExpireDate AS TIME),5) AS coupEndTime
			   ,LEFT(CAST(ActualCouponExpirationDate AS TIME),5) AS coupExpTime
			   ,KeyWords AS coupKeywords
			   ,HcHubCitiID
			   ,@RetailerLocations AS locationId
			   --,@ProductCategoryIDs AS prodCategory
		FROM Coupon C
		LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
		LEFT JOIN Retailer R ON CR.RetailID = R.RetailID
		WHERE C.CouponID=@CouponID

		--Confirmation of Success.
		SELECT @Status = 0

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HubCitiWebCouponDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of failure.
	        SELECT @Status = 1 

		END;
		 
	END CATCH;
END;







GO
