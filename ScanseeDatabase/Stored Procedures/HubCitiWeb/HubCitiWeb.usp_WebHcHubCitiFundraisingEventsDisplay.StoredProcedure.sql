USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingEventsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingEventsDisplay
Purpose					: To display List fo Fundraising Events.
Example					: usp_WebHcHubCitiFundraisingEventsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Aug 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingEventsDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @UserID Int
	, @Searchparameter Varchar(2000)
	, @LowerLimit int  
	, @ScreenName varchar(50)
	
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To get user info i.e Super/Normal user.
			DECLARE @RoleName VARCHAR(100)
			SELECT @RoleName = HcRoleName
			FROM HcUserRole UR
			INNER JOIN HcRole R ON UR.HcRoleID = R.HcRoleID
			WHERE HcAdminUserID = @UserID AND UR.HcRoleBasedModuleListID=2

			
			--To display List fo Events

			CREATE TABLE #FundraisingEvents(RowNum Int
								,FundraisingID Int	
								,FundraisingName Varchar(1000)							
								,FundraisingOrganizationName Varchar(1000)		
								,AppsiteName Varchar(1000)																						
								,StartDate Date								
								,EndDate Date
								,StartTime Time
								,EndTime Time
								,MenuItemExist Int
								,AppsiteFlag Bit)								
								

			IF @RoleName = 'ROLE_FUNDRAISING_USER'
			BEGIN

				INSERT INTO #FundraisingEvents(RowNum 
								,FundraisingID	
								,FundraisingName							
								,FundraisingOrganizationName 
								,AppsiteName								
								,StartDate								
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist
								,AppsiteFlag)
								

				SELECT			 RowNum		  
								,HcFundraisingID	
								,FundraisingName							
								,FundraisingOrganizationName 
								,HcAppSiteName								
								,StartDate								
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist
								,FundraisingAppsiteFlag
								
				FROM(
				SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY E.HcFundraisingID) RowNum
							   ,E.HcFundraisingID
							   ,FundraisingName 							   
							   ,FundraisingOrganizationName= IIF(FundraisingOrganizationName IS NULL,A.HcAppSiteName,FundraisingOrganizationName)  	
							   ,HcAppSiteName						   						  							
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   ,EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
							   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
												  FROM HcMenuItem MI 
												  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
												  WHERE LinkTypeName = 'Fundraisers' 
												  AND MI.LinkID = E.HcFundraisingID)>0 THEN 1 ELSE 0 END	
							   ,FundraisingAppsiteFlag				  							  									  											
				FROM HcFundraising E 			
				LEFT JOIN HcFundraisingRoleBasedUserCategoryAssociation UE ON E.CreatedUserID = UE.RoleBasedUserID 		
				LEFT JOIN HcFundraisingCategoryAssociation EA ON UE.HcFundraisingCategoryID = EA.HcFundraisingCategoryID 
				LEFT JOIN HcFundraisingAppsiteAssociation FA ON E.HcFundraisingID = FA.HcFundraisingID
				LEFT JOIN HcAppSite A ON FA.HcAppsiteID = A.HcAppSiteID
				WHERE (( @Searchparameter IS NOT NULL AND FundraisingName LIKE '%'+@Searchparameter+'%') OR (@Searchparameter IS NULL AND 1=1))				
				AND E.HcHubCitiID = @HubCitiID AND E.Active = 1 AND E.CreatedUserID = @UserID 
				--AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1)
				GROUP BY E.HcFundraisingID
						,FundraisingName					    
					    ,FundraisingOrganizationName 	
						,A.HcAppsiteName									
						,StartDate						
						,EndDate
						,FundraisingAppsiteFlag												
				) FundraisingEvents 
			END
			ELSE
			BEGIN
				INSERT INTO #FundraisingEvents(RowNum
								,FundraisingID			
								,FundraisingName					
								,FundraisingOrganizationName 
								,AppsiteName								
								,StartDate								
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist
								,AppsiteFlag)
								

				SELECT			 RowNum
								,HcFundraisingID
								,FundraisingName								
								,FundraisingOrganizationName 
								,HcAppsiteName								
								,StartDate								
								,EndDate 
								,StartTime
								,EndTime 
								,MenuItemExist
								,FundraisingAppsiteFlag
								
				FROM(
				SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY E.HcFundraisingID) RowNum
							   ,E.HcFundraisingID 	
							   ,FundraisingName						   
							   ,FundraisingOrganizationName= IIF(FundraisingOrganizationName IS NULL,A.HcAppSiteName,FundraisingOrganizationName) 	
							   ,HcAppSiteName						   							  							
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   ,EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
							   ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
												  FROM HcMenuItem MI 
												  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
												  WHERE LinkTypeName = 'Fundraisers' 
												  AND MI.LinkID = E.HcFundraisingID)>0 THEN 1 ELSE 0 END		
							   ,FundraisingAppsiteFlag				  						  									 
				FROM HcFundraising E
				LEFT JOIN HcFundraisingAppsiteAssociation FA ON E.HcFundraisingID = FA.HcFundraisingID
				LEFT JOIN HcAppSite A ON FA.HcAppsiteID = A.HcAppSiteID
				WHERE (( @Searchparameter IS NOT NULL AND FundraisingName LIKE '%'+@Searchparameter+'%') OR (@Searchparameter IS NULL AND 1=1))				
				AND E.HcHubCitiID = @HubCitiID AND E.Active = 1 --AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1)
				GROUP BY E.HcFundraisingID	
						,FundraisingName				    
					    ,FundraisingOrganizationName 						
						,HcAppSiteName
						,StartDate						
						,EndDate
						,FundraisingAppsiteFlag						
									
				) FundraisingEvents 
			END	               

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #FundraisingEvents
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SELECT 	   		     RowNum 
								,FundraisingID hcEventID
								,FundraisingName hcEventName
								,FundraisingOrganizationName address
								,AppsiteName								
								,StartDate eventStartDate								
								,EndDate eventEndDate
								,StartTime eventTime
								,EndTime 
								,MenuItemExist		
								,AppsiteFlag								  											
			FROM #FundraisingEvents
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY FundraisingName 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingEventsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
