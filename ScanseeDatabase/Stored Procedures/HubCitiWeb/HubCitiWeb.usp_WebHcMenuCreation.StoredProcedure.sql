USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcMenuCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcMenuCreation
Purpose					: Creating Menu For Hubciti.
Example					: usp_WebHcMenuCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcMenuCreation]
(
   ----Input variable.
	  @HubCitiID Int
	--, @HCTemplateName varchar(100)	
	, @LevelID Int
	, @UserID Int
	, @MenuName varchar(100)
	  
	--Output Variable 
	, @HCMenuID Int output
	, @DuplicateFlag Bit OutPut
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	        
	        --DECLARE @HCTemplateID INT
	        --SELECT @HCTemplateID = HcTemplateID
	        --FROM HcTemplate
	        --WHERE TemplateName = @HCTemplateName
	        
	        SET @DuplicateFlag=0
	        IF EXISTS(SELECT 1 FROM HcMenu WHERE Level=@LevelID AND HcHubCitiID = @HubCitiID AND @LevelID=1)
			BEGIN
				PRINT 'A'
				--Creating New Menu For a hubciti
				UPDATE HcMenu SET DateModified = GETDATE()
				                , ModifiedUserID=@UserID				              
				 WHERE HcHubCitiID=@HubCitiID 
				 AND Level=@LevelID				 
				 AND @LevelID=1
					  	    
				--Capture MenuID
				SELECT @HCMenuID = HcMenuID
				FROM HcMenu
				WHERE HcHubCitiID=@HubCitiID AND Level = 1
			END	 
			
			ELSE 
			BEGIN
				--Creating New Menu For a hubciti
				
				   INSERT INTO HcMenu(HcHubCitiID
									--, HcTemplateID
									, Level
									, DateCreated
									, CreatedUserID
									, MenuName)
								VALUES(@HubCitiID
									 --, @HCTemplateID
									 , @LevelID
									 , GETDATE()
									 , @UserID
									 , @MenuName)
					  	    
				--Capture MenuID
				SELECT @HCMenuID = SCOPE_IDENTITY()				
			END			   			
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcMenuCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
