USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventsDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventsDeletion
Purpose					: Deleting Selected  Event.
Example					: usp_WebHcHubCitiEventsDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventsDeletion]
(   
    --Input variable.	  
	 @HubCitiID Int   
    ,@HcEventID Int
	
  	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Logical Delete
			UPDATE HcEvents	SET Active = 0
			WHERE HcEventID = @HcEventID 
			AND HcHubCitiID = @HubCitiID

			--Deleting Selected  Event.

			--DELETE FROM HcEventLocation 
			--WHERE HcEventID =@HcEventID 

			--DELETE FROM HcEventsCategoryAssociation 
			--WHERE HcEventID =@HcEventID 

			--DELETE FROM HcEventAppsite
			--WHERE HcEventID =@HcEventID 

			--DELETE FROM HcEventPackage 
			--WHERE HcEventID =@HcEventID 

			--DELETE FROM HcEventInterval
			--WHERE HcEventID = @HcEventID
												
			--DELETE FROM HcEvents
			--WHERE HcEventID =@HcEventID  

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventsDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
