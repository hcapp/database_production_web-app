USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcMapDisplayType]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteLogisticDetails]
Purpose					: To edit Appsite logistic map details.
Example					: [usp_WebHcAppsiteLogisticDetails]

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcMapDisplayType]
(
	
	--Input Variable
	
    --Output Variables
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			
		SELECT MapDisplayTypeID isPortrtOrLandscp 
			  ,MapDisplayTypeName displayTypeName
		FROM MapDisplayType								
			
			--Confirmation of Success
			SELECT @Status = 0

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteLogisticDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
