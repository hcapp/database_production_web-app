USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiRetailLocationAssociation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiRetailLocationAssociation
Purpose					: To disply the list of the Hub Cities that are created by the SS admin.
Example					: usp_WebHcHubCitiRetailLocationAssociation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24/1/2014	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiRetailLocationAssociation]
(
   ----Input variable.
	  @HcHubcitiID Int  
	 , @UserID Int	
	 
	--Output Variable 
    , @Status int output   
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		
		    --Delete all postalcode association from HcLocationAssociation table.	
			
			IF NOT EXISTS(SELECT 1 FROM HcRetailerAssociation WHERE HcHubCitiID =@HcHubcitiID )
			BEGIN
				INSERT INTO HcRetailerAssociation(HcHubCitiID
											,RetailID
											,RetailLocationID
											,DateCreated
											,CreatedUserID
											,Associated) 
				SELECT DISTINCT @HcHubcitiID 
					  ,RL.RetailID
					  ,RL.RetailLocationID
					  ,GETDATE()
					  ,@UserID 
					  ,1
				FROM HcLocationAssociation L
				INNER JOIN RetailLocation RL ON L.PostalCode = RL.PostalCode AND L.HcCityID = RL.HcCityID AND L.StateID = RL.StateID AND L.HcHubCitiID = @HcHubcitiID 
				LEFT JOIN HcRetailerAssociation HA ON HA.RetailLocationID = RL.RetailLocationID AND HA.HcHubCitiID = @HcHubcitiID 
				WHERE HA.HcRetailerAssociationID IS NULL AND Active = 1
			END
			
			--DELETE FROM HcRetailerAssociation	
			--FROM HcRetailerAssociation A
			--INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID AND A.HcHubCitiID = @HcHubcitiID AND Associated = 1
			--LEFT JOIN HcLocationAssociation B ON RL.HcCityID = B.HcCityID AND B.StateID = RL.StateID AND RL.PostalCode = B.PostalCode AND B.HcHubCitiID = @HcHubcitiID
			--WHERE B.HcLocationAssociationID IS NULL

			ELSE
			BEGIN

			DELETE FROM HcRetailerAssociation	
			FROM HcRetailerAssociation A
			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID AND A.HcHubCitiID = @HcHubcitiID AND Associated = 1
			LEFT JOIN HcLocationAssociation B ON RL.HcCityID = B.HcCityID AND B.StateID = RL.StateID AND RL.PostalCode = B.PostalCode AND B.HcHubCitiID = @HcHubcitiID
			WHERE B.HcLocationAssociationID IS NULL AND Active = 1

			INSERT INTO HcRetailerAssociation(HcHubCitiID
											,RetailID
											,RetailLocationID
											,DateCreated
											,CreatedUserID
											,Associated) 
			SELECT DISTINCT @HcHubcitiID 
				  ,RL.RetailID
				  ,RL.RetailLocationID
				  ,GETDATE()
				  ,@UserID 
				  ,0
			FROM HcLocationAssociation L
			INNER JOIN RetailLocation RL ON L.PostalCode = RL.PostalCode AND L.HcCityID = RL.HcCityID AND L.StateID = RL.StateID AND L.HcHubCitiID = @HcHubcitiID 
			LEFT JOIN HcRetailerAssociation HA ON HA.RetailLocationID = RL.RetailLocationID AND HA.HcHubCitiID = @HcHubcitiID 
			WHERE HA.HcRetailerAssociationID IS NULL AND Active = 1	
			END

			--UPDATE HcRetailerAssociation SET Associated = 1
			--FROM HcLocationAssociation L
			--INNER JOIN RetailLocation RL ON L.PostalCode =RL.PostalCode AND L.HcHubCitiID = @HcHubcitiID 
			--INNER JOIN HcRetailerAssociation HA ON HA.RetailLocationID = RL.RetailLocationID AND HA.HcHubCitiID = @HcHubcitiID
			
			--Confirmation of success.
			SELECT @Status = 0		  
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiRetailLocationAssociation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
