USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventLogisticsDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventLogisticsDetails
Purpose					: To display event logistic details.
Example					: usp_WebHcHubCitiEventLogisticsDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Mar 2015	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventLogisticsDetails]
(   
    --Input variable.	  
	  @HubCitiID int
	, @HcEventID int
	, @UserID int
  
	--Output Variable 
	, @EventLogisticMapURL Varchar(500) Output
	, @EventLogisticImgPath varchar(1000) output
	, @EventsIsOverLayFlag bit output
	, @isPortrtOrLandscp int output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @Config VARCHAR(500)
			DECLARE @SSQRConfig VARCHAR(500)

			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT @SSQRConfig=ScreenContent
			FROM AppConfiguration			
			WHERE ConfigurationType = 'QR Code Configuration'


			--SET @EventLogisticMapURL = @SSQRConfig + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'EventLogisticMapPath') as varchar(10)) + '.htm?eventId=' + CAST(@HcEventID AS VARCHAR(10)) + '&hubcitiId=' + CAST(@HubCitiID AS VARCHAR(10)) + '&oType=' + MM.MapDisplayTypeName

			
			SELECT @EventLogisticImgPath = @Config + CAST(HcHubCitiID AS VARCHAR(100)) +'/events/' + CAST(HcEventID AS VARCHAR(100)) + '/'  +EventsLogisticImagePath
				  ,@EventsIsOverLayFlag = EventsIsOverLayFlag
				  ,@isPortrtOrLandscp = CASE WHEN e.MapDisplayTypeID IS NOT NULL THEN e.MapDisplayTypeID ELSE 1 END
				  ,@EventLogisticMapURL = @SSQRConfig + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'EventLogisticMapPath') as varchar(10)) + '.htm?eventId=' + CAST(@HcEventID AS VARCHAR(10)) + '&hubcitiId=' + CAST(@HubCitiID AS VARCHAR(10)) + '&oType=' + MapDisplayTypeName

			FROM HcEventsLogistic E
			LEFT JOIN MApdisplaytype mm on e.mapdisplaytypeid = mm.mapdisplaytypeid
			WHERE HcEventID = @HcEventID AND HcHubCitiID = @HubCitiID

			SELECT DISTINCT HcEventsLogisticButtonID ButtonId
				  ,EL.HcEventID
				  ,EL.HcHubCitiID
				  ,ButtonName
				  ,ButtonLink
			FROM HcEventsLogisticButtons EL
			INNER JOIN HcEvents E ON EL.HcEventID = E.HcEventID
			WHERE EL.HcEventID = @HcEventID AND EL.HcHubCitiID = @HubCitiID AND E.Active = 1 
			ORDER BY HcEventsLogisticButtonID 
										
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventLogisticsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
