USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAppsiteLogisticsDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteLogisticsDeletion]
Purpose					: To delete Appsite logistic map and its marker details.
Example					: [usp_WebHcAppsiteLogisticsDeletion] 

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAppsiteLogisticsDeletion]
(
	
	--Input Variable
	  @HubCitiID int
	, @HcAppsiteLogisticID int
    
    --Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN	
										
				--To Delete given appsite logistic map and its marker details
				DELETE FROM HcAppsiteMarker
				WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID

				DELETE FROM HcAppsiteOverLayCoordinates
				WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID

				DELETE FROM HcAppsiteLogistic
				WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID
				AND HcHubCitiID = @HubCitiID


			--Confirmation of Success
			SELECT @Status = 0
			COMMIT TRAN	

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteLogisticsDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRAN	
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
