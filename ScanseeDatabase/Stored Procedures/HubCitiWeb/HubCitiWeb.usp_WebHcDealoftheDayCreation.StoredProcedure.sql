USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDealoftheDayCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDelaoftheDayCreation]
Purpose					: To Deal Of the Day for a Hubciti.
Example					: [usp_WebHcDelaoftheDayCreation]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
			15/12/2015      Sagar Byali   
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDealoftheDayCreation]
(
    --Input variable
	  @UserID int
	, @HcHubCitiID Int
	, @DealName Varchar(100)
	, @DealID Int
	, @PushNowFlag BIT
	, @DealScheduleStartDate DATE
	, @DealScheduleEndDate DATE
	
	--Output Variable	
	, @IsExist INT Output  
	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	

			CREATE TABLE #DeviceList(UserToken varchar(1000)
									,Platform varchar(100)
									,HubcitiName varchar(100)
									,PushNotifyBadgeCount int)

	        DECLARE @DealType Int
			DECLARE @Config VARCHAR(1000)

			--Retrieve the server configuration.
			SELECT @Config = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'QR Code Configuration'
			AND Active = 1

			--Code to delete all expired deals 
			DELETE FROM HcDealOfTheDayStaging 
			WHERE DealScheduleEndDate <  CAST(GETDATE() AS DATE) 

			DECLARE @HcHubCitiName VARCHAR(100)
			SELECT @HcHubCitiName = HubCitiName 
			FROM HcHubCiti 
			WHERE HcHubCitiID = @HcHubCitiID

			SELECT @DealType = HcDealsID 
		    FROM HcDeals 
		    WHERE DealDescription =@DealName

			--Setting IsExist to 0 
			SET @IsExist = 0 

			CREATE TABLE #PushDeal  (dealId INT 
									,dealName VARCHAR(1000)
									,type VARCHAR(1000)
									,splUrl VARCHAR(1000)
									,hcName VARCHAR(1000)
									,HcDealsID INT
									,retailerId INT
									,retailLocationId INT
									,expirationDate DATETIME
									
								    )

			--To retrive details of the input deal if it exists in the staging table
			DECLARE @RetriveHcDealID INT
			DECLARE @RetriveDealDescription VARCHAR(100)
			
			SELECT @RetriveHcDealID = D.DealOfTheDayID , @RetriveDealDescription = DS.DealDescription
			FROM HcDealOfTheDayStaging D
			INNER JOIN HcDeals DS ON D.HcDealsID=Ds.HcDealsID
			WHERE DealOfTheDayID = @DealID AND HcHubCitiID = @HcHubCitiID
			AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL

			--To display deals which are set as 'Push Now' and Hubciti is Tyler
			IF (@PushNowFlag = '1')
			BEGIN
					--To display Coupon.
					INSERT INTO #PushDeal(dealId  
										 ,dealName 
										 ,type 
										 ,splUrl
										 ,hcName
										 ,HcDealsID
										 ,retailerId
										 ,retailLocationId
										 ,expirationDate 
										 
										 )
											

					(--To display 'Coupons' as Push Now 
					SELECT DISTINCT TOP 1 S.CouponID dealId
							, S.CouponName dealName
							, D.DealDescription type
							, S.CouponURL splUrl
							, @HcHubCitiName hcName
							, D.HcDealsID
							, null
							, null
							, ISNULL(CouponDisplayExpirationDate,CouponExpireDate) 
							--, CONVERT(DATE,ISNULL(ActualCouponExpirationDate,CouponExpireDate)) expirationDate
							--, CONVERT(VARCHAR(5), ISNULL(ActualCouponExpirationDate,CouponExpireDate),108) expirationTime
					FROM Coupon S 
					INNER JOIN Hcdeals D ON D.DealDescription=@DealName
					WHERE  S.CouponID = @DealID  AND @DealName ='Coupons' 

					UNION ALL

			        --To display 'Special offers' as Push Now 
					SELECT DISTINCT TOP 1 Q.QRRetailerCustomPageID dealId
							, Q.Pagetitle dealName
							, HD.DealDescription type
							, CASE WHEN Q.URL IS NOT NULL THEN Q.url END
							       --WHEN Q.URL IS NULL AND QT.QRTypeName != 'AnyThing Page' THEN @Config + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Special Offer Page') as varchar(10)) + '.htm?key1=' + CAST(QA.RetailID AS VARCHAR(10)) + '&key2=' + CAST(QA.RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(Q.QRRetailerCustomPageID AS VARCHAR(10)) + CASE WHEN Q.URL IS NOT NULL THEN '&EL=true' ELSE '&EL=false' END 
								   --END 
								   AS splUrl
						    , @HcHubCitiName hcName
							, HD.HcDealsID


							,(CASE WHEN (Q.URL IS NULL AND HD.DealDescription = 'SpecialOffers') THEN QA.RetailID END) AS retailerId   
							,(CASE WHEN (Q.URL IS NULL AND HD.DealDescription = 'SpecialOffers') THEN QA.RetailLocationID END)  retailLocationId					
							,Enddate
							--,CONVERT(DATE,Q.Enddate) expirationDate
							--,CONVERT(VARCHAR(8),Q.Enddate, 8) expirationTime
					FROM QRRetailerCustomPage Q 
					INNER JOIN Hcdeals HD ON HD.DealDescription=@DealName
					LEFT JOIN QRRetailerCustomPageAssociation QA ON QA.QRRetailerCustomPageID = Q.QRRetailerCustomPageID
					LEFT JOIN QRTypes QT ON QT.QRTypeID=Q.QRTypeID
					LEFT JOIN RetailLocation RL ON RL.RetailLocationID=QA.RetailLocationID AND RL.Active=1
					WHERE Q.QRRetailerCustomPageID = @DealID  AND @DealName ='Specialoffers' 

					UNION ALL

					--To display 'Hot Deal' as Push Now 
					SELECT DISTINCT TOP 1 S.ProductHotDealID dealId
							, S.HotDealName dealName
							, DD.DealDescription type
							, S.HotDealURL splUrl
							, @HcHubCitiName hcName
							, DD.HcDealsID
							, null
							, null
							, CONVERT(DATETIME,ISNULL(HotDealExpirationDate,HotDealEndDate))
							--, CONVERT(DATE,ISNULL(HotDealExpirationDate,HotDealEndDate)) expDate
							--, CONVERT(VARCHAR(8), ISNULL(HotDealExpirationDate,HotDealEndDate), 8) expTime
					FROM ProductHotDeal S 
					INNER JOIN Hcdeals DD ON DD.DealDescription=@DealName
					WHERE S.ProductHotDealID = @DealID AND @DealName ='HotDeals' 
					)

					--IF NOT EXISTS(SELECT 1 FROM HcDealOfTheDayStaging WHERE DealOfTheDayID = @DealID AND HcHubCitiID = @HcHubCitiID) OR 
						 
					--BEGIN
							UPDATE HcUserToken
							SET PushNotifyBadgeCount =PushNotifyBadgeCount + 1 
							WHERE HcHubCitiID = @HcHubCitiID AND PushNotify = 1
					--END

					

					--To insert into HcDealOfTheDayStaging
					--INSERT INTO HcDealOfTheDayStaging(DealOfTheDayID
					--								 ,DealName
					--								 ,SpecialsQRURL
					--								 ,HcHubCitiID
					--								 ,HcDealsID
					--								 ,DateCreated)



													 INSERT INTO HcDealOfTheDayStaging(HcHubCitiID
																						,DealOfTheDayID
																						,DealName
																																	
																					
																						,DealExpirationDate
																						,HcDealsID
																						,SpecialsQRURL
																						,DateCreated
																						,CreatedUserID
																						,DealScheduleStartDate
																						,DealScheduleEndDate)

					SELECT	 @HcHubCitiID
							,dealId
							,dealName 
							,expirationDate
							,HcDealsID 
							,splUrl
							,GETDATE()
							,@UserID
							,null
							,null
							
					FROM #PushDeal


					--select * from #PushDeal
					

				

					
					SET @IsExist = 1 

					--select 'a'



					
						INSERT INTO #DeviceList
						SELECT UserToken 
							  ,'Android' 					  		
							  ,HubCitiName 
							  ,PushNotifyBadgeCount 
						FROM HcUserToken U
						INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
						--INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID AND D.HcHubCitiID IS NOT NULL
						WHERE PushNotify = 1 AND H.HcHubCitiID =@HcHubCitiID AND U.HcRequestPlatformID=2
						--AND (U.DeviceID = 'c14e4993c33c2fa2' OR U.DeviceID = '29a3832895c3c01')
			 
						UNION ALL
						
						SELECT DISTINCT UserToken 
							  ,'IOS' 					  		
							  ,HubCitiName 
							  ,PushNotifyBadgeCount 
						FROM HcUserToken U
						INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
						--INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID AND D.HcHubCitiID IS NOT NULL
						WHERE PushNotify = 1 AND H.HcHubCitiID =@HcHubCitiID AND U.HcRequestPlatformID=1
						--AND ((U.DeviceID = '8C6A4AD0-FF2F-4BBE-B78F-5C5F18BA9E15' OR U.DeviceID= '8d1f7917bc2e9e91f0036554b2eebb30ab99999d') )
						

			END --End of (@PushNowFlag = '1')
			ELSE IF (@PushNowFlag = '0')
			BEGIN

					----To display PushDeal blank list
					--SELECT   dealId  
					--		,dealName 
					--		,type 
					--		,splUrl
					--		,hcName
					--		,expirationDate expDate
					--		,expirationTime expTime
					--FROM #PushDeal

				--	----To list Device details 
				--	SELECT DISTINCT UserToken deviceId
				--				   ,'Android' platform					  		
				--			       ,HubCitiName hcName
				--	FROM HcUserToken U
				--	INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
				--	WHERE PushNotify = 1 AND H.HcHubCitiID IN (@HcHubCitiID) AND HcRequestPlatformID=2
				--	AND (DeviceID = 'c14e4993c33c2fa2' OR DeviceID = '29a3832895c3c01')
			 
				--	UNION ALL

				--	SELECT DISTINCT UserToken deviceId
				--		  ,'IOS' platform					  		
				--		  ,HubCitiName hcName
				--		--,D.AppVersion
				--	FROM HcUserToken U
				--	INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
				--	INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID
				--	WHERE PushNotify = 1 AND H.HcHubCitiID IN (@HcHubCitiID) AND U.HcRequestPlatformID=1-- AND D.AppVersion = '2.3.2'


				--	AND ((U.DeviceID = '8C6A4AD0-FF2F-4BBE-B78F-5C5F18BA9E15' OR U.DeviceID= '8d1f7917bc2e9e91f0036554b2eebb30ab99999d') )
				----	OR U.DeviceID = 'BAC2571B-BEDE-4FC4-B317-DDDC75E2DB8F'
				--	--OR U.DeviceID = '5BD163B8-0380-46EB-9E94-4F54028CD80F')
				--	ORDER BY platform

					--To check whether Deal exists in Staging table 
					DECLARE @Exist BIT = 0 
					SELECT @Exist = 1 FROM HcDealOfTheDayStaging 
					WHERE ((@DealScheduleStartDate BETWEEN DealScheduleStartDate AND DealScheduleEndDate) OR (@DealScheduleEndDate BETWEEN DealScheduleStartDate AND DealScheduleEndDate)) 
					AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL AND HcHubCitiID = @HcHubCitiID
					--OR (@DealScheduleStartDate  = DealScheduleStartDate OR @DealScheduleEndDate =  DealScheduleEndDate OR @DealScheduleStartDate  = DealScheduleEndDate OR @DealScheduleEndDate  = DealScheduleStartDate)			

					--To check whether to update a Deal which exists in Staging table
					DECLARE @UpdateDeal INT 
					IF EXISTS(SELECT  1 FROM HcDealOfTheDayStaging 
							  WHERE DealOfTheDayID = @DealID AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL AND HcHubCitiID = @HcHubCitiID )
					BEGIN			
							SET @UpdateDeal = 8
					END

					--Flag: Deal cannot be updated 
					SET @IsExist = 3
					 
					--If deal exists in staging table, update the deal
					IF @UpdateDeal = 8
					BEGIN 
							IF (@RetriveDealDescription = 'Hotdeals' AND @RetriveHcDealID = @DealID)
							BEGIN
									IF NOT EXISTS (SELECT 1 FROM HcDealOfTheDayStaging 
											WHERE @RetriveHcDealID ! = DealOfTheDayID AND ((@DealScheduleStartDate BETWEEN DealScheduleStartDate AND DealScheduleEndDate )
											OR (@DealScheduleEndDate  BETWEEN DealScheduleStartDate AND DealScheduleEndDate)) AND HcHubCitiID = @HcHubCitiID)
									BEGIN
											UPDATE HcDealOfTheDayStaging
											SET DealScheduleStartDate = @DealScheduleStartDate,DealScheduleEndDate = @DealScheduleEndDate 
											FROM HcDealOfTheDayStaging D
											INNER JOIN ProductHotDeal PHD ON D.DealOfTheDayID = PHD.ProductHotDealID
											WHERE DealOfTheDayID = @DealID AND HcHubCitiID = @HcHubCitiID
												  AND @DealScheduleStartDate >= ISNULL(DealStartDate,@DealScheduleStartDate) 
												  AND @DealScheduleEndDate <= ISNULL(HotDealEndDate,@DealScheduleEndDate)
					
											SET @IsExist = 2
									END
									ELSE
									BEGIN
											SET @IsExist = 3
									END
							 END

							 ELSE IF (@RetriveDealDescription = 'Coupons' AND @RetriveHcDealID = @DealID)
							 BEGIN
									IF NOT EXISTS (SELECT 1 FROM HcDealOfTheDayStaging 
											WHERE @RetriveHcDealID ! = DealOfTheDayID AND((@DealScheduleStartDate BETWEEN DealScheduleStartDate AND DealScheduleEndDate )
											OR (@DealScheduleEndDate  BETWEEN DealScheduleStartDate  AND DealScheduleEndDate) ) AND HcHubCitiID = @HcHubCitiID)
									BEGIN

									       -- SELECT 'Updated'
											UPDATE HcDealOfTheDayStaging 
											SET DealScheduleStartDate = @DealScheduleStartDate,DealScheduleEndDate = @DealScheduleEndDate  
											FROM HcDealOfTheDayStaging D
											INNER JOIN Coupon PHD ON D.DealOfTheDayID = PHD.CouponID
											WHERE DealOfTheDayID = @DealID AND D.HcHubCitiID = @HcHubCitiID
											AND @DealScheduleStartDate >= ISNULL(CouponStartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(CouponExpireDate,@DealScheduleEndDate)

											SET @IsExist = 2
									END
									ELSE
									BEGIN
									 
											SET @IsExist = 3
									END
							END

							ELSE IF (@RetriveDealDescription = 'Specialoffers' AND @RetriveHcDealID = @DealID)
							BEGIN
									IF NOT EXISTS (SELECT 1 FROM HcDealOfTheDayStaging 
											WHERE @RetriveHcDealID ! = DealOfTheDayID AND((@DealScheduleStartDate BETWEEN DealScheduleStartDate AND DealScheduleEndDate ) 
											OR (@DealScheduleEndDate  BETWEEN DealScheduleStartDate  AND DealScheduleEndDate) )AND HcHubCitiID = @HcHubCitiID)
									BEGIN
											UPDATE HcDealOfTheDayStaging 
											SET DealScheduleStartDate = @DealScheduleStartDate,DealScheduleEndDate = @DealScheduleEndDate 
											FROM HcDealOfTheDayStaging D
											INNER JOIN QRRetailerCustomPage PHD ON D.DealOfTheDayID = PHD.QRRetailerCustomPageID
											WHERE DealOfTheDayID = @DealID AND D.HcHubCitiID = @HcHubCitiID
											AND @DealScheduleStartDate >= ISNULL(StartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(EndDate,@DealScheduleEndDate)
 
											SET @IsExist = 2

									END
									ELSE
									BEGIN
									 
											SET @IsExist = 3
									END
							 END

							
						
						END -- End of @UpdateDeal = 8

						


					--If deal doesnt exist in staging table 
					IF  (@Exist = 0)				
					BEGIN
						
						--select 'a'

						IF (@DealName ='Hotdeals') 
						BEGIN

						

							IF  EXISTS(SELECT 1 FROM ProductHotDeal WHERE ProductHotDealID = @DealID AND @DealScheduleStartDate >= ISNULL(HotDealStartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(HotDealEndDate,@DealScheduleEndDate))
							BEGIN

									IF NOT EXISTS(SELECT 1 FROM HcDealOfTheDayStaging A 
													INNER JOIN ProductHotDeal B ON A.DealOfTheDayID=B.ProductHotDealID
													WHERE A.DealOfTheDayID = @DealID AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL AND HcHubCitiID = @HcHubCitiID )
									BEGIN 

												INSERT INTO HcDealOfTheDayStaging(HcHubCitiID
																				,DealOfTheDayID
																				,DealName
																				,DealDescription
																				,Price
																				,SalePrice
																				,DealStartDate
																				,DealEndDate
																				,DealExpirationDate
																				,HcDealsID
																				,DateCreated
																				,CreatedUserID
																				,DealScheduleStartDate
																				,DealScheduleEndDate
																				)
																		SELECT DISTINCT @HcHubCitiID
																				,@DealID
																				,HotDealName  
																				,ISNULL(HotDealShortDescription,HotDeaLonglDescription)
																				,Price
																				,SalePrice
																				,HotDealStartDate 
																				,HotDealEndDate 
																				,CONVERT(DATETIME,ISNULL(HotDealExpirationDate,HotDealEndDate)) 
							
																				,@DealType
																				,GETDATE()
																				,@UserID
																				,@DealScheduleStartDate
																				,@DealScheduleEndDate
																		FROM ProductHotDeal 
																		WHERE ProductHotDealID =@DealID 
																		AND @DealScheduleStartDate >= ISNULL(HotDealStartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(HotDealEndDate,@DealScheduleEndDate)


														--Setting Deal Flag (Deal Scheduled succesfully)
														SET @IsExist = 2
									      END

									END
									ELSE
									BEGIN 
											SET @IsExist = 3
									END

						END
											
						
						ELSE IF @DealName ='SpecialOffers' 
						BEGIN

								IF  EXISTS(SELECT 1 FROM QRRetailerCustomPage Q INNER JOIN QRRetailerCustomPageAssociation QA ON QA.QRRetailerCustomPageID =Q.QRRetailerCustomPageID  
											WHERE Q.QRRetailerCustomPageID =@DealID AND @DealScheduleStartDate >= ISNULL(Q.StartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(Q.EndDate,@DealScheduleEndDate)) --AND HcHubCitiID = @HcHubCitiID)

								BEGIN
											IF NOT EXISTS (SELECT 1 FROM QRRetailerCustomPage Q 
																INNER JOIN QRRetailerCustomPageAssociation QA ON QA.QRRetailerCustomPageID =Q.QRRetailerCustomPageID  
																INNER JOIN HcDealOfTheDayStaging HDS ON HDS.DealOfTheDayID = QA.QRRetailerCustomPageID
																WHERE Q.QRRetailerCustomPageID =@DealID AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL AND HcHubCitiID = @HcHubCitiID)

											 BEGIN
													 INSERT INTO HcDealOfTheDayStaging(HcHubCitiID
																						,DealOfTheDayID
																						,DealName
																						,DealDescription												
																						,DealStartDate
																						,DealEndDate
																						,DealExpirationDate
																						,HcDealsID
																						,SpecialsQRURL
																						,DateCreated
																						,CreatedUserID
																						,DealScheduleStartDate
																						,DealScheduleEndDate)
																				SELECT top 1 @HcHubCitiID
																						,@DealID
																						,Pagetitle  
																						,ISNULL(ShortDescription,LongDescription)
																						,StartDate  
																						,EndDate 
																						,Enddate 
																						,@DealType
																						--,CASE WHEN (QT.QRTypeName = 'Give Away Page' OR QT.QRTypeName = 'Special Offer Page' OR QT.QRTypeName = 'Main Menu Page') AND Q.URL IS NULL THEN RL.RetailLocationURL END
																						, CASE WHEN Q.URL IS NOT NULL THEN Q.url END splUrl
																							   --WHEN Q.URL IS NULL AND QT.QRTypeName != 'AnyThing Page' THEN @Config + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Special Offer Page') as varchar(10)) + '.htm?key1=' + CAST(QA.RetailID AS VARCHAR(10)) + '&key2=' + CAST(QA.RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(Q.QRRetailerCustomPageID AS VARCHAR(10)) + CASE WHEN Q.URL IS NOT NULL THEN '&EL=true' ELSE '&EL=false' END 
																							   --END AS splUrl
																					
																						,GETDATE()
																						,@UserID 
																						,@DealScheduleStartDate
																						,@DealScheduleEndDate
																				FROM QRRetailerCustomPage  Q
																				INNER JOIN QRRetailerCustomPageAssociation QA ON QA.QRRetailerCustomPageID =Q.QRRetailerCustomPageID
																				INNER JOIN QRTypes QT ON QT.QRTypeID=Q.QRTypeID
																				LEFT JOIN RetailLocation RL ON RL.RetailLocationID = QA.RetailLocationID
																				AND (@DealScheduleStartDate) >= ISNULL(Q.StartDate,@DealScheduleStartDate) AND (@DealScheduleEndDate) <= ISNULL(Q.EndDate,(@DealScheduleEndDate))  
																				WHERE Q.QRRetailerCustomPageID =@DealID 
															

															--SELECT 'Updated'
																--Setting Deal Flag (Deal Scheduled succesfully)
																SET @IsExist = 2
												END
								END
								ELSE
								BEGIN 
										SET @IsExist = 3
								END

						END
						
										
						ELSE IF @DealName ='Coupons' 
						BEGIN
								IF EXISTS(SELECT 1 FROM Coupon WHERE CouponID =@DealID AND @DealScheduleStartDate >= ISNULL(CouponStartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(CouponExpireDate,@DealScheduleEndDate))
								BEGIN
										IF NOT EXISTS (SELECT 1 FROM HcDealOfTheDayStaging A
														INNER JOIN Coupon B ON A.DealOfTheDayID=B.couponID 
														WHERE A.DealOfTheDayID = @DealID AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL AND A.HcHubCitiID = @HcHubCitiID)
										 BEGIN
												--SELECT 'Coupon inserted'
												INSERT INTO HcDealOfTheDayStaging(HcHubCitiID
																				,DealOfTheDayID
																				,DealName
																				,DealDescription												
																				,SalePrice
																				,DealStartDate
																				,DealEndDate
																				,DealExpirationDate
																				,HcDealsID
																				,DateCreated
																				,CreatedUserID
																				,DealScheduleStartDate
																				,DealScheduleEndDate)
																SELECT DISTINCT @HcHubCitiID
																		,@DealID
																		,CouponName 
																		,ISNULL(CouponShortDescription,CouponLongDescription)					  
																		,CouponDiscountAmount
																		,CouponStartDate 
																		,CouponExpireDate
																		,ISNULL(CouponDisplayExpirationDate,CouponExpireDate) 
																		,@DealType
																		,GETDATE()
																		,@UserID 
																		,@DealScheduleStartDate
																		,@DealScheduleEndDate
																FROM Coupon 
																WHERE CouponID =@DealID
																AND @DealScheduleStartDate >= ISNULL(CouponStartDate,@DealScheduleStartDate) AND @DealScheduleEndDate <= ISNULL(CouponExpireDate,@DealScheduleEndDate)
	
																--Setting Deal Flag (Deal Scheduled succesfully)
																SET @IsExist = 2
												END

								END
								ELSE
									BEGIN 
											SET @IsExist = 3
									END

						

					
						END
						

			END

			
		
	END


		--To display PushDeal
					--SELECT   dealId  
					--		,dealName 
					--		,type 
					--		,splUrl
					--		,hcName
					--		,retailerId
					--		,retailLocationId
					--		,expirationDate endDate
					--		,expirationTime endTime
					--FROM #PushDeal
					--WHERE hcName =@HcHubCitiName

					SELECT
										  dealId  
										 ,dealName 
										 ,type 
										 ,splUrl
										 ,hcName
										 ,HcDealsID
										 ,retailerId
										 ,retailLocationId
										 , CONVERT(DATE,expirationDate) endDate
										 , CONVERT(VARCHAR(5), expirationDate, 108) endTime -- + (CASE WHEN DATEPART(HOUR, expirationDate) > 12 THEN 'PM' ELSE 'AM' END)
										 FROM #PushDeal
										 WHERE hcName =@HcHubCitiName
										
										 


	SELECT DISTINCT UserToken deviceId
					,platform					  		
					,HubCitiName hcName
					,PushNotifyBadgeCount badgeCount
				
	FROM #DeviceList

	--Confirmation of Success
			SELECT @Status = 0
	
	END TRY

	
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		throw;
			PRINT 'Error occured in Stored Procedure [usp_WebHcDelaoftheDayCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
