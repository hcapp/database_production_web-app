USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFindCategoryimagesList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcFindCategoryimagesList]
Purpose					: To display list of BusinessCategories along with images for Find Module.
Example					: [usp_WebHcFindCategoryimagesList]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015	     SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFindCategoryimagesList]
(   
	--Input variable
	  @HcHubCitiID int
	, @UserID int	

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			----To get the Configuration Details.	
			DECLARE @Config Varchar(500) 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			DECLARE @ConfigurationType Varchar(500)
			SELECT @ConfigurationType = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType = 'App Media Server Configuration'

			--To insert categories into Category image table if its not inserted for given Hubciti.
			INSERT INTO HcFindCategoryImage (HcHubCitiID
											,BusinessCategoryID
											,CategoryImagePath
											,DateCreated
											,CreatedUserID
											,IsUpdated
											)

									SELECT  @HcHubCitiID
											,B.BusinessCategoryID
											,B.CategoryImagePath
											,GETDATE()
											,@UserID
											,0
									FROM BusinessCategory B
									LEFT JOIN HcFindCategoryImage I ON B.BusinessCategoryID = I.BusinessCategoryID AND HcHubCitiID = @HcHubCitiID
									WHERE I.BusinessCategoryID IS NULL AND Active = 1
									AND BusinessCategoryName <> 'Other'
			
			--To display list of Find categories along with images.
			SELECT DISTINCT I.HcFindCategoryImageID AS catImgId
							,B.BusinessCategoryDisplayValue AS catName
							,catImgPath = CASE WHEN IsUpdated = 1 
											            THEN IIF(I.CategoryImagePath IS NOT NULL, @Config + CAST(@HcHubCitiID AS VARCHAR(100)) + '/' + I.CategoryImagePath, I.CategoryImagePath)
										       WHEN IsUpdated = 0 
											            THEN IIF(I.CategoryImagePath IS NOT NULL, @ConfigurationType + I.CategoryImagePath, I.CategoryImagePath)
										  END 
			FROM HcFindCategoryImage I
			INNER JOIN BusinessCategory B ON I.BusinessCategoryID = B.BusinessCategoryID 
			WHERE HcHubCitiID = @HcHubCitiID 
			ORDER BY B.BusinessCategoryDisplayValue
								
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
