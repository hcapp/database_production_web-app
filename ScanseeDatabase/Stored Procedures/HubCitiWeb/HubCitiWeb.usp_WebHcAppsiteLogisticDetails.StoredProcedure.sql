USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAppsiteLogisticDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteLogisticDetails]
Purpose					: To edit Appsite logistic map details.
Example					: [usp_WebHcAppsiteLogisticDetails]

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAppsiteLogisticDetails]
(
	
	--Input Variable
	  @HubCitiID int
	, @HcAppsiteLogisticID int
    
    --Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @AppsiteConfig Varchar(500) 

			SELECT @AppsiteConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'

			--To display details of Appsite Logistic map.
			SELECT L.HcAppsiteLogisticID AS logisticId
					, LogisticName
					, LogisticImage AS LogisticImageName
					, LogisticImagePath = @AppsiteConfig + CAST(@HubCitiID AS VARCHAR)+ '/'+ LogisticImage
					, L.RetailID AS retailerId
					, RetailName AS retailerName
					, L.RetailLocationID AS locationId
					, Address = RL.Address1 + ','+ RL.City +','+ RL.[State] +','+ RL.PostalCode
					, StartDate
					, EndDate
					, L.MapDisplayTypeID isPortrtOrLandscp
			FROM HcAppsiteLogistic L
			LEFT JOIN Retailer R ON L.RetailID = R.RetailID
			LEFT JOIN RetailLocation RL ON L.RetailLocationID = RL.RetailLocationID
			WHERE L.HcAppsiteLogisticID = @HcAppsiteLogisticID
			AND HcHubCitiID = @HubCitiID									
			
			--Confirmation of Success
			SELECT @Status = 0

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteLogisticDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
