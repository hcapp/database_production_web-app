USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiLocationInfoDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiLocationInfoDisplay
Purpose					: To disply the list of the Hub Cities that are created by the SS admin.
Example					: usp_WebHcHubCitiLocationInfoDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20/1/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiLocationInfoDisplay]
(
   ----Input variable.
	  @HcHubcitiID Int
    , @ScreenName Varchar(255)
	, @LowerLimit int
	  
	--Output Variable 
    , @Status int output
    , @MaxCnt int output
    , @NextPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
			DECLARE @ScreenContent int	
		    
			SELECT @ScreenContent =ScreenContent 
			FROM AppConfiguration where ScreenName like 'HubCitiRetaillocationSetup'

		    SELECT @UpperLimit = @ScreenContent + @LowerLimit
		    
		    --Display the hub cities created by the SS admin.
			SELECT DISTINCT Rownum=Identity(int,1,1)
			               ,State
			               ,City
						   ,PostalCode
						   ,State+City+PostalCode Combination
			INTO #Temp
			FROM HcLocationAssociation 
			WHERE HcHubCitiID =@HcHubcitiID
			
			
			--Get the total no. of records for qualifying the search criteria.
			SELECT @MaxCnt = COUNT(1) FROM #Temp
			
			--Check if there are more records for next page.
			SELECT @NextPageFlag = (CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END) 
			
			--Display the list of hub cities.
			SELECT RowNum
			     ,State
			    ,City
				,PostalCode
				,Combination
			FROM #Temp
			WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit   
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiLocationInfoDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
