USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstTempletes]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiWeb].[usp_WebHcNewsFirstTempletes]
Purpose					: To create and update news settings.
Example					: [HubCitiWeb].[usp_WebHcNewsFirstTempletes]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17/5/2016	    Sagar Byali		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstTempletes]
(
   --Input variable.

	--Output Variable
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	

	 SELECT HcTemplateID newsTempleteID
		   ,TemplateName tempName   
	 FROM HcTemplate
	 where IsNews = 1



	       --Confirmation of Success.
		   SELECT @Status = 0
	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminSetupAboutUsPage.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
