USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminCityExperienceDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminCityExperienceDetails
Purpose					: To Display CitiExperience details for given HubCiti.
Example					: usp_WebHcAdminCityExperienceDetails

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminCityExperienceDetails]
(
    --Input variable
	  @HubCitiID int
		  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
			DECLARE @HubCitiImageConfig VARCHAR(100)
			SELECT @HubCitiImageConfig = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			--To display Retailer details for given HubCiti
			SELECT  CityExperienceName
				    ,CityExperienceImagePath = @HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10)) + '/' + CityExperienceImage
			        ,R.RetailID
					,R.RetailName
					,RL.RetailLocationID
					,RL.Address1
					,RL.City
					,RL.[State]
					,RL.PostalCode
			FROM HcCityExperience CE
			INNER JOIN HcCityExperienceRetailLocation CRL ON CE.HcCityExperienceID = CRL.HcCityExperienceID
			INNER JOIN RetailLocation RL ON CRL.RetailLocationID = RL.RetailLocationID
			INNER JOIN Retailer R ON RL.RetailID = R.RetailID
			WHERE CE.HcHubCitiID = @HubCitiID AND (RL.Active=1 AND R.RetailerActive=1)
			
			
		--Confirmation of Success
		SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminCityExperienceDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
