USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventUpdation
Purpose					: Updating Evets.
Example					: usp_WebHcHubCitiEventUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventUpdation]
(
   ----Input variable. 	  
	  @UserID Int
    , @HcEventID Int
	, @HcHubCitiID Int
	, @HcEventName Varchar(1000)
	, @ShortDescription Varchar(2000)
	, @LongDescription Varchar(2000)
	, @HcEventCategoryID Varchar(4000)
	, @ImagePath Varchar(2000) 
	, @BussinessEvent Bit 
	, @PackageEvent Bit
	, @HotelEvent Bit
	, @StartDate Date
	, @EndDate Date	
	, @StartTime Time
	, @EndTime Time
	, @PackageDescription Varchar(2000)
	, @PackageTicketURL Varchar(2000) 
	, @PackagePrice Money
	, @Address Varchar(2000)
    , @City varchar(200)
    , @State Varchar(100)
    , @PostalCode Varchar(10)
    , @Latitude Float
    , @Longitude Float
    , @GeoErrorFlag BIT
	, @HcAppsiteID Varchar(max)
	, @RetailLocationID Varchar(max)
	, @HotelPrice VARCHAR(MAX)
	, @DiscountCode VARCHAR(MAX)
	, @DiscountAmount VARCHAR(MAX)
	, @Rating VARCHAR(MAX)
	, @RoomAvailabilityCheckURL VARCHAR(MAX)
	, @RoomBookingURL VARCHAR(MAX)	
	, @MoreInformationURL Varchar(2000)
	, @OngoingEvent bit 
	, @RecurrencePatternID int
	, @RecurrenceInterval int
	, @EveryWeekday bit
	, @Days varchar(1000)
	, @EndAfter int
	, @DayNumber int
	, @EventsLogisticFlag Bit
	, @EventsLogisticImagePath Varchar(2000)
	--, @EventsLogisticHTMLPath Varchar(2000)
	--, @EventsLogisticHTMLPathFlag Bit
	, @EventsIsOverLayFlag Bit
	, @ButtonName Varchar(Max)
	, @ButtonLink Varchar(max)
	, @EventLocationTitle Varchar(500)
	, @EventListingImagePath Varchar(2000)
	, @isPortrtOrLandscp int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION   
			
			DECLARE @RecurrencePattern varchar(10)
			SELECT @EveryWeekday = ISNULL(@EveryWeekday, 0)


			SELECT @RetailLocationID = REPLACE(@RetailLocationID, 'NULL', '0')
			SELECT @HotelPrice = REPLACE(@HotelPrice, 'NULL', '0')
			SELECT @DiscountCode = REPLACE(@DiscountCode, 'NULL', '0')
			SELECT @DiscountAmount = REPLACE(@DiscountAmount, 'NULL', '0')
			SELECT @Rating = REPLACE(@Rating, 'NULL', '0')
			SELECT @RoomAvailabilityCheckURL = REPLACE(@RoomAvailabilityCheckURL, 'NULL', '0')
		    SELECT @RoomBookingURL = REPLACE(@RoomBookingURL, 'NULL', '0')
		    
			SELECT @RecurrencePattern = RecurrencePattern 
			FROM HcEventRecurrencePattern 
			WHERE HcEventRecurrencePatternID = @RecurrencePatternID

		    SELECT @Days = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Days, '1', 'Sunday'), '2', 'Monday'), '3', 'Tuesday'), '4', 'Wednesday'), '5', 'Thursday'), '6', 'Friday'), '7', 'Saturday')

			IF @EveryWeekday = 1
			BEGIN
				SELECT @Days = 'Monday,Tuesday,Wednesday,Thursday,Friday'				
			END

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , RetailLocationID = Param
			INTO #RetailLocationID
			FROM dbo.fn_SplitParam(@RetailLocationID, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , HotelPrice = Param
			INTO #HotelPrice
			FROM dbo.fn_SplitParam(@HotelPrice, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , DiscountCode = Param
			INTO #DiscountCode
			FROM dbo.fn_SplitParam(@DiscountCode, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , DiscountAmount = Param
			INTO #DiscountAmount
			FROM dbo.fn_SplitParam(@DiscountAmount, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , Rating = Param
			INTO #Rating
			FROM dbo.fn_SplitParam(@Rating, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , RoomAvailabilityCheckURL = Param
			INTO #RoomAvailabilityCheckURL
			FROM dbo.fn_SplitParam(@RoomAvailabilityCheckURL, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , RoomBookingURL = Param
			INTO #RoomBookingURL
			FROM dbo.fn_SplitParam(@RoomBookingURL, ',')


			--Updating New Event		
			UPDATE HcEvents SET HcEventName = LTRIM(RTRIM(@HcEventName))  
			                    ,ShortDescription = @ShortDescription
                                ,LongDescription = @LongDescription								
								,HcHubCitiID = @HcHubCitiID
								,ImagePath = @ImagePath 
								,EventListingImagePath = @EventListingImagePath
								,BussinessEvent = @BussinessEvent 
								,PackageEvent = @PackageEvent								
								,StartDate = CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
								,EndDate = IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + ISNULL(CAST(@EndTime AS DATETIME),'00:00:00'))
								,DateModified = GETDATE() 
								,ModifiedUserID = @UserID
								,PackageDescription = CASE WHEN @PackageEvent = 1 THEN @PackageDescription ELSE NULL END
								,PackageTicketURL = CASE WHEN @PackageEvent = 1 THEN @PackageTicketURL ELSE NULL END
								,PackagePrice = CASE WHEN @PackageEvent = 1 THEN @PackagePrice ELSE NULL END	
								,HotelEvent= @HotelEvent
								,MoreInformationURL= @MoreInformationURL
								,OnGoingEvent = @OngoingEvent
								,HcEventRecurrencePatternID = IIF(@OngoingEvent = 1, @RecurrencePatternID, NULL) 
								,RecurrenceInterval = IIF(@OngoingEvent = 1, CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
																THEN @RecurrenceInterval 
															ELSE NULL 
													   END, NULL)
								,EventFrequency =  IIF(@OngoingEvent = 1, @EndAfter, NULL)
			WHERE HcEventID=@HcEventID				
			
			DELETE FROM HcEventLocation 
			WHERE HcEventID = @HcEventID 

			---Insert Event Details to Event Location Table
			IF @BussinessEvent = 0
			BEGIN
					INSERT INTO HcEventLocation(HcEventID
											   ,HcHubCitiID
											   ,Address
											   ,City
											   ,State
											   ,PostalCode
											   ,Latitude
											   ,Longuitude
											   ,GeoErrorFlag
											   ,DateCreated
											   ,CreatedUserID
											   ,EventLocationTitle)
					SELECT @HcEventID 
						  ,@HcHubCitiID 
						  ,@Address 
						  ,@City
						  ,@State 
						  ,@PostalCode 
						  ,@Latitude
						  ,@Longitude 
						  ,@GeoErrorFlag
						  ,GETDATE()
						  ,@UserID
						  ,@EventLocationTitle 
			END
			
			
			DELETE FROM HcEventsCategoryAssociation 
			WHERE HcEventID =@HcEventID 
			
			INSERT INTO HcEventsCategoryAssociation(HcEventCategoryID 
												   ,HcEventID 
												   ,HcHubCitiID
												   ,DateCreated
												   ,CreatedUserID)	
			SELECT [Param]
			      ,@HcEventID 
			      ,@HcHubCitiID
			      ,GETDATE()
			      ,@UserID
			FROM dbo.fn_SplitParam(@HcEventCategoryID, ',') 					
			
			DELETE FROM HcEventAppsite 
			WHERE HcEventID =@HcEventID 
			
			IF @BussinessEvent=1
			BEGIN
			   INSERT INTO HcEventAppsite(HcEventID
										 ,HcHubCitiID
										 ,HcAppsiteID
										 ,DateCreated										
										 ,CreatedUserID)
										 
				SELECT @HcEventID 
				      ,@HcHubCitiID 
					  ,[Param] 
					  ,GETDATE()
					  ,@UserID 
					
				FROM fn_SplitParam(@HcAppsiteID,',')
			END


			DELETE FROM HcEventPackage 
			WHERE HcEventID =@HcEventID 

			IF @HotelEvent =1
			BEGIN
				INSERT INTO HcEventPackage(HcEventID
										  ,HcHubCitiID
										  ,RetailLocationID
										  ,DateCreated										
										  ,CreatedUserID
										  ,HotelPrice 
										  ,DiscountCode 
										  ,DiscountAmount 
										  ,Rating 
										  ,RoomAvailabilityCheckURL
										  ,RoomBookingURL)
				SELECT @HcEventID 
				      ,@HcHubCitiID 
					  ,CASE WHEN A.RetailLocationID = '0' THEN NULL ELSE A.RetailLocationID END 
					  ,GETDATE()
					  ,@UserID
					  ,CASE WHEN B.HotelPrice = '0' THEN NULL ELSE B.HotelPrice END 
					  ,CASE WHEN C.DiscountCode = '0' THEN NULL ELSE C.DiscountCode END  
				      ,CASE WHEN D.DiscountAmount = '0' THEN NULL ELSE D.DiscountAmount END  
					  ,CASE WHEN E.Rating = '0' THEN NULL ELSE E.Rating END  
					  ,CASE WHEN F.RoomAvailabilityCheckURL = '0' THEN NULL ELSE F.RoomAvailabilityCheckURL END  
					  ,CASE WHEN G.RoomBookingURL = '0' THEN NULL ELSE G.RoomBookingURL END  
				FROM #RetailLocationID A
				INNER JOIN #HotelPrice B ON A.RowNum =B.RowNum
				INNER JOIN #DiscountCode C ON C.RowNum =B.RowNum
				INNER JOIN #DiscountAmount D ON C.RowNum =D.RowNum 
				INNER JOIN #Rating E ON E.RowNum =D.RowNum
				INNER JOIN #RoomAvailabilityCheckURL F ON F.RowNum =E.RowNum
				INNER JOIN #RoomBookingURL G ON G.RowNum =F.RowNum
			END		
			
			DELETE FROM HcEventInterval
			WHERE HcEventID = @HcEventID

			--For On Going Events insert the ongoing pattern into the association tables.
			IF @OngoingEvent = 1
			BEGIN
				--If the event is created as Daily with a inteval value then do not insert record into HcEventInterval table.
				--IF ((@RecurrencePattern <> 'Daily' AND @EveryWeekday <> 0) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				IF ((@RecurrencePattern = 'Weekly' OR @RecurrencePattern = 'Monthly' ) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				BEGIN
					DELETE FROM HcEventInterval WHERE HcEventID = @HcEventID
					INSERT INTO HcEventInterval(HcEventID
											  , DayNumber
											  , MonthInterval
											  , DayName
											  , DateCreated
											  , CreatedUserID)
									SELECT @HcEventID
										 , @DayNumber
										 , IIF(@RecurrencePattern ='Monthly', @RecurrenceInterval, NULL)
										 , LTRIM(RTRIM(REPLACE(REPLACE(Param, '[', ''), ']', '')))
										 , GETDATE()
										 , @UserID
									FROM [HubCitiWeb].fn_SplitParam(@Days, ',') A
					END
				END

				IF @EventsLogisticFlag =1
				BEGIN
					UPDATE HcEvents SET EventsLogisticFlag=1 
					WHERE HcEventID =@HcEventID 

					DELETE FROM HcEventsLogistic 
					WHERE HcEventID =@HcEventID 

					INSERT INTO HcEventsLogistic(HcEventID
												,HcHubCitiID
												,EventsLogisticImagePath
												,EventsIsOverLayFlag												
												,DateCreated
												,CreatedUserID
												,MapDisplayTypeID)
					SELECT @HcEventID
						  ,@HcHubCitiID 
						  ,@EventsLogisticImagePath 
						  ,@EventsIsOverLayFlag
						  ,GETDATE()
						  ,@UserID 
						  ,@isPortrtOrLandscp

					SELECT Rownum=IDENTITY(INT,1,1)
						  ,Param ButtonName
					INTO #ButtonName
					FROM fn_SplitParam(@ButtonName,',')

					SELECT Rownum=IDENTITY(INT,1,1)
						  ,Param ButtonLink
					INTO #ButtonLink
					FROM fn_SplitParam(@ButtonLink,',')

					DELETE FROM HcEventsLogisticButtons 
					WHERE HcEventID =@HcEventID 

					INSERT INTO HcEventsLogisticButtons(HcEventID
													   ,HcHubCitiID
													   ,ButtonName
													   ,ButtonLink
													   ,DateCreated
													   ,CreatedUserID)
					SELECT @HcEventID 
						  ,@HcHubCitiID 
						  ,ButtonName
						  ,BL.ButtonLink 
						  ,GETDATE()
						  ,@UserID
					FROM #ButtonName B
					INNER JOIN #ButtonLink BL ON BL.Rownum =B.Rownum 
					ORDER BY B.Rownum 

				END

				IF @EventsLogisticFlag =0
				BEGIN
					UPDATE HcEvents SET EventsLogisticFlag=0
					WHERE HcEventID =@HcEventID

					DELETE FROM HcEventsLogistic 
					WHERE HcEventID =@HcEventID 

					DELETE FROM HcEventsLogisticButtons 
					WHERE HcEventID =@HcEventID 
				END

	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
