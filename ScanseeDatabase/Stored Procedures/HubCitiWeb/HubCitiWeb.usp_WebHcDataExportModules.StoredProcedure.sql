USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDataExportModules]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDataExportModules]
Purpose					: To display list of Data Export Modules
Example					: [usp_WebHcDataExportModules]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			21 Apr 2015	     SPAN			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDataExportModules]
(   
	--Input Variable
	  @HcDataExportTypeID int

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT M.HcDataExportModuleID AS moduleId
					, HcDataExportModuleName AS moduleName
					, isMandatory = CAse WHEN  M.HcDataExportModuleID = 7 THEN 1 ELSE 0 END
			FROM HcDataExportTypeModuleAssociation A
			INNER JOIN HcDataExportModule M ON A.HcDataExportModuleID = M.HcDataExportModuleID
			WHERE A.HcDataExportTypeID = @HcDataExportTypeID
			ORDER BY HcDataExportModuleName


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDataExportModules].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
