USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminSetupPrivacyPolicy]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminSetupPrivacyPolicy
Purpose					: To set up the Privacy Policy for the Hub Citi App.
Example					: usp_WebHcAdminSetupPrivacyPolicy

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminSetupPrivacyPolicy]
(
   --Input variable.
      @HCAdminUserID int
	, @HubCitiID int
	, @BackgroundColor varchar(20)
	, @FontColor varchar(20)
	, @Title varchar(255)
	, @Content varchar(max)
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION		
			
			DECLARE @PageType int
			
			SELECT @PageType = HcPageTypeID
			FROM HcPageType
			WHERE PageType = 'Privacy Policy'
			
			--Update the Privacy Policy if one already exists.
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcHubCitiID = @HubCitiID AND HcPageTypeID = @PageType)
			BEGIN
				print 'Update'
				UPDATE HcPageConfiguration SET BackgroundColor = @BackgroundColor
									 , FontColor = @FontColor
									 , DateModified = GETDATE()
									 , ModifiedUserID = @HCAdminUserID
									 , Title = @Title
									 , Description = @Content
				WHERE HcHubCitiID = @HubCitiID
				AND HcPageTypeID = @PageType
				
			END 	
			
			--Created a new record if there is no Privacy Policy setup yet.
			ELSE
			BEGIN
				print 'Insert'
				INSERT INTO HcPageConfiguration(HcHubCitiID
									  , BackgroundColor
									  , FontColor
									  , Title
									  , Description
									  , DateCreated
									  , CreatedUserID
									  , HcPageTypeID)
							VALUES(@HubCitiID
								 , @BackgroundColor
								 , @FontColor
								 , @Title
								 , @Content
								 , GETDATE()
								 , @HCAdminUserID
								 , @PageType)
			END
					
			
			
	       --Confirmation of Success.
		   SELECT @Status = 0
	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminSetupPrivacyPolicy.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
