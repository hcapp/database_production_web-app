USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HcUserPushNotify]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserPushNotify
Purpose					: To send push notifications to respective users.
Example					: usp_HcUserPushNotify

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			07 Feb 2016		Sagar Byali		Tyler - Breaking News
1.0			06 Jan 2017		Sagar Byali		Tyler Test - Breaking News
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_HcUserPushNotify]  
(
	  @HchubCitiID INT
	, @Type varchar(1000)

	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			
			
		BEGIN TRAN

				DECLARE @DealCount INT
		
				--To fetch device list
				CREATE TABLE #DeviceIDs(deviceId VARCHAR(max)
										,platform VARCHAR(50)					  		
										,hcName VARCHAR(max)	
										,badgeCount int)

				--To fetch Deal of the day
				CREATE TABLE #HcNewsStaging(title varchar(max)
											,type varchar(max)
											,link varchar(max))

				--To fetch Deals
				CREATE TABLE #Deals(dealId int
									,dealName varchar(max)
									,type varchar(max)
									,splUrl varchar(max)
									,hcName varchar(max)
									,retailerId int
									,retailLocationId int
									,endDate datetime
									,endTime TIME)


				--To fetch news information.
				INSERT INTO #HcNewsStaging	
				SELECT    Title title		
						, NT.NewsType	
						, NT.NewsLink 	  	
				FROM HcNewsStaging NS
				INNER JOIN HcNewsType NT ON NS.HcNewsTypeID = NT.HcNewsTypeID AND NT.NewsType ='Trending'
				WHERE IsPushed = 0 

				UNION ALL
			
				SELECT    Title title		
						, NT.NewsType 
						, NT.NewsLink 	
				FROM HcNewsStaging NS
				INNER JOIN HcNewsType NT ON NS.HcNewsTypeID = NT.HcNewsTypeID AND NT.NewsType = 'Breaking News'
				WHERE IsPushed = 0

				UNION ALL
			
				SELECT    Title title		
						, NT.NewsType	
						, NT.NewsLink 	
				FROM HcNewsStaging NS
				INNER JOIN HcNewsType NT ON NS.HcNewsTypeID = NT.HcNewsTypeID AND NT.NewsType = 'Tyler Test Breaking News'
				WHERE IsPushed = 0
				ORDER BY NewsType desc

				--To update IsPushed flag
				IF EXISTS(SELECT 1 FROM #HcNewsStaging WHERE type = 'Breaking News')
				BEGIN
						--Incrementing Batch count as per the no.of News items
						UPDATE HcUserToken
						SET NewsTypeBreaking = ISNULL(NewsTypeBreaking,0) + (SELECT COUNT(1) FROM #HcNewsStaging WHERE type = 'Breaking News')
						WHERE HcHubCitiID = @HcHubCitiID
				END

				--To update IsPushed flag
				IF EXISTS(SELECT 1 FROM #HcNewsStaging WHERE type = 'Tyler Test Breaking News')
				BEGIN
						--Incrementing Batch count as per the no.of News items
						UPDATE HcUserToken
						SET NewsTypeTaylorBreakingNews = ISNULL(NewsTypeTaylorBreakingNews,0) + (SELECT COUNT(1) FROM #HcNewsStaging WHERE type = 'Tyler Test Breaking News')
						WHERE HcHubCitiID = @HcHubCitiID
				END


				--To fetch deals information.
				INSERT INTO #Deals
				SELECT  TOP 1 S.DealOfTheDayID dealId
							, S.DealName dealName
							, D.DealDescription type
							, S.SpecialsQRURL splUrl
							, H.HubCitiName hcName
							, retailerId = (CASE WHEN (QT.QRTypeName = 'AnyThing Page' AND S.SpecialsQRURL IS NULL AND QA.URL IS NULL AND D.DealDescription = 'SpecialOffers') THEN QR.RetailID END)     
							, retailLocationId = (CASE WHEN (QT.QRTypeName = 'AnyThing Page' AND S.SpecialsQRURL IS NULL AND QA.URL IS NULL AND D.DealDescription = 'SpecialOffers') THEN QR.RetailLocationID END)  
							, endDate = CONVERT(DATE,DealExpirationDate) 
							, endTime = CONVERT(VARCHAR(5), DealExpirationDate, 108) 		
				FROM HcDeals D 
				INNER JOIN HcDealOfTheDayStaging S ON D.HcDealsID = S.HcDealsID 
				INNER JOIN HcHubCiti H ON S.HcHubCitiID = H.HcHubCitiID
				LEFT JOIN QRRetailerCustomPageAssociation QR ON QR.QRRetailerCustomPageID = S.DealOfTheDayID
				LEFT JOIN QRRetailerCustomPage QA ON QR.QRRetailerCustomPageID=QA.QRRetailerCustomPageID
				LEFT JOIN QRTypes QT ON QT.QRTypeID=QA.QRTypeID
				LEFT JOIN RetailLocation RL ON RL.Retailid = QR.RetailID AND RL.RetailLocationID = QR.RetailLocationID
				WHERE H.HcHubCitiID IN (@HchubCitiID) AND CAST(Getdate() as date) BETWEEN CAST(S.DealScheduleStartDate as date) AND CAST(S.DealScheduleEndDate as date)
				AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL 
				ORDER BY s.DateCreated desc 
			

				SELECT @DealCount = COUNT(*)  
				FROM #Deals

				IF EXISTS(SELECT 1 FROM #HcNewsStaging WHERE type = 'Trending')
				BEGIN 
						UPDATE HcUserToken
						SET NewsTypeTrending =(ISNULL(NewsTypeTrending,0) + 1) 
						WHERE HcHubCitiID = @HcHubCitiID
				END
			
				UPDATE HcNewsStaging
				SET IsPushed = 1
				FROM HcNewsStaging NS
				INNER JOIN HcNewsType NT ON NS.HcNewsTypeID = NT.HcNewsTypeID 
				AND NT.NewsType in ('Breaking News','Trending','Tyler Test Breaking News')
			
----------------------Result set-------------------------------------------------------------------------------------------------------------------

					--To display News
					SELECT SS.* 
					FROM HcNewsStaging NS 
					INNER JOIN HcNewsType NT ON NS.HcNewsTypeID = NT.HcNewsTypeID	
					INNER JOIN #HcNewsStaging SS ON SS.link=NT.NewsLink AND SS.title = NS.Title	AND SS.type=NT.newsType		
					WHERE NT.NewsType = @Type
					ORDER by NT.NewsType ASC

					--To display Deals
					SELECT * 
					FROM #Deals

					--To display registered Device list subscribed for notifications (Android & IOS)
					SELECT DISTINCT  deviceId = U.UserToken 
									,platform = P.RequestPlatformtype				  		
									,hcName = H.HubCitiName 
									,badgeCount = ISNULL(NewsTypeTrending,0)+ISNULL(NewsTypeBreaking,0)+ISNULL(PushNotifyBadgeCount,0)+ISNULL(NewsTypeTaylorBreakingNews,0)  
					FROM HcUserToken U
					INNER JOIN HcRequestPlatforms P ON P.HcRequestPlatformID = U.HcRequestPlatformID
					INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
					INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID
					WHERE PushNotify = 1 AND H.HcHubCitiID = @HchubCitiID AND P.RequestPlatformtype = 'Android'
					--AND u.DeviceID IN ('29a3832895c3c01')
				
					UNION ALL

					SELECT DISTINCT  deviceId = U.UserToken 
									,platform = P.RequestPlatformtype					  		
									,hcName = H.HubCitiName 
									,badgeCount = ISNULL(NewsTypeTrending,0)+ISNULL(NewsTypeBreaking,0)+ISNULL(PushNotifyBadgeCount,0)+ISNULL(NewsTypeTaylorBreakingNews,0)   
					FROM HcUserToken U
					INNER JOIN HcRequestPlatforms P ON P.HcRequestPlatformID = U.HcRequestPlatformID
					INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
					INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID
					WHERE PushNotify = 1 AND H.HcHubCitiID = @HchubCitiID AND P.RequestPlatformtype = 'IOS'
					--AND D.DeviceID IN ('E6252BED-0ABE-4456-B8A4-79133AD56EFC','D39E6B54-46BF-4065-87BD-C9AD504D51D7')
					


--------------------------------------------------------------------------------------------------------------------------------------------------------
					INSERT INTO HcDealOfTheDay(HcHubCitiID
												,DealOfTheDayID
												,DealName
												,DealDescription
												,Price
												,SalePrice
												,DealStartDate
												,DealEndDate
												,HcDealsID
												,DateCreated
												,DateModified
												,CreatedUserID
												,ModifiedUserID)

										SELECT	 HcHubCitiID
												,DealOfTheDayID
												,DealName
												,DealDescription
												,Price
												,SalePrice
												,DealStartDate
												,DealEndDate
												,HcDealsID
												,DateCreated
												,DateModified
												,CreatedUserID
												,ModifiedUserID
										FROM HcDealOfTheDayStaging 
										WHERE HcHubCitiID IN (@HchubCitiID) 
										AND cast(DealScheduleEndDate as date) < cast(GETDATE() as date)
									

								--Confirmation of Success.
								SELECT @Status = 0
		
		COMMIT TRANSACTION	
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN

		throw;
			PRINT 'Error occured in Stored Procedure usp_HcUserPushNotify.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


















GO
