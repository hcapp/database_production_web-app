USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAnythingPagesDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAnythingPagesDisplay
Purpose					: To display List of Anything Page.
Example					: usp_WebHcHubCitiAnythingPagesDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAnythingPagesDisplay]
(  
     --Input variable.
      @HubCitiID Int
    , @SearchKey varchar(255)
    , @LowerLimit int
  
	--Output Variable 
	, @NxtPageFlag bit output
	, @MaxCnt int output	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		   DECLARE @UpperLimit INT	
						
		   SELECT @UpperLimit = @LowerLimit + ScreenContent         
		   FROM AppConfiguration         
		   WHERE ScreenName = 'HubCitiWeb'         
		   AND ConfigurationType = 'Pagination'        
		   AND Active = 1   
		  			   
			--To display List fo Anything Page
			SELECT distinct
				   HP.HcAnythingPageID
				  ,AnythingPageName AnythingPageTitle
				  ,ShortDescription
				  ,LongDescription
				  ,StartDate
				  ,EndDate
				  ,ImagePath
				  ,ImageIcon
				  ,HcHubCitiID	
				  ,CASE WHEN URL IS NULL OR URL = '' THEN  HT.HcAnyThingPageMediaType ELSE 'Website' END PageType
				  ,MenuItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
											  FROM HcMenuItem MI 
											  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
											  WHERE LinkTypeName = 'AnythingPage' 
											  AND MI.LinkID = HP.HcAnythingPageID)>0 THEN 1 ELSE 0 END	
				  ,CASE WHEN (URL IS NULL OR URL = '') AND (MediaPath IS NULL OR MediaPath = '')   THEN 'Make Your Own' ELSE 'EXISTING' END PageView									  
											  						  
			INTO #Temp	   
			FROM HcAnythingPage HP
			LEFT JOIN HcAnythingPageMedia HM ON HP.HcAnythingPageID = HM.HcAnythingPageID
			LEFT JOIN HcAnythingPageMediaType HT ON HM.MediaTypeID = HT.HcAnyThingPageMediaTypeID  			
			WHERE HcHubCitiID = @HubCitiID 
			AND ((@SearchKey IS NULL AND 1 = 1)
				OR
				(@SearchKey IS NOT NULL AND AnythingPageName LIKE '%'+@SearchKey+'%'))
			

			
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
			
			SELECT RowNum = IDENTITY(int,1,1)
				 , HcAnythingPageID
				 , AnythingPageTitle
				 , ShortDescription
				 , LongDescription
				 , StartDate
			     , EndDate
			     , ImagePath
			     , ImageIcon
			     , HcHubCitiID
			     , PageType 
			     , MenuItemExist
			     , PageView	
			into #t
			FROM #Temp

						
			--To capture max row number.        
			SELECT @MaxCnt = COUNT(RowNum) FROM #t
			
			--If no pagination required then send all the records.
			IF(@LowerLimit IS NULL)
			BEGIN
				SELECT @UpperLimit = @MaxCnt
			END

			select  *
			from #t
			WHERE RowNum BETWEEN (ISNULL(@LowerLimit, 0) + 1) AND @UpperLimit
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAnythingPagesDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
