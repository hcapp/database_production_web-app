USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDataExportHubCitiList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDataExportHubCitiList]
Purpose					: To display list of Hubcities selected by Admin for Data Export.
Example					: [usp_WebHcDataExportHubCitiList]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			13 Oct 2015	     SPAN			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDataExportHubCitiList]
(   

	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To fetch data where Export flag is set to 1
			SELECT DISTINCT B.HcHubCitiID hubCitiID
						   ,B.HubCitiName hubCitiName
			FROM HcDataExportHubCitiList A
			INNER JOIN HcHubCiti B ON A.HcHubCitiID=B.HcHubCitiID AND B.Active = 1
			WHERE HubCitiDataExportFlag = 1
			ORDER BY HubCitiName 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDataExportHubCitiList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
