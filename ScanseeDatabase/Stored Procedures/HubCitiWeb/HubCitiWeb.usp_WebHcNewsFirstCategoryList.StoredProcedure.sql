USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstCategoryList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNewsCategoryList]
Purpose					: To display list of News categories.
Example					: [usp_WebHcNewsCategoryList]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[usp_WebHcFindCategoryList]
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstCategoryList]
(   
	--Input variable
	 @HcHubCitiID int 

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY


	DECLARE @Flag BIT

	IF EXISTS(SELECT 1 from NewsFirstSubCategorySettings  s
		INNER JOIN NewsFirstSettings NS ON NS.NewsCategoryID = S.NewsCategoryID WHERE S.HcHubCitiID = @HcHubCitiID) 

	SET @Flag = 1

	ELSE

	SET @Flag = 0


			
			--To display list of Find categories along with images.
			SELECT DISTINCT  I.NewsCategoryID AS catID
							,I.NewsCategoryDisplayValue AS catName
							,isSubCatFlag = CASE WHEN SC.NewsSubCategoryID IS NOT NULL THEN '1' ELSE '0' END 
			INTO #temp				
			FROM NewsFirstCategory I
			INNER JOIN NewsFirstCatSubCatAssociation A ON A.categoryID = I.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID 
			LEFT JOIN NewsFirstSubCategoryType TY ON TY.NewsCategoryID = I.NewsCategoryID
			LEFT JOIN NewsFirstSubCategory SC ON SC.NewsSubCategoryTypeID = TY.NewsSubCategoryTypeID 
			LEFT JOIN NewsFirstSettings NN ON NN.NewsCategoryID = I.NewsCategoryID AND NN.HcHubCitiID = @HcHubCitiID 
			LEFT JOIN NewsFirstSubCategorySettings SS ON SS.NewsCategoryID = I.NewsCategoryID AND SS.NewsSubCategoryID = SC.NewsSubCategoryID AND SS.NewsCategoryID = NN.NewsCategoryID AND SS.HcHubCitiID = NN.HcHubCitiID AND NN.HcHubCitiID = @HcHubCitiID AND SS.HcHubCitiID = @HcHubCitiID 
			ORDER BY I.NewsCategoryDisplayValue


			select			 catID
							,catName
							,isSubCatFlag 
							,isSubcategoryAdded = CASE WHEN isSubCatFlag = 1 THEN @Flag END
			FROM #temp

		
			
	

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
