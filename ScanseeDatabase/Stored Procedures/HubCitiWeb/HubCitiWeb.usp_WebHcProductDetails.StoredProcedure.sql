USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcProductDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcProductDetails
Purpose					: To display the basic details of the selected product.
Example					: 

History
Version		Date				Author			Change Description
------------------------------------------------------------------------------- 
1.0			22 Jan 2015			Mohith H R		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcProductDetails]
(

	--Input Input Parameter(s)--	   
	  
	   @ProductID int	 
     , @HubCitiID int
	
	--Output Variable--
	  
     , @IOSAppID varchar(500) output
	 , @AndroidAppID varchar(500) output  
	 , @Status INT OUTPUT
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		DECLARE @ManufConfig varchar(50)
		DECLARE @Config varchar(50)
		SELECT @ManufConfig=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'

		--Retrieve the server configuration.
        SELECT @Config = ScreenContent 
        FROM AppConfiguration 
        WHERE ConfigurationType LIKE 'QR Code Configuration'

		SELECT @IOSAppID = IOSAppID
			  ,@AndroidAppID = AndroidAppID
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID
				
		
		SELECT    ProductID
				, ProductName
		        , ProductShortDescription
		        , ProductLongDescription
				, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @ManufConfig +CONVERT(VARCHAR(30),ManufacturerID)+'/' + ProductImagePath
								     ELSE ProductImagePath END
				, ModelNumber
			    , WarrantyServiceInformation
			    , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
				, QRUrl = @Config+'1000.htm?prodId='+ CAST(@ProductID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))
		FROM Product 
		WHERE ProductID = @ProductID		
		
		SET @Status = 0
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
