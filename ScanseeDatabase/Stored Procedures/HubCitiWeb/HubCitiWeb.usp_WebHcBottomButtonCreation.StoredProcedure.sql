USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcBottomButtonCreation
Purpose					: usp_WebHcBottomButtonCreation.
Example					: usp_WebHcBottomButtonCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcBottomButtonCreation]
(
   --Input variable.   	
	  --@BottomButtonName Varchar(1000)
	  @HcBottomButtonID Varchar(2000)	
	, @BottomButtonImageOn Varchar(1000)	
	, @BottomButtonImageOff Varchar(1000)
	, @BottomButtonLinkTypeID int
	, @BottomButtonLinkID Varchar(max)
	--, @Position int
	, @UserID int
	, @HcBottomButtonImageIconID int
	, @HubCitiID int
	, @ItunesURL Varchar(2000)
	, @GooglePlayURL Varchar(2000)
	, @HcBusinessSubCategoryID Varchar(max)	
	
	  
	--Output Variable 	
	, @FindClearCacheURL Varchar(1000) output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	 

		DECLARE @LinkTypeID INT
		DECLARE @EventLinkypeID INT
		DECLARE @FundraisingLinkypeID INT
		DECLARE @FiltersLinkTypeID INT

		SELECT @BottomButtonLinkID = REPLACE(@BottomButtonLinkID, 'NULL', '0')
		SELECT @HcBusinessSubCategoryID = REPLACE(@HcBusinessSubCategoryID, 'NULL', '0')
		
		--Capture Category and SubCategory ID's
		SELECT RowNum = IDENTITY(INT, 1, 1)
				,CategoryID = Param
		INTO #CategoryID
		FROM dbo.fn_SplitParamMultiDelimiter(@BottomButtonLinkID, '!~~!')                     
               
		SELECT RowNum = IDENTITY(INT, 1, 1)
				, SubCategoryId = Param
		INTO #SubCategoryID
		FROM dbo.fn_SplitParamMultiDelimiter(@HcBusinessSubCategoryID, '!~~!')  
                                                            
		SELECT CategoryID 
				,SubCategoryId      
		INTO #Find_CatSubCat
		FROM #CategoryID C
		INNER JOIN #SubCategoryID S ON C.RowNum = S.RowNum 

		SELECT @LinkTypeID = HcBottomButtonLinkTypeID
		FROM HcBottomButtonLinkType 
		WHERE BottomButtonLinkTypeName = 'Find'
	 
	    SELECT @EventLinkypeID = HcBottomButtonLinkTypeID
		FROM HcBottomButtonLinkType 
		WHERE BottomButtonLinkTypeName = 'Events'

		SELECT @FundraisingLinkypeID = HcBottomButtonLinkTypeID
		FROM HcBottomButtonLinkType 
		WHERE BottomButtonLinkTypeName = 'Fundraisers'

		--11thJan2016
		--LinkType of Filter functionality
		SELECT @FiltersLinkTypeID = HcBottomButtonLinkTypeID
		FROM HcBottomButtonLinkType 
		WHERE BottomButtonLinkTypeName = 'Filters'
	 


		IF @HcBottomButtonID IS NULL
		BEGIN
			 INSERT INTO HcBottomButton(BottomButtonImage_On
										,BottomButtonImage_Off
										,BottomButtonLinkTypeID
										,BottomButtonLinkID
										,DateCreated
										,CreatedUserID
										,HcHubCitiID
										,HcBottomButtonImageIconID)
										
								SELECT  @BottomButtonImageOn
									  , @BottomButtonImageOff
									  , @BottomButtonLinkTypeID
									  , IIF((SELECT BottomButtonLinkTypeName FROM  HcBottomButtonLinkType WHERE HcBottomButtonLinkTypeID = @BottomButtonLinkTypeID) in('Find','Events','Fundraisers','Filters'), NULL, @BottomButtonLinkID)
									  , GETDATE()
									  , @UserID
									  , @HubCitiID
									  , @HcBottomButtonImageIconID

				SELECT @HcBottomButtonID = SCOPE_IDENTITY()			
																	
		END
		ELSE
		BEGIN						  							  
			  UPDATE HcBottomButton SET BottomButtonImage_On = @BottomButtonImageOn
								 ,BottomButtonImage_Off = @BottomButtonImageOff
								 ,BottomButtonLinkTypeID = @BottomButtonLinkTypeID	
								 ,BottomButtonLinkID = IIF((SELECT BottomButtonLinkTypeName FROM  HcBottomButtonLinkType WHERE HcBottomButtonLinkTypeID = @BottomButtonLinkTypeID) in('Find','Events','Fundraisers','Filters'), NULL, @BottomButtonLinkID)		 
								 ,DateModified = GETDATE()
								 ,ModifiedUserID = @UserID
								 ,HcBottomButtonImageIconID = @HcBottomButtonImageIconID
			  WHERE HcBottomButtonID = @HcBottomButtonID
		END	 

		IF @LinkTypeID=@BottomButtonLinkTypeID		
		BEGIN
			DELETE FROM HcBottomButtonFindRetailerBusinessCategories WHERE HcBottomButonID = @HcBottomButtonID

			IF CURSOR_STATUS('global','CategoryList')>=-1
				BEGIN
				DEALLOCATE CategoryList
				END
                                                
				DECLARE @CategoryID1 int
				DECLARE @SubCategoryID1 varchar(1000)
				DECLARE CategoryList CURSOR STATIC FOR
				SELECT CategoryID
					  ,SubCategoryId          
				FROM #Find_CatSubCat 
                           
				OPEN CategoryList
                           
				FETCH NEXT FROM CategoryList INTO @CategoryID1, @SubCategoryID1
                           
				CREATE TABLE #TempBiz(BusinnessCategoryID INT)


				WHILE @@FETCH_STATUS = 0
				BEGIN   
                            
						INSERT INTO #TempBiz(BusinnessCategoryID) 			                                
						OUTPUT inserted.BusinnessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT F.[Param]						     
						FROM dbo.fn_SplitParam(@CategoryID1, '|') F


						INSERT INTO HcBottomButtonFindRetailerBusinessCategories(HcBottomButonID
																		,BusinessCategoryID
																		,DateCreated
																		,CreatedUserID																	
																		,HcBusinessSubCategoryID)
									OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT @HcBottomButtonID										
										 , @CategoryID1
										 , GETDATE()
										 , @UserID						 
										 , IIF([Param] = 0,NULL,[Param])
									FROM dbo.fn_SplitParam(@SubCategoryID1, '|')
					
									DECLARE @Cnt INT 
									SELECT @Cnt = 0
									SELECT @Cnt = COUNT(Param)
									FROM dbo.fn_SplitParam(@SubCategoryID1, '|')
                                   
                               
						TRUNCATE TABLE #TempBiz
		
						FETCH NEXT FROM CategoryList INTO @CategoryID1, @SubCategoryID1
                                  
				END                        

				CLOSE CategoryList
				DEALLOCATE CategoryList

			
		END		

		IF @EventLinkypeID =@BottomButtonLinkTypeID		
		BEGIN		    
			DELETE FROM HcBottomButtonEventCategoryAssociation WHERE HcBottomButtonID = @HcBottomButtonID

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , BottomButtonLinkID = Param
			INTO #Events
			FROM dbo.fn_SplitParam(@BottomButtonLinkID, ',')
			
			INSERT INTO HcBottomButtonEventCategoryAssociation(HcHubCitiID
															  ,HcBottomButtonID
															  ,HcEventCategoryID
															  ,DateCreated
															  ,CreatedUserID)																			 
			SELECT  @HubCitiID
					,@HcBottomButtonID
					,Param 
					,GETDATE()
					,@UserID			
			FROM HcEventsCategory  B			
			INNER JOIN dbo.fn_SplitParam(@BottomButtonLinkID, ',') P ON B.HcEventCategoryID  = P.Param 
			INNER JOIN #Events T ON P.Param = T.BottomButtonLinkID
					
			ORDER BY T.RowNum
		END

		IF @FundraisingLinkypeID =@BottomButtonLinkTypeID		
		BEGIN		    
			DELETE FROM HcFundraisingBottomButtonAssociation WHERE HcBottomButtonID = @HcBottomButtonID

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , BottomButtonLinkID = Param
			INTO #Fundraisers
			FROM dbo.fn_SplitParam(@BottomButtonLinkID, ',')
			
			INSERT INTO HcFundraisingBottomButtonAssociation(HcHubCitiID
															  ,HcBottomButtonID
															  ,HcFundraisingCategoryID
															  ,DateCreated
															  ,CreatedUserID)																			 
			SELECT  @HubCitiID
					,@HcBottomButtonID
					,Param 
					,GETDATE()
					,@UserID			
			FROM HcFundraisingCategory  B			
			INNER JOIN dbo.fn_SplitParam(@BottomButtonLinkID, ',') P ON B.HcFundraisingCategoryID  = P.Param 
			INNER JOIN #Fundraisers T ON P.Param = T.BottomButtonLinkID
					
			ORDER BY T.RowNum
		END



        --11thJan2016
		--Update Filters
		IF @FiltersLinkTypeID = @BottomButtonLinkTypeID		
		BEGIN		    
			
			DELETE FROM HcBottomButtonFilterAssociation WHERE HcBottomButtonID = @HcBottomButtonID

			SELECT RowNum = IDENTITY(INT, 1, 1)
				 , BottomButtonLinkID = Param
			INTO #Filters
			FROM dbo.fn_SplitParam(@BottomButtonLinkID, ',')
			
			INSERT INTO HcBottomButtonFilterAssociation (HcHubCitiID
															  ,HcBottomButtonID
															  ,HcFilterID
															  ,DateCreated
															  ,CreatedUserID)																			 
			SELECT   @HubCitiID
					,@HcBottomButtonID
					,HcFilterID 
					,GETDATE()
					,@UserID			
			FROM HcFilter B			
			INNER JOIN dbo.fn_SplitParam(@BottomButtonLinkID, ',') P ON B.HcFilterID  = P.Param 
			INNER JOIN #Filters T ON P.Param = T.BottomButtonLinkID
			

		  ORDER BY T.RowNum

		END


	    UPDATE HcHubCiti SET ItunesURL=@ItunesURL,GooglePlayURL=@GooglePlayURL
	    WHERE @BottomButtonLinkTypeID=(SELECT HcBottomButtonLinkTypeID 
	    FROM HcBottomButtonLinkType WHERE BottomButtonLinkTypeName='Share')
	    AND HcHubCitiID=@HubCitiID
	    
		----------------Find Clear Cache URL---03/04/2015----------------------

		DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'


		------------------------------------------------------

	    --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			SELECT ERROR_LINE()	 
			PRINT 'Error occured in Stored Procedure usp_WebHcBottomButtonCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
