USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDisplayRetailLocationInfo]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcDisplayRetailLocationInfo
Purpose					: To Display the basic infomration about the Retail Location.
Example					: usp_WebHcDisplayRetailLocationInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Feb 2015		Mohith H R			Initial Version
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDisplayRetailLocationInfo]
(
	  
	  @HubCitiID int
	, @RetailID int
    , @RetailLocationID int
       
	--Output Variable 	
	, @IOSAppID varchar(500) output
	, @AndroidAppID varchar(500) output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRAN

				BEGIN TRY		
				
				DECLARE @RetailConfig varchar(50)
				DECLARE @RetailLocationAdvertisement VARCHAR(1000)--WelcomePage/SplashAd/BannerAd(previously)
				DECLARE @RibbonAdImagePath varchar(1000)--BannerAd
				DECLARE @RibbonAdURL VARCHAR(1000)--BannerAdURL
				DECLARE @RetailerPageQRURL VARCHAR(1000)
				DECLARE @Config VARCHAR(100)
				DECLARE @SplashAdID int
				DECLARE @BannerAdID int	 
				DECLARE @RetailerDetailsID INT

				SELECT @IOSAppID = IOSAppID
					  ,@AndroidAppID = AndroidAppID
				FROM HcHubCiti
				WHERE HcHubCitiID = @HubCitiID
				
			    
				--Retrieve the server configuration.
				SELECT @Config = ScreenContent 
				FROM AppConfiguration 
				WHERE ConfigurationType LIKE 'QR Code Configuration'
				AND Active = 1 
				
				--To get server Configuration.
				SELECT @RetailConfig= ScreenContent  
				FROM AppConfiguration   
				WHERE ConfigurationType='Web Retailer Media Server Configuration'	
				
				
				--Get the associated Splash Ad/ Welcome Page Details.
				SELECT DISTINCT @RetailLocationAdvertisement =  @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.SplashAdImagePath
					 , @SplashAdID = RLA.AdvertisementSplashID
				FROM AdvertisementSplash RLA
				INNER JOIN RetailLocationSplashAd RL ON RL.RetailLocationID = RL.RetailLocationID
				WHERE RL.RetailLocationID = @RetailLocationID
				AND RLA.RetailID = @RetailID
				AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)
				AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementSplash ASP 
									 INNER JOIN  RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID = RLS.AdvertisementSplashID
									 WHERE RetailLocationID = @RetailLocationID)
									 
				--Get the associated Banner Ad Details.
				SELECT DISTINCT @RibbonAdImagePath = @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
					 , @RibbonAdURL = RLA.BannerAdURL
					 , @BannerAdID = RLA.AdvertisementBannerID
				FROM AdvertisementBanner RLA
				INNER JOIN RetailLocationBannerAd RL ON RL.RetailLocationID = RL.RetailLocationID
				WHERE RL.RetailLocationID = @RetailLocationID
				AND RLA.RetailID = @RetailID
				AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1) --if the end date is set null then that ad is always active.
				AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementBanner ASP 
									 INNER JOIN  RetailLocationBannerAd RLS ON ASP.AdvertisementBannerID = RLS.AdvertisementBannerID
									 WHERE RetailLocationID = @RetailLocationID)
				
				--Get the associated QRPage URL.
				SELECT DISTINCT @RetailerPageQRURL = CASE WHEN QR.QRRetailerCustomPageID IS NOT NULL THEN 
							--@Config  + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(@RetailID AS VARCHAR(10)) + '&retlocId=null&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(ISNULL(@HubCitiId,'null') AS VARCHAR(10))  
							@Config  + '2000.htm?retId=' + CAST(@RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(@RetailLocationID AS VARCHAR(10)) + '&hubcitiId='+ CAST(ISNULL(@HubCitiId,'null') AS VARCHAR(10))  
							ELSE NULL END			
				FROM QRRetailerCustomPage QR
				INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QR.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
				--INNER JOIN QRTypes Q ON QR.QRTypeID = Q.QRTypeID
				WHERE QR.RetailID = @RetailID
				AND QRRCPA.RetailLocationID = @RetailLocationID
				--AND Q.QRTypeName = 'Main Menu Page'
								
				--Find if there is any sale or discounts associated with the Retail Location.
				SELECT DISTINCT Retailid , RetailLocationid
				INTO #RetailItemsonSale
				FROM 
				(SELECT DISTINCT b.RetailID, a.RetailLocationID 
				from RetailLocationDeal a 
				inner join RetailLocation b on a.RetailLocationID = b.RetailLocationID AND b.Active=1
				inner join RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
												   and a.ProductID = c.ProductID 
												   AND A.RetailLocationID = @RetailLocationID
												   and GETDATE() between isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)

				UNION 

				SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
				FROM Coupon C 
				INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				AND CR.RetailLocationID = @RetailLocationID
				GROUP BY C.CouponID
						,NoOfCouponsToIssue
						,CR.RetailID
						,CR.RetailLocationID
				HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
						 ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
						 								   
				UNION
				 
				SELECT DISTINCT  RR.RetailID, RR.RetailLocationID  
				from Rebate R 
				inner join RebateRetailer RR ON R.RebateID=RR.RebateID
				where RR.RetailLocationID = @RetailLocationID
				AND GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

				UNION 

				SELECT DISTINCT  c.retailid, a.RetailLocationID 
				from  LoyaltyDeal a
				INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
				inner join RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
											and a.ProductID = LDP.ProductID 
				inner join RetailLocation c on a.RetailLocationID = b.RetailLocationID 
				Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
				AND A.RetailLocationID = @RetailLocationID

				UNION

				SELECT DISTINCT rl.RetailID, rl.RetailLocationID
				FROM ProductHotDeal p
				INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
				INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID
				LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
				LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
				AND rl.RetailLocationID = @RetailLocationID AND rl.Active=1
				GROUP BY P.ProductHotDealID
						,NoOfHotDealsToIssue
						,rl.RetailID
						,rl.RetailLocationID
				HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
						 ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  
				
				UNION

				SELECT DISTINCT q.RetailID, qa.RetailLocationID
				from QRRetailerCustomPage q
				inner join QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
				inner join QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
				where qa.RetailLocationID = @RetailLocationID 
				and GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE()+1)) a			
						 
				--Display the Basic details of the Retail location.		
				SELECT DISTINCT R.RetailID retailerId
					 , R.RetailName retailerName
					 , RL.RetailLocationID retailLocationID
					 , RL.Address1+','+RL.City+','+RL.State+','+RL.PostalCode   retaileraddress1					
					 , CASE WHEN RL.CorporateAndStore=1 THEN R.CorporatePhoneNo ELSE C.ContactPhone END contactPhone
					 , CASE WHEN RL.RetailLocationURL IS NULL THEN R.RetailURL ELSE RL.RetailLocationURL END retailerURL
					 , @RetailLocationAdvertisement bannerAdImagePath
					 , @RibbonAdImagePath ribbonAdImagePath
					 , @RibbonAdURL ribbonAdURL
					 , image = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
					 --, @RetailerPageQRURL QRUrl 
					 , SaleFlag = CASE WHEN (SELECT COUNT(1) FROM #RetailItemsonSale)>0 THEN 1 ELSE 0 END
					 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')		
					 , @BannerAdID RibbonAdID
					 , @SplashAdID BannerAdID	
					 , QRUrl = @Config  + '2000.htm?retId=' + CAST(@RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(@RetailLocationID AS VARCHAR(10)) + '&hubcitiId='+ CAST(ISNULL(@HubCitiId,'null') AS VARCHAR(10))  				
				FROM  RetailLocation RL 
				INNER JOIN Retailer R ON R.RetailID = RL.RetailID			
				LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
				LEFT JOIN Contact C ON C.ContactID = RC.ContactID		
				WHERE RL.RetailID = @RetailID 
				AND RL.RetailLocationID = @RetailLocationID
				AND (RL.Active=1 AND R.RetailerActive=1)	
				
			
		   --Confirmation of Success.
		   SELECT @Status = 0	
		COMMIT TRANSACTION  
      
 END TRY    	
BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiWeb].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;






















GO
