USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonsAssociation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBottomButtonsAssociation]
Purpose					: To associate respective bottom button functionality to respective modules..
Example					: [usp_WebHcBottomButtonsAssociation] 

History
Version		     Date				Author				Change Description
-------------------------------------------------------------------------- 
1.0				7 July 2014		Mohith H R	        1.0
--------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonsAssociation]
(
	
	--Input Variable
	  @HcHubCitiID int
	, @HcModuleID int
	, @HcModuleBottomButtonIDs varchar(max)	
	--, @HcButtonPosition varchar(max)
    , @UserID int
    
    --OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY		
			

			SELECT  RowNum = IDENTITY(INT, 1, 1)
				  , BottomButtonID = Param
				  , @HcModuleID FunctionalityID
			INTO #Temp
			FROM dbo.fn_SplitParam(@HcModuleBottomButtonIDs, ',')


			DELETE FROM HcFunctionalityBottomButton WHERE HcHubCitiID = @HcHubCitiID AND HcFunctionalityID = @HcModuleID

			INSERT INTO HcFunctionalityBottomButton(  HcFunctionalityID
													, HcBottomButtonID
													, HcHubCitiID
													, DateCreated													
													, CreatedUserID)

											SELECT  @HcModuleID
															, BottomButtonID
															, @HcHubCitiID
															, GETDATE()
															, @UserID
											FROM #Temp
											ORDER BY RowNum 
			

			--Confirmation of Success
			SELECT @Status = 0
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcBottomButtonsAssociation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
