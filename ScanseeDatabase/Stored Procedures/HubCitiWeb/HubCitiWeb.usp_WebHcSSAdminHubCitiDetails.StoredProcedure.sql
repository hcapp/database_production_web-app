USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminHubCitiDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcSSAdminHubCitiDetails
Purpose					: To disply the  details of the selected Hub Citi.
Example					: usp_WebHcSSAdminHubCitiDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminHubCitiDetails]
(
   ----Input variable.
	  @ScanSeeAdminUserID int
	, @HubCitiID int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @State varchar(100)
			DECLARE @City varchar(MAX)
			DECLARE @PostalCodes varchar(MAX)	
			
			--Display the details.
			SELECT DISTINCT H.HcHubCitiID HubCitiID
				 , H.HubCitiName
				 , HC.CityExperienceName citiExperience
				 , HA.Email EmailID
				 , HA.UserName	
				 , H.DefaultPostalCode defaultZipcode				
				 , regHubcitiIds = REPLACE(STUFF((SELECT ', ' + CAST(R.HcHubcitiID AS VARCHAR(1000)) 
											 FROM HcRegionAppHubcitiAssociation R
											 INNER JOIN HcHubCiti H ON R.HcRegionAppID = H.HcHubCitiID AND H.HcHubCitiID = @HubCitiID
											 FOR XML PATH('')), 1, 2, ''),' ','')	 
				, H.SalesPersonEmail SalesPersonEmail
				, H.AdditionalEmailID emails
			FROM HcHubCiti H
			LEFT JOIN Users HA ON HA.HcHubCitiID = H.HcHubCitiID
			LEFT JOIN UserType T ON T.UserTypeID =HA.UserTypeID 
			LEFT JOIN HcCityExperience HC ON HC.HcHubCitiID = H.HcHubCitiID
			WHERE H.HcHubCitiID = @HubCitiID AND T.UserType LIKE 'HubCiti Admin' 
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcSSAdminHubCitiDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
