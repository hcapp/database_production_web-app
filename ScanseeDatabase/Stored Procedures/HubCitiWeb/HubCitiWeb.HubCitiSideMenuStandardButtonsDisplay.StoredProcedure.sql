USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[HubCitiSideMenuStandardButtonsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiSideMenuStandardButtonsDisplay]
Purpose					: To display standard side menu buttons.
Example					: [HubCitiSideMenuStandardButtonsDisplay]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[HubCitiSideMenuStandardButtonsDisplay]
(
  
      @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION	 

		SELECT   CASE WHEN ButtonName = 'Settings' THEN 'App Settings' ELSE ButtonName END menuBtnName
				,ButtonType addDeleteBtn
				,ButtonPosition btnPosition
				,IsStandard isStandard
				,HcLinkTypeID menuFucntionality
		FROM HubCitiSideMenuStandardButtons S
		INNER JOIN HcLinkType L ON L.LinkTypeName = S.ButtonName
		WHERE S.ButtonName NOT IN ('Login/Logout','Feedback')
		ORDER By ButtonPosition


	     

	    --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			SELECT ERROR_LINE()	 
			PRINT 'Error occured in Stored Procedure usp_WebHcBottomButtonCreation.'		
			--- Execute retrieval of Error info
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
