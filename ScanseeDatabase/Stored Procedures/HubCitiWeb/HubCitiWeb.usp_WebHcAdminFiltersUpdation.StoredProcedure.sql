USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminFiltersUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAdminFiltersUpdation]
Purpose					: To Update Filters for given HubCiti.
Example					: [usp_WebHcAdminFiltersUpdation]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminFiltersUpdation]
(
    --Input variable
	  @HubCitiID int
	, @CityExperienceID int
	, @FilterID int 
	, @FilterName varchar(100)
	, @RetailLocationIDs varchar(500) --Comma Seperated ID's 
	  
	--Output Variable
	, @DuplicateName int output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			BEGIN TRANSACTION
			
			DECLARE @UserID INT
			DECLARE @UserType INT
			
			SELECT @UserType = UserTypeID 
			FROM UserType
			WHERE UserType = 'HubCiti Admin'
			
			SELECT @UserID = UserID
			FROM Users
			WHERE UserTypeID = @UserType
			AND HcHubCitiID = @HubCitiID
			
			--Check if given FilterName is already existed.
			IF EXISTS (SELECT 1 FROM HcFilter WHERE FilterName = @FilterName)
			BEGIN
				
				SET @DuplicateName = 1
				
			END
			ELSE 
			BEGIN
				
				--To Update Filter Details.
				UPDATE HcFilter
				SET FilterName = @FilterName
					,DateModified = GETDATE()
					,ModifiedUserID = @UserID
				WHERE HcFilterID = @FilterID
				
				--To Update Filter RetailLocation Details.	
				DELETE FROM HcFilterRetailLocation
				WHERE HcFilterID = @FilterID	
				
				INSERT INTO HcFilterRetailLocation ( HcFilterID
													, RetailLocationID
													, DateCreated
													, CreatedUserID
													)
											SELECT  @FilterID
													, R.Param
													, GETDATE()
													, @UserID
											FROM dbo.fn_SplitParam (@RetailLocationIDs,',') R 
				
				SET @DuplicateName = 0
			END
			
			--Confirmation of Success
			SELECT @Status = 0
			
	        COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminFiltersUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
