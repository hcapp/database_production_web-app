USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventAppSiteDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventAppSiteDisplay
Purpose					: To display list of appsites associated to events.
Example					: usp_WebHcHubCitiEventAppSiteDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			11 Feb 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventAppSiteDisplay]
(   
    --Input variable.	  
	  @HcEventID int	
  
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		   
	
			DECLARE @RetailConfig Varchar(100)
			DECLARE @Config Varchar(100)
			DECLARE @RetailerEvent int

			SELECT @RetailerEvent = RetailID
			FROM HcEvents
			WHERE HcEventID = @HcEventID
			
			SELECT @RetailConfig = ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'  
			
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'  
			
			--To display List fo appsites associated to events.
			IF @RetailerEvent IS NOT NULL
			BEGIN
				SELECT RL.RetailLocationID appSiteId					 			  
					  ,appSiteImg = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 	
					  ,RL.Address1 Address
					  ,RL.City 
					  ,RL.[State] 
					  ,RL.PostalCode 				   	   
				FROM HcRetailerEventsAssociation HE				
				INNER JOIN RetailLocation RL ON HE.RetailLocationID = RL.RetailLocationID AND Active = 1
				INNER JOIN Retailer R ON RL.RetailID = R.RetailID
				WHERE HE.HcEventID = @HcEventID AND (RL.Active=1 AND R.RetailerActive=1)
			END
			ELSE
			BEGIN
				SELECT AP.HcAppSiteID appSiteId
					  ,HcAppSiteName appSiteName				  
					  ,appSiteImg = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
					  ,RL.Address1 Address
					  ,RL.City 
					  ,RL.[State] 
					  ,RL.PostalCode 				   	   
				FROM HcAppSite AP
				INNER JOIN HcEventAppsite EA ON AP.HcAppSiteID = EA.HcAppsiteID
				INNER JOIN RetailLocation RL ON AP.RetailLocationID = RL.RetailLocationID AND Active = 1
				INNER JOIN Retailer R ON RL.RetailID = R.RetailID
				WHERE EA.HcEventID = @HcEventID AND (RL.Active=1 AND R.RetailerActive=1)
			END
									
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventAppSiteDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
