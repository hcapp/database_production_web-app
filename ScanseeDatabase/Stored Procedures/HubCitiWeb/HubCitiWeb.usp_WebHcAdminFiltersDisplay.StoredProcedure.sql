USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminFiltersDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAdminFiltersDisplay]
Purpose					: To Display Filters for given HubCiti.
Example					: [usp_WebHcAdminFiltersDisplay]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminFiltersDisplay]
(
    --Input variable
	  @HubCitiID int
	, @CityExperienceID int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
					
			 SELECT HcFilterID
				   ,FilterName
				   ,CityExperienceName
			FROM HcCityExperience CE
			INNER JOIN HcFilter F ON CE.HcCityExperienceID = F.HcCityExperienceID
			WHERE F.HcHubCitiID = @HubCitiID 
			AND F.HcCityExperienceID = @CityExperienceID
			
			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminFiltersDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
