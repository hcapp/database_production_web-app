USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNationWideHubcitiListUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNationWideHubcitiListUpdation]
Purpose					: To Update Filters for given HubCiti.
Example					: [usp_WebHcNationWideHubcitiListUpdation]

History
Version		Date			Author	              Change Description
--------------------------------------------------------------- 
1.0			3/2/2013	    Dhananjaya TR		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNationWideHubcitiListUpdation]
(
    --Input variable
	  @UserID Int
	, @HubCitiID Int
	, @NationWideFlag Bit
	, @APIPartnerIDs Varchar(2000)
	  
	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			BEGIN TRANSACTION			
			
			--Fetching HubCiti Name.
			DECLARE @HubCitiName Varchar(2000)
			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID 
			
			--Deleting Existing Nationwide details for given HubCiti.
			DELETE FROM HcHubcitiNationWideDeals
			WHERE HcHubCitiID = @HubcitiID 

			--Inserting Nationwide details for given HubCiti.
			INSERT INTO HcHubcitiNationWideDeals(HcHubCitiID
												,HubCitiName
												,NationWideDealsDisplayFlag
												,DateCreated
												,CreatedUserID
												,APIPartnerID)
										SELECT	@HubcitiID
												, @HubCitiName
												, @NationWideFlag
												, GETDATE()
												, @UserID
												, Param
										FROM fn_SplitParam(@APIPartnerIDs,',')    
			

			--Confirmation of Success
			SELECT @Status = 0
			
	        COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNationWideHubcitiListUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;

END;





GO
