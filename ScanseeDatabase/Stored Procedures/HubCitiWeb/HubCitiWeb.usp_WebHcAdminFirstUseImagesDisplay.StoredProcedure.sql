USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminFirstUseImagesDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminFirstUseImagesDisplay
Purpose					: To display First use images forgiven HubCitiID.
Example					: usp_WebHcAdminFirstUseImagesDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23/8/2016	    Shilpashree		       1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminFirstUseImagesDisplay]
(
   --Input variable
	  @HubCitiID int

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
				
				DECLARE @ImagePath Varchar(500)
				SELECT @ImagePath = ScreenContent
				FROM AppConfiguration
				WHERE ConfigurationType = 'HubCitiFirstUseImagePath'

				DECLARE @ScreenName Varchar(max)
				DECLARE @Type Varchar(max)
				DECLARE @FirstUseImage Varchar(max)
				DECLARE @Images Varchar(max)
				DECLARE @AssociatePageURL varchar(100)
				DECLARE @AssociatePageURLTypeID varchar(100)
				DECLARE @AssociatePageURLValue varchar(max)

				SELECT @ScreenName = COALESCE(@ScreenName + ',' ,'') + ScreenName
						,@Type = COALESCE(@Type + ',' ,'') + CAST(Type AS varchar(100))
						,@FirstUseImage = COALESCE(@FirstUseImage + ',' ,'') + (@ImagePath+FirstUseImage)
						,@Images = COALESCE(@Images + ',' ,'') + (FirstUseImage)
						,@AssociatePageURL = COALESCE(@AssociatePageURL + ',' ,'') + CAST(AssociatePageURL AS varchar(100))
						,@AssociatePageURLTypeID = COALESCE(@AssociatePageURLTypeID + ',' ,'') + isnull(CAST(AssociatePageURLTypeID AS varchar(100)),'NULL')
						,@AssociatePageURLValue = COALESCE(@AssociatePageURLValue + ',' ,'') + isnull(CAST(AssociatePageURLValue AS varchar(1000)),'NULL')
				FROM FirstUseLoginImage 
				WHERE HcHubCitiID = @HubCitiID
				
				SELECT 	@ScreenName ScreenName
						,@Type Type
						,@FirstUseImage FirstUseImage
						,@Images Images
						,@AssociatePageURL AssociatePageURL
						,@AssociatePageURLTypeID AssociatePageURLTypeID--REPLACE (@AssociatePageURLTypeID,',!~~!','') AssociatePageURLTypeID 
						,@AssociatePageURLValue AssociatePageURLValue--REPLACE (@AssociatePageURLValue,',!~~!','') AssociatePageURLValue 

										
			   --Confirmation of Success.
			   SELECT @Status = 0
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminFirstUseImagesDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
GO
