USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFilterRetailerListCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFilterRetailerListCreation
Purpose					: To Associate RetailLocations to Filters in Hubciti.
Example					: usp_WebHcFilterRetailerListCreation

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			25/11/2013	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFilterRetailerListCreation]
(
    --Input variable
	  @UserID int
    , @HubCitiID int
    , @FilterID int
    , @RetailLocationID varchar(Max) --Comma separated ID's
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION
			
			--To Delete previously associated RetailLocations
			DELETE FROM HcFilterRetailLocation
			WHERE HcFilterID = @FilterID
			
			--To Associate RetailLocations to given Filter	
			INSERT INTO HcFilterRetailLocation( HcFilterID
											   , RetailLocationID
											   , CreatedUserID
											   , DateCreated
											   )
										SELECT @FilterID
											   , P.Param
											   , @UserID
											   , GETDATE()
										FROM dbo.fn_SplitParam(@RetailLocationID,',')P
																								
			
			--Confirmation of Success
			SELECT @Status = 0	
			      
		COMMIT TRANSACTION
		
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFilterRetailerListCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
			ROLLBACK TRANSACTION
		END;
		 
	END CATCH;
END;







GO
