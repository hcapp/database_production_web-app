USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminCityExperienceCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAdminCityExperienceCreation]
Purpose					: To Create CitiExperience for given HubCiti.
Example					: [usp_WebHcAdminCityExperienceCreation]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminCityExperienceCreation]
(
    --Input variable
	  @HubCitiID int
	, @CityExperienceID int 
	, @RetailLocationIDs varchar(500) --Comma Seperated ID's 
	, @BottomButtonImagePath_ON varchar(1000)
	, @BottomButtonImagePath_OFF varchar(1000) 
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			BEGIN TRANSACTION
			
			DECLARE @UserID INT
			DECLARE @UserType INT
			
			SELECT @UserType = UserTypeID 
			FROM UserType
			WHERE UserType = 'ScanSee Admin'
			
			--To get ScanSee Admin UserID
			SELECT @UserID = UserID
			FROM Users
			WHERE UserTypeID = @UserType
			AND HcHubCitiID = @HubCitiID
			
			--To Add BottomButtonImagePath_ON and BottomButtonImagePath_OFF
			UPDATE HcCityExperience
			SET BottomButtonImagePath_ON = @BottomButtonImagePath_ON,
				BottomButtonImagePath_OFF = @BottomButtonImagePath_OFF
			WHERE HcCityExperienceID = @CityExperienceID
			
			--To delete existed RetailLocations in order to update it
			DELETE FROM HcCityExperienceRetailLocation
			WHERE HcCityExperienceID = @CityExperienceID
			
			--To insert RetailLocations
			INSERT INTO HcCityExperienceRetailLocation (
														 HcCityExperienceID
													   , RetailLocationID
													   , DateCreated
													   , CreatedUserID
													   )
												SELECT   @CityExperienceID
														, R.Param
														, GETDATE()
														, @UserID
												FROM dbo.fn_SplitParam(@RetailLocationIDs, ',') R  
			
			
			--Confirmation of Success
			SELECT @Status = 0
			
			COMMIT TRANSACTION
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminCityExperienceCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
