USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiBandList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventsCreation
Purpose					: Creating New Events.
Example					: usp_WebHcHubCitiEventsCreation

History
Version		Date			Author				Change Description
--------------------------------------------------------------- 
1.0			4th Feb 2014    Pavan Sharma K		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiBandList]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int
	, @LowerLimit int
	, @ScreenName varchar(500)
	, @SearchKey VARCHAR(max)
	, @HcEventID INT
	

	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output
	
	, @BandEventID int output
	, @BandEventStartDate varchar(100) output
	, @BandEventEndDate varchar(100) output


    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(MAX) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION     
	 
				DECLARE @UpperLimit int	
				
				--To get the row count for pagination.	 

				SELECT @UpperLimit = @LowerLimit + ScreenContent  
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName
				AND ConfigurationType = 'Pagination'
				AND Active = 1


				DECLARE @RegionAppFlag BIT		

				CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
                DECLARE @CityList TABLE(CityID Int,CityName Varchar(200),PostalCode varchar(100))               

                IF(SELECT 1 from HcHubCiti H INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID AND H.HcHubCitiID =@HCHubCitiID AND AL.HcAppListName ='RegionApp')>0
                BEGIN
						SET @RegionAppFlag = 1
										    
						INSERT INTO #RHubcitiList(HcHubCitiID)
						
						SELECT DISTINCT HcHubCitiID 
						FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID 
						UNION ALL
						SELECT  @HCHubCitiID AS HcHubCitiID
						
						INSERT INTO @CityList(CityID,CityName,PostalCode)		
						SELECT DISTINCT LA.HcCityID 
										,LA.City	
										,PostalCode				
						FROM #RHubcitiList H
						INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID   						
								
                END
                ELSE
                BEGIN
						SET @RegionAppFlag =0
						INSERT INTO #RHubcitiList(HchubcitiID)
						SELECT @HCHubCitiID  
	
						INSERT INTO @CityList(CityID,CityName,PostalCode)
						SELECT DISTINCT HcCityID 
										,City
										,PostalCode					
						FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID                                   
                END


				SELECT  @BandEventID = HcBandEventID
					   ,@BandEventStartDate = convert(varchar, StartDate, 101)
					   ,@BandEventEndDate = convert(varchar, EndDate, 101)
				
				FROM HcBandEvents
				WHERE HcBandEventID = @HcEventID



		 --To display bands for a particular hubciti along with categories and band names
			;WITH CTE_Display AS(SELECT distinct 
												  BB.BandID bandId
												, BB.BandName bandName
												, EE.BandStage stage
												, convert(varchar, EE.BandStartDate ,101) as eventDate
												, convert(varchar, EE.BandEndDate, 101) as eventEDate
												, LEFT(EE.BandStartTime,5)  as startTime
												, LEFT(EE.BandEndTime,5)  as endTime	
												, case when EE.HcBandEventsAssociationID is not null then '1' else '0' end action
													
					FROM BandCategory B 	
					INNER JOIN BandCategoryAssociation RB ON B.bandCategoryID = RB.bandCategoryID	
					INNER JOIN Band BB ON BB.BandID = RB.BandID		
					INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = BB.PostalCode --AND BB.City = HC.City AND BB.State = HC.State 
					INNER JOIN @CityList CN ON CN.CityID = HC.HcCityID
					INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
					LEFT JOIN HcBandEventsAssociation EE ON EE.BandID = BB.BandID AND RB.BandID = EE.BandID AND EE.HcBandEventID = @HcEventID
					LEFT JOIN HcBandEvents EEE ON EEE.hcbandeventid = EE.hcbandeventid AND EEE.HcBandEventID = @HcEventID
					WHERE BB.BandActive=1 AND (@SearchKey IS NULL OR BB.BandName LIKE '%' + @SearchKey + '%') AND H.HcHubCitiID = @hcHubCitiID

					
					)SELECT DISTINCT * INTO #Temp FROM CTE_Display


					--SELECT * FROM #Temp

				SELECT Rownum = IDENTITY(INT,1,1)
					  ,bandId
					  ,bandName
					  ,stage
					  ,eventDate
					  ,eventEDate
					  ,startTime
					  ,endTime
					  ,action
				INTO #temp1
				FROM #Temp

				--To capture max row number.  
				SELECT @MaxCnt = MAX(rowNum) FROM #Temp1

				SELECT * FROM #temp1
				where RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 

				----this flag is a indicator to enable "More" button in the UI.   
				----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		
		throw 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiBandEventsCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
