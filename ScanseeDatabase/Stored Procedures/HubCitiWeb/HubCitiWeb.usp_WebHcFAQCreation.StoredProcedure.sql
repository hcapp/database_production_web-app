USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFAQCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFAQCreation
Purpose					: Creating New FAQ.
Example					: usp_WebHcFAQCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFAQCreation]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int	
	, @FAQID Int
	, @Question VARCHAR(1000)
    , @Answer VARCHAR(5000)
    , @HcFAQCategoryID Int
	, @ScanSeeAdminUserID Int
	  
	--Output Variable 	
	, @DuplicateFlag Bit Output
	, @HcFAQID INT OUTPUT
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
								
				IF EXISTS(SELECT 1 FROM HcFAQ WHERE Question=@Question AND HcHubCitiID =@HcHubcitiID 
				AND (@FAQID IS NULL OR (@FAQID IS NOT NULL AND @FAQID <>HcFAQID )))
				BEGIN
				
					SET @DuplicateFlag=1
										
				END
				
				ELSE IF @FAQID IS NULL
				BEGIN
					
					INSERT INTO HcFAQ ( HcHubCitiID
										,Question
										,Answer
										,HcFAQCategoryID
										,DateCreated
										,CreatedUserID )	
					SELECT @HcHubcitiID 
					     , @Question  	
						 , @Answer	
						 , @HcFAQCategoryID			   
						 , GETDATE()
						 , @ScanSeeAdminUserID	
						 
					SELECT @HcFAQID = SCOPE_IDENTITY()	 	
						 
					SET @DuplicateFlag=0					
					
				END
				
				ELSE
				BEGIN
                    UPDATE HcFAQ SET HcHubCitiID=@HcHubcitiID 
									,Question=@Question
									,Answer=@Answer
									,HcFAQCategoryID=@HcFAQCategoryID 
									,DateModified=GETDATE()
									,ModifiedUserID=@ScanSeeAdminUserID
					WHERE HcFAQID =@FAQID 
					
					SET @DuplicateFlag=0	  
				END			
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
