USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRetrieveBandCategoryList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcRetrieveBandCategoryList]
Purpose					: To display list of BusinessCategories for Find Module.
Example					: [usp_WebHcRetrieveBandCategoryList]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			15 Oct 2013	     SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRetrieveBandCategoryList]
(   
  
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			SELECT B.BandCategoryID catId
				  ,B.BandCategoryName catName
				  ,SB.BandSubCategoryID subCatId
				  ,SB.BandSubCategoryName subCatName
			FROM BandCategory B
			LEFT JOIN BandSubCategoryType BST on B.BandCategoryID = BST.BandCategoryID
			LEFT JOIN BandSubCategory SB on BST.BandSubCategoryTypeID = SB.BandSubCategoryTypeID
			WHERE Active = 1
			--AND BandCategoryName <> 'Other'
			ORDER BY BandCategoryName,BandSubCategoryName
			
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcRetrieveBandCategoryList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
