USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminSetupLoginPage]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminLoginPageSetup
Purpose					: To set up the Login Page for the Hub Citi App.
Example					: usp_WebHcAdminLoginPageSetup

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminSetupLoginPage]
(
   --Input variable.
      @HCAdminUserID int
	, @HubCitiID int
	, @LogoImage varchar(1000)
	, @BackgroundColor varchar(20)
	, @FontColor varchar(20)
	, @ButtonColor varchar(20)
	, @ButtonFontColor varchar(20)
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION		
			
			DECLARE @PageType int
			
			SELECT @PageType = HcPageTypeID
			FROM HcPageType
			WHERE PageType = 'Login Page'
			
			--Update the login page if one already exists.
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcHubCitiID = @HubCitiID AND HcPageTypeID = @PageType)
			BEGIN
				print 'Update'
				UPDATE HcPageConfiguration SET BackgroundColor = @BackgroundColor
									 , FontColor = @FontColor
									 , DateModified = GETDATE()
									 , ModifiedUserID = @HCAdminUserID
									 , ButtonColor = @ButtonColor
									 , ButtonFontColor = @ButtonFontColor
				WHERE HcHubCitiID = @HubCitiID
				AND HcPageTypeID = @PageType
				
				UPDATE HcHubCiti SET LogoImage = @LogoImage
				WHERE HcHubCitiID = @HubCitiID
				
			END 	
			
			--Created a new record if there is no login page setup yet.
			ELSE
			BEGIN
				print 'Insert'
				INSERT INTO HcPageConfiguration(HcHubCitiID
									  , BackgroundColor
									  , FontColor
									  , DateCreated
									  , CreatedUserID
									  , ButtonColor
									  , ButtonFontColor
									  , HcPageTypeID)
							VALUES(@HubCitiID
								 , @BackgroundColor
								 , @FontColor
								 , GETDATE()
								 , @HCAdminUserID
								 , @ButtonColor
								 , @ButtonFontColor
								 , @PageType)
				UPDATE HcHubCiti SET LogoImage = @LogoImage
				WHERE HcHubCitiID = @HubCitiID
												 
			END
					
			
			
	       --Confirmation of Success.
		   SELECT @Status = 0
	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminLoginPageSetup.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
