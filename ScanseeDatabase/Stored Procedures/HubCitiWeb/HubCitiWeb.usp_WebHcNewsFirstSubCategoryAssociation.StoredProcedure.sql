USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]
Purpose					: To retrive Category Details
Example					: [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]
1.0			16 May 2016	     Sagar Byali			Adding subcategory to default bookmark
----------------------------------------------------------------------------------------------
*/
--exec [HubCitiWeb].[usp_WebHcNewsFirstCategoryUpdation&Deletion] 1,null,null,null,null,null,null,null
CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstSubCategoryAssociation]
(   
	--Input variable
	  @HcHubCitiID int
	, @NewsFirstSubCategoryURL varchar(1000)
	, @NewsCategoryID INT
	, @NewsSubCategoryID varchar(1000)



	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			IF @NewsSubCategoryID = ' '
			SET @NewsSubCategoryID = NULL

			
			IF @NewsFirstSubCategoryURL = ' '
			SET @NewsFirstSubCategoryURL = NULL

			DECLARE @CatName varchar(500)
			SELECT @CatName =  NewsCategoryDisplayValue
			FROM NewsFirstCategory
			WHERE NewsCategoryID = @NewsCategoryID AND Active = 1

			SELECT   Rownum=Identity(Int,1,1)
					,Param NewsFirstSubCategoryURL
			INTO #NewsFirstSubCategoryURL
			FROM fn_SplitParam(@NewsFirstSubCategoryURL,',')

			 SELECT   Rownum=Identity(Int,1,1)
					,Param NewsSubCategoryID
			INTO #NewsSubCategoryID
			FROM fn_SplitParam(@NewsSubCategoryID,',')

			--To delete old news if URL is updated
			--DECLARE @CatName varchar(500)
			--SELECT @CatName = CC.NewsCategoryDisplayValue
			--FROM NewsFirstSettings S
			--INNER JOIN NewsFirstCategory CC ON CC.NewsCategoryID = S.NewsCategoryID 
			--WHERE HcNewsSettingsID = @NewsSettingsID AND CC.Active = 1


---------------------To delete old news based on URL------------------------------------------------------------------

			-- To collect new news settings
			SELECT @NewsCategoryID NewsCategoryID
				  ,NewsSubCategoryID
				  ,@HcHubCitiID HcHubCitiID
				  ,NewsFirstSubCategoryURL
				  ,GETDATE() DateCreated
			INTO #Temp
			FROM #NewsFirstSubCategoryURL N
			INNER JOIN #NewsSubCategoryID S ON N.Rownum=S.Rownum
			WHERE N.NewsFirstSubCategoryURL IS NOT NULL AND N.NewsFirstSubCategoryURL <> ' '

			--select  *from #Temp

			--To collect old news settings
			SElect DISTINCT C.NewsCategoryID,NewsCategoryDisplayValue ,cc.NewsSubCategoryID,cc.NewsSubCategoryDisplayValue,NN.NewsFeedURL,S.NewsFirstSubCategoryURL 
			INTO #Dump
			FROM NewsFirstCategory  C
			INNER JOIN scansee.dbo.NewsFirstSubCategoryType T ON T.NewsCategoryID = C.NewsCategoryID
			INNER JOIN NewsFirstSubCategory CC ON CC.NewsSubCategoryTypeID = T.NewsSubCategoryTypeID
			INNER JOIN NewsFirstSettings NN ON NN.NewsCategoryID = C.NewsCategoryID
			INNER JOIN NewsFirstSubCategorySettings S ON S.NewsCategoryID = NN.NewsCategoryID AND S.NewsSubCategoryID = CC.NewsSubCategoryID
			WHERE c.active = 1 AND S.HcHubCitiID = @HcHubCitiID AND NN.HcHubCitiID = @HcHubCitiID AND C.NewsCategoryID = @NewsCategoryID

			--select  *From #Dump

			--To delete based on comparison
			SELECT DISTINCT NewsCategoryDisplayValue,NewsSubCategoryDisplayValue
			INTO #1
			FROM #Dump D  
			INNER JOIN #Temp S ON S.NewsSubCategoryID = D.NewsSubCategoryID AND S.NewsCategoryID = D.NewsCategoryID 
			AND D.NewsFirstSubCategoryURL = S.NewsFirstSubCategoryURL
			WHERE S.HcHubCitiID = @HcHubCitiID   

			--SELECT * FROM #1
			

			DELETE FROM RssNewsFirstFeedNews
			WHERE NewsType = @CatName AND HcHubCitiID = @HcHubCitiID AND subcategory IS NOT NULL 
			AND Subcategory NOT IN (SELECT DISTINCT NewsSubCategoryDisplayValue FROM #1)  

			DELETE FROM RssNewsFirstFeedNewsStagingTable
			WHERE NewsType = @CatName AND HcHubCitiID = @HcHubCitiID AND subcategory IS NOT NULL 
			AND Subcategory NOT IN (SELECT DISTINCT NewsSubCategoryDisplayValue FROM #1)
--------------------------------------------------------------

			DELETE FROM NewsFirstSubCategorySettings
			WHERE HcHubCitiID= @HcHubCitiID AND NewsCategoryID = @NewsCategoryID


			INSERT INTO NewsFirstSubCategorySettings(
													 NewsCategoryID
													,NewsSubCategoryID
													,HcHubCitiID
													,NewsFirstSubCategoryURL
													,DateCreated)
			SELECT @NewsCategoryID
				  ,NewsSubCategoryID
				  ,@HcHubCitiID
				  ,NewsFirstSubCategoryURL
				  ,GETDATE()
			FROM #NewsFirstSubCategoryURL N
			INNER JOIN #NewsSubCategoryID S ON N.Rownum=S.Rownum
			WHERE N.NewsFirstSubCategoryURL IS NOT NULL AND N.NewsFirstSubCategoryURL <> ' '
		

		--Associating subcategory to default bookmarks---20/9/2016------------------START

			IF EXISTS (SELECT * FROM NewsFirstSettings WHERE HcHubCitiID = @HcHubCitiID AND DefaultCategory = 1)
			BEGIN

					DELETE FROM HcDefaultNewsFirstBookmarkedSubCategories
					WHERE HcHubCitiID= @HcHubCitiID AND NewsFirstCategoryID = @NewsCategoryID

					INSERT INTO HcDefaultNewsFirstBookmarkedSubCategories(NewsSubCategoryID
																			,NewsSubCategoryDisplayValue
																			,HchubcitiID
																			,SortOrder
																			,DateCreated
																			,CreatedBy
																			,Active
																			,HcTempleteID
																	        ,NewsFirstCategoryID)
																	SELECT S.NewsSubCategoryID
																			,SC.NewsSubCategoryDisplayValue
																			,@HcHubCitiID
																			,10
																			,GETDATE()
																			,1
																			,1
																			,NULL
																			,@NewsCategoryID
																FROM #NewsFirstSubCategoryURL N
																INNER JOIN #NewsSubCategoryID S ON N.Rownum=S.Rownum
																INNER JOIN NewsFirstSubCategory SC ON S.NewsSubCategoryID = SC.NewsSubCategoryID
																WHERE S.NewsSubCategoryID IS NOT NULL
																AND N.NewsFirstSubCategoryURL IS  NOT NULL AND N.NewsFirstSubCategoryURL <> ' '
			END

	--Associating subcategory to default bookmarks---20/9/2016------------------END


	

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		throw;
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
