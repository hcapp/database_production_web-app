USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstSubPageList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[[usp_WebHcNewsCategoryUpdation&Deletion]]]
Purpose					: To delete news settings
Example					: [[[usp_WebHcNewsCategoryUpdation&Deletion]]]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali			[usp_WebHcFindCategoryimagesList]
----------------------------------------------------------------------------------------------
*/
--exec [HubCitiWeb].[usp_WebHcNewsFirstCategoryUpdation&Deletion] 1,null,null,null,null,null,null,null
create PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstSubPageList]
(   
	--Input variable
	  @HcHubCitiID int


	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		

			SELECT NewsFirstSubPageID subPageId
				  ,NewsFirstSubPageDisplayName subPageName
			FROM NewsFirstSubPage 


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcFindCategoryimagesList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
