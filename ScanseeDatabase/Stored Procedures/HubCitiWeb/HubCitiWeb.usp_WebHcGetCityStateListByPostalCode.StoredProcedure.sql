USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcGetCityStateListByPostalCode]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcGetCityStateListByPostalCode]
Purpose					: To display list of City, States using postalcode.
Example					: [usp_WebHcGetCityStateListByPostalCode]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			04/06/2015	    SPAN	         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcGetCityStateListByPostalCode]
(   
    --Input variable	  
	  @PostalCode Varchar(50)
	, @HcHubCitiID int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT DISTINCT G.City 
							, G.State stateCode
							, S.StateName
							, G.PostalCode zipCode
			FROM GeoPosition G
			INNER JOIN State S ON S.Stateabbrevation = G.State
			INNER JOIN HcLocationAssociation HA ON HA.City = G.City AND HA.State = G.State AND HA.PostalCode = G.PostalCode
			WHERE G.State = S.Stateabbrevation 
			AND G.PostalCode LIKE @PostalCode + '%'
			AND HA.HcHubCitiID = @HcHubCitiID
				       
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcGetCityStateListByPostalCode].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
