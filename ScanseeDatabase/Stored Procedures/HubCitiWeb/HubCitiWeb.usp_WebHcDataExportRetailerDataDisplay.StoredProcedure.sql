USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcDataExportRetailerDataDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcDataExportRetailerDataDisplay]
Purpose					: To display Retailer data for given module.
Example					: [usp_WebHcDataExportRetailerDataDisplay]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			21 Apr 2015	     SPAN			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcDataExportRetailerDataDisplay]
(   
	--Input Variable
	  @HcDataExportTypeID int
	, @HcHubCitiID int
	, @HcDataExportModuleID Varchar(1000)  --Comma Separated ModuleIDs
	, @StartDate datetime
	, @EndDate datetime	 

	--Output Variable 
	, @ExportFilePath varchar(1000) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY

		DECLARE @Date INT
		DECLARE @Month INT
		DECLARE @Year INT
		DECLARE @HH INT
		DECLARE @MM INT
		DECLARE @ColumnList varchar(1000)
		DECLARE @SqlCommand varchar(1000)

		SELECT @Month = FORMAT(GETDATE(), 'MM')
		SELECT @Date = FORMAT(GETDATE(), 'dd')
		SELECT @Year = FORMAT(GETDATE(), 'yyyy')
		SELECT @HH = FORMAT(GETDATE(), 'HH')
		SELECT @MM = FORMAT(GETDATE(), 'mm')

		--To get export file path
		SELECT @ExportFilePath =  'Retailer_' + CAST(@Month AS varchar(100)) + '-' +CAST(@Date AS varchar(100)) + '-' + CAST(@Year AS varchar(100))
									+ '_' + CAST(@HH AS Varchar(100)) + '-' + CAST(@MM AS Varchar(100)) + '.xls'

		SELECT ID = Param
		INTO #ModuleID
		FROM dbo.fn_SplitParamMultiDelimiter(@HcDataExportModuleID, ',')  

		--To fetch Retailer data for given HubCiti
		SELECT DISTINCT H.HubCitiName
						,RetailName
						,R.RetailID
						,R.RetailerImagePath 
						,RL.RetailLocationID
						,RL.Address1
						,RL.City 
						,RL.State
						,RL.PostalCode
						,RL.RetailLocationImagePath 
						,RL.RetailLocationURL
						,RA.DateCreated
			INTO #RetailerInfo
			FROM HcHubCiti H with(nolock)
			INNER JOIN HcLocationAssociation LA with(nolock) ON LA.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID = @HcHubCitiID
			INNER JOIN HcRetailerAssociation RA with(nolock) ON RA.HcHubCitiID = LA.HcHubCitiID AND Associated = 1 AND H.HcHubCitiID = @HcHubCitiID
			INNER JOIN Retailer R with(nolock) ON R.RetailID = RA.RetailID AND R.RetailerActive=1
			INNER JOIN RetailLocation RL with(nolock) ON RL.RetailLocationID = RA.RetailLocationID AND RL.Active = 1 AND Headquarters = 0
			WHERE R.RetailName <> 'SPANqa%'
			AND CAST(@StartDate AS DATE) <= CAST(R.DateCreated AS DATE) AND CAST(@EndDate AS DATE) >= CAST(R.DateCreated AS DATE) 

		SELECT DISTINCT R.HubCitiName
						,RetailName
						,R.RetailID
						,R.RetailerImagePath 
						,R.RetailLocationID
						,R.Address1
						,R.City 
						,R.State
						,R.PostalCode
						,R.RetailLocationImagePath 
						,R.RetailLocationURL
						,C.ContactFirstName 
						,C.ContactLastname 
						,C.ContactPhone
						,R.DateCreated 
						,BC.BusinessCategoryName 
						,BSC.BusinessSubCategoryName 
						,F.HcFilterID
						,F.FilterName 
			INTO #AllColumns1
			FROM #RetailerInfo R
			LEFT JOIN HcFilterRetailLocation FRL with(nolock) ON FRL.RetailLocationID = R.RetailLocationID
			LEFT JOIN HcFilter F with(nolock) ON F.HcFilterID = FRL.HcFilterID AND F.HcHubCitiID = @HcHubCitiID
			LEFT JOIN RetailContact RC with(nolock) ON RC.RetailLocationID = R.RetailLocationID 
			LEFT JOIN Contact C with(nolock) ON C.ContactID = RC.ContactID 
			LEFT JOIN RetailerBusinessCategory RBC with(nolock) ON RBC.RetailerID = R.RetailID 
			LEFT JOIN BusinessCategory BC with(nolock) ON BC.BusinessCategoryID = RBC.BusinessCategoryID 
			LEFT JOIN HcRetailerSubCategory RS with(nolock) ON RS.RetailLocationID = R.RetailLocationID 
			LEFT JOIN HcBusinessSubCategory BSC with(nolock) ON BSC.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID 
			ORDER BY HubCitiName, RetailName 
			
			SELECT DISTINCT HubCitiName
							,RetailName
							,RetailID
							,IIF(RetailerImagePath = '' , 'N/A',ISNULL(RetailerImagePath,'N/A')) AS RetailerLogo
							,RetailLocationID
							,IIF(Address1 = '' , 'N/A',ISNULL(Address1,'N/A')) AS Address
							,IIF(City = '' , 'N/A',ISNULL(City,'N/A')) AS City
							,IIF(State = '' , 'N/A',ISNULL(State,'N/A')) AS State
							,IIF(PostalCode = '' ,0,ISNULL(PostalCode, 0)) AS PostalCode
							,IIF(RetailLocationImagePath = '' , 'N/A',ISNULL(ISNULL(RetailLocationImagePath,RetailerImagePath),'N/A')) AS RetailLocationLogo
							,IIF(RetailLocationURL = '' , 'N/A',ISNULL(RetailLocationURL,'N/A')) AS Website
							,IIF(ContactFirstName = '' , 'N/A',ISNULL(ContactFirstName,'N/A')) AS ContactFirstName
							,IIF(ContactLastname = '' , 'N/A',ISNULL(ContactLastname,'N/A')) AS ContactLastname
							,IIF(ContactPhone = '' ,'0',ISNULL(ContactPhone,'0')) AS ContactPhone
							,IIF(DateCreated = '' , 'N/A',ISNULL(CAST(DateCreated AS Varchar(500)) ,'N/A')) AS RetailerAssociatedDate 
							,IIF(BusinessCategoryName = '' , 'N/A',ISNULL(BusinessCategoryName,'N/A')) AS CategoryName
							,IIF(BusinessSubCategoryName = '' , 'N/A',ISNULL(BusinessSubCategoryName,'N/A')) AS SubCategoryName
							,IIF(HcFilterID = '' , 0,ISNULL(CAST(HcFilterID AS Varchar(100)),0)) AS FilterID
							,IIF(FilterName = '' , 'N/A',ISNULL(FilterName,'N/A')) AS FilterName
			INTO #AllColumns
			FROM #AllColumns1

			SELECT @ColumnList = COALESCE(@ColumnList + ', ','') + HcDataExportModuleInfo   
			FROM #ModuleID M
			INNER JOIN HcDataExportModule HM with(nolock) ON M.ID = HM.HcDataExportModuleID
			INNER JOIN HcDataExportModuleInformation I with(nolock) ON HM.HcDataExportModuleID = I.HcDataExportModuleID
				
			--SET @SqlCommand = 'SELECT HubCitiName, RetailID, RetailName, RetailerLogo, ' + @ColumnList + ' FROM #AllColumns '
			SET @SqlCommand = 'SELECT DISTINCT '+ @ColumnList + ' FROM #AllColumns '
			EXEC (@SqlCommand)

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  --Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDataExportRetailerDataDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
