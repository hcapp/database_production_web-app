USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFilterListDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcHubCitiFilterList]
Purpose					: to display the Filters to be displayed in Filter module

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Jan 2016    Sagar Byali	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFilterListDisplay]
(     
      @UserID INT 
	, @HcHubCitiID INT
	

	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	        -- To give filter list associated to a hubciti
			SELECT DISTINCT HcFilterID filterID
			               ,FilterName filterName
			FROM  HcFilter
			WHERE HcHubCitiID = @HcHubCitiID
			ORDER BY FilterName
	        
			--confirmation of Success
			SELECT @Status = 0

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFilterListDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT @Status = 1 
			
		END;
		 
	END CATCH;
END;


GO
