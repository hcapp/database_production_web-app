USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsCategoryCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsCategoryCreation
Purpose					: Creating Alerts Category.
Example					: usp_WebHcHubCitiAlertsCategoryCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsCategoryCreation]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int
	, @HcAlertCategoryName Varchar(1000)
	  
	--Output Variable 	
	, @AlertCategoryID int output
	, @DuplicateFlag Bit Output	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
	               
								
				IF NOT EXISTS(SELECT 1 FROM HcAlertCategory WHERE HcAlertCategoryName=@HcAlertCategoryName AND HcHubCitiID=@HcHubCitiID)
				BEGIN
					
					INSERT INTO HcAlertCategory( HcAlertCategoryName 
												,HcHubCitiID 
												,DateCreated 												
												,CreatedUserID )	
					SELECT @HcAlertCategoryName 					   
						 , @HcHubCitiID 					    
						 , GETDATE()
						 , @UserID		
						 
					SET @DuplicateFlag=0	
					
					SET @AlertCategoryID=SCOPE_IDENTITY(); 
					
				END
				
				ELSE
				BEGIN
				
					SET @DuplicateFlag=1
										
				END
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsCategoryCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
