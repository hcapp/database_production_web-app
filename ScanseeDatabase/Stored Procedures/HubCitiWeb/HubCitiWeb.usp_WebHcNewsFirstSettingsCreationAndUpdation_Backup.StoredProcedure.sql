USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstSettingsCreationAndUpdation_Backup]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[HubCitiWeb].[usp_WebHcNewsSettingsCreationAndUpdation]]
Purpose					: To create and update news settings.
Example					: [[HubCitiWeb].[usp_WebHcNewsSettingsCreationAndUpdation]]

History
Version		Date			Author			Change Description
------------------------------------------------------------------------ 
1.0			17/5/2016	   Sagar Byali		1.0
2.0         17/9/2016      Shilpashree      Adding Default Category flag
3.0			28/9/2016	   Sagar Byali	    Adding News ticker 
------------------------------------------------------------------------
*/

create PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstSettingsCreationAndUpdation_Backup]
(
   --Input variable.
      @HcAdminUserID int
	, @HubCitiID int
	, @NewsCategoryID int  --All templetes
	, @NewsCategoryColor VARCHAR(1000) --All templetes
	, @DisplayTypeName VARCHAR(1000) 
	, @NewsFeedURL VARCHAR(1000)
	, @NewsSettingsID INT -- Manage Category Updation
	, @NewsSubPageName VARCHAR(1000)
	, @NewsFirstSubCategoryURL varchar(1000)
	, @NewsSubCategoryID varchar(1000)
	, @IsDefault Bit
	--, @NewsTicker BIT
	--, @NewsStories INT

	--Output Variable
	, @isCategoryAdded bit output
	, @IsSubCategoryAdded bit output
	--, @ClearCacheURL Varchar(max) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION	

			 DECLARE @NewsCategoryCombinationTempleteTypeID INT
			 DECLARE @NewsSubPageID INT
			 DECLARE @NewsCategoryDisplayValue VARCHAR(100)

			 SELECT @NewsCategoryCombinationTempleteTypeID = NewsCategoryTempleteTypeID
			 FROM NewsFirstCategoryTempleteType
			 WHERE NewsCategoryTempleteTypeDisplayValue =  @DisplayTypeName

			 SELECT @NewsSubPageID = NewsFirstSubPageID
			 FROM NewsFirstSubPage
			 WHERE NewsFirstSubPageDisplayName =  @NewsSubPageName

			 SELECT @NewsCategoryDisplayValue = NewsCategoryDisplayValue
			 FROM Newsfirstcategory
			 WHERE NewsCategoryID = @NewsCategoryID 


	
	 				SELECT   Rownum=Identity(Int,1,1)
							,Param NewsFirstSubCategoryURL
					INTO #NewsFirstSubCategoryURL
					FROM fn_SplitParam(@NewsFirstSubCategoryURL,',')

				
					
					SELECT   Rownum=Identity(Int,1,1)
							,Param NewsSubCategoryID
					INTO #NewsSubCategoryID
					FROM fn_SplitParam(@NewsSubCategoryID,',')


					UPDATE #NewsSubCategoryID
					SET NewsSubCategoryID = NULL
					WHERE NewsSubCategoryID = 0
			 

			 

			 IF @NewsSettingsID IS NULL
			 BEGIN
					IF EXISTS (SELECT 1 FROM NewsFirstSettings WHERE HcHubCitiID=@HubCitiID AND NewsCategoryID = @NewsCategoryID)
					BEGIN
					SET @isCategoryAdded = 1
					END
		
					IF NOT EXISTS (SELECT 1 FROM NewsFirstSettings WHERE HcHubCitiID=@HubCitiID AND NewsCategoryID = @NewsCategoryID)
					BEGIN
					INSERT INTO NewsFirstSettings(HcHubCitiID
												,NewsCategoryID
												,DateCreated
												,NewsCategoryColor
												,NewsFeedURL
												,NewsFirstCategoryTempleteTypeID
												,NewsFirstSubPageID
												,DefaultCategory)
												--,NewsTicker
												--,NewsStories)
										SELECT   @HubCitiID
												,@NewsCategoryID
												,GETDATE()
												,@NewsCategoryColor
												,@NewsFeedURL
												,ISNULL(@NewsCategoryCombinationTempleteTypeID,NULL)
												,@NewsSubPageID
												,@IsDefault
												--,@NewsTicker
												--,@NewsStories

					IF (@IsDefault = 1)
					BEGIN
					INSERT INTO HcDefaultNewsFirstBookmarkedCategories(NewsCategoryID
																	,NewsCategoryDisplayValue
																	,SortOrder
																	,DateCreated
																	,CreatedBy
																	,Active
																	,CategoryColor
																	,HcHubCitiID)
															SELECT @NewsCategoryID
																	,@NewsCategoryDisplayValue
																	,10 
																	,GETDATE()
																	,@HcAdminUserID
																	,1
																	,@NewsCategoryColor
																	,@HubCitiID
											
					END
						
					
					IF EXISTS(SELECT 1 FROM #NewsSubCategoryID WHERE NewsSubCategoryID IS NOT NULL)
					BEGIN
					INSERT INTO NewsFirstSubCategorySettings(NewsCategoryID
															,NewsSubCategoryID
															,HcHubCitiID
															,NewsFirstSubCategoryURL
															,DateCreated)
														
													SELECT @NewsCategoryID
															,NewsSubCategoryID
															,@HubCitiID
															,NewsFirstSubCategoryURL
															,GETDATE()
														
													FROM #NewsFirstSubCategoryURL N
													INNER JOIN #NewsSubCategoryID S ON N.Rownum=S.Rownum
													WHERE S.NewsSubCategoryID IS NOT NULL

					IF (@IsDefault = 1)
					BEGIN
					INSERT INTO HcDefaultNewsFirstBookmarkedSubCategories(NewsSubCategoryID
																		,NewsSubCategoryDisplayValue
																		,HchubcitiID
																		,SortOrder
																		,DateCreated
																		,CreatedBy
																		,Active
																		,HcTempleteID
																		,NewsFirstCategoryID)
																SELECT S.NewsSubCategoryID
																      ,SC.NewsSubCategoryDisplayValue
																	  ,@HubCitiID
																	  ,10
																	  ,GETDATE()
																	  ,@HcAdminUserID
																	  ,1
																	  ,NULL
																	  ,@NewsCategoryID
															FROM #NewsFirstSubCategoryURL N
													        INNER JOIN #NewsSubCategoryID S ON N.Rownum=S.Rownum
															INNER JOIN NewsFirstSubCategory SC ON S.NewsSubCategoryID = SC.NewsSubCategoryID
															WHERE S.NewsSubCategoryID IS NOT NULL
					END
					END
						--Added new Category
						SET @isCategoryAdded = 0
					END
				END
		 
				 IF @NewsSettingsID IS NOT NULL
				 BEGIN
						UPDATE NewsFirstSettings
						SET  NewsFeedURL = @NewsFeedURL
							,NewsCategoryColor = @NewsCategoryColor
							,NewsFirstCategoryTempleteTypeID = @NewsCategoryCombinationTempleteTypeID
							,NewsFirstSubPageID = @NewsSubPageID
							,DefaultCategory = @IsDefault
							--,NewsTicker = @NewsTicker
							--,NewsStories = @NewsStories
						WHERE HcHubCitiID = @HubCitiID AND HcNewsSettingsID = @NewsSettingsID
						
						DELETE FROM HcDefaultNewsFirstBookmarkedCategories
						WHERE HcHubCitiID = @HubCitiID AND NewsCategoryID = @NewsCategoryID

						IF (@IsDefault = 1)
						BEGIN
						INSERT INTO HcDefaultNewsFirstBookmarkedCategories(NewsCategoryID
																	,NewsCategoryDisplayValue
																	,SortOrder
																	,DateCreated
																	,CreatedBy
																	,Active
																	,CategoryColor
																	,HcHubCitiID)
															SELECT @NewsCategoryID
																	,@NewsCategoryDisplayValue
																	,10 
																	,GETDATE()
																	,@HcAdminUserID
																	,1
																	,@NewsCategoryColor
																	,@HubCitiID
						END

						SET @isCategoryAdded = NULL

				  END


				  update NewsFirstSettings set NewsCategoryColor = '#000000' where NewsCategoryColor =' '

				  	
--------------------------------------------------MAIN MENU CACHING CHANGES---------------------------------------------------------------------------------------------
----------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------

--		IF EXISTS (SELECT 1 FROM HcHubCiti WHERE HcAppListID = 1 AND HcHubCitiID = @HubCitiID)		
--		BEGIN

--				DECLARE @ClearCacheURL1 varchar(1000),@ClearCacheURL2 varchar(1000),@ClearCacheURL3 varchar(1000)
--				SELECT @ClearCacheURL1= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema3'
--				SELECT @ClearCacheURL2= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema2'
--				SELECT @ClearCacheURL3= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema1'
--				SET @ClearCacheURL = @ClearCacheURL1 +','+ @ClearCacheURL2 +','+ @ClearCacheURL3

--		END

--		ELSE 
--		BEGIN

--				DECLARE @ClearCacheURL11 varchar(1000),@ClearCacheURL22 varchar(1000),@ClearCacheURL33 varchar(1000)
--				SELECT @ClearCacheURL11= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema3'
--				SELECT @ClearCacheURL22= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema2'
--				SELECT @ClearCacheURL33= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema1'
--				SET @ClearCacheURL = @ClearCacheURL11 +','+ @ClearCacheURL22 +','+ @ClearCacheURL33

--		END
--------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------

		 
			

	       --Confirmation of Success.
		   SELECT @Status = 0

	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminSetupAboutUsPage.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
