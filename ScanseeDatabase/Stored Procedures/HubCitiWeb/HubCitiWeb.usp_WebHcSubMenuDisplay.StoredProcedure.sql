USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSubMenuDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcSubMenuDisplay]
Purpose					: To Display the list of sub menus for a given hub citi.
Example					: [usp_WebHcSubMenuDisplay]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			17/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSubMenuDisplay]
(
    --Input variable
	  @HubCitiID int
	, @SearchKey varchar(1000)  	
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @HubCitiSideMenu bit
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @UpperLimit int

			--To get the row count for pagination.	 
			SELECT @UpperLimit = ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   

			CREATE TABLE #HcMenu(Rownum INT, HcMenuID INT,HcHubCitiID INT,HcTemplateID INT,Level BIT,MenuName VARCHAR(1000),IsAssociateNews BIT,IsDefaultHubCitiSideMenu BIT)
			
			SET @HubCitiSideMenu = ISNULL(@HubCitiSideMenu,'0')

			IF @HubCitiSideMenu = 0 
			BEGIN

					INSERT INTO #HcMenu
					--To Display the list of sub menus for a given hub citi		
					SELECT ROW_NUMBER() OVER(ORDER BY MenuName ASC) Rownum
						  ,H.HcMenuID
						  ,HcHubCitiID
						  ,HcTemplateID
						  ,Level
						  ,MenuName
						  ,IsAssociateNews = CASE WHEN IsAssociateNews = 1 THEN 1 ELSE 0 END 
						  ,IsDefaultHubCitiSideMenu = 0
					FROM HcMenu H
					INNER JOIN HcMenuItem HM ON HM.HcMenuID = H.HcMenuID AND HubCitiSideMenu <> 1
					WHERE Level IS NULL AND HcHubCitiID=@HubCitiID AND (@SearchKey IS NULL OR (@SearchKey IS NOT NULL AND MenuName LIKE '%'+@SearchKey+'%'))
					GROUP BY H.HcMenuID,HcHubCitiID,HcTemplateID,Level,MenuName,IsAssociateNews,IsDefaultHubCitiSideMenu 
			END
			ELSE
			BEGIN
					INSERT INTO #HcMenu
					SELECT ROW_NUMBER() OVER(ORDER BY MenuName ASC) Rownum
						  ,H.HcMenuID
						  ,HcHubCitiID
						  ,HcTemplateID
						  ,Level
						  ,MenuName
						  ,IsAssociateNews = 0
						  ,IsDefaultHubCitiSideMenu = CASE WHEN IsDefaultHubCitiSideMenu = 1 THEN 1 ELSE 0 END
					FROM HcMenu H
					INNER JOIN HcMenuItem HM ON HM.HcMenuID = H.HcMenuID
					WHERE Level IS NULL AND HcHubCitiID=@HubCitiID AND HubCitiSideMenu = 1 AND (@SearchKey IS NULL OR (@SearchKey IS NOT NULL AND MenuName LIKE '%'+@SearchKey+'%'))	
					GROUP BY H.HcMenuID,HcHubCitiID,HcTemplateID,Level,MenuName,IsAssociateNews,IsDefaultHubCitiSideMenu
			END

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #HcMenu			

			IF @LowerLimit IS NULL
			BEGIN
				SET @LowerLimit=0
				SET @UpperLimit=@MaxCnt					
			END
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@UpperLimit+@LowerLimit)) > 0 THEN 1 ELSE 0 END 
			print @MaxCnt --print @UpperLimit

			SELECT   Rownum
			        ,HcMenuID menuId
				    ,HcHubCitiID
				    ,HcTemplateID
					,Level
					,MenuName
					,IsAssociateNews isNewsTemp
					,IsDefaultHubCitiSideMenu isDefault
			FROM #HcMenu
			ORDER BY Rownum
			OFFSET @LowerLimit ROWS
			FETCH NEXT ISNULL(@UpperLimit,1) ROWS ONLY	
			
			--Confirmation of Success
			SELECT @Status = 0		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcSubMenuDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
