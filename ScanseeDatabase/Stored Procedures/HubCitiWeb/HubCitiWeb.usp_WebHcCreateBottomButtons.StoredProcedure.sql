USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCreateBottomButtons]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcCreateBottomButtons]
Purpose					: To create bottom buttons wrt hubciti.
Example					: [usp_WebHcCreateBottomButtons] 

History
Version		     Date				Author				Change Description
-------------------------------------------------------------------------- 
1.0				23 April 2014		Mohith H R	        1.0
--------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCreateBottomButtons]
(
	
	--Input Variable
	  @HubCitiID int	
    , @UserID int
    
    --OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY		
			
			DECLARE @FindID int
			--List of modules to have the standard bottom buttons.
			DECLARE @Functionality TABLE(ID INT IDENTITY(1, 1), FunctionalityID INT, Functionality VARCHAR(1000))
			INSERT INTO @Functionality(FunctionalityID, Functionality)
			SELECT HcLinkTypeID
				 , LinkTypeDisplayName
			FROM HcLinkType
			WHERE LinkTypeDisplayName IN ('Find','Nearby','Event','Deals','Find Category') 

			--Standard bottom buttons to be created for the modules.
			DECLARE @BottomButtons TABLE(ID INT IDENTITY(1, 1), BottomButtonLinkType varchar(100))
			INSERT INTO @BottomButtons (BottomButtonLinkType) VALUES('Find'), ('Nearby'), ('Share'),('Preference')
		 
			CREATE TABLE #Temp(BottomButtonID INT, BottomButtonName VARCHAR(255))

			--Bottom Buttons 
			INSERT INTO HcBottomButton(	  BottomButtonName
										, BottomButtonLinkTypeID										
										, DateCreated										
										, CreatedUserID
										, HcBottomButtonImageIconID
										, HcHubCitiID)	
					
					OUTPUT inserted.HcBottomButtonID, inserted.BottomButtonName INTO #Temp(BottomButtonID, BottomButtonName)	
										
																		
					SELECT B.BottomButtonLinkType
							, A.HcBottomButtonLinkTypeID
							, GETDATE()
							, @UserID
							, C.HcBottomButtonImageIconID
							, @HubCitiID
					FROM HcBottomButtonLinkType A
					INNER JOIN @BottomButtons B ON A.BottomButtonLinkTypeDisplayName = B.BottomButtonLinkType
					INNER JOIN HcBottomButtonImageIcons C ON C.HcBottomButtonImageIcon LIKE '%'+B.BottomButtonLinkType+'%'
					ORDER BY B.ID ASC
					--CROSS JOIN @Functionality	   
							
			--SET @BottomButtonID1 = SCOPE_IDENTITY()


			INSERT INTO HcFunctionalityBottomButton(  HcFunctionalityID
													, HcBottomButtonID
													, HcHubCitiID
													, DateCreated													
													, CreatedUserID)

					SELECT DISTINCT FunctionalityID
							      , BottomButtonID
								  , @HubCitiID
								  , GETDATE()
								  , @UserID
					FROM #Temp T
					CROSS JOIN @Functionality F 
					WHERE F.Functionality NOT LIKE 'Deals' AND T.BottomButtonName NOT LIKE 'Preference'
					ORDER BY FunctionalityID



			INSERT INTO HcFunctionalityBottomButton(  HcFunctionalityID
													, HcBottomButtonID
													, HcHubCitiID
													, DateCreated													
													, CreatedUserID)

					SELECT DISTINCT FunctionalityID
							      , BottomButtonID
								  , @HubCitiID
								  , GETDATE()
								  , @UserID
					FROM #Temp T
					CROSS JOIN @Functionality F 
					WHERE F.Functionality LIKE 'Deals' AND T.BottomButtonName NOT LIKE 'Find'
					ORDER BY FunctionalityID


			SELECT @FindID = B.HcBottomButtonID				
			FROM #Temp T
			INNER JOIN HcBottomButton B ON T.BottomButtonID = B.HcBottomButtonID
			WHERE T.BottomButtonName = 'Find'			 

			--Associate the Find Bottom Button to Business Categories.
			INSERT INTO HcBottomButtonFindRetailerBusinessCategories(HcBottomButonID
																   , BusinessCategoryID
																   , DateCreated
																   , CreatedUserID
																   , HcBusinessSubCategoryID)
														SELECT @FindID
															 , A.BusinessCategoryID
															 , GETDATE()
															 , @UserID
															 , C.HcBusinessSubCategoryID
														FROM BusinessCategory A
														LEFT  JOIN HcBusinessSubCategoryType B ON A.BusinessCategoryID=B.BusinessCategoryID
														LEFT  JOIN HcBusinesssubCategory C ON B.HcBusinessSubCategoryTypeID=C.HcBusinessSubCategoryTypeID
														WHERE BusinessCategoryName <> 'Other'

			
			--Confirmation of Success
			SELECT @Status = 0
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcCreateBottomButtons].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
