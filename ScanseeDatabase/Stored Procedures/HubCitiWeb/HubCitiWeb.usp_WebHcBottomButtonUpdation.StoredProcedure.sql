USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcBottomButtonUpdation
Purpose					: Creating Menu Items for a hubciti.
Example					: usp_WebHcBottomButtonUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonUpdation]
(
   --Input variable.  
      @HcBottomButtonID int	
    , @BottomButtonName Varchar(1000)
	, @BottomButtonImageOn Varchar(1000)	
	, @BottomButtonImageOff Varchar(1000)	
	, @BottomButtonLinkTypeID int
	, @BottomButtonLinkID int
	, @Position int
	, @UserID int
	, @HcBottomButtonImageIconID int
	, @HubCitiID int
	  
	--Output Variable 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
			
			UPDATE HcBottomButton SET BottomButtonName = @BottomButtonName
									 ,BottomButtonImage_On = @BottomButtonImageOn
									 ,BottomButtonImage_Off = @BottomButtonImageOff									 
									 ,BottomButtonLinkTypeID = @BottomButtonLinkTypeID
									 ,BottomButtonLinkID = @BottomButtonLinkID
									 ,DateModified = GETDATE()
									 ,ModifiedUserID = @UserID
									 ,HcBottomButtonImageIconID = @HcBottomButtonImageIconID
			WHERE HcBottomButtonID = @HcBottomButtonID
	        
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			SELECT ERROR_LINE()	 
			PRINT 'Error occured in Stored Procedure usp_WebHcBottomButtonUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
