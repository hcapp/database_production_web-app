USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminAssociatedPostalCodeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcSSAdminAssociatedPostalCodeDisplay]
Purpose					: To disply the list of the Postal code associated to Hubciti.
Example					: [usp_WebHcSSAdminAssociatedPostalCodeDisplay]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			17/1/2014	    SPAN			 1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminAssociatedPostalCodeDisplay]
(
    --Input variable
	  @HubCitiID int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To check the association of Retaillocation 
			SELECT DISTINCT L.State 
							, S.StateName 
							, L.City
							, L.PostalCode 
							, isAssocaited = 1
			INTO #Temp
			FROM HcLocationAssociation L
			INNER JOIN State S ON S.StateID =L.StateID AND L.HcHubCitiID = @HubCitiID
			INNER JOIN HcRetailerAssociation RA ON L.HcHubCitiID = RA.HcHubCitiID AND Associated = 1
			INNER JOIN RetailLocation RL ON RA.RetailLocationID = RL.RetailLocationID AND L.PostalCode = RL.PostalCode
			WHERE L.HcHubCitiID = @HubCitiID AND RL.Active=1

			
			--If RetailLocation is associated then it will not allow user to deassociate that postalcode.
			SELECT DISTINCT State 
						  , StateName 
						  , City
						  , PostalCode PostalCodes
						  , isAssocaited 
			FROM #Temp
			UNION ALL
			SELECT State 
			      , S.StateName 
			      , City
				  , PostalCode PostalCodes
				  , isAssocaited = 0
			FROM HcLocationAssociation L
			INNER JOIN State S ON S.StateID =L.StateID 
			WHERE HcHubCitiID = @HubCitiID AND PostalCode NOT IN (SELECT PostalCode FROM #Temp)
			ORDER BY State ASC,City Asc
			
			--Confirmation of Success
			SELECT @Status = 0
			
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcSSAdminAssociatedPostalCodeDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
