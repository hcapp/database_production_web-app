USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceRetailLocationDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcCityExperienceRetailLocationDeletion
Purpose					: Delete the Location associated to the Citi Experience.
Example					: usp_WebHcCityExperienceRetailLocationDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceRetailLocationDeletion]
(   
    --Input variable.	  
	  @CitiExperienceID int
    , @RetailLocationID VARCHAR(4000) 
  
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DELETE FROM HcCityExperienceRetailLocation 
			FROM HcCityExperienceRetailLocation A
			INNER JOIN [HubCitiWeb].fn_SplitParam(@RetailLocationID, ',') B ON A.RetailLocationID = B.Param
			AND HcCityExperienceID = @CitiExperienceID
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceRetailLocationDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
