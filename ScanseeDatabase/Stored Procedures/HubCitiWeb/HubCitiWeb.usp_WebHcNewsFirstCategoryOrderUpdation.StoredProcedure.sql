USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstCategoryOrderUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[[usp_WebHcNewsFirstCategoryOrderUpdation]]]
Purpose					: To update Newscategories with proper order
Example					: [[[usp_WebHcNewsFirstCategoryOrderUpdation]]]

History
Version		   Date				 Author					Change Description
-------------------------------------------------------------------------------------------
1.0         11/09/2016			 Sagar Byali			 Sort Order updation 
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstCategoryOrderUpdation]
(   
	--Input variable
	  @HcHubCitiID int
	, @UserID int
	, @NewsCategoryIDs VARCHAR(1000)

	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY

			DECLARE @SortOrder INT = 0
			
			--To add order to categories
			SELECT NewsCategoryOrder = IDENTITY(INT, 1, 1)
				  ,HcNewsSettingsID = Param
			INTO #Categories
			FROM dbo.fn_SplitParamMultiDelimiter(@NewsCategoryIDs, ',')

			--To update order
			UPDATE NewsFirstSettings
			SET SortOrder = C.NewsCategoryOrder
			FROM NewsFirstSettings N
			INNER JOIN #Categories C ON C.HcNewsSettingsID = N.HcNewsSettingsID
			WHERE N.HcHubCitiID = @HcHubCitiID
			
			--To update order in default categories

			DELETE FROM HcDefaultNewsFirstBookmarkedCategories WHERE HcHubCitiID = @HcHubCitiID
					
			INSERT INTO HcDefaultNewsFirstBookmarkedCategories
			(NewsCategoryID
			,NewsCategoryDisplayValue
			,SortOrder
			,DateCreated
			,CreatedBy
			,Active
			,CategoryColor
			,HcHubCitiID
			,NewsLinkCategoryName
			,IsNewsFeed)
			SELECT  N.NewsCategoryID
					,NewsCategoryDisplayValue
					,SortOrder = ROW_NUMBER() OVER (ORDER BY N.sortorder)
					,GETDATE()
					,3
					,1
					,N.NewsCategoryColor
					,@HcHubCitiID
					,NewsLinkCategoryName
					,IsNewsFeed
			FROM NewsFirstSettings N
			LEFT JOIN NewsFirstCategory T ON T.NewsCategoryID = N.NewsCategoryID AND Active = 1
			WHERE N.HcHubCitiID = @HcHubCitiID  AND DefaultCategory = 1
			ORDER BY N.sortorder
		
		SELECT * FROM HcDefaultNewsFirstBookmarkedCategories WHERE HchubcitiID = @HcHubCitiID

			UPDATE HcUserNewsFirstSingleSideNavigation
			SET IsSideNavigationChanged= 1
			WHERE hcHubcitiID = @hchubcitiID
				  
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		--THROW;
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsFirstCategoryDetails].'		
			--- Execute retrieval of Error info.
			DECLARE @ErrorLine INT
			       ,@ErrorCode INT
				   ,@ErrorProcedure varchar(1000)
				   
			SET @ErrorLine = ERROR_LINE()
			SET @ErrorCode = ERROR_NUMBER() 
			SET @ErrorProcedure = '[HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails]'

			EXEC [usp_GetErrorInfoOutPut]  @ErrorProcedure, @Errorline, @ErrorCode , @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
	
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
