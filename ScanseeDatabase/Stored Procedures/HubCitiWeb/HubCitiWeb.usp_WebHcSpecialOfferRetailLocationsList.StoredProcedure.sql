USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSpecialOfferRetailLocationsList]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcSpecialOfferRetailLocationsList]
Purpose					: To enable the hub citi admin to reset the password after first login.
Example					: [usp_WebHcSpecialOfferRetailLocationsList]

History
Version		Date			Author	     Change Description
--------------------------------------------------------------- 
1.0			3/16/2015        SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSpecialOfferRetailLocationsList]
(
    --Input variable
	  @PageID int
	, @RetailID int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @QRTypeID int
			SELECT @QRTypeID = QRTypeID
			FROM QRTypes
			WHERE QRTypeName = 'Special Offer Page'
			
			--To get Retailer Configuration
			DECLARE @RetailConfig Varchar(250)
			DECLARE @Config Varchar(250)

			SELECT @RetailConfig= ScreenContent  
			FROM AppConfiguration   
			WHERE ConfigurationType='Web Retailer Media Server Configuration'

			SELECT @Config=ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType='App Media Server Configuration'

			SELECT DISTINCT R.RetailName	
					, RL.Address1
					, RL.City
					, RL.State
					, RL.PostalCode
					, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,
										(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)),
												@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)	 
			FROM QRRetailerCustomPage P
			INNER JOIN QRRetailerCustomPageAssociation A ON P.QRRetailerCustomPageID = A.QRRetailerCustomPageID
			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
			INNER JOIN Retailer R ON A.RetailID = R.RetailID
			WHERE P.QRRetailerCustomPageID = @PageID AND P.QRTypeID = @QRTypeID
			AND A.RetailID = @RetailID AND (RL.Active=1 AND R.RetailerActive=1)

			
			--Confirmation of Success
			SELECT @Status = 0

	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcSpecialOfferRetailLocationsList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
