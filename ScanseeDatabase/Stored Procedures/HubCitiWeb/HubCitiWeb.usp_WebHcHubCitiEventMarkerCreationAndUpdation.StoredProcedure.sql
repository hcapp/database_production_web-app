USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiEventMarkerCreationAndUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiEventMarkerCreationAndUpdation
Purpose					: Creating Events marker Details.
Example					: usp_WebHcHubCitiEventMarkerCreationAndUpdation

History
Version		Date			    Author			Change Description
--------------------------------------------------------------- 
1.0			25thMarch2015	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiEventMarkerCreationAndUpdation]
(
   ----Input variable.		  
	  @HcEventsID Int
	, @UserID Int
	, @EventMarkerID Int
	, @EventMarkerName Varchar(2000)
	, @EventMarkerImagePath Varchar(2000)
	, @Latitude Float
	, @Longitude Float
	  
	--Output Variable 	
	, @DuplicateLatLong Bit	OUTPUT
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

		IF EXISTS (SELECT 1 FROM HcEventsOverLayMarkerDetails WHERE HcEventID =@HcEventsID AND Latitude =@Latitude AND Longitude =@Longitude 
		           AND (@EventMarkerID IS NULL OR (@EventMarkerID IS NOT NULL AND @EventMarkerID <> HcEventsOverLayMarkerDetailID)) )
        BEGIN

			SET @DuplicateLatLong =1

		END

		ELSE
		BEGIN
	        
			IF @EventMarkerID IS NULL
			BEGIN

				--Creating Events marker Details.
				INSERT INTO HcEventsOverLayMarkerDetails(HcEventID
														,EventMarkerName
														,EventMarkerImagePath
														,Latitude
														,Longitude
														,DateCreated
														,CreatedUserID)
				SELECT @HCEventsID 
					  ,@EventMarkerName 
					  ,@EventMarkerImagePath 
					  ,@Latitude 
					  ,@Longitude 
					  ,GETDATE()
					  ,@UserID
					  
			END 
			
			ELSE
			BEGIN
				UPDATE HcEventsOverLayMarkerDetails SET HcEventID =@HCEventsID 
				                                       ,EventMarkerName =@EventMarkerName 
													   ,EventMarkerImagePath =@EventMarkerImagePath 
													   ,Latitude =@Latitude 
													   ,Longitude =@Longitude 
				WHERE HcEventsOverLayMarkerDetailID=@EventMarkerID 
			END
		SET @DuplicateLatLong =0	       
	END		
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiEventMarkerCreationAndUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
