USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonIamgeIconsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcBottomButtonIamgeIconsDisplay
Purpose					: To display list of Bottom Button For Hubciti.
Example					: usp_WebHcBottomButtonIamgeIconsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21/10/2013	    Mohith H R	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonIamgeIconsDisplay]
(
   
	--Output Variable 
      @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	        
	       
	        DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT HcBottomButtonImageIconID  menuIconId
			      ,imagePath = @Config + HcBottomButtonImageIcon
			      ,imagePathOff = @Config + HcBottomButtonImageIcon_Off
			FROM HcBottomButtonImageIcons
			WHERE Active =1
								
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcBottomButtonIamgeIconsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
