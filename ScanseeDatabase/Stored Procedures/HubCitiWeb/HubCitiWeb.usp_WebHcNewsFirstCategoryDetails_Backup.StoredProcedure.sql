USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails_Backup]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [[usp_WebHcNewsCategoryDetails]]
Purpose					: To display list of News categories along with images for Combinational Templete.
Example					: [[usp_WebHcNewsCategoryDetails]]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			16 May 2016	     Sagar Byali		 [usp_WebHcNewsFirstCategoryDetails]
2.0         17/9/2016        Shilpashree         Adding Default Category flag
3.0			28/9/2016		 Sagar Byali	     Adding News ticker 
----------------------------------------------------------------------------------------------
*/
create PROCEDURE [HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails_Backup]
(   
	--Input variable
	  @HcHubCitiID int
	, @UserID int
	, @SearchKey VARCHAR(1000)
	, @NewsSettingsID VARCHAR(1000)
	, @SortOrder VARCHAR(1000)
	, @LowerLimit INT
	, @ScreenName VARCHAR(1000)	

	--Output Variable 
	, @MaxCnt INT output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
			--To update Sort order
			SELECT RowNum = IDENTITY(INT, 1, 1)
				  ,NewsSettingsID = Param
			INTO #Temp1
			FROM dbo.fn_SplitParamMultiDelimiter(@NewsSettingsID, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
				  ,SortOrderID = Param
			INTO #Temp2
			FROM dbo.fn_SplitParamMultiDelimiter(@SortOrder, ',')

			SELECT NewsSettingsID
				  ,SortOrderID
			INTO #Sort
			FROM #Temp1 T1
			INNER JOIN #Temp2 T2 ON T1.RowNum=T2.RowNum

			IF @SortOrder IS NOT NULL
			BEGIN
					UPDATE NewsFirstSettings
					SET SortOrder = SortOrderID
					FROM NewsFirstSettings S
					INNER JOIN #Sort T ON T.NewsSettingsID = S.HcNewsSettingsID
			END

			DECLARE @UpperLimit VARCHAR(1000)
				  , @RowsPerPage varchar(1000)

			--To get the row count for pagination.  
            SELECT @UpperLimit = @LowerLimit + ScreenContent
            FROM AppConfiguration   
            WHERE ScreenName = @ScreenName
            AND ConfigurationType = 'Web Pagination' AND Active = 1

			--To display list of News categories along with Color.
			SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY I.HcNewsSettingsID ASC) As Rownum
							,I.HcNewsSettingsID newsSettingsID
							,I.NewsCategoryID AS catId
							,B.NewsCategoryDisplayValue AS catName
							,NewsFeedURL catfeedURL
							,ISNULL(I.NewsCategoryColor,B.NewsCategoryColor) catColor
							,NewsCategoryTempleteTypeDisplayValue  displayType
							,T.NewsCategoryTempleteTypeId newsTempleteTypeId	
							,I.SortOrder  sortOder
							,DefaultCategory As isDefault
							--,NewsTicker isNewsTicker
							--,NewsStories newsStories
			INTO #Temp
			FROM NewsFirstSettings I
			INNER JOIN NewsFirstCategory B ON I.NewsCategoryID = B.NewsCategoryID 
			LEFT JOIN NewsFirstCategoryTempleteType T ON T.NewsCategoryTempleteTypeID = I.NewsFirstCategoryTempleteTypeID
			WHERE HcHubCitiID = @HcHubCitiID --AND NewsFeedURL IS NOt NULL 
			AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (B.NewsCategoryDisplayValue LIKE '%'+@SearchKey+'%')) OR (@SearchKey IS NULL))
			 
			SELECT @MaxCnt = count(1) FROM #Temp
			
			SELECT * 
			FROM #Temp
			WHERE Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit        
			ORDER BY Rownum,catName			

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		--THROW;
			PRINT 'Error occured in Stored Procedure [usp_WebHcNewsFirstCategoryDetails].'		
			--- Execute retrieval of Error info.
			DECLARE @ErrorLine INT
			       ,@ErrorCode INT
				   ,@ErrorProcedure varchar(1000)
				   
			SET @ErrorLine = ERROR_LINE()
			SET @ErrorCode = ERROR_NUMBER() 
			SET @ErrorProcedure = '[HubCitiWeb].[usp_WebHcNewsFirstCategoryDetails]'

			EXEC [usp_GetErrorInfoOutPut]  @ErrorProcedure, @Errorline, @ErrorCode , @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
	
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
