USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonLinkTypeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBottomButtonLinkTypeDisplay]
Purpose					: To Display List of Link Types.
Example					: [usp_WebHcBottomButtonLinkTypeDisplay]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonLinkTypeDisplay]
(
	  @HcHubCitiID int
	    
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @HubCitiName varchar(100)

			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID
			
			--To Display List of Link Types		
			
			IF @HubCitiName = 'spanqa.regionsapp' OR @HubCitiName = 'AddisonTX'
			BEGIN
				SELECT HcBottomButtonLinkTypeID menuTypeId
					 , BottomButtonLinkTypeName menuTypeVal
					 , BottomButtonLinkTypeDisplayName menuTypeName
				FROM HcBottomButtonLinkType		
				WHERE Active = 1
				AND ((BottomButtonLinkTypeName = 'Filters' AND (SELECT COUNT(1) FROM HcFilter WHERE HcHubCitiID = @HcHubCitiID)>0)
				OR (BottomButtonLinkTypeName <> 'Filters'))
				ORDER BY menuTypeName ASC
			END
			ELSE
			BEGIN


			--To display all existing link types

				--Hubregion/Hubciti check 
				IF EXISTS(SELECT 1 FROM HcHubCiti H
				          INNER JOIN HcAppList A ON H.HcAppListID=A.HcAppListID AND A.HcAppListName='RegionApp' AND H.HcHubCitiID=@HcHubCitiID)  

                BEGIN
				--To display Link types of HubRegion
				SELECT HcBottomButtonLinkTypeID menuTypeId
					 , BottomButtonLinkTypeName menuTypeVal
					 , BottomButtonLinkTypeDisplayName menuTypeName
				FROM HcBottomButtonLinkType		
				WHERE Active = 1 AND BottomButtonLinkTypeName <> 'GovQA'
				AND ((BottomButtonLinkTypeName = 'Filters' AND (SELECT COUNT(1) FROM HcFilter WHERE HcHubCitiID = @HcHubCitiID)>0)
				OR (BottomButtonLinkTypeName <> 'Filters'))
				ORDER BY menuTypeName ASC
				END

				ELSE

				BEGIN
				--To display Link types of Hubciti
				SELECT HcBottomButtonLinkTypeID menuTypeId
					 , BottomButtonLinkTypeName menuTypeVal
					 , BottomButtonLinkTypeDisplayName menuTypeName
				FROM HcBottomButtonLinkType		
				WHERE Active = 1 AND BottomButtonLinkTypeName <> 'GovQA' AND BottomButtonLinkTypeName <> 'City Favorites' 
				AND ((BottomButtonLinkTypeName = 'Filters' AND (SELECT COUNT(1) FROM HcFilter WHERE HcHubCitiID = @HcHubCitiID)>0)
				OR (BottomButtonLinkTypeName <> 'Filters'))
				ORDER BY menuTypeName ASC
			END

			END
			
			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcBottomButtonLinkTypeDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
