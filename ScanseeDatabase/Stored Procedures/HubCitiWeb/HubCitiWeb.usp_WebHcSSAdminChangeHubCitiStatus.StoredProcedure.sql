USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminChangeHubCitiStatus]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcSSAdminHubCitiDeactivation
Purpose					: To deativate the given Hub Citi.
Example					: usp_WebHcSSAdminHubCitiDeactivation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			11/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminChangeHubCitiStatus]
(
   ----Input variable.
	  @ScanSeeAdminUserID int
	, @HubCitiID int
	, @Deactivate bit
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
			
			--Deactivate the Hub Citi
			UPDATE HcHubCiti SET Active = CASE WHEN @Deactivate = 1 THEN 0 ELSE 1 END 
							   , ModifiedUserID = @ScanSeeAdminUserID
							   , DateModified = GETDATE() 
			WHERE HcHubCitiID = @HubCitiID
			
			--Change the user to deactivated state.
			UPDATE Users SET [Enabled] = CASE WHEN @Deactivate = 1 THEN 0 ELSE 1 END 
			FROM Users U 
			INNER JOIN HcHubCiti HC ON HC.HcHubCitiID = U.HcHubCitiID
			AND HC.HcHubCitiID = @HubCitiID
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcSSAdminHubCitiDeactivation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
