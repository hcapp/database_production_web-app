USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_HubCitiWebCouponDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HubCitiWebCouponDisplay    
Purpose				  : To display list of coupons.   
Example				  : usp_HubCitiWebCouponDisplay    
    
History
Version		Date			Author			Change Description
-----------------------------------------------------------------
1.0        12/6/2016	  Shilpashree	    Initial version
-----------------------------------------------------------------  
*/    
    
CREATE PROCEDURE [HubCitiWeb].[usp_HubCitiWebCouponDisplay]    
(    
	   @HcHubCitiID int    
	 , @CouponSearch Varchar(100)    
	 , @LowerLimit int
	 , @ScreenName varchar(50)
     
	 --Output Variable   
	  , @MaxCnt int output    
	  , @NxtPageFlag bit output 
	  , @Status int output    
	  , @ErrorNumber int output    
	  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
	BEGIN TRY    
	   
		DECLARE @UpperLimit int

		--To get the row count for pagination.	 
		SELECT @UpperLimit = @LowerLimit + ScreenContent
		FROM AppConfiguration   
		WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1   
 
		SELECT DISTINCT RowNum = ROW_NUMBER() OVER (ORDER BY CouponDateAdded DESC, CouponName ASC) 
					,C.CouponID     
					,CouponName     
					,BannerTitle     
					,CouponShortDescription   
					,CONVERT(VARCHAR(10),CouponStartDate,105) + ' ' + CONVERT(VARCHAR(10),CouponStartDate,108) CouponStartDate    
					,CONVERT(VARCHAR(10),CouponExpireDate,105) + ' ' + CONVERT(VARCHAR(10),CouponExpireDate,108) CouponEndDate    
					,ActualCouponExpirationDate AS CouponExpireDate 
					,KeyWords AS keyword
					,ISNULL(FC.Featured,0) Featured 
					,IsEnded = CASE WHEN CAST(C.CouponExpireDate as smalldatetime) < cast(getdate() as smalldatetime) then 1 else 0 end
		INTO #Coupons
		FROM Coupon C  
		LEFT JOIN HubCitiFeaturedCoupon FC ON FC.CouponID = C.CouponID AND FC.HubCitiID = C.HcHubCitiID AND FC.HubCitiID = @HcHubCitiID
		WHERE C.HcHubCitiID = @HcHubCitiID --AND isnull(cast(C.ActualCouponExpirationDate as date),getdate()+1) >= cast(GETDATE() as date)
		AND (C.CouponName LIKE CASE WHEN @CouponSearch IS NOT NULL THEN '%'+@CouponSearch+'%' ELSE '%' END
				OR C.KeyWords LIKE CASE WHEN @CouponSearch IS NOT NULL THEN '%'+@CouponSearch+'%' ELSE '%' END)
 	
		--To capture max row number.  
		SELECT @MaxCnt = isnull(MAX(RowNum),0) FROM #Coupons
 
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

		SELECT  RowNum    
			,CouponID couponId   
			,CouponName coupTitle  
			,BannerTitle coupBanTitle     
			,CouponShortDescription coupDescription    
			,CouponStartDate coupStartDate  
			,CouponEndDate coupEndDate   
			,CouponExpireDate coupExpiryDate
			,keyword coupKeywords
			,Featured coupfeature
			,IsEnded
		FROM #Coupons    
		WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  
		   
		--Confirmation of Success
		SELECT @Status = 0	 
        
	END TRY    
      
	BEGIN CATCH    
     
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponDisplay.'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       
			--Confirmation of Failure
			SELECT @Status = 1
		END;    
       
	END CATCH;    
END;






GO
