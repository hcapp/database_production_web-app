USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAssociatedRetailersDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAssociatedRetailersDisplay]
Purpose					: To disply the list of Retailers associated to given Hubciti.
Example					: [usp_WebHcAssociatedRetailersDisplay]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			20/1/2014	     SPAN			 1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAssociatedRetailersDisplay]
(
    --Input variable
	  @HubCitiID int
	, @City varchar(50)
	, @State varchar(50)
	, @PostalCode varchar(MAX)
	, @RetailName varchar(100)
	, @LowerLimit int
	, @ScreenName varchar(50)
	, @AssociatedFlag bit  
	  
	--Output Variable
	, @MaxCnt int output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			-- TO Remove Special Character

			SELECT @RetailName = REPLACE(REPLACE(@RetailName,'’',''),'?','')
			
			DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
		    CREATE TABLE #Retailer(RowNum INT IDENTITY (1,1)
									,RetailID INT
									,RetailName NVARCHAR(2000)
									,RetailLocationID INT
									,Address1 Varchar(2000)
									,City Varchar(30)
									,[State] varchar(10)
									,PostalCode varchar(20)
									)
									
		    
			--To Display list of Associted Retailers based on searchKey,Here Searchkey can be City,State,PostalCode,RetailName
			
			IF @AssociatedFlag = 1
			BEGIN
					INSERT INTO #Retailer(RetailID
										  ,RetailName
										  ,RetailLocationID
										 ,Address1
										,City 
										,[State]
										,PostalCode
										  )
					SELECT RetailID
							,RetailName
							,RetailLocationID
							,Address1
							,City 
							,[State]
							,PostalCode
										
					FROM
						(SELECT DISTINCT R.RetailID
										,CONVERT(NVARCHAR(max),R.RetailName) RetailName--REPLACE(REPLACE(R.RetailName,'’',''),'?','') RetailName
										,RL.RetailLocationID
										,RL.Address1
										,RL.City 
										,RL.[State]
										,RL.PostalCode										
										--,RetailAddress = R.RetailName + ', ' + RL.Address1 + ', ' + RL.City + ', ' + RL.[State] + ', ' + RL.PostalCode
						FROM HcRetailerAssociation RA
						INNER JOIN Retailer R ON RA.RetailID = R.RetailID AND RA.HcHubCitiID = @HubCitiID AND RA.Associated = 1
						INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND RA.RetailLocationID = RL.RetailLocationID AND RL.Headquarters = 0 AND Active = 1
						INNER JOIN HcLocationAssociation LA ON RL.PostalCode = LA.PostalCode  AND LA.HcHubCitiID = @HubCitiID AND Associated = 1 AND RL.HcCityID = LA.HcCityID AND RL.[StateID] = LA.[StateID]
						LEFT JOIN [HubCitiWeb].[fn_SplitParam](@PostalCode, ',')Fn ON Fn.Param = LA.PostalCode
						WHERE ((@City IS NULL) OR (@City IS NOT NULL AND (LA.City = @City)))
						AND ((@State IS NULL) OR (@State IS NOT NULL AND (LA.[State] = @State)))
						-- ((LA.City = @City) OR (@City IS NULL))
						--AND ((LA.[State] = @State) OR (@state IS NULL))
						AND ((LA.PostalCode = FN.Param) OR (FN.Param IS NULL))
						AND (((REPLACE(REPLACE(R.RetailName,'’',''),'?','')) LIKE '%'+ @RetailName +'%') OR (@RetailName IS NULL))
						AND R.RetailerActive=1
						)AssociatedRetailer
					ORDER BY RetailName
									
			END
			
			--To Display list of UnAssocited Retailers based on searchKey,Here Searchkey can be City,State,PostalCode,RetailName
			
			ELSE IF @AssociatedFlag = 0
			BEGIN
			
					INSERT INTO #Retailer(RetailID 
										  ,RetailName 
										  ,RetailLocationID 
										  ,Address1 
											,City  
											,[State] 
											,PostalCode 
										  )
					SELECT RetailID
							,RetailName
							,RetailLocationID
							,Address1
											,City 
											,[State]
											,PostalCode
					FROM
						(SELECT DISTINCT R.RetailID 
										,CONVERT(NVARCHAR(max),R.RetailName) RetailName--REPLACE(REPLACE(R.RetailName,'’',''),'?','') RetailName
										,RL.RetailLocationID 
										,RL.Address1 
										,RL.City 
										,RL.[State] 
										,RL.PostalCode 
										--,RetailAddress = R.RetailName + ', ' + RL.Address1 + ', ' + RL.City + ', ' + RL.[State] + ', ' + RL.PostalCode
						FROM HcLocationAssociation LA 
						INNER JOIN RetailLocation RL ON RL.PostalCode = LA.PostalCode AND RL.Headquarters = 0 AND Active = 1 AND RL.HcCityID = LA.HcCityID AND RL.[StateID] = LA.[StateID]
						INNER JOIN Retailer R ON R.RetailID = RL.RetailID
						INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID =RL.RetailLocationID AND RA.HcHubCitiID =@HubCitiID  
						LEFT JOIN [HubCitiWeb].[fn_SplitParam](@PostalCode, ',')Fn ON Fn.Param = LA.PostalCode
						WHERE LA.HcHubCitiID = @HubCitiID AND Associated = 0
						AND ((@City IS NULL) OR (@City IS NOT NULL AND (LA.City = @City)))
						AND ((@State IS NULL) OR (@State IS NOT NULL AND (LA.[State] = @State)))
						-- ((LA.City = @City) OR (@City IS NULL))
						--AND ((LA.[State] = @State) OR (@state IS NULL))
						AND ((LA.PostalCode = FN.Param) OR (FN.Param IS NULL))
						AND (((REPLACE(REPLACE(R.RetailName,'’',''),'?','')) LIKE '%'+ @RetailName +'%') OR (@RetailName IS NULL))
						AND R.RetailerActive=1
									
						)UnAssociatedRetailer
				ORDER BY RetailName 
			
			END
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Retailer
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
	

			SELECT Rownum
				   , RetailID retId
				   , RetailName retName
				   , RetailLocationID retLocId
			       ,Address1 address
				   ,City  city
				   ,[State] state
				   ,PostalCode postalCode
			FROM #Retailer
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY RowNum 
			
			--Confirmation of Success
			SELECT @Status = 0
			
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAssociatedRetailersDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













GO
