USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRoleBasedUserCreationandUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcRoleBasedUserCreationandUpdation
Purpose					: To create the hub citi Role based users by admin.
Example					: usp_WebHcRoleBasedUserCreationandUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24/07/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcRoleBasedUserCreationandUpdation]
(
   ----Input variable.
      @HubcitiUserID Int --Hubciti Created UserID
    , @HcHubCitiID Int
	, @RoleBasedUserID INT --Use while updating
	, @FirstName Varchar(1000)
	, @LastName Varchar(1000)
	, @EmailID varchar(255)
	, @UserName varchar(255)
	, @UserStatus Bit
	, @Password varchar(255)
	, @ModuleID Varchar(1000)--Comma seperated
	, @UserType Varchar(1000) --Comma seperated
	, @EventCategoryID Varchar(2000) --Comma separated
	, @FundraisingEventCategoryID Varchar(2000) --Comma separated
	
	
	  
	--Output Variable 
    , @Status int output	
    , @DuplicateEmail bit output   
    , @DuplicateUserName bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
			--DECLARE @HubCitiID INT
			DECLARE @HcRoleUserID INT
			SET @DuplicateEmail = 0
			SET @DuplicateUserName = 0	
			
			
			SELECT RowNum = IDENTITY(INT, 1, 1)
					, ModuleID = Param
			INTO #Module
			FROM dbo.fn_SplitParam(@ModuleID, ',')

			SELECT RowNum = IDENTITY(INT, 1, 1)
					, UserType = Param
			INTO #UserType
			FROM dbo.fn_SplitParam(@UserType, ',')	
					
			--Check for User Name duplication.
			IF EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName AND @RoleBasedUserID IS NULL) --AND (@RoleBasedUserID IS NULL OR( @RoleBasedUserID IS NOT NULL AND @RoleBasedUserID <>UserID))) 
			BEGIN
				
				SET @DuplicateUserName = 1
			END
			--Check if the EmailID duplication.
			--ELSE IF EXISTS(SELECT 1 FROM Users WHERE Email = @EmailID AND @RoleBasedUserID IS NULL) --AND (@RoleBasedUserID IS NULL OR( @RoleBasedUserID IS NOT NULL AND @RoleBasedUserID <>UserID)))-- NOT NULL AND HcHubCitiID <>@HcHubCitiID )
			--BEGIN			   
			--	SET @DuplicateEmail = 1
			--END		

			ELSE IF @RoleBasedUserID IS NULL			
				--Creation of Hub Citi.
				BEGIN						
		
				  
						--Created the Role Based user.
						INSERT INTO Users( FirstName 
						                  ,Lastname 
										  ,Email
										  ,UserName
										  ,Password										
										  ,DateCreated
										  ,CreatedUserID
										  ,UserTypeID 										  
										  ,Enabled
										  ,HcHubCitiID)
						SELECT @FirstName 
						      ,@LastName   
						      ,@EmailID
							  , @UserName
							  , @Password									
							  , GETDATE()
							  , @HubcitiUserID 
							  , (SELECT UserTypeID FROM UserType WHERE UserType='Super User')
							  , @UserStatus
							  , @HcHubCitiID
									 
						SELECT @HcRoleUserID = SCOPE_IDENTITY()	

					
						--Associate the user to role
						
						INSERT INTO HcUserRole(HcAdminUserID
												, HcRoleID
												, DateCreated
												, CreatedUserID
												,HcRoleBasedModuleListID)
						SELECT    @HcRoleUserID
								, HcRoleID 
								, GETDATE()
								, @HubcitiUserID 
								,M.ModuleID
						FROM HcRole R 
						INNER JOIN #UserType UR ON UR.UserType = R.HcRoleName
						INNER JOIN #Module M ON M.RowNum=UR.RowNum
						--INNER JOIN fn_SplitParam(@UserType,',') F ON F.Param = R.HcRoleName

						--WHERE HcRoleName IN ('ROLE_EVENT_USER','ROLE_FUNDRAISING_USER')
						
						
						IF @EventCategoryID IS NOT NULL AND @FundraisingEventCategoryID IS NOT NULL
						BEGIN 

							UPDATE Users SET UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Normal User')
							WHERE UserID=@HcRoleUserID
							
							--Assoiate user to Event category
							INSERT INTO HcRoleBasedUserEventCategoryAssociation(HcEventCategoryID
																			   ,HcHubcitiID
																			   ,UserID
																			   ,DateCreated
																			   ,CreatedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@HcRoleUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@EventCategoryID,',')

							--Assoiate user to Fundraising Event category
							INSERT INTO HcFundraisingRoleBasedUserCategoryAssociation(HcFundraisingCategoryID
																			   ,HcHubcitiID
																			   ,RoleBasedUserID
																			   ,DateCreated
																			   ,CreatedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@HcRoleUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@FundraisingEventCategoryID,',')
						END
						ELSE IF @EventCategoryID IS NOT NULL AND @FundraisingEventCategoryID IS NULL
						BEGIN 
							
							UPDATE Users SET UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Normal User')
							WHERE UserID=@HcRoleUserID


							--Assoiate user to Event category
							INSERT INTO HcRoleBasedUserEventCategoryAssociation(HcEventCategoryID
																			   ,HcHubcitiID
																			   ,UserID
																			   ,DateCreated
																			   ,CreatedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@HcRoleUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@EventCategoryID,',')
						 END
						 ELSE IF @EventCategoryID IS NULL AND @FundraisingEventCategoryID IS NOT NULL
						 BEGIN

							UPDATE Users SET UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Normal User')
							WHERE UserID=@HcRoleUserID

							--Assoiate user to Fundraising Event category
							INSERT INTO HcFundraisingRoleBasedUserCategoryAssociation(HcFundraisingCategoryID
																			   ,HcHubcitiID
																			   ,RoleBasedUserID
																			   ,DateCreated
																			   ,CreatedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@HcRoleUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@FundraisingEventCategoryID,',')
						 END
			
			   END

			   ELSE IF @RoleBasedUserID IS NOT NULL 
			   BEGIN 

						IF @Password IS NOT NULL
						BEGIN 

							UPDATE Users SET FirstName =@FirstName 
												  ,Lastname =@LastName 	
												  ,Email =@EmailID 
												  ,Password =@Password 									  
												  ,DateModified =GETDATE()
												  ,ModifiedUserID = @HubcitiUserID 
												  ,UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Super User')
												  ,Enabled=@UserStatus 
							WHERE UserID =@RoleBasedUserID 

							UPDATE Users SET ResetPassword =0
							WHERE UserID =@RoleBasedUserID

						END

						ELSE 
						BEGIN 

							UPDATE Users SET FirstName =@FirstName 
												  ,Lastname =@LastName 																			  
												  ,DateModified =GETDATE()
												  ,ModifiedUserID = @HubcitiUserID 
												  ,UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Super User')
												  ,Enabled=@UserStatus 
							WHERE UserID =@RoleBasedUserID						
						END


					DELETE FROM HcUserRole 
					WHERE HcAdminUserID=@RoleBasedUserID 
					
					--Associate the user created to the user role			
					INSERT INTO HcUserRole(HcAdminUserID
											, HcRoleID
											, DateCreated
											, CreatedUserID
											,HcRoleBasedModuleListID)
					SELECT    @RoleBasedUserID
							, HcRoleID 
							, GETDATE()
							, @HubcitiUserID 
							, M.ModuleID
					FROM HcRole R  
					INNER JOIN #UserType UR ON UR.UserType = R.HcRoleName
					INNER JOIN #Module M ON M.RowNum=UR.RowNum
						
					 
                    DELETE FROM HcRoleBasedUserEventCategoryAssociation 
					WHERE UserID =@RoleBasedUserID 

					DELETE FROM HcFundraisingRoleBasedUserCategoryAssociation 
					WHERE RoleBasedUserID =@RoleBasedUserID 

					IF @EventCategoryID IS NOT NULL AND @FundraisingEventCategoryID IS NOT NULL
						BEGIN 

							UPDATE Users SET UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Normal User')
							WHERE UserID=@HcRoleUserID
							
							--Assoiate user to Event category
							INSERT INTO HcRoleBasedUserEventCategoryAssociation(HcEventCategoryID
																			   ,HcHubcitiID
																			   ,UserID
																			   ,DateModified
																			   ,ModifiedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@RoleBasedUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@EventCategoryID,',')

							--Assoiate user to Fundraising Event category
							INSERT INTO HcFundraisingRoleBasedUserCategoryAssociation(HcFundraisingCategoryID
																			   ,HcHubcitiID
																			   ,RoleBasedUserID
																			   ,DateModified
																			   ,ModifiedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@RoleBasedUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@FundraisingEventCategoryID,',')
						END
						ELSE IF @EventCategoryID IS NOT NULL AND @FundraisingEventCategoryID IS NULL
						BEGIN 
							
							UPDATE Users SET UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Normal User')
							WHERE UserID=@HcRoleUserID


							--Assoiate user to Event category
							INSERT INTO HcRoleBasedUserEventCategoryAssociation(HcEventCategoryID
																			   ,HcHubcitiID
																			   ,UserID
																			   ,DateModified
																			   ,ModifiedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@RoleBasedUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@EventCategoryID,',')
						 END
						 ELSE IF @EventCategoryID IS NULL AND @FundraisingEventCategoryID IS NOT NULL
						 BEGIN

							UPDATE Users SET UserTypeID =(SELECT UserTypeID FROM UserType WHERE UserType='Normal User')
							WHERE UserID=@HcRoleUserID

							--Assoiate user to Fundraising Event category
							INSERT INTO HcFundraisingRoleBasedUserCategoryAssociation(HcFundraisingCategoryID
																			   ,HcHubcitiID
																			   ,RoleBasedUserID
																			   ,DateModified
																			   ,ModifiedUserID)
							SELECT Param 
							      ,@HcHubCitiID 
								  ,@RoleBasedUserID
								  ,GETDATE()
								  ,@HubcitiUserID 
							FROM fn_SplitParam(@FundraisingEventCategoryID,',')
						 END
						

			   END


	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcRoleBasedUserCreationandUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
