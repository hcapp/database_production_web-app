USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingCategoryUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingCategoryUpdation
Purpose					: Update selected Fundraising Category.
Example					: usp_WebHcHubCitiFundraisingCategoryUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Aug 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingCategoryUpdation]
(   
    --Input variable.	  
      @ScanSeeAdminUserID Int
    , @HcFundraisingCategoryID Int
	, @HcFundraisingCategoryName Varchar(1000)
	, @CategoryImagePath Varchar(1000)
  
	--Output Variable 
	, @DuplicateFlag bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
				
				--Update selected fundraising category.
			
				IF(SELECT COUNT(1) FROM HcFundraisingCategory WHERE FundraisingCategoryName = @HcFundraisingCategoryName AND HcFundraisingCategoryID <> @HcFundraisingCategoryID)=0
				BEGIN
					UPDATE HcFundraisingCategory SET FundraisingCategoryName = @HcFundraisingCategoryName 
											  ,CategoryImagePath = @CategoryImagePath
											  ,DateModified = GETDATE()
											  ,ModifiedUserID = @ScanSeeAdminUserID										  
					WHERE HcFundraisingCategoryID = @HcFundraisingCategoryID  
			
					SET @DuplicateFlag=0
				END

				ELSE
				BEGIN
					SET @DuplicateFlag=1	
				END 									
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingCategoryUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
