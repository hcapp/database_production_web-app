USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiImageIconsDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebHcHubCitiImageIconsDisplay]
Purpose               : To Display the default image icons in the Retailer Anything Page Creation & Special Offer Creation.    
Example               : [usp_WebHcHubCitiImageIconsDisplay]    
    
History    
Version  Date			 Author				Change Description    
---------------------------------------------------------------     
1.0      22ndOct2013     Span		Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiImageIconsDisplay]    
(    
    @PageType varchar(200)
 --Output Variable--    
   , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY    
 
	 --To get Media Server Configuration.  
	  DECLARE @Config varchar(50)    
	  SELECT @Config=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Retailer Media Server Configuration'   
	    
	    
	    
	  --Concatenate the image icon name with the Retailer Media Server Configuration.   
      SELECT  QRRetailerCustomPageIconID CustomPageIconID
			, ImagePath = @Config + QRRetailerCustomPageIconImagePath
      FROM QRRetailerCustomPageIcons --WHERE PageType In (Select QRPageTypeID from QRRetailerCustomPageType where QRPageTypeName IN(@PageType,'ALL'))
      
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiImageIconsDisplay.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       
  END;    
       
 END CATCH;    
END;







GO
