USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminResetPassword]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminResetPassword
Purpose					: To enable the hub citi admin to reset the password after first login.
Example					: usp_WebHcAdminResetPassword

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminResetPassword]
(
   ----Input variable.
      @UserID int	  
	, @NewPassword varchar(100)
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN
			
			UPDATE Users SET Password = @NewPassword
						   , ResetPassword = 1
			WHERE UserID = @UserID  
			
			--Confirmation of Success
			SELECT @Status = 0
			
	    COMMIT TRAN  
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminResetPassword.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
			ROLLBACK TRAN
		END;
		 
	END CATCH;
END;







GO
