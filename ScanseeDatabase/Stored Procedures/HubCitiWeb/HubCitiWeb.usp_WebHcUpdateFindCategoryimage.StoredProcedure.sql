USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcUpdateFindCategoryimage]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcUpdateFindCategoryimage]
Purpose					: To update Find category image.
Example					: [usp_WebHcUpdateFindCategoryimage]

History
Version		   Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015	     SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcUpdateFindCategoryimage]
(   
	--Input variable
	  @HcHubCitiID int
	, @HcFindCategoryImageID int
	, @CategoryImage varchar(250)
	, @UserID int	

	--Output Variable 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To update find category image in HcFindCategoryImage table for given Hubciti.
			UPDATE HcFindCategoryImage
			SET CategoryImagePath = @CategoryImage
				,IsUpdated = 1
				,DateModified = GETDATE()
				,ModifiedUserID = @UserID
			WHERE HcFindCategoryImageID = @HcFindCategoryImageID
			AND HcHubCitiID = @HcHubCitiID

			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		  ------------------------------------------------
		
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcUpdateFindCategoryimage].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
