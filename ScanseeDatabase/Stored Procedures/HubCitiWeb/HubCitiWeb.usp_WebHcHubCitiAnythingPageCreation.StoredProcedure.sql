USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAnythingPageCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAnythingPageCreation
Purpose					: Creating Any thing Page.
Example					: usp_WebHcHubCitiAnythingPageCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAnythingPageCreation]
(
   ----Input variable.			
	  @AnythingPageName Varchar(2000)
	, @ShortDescription Varchar(2000)	
	, @LongDescription Varchar(2000)
	, @StartDate DateTime
	, @EndDate DateTime
	, @MediaPath Varchar(2000)
	, @ImageIconID INT
	, @ImageIconPath varchar(1000)
	, @HcAnyThingPageMediaTypeID INT
	, @URL VARCHAR(1000)
	, @UserID Int
	, @HcHubCitiID Int
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
				
				DECLARE @AnythingPageID INT
				DECLARE @MediaTypeID INT 
				
	               
				--Creating New Anything page
				INSERT INTO HcAnythingPage(AnythingPageName
										  ,ShortDescription
										  ,LongDescription
										  ,StartDate
										  ,EndDate
										  ,ImagePath
										  ,ImageIcon
										  ,DateCreated
										  ,CreatedUserID
										  ,URL
										  ,HcHubCitiID)				
				SELECT  @AnythingPageName 
					  , @ShortDescription 	
					  , @LongDescription
					  , @StartDate 
					  , @EndDate 
					  , @ImageIconPath
					  , @ImageIconID
					  , GETDATE()
					  , @UserID 
					  , @URL
					  , @HcHubCitiID 
				
				SELECT @AnythingPageID = SCOPE_IDENTITY()
				
				--When linked to existing web site do not insert into media.
				IF @URL IS NULL OR @URL = ''
				BEGIN
					INSERT INTO HcAnythingPageMedia(HcAnythingPageID
												   ,MediaTypeID
												   ,MediaPath
												   ,DateCreated
												   ,CreatedUserID)
									SELECT @AnythingPageID
										 , @HcAnyThingPageMediaTypeID
										 , @MediaPath
										 , GETDATE()
										 , @UserID
				END
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAnythingPageCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
