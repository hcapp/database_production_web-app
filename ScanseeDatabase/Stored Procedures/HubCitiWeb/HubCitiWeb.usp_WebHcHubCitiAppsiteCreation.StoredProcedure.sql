USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAppsiteCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAppsiteCreation
Purpose					: Creating Appsite.
Example					: usp_WebHcHubCitiAppsiteCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAppsiteCreation]
(
   ----Input variable.		  
	  @RetailLocationID Int
	, @UserID Int
	, @HcHubCitiID Int
	, @HcAppSiteName Varchar(100)
	  
	--Output Variable 	
	, @HcAppSiteID Int output
	, @DuplicateExist Int output	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
				--Creating New Appsite
				--0 - Success
				--1 - Retail lOc has an Appsite
				--2 - Name already exists
				
				IF NOT EXISTS(SELECT 1 FROM HcAppSite WHERE @HcHubCitiID=HcHubCitiID AND RetailLocationID=@RetailLocationID)
				BEGIN
					
					IF NOT EXISTS(SELECT 1 FROM HcAppSite WHERE @HcHubCitiID=HcHubCitiID AND HcAppSiteName=@HcAppSiteName )
					BEGIN
						INSERT INTO HcAppSite(HcHubCitiID
											 ,RetailLocationID
											 ,DateCreated
											 ,CreatedUserID
											 ,HcAppSiteName)				
						SELECT  @HcHubCitiID 
							  , @RetailLocationID 					 
							  , GETDATE()
							  , @UserID
							  , @HcAppSiteName
							  
						SET @HcAppSiteID=SCOPE_IDENTITY()	  
							  	
					    SET @DuplicateExist=0					
					END
					
					ELSE
					BEGIN
						SET @DuplicateExist=2
					END
					
				END
				
				ELSE
				BEGIN			
					SET @DuplicateExist=1
						--UPDATE HcAppSite SET RetailLocationID=@RetailLocationID
						--					,DateModified=GETDATE()
						--					,ModifiedUserID=@UserID
						--					,HcAppSiteName=@HcAppSiteName
						--WHERE @HcHubCitiID=HcHubCitiID 
						--AND RetailLocationID=@RetailLocationID
				END
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAppsiteCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
