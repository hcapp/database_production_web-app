USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsCategoryDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsCategoryDisplay
Purpose					: To display List fo Alerts Categories.
Example					: usp_WebHcHubCitiAlertsCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsCategoryDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @CategoryName Varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(200)
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		    DECLARE @UpperLimit int
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   
			

			--To display List fo Alerts Categories
			SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY HcAlertCategoryID) As Rownum
			      ,P.HcAlertCategoryID catId
			      ,HcAlertCategoryName catName
				  ,associateCate=CASE WHEN (SELECT COUNT(1) FROM HcAlertCategoryAssociation WHERE HcAlertCategoryID=P.HcAlertCategoryID) >0 THEN 1 ELSE 0 END				 		
			INTO #Alerts
			FROM HcAlertCategory P 
			WHERE HcHubCitiID=@HubCitiID AND ((@CategoryName IS NOT NULL AND HcAlertCategoryName like '%'+@CategoryName+'%') OR (@CategoryName IS NULL AND 1=1)) 
			
			
			--To capture max row number.  
			SELECT @MaxCnt = COUNT(RowNum) FROM #Alerts
			
			--If the lower limit is not sent then dont paginate.
			IF @LowerLimit IS NULL
			BEGIN
				SELECT @LowerLimit = 0
					 , @UpperLimit = @MaxCnt
			END
			
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
													
			
			SELECT Rownum
			      ,catId
				  ,catName
				  ,associateCate
			FROM #Alerts 
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY RowNum 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
