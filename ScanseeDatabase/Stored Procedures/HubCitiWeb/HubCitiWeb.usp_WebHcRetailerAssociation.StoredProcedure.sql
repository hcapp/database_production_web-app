USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcRetailerAssociation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcRetailerAssociation]
Purpose					: To Associate Retailers to Hubciti
Example					: [usp_WebHcRetailerAssociation]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			1/20/2014	    SPAN		    1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE  [HubCitiWeb].[usp_WebHcRetailerAssociation]
(
    --Input variable
      @HubCitiID int
    --, @RetailID int
    , @RetailLocationIDs varchar(1000)  --Comma separated RetailLocationID's

	--Output Variable 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION
			
					    
			--To Associate Retailers to given HubCiti
			INSERT INTO HcRetailerAssociation(HcHubCitiID
											  ,RetailID
											  ,RetailLocationID
											  ,DateCreated
											  ,Associated
											  )
			SELECT @HubCitiID
					,RL.RetailID
					,Param
					,GETDATE()
					,1
			FROM fn_SplitParam (@RetailLocationIDs,',') P
			INNER JOIN RetailLocation RL ON RL.RetailLocationID =P.Param
			LEFT JOIN HcRetailerAssociation L ON L.RetailLocationID =P.Param AND L.HcHubCitiID =@HubCitiID 
			WHERE HcRetailerAssociationID IS NULL AND Active = 1

			UPDATE HcRetailerAssociation SET Associated = 1
			FROM HcRetailerAssociation A
			INNER JOIN fn_SplitParam (@RetailLocationIDs,',') P ON A.RetailLocationID = P.Param AND A.HcHubCitiID = @HubCitiID

			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		    ------------------------------------------------

			--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION

	END TRY

BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcRetailerAssociation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
END CATCH;

END;









GO
