USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminSetupAboutUsPage]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcAdminSetupAboutUsPage
Purpose					: To set up the About Us Page.
Example					: usp_WebHcAdminSetupAboutUsPage

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminSetupAboutUsPage]
(
   --Input variable.
      @HCAdminUserID int
	, @HubCitiID int
	, @Image varchar(255)
	, @Content varchar(max)
	
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION		
			
			DECLARE @PageType int
			
			SELECT @PageType = HcPageTypeID
			FROM HcPageType
			WHERE PageType = 'About Us Page'
			
			--Update the About Us page if one already exists.
			IF EXISTS(SELECT 1 FROM HcPageConfiguration WHERE HcHubCitiID = @HubCitiID AND HcPageTypeID = @PageType)
			BEGIN
				print 'Update'
				UPDATE HcPageConfiguration SET Image = @Image
									 , Description = @Content
									 , DateModified = GETDATE()
									 , ModifiedUserID = @HCAdminUserID
				WHERE HcHubCitiID = @HubCitiID
				AND HcPageTypeID = @PageType
				
				
			END 
			
			--Created a new record if there is no About Us page setup yet.
			ELSE
			BEGIN
				print 'Insert'
				INSERT INTO HcPageConfiguration(HcHubCitiID
									  , Image
									  , Description
									  , DateCreated
									  , CreatedUserID
									  , HcPageTypeID)
							VALUES(@HubCitiID
								 , @Image
								 , @Content
								 , GETDATE()
								 , @HCAdminUserID
								 , @PageType)
			END	
			
	       --Confirmation of Success.
		   SELECT @Status = 0
	 COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcAdminSetupAboutUsPage.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
