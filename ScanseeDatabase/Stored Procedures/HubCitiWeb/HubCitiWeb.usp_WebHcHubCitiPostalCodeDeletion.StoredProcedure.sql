USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiPostalCodeDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : usp_WebHcHubCitiPostalCodeDeletion
Purpose                                  : To disply the list of the Hub Cities that are created by the SS admin.
Example                                  : usp_WebHcHubCitiPostalCodeDeletion

History
Version              Date                 Author               Change Description
--------------------------------------------------------------- 
1.0                  20/1/2014         Dhananjaya TR    1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiPostalCodeDeletion]
(
   ----Input variable.
         @HcHubcitiID Int   
       , @PostalCode Varchar(2000) --Comma Separated PostalCodes
       , @City Varchar(2000)
       , @State Varchar(2000)
       
       --Output Variable 
    , @Status int output   
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
                     
              
                  --Delete all postalcode association from HcLocationAssociation table.
                     DELETE FROM HcLocationAssociation 
                     WHERE HcHubCitiID =@HcHubcitiID
                     
                     SELECT rownum=identity(int,1,1)
                           ,param PostalCode
                     INTO #PostalCode
                     FROM fn_Splitparam(@PostalCode,',') 

                     SELECT rownum=identity(int,1,1)
                           ,param City
                     INTO #City
                     FROM fn_Splitparam(@City,',') 

                     SELECT rownum=identity(int,1,1)
                           ,param State
                     INTO #State
                     FROM fn_Splitparam(@State,',') 

                     SELECT P.PostalCode 
                           ,C.City 
                             ,S.State
                     INTO #List
                     FROM #PostalCode P
                     INNER JOIN #City C ON C.rownum=P.rownum
                     INNER JOIN #State S ON S.rownum=P.rownum 

                     DELETE FROM HcFilterRetailLocation
                     FROM HcFilterRetailLocation F
                     INNER JOIN HcFilter FI ON FI.HcFilterID =F.HcFilterID AND HcHubCitiID =@HcHubcitiID 
                     INNER JOIN RetailLocation RL ON RL.RetaillocationID=F.RetaillocationID 
                     INNER JOIN #List L ON L.postalcode=RL.postalcode AND RL.city=L.city AND RL.state=L.State 
                     
                     
                     DELETE FROM HcCityExperienceRetailLocation 
                     FROM HcCityExperienceRetailLocation CE   
                     INNER JOIN HcCityExperience C ON C.HcCityExperienceID =CE.HcCityExperienceID AND C.HcHubCitiID =@HcHubcitiID         
                     INNER JOIN RetailLocation RL ON RL.RetaillocationID=CE.RetailLocationID  
                     INNER JOIN #List L ON L.postalcode=RL.postalcode AND RL.city=L.city AND RL.state=L.State   
					 
					 DELETE FROM HcRegionAppMenuItemCityAssociation 
					 FROM HcRegionAppMenuItemCityAssociation RMC 
					 INNER JOIN HcCity C ON RMC.HcCityID = C.HcCityID
					 INNER JOIN #List L ON C.CityName = L.City  
					 WHERE RMC.HcHubcitiID = @HcHubcitiID                                  
                     
                     --Confirmation of Success
                     SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiPostalCodeDeletion.'            
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;










GO
