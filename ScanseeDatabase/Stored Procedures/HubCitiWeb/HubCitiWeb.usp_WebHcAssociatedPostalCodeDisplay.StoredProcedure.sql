USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAssociatedPostalCodeDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAssociatedPostalCodeDisplay]
Purpose					: To disply list of associated Postalcodes for given HubCiti.
Example					: [usp_WebHcAssociatedPostalCodeDisplay]

History
Version		Date			Author		 Change Description
--------------------------------------------------------------- 
1.0			1/21/2014	    SPAN		       1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAssociatedPostalCodeDisplay]
(
    
    --Input variable
      @HubCitiID int
	, @State varchar(100) 
	, @City varchar(100)
	
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT DISTINCT PostalCode postalCode
			FROM HcLocationAssociation
			WHERE HcHubCitiID = @HubCitiID
			AND [State] = @State
			AND City = @City
			ORDER BY PostalCode
			
			--Confirmation of Success
			SELECT @Status = 0
			
	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAssociatedPostalCodeDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
