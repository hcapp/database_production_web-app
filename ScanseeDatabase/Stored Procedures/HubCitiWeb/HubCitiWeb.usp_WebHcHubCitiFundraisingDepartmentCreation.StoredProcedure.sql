USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingDepartmentCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingDepartmentCreation
Purpose					: Creating Departments for fundraising.
Example					: usp_WebHcHubCitiFundraisingDepartmentCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Aug 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingDepartmentCreation]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int	
	, @HcDepartmentName Varchar(1000)	
	  
	--Output Variable 	
	, @HcFundraisingDepartmentID Int output	
	, @DuplicateFlag Bit output
    , @Status Int output        
	, @ErrorNumber Int output
	, @ErrorMessage Varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
											
			IF EXISTS(SELECT 1 FROM HcFundraisingDepartments WHERE FundraisingDepartmentName=@HcDepartmentName AND HcHubCitiID = @HcHubCitiID)
			BEGIN
				SET @DuplicateFlag = 1
			END
			ELSE
			BEGIN			
				INSERT INTO HcFundraisingDepartments(FundraisingDepartmentName
													 ,HcHubCitiID
													 ,CreatedUserID												
													 ,DateCreated)
												
											SELECT @HcDepartmentName
												 , @HcHubCitiID
												 , @UserID 			      
												 , GETDATE() 

				SELECT @HcFundraisingDepartmentID = SCOPE_IDENTITY()
				SET @DuplicateFlag = 0
		     END
																		
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingDepartmentCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
