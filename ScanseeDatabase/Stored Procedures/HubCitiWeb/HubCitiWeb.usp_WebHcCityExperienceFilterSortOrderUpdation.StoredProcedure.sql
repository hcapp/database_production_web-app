USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcCityExperienceFilterSortOrderUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebHcCityExperienceFilterSortOrderUpdation]
Purpose                  : To sort city experience filters accordingly.
Example                  : [usp_WebHcCityExperienceFilterSortOrderUpdation]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               26th June 2014        Mohith H R   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcCityExperienceFilterSortOrderUpdation]
(

      --Input Input Parameter(s)--  
      
        @HcHubCitiID INT
      , @HcFilterID Varchar(MAX) --Comma separated FilterId's
	  , @HcCityExperienceID INT
      , @SortOrder VARCHAR (255)
	  , @UserID INT
      
      --Output Variable--
	  , @Status INT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN
  
      BEGIN TRY
      
      BEGIN TRANSACTION
      
             CREATE TABLE #Temp(Rownum INT IDENTITY(1,1),SortID varchar(100))
             INSERT INTO #Temp (SortID)
			 SELECT R.Param 
			 FROM dbo.fn_SplitParam(@SortOrder, ',') R

			 --Store Sort order to temp table
			 CREATE TABLE #Temp1(Rownum INT IDENTITY(1,1),FilterID varchar(100))
			 INSERT INTO #Temp1(FilterID)
			 SELECT R.Param 
			 FROM dbo.fn_SplitParam(@HcFilterID,',') R
             
             --Store  AnyThingPageIds to temp table
			 CREATE TABLE #Temp2(Rownum INT IDENTITY(1,1),SortID VARCHAR(255),FilterID VARCHAR(100))
             INSERT INTO #Temp2 (SortID,FilterID )
			 SELECT	T1.SortID 
				   ,T2.FilterID  
			 FROM #Temp T1 
			 INNER JOIN #Temp1 T2 ON T1.Rownum =T2.Rownum 

		   
		    --Update Anythingpage based based on sorting.
		    UPDATE HcFilter SET SortOrder = T.SortID 
						       ,ModifiedUserID = @UserID
		    FROM HcFilter F
	        INNER JOIN #temp2 T ON T.FilterID =F.HcFilterID
			WHERE HcHubCitiID = @HcHubCitiID AND HcCityExperienceID=@HcCityExperienceID
	        
	   --Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION      
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebHcCityExperienceFilterSortOrderUpdation.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  --Confirmation of Failure.
                  SELECT @Status =1
                  ROLLBACK TRANSACTION;              
                  
            END;
            
      END CATCH;
END;





GO
