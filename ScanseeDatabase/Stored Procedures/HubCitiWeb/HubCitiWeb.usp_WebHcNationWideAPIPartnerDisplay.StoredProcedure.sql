USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNationWideAPIPartnerDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNationWideAPIPartnerDisplay]
Purpose					: To display API Partners for a selected Hubciti.
Example					: [usp_WebHcNationWideAPIPartnerDisplay]

History
Version		Date			Author	              Change Description
--------------------------------------------------------------- 
1.0			12/9/2015		Bindu T A			  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNationWideAPIPartnerDisplay]
(
		--Input variables
		--@HubCitiID Int,

		--Output variables
		@Status int output,
		@ErrorNumber int output,
		@ErrorMessage varchar(1000) output 
)

AS
BEGIN	
	BEGIN TRY
	
			
			SELECT DISTINCT APIPartnerID
							,APIPartnerName 
			FROM APIPartner
			WHERE APIPartnerName IN ('Plum District','CrowdSavings','Gilt City','CBS Local','Voice Daily Deals',
			'Eversave','Half Off Depot','Mamasource','Restaurant.com','Living Social')

			--SELECT DISTINCT A.APIPartnerID
			--				,A.APIPartnerName 
			--FROM HcHubCiti HC  
			--INNER JOIN HCLocationAssociation HLA ON HC.HcHubCitiID = HLA.HcHubCitiID
			--INNER JOIN ProductHotDealLocation PHL ON PHL.PostalCode = HLA.PostalCode AND PHL.State = HLA.State AND PHL.City = HLA.City
			--INNER JOIN ProductHotDeal PD ON PHL.ProductHotDealID = PD.ProductHotDealID
			--INNER JOIN APIPartner A  ON PD.APIPartnerID = A.APIPartnerID
			--WHERE HC.Active = 1 AND PD.HotDealProvider IS NOT NULL AND HC.HcHubCitiID = @HubCitiID
			--AND ISNULL(PD.HotDealExpirationDate,PD.HotDealEndDate) >= GETDATE()

			--Confirmation of Success
			SELECT @Status = 0

	END TRY 

	BEGIN CATCH
	  
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNationWideAPIPartnerDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;

END


GO
