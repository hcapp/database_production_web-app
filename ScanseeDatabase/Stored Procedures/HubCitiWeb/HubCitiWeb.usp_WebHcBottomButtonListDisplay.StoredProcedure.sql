USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonListDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBottomButtonListDisplay]
Purpose					: To Display list of Modules for associating BottomButtons.
Example					: [usp_WebHcBottomButtonListDisplay]

History
Version		  Date			Author	   Change Description
--------------------------------------------------------------- 
1.0			04 jul 2014	     SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonListDisplay]
(

	--Input Variable 
	  @HcHubCitiID int
    , @UserID int 
	    
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT FBB.HcHubCitiID 
				 , FBB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,HM.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , imagePath=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , imagePathOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(HM.BottomButtonLinkTypeName = 'Filters', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HcHubCitiID), BottomButtonLinkID) 
				 , HM.BottomButtonLinkTypeName AS btnLinkTypeName	
				 , FBB.HcFunctionalityID functionalityId
				 , LT.LinkTypeDisplayName functionalityType 
				 --, IsMandatory=IIF(LinkTypeDisplayName='Deals' AND BottomButtonLinkTypeDisplayName='Preference',1,0)
				 , IsMandatory=CASE WHEN LinkTypeDisplayName='Deals' AND BottomButtonLinkTypeDisplayName='Preference' THEN 1
									WHEN LinkTypeDisplayName IN ('Experience','Find','Filters','Nearby') AND BottomButtonLinkTypeDisplayName = 'Map' THEN 0
									WHEN LinkTypeDisplayName IN ('Event','Fundraisers') AND BottomButtonLinkTypeDisplayName='SortFilter' THEN 1
									WHEN LinkTypeDisplayName IN ('Experience','Find','Filters','Nearby') AND BottomButtonLinkTypeDisplayName='SortFilter' THEN 0  
									WHEN LinkTypeDisplayName IN ('Band','Band Events') AND BottomButtonLinkTypeDisplayName='SortFilter' THEN 1
									ELSE 0 END
			--INTO #temp	 			
			FROM HcFunctionalityBottomButton FBB			
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType HM ON HM.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			WHERE FBB.HcHubCitiID = @HcHubCitiID AND LT.ActiveBottomButton =1
			ORDER BY LT.LinkTypeDisplayName, btnLinkTypeID 

			
			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcBottomButtonListDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
