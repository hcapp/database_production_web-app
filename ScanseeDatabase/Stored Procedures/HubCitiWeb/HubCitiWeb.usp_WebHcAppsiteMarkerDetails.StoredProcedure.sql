USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAppsiteMarkerDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteMarkerDetails]
Purpose					: To edit Appsite logistic map marker details.
Example					: [usp_WebHcAppsiteMarkerDetails] 

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAppsiteMarkerDetails]
(
	
	--Input Variable
	  @HubCitiID int
	, @HcAppsiteLogisticID int
    
    --Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
	, @AppsiteMapImagePath varchar(1000) OUTPUT
    , @AppsiteLogisticLatitude Float OUTPUT
    , @AppsiteLogisticLongitude Float OUTPUT
    , @AppsiteLogisticTopLeftLat Float OUTPUT
    , @AppsiteLogisticTopLeftLong Float OUTPUT
    , @AppsiteLogisticBottomRightLat Float OUTPUT
    , @AppsiteLogisticBottomRightLong Float OUTPUT
	, @InitialZoomLevel INT OUTPUT
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @AppsiteConfig Varchar(500) 

			SELECT @AppsiteConfig =ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'

				 
		DECLARE @HubCitiConfig VARCHAR(1000)
		DECLARE @RetailerConfig varchar(100)
		
		SELECT @HubCitiConfig = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration' 

		SELECT @RetailerConfig = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Web Retailer Media Server Configuration' 

			SELECT @AppsiteLogisticLatitude = Latitude
				  ,@AppsiteLogisticLongitude = Longitude
				  ,@AppsiteLogisticTopLeftLat = TopLeftLatitude
				  ,@AppsiteLogisticTopLeftLong = TopLeftLongitude
				  ,@AppsiteLogisticBottomRightLat = BottomRightLatitude
				  ,@AppsiteLogisticBottomRightLong = BottomRightLongitude
				  ,@InitialZoomLevel = InitialZoomLevel 
			FROM HcAppsiteOverLayCoordinates
			WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID 

			SET @AppsiteMapImagePath = @AppsiteConfig+CAST(@HubCitiID AS VARCHAR)+ '/logistics/'+CAST(@HcAppsiteLogisticID AS VARCHAR)+'/'

			--To display details of Appsite Logistic map marker points.
			 SELECT L.HcAppsiteLogisticID
					, MarkerName
					, MarkerImage AS MarkerImageName
					--SSQR imagepath--
					, markerImage = @AppsiteConfig + CAST(@HubCitiID AS VARCHAR)+ '/logistics/'+CAST(@HcAppsiteLogisticID AS VARCHAR)+'/' + MarkerImage
					--Hubciti imagepath--
					, markerImagePath = @AppsiteConfig + CAST(@HubCitiID AS VARCHAR)+ '/logistics/'+CAST(@HcAppsiteLogisticID AS VARCHAR)+'/' + MarkerImage
					, Latitude AS markerLatitude
					, Longitude AS markerLongitude
					, M.HcAnythingPageID anythingPageID
					, CASE WHEN URL IS NOT NULL THEN URL 				
					       ELSE (SELECT @HubCitiConfig + CAST(@HubCitiID AS VARCHAR(10))+ '/' + MediaPath FROM HcAnythingPageMedia WHERE HcAnythingPageMediaID = PM.HcAnythingPageMediaID) END anythingPageURL
			FROM HcAppsiteLogistic L
			INNER JOIN HcAppsiteMarker M ON L.HcAppsiteLogisticID = M.HcAppsiteLogisticID
			LEFT JOIN HcAnythingPage A ON A.HcAnythingPageID = M.HcAnythingPageID
			LEFT JOIN HcAnythingPageMedia PM ON PM .HcAnythingPageID = A.HcAnythingPageID
			LEFT JOIN HcAnyThingPageMediaType MT ON MT.HcAnyThingPageMediaTypeID = PM.HcAnythingPageMediaID
			WHERE L.HcAppsiteLogisticID = @HcAppsiteLogisticID AND M.HcAppsiteLogisticID = @HcAppsiteLogisticID
			AND L.HcHubCitiID = @HubCitiID
			ORDER BY MarkerName									
			
			--Confirmation of Success
			SELECT @Status = 0

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteMarkerDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
