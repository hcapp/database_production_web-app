USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcNationWideHubcitiListDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcNationWideHubcitiListDisplay]
Purpose					: To disply list of Nation wide hubciti list Display.
Example					: [usp_WebHcNationWideHubcitiListDisplay]

History
Version		Date			Author		           Change Description
--------------------------------------------------------------- 
1.0			3/2/2014	    Dhananjaya TR		   1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcNationWideHubcitiListDisplay]
(
    
    --Input variable
      @UserID int
	, @SearchKey Varchar(500)
	, @LowerLimit int  
    , @ScreenName varchar(50)
	  
	--Output Variable 
	, @MaxCnt int  output
    , @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
			DECLARE @APIPartnerIDs VARCHAR(1000)

			SELECT @UpperLimit = @LowerLimit + ScreenContent	--NO OF RECORDS TO DISPLAY BETWEEN UPPER NAD LOWER LIMIT
            FROM AppConfiguration   
            WHERE ScreenName = @ScreenName 
            AND ConfigurationType = 'Pagination'
            AND Active = 1

			--Inserting data into HcHubcitiNationWideDeals
			INSERT INTO HcHubcitiNationWideDeals(HcHubCItiID					--	SELECT * FROM HcHubcitiNationWideDeals
												,NationWideDealsDisplayFlag
												,DateCreated
												,CreatedUserID
												,APIPartnerID)
			SELECT DISTINCT H.HcHubCitiID  
				  ,0
				  ,GETDATE()
				  ,H.CreatedUserID
				  ,NULL
			FROM HcHubCiti H
			LEFT JOIN HcHubcitiNationWideDeals N ON N.HcHubCItiID =H.HcHubCitiID 
			WHERE N.HcHubCItiID IS NULL

	
			--Inserting data into HcDataExportHubCitiList
			INSERT INTO HcDataExportHubCitiList (HcHubCItiID					--SELECT * FROM HcDataExportHubCitiList
												,HubCitiDataExportFlag
												,DateCreated
												,CreatedUserID)
			SELECT DISTINCT H.HcHubCitiID  
				  ,0
				  ,GETDATE()
				  ,H.CreatedUserID 
			FROM HcHubCiti H
			LEFT JOIN HcDataExportHubCitiList D ON D.HcHubCitiID =H.HcHubCitiID 
			WHERE D.HcHubCitiID IS NULL 

			--Collecting Nationwidedeal Hubcities
			
			SELECT DISTINCT D.HcHubCitiID
						   ,D.NationWideDealsDisplayFlag
						   ,APIPartnerID = STUFF(( SELECT ',' + CAST(ND.APIPartnerID AS VARCHAR(1000))
										FROM HcHubcitiNationWideDeals ND
										WHERE  ND.HcHubCitiID = D.HcHubCitiID 
										FOR XML PATH ('')
										), 1,1,'')
			INTO #IsNationWide			   			   		          
			FROM HcHubcitiNationWideDeals D
			INNER JOIN HcHubCiti H ON D.HcHubCItiID = H.HcHubCitiID AND H.Active = 1

			--Collecting Dataexport Hubcities 
			SELECT DISTINCT D.HcHubCitiID  
						   ,D.HubCitiDataExportFlag    
			INTO #IsExport			         
			FROM HcDataExportHubCitiList D
			INNER JOIN HcHubCiti H ON D.HcHubCItiID = H.HcHubCitiID AND H.Active = 1
			
			--Inserting the matching data into a temporary table #Result
			SELECT IDENTITY(INT, 1,1) AS Rownum
			      ,C.HcHubCitiID AS hubCitiID 
			      ,C.HubCitiName AS hubCitiName
				  ,NationWideDealsDisplayFlag AS isNationwide
				  ,HubCitiDataExportFlag AS isDataExport
				  ,A.APIPartnerID
			INTO #Result
			FROM #IsNationWide A
			INNER JOIN #IsExport B ON A.HcHubCItiID = B.HcHubCitiID 
			INNER JOIN HcHubCiti C ON C.HcHubCitiID = B.HcHubCitiID
			AND (@SearchKey IS NULL OR (@SearchKey IS NOT NULL AND hubCitiName LIKE '%'+@SearchKey +'%'))
			ORDER BY hubCitiName

			--Displaying the result based on pagination
			SELECT Rownum
			      ,hubCitiID 
				  ,hubCitiName
				  ,isNationwide
				  ,isDataExport
				  ,APIPartnerID AS apiPartners
			FROM #Result
			WHERE (Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit) 
			--AND (@SearchKey IS NULL OR (@SearchKey IS NOT NULL AND hubCitiName LIKE '%'+@SearchKey +'%'))

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Result

			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			--Confirmation of Success
			SELECT @Status = 0		      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcNationWideHubcitiListDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
