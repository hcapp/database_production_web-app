USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiAlertsDeletion]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiAlertsDeletion
Purpose					: Deleting Selected  Alerts
Example					: usp_WebHcHubCitiAlertsDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20/11/2013	    Span	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiAlertsDeletion]
(   
    --Input variable.	  
	 @HubCitiID Int   
    ,@HcAlertID Int
	
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Deleting Selected  Alerts.
			DELETE FROM HcAlertCategoryAssociation 
			WHERE HcAlertID=@HcAlertID
			
			DELETE FROM HcAlerts    
			WHERE HcAlertID =@HcAlertID  
												
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
