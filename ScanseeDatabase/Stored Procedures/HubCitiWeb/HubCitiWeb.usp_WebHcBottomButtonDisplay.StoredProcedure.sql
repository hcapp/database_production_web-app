USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcBottomButtonDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBottomButtonDisplay]
Purpose					: To display list of Bottom Button For Hubciti.
Example					: [usp_WebHcBottomButtonDisplay]


History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21/10/2013	    Mohith H R	          1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcBottomButtonDisplay]
(
    --Input variable.
      @HcHubCitiID Int
    , @HcMenuID int
      
	--Output Variable 
	, @HcBottomButtonTypeID int output
	, @ButtonType varchar(100) output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN


	BEGIN TRY
	 BEGIN TRANSACTION
	        
	       
	        DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'		
			
			SELECT @HcBottomButtonTypeID = HB.HcBottomButtonTypeId
				  ,@ButtonType = HB.ButtonType
			FROM HcMenuCustomUI HC
			INNER JOIN HcBottomButtonTypes HB ON HC.HcBottomButtonTypeID = HB.HcBottomButtonTypeId
			WHERE HubCitiId = @HcHubCitiID	

			SELECT DISTINCT Rownum = Identity(INT,1,1)
					            , MI.HcBottomButtonID
                                , FI.BusinessCategoryID                                                           
                                , HcBusinessSubCategoryID = REPLACE(STUFF((SELECT ', ' + CAST(F.HcBusinessSubCategoryID AS VARCHAR(100))
                                                                                                    FROM HcBottomButtonFindRetailerBusinessCategories F
                                                                                                    WHERE F.HcBottomButonID = MI.HcBottomButtonID AND F.BusinessCategoryID =FI.BusinessCategoryID 
                                                                                                    ORDER BY F.HcBottomButtonFindRetailerBusinessCategoryID
                                                                                                    FOR XML PATH(''))
                                                                                                    , 1, 2, ''), ' ', '')
                     
                     
			INTO #Temp1
			FROM HcBottomButton MI              
			INNER JOIN HcBottomButtonFindRetailerBusinessCategories FI ON FI.HcBottomButonID = MI.HcBottomButtonID			
			WHERE MI.HCHubcitiID=@HcHubCitiID 

			IF @HcMenuID IS NULL 
			BEGIN
				SELECT DISTINCT BB.HcBottomButtonID bottomBtnId
							  , imagePath = CASE WHEN BottomButtonImage_On IS NULL THEN  @Config+'/'+BI.HcBottomButtonImageIcon  									  
												   ELSE @Config + CONVERT(VARCHAR(100),H.HcHubCitiID)+'/'+BottomButtonImage_On END	
							  , imagePathoff = CASE WHEN BottomButtonImage_Off IS NULL THEN  @Config+'/'+BI.HcBottomButtonImageIcon_Off  									  
												   ELSE @Config + CONVERT(VARCHAR(100),H.HcHubCitiID)+'/'+BottomButtonImage_Off END					   
							  , BB.BottomButtonLinkTypeID menuFucntionality
							  , btnLinkId = CASE WHEN B.BottomButtonLinkTypeName = 'Find' THEN REPLACE((STUFF((SELECT  ', ' + CAST(A.BusinessCategoryID AS VARCHAR(10))
																			FROM #Temp1 A																			 																																				
																			WHERE A.HcBottomButtonID = BB.HcBottomButtonID
																			--ORDER BY HcBottomButtonFindRetailerBusinessCategoryID																			
																			FOR XML PATH('')), 1, 2, '')), ' ', '') 
																			ELSE CASE WHEN BottomButtonLinkTypeName = 'Events' 
																				THEN REPLACE((STUFF((SELECT ', ' + CAST(HcEventCategoryID AS VARCHAR(10))
																				FROM HcBottomButtonEventCategoryAssociation A																																						 																																				
																				WHERE A.HcBottomButtonID = BB.HcBottomButtonID		
																				ORDER BY HcBottomButtonEventCategoryAssociationID																		
																				FOR XML PATH('')), 1, 2, '')), ' ', '') 
																			ELSE CASE WHEN BottomButtonLinkTypeName = 'Fundraisers' 
																				THEN REPLACE((STUFF((SELECT ', ' + CAST(HcFundraisingCategoryID AS VARCHAR(10))
																				FROM HcFundraisingBottomButtonAssociation A																																						 																																				
																				WHERE A.HcBottomButtonID = BB.HcBottomButtonID		
																				ORDER BY HcFundraisingBottomButtonAssociationID																		
																				FOR XML PATH('')), 1, 2, '')), ' ', '')

																				--Interest changes (12th Jan 2016)
																		   ELSE CASE WHEN BottomButtonLinkTypeName = 'Filters' 
																				THEN REPLACE((STUFF((SELECT ', ' + CAST(HcfilterID AS VARCHAR(10))
																				FROM HcBottomButtonFilterAssociation A																																						 																																				
																				WHERE A.HcBottomButtonID = BB.HcBottomButtonID		
																				ORDER BY HcBottomButtonFilterAssociationID																		
																				FOR XML PATH('')), 1, 2, '')), ' ', '') 
																			ELSE CAST(BB.BottomButtonLinkID AS VARCHAR(100)) 
																			END
																			END
																			END
																			END
							  ,	B.BottomButtonLinkTypeName funType  
							  , H.ItunesURL iTunesLnk
							  , H.GooglePlayURL playStoreLnk
							  , BB.HcBottomButtonImageIconID iconId
							  , BB.BottomButtonImage_On imageName
							  , BB.BottomButtonImage_Off imageNameOff
							  , 0 Position
							  , (REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.HcBusinessSubCategoryID IS NULL,'ASD',F.HcBusinessSubCategoryID) AS VARCHAR(MAX))
                                                                           FROM #Temp1 F
                                                                           WHERE F.HcBottomButtonID = BB.HcBottomButtonID
                                                                           ORDER BY Rownum
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', ''),'ASD','NULL')) findSubCatIds
							 --, (REPLACE(REPLACE(STUFF((SELECT '!~~! ' + CAST(IIF(F.HcBusinessSubCategoryID IS NULL,'ASD',F.HcBusinessSubCategoryID) AS NVARCHAR(100))
        --                                                                   FROM #Temp1 F
        --                                                                   WHERE F.HcBottomButtonID = BB.HcBottomButtonID
        --                                                                   ORDER BY Rownum
        --                                                                   FOR XML PATH(''))
        --                                                                   , 1, 2, ''), ' ', ''),'ASD','NULL'))svdSubCate

						
						      , (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.HcBusinessSubCategoryID IS NULL,'asd',F.HcBusinessSubCategoryID) AS VARCHAR(MAX))
                                                                           FROM #Temp1 F
                                                                           WHERE F.HcBottomButtonID = BB.HcBottomButtonID
                                                                           ORDER BY Rownum 
                                                                           FOR XML PATH(''))
                                                                           , 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) svdSubCate


				FROM HcBottomButton BB
				INNER JOIN HcHubCiti H ON H.HcHubCitiID = BB.HcHubCitiID
				--LEFT JOIN HcBottomButtonFindRetailerBusinessCategories F ON BB.HcBottomButtonID = F.HcBottomButonID
				LEFT JOIN HCBottomButtonLinkType B ON BB.BottomButtonLinkTypeID = B.HcBottomButtonLinkTypeID
				LEFT JOIN HcMenuBottomButton HBB ON BB.HcBottomButtonID = HBB.HcBottomButtonID 
				LEFT JOIN HcBottomButtonImageIcons BI ON BB.HcBottomButtonImageIconID = BI.HcBottomButtonImageIconID 
				WHERE H.HcHubCitiID = @HcHubCitiID						
				ORDER BY Position ASC
			
			END	
			
			ELSE
			BEGIN
				SELECT DISTINCT BB.HcBottomButtonID bottomBtnId
							  , imagePath = CASE WHEN BottomButtonImage_On IS NULL THEN  @Config+'/'+BI.HcBottomButtonImageIcon  									  
												   ELSE @Config + CONVERT(VARCHAR(100),H.HcHubCitiID)+'/'+BottomButtonImage_On END	
							  , imagePathoff = CASE WHEN BottomButtonImage_Off IS NULL THEN  @Config+'/'+BI.HcBottomButtonImageIcon_Off  									  
												   ELSE @Config + CONVERT(VARCHAR(100),H.HcHubCitiID)+'/'+BottomButtonImage_Off END						   
							  , BottomButtonLinkTypeID menuFucntionality
							  , btnLinkId = CASE WHEN B.BottomButtonLinkTypeName = 'Find' THEN REPLACE((STUFF((SELECT ', ' + CAST(A.BusinessCategoryID AS VARCHAR(10))
																			FROM HcBottomButtonFindRetailerBusinessCategories A																			 																																				
																			WHERE A.HcBottomButonID = BB.HcBottomButtonID	
																			ORDER BY HcBottomButtonFindRetailerBusinessCategoryID																		
																			FOR XML PATH('')), 1, 2, '')), ' ', '') ELSE CAST(BB.BottomButtonLinkID AS VARCHAR(100)) END
						      ,	B.BottomButtonLinkTypeName funType  
							  , H.ItunesURL
							  , H.GooglePlayURL
							  , BB.HcBottomButtonImageIconID iconId
							  , BB.BottomButtonImage_On imageName
							  , BB.BottomButtonImage_Off imageNameOff
							  , HBB.Position
				FROM HcBottomButton BB
				INNER JOIN HcHubCiti H ON H.HcHubCitiID = BB.HcHubCitiID
				--LEFT JOIN HcBottomButtonFindRetailerBusinessCategories F ON BB.HcBottomButtonID = F.HcBottomButonID
				LEFT JOIN HCBottomButtonLinkType B ON BB.BottomButtonLinkTypeID = B.HcBottomButtonLinkTypeID
				LEFT JOIN HcMenuBottomButton HBB ON BB.HcBottomButtonID = HBB.HcBottomButtonID 
				LEFT JOIN HcBottomButtonImageIcons BI ON BB.HcBottomButtonImageIconID = BI.HcBottomButtonImageIconID 
				WHERE H.HcHubCitiID = @HcHubCitiID AND HBB.HcMenuID = @HcMenuID						
				ORDER BY Position ASC
			END
								
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcBottomButtonDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
