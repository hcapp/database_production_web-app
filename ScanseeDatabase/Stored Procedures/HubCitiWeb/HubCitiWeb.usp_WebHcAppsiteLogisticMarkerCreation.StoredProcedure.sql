USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAppsiteLogisticMarkerCreation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAppsiteLogisticMarkerCreation]
Purpose					: To create Appsite logistic map marker points.
Example					: [usp_WebHcAppsiteLogisticMarkerCreation] 

History
Version		    Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			 8/24/2015	      SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAppsiteLogisticMarkerCreation]
(
	
	--Input Variable
	  @UserID int
	, @HubCitiID int
	, @HcAppsiteLogisticID int
	, @HcAnythingPageID varchar(max)
	, @MarkerName varchar(max)
	, @MarkerImage varchar(max)
	, @Latitude varchar(max)
	, @Longitude varchar(max)
    
    --Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN
				
				DELETE FROM HcAppsiteMarker 
				WHERE HcAppsiteLogisticID = @HcAppsiteLogisticID

				IF (@MarkerName IS NOT NULL AND @MarkerImage IS NOT NULL AND @Latitude IS NOT NULL AND @Longitude IS NOT NULL)
				BEGIN

				SELECT Rownum = IDENTITY(int,1,1)
					, Param
				INTO #MarkerName
				FROM dbo.fn_SplitParamMultiDelimiter(@MarkerName,'!~~!') 
				
				SELECT Rownum = IDENTITY(int,1,1)
					, Param
				INTO #MarkerImage
				FROM dbo.fn_SplitParamMultiDelimiter(@MarkerImage,'!~~!') 
				
				SELECT Rownum = IDENTITY(int,1,1)
					, Param
				INTO #Latitude
				FROM dbo.fn_SplitParamMultiDelimiter(@Latitude,'!~~!') 
				
				SELECT Rownum = IDENTITY(int,1,1)
					, Param
				INTO #Longitude
				FROM dbo.fn_SplitParamMultiDelimiter(@Longitude,'!~~!') 
				

				--Anything page association to Marker details
				SELECT Rownum = IDENTITY(int,1,1)
					, Param
				INTO #AnythingPage
				FROM dbo.fn_SplitParamMultiDelimiter(@HcAnythingPageID,'!~~!')
				

				
				----Create Marker points for given Appsite Logistic ID
				INSERT INTO HcAppsiteMarker(HcAppsiteLogisticID
											, MarkerName
											, MarkerImage
											, Latitude
											, Longitude
											, DateCreated
											, CreatedUserID
											, HcAnythingPageID
											)
									SELECT @HcAppsiteLogisticID
											, N.Param
											, I.Param
											, Lat.Param
											, Long.Param
											, GETDATE()
											, @UserID
											, CASE WHEN AP.Param = 'NULL' THEN NULL ELSE AP.Param END
									FROM #MarkerName N
									INNER JOIN #MarkerImage I ON N.Rownum = I.Rownum
									INNER JOIN #Latitude Lat ON N.Rownum = Lat.Rownum
									INNER JOIN #Longitude Long ON N.Rownum = Long.Rownum
									INNER JOIN #AnythingPage AP ON N.Rownum = AP.Rownum

			END

			--Confirmation of Success
			SELECT @Status = 0
			COMMIT TRAN

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebHcAppsiteLogisticMarkerCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRAN;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
