USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiBandAndBandEventDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcHubCitiBandAndBandEventDetails]
Purpose					: Band Event SSQR - Share Changes
Example					: [usp_WebHcHubCitiBandAndBandEventDetails]

History
Version		Date				Author			Change Description
------------------------------------------------------------------------------ 
1.0			18th July 2016    Sagar Byali		Band Event SSQR - Share Changes
-------------------------------------------------------------------------------
*/

create PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiBandAndBandEventDetails]
(   
    --Input variable.	  
	  @HcBandEventID Int
    , @HubCitiID int
  
    , @Status int output	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
	, @IOSAppID varchar(1000) output
	, @AndroidAppID varchar(1000) output
)
AS
BEGIN

	BEGIN TRY


		    DECLARE @Configg VARCHAR(500)
				   ,@RetailConfigg varchar(1000)
				   ,@SSQRURL varchar(1000) = 'http://10.122.61.145:8080/'
				   ,@Config VARCHAR(500) = 'http://10.122.61.146:8080/Images/band/'
				   	 
			SELECT @Configg = ScreenContent
            FROM AppConfiguration
            WHERE ConfigurationType = 'Hubciti Media Server Configuration'
					 
            SELECT @RetailConfigg = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'Web Band Media Server Configuration'   

			SELECT @IOSAppID = IOSAppID
				  ,@AndroidAppID = AndroidAppID
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID


				---------To display Band Event Details-------------------
				SELECT DISTINCT title = isnull(EventLocationTitle,BandName) 
							   ,bandEvtTitle = HcBandEventName  
							   ,shortDesc = ShortDescription 
							   ,longDesc = LongDescription  								
							   ,bndEvntImg = IIF(E.BandID IS NULL,(@Configg + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(ImagePath,EventListingImagePath)),@RetailConfigg+CAST(E.BandID AS VARCHAR)+'/'+ ISNULL(ImagePath,EventListingImagePath)) 
							   ,startDate =convert(varchar,startdate,6)
							   ,startTime = convert(varchar,cast(startdate as time),100)
							   ,endDate =convert(varchar,startdate,6)
							   ,endTime = convert(varchar,cast(startdate as time),100)
							   ,bandID = e.BandID 
							   ,bandName = BandName 
							   ,bandVenueFlag =  E.BussinessEvent 
							   ,bandAddress = case when E.BussinessEvent = 0 then el.Address + ', ' +el.City + ', '+ el.State + ', '+el.PostalCode end 
							   ,bandSumURL ='http://10.122.61.145:8080/SSQR/qr/3002.htm?bandId='+ cast(BB.BandID as varchar) +'&hubcitiId='+ cast(@HubCitiID as varchar)					 
							   ,retSumURL = @ssqrURL + 'SSQR/qr/2000.htm?retId='+ cast(RetailID as varchar) +'&hubcitiId='+ cast(@HubCitiID as varchar)+'&retlocId='+cast(RetailLocationID as varchar)
				FROM HcBandEvents E
				LEFT JOIN HcBandEventLocation EL ON E.HcBandEventID =EL.HcBandEventID 
				LEFT JOIN HcBandEventInterval EI ON EL.HcbandEventID = EI.HcbandEventID
				LEFT JOIN Band BB ON BB.BandID = e.BandID  
				LEFT JOIN HcBandRetailerEventsAssociation BBB ON BBB.HcBandEventID = E.HcBandEventID AND BBB.HcBandEventID = @HcbandEventID
				WHERE E.HcbandEventID = @HcbandEventID   

			
				--Confirmation of Success
				SELECT @Status = 0	      
				      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiRetailerEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
