USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraisingDepartmentDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraisingDepartmentDisplay
Purpose					: To display fundraising departments.
Example					: usp_WebHcHubCitiFundraisingDepartmentDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25 Aug 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraisingDepartmentDisplay]
(
   ----Input variable. 	  
	  @UserID Int
	, @HcHubCitiID Int			
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
											
					
			     SELECT HcFundraisingDepartmentID deptID
					   ,FundraisingDepartmentName deptName
				 FROM HcFundraisingDepartments	
				 WHERE HcHubCitiID = @HcHubCitiID										 
																		
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingDepartmentDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
