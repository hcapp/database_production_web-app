USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcSSAdminHubCitiUpdation]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcSSAdminHubCitiUpdation
Purpose					: To create the hub citi admin users by ScanSee admin.
Example					: usp_WebHcSSAdminHubCitiUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			11/9/2013	    Span		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcSSAdminHubCitiUpdation]
(
   ----Input variable.
	  @ScanSeeAdminUserID int
	, @HubCitiID int
	, @HubCitiName varchar(255)
	, @CityExperience varchar(255)
	, @State varchar(100)
	, @City varchar(4000)-- Comma separated cities
	, @PostalCode varchar(max) --Comma separated zip codes.
	--, @UserName varchar(255)
	--, @EmailID varchar(255)
	  
	--Output Variable 
    , @Status int output
    , @DuplicateEmail bit output
    , @DuplicateHubCitiName bit output
    , @DuplicateUserName bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
			DECLARE @HcAdminUserID INT
			SET @DuplicateHubCitiName = 0	
			
						
			--Check if the Hub Citi Name duplication.
			IF EXISTS(SELECT 1 FROM HcHubCiti WHERE HubCitiName = @HubCitiName AND HcHubCitiID <> @HubCitiID)
			BEGIN
				SET @DuplicateHubCitiName = 1
			END
			ELSE			
				--Creation of Hub Citi.
				BEGIN			
					UPDATE HcHubCiti SET HubCitiName = @HubCitiName
									 , DateModified = GETDATE()
									 , ModifiedUserID = @ScanSeeAdminUserID
					WHERE HcHubCitiID = @HubCitiID
									 					
					--If the SS admin is associating the Hub citi to set of cities within the given state.
					
						DELETE FROM HcLocationAssociation WHERE HcHubCitiID = @HubCitiID
						INSERT INTO HcLocationAssociation(HcHubCitiID
														, City
														, State
														, PostalCode 
														, DateCreated
														, CreatedUserID)
												SELECT @HubCitiID
													 , G.City
													 , @State
													 , P.Param 
													 , GETDATE()
													 , @ScanSeeAdminUserID
												FROM dbo.fn_SplitParam(@PostalCode, ',') P  
												INNER JOIN GeoPosition G ON G.PostalCode = P.Param	
												AND G.City = @City
												AND G.State = @State
					
					--Update the City Experience Details.
					UPDATE HcCityExperience SET CityExperienceName = @CityExperience
											  , DateModified = GETDATE()
											  , ModifiedUserID = @HcAdminUserID
					WHERE HcHubCitiID = @HubCitiID 
					--If the Citi Experience is not created then create.
					IF (@@ROWCOUNT = 0)
					BEGIN
						INSERT INTO HcCityExperience(CityExperienceName
													, HcHubCitiID
													, DateCreated
													, CreatedUserID)
											VALUES(@HubCitiName
												 , @HubCitiID
												 , GETDATE()
												 , @ScanSeeAdminUserID)
					END
					
					--Logic is commented because these are read only fields in the Web
					----Update the user details.
					--UPDATE Users SET UserName = @UserName
					--					 , Email = @EmailID					
					--FROM Users HA
					--INNER JOIN HcUserRole HC ON HA.HcHubCitiID = HA.HcHubCitiID
					--WHERE HA.CreatedUserID = @ScanSeeAdminUserID
					--AND HcHubCitiID = @HubCitiID
					--AND HC.HcRoleID = (SELECT HcRoleID FROM HcRole WHERE HcRoleName = 'Role_Admin')
										
					
			END
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_WebHcSSAdminHubCitiUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
