USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcFAQCategoryDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcFAQCategoryDisplay
Purpose					: Display list of FAQ Category.
Example					: usp_WebHcFAQCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcFAQCategoryDisplay]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int
	, @SearchKey varchar(1000)  	
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @FAQCategoryId Varchar(MAX) --Comma separated Anythingpageids
    , @SortOrder VARCHAR (255)
	  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

	 		--set transaction   isolation level read uncommitted

	            DECLARE @UpperLimit int

				--To get the row count for pagination.	 
				SELECT @UpperLimit = ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
	            
				IF @FAQCategoryId IS NOT NULL
					BEGIN
						 CREATE TABLE #Temp(Rownum INT IDENTITY(1,1),SortID varchar(100))
						 INSERT INTO #Temp (SortID)
						 SELECT R.Param 
						 FROM dbo.fn_SplitParam(@SortOrder, ',') R

						 --Store Sort order to temp table
						 CREATE TABLE #Temp1(Rownum INT IDENTITY(1,1),PageID varchar(100))
						 INSERT INTO #Temp1(PageID)
						 SELECT R.Param 
						 FROM dbo.fn_SplitParam(@FAQCategoryId,',') R
             
						 --Store  FAQ CategoryID to temp table
						 CREATE TABLE #Temp2(Rownum INT IDENTITY(1,1),SortID VARCHAR(255),PageID VARCHAR(100))
						 INSERT INTO #Temp2 (SortID,PageID )
						 SELECT	T1.SortID 
							   ,T2.PageID  
						 FROM #Temp T1 
						 INNER JOIN #Temp1 T2 ON T1.Rownum =T2.Rownum 

		   
						--Update FAQ Category based on sorting.
						UPDATE HcFAQCategory  SET SortOrder = T.SortID 
						FROM HcFAQCategory  R
						INNER JOIN #temp2 T ON T.PageID =R.HcFAQCategoryID  
					END
				
				   
				--Display list FAQ Categories
								
				SELECT  ROW_NUMBER() OVER(ORDER BY IIF(C.SortOrder=0 OR C.SortOrder IS NULL,10000,C.SortOrder),C.HcFAQCategoryID,CategoryName ASC) Rownum
				               ,C.HcFAQCategoryID 
				               ,CategoryName 
							   ,Associateflag=(CASE WHEN F.HcFAQCategoryID IS NULL THEN 0 ELSE 1 END)
							   ,C.SortOrder
				INTO #FAQCategories
				FROM HcFAQCategory C
				LEFT JOIN HcFAQ F ON F.HcFAQCategoryID =C.HcFAQCategoryID 
				WHERE C.HcHubCitiID=@HcHubcitiID AND (@SearchKey IS NULL OR (@SearchKey IS NOT NULL AND CategoryName LIKE '%'+@SearchKey+'%'))				
				GROUP BY C.HcFAQCategoryID,CategoryName,F.HcFAQCategoryID ,C.SortOrder

				--To capture max row number.  
				SELECT @MaxCnt = MAX(RowNum) FROM #FAQCategories				

				IF @LowerLimit IS NULL
				BEGIN
					SET @LowerLimit=0
					SET @UpperLimit=@MaxCnt					
				END
				 
				--this flag is a indicator to enable "More" button in the UI.   
				--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@UpperLimit+@LowerLimit)) > 0 THEN 1 ELSE 0 END 
				
				SELECT Rownum rowNum
				      ,HcFAQCategoryID faqCatId
				      ,CategoryName faqCatName
					  ,Associateflag
					  ,SortOrder sortOrder
				FROM #FAQCategories
				ORDER BY Rownum
				OFFSET @LowerLimit ROWS
				FETCH NEXT ISNULL(@UpperLimit,1) ROWS ONLY						
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
