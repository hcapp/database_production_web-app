USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcHubCitiFundraiserEventDisplay]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcHubCitiFundraiserEventDisplay
Purpose					: To display list of events associated to fundraiser.
Example					: usp_WebHcHubCitiFundraiserEventDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2014	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcHubCitiFundraiserEventDisplay]
(   
    --Input variable.	  
	  @HcFundraiserID int	
  
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		   
	
			DECLARE @RetailConfig Varchar(100)
			DECLARE @Config Varchar(100)
			DECLARE @RetailerFundID int

			SELECT @RetailerFundID = RetailID
			FROM HcFundraising
			WHERE HcFundraisingID = @HcFundraiserID
			
			SELECT @RetailConfig = ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'  
			
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Hubciti Media Server Configuration'  
			
			--To display List fo appsites associated to events.
			
			SELECT DISTINCT HcEventName 
			--http://localhost:8080/Images/retailer/1253/bg9_33934.png
			--eventImagePath = @Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath
					,eventImagePath= CASE WHEN @RetailerFundID IS NULL THEN @Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
									      ELSE @RetailConfig+'/'+CAST(@RetailerFundID AS VARCHAR(100))+'/'+E.ImagePath END
					,eventStartDate =CAST(StartDate AS DATE)
					,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
					,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		 ELSE HE.Address 
										 END
							  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		 ELSE HE.City 
									  END
							  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		 ELSE HE.State 
									  END
							  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcEventAppsite A 
																			  INNER JOIN HcAppSite B ON A.HcAppsiteID = B.HcAppsiteID 
																			  INNER JOIN RetailLocation RL ON B.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		 ELSE HE.PostalCode 
									  END  		   	   
			FROM HcEvents E
			INNER JOIN HcFundraisingEventsAssociation FE ON E.HcEventID = Fe.HcEventID	
			INNER JOIN HcEventLocation HE ON FE.HcEventID = HE.HcEventID		
			WHERE FE.HcFundraisingID = @HcFundraiserID
			AND E.Active = 1
									
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraiserEventDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
