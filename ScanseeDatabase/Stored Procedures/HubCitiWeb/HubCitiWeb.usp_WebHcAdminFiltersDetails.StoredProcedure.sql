USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiWeb].[usp_WebHcAdminFiltersDetails]    Script Date: 4/6/2017 1:08:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcAdminFiltersDetails]
Purpose					: To Display Filters details for given HubCiti.
Example					: [usp_WebHcAdminFiltersDetails]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			01/10/2013	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiWeb].[usp_WebHcAdminFiltersDetails]
(
    --Input variable
	  @HubCitiID int
	, @CityExperienceID int
	, @FilterID int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To display Filter Retailer Details
			SELECT FilterName
				   ,R.RetailID
				   ,R.RetailName
				   ,FRL.RetailLocationID
				   ,RL.Address1
				   ,RL.City
				   ,RL.[State]
				   ,RL.PostalCode
			FROM HcFilter F
			INNER JOIN HcFilterRetailLocation FRL ON F.HcFilterID = FRL.HcFilterID
			INNER JOIN RetailLocation RL ON FRL.RetailLocationID = RL.RetailLocationID
			INNER JOIN Retailer R ON RL.RetailID = R.RetailID
			WHERE F.HcFilterID = @FilterID AND (RL.Active=1 AND R.RetailerActive=1)
			
			--Confirmation of Success
			SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminFiltersDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
