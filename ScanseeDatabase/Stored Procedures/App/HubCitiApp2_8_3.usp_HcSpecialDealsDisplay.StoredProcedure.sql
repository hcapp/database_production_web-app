USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcSpecialDealsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [HubCitiApp2_8_3].[usp_HcSpecialDealsDisplay]  
(  
		
	--Input Variables
	  @UserID int
	, @HubCitiID int  	
	, @Latitude decimal(18,6)
	, @Longitude decimal(18,6)
	, @PostalCode varchar(10)
	
	--OutPut Variables
	, @UserOutOfRange bit output 
	, @DefaultPostalCode varchar(50) output
	, @Status int output  
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
 
		DECLARE @SpecialOfferFlag bit  
			  , @HotDealFlag bit =0
			  , @DiscountFlag bit 
			  , @SaleFlag bit
			  , @DistanceFromUser FLOAT
			  --, @PostalCode varchar(10)

			  --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
					
		
			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
				IF @PostalCode IS NOT NULL
				BEGIN
					SELECT @Latitude=Latitude
						  ,@Longitude=Longitude
					FROM GeoPosition 
					Where PostalCode = @PostalCode
				END

				ELSE
				BEGIN
					SELECT @Latitude = G.Latitude
						 , @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID
				END
			END	
			
			
						  
		 --If the radius is not received then consider the global radius.
		 DECLARE @Radius INT		
		
	     IF @Radius IS NULL
	     BEGIN
			SELECT @Radius = ScreenContent
			FROM AppConfiguration
			WHERE ScreenName = 'DefaultRadius'
	     END	

		--Check if the RetailLocation has Special Offers
		SELECT @SpecialOfferFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM QRRetailerCustomPage Q
		INNER JOIN QRRetailerCustomPageAssociation QRA ON Q.QRRetailerCustomPageID = QRA.QRRetailerCustomPageID
		INNER JOIN QRTypes QRT ON QRT.QRTypeID = Q.QRTypeID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = QRA.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID
		INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HubCitiID AND Associated = 1
		INNER JOIN GeoPosition G ON HL.PostalCode = G.PostalCode
		WHERE QRT.QRTypeName = 'Special Offer Page'	
		AND (ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) < = @Radius
		AND GETDATE() BETWEEN ISNULL(StartDate,GETDATE()-1) AND ISNULL(EndDate, GETDATE() + 1)

		--Check if the RetailLocation has Hotdeals
		SELECT @HotDealFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM ProductHotDeal P
		INNER JOIN ProductHotDealRetailLocation R ON P.ProductHotDealID = R.ProductHotDealID
		INNER JOIN ProductHotDealLocation PL ON R.ProductHotDealID = PL.ProductHotDealID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = R.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID
		INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HubCitiID AND Associated = 1
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.PostalCode
		LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID AND HCuserID = @UserID		
		WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1) 
		AND (ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) < = @Radius
		GROUP BY P.ProductHotDealID
				,NoOfHotDealsToIssue
		HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				 ELSE ISNULL(COUNT(HCUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)
				 
		SELECT @HotDealFlag = CASE WHEN (@HotDealFlag + COUNT(DISTINCT P.ProductHotDealID)) > 0  THEN 1 ELSE 0 END
		FROM ProductHotDeal P
		LEFT JOIN ProductHotDealLocation PL ON P.ProductHotDealID = PL.ProductHotDealID
		WHERE PL.ProductHotDealID IS NULL
		AND GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1) 

        
        SELECT @SaleFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM RetailLocation RL
		INNER JOIN RetailLocationDeal RD ON RL.RetailLocationID = RD.RetailLocationID AND RL.Active=1		
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID
		INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HubCitiID AND Associated = 1
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.PostalCode		
		WHERE GETDATE() BETWEEN ISNULL(SaleStartDate, GETDATE() - 1) AND ISNULL(SaleEndDate, GETDATE() + 1) 
   		AND (ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) < = @Radius
		
		--Check if the RetailLocation has coupons.
		SELECT C.CouponID, CR.RetailLocationID
		INTO #Coupon
		FROM Coupon C
		INNER JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = CR.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HubCitiID		
		INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HubCitiID AND Associated = 1
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.PostalCode
		LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcuserID = @UserID
		AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
   		WHERE (ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) < = @Radius
		GROUP BY C.CouponID
				,NoOfCouponsToIssue
				,CR.RetailLocationID
		HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				 ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
		
		INSERT INTO #Coupon(CouponID, RetailLocationID)
		SELECT C.CouponID, CR.RetailLocationID
		FROM Coupon C
		LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID  
		LEFT JOIN HcUserCouponGallery U ON U.CouponID = C.CouponID AND U.HcuserID = @UserID
		WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
		AND CR.CouponRetailerID IS NULL
		GROUP BY C.CouponID
				,NoOfCouponsToIssue
				,CR.RetailLocationID
		HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				 ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0) 

		--Check if the RetailLocation has Loyalty
		SELECT LoyaltyDealID, RL.RetailLocationID
		INTO #Loyalty
		FROM LoyaltyDeal LD
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = LD.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID
		INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HubCitiID AND Associated = 1
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.PostalCode
		WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
   		AND (ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) < = @Radius

		--Check if the RetailLocation has Rebate
		SELECT R.RebateID, RR.RetailLocationID
		INTO #Rebate
		FROM RebateRetailer RR
		INNER JOIN Rebate R ON R.RebateID = RR.RebateID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = RR.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID
		INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HubCitiID AND Associated = 1
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.PostalCode
		WHERE GETDATE() BETWEEN ISNULL(RebateStartDate, GETDATE()-1) AND ISNULL(RebateEndDate, GETDATE() + 1)
   		AND (ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) < = @Radius

		--Enable Discount falg
		SELECT @DiscountFlag = CASE WHEN COUNT(DISTINCT(C.CouponID))+COUNT(DISTINCT(L.LoyaltyDealID))+COUNT(DISTINCT(R.RebateID))>0 THEN 1 ELSE 0 END
		FROM #Coupon C
		FULL OUTER JOIN #Loyalty L ON L.RetailLocationID = C.RetailLocationID
		FULL OUTER JOIN #Rebate R ON R.RetailLocationID = L.RetailLocationID   
       

		SELECT @SpecialOfferFlag specialOffFlag
			 , @HotDealFlag hotDealFlag
			 , @DiscountFlag clrFlag   
			 , @SaleFlag saleFlag   													
	
		--Confirmation of Success.
		SELECT @Status = 0	
												  
		
			
 	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminFiltersCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION; 
			--Confirmation of failure.
			SELECT @Status = 1 			
		END;
		 
	END CATCH;
END;

GO
