USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcDealsMaxCntGalleryAllCouponsByProduct]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcGalleryAllCouponsByProduct
Purpose					: To Display All Coupons By Product
Example					: usp_HcGalleryAllCouponsByProduct

History
Version		 Date		     Author	           Change Description
------------------------------------------------------------------------------- 
1.0		     4/28/2015	     Span					1.0
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcDealsMaxCntGalleryAllCouponsByProduct]
(

	--Input Variables 	
	   @HcUserID int
	 , @HcHubcitiID int
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(100)	
	
	--Output Variables
	 , @MaxCnt int output

)
AS
BEGIN

	BEGIN TRY		
		
			 DECLARE @UserOutOfRange bit 
			 DECLARE @DefaultPostalCode varchar(50)
			 DECLARE @DistanceFromUser FLOAT 
			 DECLARE @Status bit 
			 DECLARE @ErrorNumber int 
			 DECLARE @ErrorMessage varchar(1000)  	
			 DECLARE @LocCategory Int		
			
		--SELECT DISTINCT BusinessCategoryID 
		--INTO #UserLocCategories
		--FROM HcUserPreferredCategory 
		--WHERE HcUserID=@HcUserID --AND HcHubCitiID =@HcHubcitiID 

		--SELECT @LocCategory=COUNT(1) FROM #UserLocCategories 

		--To get Bottom Button Image URl
		DECLARE @Config VARCHAR(500)
		SELECT @Config = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'
		
		
		EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @HcUserID, @HcHubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)	
			BEGIN
				IF @PostalCode IS NOT NULL
				BEGIN
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude
					FROM GeoPosition 
					WHERE PostalCode = @PostalCode
				END
				ELSE 
				BEGIN
				    SELECT @PostalCode=PostalCode 
					FROM HcUser WHERE HcUserID=@HcUserID
				    
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude						
					FROM GeoPosition 
					WHERE PostalCode = @PostalCode
					 
				END
			END	
		--To get Default Radius
		DECLARE @Radius FLOAT
		SELECT @Radius = ScreenContent 
		FROM AppConfiguration
		WHERE ScreenName = 'DefaultRadius'
		AND ConfigurationType = 'DefaultRadius' 

		CREATE TABLE #Coupons(Row_Num INT IDENTITY(1, 1)
							  , CategoryID INT 
							  , CategoryName VARCHAR(255)
							  , CouponID INT
							  , CouponName VARCHAR(500)
							  , CouponDescription VARCHAR(1000)
							  , CouponStartDate DATETIME
							  , CouponExpireDate DATETIME
							  , CouponDiscountAmount MONEY
							  , ClaimFlag BIT
							  , RedeemFlag BIT
							  , NewFlag BIT
							  , NoOfCouponsToIssue INT
							  , Distance FLOAT
							  ) 
		
		--Check if the User has selected the any categories
		DECLARE @CategorySet BIT
		SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
		FROM HcUserCategory 
		WHERE HcUserID = @HcUserID	
		
		
		--Check if the user has not given latitude,longitude and PopulationCenterID
		IF (@Latitude IS NULL AND @Longitude IS NULL)
		BEGIN
			  
				INSERT INTO #Coupons( CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
							 )
				    SELECT  CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
				     FROM
					--Coupons associated to Products but not to RetailLocations
					(SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
								  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
								  , C.CouponID
								  , C.CouponName
								  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
								  , CouponStartDate
								  , CouponExpireDate
								  , CouponDiscountAmount
								  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
								  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
								  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
								  , NoOfCouponsToIssue
								  , '0' Distance
								  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
							FROM Coupon C
							INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
							LEFT JOIN Product P ON CP.ProductID = P.ProductID
							LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
							LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
							LEFT JOIN CouponRetailer CR ON CP.CouponID = CR.CouponID
							LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1 
							LEFT JOIN HcRetailerAssociation HL ON  HL.HcHubCitiID=@HcHubcitiID AND RL.RetailLocationID =HL.RetailLocationID AND Associated =1 
							LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID							
							WHERE CR.CouponRetailerID IS NULL --AND HL.HcHubCitiID=@HcHubcitiID 
							--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
							AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
							AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory  WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
						)Coupon			  			  
			 			  
		END					
		--Check if the user has given latitude and longitude
		IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)
		BEGIN
		
					INSERT INTO #Coupons( CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
										 )
							SELECT CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
							FROM
							--Coupons associated to Products but not to RetailLocations W.R.T Latitude and Longitude
						    (SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL --AND HL.HcHubCitiID=@HcHubcitiID
									--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
									
									--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									 SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , Distance = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									--AND HL.HcHubCitiID = @HcHubcitiID
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									
									--Coupons not associated to RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL AND CP.CouponProductId IS NULL --AND HL.HcHubCitiID=@HcHubcitiID
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									)Coupons 														
			
	 END
	--Check if user has given populationCenterID
	
		SELECT	 rowNumber
				  ,	CategoryID
				  , CategoryName  
				  , CouponID
				  , CouponName
				  , CouponDescription
				  , CouponStartDate
				  , CouponExpireDate
				  , CouponDiscountAmount
				  , ClaimFlag
				  , RedeemFlag
				  , NewFlag
				  , NoOfCouponsToIssue
				  , Distance 
				  , Used
		INTO #Temp
		FROM
	    (SELECT  rowNumber = ROW_NUMBER() OVER(ORDER BY Distance,CategoryName ASC,NewFlag DESC,CouponName ASC)
				  ,	CategoryID
				  , CategoryName  
				  , C.CouponID
				  , CouponName
				  , CouponDescription
				  , CouponStartDate
				  , CouponExpireDate
				  , CouponDiscountAmount
				  , ClaimFlag
				  , RedeemFlag
				  , NewFlag
				  , NoOfCouponsToIssue
				  , MIN(Distance) Distance
				  , Used = COUNT(HcUserCouponGalleryID)
		FROM #Coupons C
		LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
		--LEFT JOIN CouponRetailer CR ON CR.CouponID =C.CouponID 
		--LEFT JOIN RetailerBusinessCategory RBC ON RBC.RetailerID =CR.RetailID 
		--LEFT JOIN #UserLocCategories UC ON UC.BusinessCategoryID =RBC.BusinessCategoryID 
		--WHERE (@LocCategory =0 OR (UC.BusinessCategoryID IS NOT NULL AND UC.BusinessCategoryID =RBC.BusinessCategoryID))
		GROUP BY CategoryID
				  , CategoryName  
				  , C.CouponID
				  , CouponName
				  , CouponDescription
				  , CouponStartDate
				  , CouponExpireDate
				  , CouponDiscountAmount
				  , ClaimFlag
				  , RedeemFlag
				  , NewFlag
				  , NoOfCouponsToIssue
				  , Distance
		 HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)													   
		)Temp
		 
		--To capture max row number.  
		SELECT @MaxCnt = MAX(rowNumber) FROM #Temp  
				
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcGalleryAllCouponsByProduct.'		
				
		END;
		 
	END CATCH;
END;






















GO
