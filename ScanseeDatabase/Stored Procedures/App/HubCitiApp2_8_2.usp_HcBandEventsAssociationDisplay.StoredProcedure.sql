USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcBandEventsAssociationDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name		 : [HubCitiApp2_8_2].[usp_HcBandEventsAssociationDisplay]
Purpose                      : To display List of Bands associated to BandEvents.
Example                      : [HubCitiApp2_8_2].[usp_HcBandEventsAssociationDisplay]

History
Version      Date                Author               Change Description
------------------------------------------------------------------------------------ 
1.0          04/21/2016			Bindu T A		 Initial Version - usp_HcBandEventsAssociationDisplay
------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcBandEventsAssociationDisplay]
(   
		
	--Input variable.   
		 @BandEventID INT 
	   , @HubCitiID int
       , @UserID Int    
       , @LowerLimit int  
       , @ScreenName varchar(50)
	   
       --Output Variable 
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		DECLARE @UpperLimit int
        DECLARE @RetailConfig Varchar(1000)
		DECLARE @Config VARCHAR(500)
					
		SELECT @Config = 
		ScreenContent
        FROM AppConfiguration
        WHERE ConfigurationType = 'Web Band Media Server Configuration' 

		 --To get the row count for pagination.   

        SELECT @UpperLimit = @LowerLimit + 
		ScreenContent  
        FROM AppConfiguration   
        WHERE ScreenName = @ScreenName
        AND ConfigurationType = 'Pagination'
        AND Active = 1

		SELECT DISTINCT
		BA.BandID,
		B.BandName,
		E.HcBandEventID,
		E.HcBandEventName,BandCategoryName,
		BA.BandStage,
		CAST(BA.BandstartDate AS DATE) BandstartDate,
		CAST(BA.BandEndDate AS DATE) BandEndDate,
		REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(BA.BandstartTime AS Time),100),'AM',' AM') ,'PM',' PM') BandstartTime,
		REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(BA.BandEndTime AS Time),100),'AM',' AM') ,'PM',' PM') BandEndTime,
		CASE WHEN BandImagePath IS NOT NULL THEN @Config+CONVERT(VARCHAR(30),B.BandID)+'/'+ BandImagePath 
											   ELSE 'http://66.228.143.28:8080/Images/band/default-logo.jpg' END  BandImagePath 
		--@Config + CAST(B.BandID AS VARCHAR(100))+'/'+B.BandImagePath
		INTO #temp
		FROM HcBandEventsAssociation BA
		INNER JOIN Band B ON B.BandID = BA.BandID 
		INNER JOIN HcBandEvents E ON BA.HcBandEventID= E.HcBandEventID AND E.HcBandEventID = @BandEventID
		INNER JOIN BandCategoryAssociation BC ON BC.BandID = BA.BandID 
		INNER JOIN BandCategory C ON C.BandCategoryID = BC.BandCategoryID
		WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND B.BandActive=1
				
		SELECT  DISTINCT E.BandID,E.BandName,E.HcBandEventID,E.HcBandEventName,
		LTRIM(REPLACE(STUFF((SELECT ', ' + BandCategoryName
														FROM #temp t
														WHERE E.BandId = t.BandId
														FOR XML PATH('')),1,1,''),'&amp;','&')) BandCategoryName,
		E.BandStage,E.BandstartDate,E.BandEndDate,E.BandstartTime,E.BandEndTime,E.BandImagePath
		INTO #Result1
		FROM #temp E
		
		SELECT RowNum = ROW_NUMBER() OVER (ORDER BY BandstartDate,BandstartTime),
		BandID,BandName,HcBandEventID,HcBandEventName,BandCategoryName,BandStage,BandstartDate,BandEndDate,BandstartTime,BandEndTime,BandImagePath
		INTO #Result
		FROM #Result1
		
		SELECT RowNum rowNum,
		BandID bandID,
		BandName bandName,
		HcBandEventID bEvtId,
		HcBandEventName bEvtName,
		BandCategoryName catName,
		BandStage stage,
		BandstartDate sDate,
		BandEndDate eDate,
		BandstartTime sTime,
		BandEndTime eTime,
		BandImagePath bandImgPath
		FROM #Result
		WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit
		ORDER BY rowNum,CAST(BandstartDate AS DATE),CAST(BandstartTime AS TIME)
		
		SELECT @MaxCnt = COUNT(1) FROM #Result1

		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

		--EXEC [HubCitiApp2_8_2].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,'Band Events',@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                

		DROP TABLE #temp

	--Confirmation of Success
			SELECT @Status = 0    		

	END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_HcBandEventsAssociationDisplay.'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END
              
       END CATCH
END





GO
