USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcCouponGallerySortFilterListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcCouponGallerySortFilterListDisplay]
Purpose                 :  To get the list of Sort and filter items for Coupons Gallery.
Example                 :  [usp_HcCouponGallerySortFilterListDisplay]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          12/13/2016     Shilpashree         1.1
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcCouponGallerySortFilterListDisplay]
(   
    --Input variable
	  @UserID int
    , @HcHubCitiID int
	, @SearchKey varchar(500)
	, @Latitude Decimal(18,6)
	, @Longitude Decimal(18,6)
	, @PostalCode varchar(100)	

	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
       BEGIN TRY

		DECLARE @Distance bit = 1
				, @Alphabetically bit = 1	
				, @Category bit = 0	
				, @City bit = 0
				, @MaxCnt1 int
				
			
		----To Filter Coupons based on Location categories
		EXEC [HubCitiApp2_8_3].[usp_HcDealsMaxCntGallerySortFilterCouponsByLocation] @UserID,@HcHubcitiID,@SearchKey,@Latitude,@Longitude,@PostalCode, @MaxCnt = @MaxCnt1 OUTPUT 

		IF(@MaxCnt1 > 0)
		BEGIN
			SET @Category = 1
			SET @City = 1
		END

		CREATE TABLE #Tempfilter (Type Varchar(50), Items Varchar(50),Flag bit)
		INSERT INTO #Tempfilter VALUES ('Sort Items by','Distance',@Distance)
		INSERT INTO #Tempfilter VALUES ('Sort Items by','Alphabetically',@Alphabetically)

		INSERT INTO #Tempfilter VALUES ('Filter Items by','Category',@Category)
		INSERT INTO #Tempfilter VALUES ('Filter Items by','City',@City)
			
		
		SELECT Type AS fHeader
				,Items AS filterName
		FROM #Tempfilter
		WHERE Flag = 1
			
		--Confirmation of Success.
        SET @Status = 0

	END TRY

    BEGIN CATCH
		--Check whether the Transaction is uncommitable.
        IF @@ERROR <> 0    
        BEGIN    
			PRINT 'Error occured in Stored Procedure [HubCitiApp2_8_3].[usp_HcCouponGallerySortFilterListDisplay]'      
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
           
			--Confirmation of failure.
			SET @Status = 1
        END;    
            
      END CATCH;
										
END;




GO
