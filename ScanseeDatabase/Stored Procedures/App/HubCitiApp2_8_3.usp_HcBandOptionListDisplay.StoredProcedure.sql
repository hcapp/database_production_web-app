USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcBandOptionListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [HubCitiApp2_3_3].[usp_HcEventsOptionListDisplay]
Purpose                 :  To get the list of filter options for Events (Filter and Sort)
Example                 :  [HubCitiApp2_3_3].[usp_HcEventsOptionListDisplay]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          8/05/2015       SPAN            1.1
-------------------------------------------------------------------------------
*/

--exec [HubCitiApp2_8_3].[usp_HcBandOptionListDisplay] 2162,10533,null,null,null,null,null,null,null,null,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcBandOptionListDisplay]
(   
           
       --Input variable.        
         @HcHubCitiID int
       , @UserID Int
       , @HcMenuItemID int
       , @HcBottomButtonID int
	   , @SearchKey varchar(2000)	
	   , @Latitude float
	   , @Longitude float
	   , @Postalcode Varchar(200)
	  --   @BandEventTypeID int,
		--@BandID int
	   --, @RetailID int
	   --, @RetailLocationID int
	  -- , @FundRaisingID Int

       --User Tracking
       --, @MainMenuID Int
  
       --Output Variable 
        , @UserOutOfRange bit output
        , @DefaultPostalCode VARCHAR(10) output
		--, @MaxCnt int  output
		--, @NxtPageFlag bit output 
        , @Status int output
        , @ErrorNumber int output
        , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY

			DECLARE @RegionApp Bit

			SELECT @RegionApp = IIF(HcAppListID=2,1,0)
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID 
              
			--To display Filter items
		   
		    DECLARE	@EventDateSort bit = 1	   
			DECLARe @Distance bit = 1
			DECLARE @Alphabetically bit = 1	
			DECLARE @EventDate bit = 1		 
			DECLARE @Category bit = 1						
			DECLARE @City bit = 1

			CREATE TABLE #Tempfilter (Type Varchar(50), Items Varchar(50),Flag bit)
			INSERT INTO #Tempfilter VALUES ('Sort Items by','Name',@Alphabetically)

			INSERT INTO #Tempfilter VALUES ('Filter Items by','Genre',@Category)
			--INSERT INTO #Tempfilter VALUES ('Filter Items by','City',@Category)
		
			
			IF @RegionApp = 1
			BEGIN	
				SELECT Type AS fHeader
					  ,Items AS filterName
				FROM #Tempfilter
			END
			ELSE
			BEGIN
				SELECT Type AS fHeader
					  ,Items AS filterName
				FROM #Tempfilter
				WHERE Items <> 'City'
			END
				
			--Confirmation of Success.
            SELECT @Status = 0


	END TRY

    BEGIN CATCH
      
			--Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0    
            BEGIN    
            PRINT 'Error occured in Stored Procedure [HubCitiApp2_3_3].[usp_HcEventsOptionListDisplay]'      
            --- Execute retrieval of Error info.  
            EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			 
            --Confirmation of failure.
            SELECT @Status = 1
            END;    
            
      END CATCH;


												
END;


























GO
