USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcEventsOptionCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	 : [HubCitiApp2_3_3].[usp_HcEventsOptionCategoryDisplay]
Purpose                  : To display List of Categories concerened to Events for both hubciti or hubregion
Example                  : [HubCitiApp2_3_3].[usp_HcEventsOptionCategoryDisplay]

History
Version     Date             Author        Change Description
--------------------------------------------------------------- 
1.0        7/15/2015        Span               1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcEventsOptionCategoryDisplay]
(   
    --Input variable.        
         @HcHubCitiID int
       , @UserID Int       
       , @Latitude Float
       , @Longitude Float
       , @Postalcode Varchar(200)       
       , @HcMenuItemID int
       , @HcBottomButtonID int
       , @SearchKey Varchar(1000)	   
	   , @FundRaisingID Int
	   , @RetailID int
 	   , @RetailLocationID int

       --Output Variable 
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
               
            DECLARE @DistanceFromUser FLOAT
            DECLARE @UserLatitude float
            DECLARE @UserLongitude float 
					 
            IF @Postalcode IS NOT NULL AND @Latitude IS NULL
            BEGIN
                    SELECT @Latitude = G.Latitude
                            , @Longitude = G.Longitude
                    FROM GeoPosition G
                WHERE   G.PostalCode = @Postalcode 
                                 
            END
              
            SELECT @UserLatitude = @Latitude
                , @UserLongitude = @Longitude                  

            IF (@UserLatitude IS NULL) 
            BEGIN
                SELECT @UserLatitude = Latitude
                        , @UserLongitude = Longitude
                FROM HcUser A
                INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                WHERE HcUserID = @UserID 
            END
            --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
            IF (@UserLatitude IS NULL) 
            BEGIN
                SELECT @UserLatitude = Latitude
                                , @UserLongitude = Longitude
                FROM HcHubCiti A
                INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                WHERE A.HcHubCitiID = @HcHubCitiID
            END

            print @UserLatitude
            print @UserLongitude
             
            --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
            EXEC [HubCitiApp7].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
            SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)

            --Derive the Latitude and Longitude in the absence of the input.
            IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
            BEGIN
                IF @Postalcode IS NULL
                BEGIN
                        SELECT @Latitude = G.Latitude
                                , @Longitude = G.Longitude
                        FROM GeoPosition G
                        INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                        WHERE U.HcUserID = @UserID
                END
                ELSE
                BEGIN
                        SELECT @Latitude = Latitude
                                , @Longitude = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @Postalcode
                END
            END

			DECLARE @HcAppListID int
			SELECT @HcAppListID = HcAppListID
			FROM HcAppList
			WHERE HcAppListName = 'RegionApp'

			--HUBREGION SECTION

			IF EXISTS (SELECT 1 FROM HcHubCiti WHERE HcHubCitiID = @HcHubCitiID AND HcAppListID = @HcAppListID)
			BEGIN

				SELECT HcHubCitiID
				INTO #RHubcitiList
				FROM
					(SELECT RA.HcHubCitiID
					FROM HcHubCiti H
					LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
					WHERE H.HcHubCitiID  = @HcHubCitiID
					UNION ALL
					SELECT @HcHubCitiID)A

				DECLARE @HubCitis varchar(100)
				SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
				FROM #RHubcitiList

				IF(@HcMenuItemID IS NOT NULL)
					BEGIN
						--MenuItem	

						SELECT DISTINCT H.HcHubCitiID AS HubcitiID,EC.HcEventCategoryID AS EventCategoryID
										,EC.HcEventCategoryName AS EventCategoryName,E.HcEventID AS EventID
						INTO #AllEventCategories
						FROM HcMenuItemEventCategoryAssociation ME
						INNER JOIN HcEventsCategoryAssociation ECA ON  ME.HcEventCategoryID = ECA.HcEventCategoryID AND  ME.HcMenuItemID = @HcMenuItemID	
						INNER JOIN HcEventsCategory EC ON ECA.HcEventCategoryID = EC.HcEventCategoryID
						INNER JOIN HcEvents E ON ECA.HcEventID = E.HcEventID
						LEFT JOIN HcHubCiti H ON E.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
							
					END
					ELSE IF(@HcBottomButtonID IS NOT NULL)
					BEGIN
						--BottomButton
								
						SELECT DISTINCT H.HcHubCitiID AS HubcitiID,EC.HcEventCategoryID AS EventCategoryID
										,EC.HcEventCategoryName AS EventCategoryName,E.HcEventID AS EventID
						INTO #AllEventCategories1
						FROM HcBottomButtonEventCategoryAssociation ME
						INNER JOIN HcEventsCategoryAssociation ECA ON  ME.HcEventCategoryID = ECA.HcEventCategoryID AND  ME.HcBottomButtonID = @HcBottomButtonID	
						INNER JOIN HcEventsCategory EC ON ECA.HcEventCategoryID = EC.HcEventCategoryID
						INNER JOIN HcEvents E ON ECA.HcEventID = E.HcEventID
						LEFT JOIN HcHubCiti H ON E.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1

					END
												
					CREATE TABLE #Temp1111(HcEventID Int
											,HcHubCitiID Int
											,HcEventCategoryID Int 
											,HcEventCategoryName Varchar(500)
											,Distance Int                                                                            
											,DistanceActual	Int
											,City Varchar(100)
											,Lat Float
											,Long Float	
											)		
					

				IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
				BEGIN
							
					INSERT INTO #Temp1111(HcEventID 
										,HcHubCitiID
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual
										,City
										,Lat 
										,Long									  
										)
								SELECT HcEventID 
									  ,HcHubCitiID 
									  ,EventCategoryID
									  ,EventCategoryName 
									  ,Distance                                                                        
									  ,DistanceActual
									  ,CityName
									  ,RetailLocationLatitude 
								     ,RetailLocationLongitude									  
					FROM(
					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,C.CityName	
									,RL1.RetailLocationLatitude 
								    ,RL1.RetailLocationLongitude				
					FROM HcEvents E 
					INNER JOIN #AllEventCategories A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
					INNER JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
					LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
					LEFT JOIN RetailLocation  RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (RL1.RetailLocationID =EP.RetailLocationID)
					LEFT JOIN HcCity C ON RL1.City = C.CityName
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																	
					UNION ALL

					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,C.CityName	
									,RL1.Latitude  RetailLocationLatitude
									,RL1.Longitude 	RetailLocationLongitude				
					FROM HcEvents E 
					INNER JOIN #AllEventCategories A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
					INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
					LEFT JOIN HcCity C ON HL.City = C.CityName
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																	
					UNION ALL
					
					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,HC.CityName
									,RL.RetailLocationLatitude 
									,RL.RetailLocationLongitude				
					FROM #AllEventCategories A 
					INNER JOIN HcEvents E on A.EventID = E.HcEventID
					INNER JOIN HcRetailerEventsAssociation RE on E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
					INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND RE.RetailLocationID = R.RetailLocationID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
					INNER JOIN RetailLocation  RL ON RE.RetailLocationID=RL.RetailLocationID  
					INNER JOIN HcCity HC ON RL.City = HC.CityName 
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1 AND RL.Active = 1
																	
					UNION ALL
					
					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,HC.CityName
									,RL1.Latitude   RetailLocationLatitude 
									,RL1.Longitude  RetailLocationLongitude				
					FROM #AllEventCategories A 
					INNER JOIN HcEvents E on A.EventID = E.HcEventID AND E.HcHubCitiID IS NULL
					INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and A.EventCategoryID = c.HcEventCategoryID
					INNER JOIN HcEventLocation EL on E.HcEventID = EL.HcEventID
					INNER JOIN GeoPosition  RL1 ON EL.City =RL1.City AND EL.State =RL1.State AND RL1.PostalCode =EL.PostalCode
					INNER JOIN HcCity HC ON EL.City = HC.CityName 
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
					
					)A
				END
				ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
				BEGIN
						INSERT INTO #Temp1111(HcEventID 
										,HcHubCitiID
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual
										,City
										,Lat
										,Long									  
										)
								SELECT HcEventID 
									  ,HcHubCitiID 
									  ,EventCategoryID
									  ,EventCategoryName 
									  ,Distance                                                                        
									  ,DistanceActual
									  ,CityName
									  ,RetailLocationLatitude
									  ,RetailLocationLongitude									  
					FROM(
					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,C.CityName
									,RL1.RetailLocationLatitude 
									,RL1.RetailLocationLongitude				
					FROM HcEvents E 
					INNER JOIN #AllEventCategories1 A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
					INNER JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
					LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
					LEFT JOIN RetailLocation  RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (RL1.RetailLocationID =EP.RetailLocationID)
					LEFT JOIN HcCity C ON RL1.City = C.CityName
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
								
					UNION ALL

					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,C.CityName
									,RL1.Latitude  RetailLocationLatitude
									,RL1.Longitude 	RetailLocationLongitude				
					FROM HcEvents E 
					INNER JOIN #AllEventCategories1 A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
					INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
					LEFT JOIN HcCity C ON HL.City = C.CityName
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																	
					UNION ALL

					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,HC.CityName
									,RL.RetailLocationLatitude
									,RL.RetailLocationLongitude				
					FROM #AllEventCategories1 A 
					INNER JOIN HcEvents E on A.EventID = E.HcEventID
					INNER JOIN HcRetailerEventsAssociation RE on E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
					INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND RE.RetailLocationID = R.RetailLocationID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
					INNER JOIN RetailLocation  RL ON RE.RetailLocationID=RL.RetailLocationID AND RL.Active = 1 
					INNER JOIN HcCity HC ON RL.City = HC.CityName 
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																	
					UNION ALL

					SELECT DISTINCT  E.HcEventID 
									,E.HcHubCitiID 	
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0
									,HC.CityName
									,RL1.Latitude   RetailLocationLatitude 
									,RL1.Longitude  RetailLocationLongitude				
					FROM #AllEventCategories1 A 
					INNER JOIN HcEvents E on A.EventID = E.HcEventID AND E.HcHubCitiID IS NULL
					INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and A.EventCategoryID = c.HcEventCategoryID
					INNER JOIN HcEventLocation EL on E.HcEventID = EL.HcEventID
					INNER JOIN GeoPosition  RL1 ON EL.City =RL1.City AND EL.State =RL1.State AND RL1.PostalCode =EL.PostalCode
					INNER JOIN HcCity HC ON EL.City = HC.CityName 
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
					
					) E

					END
					ELSE IF (@RetailID IS NOT NULL)
					BEGIN
						
						INSERT INTO #Temp1111(HcEventID 
										,HcHubCitiID
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual
										,City
										,Lat
										,Long									  
										)
								SELECT HcEventID 
									  ,HcHubCitiID 
									  ,HcEventCategoryID
									  ,HcEventCategoryName 
									  ,Distance                                                                        
									  ,DistanceActual
									  ,CityName
									  ,RetailLocationLatitude
									  ,RetailLocationLongitude									  
						FROM(
						SELECT DISTINCT  E.HcEventID 
										,E.HcHubCitiID 	
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           				
										,DistanceActual =0
										,HC.CityName
										,RL1.RetailLocationLatitude
										,RL1.RetailLocationLongitude				
						FROM HcEvents E 
						INNER JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
						INNER JOIN HcRetailerAssociation RA ON RE.RetailID = RA.RetailID AND RE.RetailLocationID = RA.RetailLocationID AND Associated = 1 AND RA.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))	
						INNER JOIN RetailLocation RL1 ON RE.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 	
						INNER JOIN HcCity Hc ON RL1.City = Hc.CityName
						WHERE E.RetailID =@RetailID AND RE.RetailLocationID=@RetailLocationID					
						AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1					
						) E


					END					
					ELSE IF(@FundRaisingID IS NOT NULL)
					BEGIN
					
					INSERT INTO #Temp1111(HcEventID 
										,HcHubCitiID
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual
										,City
										,Lat
										,Long									  
										)
								SELECT HcEventID 
									  ,HcHubCitiID 
									  ,HcEventCategoryID
									  ,HcEventCategoryName 
									  ,Distance                                                                        
									  ,DistanceActual
									  ,CityName
									  ,RetailLocationLatitude
								      ,RetailLocationLongitude									  
						FROM(
						SELECT DISTINCT  E.HcEventID 
										,E.HcHubCitiID 	
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           				
										,DistanceActual =0
										,HC.CityName
										,RL1.RetailLocationLatitude
									    ,RL1.RetailLocationLongitude			
						FROM HcFundraising HF
						INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
						INNER JOIN HcEvents E ON HF.HcHubCitiID = E.HcHubCitiID AND F.HcEventID = E.HcEventID
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
						LEFT JOIN HcEventLocation HL ON E.HcEventID =HL.HcEventID 
						LEFT JOIN HcEventAppsite EAP ON E.HcEventID = EAP.HcEventID
						LEFT JOIN HcAppSite HA ON EAP.HcAppSiteID = HA.HcAppsiteID
						LEFT JOIN HcEventPackage EP ON EA.HcEventID =EP.HcEventID
						LEFT JOIN RetailLocation RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (EP.RetailLocationID =RL1.RetailLocationID)	
						LEFT JOIN HcRetailerAssociation RA ON RL1.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated =1						                                    
						INNER JOIN HcCity Hc ON RL1.City = Hc.CityName OR HL.City = Hc.CityName
						WHERE F.HcFundRaisingID = @FundRaisingID
						AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1	
						
						UNION ALL

						SELECT DISTINCT  E.HcEventID 
										,E.HcHubCitiID 	
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           				
										,DistanceActual =0
										,HC.CityName
										,RL1.RetailLocationLatitude
								        ,RL1.RetailLocationLongitude			
						FROM HcFundraising HF
						INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
						INNER JOIN  HcEvents E ON HF.RetailID = E.RetailID AND F.HcEventID = E.HcEventID
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
						LEFT JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
						LEFT JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID 
						LEFT JOIN HcRetailerAssociation RA ON RE.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated =1							 
						LEFT JOIN RetailLocation RL1 ON RE.RetailLocationID =RL1.RetailLocationID AND RL1.Active = 1	
						INNER JOIN HcCity Hc ON RL1.City = Hc.CityName OR HL.City = Hc.CityName
						WHERE F.HcFundRaisingID = @FundRaisingID 
						AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
											
						) E
					
					END

					
					SELECT DISTINCT HcEventID 
									,T.HcHubCitiID 
									,HcEventCategoryID 
									,HcEventCategoryName 										                                                                    
									,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(Long) IS NULL THEN MIN(G.Longitude) ELSE MIN(Long) END/ 57.2958))))*6371) * 0.6214 END, 0))
									,DistanceActual =(ISNULL(CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(Long) IS NULL THEN MIN(G.Longitude) ELSE MIN(Long) END/ 57.2958))))*6371) * 0.6214, 1, 1) END ,0))                           
					INTO #Events
					FROM #Temp1111 T
					LEFT JOIN Geoposition G ON T.City =G.City 
					GROUP BY HcEventID 
							,T.HcHubCitiID 
							,HcEventCategoryID 
							,HcEventCategoryName 
				
							
					SELECT DISTINCT	HcEventCategoryID busCatId
								  , HcEventCategoryName busCatName										                                                                    							  			
					FROM #Events 
					ORDER BY HcEventCategoryName
				
			END						
			ELSE

			----HUBCITI SECTION----
			
			BEGIN

			 SELECT HcEventID
					,HcHubCitiID
					,RetailID
					,StartDate
					,EndDate
					,Active	
			INTO #Eventsss
			FROM HcEvents 
			WHERE (HcHubCitiID =@HcHubCitiID OR HcHubCitiID IS NULL)
			AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND Active = 1					

			SELECT DISTINCT RetailLocationID
			INTO #RLListss 
			FROM (
					
			SELECT DISTINCT RL.RetailLocationID 					
			FROM #Eventsss E
			INNER JOIN HcEventLocation EL ON EL.HcEventID =E.HcEventID 
			INNER JOIN RetailLocation RL ON RL.Address1 =EL.Address AND RL.State =EL.State AND RL.City =EL.City AND RL.PostalCode =EL.PostalCode AND RL.Active = 1

			UNION ALL

			SELECT RL.RetailLocationID 					
			FROM #Eventsss E
			INNER JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
			INNER JOIN HcRetailerAssociation RA ON RA.RetailID = RE.RetailID AND RE.RetailLocationID = RA.RetailLocationID AND Associated =1				
			INNER JOIN RetailLocation RL ON RL.RetailLocationID =RA.RetailLocationID AND RL.Active = 1

			UNION ALL

			SELECT RL.RetailLocationID 					
			FROM #Eventsss E
			INNER JOIN HcEventAppsite EA ON E.HcEventID = EA.HcEventID
			INNER JOIN HcAppSite A ON EA.HcAppsiteID = A.HcAppSiteID
			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
			)A 

			CREATE TABLE #Temp111(HcEventID Int
                                    ,HcHubCitiID Int
                                    ,HcEventCategoryID Int 
                                    ,HcEventCategoryName Varchar(500)
									,Distance Int                                                                            
									,DistanceActual	Int									  
									,RetailLocationID Int
									,Latitude Float
									,Longitude Float
									)
                     
            --To display List of Events
			IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
			BEGIN
				
				INSERT INTO #Temp111(HcEventID 
									,HcHubCitiID 
									,HcEventCategoryID
									,HcEventCategoryName 
									,Distance                                                                        
									,DistanceActual									  
									,RetailLocationID
									,Latitude 
									,Longitude)
						  SELECT HcEventID 
								,HcHubCitiID 
								,HcEventCategoryID
								,HcEventCategoryName 
								,Distance                                                                        
								,DistanceActual									  
								,RetailLocationID
								,Latitude 
								,Longitude
						FROM(
						SELECT DISTINCT E.HcEventID 
										,E.HcHubCitiID 
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           
										,DistanceActual =0
										,RL1.RetailLocationID 
										,Latitude=ISNULL(HL.Latitude,RL1.RetailLocationLatitude)
										,Longitude=ISNULL(HL.Longuitude,RL1.RetailLocationLongitude)
						FROM #Eventsss  E 
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID AND (EA.HcHubCitiID =@HcHubCitiID OR E.HcHubCitiID IS NULL)
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  
						INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID AND MIC.HcHubCitiID =@HcHubCitiID AND MIC.HcMenuItemID = @HcMenuItemID 
						LEFT JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
						LEFT JOIN HcEventAppsite A ON E.HcEventID = A.HcEventID AND A.HcHubCitiID = @HcHubCitiID --AND A.HcEventID IS NULL
						LEFT JOIN HcAppSite HA ON A.HcAppSiteID = HA.HcAppsiteID AND HA.HcHubCitiID = @HcHubCitiID                     
						LEFT JOIN HcEventPackage EP ON E.HcEventID =EP.HcEventID AND EP.HcHubCitiID = @HcHubCitiID                                      
						LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HcHubCitiID AND Associated = 1 AND (E.RetailID = RA.RetailID OR 1=1)
						LEFT JOIN #RLListss RRR ON RRR.RetailLocationID =RA.RetailLocationID 
						LEFT JOIN RetailLocation RL1 ON (RL1.RetailLocationID = RRR.RetailLocationID) AND RL1.Active = 1--OR (RL1.RetailLocationID = HA.RetailLocationID) OR (RRR.RetailLocationID =RL1.RetailLocationID)                
						LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID --AND  RE.HcEventID IS NULL							 
						WHERE ((EA.HcHubCitiID = @HcHubCitiID) OR (E.HcHubCitiID IS NULL AND E.RetailID = RA.RetailID))
						AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1	)A	
							
					END
					ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
					BEGIN
				
						INSERT INTO #Temp111(HcEventID 
											,HcHubCitiID 
											,HcEventCategoryID
											,HcEventCategoryName 
											,Distance                                                                        
											,DistanceActual									  
											,RetailLocationID
											,Latitude 
											,Longitude)
								  SELECT HcEventID 
										,HcHubCitiID 
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual									  
										,RetailLocationID
										,Latitude 
										,Longitude
						FROM(
						SELECT DISTINCT E.HcEventID 
										,E.HcHubCitiID 
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           
										,DistanceActual =0
										,RL1.RetailLocationID 
										,Latitude=ISNULL(HL.Latitude,RL1.RetailLocationLatitude)
										,Longitude=ISNULL(HL.Longuitude,RL1.RetailLocationLongitude)
						FROM #Eventsss  E 
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID AND (EA.HcHubCitiID =@HcHubCitiID OR E.HcHubCitiID IS NULL)
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  
						INNER JOIN HcBottomButtonEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID AND MIC.HcHubCitiID =@HcHubCitiID AND MIC.HcBottombuttonID = @HcBottomButtonID 
						LEFT JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
						LEFT JOIN HcEventAppsite A ON E.HcEventID = A.HcEventID AND A.HcHubCitiID = @HcHubCitiID --AND A.HcEventID IS NULL
						LEFT JOIN HcAppSite HA ON A.HcAppSiteID = HA.HcAppsiteID AND HA.HcHubCitiID = @HcHubCitiID                     
						LEFT JOIN HcEventPackage EP ON E.HcEventID =EP.HcEventID AND EP.HcHubCitiID = @HcHubCitiID                                      
						LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HcHubCitiID AND Associated = 1 AND (E.RetailID = RA.RetailID OR 1=1)
						LEFT JOIN #RLListss RRR ON RRR.RetailLocationID =RA.RetailLocationID 
						LEFT JOIN RetailLocation RL1 ON (RL1.RetailLocationID = RRR.RetailLocationID) AND RL1.Active = 1 --OR (RL1.RetailLocationID = HA.RetailLocationID) OR (RRR.RetailLocationID =RL1.RetailLocationID)                
						LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID --AND  RE.HcEventID IS NULL							 
						WHERE ((EA.HcHubCitiID =@HcHubCitiID) OR (E.HcHubCitiID IS NULL AND E.RetailID = RA.RetailID))
						AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1)B
							
					END
					
					ELSE IF (@FundRaisingID IS NOT NULL)
					BEGIN

							INSERT INTO #Temp111(HcEventID 
										,HcHubCitiID 
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual									  
										,RetailLocationID
										,Latitude 
										,Longitude)
								  SELECT HcEventID 
										,HcHubCitiID 
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual									  
										,RetailLocationID
										,Latitude 
										,Longitude
						FROM(
						SELECT DISTINCT E.HcEventID 
										,E.HcHubCitiID 
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           
										,DistanceActual =0
										,RL1.RetailLocationID 
										,Latitude=ISNULL(HL.Latitude,RL1.RetailLocationLatitude)
										,Longitude=ISNULL(HL.Longuitude,RL1.RetailLocationLongitude)
						FROM HcFundraising HF
						INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
						INNER JOIN  HcEvents E ON F.HcEventID = E.HcEventID --(HF.HcHubCitiID = E.HcHubCitiID OR HF.RetailID = E.RetailID) AND F.HcEventID = E.HcEventID
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
						LEFT JOIN HcEventLocation HL ON E.HcEventID =HL.HcEventID 
						LEFT JOIN HcEventAppsite EAP ON E.HcEventID = EAP.HcEventID
						LEFT JOIN HcAppSite HA ON EAP.HcAppSiteID = HA.HcAppsiteID
						LEFT JOIN HcEventPackage EP ON EA.HcEventID =EP.HcEventID
						LEFT JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
						LEFT JOIN RetailLocation RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (RE.RetailLocationID =RL1.RetailLocationID) OR (EP.RetailLocationID =RL1.RetailLocationID)	
						LEFT JOIN HcRetailerAssociation RA ON RL1.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated =1
						WHERE F.HcFundRaisingID = @FundRaisingID
						AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1	AND HF.Active = 1)C														 					   							
				END

				ELSE IF (@RetailID IS NOT NULL)
				BEGIN

				INSERT INTO #Temp111(HcEventID 
								,HcHubCitiID 
								,HcEventCategoryID
								,HcEventCategoryName 
								,Distance                                                                        
								,DistanceActual									  
								,RetailLocationID
								,Latitude 
								,Longitude)
					  SELECT HcEventID 
							,HcHubCitiID 
							,HcEventCategoryID
							,HcEventCategoryName 
							,Distance                                                                        
							,DistanceActual									  
							,RetailLocationID
							,Latitude 
							,Longitude
						FROM(
						SELECT DISTINCT E.HcEventID 
										,E.HcHubCitiID 
										,EC.HcEventCategoryID 
										,EC.HcEventCategoryName 
										,Distance=0                                           
										,DistanceActual =0
										,RL1.RetailLocationID 
										,Latitude = RL1.RetailLocationLatitude
										,Longitude = RL1.RetailLocationLongitude
						FROM HcEvents E 
						INNER JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
						INNER JOIN HcRetailerAssociation RA ON RE.RetailID = RA.RetailID AND RE.RetailLocationID = RA.RetailLocationID AND Associated = 1 AND RA.HcHubCitiID = @HcHubCitiID	
						INNER JOIN RetailLocation RL1 ON RA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1
						INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
						INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 	
						WHERE E.RetailID =@RetailID AND RE.RetailLocationID=@RetailLocationID
						AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1)D
				 END

				 SELECT HcEventID 
						,HcHubCitiID 
						,HcEventCategoryID
						,HcEventCategoryName 
						,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(T.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(T.Longitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
						,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(T.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(T.Longitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
                INTO #Events2
				FROM #Temp111 T                    
                LEFT JOIN RetailLocation RL1 ON RL1.RetailLocationID =T.RetailLocationID AND RL1.Active = 1                   
                LEFT JOIN Geoposition G ON (G.Postalcode=RL1.Postalcode) 
				GROUP BY HcEventID
						,HcHubCitiID
						,HcEventCategoryID
						,HcEventCategoryName 

				SELECT DISTINCT HcEventCategoryID busCatId
								,HcEventCategoryName busCatName										                                                                    							  			
				FROM #Events2 T
				ORDER BY HcEventCategoryName

			END

			--Confirmation of Success
			SELECT @Status = 0   
													
       END TRY
	  
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure [HubCitiApp2_3_3].[usp_HcEventsOptionCategoryDisplay]'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;
























GO
