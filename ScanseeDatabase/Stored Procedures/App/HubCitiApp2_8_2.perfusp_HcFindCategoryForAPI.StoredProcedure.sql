USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[perfusp_HcFindCategoryForAPI]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFindCategoryForAPI
Purpose					: To display the Categories under a selected Category.
Example					: 

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			08th Nov 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[perfusp_HcFindCategoryForAPI]
(
	   @ApiPartnerName Varchar(50)
	 , @CategoryName varchar(100)
	 , @HcHubcitiID Int
	--Output Variable 
	,  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
			SELECT  BusinessCategoryID 
					,REPLACE(CategoryName,'&','') CatName
			FROM  FindSourceCategory F
			INNER JOIN APIPartner A ON A.APIPartnerID=F.APIPartnerID
			WHERE ActiveFlag=1 And APIPartnerName=@ApiPartnerName
			AND CategoryDisplayName = @CategoryName
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcFindCategoryForAPI.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;












































GO
