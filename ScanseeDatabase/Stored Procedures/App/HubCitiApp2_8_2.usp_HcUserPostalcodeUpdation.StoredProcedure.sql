USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserPostalcodeUpdation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserPostalcodeUpdation
Purpose					: To update the user postal code when the Location Services is off and the user has not entered the Postal code at the time of							  Registration.
Example					: usp_HcUserPostalcodeUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th Nov 2013	Dhananjaya TR   Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserPostalcodeUpdation]
(
	  @HcUserID int
	, @PostalCode varchar(20)
	
	--Output Variables
	, @InvalidPostalCode bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
			
			--Check if the given Postal code is a valid one & notify the user
			IF EXISTS(SELECT 1 FROM GeoPosition WHERE PostalCode = @PostalCode)
			BEGIN
				UPDATE HcUser SET PostalCode = @PostalCode
				WHERE HcUserID = @HcUserID 			
				SET @InvalidPostalCode = 0
			END
			ELSE
			BEGIN
				SET @InvalidPostalCode = 1
			END
		
		--Confirmation of Success.
		SET @Status = 0		
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserPostalcodeUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
