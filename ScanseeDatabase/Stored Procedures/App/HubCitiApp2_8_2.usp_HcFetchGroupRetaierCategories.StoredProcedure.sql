USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcFetchGroupRetaierCategories]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFetchGroupRetaierCategories
Purpose					: To display User'd Preferred retailers.
Example					: usp_HcFetchGroupRetaierCategories 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Feb 2013 	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcFetchGroupRetaierCategories] 
(
	 
	 @RetailGroupID INT
   , @HubCitiID int
	 	
	--OutPut Variable
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	
	    DECLARE @ConfigirationType varchar(50)
		SELECT @ConfigirationType = ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType = 'App Media Server Configuration'
		
		--To fetch Retailer's Info 
		SELECT DISTINCT B.BusinessCategoryID categoryID
		      ,B.BusinessCategoryName categoryName	
		      ,@ConfigirationType+F.CategoryImagePath catImg            
		FROM HcCityExperience R
		INNER JOIN HcCityExperienceRetailLocation C ON R.HcCityExperienceID = C.HcCityExperienceID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = C.RetailLocationID AND RL.Headquarters = 0 AND RL.Active = 1
		INNER JOIN RetailerBusinessCategory RB ON RL.RetailID = RB.RetailerID 
		INNER JOIN BusinessCategory B ON RB.BusinessCategoryID = B.BusinessCategoryID AND B.BusinessCategoryName <> 'Other'
		INNER JOIN FindSourceCategory F ON F.BusinessCategoryID =B.BusinessCategoryID 
		WHERE C.HcCityExperienceID =  @RetailGroupID	
		ORDER BY B.BusinessCategoryName ASC	
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcFetchGroupRetaierCategories.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	 
		END;
		 
	END CATCH;
END;













































GO
