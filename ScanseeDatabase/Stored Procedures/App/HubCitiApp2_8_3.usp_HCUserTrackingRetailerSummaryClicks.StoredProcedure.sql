USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HCUserTrackingRetailerSummaryClicks]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HCUserTrackingRetailerSummaryClicks
Purpose					: To capture the click on the buttons on the Retailer Summary page.
Example					: usp_HCUserTrackingRetailerSummaryClicks

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30thOct2013     Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HCUserTrackingRetailerSummaryClicks]
(
	  
	  @RetailerDetailsID int
	, @AnythingPageListID int
	, @RetailerListID int
	, @BannerADClick bit
	, @CallStoreClick bit 
	, @BrowseWebsiteClick bit
	, @GetDirectionsClick bit
	, @AnythingPageClick bit

       
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
	
			--Update the Retailer Details tracking table.
			UPDATE HubCitiReportingDatabase..RetailerDetail SET BannerAdClick = CASE WHEN BannerAdClick = 0 THEN @BannerADClick ELSE 1 END
															   , CallStore = CASE WHEN CallStore = 0 THEN @CallStoreClick ELSE 1 END
															   , Website = CASE WHEN Website = 0 THEN @BrowseWebsiteClick ELSE 1 END
															   , GetDirection = CASE WHEN GetDirection = 0 THEN @GetDirectionsClick ELSE 1 END
			WHERE RetailerDetailID = @RetailerDetailsID
			
			--Update the Anything Page tracking table.
			UPDATE HubCitiReportingDatabase..AnythingPageList SET AnythingPageClick = CASE WHEN AnythingPageClick = 0 THEN @AnythingPageClick ELSE 1 END
			WHERE AnythingPageListID = @AnythingPageListID
		  
		  --Confirmation of Success.
		  SELECT @Status=0	
	   COMMIT TRANSACTION
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_HCUserTrackingRetailerSummaryClicks>.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
		    ROLLBACK TRANSACTION;
		    --Confirmation of failure.
		    SELECT @Status = 1			
		END;
		 
	END CATCH;
END;













































GO
