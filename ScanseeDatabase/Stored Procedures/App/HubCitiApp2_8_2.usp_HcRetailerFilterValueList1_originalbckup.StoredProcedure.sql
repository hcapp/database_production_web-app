USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailerFilterValueList1_originalbckup]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_HcRetailerFilterValueList]
Purpose                  : To display List of Retailer Filter Values.
Example                  : [usp_HcRetailerFilterValueList]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailerFilterValueList1_originalbckup]
(

      --Input Parameters     
        @UserID INT
	  , @BusinessCategoryID INT
	  , @HcHubCitiID INT
	  , @Latitude Decimal(18,6)
	  , @Longitude  Decimal(18,6)
	  , @Radius int
	  , @HcCityID Varchar(2000)
      , @HcMenuItemId Int 
	  , @HcBottomButtonId Int
	  , @SearchKey Varchar(200)
	  , @FilterValueName Varchar(200)
      
      --Output Parameters	 
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
			
			 DECLARE @PostalCode varchar(10)
			 DECLARE @UserOutOfRange bit  
			 DECLARE @DefaultPostalCode varchar(50) 
			 DECLARE @DistanceFromUser FLOAT
			 DECLARE @Status int
			 DECLARE @Length int

				
				SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
				SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
				WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
				WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
				ELSE @SearchKey END)
				
			   

			DECLARE @AdminFilterID int
			SELECT @AdminFilterID = AdminFilterID
			FROM AdminFilter
			WHERE FilterName = @FilterValueName

	        DECLARE @RegionAppFlag Bit
	    
			IF @Latitude IS NULL
			BEGIN
				 SELECT @Latitude = Latitude
					  , @Longitude = Longitude
				 FROM HcUser A
				 INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				 WHERE HcUserID = @UserID 
			END
			--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
			IF (@Latitude IS NULL) 
			BEGIN
				SELECT @Latitude = Latitude
						, @Longitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID
			END	 
			
			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
			EXEC [HubCitiApp8].[usp_HcUserHubCitiRangeCheck] @UserID, @HCHubCitiID, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

			 select @PostalCode = defaultpostalcode from hchubciti where HcHubCitiid = @HcHubCitiID  and @PostalCode is null
 
			--Derive the Latitude and Longitude in the absence of the input.
                IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
                BEGIN
                            --If the postal code is passed then derive the co ordinates.
                            IF @PostalCode IS NOT NULL
                            BEGIN
                                    SELECT @Latitude = Latitude
                                            , @Longitude = Longitude
                                    FROM GeoPosition 
                                    WHERE PostalCode = @PostalCode
                            END          
                            ELSE
                            BEGIN
                                    SELECT @Latitude = G.Latitude
                                            , @Longitude = G.Longitude
                                    FROM GeoPosition G
                                    INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                    WHERE U.HcUserID = @UserID 
                            END                                                                               
                END    
                             

			IF(SELECT 1 from HcHubCiti H
			   INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
				                       AND H.HcHubCitiID =@HCHubCitiID AND AL.HcAppListName ='RegionApp')>0
				BEGIN
						SET @RegionAppFlag =1    
						
				END
			ELSE
				BEGIN
						SET @RegionAppFlag =0
					                                   
				END


			--To Fetch region app associated citylist	
			CREATE TABLE #CityList(CityID Int,CityName Varchar(200))		  
			INSERT INTO #CityList(CityID,CityName)
					
			SELECT DISTINCT C.HcCityID 
					,C.CityName					
			FROM HcHubCiti  H
			INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID 
			INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID
			LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =C.HcCityID
			LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID	AND UP.HcHubcitiID =H.HchubcitiID 							        
			WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
			OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param )
			OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID =@UserID)						  
			OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID =@UserID)

			 IF @Radius IS NULL
			 BEGIN
				SELECT @Radius = LocaleRadius
                FROM HcUserPreference 
				WHERE HcUserID = @UserID
                           
                SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 
			 END
      
	         -- To display List of Filter Values Associated to Hubciti.

			 	CREATE TABLE #BusinessCategory(BusinessCategoryID int , HcBusinessSubCategoryID int)
					CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
					--CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100))
					CREATE TABLE #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)

			   IF (@HcMenuItemId IS NOT NULL)
						BEGIN 
                            INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,HcBusinessSubCategoryID               
							FROM HcMenuFindRetailerBusinessCategories F
							WHERE F.BusinessCategoryid = @BusinessCategoryID AND HcMenuItemID = @HcMenuItemId
						END
					ELSE IF (@HcBottomButtonID IS NOT NULL)
					BEGIN
		
							INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,HcBusinessSubCategoryID               
							FROM HcBottomButtonFindRetailerBusinessCategories F  
							WHERE F.BusinessCategoryid = @BusinessCategoryID AND HcBottomButonID = @HcBottomButtonID
					END

					                      
                                     
					SELECT DISTINCT BusinessCategoryID  INTO #SubCategorytype  FROM HcBusinessSubCategorytype  WITH(NOLOCK) WHERE BusinessCategoryID=@BusinessCategoryID

					SELECT DISTINCT BusinessCategoryID INTO #NONSUB FROM BusinessCategory WITH(NOLOCK)   WHERE BusinessCategoryID=@BusinessCategoryID  
					EXCEPT
					SELECT DISTINCT BusinessCategoryID  FROM #SubCategorytype WITH(NOLOCK)
					        
                    SELECT DISTINCT * INTO #HcMenuFindRetailerBusinessCategories 
					FROM HcMenuFindRetailerBusinessCategories  WHERE  BusinessCategoryID=@BusinessCategoryID
					AND HcBusinessSubCategoryID IS NOT NULL
					AND HcMenuItemID=@HcMenuItemId

					SELECT DISTINCT * INTO #HcMenuFindRetailerBusinessCategories1 
					FROM HcMenuFindRetailerBusinessCategories  WHERE  BusinessCategoryID=@BusinessCategoryID
					AND HcBusinessSubCategoryID IS  NULL
					AND HcMenuItemID=@HcMenuItemId

					SELECT DISTINCT * INTO #HcBottomButtonFindRetailerBusinessCategories 
					from HcBottomButtonFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
					AND HcBusinessSubCategoryID IS NOT NULL
					and HcBottomButonID=@HcBottomButtonID

					SELECT DISTINCT * INTO #HcBottomButtonFindRetailerBusinessCategories1 
					from HcBottomButtonFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
					AND HcBusinessSubCategoryID IS  NULL
					and HcBottomButonID=@HcBottomButtonID

					SELECT DISTINCT * INTO #RetailerBusinessCategory2  FROM RetailerBusinessCategory WHERE  BusinessCategoryID=@BusinessCategoryID

					CREATE  INDEX  ix_nn  on #HcMenuFindRetailerBusinessCategories(BusinessCategoryID,
					HcBusinessSubCategoryID)

					CREATE clustered INDEX  ix_nn1  on #HcMenuFindRetailerBusinessCategories(HcMenuItemID)

                    IF EXISTS(SELECT  1 FROM #SubCategorytype)
                    BEGIN
                            IF @HcMenuItemId  is not null
									 BEGIN

										 INSERT  INTO #RetailerBusinessCategory1
										 SELECT DISTINCT  RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
										 FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										 INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
										 ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										 AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
										 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										 WHERE HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID

										 INSERT  INTO #RetailerBusinessCategory1
										 SELECT  DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
										 FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										 INNER JOIN #HcMenuFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
										 ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										 WHERE HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID
									 END
							ELSE IF @HcBottomButtonID  is not null
								BEGIN
								INSERT  INTO #RetailerBusinessCategory1
								SELECT  distinct RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
								FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
								INNER JOIN #HcBottomButtonFindRetailerBusinessCategories HCB WITH(NOLOCK)
								ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
								INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
								inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
								where HCB.HcBottomButonID=@HcBottomButtonID and RBC.BusinessCategoryID=@BusinessCategoryID

								INSERT  INTO #RetailerBusinessCategory1
								SELECT  distinct RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
								FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
								INNER JOIN #HcBottomButtonFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
								ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
								inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
								where HCB.HcBottomButonID=@HcBottomButtonID and RBC.BusinessCategoryID=@BusinessCategoryID
								END

								ELSE
								BEGIN
									SELECT DISTINCT BusinessCategoryID INTO #BusinessCategory1 FROM #BusinessCategory

									INSERT  INTO #RetailerBusinessCategory1
									SELECT  DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
									FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									INNER JOIN #BusinessCategory1 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									WHERE RBC.BusinessCategoryID=@BusinessCategoryID
								END
					END
					ELSE 
					BEGIN
							SELECT DISTINCT BusinessCategoryID INTO #BusinessCategory2 FROM #BusinessCategory

							INSERT  INTO #RetailerBusinessCategory1
							SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
							FROM RetailerBusinessCategory RBC
							INNER JOIN #BusinessCategory2 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
							INNER JOIN #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID
							AND RBC.BusinessCategoryID=@BusinessCategoryID

					END

				    SELECT  #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1 


			 SELECT DISTINCT AdminFilterValueID 
						   ,FilterValueName 
						   ,Distance 
			 INTO #Temp
			 FROM
			 (
			 SELECT DISTINCT A.AdminFilterValueID 
					   ,A.FilterValueName 
					   ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
			 FROM HcRetailerAssociation R			
			 INNER JOIN RetailerFilterAssociation RF ON R.RetailLocationID =RF.RetailLocationID AND HcHubCitiID =@HcHubCitiID AND R.Associated =1 AND RF.BusinessCategoryID =@BusinessCategoryID
			 INNER JOIN RetailLocation RL ON RL.RetailLocationID =R.RetailLocationID AND RL.Active=1
			 INNER JOIN Retailer RR ON RR.RetailID=RL.RetailID	AND RR.RetailerActive=1	
			 inner join 	#RetailerBusinessCategory1  on #RetailerBusinessCategory1.RetailerID=RR.RetailID
			 INNER JOIN AdminFilter AF ON AF.AdminFilterID =RF.AdminFilterID 
			 INNER JOIN AdminFilterCategoryAssociation ACA ON ACA.AdminFilterID =AF.AdminFilterID AND ACA.BusinessCategoryID =@BusinessCategoryID
			 LEFT JOIN RetailerKeywords RK ON RK.RetailID =RR.RetailID
			 INNER JOIN AdminFilterValueAssociation AFV ON AFV.AdminFilterID =AF.AdminFilterID AND RF.AdminFilterValueID=AFV.AdminFilterValueID 
			 INNER JOIN AdminFilterValue A ON A.AdminFilterValueID =AFV.AdminFilterValueID 
			 LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode
			 INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityName =RL.City) OR (@RegionAppFlag =0) 		 
			  WHERE AF.AdminFilterID = @AdminFilterID
			  AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND RR.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
			  )A

			  SELECT DISTINCT AdminFilterValueID fValueId
						,FilterValueName	fValueName
			  FROM #Temp
			  WHERE Distance <= @Radius
			  ORDER BY FilterValueName 
			   

      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_HcRetailerFilterValueList.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                                
                  
            END;
            
      END CATCH;
END;




























GO
