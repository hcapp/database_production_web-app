USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiUpdateDeviceDetailsMenuDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_3_3].[[usp_HcHubCitiUpdateDeviceDetailsMenuDisplay]]
Purpose					: To update device details for a user
Example					: [HubCitiApp2_3_3].[[usp_HcHubCitiUpdateDeviceDetailsMenuDisplay]]


History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			01/14/2016	    Sagar Byali		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiUpdateDeviceDetailsMenuDisplay]

(
    --Input variable.

      @HubCitiID INT
    , @HcUserID Int
    , @RequestPlatformtype varchar(100)        
	, @DeviceID varchar(100)
	, @PlatformOSVersion varchar(100)
    , @AppVersion varchar(100)
    , @PrimaryDevice INT
     
	--Output Variable 
	--, @PrimaryDeviceFlag int output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY


		BEGIN	

				--If the DeviceID is already registered with the user check if it is primary device for the User and alert him if he wants to make it as primary device.	
				IF EXISTS (SELECT 1 FROM HcUserDeviceAppVersion WHERE HcUserID = @HcUserID  AND DeviceId=@DeviceID)					
				BEGIN


						UPDATE HcUserDeviceAppVersion 
						SET PlatformOSVersion = @PlatformOSVersion, DateModified = GETDATE()
						WHERE DeviceID = @DeviceID AND HcUserID = @HcUserID
				END

					
				--If the device is not registered to the user. 
				ELSE IF NOT EXISTS (SELECT 1 FROM HcUserDeviceAppVersion WHERE DeviceId=@DeviceID)						
				BEGIN

					INSERT INTO HcUserDeviceAppVersion(HcUserID, DeviceID, AppVersion, DateCreated, PrimaryDevice,HcHubCitiID,PlatformOSVersion)
					VALUES(@HcUserID, @DeviceID, @AppVersion, GETDATE(), 1, @HubCitiID, @PlatformOSVersion)
				END

	

			--Confirmation of Success
			SELECT @Status = 0
			
	  END



	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiMenuDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;































GO
