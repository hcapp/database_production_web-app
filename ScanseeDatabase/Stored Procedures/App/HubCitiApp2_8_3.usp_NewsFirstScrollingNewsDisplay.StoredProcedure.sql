USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_NewsFirstScrollingNewsDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_5].[usp_NewsFirstScrollingNewsDisplay]
Purpose					: To display news as per Scrolling templete
Example					: [HubCitiApp2_5].[usp_NewsFirstScrollingNewsDisplay]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26 May 2016		Sagar Byali		Initial Version
1.1			12/15/2016		 Bindu T A		Back Button Changes
---------------------------------------------------------------
*/

CREATE PROCEDURE[HubCitiApp2_8_3].[usp_NewsFirstScrollingNewsDisplay]
(	
	  @CategoryType varchar(500)
	, @HubCitiID int 
	, @LowerLimit INT
	, @ScreenName VARCHAR(1000)
	, @SearchKey VARCHAR(1000)
	, @HcUserID INT
	, @DateCheck varchar(1000)		
	, @isSideNavigation int
	, @LinkID int
	, @Level int
			
	--Output Variable 
	, @bannerImg VARCHAR(1000) Output
	, @weatherurl VARCHAR(1000) Output
	, @homeImgPath VARCHAR(1000) Output
	, @bkImgPath VARCHAR(1000) Output
	, @backButtonColor VARCHAR(1000) Output
	, @titleBkGrdColor  VARCHAR(1000) Output
	, @titleTxtColor   VARCHAR(1000) Output
	, @MaxCnt int Output
	, @NxtPageFlag bit Output
	, @TemplateChanged int output
	, @TemplateName varchar(100) output
	, @ModifiedDate varchar(100) output
	, @SubPage varchar(100) output

	, @HamburgerImagePath varchar(1000) output
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY



	--@isSideNavigation = 0 ---Scrolling news
	--@isSideNavigation = 1 ---Side Navigation
	--@isSideNavigation = 2 ---Search News/ news details
	--@isSideNavigation = 3 ---Sub Category news


select distinct c.NewsSubCategoryID
into #Ignore
from NewsFirstSettings N
inner join NewsFirstSubCategorySettings C on c.NewsCategoryID = C.NewsCategoryID and c.HcHubCitiID = n.HcHubCitiID and c.HcHubCitiID = @HubCitiID 
and ltrim(rtrim(c.NewsFirstSubCategoryURL)) = ltrim(rtrim(n.NewsFeedURL))


--To set Weather URL, Banner Image & thumbnailposition---------------START-----------------------------------------------
		declare @NewsImage varchar(max),@newscategoryimage varchar(max),@thumbnailposition varchar(10),@master varchar(100)

		SELECT @NewsImage = ScreenContent + CONVERT(VARCHAR,@HubCitiID)+ '/'
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		DECLARE @DefaultHamburgerIcon VARCHAR(1000)
		select @DefaultHamburgerIcon = ScreenContent +  'images/hamburger.png' 
		from AppConfiguration 
		where ScreenName = 'Base_URL'

		SELECT    @newscategoryimage = @NewsImage + NewsCategoryImage
				, @bannerImg = @NewsImage + NewsBannerImage
				, @weatherurl =  WeatherURL 
				, @thumbnailposition = NewsThumbnailPosition
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID 
			
			--To Set weather URL
			IF @isSideNavigation <> 0
			SET @weatherurl = null

			-- To set Main menu ID
			IF @Level = 1 
			BEGIN
					declare @HcMenuID1 int
					select  @HcMenuID1 = HcMenuID 
					FROM Hcmenu 
					where Level = 1 
					AND HcHubCitiID = @HubCitiID --AND IsAssociateNews = 1
			END
			ELSE
			BEGIN
					set  @HcMenuID1 = @LinkID
			END

		SET @backButtonColor = '#ffffff'
		
		SELECT @backButtonColor =ISNULL(S.backbuttoncolor,'#ffffff')
		FROM NewsFirstSettings S
		LEFT JOIN NewsFirstCategory  N ON N.NewsCategoryID= S.NewsCategoryID
		LEFT JOIN NewsFirstSubCategorySettings C ON S.NewsCategoryID = C.NewsCategoryID AND N.NewsCategoryID = C.NewsCategoryID AND C.HcHubCitiID = @HubCitiID
		LEFT JOIN NewsFirstSubCategory CC ON CC.NewsSubCategoryID = C.NewsSubCategoryID 
		WHERE S.HcHubCitiID = @HubCitiID AND (N.NewsCategoryName = @CategoryType OR CC.NewsSubCategoryName = @CategoryType)
	
--To set Weather URL, Banner Image & thumbnailposition---------------END-----------------------------------------------


------------- Custom Navigation Bar changes -----START--------------------------
			DECLARE @ServerConfig VARCHAR(500),@Config varchar(1000)

			

			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT @ServerConfig = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType= 'Hubciti Media Server Configuration'
			
			SELECT	@homeImgPath = 	CASE WHEN HU.homeIconName IS NULL THEN @ServerConfig+'customhome.png' ELSE  @Config +CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName END
					 ,@bkImgPath = 	CASE WHEN HU.backButtonIconName IS NULL THEN @ServerConfig+'customback.png' ELSE @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName END
					 ,@titleBkGrdColor =  ISNULL( HU.backGroundColor ,'#000000')
					 ,@titleTxtColor =  ISNULL(HU.titleColor ,'#ffffff')
					 ,@HamburgerImagePath = ISNULL(@NewsImage + NewsHamburgerIcon,@DefaultHamburgerIcon)
				FROM HcHubCiti HC
				LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
				LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
				WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					 
-------------------- Custom Navigation Bar changes - END----------------------------------------------------------------------------------------

-------------------Pagination------------ START------------------------------------------------------------------------------------------------------------
			DECLARE @UpperLimit VARCHAR(1000)
				  , @RowsPerPage varchar(1000)
				  , @Exists BIT
				  , @BookMarkExists BIT

			--To get the row count for pagination.  
            SELECT @UpperLimit = @LowerLimit + ScreenContent 
                 , @RowsPerPage = ScreenContent
            FROM AppConfiguration   
            WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1	
-------------------Pagination------------ END------------------------------------------------------------------------------------------------------------



---- -------------------------------------Sub page -------START-------------------------------------------- 
			IF (@isSideNavigation = 1 OR @isSideNavigation = 3)
			BEGIN
					SELECT @SubPage = P.NewsFirstSubPageDisplayName 
					FROM NewsFirstSettings S
					INNER JOIN NewsFirstSubPage P ON P.NewsFirstSubPageID = S.NewsFirstSubPageID
					LEFT JOIN NewsFirstCategory N ON N.NewsCategoryID = S.NewsCategoryID
					LEFT JOIN NewsFirstSubCategoryType NN ON NN.NewsCategoryID = N.NewsCategoryID
					LEFT JOIN NewsFirstSubCategory CC ON CC.NewsSubCategoryTypeID = NN.NewsSubCategoryTypeID 
					WHERE HcHubCitiID = @HubCitiID AND (NewsCategoryDisplayValue = @CategoryType OR CC.NewsSubCategoryDisplayValue = @CategoryType)
			END
-- -------------------------------------Sub page -------END------------------------------------------------- 


-----------------------------------To Collect News--------------START------------------------------------------



			SELECT  Newstype
					,HcHubCitiID
				    ,max(datecreated) as MaxDate
            INTO #2DaysNews 
            FROM RssNewsFirstFeedNews  where HcHubCitiID=@HubCitiID
            GROUP BY Newstype,HcHubCitiID

	
		    SELECT R.* 
			INTO #HubCitiNewss
			FROM #2DaysNews T
			INNER JOIN  RssNewsFirstFeedNews R   on T.Newstype=R.Newstype 
			where @HubCitiID=R.HcHubCitiID AND DATEDIFF(Day,R.DateCreated,T.MaxDate) <= 15
			
			--To search in all the news
			select * into #SearchHubCitiNewss  from #HubCitiNewss

			--To collect news based on side navigation input
			SELECT  *
			INTO #HubCitiNews
			FROM #HubCitiNewss
			WHERE HcHubCitiID = @HubCitiID AND (NewsType = @CategoryType AND @isSideNavigation <> 3)
			union
			SELECT  *
			FROM #HubCitiNewss
			WHERE HcHubCitiID = @HubCitiID AND (subcategory = @CategoryType AND @isSideNavigation = 3)

			create nonclustered index NCI_publisheddate ON #HubCitiNews(publisheddate)
			create nonclustered index NCI_newstype ON #HubCitiNews(newstype)

			--select  *from #HubCitiNews where NewsType = 'sports'

			----To set todays date to null			
			--update #HubCitiNews
			--set PublishedDate = null
			--where left(replace(getdate(),'  ,',','),11) = replace(left(replace(publisheddate,'  ,',','),12),',','')

			ALTER TABLE #HubCitiNews ADD NewsCatID INT
			ALTER TABLE #HubCitiNews ADD NewsSubCatID INT
			CREATE CLUSTERED INDEX IX_TestTable_TestCol1 ON #HubCitiNews (hcHubCitiID);

			UPDATE T
			SET NewsCatID = NewsCategoryID  
			FROM #HubCitiNews T
			INNER JOIN NewsFirstCategory N ON N.NewsCategoryDisplayValue=T.NewsType AND active =1

			--select  *from #HubCitiNews where NewsType = 'sports'

			UPDATE T
			SET NewsSubCatID = NewssubCategoryID  
			FROM #HubCitiNews T
			INNER JOIN NewsFirstSubCategory N ON N.NewsSubCategoryDisplayValue=T.subcategory 

			CREATE TABLE #News(RssNewsFirstFeedNewsID INT
								,NewsType VARCHAR(100)
								,NewsCategoryColor VARCHAR(10)
								,Title VARCHAR(max)
								,ImagePath VARCHAR(max)
								,ShortDescription  VARCHAR(max)
								,LongDescription VARCHAR(max)
								,Link VARCHAR(500)
								,PublishedDate DATETIME
							--	,PubStrtDate DATETIME
								,PublishedTime DATETIME		
								,Classification VARCHAR(2000)
								,section VARCHAR(1000)
								,adcopy VARCHAR(1000)
								,Author VARCHAR(250)		
								,SubCategory VARCHAR(100)
								,VideoLink VARCHAR(max)
								,thumbnail VARCHAR(max)
								,NewsCategoryFontColor varchar(10))


-----------------------------------To Collect News--------------END------------------------------------------






--------------------To display Searched News-------------------START----------------------------------------------
		  IF @isSideNavigation = 2
		  BEGIN

		
				DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

				SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
						WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
						WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
						ELSE @SearchKey END)

;with cte as(
				SELECT RN = ROW_NUMBER () over (PARTITION BY Title order by Title)
				
					,RssNewsFirstFeedNewsID itemId
					,NewsType feedType
					,Title title
					,case when ImagePath is null or imagepath = ' ' then @newscategoryimage else ImagePath end image
					,ShortDescription sDesc
					,LongDescription lDesc
					,Link = NULL
					,date =  replace(PublishedDate,'  ,',',')
											
					,time = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
					,Classification = NULL
					,NULL section
					,NULL adcopy
					,author 
					,NULL subcategory
					,NULL VideoLink
					,null thumbnail
					,NULL NewsCategoryFontColor
				
				FROM #SearchHubCitiNewss
				WHERE ((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (Title LIKE '%'+@SearchKey+'%' OR ShortDescription = '%'+@SearchKey+'%' OR LongDescription = '%'+@SearchKey+'%' OR author = '%'+@SearchKey+'%' )) and  Title is not null
				AND HcHubCitiID= @HubCitiID

				UNION

				SELECT DISTINCT  RNN = ROW_NUMBER () over (PARTITION BY Title order by Title )
				    ,RssNewsFirstFeedNewsID itemId
					,NewsType feedType
					,Title title
					,case when ImagePath is null or imagepath = ' ' then @newscategoryimage else ImagePath end image
					,ShortDescription sDesc
					,LongDescription lDesc
					,Link = NULL
					,date =  replace(PublishedDate,'  ,',',')
				
					,time = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
					,Classification = NULL
					,NULL section
					,NULL adcopy
					,author 
					,NULL subcategory
					,NULL VideoLink
					,null thumbnail
					,NULL NewsCategoryFontColor
				FROM #SearchHubCitiNewss
				WHERE ((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (NewsType LIKE '%'+@SearchKey+'%'))  and  Title is not null
				AND HcHubCitiID= @HubCitiID  
				)   
				SELECT * INTO #SearchNews from CTE

		
				
				SELECT *
				INTO #SearchNewsPagination1
				FROM #SearchNews RFN
				WHERE RN = 1

				

				SELECT rowNum = ROW_NUMBER() over(order by itemId)
					  ,itemId
					  ,title
					  ,feedType
					  ,image
					  ,sDesc
					  ,lDesc
					  ,Link
					  ,date
					  ,time
					  ,Classification
					  ,section
					  ,adcopy
					  ,author
					  ,subcategory
					  ,VideoLink
					  ,thumbnail
					  ,NewsCategoryFontColor
				INTO #SSearchNews
				fROM #SearchNewsPagination1
								
				--To Set Max count for Pagination
				SELECT @MaxCnt = count(1) FROM #SSearchNews

				SELECT *
				FROM #SSearchNews
				WHERE Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit  

				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END     
								   
			END 
--------------------To display Searched News-------------------END-----------------------------------------------------

		
------------------------To display Subcategory News -------------------START----------------------------------------------
			IF @isSideNavigation = 3
			
			BEGIN
			
						IF EXISTS(SELECT 1 FROM HcUserNewsFirstBookmarkedSubCategories WHERE HcUserID = @HcUserID AND HchubcitiID = @HubCitiID)
						BEGIN

						INSERT INTO #News
						SELECT  distinct RssNewsFirstFeedNewsID 
								,subcategory
								,NN.NewsCategoryColor
					
								,Title
								,ImagePath
								,ShortDescription 
								,LongDescription 
								,Link
								,PublishedDate --= CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
								,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
								,Classification 
								,section
								,adcopy
								,author 
								,subcategory
								,VideoLink
								,thumbnail
								,NewsCategoryFontColor
						FROM HcUser U
						INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.hcuserID = @hcuserid
						INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID   AND M.hcmenuid = @hcmenuid1
						INNER JOIN #HubCitiNews N ON N.HcHubCitiID = M.HchubcitiID 
						INNER JOIN NewsFirstsubcategorySettings NS ON NS.HcHubCitiID = M.HcHubCitiID AND NS.NewsSubCategoryID = N.NewsSubCatID  
						INNER JOIN NewsFirstSettings NN ON NN.NewsCategoryID = NS.NewsCategoryID AND NN.HcHubCitiID = M.HcHubCitiID 
						LEFT JOIN HcUserNewsFirstBookmarkedSubCategories BC ON BC.HcUserID = U.HcUserID AND BC.HchubcitiID = NS.HcHubCitiID AND BC.NewsSubCategoryID = NS.NewsSubCategoryID
						LEFT JOIN HcUserNewsFirstSideNavigationSubCategories CC ON CC.HcUserID = U.HcUserID AND CC.HchubcitiID = NS.HcHubCitiID AND CC.NewsSideNavigationSubCategoryID = NS.NewsSubCategoryID 
						WHERE M.HcHubCitiID = @HubCitiID and  Title is not null
						END

						ELSE

						BEGIN

						INSERT INTO #News
						SELECT distinct  RssNewsFirstFeedNewsID 
								,subcategory
								,NN.NewsCategoryColor
					
								,Title
								,ImagePath
								,ShortDescription 
								,LongDescription 
								,Link
								,PublishedDate --= CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
								--,PubStrtDate= convert(datetime,PublishedDate,105) 
								,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
					
								,Classification 
								,section
								,adcopy 
								,author 
								,subcategory
								,VideoLink
								,thumbnail
								,NewsCategoryFontColor
						FROM HcUser U
						INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcHubCitiID = @HubCitiID AND UD.HcUserID = @HcUserID
						INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID   AND HcMenuID = @HcMenuID1 AND M.HcHubCitiID = @HubCitiID
						INNER JOIN #HubCitiNews N ON N.HcHubCitiID = M.HchubcitiID
						INNER JOIN HcDefaultNewsFirstBookmarkedSubCategories BC ON BC.NewsSubCategoryID = N.NewsSubCatID AND BC.HchubcitiID = N.HcHubCitiID AND BC.HchubcitiID = @HubCitiID
						---INNER JOIN HcUserNewsFirstSideNavigationSubCategories ZZ ON ZZ.NewsSideNavigationSubCategoryID = BC.NewsSubCategoryID AND ZZ.HchubcitiID = N.HcHubCitiID AND zz.HchubcitiID = @HubCitiID
						LEFT JOIN NewsFirstSettings NN ON NN.NewsCategoryID = N.NewsCatID AND BC.HchubcitiID = M.HcHubCitiID AND NN.HcHubCitiID = @HubCitiID
						LEFT JOIN NewsFirstsubcategorySettings NS ON NS.HcHubCitiID = M.HcHubCitiID AND M.HcHubCitiID = NS.HcHubCitiID AND NS.NewsCategoryID = NN.NewsCategoryID AND NS.HcHubCitiID = @HubCitiID
						LEFT JOIN NewsFirstCategoryTempleteType TT ON TT.NewsCategoryTempleteTypeID = NN.NewsFirstCategoryTempleteTypeID
						where  Title is not null

						END
	
							SELECT   rowNum = ROW_NUMBER() OVER(ORDER BY CASE WHEN PublishedDate is null THEN 1  
												 WHEN PublishedDate is not null THEN 1 END, PublishedDate desc, CAST(PublishedTime AS time) desc )	
									,RssNewsFirstFeedNewsID itemId						   
									,NewsType feedType
									,NewsCategoryColor catColor
									,NewsCategoryFontColor CatTxtColor
									,isnull(@thumbnailposition,'Left') imgposition
									,Title title
									,case when ImagePath is null or ImagePath =' ' then @NewsCategoryImage else ImagePath end image
									,ShortDescription sDesc
									,LongDescription lDesc
									,Link 
									,PublishedDate --= CONVERT(VARCHAR(26), convert(date,convert(datetime,PublishedDate)), 109)
									,FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt') time
									,author
									,Classification 
									,section
									,adcopy
									,subcategory
									,VideoLink
									,thumbnail 
							INTO #SubPageNews1
							FROM #News RFN
							
								
							--To Set Max count for Pagination
							SELECT @MaxCnt = count(1) FROM #SubPageNews1



							
					--To set todays date to null			
			update #SubPageNews1
			set PublishedDate = null
			where left(replace(getdate(),'  ,',','),11) = replace(left(replace(publisheddate,'  ,',','),12),',','')

							SELECT  rowNum 
									,itemId						   
									,feedType
									,catColor
									,CatTxtColor
									,imgposition
									,title
									,image
									,sDesc
									,lDesc
									,Link 
									,date = replace(SUBSTRING(cast(PublishedDate as varchar(1000)),1,7) + ' , ' +SUBSTRING(cast(PublishedDate as varchar(1000)),8,4),'  ,',',')  
									,time
									,author
									,Classification 
									,section
									,adcopy
									,subcategory
									,VideoLink
									,thumbnail 
								
							FROM #SubPageNews1
							WHERE Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit  

							SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 


							SET @TemplateChanged = 0

			END
--------------------To display Sub category News-------------------END----------------------------------------------

--------------------To display side navigation News-------------------START----------------------------------------------
		if @isSideNavigation = 1
		BEGIN


			INSERT INTO #News
			SELECT DISTINCT RssNewsFirstFeedNewsID 
					,NewsType
					,NS.NewsCategoryColor
					
					,Title
					,ImagePath
					,ShortDescription 
					,LongDescription 
					,Link
					,PublishedDate
					,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
					,Classification 
					,section
					,adcopy 
					,author
					
					,subcategory
					,VideoLink
					,thumbnail
					,NewsCategoryFontColor
			FROM HcUser U
			INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
			INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID   AND M.HcMenuID = @HcMenuID1
			INNER JOIN NewsFirstSettings NS ON NS.HcHubCitiID = M.HcHubCitiID 
			INNER JOIN #HubCitiNews N ON N.HcHubCitiID = NS.HchubcitiID AND NS.NewsCategoryID = N.newscatid
			LEFT JOIN #ignore i on i.NewsSubCategoryID = n.NewsSubCatID
			WHERE M.HcHubCitiID = @HubCitiID AND N.NewsType = @CategoryType and  Title is not null and i.NewsSubCategoryID is null
			
			  

	
			--To Set Max count for Pagination
			SELECT @MaxCnt = count(1) FROM #News

			SELECT   rowNum = ROW_NUMBER() over(partition by NewsType ORDER BY CASE WHEN PublishedDate is null THEN 1  
						  WHEN PublishedDate is not null THEN 1 END, PublishedDate desc, CAST(PublishedTime AS time) desc ) 
					,RssNewsFirstFeedNewsID itemId
					,NewsType catName
					,NewsCategoryColor catColor
					,NewsCategoryFontColor catTxtColor
					,@thumbnailposition  imgposition
					,Title title
					,case when ImagePath is null or ImagePath =' ' then @NewsCategoryImage else ImagePath end image
					,ShortDescription sDesc
					,LongDescription lDesc
					,Link link
					,PublishedDate 
					,FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt') time
					,Classification 
					,section
					,adcopy
					,author
					,subcategory
					,VideoLink
					,thumbnail 

			INTO #NewsDisplayy
			FROM #News 
			
			
			IF @isSideNavigation = 1
			BEGIN


			
					--To set todays date to null			
			update #NewsDisplayy
			set PublishedDate = null
			where left(replace(getdate(),'  ,',','),11) = replace(left(replace(publisheddate,'  ,',','),12),',','')

					SELECT rowNum 
					,itemId
					,catName
					,catColor
					, catTxtColor
					,imgposition
					,title
					,image
					, sDesc
					, lDesc
					, link
					,date = replace(SUBSTRING(cast(PublishedDate as varchar(1000)),1,7) + ' , ' +SUBSTRING(cast(PublishedDate as varchar(1000)),8,4),'  ,',',')  
							
					,time
					,Classification 
					,section
					,adcopy
					,author
					,subcategory
					,VideoLink
					,thumbnail 
					
					FROM #NewsDisplayy
					WHERE Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit         
					ORDER BY Rownum  asc 

					SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END      
			end
			END
			
--------------------To display side navigation News-------------------END----------------------------------------------

		 
----------------------------------Istemplete changed-------START------------------------------------------------------


		DECLARE @TempID int, @ExistTempID int, @NewsFlag int
		

	  SET @ModifiedDate=replace(@ModifiedDate,'  ',' ')
		SET @DateCheck=replace(@DateCheck,'  ',' ')	
		--select @DateCheck
		set  @ModifiedDate=convert(datetime,@ModifiedDate)
		set  @DateCheck=convert(datetime,@DateCheck)
	
		--SELECT @DateCheck = cast(@DateCheck as datetime)
		 
		 if  isnull(@Level,1)<>1
         BEGIN
		    if not exists(SELECT 1 FROM HcNewsFirstTempFlag H where HcHubCitiID = @HubCitiID  and isnull(Level,1)=0 and linkid=@LinkID) and isnull(@Level,1)<>1 
			begin
				 insert into HcNewsFirstTempFlag(HcHubCitiID,level,linkid) 
				 select @HubCitiID,0,@LinkID
			end
		 END
		  if not exists(SELECT 1 FROM HcNewsFirstTempFlag H where HcHubCitiID = @HubCitiID  and isnull(Level,1)=1)  and isnull(@Level,1)=1 
         BEGIN
				 insert into HcNewsFirstTempFlag(HcHubCitiID,level) 
				 select @HubCitiID,1
		 END

			 update   HcNewsFirstTempFlag set level=1  where level is null and HcHubCitiID = @HubCitiID

		 if isnull(@Level,1)=1
		 begin
		  SELECT top 1 @tempid = HcTemplateID FROM HcMenu H where HcHubCitiID = @HubCitiID and Level=1

				 if exists(SELECT 1 FROM HcMenu H where HcHubCitiID = @HubCitiID and IsAssociateNews=1 and Level=1)
				 BEGIN 
						 SET @NewsFlag=1
						 update HcNewsFirstTempFlag set NewsFlag=1,level=1,linkid=@LinkID where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=1
				 END
				 else
				 BEGIN
						SET @NewsFlag=0
						update HcNewsFirstTempFlag set NewsFlag=0,level=1,linkid=@LinkID where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=1
				 END
		 end
		 else
		    BEGIN
			 SELECT top 1 @tempid = HcTemplateID FROM HcMenu H where HcHubCitiID = @HubCitiID and  HcMenuID=@LinkID
		      if exists(SELECT 1 FROM HcMenu H where HcHubCitiID = @HubCitiID and IsAssociateNews=1 and HcMenuID=@LinkID)
		  		 BEGIN 
						 SET @NewsFlag=1
						 update HcNewsFirstTempFlag set NewsFlag=1,level=0 where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=0
				 END
				 else
				 BEGIN
						SET @NewsFlag=0
						update HcNewsFirstTempFlag set NewsFlag=0,level=0 where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=0
				 END
            end

			 
		
		
		 update HcNewsFirstTempFlag  
		 set TemplateID=@tempid  
		 where TemplateID is null and HcHubCitiID=@HubCitiID
		 and isnull(level,1)=case when @Level=1 then '1' else 0 end
		 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 

		select top 1 @Existtempid = TemplateID  
		from HcNewsFirstTempFlag  
		where HcHubCitiID=@HubCitiID
		and isnull(level,1)=case when @Level=1 then '1' else 0 end
		 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 

	  IF ( @NEWSflag=0)
		begin
		
		--select 'a'

				update HCNewsFirstTempFlag 
				set NewsFlag=@NEWSflag,TemplateID=@tempid 
				where HcHubCitiID=@HubCitiID and level=case when @Level=1 then '1' else 0 end
				
				 SET @TemplateChanged = 1
				
		          if isnull(@Level,1)=1
					begin
							SELECT @TemplateName = isnull(T.TemplateName,0)
							,@ModifiedDate = convert(datetime,H.DateCreated)
							FROM HcTemplate  T 
							INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
							WHERE HcHubCitiID = @HubCitiID AND Level=1       
					end
					else
					begin
							SELECT @TemplateName = isnull(T.TemplateName,0)
							,@ModifiedDate = convert(datetime,H.DateCreated)
							FROM HcTemplate  T 
							INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
							WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

					end

				
				SELECT  @Status = 0
				 
				
	    end

		else
	    begin

	

		

			SET @LinkID =IIF(@LinkID IS NULL OR @LinkID=0 ,Null,@LinkID)
			DECLARE @MenuName varchar(1000), @HcMenuID int 
			
			SELECT @MenuName = MenuName
			FROM HcMenu
			WHERE HcMenuID = @LinkID AND ISNULL(@LinkID, 0) <> 0
			OR ISNULL(@LinkID, 0) = 0 AND Level = 1
-------------------------------Istemplete changed-------END------------------------------------------------------------------



------------Scrolling news display --------------------START-----------------------------------------------------------
			IF @isSideNavigation = 0
			BEGIN
			IF EXISTS(SELECT 1 FROM HcUserNewsFirstBookmarkedCategories WHERE HcUserID = @HcUserID AND HchubcitiID = @HubCitiID)
			
			BEGIN
			
			--select * from #ignore
			--select * from #HubCitiNews
			

			INSERT INTO #News
			SELECT DISTINCT RssNewsFirstFeedNewsID 
					,NewsType
					,NS.NewsCategoryColor
				--	,NewsThumbnailPosition 
					,Title
					,ImagePath
					,ShortDescription 
					,LongDescription 
					,Link
					,PublishedDate --= CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
					--,PubStrtDate= convert(datetime,PublishedDate,105) 
					,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
					,Classification 
					,section
					,adcopy 
					,author
					--,NewsBannerImage
					,subcategory
					,VideoLink
					,thumbnail
					,NewsCategoryFontColor
			FROM HcUser U
			INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
			INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID  AND M.IsAssociateNews = 1 AND M.HcMenuID = @HcMenuID1
			INNER JOIN NewsFirstSettings NS ON NS.HcHubCitiID = M.HcHubCitiID
			INNER JOIN HcUserNewsFirstBookmarkedCategories BC ON BC.HcUserID = U.HcUserID AND BC.HchubcitiID = NS.HcHubCitiID AND BC.NewsCategoryID = NS.NewsCategoryID 
			INNER JOIN #HubCitiNews N ON N.HcHubCitiID = BC.HchubcitiID AND N.newsCatid =NS.NewsCategoryID AND  N.newsCatid =BC.NewsCategoryID
			LEFT JOIN #ignore i on i.NewsSubCategoryID = n.NewsSubCatID 
			WHERE Title is not null  and i.NewsSubCategoryID is null 
			
			END

			ELSE

			BEGIN
			--select @HcUserID,@HcMenuID1

			--select 'mm'

			--select *  from #HubCitiNews where newstype = 'sports'
			

			INSERT INTO #News
			SELECT DISTINCT RssNewsFirstFeedNewsID 
					,NewsType
					,ISNULL(NN.NewsCategoryColor,'#fffff')
					--,NewsThumbNailPosition 
					,Title
					,ImagePath
					,ShortDescription 
					,LongDescription 
					,Link
					,PublishedDate --= CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
					--,PubStrtDate= convert(datetime,PublishedDate,105) 
					,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
					,Classification 
					,section
					,adcopy 
					,author
				--	,NewsBannerImage
					,subcategory
					,VideoLink
					,thumbnail
					,NewsCategoryFontColor
			FROM HcUser U
			INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
			INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID  AND M.IsAssociateNews = 1 AND HcMenuID = @HcMenuID1 
			INNER JOIN #HubCitiNews N ON N.HcHubCitiID = M.HchubcitiID
			LEFT JOIN #ignore i on i.NewsSubCategoryID = n.NewsSubCatID
			inner JOIN HcDefaultNewsFirstBookmarkedCategories BC ON BC.NewsCategoryID = N.NewsCatID AND BC.HchubcitiID = N.HcHubCitiID 
			inner JOIN NewsFirstSettings NN ON NN.NewsCategoryID = BC.NewsCategoryID AND BC.HchubcitiID = M.HcHubCitiID AND NN.NewsCategoryID = N.newscatid AND NN.HchubcitiID = @HubCitiID 
			LEFT JOIN NewsFirstCategoryTempleteType TT ON TT.NewsCategoryTempleteTypeID = NN.NewsFirstCategoryTempleteTypeID
			WHERE BC.HchubcitiID = @HubCitiID and  Title is not null and i.NewsSubCategoryID is null
			--order by itemId 
			END

			
			--To Set Max count for Pagination
			SELECT @MaxCnt = count(1) FROM #News

			SELECT   rowNum = ROW_NUMBER() over(partition by NewsType ORDER BY CASE WHEN PublishedDate is null THEN 1  
						  WHEN PublishedDate is not null THEN 1 END, PublishedDate desc, CAST(PublishedTime AS time) desc ) 
					,RssNewsFirstFeedNewsID itemId
					,NewsType catName
					,NewsCategoryColor catColor
					,NewsCategoryFontColor catTxtColor
					,isnull(@thumbnailposition,'Left')  imgposition
					,Title title
					,case when ImagePath is null or ImagePath =' ' then @NewsCategoryImage else ImagePath end image
					,ShortDescription sDesc
					,LongDescription lDesc
					,Link link
					,PublishedDate
					,FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt') time
					,Classification 
					,section
					,adcopy
					,author
					,subcategory
					,VideoLink
					,thumbnail 
			INTO #NewsDisplay
			FROM #News 
			

			
	
			--To display Scrolling News
			IF @isSideNavigation = 0 
			BEGIN

					--To set todays date to null			
			update #NewsDisplay
			set PublishedDate = null
			where left(replace(getdate(),'  ,',','),11) = replace(left(replace(publisheddate,'  ,',','),12),',','')



					SELECT rowNum 
					, itemId
					, catName
					, catColor
					, catTxtColor
					,  imgposition
					, title
					,image
					, sDesc
					, lDesc
					, link
							,date =replace(SUBSTRING(cast(PublishedDate as varchar(1000)),1,7) + ' , ' +SUBSTRING(cast(PublishedDate as varchar(1000)),8,4),'  ,',',')  
							,time,Classification 
					,section
					,adcopy
					,author
					,subcategory
					,VideoLink
					,thumbnail 
					FROM #NewsDisplay
					WHERE Rownum BETWEEN (@LowerLimit+1) AND @UpperLimit        
					--ORDER BY Rownum,itemId  

					SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END      
			END
------------Scrolling news display --------------------END-----------------------------------------------------------

--Temlete changed implementation-------------------------
		
if isnull(@Level,1)=1

begin
			IF EXISTS(SELECT 1 FROM HcMenu H
						INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
						WHERE left(convert(datetime,h.DateCreated),19)  = left(convert(datetime,@DateCheck),19) AND HcHubCitiID = @HubCitiID 
						and Level=1) or @DateCheck  is null
			BEGIN
						SELECT @TemplateChanged = 0 
			END
			ELSE
			BEGIN
					SELECT  @TemplateChanged = 1
            END

end
else

begin
			IF EXISTS(SELECT 1 FROM HcMenu H
						INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
						WHERE left(convert(datetime,h.DateCreated),19)  = left(convert(datetime,@DateCheck),19) AND HcHubCitiID = @HubCitiID 
						and h.HcMenuID=@LinkID) or @DateCheck  is null
			BEGIN
						SELECT @TemplateChanged = 0 
			END
			ELSE
			BEGIN
					SELECT  @TemplateChanged = 1
            END

end


		 


	if isnull(@Level,1)=1
	begin
			SELECT @TemplateName = isnull(T.TemplateName,0)
					,@ModifiedDate = convert(datetime,H.DateCreated)
			FROM HcTemplate  T 
			INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
			WHERE HcHubCitiID = @HubCitiID AND Level=1       
	end
	else
	begin
	SELECT @TemplateName = isnull(T.TemplateName,0)
					,@ModifiedDate = convert(datetime,H.DateCreated)
			FROM HcTemplate  T 
			INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
			WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

	end

	update HCNewsFirstTempFlag 
	set NewsFlag=@NEWSflag,TemplateID=@tempid ,linkid=@LinkID
	where HcHubCitiID=@HubCitiID and level=case when @Level=1 then '1' else 0 end
	and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end
			 
			SET @Status=0	
				

			ENd
	
	IF @DateCheck IS  NULL or @issidenavigation = 1 or @isSideNavigation = 3
		set @TemplateChanged = 0

	select top 1 @master=HcTemplateID  from HcTemplate  where  TemplateName='Scrolling News Template'
	if (@Existtempid<>@tempid  or @tempid<>@master )
			SET @TemplateChanged = 1


				IF  @issidenavigation = 1 or @isSideNavigation = 3
		set @TemplateChanged = 0
	
	END-----Is templete changed-----------   END------------------
	

			 

			 IF  @issidenavigation = 2 set @TemplateChanged = 0
		
		  
	  					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN

		throw;
			
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;










GO
