USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcSalesRetailersDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcFetchSalesListPagination  
Purpose     : To list sales in the given HubCiti.  
Example     : EXEC [Version2].usp_HcFetchSalesListPagination 35, 2, 0, 50  
History  
Version  Date			Author					Change Description  
---------------------------------------------------------------   
1.0		21st Jan 2014	Pavan Sharma K			Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcSalesRetailersDisplay]  
(  
   @UserID int  
 , @HubCitiID int  
 --, @CategoryID int  
 , @LowerLimit int  
 , @ScreenName varchar(50) 
 , @Latitude float
 , @Longitude float
 
 --User Tracking Inputs
 --, @MainMenuID int  


 --OutPut Variable  
 , @MaxCnt int output
 , @NxtPageFlag bit output  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
	BEGIN TRANSACTION
		--To get Media Server Configuration.  
		DECLARE @ManufConfig varchar(50)    
		SELECT @ManufConfig=ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'

		--To get the row count for pagination.  
		DECLARE @UpperLimit int   
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
		FROM AppConfiguration   
		WHERE ScreenName = @ScreenName   
		AND ConfigurationType = 'Pagination'  
		AND Active = 1  
		--DECLARE @MaxCnt int 
		
		IF @Latitude IS NULL AND @Longitude IS NULL
				BEGIN
					SELECT @Latitude = G.Latitude
						 , @Longitude = G.Longitude
					FROM HcUser H
					INNER JOIN GeoPosition G ON G.PostalCode = H.PostalCode
					WHERE HcUserID = @UserID
				END 

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive=1
		
		--To get list of Product available in the Retail Store.  
		SELECT  RL.RetailLocationID   
		, ProductID   
		INTO #PROD  
		FROM RetailLocationProduct  RLP
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND RL.HcCityID = H.HcCityID AND RL.StateID = H.StateID
		WHERE H.HcHubCitiID = @HubCitiID

		--Table Variable to store sale Product.   
		DECLARE @DiscountProduct TABLE (ProductId int, SalePrice money, DiscountAmount varchar(100),DiscountType varchar(10))  

		--To get Product on Deal.  
		INSERT INTO @DiscountProduct  
		SELECT RD.ProductID, rd.SalePrice, ((RD.Price - SalePrice) / RD.Price) * 100 ,'Deal'  
		FROM RetailLocationDeal RD  
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = RD.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND RL.HcCityID = H.HcCityID AND RL.StateID = H.StateID
		WHERE H.HcHubCitiID = @HubCitiID     
		AND GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)

		--To get Products which has Coupons on it.  
		INSERT INTO @DiscountProduct  
		SELECT CP.ProductID, c.CouponDiscountAmount,C.CouponDiscountPct ,'Coupon'  
		FROM Coupon C  
		INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID  
		INNER JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
		INNER JOIN #PROD p on p.ProductID = CP.ProductID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = CR.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND RL.HcCityID = H.HcCityID AND RL.StateID = H.StateID AND H.HcHubCitiID = @HubCitiID  
		LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID  
		WHERE GETDATE() BETWEEN C.CouponStartDate AND C.CouponExpireDate
		GROUP BY C.CouponID
				,NoOfCouponsToIssue
				,CP.ProductID
				,C.CouponDiscountAmount
				,C.CouponDiscountPct
		HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)  

		--To get Products which has Rebate on it.  
		INSERT INTO @DiscountProduct  
		SELECT RP.ProductID, R.RebateAmount,NULL ,'Rebate'  
		FROM Rebate R  
		INNER JOIN RebateProduct RP on RP.RebateID = R.RebateID 
		INNER JOIN RebateRetailer RR ON RR.RebateID = R.RebateID  
		INNER JOIN #PROD P ON P.ProductID = RP.ProductID   
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = RR.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND RL.HcCityID = H.HcCityID AND RL.StateID = H.StateID AND H.HcHubCitiID = @HubCitiID  
		WHERE GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate   


		--To get Products which has Loyalty on it.  
		INSERT INTO @DiscountProduct  
		select L.ProductID, L.LoyaltyDealDiscountAmount,L.LoyaltyDealDiscountPct ,'Loyalty'  
		from LoyaltyDeal L   
		INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = L.LoyaltyDealID
		INNER JOIN #PROD P ON P.ProductID = LDP.ProductID   
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = L.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND RL.HcCityID = H.HcCityID AND RL.StateID = H.StateID AND H.HcHubCitiID = @HubCitiID  
		WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)


		--To get Hot Deal Products.  
		INSERT INTO @DiscountProduct  
		SELECT H.ProductID, ph.SalePrice,PH.HotDealDiscountPct,'Hot Deals'  
		from HotDealProduct h   
		INNER JOIN ProductHotDealRetailLocation PR ON PR.ProductHotDealID=h.ProductHotDealID  
		INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = H.ProductHotDealID   
		INNER JOIN #PROD P ON P.ProductID = H.ProductID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = PR.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND RL.HcCityID = H.HcCityID AND RL.StateID = H.StateID AND HL.HcHubCitiID = @HubCitiID    
		WHERE GETDATE() BETWEEN ISNULL(PH.HotDealStartDate, GETDATE() -1) AND ISNULL(PH.HotDealEndDate, GETDATE() + 1)   


		--To get the MIN sale price of a product.  
		SELECT ProductID, MIN(SalePrice) AS SalePrice  
		INTO #SaleProd  
		FROM @DiscountProduct P  
		GROUP BY ProductId  

		--And to check whether highest sale price product belongs to Deals. if yes enabling the flag, to get the prod image from RetailLocationDeal table  
		SELECT DP.ProductId, DP.SalePrice,DP.DiscountAmount ,Flag = 1  
		INTO #Deal  
		FROM @DiscountProduct DP  
		INNER JOIN #SaleProd P ON DP.ProductId = P.ProductId AND DP.SalePrice = P.SalePrice   
		WHERE DP.DiscountType = 'Deal'  

		SELECT ProductId, SalePrice,DiscountAmount ,Flag  
		INTO #Product  
		FROM ( --To get highest deal product  
		SELECT ProductId, SalePrice,DiscountAmount ,Flag   
		FROM #Deal   
		UNION ALL   
		--To get highest non deal (ie coupon, hotdeals etc) product    
		SELECT S.ProductId, S.SalePrice,D.DiscountAmount ,Flag = 1 
		FROM #SaleProd S  
		INNER JOIN @DiscountProduct D ON D.ProductId=S.ProductId AND D.SalePrice=S.SalePrice  
		WHERE S.ProductId NOT IN (SELECT ProductID FROM #Deal)   
		) Details  



		--;WITH ProductList  
		--AS  
		--(  
		SELECT DISTINCT P.ProductID, Row_Num = IDENTITY(INT,1,1)  
		, P.ProductName   
		, P.ManufacturerID    
		, P.ModelNumber   
		, P.ProductLongDescription   
		, P.ProductExpirationDate   
		, ProductImagePath =CASE WHEN D.ThumbNailProductImagePath IS NULL 
									THEN CASE WHEN P.WebsiteSourceFlag = 1 
										THEN CASE WHEN P.ProductImagePath LIKE '%http%' THEN P.ProductImagePath 
									ELSE @ManufConfig + CAST(P.ManufacturerID AS VARCHAR(10))+'/'+P.ProductImagePath END
								 ELSE P.ProductImagePath END
							ELSE D.ThumbNailProductImagePath END  
		, P.SuggestedRetailPrice   
		, P.Weight   
		, P.WeightUnits  
		, PR.SalePrice   
		, D.Price  
		, DiscountAmount
		--, @RetailID RetailID   
		, RetailLocationDealID  
		, R.RetailID
		, R.RetailName
		, RL.RetailLocationID
		, RL.Address1
		, RL.City
		, RL.State
		, RL.PostalCode
		, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  

		
		INTO #ProductList    
		FROM #Product PR  
		--INNER JOIN (  
		--   select MIN(productid) productid,  
		--   productname,ModelNumber, ProductLongDescription,  
		--   ManufacturerID    
		--   , ProductExpirationDate  
		--   , ProductImagePath  = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
		--																		THEN @ManufConfig
		--																		+CONVERT(VARCHAR(30),ManufacturerID)+'/'
		--																		+ProductImagePath ELSE ProductImagePath 
		--																  END   
		--                     ELSE ProductImagePath END  
		--   , SuggestedRetailPrice   
		--   , Weight   
		--   , WeightUnits   
		--   , ScanTypeID  
		--   from Product   
		--   group by ProductName,ScanCode,ModelNumber          
		--   , ProductLongDescription   
		--   , ManufacturerID    
		--   , ProductExpirationDate   
		--   , ProductImagePath   
		--   , SuggestedRetailPrice   
		--   , Weight   
		--   , WeightUnits   
		--   , ScanTypeID 
		--   , WebsiteSourceFlag           
		--   )   
		INNER JOIN Product P ON P.ProductID = PR.ProductID  
		LEFT JOIN RetailLocationDeal D ON D.ProductID = P.ProductID AND PR.Flag = 1 
		LEFT JOIN RetailLocation RL ON RL.RetailLocationID = D.RetailLocationID AND RL.Active=1
		LEFT JOIN Retailer R ON R.RetailID = RL.RetailID AND R.RetailerActive=1
		LEFT JOIN GeoPosition G ON G.PostalCode = R.PostalCode
		LEFT JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND H.HcCityID = RL.HcCityID AND H.StateID = RL.StateID
		LEFT JOIN #DuplicateRet DR ON DR.DuplicateRetailerID = R.RetailID  
		WHERE (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
		AND H.HcHubCitiID = @HubCitiID AND DR.DuplicateRetailerID IS NULL

		--)  
		--To capture max row number.  
		SELECT @MaxCnt = MAX(Row_Num) FROM #ProductList  

		print @MaxCnt  
		--this flag is a indicator to enable "More" button in the UI.   
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  

		SELECT Row_Num  rowNumber 
			, RetailID retailerId
			, RetailName retailerName
			, RetailLocationID retailLocationId
			, PL.Address1 retaileraddress1
			, PL.City
			, PL.State
			, PL.PostalCode
			, Distance distance
		--, ProductID productId  
		--, ProductName  productName  
		--, ManufName  manufacturersName  
		--, ModelNumber modelNumber  
		--, ProductLongDescription productDescription  
		--, ProductExpirationDate productExpDate  
		--, ProductImagePath imagePath  
		--, SuggestedRetailPrice suggestedRetailPrice  
		--, Weight productWeight  
		--, WeightUnits weightUnits  
		--, SalePrice salePrice  
		--, Price regularPrice  
		--, DiscountAmount discount
		----, RetailID   
		--, RetailLocationDealID
		--INTO #ProdList     
		FROM #ProductList PL  
		LEFT JOIN Manufacturer M ON M.ManufacturerID = PL.ManufacturerID   
		WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit    

		----;WITH ProductList  
		----AS  
		----(  
		-- SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY P.ProductName)  
		--  , P.ProductID   
		--  , P.ProductName   
		--  , P.ManufacturerID    
		--  , P.ModelNumber   
		--  , P.ProductLongDescription   
		--  , P.ProductExpirationDate   
		--  , P.ProductImagePath   
		--  , P.SuggestedRetailPrice   
		--  , P.Weight   
		--  , P.WeightUnits  
		--  , RP.SalePrice  
		--  , RP.SaleStartDate   
		--  , RP.SaleEndDate    
		-- INTO #ProductList    
		-- FROM ProductCategory PC   
		--  INNER JOIN Product P ON P.ProductID = PC.ProductID  
		--  INNER JOIN RetailLocationProduct RP ON RP.ProductID = P.ProductID   
		--           AND RP.RetailLocationID = @RetailLocationID   
		-- WHERE PC.CategoryID  = @CategoryID  
		--  AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
		--  AND ISNULL(RP.SalePrice, 0) <> 0  
		--  AND GETDATE() BETWEEN RP.SaleStartDate AND RP.SaleEndDate   
		----)  
		----To capture max row number.  
		--SELECT @MaxCnt = MAX(Row_Num) FROM #ProductList  

		----this flag is a indicator to enable "More" button in the UI.   
		----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		--SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  

		--SELECT Row_Num  rowNumber  
		--  , ProductID productId  
		--  , ProductName  productName  
		--  , ManufName  manufacturersName  
		--  , ModelNumber modelNumber  
		--  , ProductLongDescription productDescription  
		--  , ProductExpirationDate productExpDate  
		--  , ProductImagePath imagePath  
		--  , SuggestedRetailPrice suggestedRetailPrice  
		--  , Weight productWeight  
		--  , WeightUnits weightUnits  
		--  , SalePrice salePrice  
		--  , SaleStartDate   
		--  , SaleEndDate   
		--FROM #ProductList PL  
		-- INNER JOIN Manufacturer M ON M.ManufacturerID = PL.ManufacturerID   
		--WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   




	
	  --Confirmation of Success.
	  SELECT @Status = 0	
	COMMIT TRANSACTION
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcFetchSalesListPagination.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.
   SELECT @Status = 1
  END;  
     
 END CATCH;  
END;












































GO
