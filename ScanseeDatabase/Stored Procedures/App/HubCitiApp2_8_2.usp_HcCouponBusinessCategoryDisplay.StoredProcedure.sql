USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcCouponBusinessCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CouponBusinessCategoryDisplay
Purpose					: To Display BusinessCategory Names
Example					: usp_CouponBusinessCategoryDisplay

History
Version		 Date		Author	  Change Description
------------------------------------------------------------------------------- 
1.0		 20th Jun 2013	 SPAN	   Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcCouponBusinessCategoryDisplay]
(

	--Input Variables 	
	   @UserID int
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(500)
	 , @HubCitiID int
	   	
	--Output Variables	
	 , @UserOutOfRange bit output    
	 , @DefaultPostalCode varchar(50) output
	 , @Status bit output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			
		--To get Default Radius
		DECLARE @Radius FLOAT
		DECLARE @DistanceFromUser FLOAT

		SELECT @Radius = ScreenContent 
		FROM AppConfiguration
		WHERE ScreenName = 'DefaultRadius'
		AND ConfigurationType = 'DefaultRadius'
		
		
		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
						 
		--Derive the Latitude and Longitude in the absence of the input.
		IF @Latitude IS NULL AND @Longitude IS NULL		
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
					 , @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
					 , @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @UserID
			END
		END
		--Check if the user has given latitude and longitude
		--IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)
		--BEGIN
			SELECT busCatId
					,busCatName
			FROM
			--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
			(SELECT DISTINCT BC.BusinessCategoryID busCatId
						  , BC.BusinessCategoryName	busCatName	 
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID
			INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND H.HcCityID = RL.HcCityID AND H.StateID = RL.StateID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
			INNER JOIN RetailerBusinessCategory RBC ON CR.RetailID = RBC.RetailerID
			INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID
			WHERE GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE() + 1) AND RL.Active = 1
			AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
			
			--Coupons associated to RetailLocations but not to Products W.R.T Latitude and Longitude
			UNION
			SELECT DISTINCT BC.BusinessCategoryID busCatId
						  , BC.BusinessCategoryName	busCatName	 
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID
			INNER JOIN HcLocationAssociation H ON H.PostalCode = RL.PostalCode AND H.HcCityID = RL.HcCityID AND H.StateID = RL.StateID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
			INNER JOIN RetailerBusinessCategory RBC ON CR.RetailID = RBC.RetailerID
			INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID
			WHERE CP.ProductID IS NULL AND RL.Active = 1
			AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE() + 1)
			AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
			)BIZCAT
			ORDER BY busCatName ASC
		--END		
		  
		 --Confirmation of Success.
		SELECT @Status = 0 	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CouponBusinessCategoryDisplay.'		
		-- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	
		--Confirmation of failure.
			SELECT @Status = 1		
		END;
		 
	END CATCH;
END;














































GO
