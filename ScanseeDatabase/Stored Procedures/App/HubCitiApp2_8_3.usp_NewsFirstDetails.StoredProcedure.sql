USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_NewsFirstDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_8_3].[usp_NewsFirstDetails]
Purpose					: 
Example					: [HubCitiApp2_8_3].[usp_NewsFirstDetails]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0							Sagar Byali
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_NewsFirstDetails]
(	
	  @RssFeedNewsID int
	  		
	--Output Variable 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
	
		DECLARE @HcHubCitiID int
			   ,@DefaultImage varchar(1000)
		SELECT @HcHubCitiID =HcHubCitiID FROM RssNewsFirstFeedNews where RssNewsFirstFeedNewsID =@RssFeedNewsID 

		--To set default image
		SELECT @DefaultImage = ScreenContent + CONVERT(VARCHAR,@HcHubCitiID) 
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT @DefaultImage = @DefaultImage + '/' + NewsCategoryImage
		FROM HcHubCiti
		where HcHubCitiID = @HcHubCitiID 


		--To display news details
			SELECT RssNewsFirstFeedNewsID itemId
				--,NewsType CategoryType
				,Title title
				,case when ImagePath = '  ' OR ImagePath IS NULL THEN @DefaultImage else ImagePath END image
				,ShortDescription sDesc
				,LongDescription lDesc
				,Link link
				,date = CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
				--,PubStrtDate= convert(datetime,PublishedDate,105) 
				,time = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
				--,Classification
				--,Section section
				--,AdCopy adcopy
				,author
				--,subcategory
				,VideoLink
			FROM RssNewsFirstFeedNews
			WHERE RssNewsFirstFeedNewsID = @RssFeedNewsID			 
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;













GO
