USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserSettingsPageDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserSettingsPageDisplay
Purpose					: To display the settings page dynamicaly configured by the hub citi admin.
Example					: usp_HcUserSettingsPageDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19/12/2013	    Pavan Sharma K	 1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserSettingsPageDisplay]
(
  	  @HubCitiID Int	
	, @UserID int
	  
	--Output Variable	
    , @Status int output  
	, @EducationLevel bit output
	, @MarialStatus bit output
	, @IncomeRange bit output      
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
			
			DECLARE @Config varchar(100)

			SELECT @Config = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'


			--Collect the fields for the settings page of the hub citi.
		    SELECT A.HcUserRegistrationFormFieldsID
				 , B.RegistrationFormFieldName
				 , A.Position
				 , A.Required 
			INTO #Fields
			FROM HcHubcitiUserRegistrationForms A
			INNER JOIN HcUserRegistrationFormFields B ON A.HcUserRegistrationFormFieldsID = B.HcUserRegistrationFormFieldsID 
			WHERE HchubcitiID = @HubCitiID

			--Unpivot the column to rows and get the fields entered by the user.
			SELECT HcUserID, RegistrationFormFieldName, Value
			INTO #Temp
			FROM
				(SELECT A.HcUserID 
					 , ISNULL(CAST(FirstName AS VARCHAR(1000)), '')FirstName
					 , ISNULL(CAST(LastName AS VARCHAR(1000)), '')LastName
					 , ISNULL(CAST(Email AS VARCHAR(1000)), '')Email
					 , ISNULL(CAST(PostalCode AS VARCHAR(1000)), '')ZipCode
					 , ISNULL(CAST(PhoneNO AS VARCHAR(1000)), '')Mobile
					 , ISNULL(CAST(Gender AS VARCHAR(1000)), '')Gender
					 , ISNULL(CAST(E.EducationLevelDescription AS VARCHAR(1000)), '')EducationLevel
					 , ISNULL(CAST(MaritalStatusName AS VARCHAR(1000)), '')MaritalStatus
					 , ISNULL(CAST(IncomeRange AS VARCHAR(1000)), '')IncomeRange
					 , ISNULL(CAST(DateOfBirth AS VARCHAR(1000)), '')DateOfBirth
				FROM HcUser A
				LEFT JOIN HcUserDemographic B ON A.HcUserID = B.HcUserID
				LEFT JOIN EducationLevel E ON E.EducationLevelID = B.EducationLevelID
				LEFT JOIN MaritalStatus M ON M.MaritalStatusID = B.MaritalStatusID
				LEFT JOIN IncomeRange I ON I.IncomeRangeID = B.IncomeRangeID
				WHERE A.HcUserID = @UserID) P
			UNPIVOT
				(
					Value for RegistrationFormFieldName IN (FirstName
														 , LastName
														 , Email
														 , ZipCode
														 , Mobile
														 , Gender
														 , EducationLevel
														 , MaritalStatus
														 , IncomeRange
														 , DateOfBirth
														 )
				) AS Unpvt

			SELECT A.RegistrationFormFieldName
				 , Value = CASE WHEN T.Value = '' THEN NULL ELSE T.Value END
				 , A.Position
				 , A.Required 
			INTO #SettigsForm
			FROM #Fields A
			LEFT JOIN #Temp T ON A.RegistrationFormFieldName = REPLACE(T.RegistrationFormFieldName, 'EducationLevel', 'Education')
			ORDER BY A.Position ASC
			
			UPDATE #SettigsForm SET Value = @Config + CAST(@HubCitiID AS VARCHAR(10)) +'/'+ (SELECT UserSettingsImagePath FROM HcMenuCustomUI WHERE HubCitiId = @HubCitiID)
			WHERE RegistrationFormFieldName = 'Image'

			UPDATE #SettigsForm SET Value =  LEFT(CAST(DATENAME(MM, Value) AS VARCHAR(100)), 3) +'-'+ CAST(DAY(Value) AS VARCHAR(100))+'-'+CAST(YEAR(Value) AS VARCHAR(100))
			WHERE RegistrationFormFieldName = 'DateOfBirth'

			SELECT Position
			     , fieldName = [HubCitiApp2_3_3].[fn_SpaceBeforeCaps] (RegistrationFormFieldName)
				 , Value --= CASE WHEN RegistrationFormFieldName LIKE '%Date%' THEN CONVERT(DATE, Value, 111) ELSE VALUE END
				 , Required requiredField
			FROM #SettigsForm
			ORDER BY Position

			
			
			--Send the values for the drop down based on what is configured by the Hub Citi admin.
			--Check for Education level in the form.
			IF EXISTS (SELECT 1 FROM #SettigsForm WHERE RegistrationFormFieldName = 'Education')
			BEGIN
				
				SET @EducationLevel = 1
				/*Commented on request from Kumar*/
				--SELECT EducationLevelID as educatLevelId
				--	 , EducationLevelDescription as educatLevelName
				--FROM EducationLevel
			END
			
			--Check for marital status in the form.
			IF EXISTS (SELECT 1 FROM #SettigsForm WHERE RegistrationFormFieldName = 'MaritalStatus')
			BEGIN
				SET @MarialStatus = 1
				--SELECT MaritalStatusID as mStatusId
				--	 , MaritalStatusName as mStatusName
				--FROM MaritalStatus
			END
			
			--Check for Income Range in the form.
			IF EXISTS (SELECT 1 FROM #SettigsForm WHERE RegistrationFormFieldName = 'IncomeRange')
			BEGIN
				SET @IncomeRange = 1
				--SELECT IncomeRangeID as icRangeId
    --                  ,IncomeRange as icRangeName
				--FROM IncomeRange
			END


		   --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcUserSettingsPageDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









































GO
