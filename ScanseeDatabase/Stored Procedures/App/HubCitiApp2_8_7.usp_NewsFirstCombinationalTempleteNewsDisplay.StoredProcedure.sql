USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_NewsFirstCombinationalTempleteNewsDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_5].[usp_NewsFirstScrollingNewsDisplay]
Purpose					: To display news as per Scrolling templete
Example					: [HubCitiApp2_5].[usp_NewsFirstScrollingNewsDisplay]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26 May 2016		Sagar Byali		Initial Version
---------------------------------------------------------------
*/



CREATE PROCEDURE[HubCitiApp2_8_7].[usp_NewsFirstCombinationalTempleteNewsDisplay]
(	
	  @HubCitiID int 
	, @LowerLimit INT
	, @ScreenName VARCHAR(1000)
	, @SearchKey VARCHAR(1000)
	, @HcUserID INT
	, @DateCheck varchar(1000)		
	, @LinkID int
	, @Level int
			
	--Output Variable 
	, @bannerImg VARCHAR(1000) Output
	, @weatherurl VARCHAR(1000) Output
	, @homeImgPath VARCHAR(1000) Output
	, @bkImgPath VARCHAR(1000) Output
	, @titleBkGrdColor VARCHAR(1000) Output
	, @titleTxtColor VARCHAR(1000) Output
	, @MaxCnt int Output
	, @NxtPageFlag bit Output
	, @TemplateChanged int output
	, @TemplateName varchar(100) output
	, @LowerLimitFlag int output
	, @ModifiedDate varchar(100) output
	, @SubPage varchar(100) output
	, @HamburgerImagePath varchar(1000) output
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY


	
select distinct c.NewsSubCategoryID
into #Ignore
from NewsFirstSettings N
inner join NewsFirstSubCategorySettings C on c.NewsCategoryID = C.NewsCategoryID and c.HcHubCitiID = n.HcHubCitiID and c.HcHubCitiID = @HubCitiID 
and ltrim(rtrim(c.NewsFirstSubCategoryURL)) = ltrim(rtrim(n.NewsFeedURL))


			DECLARE @thumbnailposition varchar(1000), @newscategoryimage varchar(1000)
			DECLARE @ServerConfig VARCHAR(500),@Config varchar(1000)
			DECLARE @TempID int, @ExistTempID int, @NewsFlag int, @NewsImage varchar(max)
			declare @master bigint

		SELECT @NewsImage = ScreenContent + CONVERT(VARCHAR,@HubCitiID)+ '/'
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		
		DECLARE @DefaultHamburgerIcon VARCHAR(1000)
		select @DefaultHamburgerIcon = ScreenContent +  'images/hamburger.png' 
		from AppConfiguration 
		where ScreenName = 'Base_URL'

		SELECT    @newscategoryimage = @NewsImage + NewsCategoryImage
				, @bannerImg = @NewsImage + NewsBannerImage
				, @weatherurl =  WeatherURL 
				, @thumbnailposition = NewsThumbnailPosition
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID 
			
						------------- Custom Navigation Bar changes -------------------------------

			

			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT @ServerConfig = ScreenContent FROM AppConfiguration WHERE ConfigurationType= 'Hubciti Media Server Configuration'
			
			SELECT	@homeImgPath = 	CASE WHEN HU.homeIconName IS NULL THEN @ServerConfig+'customhome.png' ELSE  @Config +CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName END
					 ,@bkImgPath = 	CASE WHEN HU.backButtonIconName IS NULL THEN @ServerConfig+'customback.png' ELSE @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName END
					 ,@titleBkGrdColor =  ISNULL( HU.backGroundColor ,'#000000')
					 ,@titleTxtColor =  ISNULL(HU.titleColor ,'#ffffff')
					 ,@HamburgerImagePath = ISNULL(@NewsImage + NewsHamburgerIcon,@DefaultHamburgerIcon)
				FROM HcHubCiti HC
				LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
				LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
				WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					 
			
	

		SET @ModifiedDate=replace(@ModifiedDate,'  ',' ')
		SET @DateCheck=replace(@DateCheck,'  ',' ')	
		set  @ModifiedDate=convert(datetime,@ModifiedDate)
		set  @DateCheck=convert(datetime,@DateCheck)
	
		--SELECT @DateCheck = cast(@DateCheck as datetime)
		
        SET @ModifiedDate = cast(replace(@ModifiedDate,'  ',' ') as datetime)
		SET @DateCheck = cast(@DateCheck as datetime)
		 
		 if  isnull(@Level,1)<>1
         BEGIN
		    if not exists(SELECT 1 FROM HcNewsFirstTempFlag H where HcHubCitiID = @HubCitiID  and isnull(Level,1)=0 and linkid=@LinkID) and isnull(@Level,1)<>1 
			begin
				 insert into HcNewsFirstTempFlag(HcHubCitiID,level,linkid) 
				 select @HubCitiID,0,@LinkID
			end
		 END
		  if not exists(SELECT 1 FROM HcNewsFirstTempFlag H where HcHubCitiID = @HubCitiID  and isnull(Level,1)=1)  and isnull(@Level,1)=1 
         BEGIN
				 insert into HcNewsFirstTempFlag(HcHubCitiID,level) 
				 select @HubCitiID,1
		 END

			 update   HcNewsFirstTempFlag set level=1  where level is null and HcHubCitiID = @HubCitiID

		 if isnull(@Level,1)=1
		 begin
		  SELECT top 1 @tempid = HcTemplateID FROM HcMenu H where HcHubCitiID = @HubCitiID and Level=1

				 if exists(SELECT 1 FROM HcMenu H where HcHubCitiID = @HubCitiID and IsAssociateNews=1 and Level=1)
				 BEGIN 
						 SET @NewsFlag=1
						 update HcNewsFirstTempFlag set NewsFlag=1,level=1,linkid=@LinkID where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=1
				 END
				 else
				 BEGIN
						SET @NewsFlag=0
						update HcNewsFirstTempFlag set NewsFlag=0,level=1,linkid=@LinkID where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=1
				 END
		 end
		 else
		    BEGIN
			 SELECT top 1 @tempid = HcTemplateID FROM HcMenu H where HcHubCitiID = @HubCitiID and  HcMenuID=@LinkID
		      if exists(SELECT 1 FROM HcMenu H where HcHubCitiID = @HubCitiID and IsAssociateNews=1 and HcMenuID=@LinkID)
		  		 BEGIN 
						 SET @NewsFlag=1
						 update HcNewsFirstTempFlag set NewsFlag=1,level=0 where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=0
				 END
				 else
				 BEGIN
						SET @NewsFlag=0
						update HcNewsFirstTempFlag set NewsFlag=0,level=0 where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=0
				 END
            end

		 update HcNewsFirstTempFlag  
		 set TemplateID=@tempid  
		 where TemplateID is null and HcHubCitiID=@HubCitiID
		 and isnull(level,1)=case when @Level=1 then '1' else 0 end
		 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 

		select top 1 @Existtempid = TemplateID  
		from HcNewsFirstTempFlag  
		where HcHubCitiID=@HubCitiID
		and isnull(level,1)=case when @Level=1 then '1' else 0 end
		 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 

	  IF ( @NEWSflag=0)
		begin
				update HCNewsFirstTempFlag 
				set NewsFlag=@NEWSflag,TemplateID=@tempid 
				where HcHubCitiID=@HubCitiID and level=case when @Level=1 then '1' else 0 end
				
				 SET @TemplateChanged = 1
				
		          if isnull(@Level,1)=1
					begin
							SELECT @TemplateName = isnull(T.TemplateName,0)
							,@ModifiedDate = convert(datetime,H.DateCreated)
							FROM HcTemplate  T 
							INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
							WHERE HcHubCitiID = @HubCitiID AND Level=1       
					end
					else
					begin
							SELECT @TemplateName = isnull(T.TemplateName,0)
							,@ModifiedDate = convert(datetime,H.DateCreated)
							FROM HcTemplate  T 
							INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
							WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

					end


						--select 'b'
				
				SELECT  @Status = 0
				 
	    end

		ELSE

	    BEGIN

		
						SET @LinkID =IIF(@LinkID IS NULL OR @LinkID=0 ,Null,@LinkID)
		
			
						DECLARE @MenuName varchar(1000), @hcmenuid int 
			
						SELECT @MenuName = MenuName 
						FROM HcMenu
						WHERE HcMenuID = @LinkID AND ISNULL(@LinkID, 0) <> 0
						OR ISNULL(@LinkID, 0) = 0 AND Level = 1	

						SELECT @HcMenuID = hcMenuid from hcmenu where HcHubCitiID = @HubCitiID and level =1 AND IsAssociateNews = 1
						
						IF @LinkID IS NOT NULL SET @HcMenuID = @LinkID
			
						--select @HcMenuID
						 -------------------------------------------------
	
						DECLARE @UpperLimit VARCHAR(1000)
							  , @RowsPerPage varchar(1000)
							  , @Exists BIT
							  , @BookMarkExists BIT

						--To get the row count for pagination.  
						SELECT @UpperLimit = @LowerLimit + ScreenContent 
							 , @RowsPerPage = ScreenContent
						FROM AppConfiguration   
						WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1	
			
									--To fetch LatestNews---
			SELECT  Newstype
					,HcHubCitiID
				    ,max(datecreated) as MaxDate
            INTO #2DaysNews 
            FROM RssNewsFirstFeedNews 
            GROUP BY Newstype,HcHubCitiID


			

			SELECT R.* 
			INTO #HubCitiNewss
			FROM RssNewsFirstFeedNews R 
			INNER JOIN #2DaysNews T on T.Newstype=R.Newstype AND T.HcHubCitiID=R.HcHubCitiID AND DATEDIFF(Day,R.DateCreated,T.MaxDate) <= 15

		

						SELECT  *
						INTO #HubCitiNews
						FROM #HubCitiNewss
						WHERE HcHubCitiID = @HubCitiID --AND NewsType = @CategoryType-- AND @isSideNavigation <> 3) OR (subcategory = @CategoryType AND @isSideNavigation = 3))
			
		--To set todays date to null			
--update #HubCitiNews
--set PublishedDate = null
--where left(replace(getdate(),'  ,',','),11) = replace(left(replace(publisheddate,'  ,',','),12),',','')

		

						ALTER TABLE #HubCitiNews ADD NewsCatID INT
						alter table #HubCitiNews ADD NewsSubCatID int

						UPDATE T
						SET NewsCatID = NewsCategoryID  
						FROM #HubCitiNews T
						INNER JOIN NewsFirstCategory N ON N.NewsCategoryDisplayValue=T.NewsType
						where active =1

						
						UPDATE T
						SET NewsSubCatID = N.NewsSubCategoryID  
						FROM #HubCitiNews T
						INNER JOIN NewsFirstSubCategory N ON N.NewsSubCategoryDisplayValue=T.subcategory

						CREATE CLUSTERED INDEX IX_TestTable_TestCol1   
						ON #HubCitiNews (hcHubCitiID);

						CREATE NONCLUSTERED INDEX IX1_TestTable_TestCol1   
						ON #HubCitiNews (hcHubCitiID,NewsCatID);

						CREATE TABLE #News			(RssNewsFirstFeedNewsID INT
													,NewsType VARCHAR(50)
													,NewsCategoryColor VARCHAR(50)
													,NewsCategoryTempleteTypeDisplayValue VARCHAR(100)
													,Title VARCHAR(max)
													,ImagePath VARCHAR(max)
													,ShortDescription  VARCHAR(max)
													,LongDescription VARCHAR(max)
													,Link VARCHAR(max)
													,PublishedDate DATETIME
													,PubStrtDate DATETIME
													,PublishedTime DATETIME
										
													,Classification VARCHAR(1000)
													,section VARCHAR(1000)
													,adcopy VARCHAR(1000)
													,Author VARCHAR(1000)
										
													,SubCategory VARCHAR(20)
													,VideoLink VARCHAR(max)
													,thumbnail VARCHAR(max)
													,SortOrder INT
													,NewsCategoryFontColor VARCHAR(60)
													,NonFeedNewslink varchar(1000)
													,BackButtonColor varchar(100)
													)



						IF EXISTS(SELECT 1 FROM HcUserNewsFirstBookmarkedCategories WHERE HcUserID = @HcUserID AND HchubcitiID = @HubCitiID)
						--To display News records
						BEGIN

		
						INSERT INTO #News
						SELECT DISTINCT RssNewsFirstFeedNewsID 
								,NewsType
								,NewsCategoryColor
								,NewsCategoryTempleteTypeDisplayValue 
								,Title
								,ImagePath
								,ShortDescription 
								,LongDescription 
								,Link
								,PublishedDate = CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
								,PubStrtDate= convert(datetime,PublishedDate,105) 
								,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
								,Classification 
								,section
								,adcopy 
								,author
					
								,subcategory
								,VideoLink
								,thumbnail
								,BC.SortOrder
								,NewsCategoryFontColor
								,null
								,ns.BackButtonColor
						FROM HcUser U
						INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
						INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID  AND M.IsAssociateNews = 1 AND HcMenuID = @HcMenuID
						INNER JOIN NewsFirstSettings NS ON NS.HcHubCitiID = M.HcHubCitiID 
						INNER JOIN #HubCitiNews N ON N.HcHubCitiID = NS.HchubcitiID AND N.NewsCatID = NS.NewsCategoryID 
						INNER JOIN NewsFirstCategoryTempleteType TT ON TT.NewsCategoryTempleteTypeID = NS.NewsFirstCategoryTempleteTypeID
						INNER JOIN HcUserNewsFirstBookmarkedCategories BC ON BC.HcUserID = U.HcUserID AND BC.HchubcitiID = NS.HcHubCitiID AND BC.NewsCategoryID = N.newsCatID
						LEFT JOIN #ignore i on i.NewsSubCategoryID = n.NewsSubCatID
						WHERE M.HcHubCitiID = @HubCitiID and  Title is not null and i.NewsSubCategoryID is null

						UNION

						SELECT DISTINCT NULL 
								,Ns.NewsLinkCategoryName
								,NewsCategoryColor
								,NULL 
								,NULL
								,NULL
								,NULL 
								,NULL 
								,NULL
								,PublishedDate = NULL --CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
								,PubStrtDate= NULL --convert(datetime,PublishedDate,105) 
								,PublishedTime = NULL--FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
								,NULL
								,NULL
								,NULL 
								,NULL
					
								,NULL
								,NULL
								,NULL
								,BC.SortOrder
								,NewsCategoryFontColor
								,NS.NewsFeedURL
								,ns.BackButtonColor
						FROM HcUser U
						INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
						INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID  AND M.IsAssociateNews = 1 AND HcMenuID = @HcMenuID
						INNER JOIN NewsFirstSettings NS ON NS.HcHubCitiID = M.HcHubCitiID 
						INNER JOIN HcUserNewsFirstBookmarkedCategories BC ON BC.HcUserID = U.HcUserID AND BC.HchubcitiID = NS.HcHubCitiID AND BC.NewsLinkCategoryName = NS.NewsLinkCategoryName
						WHERE M.HcHubCitiID = @HubCitiID 
						order by BC.SortOrder
						END

						ELSE

						BEGIN

						INSERT INTO #News
						SELECT DISTINCT RssNewsFirstFeedNewsID 
								,NewsType
								,NN.NewsCategoryColor
								,NewsCategoryTempleteTypeDisplayValue
								,Title
								,ImagePath
								,ShortDescription 
								,LongDescription 
								,Link
								,PublishedDate = CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
								,PubStrtDate= convert(datetime,PublishedDate,105) 
								,PublishedTime = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
								,Classification 
								,section
								,adcopy 
								,author
					
								,subcategory
								,VideoLink
								,thumbnail
								,BC.SortOrder
								,NewsCategoryFontColor
								,null
							,nn.BackButtonColor
						FROM HcUser U
						INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
						INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID  AND M.IsAssociateNews = 1 AND HcMenuID = @HcMenuID 
						INNER JOIN #HubCitiNews N ON N.HcHubCitiID = M.HchubcitiID
						LEFT JOIN #ignore i on i.NewsSubCategoryID = n.NewsSubCatID
						INNER JOIN HcDefaultNewsFirstBookmarkedCategories BC ON BC.NewsCategoryID = N.NewsCatID AND BC.HchubcitiID = N.HcHubCitiID 
						INNER JOIN NewsFirstSettings NN ON NN.NewsCategoryID = BC.NewsCategoryID AND BC.HchubcitiID = M.HcHubCitiID AND NN.NewsCategoryID = N.newscatid AND NN.HchubcitiID = @HubCitiID 
						INNER JOIN NewsFirstCategoryTempleteType TT ON TT.NewsCategoryTempleteTypeID = NN.NewsFirstCategoryTempleteTypeID
						WHERE BC.HchubcitiID = @HubCitiID and  Title is not null and i.NewsSubCategoryID is null

						UNION

						SELECT DISTINCT NULL 
								,NN.NewsLinkCategoryName
								,NewsCategoryColor
								,NULL 
								,NULL
								,NULL
								,NULL 
								,NULL 
								,NULL
								,PublishedDate = NULL --CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
								,PubStrtDate= NULL --convert(datetime,PublishedDate,105) 
								,PublishedTime = NULL--FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
								,NULL
								,NULL
								,NULL 
								,NULL
					
								,NULL
								,NULL
								,NULL
								,BC.SortOrder
								,NewsCategoryFontColor
								,NN.NewsFeedURL
								,nn.BackButtonColor
						FROM HcUser U
						INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = U.HcUserID AND UD.HcUserID = @HcUserID
						INNER JOIN HcMenu M ON M.HcHubCitiID = UD.HcHubCitiID  AND M.IsAssociateNews = 1 AND HcMenuID = @HcMenuID 
						INNER JOIN NewsFirstSettings NN ON  NN.HchubcitiID = M.HcHubCitiID AND NN.HchubcitiID = @HubCitiID 
						INNER JOIN HcDefaultNewsFirstBookmarkedCategories BC ON BC.NewsLinkCategoryName = NN.NewsLinkCategoryName AND BC.HchubcitiID = NN.HcHubCitiID 
						WHERE BC.HchubcitiID = @HubCitiID 
						order by bc.SortOrder


						END

						

	
						--To Set Max count for Pagination
						SELECT @MaxCnt = count(1) FROM #News

						;WITH Ten_NewsStories AS
						(SELECT   rowNum = ROW_NUMBER() OVER (PARTITION BY NewsType ORDER BY CASE WHEN PublishedDate is null THEN 1 
						 WHEN PublishedDate is not null THEN 1 END, PublishedDate desc, CAST(PublishedTime AS time) desc )
								,RssNewsFirstFeedNewsID itemId
								,upper(NewsType) catName
								,NewsCategoryColor catColor
								,NewsCategoryFontColor catTxtColor
								,NewsCategoryTempleteTypeDisplayValue  displayType
								,Title title
								,case when ImagePath is null or ImagePath =' ' then @newscategoryimage else ImagePath end image
								,ShortDescription sDesc
								,LongDescription lDesc
								,Link link
								,replace(SUBSTRING(cast(PublishedDate as varchar(1000)),1,7) + ' , ' +SUBSTRING(cast(PublishedDate as varchar(1000)),8,4),'  ,',',')   date
								,PubStrtDate
								, FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt') time
								,Classification 
								,section
								,NULL adcopy
								,author
				
								,subcategory
								,VideoLink
								,thumbnail 
								,SortOrder
								,NonFeedNewslink nonfeedlink
								,BackButtonColor backButtonColor
							FROM #News)
							SELECT * INTO #NewsDisplay FROM Ten_NewsStories WHERE rowNum BETWEEN 1 AND 10



																--To set todays date to null			
			update #NewsDisplay
			set date = null
			where left(replace(getdate(),'  ,',','),11) = replace(left(replace(date,'  ,',','),12),',','')
						
						;WITH News_Display AS(
						SELECT DENSE_RANK() OVER (ORDER BY sortorder) AS orderby
							   , * 
						FROM #NewsDisplay  
						)SELECT * INTO #CNews FROM News_Display ORDER BY SortOrder 


						SELECT @maxcnt= COUNT(DISTINCT orderby) FROM #CNews  



						--To Display Combinational News
						SELECT * FROM #CNews
						WHERE orderby BETWEEN (@LowerLimit+1) AND @UpperLimit
						
						

						SET @LowerLimitFlag = (@LowerLimit + 3)
	
						--this flag is a indicator to enable "More" button in the UI.   
						--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
						SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
						IF @NxtPageFlag = 0 SET @LowerLimitFlag= 0
                      

							--Temlete changed implementation----START------
					
							if isnull(@Level,1)=1

							begin
										IF EXISTS(SELECT 1 FROM HcMenu H
													INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
													WHERE left(convert(datetime,h.DateCreated),19)  = left(convert(datetime,@DateCheck),19) AND HcHubCitiID = @HubCitiID 
													and Level=1) or @DateCheck  is null
										BEGIN
													SELECT @TemplateChanged = 0 
										END
										ELSE
										BEGIN
												SELECT  @TemplateChanged = 1
										END

							end
							else

							begin
										IF EXISTS(SELECT 1 FROM HcMenu H
													INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
													WHERE left(convert(datetime,h.DateCreated),19)  = left(convert(datetime,@DateCheck),19) AND HcHubCitiID = @HubCitiID 
													and h.HcMenuID=@LinkID) or @DateCheck  is null
										BEGIN
													SELECT @TemplateChanged = 0 
										END
										ELSE
										BEGIN
												SELECT  @TemplateChanged = 1
										END

							end




				if isnull(@Level,1)=1
				begin
						SELECT @TemplateName = isnull(T.TemplateName,0)
								,@ModifiedDate = convert(datetime,H.DateCreated)
						FROM HcTemplate  T 
						INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
						WHERE HcHubCitiID = @HubCitiID AND Level=1       
				end
				else
				begin
				SELECT @TemplateName = isnull(T.TemplateName,0)
								,@ModifiedDate = convert(datetime,H.DateCreated)
						FROM HcTemplate  T 
						INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
						WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

				end

				update HCNewsFirstTempFlag 
				set NewsFlag=@NEWSflag,TemplateID=@tempid ,linkid=@LinkID
				where HcHubCitiID=@HubCitiID and level=case when @Level=1 then '1' else 0 end
				and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end
			 
						 SET @Status=0	

	end
			 
			 select top 1 @master=HcTemplateID  from HcTemplate  where  TemplateName='Combination News Template'
IF @DateCheck IS  NULL
set @TemplateChanged = 0

if (@Existtempid<>@tempid  or @tempid<>@master )
SET @TemplateChanged = 1

			 SET @Status=0	
		  --select @TemplateChanged
	  					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			
			throw;
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;











GO
