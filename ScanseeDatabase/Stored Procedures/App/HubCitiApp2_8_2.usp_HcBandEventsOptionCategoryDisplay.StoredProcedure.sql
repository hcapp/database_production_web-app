USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcBandEventsOptionCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name		 : [HubCitiApp2_8_2].[[usp_HcBandEventsOptionCategoryDisplay]]
Purpose                      : To display list of Cities which has Events for given HubCitiID(RegionApp).
Example                      : [HubCitiApp2_8_2].[[usp_HcBandEventsOptionCategoryDisplay]]

History
Version      Date                Author               Change Description
------------------------------------------------------------------------------------ 
1.0          16/09/2016			Shilpashree		 Initial Version - usp_HcBandEventsPreferredCityList
------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcBandEventsOptionCategoryDisplay]
(   
    --Input variable.        
         @HubCitiID int
       , @UserID Int
       , @Latitude Float
       , @Longitude Float
       , @Postalcode Varchar(200)
	   , @SearchParameter Varchar(1000)
       --, @RetailID int
	   , @HcMenuItemID int
       , @HcBottomButtonID int

       --Output Variable
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN
       BEGIN TRY
           
            DECLARE @UserLatitude float
            DECLARE @UserLongitude float 
			DECLARE @BandCategoryID VARCHAR(1000)
			DECLARE @BandID INT
			DECLARE @UserOutOfRange bit
			DECLARE @DefaultPostalCode Varchar(200)
			DECLARE @DistanceFromUser FLOAT
			DECLARE @Radius int = 99
			DECLARE @RetailID int
			SET @BandID = @RetailID
														   
			--IF (@Radius IS NULL)
			--BEGIN
			SELECT @Radius = LocaleRadius
			FROM UserPreference WHERE UserID = @UserID
			--END

			IF (@Radius IS NULL)
			SELECT @Radius = 90
		
			IF @Latitude IS NULL AND @Postalcode IS NOT NULL
			BEGIN
			SELECT @Latitude =Latitude 
					,@Longitude =Longitude
			FROM GeoPosition
			WHERE PostalCode =@Postalcode 
			END
			
            SET @UserLatitude = @Latitude
            SET @UserLongitude = @Longitude                  

            IF (@UserLatitude IS NULL) 
            BEGIN
                SELECT @UserLatitude = Latitude, @UserLongitude = Longitude
                FROM BandUsers A
                INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                WHERE BandUserID = @UserID 
            END
            --Pick the co ordinates of the default postal code if the user has not configured the Postal Code.
            IF (@UserLatitude IS NULL) 
            BEGIN
                SELECT @UserLatitude = Latitude, @UserLongitude = Longitude
                FROM HcHubCiti A
                INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                WHERE A.HcHubCitiID = @HubCitiID
            END
			
            --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
            EXEC [HubCitiApp2_8_2].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
            SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)
			
            --Derive the Latitude and Longitude in the absence of the input.
            IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
            BEGIN
                IF @Postalcode IS NULL
                BEGIN
                        SELECT @Latitude = G.Latitude, @Longitude = G.Longitude
                        FROM GeoPosition G
                        INNER JOIN Users U ON G.PostalCode = U.PostalCode
                        WHERE U.UserID = @UserID
                END
                ELSE
                BEGIN
                        SELECT @Latitude = Latitude, @Longitude = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @Postalcode
                END
            END
			
			SELECT HcHubCitiID
			INTO #RHubcitiList
			FROM
				(SELECT RA.HcHubCitiID
				FROM HcHubCiti H
				LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
				WHERE H.HcHubCitiID  = @HubCitiID
				UNION ALL
				SELECT @HubCitiID)A

			DELETE FROM #RHubcitiList WHERE HcHubCitiID IS NULL

			--SELECT DISTINCT P.HcHubcitiID
			--		, P.HcUserID
			--		, HcCityID = IIF(P.HcCityID IS NULL, LA.HcCityID, P.HcCityID)
			--INTO #PreferredCiti
			--FROM HcUsersPreferredCityAssociation P
			--INNER JOIN #RHubcitiList H ON P.HcHubcitiID = H.HcHubcitiID
			--INNER JOIN HcLocationAssociation LA ON H.HcHubcitiID = LA.HcHubCitiID
			--WHERE P.HcUserID = @UserID

			--SELECT DISTINCT HcHubcitiID, HcUserID, P.HcCityID, C.CityName 
			--INTO #PreferredCities
			--FROM #PreferredCiti P
			--INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
			
			SELECT DISTINCT LA.HcHubCitiID,LA.HcCityID,LA.City AS CityName
			INTO #PreferredCities
			FROM #RHubcitiList H
			INNER JOIN HcLocationAssociation LA ON H.HcHubcitiID = LA.HcHubCitiID

			SELECT DISTINCT  HE.HcBandEventID, HE.HcHubCitiID, B.BandID
			INTO #BandEvents
			FROM HcBandEvents HE
			INNER JOIN HcBandEventsAssociation HA ON HA.HcBandEventID= HE.HcBandEventID
			INNER JOIN Band B ON HE.BandID = B.BandID AND B.BandActive = 1
			LEFT JOIN #RHubcitiList RH ON HE.HcHubCitiID = RH.HcHubcitiID
			LEFT JOIN HcBandEventInterval EI ON HE.HcBandEventID = EI.HcBandEventID
			LEFT JOIN HcEventRecurrencePattern R ON R.HcEventRecurrencePatternID = HE.HcEventRecurrencePatternID
			WHERE (HE.HcHubCitiID = RH.HcHubcitiID OR HE.HcHubCitiID IS NULL)
			AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND Active = 1	
				
			SELECT DISTINCT  E.HcbandEventID AS EventID
							,E.BandID
							,EC.HcBandEventCategoryID  AS HcEventCategoryID
							,EC.HcBandEventCategoryName AS HcEventCategoryName
							,City = isnull(HL.City,RL.City)
							,Distance = (ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214 END, 0))
							,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
			INTO #Temp1
			FROM #BandEvents  E 
			INNER JOIN HcBandEventsCategoryAssociation EA ON EA.HcBandEventID =E.hcBandEventID 
			INNER JOIN HcBandEventsCategory EC ON EC.HcBandEventCategoryID =EA.HcBandEventCategoryID
			INNER JOIN #RHubcitiList RH ON (E.HcHubCitiID = RH.HcHubcitiID OR E.HcHubCitiID IS NULL)
			LEFT JOIN HcBandEventLocation HL ON HL.HcBandEventID =E.HcBandEventID 
			LEFT JOIN HcBandEventAppsite A ON E.hcBandEventID = A.HcBandEventID AND A.HcHubCitiID = @HubCitiID AND A.HcBandEventID IS NULL
			LEFT JOIN HcBandAppSite HA ON A.HcBandAppSiteID = HA.HcBandAppSiteID AND HA.HcHubCitiID = @HubCitiID 
			LEFT JOIN Geoposition G ON (G.Postalcode=HL.Postalcode)                     
			LEFT JOIN HcBandRetailerEventsAssociation L ON L.HcBandEventID= E.HcBandEventID  
			LEFT JOIN Retailer R On R.RetailID = L.retailid
			LEFT JOIN RetailLocation RL on RL.RetailID=R.RetailID AND RL.RetailID=L.RetailID AND RL.RetailLocationID=L.RetailLocationID
			WHERE (E.HcHubCitiID = RH.HcHubcitiID OR E.HcHubCitiID IS NULL)
			GROUP BY E.HcbandEventID,E.BandID,EC.HcBandEventCategoryID,EC.HcBandEventCategoryName,isnull(HL.City,RL.City)
		
			SELECT EventID
				,BandID
				,HcEventCategoryID
				,HcEventCategoryName
				,Distance
				,DistanceActual
				,C.HcCityID
				,C.CityName
			INTO #Temp2
			FROM #Temp1 T
			INNER JOIN #PreferredCities C ON T.City = C.CityName 
	
			CREATE TABLE #Events(HcEventID Int
							,BandID Int
							,HcEventCategoryID INT
                            ,HcEventCategoryName Varchar(2000)
							,Distance Float
							,DistanceActual float)

			IF (@BandID IS NOT NULL)			
			BEGIN
                INSERT INTO #Events(HcEventID  
									,BandID
									,HcEventCategoryID
									,HcEventCategoryName
                                    ,Distance
                                    ,DistanceActual)
							SELECT  EventID HcEventID 
									,BandID
									,HcEventCategoryID
									,HcEventCategoryName
                                    ,Distance   
                                    ,DistanceActual
								FROM #Temp2       
								WHERE BandID = @BandID 
			END 
			IF (@BandID IS NULL)
			BEGIN
                INSERT INTO #Events(HcEventID 
									,BandID 
									,HcEventCategoryID
									,HcEventCategoryName
                                    ,Distance
                                    ,DistanceActual)
								SELECT  EventID 
									,BandID
									,HcEventCategoryID
									,HcEventCategoryName
                                    ,Distance   
                                    ,DistanceActual
								FROM #Temp2  
								WHERE  Distance <= @Radius
			END
				
			 SELECT DISTINCT HcEventCategoryID AS busCatId, HcEventCategoryName AS busCatName
			 FROM #Events
						
			--Confirmation of Success
			SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure [usp_HcBandEventsPreferredCityList].'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;








GO
