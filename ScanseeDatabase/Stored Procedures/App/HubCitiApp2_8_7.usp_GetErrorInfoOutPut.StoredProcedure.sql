USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_GetErrorInfoOutPut]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_GetErrorInfoOutPut  
Purpose     : Retrieve error information  
Example     : EXEC usp_GetErrorInfoOutPut   
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------  
1.0   12th May 2011 Padmapriya M Initial Version  
---------------------------------------------------------------   
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_GetErrorInfoOutPut]  
(
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS  
BEGIN  
  
--To get Error Information  
 SELECT @ErrorNumber = ERROR_NUMBER()   
  ,@ErrorMessage = ERROR_MESSAGE()  
    
END

























GO
