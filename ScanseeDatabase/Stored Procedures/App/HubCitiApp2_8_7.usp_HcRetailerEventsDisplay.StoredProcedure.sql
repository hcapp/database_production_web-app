USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcRetailerEventsDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcRetailerEventsDisplay
Purpose					: To display List fo Events.
Example					: usp_HcRetailerEventsDisplay

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/30/2014      SPAN				1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcRetailerEventsDisplay]
(   
    --Input variable.
	  @HubCitiID int	  
	, @RetailID int
    , @RetailLocationID int
	, @UserID int
	, @CategoryID int
	, @Latitude Float
	, @Longitude Float
	, @Postalcode Varchar(200)
	, @SearchParameter Varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @SortColumn Varchar(200)
	, @SortOrder Varchar(100)
	, @GroupColumn varchar(50)

	--User Tracking
	, @MainMenuID Int
  
	--Output Variable 
	, @UserOutOfRange bit output
	, @DefaultPostalCode VARCHAR(10) output
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int
			DECLARE @DistanceFromUser FLOAT
			DECLARE @UserLatitude float
			DECLARE @UserLongitude float 

			SELECT @UserLatitude = @Latitude
				 , @UserLongitude = @Longitude			

			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
					 , @UserLongitude = Longitude
				FROM HcUser A
				INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				WHERE HcUserID = @UserID 
			END
			--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HubCitiID
			END

			print @UserLatitude
			print @UserLongitude
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)

			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
				IF @Postalcode IS NULL
				BEGIN
					SELECT @Latitude = G.Latitude
						 , @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID
				END
				ELSE
				BEGIN
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude
					FROM GeoPosition 
					WHERE PostalCode = @Postalcode
				END
			END

			DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To derive postal code if event is not a business
			DECLARE @ZipCode varchar(10)
			SELECT @ZipCode = PostalCode
			FROM GeoPosition
			WHERE Latitude = ISNULL(@Latitude,@UserLatitude) And Longitude = ISNULL(@Longitude,@Longitude)
			
			--To display List of Events
			SELECT DISTINCT E.HcEventID 
						   ,HcEventName  
			               ,ShortDescription
                           ,LongDescription
						   ,E.HcHubCitiID 								
						   ,ImagePath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath
						   ,BussinessEvent 
						   ,StartDate =CAST(StartDate AS DATE)
						   ,EndDate =CAST(EndDate AS DATE)
						   ,StartTime =CAST(StartDate AS Time)
						   ,EndTime =CAST(EndDate AS Time)						  
						   ,EC.HcEventCategoryID 
						   ,EC.HcEventCategoryName 							
			               ,Distance = ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)
						   ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(RL.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL.RetailLocationLatitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(RL.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL.RetailLocationLatitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(RL.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END					
						   ,E.OnGoingEvent 
			INTO #Temp
			FROM HcEvents E 
			INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID = E.HcEventID 
			INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID = EA.HcEventCategoryID
			LEFT JOIN HcEventLocation EL ON E.HcEventID = EL.HcEventID
			LEFT JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID 
			LEFT JOIN RetailLocation RL ON RL.RetailLocationID = RL.RetailLocationID AND RL.Active=1
			LEFT JOIN Geoposition G ON RL.Postalcode = G.Postalcode			
			WHERE E.RetailID = @RetailID AND (RE.RetailLocationID = @RetailLocationID or 1=1)
			AND (( ISNULL(@CategoryID, '0') <> '0' AND EC.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))				
            AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
            AND GETDATE() < ISNULL(EndDate, GETDATE()+1)
			AND (EL.PostalCode = @ZipCode OR 1=1)
            GROUP BY E.HcEventID 
				   , HcEventName
				   , ShortDescription
                   , LongDescription
				   , E.HcHubCitiID 								
				   , ImagePath
				   , BussinessEvent
				   , StartDate
				   , EndDate
				   , EC.HcEventCategoryID
				   , EC.HcEventCategoryName
				   , OnGoingEvent

		
                 CREATE TABLE #Events(RowNum INT IDENTITY(1, 1)
									   ,HcEventID Int
									   ,HcEventName  Varchar(2000)
									   ,ShortDescription Varchar(2000)
									   ,LongDescription  Varchar(2000)
									   ,HcHubCitiID Int								
									   ,ImagePath Varchar(2000) 
									   ,BussinessEvent Bit
									   ,StartDate date
									   ,EndDate date
									   ,StartTime Time
									   ,EndTime Time						  
									   ,HcEventCategoryID int
									   ,HcEventCategoryName Varchar(2000)
									   ,Distance Float
									   ,DistanceActual float
									   ,OnGoingEvent bit)

                    INSERT INTO #Events(HcEventID  
									   ,HcEventName  
									   ,ShortDescription 
									   ,LongDescription 
									   ,HcHubCitiID 							
									   ,ImagePath
									   ,BussinessEvent 
									   ,StartDate 
									   ,EndDate 
									   ,StartTime 
									   ,EndTime 						  
									   ,HcEventCategoryID 
									   ,HcEventCategoryName
									   ,Distance
									   ,DistanceActual
									   ,OnGoingEvent)

							   SELECT  HcEventID 
									  ,HcEventName  
									  ,ShortDescription
									  ,LongDescription
									  ,HcHubCitiID 								
									  ,ImagePath
									  ,BussinessEvent
									  ,StartDate 
									  ,EndDate 
									  ,StartTime
									  ,EndTime				
									  ,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
									  ,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
									  ,Distance	
									  ,DistanceActual
									  ,OnGoingEvent
								FROM #Temp		
								GROUP BY HcEventID 
									  ,HcEventName  
									  ,ShortDescription
									  ,LongDescription
									  ,HcHubCitiID 								
									  ,ImagePath
									  ,BussinessEvent
									  ,StartDate 
									  ,EndDate 
									  ,StartTime
									  ,EndTime				
									  ,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
									  ,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
									  ,Distance
									  ,DistanceActual
									  ,OnGoingEvent
								ORDER BY OnGoingEvent DESC, 
										CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'HcEventName' THEN HcEventName
											  WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
											  WHEN @SortOrder = 'ASC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END,
										 CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
											  WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
											  WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
			

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Events
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			--User Tracking
           
			CREATE TABLE #Temp1(EventsListID INT, HcEventID Int)
			
			INSERT INTO HubCitiReportingDatabase..EventsList(MainMenuID
															 ,HcEventID 
															 ,EventClick 
			                                                 ,DateCreated)

			OUTPUT Inserted.EventsListID,inserted.HCEventID INTO #Temp1(EventsListID ,HcEventID)

			SELECT @MainMenuID
			      , HcEventID
				  , 0
				  , GETDATE()
		    FROM #Events		
			
			DECLARE @Seed INT
			SELECT @Seed = ISNULL(MIN(EventsListID), 0)
			FROM #Temp1			
			
			SELECT DISTINCT RowNum = Row_Number() over(order by RowNum asc)
				   ,HcEventID  
				   ,HcEventName  
				   ,ShortDescription 
				   ,LongDescription 
				   ,HcHubCitiID 							
				   ,ImagePath 
				   ,BussinessEvent
				   ,StartDate 
				   ,EndDate 
				   ,StartTime 
				   ,EndTime 						  
				   ,HcEventCategoryID 
				   ,HcEventCategoryName
				   ,Distance
				   ,DistanceActual
				   ,OnGoingEvent 
			INTO #Event
			FROM #Events
			
			DECLARE @SQL VARCHAR(1000) = 'ALTER TABLE #Event ADD EventsListID1 INT IDENTITY('+CAST(@Seed AS VARCHAR(100))+', 1)'
			EXEC (@SQL)
			
			SELECT DISTINCT Rownum rowNum
			      ,T.EventsListID eventListId						 
			      ,E.HcEventID eventId
			      ,HcEventName eventName
			      ,ShortDescription shortDes
                  ,LongDescription longDes
				  ,HcHubCitiID hubCitiId								
				  ,ImagePath imgPath
				  ,BussinessEvent busEvent
				  ,StartDate 
				  ,EndDate 
				  ,StartTime
				  ,EndTime				
				  ,HcEventCategoryID eventCatId
				  ,HcEventCategoryName eventCatName	
				  ,Distance	= ISNULL(DistanceActual, Distance)	
				  ,ISNULL(OnGoingEvent, 0)	isOnGoing								
			FROM #Event E
			INNER JOIN #Temp1 T ON T.EventsListID=E.EventsListID1
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  	
		    ORDER BY RowNum 
			
			----To get list of BottomButtons for this Module
			--DECLARE @ModuleName Varchar(50) 
			--SET @ModuleName = 'Event'

			--EXEC [HubCitiApp2_3_3].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,@ModuleName,@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output		    

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcRetailerEventsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








































GO
