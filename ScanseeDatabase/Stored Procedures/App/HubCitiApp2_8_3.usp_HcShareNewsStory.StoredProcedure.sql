USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcShareNewsStory]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : [usp_HcShareNewsStory]
Purpose                 : 
Example                 : [usp_HcShareNewsStory]

History
Version           Date              Author			 Change Description
--------------------------------------------------------------- 
1.0				2/22/2017			Sagar Byali       1.0
---------------------------------------------------------------
*/

 create PROCEDURE[HubCitiApp2_8_3].[usp_HcShareNewsStory]
(
        
	 	@NewsID int
    
      --Output Variable 
	  , @NewsShareText varchar(1000) output
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
      
		BEGIN TRANSACTION

		--
		SET @NewsShareText = 'I found this News Article in the ' + (SELECT hubcitiname FROM RssNewsFirstFeedNews B INNER JOIN HchubCiti HC ON HC.HcHubCitiID = B.HcHubCitiID WHERE B.RssNewsFirstFeedNewsID = @NewsID)+ ' and thought you might be interested:'
    
		SELECT DISTINCT   --RssNewsFirstFeedNewsID itemId
		                  Title title
						, NewsType type
		                , image = ISNULL(CASE WHEN CHARINDEX(',', ImagePath) > 0 THEN LEFT(ImagePath, CHARINDEX(',', ImagePath)-1) ELSE ImagePath END,(SELECT screencontent FROM AppConfiguration where ConfigurationType ='Web News First') + CAST(B.HcHubCitiID AS VARCHAR(5)) + '/' +  NewsCategoryImage)							  
						, qrUrl = (SELECT screencontent FROM AppConfiguration where ConfigurationType ='QR Code Configuration') + '4000.htm?newsId='+ CAST(B.RssNewsFirstFeedNewsID AS VARCHAR(10))--+ '&hubcitiId='+ HubCitiName
		FROM RssNewsFirstFeedNews B
	    INNER JOIN HchubCiti HC ON HC.HcHubCitiID = B.HcHubCitiID
		WHERE B.RssNewsFirstFeedNewsID = @NewsID
            
           --Confirmation of Success.
		   SELECT @Status = 0 
        COMMIT TRANSACTION          
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure [usp_HcShareBandAppsite].'       
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;
            
      END CATCH;
END;









GO
