USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcSpecialOffersRetailersDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcRetailerLocationSpecialOffersDisplay
Purpose					: To List the Special Offers (Special Offer Page) for the given Retailer Location.
Example					: usp_HcRetailerLocationSpecialOffersDisplay

History
Version		Date			Author				Change Description
--------------------------------------------------------------- 
1.0			21st Jan 2014    Pavan Sharma K		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcSpecialOffersRetailersDisplay]
(
	  
	  @UserID int
    , @HcHubCitiID Int
    , @LowerLimit int
    , @ScreenName varchar(100)   
	, @Latitude Decimal(18,6)
	, @Longitude Decimal(18,6)
	, @PostalCode varchar(20)
   
	--Output Variable 
	, @Status int output   
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output  
	, @NxtPageFlag bit output
	, @MaxCnt int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
				
				DECLARE @UserIDs int 
				SELECT @UserIDs = @UserID
				DECLARE @HcHubCitiIDs Int
				SELECT @HcHubCitiIDs = @HcHubCitiID
				DECLARE @Latitudes Decimal(18,6)
				SELECT @Latitudes = @Latitude
				DECLARE @Longitudes Decimal(18,6)
				SELECT @Longitudes = @Longitude
				DECLARE  @PostalCodes varchar(20)
				SELECT @PostalCodes = @PostalCode

				DECLARE @QRType INT
				, @CodeConfig VARCHAR(100)
				, @QRTypeCode INT			      
				, @UpperLimit int  
				, @RetailConfig VARCHAR(1000)
				, @Radius INT
				, @DistanceFromUser FLOAT
				, @UserLatitude float
				, @UserLongitude float 

				SELECT @UserLatitude = @Latitudes
					 , @UserLongitude = @Longitudes

				IF (@UserLatitude IS NULL) 
				BEGIN
					SELECT @UserLatitude = Latitude
						 , @UserLongitude = Longitude
					FROM HcUser A
					INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
					WHERE HcUserID = @UserIDs 
				END

				
				SELECT @RetailConfig=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='Web Retailer Media Server Configuration'	 
				--DECLARE @MaxCnt int
				
			   --To get the row count for pagination.  	           
			   SELECT @UpperLimit = @LowerLimit + ScreenContent         
			   FROM AppConfiguration         
			   WHERE ScreenName = @ScreenName         
			   AND ConfigurationType = 'Pagination'        
			   AND Active = 1       
				
				--Retrieve the server configuration.
				SELECT @CodeConfig = ScreenContent 
				FROM AppConfiguration 
				WHERE ConfigurationType LIKE 'QR Code Configuration'
				AND Active = 1
				 
				--Capture the ID for Special Offers.
				SELECT @QRType = QRTypeID 
					 , @QRTypeCode = QRTypeCode
				FROM QRTypes WHERE QRTypeName LIKE 'Special Offer Page'	

				EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @UserIDs, @HcHubCitiIDs, @Latitudes, @Longitudes, @PostalCodes, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
				SELECT @PostalCodes = ISNULL(@DefaultPostalCode, @PostalCodes) 

				IF (@Latitudes IS NULL AND @Longitudes IS NULL) OR (@UserOutOfRange = 1)
				BEGIN
					IF @PostalCodes IS NOT NULL
					BEGIN
						SELECT @Latitudes = G.Latitude
							 , @Longitudes = G.Longitude
						FROM GeoPosition G
						WHERE PostalCode = @PostalCodes
					END
					ELSE
					BEGIN
						SELECT @Latitudes = G.Latitude
							 , @Longitudes = G.Longitude
						FROM GeoPosition G
						INNER JOIN HCUSER H ON H.PostalCode = G.PostalCode
						WHERE H.HcUserID = @UserIDs
					END
				END
				
				DECLARE @LocCategory Int		
			
				SELECT DISTINCT BusinessCategoryID 
				INTO #UserLocCategories
				FROM HcUserPreferredCategory 
				WHERE HcUserID=@UserIDs AND HcHubCitiID =@HcHubCitiIDs 

				SELECT @LocCategory=COUNT(1) FROM #UserLocCategories 

				SELECT @Radius = ScreenContent
				FROM AppConfiguration
				WHERE ScreenName = 'DefaultRadius'

				SELECT RowNum = ROW_NUMBER() OVER (ORDER BY  Distance, RetailName ASC)
					 , RetailID
					 , RetailName
					 , RetailerImagePath
					 , RetailLocationID
					 , Address1
					 , City
					 , State
					 , PostalCode
					 , Distance
					 , DistanceActual
				INTO #SpecialOffers
				FROM  
				(
					SELECT DISTINCT  R.RetailID
								  , R.RetailName
								  --, RetailerImagePath = @RetailConfig + CAST(R.RetailID AS VARCHAR(100)) + '/' + R.RetailerImagePath
								  , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@CodeConfig+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
								  --, RetailerImagePath=IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@CodeConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
								  , RL.RetailLocationID
								  , RL.Address1
								  , RL.City
								  , RL.State
								  , RL.PostalCode
								  , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitudes / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitudes / 57.2958) * COS((@Longitudes / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
								  , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
					FROM QRRetailerCustomPage QR
					INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QR.QRRetailerCustomPageID
					INNER JOIN RetailLocation RL ON RL.RetailLocationID=QRRCPA.RetailLocationID AND RL.Active = 1 
					INNER JOIN Retailer R ON R.RetailID = QRRCPA.RetailID AND R.RetailerActive=1
					INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
					INNER JOIN HcLocationAssociation HL ON HL.Postalcode=RL.Postalcode AND HL.HcHubCitiID = @HcHubCitiIDs
					INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiIDs AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
					LEFT JOIN RetailerBusinessCategory RB ON RB.RetailerID =QRRCPA.RetailID 
					--LEFT JOIN HcUserPreferredCategory US ON US.BusinessCategoryID =RB.BusinessCategoryID 
					LEFT JOIN HcUserPreferredCategory UP ON UP.HcUserID=@UserIDs --AND UP.HcHubCitiID =@HcHubCitiIDs  
					WHERE QRTypeID = @QRType 
					--AND (GETDATE() BETWEEN ISNULL(StartDate, GETDATE() - 1) AND ISNULL(EndDate, GETDATE() + 1))
					--AND ((UP.HcUserPreferredCategoryID IS NOT NULL AND UP.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID=QR.RetailID) OR (P.HcUserPreferredCategoryID IS NULL
					AND ((UP.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID=QR.RetailID) OR (UP.BusinessCategoryID IS NULL))
					AND ((StartDate IS NULL AND EndDate IS NULL) OR (GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)))
					--AND ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitudes / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitudes / 57.2958) * COS((@Longitudes / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius
					--AND (@LocCategory=0 OR (US.BusinessCategoryID IS NOT NULL AND US.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID =QRRCPA.RetailID))
					
					--FROM HcUserPreferredCategory PC
					--INNER JOIN RetailerBusinessCategory BC ON PC.BusinessCategoryID = BC.BusinessCategoryID
					--INNER JOIN QRRetailerCustomPageAssociation QRA ON BC.RetailerID = QRA.RetailID
					--INNER JOIN QRRetailerCustomPage P ON QRA.QRRetailerCustomPageID = P.QRRetailerCustomPageID
					--INNER JOIN RetailLocation RL ON RL.RetailLocationID=QRA.RetailLocationID AND RL.Active = 1
					--INNER JOIN Retailer R ON R.RetailID = QRA.RetailID AND  R.RetailerActive=1
					--INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
					--INNER JOIN HcLocationAssociation HL ON HL.Postalcode=RL.Postalcode AND HL.HcHubCitiID = 4173
					--INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =4173 AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
					--WHERE PC.HcUserID = @UserIDs AND QRTypeID = @QRType AND RL.Active = 1
					--AND ((StartDate IS NULL AND EndDate IS NULL) OR (GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)))
				 --   AND ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitudes / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitudes / 57.2958) * COS((@Longitudes / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius
					
				)A
				ORDER BY  DistanceActual, RetailName ASC
				
				SELECT DISTINCT  RowNum 
					 , S.RetailID
					 , RetailName
					 , RetailerImagePath
					 , S.RetailLocationID
					 , S.Address1
					 , S.City
					 , S.State
					 , S.PostalCode
					 , Distance
					 , DistanceActual
				INTO #Specials
				FROM #SpecialOffers S
				INNER JOIN RetailLocation RL ON S.RetailLocationID=rl.RetailLocationID AND RL.Active=1
				INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
				WHERE ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitudes / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitudes / 57.2958) * COS((@Longitudes / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius					

				--To capture max row number.        
				SELECT @MaxCnt = MAX(RowNum) FROM #Specials
				
				 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
				
				SELECT  RowNum rowNumber
					 , RetailID retailerId
					 , RetailName retailerName
					 , RetailerImagePath retailImgPath
					 , RetailLocationID retailLocationId
					 , Address1 retaileraddress1
					 , City
					 , State
					 , PostalCode
					 , distance = ISNULL(DistanceActual, Distance)
				FROM #Specials
				WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit  
				
			--To get list of BottomButtons for this Module
		 
		  --To get Bottom Button Image URl
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

            DECLARE @ModuleName Varchar(50) 
            SET @ModuleName = 'Deals'					
               
			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
				 , BB.HcBottomButtonID AS bottomBtnID
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HcHubCitiIDs AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HcHubCitiIDs AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HcHubCitiIDs), BottomButtonLinkID) 
				 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiIDs ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiIDs)
										  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
								                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HcHubCitiIDs) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																					            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																					            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiIDs)
			                      	 
			                       ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HcHubCitiIDs
			ORDER by HcFunctionalityBottomButtonID				
								
			--Confirmation of Success.
			SELECT @Status = 0	
		COMMIT TRANSACTION		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
































GO
