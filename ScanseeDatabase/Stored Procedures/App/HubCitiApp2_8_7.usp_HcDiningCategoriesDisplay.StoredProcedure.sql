USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcDiningCategoriesDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcDiningCategoriesDisplay]
Purpose					: To display dining Categories list.
Example					: [usp_HcDiningCategoriesDisplay]

History
Version		  Date		      Author	 Change Description
--------------------------------------------------------------- 
1.0		   20th Nov 2013	   SPAN	           1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcDiningCategoriesDisplay]
(
	--Input Variables
	  @HubCitiID int

	--Output Variables
	, @Status int output
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
  
)
AS
BEGIN

	BEGIN TRY
		
		--To display list of Dining Categories for given HubCiti.
		SELECT DISTINCT BSC.HcBusinessSubCategoryID catId
					   ,BSC.BusinessSubCategoryName catName
		FROM HcBusinessSubCategory BSC
		INNER JOIN HcRetailerSubCategory RSC ON BSC.HcBusinessSubCategoryID = RSC.HcBusinessSubCategoryID
		INNER JOIN RetailLocation RL ON RSC.RetailLocationID = RL.RetailLocationID
		INNER JOIN HcLocationAssociation LA ON RL.PostalCode = LA.PostalCode
		INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
		WHERE  LA.HcHubCitiID = @HubCitiID AND RL.Active = 1
		ORDER BY BSC.BusinessSubCategoryName

	
		--Confirmation of Success
		SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcDiningCategoriesDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

















































GO
