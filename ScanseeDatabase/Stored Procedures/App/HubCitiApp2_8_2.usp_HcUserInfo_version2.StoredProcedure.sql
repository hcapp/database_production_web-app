USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserInfo_version2]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcUserInfo]
Purpose					: To capture HubCiti User information
Example					: [usp_HcUserInfo] 

History
Version		     Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			9th sept 2013	  SPAN	        1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserInfo_version2]
(
	 @HubCitiID INT 
	,@UserID INT
	,@FirstName VARCHAR(20)
	,@Lastname VARCHAR(30)
	,@PostalCode VARCHAR(10)
	,@Gender BIT
	,@DOB DATE 
	,@MobilePhone CHAR(10)
    ,@DeviceID VARCHAR(60)
    ,@UniversityIDs VARCHAR(1000)
    ,@Email VARCHAR(500)
	,@IncomeRangeID int
	,@MaritalStatusID int
	,@EducationLevelID int
    
    --OutPut Variable
	, @ExistsEmail BIT OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
					
			SELECT @ExistsEmail= 1

			IF EXISTS (SELECT 1 FROM hcuser H  INNER JOIN HcUserDeviceAppVersion D ON D.HcUserID = H.HcUserID  WHERE H.HcUserID = @UserID  AND HcHubCitiID = @HubCitiID)
			BEGIN
				IF NOT EXISTS(SELECT 1 FROM HcUser WHERE Email = @Email AND HcUserID <> @UserID)
				 BEGIN 
					 UPDATE HcUser 
					 SET FirstName = @FirstName 
						,Lastname = @Lastname 
						,PostalCode = @PostalCode 
						,Gender = @Gender
						,DateOfBirth = @DOB 
						,PhoneNO =  @MobilePhone 
						,FirstUseComplete = 1 
						,DateModified = GETDATE() 
						,Email = @Email 
					 WHERE HcUserID = @UserID 

					 SELECT @ExistsEmail = 0
				 END
				 ELSE 
					SELECT @ExistsEmail = 1
			END
						
			IF EXISTS (SELECT 1 FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID AND HcHubCitiID = @HubCitiID)
			BEGIN
				UPDATE HcUserDeviceAppVersion 
				SET DeviceID = @DeviceID 
					,DateModified = GETDATE()
				WHERE HcUserID = @UserID 
			END
			ELSE
			BEGIN
				INSERT INTO HcUserDeviceAppVersion(HcUserID
												   ,HcHubCitiID
												   ,DeviceID
												   ,DateCreated
												   ,DateModified)
											VALUES(@UserID 
												   ,@HubCitiID
												   ,@DeviceID			   
												   ,GETDATE()
												   ,GETDATE())
			END		
					
					
			IF EXISTS (SELECT 1 FROM HcUserUniversity WHERE HcUserID = @UserID)
			BEGIN
				IF(@UniversityIDs IS NULL)
				BEGIN
					DELETE FROM HcUserUniversity WHERE HcUserID = @UserID
				END
				ELSE
				BEGIN
					UPDATE HcUserUniversity
					SET UniversityID = @UniversityIDs
						,DateModified = GETDATE()
					WHERE HcUserID = @UserID 
				END						
			END
			ELSE
			BEGIN
			--INSERT NEW UNIVERSITY IDs.
				INSERT INTO HcUserUniversity(HcUserID
											 ,UniversityID
											 ,DateCreated
											 ,DateModified)
									 VALUES(@UserID
											,@UniversityIDs
											,GETDATE()
											,GETDATE())
			END			
			
			IF EXISTS (SELECT 1 FROM HcUserDemographic WHERE HcUserID = @UserID)
			BEGIN
				UPDATE HcUserDemographic
				 SET IncomeRangeID = @IncomeRangeID
					,EducationLevelID = @EducationLevelID
					,MaritalStatusID = @MaritalStatusID
					,DateModified = GETDATE()
				WHERE HcUserID = @UserID
			END
			ELSE 
			BEGIN
				INSERT INTO HcUserDemographic(HcUserID, IncomeRangeID, EducationLevelID, MaritalStatusID, DateCreated)
				VALUES(@UserID, @IncomeRangeID, @EducationLevelID, @MaritalStatusID, GETDATE())
			END

			 
			--Confirmation of Success
			SELECT @Status = 0
		    COMMIT TRANSACTION
		
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcUserInfo].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
