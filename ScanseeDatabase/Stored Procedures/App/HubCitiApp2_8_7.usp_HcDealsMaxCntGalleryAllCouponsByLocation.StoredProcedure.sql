USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcDealsMaxCntGalleryAllCouponsByLocation]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcGalleryAllCouponsByLocation
Purpose					: To Display All Coupons By Location
Example					: usp_HcGalleryAllCouponsByLocation

History
Version		 Date		     Author	           Change Description
------------------------------------------------------------------------------- 
1.0		     4/28/2015	     Span					1.0
2.0        12/23/2016    Shilpashree       Introducing Sort & filter items
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcDealsMaxCntGalleryAllCouponsByLocation]
(

	--Input Variables 	
	   @HcUserID int
	 , @HcHubcitiID int
	 , @SearchKey varchar(500)
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(500)	
	 
	--Output Variables
	 , @MaxCnt int output

)
AS
BEGIN

	BEGIN TRY		
			
		 DECLARE @UserOutOfRange bit 
				, @DefaultPostalCode varchar(50)
				, @DistanceFromUser FLOAT 
				, @Status bit 
				, @ErrorNumber int 
				, @ErrorMessage varchar(1000)
				, @Radius int 
				, @LocCategory int 
				, @UserLatitude float
				, @UserLongitude float

		CREATE TABLE #RetailerBusinessCategory(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)
		CREATE TABLE #FilteredCoupons(CouponID int, HcHubCitiID int, RetailID int, RetailLocationID int,NoOfCouponsToIssue int,UsedCoupons int)
		CREATE TABLE #Coupons(CouponID INT
								, CouponName VARCHAR(500)
								, RetailID INT
								, RetailName VARCHAR(200)
								, CouponImagePath VARCHAR(500)
								, BannerTitle VARCHAR(1000)
								, Distance FLOAT
								, Counts INT
								)

		SET @UserLatitude = @Latitude
        SET @UserLongitude = @Longitude
		
		SELECT @Radius = LocaleRadius
			FROM HcUserPreference 
				WHERE HcUserID = @HcUserID

		IF (@Radius IS NULL)
		BEGIN

			SELECT @Radius=ScreenContent 
				FROM AppConfiguration
					WHERE ScreenName = 'DefaultRadius' AND ConfigurationType = 'DefaultRadius'	
		END
		
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
		END
            --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID
		END	

		EXEC [HubCitiapp2_8_7].[usp_HcUserHubCitiRangeCheck] @HcUserID, @HcHubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
		
		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
						, @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
						, @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @HcUserID
			END
		END	

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
			FROM Retailer 
				WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive = 1

		SELECT DISTINCT BusinessCategoryID,HcBusinessSubCategoryID 
		INTO #UserPrefLocCategories
			FROM HcUserPreferredCategory 
				WHERE HcUserID = @HcUserID AND HcHubCitiID = @HcHubcitiID 

		SELECT @LocCategory = COUNT(1) FROM #UserPrefLocCategories 

		IF (@LocCategory = 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
					WHERE RA.HcHubCitiID = @HcHubcitiID
		END
		ELSE IF (@LocCategory > 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
				LEFT JOIN #UserPrefLocCategories PC ON RBC.BusinessCategoryID = PC.BusinessCategoryID AND RBC.BusinessSubCategoryID = PC.HcBusinessSubCategoryID
					WHERE RA.HcHubCitiID = @HcHubcitiID
		END

		--INSERT HubCiti Coupons
		INSERT INTO #FilteredCoupons(CouponID,HcHubCitiID,RetailID,RetailLocationID,NoOfCouponsToIssue,UsedCoupons)
		SELECT DISTINCT C.CouponID
						,C.HcHubCitiID
						,CR.RetailID
						,CR.RetailLocationID
						,NoOfCouponsToIssue
						,UsedCoupons = COUNT (HCUserCouponGalleryID)
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID 
			INNER JOIN HcRetailerAssociation RA ON CR.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubcitiID AND RA.Associated = 1 
			LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
				WHERE C.HcHubCitiID = @HcHubcitiID AND C.RetailID IS NULL
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
					GROUP BY C.CouponID
							,C.HcHubCitiID
							,CR.RetailID
							,CR.RetailLocationID
							,NoOfCouponsToIssue
						HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
	
		--INSERT HubCiti Associted Retailer Coupons
		INSERT INTO #FilteredCoupons(CouponID,RetailID,RetailLocationID,NoOfCouponsToIssue,UsedCoupons)
		SELECT DISTINCT C.CouponID
						,CR.RetailID
						,CR.RetailLocationID
						,NoOfCouponsToIssue
						,UsedCoupons = COUNT (HCUserCouponGalleryID)
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID AND C.RetailID = CR.RetailID
			INNER JOIN HcRetailerAssociation RA ON CR.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubcitiID AND RA.Associated = 1 
			LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
				WHERE C.HcHubcitiID IS NULL
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
					GROUP BY C.CouponID
							,CR.RetailID
							,CR.RetailLocationID
							,NoOfCouponsToIssue
						HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
		
		SELECT DISTINCT FC.CouponID
					,R.RetailID
					,R.RetailName
					,FC.RetailLocationID
					,RL.HcCityID
					,RL.PostalCode
					,RL.RetailLocationLatitude
					,RL.RetailLocationLongitude
					,RBC.BusinessCategoryID
					,Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
					,DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)    
		INTO #RetailerSet
		FROM #FilteredCoupons FC
		INNER JOIN #RetailerBusinessCategory RBC  ON FC.RetailID = RBC.RetailerID
		INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1
		INNER JOIN Retaillocation RL ON R.RetailID = RL.RetailID AND FC.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
		INNER JOIN HcLocationAssociation HL ON RL.HcCityID = HL.HcCityID AND RL.PostalCode = HL.PostalCode AND HL.HcHubCitiID = @HcHubcitiID
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.Postalcode AND G.City = HL.City
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL
	
		SELECT DISTINCT rowNumber = IDENTITY(INT,1,1)
					, C.CouponID
					, C.CouponName
					, R.RetailID 
					, RetailName 
					, R.RetailLocationID
					, C.BannerTitle
					, Distance
		INTO #CouponsList
		FROM #RetailerSet R
		INNER JOIN Coupon C ON R.CouponID = C.CouponID  
			WHERE Distance <= @Radius
			AND  (C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
				OR C.KeyWords LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END)
			
				   
	--To capture max row number.  
	SELECT @MaxCnt = MAX(rowNumber) FROM #CouponsList  
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcDealsMaxCntGalleryAllCouponsByLocation].'		
		END;
		 
	END CATCH;
END;



GO
