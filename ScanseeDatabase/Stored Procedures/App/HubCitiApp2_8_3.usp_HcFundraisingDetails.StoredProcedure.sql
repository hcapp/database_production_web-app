USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFundraisingDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFundraisingDetails
Purpose					: To display Fundraising Details.
Example					: usp_HcFundraisingDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25thAug2014     Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFundraisingDetails]
(   
    --Input variable.	  
	  @HcFundraisingID Int

	--User Tracking
	, @FundraisingListID Int
	, @HcHubCitiID Int
  
	--Output Variable 
	, @AppSiteFlag bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @HcEventCategoryID Varchar(2000)
			DECLARE @AppSiteID Int
			DECLARE @RetailLocationID Varchar(2000)
			
			DECLARE @Config VARCHAR(500)

			DECLARE @CategoryNames VARCHAR(1000)
			DECLARE @RetailConfig Varchar(1000)
			SET @AppSiteFlag=0

			SELECT @RetailConfig = ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'	

	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'


			--To display Fundraising Details.
			SELECT DISTINCT FundraisingName fundName  
					        ,FundraisingOrganizationName orgztnHosting
							,F.ShortDescription sDescription
							,F.LongDescription lDescription								
							,F.HcHubCitiID hubCitiId 
							--,imgPath=(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath)								  						
							,imgPath=IIF(F.RetailID IS NULL,(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(F.RetailID AS VARCHAR)+'/'+F.FundraisingOrganizationImagePath)                                                                                 
							,sDate =CAST(DATEPART(MM,F.StartDate) AS VARCHAR(10))+'/'+CAST(DATEPART(DD,F.StartDate) AS VARCHAR(10))+'/'+CAST(DATEPART(YY,F.StartDate) AS VARCHAR(10))--CAST(StartDate AS DATE)
							,eDate =CAST(DATEPART(MM,F.EndDate) AS VARCHAR(10))+'/'+CAST(DATEPART(DD,F.EndDate) AS VARCHAR(10))+'/'+CAST(DATEPART(YY,F.EndDate) AS VARCHAR(10))--CAST(EndDate AS DATE)								 								 
							,F.MoreInformationURL moreInfoURL
							,PurchaseProductURL purchaseProducts
							,FundraisingGoal fundGoal
							,CurrentLevel currentLevel	
							,isEventFlag=IIF(FE.HcFundraisingID  IS NOT NULL AND FE.HcEventID IS NOT NULL AND E.HcEventID IS NOT NULL,1,0)
							,title = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName													  	  											
			FROM HcFundraising F
			LEFT JOIN HcFundraisingEventsAssociation FE ON F.HcFundraisingID =FE.HcFundraisingID 
			LEFT JOIN HcEvents E ON FE.HcEventID = E.HcEventID AND (GETDATE() <= ISNULL(E.EndDate,GETDATE()+1)) AND E.Active = 1 
			WHERE F.HcFundraisingID =@HcFundraisingID
			
			
			--Setting the Appsite FLag (if present = 1) 	
			--SELECT @AppSiteFlag = IIF(FundraisingOrganizationName IS NULL,1,0) 
			--FROM HcFundraising 
			--WHERE HcFundraisingID =@HcFundraisingID 

			--User Tracking

			UPDATE HubCitiReportingDatabase..FundraisingList SET FundraisingClick =1
			WHERE FundraisingListID  =@FundraisingListID 
				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcFundraisingDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
