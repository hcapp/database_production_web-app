USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcCityExperienceCategoryList_08232016bfemovfromQA]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : [usp_HcCityExperienceFilterList]
Purpose                    : To display CityExperience Category listing screen.
Example                    : [usp_HcCityExperienceFilterList]

History
Version      Date                Author       Change Description
--------------------------------------------------------------- 
1.0          08 May 2015         Mohith H R       1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcCityExperienceCategoryList_08232016bfemovfromQA]
(
      --Input variable.
	     @UserID int
	   , @HcHubCitiID Int
	   , @CityExperienceID int
	   --, @CategoryID Varchar(100) --Comma Separated Category IDs
       , @SearchKey varchar(255)
       --, @LowerLimit int  
       --, @ScreenName varchar(50)
       , @Latitude decimal(18,6)    
       , @Longitude decimal(18,6)          
	   , @CityID Varchar(1000) --Comma Separated CityIDs	  
       
     
      --Output Variable  	    
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
                     
					 DECLARE @UserID1 int = @UserID
					 DECLARE @HcHubCitiID1 int = @HcHubCitiID
					 DECLARE @CityExperienceID1 int = @CityExperienceID

					 DECLARE @UserOutOfRange bit 
					 DECLARE @DefaultPostalCode VARCHAR(10) 
				     DECLARE @RetailAffiliateCount int    

					 DECLARE @RegionAppID int
					 DECLARE @HcAppListID int

					 SELECT @HcAppListID = HcAppListID
					 FROM HcApplist
					 WHERE HcAppListName = 'RegionApp'

					 SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
					 FROM HcHubCiti H
					 WHERE HcHubCitiID = @HcHubCitiID1
					 
                     DECLARE @Config VARCHAR(100)   
					 DECLARE @ZipCode varchar(10) 
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @ModuleName varchar(100) = 'Experience'
                     
                     DECLARE @Globalimage varchar(50)
                     DECLARE @CityExpDefaultConfig varchar(50)

                     DECLARE @Tomorrow DATETIME = GETDATE() + 1
					 DECLARE @Yesterday DATETIME = GETDATE() - 1
                     
                     SELECT @Globalimage =ScreenContent 
                     FROM AppConfiguration 
                     WHERE ConfigurationType ='Image Not Found'
               
                     SELECT @Config=ScreenContent
                     FROM AppConfiguration 
                     WHERE ConfigurationType='App Media Server Configuration'
                     
                     SELECT @CityExpDefaultConfig = ScreenContent
                     FROM AppConfiguration 
                     WHERE ConfigurationType = 'City Experience Default Image Path'
                     AND Active = 1
                     
                     DECLARE @RetailConfig varchar(50)
                     SELECT @RetailConfig=ScreenContent
                     FROM AppConfiguration 
                     WHERE ConfigurationType='Web Retailer Media Server Configuration'
                     
                     --To get the row count for pagination.  
                     --DECLARE @UpperLimit int   
                     --SELECT @UpperLimit = @LowerLimit + ScreenContent   
                     --FROM AppConfiguration   
                     --WHERE ScreenName = @ScreenName 
                     --AND ConfigurationType = 'Pagination'
                     --AND Active = 1       
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     
                     SELECT  @UserLatitude = @Latitude
                            ,@UserLongitude = @Longitude

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcUser A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE HcUserID = @UserID1 
                     END

                     --Pick the co ordinates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HcHubCitiID1
                     END

					 print @UserLatitude
                     print @UserLongitude
                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
                                                               

                                         
                    --To identify Retailer that have products on Sale or any type of discount
                    SELECT DISTINCT Retailid , RetailLocationid
                    INTO #RetailItemsonSale
                    FROM 
                    (SELECT b.RetailID, a.RetailLocationID 
                    FROM RetailLocationDeal a 
                    INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID AND b.Active = 1
                    INNER JOIN HcLocationAssociation HL ON b.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = B.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                               
                    INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                                                            and a.ProductID = c.ProductID
                                                                            and GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) and ISNULL(a.SaleEndDate, @Tomorrow)
                    UNION ALL 
                    SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                    FROM Coupon C 
                    INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
                    INNER JOIN RetailLocation RL ON RL.RetailID = CR.RetailID AND RL.Active = 1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                   
                    LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                    WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                    GROUP BY C.CouponID
                                ,NoOfCouponsToIssue
                                ,CR.RetailID
                                ,CR.RetailLocationID
                    HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                                ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                          
                    UNION ALL  

                    select  RR.RetailID, 0 as RetaillocationID  
                    from Rebate R 
                    INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
                    INNER JOIN RetailLocation RL ON RL.RetailID = RR.RetailID AND RL.Active = 1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                                
                    WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

                    UNION ALL  

                    SELECT  c.retailid, a.RetailLocationID 
                    FROM  LoyaltyDeal a
                    INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
                    INNER JOIN RetailLocation c on a.RetailLocationID = c.RetailLocationID AND c.Active = 1
                    INNER JOIN HcLocationAssociation HL ON c.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1              
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = C.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    INNER JOIN RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
                                                                    and b.ProductID = LDP.ProductID 
                    WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                    UNION ALL 

                    SELECT DISTINCT rl.RetailID, rl.RetailLocationID
                    FROM ProductHotDeal p
                    INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                    INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active = 1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                                                                                   
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                    LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                    WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
                    GROUP BY P.ProductHotDealID
                                ,NoOfHotDealsToIssue
                                ,rl.RetailID
                                ,rl.RetailLocationID
                    HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                                ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                    UNION ALL 

                    select q.RetailID, qa.RetailLocationID
                    from QRRetailerCustomPage q
                    INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                    INNER JOIN RetailLocation RL ON RL.RetailLocationID=qa.RetailLocationID AND RL.Active = 1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                     
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                    where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
                    ) Discount    
                     
                     
                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @ZipCode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                  WHERE U.HcUserID = @UserID1
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @ZipCode
                           END
                     END
                    
					 DECLARE @UserPreferredCity bit 
					 SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID1 AND HcCityID IS NULL THEN 0
													    WHEN HcUserID = @UserID1 AND HcCityID IS NOT NULL THEN 1
													    ELSE 0 END
					 FROM HcUsersPreferredCityAssociation
					 WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1

					 SELECT @UserPreferredCity = ISNULL(@UserPreferredCity,0)

					 CREATE TABLE #Retail(RetailID  int  	
									, RetailLocationID int	
									, RetailName varchar(1000)							
									, City varchar(500)   									
									, SaleFlag int									
									, BusinessCategoryID  int									
									, HcCityID int)

					 --SELECT Param BusCatIDs
					 --INTO #BusinessCategoryIDs
					 --FROM fn_SplitParam (@CategoryID,',')


                     --Filter the retailers who belong to the given Group.
					
					 IF @CityID IS NULL

					 BEGIN

					 insert into #Retail(RetailID
								    , RetailName
					                , RetailLocationID   																		
									, City    									
									, SaleFlag									
									, BusinessCategoryID) 																		
							 SELECT  
							          RetailID    		
									, RetailName							
									, RetailLocationID																		
									, City    									
									, SaleFlag									
									, BusinessCategoryID 																								 
							 FROM
							 (SELECT DISTINCT TOP 100 PERCENT R.RetailID     										
										, R.RetailName	
										, RL.RetailLocationID 																				
										, RL.City										
										, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END										
										, BC.BusinessCategoryID 																				
						  FROM Retailer R    
						  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID   
						  INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode
						  INNER JOIN HcCity C ON HL.HcCityID = C.HcCityID                                   
						  INNER JOIN HcCityExperienceRetailLocation CE ON CE.RetailLocationID = RL.RetailLocationID AND Headquarters = 0                                                                                  
						  INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						  INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						  --LEFT JOIN HcFilterRetailLocation FRL ON CE.RetailLocationID = FRL.RetailLocationID
						  LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						  WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND RL.Active =1 AND R.RetailerActive = 1 
						  AND @UserPreferredCity = 0 AND CE.HcCityExperienceID = @CityExperienceID1
						  AND ((@CityID IS NULL AND 1=1) OR (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityID,',')))) 
						  --AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
								--	OR
							 --  (ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiapp2_8_7].fn_SplitParam(@CategoryID, ','))))
						  --AND ((@CategoryID IS NULL AND 1=1) OR (RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs)))
						  AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END

						  UNION 

						  SELECT DISTINCT TOP 100 PERCENT R.RetailID     										
										, R.RetailName	
										, RL.RetailLocationID 																		
										, RL.City																														
										, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END										
										, BC.BusinessCategoryID 																				
						FROM HcHubCiti H
						INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
						INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
						INNER JOIN HcUsersPreferredCityAssociation UC ON (C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1) 
						INNER JOIN RetailLocation RL ON LA.PostalCode =RL.PostalCode AND Headquarters = 0 
						INNER JOIN Retailer R ON RL.RetailID = R.RetailID AND R.RetailerActive = 1
						INNER JOIN HcCityExperienceRetailLocation CER ON CER.RetailLocationID = RL.RetailLocationID AND CER.HcCityExperienceID = @CityExperienceID1                                                                                    
						INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						--LEFT JOIN HcFilterRetailLocation FRL ON CER.RetailLocationID = FRL.RetailLocationID
						LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						WHERE @RegionAppID = 1  AND RL.Active =1  AND @UserPreferredCity = 1 AND H.HcHubCitiID  = @HcHubCitiID1
						AND ((@CityID IS NULL AND 1=1) OR (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityID,',')))) --@CityID IS NOT NULL AND
						--AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
						--	OR
						--	(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiapp2_8_7].fn_SplitParam(@CategoryID, ','))))
						--AND ((@CategoryID IS NULL AND 1=1) OR (RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs)))
						AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END

						)Retailer		
				END
				ELSE
				BEGIN
				insert into #Retail(  RetailID    
									, RetailName
									, RetailLocationID																		
									, City    									
									, SaleFlag									
									, BusinessCategoryID) 								
									
							SELECT    RetailID    		
							        , RetailName							
							        , RetailLocationID									
									, City    									   									
									, SaleFlag									
									, BusinessCategoryID 									
							 FROM
							 (SELECT DISTINCT TOP 100 PERCENT R.RetailID     										
							            , R.RetailName
										, RL.RetailLocationID 																				
										, RL.City										
										, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END										
										, BC.BusinessCategoryID 										
							  FROM Retailer R    
							  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND R.RetailerActive = 1  
							  INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode
							  INNER JOIN HcCity C ON HL.HcCityID = C.HcCityID                                   
							  INNER JOIN HcCityExperienceRetailLocation CE ON CE.RetailLocationID = RL.RetailLocationID AND Headquarters = 0                                                                                  
							  INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
							  INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
							  --LEFT JOIN HcFilterRetailLocation FRL ON CE.RetailLocationID = FRL.RetailLocationID
							  LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
							  WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND RL.Active =1 
							  AND @UserPreferredCity = 0 AND CE.HcCityExperienceID = @CityExperienceID1
							  AND ((@CityID IS NULL AND 1=1) OR (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityID,',')))) 
							  --AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
							  --OR(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiapp2_8_7].fn_SplitParam(@CategoryID, ','))))
							  --AND ((@CategoryID IS NULL AND 1=1) OR (RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs)))
							  AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END

						  UNION 

						  SELECT DISTINCT TOP 100 PERCENT R.RetailID 
						                , R.RetailName    										
										, RL.RetailLocationID 										
										, RL.City										
										, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END										
										, BC.BusinessCategoryID 										
						FROM HcHubCiti H
						INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
						INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
						left JOIN HcUsersPreferredCityAssociation UC ON (C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1) 
						INNER JOIN RetailLocation RL ON LA.PostalCode =RL.PostalCode AND Headquarters = 0 
						INNER JOIN Retailer R ON RL.RetailID = R.RetailID 
						INNER JOIN HcCityExperienceRetailLocation CER ON CER.RetailLocationID = RL.RetailLocationID AND CER.HcCityExperienceID = @CityExperienceID1                                                                                    
						INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						--LEFT JOIN HcFilterRetailLocation FRL ON CER.RetailLocationID = FRL.RetailLocationID
						LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						WHERE @RegionAppID = 1 AND RL.Active =1 AND R.RetailerActive = 1
						AND @UserPreferredCity = 1 AND H.HcHubCitiID  = @HcHubCitiID1
						AND ((@CityID IS NULL AND 1=1) OR (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityID,',')))) --@CityID IS NOT NULL AND
						--AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
						--	OR
						--	(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiapp2_8_7].fn_SplitParam(@CategoryID, ','))))
						--AND ((@CategoryID IS NULL AND 1=1) OR (RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs)))
						AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END

						)Retailer						
				END
		
                                                            
                --Options Listing
																		
				SELECT DISTINCT B.BusinessCategoryID busCatId
							   ,B.BusinessCategoryName busCatName                
				FROM #Retail R
				INNER JOIN BusinessCategory B ON R.BusinessCategoryID = B.BusinessCategoryID	
				WHERE BusinessCategoryName <> 'Other'
				ORDER BY BusinessCategoryName			
				
				                              
                --Confirmation of Success
                SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure [usp_HcCityExperienceFilterList].'            
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;

















































GO
