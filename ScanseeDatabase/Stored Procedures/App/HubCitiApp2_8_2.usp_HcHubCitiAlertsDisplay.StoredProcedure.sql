USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiAlertsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiAlertsDisplay
Purpose					: To display List of Alerts Severity.
Example					: usp_HcHubCitiAlertsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    SPAN			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiAlertsDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
	, @UserID Int
	, @LowerLimit int  
	, @ScreenName varchar(50)
	
	--UserTracking  
	, @MainMenuID int
  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output
	, @AlertImageRed varchar(1000) output
	, @AlertImageGreen varchar(1000) output
	, @AlertImageYellow varchar(1000) output
	--, @AlertImageGrey varchar(1000) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
			DECLARE @Config VARCHAR(500)			
			
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT @AlertImageRed = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'AlertSeverityImageRed'
			
			SELECT @AlertImageGreen = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'AlertSeverityImageGreen'
			
			SELECT @AlertImageYellow = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'AlertSeverityImageYellow'
			
			--SELECT @AlertImageGrey = ScreenContent
			--FROM AppConfiguration
			--WHERE ConfigurationType = 'AlertSeverityImageGrey'
				
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   

			--To display List fo Alerts Severity
			SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY A.HCAlertID) AS Rownum
			               ,A.HcAlertID			               
			               ,HcAlertName 
			               ,ShortDescription						
						   ,A.HcHubCitiID 
						   ,S.HcSeverityID 
						   ,ImagePath=@Config+S.ImagePath 
						   ,StartDate = CAST(StartDate AS DATE)
						   ,EndDate = CAST(EndDate AS DATE)
						   ,StartTime = CAST(StartDate AS Time)
						   ,EndTime = CAST(EndDate AS Time)
						   ,AA.HcAlertCategoryID 
						   ,AA.HcAlertCategoryName 			
			INTO #HcAlertCategory										  											
			FROM HcAlerts A	
			INNER JOIN HcAlertCategoryAssociation AC ON AC.HcAlertID =A.HcAlertID
			INNER JOIN HcAlertCategory AA ON AA.HcAlertCategoryID =AC.HcAlertCategoryID	
			INNER JOIN HcSeverity S ON S.HcSeverityID =A.HcSeverityID	
			WHERE A.HcHubCitiID=@HubCitiID --AND (( @SearchParameter IS NOT NULL AND HcAlertName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
			AND GETDATE() < EndDate
			ORDER BY HcAlertCategoryName,HcAlertName 			
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #HcAlertCategory
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 			
			
			--User Tracking Section.
		
			--Capture the impression of the alerts.
			CREATE TABLE #Temp(AlertListID INT
							 , AlertID INT)						 
			INSERT INTO HubCitiReportingDatabase..AlertList(MainMenuID
														   ,AlertID													   
														   ,DateCreated)
			OUTPUT inserted.AlertListID, inserted.AlertID INTO #Temp(AlertListID,AlertID)
												   
			SELECT @MainMenuID
				 , HcAlertID
				 , GETDATE()
			FROM #HcAlertCategory
			
			--Display the records along with the key of the tracking table.
			SELECT Rownum rowNum
			       ,AlertListID		
			       ,HcAlertID alertID		               
	               ,HcAlertName alertName
	               ,ShortDescription [Description]						
				   ,HcHubCitiID hubCitiId
				   ,HcSeverityID severityId
				   ,StartDate 
				   ,EndDate 
				   ,StartTime
				   ,EndTime
				   ,HcAlertCategoryID alertCatId
				   ,HcAlertCategoryName alertCatName
				   ,ImagePath sevImgPath			   	
			FROM #HcAlertCategory C 
			INNER JOIN #Temp T ON C.HcAlertID = T.AlertID	
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 
			ORDER BY  rowNum
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiAlertsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











































GO
