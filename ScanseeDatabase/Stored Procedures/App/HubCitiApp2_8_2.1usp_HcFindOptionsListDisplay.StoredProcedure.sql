USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[1usp_HcFindOptionsListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcFindOptionsListDisplay]
Purpose                 :  To get the list of filter options for Find Module.
Example                 :  [usp_HcFindOptionsListDisplay]
History
Version       Date         Author          Change Description
------------------------------------------------------------------------------- 
1.0       5/05/2015       SPAN            1.1
2.0       12/2/2016      Shilpashree     Re-written code for Optimization.
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_2].[1usp_HcFindOptionsListDisplay]
(
      --Input variable
         @UserID INT  
       , @CategoryName varchar(250)
       , @Latitude Decimal(18,6)
       , @Longitude  Decimal(18,6)
       , @HcHubCitiID INT
       , @HcMenuItemID INT
       , @HcBottomButtonID INT
	   , @SearchKey Varchar(1000)

      --Output Variable 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      BEGIN TRY
			SET NOCOUNT ON
		
		DECLARE @Radius int
				, @PostalCode varchar(10)
				, @Tomorrow DATETIME = GETDATE() + 1
				, @Yesterday DATETIME = GETDATE() - 1
				, @DistanceFromUser FLOAT
				, @UserLatitude decimal(18,6)
				, @UserLongitude decimal(18,6) 
				, @RegionAppFlag Bit
				, @BusinessCategoryID Int
				, @UserOutOfRange bit   
				, @DefaultPostalCode varchar(50)
				, @Length INT
				, @UserConfiguredPostalCode varchar(50)
				, @IsSubCategory bit = 0

		DECLARE @UserID1 INT = @UserID,
				@HcHubCitiID1 INT = @HcHubCitiID,
				@HcMenuItemID1 INT = @HcMenuItemID,
				@HcBottomButtonID1 INT = @HcBottomButtonID,
				@CategoryName1 VARCHAR(100) = @CategoryName
		
		SELECT @UserConfiguredPostalCode = PostalCode FROM HcUser WHERE HcUserID = @UserID1		
		SET @UserLatitude = @Latitude
		SET @UserLongitude = @Longitude
		SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
								WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
								WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
								ELSE @SearchKey END)

		CREATE TABLE #SubCategoryList(BusinessCategoryID int,SubCategoryID int)
		CREATE TABLE #HcRetailerSubCategory(RetailID int,RetailLocationID int,BusinessCategoryID int,HcBusinessSubCategoryID int)
		CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
		CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100),HcHubCitiID int)
		CREATE TABLE #RetailItemsonSale1(Retailid bigint , RetailLocationid bigint)
        CREATE TABLE #Retail(RetailID INT 
                            , RetailName VARCHAR(1000)
                            , RetailLocationID INT
                            , City VARCHAR(1000)    
                            , PostalCode VARCHAR(20)
                            , retLatitude FLOAT
                            , retLongitude FLOAT
                            , Distance FLOAT  
                            , DistanceActual FLOAT                                                   
							, SaleFlag BIT
							, HcBusinessSubCategoryID INT
                            , BusinessSubCategoryName VARCHAR(1000))
							   
		SELECT @BusinessCategoryID=BusinessCategoryID 
			FROM BusinessCategory 
				WHERE BusinessCategoryName LIKE @CategoryName1

		SELECT @IsSubCategory = 1
			FROM HcBusinessSubCategoryType T
			INNER JOIN HcBusinessSubCategory S ON T.HcBusinessSubCategoryTypeID = S.HcBusinessSubCategoryTypeID
				WHERE T.BusinessCategoryID = @BusinessCategoryID
				
		SELECT DISTINCT RBC.*,R.RetailName 
		INTO #RetailerBusinessCategory 
			FROM RetailerBusinessCategory RBC
			INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1 
				WHERE BusinessCategoryID=@BusinessCategoryID
	
		IF (@IsSubCategory = 1)
		BEGIN
			
			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcMenuFindRetailerBusinessCategories 
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcMenuItemID=@HcMenuItemID

			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcBottomButtonFindRetailerBusinessCategories  
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcBottomButonID=@HcBottomButtonID

			INSERT INTO #HcRetailerSubCategory(RetailID,RetailLocationID,BusinessCategoryID,HcBusinessSubCategoryID)
			SELECT RS.RetailID,RS.RetailLocationID,RS.BusinessCategoryID,RS.HcBusinessSubCategoryID
			FROM HcRetailerSubCategory RS
			INNER JOIN #SubCategoryList SL ON RS.BusinessCategoryID = SL.BusinessCategoryID AND RS.HcBusinessSubCategoryID = SL.SubCategoryID
			
		END

		IF(SELECT 1 FROM HcHubCiti H
                    INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                    AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
        BEGIN
				SET @RegionAppFlag = 1
									
				INSERT INTO #RHubcitiList(HcHubCitiID)
				(SELECT DISTINCT HcHubCitiID 
				FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
				UNION ALL
				SELECT  @HCHubCitiID1 AS HcHubCitiID
				)
				
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)		
				SELECT DISTINCT LA.HcCityID 
								,LA.City	
								,LA.PostalCode
								,H.HcHubCitiID	                         
				FROM #RHubcitiList H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HcHubCitiID1 
				LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1  AND UP.HcHubcitiID =H.HchubcitiID                                                      
				WHERE( UP.HcUsersPreferredCityAssociationID IS NOT NULL AND LA.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1)                                        
				OR (UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID1)
						
				--INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)		
				--SELECT DISTINCT LA.HcCityID 
				--				,LA.City	
				--				,LA.PostalCode
				--				,H.HcHubCitiID				
				--FROM #RHubcitiList H
				--INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID AND LA.HcHubCitiID = @HCHubCitiID1   						
				--INNER JOIN #CityIDs C ON LA.HcCityID=C.CityID 
        END
		ELSE
        BEGIN
				SET @RegionAppFlag =0

				INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID1 
				 
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)
				SELECT DISTINCT HcCityID 
								,City
								,PostalCode	
								,HcHubCitiID				
				FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID1                                   
		END
					
		SELECT @UserLatitude = @Latitude
			 , @UserLongitude = @Longitude

		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
		END
         
		--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID1
		END

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
		EXEC [HubCitiApp2_8_2].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFROMUser OUTPUT
		--select @DefaultPostalCode = defaultpostalcode from hchubciti where HcHubCitiid = @HcHubCitiID  and @PostalCode is null
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
		BEGIN
			SELECT @Latitude = Latitude
					, @Longitude = Longitude
			FROM GeoPosition 
			WHERE PostalCode = @PostalCode                                                                             
		END  
					
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
			FROM Retailer 
				WHERE DuplicateRetailerID IS NOT NULL
                   
		--Get the user preferred radius.
		SELECT @Radius = LocaleRadius
			FROM HcUserPreference 
				WHERE HcUserID = @UserID1
                                         
        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
											WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 
				
		SELECT DISTINCT RBC.RetailerID 
				, RBC.RetailName    
				, RL.RetailLocationID 
				, RL.City
				, RL.PostalCode
				, RetailLocationLatitude
				, RetailLocationLongitude
				, SaleFlag = 0 
				, HL.HchubcitiID 
				, RS.HcBusinessSubCategoryID
		INTO #RetailInfo1
		FROM #RetailerBusinessCategory RBC
		INNER JOIN RetailLocation RL ON RBC.RetailerID = RL.RetailID AND RL.Active = 1
		INNER JOIN #CityList HL ON RL.PostalCode=HL.PostalCode AND RL.HcCityID = HL.CityID AND HL.HcHubCitiID=@HCHubCitiID1 
		INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
		INNER JOIN HcRetailerAssociation RA ON RL.RetailID = RA.RetailID AND RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = RH.HchubcitiID
		LEFT JOIN #HcRetailerSubCategory RS ON RL.RetailLocationID = RS.RetailLocationID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
		
		SELECT * INTO #RetailInfo FROM #RetailInfo1 WHERE 1=2

		IF(@IsSubCategory = 1)
		BEGIN
				INSERT INTO #RetailInfo
				SELECT R.* 
				FROM #RetailInfo1 R
				INNER JOIN #HcRetailerSubCategory RS ON R.RetailLocationID = RS.RetailLocationID
		END
		ELSE IF(@IsSubCategory = 0)
		BEGIN
				INSERT INTO #RetailInfo
				SELECT R.* 
				FROM #RetailInfo1 R
		END
                     
		INSERT INTO #Retail 
        SELECT  *
		FROM 
		(SELECT DISTINCT RL.RetailerID 
					, RL.RetailName    
					, RL.RetailLocationID 
					, RL.City
					, RL.PostalCode
					, RL.RetailLocationLatitude  retLatitude
					, RL.RetailLocationLongitude retLongitude
					, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					, DistanceActual = 0
					--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
					, SaleFlag = 0
					, SB.HcBusinessSubCategoryID
					, BusinessSubCategoryName   
		FROM #RetailInfo RL
		LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID = RL.HcBusinessSubCategoryID 
		LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode AND G.City = RL.City 
		LEFT JOIN RetailerKeywords RK ON RL.RetailerID = RK.RetailID                                 
        WHERE (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
		) Retailer
        WHERE Distance <= @Radius                             
                       
        INSERT  INTO  #RetailItemsonSale1
		SELECT DISTINCT R.RetailID, A.RetailLocationID 
			FROM RetailLocationDeal A 
			INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
			INNER JOIN RetailLocationProduct C ON A.RetailLocationID = C.RetailLocationID AND A.ProductID = c.ProductID
			AND GETDATE() BETWEEN ISNULL(A.SaleStartDate, @Yesterday) AND ISNULL(A.SaleEndDate, @Tomorrow)
		
		INSERT  INTO  #RetailItemsonSale1
		SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  AS RetaillocationID 
			FROM Coupon C 
			INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
			INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
			LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
					GROUP BY C.CouponID
							,NoOfCouponsToIssue
							,CR.RetailID
							,CR.RetailLocationID
					HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
					ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   

		
		INSERT  INTO  #RetailItemsonSale1
		SELECT DISTINCT R.RetailID, R.RetailLocationID
			FROM ProductHotDeal p
			INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
			INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
			LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
			LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealENDDate, @Tomorrow)
					GROUP BY P.ProductHotDealID
							,NoOfHotDealsToIssue
							,R.RetailID
							,R.RetailLocationID
					HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
					ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

		INSERT  INTO  #RetailItemsonSale1
		SELECT q.RetailID, qa.RetailLocationID
			FROM QRRetailerCustomPage q
			INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
			INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
			INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID AND qt.QRTypeName = 'Special Offer Page'
				WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') AND isnull(q.ENDdate,@Tomorrow)
					
                    
		SELECT DISTINCT r.RetailID    
						, RetailName
						, rl.RetailLocationID
						, r.City    
						, r.PostalCode
						, retLatitude
						, retLongitude
						, Distance           
						, DistanceActual                                       
						, SaleFlag= CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END
						, HcBusinessSubCategoryID
		INTO #Retailer    
		FROM #Retail R
		INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
		LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
		

			--To display Filter items
			DECLARE @Distance bit = 1
			DECLARE @Alphabetically bit = 1
			DECLARE @LocalSpecials bit = 0
			DECLARE @Category bit = 0
			DECLARE @Options bit = 0
			DECLARE @City bit = 0
			DECLARE @Interests bit = 0

			SELECT @LocalSpecials = 1 --IIF(MAX(ISNULL(SaleFlag,0)) = 1,1,0)
			FROM #Retailer
			WHERE SaleFlag = 1

			SELECT @Category = 1
			FROM #Retailer
			WHERE HcBusinessSubCategoryID IS NOT NULL
			AND @IsSubCategory = 1

			SELECT @City = 1
			FROM #Retailer
			WHERE City IS NOT NULL AND (@RegionAppFlag = 1)

			SELECT @Interests = 1 
			FROM #Retailer R
			INNER JOIN HcFilterRetailLocation RF ON R.RetailLocationID = RF.RetailLocationID
			INNER JOIN HcFilter F ON RF.HcFilterID = F.HcFilterID
			WHERE F.HcHubCitiID = @HcHubCitiID1

			SELECT @Options = 1
			FROM #Retailer R
			INNER JOIN RetailerFilterAssociation F ON R.RetailLocationID = F.RetailLocationID AND F.BusinessCategoryID = @BusinessCategoryID
			AND F.AdminFilterID IS NOT NULL AND F.AdminFilterValueID IS NULL

			SELECT DISTINCT F.FilterName  AS FilterName
			INTO #Filterlist
			FROM #Retailer R
			INNER JOIN RetailerFilterAssociation RF ON R.RetailLocationID = RF.RetailLocationID
			INNER JOIN AdminFilterValueAssociation A ON RF.AdminFilterValueID = A.AdminFilterValueID AND RF.BusinessCategoryID = @BusinessCategoryID
			INNER JOIN AdminFilterValue AV ON A.AdminFilterValueID = AV.AdminFilterValueID
			INNER JOIN AdminFilter F ON A.AdminFilterID = F.AdminFilterID AND RF.AdminFilterID = F.AdminFilterID
			ORDER BY FilterName
					
			DECLARE @Temp TABLE(Type Varchar(50), Items Varchar(50),Flag bit)
			INSERT INTO @Temp VALUES ('Sort Items by','Distance',@Distance)
			INSERT INTO @Temp VALUES ('Sort Items by','Alphabetically',@Alphabetically)

			INSERT INTO @Temp VALUES ('Filter Items by','Local Specials !',@LocalSpecials)
			INSERT INTO @Temp VALUES ('Filter Items by','Category',@Category)
			INSERT INTO @Temp VALUES ('Filter Items by','Options',@Options)
						
			INSERT INTO @Temp (Type
								,Items
								,Flag)
							SELECT 'Filter Items by'
								, FilterName
								, 1
							FROM #Filterlist

			INSERT INTO @Temp VALUES ('Filter Items by','City',@City)
			INSERT INTO @Temp VALUES ('Filter Items by','Interests',@Interests)

			SELECT Type AS fHeader
					,Items  AS filterName
			FROM @Temp
			WHERE Flag = 1
						
			--Confirmation of success
			SELECT @Status = 0 

	END TRY
            
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN

			PRINT 'Error occured in Stored Procedure [usp_HcFindOptionsListDisplay].'                            
			--Execute retrieval of Error info.
			EXEC [HubCitiApp8].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SELECT @Status = 1                   
		END;
	END CATCH;
END;
       





















GO
