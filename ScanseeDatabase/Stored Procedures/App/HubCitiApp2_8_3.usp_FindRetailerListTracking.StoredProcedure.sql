USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_FindRetailerListTracking]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [HubCitiApp2_8_3].[usp_FindRetailerListTracking]
Purpose                 :  To track the Retailer and Location list in Find Module.
Example                 :  

History
Version       Date           Author				Change Description
------------------------------------------------------------------------------- 
1.0          03/17/2016       Sagar Byali       Initial Version
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_FindRetailerListTracking]
(

     --Input Parameters 
	  @MainMenuID INT
	, @RetailID VARCHAR(1000)
	, @RetailLocationID VARCHAR(1000)
	, @CategoryName VARCHAR(50)

	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 

)
AS
BEGIN


	BEGIN TRY
            

			-- To Collect RetailID's passed as input
			SELECT Rownum=Identity(Int,1,1)
					,Param RetailID
			INTO #RetailID
			FROM fn_SplitParam(@RetailID,',')

			-- To Collect RetailLocationID's passed as input
			SELECT Rownum=Identity(Int,1,1)
					,Param RetailLocationID
			INTO #RetailLocationID
			FROM fn_SplitParam(@RetailLocationID,',')

			-- To Collect RetailID & RetailLocationID into #RetailerList
			SELECT RetailID
				  ,RetailLocationID
			INTO #RetailerList
			FROM #RetailID R
			INNER JOIN #RetailLocationID RL ON R.Rownum = RL.Rownum

     
			DECLARE @CategoryID VARCHAR(1000)
			SELECT @CategoryID = BusinessCategoryID 
			FROM BusinessCategory
			WHERE BusinessCategoryName = @CategoryName

            --Table to track the Retailer List.
            CREATE TABLE #Temp(Rowno int IDENTITY(1,1)
								,RetailerListID int
                                ,LocationDetailID int
                                ,MainMenuID int
                                ,RetailID int
                                ,RetailLocationID int)  

            --Capture the impressions of the Retailer list.
            INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
                                                            , RetailID
                                                            , RetailLocationID
                                                            , FindCategoryID
                                                            , DateCreated)
                     
            OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailID, RetailLocationID)                                             
																								SELECT DISTINCT @MainMenuId
																											, RetailID
																											, RetailLocationID
																											, @CategoryID
																											, GETDATE()
																								FROM #RetailerList 

			SELECT RetailerListID retListID
				  ,RetailID retailerId
				  ,RetailLocationID
				  
			 FROM #Temp


			
																								
																								
			--Confirmation of Success																						
			 SELECT	@Status  = 0                          
               
            
       END TRY
            
	BEGIN CATCH
      
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN

			SELEct ERROR_LINE()
					PRINT 'Error occured in Stored Procedure [HubCitiApp2_8_3].[usp_FindRetailerListTracking].'   
					 SELECT	@Status  = 1             
					            
					--Execute retrieval of Error info.
					EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
					PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
	END;
            
	END CATCH;
	END;
       




























GO
