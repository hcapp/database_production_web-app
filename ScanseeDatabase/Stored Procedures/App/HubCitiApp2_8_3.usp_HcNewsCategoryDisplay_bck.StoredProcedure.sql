USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcNewsCategoryDisplay_bck]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcPreferredLocationCategoryDisplay
Purpose					: To dispaly User preferred Location Categories.
Example					: usp_HcPreferredLocationCategoryDisplay

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			2/26/2015       SPAN             1.0
---------------------------------------------------------------
*/

--EXEC [HubCitiApp2_8_1].[usp_HcNewsCategoryDisplay] 12507,8, null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcNewsCategoryDisplay_bck]
(
	 --Input variable
	  @HcUserID int
	, @HubCitiID int
		
	--Output Variable 	
	, @BookMark  varchar(1000) output 
	, @Section   varchar(1000) output 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

--delete from HcDefaultNewsFirstBookmarkedCategories WHERE HcHubCitiID = @HubCitiID 
            
--INSERT INTO HcDefaultNewsFirstBookmarkedCategories(NewsCategoryID,NewsCategoryDisplayValue,HchubcitiID,SortOrder,DateCreated,Active,CategoryColor,Issubmenu)
            
--SELECT C.NewsCategoryID, NewsCategoryDisplayValue, NS.HcHubCitiID, ROW_NUMBER() OVER (ORDER BY C.newscategoryname) AS SortOrder,GETDATE(),1,NS.NewsCategoryColor,0
--FROM NewsFirstSettings NS
--INNER JOIN NewsFirstCategory C ON C.NewsCategoryID = NS.NewsCategoryID
--WHERE NS.HcHubCitiID = @HubCitiID
--ORDER BY NewsCategoryDisplayValue

				
			SELECT NS.NewsCategoryID,NS.NewsCategoryColor,NS.HcHubCitiID,NS.NewsCategoryFontColor
			INTO #Color
			FROM HcDefaultNewsFirstBookmarkedCategories D
			INNER JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID
			WHERE D.HchubcitiID = @HubCitiID 

			
			IF EXISTS (SELECT 1 FROM HcUserNewsFirstSingleSideNavigation WHERE HcUserID = @HcUserID AND HcHubCitiID = @HubCitiID)
			AND NOT EXISTS(select 1  from  HcUser  where username in('WelcomeScanSeeGuest','GuestLogin')  and hcuserid=@HcuserID)
			 BEGIN
			--SELECT 'aa'
				--To fetch Business categories for given user
				--SELECT DISTINCT   NC.NewsCategoryID AS catId
				--				, NC.NewsCategoryDisplayValue AS catName
				--				, 1 AS subCatId
				--				, 'All' AS subCatName
				--				, UF.Flag
				--				, UF.SortOrder
				--				--, isAdded = 1
				--				, N.NewsCategoryColor catColor
				--				, 0 isSubCategory
				--				,N.NewsCategoryFontColor catTxtColor
				--FROM NewsFirstCategory NC
				--INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON NC.NewsCategoryID = UF.NewsSideNavigationID
			 --   INNER JOIN NewsFirstSettings  N on N.NewsCategoryID=NC.NewsCategoryID AND N.HcHubCitiID = @HubCitiID
				--WHERE NC.Active = 1 AND UF.HcUserID= @HcUserID AND UF.HchubcitiID= @HubCitiID AND UF.Flag= 2

				--UNION
				
				--To fetch Business sub categories for given user
				--SELECT DISTINCT  ns.NewsCategoryID AS catId
				--				, ns.NewsCategoryDisplayValue AS catName
				--				, ISNULL(US.NewsSideNavigationSubCategoryID,1) AS subCatId
				--				, ISNULL(US.NewsSideNavigationSubCategoryDisplayValue,'All') AS subCatName
				--				, US.Flag
				--				, UF.SortOrder
				--				, isAdded = 1
				--				, N1.NewsCategoryColor catColor
				--				, 1 isSubCategory
				--				, '#ff0000' catTxtColor
				--FROM HcUserNewsFirstSideNavigationSubCategories US 
				--INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON UF.NewsSideNavigationID= US.NewsFirstCategoryID 
				--AND UF.HchubcitiID= @HubCitiID AND UF.HcUserID=@HcUserID
				--INNER JOIN NewsFirstSettings  N1 on N1.NewsCategoryID=US.NewsFirstCategoryID AND N1.HcHubCitiID = @HubCitiID
				--inner join NewsFirstCategory ns on ns.NewsCategoryID= US.NewsFirstCategoryID 
				--inner join NewsFirstSubCategorySettings sub  on ns.NewsCategoryID= sub.NewsCategoryID
				--and us. NewsSideNavigationSubCategoryID=sub.NewsSubCategoryID
				
				--WHERE US.HcUserID= @HcUserID AND US.HchubcitiID= @HubCitiID and UF.HchubcitiID= @HubCitiID

				--To fetch Business sub categories for given user
				--SELECT DISTINCT  US.NewsCategoryID AS catId
				--				, US.NewsCategoryDisplayValue AS catName
				--				, ISNULL(sub.NewsSubCategoryID,1) AS subCatId
				--				, ISNULL(sub.NewsSubCategoryDisplayValue,'All') AS subCatName
				--				, UF.Flag
				--				, UF.SortOrder
				--				--, isAdded = 1
				--				, N1.NewsCategoryColor catColor
				--				, 1 isSubCategory
				--				,NewsCategoryFontColor catTxtColor
				--FROM NewsFirstCategory US 
				--inner join NewsFirstsubCategorytype ty on ty.NewsCategoryID=US.NewsCategoryID 
				--inner join NewsFirstsubCategory sub on sub.NewsSubCategoryTypeID=ty.NewsSubCategoryTypeID
				--INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON UF.NewsSideNavigationID= US.NewsCategoryID 
				--AND UF.HchubcitiID= @HubCitiID AND UF.HcUserID=@HcUserID
				--INNER JOIN NewsFirstSettings  N1 on N1.NewsCategoryID=US.NewsCategoryID AND N1.HcHubCitiID = @HubCitiID
				--inner join NewsFirstSubCategorySettings sub1  on N1.NewsCategoryID= sub1.NewsCategoryID
				--and sub1.NewsSubCategoryID=sub.NewsSubCategoryID
				--WHERE  US.Active = 1 AND sub1.HchubcitiID= @HubCitiID 				 
				--and uf.Flag=2
				--ORDER BY SortOrder	
				
				SELECT DISTINCT   NC.NewsCategoryID AS catId
								, NC.NewsCategoryDisplayValue AS catName
								, 1 AS subCatId
								, 'All' AS subCatName
								, UF.Flag
								, UF.SortOrder
								--, isAdded = 1
								, N.NewsCategoryColor catColor
								, 0 isSubCategory
								,N.NewsCategoryFontColor catTxtColor
				FROM NewsFirstCategory NC
				INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = NC.NewsCategoryID AND HA.HcHubcitiID = @HubCitiID
				INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON NC.NewsCategoryID = UF.NewsSideNavigationID
			    INNER JOIN NewsFirstSettings  N on N.NewsCategoryID=NC.NewsCategoryID AND N.HcHubCitiID = @HubCitiID
				LEFT JOIN HcMenuITem HM ON HM.MenuItemName = UF.NewsSideNavigationDisplayValue 
				AND HM.hcMenuID = (SELECT HcMenuID FROM hcMenu WHERE HcHubcitiID =@HubCitiID AND IsDefaultHubCitiSideMenu=1)
				WHERE NC.Active = 1 AND UF.HcUserID= @HcUserID AND UF.HchubcitiID= @HubCitiID AND UF.Flag= 2 AND HM.HcMenuItemID IS NULL

				UNION

				SELECT DISTINCT   NC.NewsCategoryID AS catId
								, NC.NewsCategoryDisplayValue AS catName
								, 1 AS subCatId
								, 'All' AS subCatName
								, UF.Flag
								, UF.SortOrder
								--, isAdded = 1
								, N.NewsCategoryColor catColor
								, 0 isSubCategory
								,N.NewsCategoryFontColor catTxtColor
				FROM NewsFirstCategory NC
				INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = NC.NewsCategoryID AND HA.HcHubcitiID = @HubCitiID
				INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON NC.NewsCategoryID = UF.NewsSideNavigationID AND UF.HcUserID= @HcUserID AND UF.HchubcitiID= @HubCitiID
			    INNER JOIN NewsFirstSettings  N on N.NewsCategoryID=NC.NewsCategoryID AND N.HcHubCitiID = @HubCitiID
				INNER JOIN HcMenuITem HM ON LTRIM(RTRIM(HM.MenuItemName)) = LTRIM(RTRIM(UF.NewsSideNavigationDisplayValue))
				AND HM.hcMenuID = (SELECT HcMenuID FROM hcMenu WHERE HcHubcitiID =@HubCitiID AND IsDefaultHubCitiSideMenu=1)
				INNER JOIN HcRegionAppMenuItemCityAssociation RMC ON RMC.HcMenuItemID= HM.HcMenuItemID AND RMC.HcHubcitiID= @HubCitiID 
				INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND RMC.HcHubcitiId=UC.HcHubcitiId AND UC.HcHubcitiID= @HubCitiID
				inner join HcLocationAssociation on HcLocationAssociation.HcHubCitiID=RMC.HcHubcitiId and  RMC.HcCityID=HcLocationAssociation.HcCityID
				WHERE HcLocationAssociation.HcHubCitiID= @HubCitiID  AND NC.Active = 1   AND UF.Flag= 2 --AND UC.HcUsersPreferredCityAssociationID IS NULL

				UNION

				SELECT DISTINCT  US.NewsCategoryID AS catId
								, US.NewsCategoryDisplayValue AS catName
								, ISNULL(sub.NewsSubCategoryID,1) AS subCatId
								, ISNULL(sub.NewsSubCategoryDisplayValue,'All') AS subCatName
								, UF.Flag
								, UF.SortOrder
								--, isAdded = 1
								, N1.NewsCategoryColor catColor
								, 1 isSubCategory
								,NewsCategoryFontColor catTxtColor
				FROM NewsFirstCategory US 
				inner join NewsFirstsubCategorytype ty on ty.NewsCategoryID=US.NewsCategoryID 
				inner join NewsFirstsubCategory sub on sub.NewsSubCategoryTypeID=ty.NewsSubCategoryTypeID
				INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = US.NewsCategoryID AND sub.NewsSubCategoryID = HA.SubCategoryID AND HA.HcHubcitiID = @HubCitiID
				INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON UF.NewsSideNavigationID= US.NewsCategoryID 
				AND UF.HchubcitiID= @HubCitiID AND UF.HcUserID=@HcUserID
				INNER JOIN NewsFirstSettings  N1 on N1.NewsCategoryID=US.NewsCategoryID AND N1.HcHubCitiID = @HubCitiID
				inner join NewsFirstSubCategorySettings sub1  on N1.NewsCategoryID= sub1.NewsCategoryID
				and sub1.NewsSubCategoryID=sub.NewsSubCategoryID
				LEFT JOIN HcMenuITem HM ON HM.MenuItemName = UF.NewsSideNavigationDisplayValue 
				AND HM.hcMenuID = (SELECT HcMenuID FROM hcMenu WHERE HcHubcitiID =@HubCitiID AND IsDefaultHubCitiSideMenu=1)
				WHERE  US.Active = 1 AND sub1.HchubcitiID= @HubCitiID 				 
				and uf.Flag=2 AND HM.HcMenuItemID IS NULL

				UNION

				SELECT DISTINCT  US.NewsCategoryID AS catId
								, US.NewsCategoryDisplayValue AS catName
								, ISNULL(sub.NewsSubCategoryID,1) AS subCatId
								, ISNULL(sub.NewsSubCategoryDisplayValue,'All') AS subCatName
								, UF.Flag
								, UF.SortOrder
								--, isAdded = 1
								, N1.NewsCategoryColor catColor
								, 1 isSubCategory
								,NewsCategoryFontColor catTxtColor
				FROM NewsFirstCategory US 
				inner join NewsFirstsubCategorytype ty on ty.NewsCategoryID=US.NewsCategoryID 
				inner join NewsFirstsubCategory sub on sub.NewsSubCategoryTypeID=ty.NewsSubCategoryTypeID
				INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = US.NewsCategoryID AND sub.NewsSubCategoryID = HA.SubCategoryID AND HA.HcHubcitiID = @HubCitiID
				INNER JOIN HcUserNewsFirstSingleSideNavigation UF ON UF.NewsSideNavigationID= US.NewsCategoryID 
				AND UF.HchubcitiID= @HubCitiID AND UF.HcUserID=@HcUserID
				INNER JOIN NewsFirstSettings  N1 on N1.NewsCategoryID=US.NewsCategoryID AND N1.HcHubCitiID = @HubCitiID
				inner join NewsFirstSubCategorySettings sub1  on N1.NewsCategoryID= sub1.NewsCategoryID
				and sub1.NewsSubCategoryID=sub.NewsSubCategoryID and uf.Flag=2
				INNER JOIN HcMenuITem HM ON LTRIM(RTRIM(HM.MenuItemName)) = LTRIM(RTRIM(UF.NewsSideNavigationDisplayValue))
				AND HM.hcMenuID = (SELECT HcMenuID FROM hcMenu WHERE HcHubcitiID =@HubCitiID AND IsDefaultHubCitiSideMenu=1)
				INNER JOIN HcRegionAppMenuItemCityAssociation RMC ON RMC.HcMenuItemID= HM.HcMenuItemID AND RMC.HcHubcitiID= @HubCitiID 
				INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND RMC.HcHubcitiId=UC.HcHubcitiId AND UC.HcHubcitiID= @HubCitiID
				inner join HcLocationAssociation on HcLocationAssociation.HcHubCitiID=RMC.HcHubcitiId and  RMC.HcCityID=HcLocationAssociation.HcCityID
				AND UC.HcUsersPreferredCityAssociationID IS NULL
				ORDER BY SortOrder
								
			 END
			ELSE
			 BEGIN
				
				DECLARE @Defcount INT,@LinkID INT

				--SELECT @Defcount
			 
			  --SELECT 'BB'
				-- to display categories set as menu in web
								
				SELECT DISTINCT @Defcount = count(*)
				FROM HcMenuItem HM
				INNER JOIN HcMenu M ON HM.HcMenuID= M.HcMenuID AND M.HcHubCitiID= @HubCitiID AND M.IsDefaultHubCitiSideMenu=1	

				SELECT  @Defcount

				SELECT @LinkID = HcMenuID
				FROM HcMenu 
				WHERE HcHubCitiID= @HubCitiID AND IsDefaultHubCitiSideMenu= 1
				
				SELECT DISTINCT
				NC.NewsCategoryID AS catID,
				HM.MenuItemName catname,
				1 AS subCatId,
				'All' AS subCatName,
				2 Flag,
				HM.Position SortOrder,
				--1 isAdded,
				N1.NewsCategoryColor AS catColor,
				1 isSubCategory,
				N1.NewsCategoryFontColor catTxtColor
				FROM HcMenu M
				INNER JOIN HcMenuItem HM ON HM.HcMenuID= M.HcMenuID AND M.HcHubCitiID= @HubCitiID AND M.IsDefaultHubCitiSideMenu=1	
				INNER JOIN NewsFirstCategory NC ON LTRIM(RTRIM(NC.NewsCategoryName))= LTRIM(RTRIM(HM.MenuItemName))
				INNER JOIN NewsFirstSettings  N1 on N1.NewsCategoryID=NC.NewsCategoryID AND N1.HcHubCitiID = @HubCitiID
				INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = NC.NewsCategoryID  AND HA.HcHubcitiID = @HubCitiID
				--LEFT JOIN HcDefaultNewsFirstBookmarkedCategories N2 ON N2.NewsCategoryID = N1.NewsCategoryID AND N2.HcHubCitiID = @HubCitiID 
				LEFT JOIN NewsFirstSubCategoryType NT ON NT.NewsCategoryID = NC.NewsCategoryID AND NewsSubCategoryTypeID IS NULL   
				--LEFT JOIN NewsFirstSubCategory NS ON NS.NewsSubCategoryTypeID = NT.NewsSubCategoryTypeID
				WHERE  (LTRIM(RTRIM(HM.HcLinkTypeID)) = (SELECT HcLinkTypeID FROM hcLinkType WHERE LinkTypeName = 'News') ) --AND N2.NewsCategoryID IS NULL

				---- to display sub-categories set as menu in web
				----UNION

				SELECT DISTINCT NC.NewsCategoryID AS catID,
				HM.MenuItemName catname,
				ISNULL(NS.NewsSubCategoryID,1) AS subCatID,
				ISNULL(NS.NewsSubCategoryDisplayValue,'All') AS subCatName,
				2 Flag,
				HM.Position SortOrder,
				--1 isAdded,
				N1.NewsCategoryColor AS catColor,
				1 isSubCategory,
				N1.NewsCategoryFontColor catTxtColor
				FROM HcMenu M
				INNER JOIN HcMenuItem HM ON HM.HcMenuID= M.HcMenuID AND M.HcHubCitiID= @HubCitiID AND M.IsDefaultHubCitiSideMenu=1
				INNER JOIN NewsFirstCategory NC ON LTRIM(RTRIM(NC.NewsCategoryName))= LTRIM(RTRIM(HM.MenuItemName))
				INNER JOIN NewsFirstSettings N1 on N1.NewsCategoryID=NC.NewsCategoryID AND N1.HcHubCitiID = @HubCitiID
				INNER JOIN NewsFirstSubCategorySettings N2 ON N2.NewsCategoryID = N1.NewsCategoryID AND N2.HcHubCitiID= @HubCitiID
				INNER JOIN NewsFirstSubCategoryType NT ON NT.NewsCategoryID = NC.NewsCategoryID
				INNER JOIN NewsFirstSubCategory NS ON NS.NewsSubCategoryTypeID = NT.NewsSubCategoryTypeID AND N2.NewsSubCategoryID= NS.NewsSubCategoryID
				INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = NC.NewsCategoryID AND NS.NewsSubCategoryID = HA.SubCategoryID AND HA.HcHubcitiID = @HubCitiID
				WHERE (LTRIM(RTRIM(HM.HcLinkTypeID)) = (SELECT HcLinkTypeID FROM hcLinkType WHERE LinkTypeName = 'News') )
				
				----UNION

				----To Fetch Default Categories 

				--SELECT DISTINCT   DF.NewsCategoryID AS catId
				--				, DF.NewsCategoryDisplayValue AS catName
				--				, 1 AS subCatId
				--				, 'All' AS subCatName
				--				, 2 Flag
				--				, ISNULL(@Defcount,0)+DF.SortOrder SortOrder
				--				--, isAdded = 1
				--				, N.NewsCategoryColor catColor
				--				, isSubCategory = CASE WHEN HA.SubCategoryID IS NOT NULL THEN 1 ELSE 0 END
				--				, NewsCategoryFontColor catTxtColor
				--FROM NewsFirstCategory NC
				--INNER JOIN HcDefaultNewsFirstBookmarkedCategories DF ON NC.NewsCategoryID = DF.NewsCategoryID
				--INNER join #Color n on n.NewsCategoryID=DF.NewsCategoryID  AND n.HcHubCitiID = DF.HchubcitiID
				--LEFT JOIN HcMenuItem HM ON LTRIM(RTRIM(Hm.MenuItemName)) = LTRIM(RTRIM(DF.NewsCategoryDisplayValue)) AND HM.HcMenuID= @LinkID
				--INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = NC.NewsCategoryID  AND HA.HcHubcitiID = @HubCitiID
				----LEFT JOIN NewsFirstSubCategoryType NST ON NC.NewsCategoryID= NST.NewsCategoryID
				----LEFT JOIN NewsFirstSubCategory NS ON NS.NewsSubCategoryTypeID = NST.NewsSubCategoryTypeID
				--WHERE DF.HchubcitiID = @HubCitiID and NC.Active = 1 AND HM.HcMenuItemID IS  NULL
				
				----UNION  

				----To fetch Business sub categories 
				----SELECT DISTINCT DC.NewsCategoryID AS catId
				----				, DC.NewsCategoryDisplayValue AS catName
				----				, ISNULL(DF.NewsSubCategoryID,1) AS subCatId
				----				, ISNULL(DF.NewsSubCategoryDisplayValue,'All') AS subCatName
				----				, 2 Flag
				----				, DC.SortOrder
				----				, 1 isAdded
				----				, N.NewsCategoryColor catColor
				----				, 1 isSubCategory
				----				, '#000000' catTxtColor
				----FROM HcDefaultNewsFirstBookmarkedCategories  DC 
				----INNER JOIN HcDefaultNewsFirstBookmarkedSubCategories DF ON DF.NewsFirstCategoryID = DC.NewsCategoryID 
				----AND DF.HchubcitiID = @HubCitiID
				----INNER join #Color n on n.NewsCategoryID=DF.NewsFirstCategoryID  AND n.HcHubCitiID =DF.HchubcitiID
				----inner JOIN NewsFirstCategory NS ON NS.NewsCategoryID = DC.NewsCategoryID
				----WHERE dc.HchubcitiID = @HubcitiID

				--	SELECT DISTINCT DC.NewsCategoryID AS catId
				--				, DC.NewsCategoryDisplayValue AS catName
				--				, ISNULL(sub.NewsSubCategoryID,1) AS subCatId
				--				, ISNULL(sub.NewsSubCategoryDisplayValue,'All') AS subCatName
				--				, 2 Flag
				--				, @Defcount+DC.SortOrder SortOrder
				--				--, 1 isAdded
				--				, N.NewsCategoryColor catColor
				--				, 1 isSubCategory
				--				, NewsCategoryFontColor catTxtColor
				--FROM HcDefaultNewsFirstBookmarkedCategories  DC 
				--INNER JOIN NewsFirstSubCategorySettings DF ON DF.NewsCategoryID = DC.NewsCategoryID 
				--AND DF.HchubcitiID = @HubCitiID
				--inner join NewsFirstsubCategorytype ty on ty.NewsCategoryID=DC.NewsCategoryID 
				--inner join NewsFirstsubCategory sub on sub.NewsSubCategoryTypeID=ty.NewsSubCategoryTypeID
				--INNER join #Color n on n.NewsCategoryID=DC.NewsCategoryID  AND n.HcHubCitiID =DC.HchubcitiID
				--inner JOIN NewsFirstCategory NS ON NS.NewsCategoryID = DC.NewsCategoryID
				--INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = NS.NewsCategoryID AND sub.NewsSubCategoryID = HA.SubCategoryID AND HA.HcHubcitiID = @HubCitiID
				--LEFT JOIN HcMenuItem HM ON LTRIM(RTRIM(Hm.MenuItemName)) = LTRIM(RTRIM(DC.NewsCategoryDisplayValue)) AND HM.HcMenuID= @LinkID
				--WHERE dc.HchubcitiID = @HubcitiID and ns.Active = 1 AND hM.HcMenuID IS  NULL
				ORDER BY SortOrder

				

			 END


	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcPreferredLocationCategoryDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
