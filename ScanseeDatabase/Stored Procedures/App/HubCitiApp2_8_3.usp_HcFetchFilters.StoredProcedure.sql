USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFetchFilters]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFetchFilters]
Purpose					: To display User'd Preferred retailers.
Example					: [usp_HcFetchFilters] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15th Oct 2013 	Pavan Sharma K	     1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFetchFilters] 
(
	  @CitiExperienceID int
	, @UserID int	 
	, @HcBottomButtonID int
	, @HcMenuItemID int
	
	--OutPut Variable
	, @MaxCnt int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	
	    DECLARE @Config varchar(50)
		SELECT @Config = ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'
	
	    DECLARE @Globalimage varchar(50)
        SELECT @Globalimage =ScreenContent 
        FROM AppConfiguration 
        WHERE ConfigurationType = 'Image Not Found'

		DECLARE @ModuleName varchar(20) = 'Filters'
		DECLARE @HcHubcitiID int

		SELECT @HcHubcitiID = HcHubCitiID
		FROM HcCityExperience
		WHERE HcCityExperienceID = @CitiExperienceID


		CREATE TABLE #temp(Row_Num INT
		                  ,retAffId INT
						  ,retAffName VARCHAR(1000)
	                      ,retAffImg VARCHAR(1000)
						  ,SortOrder INT
						  )

		--To fetch Retailer's Info based on Menuitem and Bottombutton 
		IF @HcBottomButtonID IS NOT NULL
		BEGIN

		INSERT INTO #temp
		SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(R.SortOrder,10000), R.HcFilterID ASC) Row_Num
			  ,R.HcFilterID retAffId 
		      ,FilterName retAffName  
		      ,@Config+CAST(C.HcHubCitiID AS VARCHAR(10))+'/'+ISNULL(R.ButtonImagePath,@Globalimage) retAffImg
			  ,SortOrder  
	    
		FROM HcFilter R
		INNER JOIN HcCityExperience C ON C.HcCityExperienceID = R.HcCityExperienceID
		INNER JOIN HcBottomButtonFilterAssociation FA ON FA.HcFilterID = R.HcFilterID 
		WHERE R.HcCityExperienceID = @CitiExperienceID AND FA.HcBottomButtonID = @HcBottomButtonID
		END

		ELSE IF @HcMenuItemID IS NOT NULL
		BEGIN

		INSERT INTO #temp
		SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(R.SortOrder,10000), R.HcFilterID ASC) Row_Num
			  ,R.HcFilterID retAffId 
		      ,FilterName retAffName  
		      ,@Config+CAST(C.HcHubCitiID AS VARCHAR(10))+'/'+ISNULL(R.ButtonImagePath,@Globalimage) retAffImg
			  ,SortOrder 
		FROM HcFilter R
		INNER JOIN HcCityExperience C ON C.HcCityExperienceID = R.HcCityExperienceID
		INNER JOIN HcMenuItemFilterAssociation FA ON FA.HcFilterID = R.HcFilterID 
		WHERE R.HcCityExperienceID = @CitiExperienceID AND FA.HcMenuItemID = @HcMenuItemID
		END

		SELECT* FROM #temp

		--Capture the number of Partners. If only one partner then navigate to Retailer List screen else go to Partner list scree.
		SELECT @MaxCnt = @@ROWCOUNT 		
		
		--To display bottom buttons								
		--EXEC [HubCitiApp2_1].[usp_HcFunctionalityBottomButtonDisplay] @HcHubcitiID, @ModuleName, @UserID, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

		SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			     , HM.HcHubCitiID 
				 , BB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HcHubcitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HcHubcitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HcHubcitiID), BottomButtonLinkID) 
				 --, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubcitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubcitiID)
			                      	 
			                       ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID AND FBB.HcHubCitiID =@HcHubcitiID
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = 'Find Category'
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 

	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcFetchFilters].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	 
		END;
		 
	END CATCH;
END;













































GO
