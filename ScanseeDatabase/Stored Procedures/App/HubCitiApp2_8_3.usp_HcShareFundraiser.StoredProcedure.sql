USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcShareFundraiser]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_HcShareFundraiser
Purpose                 : To share fundraiser details.
Example                 : usp_HcShareFundraiser

History
Version           Date              Author			  Change Description
------------------------------------------------------------------------ 
1.0				  29th Jan 2015     Mohith H R        1.0
------------------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcShareFundraiser]
(
        
		@HcFundraisingID int
	  ,	@UserID int
	  , @HubCitiId int
    
      --Output Variable 
      , @ShareText varchar(500) output
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
      
		BEGIN TRANSACTION
      
            DECLARE @RetailConfig varchar(50)
			DECLARE @Config VARCHAR(100)  
			DECLARE @Config1 VARCHAR(100)            
			DECLARE @Configuration VARCHAR(100)
            DECLARE @QRType INT     
          
			--Retrieve the server configuration.
            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 
             
            --To get server Configuration.
            SELECT @RetailConfig= ScreenContent  
            FROM AppConfiguration   
            WHERE ConfigurationType='Web Retailer Media Server Configuration' 
            
            --To get the text used for share.         
            DECLARE @HubCitiName varchar(100)
		 
			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
		       
            SELECT @ShareText = 'I found this Fundraiser in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti Fundraiser Share Text'    
			
			SELECT @Config1 = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'       
                  
            --Display the Basic details of the Retail location.   
			
			SELECT DISTINCT  FundraisingName fundName  
							,FundraisingOrganizationName orgztnHosting
							,ShortDescription sDescription
							--,LongDescription lDescription								
							,HcHubCitiID hubCitiId 							
							,imgPath=IIF(RetailID IS NULL,(@Config1 + CAST(HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(RetailID AS VARCHAR)+'/'+FundraisingOrganizationImagePath)                                                                                 
							,sDate =CAST(DATEPART(MM,StartDate) AS VARCHAR(10))+'/'+CAST(DATEPART(DD,StartDate) AS VARCHAR(10))+'/'+CAST(DATEPART(YY,StartDate) AS VARCHAR(10))--CAST(StartDate AS DATE)
							,eDate =CAST(DATEPART(MM,EndDate) AS VARCHAR(10))+'/'+CAST(DATEPART(DD,EndDate) AS VARCHAR(10))+'/'+CAST(DATEPART(YY,EndDate) AS VARCHAR(10))--CAST(EndDate AS DATE)								 								           
							--,QRUrl = @Config+'2500.htm?key1='+ CAST(@HcFundraisingID AS VARCHAR(10)) --+'&key2='+ CAST(@HubCitiId AS VARCHAR(10)) 
							,QRUrl = @Config+'2500.htm?fundEvtId='+ CAST(@HcFundraisingID AS VARCHAR(10)) + '&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10)) --'&hubcitiId='+ IIF(RetailID IS NULL,(CAST(@HubCitiId AS VARCHAR(10))),'null') 
			FROM HcFundraising
			WHERE HcFundraisingID = @HcFundraisingID --AND HcHubCitiID= @HubCitiId
			
           --Confirmation of Success.
		   SELECT @Status = 0 
        COMMIT TRANSACTION          
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_HcShareFundraiser.'       
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;
            
      END CATCH;
END;












































GO
