USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserTokenInsertion]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserTokenInsertion
Purpose					: To insert User Token.
Example					: usp_HcUserTokenInsertion 

Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21 Jan 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserTokenInsertion]
(	 
	  @DeviceID varchar(60)
	 ,@UserToken varchar(1000)
	 ,@FaceBookAuthenticatedUser bit 
	 ,@RequestPlatformType varchar(100)
	 ,@HcHubCitiKey varchar(500)

	 --Output Variable 
	 --,@Result int output
	 ,@Status int output
	 ,@ErrorNumber int output
	 ,@ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION

			DECLARE @RequestPlatformID int
			DECLARE @HcHubCitiID int					

			SELECT @RequestPlatformID = HcRequestPlatformID
			FROM HcRequestPlatforms
			WHERE RequestPlatformtype = @RequestPlatformType

			SELECT @HcHubCitiID = HcHubCitiID
			FROM HcHubCiti
			WHERE HcHubCitiKey = @HcHubCitiKey
		
			--Checking for the existence of the TokenId for the same device.
			IF EXISTS (SELECT 1
					   FROM HcUserToken
					   WHERE DeviceID = @DeviceID AND HcHubCitiID = @HcHubCitiID) 
					   --AND UserToken = @UserToken AND FaceBookAuthenticatedUser=@FaceBookAuthenticatedUser)
			BEGIN
				  UPDATE HcUserToken SET UserToken = @UserToken, DateCreated = GETDATE() WHERE DeviceID = @DeviceID AND HcHubCitiID = @HcHubCitiID
				  SET @Status = 0
			END
			
			ELSE IF NOT EXISTS (SELECT 1
						   FROM HcUserToken
						   WHERE DeviceID = @DeviceID AND HcHubCitiID = @HcHubCitiID) 
						   --AND UserToken = @UserToken AND FaceBookAuthenticatedUser=@FaceBookAuthenticatedUser)
			BEGIN
				INSERT INTO HcUserToken([DeviceID]
						   ,[UserToken]
						   ,[FaceBookAuthenticatedUser]
						   ,DateCreated
						   ,HcRequestPlatformID
						   ,HcHubCitiID)
				SELECT
					    @DeviceID
					   ,@UserToken
					   ,@FaceBookAuthenticatedUser
					   ,GETDATE()
					   ,@RequestPlatformID
					   ,@HcHubCitiID

				SET @Status = 0
				END				
				



		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserTokenInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





























GO
