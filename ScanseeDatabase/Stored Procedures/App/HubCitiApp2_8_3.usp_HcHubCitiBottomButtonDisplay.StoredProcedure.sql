USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiBottomButtonDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiBottomButtonDisplay
Purpose					: To disply the list of the Main Menu Button Details associated to the Hubciti.
Example					: usp_HcHubCitiBottomButtonDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiBottomButtonDisplay]
(
    --Input variable.
      @HcMenuItemID Int
    , @HubCitiID Varchar(100)
    , @LevelID Int  
    , @UserID Int
    , @MenuID Int
    
    ----User Tracking Inputs     
    --, @MainMenuID Int
	  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT HM.HcMenuID as MenuID			
				 , BottomButtonName
				 , CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On END
				 , CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off END
				 , BottomButtonLinkTypeID
				 , BottomButtonLinkID
				 , Position				
				 , CASE WHEN HcBottomButtonImageIconID IS NOT NULL THEN (SELECT HcBottomButtonImageIcon FROM HcBottomButtonImageIcons WHERE HcBottomButtonImageIconID=BB.HcBottomButtonImageIconID)	END		
			FROM HcMenu HM
			INNER JOIN HcMenuBottomButton BM ON BM.HcMenuID=HM.HcMenuID
			INNER JOIN HcBottomButton BB ON BB.HcBottomButtonID=BM.HcBottomButtonID
			WHERE BM.HcMenuID=@MenuID	
						
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiBottomButtonDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
