USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcGalleryUsedHotDealsByProduct]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcGalleryUsedHotDealsByProduct 
Purpose     : To display the used HotDeals.  
Example     : usp_HcGalleryUsedHotDealsByProduct 
  
History  
Version  Date           Author          Change Description  
---------------------------------------------------------------   
1.0      3rd JAN 2014	Dhananjaya TR 	Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcGalleryUsedHotDealsByProduct]  
(  

       @UserID int
	 , @SearchKey varchar(100)
	 , @LowerLimit int
     , @ScreenName varchar(100)
	
	--UserTracking Input Variables
	 , @MainMenuID int
	 
	--Output Variables
	 , @MaxCnt int output
	 , @NxtPageFlag bit output	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output  
  
)  
AS  
BEGIN  
  
	BEGIN TRY  
 
		  --To get Media Server Configuration.  
		  DECLARE @RetailConfig varchar(50)    
		  SELECT @RetailConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Retailer Media Server Configuration'  
		       
		  --To get the row count for pagination.  
		  DECLARE @UpperLimit int   
		  SELECT @UpperLimit = @LowerLimit + ScreenContent   
		  FROM AppConfiguration   
		  WHERE ScreenName = @ScreenName   
		  AND ConfigurationType = 'Pagination'  
		  AND Active = 1  
  
		  CREATE TABLE #Gallery(Row_Num INT IDENTITY(1, 1)
							  , UserHotDealGalleryID  int
							  , HotDealID INT 
							  , HotDealName VARCHAR(255)
							  , HotDealDiscountAmount MONEY
							  , HotDealDiscountPct FLOAT
							  , HotDealDescription VARCHAR(1000) 
							  , HotDealStartDate DATETIME
							  , HotDealEndDate DATETIME
							  , HotDealExpireDate DATETIME
							  , HotDealURL VARCHAR(1000)
							  , HotDealImagePath VARCHAR(1000)
							  , UsedFlag BIT
							  , CategoryID int
							  , CategoryName varchar(100))
							  
	
		   INSERT INTO #Gallery(UserHotDealGalleryID
									,HotDealID   
									,HotDealName  
									,HotDealDiscountAmount  
									,HotDealDiscountPct  
									,HotDealDescription 							
									,HotDealStartDate
									,HotDealEndDate  
									,HotDealExpireDate  
									,HotDealURL  
									,HotDealImagePath  
									,UsedFlag  
									,CategoryID
									,CategoryName)
									
				   SELECT DISTINCT  HcUserHotDealGalleryID
									,P.ProductHotDealID   
									,HotDealName  
									,HotDealDiscountAmount  
									,HotDealDiscountPct  
									,CASE WHEN HotDealShortDescription  IS NOT NULL THEN HotDealShortDescription ELSE HotDeaLonglDescription END
									,HotDealStartDate  
									,HotDealEndDate
									,HotDealExpirationDate
									,HotDealURL  
									,HotDealImagePath=CASE WHEN HotDealImagePath IS NULL
														 THEN [HubCitiApp2_1].[fn_HotDealImagePath](P.ProductHotDealID)
													  ELSE 
														 CASE WHEN HotDealImagePath IS NOT NULL
															  THEN CASE WHEN P.WebsiteSourceFlag = 1 
																		THEN @RetailConfig +CONVERT(VARCHAR(30),P.RetailID)+'/'+HotDealImagePath 
																	ELSE HotDealImagePath 
																	END
														 END 
													 END  
									,Used
									,CategoryID = CASE WHEN C.CategoryID IS NULL THEN 0 ELSE C.CategoryID END
									,CategoryName = CASE WHEN C.ParentCategoryName IS NULL THEN 'Others' ELSE C.ParentCategoryName +' - '+C.SubCategoryName END 
					FROM ProductHotDeal P
					INNER JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
					LEFT JOIN Category C ON P.CategoryID = C.CategoryID
					LEFT JOIN ProductHotDealLocation PL ON P.ProductHotDealID = PL.ProductHotDealID
					LEFT JOIN ProductHotDealRetailLocation PRL ON P.ProductHotDealID = PRL.ProductHotDealID
					WHERE PL.ProductHotDealLocationID IS NULL AND PRL.ProductHotDealRetailLocationID IS NULL
					AND HcUserID = @UserID AND Used = 1
					AND HotDealName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
					AND CASE WHEN HotDealExpirationDate < GETDATE() THEN DATEDIFF(DAY,GETDATE(),HotDealExpirationDate) 
							ELSE DATEPART(DAY,GETDATE()) END <=90  		
					ORDER BY CategoryName,HotDealName ASC 
				
				  --To capture max row number.  
				  SELECT @MaxCnt = MAX(Row_Num) FROM #Gallery  
				  --this flag is a indicator to enable "More" button in the UI.   
				  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
				  SELECT  Row_Num rowNumber 
							,UserHotDealGalleryID hDGallId
							,HotDealID   
							,HotDealName  
							,HotDealDiscountAmount hDDiscountAmount   
							,HotDealDiscountPct hDDiscountPct  
							,HotDealDescription hDDesc 
							,HotDealStartDate hDStartDate  
							,HotDealEndDate hDEndDate 
							,HotDealExpireDate hDExpDate  
							,HotDealURL hdURL 
							,HotDealImagePath  
							,UsedFlag 
							,CategoryID catId
							,CategoryName catName 
				  FROM #Gallery  
				  WHERE Row_Num BETWEEN (@LowerLimit + 1) AND  @UpperLimit  
				  ORDER BY CategoryName,HotDealName ASC  
    
	END TRY  
    
	 BEGIN CATCH   
   
	  --Check whether the Transaction is uncommitable.  
	  IF @@ERROR <> 0  
	  BEGIN  
	   PRINT 'Error occured in Stored Procedure <usp_HcGalleryUsedHotDealsByProduct>.'    
	   --- Execute retrieval of Error info.  
	   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
	  END;  
     
	END CATCH;  
END;







































GO
