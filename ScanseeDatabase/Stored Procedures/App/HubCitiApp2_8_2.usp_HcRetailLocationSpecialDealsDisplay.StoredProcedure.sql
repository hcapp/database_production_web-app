USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailLocationSpecialDealsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_HcRetailLocationSpecialDealsDisplay]  
Purpose				  : To check the different discounts available at the RetailLcoation.
Example				  : [usp_HcRetailLocationSpecialDealsDisplay]
History  
Version		Date           Author	Change Description  
---------------------------------------------------------  
1.4		24th Oct 2013      SPAN		      1.0 
---------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailLocationSpecialDealsDisplay]  
(  
		
	--Input Variables
	  @UserID int
	, @RetailID int  
	, @RetailLocationID int 
	, @HcHubCitiID int	
	
	--OutPut Variables
	, @MenuColor Varchar(500) output
	, @MenuFontColor Varchar(500) output
	, @Status int output  
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
 
		DECLARE @SpecialOfferFlag bit  
			  , @HotDealFlag bit =0
			  , @CouponFlag bit 
			  , @SaleFlag bit
			  , @FundRaisersFlag Bit


		SELECT @MenuColor=MenuButtonColor
			  ,@MenuFontColor=MenuButtonFontColor
		FROM HcMenuCustomUI 
		WHERE HubCitiId=@HcHubCitiID 
			 
			  
		--Check if the RetailLocation has Special Offers
		SELECT @SpecialOfferFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM QRRetailerCustomPage Q
		INNER JOIN QRRetailerCustomPageAssociation QRA ON Q.QRRetailerCustomPageID = QRA.QRRetailerCustomPageID
		INNER JOIN QRTypes QRT ON QRT.QRTypeID = Q.QRTypeID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = QRA.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID
		INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		WHERE QRT.QRTypeName = 'Special Offer Page'
		AND Q.RetailID = @RetailID
		AND QRA.RetailLocationID = @RetailLocationID		
		AND GETDATE() BETWEEN ISNULL(StartDate,GETDATE()-1) AND ISNULL(EndDate, GETDATE() + 1)

		--Check if the RetailLocation has Hotdeals
		SELECT @HotDealFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM ProductHotDeal P
		INNER JOIN ProductHotDealRetailLocation R ON P.ProductHotDealID = R.ProductHotDealID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = R.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID
		INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		LEFT JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
		WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1) AND R.RetailLocationID =@RetailLocationID 
		GROUP BY P.ProductHotDealID
				,NoOfHotDealsToIssue
		HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				 ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)

        
        SELECT @SaleFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM RetailLocation RL
		INNER JOIN RetailLocationDeal RD ON RL.RetailLocationID = RD.RetailLocationID		
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID	
		INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		WHERE GETDATE() BETWEEN ISNULL(SaleStartDate, GETDATE() - 1) AND ISNULL(SaleEndDate, GETDATE() + 1) AND RL.RetailLocationID =@RetailLocationID 

		--Check if the RetailLocation has coupons.
		SELECT @CouponFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM Coupon C
		INNER JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = CR.RetailLocationID AND RL.Active=1
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID
		INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID
		WHERE CR.RetailLocationID = @RetailLocationID
		AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
		GROUP BY C.CouponID
				,NoOfCouponsToIssue
				,CR.RetailLocationID
		HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)     

		----Check if the RetailLocation has Loyalty
		--SELECT LoyaltyDealID, RL.RetailLocationID
		--INTO #Loyalty
		--FROM LoyaltyDeal LD
		--INNER JOIN RetailLocation RL ON RL.RetailLocationID = LD.RetailLocationID
		--LEFT JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HcHubCitiID
		--LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		--WHERE RL.RetailLocationID = @RetailLocationID
		--AND GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)

		----Check if the RetailLocation has Rebate
		--SELECT R.RebateID, RR.RetailLocationID
		--INTO #Rebate
		--FROM RebateRetailer RR
		--INNER JOIN Rebate R ON R.RebateID = RR.RebateID
		--INNER JOIN RetailLocation RL ON RL.RetailLocationID = RR.RetailLocationID
		--LEFT JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode AND HcHubCitiID = @HcHubCitiID
		--LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		--WHERE RR.RetailLocationID = @RetailLocationID

		--Check if the Retailer had created fundraisers
		SELECT @FundRaisersFlag =CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		FROM HcRetailerFundraisingAssociation  RF
		INNER JOIN HcFundraising F ON RF.HcFundraisingID=F.HcFundraisingID
		WHERE RF.RetailID = @RetailID AND RetailLocationID = @RetailLocationID 
		AND GETDATE() <= ISNULL(F.EndDate, GETDATE()+ 1) AND F.Active = 1
		 
		--Enable Discount falg
		CREATE TABLE #Deals(DealName Varchar(100),DealFlag Bit)

		INSERT INTO #Deals(DealName,DealFlag)

		SELECT DealName ,DealFlag
		FROM
		(
		SELECT (CASE WHEN @SpecialOfferFlag=1 THEN 'Special Offers' ELSE NULL END) DealName
		       ,(CASE WHEN @SpecialOfferFlag=1 THEN 1 ELSE NULL END) DealFlag

       UNION ALL

	   SELECT (CASE WHEN @HotDealFlag=1 THEN 'Deals' ELSE NULL END) DealName
		       ,(CASE WHEN @HotDealFlag=1 THEN 1 ELSE NULL END) DealFlag

	   UNION ALL

	   SELECT (CASE WHEN @SaleFlag=1 THEN 'Sales' ELSE NULL END) DealName
		       ,(CASE WHEN @SaleFlag=1 THEN 1 ELSE NULL END) DealFlag

	   UNION ALL

	   SELECT (CASE WHEN @CouponFlag =1 THEN 'Coupons' ELSE NULL END) DealName
		       ,(CASE WHEN @CouponFlag=1 THEN 1 ELSE NULL END) DealFlag

       UNION ALL

	   SELECT (CASE WHEN @FundRaisersFlag=1 THEN 'Fundraisers' ELSE NULL END) DealName
		       ,(CASE WHEN @FundRaisersFlag=1 THEN 1 ELSE NULL END) DealFlag
		)A


		SELECT DealName specialNames,DealFlag 
		FROM #Deals 
		WHERE DealName IS NOT NULL
		ORDER BY DealName 	

		--SELECT @SpecialOfferFlag specialOffFlag
		--	 , @HotDealFlag hotDealFlag
		--	 , @DiscountFlag clrFlag   
		--	 , @SaleFlag saleFlag  

	 --  INTO #TEMP       

		--SELECT @SpecialOfferFlag specialOffFlag
		--	 , @HotDealFlag hotDealFlag
		--	 , @DiscountFlag clrFlag   
		--	 , @SaleFlag saleFlag   													
	
		--Confirmation of Success.
		SELECT @Status = 0	
												  
		
			
 	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcAdminFiltersCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION; 
			--Confirmation of failure.
			SELECT @Status = 1 			
		END;
		 
	END CATCH;
END;













































GO
