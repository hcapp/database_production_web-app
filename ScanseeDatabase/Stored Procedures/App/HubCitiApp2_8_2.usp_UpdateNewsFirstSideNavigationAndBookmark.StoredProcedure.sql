USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_UpdateNewsFirstSideNavigationAndBookmark]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_UpdateNewsFirstSideNavigationAndBookmark]
Purpose					: To display and insert list of News categories on a BookmarkBar.
Example					: [usp_UpdateNewsFirstSideNavigationAndBookmark]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			26 May 2016	     Bindu T A			[usp_UpdateNewsFirstSideNavigationAndBookmark]
1.1			16 Dec 2016		 Bindu T A			Changes with respect to single side navigation throughout the app
1.2			30 Dec 2016		 Sagar Byali		Added Non-Feed news categories updation
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_UpdateNewsFirstSideNavigationAndBookmark]
(   
	--Input variable
	  @LinkID INT,
	  @LevelID INT,
	  @HubcitiID INT 
	 ,@HcuserID INT
	 ,@BookmarkOrder VARCHAR(max)
	 ,@NavigationID VARCHAR(500)
	 ,@HcTemplateID INT
	--Output Variable 
     ,@Status int output
	 ,@ErrorNumber int output
	 ,@ErrorMessage varchar(1000) output 
)
AS
BEGIN
		
	BEGIN TRY

		IF @LinkID IS NULL OR @LinkID = 0
		SELECT @LinkID = (SELECT top 1 HCMenuID FROM HcMenu WHERE HcHubcitiId=@HubcitiID AND Level=1)

		Update a 
		set NewsSideNavigationID = b.HcMenuItemID  
		from HcUserNewsFirstSideNavigation a 
		inner join HcMenuItem b on b.MenuItemName=a.NewsSideNavigationDisplayValue
		inner join  HcMenu on HcMenu.HcMenuID=b.HcMenuID
		where a.Flag=1 and a.NewsSideNavigationID<>b.HcMenuItemID and a.HchubcitiID=@HubcitiID 
		and HcMenu.HchubcitiID=@HubcitiID AND HcMenu.HcMenuID=@LinkID

		DECLARE @SectionOrder VARCHAR(500) = ''

		SELECT @SectionOrder = @SectionOrder + CONVERT(VARCHAR(100),NewsCategoryID) + ','
		FROM NewsFirstCategory
		
		SELECT RowNum = IDENTITY(INT, 1, 1)
		      ,BookmarkSortOrderID = Param
		INTO #Temp1
		FROM dbo.fn_SplitParamMultiDelimiter(@BookmarkOrder, '!~~!')

		--select * from #Temp1

		SELECT RowNum = IDENTITY(INT, 1, 1)
			  ,SectionSortOrderID = Param
		INTO #Temp2
		FROM dbo.fn_SplitParamMultiDelimiter(@SectionOrder, ',')

		-----------------BOOKMARK UPDATION-----------------
		IF (@BookmarkOrder IS NOT NULL AND @BookmarkOrder <> '0' )
		BEGIN
			
						DELETE FROM HcUserNewsFirstBookmarkedCategories 
						WHERE HcUserID = @HcuserID AND HcHubcitiId = @HubcitiID 
			
						INSERT INTO HcUserNewsFirstBookmarkedCategories(  HcUserID
																		, NewscategoryID
																		, NewscategoryDisplayValue
																		, HchubcitiID
																		, Isbookmark
																		, Sortorder
																		, IsNewsFeed
																		, NewsLinkCategoryName)
						SELECT  @HcuserID
							   ,NewsCategoryID
							   ,NC.NewsCategoryDisplayValue
							   ,@HubcitiID
							   ,1
							   ,RowNum
							   ,1
							   ,NULL
						FROM #Temp1 T
						INNER JOIN NewsFirstCategory NC ON T.BookmarkSortOrderID = NC.NewsCategoryDisplayValue
						WHERE Active = 1

						UNION

						SELECT  @HcuserID
							   ,NULL
							   ,NULL
							   ,@HubcitiID
							   ,1
							   ,RowNum
							   ,0
							   ,NewsLinkCategoryName
						FROM #Temp1 T
						INNER JOIN NewsFirstSettings NC ON T.BookmarkSortOrderID = NC.NewsLinkCategoryName
						WHERE IsNewsFeed = 0 and NC.HcHubCitiID = @HubcitiID  AND IsNewsFeed ! =1 
						ORDER BY RowNum

			END
			ELSE IF (@BookmarkOrder = '0')
			BEGIN
				DELETE FROM HcUserNewsFirstBookmarkedCategories WHERE HcUserID = @HcuserID AND HcHubcitiId=@HubcitiID 
		    END
		------------------------------------------------------------

		------------------------SIDE NAVIGATION BAR UPDATION-----------------
		IF (@NavigationID IS NOT NULL AND @NavigationID <> '0') 
		BEGIN
				UPDATE HcUserNewsFirstSingleSideNavigation
				SET IsSideNavigationChanged = 0
				WHERE HchubcitiID= @HubcitiID AND HcUserID = @HcuserID
			
			SELECT  Rownum = IDENTITY(INT, 1, 1),
					NavID = Param,
					ID= replace(replace(Param,'-C',''),'-F',''),
					Flag = CASE WHEN CHARINDEX('-F',Param)>0 THEN 1  ELSE 2 END,
					HcUserID = @HcuserID,
					HcHubCitiID = @HubcitiID
			INTO #NavigationIDs
			FROM dbo.fn_SplitParamMultiDelimiter(@NavigationID, ',')
		
			DELETE FROM HcUserNewsFirstSingleSideNavigation
			WHERE HcUserID = @HcuserID AND HcHubCitiID = @HubcitiID
		
			INSERT INTO HcUserNewsFirstSingleSideNavigation (SortOrder
															,HcUserID
															,NewsSideNavigationID
															,Flag
															,NewsSideNavigationDisplayValue
															,HchubcitiID
															,Active)
			SELECT  Rownum,
					@HcuserID,
					N.ID,
					N.Flag,
					CASE WHEN N.Flag = 1 THEN MI.MenuItemName WHEN N.Flag =2 THEN NC.NewsCategoryName END,
					@HubcitiID,
					1
			FROM #NavigationIDs N
			LEFT JOIN HcMenuItem MI ON N.ID = MI.HcMenuItemID
			LEFT JOIN NewsFirstCategory NC ON NC.NewsCategoryID= N.ID

		END
		ELSE IF (@NavigationID = '0')
		BEGIN
			DELETE FROM HcUserNewsFirstSingleSideNavigation WHERE HcUserID = @HcuserID AND HcHubCitiID = @HubcitiID 	   
		END

		------------------------------------------------------------
		SELECT @Status = 0

END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_UpdateNewsFirstSideNavigationAndBookmark]'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END
		 
	END CATCH
END







GO
