USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcGallaryHotDealRedeem]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcGallaryHotDealRedeem] 
Purpose					:
Example					: [usp_HcGallaryHotDealRedeem]  

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd JAN 2014	Dhananjaya TR  	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcGallaryHotDealRedeem]
(
    --Input Variables
	  @UserID int
	, @HotDealId int
	
	--User Tracking inputs
	, @MainMenuId int 
	
	--Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			UPDATE HcUserHotDealGallery  
			SET Used  = 1
			WHERE HcUserID  = @UserID
			AND HotDealID = @HotDealId	
									
			--User Tracking Session
			UPDATE HubCitiReportingDatabase..HotDealList SET HotDealUsed = 1
			WHERE ProductHotDealID = @HotDealId AND MainMenuID = @MainMenuId
													
			--Confirmation of Success.
			 SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcGallaryHotDealRedeem] .'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








































GO
