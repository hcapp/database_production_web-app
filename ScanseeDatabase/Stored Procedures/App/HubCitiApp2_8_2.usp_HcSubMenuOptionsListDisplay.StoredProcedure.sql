USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcSubMenuOptionsListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcSubMenuOptionsListDisplay]
Purpose					: To display filter and sort page for sub menu
Example					: [usp_HcSubMenuOptionsListDisplay]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			18 May 2015    Span           1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcSubMenuOptionsListDisplay]
(
	 --Input Variable
	  @UserID Int
	, @HcHubCitiID int
	, @MenuID int
	
	--Output Variable 		
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To display Filter items
			DECLARe @Name bit = 1
			DECLARE @Department bit = 0
			DECLARE @Type bit = 0
			DECLARE @City bit = 0

			--To check given HubCiti is Region app or not
			DECLARE @RegionAppFlag bit = 0

			IF(SELECT 1 from HcHubCiti H
				  INNER JOIN HcAppList AL ON H.HcAppListID = AL.HcAppListID 
				  AND H.HcHubCitiID = @HcHubCitiID AND AL.HcAppListName = 'RegionApp')>0
					BEGIN
						SELECT @RegionAppFlag = 1
					END
			 ELSE
					BEGIN
						SELECT @RegionAppFlag =0
					END

			DECLARE @GuestUser int
			DECLARE @CityPrefNotVisitedUser bit

			IF (@RegionAppFlag = 1)
			BEGIN
					SELECT @GuestUser = U.HcUserID
					FROM HcUser U
					INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
					WHERE UserName = 'GuestLogin'
					AND DA.HcHubCitiID = @HcHubCitiID
			
					IF NOT EXISTS (SELECT HcUserID FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HcHubCitiID AND HcUserID = @UserID)
					BEGIN
						SET @CityPrefNotVisitedUser = 1
					END
					ELSE
					BEGIN
						SET @CityPrefNotVisitedUser = 0
					END
			
				--To Check City is there for given menu or not	
			
					SELECT DISTINCT C.HcCityID  
								  , CityName
					INTO #City			  
					FROM HcMenuItem MI
					INNER JOIN HcRegionAppMenuItemCityAssociation RMC ON MI.HcMenuItemID = RMC.HcMenuItemID AND RMC.HCHubcitiID = @HcHubCitiID
					LEFT JOIN HcUsersPreferredCityAssociation UC ON ((RMC.HcCityID = UC.HcCityID OR UC.HcCityID IS NULL) AND UC.HcHubcitiID = @HcHubCitiID AND UC.HcUserID = @UserID) OR (@GuestUser = @UserID) OR (@CityPrefNotVisitedUser = 1)
					INNER JOIN HcCity C ON RMC.HcCityID = C.HcCityID		
					INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
					LEFT JOIN HCDepartments D ON D.HcDepartmentID = MI.HcDepartmentID
					LEFT JOIN HcMenuItemType T ON T.HcMenuItemTypeID = MI.HcMenuItemTypeID
					WHERE RMC.HCHubcitiID = @HcHubCitiID AND (MI.HcMenuID = ISNULL(@MenuID, 0) OR (ISNULL(@MenuID, 0) = 0 )) 
					
					SELECT @City = 1
					FROM #City
					WHERE HcCityID IS NOT NULL

			END


			--To Check Department is there for given menu or not			
			SELECT DISTINCT MI.HcDepartmentID 
							,D.HcDepartmentName 
			INTO #Department
			FROM HcMenuItem MI	
			INNER JOIN HcMenu M ON MI.HcMenuID = M.HcMenuID	
			LEFT JOIN HcDepartments D ON MI.HcDepartmentID = D.HcDepartmentID
			WHERE M.HcHubCitiID = @HcHubCitiID AND M.HcMenuID = @MenuID
			AND  (MI.HcDepartmentID IS NOT NULL
			OR D.HcDepartmentName IS NOT NULL)
			
			--To Check Type is there for given menu or not		
			SELECT DISTINCT MI.HcMenuItemTypeID 
							,D.HcMenuItemTypeName 
			INTO #Type
			FROM HcMenuItem MI	
			INNER JOIN HcMenu M ON MI.HcMenuID = M.HcMenuID	
			LEFT JOIN HcMenuItemType D ON MI.HcMenuItemTypeID = D.HcMenuItemTypeID
			WHERE M.HcHubCitiID = @HcHubCitiID AND M.HcMenuID = @MenuID
			AND  (MI.HcMenuItemTypeID IS NOT NULL
				  OR D.HcMenuItemTypeName IS NOT NULL)
			
			
			SELECT @Department = 1
			FROM #Department
			WHERE HcDepartmentID IS NOT NULL

			SELECT @Type = 1
			FROM #Type
			WHERE HcMenuItemTypeID IS NOT NULL

			CREATE TABLE #Temp (Type Varchar(50), Items Varchar(50),Flag bit)

			INSERT INTO #Temp VALUES ('Sort Items by','Name',@Name)
			INSERT INTO #Temp VALUES ('Filter Items by','Department',@Department)
			INSERT INTO #Temp VALUES ('Filter Items by','Type',@Type)
			INSERT INTO #Temp VALUES ('Filter Items by','City',@City)
				
			SELECT Type AS fHeader
					,Items  AS filterName
			FROM #Temp
			WHERE Flag = 1


			--Confirmation of Success.
			SELECT @Status = 0
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcSubMenuOptionsListDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;























GO
