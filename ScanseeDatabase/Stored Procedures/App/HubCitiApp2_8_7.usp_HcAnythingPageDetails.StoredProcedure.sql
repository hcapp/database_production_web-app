USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcAnythingPageDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcAnythingPageDetails
Purpose					: Update Any thing Page.
Example					: usp_HcAnythingPageDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28/10/2013	    Pavan Sharma K	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcAnythingPageDetails]
(
   ----Input variable.	
   	  @AnythingPageID int
	, @HcHubCitiID Int
	
	--User Tracking Inputs 
	, @MainMenuID Int
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
		DECLARE @HubCitiConfig VARCHAR(1000)
		DECLARE @RetailerConfig varchar(100)
		
		SELECT @HubCitiConfig = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration' 

		SELECT @RetailerConfig = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Web Retailer Media Server Configuration' 
				
				SELECT AnythingPageName pageTitle
					 , ShortDescription sDescription
					 , LongDescription
					 , StartDate
					 , EndDate
					 , logoImagePath = IIF(A.ImageIcon IS NOT NULL, @RetailerConfig + QRRetailerCustomPageIconImagePath, @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + ImagePath)
					 , ImageIcon imageIconId
					 , RTRIM(LTRIM(URL)) pageLink
					 , MediaPath = @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + MediaPath
					 , Mediapath pathName
					 , CASE WHEN URL IS NULL OR URL = '' THEN  HcAnyThingPageMediaTypeID ELSE (SELECT HcAnyThingPageMediaTypeID FROM HcAnyThingPageMediaType WHERE HcAnyThingPageMediaType = 'Website') END pageType				
					 , CASE WHEN MediaPath IS NOT NULL THEN (SELECT @HubCitiConfig + CAST(@HcHubCitiID AS VARCHAR(10))+ '/' + MediaPath FROM HcAnythingPageMedia WHERE HcAnythingPageMediaID = B.HcAnythingPageMediaID) END pageTempLink
				FROM HcAnythingPage A
				LEFT JOIN HcAnythingPageMedia B ON A.HcAnythingPageID = B.HcAnythingPageID
				LEFT JOIN QRRetailerCustomPageIcons D ON D.QRRetailerCustomPageIconID = A.ImageIcon
				LEFT JOIN HcAnyThingPageMediaType C ON C.HcAnyThingPageMediaTypeID = B.MediaTypeID
				WHERE A.HcAnythingPageID = @AnythingPageID
			
			  --User Tracking		
				--CREATE TABLE #Temp(AnythingPageListID Int,AnythingPageID Int)		
				INSERT INTO HubCitiReportingDatabase..AnythingPageList(AnythingPageID, MainMenuID, AnythingPageClick, DateCreated)					
				SELECT @AnythingPageID
					 , @MainMenuID
					 , 1
					 , GETDATE()
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcAnythingPageDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
