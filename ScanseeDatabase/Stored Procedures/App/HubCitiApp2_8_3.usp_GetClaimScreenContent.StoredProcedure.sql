USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_GetClaimScreenContent]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GetScreenContent]
Purpose					: To get Image of Product Summary page of Claim Business and Sales Person ID
Example					: [usp_GetScreenContent]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		 8/22/2016			Bindu T A		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_GetClaimScreenContent]
(
	 @ConfigurationType varchar(50)
	, @HcHubCitiID INT
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @Config varchar(50)
			DECLARE @SalesEmail VARCHAR(500)

			SELECT @SalesEmail = 
			SalesPersonEmail FROM HcHubCiti WHERE HcHubCitiID =  @HcHubCitiID
			
			--To get Email Template URL.
			IF @ConfigurationType = 'ShareURL'
			BEGIN
			SELECT @Config=
			ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Configuration of server'
			END
			--For all the other URL framing.
			ELSE
			BEGIN
			SELECT @Config=
			ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
			END

			SELECT  ConfigurationType
					,ScreenContent	= CASE WHEN ScreenName = 'To_Address' AND ScreenContent<>@salesEmail  THEN ScreenContent+','+@SalesEmail
										WHEN ScreenName = 'To_Address' AND ScreenContent=@salesEmail THEN ScreenContent
										WHEN ScreenName = 'To_Address' AND @salesEmail IS NULL THEN ScreenContent
										WHEN ScreenName = 'Subject'	THEN ScreenContent END						
					,ScreenName 
					,Active 
					,DateCreated
					,DateModified
			FROM  AppConfiguration 
			WHERE ConfigurationType=@ConfigurationType


	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GetScreenContent].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;








GO
