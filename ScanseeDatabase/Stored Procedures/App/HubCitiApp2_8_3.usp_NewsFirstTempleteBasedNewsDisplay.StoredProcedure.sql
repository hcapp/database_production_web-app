USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_NewsFirstTempleteBasedNewsDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssFeedNewsDisplay
Purpose					: 
Example					: usp_WebRssFeedNewsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_NewsFirstTempleteBasedNewsDisplay]
(	
	  @TempleteType varchar(500)
	, @HubCitiID int 
	, @SearchKey varchar(500) 	
		
	--Output Variable 
	, @bannerImg VARCHAR(1000) output
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
				DECLARE @UpperLimit INT
					   ,@LowerLimit INt
					   ,@RowsPerPage INT
					   ,@ScreenName VARCHAR(1000)
					   ,@MaxCnt INT
					   ,@NxtPageFlag INT

			    SET @bannerImg = 'NewsBanner.png'
	
	            SELECT @UpperLimit = @LowerLimit + 3 
                     , @RowsPerPage = ScreenContent
                FROM AppConfiguration   
                WHERE ScreenName = @ScreenName 
                AND ConfigurationType = 'Pagination'
                AND Active = 1   	  

		  IF @TempleteType='Scrolling'
		  BEGIN

				SELECT ROW_NUMBER() over(order by RssFeedNewsID) AS RowNumber 
				    ,RssFeedNewsID 
					,NewsType 
					,Title
					,ImagePath 
					,ShortDescription 
					,LongDescription 
					,Link 
					,CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END PublishedDate
					,convert(datetime,PublishedDate,105) PubStrtDate
					,Classification
					,Section
					,AdCopy
					--,   Time
					
			  INTO #News
			  FROM RssFeedNews RFN
			  WHERE HcHubCitiID = @HubCitiID 
			  AND ((RFN.Title IS NOT NULL 
			  AND (((@SearchKey IS NOT NULL) AND (Title LIKE '%'+@SearchKey+'%' OR NewsType LIKE '%'+@SearchKey+'%'))
							OR (@SearchKey IS NULL) OR ( @SearchKey = ''))))
			 
			
			SELECT @MaxCnt = count(RowNumber) FROM #News
                                  
               --this flag is a indicator to enable "More" button in the UI.   
               --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
            SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			
				SELECT 
					RowNumber rowNumber
				    ,RssFeedNewsID itemId
					,NewsType feedType
					,Title
					,ImagePath image
					,ShortDescription sDesc
					,LongDescription lDesc
					,Link link
					,PublishedDate date
					,PubStrtDate
					,Classification
					,Section
					,AdCopy
					--,LEFT AS imgPosition  
				FROM #News


			 
			END		
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;











GO
