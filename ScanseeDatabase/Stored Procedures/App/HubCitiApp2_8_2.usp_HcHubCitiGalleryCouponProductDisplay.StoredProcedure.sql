USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiGalleryCouponProductDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HcHubCitiGalleryCouponProductDisplay    
Purpose               : To display Product details Associated to given Coupon  
Example               : usp_HcHubCitiGalleryCouponProductDisplay    
    
History
Version		   Date			Author	  Change Description
-----------------------------------------------------------
1.0			6/11/2013	    SPAN	       1.0
-----------------------------------------------------------  
*/    
    
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiGalleryCouponProductDisplay]    
(  

	--Input Variables  
	  @UserID int    
	, @CouponID int 
	, @HcHubcitiID int   

	--User Tracking input Variables
	, @CouponListID int
	, @MainMenuID int
	 
	--OutPut Variables
	, @Status int output
	, @ErrorNumber int output    
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY    
		
		--To get Server Configuration
		DECLARE @ManufConfig varchar(50) 
		SELECT @ManufConfig = ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		SELECT  rowNumber = ROW_NUMBER() OVER(ORDER BY ProductName)
				,CP.ProductID
				,P.ProductName
				,ProductDescription = CASE WHEN P.ProductShortDescription IS NULL THEN P.ProductLongDescription ELSE P.ProductShortDescription END
				,ProductImagePath = CASE WHEN P.WebsiteSourceFlag = 1
										 THEN @ManufConfig + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+ P.ProductImagePath
									ELSE P.ProductImagePath END
		INTO #ProductList						
		FROM Coupon C
		INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
		INNER JOIN Product P ON CP.ProductID = P.ProductID
		WHERE  C.CouponID = @CouponID 
		--ORDER BY rowNumber
	  
	   --User Tracking section.
	   CREATE TABLE #Temp1(ProductListID INT	
						, ProductID INT)
			
	   INSERT INTO HubCitiReportingDatabase..ProductList(MainMenuID
												         ,ProductID
												         ,DateCreated
													     )
	   OUTPUT inserted.ProductListID, inserted.ProductID INTO #Temp1(ProductListID,ProductID)
													   SELECT @MainMenuID
															, productId
															, GETDATE()
													   FROM #ProductList
	   
		--Display along with the primary key of the tracking table.
		SELECT rowNumber
			 , T.ProductListID prodListID
			 , P.productId
			 , productName
			 , ProductDescription  
			 , ProductImagePath  
		FROM #ProductList P
		INNER JOIN #Temp1 T ON T.ProductID = P.productId
		
	    --Confirmation of Success.
		SELECT @Status = 0	
	 
 END TRY    
     
	BEGIN CATCH  

		--Check whether the Transaction is uncommitable.  
		IF @@ERROR <> 0  
		BEGIN  
		PRINT 'Error occured in Stored Procedure [usp_HcHubCitiGalleryCouponProductDisplay].'    
		--- Execute retrieval of Error info.  
		EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
		SELECT @Status = 1
		END;  
	 
	END CATCH;  
END;










































GO
