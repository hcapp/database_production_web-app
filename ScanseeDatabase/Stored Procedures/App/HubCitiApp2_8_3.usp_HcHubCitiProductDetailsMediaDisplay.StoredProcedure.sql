USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiProductDetailsMediaDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiProductDetailsMediaDispaly
Purpose					: To diplay Product related Audio, Video and other Files in HubCiti.
Example					: usp_HcHubCitiProductDetailsMediaDispaly 1

History
Version		   Date			Author	  Change Description
-----------------------------------------------------------
1.0			6/11/2013	    SPAN	       1.0
-----------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiProductDetailsMediaDisplay]
(
	  @ProductID int
	 ,@MediaType varchar(20)
	 ,@HcHubCitiID int
	 
	 --User Tracking Inputs
     , @ProductListID int
     	 
	 --OutPut Variable
	 , @Status int output 
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
				
			--To get Server Configuration
			 DECLARE @ManufConfig varchar(50) 
			 DECLARE @Config varchar(50)
			  
			 SELECT @ManufConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
			 
			  SELECT @Config = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='App Media Server Configuration'  
			 
			-- To Fetch Product Media info based on the media type sent.
			SELECT  PM.ProductMediaID 
					, PM.ProductMediaName 
					, ProductMediaPath = CASE WHEN PM.ProductMediaPath IS NOT NULL 
											  THEN CASE WHEN (P.WebsiteSourceFlag = 1 OR ProductMediaPath NOT LIKE '%http%')
														THEN @ManufConfig+CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +PM.ProductMediaPath 
												   ELSE PM.ProductMediaPath END   
										 ELSE PM.ProductMediaPath END  
			INTO #ProductMediaList
			FROM ProductMedia PM
			INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID 
			LEFT JOIN Product P ON P.ProductID = PM.ProductID
			WHERE PM.ProductID = @ProductID 
			AND PT.ProductMediaType = @MediaType
			
			--User Tracking section.
			
			CREATE TABLE #Temp(ProductMediaListID int,ProductMediaID int)

			INSERT INTO HubCitiReportingDatabase..ProductMediaList(ProductListID
																  ,ProductMediaID
																  ,DateCreated)
			OUTPUT inserted.ProductMediaListID,inserted.ProductMediaID INTO #Temp(ProductMediaListID,ProductMediaID)
															SELECT @ProductListID 
																  ,ProductMediaID 
																  ,GETDATE()
															FROM #ProductMediaList 
																  
			SELECT  T.ProductMediaListID pmLstID 
					, P.ProductMediaID  
					, ProductMediaName 
					, ProductMediaPath			
			FROM #ProductMediaList P 
			INNER JOIN #Temp T ON P.ProductMediaID = T.ProductMediaID 
							
			--Confirmation of Success.
			SELECT @Status = 0		
			COMMIT TRANSACTION
							
	END TRY
		 
	BEGIN CATCH	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiProductDetailsMediaDispaly.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
