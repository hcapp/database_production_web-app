USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcGetUserModuleRadius]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcGetUserModuleRadius]
(
	  @UserID int  
	, @ModuleName varchar(50)
	--Output Variable 
	
	, @Radius int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	--Fetch the default radius for find module.
	IF @ModuleName = 'Find'
	BEGIN
		IF EXISTS(SELECT 1 FROM HcUserPreference WHERE HcUserID = @UserID)
		BEGIN
			SELECT @Radius = LocaleRadius
			FROM HcUserPreference
			WHERE HcUserID = @UserID
		END
		
		--If the user has not configured any default radius then consider the global radius
		IF @Radius IS NULL
		BEGIN
			SELECT @Radius = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'DefaultRadius'
			AND ScreenName = 'DefaultRadius'
			AND Active = 1
		END
	END
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GetUserModuleRadius].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;












































GO
