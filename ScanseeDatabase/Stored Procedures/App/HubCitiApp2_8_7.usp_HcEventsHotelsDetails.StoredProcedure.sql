USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcEventsHotelsDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcEventsHotelsDetails
Purpose					: To display details of Hotel.
Example					: usp_HcEventsHotelsDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcEventsHotelsDetails]
(   
    --Input variable.
	  @UserID Int	  
	, @HubCitiID int
    , @HcEventID Int
    , @RetailLocationID Int	
	--, @Latitude Float
	--, @Longitude Float
	--, @PostalCode Varchar(100)

	--User Tracking 
	--, @MainMenuID Int
	, @RetailerListID Int
  
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
			--DECLARE @Latitude Float
			--DECLARE @Longitude Float
			--DECLARE @PostalCode Varchar(100)

			DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Web Retailer Media Server Configuration'

			DECLARE @RetailConfig varchar(50)
			SELECT @RetailConfig=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Web Retailer Media Server Configuration'

			----Select User Postalcode
			--if @PostalCode is null 
			--begin
			--Select @PostalCode=PostalCode
			--From HcUser where HcUserID =@UserID 
			--End

			--if @Latitude is null and @Longitude is null
			--begin
			--SELECT @Latitude=Latitude
			--      ,@Longitude=Longitude
			--FROM GeoPosition
			--WHERE PostalCode=@PostalCode
			--end


			--To display details of Hotel
			SELECT DISTINCT RL.RetailLocationID as retailLocationId
				  ,R.RetailID
				  ,R.RetailName
				  ,RL.Address1	as address		      
				  ,HotelPrice as price
				  ,ISNULL(E.ShortDescription,E.LongDescription) description
				 -- ,Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN RL.RetailLocationLongitude IS NULL THEN G.Longitude ELSE RL.RetailLocationLongitude END/ 57.2958))))*6371) * 0.6214 END
			      ,E.PackagePrice 
				  ,ImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 				 
				  ,C.ContactPhone 
				  ,EP.Rating				  
				  ,EP.RoomAvailabilityCheckURL as roomCheckUrl
				  ,EP.RoomBookingURL as roomBookUrl	 	
			FROM HcEvents E 
			INNER JOIN HcEventPackage EP ON EP.HcEventID =E.HcEventID 
			INNER JOIN RetailLocation RL ON RL.RetailLocationID =EP.RetailLocationID 
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID 
			INNER JOIN GeoPosition G ON G.PostalCode=RL.PostalCode
			LEFT JOIN RetailContact RC ON RL.RetailLocationID =RC.RetailLocationID 
			LEFT JOIN Contact C ON RC.ContactID =C.ContactID
			WHERE E.HcHubCitiID=@HubCitiID AND E.HcEventID=@HcEventID AND EP.RetailLocationID=@RetailLocationID AND RL.Active = 1 AND R.RetailerActive = 1
							
			
			--User Tracking			
			
			UPDATE HubCitiReportingDatabase..RetailerList SET RetailLocationClick =1
			WHERE RetailerListID =@RetailerListID						
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcEventsHotelsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


















































GO
