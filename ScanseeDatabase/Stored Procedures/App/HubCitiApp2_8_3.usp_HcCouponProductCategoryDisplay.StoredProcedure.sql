USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcCouponProductCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcCouponProductCategoryDisplay]
Purpose					: To Display ProductCategory Names
Example					: [usp_HcCouponProductCategoryDisplay]

History
Version		 Date		Author	           Change Description
------------------------------------------------------------------------------- 
1.0		 21stNov 2013	Dhananjaya TR	   Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcCouponProductCategoryDisplay]
(

	--Input Variables 	
	   @UserID int
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(500)	
	 , @HcHubcitiID Int
	
	--Output Variables	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		--Check if the User has Configured any categories
		DECLARE @CategorySet bit
		SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
		FROM HcUserCategory 
		WHERE hcUserID = @UserID 
		
		--To get Default Radius
		DECLARE @Radius FLOAT
		SELECT @Radius = ScreenContent 
		FROM AppConfiguration
		WHERE ScreenName = 'DefaultRadius'
		AND ConfigurationType = 'DefaultRadius'
			 
		--Derive the Latitude and Longitude in the absence of the input.
		IF @Latitude IS NULL AND @Longitude IS NULL		
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
					 , @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
					 , @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @UserID
			END
		END
		--Check if the user has given latitude and longitude
		--IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)
		--BEGIN									
			--To get all the ProductCategory Names.
			SELECT  catId
					,catName
			FROM
			--Coupons associated to Products but not to RetailLocations W.R.T Latitude and Longitude
			(SELECT DISTINCT catId  = CASE WHEN CAT.CategoryID IS NULL THEN '-1' ELSE CAT.CategoryID END
					,catName = CASE WHEN CAT.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END
			FROM Coupon C
			INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			LEFT JOIN ProductCategory PC ON CP.ProductID = PC.ProductID
			LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
			LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			LEFT JOIN RetailLocation RL ON RL.RetailLocationID=CR.RetailLocationID AND RL.Active = 1
			LEFT JOIN HcLocationAssociation HL ON HL.PostalCode=RL.PostalCode AND HL.HcCityID=RL.HcCityID AND RL.StateID=HL.StateID
			LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
			WHERE CR.RetailLocationID IS NULL AND HL.HcHubCitiID=@HcHubcitiID
			AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
			AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE() + 1)
			
			--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
			UNION 
			SELECT DISTINCT catId  = CASE WHEN CAT.CategoryID IS NULL THEN '-1' ELSE CAT.CategoryID END
					,catName = CASE WHEN CAT.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END
			FROM Coupon C
			INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			LEFT JOIN ProductCategory PC ON CP.ProductID = PC.ProductID
			LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
			INNER JOIN HcLocationAssociation HL ON HL.PostalCode=RL.PostalCode AND HL.HcCityID=RL.HcCityID AND RL.StateID=HL.StateID
			INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
			WHERE HL.HcHubCitiID=@HcHubcitiID AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
			AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE() + 1)
			AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
			
			--Coupons not associated to RetailLocations and Products W.R.T Latitude and Longitude
			UNION
			SELECT DISTINCT catId  = CASE WHEN CAT.CategoryID IS NULL THEN '-1' ELSE CAT.CategoryID END
					,catName = CASE WHEN CAT.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END
			FROM Coupon C
			LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			LEFT JOIN ProductCategory PC ON CP.ProductID = PC.ProductID
			LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
			LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
			LEFT JOIN HcLocationAssociation HL ON HL.PostalCode=RL.PostalCode AND HL.HcCityID=RL.HcCityID AND RL.StateID=HL.StateID
			LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
			WHERE CR.CouponRetailerID IS NULL AND CP.CouponProductId IS NULL AND HL.HcHubCitiID=@HcHubcitiID 
			AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
			AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
			)Cat
			ORDER BY catName ASC
			
		--END
		--Check if user has given populationCenterID
		
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcCouponProductCategoryDisplay].'		
		-- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;















































GO
