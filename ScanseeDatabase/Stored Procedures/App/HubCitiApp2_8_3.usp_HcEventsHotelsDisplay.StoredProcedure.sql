USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcEventsHotelsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcEventsHotelsDisplay
Purpose					: To display List fo Events Categories.
Example					: usp_HcEventsHotelsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcEventsHotelsDisplay]
(   
    --Input variable.
	  @UserID Int	  
	, @HubCitiID int
    , @HcEventID Int
	, @LowerLimit int  
	, @ScreenName varchar(200)
	, @Latitude Float
	, @Longitude Float
	, @PostalCode Varchar(100)

	--User Tracking 
	, @MainMenuID Int
  
	--Output Variable 
	, @UserOutOfRange bit output
	, @DefaultPostalCode VARCHAR(10) output
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @UpperLimit int
			DECLARE @DistanceFromUser FLOAT
			--DECLARE @Latitude Float
			--DECLARE @Longitude Float
			--DECLARE @PostalCode Varchar(100)

			DECLARE @RetailConfig VARCHAR(500)
	        SELECT @RetailConfig = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Web Retailer Media Server Configuration'

			DECLARE @UserLatitude float
			DECLARE @UserLongitude float 

			SELECT @UserLatitude = @Latitude
				 , @UserLongitude = @Longitude

			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
					 , @UserLongitude = Longitude
				FROM HcUser A
				INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				WHERE HcUserID = @UserID 
			END
			--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HubCitiID
			END

			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1  
			
			--To fetch all the duplicate retailers.
			SELECT DISTINCT DuplicateRetailerID 
			INTO #DuplicateRet
			FROM Retailer 
			WHERE DuplicateRetailerID IS NOT NULL 

			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
				IF @PostalCode IS NULL
				BEGIN
					SELECT @Latitude = G.Latitude
						 , @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID
				END
				ELSE
				BEGIN
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude
					FROM GeoPosition 
					WHERE PostalCode = @PostalCode
				END
			END


			--To display List fo Alerts Categories
			SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1) --DISTINCT ROW_NUMBER() OVER (ORDER BY RL.RetailLocationID) Rownum
			      ,RL.RetailLocationID 
				  ,R.RetailID
				  ,R.RetailName 
			      ,Rating
				  ,HotelPrice
				  ,Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN RL.RetailLocationLongitude IS NULL THEN G.Longitude ELSE RL.RetailLocationLongitude END/ 57.2958))))*6371) * 0.6214 END
				  ,DistanceActual=CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN RL.RetailLocationLongitude IS NULL THEN G.Longitude ELSE RL.RetailLocationLongitude END/ 57.2958))))*6371), 1, 1) * 0.6214 END
			      ,E.PackagePrice 
				  ,ImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
			INTO #Events
			FROM HcEvents E 
			INNER JOIN HcEventPackage EP ON EP.HcEventID =E.HcEventID 
			INNER JOIN RetailLocation RL ON RL.RetailLocationID =EP.RetailLocationID 
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID 
			INNER JOIN GeoPosition G ON G.PostalCode=RL.PostalCode
			LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
			WHERE E.HcHubCitiID=@HubCitiID AND E.HcEventID=@HcEventID
			AND D.DuplicateRetailerID IS NULL AND RL.Active = 1 AND R.RetailerActive = 1
			
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Events
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			--User Tracking

			CREATE TABLE #Temp(RetailerListID Int,RetailLocationID Int)

			INSERT INTO HubCitiReportingDatabase..RetailerList( MainMenuID
																,RetailLocationID
																,RetailID
																,RetailLocationClick																															
																,DateCreated)

			OUTPUT inserted.RetailerListID,inserted.RetailLocationID INTO #Temp(RetailerListID,RetailLocationID)

			SELECT @MainMenuID
			      ,RetailLocationID
				  ,RetailID 
				  ,0
				  ,GETDATE()
			FROM #Events

			SELECT DISTINCT Rownum
			      ,T.RetailerListID retListId
			      ,E.RetailID retailerId
			      ,E.RetailLocationID 
			      ,RetailName as RetailerName
				  ,Rating
				  ,HotelPrice as price
				  ,Distance = ISNULL(DistanceActual, Distance)
				  ,ImagePath logoImagePath
			FROM #Events E
			INNER JOIN #Temp T ON E.RetailLocationID =T.RetailLocationID 
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
			--GROUP BY Rownum
					--,T.RetailerListID 
					--,E.RetailID
					--,E.RetailLocationID
					--,RetailName
					--,Rating
					--,HotelPrice
					--,Distance
					--,ImagePath
		    ORDER BY RowNum 									
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcEventsHotelsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

















































GO
