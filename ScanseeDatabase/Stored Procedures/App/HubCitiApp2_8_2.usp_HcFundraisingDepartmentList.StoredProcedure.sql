USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcFundraisingDepartmentList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFundraisingDepartmentList
Purpose					: To display List of Departments related to FundRaising.
Example					: usp_HcFundraisingDepartmentList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21/11/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcFundraisingDepartmentList]
(   
    --Input variable.	  
	  @HubCitiID int
	, @UserID Int
	, @HcMenuItemID int
	, @HcBottomButtonID int
	, @RetailID int
	, @RetailLocationID int

	--User Tracking
  
	--Output Variable 
	--, @MaxCnt int  output
	--, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @Config Varchar(500)
				
			----To get the Configuration Details.	 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			--MenuItem
			SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
			INTO #RegionCat
			FROM HcHubCiti H                     
			INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
			INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
			INNER JOIN HcMenuItemFundraisingCategoryAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcMenuItemID = @HcMenuItemID
			WHERE H.HcHubCitiID = @HubCitiID 

					 
			CREATE TABLE #AllEventCategories(HubcitiID INT,FundraisingCategoryID INT,FundraisingCategoryName VARCHAR(100),FundraisingID INT,FundraisingDepartmentID INT)

			INSERT INTO #AllEventCategories(HubcitiID,FundraisingCategoryID,FundraisingCategoryName,FundraisingID,FundraisingDepartmentID)
					 
			SELECT DISTINCT HR.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
			FROM HcHubCiti H
			LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
			--INNER JOIN HcEvents E ON HR.HcHubCitiID = E.HcHubCitiID  
			INNER JOIN HcFundraisingCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
			INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
			INNER JOIN #RegionCat RC ON HE.HcFundraisingCategoryID = RC.HcFundraisingCategoryID
			INNER JOIN HcMenuItemFundraisingCategoryAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcMenuItemID = @HcMenuItemID 
			WHERE HR.HcRegionAppID = @HubCitiID 
                    
			UNION ALL

			SELECT HcHubCitiID,HcFundraisingCategoryID,FundraisingCategoryName,HcFundraisingID,HcFundraisingDepartmentID
			FROM #RegionCat

			--BottomButton
			SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
			INTO #RegionCat1
			FROM HcHubCiti H                     
			INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
			INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
			INNER JOIN HcFundraisingBottomButtonAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID
			WHERE H.HcHubCitiID = @HubCitiID

 
			CREATE TABLE #AllEventCategories1(HubcitiID INT,FundraisingCategoryID INT,FundraisingCategoryName VARCHAR(100),FundraisingID INT,FundraisingDepartmentID INT)

			INSERT INTO #AllEventCategories1(HubcitiID,FundraisingCategoryID,FundraisingCategoryName,FundraisingID,FundraisingDepartmentID)
					
			SELECT DISTINCT HR.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
			FROM HcHubCiti H
			LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
			--INNER JOIN HcEvents E ON HR.HcHubCitiID = E.HcHubCitiID  
			INNER JOIN HcFundraisingCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
			INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
			INNER JOIN #RegionCat1 RC ON HE.HcFundraisingCategoryID = RC.HcFundraisingCategoryID
			INNER JOIN HcFundraisingBottomButtonAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID 
			WHERE HR.HcRegionAppID = @HubCitiID 
                    
			UNION ALL

			SELECT HcHubCitiID,HcFundraisingCategoryID,FundraisingCategoryName,HcFundraisingID,HcFundraisingDepartmentID
			FROM #RegionCat1

			CREATE TABLE #RHubcitiList(HchubcitiID Int) 
			INSERT INTO #RHubcitiList( HchubcitiID)
			SELECT HcHubCitiID
			FROM
				(SELECT RA.HcHubCitiID
				FROM HcHubCiti H
				LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
				WHERE H.HcHubCitiID  = @HubcitiID
				UNION ALL
				SELECT @HubcitiID)A

			DECLARE @HubCitis varchar(100)
			SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
			FROM #RHubcitiList
			
			CREATE TABLE #FundraiserDepartmentList(RowNum INT IDENTITY(1,1)
								,HcFundraisingDepartmentID  int
								,FundraisingDepartmentName Varchar(1000))

			
			IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
				BEGIN

					 INSERT INTO #FundraiserDepartmentList(HcFundraisingDepartmentID  
												  ,FundraisingDepartmentName) 
				
					SELECT DISTINCT	FD.HcFundraisingDepartmentID 
								   ,FD.FundraisingDepartmentName  
								   --,IIF(EC.CategoryImagePath IS NOT NULL ,@Config+EC.CategoryImagePath,EC.CategoryImagePath) catImgPath
					FROM #AllEventCategories A
					--HcMenuItemFundraisingCategoryAssociation MF
					--INNER JOIN HcFundraisingCategoryAssociation FC ON FC.HcFundraisingCategoryID =MF.HcFundraisingCategoryID AND FC.HcHubcitiID=@HubCitiID AND MF.HcMenuItemID = @HcMenuItemID OR FC.HcHubCitiID IS NULL
					INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=A.FundraisingDepartmentID 	
					INNER JOIN HcFundraising F ON F.HcFundraisingID=A.FundraisingID	
					WHERE GETDATE() <= ISNULL(F.EndDate, GETDATE()+ 1)
					--ORDER BY FD.FundraisingDepartmentName  ASC 

					UNION ALL

					SELECT DISTINCT	FD.HcFundraisingDepartmentID  
								   ,FD.FundraisingDepartmentName  

					FROM HcLocationAssociation HL
							INNER JOIN #AllEventCategories A on HL.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcRetailerAssociation R on A.HubcitiID = R.HcHubCitiID and Associated=1
							INNER JOIN HcFundraising F on R.RetailID = F.RetailID and F.RetailID is not null
							INNER JOIN HcFundraisingCategoryAssociation FCA on F.HcFundraisingID = FCA.HcFundraisingID and FCA.HcHubCitiID is null and A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
							INNER JOIN HcFundraisingCategory FC on FCA.HcFundraisingCategoryID=FC.HcFundraisingCategoryID
							INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=FCA.HcFundraisingDepartmentID 
							--LEFT JOIN HcRetailerFundraisingAssociation L on F.HcFundraisingID = L.HcFundraisingID and R.RetailID = L.RetailID
							--LEFT JOIN RetailLocation RL on RL.RetailLocationID=L.RetailLocationID 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1

				END			
				
				ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
				BEGIN

					INSERT INTO #FundraiserDepartmentList(HcFundraisingDepartmentID  
												  ,FundraisingDepartmentName)

					SELECT DISTINCT	FD.HcFundraisingDepartmentID  
								   ,FD.FundraisingDepartmentName  
								   --,IIF(EC.CategoryImagePath IS NOT NULL ,@Config+EC.CategoryImagePath,EC.CategoryImagePath) catImgPath
					FROM #AllEventCategories1 A
					--HcFundraisingBottomButtonAssociation BF
					--INNER JOIN HcFundraisingCategoryAssociation FC ON FC.HcFundraisingCategoryID =BF.HcFundraisingCategoryID AND FC.HcHubcitiID=@HubCitiID AND BF.HcBottomButtonID = @HcBottomButtonID OR FC.HcHubCitiID IS NULL
					INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=A.FundraisingDepartmentID 	
					INNER JOIN HcFundraising F ON F.HcFundraisingID=A.FundraisingID	
					WHERE GETDATE() <= ISNULL(F.EndDate, GETDATE()+ 1)
					--ORDER BY FD.FundraisingDepartmentName  ASC

					UNION ALL

					SELECT DISTINCT	FD.HcFundraisingDepartmentID  
								   ,FD.FundraisingDepartmentName  
					FROM HcLocationAssociation HL
							INNER JOIN #AllEventCategories A on HL.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcRetailerAssociation R on A.HubcitiID = R.HcHubCitiID and Associated=1
							INNER JOIN HcFundraising F on R.RetailID = F.RetailID and F.RetailID is not null
							INNER JOIN HcFundraisingCategoryAssociation FCA on F.HcFundraisingID = FCA.HcFundraisingID and FCA.HcHubCitiID is null and A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
							INNER JOIN HcFundraisingCategory FC on FCA.HcFundraisingCategoryID=FC.HcFundraisingCategoryID
							INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=FCA.HcFundraisingDepartmentID 
							--LEFT JOIN HcRetailerFundraisingAssociation L on F.HcFundraisingID = L.HcFundraisingID and R.RetailID = L.RetailID
							--LEFT JOIN RetailLocation RL on RL.RetailLocationID=L.RetailLocationID 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
				END
				
				ELSE IF(@RetailID IS NOT NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NULL)
				BEGIN 

					INSERT INTO #FundraiserDepartmentList(HcFundraisingDepartmentID  
												  ,FundraisingDepartmentName)

					SELECT DISTINCT	FD.HcFundraisingDepartmentID  
								   ,FD.FundraisingDepartmentName  
					FROM HcFundraising F 
							 INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID =FCA.HcFundraisingID 
							 INNER JOIN HcFundraisingCategory FC ON FCA.HcFundraisingCategoryID =FC.HcFundraisingCategoryID 
							 INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=FCA.HcFundraisingDepartmentID 
							 INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated =1							 
							 INNER JOIN HcRetailerFundraisingAssociation RF ON RA.RetailLocationID = RF.RetailLocationID AND F.HcFundraisingID = RF.HcFundraisingID
							 INNER JOIN RetailLocation RL1 ON RL1.RetailLocationID =RF.RetailLocationID	AND RL1.Active = 1
							 --INNER JOIN HcCity C ON RL1.City = C.CityName
							 WHERE F.RetailID =@RetailID AND RF.RetailLocationID = @RetailLocationID
							 AND  GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 
					
					--HcLocationAssociation HL
					--		INNER JOIN #AllEventCategories A on HL.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
					--		INNER JOIN HcRetailerAssociation R on A.HubcitiID = R.HcHubCitiID and Associated=1
					--		INNER JOIN HcFundraising F on R.RetailID = F.RetailID and F.RetailID is not null
					--		INNER JOIN HcFundraisingCategoryAssociation FCA on F.HcFundraisingID = FCA.HcFundraisingID and FCA.HcHubCitiID is null and A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
					--		INNER JOIN HcFundraisingCategory FC on FCA.HcFundraisingCategoryID=FC.HcFundraisingCategoryID
					--		INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=FCA.HcFundraisingDepartmentID 
					--		--LEFT JOIN HcRetailerFundraisingAssociation L on F.HcFundraisingID = L.HcFundraisingID and R.RetailID = L.RetailID
					--		--LEFT JOIN RetailLocation RL on RL.RetailLocationID=L.RetailLocationID 
					--		WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
				END

				ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NULL)
				BEGIN 
							INSERT INTO #FundraiserDepartmentList(HcFundraisingDepartmentID  
																,FundraisingDepartmentName) 

							SELECT DISTINCT FD.HcFundraisingDepartmentID 
										   ,FD.FundraisingDepartmentName
							                	
							FROM HcFundraising F 
							INNER JOIN #RHubcitiList HL ON HL.HchubcitiID=F.HcHubCitiID
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID=FCA.HcFundraisingID 
							INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID =FCA.HcFundraisingDepartmentID 							
							LEFT JOIN HcRetailerFundraisingAssociation 	RF ON RF.HcFundraisingID=F.HcFundraisingID								                         
							LEFT JOIN HcFundraisingAppsiteAssociation FA ON F.HcFundraisingID = FA.HcFundraisingID					
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1	AND RF.HcFundraisingID IS NULL														    	   
							
							UNION ALL								
					
						
							 --To display List of Events for Retiler
							 SELECT DISTINCT FD.HcFundraisingDepartmentID 
										    ,FD.FundraisingDepartmentName													
							 FROM HcFundraising F 
							 INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID =FCA.HcFundraisingID 
							 INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID =FCA.HcFundraisingDepartmentID 	
							 INNER JOIN HcFundraisingCategory FC ON FCA.HcFundraisingCategoryID =FC.HcFundraisingCategoryID 
							 --INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated =1							 
							 INNER JOIN HcRetailerFundraisingAssociation RF ON F.HcFundraisingID = RF.HcFundraisingID --RA.RetailLocationID = RF.RetailLocationID
							 INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID=RF.RetailLocationID AND Associated =1 AND RA.HcHubCitiID =@HubCitiID
							
							 WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 

					

				END


				SELECT HcFundraisingDepartmentID departmentId
					  ,FundraisingDepartmentName departmentName
				FROM #FundraiserDepartmentList
				ORDER BY FundraisingDepartmentName


			--To get list of BottomButtons for this Module
			DECLARE @ModuleName Varchar(50) 
			SET @ModuleName = 'FundRaising'

			EXEC [HubCitiApp2_3_3].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,@ModuleName,@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output		    

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcFundraisingDepartmentList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





























GO
