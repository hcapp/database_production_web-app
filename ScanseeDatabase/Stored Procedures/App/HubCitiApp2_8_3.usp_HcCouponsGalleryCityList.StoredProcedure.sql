USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcCouponsGalleryCityList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcCouponsGalleryCityList]
Purpose					: To Display Gallery coupons City list. 
Example					: [usp_HcCouponsGalleryCityList]

History
Version		 Date		     Author	           Change Description
------------------------------------------------------------------------------- 
2.0        12/27/2016    Shilpashree       Introducing Coupon Gallery city list
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcCouponsGalleryCityList]
(
	--Input Variables 	
	   @HcUserID int
	 , @HcHubcitiID int
	 , @SearchKey varchar(500)
	 , @Latitude Decimal(18,6)
	 , @Longitude Decimal(18,6)
	 , @PostalCode varchar(100)	
	 
	--Output Variables
	 , @Status bit output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
			
		--To get Media Server Configuration.  
		DECLARE  @DistanceFromUser FLOAT
				, @DefaultPostalCode Varchar(10)
				, @UserOutOfRange bit
				, @Radius FLOAT
				, @LocCategory Int
		
		CREATE TABLE #RetailerBusinessCategory(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)
		CREATE TABLE #GalleryCoupons(CouponID int,Claimed bit,Redeemed bit, Expired bit)
		CREATE TABLE #Coupons(CouponID INT
								, CouponName VARCHAR(500)
								, RetailID INT
								, RetailName VARCHAR(200)
								, CouponImagePath VARCHAR(500)
								, BannerTitle VARCHAR(1000)
								, Distance FLOAT
								, Counts INT
								, Claimed BIT
								, Redeemed BIT
								, Expired BIT)

		SELECT @Radius = LocaleRadius
			FROM HcUserPreference 
				WHERE HcUserID = @HcUserID

		IF (@Radius IS NULL)
		BEGIN

			SELECT @Radius=ScreenContent 
				FROM AppConfiguration
					WHERE ScreenName = 'DefaultRadius' AND ConfigurationType = 'DefaultRadius'	
		END

		EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @HcUserID, @HcHubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
						, @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
						, @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @HcUserID
			END
		END	

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
			FROM Retailer 
				WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive = 1

		SELECT DISTINCT BusinessCategoryID,HcBusinessSubCategoryID 
		INTO #UserPrefLocCategories
			FROM HcUserPreferredCategory 
				WHERE HcUserID = @HcUserID AND HcHubCitiID = @HcHubcitiID 

		SELECT @LocCategory = COUNT(1) FROM #UserPrefLocCategories 

		IF (@LocCategory = 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
					WHERE RA.HcHubCitiID = @HcHubcitiID
		END
		ELSE IF (@LocCategory > 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
				INNER JOIN #UserPrefLocCategories PC ON RBC.BusinessCategoryID = PC.BusinessCategoryID --AND RBC.BusinessSubCategoryID = PC.HcBusinessSubCategoryID
					WHERE RA.HcHubCitiID = @HcHubcitiID
		END
		
		INSERT INTO #GalleryCoupons
		SELECT DISTINCT UG.CouponID
						,Claimed = CASE WHEN Claim = 1 AND UsedFlag = 0 AND ISNULL(C.ActualCouponExpirationDate,CouponExpireDate+30) >= GETDATE() THEN 1 ELSE 0 END
						,UsedFlag AS Redeemed
						,Expired = CASE WHEN Claim = 1 AND UsedFlag = 0 AND ISNULL(C.ActualCouponExpirationDate,CouponExpireDate+30) < GETDATE() THEN 1 ELSE 0 END
		FROM HcUserCouponGallery UG
		INNER JOIN Coupon C ON UG.CouponID = C.CouponID
		WHERE HcuserID = @HcUserID

		SELECT DISTINCT GC.CouponID
					,RL.HcCityID
					,RL.City
					,Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
		INTO #RetailerSet
		FROM #GalleryCoupons GC
		INNER JOIN Coupon C ON GC.CouponID = C.CouponID
		INNER JOIN CouponRetailer CR ON GC.CouponID = CR.CouponID
		INNER JOIN #RetailerBusinessCategory RBC  ON CR.RetailID = RBC.RetailerID
		INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1
		INNER JOIN Retaillocation RL ON R.RetailID = RL.RetailID AND CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
		INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID = RL.RetailLocationID AND Associated = 1 AND RA.HcHubCitiID = @HcHubcitiID
		INNER JOIN HcLocationAssociation HL ON RL.HcCityID = HL.HcCityID AND RL.PostalCode = HL.PostalCode AND HL.HcHubCitiID = RA.HcHubcitiID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL
		AND (C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
				OR C.KeyWords LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END)

		SELECT DISTINCT HcCityID AS cityId
				, City AS cityName
			FROM #RetailerSet 
				WHERE Distance <= @Radius
					ORDER BY City

		--Confirmation of success.
		SET @Status = 0

		END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcCouponsGalleryCityList].'		
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of failure.
			SET @Status = 1
					
		END;
		 
	END CATCH;
END;





GO
