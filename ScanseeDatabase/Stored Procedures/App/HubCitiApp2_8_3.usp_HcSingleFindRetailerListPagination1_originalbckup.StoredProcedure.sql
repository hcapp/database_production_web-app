USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcSingleFindRetailerListPagination1_originalbckup]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  usp_HcFindRetailerListPagination
Purpose                 :  To get the list of near by retailer for Find Module.
Example                 :  

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/17/2012       SPAN            Initial Version
1.0			03/03/2016       Sagar Byali	 Revised Version - Find Performance Changes
1.0			08/23/2016       Prakash C 	     Rewrite full code for Optimization
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcSingleFindRetailerListPagination1_originalbckup]
(
				  --Input Parameters 
					 @Postalcode varchar(100)
				   , @UserConfiguredPostalCode Varchar(10)
				   , @CategoryName varchar(2000)
				   , @Latitude Decimal(18,6)
				   , @Longitude  Decimal(18,6)
				   , @Radius int
				   , @LowerLimit int  
				   , @ScreenName varchar(50)
				   , @HCHubCitiID Int
				   , @HcMenuItemId Int
				   , @HcBottomButtonId Int
				   , @SearchKey Varchar(100)
				   , @BusinessSubCategoryID Varchar(2000) 
				   , @SortColumn varchar(50)
				   , @SortOrder varchar(10)   
				   , @HcCityID Varchar(3000)
				   , @FilterID Varchar(1000)
				   , @FilterValuesID Varchar(1000) 
				   , @LocalSpecials bit 
				   , @Interests Varchar(1000) 
				   , @requestedTime Time
				   , @MainMenuID int
				   --Output Variable
				   , @NoRecordsMsg nvarchar(max) output
				   , @UserOutOfRange bit output  
				   , @DefaultPostalCode varchar(50) output             
				   , @MaxCnt int  output
				   , @NxtPageFlag bit output 
				   , @Status bit output  
				   , @ErrorNumber int output  
				   , @ErrorMessage varchar(1000) output 
) --with recompile
AS
BEGIN
    BEGIN TRY      
            SET NOCOUNT ON
		
			        DECLARE  
     				  @CategoryID INT  
					, @RowCount int 
					, @Count INT = 1
					, @MaxCount INT 
					, @SQL VARCHAR(1000)                     
					, @RetSearch VARCHAR(1000)
					, @Config varchar(50)                     
					, @RetailConfig varchar(50)
					, @UpperLimit int   
					, @Tomorrow DATETIME = GETDATE() + 1
					, @Yesterday DATETIME = GETDATE() - 1
					, @RowsPerPage int
					, @NearestPostalCode varchar(50)
					, @DistanceFROMUser FLOAT
					, @UserLatitude float
					, @UserLongitude float 
					, @SpecialChars VARCHAR(1000)
					, @ModuleName varchar(100) = 'Find'
					, @RegionAppFlag Bit
					, @GuestLoginFlag BIT=0
					, @BusinessCategoryID Int
					, @PostalCodeCopy VARCHAR(10)
					, @Length INT
					, @Globalimage varchar(50)
					, @CategoryName1 VARCHAR(100) = @CategoryName
					, @HcMenuItemId1 INT = @HcMenuItemId 
					, @HCHubCitiID1 INT =  @HCHubCitiID
					, @BusinessSubCategoryID1 VARCHAR(1000) = @BusinessSubCategoryID
					, @HcBottomButtonId1 INT = @HcBottomButtonId
					, @CategoryID1 INT 
					,@Latitude11 bigint=@Latitude

                   -- DECLARE @BusinessSubCategoryIDs TABLE (ID int identity(1,1), BusCatIDs int)
					DECLARE @Interestss TABLE (ID int identity(1,1), IntIDs int)
					DECLARE @Filter TABLE (ID int identity(1,1), FilterIDs int)
					DECLARE @FilterValues TABLE (ID int identity(1,1), FilterValuesIDs int)
					DECLARE @BusinessSubCategoryID1s Table (ID int identity(1,1), BusCatIDs int)

					CREATE TABLE #BusinessCategory(BusinessCategoryID int , HcBusinessSubCategoryID int)
					CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
					CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100))
					CREATE TABLE #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)
					CREATE TABLE #RetailItemsonSale1(Retailid bigint , RetailLocationid bigint)
                    CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                    , RetailID INT 
                                    , RetailName VARCHAR(1000)
                                    , RetailLocationID INT
                                    , Address1 VARCHAR(1000)
									, Address2 varchar(1000)
                                    , City VARCHAR(1000)    
                                    , State CHAR(2)
                                    , PostalCode VARCHAR(20)
                                    , retLatitude FLOAT
                                    , retLongitude FLOAT
                                    , RetailerImagePath VARCHAR(1000)      
                                    , Distance FLOAT  
                                    , DistanceActual FLOAT                                                   
                                    , BusinessSubCategoryID int
									, SaleFlag BIT
									, locationOpen VARCHAR(10))
					 
					SET @PostalCodeCopy = @Postalcode
                    SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
					SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
							WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
							WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
							ELSE @SearchKey END)
				    SET @UserLatitude = @Latitude
                    SET @UserLongitude = @Longitude
     --               SET @Globalimage=[dbo].[fn_ConfigvalueforScreencontent]('Image Not Found')
					--SET @Config=[dbo].[fn_ConfigvalueforScreencontent]('App Media Server Configuration')
					--SET @RetailConfig=[dbo].[fn_ConfigvalueforScreencontent]('Web Retailer Media Server Configuration')
					--SET @RowsPerPage=[dbo].[fn_ConfigvalueforScreencontent]('Pagination')
					--SET @UpperLimit=isnull(@UpperLimit,0)+convert(int,[dbo].[fn_ConfigvalueforScreencontent]('Pagination'))

					 SELECT @Config=ScreenContent
					FROM AppConfiguration 
					WHERE ConfigurationType='App Media Server Configuration'

					SELECT @RetailConfig=ScreenContent
					FROM AppConfiguration 
					WHERE ConfigurationType='Web Retailer Media Server Configuration'

					SELECT @Globalimage =ScreenContent 
					FROM AppConfiguration 
					WHERE ConfigurationType ='Image Not Found'

					SELECT @RowsPerPage = ScreenContent,
					@UpperLimit = @LowerLimit + ScreenContent 
					FROM AppConfiguration   
					WHERE ScreenName = @ScreenName 
					AND ConfigurationType = 'Pagination'
					AND Active = 1   

					SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName1

					INSERT INTO @BusinessSubCategoryID1s (BusCatIDs)
					SELECT Param
					FROM fn_SplitParam (@BusinessSubCategoryID1,',')

				    INSERT INTO @Interestss (IntIDs)
					SELECT Param
					FROM fn_SplitParam (@Interests,',')

					--Filter Implementation 
					IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
					BEGIN
							SELECT Param FilterIDs
							INTO #Filters
							FROM fn_SplitParam(@FilterID,',') P
							LEFT JOIN AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
							WHERE AF.AdminFilterID IS NULL

							SELECT Param FilterValues
							INTO #FilterValues
							FROM fn_SplitParam(@FilterValuesID,',')

							SELECT DISTINCT RetailLocationID 
							INTO #FilterRetaillocations
							FROM(
							SELECT RF.RetailLocationID                                    
							FROM RetailerFilterAssociation RF
							INNER JOIN #FilterValues F ON F.FilterValues =RF.AdminFilterValueID AND BusinessCategoryID =@BusinessCategoryID
							UNION
						    SELECT RF.RetailLocationID                                    
							FROM RetailerFilterAssociation RF
							INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BusinessCategoryID =@BusinessCategoryID
							)A

					END

					--To collect Cities passed as input for RegionApp
					SELECT Param CityID
					INTO #CityIDs
					FROM fn_SplitParam(@HcCityID,',')

				    IF(SELECT 1 FROM HcHubCiti H
                             INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                             AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
                    BEGIN
							SET @RegionAppFlag = 1
									
							INSERT INTO #RHubcitiList(HcHubCitiID)
							(SELECT DISTINCT HcHubCitiID 
							FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
							UNION ALL
							SELECT  @HCHubCitiID1 AS HcHubCitiID
							)

							INSERT INTO #CityList(CityID,CityName,PostalCode)		
							SELECT DISTINCT LA.HcCityID 
											,LA.City	
											,PostalCode				
							FROM #RHubcitiList H
							INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID1   						
							INNER JOIN #CityIDs C ON LA.HcCityID=C.CityID 
                    END
					ELSE
                    BEGIN
							SET @RegionAppFlag =0
							INSERT INTO #RHubcitiList(HchubcitiID)
							SELECT @HCHubCitiID1  
							INSERT INTO #CityList(CityID,CityName,PostalCode)
							SELECT DISTINCT HcCityID 
										   ,City
										   ,PostalCode					
							FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID1                                   
			        END
					
					SELECT @UserLatitude = @Latitude
						 , @UserLongitude = @Longitude

					IF (@UserLatitude IS NULL) 
					BEGIN
							SELECT @UserLatitude = Latitude
								 , @UserLongitude = Longitude
							FROM GeoPosition 
							WHERE PostalCode = @PostalCode
					END
                     --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
					IF (@UserLatitude IS NULL) 
					BEGIN
							SELECT @UserLatitude = Latitude
								 , @UserLongitude = Longitude
							FROM HcHubCiti A
							INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
							WHERE A.HcHubCitiID = @HCHubCitiID1
					END

			        --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
					EXEC [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFROMUser OUTPUT
					--select @DefaultPostalCode = defaultpostalcode from hchubciti where HcHubCitiid = @HcHubCitiID  and @PostalCode is null
					SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

					--Set the Next page flag to 0 initially.
					SET @NxtPageFlag = 0

					--Derive the Latitude and Longitude in the absence of the input.
				    IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
				    BEGIN
						SELECT @Latitude = Latitude
								, @Longitude = Longitude
						FROM GeoPosition 
						WHERE PostalCode = ISNULL(@PostalCode,@PostalCodeCopy)                                                                              
					END  
					
					--To fetch all the duplicate retailers.
					SELECT DISTINCT DuplicateRetailerID 
					INTO #DuplicateRet
					FROM Retailer 
					WHERE DuplicateRetailerID IS NOT NULL

				    SELECT @CategoryID1 = BusinessCategoryID 
					FROM BusinessCategory     WHERE BusinessCategoryName = @CategoryName
					
                    IF (@HcMenuItemId IS NOT NULL)
						BEGIN 
                            INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,HcBusinessSubCategoryID               
							FROM HcMenuFindRetailerBusinessCategories F
							WHERE F.BusinessCategoryid = @CategoryID1 AND HcMenuItemID = @HcMenuItemId
						END
					ELSE IF (@HcBottomButtonID IS NOT NULL)
					BEGIN
		
							INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,HcBusinessSubCategoryID               
							FROM HcBottomButtonFindRetailerBusinessCategories F  
							WHERE F.BusinessCategoryid = @CategoryID1 AND HcBottomButonID = @HcBottomButtonID
					END

					SELECT @CategoryID = BusinessCategoryID
					FROM #BusinessCategory                       
                                     
					SELECT DISTINCT ST.BusinessCategoryID  
					INTO #SubCategorytype  
					FROM HcBusinessSubCategorytype ST
					INNER JOIN HcBusinessSubCategory SC ON ST. HcBusinessSubCategorytypeID = SC.HcBusinessSubCategorytypeID
					WHERE BusinessCategoryID=@BusinessCategoryID

					SELECT DISTINCT BusinessCategoryID INTO #NONSUB FROM BusinessCategory WITH(NOLOCK)   WHERE BusinessCategoryID=@BusinessCategoryID  
					EXCEPT
					SELECT DISTINCT BusinessCategoryID  FROM #SubCategorytype WITH(NOLOCK)
					        
                    SELECT DISTINCT * INTO #HcMenuFindRetailerBusinessCategories 
					FROM HcMenuFindRetailerBusinessCategories  WHERE  BusinessCategoryID=@BusinessCategoryID
					AND HcBusinessSubCategoryID IS NOT NULL
					AND HcMenuItemID=@HcMenuItemId

					SELECT DISTINCT * INTO #HcMenuFindRetailerBusinessCategories1 
					FROM HcMenuFindRetailerBusinessCategories  WHERE  BusinessCategoryID=@BusinessCategoryID
					AND HcBusinessSubCategoryID IS  NULL
					AND HcMenuItemID=@HcMenuItemId

					SELECT DISTINCT * INTO #HcBottomButtonFindRetailerBusinessCategories 
					from HcBottomButtonFindRetailerBusinessCategories  where  BusinessCategoryID=@CategoryID
					AND HcBusinessSubCategoryID IS NOT NULL
					and HcBottomButonID=@HcBottomButtonID

					SELECT DISTINCT * INTO #HcBottomButtonFindRetailerBusinessCategories1 
					from HcBottomButtonFindRetailerBusinessCategories  where  BusinessCategoryID=@CategoryID
					AND HcBusinessSubCategoryID IS  NULL
					and HcBottomButonID=@HcBottomButtonID

					SELECT DISTINCT * INTO #RetailerBusinessCategory2  FROM RetailerBusinessCategory WHERE  BusinessCategoryID=@BusinessCategoryID

					CREATE  INDEX  ix_nn  on #HcMenuFindRetailerBusinessCategories(BusinessCategoryID,
					HcBusinessSubCategoryID)

					CREATE clustered INDEX  ix_nn1  on #HcMenuFindRetailerBusinessCategories(HcMenuItemID)

					SELECT DISTINCT RS.HcBusinessSubCategoryID
					INTO #HcRetailerSubCategory
					FROM #HcMenuFindRetailerBusinessCategories HCB
					INNER JOIN HcRetailerSubCategory RS on HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID

                    IF EXISTS(SELECT  1 FROM #SubCategorytype)
                    BEGIN
                            IF @HcMenuItemId  is not null
									 BEGIN

										 INSERT  INTO #RetailerBusinessCategory1
										 SELECT DISTINCT  RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
										 FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										 INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
										 ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										 AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
										 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										 INNER JOIN #HcRetailerSubCategory RS ON HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID
										 WHERE HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@CategoryID

										 INSERT  INTO #RetailerBusinessCategory1
										 SELECT  DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
										 FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										 INNER JOIN #HcMenuFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
										 ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										 WHERE HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@CategoryID
									 END
							ELSE IF @HcBottomButtonID  is not null
								BEGIN
										INSERT  INTO #RetailerBusinessCategory1
										SELECT  distinct RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
										FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										INNER JOIN #HcBottomButtonFindRetailerBusinessCategories HCB WITH(NOLOCK)
										ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
										INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										INNER JOIN #HcRetailerSubCategory RS ON HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID
										where HCB.HcBottomButonID=@HcBottomButtonID and RBC.BusinessCategoryID=@CategoryID

										INSERT  INTO #RetailerBusinessCategory1
										SELECT  distinct RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
										FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										INNER JOIN #HcBottomButtonFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
										ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										where HCB.HcBottomButonID=@HcBottomButtonID and RBC.BusinessCategoryID=@CategoryID
								END

								ELSE
								BEGIN
									SELECT DISTINCT BusinessCategoryID INTO #BusinessCategory1 FROM #BusinessCategory

									INSERT  INTO #RetailerBusinessCategory1
									SELECT  DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
									FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									INNER JOIN #BusinessCategory1 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									WHERE RBC.BusinessCategoryID=@CategoryID
								END
					END
					ELSE 
					BEGIN
							SELECT DISTINCT BusinessCategoryID INTO #BusinessCategory2 FROM #BusinessCategory

							INSERT  INTO #RetailerBusinessCategory1
							SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
							FROM RetailerBusinessCategory RBC
							INNER JOIN #BusinessCategory2 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
							INNER JOIN #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID
							AND RBC.BusinessCategoryID=@BusinessCategoryID

					END

				    SELECT  #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1                  

					SELECT DISTINCT R.RetailID ,RetailName,WebsiteSourceFlag,RetailerImagePath,RBC.BusinessSubCategoryID
					INTO #tempree
					FROM Retailer R 
					INNER JOIN #RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
					AND RBC.BusinessCategoryID = @BusinessCategoryID

				

                    SELECT DISTINCT HL.* INTO #city  FROM HcLocationAssociation HL 
					INNER JOIN #CityList CL ON  CL.CityID =HL.HcCityID 
					INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID

					; with aa as(
					SELECT  DISTINCT RL.RetailID  --,HL.HcCityID    
					, R.RetailName
					, RL.RetailLocationID 
					,WebsiteSourceFlag,RetailerImagePath
					, saleflag= 0  ,HL.HchubcitiID ,BusinessSubCategoryID	
					FROM #tempree r
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID 
					INNER JOIN #city HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID 
					)

					select * into #SELECT1  from aa

					

					SELECT * INTO #SELECT2 FROM #SELECT1  WHERE 1<>1

					IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
 
					INSERT INTO #SELECT2
					SELECT #SELECT1.*  FROM #SELECT1   INNER JOIN #FilterRetaillocations  ON #FilterRetaillocations.RetailLocationID=#SELECT1.RetailLocationID


					ELSE
					BEGIN
					INSERT INTO #SELECT2
					SELECT #SELECT1.*  FROM #SELECT1
					END

                     SELECT * INTO #SELECT  FROM #SELECT2


					SELECT DISTINCT 	
							  #SELECT.RetailID,RetailName
							, RL.RetailLocationID ,WebsiteSourceFlag,RetailerImagePath
							, saleflag= 0  ,HchubcitiID --INTO #tempre
							, RL.Address1    , RL.Address2     
							, RL.City
							, RL.State
							, RL.PostalCode,Headquarters,Active
							, RetailLocationLatitude,RetailLocationLongitude,RetailLocationImagePath,BusinessSubCategoryID
							, locationOpen  = 
									CASE WHEN RL.StartTimeUTC < RL.ENDTimeUTC THEN (CASE WHEN CAST(@requestedTime as time) >= RL.StartTimeUTC AND CAST(@requestedTime as time) < RL.ENDTimeUTC THEN 'Open'
									WHEN RL.StartTimeUTC IS NULL OR RL.ENDTimeUTC IS NULL THEN 'N/A'
									ELSE 'Close' END)
									WHEN RL.StartTimeUTC >= RL.ENDTimeUTC THEN (CASE WHEN CAST(@requestedTime AS TIME) >= RL.StartTimeUTC AND convert(int,substring(convert(varchar(20),@requestedTime),1,2))<convert(int,substring(convert(varchar(20),RL.ENDTimeUTC),1,2))+24 THEN 'Open'
									WHEN CAST(@requestedTime AS TIME) < RL.ENDTimeUTC THEN 'Open'
									ELSE 'Close' END  )
									WHEN RL.StartTimeUTC IS NULL OR RL.ENDTimeUTC IS NULL THEN 'N/A'
									ELSE 'Close' END--INTO #tempre
					INTO #tempre1
					FROM RetailLocation  RL INNER JOIN #SELECT on #SELECT.RetailLocationID=RL.RetailLocationID

					SELECT  RL.* INTO  #tempre FROM #tempre1 RL
					INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID 
					AND RLC.HcHubCitiID = RL.HchubcitiID  AND Associated =1

					
					SELECT DISTINCT Rl.RetailID     
					, Rl.RetailName
					, RL.RetailLocationID 
					, RL.Address1   , RL.Address2     
					, RL.City
					, RL.State
					, RL.PostalCode
					, ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
					, ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
					, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
					, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available AND 1 = Sale available
					, RL.BusinessSubCategoryID
					, SaleFlag = 0  ,locationOpen
					INTO #Retail3
					FROM #tempre RL
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode  AND G.City = RL.City                                   
				    LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID
					LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID 
					LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
					WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1 
					AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
					OR (@SearchKey IS NULL))
					AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM @Interestss))

					IF NOT EXISTS (SELECT 1 FROM #NONSUB) OR (@BusinessSubCategoryID IS NOT NULL)					
					BEGIN
						
						INSERT INTO #Retail(RetailID
									, RetailName
									, RetailLocationID 
									, Address1   
									, Address2     
									, City
									, State
									, PostalCode
									, retLatitude
									, retLongitude
									, RetailerImagePath
									, Distance
									, DistanceActual
									, SaleFlag
									, locationOpen)
						SELECT DISTINCT R.RetailID
									, R.RetailName
									, R.RetailLocationID 
									, R.Address1   
									, R.Address2     
									, R.City
									, R.State
									, R.PostalCode
									, R.retLatitude
									, R.retLongitude
									, R.RetailerImagePath
									, R.Distance
									, R.DistanceActual
									, R.SaleFlag
									, R.locationOpen   
						FROM #Retail3 R
						INNER JOIN HcRetailerSubCategory S ON R.RetailLocationID = S.RetailLocationID AND R.BusinessSubCategoryID = S.HcBusinessSubCategoryID	
						WHERE Distance <= @Radius
						AND (@BusinessSubCategoryID1 IS NULL OR S.HcBusinessSubCategoryID IN (SELECT BusCatIDs FROM @BusinessSubCategoryID1s)) 
					END
					ELSE IF EXISTS (SELECT 1 FROM #NONSUB)
					BEGIN
						INSERT INTO #Retail(RetailID
									, RetailName
									, RetailLocationID 
									, Address1   
									, Address2     
									, City
									, State
									, PostalCode
									, retLatitude
									, retLongitude
									, RetailerImagePath
									, Distance
									, DistanceActual
									, SaleFlag
									, locationOpen)
						SELECT DISTINCT R.RetailID
									, R.RetailName
									, R.RetailLocationID 
									, R.Address1   
									, R.Address2     
									, R.City
									, R.State
									, R.PostalCode
									, R.retLatitude
									, R.retLongitude
									, R.RetailerImagePath
									, R.Distance
									, R.DistanceActual
									, R.SaleFlag
									, R.locationOpen  
						FROM #Retail3 R
						WHERE Distance <= @Radius
					END

					insert  INTO  #RetailItemsonSale1
					SELECT DISTINCT R.RetailID, a.RetailLocationID 
					FROM RetailLocationDeal a 
					INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
					INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
					AND a.ProductID = c.ProductID
					AND GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) AND ISNULL(a.SaleENDDate, @Tomorrow)
					insert  INTO  #RetailItemsonSale1
					SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
					FROM Coupon C 
					INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
					INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
					LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
					WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
					GROUP BY C.CouponID
					,NoOfCouponsToIssue
					,CR.RetailID
					,CR.RetailLocationID
					HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
					ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   

					insert  INTO  #RetailItemsonSale1
					SELECT DISTINCT  RR.RetailID, 0 as RetaillocationID  
					FROM Rebate R 
					INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
					INNER JOIN #Retail RE ON RE.RetailLocationID =RR.RetailLocationID
					WHERE GETDATE() BETWEEN RebateStartDate AND RebateENDDate 

					insert  INTO  #RetailItemsonSale1

					SELECT DISTINCT R.retailid, a.RetailLocationID 
					FROM  LoyaltyDeal a
					INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
					INNER JOIN #Retail R ON R.RetailLocationID =a.RetailLocationID
					INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID 
					AND b.ProductID = LDP.ProductID 
					WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

					insert  INTO  #RetailItemsonSale1

					SELECT DISTINCT R.RetailID, R.RetailLocationID
					FROM ProductHotDeal p
					INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
					INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
					LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
					LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
					WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealENDDate, @Tomorrow)
					GROUP BY P.ProductHotDealID
					,NoOfHotDealsToIssue
					,R.RetailID
					,R.RetailLocationID
					HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
					ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

					insert  INTO  #RetailItemsonSale1

					SELECT q.RetailID, qa.RetailLocationID
					FROM QRRetailerCustomPage q
					INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
					INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
					INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID AND qt.QRTypeName = 'Special Offer Page'
					WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') AND isnull(q.ENDdate,@Tomorrow)
					--) Discount 

					SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
												ELSE SplashAdImagePath
												END
												, SplashAdID  = ASP.AdvertisementSplashID
												, R.RetailLocationID  
										 INTO #aa
					FROM #Retail R 
					INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
					INNER JOIN RetailLocation RL ON RL.RetailLocationID=RS.RetailLocationID
					INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.ENDDate, GETDATE() + 1)

					SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
												ELSE AB.BannerAdImagePath
												END 
												, RibbonAdURL = AB.BannerAdURL
												, RetailLocationAdvertisementID = AB.AdvertisementBannerID
												, R.RetailLocationID
										  INTO #bb
					FROM #Retail R 
					INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
					INNER JOIN RetailLocation RL ON RL.RetailLocationID=RB.RetailLocationID
					INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
					WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.ENDDate, GETDATE() + 1)
					
				    SELECT DISTINCT RowNum rowNumber  
										, R.RetailID retailerId    
										, RetailName retailerName
										, R.RetailLocationID retailLocationID
										, Address1 retaileraddress1
										, Address2 retaileraddress2
										 , City City
										, State State
										, PostalCode PostalCode
										, retLatitude
										, retLongitude    
										, RetailerImagePath logoImagePath     
										, Distance
										, DistanceActual                         
										, S.BannerAdImagePath bannerAdImagePath    
										, B.RibbonAdImagePath ribbonAdImagePath    
										, B.RibbonAdURL ribbonAdURL    
										, B.RetailLocationAdvertisementID advertisementID  
										, S.SplashAdID splashAdID
										, SaleFlag = CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
										, locationOpen
                    INTO #Retailer    
					FROM #Retail R
					LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
					LEFT JOIN #aa S ON R.RetailLocationID = S.RetailLocationID
					LEFT JOIN #bb B ON R.RetailLocationID = B.RetailLocationID

					SELECT		rowNumber =  ROW_NUMBER() OVER (ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
								WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
								WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
								END ASC ,Distance)  
								, retailerId    
								, retailerName
								, retailLocationID
								, retaileraddress1
								, retaileraddress2
								, City
								, State
								, PostalCode
								, retLatitude
								, retLongitude    
								, logoImagePath     
								, Distance
								, DistanceActual                         
								, bannerAdImagePath    
								, ribbonAdImagePath    
								, ribbonAdURL    
								, advertisementID  
								, splashAdID
								, SaleFlag 
								, locationOpen
					INTO #Retailer1 
					FROM #Retailer
					WHERE (ISNULL(@LocalSpecials,0) = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))
					--To capture max row number.  
					SELECT @MaxCnt = count(1) FROM #Retailer1

				

					--this flag is a indicator to enable "More" button in the UI.   
					--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
					SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

					SELECT DISTINCT   rowNumber  
									, retailerId    
									, retailerName
									, retailLocationID
									, retaileraddress1
									, retaileraddress2
									, City
									, State
									, PostalCode
									, retLatitude
									, retLongitude    
									, logoImagePath     
									, Distance
									, DistanceActual                         
									, bannerAdImagePath    
									, ribbonAdImagePath    
									, ribbonAdURL    
									, advertisementID  
									, splashAdID
									, SaleFlag 
									, locationOpen  
					INTO #RetailerList 
					FROM #Retailer1
					WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
					ORDER BY rowNumber   
					--  SELECT '334', getdate()                
					INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
					,CityID
					,DateCreated)
					SELECT DISTINCT @MainMenuID 
									,CityID 
									,GETDATE()
									FROM #CityList 
					--Table to track the Retailer List.
					CREATE TABLE #Temp(Rowno int IDENTITY(1,1)
					,RetailerListID int
					,LocationDetailID int
					,MainMenuID int
					,RetailID int
					,RetailLocationID int)  

					--Capture the impressions of the Retailer list.
					INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
					, RetailID
					, RetailLocationID
					, FindCategoryID
					, DateCreated)
                    OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)                                             
					SELECT DISTINCT @MainMenuId
					, retailerId
					, RetailLocationID
					, @CategoryID
					, GETDATE()
					FROM #RetailerList  


					
					--Display the Retailer along with the keys generated in the Tracking table.
					SELECT  rowNumber
					, T.RetailerListID retListID
					, retailerId
					, retailerName
					, T.RetailLocationID
					, retaileraddress1
					, retaileraddress2
					, City
					, State
					, PostalCode                                   
					, retLatitude
					, retLongitude
					, Distance = case when @Latitude11 is null then  ISNULL(Distance,DistanceActual ) else   ISNULL(DistanceActual,Distance) end
					, logoImagePath
					, bannerAdImagePath
					, ribbonAdImagePath
					, ribbonAdURL
					, advertisementID
					, splashAdID
					, SaleFlag   
					, locationOpen
					FROM #Temp T
					INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID 
					ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(case when @Latitude11 is null then  ISNULL(Distance,DistanceActual ) else   ISNULL(DistanceActual,Distance) end AS SQL_VARIANT)
								WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
								WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
								END ASC    

					--To display bottom buttons                                                
					EXEC [HubCitiApp2_8_3].[Find_usp_HcFunctionalityBottomButtonDisplay] @HCHubCitiID1, @ModuleName, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

					SELECT @Status = 0  
				 
					DECLARE @UserPrefCities NVarchar(MAX)

					IF (@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0) AND (@SearchKey IS NULL) AND @BusinessSubCategoryID IS NULL AND @HcCityID IS NULL 
					AND @FilterID IS NULL AND  @FilterValuesID IS NULL AND @LocalSpecials = 0 AND @Interests IS NULL
					BEGIN 
							SELECT DISTINCT cityname into #tempp FROM #CityList
							SELECT @UserPrefCities = COALESCE( @UserPrefCities+',' ,'') + UPPER(LEFT( CityName,1))+LOWER(SUBSTRING( CityName,2,LEN( CityName))) 
							FROM #tempp
						
							SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
							'.\n\nUpdate your city preferences in the settings menu.'

					END
					ELSE IF ((@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0)) 
					AND (@SearchKey IS NOT NULL OR @BusinessSubCategoryID IS NOT NULL OR @HcCityID IS NOT NULL OR @FilterID IS NOT NULL
					OR  @FilterValuesID IS NOT NULL OR @LocalSpecials = 1 OR @Interests IS NOT NULL)
					BEGIN
							SELECT @NoRecordsMsg = 'No Records Found.'
							END
							ELSE IF (@RegionAppFlag = 0) AND (ISNULL(@MaxCnt,0) = 0) 
							BEGIN
							SELECT @NoRecordsMsg = 'No Records Found.'
					END

					--DROP TABLE #BusinessCategory
					--DROP TABLE #RHubcitiList
					--DROP TABLE #CityList
					--DROP TABLE #RetailerBusinessCategory1
					--DROP TABLE #RetailItemsonSale1
					--DROP TABLE #Retail1
					--DROP TABLE #Filters
					--DROP TABLE #FilterValues
					--DROP TABLE #FilterRetaillocations
					--DROP TABLE #CityIDs
					--DROP TABLE #DuplicateRet
					--DROP TABLE #SubCategorytype
					--DROP TABLE #NONSUB
					--DROP TABLE #HcMenuFindRetailerBusinessCategories
					--DROP TABLE #HcMenuFindRetailerBusinessCategories1
					--DROP TABLE #RetailerBusinessCategory2
					--DROP TABLE #BusinessCategory1
					--DROP TABLE #BusinessCategory2
					--DROP TABLE #RetailerBusinessCategory
					--DROP TABLE #tempree
					--DROP TABLE #city
					--DROP TABLE #SELECT
					--DROP TABLE #tempre1
					--DROP TABLE #tempre
					--DROP TABLE #Retail
					--DROP TABLE #aa
					--DROP TABLE #bb
					--DROP TABLE #Retailer
					--DROP TABLE #Retailer1
					--DROP TABLE #RetailerList
					--DROP TABLE #Temp
    END TRY
            
	BEGIN CATCH
					INSERT  INTO SCANSEEVALIDATIONERRORS(ERRORCODE,ERRORLINE,ERRORDESCRIPTION,ERRORPROCEDURE,Inputon)
					VALUES(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE(),getdate())
					SET @ErrorMessage=ERROR_MESSAGE()  
					SET @ErrorNumber=ERROR_NUMBER()    
	 END CATCH;
END;







GO
