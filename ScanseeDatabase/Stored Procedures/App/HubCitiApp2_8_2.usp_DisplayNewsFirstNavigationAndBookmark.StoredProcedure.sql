USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_DisplayNewsFirstNavigationAndBookmark]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_DisplayNewsFirstNavigationAndBookmark]
Purpose					: To display and insert list of News categories on a BookmarkBar.
Example					: [usp_DisplayNewsFirstNavigationAndBookmark]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			26 May 2016	     Bindu T A			[usp_DisplayNewsFirstNavigationAndBookmark]
----------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_DisplayNewsFirstNavigationAndBookmark]
(   
	--Input variable
	  @HubcitiID INT 
	 ,@HcuserID INT
		--Output Variable 
	 ,@BookMark Varchar(10) output
	 ,@Section Varchar(10) output
     ,@Status int output
	 ,@ErrorNumber int output
	 ,@ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

	update a set NewsSideNavigationID=b.HcMenuItemID  from HcUserNewsFirstSideNavigation a  inner join HcMenuItem b on b.MenuItemName=a.NewsSideNavigationDisplayValue
inner join  HcMenu  on HcMenu.HcMenuID=b.HcMenuID
where a.Flag=1 and a.NewsSideNavigationID<>b.HcMenuItemID
and   a.HchubcitiID=@HubcitiID and HcMenu.HchubcitiID=@HubcitiID
		
		--------------------------------BookMark List-----------------------------------

		IF EXISTS (SELECT 1 FROM HcUserNewsFirstBookmarkedCategories WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID )
		BEGIN 
			SELECT distinct C.NewscategoryID parCatId, NewscategoryDisplayValue parCatName,Isbookmark isAdded,C.Sortorder sortOrder, S.NewsCategoryColor catColor
			FROM HcUserNewsFirstBookmarkedCategories C
			inner join HcNewsSettings S ON s.NewsCategoryID = C.HcTemplateID 
			WHERE HcUserID= @HcuserID AND C.HcHubcitiId=@HubcitiID
			
			SELECT @BookMark = 'BookMark' ,@Section= 'Section'
	
		 END
		ELSE 
		 BEGIN
			SELECT distinct C.NewscategoryID parCatId, NewscategoryDisplayValue parCatName,1 isAdded,C.Sortorder sortOrder, categorycolor catColor
			FROM HcDefaultNewsFirstBookmarkedCategories C
			--inner join HcNewsSettings S ON s.NewsCategoryID = C.HcTemplateID 
			ORDER BY SortOrder
			
			SELECT @Bookmark = 'BookMark' ,@Section= NULL
		 END

		 ------------------------------------------------------------------------------
		 --------------------------------Side Navigation List--------------------------

		 DECLARE @MenuID INT 
		 SELECT @MenuID = (SELECT HCMenuID FROM HcMenu WHERE CreatedUserID= @HcuserID AND HcHubcitiId=@HubcitiID AND Level=1)

		 IF EXISTS (SELECT 1 FROM HcUserNewsFirstSideNavigation WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID )
		BEGIN 

			SELECT distinct NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,Flag isAdded,C.Sortorder sortOrder, S.NewsCategoryColor catColor
			FROM HcUserNewsFirstSideNavigation C
			inner join NewsFirstCategory T ON T.NewsCategoryDisplayValue = C.NewsSideNavigationDisplayValue
			inner join HcNewsSettings S ON s.NewsCategoryID = T.NewsCategoryID 
			WHERE HcUserID= @HcuserID AND C.HcHubcitiId=@HubcitiID
			ORDER BY SortOrder
			
			SELECT @BookMark = 'BookMark' ,@Section= 'Section'
	
		 END
		ELSE 
		 BEGIN

			SELECT distinct HcMenuItemID parCatId, MenuItemName parCatName,1 isAdded,Position sortOrder,NULL catColor
			FROM HcMenuItem 
			WHERE HcMenuID = @MenuID AND CreatedUserID= @HcuserID --AND HcHubcitiId=@HubcitiID
			ORDER BY Position
			
			SELECT @Bookmark = 'BookMark' ,@Section= NULL
		 END

		 ----------------------------------------------------------------------------------

		--Confirmation of Success
		SELECT @Status = 0	      	
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_DisplayNewsFirstNavigationAndBookmark].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

















GO
