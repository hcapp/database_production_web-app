USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcFetchTutorialImages]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFetchTutorialImages
Purpose					: To fetch Tutorial Media Information.
Example					: usp_HcFetchTutorialImages

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23rd April 2013	SPAN		Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcFetchTutorialImages]
--(
	--@WelcomeVideo VARCHAR(1000) OUTPUT
--)

AS
BEGIN

	BEGIN TRY
	
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='App Media Server Configuration'
		AND Active = 1
		
		--SELECT @WelcomeVideo = ScreenContent
		--FROM AppConfiguration
		--WHERE ConfigurationType = 'Welcome Video'
		--AND Active = 1
		
		SELECT HcTutorialMediaID tutorialID 
		     , TutorialMediaName tutorialName			
			 , @Config+SlideImagePath TutorialImagePath				 		 
		FROM HcTutorialMedia
		ORDER BY TutorialMediaName	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcFetchTutorialImages.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			
		END;
		 
	END CATCH;
END;














































GO
