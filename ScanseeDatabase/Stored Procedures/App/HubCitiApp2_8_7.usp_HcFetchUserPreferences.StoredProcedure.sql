USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcFetchUserPreferences]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFetchUserPreferences
Purpose					: To list the Distances
Example					: usp_HcFetchUserPreferences 
History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.4		4th Dec 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcFetchUserPreferences]
(
	--Input Parameter(s)--	  
	  @UserID INT
	, @DeviceID VARCHAR(500) 
	, @HubCitiID INT
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			
			--To get the LocaleRadius for user.
			--DECLARE @DeviceID1 VARCHAR(100) 
						
			--SELECT @DeviceID1=DeviceID
			--FROM HcUserDeviceAppVersion 
			--WHERE HcUserID =@UserID
			--AND PrimaryDevice = 1

			DECLARE @HubCitiName varchar(500)

			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
			
			IF @HubCitiName = 'Tyler'
			BEGIN
				SELECT DISTINCT UP.LocaleRadius
					  ,ISNULL(UP.EnablePushNotification, 0)PushNotify
					  ,UD.HcHubCitiID hubCitiId
					  ,UD.DeviceID
					  ,UP.LocationService AS bLocService
				FROM HcUserPreference UP
				LEFT JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = UP.HcUserID 
				LEFT JOIN HcUserToken UT ON UT.DeviceID = UD.DeviceID  
				WHERE UP.HcUserID = @UserID AND UD.DeviceID = @DeviceID --AND UD.HcHubCitiID = @HubCitiID
							
			END
			ELSE
			BEGIN
				SELECT DISTINCT UP.LocaleRadius
					  ,ISNULL(UP.EnablePushNotification, 0)PushNotify
					  ,UT.HcHubCitiID hubCitiId
					  ,UP.LocationService AS bLocService
				FROM HcUserPreference UP
				LEFT JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = UP.HcUserID 
				LEFT JOIN HcUserToken UT ON UT.DeviceID = UD.DeviceID  
				WHERE UP.HcUserID = @UserID AND UD.DeviceID = @DeviceID --AND UT.HcHubCitiID = @HubCitiID
			END
	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcFetchUserPreferences.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;






GO
