USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcSubMenuPreferredCityList_06232016]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcSubMenuPreferredCityList]
Purpose					: To display list of Cities which has SubMenuItems for given HubCitiID(RegionApp).
Example					: [usp_HcSubMenuPreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  10/27/2014       SPAN              1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcSubMenuPreferredCityList_06232016]
(
   
    --Input variable	  
      @LinkID Int
    , @HcHubCitiID Varchar(100)
    , @LevelID Int  
    , @UserID Int
    , @TypeID int
    , @DepartmentID int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			

			DECLARE @GuestUser int
			DECLARE @CityPrefNotVisitedUser bit

			SELECT @GuestUser = U.HcUserID
			FROM HcUser U
			INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
			WHERE UserName = 'GuestLogin'
			AND DA.HcHubCitiID = @HcHubCitiID

			
			IF NOT EXISTS (SELECT HcUserID FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HcHubCitiID AND HcUserID = @UserID)
			BEGIN
				SET @CityPrefNotVisitedUser = 1
			END
			ELSE
			BEGIN
				SET @CityPrefNotVisitedUser = 0
			END


			IF @DepartmentID =0
			BEGIN

				SET @DepartmentID = NULL
			END

			IF @TypeID =0
			BEGIN

				SET @TypeID = NULL
			END

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc) 
			      ,H.HcMenuItemID				  
			INTO #group1
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE HcHubCitiID = @HcHubCitiID AND (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID 	
				  , MenuItemName			 
			INTO #group2
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where HcHubCitiID =@HcHubCitiID AND (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group3
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where HcHubCitiID = @HcHubCitiID  AND (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group4
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where HcHubCitiID = @HcHubCitiID AND (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 

            --Select Menuitems based on the Department and Type values
			SELECT H.HcMenuItemID 
				  , H.MenuItemName
			INTO #menuitem
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID
			WHERE ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND H.HcMenuItemTypeID =@TypeID )) 
						AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND H.HcDepartmentID  =@DepartmentID))))
						AND HcHubCitiID =@HcHubCitiID 
			ORDER BY H.HcMenuItemID
		

			DECLARE @maxGroup Int
			DECLARE @minGroup Int

			--Select Maximum menuitemid in the selected group template
			SELECT @maxGroup = MAX(HcMenuItemID) From #group1
			SELECT @minGroup = Min(HcMenuItemID) From #group1

			--Here based on the temporary table data, selecting menuitemid under a group and storing in temp table
			select DISTINCT A.HcMenuItemID     
			INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )	--here check the menuitem is belong to first group			
			OR
			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID) 
			
			OR 
			(@maxGroup <M.HcMenuItemID 
			AND @maxGroup =A.HcMenuItemID)
						

			--To get a list of City names
			SELECT DISTINCT C.HcCityID As CityID
						  , CityName
						  , isCityChecked = CASE WHEN (@GuestUser = @UserID) OR (@CityPrefNotVisitedUser = 1) THEN 1
												 WHEN UC.HcUsersPreferredCityAssociationID IS NOT NULL THEN 1
												 ELSE 0 END
			FROM HcMenuItem MI
			INNER JOIN HcRegionAppMenuItemCityAssociation RMC ON MI.HcMenuItemID = RMC.HcMenuItemID AND RMC.HCHubcitiID = @HcHubCitiID
			LEFT JOIN HcUsersPreferredCityAssociation UC ON ((RMC.HcCityID = UC.HcCityID OR UC.HcCityID IS NULL) AND UC.HcHubcitiID = @HcHubCitiID AND UC.HcUserID = @UserID) OR (@GuestUser = @UserID) OR (@CityPrefNotVisitedUser = 1)
			INNER JOIN HcCity C ON RMC.HcCityID = C.HcCityID		
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			LEFT JOIN HCDepartments D ON D.HcDepartmentID = MI.HcDepartmentID
			LEFT JOIN HcMenuItemType T ON T.HcMenuItemTypeID = MI.HcMenuItemTypeID
			LEFT JOIN #Groups G ON G.HcMenuItemID =MI.HcMenuItemID  
			WHERE RMC.HCHubcitiID = @HcHubCitiID AND (MI.HcMenuID = ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 )) 
			AND ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND MI.HcMenuItemTypeID =@TypeID )) 
			AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND MI.HcDepartmentID  =@DepartmentID))) OR G.HcMenuItemID =MI.HcMenuItemID)
			ORDER BY CityName ASC


			--Confirmation of Success.
			SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcSubMenuPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;































GO
