USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_WebHcIOSAndroidDealPushNotifyCertificateDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHcIOSAndroidDetails
Purpose					: To fetch IOS and Android details.
Example					: usp_WebHcIOSAndroidDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
			29-Feb-2016		Sagar Byali
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_WebHcIOSAndroidDealPushNotifyCertificateDetails]
(	
	  @HcHubCitiID INT 

	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		
		DECLARE @HubCitiName VARCHAR(100)
		SELECT @HubCitiName = Hubcitiname from HcHubCiti WHERE HcHubCitiID = @HcHubCitiID	

		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Host'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Port'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Certificate Path'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='APNS Certificate Password'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='GCM URL'
		UNION ALL
		SELECT ConfigurationType,ScreenContent FROM AppConfiguration WHERE ConfigurationType='GCM API KEY' AND ScreenName=@HubCitiName
		
			
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHcIOSAndroidDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;































GO
