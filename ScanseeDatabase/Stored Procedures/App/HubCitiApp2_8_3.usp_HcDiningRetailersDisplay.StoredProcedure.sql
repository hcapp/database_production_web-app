USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcDiningRetailersDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcDiningRetailersDisplay]
Purpose					: To display dining Retailers list.
Example					: [usp_HcDiningRetailersDisplay]

History
Version		  Date		      Author	 Change Description
--------------------------------------------------------------- 
1.0		   20th Nov 2013	   SPAN	           1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcDiningRetailersDisplay]
(
	--Input Variables
	  @UserID int 
    , @CuisineTypeID int
	, @HubCitiID int   
	, @SearchParameter varchar(255)
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)    
	, @PostalCode varchar(10)
	, @Radius int  
	, @LowerLimit int  
	, @ScreenName varchar(50) 
	, @SortColumn varchar(50)
	, @SortOrder varchar(10)
	
	--User Tracking Variables
	, @MainMenuID int
	
	--Output Variables
	, @MaxCnt int  output
	, @NxtPageFlag bit output  
	, @Status int output
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
  
)
AS
BEGIN

	BEGIN TRY
				
				DECLARE @Config varchar(50)
				DECLARE @RetailConfig varchar(50)
				
				SELECT @Config = ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType = 'App Media Server Configuration'
				
				SELECT @RetailConfig = ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
				
				--To get the Row count for Pagination.  
				DECLARE @UpperLimit int   
				SELECT @UpperLimit = @LowerLimit + ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination' AND Active = 1
				
				--To fetch all the duplicate retailers.
				SELECT DISTINCT DuplicateRetailerID 
				INTO #DuplicateRet
				FROM Retailer 
				WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive = 1

				--If the radius is not received then consider the global radius.
				IF @Radius IS NULL
				BEGIN
					SELECT @Radius = ScreenContent
					FROM AppConfiguration
					WHERE ScreenName = 'DefaultRadius'
				END

				--While User search by PostalCode, co-ordinates are fetched from GeoPosition table.    
				--Derive the Latitude and Longitude in the absence of the input.
				IF @Latitude IS NULL AND @Longitude IS NULL
				BEGIN
					--If the postal code is passed then derive the co ordinates.
					IF @PostalCode IS NOT NULL
					BEGIN
						SELECT @Latitude = Latitude
							 , @Longitude = Longitude
						FROM GeoPosition 
						WHERE PostalCode = @PostalCode
					END
					ELSE
					--If the user has not passed the co ordinates & postal code then cosidered the user configured postal code.
					BEGIN
						SELECT @Latitude = G.Latitude
							 , @Longitude = G.Longitude
						FROM GeoPosition G
						INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
						WHERE U.HcUserID = @UserID
					END
				END  
				
				--To get Retailers Details for given HubCiti and cuisin type 
				SELECT Row_Num = IDENTITY(INT,1,1)
							, RetailID    
							, RetailName
							, RetailLocationID
							, Address1
							, Address2
							, Address3
							, Address4
							, City    
							, [State]
							, PostalCode
							, RetailerImagePath    
							, Distance 								
				INTO #Retail
				FROM  
			    (SELECT DISTINCT R.RetailID 
						, R.RetailName 
						, RL.RetailLocationID 
						, RL.Address1
						, RL.Address2
						, RL.Address3
						, RL.Address4
						, RL.City
						, RL.[State]
						, RL.PostalCode
						, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)
						, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
				FROM HcBusinessSubCategory BSC
				INNER JOIN HcRetailerSubCategory RSC ON BSC.HcBusinessSubCategoryID = RSC.HcBusinessSubCategoryID
				INNER JOIN RetailLocation RL ON RSC.RetailLocationID = RL.RetailLocationID 
				INNER JOIN HcLocationAssociation LA ON RL.PostalCode = LA.PostalCode
				INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
				INNER JOIN Retailer R ON RL.RetailID = R.RetailID AND R.RetailerActive = 1
				LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
				LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
				WHERE LA.HcHubCitiID = @HubCitiID AND D.DuplicateRetailerID IS NULL
				AND RL.Active = 1 
				AND ((@SearchParameter IS NOT NULL AND R.RetailName LIKE '%'+@SearchParameter+'%')
					OR 
					(@SearchParameter IS NULL AND 1 = 1))
				AND (
					(@CuisineTypeID = 0 AND 1=1)
					OR (@CuisineTypeID <> 0 AND BSC.HcBusinessSubCategoryID = @CuisineTypeID) 
					)) Retail
				ORDER BY CASE WHEN ISNULL(@SortOrder, '0') = '0'
							 THEN Distance 
						 END ASC, 
						 CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'RetailName' THEN RetailName  
						      WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(Distance AS SQL_VARIANT)
						 END ASC,
						  CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailName' THEN RetailName  
						      WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(Distance AS SQL_VARIANT) 
						 END DESC
				
				--To capture Max Row Number.  
				SELECT @MaxCnt = MAX(Row_Num) FROM #Retail

				--This flag is a indicator to enable "More" button in the UI.   
				--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
				
				SELECT  Row_Num rowNumber
						, RetailID retailerId
						, RetailName retailerName
						, R.RetailLocationID retailLocationID
						, Address1 retaileraddress1
						, Address2 retaileraddress2
						, Address3 retaileraddress3
						, Address4 retaileraddress4
						, City City
						, [State] State
						, PostalCode PostalCode 
						, RetailerImagePath logoImagePath
						, Distance
						, S.BannerAdImagePath bannerAdImagePath    
						, B.RibbonAdImagePath ribbonAdImagePath    
						, B.RibbonAdURL ribbonAdURL    
						, B.RetailLocationAdvertisementID advertisementID  
						, S.SplashAdID splashAdID
				INTO #RetailerList
				FROM #Retail R
				LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL  
														   THEN CASE WHEN ASP.WebsiteSourceFlag = 1 
																	 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath
															    ELSE @Config+SplashAdImagePath END  
													  ELSE SplashAdImagePath END
								   , SplashAdID  = ASP.AdvertisementSplashID
								   , R.RetailLocationID 
						   FROM #Retail R
						   INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
						   INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
						   AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)) S
				ON R.RetailLocationID = S.RetailLocationID
			    LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL 
															THEN CASE WHEN AB.WebsiteSourceFlag = 1 
																		THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath 
																 ELSE @Config+AB.BannerAdImagePath END  
													  ELSE AB.BannerAdImagePath END
								  , RibbonAdURL = AB.BannerAdURL
								  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
								  , R.RetailLocationID 
						   FROM #Retail R
						   INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
						   INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
						   WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)) B
				ON R.RetailLocationID = B.RetailLocationID
				WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit
					 
				--User Tracking Session					 

				--Table to track the Retailer List.
				CREATE TABLE #Temp(RetailerListID int
									,MainMenuID int
									,RetailID int
									,RetailLocationID int)                      

				--Capture the impressions of the Retailer list.
				INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
																	, RetailID
																	, RetailLocationID
																	, DateCreated)
				OUTPUT inserted.RetailerListID,inserted.MainMenuID,inserted.RetailID, inserted.RetailLocationID INTO #Temp(RetailerListID,MainMenuID,RetailID, RetailLocationID)							
																SELECT @MainMenuId
																	 , retailerId
																	 , retailLocationID
																	 , GETDATE()
																FROM #RetailerList
				
				--Display the Retailer along with the keys generated in the Tracking table.
				SELECT rowNumber
						, T.RetailerListID retListID
						, retailerId
						, retailerName
						, T.RetailLocationID
						, retaileraddress1
						, retaileraddress2
						, retaileraddress3
						, retaileraddress4
						, City
						, State
						, PostalCode
						, logoImagePath
						, Distance
						, bannerAdImagePath
						, ribbonAdImagePath
						, ribbonAdURL
						, advertisementID
						, splashAdID
				FROM #Temp T
				INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID
		
				--Confirmation of Success
				SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcDiningRetailersDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
















































GO
