USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcBandEventsDisplay_backup]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name					 : [HubCitiApp2_8_3].[usp_HcBandEventsDisplay]
Purpose                                  : To display List fo Events.
Example                                  : [HubCitiApp2_8_3].[usp_HcBandEventsDisplay]

History
Version              Date                 Author               Change Description
------------------------------------------------------------------------------------ 
1.0               04/21/2016			Bindu T A				Initial Version - usp_HcBandEventsDisplay
2.0				  07/14/2016			Bindu T A				Changes W.R.T NearBy, Ongoing and Upcoming Band Events
------------------------------------------------------------------------------------
*/



--exec [HubCitiApp2_8_3].[usp_HcBandEventsDisplay] 2162,10533,null,null,null,null,null,null,0,'clr screen',null,null,null,null,null,null,null,null,null,3 ,null,null,null,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcBandEventsDisplay_backup]
(   
    --Input variable.        
         @HubCitiID int
       , @UserID Int
       , @CategoryID VARCHAR(1000)
       , @Latitude Float
       , @Longitude Float
	   , @Radius INT
       , @Postalcode Varchar(200)
       , @SearchParameter Varchar(1000)
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @SortColumn Varchar(200)
       , @SortOrder Varchar(100)
       , @GroupColumn varchar(50)
       , @HcMenuItemID int
       , @HcBottomButtonID int
       , @RetailID int
       , @RetailLocationID int
	   , @FundRaisingID Int
	   , @EventDate Date
	   , @BandEventTypeID INT 

       --User Tracking
       , @MainMenuID Int
  
       --Output Variable 
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
					--PRINT 'A'
               
                     DECLARE @UpperLimit int
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     DECLARE @RetailConfig Varchar(1000)
					-- DECLARE @BandUserID Int
					 DECLARE @BandCategoryID VARCHAR(1000)
					 DECLARE @BandID INT

					 SET @BandID = @RetailID
					 --SET @BandUserID = @UserID
					 SET @BandCategoryID = @CategoryID

					 --Changes for Band 7/14/16

					 SET @BandEventTypeID = NULL

					 --DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchParameter)))

					 --SET @SearchParameter = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchParameter,1, @Length-1)
						--					   WHEN (@Length = 5) THEN SUBSTRING(@SearchParameter,1, @Length-2)
						--					   WHEN (@Length >= 6) THEN SUBSTRING(@SearchParameter,1, @Length-3) 
						--					   ELSE @SearchParameter END)
											   
					 IF (@Radius IS NULL)
					 BEGIN
					 SELECT @Radius = ISNULL(LocaleRadius, 5)   
					 FROM UserPreference   
					 WHERE UserID = @UserID
					 END

                     SELECT @RetailConfig = 
					 ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Band Media Server Configuration'   

					-- SELECT @RetailConfig= 'http://10.10.221.209:8080/Images/retailer/'

					  IF @Latitude IS NULL AND @Postalcode IS NOT NULL
					 BEGIN
						SELECT @Latitude =Latitude 
						      ,@Longitude =Longitude
						FROM GeoPosition
						WHERE PostalCode =@Postalcode 
						
					 END

                     SET @UserLatitude = @Latitude
                     SET @UserLongitude = @Longitude                  

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                  , @UserLongitude = Longitude
                           FROM BandUsers A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE BandUserID = @UserID 
                     END
                     --Pick the co ordinates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
					
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HubCitiID

                     END

					  -- select @UserLatitude,@UserLongitude

                     --print @UserLatitude
                     --print @UserLongitude
					 		--	DECLARE @WeeklyDays Varchar(2000)
				--	 SELECT @WeeklyDays=COALESCE(@WeeklyDays+',', '')+CAST([DayName] AS VARCHAR)
		--	FROM HcEventInterval
			--INNER JOIN HcEventInterval E ON H.HcEventID = E.HcEventID
			--WHERE HcEventID = @H

					 SELECT ID, Day
			INTO #DayLookup
			FROM 			
			(
				SELECT 1 AS 'ID', 'Sunday' 'Day'
				UNION
				SELECT 2, 'Monday'
				UNION
				SELECT 3, 'Tuesday'
				UNION
				SELECT 4, 'Wednesday'
				UNION
				SELECT 5, 'Thursday'
				UNION
				SELECT 6, 'Friday'
				UNION
				SELECT 7, 'Saturday')A
					                           
                     --To get the row count for pagination.   
                     SELECT @UpperLimit = @LowerLimit + 
					 ScreenContent  
                     FROM AppConfiguration   
                     WHERE ScreenName = 'clr screen' 
                     AND ConfigurationType = 'Pagination'
                     AND Active = 1

                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)
					-- select @DefaultPostalCode,@Postalcode

                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @Postalcode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN Users U ON G.PostalCode = U.PostalCode
                                  WHERE U.UserID = @UserID
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @Postalcode
                           END
                     END

                     DECLARE @Config VARCHAR(500)
					 SELECT @Config = 
					 ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'


					 SELECT DISTINCT  HE.HcBandEventID
							,HcBandEventName
							,ShortDescription
							,LongDescription
							,ImagePath
						
							,HcHubCitiID
							,StartDate
							,EndDate
							,HE.DateCreated
							,HE.DateModified
							,HE.CreatedUserID
							,HE.ModifiedUserID
							,MoreInformationURL
							,HE.HcEventRecurrencePatternID
							--,RecurrenceInterval
							,EventFrequency
							,ISNULL(OnGoingEvent,0)OnGoingEvent
							,B.BandID
							,B.BandName
							,Active
							 ,recurringDays = CASE WHEN R.RecurrencePattern = 'Daily'
												 THEN CASE WHEN EI.HcBandEventIntervalID IS NULL 
																		THEN 'Occurs Every ' +IIF(HE.RecurrenceInterval = 1, '',  CAST(HE.RecurrenceInterval AS VARCHAR(100))+' ') + 'Day(s)' 	
																	  ELSE 'Occurs Every Weekday' 
													  END
												WHEN R.RecurrencePattern = 'Weekly'
													THEN 'Occurs Every '+IIF(RecurrenceInterval = 1, '', CAST(RecurrenceInterval AS VARCHAR(10))+' ') + 'Week(s) on ' + STUFF((SELECT ', ' + [DayName]
																																												FROM HcBANDEventInterval F			
																																												LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																																												WHERE F.HcBandEventID = HE.HcBandEventID
																																												ORDER BY L.ID
																																												FOR XML PATH('')), 1, 2, '')
												WHEN R.RecurrencePattern = 'Monthly'
													THEN CASE WHEN [DayName] IS NULL THEN 'Day '+ CAST(DayNumber AS VARCHAR(10)) + ' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
															  ELSE 'The ' + CASE WHEN DayNumber = 1 THEN 'First '
																				 WHEN DayNumber = 2 THEN 'Second '
																				 WHEN DayNumber = 3 THEN 'Third '
																				 WHEN DayNumber = 4 THEN 'Fourth '
																				 WHEN DayNumber = 5 THEN 'Last ' END													  
															   +  STUFF((SELECT ', ' + [DayName]
																		 FROM HcBANDEventInterval F			
																		 LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																		 WHERE F.HcBandEventID = HE.HcBandEventID
																		 ORDER BY L.ID
																		 FOR XML PATH('')), 1, 2, '') +' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
														 END
										  END	 ,HE.HcEventRecurrencePatternID recurrencePatternID
									 ,R.RecurrencePattern recurrencePatternName	
									 ,[isWeekDay] = CASE WHEN R.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
									-- ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
									 ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
									 ,ByDayNumber = CASE WHEN R.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
									 ,DayNumber
									 ,EventListingImagePath 
					INTO #BandEvents
					FROM hcBandEvents HE
					inner JOIN Band B ON HE.BandID = B.BandID AND B.BandActive = 1
					LEFT JOIN HcBANDEventInterval EI ON He.HcBandEventID = EI.HcBandEventID
					LEFT JOIN HcEventRecurrencePattern R ON R.HcEventRecurrencePatternID = HE.HcEventRecurrencePatternID
					WHERE (HcHubCitiID =@HubCitiID OR HcHubCitiID IS NULL)
					AND GETDATE() < ISNULL(EndDate, GETDATE()+1) 
					AND Active = 1	


					select *  from #BandEvents 
					
					--SELECT *  FROM #BandEvents 
					  			
					SELECT Param CatID
					INTO #CategoryList
					FROM fn_SplitParam(@BandCategoryID,',')

					CREATE TABLE #Temp1(Bandid int,EventID Int
                                            ,EventName Varchar(2000) 
											,BandName Varchar(2000)
											,ShortDescription Varchar(2000) 
											,LongDescription Varchar(2000)                                                     
                                            ,HcHubCitiID Int
											,EventAddress Varchar(2000)
											,EventLatitude Float
											,EventLongitude Float
                                            ,ImagePath Varchar(2000) 
                                            ,MoreInformationURL Varchar(1000)                                                    
                                            ,StartDate Date
                                            ,EndDate Date
                                            ,StartTime VARCHAR(15)
                                            ,EndTime VARCHAR(15)                                   
                                            ,MenuItemExist Int
                                            ,EventCategoryID INT
                                            ,EventCategoryName Varchar(500)
										    ,Distance Int                                                                            
										    ,DistanceActual	Int									  
                                            ,OnGoingEvent Int
										    ,RetailLocationID Int
											,Latitude Float
											,Longitude Float
											,SignatureEvent bit
											,recurringDays VARCHAR(500)
											,listingImgPath VARCHAR(500)
											)

					
                      INSERT INTO #Temp1(Bandid,EventID 
												,EventName
												,BandName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,EventAddress
												,EventLatitude
												,EventLongitude
												,ImagePath 
												,MoreInformationURL                                               
												,StartDate
												,EndDate 
												,StartTime
												,EndTime                                
												,MenuItemExist
												,EventCategoryID
												,EventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,Latitude 
												,Longitude
												,SignatureEvent
												,recurringDays
												,listingImgPath)
							 SELECT bandid,EventID 
												,EventName
												,BandName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,EventAddress
												,EventLatitude
												,EventLongitude
												,ImagePath 
												,MoreInformationURL                                               
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,Latitude 
												,Longitude
												,SignatureEvent
												,recurringDays
												,listingImgPath
							FROM(

							SELECT DISTINCT  E.bandid, E.HcbandEventID AS EventID
													,E.hcBandEventName  AS EventName
													,E.BandName
													,ShortDescription ShortDescription
													,LongDescription   LongDescription                                                  
													,E.HcHubCitiID  HcHubCitiID
													,isnull(HL.Address,'')+','+ isnull(HL.city,'')+','+ isnull(HL.State,'')+','+ isnull(HL.PostalCode,'') EventAddress
													,isnull(HL.Latitude,RetailLocationLatitude) EventLatitude
													,isnull(HL.Longitude,RetailLocationLongitude) EventLongitude
													,ImagePath=IIF(E.BandID IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.BandID AS VARCHAR)+'/'+ ImagePath) 
													,E.MoreInformationURL                                                  
													,StartDate =CAST(StartDate AS DATE)
													,EndDate =CAST(EndDate AS DATE)
													,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(StartDate AS Time),100),'AM',' AM') ,'PM',' PM')StartTime
													,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(EndDate AS Time),100),'AM',' AM') ,'PM',' PM') EndTime
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Events' 
																					 AND MI.LinkID = E.HcBandEventID)>0 THEN 1 ELSE 0 END  
												   ,EC.HcBandEventCategoryID  AS HcEventCategoryID
												   ,EC.HcBandEventCategoryName AS HcEventCategoryName
												    ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214 END, 0))
								                    ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
												    						--     ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
								                  --  ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
												   ,E.OnGoingEvent  OnGoingEvent
												   ,NULL RetailLocationID 
												   ,Latitude = ISNULL(HL.Latitude,NULL)
												   ,Longitude = ISNULL(HL.Longitude,NULL)
												   ,SignatureEvent = CASE WHEN (EC.HcBandEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END
												   ,recurringDays
												   ,listingImgPath=IIF(E.BandID IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+EventListingImagePath),@RetailConfig+CAST(E.BandID AS VARCHAR)+'/'+ EventListingImagePath) 
							 FROM #BandEvents  E 
							 INNER JOIN HcBandEventsCategoryAssociation EA ON EA.HcBandEventID =E.hcBandEventID AND (E.HcHubCitiID =@HubCitiID OR E.HcHubCitiID IS NULL)
							 INNER JOIN HcBandEventsCategory EC ON EC.HcBandEventCategoryID =EA.HcBandEventCategoryID
							 --INNER JOIN HcMenuItemBandCategories MIC ON EC.HcBandEventCategoryID = MIC.HcEventCategoryID AND MIC.HcHubCitiID =@HubCitiID AND MIC.HcMenuItemID = @HcMenuItemID 
							 LEFT JOIN HcBandEventLocation HL ON HL.HcBandEventID =E.HcBandEventID 
							 LEFT JOIN HcBandEventAppsite A ON E.hcBandEventID = A.HcBandEventID AND A.HcHubCitiID = @HubCitiID AND A.HcBandEventID IS NULL
							 LEFT JOIN HcBandAppSite HA ON A.HcBandAppSiteID = HA.HcBandAppSiteID AND HA.HcHubCitiID = @HubCitiID 
							    LEFT JOIN Geoposition G ON (G.Postalcode=HL.Postalcode)                     
							 --LEFT JOIN HcEventPackage EP ON E.HcEventID =EP.HcEventID AND EP.HcHubCitiID = @HubCitiID                                      
							 --LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated = 1 AND (E.RetailID = RA.RetailID OR 1=1)
							 --LEFT JOIN #RLListss RRR ON RRR.RetailLocationID =RA.RetailLocationID 
							 --LEFT JOIN RetailLocation RL1 ON (RL1.RetailLocationID = RRR.RetailLocationID) AND RL1.Active = 1 --OR (RL1.RetailLocationID = HA.RetailLocationID) OR (RRR.RetailLocationID =RL1.RetailLocationID)                
							 LEFT JOIN HcBandEventsAssociation RE ON  E.HcBandEventID = RE.HcBandEventID --AND  RE.RetailLocationID = RA.RetailLocationID--AND  RE.HcEventID IS NULL							 
							 ----*****--
                              LEFT JOIN HcbandRetailerEventsAssociation L  ON L.hcBandEventID= E.HcBandEventID  
								LEFT join Retailer R On R.RetailID = L.retailid
								left  join RetailLocation RL on RL.RetailID=R.RetailID AND RL.RetailID=L.RetailID
								AND RL.RetailLocationID=L.RetailLocationID
					 ----*****--

							 WHERE ((EA.HcHubCitiID =@HubCitiID) OR (E.HcHubCitiID IS NULL)) --AND E.BandID = RA.BandID))
							 AND ((ISNULL(@CategoryID, '0') <> '0' 
							 AND EA.HcBandEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))
							-- AND (HL.PostalCode = @Postalcode) 

							 AND GETDATE() < ISNULL(EndDate, GETDATE()+1)
							  AND E.Active = 1							
							 GROUP BY E.hcbandEventID 
									  , hcbandEventName
									  , E.BandID
									  , BandName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  , isnull(HL.Address,'')+','+ isnull(HL.city,'')+','+ isnull(HL.State,'')+','+ isnull(HL.PostalCode,'')
									  , HL.Latitude
									  , HL.Longitude   
									  , ImagePath
									  , E.MoreInformationURL
									  , EventListingImagePath
									  , StartDate
									  , EndDate
									  , EC.HcBandEventCategoryID
									  , EC.HcBandEventCategoryName
									  , OnGoingEvent
									  , E.BandID
									  , RE.HcBandEventID
									  --, RL1.RetailLocationID
									  , hl.Latitude
									  , hl.Longitude,RetailLocationLatitude,RetailLocationLongitude,recurringDays
									  ) E

									 --SELECT * FROM #Temp1
					
                 CREATE TABLE #Events(RowNum INT IDENTITY(1, 1)
                                    ,HcEventID Int
                                    ,HcEventName  Varchar(2000)
									,BandID INT
									,BandName VARCHAR(2000)
                                    ,ShortDescription Varchar(2000)
                                    ,LongDescription  Varchar(2000)                                                
                                    ,HcHubCitiID Int
									,EventAddress Varchar(2000)
									,EventLatitude Float
									,EventLongitude Float
                                    ,ImagePath Varchar(2000) 
									,MoreInformationURL Varchar(2000)                                             
                                    ,StartDate date
                                    ,EndDate date
                                    ,StartTime VARCHAR(15)
                                    ,EndTime VARCHAR(15)                                        
                                    ,MenuItemExist bit
                                    ,HcEventCategoryID INT
                                    ,HcEventCategoryName Varchar(2000)
                                    ,Distance Float
                                    ,DistanceActual float
                                    ,OnGoingEvent bit
									,SignatureEvent bit
									,recurringDays Varchar(500)
									,listingImgPath VARCHAR(2000))

					if (@BandID is not null and  @BandEventTypeID is  null)			
					begin
					
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName  
										,BandID
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath        
										,MoreInformationURL                                         
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent
										,recurringDays
										,listingImgPath)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,BandID
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath     
										   ,MoreInformationURL                                                
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   , listingImgPath
								 FROM #Temp1        
								 WHERE  bandid=@BandID 
								 GROUP BY EventID 
										 ,EventName  
										 ,BandID
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath   
										 ,MoreInformationURL                                                  
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays,listingImgPath
								 --ORDER BY --SignatureEvent DESC, OnGoingEvent DESC, 
									--CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
									--	ELSE CAST(StartDate AS sql_variant)
									----CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									----	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									----	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									--END ASC,Distance  

									end 

					/*if (@BandID is  null and @BandEventTypeID is not null)         

					BEGIN

					if @BandEventTypeID=2---Near by EVENT
					
					  begin

                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
								 FROM #Temp1  where isnull(Distance,DistanceActual) <=5
								 --isnull(Distance,DistanceActual) <= ISNULL(@Radius, 5) 
								 AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>=CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101))
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									end

									if @BandEventTypeID=1---ON GOING  EVENT
					
					  begin

					 
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
								 FROM #Temp1 
								 WHERE OnGoingEvent=1  AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>=CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101))
								 --CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)) between
								  -- CONVERT(DATETIME,CONVERT(VARCHAR,StartDate,101))
						-- AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,StartDate),101))
						-- AND isnull(Distance,DistanceActual) <= ISNULL(@Radius, 5) 
							 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,listingImgPath
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									END


									if @BandEventTypeID=3---Up coming EVENT
					
					  begin

			

                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
								 FROM #Temp1
								 WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>=CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101))
								 -- WHERE ( CONVERT(DATETIME,CONVERT(VARCHAR,StartDate,101))>CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)) 
								-- and  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)))
								 -- AND isnull(Distance,DistanceActual) <= ISNULL(@Radius, 5) 
							GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,listingImgPath
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									enD

									enD*/

					if (@BandID is null AND @BandEventTypeID is null AND ISNULL(@SearchParameter,'')= '')   
					begin
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                   
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                   
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
								 FROM #Temp1        
								 WHERE  Distance <= ISNULL(@Radius, 50) 
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                               
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,listingImgPath
								 --ORDER BY --SignatureEvent DESC, OnGoingEvent DESC, 
									--CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
									--ELSE CAST(StartDate AS sql_variant)
									----CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									----	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									----	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									--END ASC,Distance  

					end

					if (@BandEventTypeID is  null  and @BandID is null AND ISNULL(@SearchParameter,'') <> '')   -- Search Button
					BEGIN
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName  
										,BandID
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath         
										,MoreInformationURL                                          
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath)
								 SELECT   EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                    
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
								 FROM #Temp1  
								 WHERE  Distance <= ISNULL(@Radius, 50) 
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                             
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,listingImgPath
					--			 ORDER BY --SignatureEvent DESC, OnGoingEvent DESC, 
					--				CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
					--					WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(DistanceActual AS sql_variant)
					--					WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
					--					WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
					--					ELSE CAST(StartDate AS sql_variant)
					--				--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
					--				--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
					--				--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
					--				END ASC,Distance  
					end

					--SELECT * FROM #Events
											
                   
                     ---User Tracking

           
                  --   CREATE TABLE #Temp2(EventsListID INT, HcEventID Int)
                     
                  --   INSERT INTO HubCitiReportingDatabase..EventsList(MainMenuID
                  --                                                   ,HcEventID 
                  --                                                   ,EventClick 
                  --                                                   ,DateCreated)

                  --   OUTPUT Inserted.EventsListID,inserted.HCEventID INTO #Temp2(EventsListID ,HcEventID)

                  --   SELECT @MainMenuID
                  --         ,HcEventID
                  --         ,0
                  --         ,GETDATE()
                  --FROM #Events  
				  
                     
                  --   DECLARE @Seed INT
                  --   SELECT @Seed = ISNULL(MIN(EventsListID), 0)
                  --   FROM #Temp2                
                     
                     SELECT DISTINCT RowNum =1
                              ,HcEventID  
                              ,HcEventName  
							  ,BandID
							  ,BandName
                              ,ShortDescription 
                              ,LongDescription                                                  
                              ,HcHubCitiID 
							  ,EventAddress
							 ,EventLatitude
							 ,EventLongitude
                              ,ImagePath 
							  ,MoreInformationURL                                                 
                              ,StartDate 
                              ,EndDate 
                              ,StartTime 
                              ,EndTime                                       
                              ,MenuItemExist 
                              ,HcEventCategoryID 
                              ,HcEventCategoryName
                              ,Distance
                              ,DistanceActual
                              ,OnGoingEvent 						 
							  ,SignatureEvent,recurringDays
							  ,listingImgPath
                     INTO #Event
                     FROM #Events
					
					--SELECT * FROM #Event

					  SELECT DISTINCT RowNum =1
                              ,HcEventID  
                              ,HcEventName  
							  ,BandID
							  ,BandName
                              ,ShortDescription 
                              ,LongDescription                                                  
                              ,HcHubCitiID 
							  ,CASE WHEN L.HcBandRetailerEventsAssociationID is  null then EventAddress ELSE  isnull(RL.Address1,'')+','+ isnull(RL.city,'')+','+ isnull(RL.State,'')+','+ isnull(RL.PostalCode,'') END AS EventAddress
							 ,ISNULL(EventLatitude,RetailLocationLatitude) AS  EventLatitude
							 ,ISNULL(EventLongitude,RetailLocationLongitude) AS EventLongitude
                              ,ImagePath 
							  ,MoreInformationURL                                                 
                              ,StartDate 
                              ,EndDate 
                              ,E.StartTime 
                              ,E.EndTime                                       
                              ,MenuItemExist 
                              ,HcEventCategoryID 
                              ,HcEventCategoryName
                              ,Distance
                              ,DistanceActual
                              ,OnGoingEvent 						 
							  ,SignatureEvent
							  ,AppSiteFlag =  CASE WHEN L.HcBandRetailerEventsAssociationID is not null then 1 ELSE 0 END
							  ,L.RetailID as retailId
							  ,L.RetailLocationID retaillocationID
							  ,Retailname
							  ,E.recurringDays
							  ,E.listingImgPath
                     into #E
                     FROM #Events E 
					 LEFT JOIN HcbandRetailerEventsAssociation L  ON L.hcBandEventID= E.HcEventID 
					 LEFT join Retailer R On R.RetailID = L.retailid
					 left  join RetailLocation RL on RL.RetailID=R.RetailID AND RL.RetailID=L.RetailID
					 AND RL.RetailLocationID=L.RetailLocationID
					
					--SELECT * FROM #E

										
                     --DECLARE @SQL VARCHAR(1000) = 'ALTER TABLE #Event ADD EventsListID1 INT IDENTITY('+CAST(@Seed AS VARCHAR(100))+', 1)'
                     --EXEC (@SQL)

					  
					--SELECT @LowerLimit,@UpperLimit
                     

                     SELECT   rowNum 
                           --,T.EventsListID eventListId                                    
                           ,E.HcEventID eventId
                           ,E.HcEventName eventName
						   ,E.BandID 
						   ,E.BandName 
                           ,E.ShortDescription shortDes
							,E.LongDescription longDes                                                      
                             ,E.HcHubCitiID hubCitiId
							 ,E.EventAddress Address
							 ,E.EventLatitude Latitude
							 ,E.EventLongitude Longitude
                             ,E.ImagePath imgPath
							 ,E.MoreInformationURL moreInfoURL                                             
                             ,E.StartDate 
                             ,E.EndDate 
                             ,E.StartTime
                             ,E.EndTime                        
                            -- ,MenuItemExist mItemExist
                             ,E.HcEventCategoryID eventCatId
                             ,E.HcEventCategoryName eventCatName      
                             , ISNULL(DistanceActual,Distance)  Distance  
							 --,OnGoingEvent
							 --,SignatureEvent 
                            -- ,ISNULL(OnGoingEvent, 0) isOnGoing 
							  ,E.AppSiteFlag isAppSiteFlag 
							  ,E.RetailID retailId 
							  ,E.RetaillocationID retailLocationId 
							  ,E.Retailname  retailName  
							   , evtLocTitle  =  case when AppSiteFlag = 0 then EL.EventLocationKeyword ELSE E.retailName end 
							   ,recurringDays
							   ,listingImgPath
							   INTO #E1 
							   FROM #E E
							   LEFT JOIN HcBandEventLocation EL ON EL.Hcbandeventid = E.HcEventID
                     --INNER JOIN #Temp1 T ON T.EventsListID=E.EventsListID1
                     WHERE 
					((@SearchParameter is not null and ( (HcEventName LIKE '%'+@SearchParameter+'%' OR EventAddress LIKE '%'+@SearchParameter+'%'
					OR retailName LIKE '%'+@SearchParameter+'%'  or EL.EventLocationKeyword LIKE '%'+@SearchParameter+'%' )))
					or @SearchParameter is null)
					AND 
					(@EventDate IS NULL OR E.StartDate = @EventDate)
					
                   
					-- ORDER BY  case when isnull(@BandEventTypeID,0)  in(1,3) then convert(datetime,E.StartDate) else   RowNum end asc


					--SELECT * FROM #E1
					 

					 SELECT  rowNum 
                           --,T.EventsListID eventListId                                    
                           , eventId
                           , eventName
						   ,BandID 
						   ,E.BandName 
                           , shortDes
							, longDes                                                      
                             , hubCitiId
							 , Address
							 , Latitude
							 , Longitude
                             , imgPath
							 , moreInfoURL                                             
                             ,E.StartDate 
                             ,E.EndDate 
                             ,E.StartTime
                             ,E.EndTime                        
                            -- ,MenuItemExist mItemExist
                             ,eventCatId
                             , eventCatName      
                             , Distance 
							 --,OnGoingEvent
							 --,SignatureEvent 
                            -- ,ISNULL(OnGoingEvent, 0) isOnGoing 
							  ,isAppSiteFlag 
							  , retailId 
							  ,retailLocationId 
							  ,  retailName  
							   , evtLocTitle,recurringDays,listingImgPath
							   INTO #Final
							   FROM #E1 E
							  --  ORDER BY 
									--CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(Distance AS INT)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Venue' THEN evtLocTitle
									--	ELSE CAST(StartDate AS sql_variant)
									----CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									----	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									----	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									--END ASC,Distance  
 

	SELECT * INTO #Final1 FROM #Final 
	ORDER BY 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(Distance AS INT)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Venue' THEN ISNULL(evtLocTitle,'ZZ')
										ELSE CAST(StartDate AS sql_variant)
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									END ASC,Distance  
						
					--	SELECT * FROM #Final1
							  
							  SELECT  rowNum = Row_Number() over(ORDER BY 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(Distance AS INT)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Venue' THEN (CASE WHEN evtLocTitle = ' ' THEN 'ZZ' 
																										WHEN evtLocTitle IS NULL THEN 'ZZZZ'
																										ELSE evtLocTitle END) --ISNULL(evtLocTitle,'ZZ')
										ELSE CAST(StartDate AS sql_variant)
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									END ASC,Distance  )
                           --,T.EventsListID eventListId                                    
                           , eventId
                           , eventName
						   ,BandID 
						   ,E.BandName 
                           , shortDes
							, longDes                                                      
                             , hubCitiId
							 , Address
							 , Latitude
							 , Longitude
                             , imgPath
							 , moreInfoURL                                             
                             ,E.StartDate 
                             ,E.EndDate 
                             ,E.StartTime
                             ,E.EndTime                        
                            -- ,MenuItemExist mItemExist
                             ,eventCatId
                             , eventCatName      
                             , Distance  
							 --,OnGoingEvent
							 --,SignatureEvent 
                            -- ,ISNULL(OnGoingEvent, 0) isOnGoing 
							  ,isAppSiteFlag 
							  , retailId 
							  ,retailLocationId 
							  ,  retailName  
							   , evtLocTitle,recurringDays,listingImgPath
							   INTO #Result
							   FROM #Final E

							  
							 -- SELECT * FROM #Result

							  SELECT * FROM #Result
							   WHERE  RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit
							  -- ORDER BY 
									--CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(Distance AS INT)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandName 
									--	WHEN @SortOrder = 'ASC' AND @SortColumn = 'Venue' THEN evtLocTitle
									--	ELSE CAST(StartDate AS sql_variant)
									----CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									----	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									----	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									--END ASC,Distance  
						

							  -- SELECT @LowerLimit,@UpperLimit

					 SELECT @MaxCnt = COUNT(1) FROM #Result

					  --SELECT @MaxCnt

                     --To get list of BottomButtons for this Module
                     DECLARE @ModuleName Varchar(50) 
                     SET @ModuleName = 'Event'

					 

					   --To capture max row number.  
                     
                           

                     --this flag is a indicator to enable "More" button in the UI.   
                     --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                     SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

                  EXEC [HubCitiApp2_8_3].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,'Band Events',@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                


			--		 SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			--     , HM.HcHubCitiID 
			--	 , BB.HcBottomButtonID AS bottomBtnID
			--     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
			--	 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
			--	 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
			--	 , BottomButtonLinkTypeID AS btnLinkTypeID
			--	 --, btnLinkID = BottomButtonLinkID
			--	 --, btnLinkTypeName = (CASE  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
			--		--			                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
			--		--																																            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
			--		--																																            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
			--  --                     ELSE BottomButtonLinkTypeName END)
			--	 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
			--	, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			--                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			--                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			--                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			--                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			--                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			--							  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
			--					                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
			--																																		            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
			--																																		            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
			--                       ELSE BottomButtonLinkTypeName END)					
			--FROM HcMenu HM
			--INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
			--INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			--INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			--INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			--LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			--WHERE LT.LinkTypeDisplayName = 'Band Events' AND FBB.HcHubCitiID = @HubCitiID
			--ORDER by HcFunctionalityBottomButtonID
             
			 
			--Confirmation of Success
			SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_HcEventsDisplay.'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;









GO
