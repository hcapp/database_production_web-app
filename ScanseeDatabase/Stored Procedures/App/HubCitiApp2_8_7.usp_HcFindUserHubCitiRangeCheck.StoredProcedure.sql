USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcFindUserHubCitiRangeCheck]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcFindUserHubCitiRangeCheck]
(
	--Input Variable
	  @Radius int
	, @HubCitiID int
	, @Latitude Decimal(18,6)    
	, @Longitude Decimal(18,6)  
	, @PostalCode VARCHAR(5)
	, @UserConfiguredPostalCode Varchar(5)  
	--Output Variable
	, @UserOutOfRange bit output
	, @Distance float output
	, @DefaultPostalCode varchar(5) output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE   @NearestPostalCode VARCHAR(5)
					, @RandomPostalCode VARCHAR(5)
			
			SET	@UserOutOfRange = 1

			--Derive the Latitude and Longitude in the absence of the input.
			IF @Latitude IS NULL
			BEGIN
				--Derive Latitude and Longitude using Postalcode
					SELECT @Latitude=Latitude
						  ,@Longitude=Longitude
					FROM GeoPosition 
					WHERE PostalCode = ISNULL(@PostalCode,@UserConfiguredPostalCode)				
			END	

			--To find nearest PostalCode to user
			;WITH CTE AS (
							SELECT G.PostalCode
								  ,Distance = ROUND((ACOS((SIN(G.Latitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(G.Latitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (G.Longitude / 57.2958))))*6371) * 0.6214 ,1,1)
							FROM HcLocationAssociation LA
							INNER JOIN GeoPosition G ON LA.PostalCode = G.PostalCode AND LA.City = G.City AND LA.State = G.State
							WHERE LA.HcHubCitiID = @HubCitiID
						  
						  ) SELECT DISTINCT  @NearestPostalCode = PostalCode
										   , @Distance = Distance
						    FROM CTE
						    WHERE Distance = (SELECT MIN(Distance) FROM CTE)
			
			--To check whether user is within HubCiti or Not
			IF(ISNULL(@Distance, 0) >= (@Radius/2)) AND @Latitude IS NULL 
				SELECT @DefaultPostalCode = ISNULL(@NearestPostalCode,Defaultpostalcode)
				FROM HcHubCiti
				WHERE HcHubCitiID = @HubCitiID
			

			--Confirmation of Success.
		    SELECT @Status = 0
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			throw;
		END;
		 
	END CATCH;
END;















































GO
