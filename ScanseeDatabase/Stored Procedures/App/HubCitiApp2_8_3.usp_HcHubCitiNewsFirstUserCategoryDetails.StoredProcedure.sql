USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiNewsFirstUserCategoryDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcHubCitiNewsFirstUserCategoryDetails]
Purpose					: To disply the Category and Sub-Category Details for the given user and 
Example					: [usp_HcHubCitiNewsFirstUserCategoryDetails]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		06/21/2016	       Bindu T A				1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE[HubCitiApp2_8_3].[usp_HcHubCitiNewsFirstUserCategoryDetails]
(
	  @UserID INT
	, @CategoryName VARCHAR(50)

	--OUTPUT PARAMETER
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN
 BEGIN TRY
	
   DECLARE @CategoryID INT
   DECLARE @HchubcitiID INT

   SELECT DISTINCT @HchubcitiID = HchubcitiID 
   FROM HcUserDeviceAppVersion 
   WHERE HcUserID = @UserID AND HchubcitiID IS NOT NULL

   SELECT @CategoryID = 
   NewsCategoryID 
   FROM NewsFirstCategory 
   WHERE NewsCategoryName = @CategoryName AND Active = 1


   --select @CategoryID,@HchubcitiID,@CategoryName

   SELECT DISTINCT C.NewsCategoryID parCatID,
   C.NewsCategoryName parCatName,
   CSS.NewsSubCategoryID subCatId, --'1' subCatId,
   CS.NewsSubCategoryName subCatName,--'All' subCatName,
   S.NewsCategoryColor catColor,
   '1'  isSubCategory
   FROM NewsFirstSettings S
   INNER JOIN NewsFirstCategory C ON C.NewsCategoryID= S.NewsCategoryID
   INNER JOIN NewsFirstSubCategoryType CT ON CT.NewsCategoryID = C.NewsCategoryID
   INNER JOIN NewsFirstSubCategory CS ON CT.NewsSubCategoryTypeID = CS.NewsSubCategoryTypeID
   INNER JOIN NewsFirstSubCategorySettings CSS ON CSS.NewsCategoryID = S.NewsCategoryID AND CSS.NewsSubCategoryID = CS.NewsSubCategoryID AND CSS.NewsFirstSubCategoryURL <> ' '
   INNER JOIN NewsFirstCatSubCatAssociation HA ON HA.CategoryID = C.NewsCategoryID AND HA.SubCategoryID = CS.NewsSubCategoryID  AND HA.HcHubcitiID= @HchubcitiID
   INNER JOIN (select distinct subcategory,newstype From RssNewsFirstFeedNews where ltrim(rtrim(newstype)) = ltrim(rtrim(@CategoryName)) and title is not null and HcHubCitiID = @HchubcitiID and subcategory is not null) as  tt on ltrim(rtrim(tt.NewsType)) = ltrim(rtrim(C.NewsCategoryDisplayValue)) and tt.subcategory = cs.NewsSubCategoryName 
   WHERE C.NewsCategoryID = @CategoryID  AND S.HcHubCitiID= @HchubcitiID and c.Active = 1 AND Css.HcHubCitiID = @HchubcitiID
  


SELECT @Status = 0
 END TRY
		
 BEGIN CATCH
	 --Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			PRINT 'Error occured in Stored Procedure [usp_HcHubCitiNewsFirstUserCategoryDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END
		 
	END CATCH
END








GO
