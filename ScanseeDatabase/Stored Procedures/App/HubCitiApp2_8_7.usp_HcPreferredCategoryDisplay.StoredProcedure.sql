USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcPreferredCategoryDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcPreferredCategoryDisplay
Purpose					: To dispaly User'd preferred Categories.
Example					: usp_HcPreferredCategoryDisplay 1

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16th Sept 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcPreferredCategoryDisplay]
(
	@UserID int
	
	--OutPut Variable
	--, @NxtPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	
		SELECT  Row_Num=ROW_NUMBER() over(order by ParentCategoryName, subCategoryName )
			,C.CategoryID catId
		    ,C.ParentCategoryID  parCatId
			, C.ParentCategoryName  parCatName
			, C.SubCategoryID  subCatId
			, C.SubCategoryName  subCatName
			, isAdded = CASE WHEN UC.CategoryID IS NOT NULL THEN 1 ELSE 0 END
		FROM Category C
		LEFT JOIN HcUserCategory UC ON UC.CategoryID = C.CategoryID AND UC.HcUserID = @UserID 
		 	
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcPreferredCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;
















































GO
