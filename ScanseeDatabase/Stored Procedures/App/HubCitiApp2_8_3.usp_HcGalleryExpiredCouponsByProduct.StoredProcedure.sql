USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcGalleryExpiredCouponsByProduct]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GalleryExpiredCouponsByProduct]
Purpose					: To display Expired Coupons related to the user in user coupon gallery.
Example					: [usp_GalleryExpiredCouponsByProduct]

History
Version		Date			 Author	 Change Description
--------------------------------------------------------------- 
1.0			26th JUN 2013	 SPAN	 Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcGalleryExpiredCouponsByProduct]
(
	   @UserID int
	 , @SearchKey varchar(100)
	 , @LowerLimit int
     , @ScreenName varchar(100)
	
	--UserTracking Input Variables
	 , @MainMenuID int
	 
	--Output Variables
	 , @MaxCnt int output
	 , @NxtPageFlag bit output	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			--To get Media Server Configuration.  
			DECLARE @RetailConfig varchar(50)    
			SELECT @RetailConfig=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration'  
			   
			--To get the row count for pagination.
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName   
			AND ConfigurationType = 'Pagination'  
			AND Active = 1  
		
			CREATE TABLE #Gallery(Row_Num INT IDENTITY(1, 1)
									, UserCouponGalleryID  INT
									, CategoryID INT 
									, CategoryName VARCHAR(255)
									, CouponID INT
									, CouponName VARCHAR(500)
									, CouponDescription VARCHAR(1000)
									, CouponDiscountAmount MONEY
									, CouponDiscountPct FLOAT
									, CouponURL VARCHAR(500)
									, CouponImagePath VARCHAR(500)
									, CouponStartDate DATETIME
									, CouponExpireDate DATETIME
									, UsedFlag BIT 
									, ViewableOnWeb BIT
									)
				INSERT INTO #Gallery(UserCouponGalleryID 
									, CategoryID  
									, CategoryName
									, CouponID 
									, CouponName 
									, CouponDescription
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
									)
							SELECT	HcUserCouponGalleryID 
									, CategoryID  
									, CategoryName
									, CouponID 
									, CouponName 
									, CouponDescription
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
							FROM
							(SELECT DISTINCT HcUserCouponGalleryID 
									, PC.CategoryID  
									, CategoryName = CASE WHEN CAT.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END
									, C.CouponID 
									, C.CouponName 
									, CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp2_8_2.fn_CouponImage(C.CouponID)
												  ELSE CASE WHEN CouponImagePath IS NOT NULL
														THEN CASE WHEN C.WebsiteSourceFlag = 1 
																THEN @RetailConfig +CONVERT(VARCHAR(30),C.RetailID)+'/'+CouponImagePath 
															 ELSE CouponImagePath 
															 END
														END 
												  END
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
							FROM Coupon C 
							INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
							INNER JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
							LEFT JOIN ProductCategory PC ON CP.ProductID = PC.ProductID
							LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
							WHERE UsedFlag = 0 AND HcUserID = @UserID
							--AND CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
							AND ((ActualCouponExpirationDate IS NULL AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90)
												OR (ActualCouponExpirationDate IS NOT NULL AND ActualCouponExpirationDate < GETDATE()))
							AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
							UNION
							SELECT DISTINCT HcUserCouponGalleryID 
									, PC.CategoryID  
									, CategoryName = CASE WHEN CAT.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END
									, C.CouponID 
									, C.CouponName 
									, CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp2_8_2.fn_CouponImage(C.CouponID)
												  ELSE CASE WHEN CouponImagePath IS NOT NULL
														THEN CASE WHEN C.WebsiteSourceFlag = 1 
																THEN @RetailConfig +CONVERT(VARCHAR(30),C.RetailID)+'/'+CouponImagePath 
															 ELSE CouponImagePath 
															 END
														END 
												  END
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
							FROM Coupon C 
							INNER JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
							LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
							LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
							LEFT JOIN ProductCategory PC ON CP.ProductID = PC.ProductID
							LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
							WHERE CP.CouponProductId IS NULL AND CR.CouponRetailerID IS NULL
							AND UsedFlag = 0 AND HcuserID = @UserID
							--AND CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
							AND ((ActualCouponExpirationDate IS NULL AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90)
												OR (ActualCouponExpirationDate IS NOT NULL AND ActualCouponExpirationDate < GETDATE()))
							AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END)Coupon
					  
			  --To capture max row number.  
			  SELECT @MaxCnt = MAX(Row_Num) FROM #Gallery  
			  --this flag is a indicator to enable "More" button in the UI.   
			  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
			  
			  SELECT Row_Num AS rowNumber
					, UserCouponGalleryID userCoupGallId
					, ISNULL(CategoryID, -1) catId 
					, CategoryName catName
					, CouponID couponId
					, CouponName 
					, CouponDescription coupDesc
					, CouponDiscountAmount
					, CouponDiscountPct
					, CouponURL
					, CouponImagePath
					, CouponStartDate
					, CouponExpireDate 
					, UsedFlag 
					, ViewableOnWeb
			FROM #Gallery
			WHERE Row_Num BETWEEN (@LowerLimit + 1) AND  @UpperLimit
			ORDER BY CategoryName,CouponName ASC 
							
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GalleryExpiredCouponsByProduct].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
				
		END;
		 
	END CATCH;
END;








































GO
