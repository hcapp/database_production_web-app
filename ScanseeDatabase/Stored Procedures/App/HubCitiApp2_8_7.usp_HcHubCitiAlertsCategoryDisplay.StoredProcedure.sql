USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcHubCitiAlertsCategoryDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiAlertsCategoryDisplay
Purpose					: To display List fo Alerts Categories.
Example					: usp_HcHubCitiAlertsCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Mohith H R	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcHubCitiAlertsCategoryDisplay]
(   
    --Input variable.	  
	  @HubCitiID int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To display List fo Alerts Categories
			SELECT DISTINCT HcAlertCategoryID
			      ,HcAlertCategoryName 
				  ,HcHubCitiID 
				  ,DateCreated 												
				  ,CreatedUserID
			FROM HcAlertCategory WHERE HcHubCitiID=@HubCitiID
												
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiAlertsCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
