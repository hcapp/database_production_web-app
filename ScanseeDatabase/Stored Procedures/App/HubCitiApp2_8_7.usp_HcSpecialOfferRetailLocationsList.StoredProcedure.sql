USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcSpecialOfferRetailLocationsList]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcSpecialOfferRetailLocationsList]
Purpose					: To display locations associated to respective Special Offer Page.
Example					: [usp_WebHcSpecialOfferRetailLocationsList]

History
Version		Date			Author	     Change Description
--------------------------------------------------------------- 
1.0			21 May 2015     Mohith H R             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcSpecialOfferRetailLocationsList]
(
    --Input variable
	  @HcUserID int
	, @PageID int
	, @RetailID int	
	, @LowerLimit int
	, @ScreenName varchar(50)
	  
	--Output Variable 
	, @MaxCnt int output 
	, @NxtPageFlag bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @QRTypeID int
			SELECT @QRTypeID = QRTypeID
			FROM QRTypes
			WHERE QRTypeName = 'Special Offer Page'
			
			--To get Retailer Configuration
			DECLARE @RetailConfig Varchar(250)
			DECLARE @Config Varchar(250)

			SELECT @RetailConfig= ScreenContent  
			FROM AppConfiguration   
			WHERE ConfigurationType='Web Retailer Media Server Configuration'

			SELECT @Config=ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType='App Media Server Configuration'

			--To get the row count for pagination.
			 DECLARE @UpperLimit int 
			 SELECT @UpperLimit = @LowerLimit + ScreenContent 
			 FROM AppConfiguration 
			 WHERE ScreenName = @ScreenName 
			 AND ConfigurationType = 'Pagination'
			 AND Active = 1	

			SELECT DISTINCT Row_Num=IDENTITY(int,1,1)
			        , R.RetailID
			        , R.RetailName	
					, RL.RetailLocationID
					, completeAddress = RL.Address1+','+RL.City+','+RL.State+','+RL.PostalCode
					--, RL.Address1
					--, RL.City
					--, RL.State
					--, RL.PostalCode
					, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,
										(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)),
												@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)	
			INTO #SpecialOfferLocations									 
			FROM QRRetailerCustomPage P
			INNER JOIN QRRetailerCustomPageAssociation A ON P.QRRetailerCustomPageID = A.QRRetailerCustomPageID
			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID AND RL.Active=1 
			INNER JOIN Retailer R ON A.RetailID = R.RetailID AND R.RetailerActive=1
			INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode --AND HcHubCitiID = @HcHubCitiID
			INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1 -- RLC.HcHubCitiID =@HcHubCitiID 
			WHERE P.QRRetailerCustomPageID = @PageID AND P.QRTypeID = @QRTypeID
			AND A.RetailID = @RetailID


			
			 --To capture max row number.
			 SELECT @MaxCnt = MAX(Row_Num) FROM #SpecialOfferLocations
			
			  --this flag is a indicator to enable "More" button in the UI. 
			  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
			  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			  SELECT DISTINCT Row_Num
					, RetailID retailerId
			        , RetailName retailerName 	
					, RetailLocationID
					, completeAddress
					--, City
					--, State
					--, PostalCode
					, RetailerImagePath retImagePath 
			FROM #SpecialOfferLocations	
			WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
			ORDER BY Row_Num
			
			---------------Capture the impressions of the Retailer list.---------------- 3/21/2016-------------
            INSERT INTO HubCitiReportingDatabase..RetailerList(--MainMenuID	,																
																	 RetailID,
																	 RetailLocationID
																--, HcCitiExperienceID
																--, FindCategoryID 
																	, DateCreated)
			--OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)							
													SELECT --@MainMenuId,
														   retailId,
														  RetailLocationID
														--, @CityExperienceID 
														 --, CASE WHEN @CategoryID IS NOT NULL THEN NULL ELSE 0 END  --CASE WHEN @CategoryID IS NOT NULL THEN P.Param ELSE NULL END
														 , GETDATE()
													FROM #SpecialOfferLocations
			----------------------------------------------------------------------------------------------------

			
			--Confirmation of Success
			SELECT @Status = 0

	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcSpecialOfferRetailLocationsList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



























GO
