USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcGalleryAllCouponsByProduct]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcGalleryAllCouponsByProduct
Purpose					: To Display All Coupons By Product
Example					: usp_HcGalleryAllCouponsByProduct

History
Version		 Date		   Author	           Change Description
------------------------------------------------------------------------------- 
1.0		     21stNov2013   Dhananjaya TR	   Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcGalleryAllCouponsByProduct]
(

	--Input Variables 	
	   @HcUserID int
	 , @CategoryIDs varchar(100)
	 , @SearchKey varchar(500)
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(100)	
	 , @LowerLimit int
	 , @ScreenName varchar(100)
	 , @HcHubcitiID Int
	   
	--UserTracking Input Variables
	 , @MainMenuID int
	 
	--Output Variables
	 , @MaxCnt int output
	 , @NxtPageFlag bit output	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
			
		--To get Media Server Configuration.  
		DECLARE @RetailConfig varchar(50)    
		SELECT @RetailConfig=ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'  
		   
		--To get the row count for pagination.
		DECLARE @UpperLimit int   
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
		FROM AppConfiguration   
		WHERE ScreenName = @ScreenName   
		AND ConfigurationType = 'Pagination'  
		AND Active = 1  
		
		DECLARE @LowerLimitLatest INT
		SET @LowerLimitLatest = @LowerLimit + 1	



		--DECLARE @LocCategory Int		
			
		--SELECT DISTINCT BusinessCategoryID 
		--INTO #UserLocCategories
		--FROM HcUserPreferredCategory 
		--WHERE HcUserID=@HcUserID AND HcHubCitiID =@HcHubcitiID 

		--SELECT @LocCategory=COUNT(1) FROM #UserLocCategories 

		--To get Bottom Button Image URl
		DECLARE @Config VARCHAR(500)
		SELECT @Config = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'
		
		
		--Derive the Latitude and Longitude in the absence of the input.
			IF @Latitude IS NULL AND @Longitude IS NULL		
			BEGIN
				IF @PostalCode IS NOT NULL
				BEGIN
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude
					FROM GeoPosition 
					WHERE PostalCode = @PostalCode
				END
				ELSE 
				BEGIN
				    SELECT @PostalCode=PostalCode 
					FROM HcUser WHERE HcUserID=@HcUserID
				    
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude						
					FROM GeoPosition 
					WHERE PostalCode = @PostalCode
					 
				END
			END	
		--To get Default Radius
		DECLARE @Radius FLOAT
		SELECT @Radius = ScreenContent 
		FROM AppConfiguration
		WHERE ScreenName = 'DefaultRadius'
		AND ConfigurationType = 'DefaultRadius' 

		CREATE TABLE #Coupons(Row_Num INT IDENTITY(1, 1)
							  , CategoryID INT 
							  , CategoryName VARCHAR(255)
							  , CouponID INT
							  , CouponName VARCHAR(500)
							  , CouponDescription VARCHAR(1000)
							  , CouponImagePath VARCHAR(500)
							  , CouponStartDate DATETIME
							  , CouponExpireDate DATETIME
							  , CouponDiscountAmount MONEY
							  , ClaimFlag BIT
							  , RedeemFlag BIT
							  , NewFlag BIT
							  , NoOfCouponsToIssue INT
							  , Distance FLOAT
							  ) 
		
		--Check if the User has selected the any categories
		DECLARE @CategorySet BIT
		SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
		FROM HcUserCategory 
		WHERE HcUserID = @HcUserID	
		
		
		--Check if the user has not given latitude,longitude and PopulationCenterID
		IF (@Latitude IS NULL AND @Longitude IS NULL)
		BEGIN
			  --Check if the User has selected all category				  
			  IF @CategoryIDs = '0'  
			  BEGIN
			  
			    print 'No Category'
				INSERT INTO #Coupons( CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponImagePath
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
							 )
				    SELECT  CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponImagePath
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
				     FROM
					--Coupons associated to Products but not to RetailLocations
					(SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
								  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
								  , C.CouponID
								  , C.CouponName
								  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
								  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
													  ELSE CASE WHEN CouponImagePath IS NOT NULL
															THEN CASE WHEN C.WebsiteSourceFlag = 1 
																	THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																 ELSE CouponImagePath 
																 END
															END 
													  END
								  , CouponStartDate
								  , CouponExpireDate
								  , CouponDiscountAmount
								  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
								  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
								  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
								  , NoOfCouponsToIssue
								  , '0' Distance
								  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
							FROM Coupon C
							INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
							LEFT JOIN Product P ON CP.ProductID = P.ProductID
							LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
							LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
							LEFT JOIN CouponRetailer CR ON CP.CouponID = CR.CouponID
							LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
							LEFT JOIN HcRetailerAssociation HL ON  HL.HcHubCitiID=@HcHubcitiID AND RL.RetailLocationID =HL.RetailLocationID AND Associated =1 
							LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID							
							WHERE CR.CouponRetailerID IS NULL --AND HL.HcHubCitiID=@HcHubcitiID 
							--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
							AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
							AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
							AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory  WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
						)Coupon			  			  
			  END
			  
			 
			  --Check if the User has selected any category				  
			  ELSE IF @CategoryIDs > '0'  
			  BEGIN
				print 'Category'
				INSERT INTO #Coupons( CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponImagePath
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
							 )
				    SELECT  CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponImagePath
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
				     FROM
					--Coupons associated to Products but not to RetailLocations
					(SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
								  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
								  , C.CouponID
								  , C.CouponName
								  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
								  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
													  ELSE CASE WHEN CouponImagePath IS NOT NULL
															THEN CASE WHEN C.WebsiteSourceFlag = 1 
																	THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																 ELSE CouponImagePath 
																 END
															END 
													  END
								  , CouponStartDate
								  , CouponExpireDate
								  , CouponDiscountAmount
								  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
								  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
								  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
								  , NoOfCouponsToIssue
								  , '0' Distance
								  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
							FROM Coupon C
							INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
							LEFT JOIN Product P ON CP.ProductID = P.ProductID
							LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
							LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
							INNER JOIN [HubCitiApp2_3_3].fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
							LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
							LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1 
							LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
							LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
							WHERE CR.CouponRetailerID IS NULL --AND HL.HcHubCitiID=@HcHubcitiID
							--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
							AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
							AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
							AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
						)Coupon
			 END
		     --Check if the User has selected UnCategorized			  
		     ELSE IF @CategoryIDs = '-1'  
		     BEGIN
				print 'UnCategorized'
				INSERT INTO #Coupons( CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponImagePath
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
							 )
				    SELECT  CategoryID 
							  ,CategoryName 
							  , CouponID
							  , CouponName
							  , CouponDescription
							  , CouponImagePath
							  , CouponStartDate
							  , CouponExpireDate
							  , CouponDiscountAmount
							  , ClaimFlag
							  , RedeemFlag
							  , NewFlag
							  , NoOfCouponsToIssue
							  , Distance
				     FROM
					--Coupons associated to Products but not to RetailLocations
					(SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
								  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
								  , C.CouponID
								  , C.CouponName
								  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
								  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
													  ELSE CASE WHEN CouponImagePath IS NOT NULL
															THEN CASE WHEN C.WebsiteSourceFlag = 1 
																	THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																 ELSE CouponImagePath 
																 END
															END 
													  END
								  , CouponStartDate
								  , CouponExpireDate
								  , CouponDiscountAmount
								  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
								  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
								  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
								  , NoOfCouponsToIssue
								  , '0' Distance
								  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
							FROM Coupon C
							INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
							LEFT JOIN Product P ON CP.ProductID = P.ProductID
							LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
							LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
							--INNER JOIN [HubCitiApp2_3_3].fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
							LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
							LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
							LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
							LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
							WHERE CR.CouponRetailerID IS NULL AND PC.CategoryID IS NULL --AND HL.HcHubCitiID=@HcHubcitiID
							--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
							AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
							AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
							--AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
						)Coupon	
			 END	
		END					
		--Check if the user has given latitude and longitude
		IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)
		BEGIN
			PRINT 'P'
			--Check if the User has selected all category				  
			  IF @CategoryIDs = '0'  
			  BEGIN			  
				 print 'No Category'
					INSERT INTO #Coupons( CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponImagePath
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
										 )
							SELECT CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponImagePath
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
							FROM
							--Coupons associated to Products but not to RetailLocations W.R.T Latitude and Longitude
						    (SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL --AND HL.HcHubCitiID=@HcHubcitiID
									--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
									
									--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									 SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , Distance = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									--AND HL.HcHubCitiID = @HcHubcitiID
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									
									--Coupons not associated to RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL AND CP.CouponProductId IS NULL --AND HL.HcHubCitiID=@HcHubcitiID
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									)Coupons 														
			END	
		   --Check if the User has selected any category				 						  
		   ELSE IF @CategoryIDs > '0'  
		   BEGIN
				print 'Category'
				  INSERT INTO #Coupons( CategoryID
									  , CategoryName  
									  , CouponID
									  , CouponName
									  , CouponDescription
									  , CouponImagePath
									  , CouponStartDate
									  , CouponExpireDate
									  , CouponDiscountAmount
									  , ClaimFlag
									  , RedeemFlag
									  , NewFlag
									  , NoOfCouponsToIssue
									  , Distance
									  )
							SELECT CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponImagePath
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
							FROM
							--Coupons associated to Products but not to RetailLocations W.R.T Latitude and Longitude
						    (SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									INNER JOIN [HubCitiApp2_3_3].fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL AND RLC.HcHubCitiID=@HcHubcitiID
									--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
									
									--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									 SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									INNER JOIN [HubCitiApp2_3_3].fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
									INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1 
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID  AND HcUserID = @HcUserID
									WHERE GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									AND RLC.HcHubCitiID=@HcHubcitiID
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null))  OR (@Categoryset = 0 AND 1=1)) 
										
									--Coupons not associated to RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									INNER JOIN [HubCitiApp2_3_3].fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL AND CP.CouponProductId IS NULL AND RLC.HcHubCitiID=@HcHubcitiID
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									)Coupons 
				END
			  --Check if the User has selected UnCategorized				  
			  IF @CategoryIDs = '-1'  
			  BEGIN
				 print 'UnCategorized'
					INSERT INTO #Coupons( CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponImagePath
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
										 )
							SELECT CategoryID 
										  ,CategoryName 
										  , CouponID
										  , CouponName
										  , CouponDescription
										  , CouponImagePath
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag
										  , RedeemFlag
										  , NewFlag
										  , NoOfCouponsToIssue
										  , Distance
							FROM
							--Coupons associated to Products but not to RetailLocations W.R.T Latitude and Longitude
						    (SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL AND PC.CategoryID IS NULL AND RLC.HcHubCitiID=@HcHubcitiID
									--AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									--AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1))
									
									--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									 SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND PC.CategoryID IS NULL AND RLC.HcHubCitiID=@HcHubcitiID
									AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									--AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									
									--Coupons not associated to RetailLocations and Products W.R.T Latitude and Longitude
									UNION ALL
									SELECT DISTINCT CategoryID = CASE WHEN PC.CategoryID IS NULL THEN '-1' ELSE PC.CategoryID END
										  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE CAT.ParentCategoryName +' - '+CAT.SubCategoryName END 
										  , C.CouponID
										  , C.CouponName
										  , CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
										  , CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp8.fn_CouponImage(C.CouponID)
															  ELSE CASE WHEN CouponImagePath IS NOT NULL
																	THEN CASE WHEN C.WebsiteSourceFlag = 1 
																			THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
																		 ELSE CouponImagePath 
																		 END
																	END 
															  END
										  , CouponStartDate
										  , CouponExpireDate
										  , CouponDiscountAmount
										  , ClaimFlag = CASE WHEN UsedFlag IS NOT NULL THEN 1 ELSE 0 END
										  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
										  , NewFlag = CASE WHEN (DATEDIFF(DD, CouponStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
										  , NoOfCouponsToIssue
										  , '0' Distance
										  --, Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
									FROM Coupon C
									LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
									LEFT JOIN Product P ON CP.ProductID = P.ProductID
									LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
									LEFT JOIN Category CAT ON PC.CategoryID = CAT.CategoryID
									LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
									LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
									LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID AND HcUserID = @HcUserID
									WHERE CR.CouponRetailerID IS NULL AND CP.CouponProductId IS NULL
									AND PC.CategoryID IS NULL AND RLC.HcHubCitiID=@HcHubcitiID
									AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
									AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
									--AND ((@Categoryset = 1 AND (PC.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @HcUserID) OR PC.CategoryID is null)) OR (@Categoryset = 0 AND 1=1)) 
									)Coupons 			
												
			END	
	 END

	--Check if user has given populationCenterID

		SELECT	 rowNumber
				  ,	CategoryID
				  , CategoryName  
				  , CouponID
				  , CouponName
				  , CouponDescription
				  , CouponImagePath
				  , CouponStartDate
				  , CouponExpireDate
				  , CouponDiscountAmount
				  , ClaimFlag
				  , RedeemFlag
				  , NewFlag
				  , NoOfCouponsToIssue
				  , Distance 
				  , Used
		INTO #Temp
		FROM
	    (SELECT  rowNumber = ROW_NUMBER() OVER(ORDER BY Distance,CategoryName ASC,NewFlag DESC,CouponName ASC)
				  ,	CategoryID
				  , CategoryName  
				  , C.CouponID
				  , CouponName
				  , CouponDescription
				  , CouponImagePath
				  , CouponStartDate
				  , CouponExpireDate
				  , CouponDiscountAmount
				  , ClaimFlag
				  , RedeemFlag
				  , NewFlag
				  , NoOfCouponsToIssue
				  , MIN(Distance) Distance
				  , Used = COUNT(HcUserCouponGalleryID)
		FROM #Coupons C
		LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
		LEFT JOIN CouponRetailer CR ON CR.CouponID =C.CouponID 
		LEFT JOIN RetailerBusinessCategory RBC ON RBC.RetailerID =CR.RetailID 
		--LEFT JOIN #UserLocCategories UC ON UC.BusinessCategoryID =RBC.BusinessCategoryID 
		--WHERE (@LocCategory =0 OR (UC.BusinessCategoryID IS NOT NULL AND UC.BusinessCategoryID =RBC.BusinessCategoryID))
		GROUP BY CategoryID
				  , CategoryName  
				  , C.CouponID
				  , CouponName
				  , CouponDescription
				  , CouponImagePath
				  , CouponStartDate
				  , CouponExpireDate
				  , CouponDiscountAmount
				  , ClaimFlag
				  , RedeemFlag
				  , NewFlag
				  , NoOfCouponsToIssue
				  , Distance
		 HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)													   
		)Temp
		 
		--To capture max row number.  
		SELECT @MaxCnt = MAX(rowNumber) FROM #Temp  
		--this flag is a indicator to enable "More" button in the UI.   
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 						
		
		 SELECT  rowNumber
			  ,	CategoryID catId
			  , CategoryName catName 
			  , CouponID
			  , CouponName
			  , CouponDescription coupDesc
			  , CouponImagePath
			  , CouponStartDate
			  , CouponExpireDate
			  , CouponDiscountAmount
			  , ClaimFlag
			  , RedeemFlag
			  , NewFlag
			  , NoOfCouponsToIssue coupToIssue
			  , Distance
			  , Used
	    INTO #CouponList
		FROM #Temp
		WHERE rowNumber BETWEEN (@LowerLimit + 1) AND @UpperLimit
        ORDER BY rowNumber
        
        -- User Tracking Section   
		CREATE TABLE #Temp5(CouponListID int
							,CouponID int)
		
		DECLARE @SQL VARCHAR(500) = 'ALTER TABLE #Temp5 ADD RowNum INT IDENTITY('+CAST(@LowerLimitLatest AS VARCHAR(10))+', 1)'							
		EXEC (@SQL)
		
		INSERT INTO HubCitiReportingDatabase..CouponsList(MainMenuID
													      ,CouponID
													      ,DateCreated)
		OUTPUT inserted.CouponsListID,inserted.CouponID INTO #Temp5(CouponListID,CouponID)
														  
		SELECT @MainMenuID 
			   ,CouponID 
			   ,GETDATE() 
		FROM #CouponList  	
      	
      	SELECT 	rowNumber
      	      , CouponListID
			  ,	ISNULL(catId, 0)catId
			  , catName 
			  , CL.CouponID
			  , CouponName
			  , coupDesc
			  , CouponImagePath
			  , CouponStartDate
			  , CouponExpireDate
			  , CouponDiscountAmount
			  , ClaimFlag
			  , RedeemFlag
			  , NewFlag
			  , coupToIssue
			  , Distance
			  , Used
		FROM #CouponList CL
		INNER JOIN #Temp5 T ON CL.rowNumber = T.RowNum
		ORDER BY rowNumber

		--To get list of BottomButtons for this Module
		DECLARE @ModuleName Varchar(50) 
		SET @ModuleName = 'Deals'					
               
		SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
				, BB.HcBottomButtonID AS bottomBtnID
				, bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				, bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				, BottomButtonLinkTypeID AS btnLinkTypeID
				, btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HcHubCitiID), BottomButtonLinkID) 
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
											INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
											WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
																																				INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																				INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																				WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID)
										WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
											WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HcHubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																							WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID)
			                      	 
								ELSE BottomButtonLinkTypeName END)					
		FROM HcMenu HM
		INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
		INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
		INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
		INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
		LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
		WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HcHubCitiID
		ORDER by HcFunctionalityBottomButtonID
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcGalleryAllCouponsByProduct.'		
		-- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;














































GO
