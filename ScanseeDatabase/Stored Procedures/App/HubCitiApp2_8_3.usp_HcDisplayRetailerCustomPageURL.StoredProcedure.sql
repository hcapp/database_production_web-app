USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcDisplayRetailerCustomPageURL]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcDisplayRetailerCustomPageURL
Purpose					: To Display the Retailer Custom Page URLs.
Example					: usp_HcDisplayRetailerCustomPageURL

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th Jan 2015	Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcDisplayRetailerCustomPageURL]
(
	  
	  @UserID int
	, @RetailID int  
	, @PageID int  
	, @HubCitiID int
    
	--Output Variable 
	, @ShareText varchar(500) output
    --, @AppIcon varchar(500) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		--To get Media Server Configuration.  	   
		 DECLARE @RetailerConfi varchar(50)

		 SELECT @RetailerConfi= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration' 
	
	     --To Get App Media Server Configuration 
		 DECLARE @Config varchar(50)    
		 SELECT @Config=ScreenContent    
		 FROM AppConfiguration     
		 WHERE ConfigurationType='App Media Server Configuration'
		 
		 DECLARE @RetailerConfig varchar(50)
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='QR Code Configuration'
		 AND Active = 1

		 DECLARE @HubCitiConfig VARCHAR(1000)
		 SELECT @HubCitiConfig = ScreenContent
		 FROM AppConfiguration
		 WHERE ConfigurationType = 'Hubciti Media Server Configuration' 
		
		 --To get the text used for share.         
         DECLARE @HubCitiName varchar(100)
		 --DECLARE @AppIcon varchar(1000)
		 
		 SELECT @HubCitiName = HubCitiName
			   --,@AppIcon = @HubCitiConfig+CAST(@HubCitiID AS VARCHAR(10))+'/'+AppIcon
		 FROM HcHubCiti
		 WHERE HcHubCitiID = @HubCitiID
		       
         DECLARE @PageName varchar(100)

		 SELECT @PageName = QRTypeName
		 FROM QRRetailerCustomPage Q
		 INNER JOIN QRTypes R ON Q.QRTypeID = R.QRTypeID
		 WHERE QRRetailerCustomPageID = @PageID 

		 IF @RetailID IS NOT NULL
		 BEGIN					       
			 SELECT @ShareText = 'I found this ' +@PageName+ ' in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
			 --FROM AppConfiguration 
			 --WHERE ConfigurationType LIKE 'HubCiti AnythingPage/SpecialOffer Share Text' 
		 END
		 ELSE
		 BEGIN
			SELECT @ShareText = 'I found this AnyThing Page in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
		 END
	 	 
		SELECT 
			   QRTypeCode
		     , QRRCP.RetailID		       
		     , QRRCP.QRRetailerCustomPageID  
		     , COUNT(QRRCPA.RetailLocationID) Counts
		INTO #Temp
		FROM QRRetailerCustomPage QRRCP  
		INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
		INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCP.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
		WHERE QRRCPA.QRRetailerCustomPageID = @PageID
		AND QRRCP.RetailID = @RetailID
		GROUP BY QRTypeCode
		     , QRRCP.RetailID		     
		     , QRRCP.QRRetailerCustomPageID 
			 
		CREATE TABLE #AnythingPage(PageTitle VARCHAR(1000)
								  ,ImagePath VARCHAR(2000)
								  ,QRUrl VARCHAR(1000))
								  --,AppIcon VARCHAR(1000))	  		
		
		IF @RetailID IS NOT NULL
		BEGIN
			INSERT INTO #AnythingPage(PageTitle
									 ,ImagePath
									 ,QRUrl)
									 --,AppIcon)
			SELECT  pagetitle
				   ,retImage
				   ,QRUrl
				   --,AppIcon	
			FROM						   
			(SELECT DISTINCT  QRRCP.Pagetitle pageTitle					  				 
						   , CASE WHEN [Image] IS NULL THEN (SELECT @RetailerConfi + QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID) 
									ELSE @RetailerConfi + CAST(ISNULL(@RetailID, (SELECT RetailID FROM RetailLocation RL WHERE RL.RetailLocationID = H.RetailLocationID AND RL.Active = 1)) AS VARCHAR(10))+ '/' + [Image] END retImage
						   --If the user has selected the upload pdf or images or the link to existing then follow the SSQR template to construct the URL.
						   , QRUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL
									 --THEN CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?key1=' + CAST(T.RetailID AS VARCHAR(10))+ '&key2=0&key3=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10)) +'&EL=true'
										--	   WHEN COUNTS = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?key1=' + CAST(T.RetailID AS VARCHAR(10))+ '&key2=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&EL=true'
										--  END 
									 --ELSE			
									 ----If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
										--  CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?key1=' + CAST(T.RetailID AS VARCHAR(10)) + '&key2=0&key3=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+ '&EL=false'
										--	   WHEN Counts = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?key1=' + CAST(T.RetailID AS VARCHAR(10)) + '&key2='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&EL=false'
										--  END
									 --END	
									 THEN CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10)) +'&source=HubCiti&EL=true'
											   WHEN COUNTS = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti&EL=true'
										  END 
									 ELSE			
									 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
										  CASE WHEN Counts > 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+ '&source=HubCiti&EL=false'
											   WHEN Counts = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti&EL=false'
										  END
									 END
							--, @AppIcon AppIcon		 		               
			FROM #Temp T
			INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON T.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
			INNER JOIN QRRetailerCustomPage QRRCP ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID
			LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = T.QRRetailerCustomPageID
			LEFT JOIN HcAppSite H ON H.RetailLocationID = QRRCPA.RetailLocationID) AS A
		END
		ELSE
		BEGIN
			INSERT INTO #AnythingPage(PageTitle
										 ,ImagePath
										 ,QRUrl)
										 --,AppIcon)
			SELECT  pagetitle
					,retImage
					,QRUrl
					--,AppIcon	
			FROM
			(SELECT AnythingPageName pageTitle					
				  ,retImage = IIF(A.ImageIcon IS NOT NULL, @RetailerConfi + QRRetailerCustomPageIconImagePath, @HubCitiConfig + CAST(A.HcHubCitiID AS VARCHAR(100)) + '/' + ImagePath)					
				  --,QRUrl = @RetailerConfig +'2100.htm?key1='+ CAST(A.HcAnythingPageID AS VARCHAR(10))		
				  ,QRUrl = @RetailerConfig +'2100.htm?retId=null'+ '&retlocId=null&pageId='+ CAST(@PageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&source=HubCiti'+ CASE WHEN A.URL IS NOT NULL OR B.MediaPath LIKE '%pdf' OR B.MediaPath LIKE '%png' THEN '&EL=true' ELSE '&EL=false'	END			
				  --,@AppIcon AppIcon
			FROM HcAnythingPage A
			LEFT JOIN HcAnythingPageMedia B ON A.HcAnythingPageID = B.HcAnythingPageID
			LEFT JOIN QRRetailerCustomPageIcons D ON D.QRRetailerCustomPageIconID = A.ImageIcon
			LEFT JOIN HcAnyThingPageMediaType C ON C.HcAnyThingPageMediaTypeID = B.MediaTypeID		
			WHERE A.HcAnythingPageID = @PageID AND HcHubCitiID = @HubCitiID) A
		END


		SELECT PageTitle pagetitle
			  ,ImagePath retImage
			  ,QRUrl QRUrl	
			  --,AppIcon
		FROM #AnythingPage
			
			
		SET @Status = 0		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			SET @Status = 1
		END;
		 
	END CATCH;
END;



 





























GO
