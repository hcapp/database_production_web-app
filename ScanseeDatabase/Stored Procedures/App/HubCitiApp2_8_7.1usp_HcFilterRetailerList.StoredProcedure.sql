USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[1usp_HcFilterRetailerList]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFilterRetailerList]
Purpose					: To display Filter RetailerList.
Example					: [usp_HcFilterRetailerList]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		15/10/2013	    Pavan Sharma K	    1.0
2.0     12/21/2016      Shilpashree       optimization
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[1usp_HcFilterRetailerList]
(
    --Input variable.
       @UserID int 
     , @HCHubCitiID Int     
	 , @FilterID int
	 , @CategoryID Varchar(1000)  --Comma separated CategoryIDs
	 , @SearchKey varchar(255)
	 , @LowerLimit int  
	 , @ScreenName varchar(50)
	 , @Latitude decimal(18,6)    
	 , @Longitude decimal(18,6)    
	 , @ZipCode varchar(10)	 
	 , @SortColumn Varchar(200)
	 , @SortOrder Varchar(100)
	 , @LocalSpecials Bit	
	 
	 --Inputs for User Tracking
	 , @MainMenuId int
	   
	 --OutPut Variable
	 , @NoRecordsMsg nvarchar(max) output
	 , @UserOutOfRange bit output
	 , @DefaultPostalCode VARCHAR(10) output
	 , @RetailGroupButtonImagePath varchar(1000) output
	 , @MaxCnt int  output
	 , @NxtPageFlag bit output 
	 , @Status int output
	 , @ErrorNumber int output  
	 , @ErrorMessage varchar(1000) output  
     , @Description varchar(2000) output 
)
AS
BEGIN

	BEGIN TRY
			
		DECLARE @RegionAppID int = 0
				, @Config VARCHAR(100)
				, @RetailAffiliateCount INT
				, @CityExperienceID INT
				, @DistanceFromUser FLOAT
				, @Globalimage varchar(50)
				, @CityExpDefaultConfig varchar(50)
				, @ModuleName varchar(20) = 'Interests'
				, @UserLatitude float
				, @UserLongitude float 
				, @RetailConfig varchar(50)
				, @UpperLimit int 
				, @UserPreferredCity bit = 0

		CREATE TABLE #RetailItemsonSale(Retailid bigint , RetailLocationid bigint)
        CREATE TABLE #Retail1(RetailID INT 
                            , RetailName VARCHAR(1000)
                            , RetailLocationID INT
                            , Address1 VARCHAR(1000)
							, Address2 varchar(1000)
                            , City VARCHAR(1000)    
                            , State CHAR(2)
                            , PostalCode VARCHAR(20)
                            , retLatitude FLOAT
                            , retLongitude FLOAT
                            , RetailerImagePath VARCHAR(1000)      
                            , Distance FLOAT  
                            , DistanceActual FLOAT                                                   
                            , BusinessSubCategoryID int
							, SaleFlag BIT
							, locationOpen VARCHAR(10))

		SET @UserLatitude = @Latitude
		SET @UserLongitude = @Longitude
		SET @Globalimage = [dbo].[fn_ConfigvalueforScreencontent]('Image Not Found')
		SET @Config = [dbo].[fn_ConfigvalueforScreencontent]('App Media Server Configuration')
		SET @RetailConfig = [dbo].[fn_ConfigvalueforScreencontent]('Web Retailer Media Server Configuration')
		SET @CityExpDefaultConfig = [dbo].[fn_ConfigvalueforScreencontent]('City Experience Default Image Path')

		SELECT @RegionAppID = 1
			FROM HcHubCiti H
			INNER JOIN HcApplist A ON H.HcAppListID = A.HcAppListID
				WHERE HcHubCitiID = @HCHubCitiID AND HcAppListName = 'RegionApp'

		SELECT @CityExperienceID = HcCityExperienceID
			FROM HcFilter 
				WHERE HcFilterID = @FilterID
			
		--To get the row count for pagination.  
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
				WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1   
			
		--To get the image path of the given group.	
		SELECT @RetailGroupButtonImagePath = @Config + ISNULL(ButtonImagePath, @CityExpDefaultConfig)
			FROM HcCityExperience
				WHERE HcCityExperienceID = @CityExperienceID

		SELECT Param BusCatIDs
		INTO #BusinessCategoryIDs
			FROM fn_SplitParam (@CategoryID,',')

		--To display Short Description as output parameter
		SELECT @Description = FilterDescription 
			FROM HcFilter
				WHERE HcFilterID = @FilterID

		IF (@UserLatitude IS NULL) 
		BEGIN
			SELECT @UserLatitude = Latitude
					, @UserLongitude = Longitude
			FROM HcUser A
			INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				WHERE HcUserID = @UserID 
		END	

		--Pick the co ordinates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
			SELECT @UserLatitude = Latitude
					, @UserLongitude = Longitude
			FROM HcHubCiti A
			INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID
		END

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiapp2_8_7].[usp_HcUserHubCitiRangeCheck] @UserID, @HCHubCitiID, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
		
		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			IF @ZipCode IS NULL
			BEGIN
				SELECT @Latitude = G.Latitude
						, @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @UserID
			END
			ELSE
			BEGIN
				SELECT @Latitude = Latitude
						, @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @ZipCode
			END
		END
			
		SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID AND HcCityID IS NULL THEN 0
										WHEN HcUserID = @UserID AND HcCityID IS NOT NULL THEN 1
										ELSE 0 END
		FROM HcUsersPreferredCityAssociation
		WHERE HcHubcitiID = @HCHubCitiID AND HcUserID = @UserID
		
			
		-- To identify Retailer that have products on Sale or any type of discount
			
		    
				
		--Filter the Retailers based on the given Affiliate.						
						SELECT Row_Num = IDENTITY(INT,1,1) 
										, RetailID    
										, RetailName
										, RetailLocationID
										, Address1
										, Address2
										, Address3
										, Address4
										, City    
										, State
										, PostalCode									
										, RetailerImagePath    
										, Distance 	
										, DistanceActual
										, SaleFlag	
										, RetailLocationLatitude
										, RetailLocationLongitude
										--, HcCityID																																
							INTO #Retail
							FROM 
							(SELECT DISTINCT TOP 100 PERCENT R.RetailID     
														   , R.RetailName
														   , RL.RetailLocationID 
														   , RL.Address1     
														   , RL.Address2
														   , RL.Address3
														   , RL.Address4
														   , RL.City
														   , RL.State
														   , RL.PostalCode													   
														  , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
														  , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
														  , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) 
								 , SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
														   , RetailLocationLatitude=CASE WHEN RL.RetailLocationLatitude IS NOT NULL THEN RL.RetailLocationLatitude ELSE G.Latitude END 
														   , RetailLocationLongitude=CASE WHEN RL.RetailLocationLongitude IS NOT NULL THEN RL.RetailLocationLongitude ELSE G.Longitude END  
														  -- , C.HcCityID 															  
							FROM Retailer R    
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0   
							INNER JOIN HcLocationAssociation HLA ON HLA.HcHubCitiID = @HCHubCitiID AND HLA.PostalCode = RL.PostalCode  
							INNER JOIN HcCity C ON HLA.HcCityID = C.HcCityID
							INNER JOIN HcFilterRetailLocation HL ON HL.HcFilterID=@FilterID AND HL.RetailLocationID=RL.RetailLocationID 
							INNER JOIN RetailerBusinessCategory RB ON R.RetailID =RB.RetailerID 		                
							LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode 
							WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND @UserPreferredCity = 0 AND RL.Active =1 AND R.RetailerActive = 1
							AND ((@CategoryID IS NULL AND 1 = 1) OR RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs))
							AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+ @SearchKey +'%' END

							UNION ALL

							SELECT DISTINCT TOP 100 PERCENT R.RetailID     
														   , R.RetailName
														   , RL.RetailLocationID 
														   , RL.Address1     
														   , RL.Address2
														   , RL.Address3
														   , RL.Address4
														   , RL.City
														   , RL.State
														   , RL.PostalCode													   
														  , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
														  , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
														  , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) 
														, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
														   , RetailLocationLatitude=CASE WHEN RL.RetailLocationLatitude IS NOT NULL THEN RL.RetailLocationLatitude ELSE G.Latitude END 
														   , RetailLocationLongitude=CASE WHEN RL.RetailLocationLongitude IS NOT NULL THEN RL.RetailLocationLongitude ELSE G.Longitude END  
							FROM Retailer R    
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0   
							INNER JOIN HcLocationAssociation HLA ON  HLA.HcHubCitiID = @HCHubCitiID AND HLA.PostalCode = RL.PostalCode  
							INNER JOIN HcCity C ON HLA.HcCityID = C.HcCityID
						    INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HcHubcitiID
							INNER JOIN HcFilterRetailLocation HL ON HL.HcFilterID=@FilterID AND HL.RetailLocationID=RL.RetailLocationID 
							INNER JOIN RetailerBusinessCategory RB ON R.RetailID =RB.RetailerID 		                
							LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode 
							WHERE @RegionAppID = 1 AND @UserPreferredCity = 1 AND RL.Active = 1 AND R.RetailerActive = 1
							AND ((@CategoryID IS NULL AND 1 = 1) OR RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs))
							AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+ @SearchKey +'%' END
							)Retailer 
							
		SELECT Row_Num rowNumber  
						, RetailID retailerId    
						, RetailName retailerName
						, R.RetailLocationID retailLocationID
						, Address1 retaileraddress1
						, Address2 retaileraddress2
						, City City
						, State State
						, PostalCode PostalCode    
						, RetailerImagePath logoImagePath     
						, Distance  
						, DistanceActual    
						, S.BannerAdImagePath bannerAdImagePath    
						, B.RibbonAdImagePath ribbonAdImagePath    
						, B.RibbonAdURL ribbonAdURL    
						, B.RetailLocationAdvertisementID advertisementID  
						, S.SplashAdID splashAdID
						, SaleFlag
						, RetailLocationLatitude retLatitude
						, RetailLocationLongitude retLongitude  						
					 INTO #RetailerList	
					 FROM #Retail R
					 LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
																	ELSE SplashAdImagePath
																  END
									   , SplashAdID  = ASP.AdvertisementSplashID
									   , R.RetailLocationID 
								FROM #Retail R
									INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
									INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
									AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)) S
							ON R.RetailLocationID = S.RetailLocationID
					 LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
																ELSE AB.BannerAdImagePath
															END 
									  , RibbonAdURL = AB.BannerAdURL
									  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
									  , R.RetailLocationID 
								 FROM #Retail R
								 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
								 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
								 WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)) B
					 ON R.RetailLocationID = B.RetailLocationID
				
		SELECT  rowNumber = ROW_NUMBER() OVER 
						(ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
										  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT) 
										  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
									  END ASC,Distance)
					, retailerId    
					, retailerName
					, retailLocationID
					, retaileraddress1
					, retaileraddress2
					, City
					, State
					, PostalCode PostalCode    
					, logoImagePath     
					, Distance  
					, DistanceActual    
					, bannerAdImagePath    
					, ribbonAdImagePath    
					, ribbonAdURL    
					, advertisementID  
					, splashAdID
					, SaleFlag
					, retLatitude
					, retLongitude
		INTO #FinalList
		FROM #RetailerList
		WHERE (@LocalSpecials = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))	
				
		--To capture max row number.  
		SELECT @MaxCnt = COUNT(rowNumber) FROM #FinalList
					 
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
		
		--UserTracking --Table to track the Retailer List.
		CREATE TABLE #Temp(RetailerListID int
                        ,MainMenuID int
                        ,RetailID int
                        ,RetailLocationID int
                        ,HcCitiExperienceID int)  
                      
        --Capture the impressions of the Retailer list.
        INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID																	
													, RetailID
													, RetailLocationID
													, HcCitiExperienceID
													, FindCategoryID 
													, DateCreated)
		OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)							
									SELECT @MainMenuId
											, retailerId
											, RetailLocationID
											, @CityExperienceID 
											, CASE WHEN @CategoryID IS NOT NULL THEN NULL ELSE 0 END  --CASE WHEN @CategoryID IS NOT NULL THEN P.Param ELSE NULL END
											, GETDATE()
									FROM #FinalList
									--LEFT JOIN [HubCitiapp2_8_7].fn_SplitParam(@CategoryID, ',') P ON 1=1
		--Table to Track Cities
		--SELECT DISTINCT HcCityID
		--INTO #City
		--FROM #CityList
						
		--INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
		--												,CityID
		--												,DateCreated
		--												)
		--										SELECT @MainMenuId
		--											   ,HcCityID
		--											   ,Getdate()
		--										FROM #City
		
		--Display the Retailer along with the keys generated in the Tracking table.
		SELECT rowNumber
				, T.RetailerListID retListID
				, retailerId
				, retailerName
				, T.RetailLocationID
				, retaileraddress1
				, retaileraddress2
				, City
				, State
				, PostalCode
				, logoImagePath
				, Distance = ISNULL(DistanceActual, Distance)
				, bannerAdImagePath
				, ribbonAdImagePath
				, ribbonAdURL
				, advertisementID
				, splashAdID
				, SaleFlag	
				, retLatitude
				, retLongitude														 					 
		FROM #Temp T
		INNER JOIN #FinalList R ON  R.RetailLocationID = T.RetailLocationID
		WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
		ORDER BY rowNumber	

		--To display bottom buttons								
		EXEC [HubCitiapp2_8_7].[usp_HcFunctionalityBottomButtonDisplay] @HcHubcitiID, @ModuleName, @UserID, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

		--To display message when no Retailers display for user preferred cities.
		DECLARE @UserPrefCities NVarchar(MAX)

		IF (@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0) AND (@SearchKey IS NULL) AND @CategoryID IS NULL AND @LocalSpecials = 0
		BEGIN 
			SELECT @UserPrefCities = COALESCE(@UserPrefCities+',' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName))) 
			FROM HcUsersPreferredCityAssociation P
			INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
			WHERE HcHubcitiID = @HCHubCitiID AND HcUserID = @UserID
						
		SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
								'.\n\nUpdate your city preferences in the settings menu.'

		END
		ELSE IF ((@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0)) 
			AND (@SearchKey IS NOT NULL OR @CategoryID IS NOT NULL OR @LocalSpecials = 1)
		BEGIN
					
		SELECT @NoRecordsMsg = 'No Records Found.'

		END
		ELSE IF (@RegionAppID = 0) AND (ISNULL(@MaxCnt,0) = 0)
		BEGIN
		SELECT @NoRecordsMsg = 'No Records Found.'
		END
			
		--Confirmation of Success
		SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcFilterRetailerList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
