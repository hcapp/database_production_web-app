USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_NewsFirstSingleSideNavigationDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_NewsFirstSingleSideNavigationDisplay]
Purpose					: To display single Side Navigation thoughout the app.
Example					: [usp_NewsFirstSingleSideNavigationDisplay]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12/15/2016		 1.0				Bindu T A -- To display single Side Navigation thoughout the app
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_NewsFirstSingleSideNavigationDisplay]

(
    --Input variable.
      @LinkID Int
    , @HubCitiID Varchar(100)
    , @LevelID Int  
    , @UserID Int      
       
	--Output Variable 
	, @NoRecordsMsg nvarchar(max) output
	, @FilterID int output
	, @FilterName varchar(255) output
	, @FilterCount int output
	, @DownLoadLinkIOS Varchar(1000) output
	, @DownLoadLinkAndroid Varchar(1000) output
	, @RetailGroupButtonImagePath varchar(1000) output
	, @AppIconImagePath varchar(1000) output
	, @HCMenuBannerImage Varchar(1000) output
	, @HcDepartmentFlag bit output
	, @HcTypeFlag bit output
	, @NoOfColumns int output
	, @MenuName varchar(255) output
	, @IsRegionApp bit output
	, @TemplateChanged bit output
	, @TempleteBackgroundImage VARCHAR(1000) output
	, @DisplayLabel BIT output
	, @LabelBckGndColor Varchar(250) output
	, @LabelFontColor Varchar(250) output
	, @homeImgPath varchar(250) output
	, @bkImgPath varchar(250) output
	, @titleBkGrdColor varchar(250) output
	, @titleTxtColor varchar(250) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN


	BEGIN TRY

	DECLARE @SortOrder Varchar(100)  
    , @TypeID int
    , @DepartmentID int
	, @DateCheck datetime
	, @DeviceType varchar(100) --if input is Ipad then only send ipad image values

	SET @SortOrder = NULL
    SET @TypeID = NULL
    SET @DepartmentID = NULL
	SET @DateCheck = NULL
	SET @DeviceType = NULL

	IF @LinkID IS  NULL or @LinkID=0
		SELECT @LinkID = (SELECT top 1 HCMenuID FROM HcMenu WHERE  HcHubcitiId=@HubcitiID AND IsDefaultHubCitiSideMenu=1  )

		-- SELECT @LinkID 

		UPDATE a set NewsSideNavigationID=b.HcMenuItemID  
		FROM HcUserNewsFirstSingleSideNavigation a 
		INNER join HcMenuItem b on b.MenuItemName=a.NewsSideNavigationDisplayValue
		INNER join  HcMenu  on HcMenu.HcMenuID = b.HcMenuID
		WHERE a.Flag=1 and a.NewsSideNavigationID <> b.HcMenuItemID
		AND   a.HchubcitiID = @HubcitiID and HcMenu.HchubcitiID=@HubcitiID
		AND HcMenu.HcMenuID = @LinkID

	 IF EXISTS(SELECT 1 FROM HcMenu H
			INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
			WHERE HM.DateCreated = @DateCheck AND HcHubCitiID = @HubCitiID) AND @LinkID = 0
	 BEGIN
		SELECT @TemplateChanged = 0, @Status = 0
	 END

     ELSE IF EXISTS(SELECT 1 FROM HcMenu H
			   INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
			   WHERE HM.DateCreated = @DateCheck AND HcHubCitiID = @HubCitiID) AND @LinkID <> 0
		BEGIN
			SELECT @TemplateChanged = 0, @Status = 0
		END
	ELSE IF NOT EXISTS (SELECT 1 FROM HcMenu H
			INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
			WHERE IsDefaultHubCitiSideMenu =1 AND HcHubCitiID = @HubCitiID )
		BEGIN
			SELECT @TemplateChanged = 0, @Status = 0
		END

	  ELSE
		BEGIN	
			DECLARE @Template Varchar(100)
				, @CityConfig VARCHAR(1000)
				, @CityExpDefaultConfig VARCHAR(1000)			
				, @GroupedTabTextColor VARCHAR(100)
				, @GroupedTabTextFontColor VARCHAR(100)
				, @Config VARCHAR(500)
				, @HcAppListName VARCHAR(10)
				
			SET @LinkID =IIF(@LinkID IS NULL OR @LinkID=0 ,Null,@LinkID)
			SET @TypeID =IIF(@TypeID IS NULL OR @typeID=0,Null,@TypeID)
			SET @DepartmentID =IIF(@DepartmentID IS NULL OR @DepartmentID=0,Null,@DepartmentID)		
			
			DECLARE @HcLinkTypeIDS VARCHAR(100)
			SELECT @HcLinkTypeIDS = COALESCE(@HcLinkTypeIDS+',','') + CAST(HcLinkTypeID AS VARCHAR(100))
			FROM HcLinkType WHERE LinkTypeName IN ('Text','Label')
			
			SELECT @MenuName = MenuName
			FROM HcMenu
			WHERE HcMenuID = @LinkID AND ISNULL(@LinkID, 0) <> 0
			OR ISNULL(@LinkID, 0) = 0 AND Level = 1
			
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			IF @DepartmentID =0
			BEGIN
				SET @DepartmentID = NULL
			END

			IF @TypeID =0
			BEGIN
				SET @TypeID = NULL
			END
			 
			--To get the Filter Details and Count of filters for a given hub Citi.
			SELECT @FilterID = F.HcFilterID
				 , @FilterName = F.FilterName
			FROM HcCityExperience H
			INNER JOIN HcFilter F ON H.HcCityExperienceID = F.HcCityExperienceID AND H.HcHubCitiID = @HubCitiID

			SELECT @FilterCount = COUNT(1)
			FROM HcFilter 
			WHERE HcHubCitiID = @HubCitiID
           
		   --------------------------------------------------------
			SELECT DISTINCT HM.HcTemplateID
			INTO #Templates
			FROM HcMenuItem MI
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			INNER JOIN HcTemplate T ON HM.HcTemplateID = T.HcTemplateID
			WHERE HM.HCHubcitiID=@HubCitiID AND	(@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID ))
			AND T.TemplateName IN ('Combo Template','Grouped Tab','Grouped Tab With Image') 
			    
		   CREATE TABLE #Groups(HcMenuItemID int)
		   IF EXISTS (SELECT 1 FROM #Templates)
		   BEGIN
			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc) 
			      ,H.HcMenuItemID				  
			INTO #group1
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE  (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID 	
				  , MenuItemName			 
			INTO #group2
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group3
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where  (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group4
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 
			
            --Select menuitems based on the Department and Type values
			SELECT H.HcMenuItemID 
				  , H.MenuItemName
			INTO #menuitem
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID
			WHERE ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND H.HcMenuItemTypeID =@TypeID )) 
						AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND H.HcDepartmentID  =@DepartmentID))))
						
			ORDER BY H.HcMenuItemID
	
			Declare @maxGroup Int
			DECLARE @minGroup Int

			--Select maximum menuitemid in the selected group template
			SELECT @maxGroup = MAX(HcMenuItemID) From #group1
			SELECT @minGroup = Min(HcMenuItemID) From #group1
		
			--Here based on the temporary table data, selecting menuitemid under a group and storing in temp table
			
			INSERT INTO #Groups
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )	--here check the menuitem is belong to first group			
			union
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			where
			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID 
								AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID) 
			union
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			where 
			(@maxGroup <M.HcMenuItemID AND @maxGroup =A.HcMenuItemID)
			
			SELECT DISTINCT A.HcMenuItemID
			             ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
			INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )				
			union
			SELECT DISTINCT A.HcMenuItemID
			              
			              ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
			--INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE
			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID 
							AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID)
			union
			SELECT DISTINCT A.HcMenuItemID
			              
			              ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
			--INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE 
			(@maxGroup <M.HcMenuItemID AND @maxGroup =A.HcMenuItemID)

			SELECT DISTINCT M.GroupIDs
			      ,MenuitemsID
				  ,M.MenuItemName GroupName
			INTO #Sort
			FROM
			(SELECT DISTINCT HcMenuItemID GroupIDs
			                ,menuitemid MenuitemsID
			FROM #SortOrders
			UNION ALL
			SELECT DISTINCT HcMenuItemID GroupIDs
			               ,HcMenuItemID MenuitemsID
			FROM #SortOrders)A
			INNER JOIN HcMenuItem M ON M.HcMenuItemID =A.GroupIDs
			ORDER BY MenuitemsID,M.GroupIDs Asc

			
			END
			
			--For the Grouped tab template send the button & the font colors from the configuration table.
			SELECT @GroupedTabTextColor = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti Grouped Tab Text Color'
			AND ScreenName = 'HubCiti Grouped Tab Text Color'
			
			
			SELECT @GroupedTabTextFontColor = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti Grouped Tab Text Font Color'
			AND ScreenName = 'HubCiti Grouped Tab Text Font Color'

			SELECT @CityConfig=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
		
			SELECT @CityExpDefaultConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'City Experience Default Image Path'
			AND Active = 1
			
			SELECT Param LinkTypeIDs
			INTO #LinkTypeIDs
			FROM fn_SplitParam(@HcLinkTypeIDS,',')

			SELECT @DownLoadLinkIOS=ItunesURL
			FROM HcHubCiti HC
			WHERE HC.HcHubCitiID=@HubCitiID  
						
			
			SELECT @DownLoadLinkAndroid=GooglePlayURL
			FROM HcHubCiti HC
			WHERE HC.HcHubCitiID=@HubCitiID 
			
			SELECT @RetailGroupButtonImagePath = @CityConfig + ISNULL(R.ButtonImagePath, @CityExpDefaultConfig)
			FROM RetailGroup R
			INNER JOIN HcCityExperience C ON C.HcCityExperienceID=R.RetailGroupID AND HcHubCitiID =@HubCitiID 
			
			SELECT @RetailGroupButtonImagePath = ISNULL(@RetailGroupButtonImagePath, @CityConfig + @CityExpDefaultConfig)
			
			--Send AppIcon
			SELECT @AppIconImagePath = @Config + CAST(HcHubCitiID AS VARCHAR(10)) + '/' + AppIcon
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
			
			SELECT DISTINCT @HCMenuBannerImage= IIF(HCMenuBannerImage IS NOT NULL AND HCMenuBannerImage <> '',@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HCMenuBannerImage,null )
				  ,@HcDepartmentFlag = HcDepartmentFlag
				  ,@HcTypeFlag = HcTypeFlag	
				  ,@NoOfColumns = NoOfColumns
			FROM HcMenuItem MI
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			INNER JOIN HCTemplate HCT on HCT.HCTemplateID = HM.HCTemplateID
			INNER JOIN HCLinkType N ON N.HcLinkTypeID = MI.HcLinkTypeID
			WHERE HM.HCHubcitiID=@HubCitiID AND --(MI.HcMenuID=ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 AND Level=1))   
			(@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID ))   
						
			CREATE TABLE #MenuItems(RowNum INT IDENTITY(1,1)
									,MenuID INT
									,HubCitiId INT
									,templateName VARCHAR(250)
									,Level INT
									,mItemID INT
									,mItemName VARCHAR(250)
									,LinkTypeName VARCHAR(250)
									,LinkTypeID INT
									,LinkID INT
									,Position INT
									,mItemImg VARCHAR(1000)
									,mBkgrdColor VARCHAR(100)
									,mBkgrdImage VARCHAR(1000)
									,mBtnColor VARCHAR(100)
									,mBtnFontColor VARCHAR(100)
									,smBkgrdColor VARCHAR(100)
									,smBkgrdImage VARCHAR(1000)
									,smBtnColor VARCHAR(100)
									,smBtnFontColor VARCHAR(100)
									,HcDepartmentID INT
									,HcMenuItemTypeID INT			
									,departmentName VARCHAR(250)
									,mItemTypeName VARCHAR(250)
									,HcMenuItemShapeID INT
									,HcMenuItemShape  VARCHAR(100)
									,mGrpBkgrdColor VARCHAR(100)
									,mGrpFntColor VARCHAR(100)
									,smGrpBkgrdColor VARCHAR(100)
									,smGrpFntColor VARCHAR(100)
									,MenuName VARCHAR(250)
									,mFontColor VARCHAR(100)
									,smFontColor VARCHAR(100)
									,dateModified datetime
									,TemplateBackgroundColor VARCHAR(100)
									--,NoOfColumns INT
									)

			CREATE TABLE #Menuitemss
			(
			MenuItemID INT
			)

			DECLARE @RegionAppFlag bit = 0

			IF(SELECT 1 from HcHubCiti H
				  INNER JOIN HcAppList AL ON H.HcAppListID = AL.HcAppListID 
				  AND H.HcHubCitiID = @HubCitiID AND AL.HcAppListName = 'RegionApp')>0
					BEGIN
						SELECT @RegionAppFlag = 1
					END
			 ELSE
					BEGIN
						SELECT @RegionAppFlag =0
					END

			DECLARE @GuestUser int
			DECLARE @CityPrefNotVisitedUser bit

		IF (@RegionAppFlag = 1)
		BEGIN
			--select 'a'
					
					SELECT @GuestUser = U.HcUserID
					FROM HcUser U
					INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
					WHERE UserName = 'GuestLogin'
					AND DA.HcHubCitiID = @HubCitiID
			
					IF NOT EXISTS (SELECT HcUserID FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HubCitiID AND HcUserID = @UserID and HcCityID is not null)
					BEGIN
						SET @CityPrefNotVisitedUser = 1
					END
					ELSE
					BEGIN
						SET @CityPrefNotVisitedUser = 0
					END

					--SELECT @CityPrefNotVisitedUser
			
				--To Check City is there for given menu or not	
				IF @CityPrefNotVisitedUser=0 AND ISNULL(@GuestUser,0) <> @UserID
				BEGIN
					--select 'nn1'
					--select @LinkID
					IF EXISTS (SELECT 1 FROM HcUserNewsFirstSingleSideNavigation WHERE HchubcitiID= @HubCitiID AND HcUserID = @UserID)
					BEGIN 					
						INSERT INTO #Menuitemss
						SELECT DISTINCT HM.HcMenuItemID
						from HcRegionAppMenuItemCityAssociation  RMC
						INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND RMC.HcHubcitiId=UC.HcHubcitiId
						inner join HcLocationAssociation on HcLocationAssociation.HcHubCitiID=RMC.HcHubcitiId and  RMC.HcCityID=HcLocationAssociation.HcCityID
						INNER JOIN  HcMenuItem HM on HM.HcMenuItemID= RMC.HcMenuItemID
						INNER JOIN HcUserNewsFirstSingleSideNavigation UN on HM.HcMenuItemID=UN.NewsSideNavigationID 
						INNER JOIN HcMenu MI ON Mi.HcMenuID = HM.HCMenuID AND MI.HcHubCitiID = @HubCitiID 
						AND ((@LinkID IS NULL AND Level =1) OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID))	
						where RMC.HCHubcitiID = @HubCitiID and HM.HcMenuID = @LinkID AND UN.HcHubcitiId=@HubCitiID   and Flag= 1 --AND UN.HcUserID=@UserID
						AND UC.HcUserID=@UserID
					END
					ELSE 
					BEGIN 
						INSERT INTO #Menuitemss
						SELECT DISTINCT HM.HcMenuItemID
						FROM HcRegionAppMenuItemCityAssociation  RMC
						INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND RMC.HcHubcitiId=UC.HcHubcitiId AND  RMC.HCHubcitiID = @HubCitiID 
						inner join HcLocationAssociation on HcLocationAssociation.HcHubCitiID=RMC.HcHubcitiId and  RMC.HcCityID=HcLocationAssociation.HcCityID
						INNER JOIN  HcMenuItem HM on HM.HcMenuItemID= RMC.HcMenuItemID AND  HM.HcMenuID= @linkID
						LEFT JOIN HcUserNewsFirstSingleSideNavigation UN on HM.HcMenuItemID=UN.NewsSideNavigationID AND UN.HcHubcitiId=@HubCitiID and Flag= 1
						where UC.HcUserID=@UserID
					END
				
				END  
				ELSE
				BEGIN

					INSERT INTO #Menuitemss
					SELECT DISTINCT MI.HcMenuItemID
					FROM HcMenuItem MI
					INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID AND HM.HcHubCitiID = @HubCitiID 
					AND ((@LinkID IS NULL AND Level =1) OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID))

				END
									 
		  END
		  ELSE
		  BEGIN 
			--select 'b'
				INSERT INTO #Menuitemss
				SELECT DISTINCT MI.HcMenuItemID
						
				FROM HcMenuItem MI
				INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID AND HM.HcHubCitiID = @HubCitiID 
				AND ((@LinkID IS NULL AND Level =1) OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID))
				
		 ENd

		-- SELECT * FROM #Menuitemss

				INSERT INTO #MenuItems (MenuID
										,HubCitiId
										,templateName
										,Level
										,mItemID
										,mItemName
										,LinkTypeName
										,LinkTypeID
										,LinkID
										,Position
										,mItemImg
										,mBkgrdColor
										,mBkgrdImage
										,mBtnColor
										,mBtnFontColor
										,smBkgrdColor
										,smBkgrdImage
										,smBtnColor
										,smBtnFontColor
										,HcDepartmentID
										,HcMenuItemTypeID			
										,departmentName
										,mItemTypeName
										,HcMenuItemShapeID 
										,HcMenuItemShape 
										,mGrpBkgrdColor
										,mGrpFntColor
										,smGrpBkgrdColor
										,smGrpFntColor
										,MenuName
										,mFontColor
										,smFontColor
										,dateModified
										,TemplateBackgroundColor
										)
			         SELECT DISTINCT MenuID
							,HubCitiId
							,templateName
							,Level
							,mItemID
							,mItemName
							,LinkTypeName
							,LinkTypeID
							,LinkID
							,Position
							,mItemImg
							,mBkgrdColor
							,mBkgrdImage
							,mBtnColor
							,mBtnFontColor
							,smBkgrdColor
							,smBkgrdImage
							,smBtnColor
							,smBtnFontColor
							,HcDepartmentID
							,HcMenuItemTypeID			
							,departmentName
							,mItemTypeName
							,HcMenuItemShapeID 
							,HcMenuItemShape 
							,mGrpBkgrdColor
							,mGrpFntColor
							,smGrpBkgrdColor
							,smGrpFntColor
							,MenuName
							,mFontColor
							,smFontColor
							,dateModified
							,TemplateBackgroundColor
					FROM
					(SELECT DISTINCT HM.HcMenuID as MenuID			
						  ,HM.HCHubCitiID as HubCitiId
						  ,HCT.TemplateName as templateName
						  ,HM.Level
						  ,MI.HcMenuItemID as mItemID
						  ,MenuItemName	as mItemName
						  ,LinkTypeName,N.HcLinkTypeID as LinkTypeID,LinkID	   		    
						  ,Position	
						  ,mItemImg= CASE WHEN @DeviceType = 'Ipad' AND (HCT.TemplateName = '4X4 Grid' OR HCT.TemplateName = 'Rectangular Grid') 
								  THEN @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(MI.HcMenuItemIpadImagePath,MI.HcMenuItemImagePath)
								  WHEN @DeviceType = 'Ipad' AND (HCT.TemplateName != '4X4 Grid' OR HCT.TemplateName<> 'Rectangular Grid') 
								  THEN @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath
								  ELSE @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+MI.HcMenuItemImagePath 
							END
						  ,mBkgrdColor = IIF(HM.Level = 1, MenuBackgroundColor, NULL)
						  ,mBkgrdImage = IIF(HM.Level = 1, @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+MenuBackgroundImage, NULL)
						  ,mBtnColor = IIF(HM.Level = 1, MenuButtonColor, NULL)
						  ,mBtnFontColor = IIF(HM.Level = 1, MenuButtonFontColor, NULL)
						  ,smBkgrdColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuBackgroundColor, NULL)
						  ,smBkgrdImage = IIF(ISNULL(HM.Level, 0) <> 1,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+SubMenuBackgroundImage, NULL)				  
						  ,smBtnColor = IIF(ISNULL(HM.Level, 0) <> 1, CASE WHEN N.LinkTypeName = 'Text' OR N.LinkTypeName = 'Label' THEN @GroupedTabTextColor ELSE SubMenuButtonColor END, NULL)
						  ,smBtnFontColor = IIF(ISNULL(HM.Level, 0) <> 1, CASE WHEN N.LinkTypeName = 'Text' OR N.LinkTypeName = 'Label' THEN @GroupedTabTextFontColor ELSE SubMenuButtonFontColor END, NULL) 
						  ,MI.HcDepartmentID
						  ,MI.HcMenuItemTypeID			
						  ,D.HcDepartmentName departmentName
						  ,T.HcMenuItemTypeName mItemTypeName
						  ,S.HcMenuItemShapeID 
						  ,S.HcMenuItemShape 
						  ,mGrpBkgrdColor = IIF(HM.Level = 1, MenuGroupBackgroundColor, NULL)
						  ,mGrpFntColor = IIF(HM.Level = 1, MenuGroupFontColor, NULL)
						  ,smGrpBkgrdColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuGroupBackgroundColor, NULL)
						  ,smGrpFntColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuGroupFontColor, NULL)
						  ,HM.MenuName
						  ,MenuIconicFontColor mFontColor
						  ,SubMenuIconicFontColor smFontColor
						  ,IIF(@LinkID = 0,HM.DateCreated,MI.DateCreated) dateModified
						  ,TemplateBackgroundColor
					FROM HcMenuItem MI			
					INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
					INNER JOIN HCTemplate HCT on HCT.HCTemplateID = HM.HCTemplateID
					INNER JOIN HCLinkType N ON N.HcLinkTypeID = MI.HcLinkTypeID
					LEFT JOIN HcMenuCustomUI CI ON HM.HcHubCitiID = CI.HubCitiId
					LEFT JOIN HCDepartments D ON D.HcDepartmentID = MI.HcDepartmentID
					LEFT JOIN HcMenuItemType T ON T.HcMenuItemTypeID = MI.HcMenuItemTypeID
					LEFT JOIN HcMenuItemShape S ON S.HcMenuItemShapeID =MI.HcMenuItemShapeID
					LEFT JOIN #Groups G ON G.HcMenuItemID =MI.HcMenuItemID  
					WHERE  HM.HCHubcitiID=@HubCitiID AND (MI.HcMenuID=ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 AND Level=1)) 
					AND ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND MI.HcMenuItemTypeID =@TypeID )) 
					AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND MI.HcDepartmentID  =@DepartmentID))) 
					OR G.HcMenuItemID =MI.HcMenuItemID)
					)Ab

			
			SELECT @TemplateChanged = 1	
			
			--Confirmation of Success
			SELECT @Status = 0
			
			--To display message when no Retailers display for user preferred cities.
				DECLARE @MaxCnt int
				DECLARE @UserPrefCities NVarchar(MAX)

				SELECT @MaxCnt = Count(1) FROM #MenuItems

				DECLARE @Count INT
				SET @Count=(SELECT Count(LinkTypeName) from #MenuItems
							WHERE LinkTypeName = 'Label' OR LinkTypeName ='Text')

				 DECLARE @Cnt INT = 0
				 SELECT @Cnt=Count(1) FROM #MenuItems
				 
				 IF (@Cnt <= 0)
				 BEGIN
						SELECT @NoRecordsMsg = 'No Records Found.'
				 END
				 
			--SELECT * FROM #MenuItems
					
			DECLARE @TemplateName Varchar(100)
			SELECT @TemplateName=templateName  FROM #MenuItems 

			--Counts initialized for showing menu items count
			DECLARE @UCount INT
			DECLARE @CatCount INT
			DECLARE @DCount INT

			SELECT @UCount = Count(1) FROM HcUserNewsFirstSingleSideNavigation WHERE Hcuserid= @UserID AND HchubcitiID = @HubCitiID AND Flag= 2
			SELECT @DCount = COUNT(1) FROM HcDefaultNewsFirstBookmarkedCategories D
			INNER JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID
			WHERE D.HchubcitiID = @HubCitiID 
			--HcDefaultNewsFirstBookmarkedCategories D INNER JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID
			--WHERE D.HchubcitiID = @HubCitiID AND NS.HcHubCitiID=@HubCitiID
					
		IF @Ucount = 0
			SELECT @Ucount = NULL
	
		DECLARE @HcCityID VARCHAR(500)
		SELECT @HcCityID = NULL

		SELECT * INTO #tempci FROM HcRegionAppMenuItemCityAssociation  WHERE HcHubcitiID= @HubCitiID

		CREATE TABLE #city ( HccityID INT )


		--SELECT * FROM #tempci

		--SELECT @LinkID
		
			INSERT INTO #city
			SELECT  DISTINCT  uc.HcCityID 
			from #tempci  RMC
			INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND RMC.HcHubcitiId=UC.HcHubcitiId
			INNER JOIN  HcMenuItem HM on HM.HcMenuItemID= RMC.HcMenuItemID AND hM.HcMenuID = @LinkID
			INNER JOIN HcUserNewsFirstSingleSideNavigation UN  on HM.HcMenuItemID=UN.NewsSideNavigationID 
			WHERE RMC.HcHubcitiID= @HubCitiID AND UN.HchubcitiID= @HubCitiID --AND UN.HcUserID= @UserID
						 
		--SELECT * FROM #city

	SELECT @HcCityID = COALESCE(@HcCityID + ', ', '') + cast(HcCityID AS VARCHAR(500)) FROM #city

	--SELECT @HcCityID,@TypeID,@DepartmentID
							
	IF (@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0 OR (@Count=@MaxCnt)) AND (@HcCityID IS NULL AND @TypeID IS NULL AND @DepartmentID IS NULL)
	 BEGIN 
	 --SELECT 'A'
	  
		SELECT @UserPrefCities = COALESCE(@UserPrefCities+',' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName))) 
		FROM HcUsersPreferredCityAssociation P
		INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
		WHERE HcHubcitiID = @HubCitiID AND HcUserID = @UserID
						
		SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
								'.\n\nUpdate your city preferences in the settings menu.'

	 END
	ELSE IF ((@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0)  AND (@HcCityID IS NOT NULL OR @TypeID IS NOT NULL OR @DepartmentID IS NOT NULL ))
	 BEGIN
		SELECT @NoRecordsMsg = 'No Records Found.'
	 END

	ELSE IF (@RegionAppFlag = 0) AND (ISNULL(@MaxCnt,0) = 0)
	 BEGIN
		SELECT @NoRecordsMsg = 'No Records Found.'
	 END

	 --SELECT * FROM #MenuItems

	IF EXISTS( SELECT 1 FROM HcUserNewsFirstSingleSideNavigation WHERE HcUserID = @UserID AND HchubcitiID= @HubCitiID )
	 AND NOT EXISTS(select 1  from  HcUser  where username='WelcomeScanSeeGuest'  and hcuserid=@userID)
		BEGIN	
		--SELECT 'AA'
			--select @TemplateName
		-- SELECT 1
			IF @SortOrder ='ASC' AND (@TemplateName IN ('Grouped Tab','Combo Template','Grouped Tab With Image'))
			--IF @SortOrder ='ASC' AND (@TemplateName = 'Grouped Tab' OR  @TemplateName = 'Combo Template' OR  @TemplateName = 'Grouped Tab With Image')
			BEGIN	
			-- SELECT  'A'
				 SELECT  RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					  --,LinkTypeName			
					  --,LinkTypeID
					  --,LinkID 
					  ,LinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
													  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
													  WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID ) = 1 AND M.LinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																						   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																						   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																						   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
												WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
													  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							   INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID
																																							   WHERE HcMenuItemID = M.mItemID AND HcHubCitiID =@HubCitiID)
										   ELSE LinkTypeName END)
						,LinkTypeID
						,LinkID =  CASE WHEN M.LinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID) 
										  WHEN M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																			 INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
																			 WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 AND M.LinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
																																							INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
																																							WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
										  WHEN M.LinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
																			  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN (SELECT HcEventCategoryID FROM HcMenuItemEventCategoryAssociation
																																							WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID)	

									 ELSE LinkID END	
					  --,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,	GroupName 	
					  , SortD = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='DESC' THEN 'Z'+ mItemName ELSE mItemName END
					  , SortA = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='ASC' THEN ' ' ELSE mItemName END
					  , mGrpBkgrdColor 
					  , mGrpFntColor                        
					  , smGrpBkgrdColor 
					  , smGrpFntColor 
					  , MenuName
					  , mFontColor
					  , smFontColor
					  , M.dateModified	
					  , TemplateBackgroundColor AS templateBgColor 
					  --, NoOfColumns
					  , N.Flag
					  , N.SortOrder
					  --, NULL catColor
		        FROM #MenuItems M
				LEFT JOIN #Sort S ON M.mItemID=S.MenuitemsID 
				INNER JOIN HcUserNewsFirstSingleSideNavigation N ON N.NewsSideNavigationID= M.mItemID AND N.HcUserID = @UserID
				and n.flag=1 AND M.HubCitiId= @HubCitiID  and N.HchubcitiID= @HubCitiID
				WHERE @Count!=@MaxCnt AND LTRIM(RTRIM(LinkTypeName)) <> 'News'			
			    ORDER BY GroupName ,SortA ASC
						
			END
			
		   ELSE	IF @SortOrder ='DESC' AND (@TemplateName = 'Grouped Tab' OR  @TemplateName = 'Combo Template' OR  @TemplateName = 'Grouped Tab With Image')
			BEGIN	
			-- SELECT 'B'		
				 SELECT  RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					  --,LinkTypeName			
					  --,LinkTypeID
					  --,LinkID
					  ,LinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
													  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
													  WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID ) = 1 AND M.LinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																						   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																						   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																						   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
												WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
													  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							   INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID
																																							   WHERE HcMenuItemID = M.mItemID AND HcHubCitiID =@HubCitiID)
										   ELSE LinkTypeName END)
						,LinkTypeID
						,LinkID =  CASE WHEN M.LinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID) 
										  WHEN M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																			 INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
																			 WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 AND M.LinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
																																							INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
																																							WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
										  WHEN M.LinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
																			  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN (SELECT HcEventCategoryID FROM HcMenuItemEventCategoryAssociation
																																							WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID)	

									 ELSE LinkID END 
					  --,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,	GroupName 	
					  , SortD = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='DESC' THEN 'Z'+ mItemName ELSE mItemName END
					  , SortA = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='ASC' THEN ' ' ELSE mItemName END		
					  , mGrpBkgrdColor 
					  , mGrpFntColor                        
					  , smGrpBkgrdColor 
					  , smGrpFntColor 
					  , MenuName
					  , mFontColor
					  , smFontColor	
					  , M.dateModified
					  , TemplateBackgroundColor	AS templateBgColor 	                 
					  --, NoOfColumns
					  , N.Flag
					  , N.SortOrder
					 -- , NULL catColor
		        FROM #MenuItems M
				LEFT JOIN #Sort S ON M.mItemID=S.MenuitemsID  
				INNER JOIN HcUserNewsFirstSingleSideNavigation N ON N.NewsSideNavigationID= M.mItemID AND N.HcUserID = @UserID
				AND n.flag=1 AND M.HubCitiId= @HubCitiID and n.flag=1
				WHERE LTRIM(RTRIM(LinkTypeName)) <> 'News'		
				ORDER BY GroupName desc ,SortD Desc						
			END			    
						 
           ELSE
		   BEGIN
			 -- SELECT 'C'
				
				 SELECT  RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					  --,LinkTypeName	= 'a'		
					  --,LinkTypeID
					  --,LinkID = '1'
					 ,LinkTypeName = IIF((SELECT COUNT(DISTINCT A.BusinessCategoryID)  FROM HcMenuFindRetailerBusinessCategories A 
										INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
										WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 , 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																		   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																		   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																		   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID), 
								     LinkTypeName)
					,LinkTypeID
					,LinkID = IIF((M.LinkTypeName IN ('Filters','City Experience')),
											(SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID),
								IIF( ( M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																	INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
																	WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID) = 1),
										
															(SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
															INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID = A.BusinessCategoryID 
															WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID)
									 ,LinkID)) 	
					  --,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,mGrpBkgrdColor 
					  ,mGrpFntColor                        
					  ,smGrpBkgrdColor 
					  ,smGrpFntColor 
					  ,MenuName
					  ,mFontColor
					  ,smFontColor	
					  ,M.dateModified
					  ,TemplateBackgroundColor AS templateBgColor 
					  --,NoOfColumns
					  ,N.Flag 
				      ,N.SortOrder
					  --,NULL catColor						                
		        FROM #MenuItems M	
				INNER JOIN #Menuitemss  m1 on m.mItemID=m1.MenuItemID
				INNER JOIN HcUserNewsFirstSingleSideNavigation N ON N.NewsSideNavigationID= M.mItemID AND N.HcUserID = @UserID
				and M.HubCitiId= @HubCitiID  AND n.flag=1
				WHERE LTRIM(RTRIM(LinkTypeName)) <> 'News'	

			END	
	ENd
   ELSE 
    BEGIN
	 --SELECT 'BB'
		SELECT  RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					 ,LinkTypeName = IIF((SELECT COUNT(DISTINCT A.BusinessCategoryID)  FROM HcMenuFindRetailerBusinessCategories A 
										INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
										WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 , 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																		   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																		   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																		   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID), 
								     LinkTypeName)
					,LinkTypeID
					,LinkID = IIF((M.LinkTypeName IN ('Filters','City Experience')),
											(SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID),
								IIF( ( M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																	INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
																	WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID) = 1),
										
															(SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
															INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID = A.BusinessCategoryID 
															WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID)
									 ,LinkID)) 	
					  --,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,mGrpBkgrdColor 
					  ,mGrpFntColor                        
					  ,smGrpBkgrdColor 
					  ,smGrpFntColor 
					  ,MenuName
					  ,mFontColor
					  ,smFontColor	
					  ,M.dateModified
					  ,TemplateBackgroundColor AS templateBgColor 
					  ,1 Flag
					  ,SortOrder =  M.Position  -- case when @UserID = 10182 then 4+ M.Position else END
					  --,NULL catColor					                
		        FROM #MenuItems M
				INNER JOIN #Menuitemss  m1 on m.mItemID=m1.MenuItemID
				WHERE @Count!=@MaxCnt AND LTRIM(RTRIM(LinkTypeName)) <> 'News'	 
		END

			
		--SELECT @UCount,@DCount
										
		EXEC [HubCitiApp2_8_3].[usp_HcNewsCategoryDisplay] @UserID,@HubCitiID, null,null,null,null,null
			
		----------------Two Image Template Changes 3/2/2106---------------------
										
		SELECT  @TempleteBackgroundImage= @config+ CAST(H.HCHubcitiID AS VARCHAR(100))+'/'+CAST(H.TemplateBackgroundImage AS VARCHAR(100)) --@config
			    ,@DisplayLabel=H.DisplayLabel
				,@LabelBckGndColor = H.LabelBackGroundColor
				,@LabelFontColor = H.LabelFontColor
		FROM HcMenu H
		WHERE HcHubCitiID=@HubCitiID AND (@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =h.HcMenuID )) 
		---------------------------------END--------------------------
		------------- Custom Navigation Bar changes -------------------------------
			
		SELECT	@homeImgPath = 	@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName 
					,@bkImgPath =  @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName 
				,@titleBkGrdColor =   HU.backGroundColor 
					,@titleTxtColor =  HU.titleColor 
			FROM HcHubCiti HC
			LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
			LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
			WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					 
			
		-------------------------------------------------------------------------	 
				 	      
	  END

	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiAddNewsFirstMenuDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
