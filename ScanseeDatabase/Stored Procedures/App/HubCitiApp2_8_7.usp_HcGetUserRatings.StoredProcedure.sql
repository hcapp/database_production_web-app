USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcGetUserRatings]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_HcGetUserRatings]  
Purpose				  : Retrieve Average User Rating for product in context 
Example				  : EXEC [HubCitiApp2_1].[usp_HcGetUserRatings] ''  
  
History  
Version   Date          Author                Change Description  
---------------------------------------------------------------  
1.0      30thOct2013   SPAN Infotech India   Initial Version  
---------------------------------------------------------------   
*/ 

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcGetUserRatings]
(
	 --Input Variable
	  @prProductID int
	, @prUserID int
	, @HcHubCitiID int
	
	--Output Variable 
	, @ErrorNumber	int output
	, @ErrorMessage	varchar(1000) output 	
)	
AS

BEGIN
	
	BEGIN TRY
		
		DECLARE @ProductID int, 
				@UserID	int, 
				@Counter bit 
		
		-- Assign parameter value to a local variable
		SELECT  @ProductID = @prProductID,								
				@UserID    = @prUserID
				
		-- If User Rating already exists in the database
		
		IF Exists (SELECT 1 FROM HcUserRating WHERE ProductID = @ProductID And UserID = @UserID)
			BEGIN			
				-- Retrive average user rating for a product
				SELECT ProductId, AVG(Isnull(Rating,0)) AS AverageRating, COUNT(UserID) AS noOfUsersRated
				INTO #Avgrating 
				FROM HcUserRating 
				WHERE ProductID = @ProductID	
				GROUP BY ProductID
			
				SELECT ProductId
					  , Isnull(Rating,0) AS UserRating
				INTO #userrating
				FROM HcUserRating 
				WHERE ProductID = @ProductID
				AND UserID = @UserID
				
				SELECT A.ProductID
						, AverageRating avgRating
						, UserRating currentRating
						, noOfUsersRated
				FROM #Avgrating A
				INNER JOIN #userrating U ON A.ProductID=U.ProductID
			END	
					
		IF EXISTS (SELECT 1 FROM HcUserRating WHERE ProductID = @ProductID )
			-- Retrive average user rating for a product
			BEGIN			
				SELECT ProductId, AVG(Isnull(Rating,0)) AS avgRating
						, 0 AS currentRating
					    , COUNT(UserID) AS noOfUsersRated
				FROM HcUserRating 
				WHERE ProductID = @ProductID	
				GROUP BY ProductID
			END		
		ELSE
			-- If User Rating Does NOT exists in the database
			BEGIN
				SELECT ProductId, AVG(Isnull(Rating,0)) As  avgRating
					  , 0 As UserRating
					  , 0 AS noOfUsersRated
				FROM HcUserRating 
				WHERE ProductID = @ProductID	
				GROUP BY ProductID		
			END
		
	END TRY

BEGIN CATCH

	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		PRINT 'Error occured in Stored Procedure usp_HcGetUserRatings.'		
		--- Execute retrieval of Error info.
		EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT 
	END;
	 
END CATCH;
	
END















































GO
