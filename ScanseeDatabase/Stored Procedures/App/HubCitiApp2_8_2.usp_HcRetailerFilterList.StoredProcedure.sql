USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailerFilterList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_HcRetailerFilterList]
Purpose                  : To display List of Retailer Filters.
Example                  : [usp_HcRetailerFilterList]

History
Version    Date            Author          Change Description
------------------------------------------------------------------------------- 
1.0      5/13/2015         Span                1.1  
2.0      12/2/2016      Shilpashree     Re-written code for Optimization.          
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailerFilterList]
(
      --Input Parameters       
        @UserID INT
	  , @BusinessCategoryID INT
	  , @HcHubCitiID INT
	  , @Latitude Decimal(18,6)
	  , @Longitude Decimal(18,6)
	  , @Radius int
	  , @HcCityID Varchar(2000)
      , @HcMenuItemId Int 
	  , @HcBottomButtonId Int
	  , @SearchKey Varchar(200)
      
      --Output Parameters	 
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN
      BEGIN TRY
			SET NOCOUNT ON

	DECLARE @RegionAppFlag Bit
			, @PostalCode varchar(10)
			, @NearestPostalCode varchar(50)
			, @UserConfiguredPostalCode varchar(50)
			, @DistanceFromUser Float
			, @UserLatitude decimal(18,6)
			, @UserLongitude decimal(18,6)
			, @UserOutOfRange bit   
			, @DefaultPostalCode varchar(50)  
			, @Status bit
			, @Length int
			, @IsSubCategory bit = 0

		DECLARE @UserID1 INT = @UserID,
				@HcHubCitiID1 INT = @HcHubCitiID,
				@HcMenuItemID1 INT = @HcMenuItemID,
				@HcBottomButtonID1 INT = @HcBottomButtonID
		
		SELECT @UserConfiguredPostalCode = PostalCode FROM HcUser WHERE HcUserID = @UserID1		
		SET @UserLatitude = @Latitude
		SET @UserLongitude = @Longitude

		SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
							WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
							WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
							ELSE @SearchKey END)

		CREATE TABLE #SubCategoryList(BusinessCategoryID int,SubCategoryID int)
		CREATE TABLE #HcRetailerSubCategory(RetailID int,RetailLocationID int,BusinessCategoryID int,HcBusinessSubCategoryID int)
		CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
		CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100),HcHubCitiID int)
		CREATE TABLE #RetailItemsonSale1(Retailid bigint , RetailLocationid bigint)
        CREATE TABLE #Retail(RetailID INT 
                            , RetailName VARCHAR(1000)
                            , RetailLocationID INT
                            , City VARCHAR(1000)    
                            , PostalCode VARCHAR(20)
                            , retLatitude FLOAT
                            , retLongitude FLOAT
                            , Distance FLOAT  
                            , DistanceActual FLOAT                                                   
							, SaleFlag BIT)	
							
		SELECT @IsSubCategory = 1
			FROM HcBusinessSubCategoryType T
			INNER JOIN HcBusinessSubCategory S ON T.HcBusinessSubCategoryTypeID = S.HcBusinessSubCategoryTypeID
				WHERE T.BusinessCategoryID = @BusinessCategoryID
				
		SELECT DISTINCT RBC.*,R.RetailName 
		INTO #RetailerBusinessCategory 
			FROM RetailerBusinessCategory RBC
			INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1 
				WHERE BusinessCategoryID=@BusinessCategoryID
	
		IF (@IsSubCategory = 1)
		BEGIN
			
			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcMenuFindRetailerBusinessCategories 
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcMenuItemID=@HcMenuItemID

			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcBottomButtonFindRetailerBusinessCategories  
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcBottomButonID=@HcBottomButtonID

			INSERT INTO #HcRetailerSubCategory(RetailID,RetailLocationID,BusinessCategoryID,HcBusinessSubCategoryID)
			SELECT RS.RetailID,RS.RetailLocationID,RS.BusinessCategoryID,RS.HcBusinessSubCategoryID
			FROM HcRetailerSubCategory RS
			INNER JOIN #SubCategoryList SL ON RS.BusinessCategoryID = SL.BusinessCategoryID AND RS.HcBusinessSubCategoryID = SL.SubCategoryID
			
		END	
									
		IF(SELECT 1 FROM HcHubCiti H
                    INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                    AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
        BEGIN
				SET @RegionAppFlag = 1
									
				INSERT INTO #RHubcitiList(HcHubCitiID)
				(SELECT DISTINCT HcHubCitiID 
				FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
				UNION ALL
				SELECT  @HCHubCitiID1 AS HcHubCitiID
				)
				
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)		
				SELECT DISTINCT LA.HcCityID 
								,LA.City	
								,LA.PostalCode
								,H.HcHubCitiID	                         
				FROM #RHubcitiList  H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID 
				LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =LA.HcCityID
				LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID	AND UP.HcHubcitiID =H.HchubcitiID 							        
				WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
				OR (@HcCityID IS NOT NULL AND LA.HcCityID =S.Param )
				OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND LA.HcCityID =UP.HcCityID AND UP.HcUserID =@UserID)						  
				OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID =@UserID)
        END
		ELSE
        BEGIN
				SET @RegionAppFlag =0

				INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID1 
				 
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)
				SELECT DISTINCT HcCityID 
								,City
								,PostalCode	
								,HcHubCitiID				
				FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID1                                   
		END
					
		SELECT @UserLatitude = @Latitude
			 , @UserLongitude = @Longitude

		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
		END
         
		--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID1
		END

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
		EXEC [HubCitiApp2_8_2].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFROMUser OUTPUT
		--select @DefaultPostalCode = defaultpostalcode from hchubciti where HcHubCitiid = @HcHubCitiID  and @PostalCode is null
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
		BEGIN
			SELECT @Latitude = Latitude
					, @Longitude = Longitude
			FROM GeoPosition 
			WHERE PostalCode = @PostalCode                                                                             
		END  
					
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL
                   
		--Get the user preferred radius.
		SELECT @Radius = LocaleRadius
        FROM HcUserPreference 
        WHERE HcUserID = @UserID1
                                         
        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
											WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 

			
		SELECT DISTINCT RBC.RetailerID 
				, RBC.RetailName    
				, RL.RetailLocationID 
				, RL.City
				, RL.PostalCode
				, RetailLocationLatitude
				, RetailLocationLongitude
				, SaleFlag = 0 
				, HL.HchubcitiID 
		INTO #RetailInfo1
		FROM #RetailerBusinessCategory RBC
		INNER JOIN RetailLocation RL ON RBC.RetailerID = RL.RetailID AND RL.Active = 1
		INNER JOIN #CityList HL ON RL.PostalCode=HL.PostalCode AND RL.HcCityID = HL.CityID AND HL.HcHubCitiID=@HCHubCitiID1 
		INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
		INNER JOIN HcRetailerAssociation RA ON RL.RetailID = RA.RetailID AND RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = RH.HchubcitiID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
		
		SELECT * INTO #RetailInfo FROM #RetailInfo1 WHERE 1=2

		IF(@IsSubCategory = 1)
		BEGIN
				INSERT INTO #RetailInfo
				SELECT R.* 
				FROM #RetailInfo1 R
				INNER JOIN #HcRetailerSubCategory RS ON R.RetailLocationID = RS.RetailLocationID
		END
		ELSE IF(@IsSubCategory = 0)
		BEGIN
				INSERT INTO #RetailInfo
				SELECT R.* 
				FROM #RetailInfo1 R
		END

		INSERT INTO #Retail 
        SELECT  *
		FROM 
		(SELECT DISTINCT RL.RetailerID 
					, RL.RetailName    
					, RL.RetailLocationID 
					, RL.City
					, RL.PostalCode
					, RL.RetailLocationLatitude  retLatitude
					, RL.RetailLocationLongitude retLongitude
					, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					, DistanceActual = 0
					--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
					, SaleFlag = 0   
		FROM #RetailInfo RL
		LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode AND G.City = RL.City 
		LEFT JOIN RetailerKeywords RK ON RL.RetailerID = RK.RetailID                                 
        WHERE (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
		) Retailer
        WHERE Distance <= @Radius 

		SELECT DISTINCT  AdminFilterID  filterId
					,FilterName filterName
		FROM
		(
			-- To display List of Filter Values Associated to Hubciti.
			SELECT DISTINCT AF.AdminFilterID 
						,AF.FilterName
			FROM #Retail R		
			INNER JOIN RetailerFilterAssociation RF ON R.RetailLocationID= RF.RetailLocationID AND R.RetailID = RF.RetailID
						AND RF.BusinessCategoryID = @BusinessCategoryID
			INNER JOIN AdminFilter AF ON AF.AdminFilterID = RF.AdminFilterID 
			INNER JOIN AdminFilterCategoryAssociation ACA ON AF.AdminFilterID = ACA.AdminFilterID AND ACA.BusinessCategoryID = @BusinessCategoryID
			LEFT JOIN AdminFilterValueAssociation AFV ON AF.AdminFilterID = AFV.AdminFilterID 	
				WHERE AFV.AdminFilterValueID IS NULL 
		)A
		ORDER BY FilterName

      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure [usp_HcRetailerFilterList].'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                                
            END;
            
      END CATCH;
END;






GO
