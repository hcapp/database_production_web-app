USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcPreferredLocationCategoryDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcPreferredLocationCategoryDisplay
Purpose					: To dispaly User preferred Location Categories.
Example					: usp_HcPreferredLocationCategoryDisplay

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			2/26/2015       SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcPreferredLocationCategoryDisplay]
(
	 --Input variable
	  @HcUserID int
	, @HubCitiID int
		
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @NewUser bit = 0
			IF EXISTS (SELECT 1 FROM HcUserPreferredCategory WHERE HcUserID = @HcUserID AND HcHubCitiID IS NULL)
			BEGIN
				SELECT @NewUser = 1 	
			END

			--To fetch Business categories and sub categories for given user
			SELECT DISTINCT RowNumber = IDENTITY (INT,1,1)
							, BC.BusinessCategoryID AS catId
							, BC.BusinessCategoryDisplayValue AS catName
							, ISNULL(SC.HcBusinessSubCategoryID,BC.BusinessCategoryID) AS subCatId
							, ISNULL(SC.BusinessSubCategoryName,'All') AS subCatName
							, isAdded = CASE WHEN PC.BusinessCategoryID IS NOT NULL THEN 1 
											 WHEN @NewUser = 1 THEN 1 
										     ELSE 0 END
			INTo #Temp
			FROM BusinessCategory BC
			LEFT JOIN HcBusinessSubCategoryType SCT ON BC.BusinessCategoryID = SCT.BusinessCategoryID
			LEFT JOIN HcBusinessSubCategory SC ON SCT.HcBusinessSubCategoryTypeID = SC.HcBusinessSubCategoryTypeID
			LEFT JOIN HcUserPreferredCategory PC ON BC.BusinessCategoryID = PC.BusinessCategoryID AND PC.HcUserID = @HcUserID AND PC.HcHubCitiID = @HubCitiID
					  AND (SC.HcBusinessSubCategoryID = PC.HcBusinessSubCategoryID OR SC.HcBusinessSubCategoryID IS NULL)
			WHERE Active = 1

			SELECT RowNumber
				   ,catId
				   ,catName
				   ,subCatId
				   ,subCatName
				   ,isAdded
			FROM #Temp
			ORDER BY catName,subCatName
		 	
		
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcPreferredLocationCategoryDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




























GO
