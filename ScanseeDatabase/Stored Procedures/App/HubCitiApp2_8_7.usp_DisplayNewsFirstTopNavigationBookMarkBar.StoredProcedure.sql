USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_DisplayNewsFirstTopNavigationBookMarkBar]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_DisplayNewsFirstTopNavigationBookMarkBar]
Purpose					: To display and insert list of News categories on a BookmarkBar.
Example					: [usp_DisplayNewsFirstTopNavigationBookMarkBar]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			26 May 2016	     Bindu T A			[usp_DisplayNewsFirstTopNavigationBookMarkBar]
----------------------------------------------------------------------------------------------
*/

--exec [HubCitiapp2_8_7].[usp_DisplayNewsFirstTopNavigationBookMarkBar] 2231,12306,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_DisplayNewsFirstTopNavigationBookMarkBar]
(   
	  @LinkID INT,
	  @LevelID INT,
	  @HubcitiID INT 
	 ,@HcuserID INT
	 ,@BookMark Varchar(100) output
	 ,@Section Varchar(100) output
     ,@Status int output
	 ,@ErrorNumber int output
	 ,@ErrorMessage varchar(1000) output 

	--Output Variable 
	, @bannerImg VARCHAR(1000) Output
	, @weatherurl VARCHAR(1000) Output
	, @homeImgPath VARCHAR(1000) Output
	, @bkImgPath VARCHAR(1000) Output
	, @HamburgerImagePath VARCHAR(1000) Output
	, @titleBkGrdColor  VARCHAR(1000) Output

)
AS
BEGIN

	BEGIN TRY


------------- Custom Navigation Bar changes -----START--------------------------
		DECLARE @DefaultHamburgerIcon VARCHAR(1000)
			   ,@NewsImage varchar(max)
			   ,@ServerConfig VARCHAR(500)
			   ,@Config varchar(1000)

		select @DefaultHamburgerIcon = ScreenContent +  'images/hamburger.png' 
		from AppConfiguration 
		where ScreenName = 'Base_URL'
		
		SELECT @NewsImage = ScreenContent + CONVERT(VARCHAR,@HubCitiID)+ '/'
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT    @bannerImg = @NewsImage + NewsBannerImage
				, @weatherurl =  WeatherURL 		
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID 


		SELECT @Config = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT @ServerConfig = ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType= 'Hubciti Media Server Configuration'
			
		SELECT	     @homeImgPath = 	CASE WHEN HU.homeIconName IS NULL THEN @ServerConfig+'customhome.png' ELSE  @Config +CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName END
					,@bkImgPath = 	CASE WHEN HU.backButtonIconName IS NULL THEN @ServerConfig+'customback.png' ELSE @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName END
					,@titleBkGrdColor =  ISNULL( HU.backGroundColor ,'#000000')
					,@HamburgerImagePath = ISNULL(@NewsImage + NewsHamburgerIcon,@DefaultHamburgerIcon)
		FROM HcHubCiti HC
		LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
		LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
		WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)				 
-------------------- Custom Navigation Bar changes - END----------------------------------------------------------------------------------------

--------------------------------------------------------------To display bookmarks--------------------------------------------------------------------------------------------------------------------
		CREATE TABLE #Temp(parCatId INT,parCatName varchar(1000),subCatID BIT,subCatName varchar(1000),isAdded BIT,sortOrder int,isBkMark BIT,catColor varchar(1000),isSubCategory BIT,nonfeedlink varchar(1000))

		IF EXISTS (SELECT 1 FROM HcUserNewsFirstBookmarkedCategories WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID) AND NOT EXISTS(select 1 from HcUser where username='WelcomeScanSeeGuest' and hcuserid=@HcuserID)
		BEGIN 

			--SELECT 'a'	
			INSERT INTO #Temp
			--TO DISPLAY USER BOOKMARKED FEED NEWS CATEGORIES
			SELECT DISTINCT cat.NewscategoryID parCatId
						  , NewscategoryDisplayValue parCatName
						  , 1 subCatID
						  , 'All' subCatName
						  , Isbookmark isAdded
						  , cat.Sortorder sortOrder
						  , 1 isBkMark
						  , nS.NewsCategoryColor catColor
						  , 0 isSubCategory
						  , null nonfeedlink 
						 
			FROM HcUserNewsFirstBookmarkedCategories  cat 
			INNER join newsfirstsettings ns on ns.NewsCategoryID=cat.NewscategoryID and ns.HcHubcitiId=@HubcitiID
			inner join NewsFirstCatSubCatAssociation aa on aa.CategoryID = ns.NewsCategoryID and aa.HchubcitiID = @HubcitiID
			WHERE cat.HcUserID=@HcuserID AND cat.HcHubcitiId=@HubcitiID   

			UNION ALL

			--TO DISPLAY USER BOOKMARKED NON-FEED NEWS CATEGORIES
			SELECT DISTINCT null parCatId
					      , cat.NewsLinkCategoryName parCatName
						  , 1 subCatID
						  , 'All' subCatName
						  , Isbookmark isAdded
						  , cat.Sortorder sortOrder
						  , 1 isBkMark
						  , nS.NewsCategoryColor catColor
						  , 0 isSubCategory
						  , NewsFeedURL nonfeedlink 
						 
			FROM HcUserNewsFirstBookmarkedCategories  cat 
			INNER join newsfirstsettings ns on ns.NewsLinkCategoryName=cat.NewsLinkCategoryName 
			WHERE cat.HcUserID=@HcuserID AND ns.HcHubcitiId=@HubcitiID AND ns.IsNewsFeed = 0
			ORDER BY isAdded,sortOrder


			INSERT INTO #Temp
			--TO DISPLAY ALL FEED NEWS CATEGORIES
			SELECT  DISTINCT cat.NewscategoryID parCatId
						   , cat.NewscategoryDisplayValue parCatName
						   , 1 subCatID
						   , 'All' subCatName
			               , 0 isAdded
						   , ns.Sortorder sortOrder
			               , case when hcus.NewsCategoryID is null then '0' else '1' end isBkMark 
						   , NS.NewsCategoryColor catColor
						   , 0 isSubCategory
						   , null nonfeedlink 
						  
			FROM NewsFirstCategory cat  
			INNER JOIN newsfirstsettings NS on NS.NewsCategoryID=cat.NewscategoryID and NS.HcHubcitiId=@HubcitiID
			inner join NewsFirstCatSubCatAssociation aa on aa.CategoryID = ns.NewsCategoryID and aa.HchubcitiID = @HubcitiID
			LEFT JOIN HcUserNewsFirstBookmarkedCategories hcus on hcus.NewsCategoryID=cat.NewsCategoryID AND hcus.HcUserID=@HcuserID AND hcus.HchubcitiID=ns.HcHubCitiID 
			WHERE cat.Active = 1  AND ns.HcHubCitiID = @HubcitiID  

			UNION 

			--TO DISPLAY ALL NON-FEED NEWS CATEGORIES
			SELECT  DISTINCT cat.NewscategoryID parCatId
						   , cat.NewsLinkCategoryName parCatName
						   , 1 subCatID
						   , 'All' subCatName
						   , 0 isAdded
						   , cat.Sortorder sortOrder
						   , case when hcus.NewsLinkCategoryName is null then '0' else '1' end isBkMark 
						   , cat.NewsCategoryColor catColor
						   , 0 isSubCategory
						   , NewsFeedURL nonfeedlink 
						  
			FROM newsfirstsettings cat 
			left join HcUserNewsFirstBookmarkedCategories hcus on hcus.NewsLinkCategoryName=cat.NewsLinkCategoryName AND hcus.HcUserID=@HcuserID AND hcus.HchubcitiID=cat.HcHubCitiID 
			WHERE cat.IsNewsFeed = 0 AND cat.HcHubCitiID= @HubcitiID
			ORDER BY isAdded,sortOrder
		
		 END
		 ELSE 
		 BEGIN

			INSERT INTO #Temp
			--TO DISPLAY DEFAULT BOOKMARK FEED NEWS CATEGORIES		
			SELECT DISTINCT def.NewscategoryID parCatId
						  , ns.NewscategoryDisplayValue parCatName
						  , 1 subCatID
						  , 'All' subCatName
						  , 1 isAdded
						  , n.Sortorder sortOrder
						  , 1 isBkMark
						  , n.newsCategoryColor catColor
						  , 0 isSubCategory
						  , null nonfeedlink 
						 
			FROM HcDefaultNewsFirstBookmarkedCategories def 
			INNER JOIN NewsFirstCategory ns on ns.NewsCategoryID=def.NewscategoryID 
			INNER JOIN newsfirstsettings n on n.NewsCategoryID=def.NewscategoryID 
			inner join NewsFirstCatSubCatAssociation aa on aa.CategoryID = ns.NewsCategoryID and aa.HchubcitiID = @HubcitiID
			WHERE n.HcHubcitiId=@HubcitiID AND ns.Active = 1 and DefaultCategory = 1 
			
			UNION ALL

			--TO DISPLAY DEFAULT BOOKMARK NON-FEED NEWS CATEGORIES			
			SELECT DISTINCT def.NewscategoryID parCatId
						  , n.NewsLinkCategoryName parCatName
			              , NULL subCatID
						  , NULL subCatName
			              , 1 isAdded
						  , n.Sortorder sortOrder
						  , 1 isBkMark
						  , n.newsCategoryColor catColor
						  , 0 isSubCategory
						  , n.NewsFeedURL nonfeedlink
						 
			FROM HcDefaultNewsFirstBookmarkedCategories  def 
			INNER join newsfirstsettings n on n.NewsLinkCategoryName=def.NewsLinkCategoryName and n.HcHubCitiID = def.HchubcitiID
			where n.HcHubcitiId=@HubcitiID and n.IsNewsFeed = 0 and DefaultCategory = 1 and def.HchubcitiID = @HubcitiID
			ORDER BY isAdded,n.sortOrder

			INSERT INTO #Temp
			--TO DISPLAY ALL DEFAULT FEED NEWS CATEGORIES
			SELECT Distinct cat.NewscategoryID parCatId
						  , cat.NewscategoryDisplayValue parCatName
						  , 1 subCatID
						  , 'All' subCatName
						  , 0 isAdded
						  , ns.Sortorder sortOrder 
						  , isBkMark = case when def.NewsCategoryID is null then '0' else '1' end  
						  , ns.NewsCategoryColor catColor
						  , 0 isSubCategory
						  , null nonfeedlink 
						 
			FROM NewsFirstCategory cat 
			INNER join newsfirstsettings ns on ns.NewsCategoryID=cat.NewscategoryID  AND Ns.HcHubCitiID = @HubcitiID
			inner join NewsFirstCatSubCatAssociation aa on aa.CategoryID = ns.NewsCategoryID and aa.HchubcitiID = @HubcitiID
			left join HcDefaultNewsFirstBookmarkedCategories def on def.NewscategoryID=cat.NewsCategoryID AND def.HchubcitiID = ns.HcHubCitiID 
			WHERE ns.HcHubCitiID = @HubcitiID and cat.active =1 

			UNION ALL

			--TO DISPLAY ALL DEFAULT NON-FEED NEWS CATEGORIES
			SELECT Distinct cat.NewscategoryID parCatId
						  , cat.NewsLinkCategoryName parCatName
			              , NULL subCatID
						  , NULL subCatName
						  , 0 isAdded
						  , cat.Sortorder sortOrder 
						  , isBkMark = case when def.NewsCategoryID is null then '0' else '1' end  
						  , cat.NewsCategoryColor catColor
						  , 0 isSubCategory
						  , cat.NewsFeedURL nonfeedlink 
						 
			FROM newsfirstsettings cat 
			left JOIN HcDefaultNewsFirstBookmarkedCategories def on def.NewsLinkCategoryName=cat.NewsLinkCategoryName AND def.HchubcitiID = cat.HcHubCitiID
			WHERE cat.IsNewsFeed = 0 and cat.HcHubCitiID = @HubcitiID  AND def.HcDefaultBookmarkedCategoriesID IS NULL
			ORDER BY isAdded,sortOrder
			
		 END

		 --To display Categories
		 SELECT * 
		 FROM #Temp
		 
		--Header information
		SELECT @BookMark = 'Favorites' ,@Section= 'Other Sections'

		--Confirmation of Success
		SELECT @Status = 0	      	
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		throw;
	
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



















GO
