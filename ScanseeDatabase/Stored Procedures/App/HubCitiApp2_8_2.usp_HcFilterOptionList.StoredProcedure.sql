USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcFilterOptionList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFilterOptionList]
Purpose					: To display Filter Category List.
Example					: [usp_HcFilterOptionList]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18 May 2015	    Mohith H R	    1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcFilterOptionList]
(
    --Input variable.
       @UserID int 
     , @HCHubCitiID Int     
	 , @FilterID int
	 --, @CategoryID Varchar(100)  --Comma separated CategoryIDs
	 , @SearchKey varchar(255)
	 --, @LowerLimit int  
	 --, @ScreenName varchar(50)
	 , @Latitude decimal(18,6)    
	 , @Longitude decimal(18,6)    
	 , @ZipCode varchar(10)	 
	 --, @SortColumn Varchar(200)
	 --, @SortOrder Varchar(100)
	
	 
	 --Inputs for User Tracking
	 --, @MainMenuId int
	 
	   
	 --OutPut Variable	
	 , @Status int output
	 , @ErrorNumber int output  
	 , @ErrorMessage varchar(1000) output  
  
)
AS
BEGIN

	BEGIN TRY

	        DECLARE @UserOutOfRange bit 
			DECLARE @DefaultPostalCode VARCHAR(10) 
			
			DECLARE @RegionAppID int
			DECLARE @HcAppListID int

			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiID = @HCHubCitiID

			DECLARE @Config VARCHAR(100)
			DECLARE @RetailAffiliateCount INT
			DECLARE @CityExperienceID INT
			DECLARE @DistanceFromUser FLOAT
			--DECLARE @Latitude decimal(18,6)    
	  --      DECLARE @Longitude decimal(18,6)    
	  --      DECLARE @ZipCode varchar(10) 
			
			DECLARE @Globalimage varchar(50)
			DECLARE @CityExpDefaultConfig varchar(50)
			DECLARE @ModuleName varchar(20) = 'Filters'
			
			SELECT @Globalimage =ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType ='Image Not Found'
	        
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
			
			SELECT @CityExpDefaultConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'City Experience Default Image Path'
			AND Active = 1
			
			DECLARE @RetailConfig varchar(50)
			SELECT @RetailConfig=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Web Retailer Media Server Configuration'
			
			--To get the row count for pagination.  
			--DECLARE @UpperLimit int   
			--SELECT @UpperLimit = @LowerLimit + ScreenContent   
			--FROM AppConfiguration   
			--WHERE ScreenName = @ScreenName 
			--AND ConfigurationType = 'Pagination'
			--AND Active = 1   
			
			DECLARE @UserLatitude float
			DECLARE @UserLongitude float 

			SELECT @UserLatitude = @Latitude
				 , @UserLongitude = @Longitude

			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
					 , @UserLongitude = Longitude
				FROM HcUser A
				INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				WHERE HcUserID = @UserID 
			END	
			--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID
			END


			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HCHubCitiID, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
			
			
			SELECT @CityExperienceID = HcCityExperienceID
			FROM HcFilter 
			WHERE HcFilterID = @FilterID
			
			-- To identify Retailer that have products on Sale or any type of discount
			SELECT DISTINCT Retailid , RetailLocationid
			INTO #RetailItemsonSale
			FROM 
				(SELECT b.RetailID, a.RetailLocationID 
				FROM RetailLocationDeal a 
				INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID AND b.Active = 1 
				INNER JOIN RetailLocationProduct c ON a.RetailLocationID = c.RetailLocationID 			
												   AND a.ProductID = c.ProductID
												   AND GETDATE() BETWEEN isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =b.PostalCode AND HL.City=b.City AND HL.State=b.State AND HL.HcHubCitiID =@HcHubcitiID  
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				UNION 
				SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
				FROM Coupon C 
				INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				INNER JOIN RetailLocation RL ON RL.RetailID =CR.RetailID AND RL.Active = 1  
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.City=RL.City AND HL.State=RL.State AND HL.HcHubCitiID =@HcHubcitiID  
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				GROUP BY C.CouponID
						,NoOfCouponsToIssue
						,CR.RetailID
						,CR.RetailLocationID
				HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
						 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)								   
				
				UNION 
				SELECT  RR.RetailID, 0 AS RetaillocationID  
				FROM Rebate R 
				INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
				INNER JOIN RetailLocation RL ON RL.RetailID =RR.RetailID AND RL.Active = 1  
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.City=RL.City AND HL.State=RL.State AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
				
				UNION 
				SELECT  c.retailid, a.RetailLocationID 
				FROM  LoyaltyDeal a
				INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
				INNER JOIN RetailLocation c ON a.RetailLocationID = c.RetailLocationID AND c.Active = 1 
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =c.PostalCode AND HL.City=c.City AND HL.State=c.State AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID
				INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID AND a.ProductID = LDP.ProductID 
				WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
				
				UNION
				SELECT DISTINCT rl.RetailID, rl.RetailLocationID
				FROM ProductHotDeal p
				INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
				INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active = 1 
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.City=RL.City AND HL.State=RL.State AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
				LEFT JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
				GROUP BY P.ProductHotDealID
						,NoOfHotDealsToIssue
						,rl.RetailID
						,rl.RetailLocationID
				HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
						 ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)  
				
				UNION
				SELECT q.RetailID, qa.RetailLocationID
				FROM QRRetailerCustomPage q
				INNER JOIN QRRetailerCustomPageAssociation qa ON qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID =qa.RetailLocationID AND RL.Active = 1 
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.City=RL.City AND HL.State=RL.State AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				INNER JOIN QRTypes qt ON qt.QRTypeID = q.QRTypeID AND qt.QRTypeName = 'Special Offer Page'
				WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') AND isnull(q.enddate,GETDATE()+1)) a			
									

		    --To get the image path of the given group.	
			--SELECT @RetailGroupButtonImagePath = @Config + ISNULL(ButtonImagePath, @CityExpDefaultConfig)
			--FROM HcCityExperience
			--WHERE HcCityExperienceID = @CityExperienceID
			
			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
				IF @ZipCode IS NULL
				BEGIN
					SELECT @Latitude = G.Latitude
						 , @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID
				END
				ELSE
				BEGIN
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude
					FROM GeoPosition 
					WHERE PostalCode = @ZipCode
				END
			END
			  
			
				--To get the image path of the given group.	
			--SELECT @RetailGroupButtonImagePath = @Config + ISNULL(ButtonImagePath, @CityExpDefaultConfig)
			--FROM HcFilter HCF
			--INNER JOIN HcFilterRetailLocation RA ON RA.HcFilterID = HCF.HcFilterID
			--WHERE RA.HcFilterID = @FilterID
			
			DECLARE @UserPreferredCity bit 
			SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID AND HcCityID IS NULL THEN 0
											WHEN HcUserID = @UserID AND HcCityID IS NOT NULL THEN 1
											ELSE 0 END
			FROM HcUsersPreferredCityAssociation
			WHERE HcHubcitiID = @HCHubCitiID AND HcUserID = @UserID

			SELECT @UserPreferredCity = ISNULL(@UserPreferredCity,0)

			--Filter the Retailers based on the given Affiliate.						
								SELECT	  BusinessCategoryID
										, BusinessCategoryName	
										, SaleFlag																						
								INTO #Retail
							FROM 
							(SELECT DISTINCT  B.BusinessCategoryID
											, B.BusinessCategoryName 	
											, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
							FROM Retailer R    
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0   
							INNER JOIN HcLocationAssociation HLA ON HLA.HcHubCitiID = @HCHubCitiID AND HLA.PostalCode = RL.PostalCode  
							INNER JOIN HcCity C ON HLA.City = C.CityName
							INNER JOIN HcFilterRetailLocation HL ON HL.HcFilterID=@FilterID AND HL.RetailLocationID=RL.RetailLocationID 
							INNER JOIN RetailerBusinessCategory RB ON R.RetailID =RB.RetailerID 
							INNER JOIN BusinessCategory B ON RB.BusinessCategoryID = B.BusinessCategoryID		                
							LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode 
							WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND @UserPreferredCity = 0 AND RL.Active = 1 AND R.RetailerActive = 1
							--AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
							--	OR
							--	(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ','))))
							--AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+ @SearchKey +'%' END

							UNION ALL

							SELECT DISTINCT   B.BusinessCategoryID
											 ,B.BusinessCategoryName
											 ,SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
							FROM Retailer R    
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0   
							INNER JOIN HcLocationAssociation HLA ON  HLA.HcHubCitiID = @HCHubCitiID AND HLA.PostalCode = RL.PostalCode  
							INNER JOIN HcCity C ON HLA.City = C.CityName
						    INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HcHubcitiID
							INNER JOIN HcFilterRetailLocation HL ON HL.HcFilterID=@FilterID AND HL.RetailLocationID=RL.RetailLocationID 
							INNER JOIN RetailerBusinessCategory RB ON R.RetailID =RB.RetailerID 
							INNER JOIN BusinessCategory B ON RB.BusinessCategoryID = B.BusinessCategoryID		                
							LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode 
							WHERE @RegionAppID = 1 AND @UserPreferredCity = 1 AND RL.Active = 1 AND R.RetailerActive = 1
							--AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
							--	OR
							--	(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ','))))
							--AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+ @SearchKey +'%' END
							)Retailer 						
								
					 
							--Options Listing
										
							DECLARE @Distance bit = 1
							DECLARE @Alphabetically bit = 1							
							DECLARE @LocalSpecials bit = 0
							DECLARE @Category bit = 0															
												
							SELECT @LocalSpecials = IIF(MAX(ISNULL(SaleFlag,0)) = 1,1,0) 								  
							FROM #Retail

							SELECT @Category = IIF(BusinessCategoryID IS NOT NULL,1,0)
							FROM #Retail
						

							CREATE TABLE #Temp (Type Varchar(50), Items Varchar(50),Flag bit)
							INSERT INTO #Temp VALUES ('Sort Items by','Distance',@Distance)	
							INSERT INTO #Temp VALUES ('Sort Items by','Alphabetically',@Alphabetically)						
							INSERT INTO #Temp VALUES ('Filter Items by','Local Specials !',@LocalSpecials)
							INSERT INTO #Temp VALUES ('Filter Items by','Category',@Category)								 
							

							SELECT Type AS fHeader
								,Items  AS filterName
							FROM #Temp
							WHERE Flag = 1						
									
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcFilterOptionList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



























GO
