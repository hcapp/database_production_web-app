USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcFundraisingOptionsListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [HubCitiApp2_1].[usp_HcFundraisingOptionsListDisplay]
Purpose                 :  To get the list of sort and filter options for Fundraising module
Example                 :  [HubCitiApp2_1].[usp_HcFundraisingOptionsListDisplay]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          7/31/2015       SPAN					1.1
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcFundraisingOptionsListDisplay]
(   
           
       --Input variable.        
         @HcHubCitiID int
       , @UserID Int
       , @HcMenuItemID int
       , @HcBottomButtonID int
	   , @SearchKey varchar(2000)	
	   , @Latitude float
	   , @Longitude float
	   , @Postalcode Varchar(200)
	   , @RetailID int
	   , @RetailLocationID int
	  

       --Output Variable 
        , @Status int output
        , @ErrorNumber int output
        , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY

			DECLARE @RegionApp Bit

			SELECT @RegionApp = IIF(HcAppListID=2,1,0)
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID 
              
			--To display Filter items
		   
		    DECLARE	@Date bit = 1	   
			--DECLARe @Distance bit = 1
			DECLARE @Alphabetically bit = 1	
			DECLARE @FundraisingDate bit = 1		 
			DECLARE @Category bit = 1						
			DECLARE @City bit = 1

			CREATE TABLE #Tempfilter (Type Varchar(50), Items Varchar(50),Flag bit)
			INSERT INTO #Tempfilter VALUES ('Sort Items by','Date',@Date)
			--INSERT INTO #Tempfilter VALUES ('Sort Items by','Distance',@Distance)
			INSERT INTO #Tempfilter VALUES ('Sort Items by','Alphabetically', @Alphabetically)

			INSERT INTO #Tempfilter VALUES ('Filter Items by','Fundraising Date', @FundraisingDate)
			INSERT INTO #Tempfilter VALUES ('Filter Items by','Category' ,@Category)
			INSERT INTO #Tempfilter VALUES ('Filter Items by','City', @City)
			
			IF @RegionApp = 1
			BEGIN	
				SELECT Type AS fHeader
					  ,Items AS filterName
				FROM #Tempfilter
			END
			ELSE
			BEGIN
				SELECT Type AS fHeader
					  ,Items AS filterName
				FROM #Tempfilter
				WHERE Items <> 'City'
			END
				
			--Confirmation of Success.
            SELECT @Status = 0


	END TRY

    BEGIN CATCH
      
			--Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0    
            BEGIN    
            PRINT 'Error occured in Stored Procedure [HubCitiApp2_1].[usp_HcFundraisingOptionsListDisplay]'      
            --- Execute retrieval of Error info.  
            EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			 
            --Confirmation of failure.
            SELECT @Status = 1
            END;    
            
      END CATCH;


												
END;


















GO
