USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcCouponRetailerDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcCouponRetailerDisplay
Purpose					: To Display Retailer Names
Example					: usp_HcCouponRetailerDisplay

History
Version		 Date		Author	  Change Description
------------------------------------------------------------------------------- 
1.0		 20th Jun 2013	 SPAN	   Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcCouponRetailerDisplay]
(

	--Input Variables 	
	   @UserID int
	 , @BusinessCategoryID Varchar(50)
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(500)	
	 , @HcHubcitiID Int
	
	--Output Variables	
	 , @UserOutOfRange bit output    
	 , @DefaultPostalCode varchar(50) output
	 , @Status bit output   
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--To get Default Radius
		DECLARE @Radius FLOAT
		DECLARE @DistanceFromUser FLOAT


		SELECT @Radius = ScreenContent 
		FROM AppConfiguration
		WHERE ScreenName = 'DefaultRadius'
		AND ConfigurationType = 'DefaultRadius'

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
			 
		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
					 , @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
					 , @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @UserID
			END
		END
		--Check if the user has given latitude and longitude
		IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)
		BEGIN
				SELECT retId
						,retName
				FROM
				--Coupons associated to both RetailLocations and Products W.R.T Latitude and Longitude
				(SELECT DISTINCT CR.RetailID retId
						      , RetailName retName
				FROM Coupon C
				INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
				INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
				INNER JOIN Retailer R ON C.RetailID = R.RetailID AND R.RetailerActive = 1
				INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode=RL.PostalCode AND HL.HcCityID=RL.HcCityID AND RL.StateID=HL.StateID
				INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
				INNER JOIN RetailerBusinessCategory RBC ON C.RetailID = RBC.RetailerID
				LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
				WHERE HL.HcHubCitiID = @HcHubcitiID AND D.DuplicateRetailerID IS NULL AND ((@BusinessCategoryID <> '0' AND BusinessCategoryID = @BusinessCategoryID) OR (@BusinessCategoryID = 0 AND 1=1))
				AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE() + 1)
				
				--Coupons associated to RetailLocations but not to Products W.R.T Latitude and Longitude
				UNION
				SELECT DISTINCT CR.RetailID retId
						      , RetailName retName
				FROM Coupon C
				INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
				LEFT JOIN CouponProduct CP ON C.CouponID = CP.CouponID
				INNER JOIN Retailer R ON C.RetailID = R.RetailID AND R.RetailerActive = 1
				INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode=RL.PostalCode AND HL.HcCityID=RL.HcCityID AND RL.StateID=HL.StateID
				INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
				INNER JOIN RetailerBusinessCategory RBC ON C.RetailID = RBC.RetailerID
				LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
				WHERE HL.HcHubCitiID = @HcHubcitiID AND CP.ProductID IS NULL AND D.DuplicateRetailerID IS NULL
				AND ((@BusinessCategoryID <> '0' AND BusinessCategoryID = @BusinessCategoryID) OR (@BusinessCategoryID = 0 AND 1=1))
				AND (ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)) <= @Radius 
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE() + 1)
				)RET
				ORDER BY retName ASC					
		END
		--Confirmation of Success.
			SELECT @Status = 0 
		--Check if user has given populationCenterID		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcCouponRetailerDisplay.'		
		-- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	
		--Confirmation of failure.
			SELECT @Status = 1			
		END;
		 
	END CATCH;
END;
















































GO
