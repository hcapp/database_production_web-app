USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcShareBandAppsite]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_HcShareAppsite
Purpose                 : 
Example                 : usp_HcShareAppsite

History
Version           Date              Author    Change Description
--------------------------------------------------------------- 
1.0				7/18/2016			Bindu T A        1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcShareBandAppsite]
(
        
	 	@UserID int,
	    @BandID int
	  , @HubCitiId int
    
      --Output Variable 
      , @ShareText varchar(500) output
	  , @HubCitiName varchar(100) output
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
      
		BEGIN TRANSACTION
      
            DECLARE @RetailConfig varchar(500) = 'https://www.scansee.net/Images/retailer/'
			DECLARE @SSQR VARCHAR(100) = 'http://10.122.61.145:8080/SSQR/qr/'              
			DECLARE @DefaultBandImage varchar(1000) = 'http://10.122.61.146:8080/Images/band/default-logo.jpg'
			DECLARE @RibbonAdImagePath varchar(1000)
			DECLARE @RibbonAdURL varchar(1000)
			DECLARE @BannerAdID varchar(1000)
			DECLARE @DefaultBandBannerImage VARCHAR(500)= 'http://10.122.61.146:8080/Images/band/banner-default.jpg'

			
				--Get the associated Banner Ad Details.
				SELECT DISTINCT   @RibbonAdImagePath = @RetailConfig  + CAST(@BandID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
								, @RibbonAdURL = RLA.BannerAdURL  --Web set Banner URL
								, @BannerAdID = RLA.bandAdvertisementBannerID
				FROM bandAdvertisementBanner RLA
				WHERE RLA.bandID = @bandID AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1) AND RLA.StartDate = (SELECT MAX(StartDate) FROM bandAdvertisementBanner ASP WHERE BandID = @bandID)					 
				
 
			 SELECT @HubCitiName = HubCitiName FROM HcHubCiti WHERE HcHubCitiID = @HubCitiID  
			 SELECT @ShareText = 'I found this Appsite in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
    
		SELECT DISTINCT   B.BandName retName
						, retImage = ISNULL(@RibbonAdImagePath,@DefaultBandBannerImage)
						---, retImage = isnull(@RetailConfig+CAST(B.BandID AS VARCHAR)+'/' +BandImagePath, @DefaultBandImage) 								  
						, QRUrl = @SSQR + '3002.htm?bandId='+ CAST(B.BandID AS VARCHAR(10))+ '&hubcitiId='+ CAST(@HubCitiId  AS VARCHAR(10))
		FROM Band B
		WHERE B.BandID = @BandID
            
           --Confirmation of Success.
		   SELECT @Status = 0 
        COMMIT TRANSACTION          
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure [usp_HcShareBandAppsite].'       
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;
            
      END CATCH;
END;









GO
