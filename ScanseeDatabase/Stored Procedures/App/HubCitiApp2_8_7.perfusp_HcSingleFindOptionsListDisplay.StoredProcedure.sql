USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[perfusp_HcSingleFindOptionsListDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcSingleFindOptionsListDisplay]
Purpose                 :  To get the list of filter options for Find single category Module.
Example                 :  [usp_HcSingleFindOptionsListDisplay]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/05/2015       SPAN            1.1
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[perfusp_HcSingleFindOptionsListDisplay]
(
      --Input variable
         @UserID int  
       , @CategoryName varchar(100)
       , @Latitude Decimal(18,6)
       , @Longitude  Decimal(18,6)
       , @HcHubCitiID Int
       , @HcMenuItemID Int
       , @HcBottomButtonID Int
	   , @SearchKey Varchar(250)

      --Output Variable 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
          
				DECLARE @Radius int
                , @PostalCode varchar(10)
                , @Tomorrow DATETIME = GETDATE() + 1
                , @Yesterday DATETIME = GETDATE() - 1
                , @DistanceFromUser FLOAT
                , @UserLatitude decimal(18,6)
                , @UserLongitude decimal(18,6) 
                , @RegionAppFlag Bit
                , @GuestLoginFlag BIT=0
				, @BusinessCategoryID Int
				, @UserOutOfRange bit   
				, @DefaultPostalCode varchar(50)
				
				DECLARE @UserID1 INT = @UserID,
				@HcHubCitiID1 INT = @HcHubCitiID,
				@HcMenuItemID1 INT = @HcMenuItemID,
				@HcBottomButtonID1 INT = @HcBottomButtonID
				
				
				--SearchKey implementation
				DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

				SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
						WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
						WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
						ELSE @SearchKey END)              

				SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName
				
				DECLARE @IsSubCategory bit = 0
				SELECT @IsSubCategory = 1
				FROM HcBusinessSubCategoryType T
				INNER JOIN HcBusinessSubCategory S ON T.HcBusinessSubCategoryTypeID = S.HcBusinessSubCategoryTypeID
				WHERE T.BusinessCategoryID = @BusinessCategoryID
					
					SET @HcBottomButtonID1 = IIF(@HcBottomButtonID1 IS NOT NULL,0,Null)

                    CREATE TABLE #CityList(CityID Int,CityName Varchar(200))

                    IF (SELECT 1 FROM HcUser WHERE UserName ='guestlogin' AND HcUserID = @UserID1) > 0
                    BEGIN
                    SET @GuestLoginFlag = 1
                    END

					CREATE TABLE #RHubCitiList(HcHubCitiID Int) 

                    IF(SELECT 1 from HcHubCiti H
						INNER JOIN HcAppList AL ON H.HcAppListID = AL.HcAppListID 
						AND H.HcHubCitiID = @HcHubCitiID1 AND AL.HcAppListName = 'RegionApp')>0
                            BEGIN
									SET @RegionAppFlag = 1
										    
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HcHubCitiID1 
									UNION ALL
									SELECT  @HcHubCitiID1 AS HcHubCitiID
									)
                            END
                    ELSE
                            BEGIN
									SET @RegionAppFlag =0
									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HcHubCitiID1                                     
                            END

                           --To Fetch region app associated citylist
						DECLARE @HcCityID varchar(100) = null                   
						INSERT INTO #CityList(CityID,CityName)
                                  
						SELECT DISTINCT C.HcCityID 
										,C.CityName                              
						FROM #RHubcitiList H
						INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HcHubCitiID1 
						INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID 
						LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =C.HcCityID
						LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1  AND UP.HcHubcitiID =H.HchubcitiID                                                      
						WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
						OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param )
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1)                                        
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID1)
						UNION
						SELECT 0,'ABC'


                        SELECT @UserLatitude = @Latitude
                            , @UserLongitude = @Longitude
               
                         IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcUser A
                                    INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                                    WHERE HcUserID = @UserID1 
                        END
                        --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                        IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcHubCiti A
                                    INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                                    WHERE A.HcHubCitiID = @HcHubCitiID1
                        END

                                                      
                        --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
						EXEC [HubCitiApp8].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
						SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
 
						--To fetch all the duplicate retailers.
						SELECT DISTINCT DuplicateRetailerID 
						INTO #DuplicateRet
						FROM Retailer 
						WHERE DuplicateRetailerID IS NOT NULL
                           
                        --Derive the Latitude and Longitude in the absence of the input.
                        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
                        BEGIN
                                    --If the postal code is passed then derive the co ordinates.
                                    IF @PostalCode IS NOT NULL
                                    BEGIN
                                            SELECT @Latitude = Latitude
                                                    , @Longitude = Longitude
                                            FROM GeoPosition 
                                            WHERE PostalCode = @PostalCode
                                    END          
                                    ELSE
                                    BEGIN
                                            SELECT @Latitude = G.Latitude
                                                    , @Longitude = G.Longitude
                                            FROM GeoPosition G
                                            INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                            WHERE U.HcUserID = @UserID1 
                                    END                                                                               
                        END    
                     
                    --Get the user preferred radius.
					SELECT @Radius = LocaleRadius
                    FROM HcUserPreference 
                    WHERE HcUserID = @UserID1
                                         
                    SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
														WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 

                    --To get Find Category associated Business Category
                    SELECT DISTINCT F.BusinessCategoryID
                    INTO #BusinessCategory
                    FROM  BusinessCategory F                        
                    LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON FB.BusinessCategoryID=F.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemID1
                    WHERE F.BusinessCategoryName = @CategoryName AND ((FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID) OR 1=1)               
                                          
                    CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                            , RetailID INT 
                                            , RetailName VARCHAR(1000)
                                            , RetailLocationID INT
                                            , Address1 VARCHAR(1000)
                                            , City VARCHAR(1000)    
                                            , State CHAR(2)
                                            , PostalCode VARCHAR(20)
                                            , retLatitude FLOAT
                                            , retLongitude FLOAT
                                            , Distance FLOAT  
                                            , DistanceActual FLOAT                                                  
                                            , SaleFlag BIT
                                            , HcBusinessSubCategoryID INT
                                            , BusinessSubCategoryName VARCHAR(1000))   

                      INSERT INTO #Retail(RetailID   
											, RetailName  
											, RetailLocationID  
											, Address1  
											, City  
											, State  
											, PostalCode  
											, retLatitude  
											, retLongitude  
											, Distance 
											, DistanceActual                                                 
											, SaleFlag  
											, HcBusinessSubCategoryID  
											, BusinessSubCategoryName)
                                SELECT  RetailID    
                                            , RetailName
                                            , RetailLocationID
                                            , Address1
                                            , City    
                                            , State
                                            , PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , Distance    
                                            , DistanceActual                                             
                                            , SaleFlag
                                            , HcBusinessSubCategoryID 
                                            , BusinessSubCategoryName
									FROM 
									(SELECT DISTINCT R.RetailID     
                                                    , R.RetailName
                                                    , RL.RetailLocationID 
                                                    , RL.Address1     
                                                    , RL.City
                                                    , RL.State
                                                    , RL.PostalCode
                                                    , RL.RetailLocationLatitude  retLatitude
                                                    , RL.RetailLocationLongitude retLongitude
													, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                    , DistanceActual = 0
                                                    --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                    , SaleFlag = 0   
                                                    , HcBusinessSubCategoryID = SB.HcBusinessSubCategoryID
                                                    , BusinessSubCategoryName 
									FROM Retailer R 
									INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
									INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
									INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
									INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode  AND HL.HcHubCitiID=@HcHubCitiID1 AND RL.HcCityID = HL.HcCityID
									INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
									INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID ) OR (@RegionAppFlag =0 AND CL.CityID=0)
									INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
                                    LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                    LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                    LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                    LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
									LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID  
                                    WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
                                    --AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                     AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
									) Retailer
                                    WHERE Distance <= @Radius                             
                         
                           SELECT DISTINCT Retailid , RetailLocationid
                           INTO #RetailItemsonSale1
                           FROM 
								(SELECT DISTINCT R.RetailID, a.RetailLocationID 
								FROM RetailLocationDeal a 
								INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
								INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
									AND a.ProductID = c.ProductID
									AND GETDATE() BETWEEN ISNULL(a.SaleStartDate, @Yesterday) AND ISNULL(a.SaleEndDate, @Tomorrow)
								UNION ALL 
								SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
								FROM Coupon C 
								INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
								INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
								LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
								WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
								GROUP BY C.CouponID
										,NoOfCouponsToIssue
										,CR.RetailID
										,CR.RetailLocationID
								HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
													ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
								UNION ALL  
								SELECT DISTINCT  RR.RetailID, 0 as RetaillocationID  
								from Rebate R 
								INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
								INNER JOIN #Retail RE ON RE.RetailLocationID =RR.RetailLocationID
								WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

								UNION ALL  
								SELECT DISTINCT R.retailid, a.RetailLocationID 
								FROM  LoyaltyDeal a
								INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
								INNER JOIN #Retail R ON R.RetailLocationID =a.RetailLocationID
								INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID AND b.ProductID = LDP.ProductID 
								WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)
                                  
								UNION ALL 
								SELECT DISTINCT R.RetailID, R.RetailLocationID
								FROM ProductHotDeal p
								INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
								INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
								LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
								LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
								WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
								GROUP BY P.ProductHotDealID
											,NoOfHotDealsToIssue
											,R.RetailID
											,R.RetailLocationID
								HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
													ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  
								UNION ALL 
								SELECT q.RetailID, qa.RetailLocationID
								FROM QRRetailerCustomPage q
								INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
								INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
								INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
								where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
								) Discount 
                    
                            SELECT DISTINCT Row_Num = RowNum
                                            , r.RetailID    
                                            , RetailName
                                            , rl.RetailLocationID
                                            , r.Address1
                                            , r.City    
                                            , r.State
                                            , r.PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , Distance           
                                            , DistanceActual                                       
                                            , SaleFlag= CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
                                            , HcBusinessSubCategoryID 
                                            , BusinessSubCategoryName
							INTO #Retailer    
							FROM #Retail R
							INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
							LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
							WHERE RL.Active = 1
							ORDER BY RowNum ASC
                        

						--To display Filter items
						DECLARE @Distance bit = 1
						DECLARE @Alphabetically bit = 1
						DECLARE @LocalSpecials bit = 0
						DECLARE @Category bit = 0
						DECLARE @Options bit = 0
						DECLARE @City bit = 0
						DECLARE @Interests bit = 0

						SELECT @LocalSpecials = 1 --IIF(MAX(ISNULL(SaleFlag,0)) = 1,1,0)
						FROM #Retailer
						WHERE SaleFlag = 1

						SELECT @Category = 1
						FROM #Retailer
						WHERE HcBusinessSubCategoryID IS NOT NULL
						AND @IsSubCategory = 1

						SELECT @City = 1
						FROM #Retailer
						WHERE City IS NOT NULL AND (@RegionAppFlag = 1)

						SELECT @Interests = 1 
						FROM #Retailer R
						INNER JOIN HcFilterRetailLocation RF ON R.RetailLocationID = RF.RetailLocationID
						INNER JOIN HcFilter F ON RF.HcFilterID = F.HcFilterID
						WHERE F.HcHubCitiID = @HcHubCitiID1

						SELECT @Options = 1
						FROM #Retailer R
						INNER JOIN RetailerFilterAssociation F ON R.RetailID = F.RetailID AND F.BusinessCategoryID = @BusinessCategoryID
						AND F.AdminFilterID IS NOT NULL AND F.AdminFilterValueID IS NULL


						SELECT DISTINCT F.FilterName  AS FilterName
						INTO #Filterlist
						FROM #Retailer R
						INNER JOIN RetailerFilterAssociation RF ON R.RetailID = RF.RetailID
						INNER JOIN AdminFilterValueAssociation A ON RF.AdminFilterValueID = A.AdminFilterValueID AND RF.BusinessCategoryID = @BusinessCategoryID
						INNER JOIN AdminFilterValue AV ON A.AdminFilterValueID = AV.AdminFilterValueID
						INNER JOIN AdminFilter F ON A.AdminFilterID = F.AdminFilterID AND RF.AdminFilterID = F.AdminFilterID
						ORDER BY FilterName

						CREATE TABLE #Temp (Type Varchar(50), Items Varchar(50),Flag bit)
						INSERT INTO #Temp VALUES ('Sort Items by','Distance',@Distance)
						INSERT INTO #Temp VALUES ('Sort Items by','Alphabetically',@Alphabetically)

						INSERT INTO #Temp VALUES ('Filter Items by','Local Specials !',@LocalSpecials)
						INSERT INTO #Temp VALUES ('Filter Items by','Category',@Category)
						INSERT INTO #Temp VALUES ('Filter Items by','Options',@Options)

						INSERT INTO #Temp (Type
										  ,Items
										  ,Flag)
									 SELECT 'Filter Items by'
											, FilterName
											, 1
									 FROM #Filterlist

						INSERT INTO #Temp VALUES ('Filter Items by','City',@City)
						INSERT INTO #Temp VALUES ('Filter Items by','Interests',@Interests)
					
						SELECT Type AS fHeader
								,Items  AS filterName
						FROM #Temp
						WHERE Flag = 1
                                           
                 --Confirmation of Success.
                 SELECT @Status = 0      
            
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
             IF @@ERROR <> 0    
                BEGIN    
                PRINT 'Error occured in Stored Procedure [usp_HcSingleFindOptionsListDisplay].'      
                --- Execute retrieval of Error info.  
                EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
                --Confirmation of failure.
                SELECT @Status = 1
                END;    
            
      END CATCH;
END;



























GO
