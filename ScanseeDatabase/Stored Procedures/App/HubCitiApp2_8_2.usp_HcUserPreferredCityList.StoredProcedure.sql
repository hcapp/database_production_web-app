USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserPreferredCityList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcUserPreferredCityList]
Purpose					: To display list of user preferred cities.
Example					: [usp_HcUserPreferredCityList]

History
Version		    Date		   Author		Change Description
--------------------------------------------------------------- 
1.0			11th Sept 2014	    SPAN  			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserPreferredCityList]
(
   
    --Input variable 	  
	  @HcHubcitiID int
	, @UserID int
  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To display Preferred cities
			IF EXISTS (SELECT 1 FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HcHubcitiID AND HcUserID = @UserID AND HcCityID IS NOT NULL)
			BEGIN

				SELECT DISTINCT C.HcCityID cityId
								,C.CityName cityName
				FROM HcUsersPreferredCityAssociation UC
				INNER JOIN HcCity C ON UC.HcCityID = C.HcCityID
				WHERE UC.HcHubcitiID = @HcHubCitiID
				AND UC.HcUserID = @UserID
				ORDER BY C.CityName
			END
			ELSE
			--To display all cities associated to given RegionApp 
			BEGIN
				SELECT DISTINCT C.HcCityID cityId
								,C.CityName cityName
				 FROM HcHubCiti H
				 --LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
				 INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID --OR RA.HcHubcitiID = LA.HcHubCitiID
				 INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
				 WHERE H.HcHubCitiID  = @HcHubcitiID
	             ORDER BY C.CityName 

			END
													
			--Confirmation of Success.
			SELECT @Status = 0

	          
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [HubCitiApp2_3_3].[usp_HcUserPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

































GO
