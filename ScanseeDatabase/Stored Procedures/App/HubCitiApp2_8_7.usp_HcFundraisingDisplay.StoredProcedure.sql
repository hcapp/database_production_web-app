USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcFundraisingDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : usp_HcFundraisingDisplay
Purpose                    : To display List of FundraisingEvents.
Example                    : usp_HcFundraisingDisplay

History
Version              Date               Author     Change Description
--------------------------------------------------------------------------------- 
1.0                  26/11/2014         SPAN        1.0
---------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcFundraisingDisplay]
(   
    --Input variable.        
         @HubCitiID int
       , @UserID Int
       , @CategoryID Int
       , @Latitude Float
       , @Longitude Float
       , @Postalcode Varchar(200)
       , @SearchParameter Varchar(1000)
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @SortColumn Varchar(200)
	   , @HCDepartmentID Int
       , @SortOrder Varchar(100)
       , @GroupColumn varchar(50)
	   , @HcMenuItemID int
       , @HcBottomButtonID int
       , @RetailID int
       , @RetailLocationID int
	   , @CityIDs Varchar(2000)

       --User Tracking
       , @MainMenuID Int
  
       --Output Variable 
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
	   , @FundDepartmentFlag Bit Output 
)
AS
BEGIN

       BEGIN TRY
               
                     DECLARE @UpperLimit int
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     DECLARE @RetailConfig Varchar(1000)
					 DECLARE @RegionAppFlag Bit=0

					 DECLARE @RegionAppID int
					 DECLARE @HcAppListID int

					 SELECT @HcAppListID = HcAppListID
					 FROM HcApplist
					 WHERE HcAppListName = 'RegionApp'

					 SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
					 FROM HcHubCiti H
					 WHERE HcHubCitiID = @HubCitiID
					 
                     SELECT @RetailConfig = ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Retailer Media Server Configuration'                       
                           
                     --To get the row count for pagination.   
                     SELECT @UpperLimit = @LowerLimit + ScreenContent   
                     FROM AppConfiguration   
                     WHERE ScreenName = @ScreenName 
                     AND ConfigurationType = 'Pagination'
                     AND Active = 1                     

                     DECLARE @Config VARCHAR(500)
                     SELECT @Config = ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'
                     
					 SET @FundDepartmentFlag=0

					 IF @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL
					 BEGIN
				
						IF EXISTS (SELECT DISTINCT	1						
									FROM HcMenuItemFundraisingCategoryAssociation MF
									INNER JOIN HcFundraisingCategoryAssociation FC ON FC.HcFundraisingCategoryID =MF.HcFundraisingCategoryID AND FC.HcHubcitiID=@HubCitiID AND MF.HcMenuItemID = @HcMenuItemID OR FC.HcHubCitiID IS NULL
									INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=FC.HcFundraisingDepartmentID 	
									INNER JOIN HcFundraising F ON F.HcFundraisingID=FC.HcFundraisingID	
									WHERE GETDATE() <= ISNULL(F.EndDate, GETDATE()+ 1) AND F.Active = 1)
				 
						BEGIN					
							SET @FundDepartmentFlag=1 
						END

					END	
					ELSE
					BEGIN
					
						IF EXISTS (SELECT DISTINCT 1
						FROM HcFundraisingBottomButtonAssociation BF
						INNER JOIN HcFundraisingCategoryAssociation FC ON FC.HcFundraisingCategoryID =BF.HcFundraisingCategoryID AND FC.HcHubcitiID=@HubCitiID AND BF.HcBottomButtonID = @HcBottomButtonID OR FC.HcHubCitiID IS NULL
						INNER JOIN HcFundraisingDepartments FD ON FD.HcFundraisingDepartmentID=FC.HcFundraisingDepartmentID 	
						INNER JOIN HcFundraising F ON F.HcFundraisingID=FC.HcFundraisingID	
						WHERE BF.HcHubCitiID=@HubCitiID AND GETDATE() <= ISNULL(F.EndDate, GETDATE()+ 1) AND F.Active = 1 )

						BEGIN					
							SET @FundDepartmentFlag=1 
						END
					
					END
				    
					

					

					CREATE TABLE #RHubcitiList(HchubcitiID Int) 
					INSERT INTO #RHubcitiList( HchubcitiID)
					SELECT DISTINCT HcHubCitiID
					FROM
						(SELECT DISTINCT RA.HcHubCitiID
						FROM HcHubCiti H
						LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
						WHERE H.HcHubCitiID  = @HubcitiID
						UNION ALL
						SELECT @HubcitiID)A

					DECLARE @HubCitis varchar(100)
					SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
					FROM #RHubcitiList


					 CREATE TABLE #Fund(RowNum INT IDENTITY(1, 1)
										,HcFundraisingID  int
										,FundraisingName Varchar(1000)  
										,ShortDescription Varchar(2000)
										,LongDescription  nVarchar(max)                                                
										,HcHubCitiID Int 
										,ImagePath Varchar(2000)                                                                                  
										,StartDate date
										,EndDate date
										,StartTime Time
										,EndTime Time                                     
										,MenuItemExist bit
										,HcFundraisingCategoryID  int
										,FundraisingCategoryName varchar(1000)
										,DepartmentFlag bit
										,CityName varchar(100))

                    IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
					BEGIN
					
							
							
							--MenuItem
					SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
					INTO #RegionCat
					FROM HcHubCiti H                     
					INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN HcMenuItemFundraisingCategoryAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcMenuItemID = @HcMenuItemID
					WHERE H.HcHubCitiID = @HubCitiID

					 
					CREATE TABLE #AllEventCategories(HubcitiID INT,FundraisingCategoryID INT,FundraisingCategoryName VARCHAR(100),FundraisingID INT,FundraisingDepartmentID INT)

					INSERT INTO #AllEventCategories(HubcitiID,FundraisingCategoryID,FundraisingCategoryName,FundraisingID,FundraisingDepartmentID)
					 
					SELECT DISTINCT HR.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
					FROM HcHubCiti H
					LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
					--INNER JOIN HcEvents E ON HR.HcHubCitiID = E.HcHubCitiID  
					INNER JOIN HcFundraisingCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN #RegionCat RC ON HE.HcFundraisingCategoryID = RC.HcFundraisingCategoryID
					INNER JOIN HcMenuItemFundraisingCategoryAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcMenuItemID = @HcMenuItemID 
					WHERE HR.HcRegionAppID = @HubCitiID 
                    
					UNION ALL

					SELECT HcHubCitiID,HcFundraisingCategoryID,FundraisingCategoryName,HcFundraisingID,HcFundraisingDepartmentID
					FROM #RegionCat
	
							
							 INSERT INTO #Fund(HcFundraisingID  
												  ,FundraisingName  
												  ,ShortDescription 
												  ,LongDescription                                         
												  ,HcHubCitiID  
												  ,ImagePath                                                                                
												  ,StartDate 
												  ,EndDate 
												  ,StartTime 
												  ,EndTime                                      
												  ,MenuItemExist 
												  ,HcFundraisingCategoryID  
												  ,FundraisingCategoryName
												  ,DepartmentFlag
												  ,CityName )	   
                     
							 --To display List of Events for MenuItem
							 SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName --= ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	  
													,ShortDescription
													,LongDescription                                                     
													,F.HcHubCitiID 
													,ImagePath=IIF(F.RetailID IS NULL,(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(F.RetailID AS VARCHAR)+'/'+F.FundraisingOrganizationImagePath)                                                                                 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Fundraisers' 
																					 AND MI.LinkID = F.HcFundraisingID)>0 THEN 1 ELSE 0 END  
												   ,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0) --@FundDepartmentFlag
												   ,C.CityName	
							FROM HcFundraising F 
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID=FCA.HcFundraisingID
							INNER JOIN HcFundraisingCategory  FC ON FC.HcFundraisingCategoryID =FCA.HcFundraisingCategoryID  
							INNER JOIN #AllEventCategories A ON F.HcFundraisingID = A.FundraisingID AND F.RetailID IS NULL
							LEFT JOIN HcFundraisingAppsiteAssociation FA ON A.FundraisingID = FA.HcFundraisingID
							LEFT JOIN HcAppSite HA ON FA.HcAppSiteID = HA.HcAppsiteID
							LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = A.HubCitiID AND Associated =1
							LEFT JOIN RetailLocation RL1 ON HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1 --OR (RA.RetailLocationID =RL1.RetailLocationID)
							LEFT JOIN HcCity C ON (RL1.HcCityID = C.HcCityID)	
							LEFT JOIN fn_SplitParam(@CityIDs,',') P ON P.Param =C.HcCityID 				                         
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 
							AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =P.Param ) OR F.FundraisingOrganizationName IS NOT NULL)
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND FundraisingName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						    AND (@HCDepartmentID IS NULL OR (@HCDepartmentID IS NOT NULL AND FCA.HcFundraisingDepartmentID =@HCDepartmentID))	   
							GROUP BY F.HcFundraisingID  
									,FundraisingName 
									,FundraisingOrganizationName  
									,ShortDescription
									,LongDescription                                                     
									,F.HcHubCitiID                                                                                       
									,StartDate 
									,EndDate                                           
									,FC.HcFundraisingCategoryID  
									,FC.FundraisingCategoryName	
									,FundraisingOrganizationImagePath
									,F.RetailID			
									,C.CityName
									,HcFundraisingDepartmentID
									
							UNION ALL

							SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	  
													,ShortDescription
													,LongDescription                                                     
													,F.HcHubCitiID 
													,ImagePath=IIF(F.RetailID IS NULL,(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(F.RetailID AS VARCHAR)+'/'+F.FundraisingOrganizationImagePath)                                                                                 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Fundraisers' 
																					 AND MI.LinkID = F.HcFundraisingID)>0 THEN 1 ELSE 0 END  
												   ,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0) --@FundDepartmentFlag
												   ,C.CityName	
							FROM #RHubcitiList RH --ON hl.HcHubcitiID=RH.HcHubcitiID 
							INNER JOIN #AllEventCategories a on 1=1
							--INNER JOIN #AllEventCategories A on HL.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcRetailerAssociation R on A.HubcitiID = R.HcHubCitiID and Associated=1
							INNER JOIN HcFundraising F on R.RetailID = F.RetailID and F.RetailID is not null 
							INNER JOIN HcFundraisingCategoryAssociation FCA on F.HcFundraisingID = FCA.HcFundraisingID and FCA.HcHubCitiID is null and A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
							INNER JOIN HcFundraisingCategory FC on FCA.HcFundraisingCategoryID=FC.HcFundraisingCategoryID
							LEFT JOIN HcRetailerFundraisingAssociation L on F.HcFundraisingID = L.HcFundraisingID --and R.RetailID = L.RetailID
							LEFT JOIN RetailLocation RL on RL.RetailLocationID=L.RetailLocationID AND RL.Active = 1 
							LEFT JOIN HcCity C ON RL.HcCityID = C.HcCityID
							LEFT JOIN fn_SplitParam(@CityIDs,',') P ON P.Param =C.HcCityID	
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
							AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =P.Param) OR F.FundraisingOrganizationName IS NOT NULL)
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND FundraisingName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						    AND (@HCDepartmentID IS NULL OR (@HCDepartmentID IS NOT NULL AND FCA.HcFundraisingDepartmentID =@HCDepartmentID))	   
							GROUP BY F.HcFundraisingID  
									,FundraisingName
									,FundraisingOrganizationName   
									,ShortDescription
									,LongDescription                                                     
									,F.HcHubCitiID                                                                                       
									,StartDate 
									,EndDate                                           
									,FC.HcFundraisingCategoryID  
									,FC.FundraisingCategoryName	
									,FundraisingOrganizationImagePath
									,F.RetailID			
									,C.CityName	
									,HcFundraisingDepartmentID

								
									
					END
					ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
					BEGIN
				
							 
							 --BottomButton
					SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
					INTO #RegionCat1
					FROM HcHubCiti H                     
					INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN HcFundraisingBottomButtonAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID
					WHERE H.HcHubCitiID = @HubCitiID

 
					CREATE TABLE #AllEventCategories1(HubcitiID INT,FundraisingCategoryID INT,FundraisingCategoryName VARCHAR(100),FundraisingID INT,FundraisingDepartmentID INT)

					INSERT INTO #AllEventCategories1(HubcitiID,FundraisingCategoryID,FundraisingCategoryName,FundraisingID,FundraisingDepartmentID)
					
					SELECT DISTINCT HR.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
					FROM HcHubCiti H
					LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
					--INNER JOIN HcEvents E ON HR.HcHubCitiID = E.HcHubCitiID  
					INNER JOIN HcFundraisingCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN #RegionCat1 RC ON HE.HcFundraisingCategoryID = RC.HcFundraisingCategoryID
					INNER JOIN HcFundraisingBottomButtonAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID 
					WHERE HR.HcRegionAppID = @HubCitiID 
                    
					UNION ALL

					SELECT HcHubCitiID,HcFundraisingCategoryID,FundraisingCategoryName,HcFundraisingID,HcFundraisingDepartmentID
					FROM #RegionCat1
							 
							 
							 
							 
							 INSERT INTO #Fund(HcFundraisingID  
												  ,FundraisingName 
												  ,ShortDescription 
												  ,LongDescription                                         
												  ,HcHubCitiID  
												  ,ImagePath                                                                                
												  ,StartDate 
												  ,EndDate 
												  ,StartTime 
												  ,EndTime                                      
												  ,MenuItemExist 
												  ,HcFundraisingCategoryID  
												  ,FundraisingCategoryName
												  ,DepartmentFlag
												  ,CityName)	
							 
							 --To display List of Events for BottomButton
							 SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	   
													,ShortDescription
													,LongDescription                                                     
													,F.HcHubCitiID 
													,ImagePath=IIF(F.RetailID IS NULL,(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(F.RetailID AS VARCHAR)+'/'+F.FundraisingOrganizationImagePath)                                                                                 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Fundraisers' 
																					 AND MI.LinkID = F.HcFundraisingID)>0 THEN 1 ELSE 0 END  
												   ,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0) --@FundDepartmentFlag
												   ,C.CityName	
							FROM HcFundraising F 
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID=FCA.HcFundraisingID
							INNER JOIN HcFundraisingCategory  FC ON FC.HcFundraisingCategoryID =FCA.HcFundraisingCategoryID  
							INNER JOIN #AllEventCategories1 A ON F.HcFundraisingID = A.FundraisingID AND F.RetailID IS NULL
							LEFT JOIN HcFundraisingAppsiteAssociation FA ON A.FundraisingID = FA.HcFundraisingID
							LEFT JOIN HcAppSite HA ON FA.HcAppSiteID = HA.HcAppsiteID
							LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = A.HubCitiID AND Associated =1
							LEFT JOIN RetailLocation RL1 ON HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1 --OR (RA.RetailLocationID =RL1.RetailLocationID)
							LEFT JOIN HcCity C ON (RL1.HcCityID = C.HcCityID)									                         
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
							AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,',')) OR F.FundraisingOrganizationName IS NOT NULL)))
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND FundraisingName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						    AND (@HCDepartmentID IS NULL OR (@HCDepartmentID IS NOT NULL AND FCA.HcFundraisingDepartmentID =@HCDepartmentID))	   
							GROUP BY F.HcFundraisingID  
									,FundraisingName
									,FundraisingOrganizationName   
									,ShortDescription
									,LongDescription                                                     
									,F.HcHubCitiID                                                                                       
									,StartDate 
									,EndDate                                           
									,FC.HcFundraisingCategoryID  
									,FC.FundraisingCategoryName	
									,FundraisingOrganizationImagePath
									,F.RetailID			
									,C.CityName
									,HcFundraisingDepartmentID
									
							UNION ALL
							SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	   
													,ShortDescription
													,LongDescription                                                     
													,F.HCHubcitiID 
													,ImagePath=IIF(F.RetailID IS NULL,(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(F.RetailID AS VARCHAR)+'/'+F.FundraisingOrganizationImagePath)                                                                                 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Fundraisers' 
																					 AND MI.LinkID = F.HcFundraisingID)>0 THEN 1 ELSE 0 END  
												   ,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0) --@FundDepartmentFlag
												   ,C.CityName	
							FROM HcLocationAssociation HL
							INNER JOIN #AllEventCategories1 A on HL.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcRetailerAssociation R on A.HubcitiID = R.HcHubCitiID and Associated=1
							INNER JOIN HcFundraising F on R.RetailID = F.RetailID and F.RetailID is not null
							INNER JOIN HcFundraisingCategoryAssociation FCA on F.HcFundraisingID = FCA.HcFundraisingID and FCA.HcHubCitiID is null and A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
							INNER JOIN HcFundraisingCategory FC on FCA.HcFundraisingCategoryID=FC.HcFundraisingCategoryID
							LEFT JOIN HcRetailerFundraisingAssociation L on F.HcFundraisingID = L.HcFundraisingID and R.RetailID = L.RetailID
							LEFT JOIN RetailLocation RL on RL.RetailLocationID=L.RetailLocationID AND RL.Active = 1 
							LEFT JOIN HcCity C ON RL.HcCityID = C.HcCityID
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
							AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,',')) OR F.FundraisingOrganizationName IS NOT NULL)))
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND FundraisingName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						    AND (@HCDepartmentID IS NULL OR (@HCDepartmentID IS NOT NULL AND FCA.HcFundraisingDepartmentID =@HCDepartmentID))	   
							GROUP BY F.HcFundraisingID  
									,FundraisingName
									,FundraisingOrganizationName   
									,ShortDescription
									,LongDescription                                                     
									,F.HcHubCitiID                                                                                       
									,StartDate 
									,EndDate                                           
									,FC.HcFundraisingCategoryID  
									,FC.FundraisingCategoryName	
									,FundraisingOrganizationImagePath
									,F.RetailID			
									,C.CityName	
									,HcFundraisingDepartmentID
				   END
				   ELSE
				   BEGIN
						 INSERT INTO #Fund(HcFundraisingID  
												  ,FundraisingName 
												  ,ShortDescription 
												  ,LongDescription                                         
												  ,HcHubCitiID  
												  ,ImagePath                                                                                
												  ,StartDate 
												  ,EndDate 
												  ,StartTime 
												  ,EndTime                                      
												  ,MenuItemExist 
												  ,HcFundraisingCategoryID  
												  ,FundraisingCategoryName
												  ,DepartmentFlag
												  ,CityName)	
							 
							 --To display List of Events for Retailer
							 SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	  
													,ShortDescription
													,LongDescription                                                     
													,F.HcHubCitiID 
													,ImagePath=IIF(F.RetailID IS NULL,(@Config + CAST(F.HCHubcitiID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath),@RetailConfig+CAST(F.RetailID AS VARCHAR)+'/'+F.FundraisingOrganizationImagePath)                                                                                 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)								 								 
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Fundraisers' 
																					 AND MI.LinkID = F.HcFundraisingID)>0 THEN 1 ELSE 0 END  
												   ,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0) --@FundDepartmentFlag        
												   ,C.CityName	
							 FROM HcFundraising F 
							 INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID =FCA.HcFundraisingID 
							 INNER JOIN HcFundraisingCategory FC ON FCA.HcFundraisingCategoryID =FC.HcFundraisingCategoryID 
							 --INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated =1							 
							 INNER JOIN HcRetailerFundraisingAssociation RF ON F.HcFundraisingID = RF.HcFundraisingID -- RA.RetailLocationID = RF.RetailLocationID
							 INNER JOIN RetailLocation RL1 ON RL1.RetailLocationID =RF.RetailLocationID	AND Rl1.Active = 1
							 INNER JOIN HcCity C ON RL1.HcCityID = C.HcCityID
							 WHERE F.RetailID =@RetailID AND RF.RetailLocationID = @RetailLocationID
							 AND  GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
							 AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,',')) OR F.FundraisingOrganizationName IS NOT NULL)))
							 AND (( ISNULL(@CategoryID, '0') <> '0' AND FC.HcFundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							 AND ((@SearchParameter IS NOT NULL AND FundraisingName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						     AND (@HCDepartmentID IS NULL OR (@HCDepartmentID IS NOT NULL AND FCA.HcFundraisingDepartmentID =@HCDepartmentID)) 
							 GROUP BY F.HcFundraisingID  
									,FundraisingName
									,FundraisingOrganizationName   
									,ShortDescription
									,LongDescription                                                     
									,F.HcHubCitiID                                                                                       
									,StartDate 
									,EndDate                                           
									,FC.HcFundraisingCategoryID  
									,FC.FundraisingCategoryName	
									,FundraisingOrganizationImagePath
									,F.RetailID			
									,C.CityName	
									,HcFundraisingDepartmentID

									
				   END

				   
                  CREATE TABLE #Fundraising(RowNum INT IDENTITY(1, 1)
                                          ,HcFundraisingID  int
                                          ,FundraisingName Varchar(1000)  
										  ,ShortDescription Varchar(2000)
                                          ,LongDescription  nVarchar(max)                                                
                                          ,HcHubCitiID Int 
                                          ,ImagePath Varchar(2000)                                                                                  
                                          ,StartDate date
                                          ,EndDate date
                                          ,StartTime Time
                                          ,EndTime Time                                     
                                          ,MenuItemExist bit
                                          ,HcFundraisingCategoryID  int
                                          ,FundraisingCategoryName varchar(1000)
										  ,DepartmentFlag bit
										  ,CityName Varchar(100))
										  						
				INSERT INTO #Fundraising(HcFundraisingID  
                                          ,FundraisingName 
										  ,ShortDescription 
                                          ,LongDescription                                         
                                          ,HcHubCitiID  
                                          ,ImagePath                                                                                
                                          ,StartDate 
                                          ,EndDate 
                                          ,StartTime 
                                          ,EndTime                                      
                                          ,MenuItemExist 
                                          ,HcFundraisingCategoryID  
                                          ,FundraisingCategoryName
										  ,DepartmentFlag
										  ,CityName)	                    
                     SELECT  HcFundraisingID  
                                ,FundraisingName   
								,ShortDescription
								,LongDescription                                                     
                                ,HcHubCitiID 
                                ,ImagePath                                                                                       
                                ,StartDate 
                                ,EndDate 
                                ,StartTime 
                                ,EndTime                                  
                                ,MenuItemExist 
                                ,HcFundraisingCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcFundraisingCategoryID END  
                                ,FundraisingCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE FundraisingCategoryName END
								,DepartmentFlag  
								,CityName        
					 FROM #Fund
                     GROUP BY HcFundraisingID  
                            ,FundraisingName   
							,ShortDescription
							,LongDescription                                                     
                            ,HcHubCitiID 
                            ,ImagePath                                                                                       
                            ,StartDate 
                            ,EndDate 
                            ,StartTime 
                            ,EndTime                                  
                            ,MenuItemExist 
                            ,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcFundraisingCategoryID END
                            ,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE FundraisingCategoryName END 
							,DepartmentFlag 
							,CityName 
					ORDER BY 
						CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Date' OR @SortColumn = 'City') THEN CAST(StartDate AS SQL_VARIANT)
							 WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Name' THEN FundraisingName
							 WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(StartDate AS SQL_VARIANT)                                                       
						END ASC ,StartDate
						--CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Date' OR @SortColumn = 'City') THEN CAST(StartDate AS SQL_VARIANT)
						--	 WHEN @SortOrder = 'DESC' AND @SortColumn = 'Name' THEN FundraisingName
						--	 WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(StartDate AS SQL_VARIANT)
						--END DESC 
                  
                     --To capture max row number.  
                     SELECT @MaxCnt = MAX(RowNum) FROM #Fundraising
                           
                     --this flag is a indicator to enable "More" button in the UI.   
                     --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                     SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

                
				     ---User Tracking
					            
                     CREATE TABLE #Temp1(FundraisingListID INT, HcFundraisingID Int)
                     
                     INSERT INTO HubCitiReportingDatabase..FundraisingList(MainMenuID
                                                                          ,HcFundraisingID 
                                                                          ,FundraisingClick 
                                                                          ,DateCreated)

                     OUTPUT Inserted.FundraisingListID,inserted.HCFundraisingID INTO #Temp1(FundraisingListID ,HcFundraisingID)

                     SELECT @MainMenuID
                           ,HcFundraisingID
                           ,0
                           ,GETDATE()
					 FROM #Fundraising           
                     
                     DECLARE @Seed INT
                     SELECT @Seed = ISNULL(MIN(FundraisingListID), 0)
                     FROM #Temp1                
                     
                     SELECT DISTINCT RowNum-- = Row_Number() over(order by RowNum asc)
									,HcFundraisingID  
                                    ,FundraisingName   
									,ShortDescription
									,LongDescription                                                     
                                    ,HcHubCitiID 
                                    ,ImagePath                                                                                       
                                    ,StartDate 
                                    ,EndDate 
                                    ,StartTime 
                                    ,EndTime                                  
                                    ,MenuItemExist 
                                    ,HcFundraisingCategoryID  
                                    ,FundraisingCategoryName  
									,DepartmentFlag
									,CityName
                     INTO #Fundraisings
                     FROM #Fundraising
                     
                     --DECLARE @SQL VARCHAR(1000) = 'ALTER TABLE #Fundraisings ADD FundraisingListID1 INT IDENTITY('+CAST(@Seed AS VARCHAR(100))+', 1)'
                     --EXEC (@SQL)
                     print 'b4 listidselect'
                     SELECT  DISTINCT RowNum
					                ,T.FundraisingListID fundListId
									, E.HcFundraisingID fundId
                                    ,FundraisingName  fundName 
									,ShortDescription sDescription
									,LongDescription lDescription                                                     
                                    ,HcHubCitiID hubCitiId
                                    ,ImagePath imgPath
									,StartDate startDate 
                                    ,EndDate endDate
                                    --,StartTime 
                                    --,EndTime                                  
                                    ,MenuItemExist 
                                    ,HcFundraisingCategoryID fundCatId 
                                    ,FundraisingCategoryName fundCatName
									,DepartmentFlag 
									,CityName                                                    
                     FROM #Fundraising E
                     INNER JOIN #Temp1 T ON T.HcFundraisingID =E.HcFundraisingID --T.FundraisingListID = E.FundraisingListID1    --
                     WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  
					 --ORDER BY RowNum 
                     print 'after ut n b4 bb'
                     --To get list of BottomButtons for this Module
                     DECLARE @ModuleName Varchar(50) 
                     SET @ModuleName = 'Fundraisers'
					 --EXEC [HubCitiApp2_3_3].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,@ModuleName,@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                
					
					 SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
								 , HM.HcHubCitiID 
								 , BB.HcBottomButtonID AS bottomBtnID
								 , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
								 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
								 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
								 , BottomButtonLinkTypeID AS btnLinkTypeID
								-- , btnLinkID = BottomButtonLinkID
								 --, btnLinkTypeName = BottomButtonLinkTypeName
								, btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
								--, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
								, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
															  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
															  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
																																								   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																								   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																								   WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
														  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
																WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																												INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																												WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
												   ELSE BottomButtonLinkTypeName END)					
					FROM HcMenu HM
					INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
					INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
					INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
					INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
					LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
					WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HubCitiID
					ORDER by HcFunctionalityBottomButtonID
                     

                     --Confirmation of Success
                     SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN 
			         SELECT ERROR_LINE()      
                     PRINT 'Error occured in Stored Procedure usp_HcFundraisingDisplay.'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;






























GO
