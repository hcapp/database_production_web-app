USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserDealsDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcUserDealsDisplay]
Purpose					: To dispaly Deals screen.
Example					: [usp_HcUserDealsDisplay]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			3/2/2015       SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserDealsDisplay]
(
	 --Input variable
	  @HcUserID int
	, @HubCitiID int
	, @SubmenuFlag Bit
	, @Latitude float
	, @Longitude float
	, @PostalCode varchar(100)
		
	--Output Variable
	, @MenuBckgrndColor Varchar(500) output
	, @MenuColor Varchar(500) output
	, @MenuFontColor Varchar(500) output 
	, @SectionColor Varchar(500) output		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE  @Coupons Bit = 0 
					, @Fundraisers Bit = 0 
					, @SearchKey Varchar(100)
					--, @Claimed Bit = 0
					--, @Expired Bit = 0
					--, @Used Bit = 0
					--, @ClaimedDeal Bit = 0
					--, @ClaimedCoupon Bit = 0
					--, @UsedDeal Bit = 0
					--, @UsedCoupon Bit = 0
					--, @ExpiredDeal Bit = 0
					--, @ExpiredCoupon Bit = 0
					--, @Deals Bit = 0 
					--, @Specials Bit = 0 
					

			DECLARE @UserPreferredProdCategory bit = 0
			, @UserPreferredLocCategory bit = 0

			DECLARE @Radius int
			SELECT @Radius = LocaleRadius
			FROM HcUserPreference
			WHERE HcUserID = @HcUserID
				
			IF @Radius IS NULL
			BEGIN
				SELECT @Radius = ScreenContent
				FROM AppConfiguration
				WHERE ScreenName = 'DefaultRadius'
			END
			 
			--To check user has Preferred Location category or Product category.
			IF EXISTS (SELECT 1 FROM HcUserCategory WHERE HcUserID = @HcUserID)
			BEGIN
			SELECT @UserPreferredProdCategory = 1
			END

			IF EXISTS (SELECT 1 FROM HcUserPreferredCategory WHERE HcUserID = @HcUserID AND HcHubCitiID = @HubCitiID )
			BEGIN
			SELECT @UserPreferredLocCategory = 1
			END

			--To get Bottom Button Image URl
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			--To get deals image URL
			DECLARE @ImagePath VARCHAR(500)
			SELECT @ImagePath = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti Deals imagepath'

			----To get Used image
			--DECLARE @UsedImage varchar(100)
			--SELECT @UsedImage = @ImagePath + ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType = 'HubCiti Deals used image'

			----To get claimed image
			--DECLARE @ClaimedImage varchar(100)
			--SELECT @ClaimedImage = @ImagePath + ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType = 'HubCiti Deals claimed image'

			----To get expired image
			--DECLARE @ExpiredImage varchar(100)
			--SELECT @ExpiredImage = @ImagePath + ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType = 'HubCiti Deals expired image'

			--To get coupons image
			DECLARE @CouponsImage varchar(100)
			SELECT @CouponsImage = @ImagePath + ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'HubCiti Deals coupons image'

			----To get deals image
			--DECLARE @DealsImage varchar(100)
			--SELECT @DealsImage = @ImagePath + ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType = 'HubCiti Deals deals image'

			--To get fundraisers image
			DECLARE @FundraisersImage varchar(100)
			SELECT @FundraisersImage = @ImagePath + ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'HubCiti Deals fundraisers image'

			----To get localspecials image
			--DECLARE @SpecialsImage varchar(100)
			--SELECT @SpecialsImage = @ImagePath + ScreenContent
			--FROM AppConfiguration 
			--WHERE ConfigurationType = 'HubCiti Deals localspecials image'

			--To get menu background color, menu font color and group section color for Deals.
			SELECT @MenuBckgrndColor = IIF(@SubmenuFlag=0,MenuBackgroundColor,SubMenuBackgroundColor)
				  ,@MenuColor = IIF(@SubmenuFlag=0,MenuButtonColor,SubMenuButtonColor)
			      ,@MenuFontColor = IIF(@SubmenuFlag=0,MenuButtonFontColor,SubMenuButtonFontColor)
				  ,@SectionColor = IIF(@SubmenuFlag=0,MenuGroupBackgroundColor,SubMenuGroupBackgroundColor)
			FROM HcMenuCustomUI 
			WHERE HubCitiID = @HubCitiID 

			--To get HubCiti list if given HubCiti is RegionApp and has associated to hubcities
			DECLARE @ApplistID int
			SELECT @ApplistID = HcAppListID
			FROM HcAppList 
			WHERE HcAppListName = 'RegionApp'

			CREATE TABLE #HcHubCitiID(HcHubCitiID int)
			IF EXISTS (SELECT 1 FROM HcHubCiti WHERE HcAppListID = @ApplistID)
			BEGIN
				INSERT INTO #HcHubCitiID 
				SELECT HcHubcitiID
				FROM HcRegionAppHubcitiAssociation
				WHERE HcRegionAppID = @HubCitiID
				UNION 
				SELECT @HubCitiID
			END
			ELSE
			BEGIN
				INSERT INTO #HcHubCitiID
				SELECT @HubCitiID	
			END
			
			----To check whether User has any Claimed HotDeals/Coupons.
			--SELECT @ClaimedDeal = 1
			--FROM HcUserHotDealGallery G
			--INNER JOIN ProductHotDeal P ON G.HotDealID = P.ProductHotDealID
			--WHERE HCuserID = @HcUserID AND Used = 0
			--AND GETDATE() BETWEEN ISNULL(P.HotDealStartDate,GETDATE()-1) AND ISNULL(P.HotDealExpirationDate,GETDATE()+1)

			--SELECT @ClaimedCoupon = 1
			--FROM HcUserCouponGallery G
			--INNER JOIN Coupon C ON G.CouponID = C.CouponID
			--WHERE HCuserID = @HcUserID AND UsedFlag = 0
			--AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)

			--SELECT @Claimed = IIF((@ClaimedDeal = 1 OR @ClaimedCoupon = 1),1,0)

			----To check whether User has any Used HotDeals/Coupons.
			--SELECT @UsedDeal = 1
			--FROM HcUserHotDealGallery G
			--INNER JOIN ProductHotDeal P ON G.HotDealID = P.ProductHotDealID
			--WHERE HCuserID = @HcUserID AND Used = 1
			--AND CASE WHEN HotDealExpirationDate < GETDATE() THEN DATEDIFF(DAY,GETDATE(),HotDealExpirationDate) ELSE DATEPART(DAY,GETDATE()) END <=90 

			--SELECT @UsedCoupon = 1
			--FROM HcUserCouponGallery G
			--INNER JOIN Coupon C ON G.CouponID = C.CouponID
			--WHERE HCuserID = @HcUserID AND UsedFlag = 1
			--AND (CASE WHEN CouponExpireDate < GETDATE() THEN DATEDIFF(DAY,GETDATE(),CouponExpireDate) ELSE DATEPART(MONTH,GETDATE()) END <=90)

			--SELECT @Used = IIF((@UsedDeal = 1 OR @UsedCoupon = 1),1,0)

			----To check whether User has any Expired HotDeals/Coupons.
			--SELECT @ExpiredDeal = 1
			--FROM HcUserHotDealGallery H
			--INNER JOIN ProductHotDeal P ON H.HotDealID = P.ProductHotDealID
			--WHERE HCuserID = @HcUserID AND Used = 0
			--AND HotDealExpirationDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),HotDealExpirationDate)<=90

			--SELECT @ExpiredCoupon = 1
			--FROM HcUserCouponGallery G
			--INNER JOIN Coupon C ON G.CouponID = C.CouponID
			--WHERE HcUserID = @HcUserID AND UsedFlag = 0
			--AND CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90

			--SELECT @Expired = IIF((@ExpiredDeal = 1 OR @ExpiredCoupon = 1),1,0)
			
			--To get Deals,Coupons,Specials,Fundraisers list

			----Deals
			--DECLARE @HotdealMaxCnt int = 0 
			--DECLARE @Category int = 0
			
			--EXEC [HubCitiapp2_8_7].[usp_HcDealsMaxCntHotDealSearchPagination] @HcUserID,@Category,@Latitude,@Longitude,@PostalCode,@HubCitiID, @MaxCnt = @HotdealMaxCnt OUTPUT
			--SELECT @Deals = IIF(ISNULL(@HotdealMaxCnt,0) > 0,1,0)
			
			--Coupons
			DECLARE @CpnLocMaxCnt int = 0 
			--DECLARE @CpnProdMaxCnt int = 0 
			DECLARE @LocCoupon int = 0
			--DECLARE @ProdCoupon int = 0

			----To Filter Coupons based on Location categories
			EXEC [HubCitiapp2_8_7].[usp_HcDealsMaxCntGalleryAllCouponsByLocation] @HcUserID,@HubcitiID,@SearchKey,@Latitude,@Longitude,@PostalCode, @MaxCnt = @CpnLocMaxCnt OUTPUT 
			SELECT @LocCoupon = IIF(ISNULL(@CpnLocMaxCnt,0) > 0,1,0)

			----To Filter Coupons based on Product categories
			--EXEC [HubCitiapp2_8_7].[usp_HcDealsMaxCntGalleryAllCouponsByProduct] @HcUserID,@HubcitiID,@Latitude,@Longitude,@PostalCode, @MaxCnt = @CpnProdMaxCnt OUTPUT 
			--SELECT @ProdCoupon = IIF(ISNULL(@CpnProdMaxCnt,0) > 0,1,0) 

			SELECT @Coupons = IIF((@LocCoupon = 1),1,0)
		
			----Specials
			--DECLARE @SplMaxCnt int = 0 

			----select @HcUserID,@HubcitiID,@Latitude,@Longitude,@PostalCode

			--EXEC [HubCitiApp2_8].[usp_HcDealsMaxCntSpecialOffersRetailersDisplay] @HcUserID,@HubcitiID,@Latitude,@Longitude,@PostalCode, @MaxCnt = @SplMaxCnt OUTPUT 
			--SELECT @Specials = IIF(ISNULL(@SplMaxCnt,0) > 0,1,0)
			
			--Fundraisers
			DECLARE @FundMaxCnt int = 0 

			EXEC [HubCitiapp2_8_7].[usp_HcDealsMaxCntFundraisingListDisplay] @HcUserID,@HubcitiID, @MaxCnt = @FundMaxCnt OUTPUT 
			SELECT @Fundraisers = IIF(ISNULL(@FundMaxCnt,0) > 0,1,0)

			--Deals Result Set
			CREATE TABLE #Temp1( ImagePath Varchar(500)
								, Name Varchar(100)
								, Flag bit)
				INSERT INTO #Temp1(	ImagePath
								  , Name
								  , Flag)
							SELECT ImagePath
								  , Name
								  , Flag
							FROM( 
							--SELECT @SpecialsImage ImagePath
							--		, 'Local Specials' Name
							--		, @Specials Flag
							-- UNION ALL
							-- SELECT @DealsImage ImagePath
							--		, 'Deals' Name
							--		, @Deals Flag
							-- UNION ALL
							 SELECT @CouponsImage ImagePath
									, 'Coupons' Name
									, @Coupons Flag
							 UNION ALL
							 SELECT @FundraisersImage ImagePath
									, 'Fundraisers' Name
									, @Fundraisers Flag
							)A


			----Gallery Result set
			--CREATE TABLE #Temp2( ImagePath Varchar(500)
			--					, Name Varchar(100)
			--					, Flag bit)
			--INSERT INTO #Temp2(	ImagePath
			--				  , Name
			--				  , Flag)
			--			SELECT ImagePath
			--				  , Name
			--				  , Flag
			--			FROM 
			--			(SELECT @ClaimedImage ImagePath
			--					, 'Claimed' Name
			--					, @Claimed Flag
			--			UNION ALL
			--			SELECT @ExpiredImage ImagePath
			--					, 'Expired' Name
			--					, @Expired Flag
			--			UNION ALL
			--			SELECT @UsedImage ImagePath
			--					, 'Used' Name
			--					, @Used Flag
			--			)B
			
			 SELECT ImagePath
					, Name
					, Flag
			 FROM #Temp1
			 WHERE Flag = 1

			 --SELECT ImagePath
				--	, Name
				--	, Flag
			 --FROM #Temp2
			 --WHERE Flag = 1
			 
		 ----To get list of BottomButtons for this Module
            DECLARE @ModuleName Varchar(50) 
            SET @ModuleName = 'Find Category'					
               
			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
				 , BB.HcBottomButtonID AS bottomBtnID
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
				 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
										  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
								                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																					            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																					            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
			                       ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HubCitiID
			ORDER by HcFunctionalityBottomButtonID
			
            --Confirmation of Success
            SELECT @Status = 0    
		
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcUserDealsDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

























GO
