USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcShareEvent]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_HcShareEvent
Purpose                 : To share event details.
Example                 : usp_HcShareEvent

History
Version           Date              Author			  Change Description
------------------------------------------------------------------------ 
1.0				  29th Jan 2015     Mohith H R        1.0
------------------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcShareEvent]
(
        
		@HcEventID int
	  ,	@UserID int
	  , @HubCitiId int
    
      --Output Variable 
      , @ShareText varchar(500) output
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
      
		BEGIN TRANSACTION
      
            DECLARE @RetailConfig varchar(50)
			DECLARE @Config VARCHAR(100)  
			DECLARE @Config1 VARCHAR(100)            
			DECLARE @Configuration VARCHAR(100)
            DECLARE @QRType INT     
          
			--Retrieve the server configuration.
            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 
             
            --To get server Configuration.
            SELECT @RetailConfig= ScreenContent  
            FROM AppConfiguration   
            WHERE ConfigurationType='Web Retailer Media Server Configuration' 
            
            --To get the text used for share.         
            DECLARE @HubCitiName varchar(100)
		 
			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
		       
			SELECT @ShareText = 'I found this Event in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti Event Share Text'    
			
			SELECT @Config1 = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'  
			
			DECLARE @RetailerEventID int

			SELECT @RetailerEventID = RetailID
			FROM HcEvents
			WHERE HcEventID = @HcEventID     
                  
            --Display the Basic details of the Retail location.   
			IF @RetailerEventID IS NULL
			BEGIN
				SELECT DISTINCT HcEventName eventName 
								,ShortDescription shortDes															
								,HcHubCitiID  hubCitiId
								,imgPath=(@Config1 + CAST(HCHubcitiID AS VARCHAR(100))+'/'+ImagePath)     
								,StartDate =CAST(StartDate AS DATE)
								,EndDate =CAST(EndDate AS DATE)
								,StartTime =CAST(StartDate AS Time)
								,EndTime =CAST(EndDate AS Time)             
								--,QRUrl = @Config+'2400.htm?key1='+ CAST(@HcEventID AS VARCHAR(10)) --+'&key2='+ CAST(@HubCitiId AS VARCHAR(10))                 							
								,QRUrl = @Config+'2400.htm?eventId='+ CAST(@HcEventID AS VARCHAR(10)) +'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10))                 							

				FROM  HcEvents  
				WHERE HcEventID = @HcEventID --AND HcHubCitiID = @HubCitiId     
			END
			ELSE
			BEGIN 
				SELECT DISTINCT HcEventName eventName 
								,ShortDescription shortDes															
								,HcHubCitiID  hubCitiId
								,imgPath=(@RetailConfig + CAST(RetailID AS VARCHAR(100))+'/'+ImagePath)  								  
								,StartDate =CAST(StartDate AS DATE)
								,EndDate =CAST(EndDate AS DATE)
								,StartTime =CAST(StartDate AS Time)
								,EndTime =CAST(EndDate AS Time)             
								--,QRUrl = @Config+'2400.htm?key1='+ CAST(@HcEventID AS VARCHAR(10)) --+'&key2='+ CAST(@HubCitiId AS VARCHAR(10))                 							
								,QRUrl = @Config+'2400.htm?eventId='+ CAST(@HcEventID AS VARCHAR(10)) +'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10))                							

				FROM  HcEvents  
				WHERE HcEventID = @HcEventID 
			END	      
            
           --Confirmation of Success.
		   SELECT @Status = 0 
        COMMIT TRANSACTION          
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_HcShareEvent.'       
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;
            
      END CATCH;
END;













































GO
