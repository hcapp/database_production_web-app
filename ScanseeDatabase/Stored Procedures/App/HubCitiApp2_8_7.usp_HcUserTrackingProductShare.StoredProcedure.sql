USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingProductShare]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcUserTrackingProductShare 
Purpose               : To capure the deails of the prouct share.  
Example               : usp_HcUserTrackingRebateShare 
  
History  
Version    Date				Author				Change Description  
---------------------------------------------------------------   
1.0        7th Nov 2013		Mohith H R			Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingProductShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @ProductID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         --Display Module Details
		 INSERT INTO HubCitiReportingDatabase..ShareProduct(MainMenuID
														   ,ShareTypeID
														   ,TargetAddress
														   ,ProductID  
														   ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@ProductID  
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0       							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingProductShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1     
  END;  
     
 END CATCH;  
END;













































GO
