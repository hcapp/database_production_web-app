USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFetchRetailerListPagination]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*    
Stored Procedure name : usp_HcFetchRetailerListPagination    
Purpose     : To list retailers with in the specified radius    
Example     : EXEC usp_HcFetchRetailerListPagination 2, 12.947150, 77.578880, 560102, 10 , 50   
    
History    
Version  Date            Author            Change Description    
---------------------------------------------------------------     
1.0      20thNov2013		 Span					1.0
1.1		12/22/2016			 Bindu T A		Day wise changes - Hours Filter Changes
---------------------------------------------------------------    
*/    
    
 CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFetchRetailerListPagination]    
(    
   @UserID int    
 , @Latitude decimal(18,6)    
 , @Longitude decimal(18,6)    
 , @ZipCode varchar(10)    
 , @Radius int    
 , @LowerLimit int  
 , @ScreenName varchar(50)
 , @HcHubcitiID Int
 , @SortColumn varchar(50)
 , @SortOrder varchar(10)
 , @requestedTime DateTime
 
 --Inputs for User Tracking
 , @Locateonmap bit
 , @MainMenuId int 
   
 --OutPut Variable

 , @UserOutOfRange BIT OUTPUT
 , @DefaultPostalCode varchar(50) output
 , @MaxCnt int  output
 , @NxtPageFlag bit output  
 , @Status int output 
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)    
AS    
BEGIN     
   
 BEGIN TRY    
 
	BEGIN TRANSACTION
 
        DECLARE @LocationDetailsID int
	    DECLARE @RetailConfig varchar(50)
	    DECLARE @CityExpDefaultConfig varchar(50)
        DECLARE @Config varchar(50) 
		DECLARE @ModuleName varchar(100) = 'NearBy'		
		
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='App Media Server Configuration'
		
		DECLARE @Globalimage varchar(50)
		SELECT @Globalimage =ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType ='Image Not Found'

		DECLARE @CityExperience bit 
        DECLARE @RetailGroupID int 
        DECLARE @RetailGroupButtonImagePath varchar(1000) 
        DECLARE @RetailAffiliateID int  
        DECLARE @RetailAffiliateCount int 
        DECLARE @RetailAffiliateName varchar(2000)
		--DECLARE @DefaultPostalCode VARCHAR(100)
		DECLARE @DateNameID Varchar(100)

		SELECT @DateNameID = HcDaysOfWeekID FROM HcDaysOfWeek WHERE HcDaysOfWeekName = (DATENAME(WEEKDAY,@requestedTime))

		SELECT @RetailConfig=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
			
		SELECT @CityExpDefaultConfig = ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType = 'City Experience Default Image Path'
		AND Active = 1
		
		 --To get the row count for pagination.  
		 DECLARE @UpperLimit int   
		 SELECT @UpperLimit = @LowerLimit + ScreenContent   
		 FROM AppConfiguration   
		 WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1  
			
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL			 
			
		DECLARE @DistanceFromUser float
		DECLARE @UserLatitude float
		DECLARE @UserLongitude float 

		SELECT @UserLatitude = @Latitude
			 , @UserLongitude = @Longitude

		IF (@UserLatitude IS NULL) 
		BEGIN
			SELECT @UserLatitude = Latitude
				 , @UserLongitude = Longitude
			FROM HcUser A
			INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
			WHERE HcUserID = @UserID 
		END		
		--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
			SELECT @UserLatitude = Latitude
					, @UserLongitude = Longitude
			FROM HcHubCiti A
			INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
			WHERE A.HcHubCitiID = @HcHubcitiID
		END
		PRINT @UserLatitude
		PRINT @UserLongitude
		
		SET @RetailAffiliateCount = 0

		SELECT @ZipCode = ISNULL(@ZipCode, (SELECT PostalCode FROM HcUser WHERE HcUserID = @UserID))
		
		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubcitiID, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
		
		-- To identify Retailer that have products on Sale or any type of discount
		SELECT DISTINCT Retailid , RetailLocationid
		INTO #RetailItemsonSale
		FROM 
			(SELECT b.RetailID, a.RetailLocationID 
			FROM RetailLocationDeal a 
			INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID AND b.Active = 1
			INNER JOIN RetailLocationProduct c ON a.RetailLocationID = c.RetailLocationID			
											   AND a.ProductID = c.ProductID
											   AND GETDATE() BETWEEN isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)
			INNER JOIN HcLocationAssociation HL ON HL.PostalCode =b.PostalCode AND HL.City=b.City AND HL.State=b.State AND HL.HcHubCitiID =@HcHubcitiID  
		UNION 
		SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
		FROM Coupon C 
		INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
		INNER JOIN RetailLocation RL ON RL.RetailID =CR.RetailID AND RL.Active = 1 
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID  
		LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID
		WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
		GROUP BY C.CouponID
				,NoOfCouponsToIssue
				,CR.RetailID
				,CR.RetailLocationID
		HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)								   
		
		UNION 
		SELECT  RR.RetailID, 0 AS RetaillocationID  
		FROM Rebate R 
		INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
		INNER JOIN RetailLocation RL ON RL.RetailID =RR.RetailID AND RL.Active = 1  
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID
		WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
		
		UNION 
		SELECT  c.retailid, a.RetailLocationID 
		FROM  LoyaltyDeal a
		INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
		INNER JOIN RetailLocation c ON a.RetailLocationID = c.RetailLocationID  AND c.Active = 1 
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode =c.PostalCode AND HL.HcCityID=c.HcCityID AND HL.StateID=c.StateID AND HL.HcHubCitiID =@HcHubcitiID
		INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID AND a.ProductID = LDP.ProductID 
		WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
		
		UNION
		SELECT DISTINCT rl.RetailID, rl.RetailLocationID
		FROM ProductHotDeal p
		INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
		INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active = 1 
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID
		LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
		LEFT JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
		WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
		GROUP BY P.ProductHotDealID
				,NoOfHotDealsToIssue
				,rl.RetailID
				,rl.RetailLocationID
		HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				 ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)  
		
		UNION
		SELECT q.RetailID, qa.RetailLocationID
		FROM QRRetailerCustomPage q
		INNER JOIN QRRetailerCustomPageAssociation qa ON qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID =qa.RetailLocationID AND RL.Active = 1   
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID
		INNER JOIN QRTypes qt ON qt.QRTypeID = q.QRTypeID AND qt.QRTypeName = 'Special Offer Page'
		WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') AND isnull(q.enddate,GETDATE()+1)) a
	 
	   --If the radius is not received then consider the global radius.
	   IF @Radius IS NULL
	   BEGIN
		SELECT @Radius = ScreenContent
		FROM AppConfiguration
		WHERE ScreenName = 'DefaultRadius'
	   END
	   
	   --While User search by Zipcode, coordinates are fetched from GeoPosition table.    
	   --Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			--If the user has not passed the co ordinates & postal code then cosidered the user configured postal code.
			IF @ZipCode IS NULL
			BEGIN
				SELECT @Latitude = G.Latitude
					 , @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @UserID
			END
			ELSE
			--If the postal code is passed then derive the co ordinates.
			BEGIN
				SELECT @Latitude = Latitude
					 , @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @ZipCode
			END
		END  
	  
      --To toggle Experience button. Decide if the user is around 100 miles from Any Groups.
	  
	   --If Coordinates exists fetching retailer list in the specified radius.    
	   IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)    
	   BEGIN	  
					 
					 SELECT Row_Num = IDENTITY(INT,1,1)
									, RetailID    
									, RetailName
									, RetailLocationID
									, Address1
									, Address2
									, Address3
									, Address4
									, City    
									, State
									, PostalCode
									, RetailerImagePath    
									, Distance 
									, DistanceActual								
									, SaleFlag
									, latitude
									, longitude
									, locationOpen
							INTO #Retail
					    FROM 
					   (SELECT DISTINCT R.RetailID     
									 , R.RetailName
									 , RL.RetailLocationID 
									 , RL.Address1     
									 , RL.Address2
									 , RL.Address3
									 , RL.Address4
									 , RL.City
									 , RL.State
									 , RL.PostalCode
								     , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
									 , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
									 , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) 
									 --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
									 , SaleFlag = CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END
									 , latitude = ISNULL(RL.RetailLocationLatitude, G.latitude)
									 , longitude = ISNULL(RL.RetailLocationLongitude, G.longitude)
									 , locationOpen = ''
										--CASE WHEN RL.StartTimeUTC < RL.EndTimeUTC THEN (CASE WHEN CAST(@requestedTime as time) >= RL.StartTimeUTC AND CAST(@requestedTime as time) < RL.ENdTimeUTC THEN 'Open'
          --                                                                                                                        WHEN RL.StartTimeUTC IS NULL OR RL.EndTimeUTC IS NULL THEN 'N/A'
          --                                                                                                                        ELSE 'Close' END)
										--					 WHEN RL.StartTimeUTC >= RL.EndTimeUTC THEN (CASE WHEN CAST(@requestedTime AS TIME) >= RL.StartTimeUTC AND convert(int,substring(convert(varchar(20),@requestedTime),1,2))<convert(int,substring(convert(varchar(20),RL.EndTimeUTC),1,2))+24 THEN 'Open'
										--																																  WHEN CAST(@requestedTime AS TIME) < RL.EndTimeUTC THEN 'Open'
										--																																  ELSE 'Close' END  )
										--					 WHEN RL.StartTimeUTC IS NULL OR RL.EndTimeUTC IS NULL THEN 'N/A'
										--					 ELSE 'Close' END
					 FROM Retailer R    
					 INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Active = 1 AND R.RetailerActive = 1    
					 INNER JOIN HcRetailerAssociation H ON H.RetailLocationID = RL.RetailLocationID AND HcHubCitiID = @HcHubcitiID AND Associated =1 
					 LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
					 -----------to get retailer that have products on sale
					 LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID	
					 LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID				
					 WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1) Retailer
					 WHERE Distance <= @Radius
				
					 ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
										  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(RetailName AS SQL_VARIANT) 
										  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
									  END ASC,Distance
							

					 
					 --To capture max row number.  
					 SELECT @MaxCnt = MAX(Row_Num) FROM #Retail
					 
					 --this flag is a indicator to enable "More" button in the UI.   
					 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
					 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END                             
                    
					 SELECT Row_Num rowNumber  
						, RetailID retailerId    
						, RetailName retailerName
						, R.RetailLocationID retailLocationID
						, Address1 retaileraddress1
						, Address2 retaileraddress2
						, Address3 retaileraddress3
						, Address4 retaileraddress4
						, City City
						, State State
						, PostalCode PostalCode    
						, RetailerImagePath logoImagePath     
						, Distance  
						, DistanceActual    
						, S.BannerAdImagePath bannerAdImagePath    
						, B.RibbonAdImagePath ribbonAdImagePath    
						, B.RibbonAdURL ribbonAdURL    
						, B.RetailLocationAdvertisementID advertisementID  
						, S.SplashAdID splashAdID
						, SaleFlag
						, latitude
						, longitude 
						, locationOpen = 
							 CASE WHEN H.StartTimeUTC < H.ENDTimeUTC THEN (CASE WHEN CAST(@requestedTime as time) >= H.StartTimeUTC AND CAST(@requestedTime as time) < H.ENDTimeUTC THEN 'Open'
										WHEN H.StartTimeUTC IS NULL OR H.ENDTimeUTC IS NULL THEN 'N/A'
										ELSE 'Close' END) 
										WHEN H.StartTimeUTC >= H.ENDTimeUTC THEN (CASE WHEN CAST(@requestedTime AS TIME) >= H.StartTimeUTC AND CONVERT(int,SUBSTRING(CONVERT(varchar(20),@requestedTime),10,2)) < CONVERT(int,SUBSTRING(CONVERT(varchar(20),H.ENDTimeUTC),10,2))+24 THEN 'Open'
										WHEN CAST(@requestedTime AS TIME) < H.ENDTimeUTC THEN 'Open'
										ELSE 'Close' END) 
										WHEN H.StartTimeUTC IS NULL OR H.ENDTimeUTC IS NULL THEN 'N/A'
								ELSE 'N/A' END
					 INTO #RetailerList  
					 FROM #Retail R
					 LEFT JOIN HcHoursFilterTime H ON H.RetailLocationID = R.RetailLocationID AND HcDaysOfWeekID = @DateNameID
					 LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
																	ELSE SplashAdImagePath
																  END
									   , SplashAdID  = ASP.AdvertisementSplashID
									   , R.RetailLocationID 
								FROM #Retail R
								INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
								INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
								AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)) S
							ON R.RetailLocationID = S.RetailLocationID
					 LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
																ELSE AB.BannerAdImagePath
															END 
									  , RibbonAdURL = AB.BannerAdURL
									  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
									  , R.RetailLocationID 
								 FROM #Retail R
								 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
								 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
								 WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)) B
							ON R.RetailLocationID = B.RetailLocationID
					 WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	 

					 
					 --UserTracking.	
					 
					 INSERT INTO HubCitiReportingDatabase..LocationDetails(MainMenuID
																		  ,Latitude
																		  ,Longitude
																		  ,PostalCode
																		  ,LocateOnMap
																		  ,CreatedDate)
														SELECT @MainMenuID
															  ,@Latitude
															  ,@Longitude
															  ,@ZipCode 
															  ,@Locateonmap 
															  ,GETDATE()
															  
					 --Capture the identity value.
					 SELECT @LocationDetailsID = SCOPE_IDENTITY()				 
					 
					 --Table to track the Retailer List.
					 CREATE TABLE #Temp(RetailerListID int
                                        ,LocationDetailID int
                                        ,MainMenuID int
                                        ,RetailID int
                                        ,RetailLocationID int)                      
                      
                      --Capture the impressions of the Retailer list.
                      INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
																	, RetailID
																	, RetailLocationID
																	, DateCreated 
																	, LocationDetailID)
					  OUTPUT inserted.RetailerListID, inserted.LocationDetailID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, LocationDetailID, MainMenuID, RetailLocationID)							
													SELECT @MainMenuId														 
														 , retailerId
														 , RetailLocationID
														 , GETDATE()
														 , @LocationDetailsID
													FROM #RetailerList
					--Display the Retailer along with the keys generated in the Tracking table.
					SELECT rowNumber
						 , T.RetailerListID retListID
						 , retailerId
						 , retailerName
						 , T.RetailLocationID
						 , retaileraddress1
						 , retaileraddress2
						 , retaileraddress3
						 , retaileraddress4
						 , City
						 , State
						 , PostalCode
						 , logoImagePath
						 , Distance = ISNULL(DistanceActual, Distance)	  
						 , bannerAdImagePath
						 , ribbonAdImagePath
						 , ribbonAdURL
						 , advertisementID
						 , splashAdID
						 , SaleFlag
						 , latitude
					     , longitude 		
						 , locationOpen				 
					FROM #Temp T
					INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID

				--	select @ModuleName
					--To display bottom buttons								

					EXEC [HubCitiApp2_8_3].[usp_HcFunctionalityBottomButtonDisplay] @HcHubcitiID, @ModuleName, @UserID, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

				 
			END  

		   --Confirmation of Success.
		   SELECT @Status = 0	
		COMMIT TRANSACTION  
      
 END TRY    
     
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
  PRINT 'Error occured in Stored Procedure usp_HcFetchRetailerListPagination.'      
  --- Execute retrieval of Error info.  
  EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
  ROLLBACK TRANSACTION;
  --Confirmation of failure.
  SELECT @Status = 1
  END;    
       
 END CATCH;    
END;







GO
