USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_DisplayNewsFirstSideNavigation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_DisplayNewsFirstSideNavigation]
Purpose					: To display and insert list of News categories on a BookmarkBar.
Example					: [usp_DisplayNewsFirstSideNavigation]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			26 May 2016	     Bindu T A			[usp_DisplayNewsFirstSideNavigation]
----------------------------------------------------------------------------------------------
*/

--exec [HubCitiApp2_8_3].[usp_DisplayNewsFirstSideNavigation] 5139,2217,12645,null,null,null,null,null
--exec [HubCitiApp2_8_3].[usp_DisplayNewsFirstSideNavigation_dev] null,0,2113,12701,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_DisplayNewsFirstSideNavigation]
(   

	--Input variable
	 -- @LinkID INT,
	  @LinkID INT,
	  @LevelID INT,
	  @HubcitiID INT 
	 ,@HcuserID INT
		--Output Variable 
	 ,@BookMark Varchar(100) output
	 ,@Section Varchar(100) output
     ,@Status int output
	 ,@ErrorNumber int output
	 ,@ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

	
		
		 --------------------------------Side Navigation List--------------------------

		   IF @LinkID IS  NULL or  @LinkID=0
		 SELECT @LinkID = (SELECT top 1 HCMenuID FROM HcMenu WHERE  HcHubcitiId=@HubcitiID AND Level=1  )
	
		
		 	Update a set NewsSideNavigationID=b.HcMenuItemID  from HcUserNewsFirstSideNavigation a 
		 inner join HcMenuItem b on b.MenuItemName=a.NewsSideNavigationDisplayValue
inner join  HcMenu  on HcMenu.HcMenuID=b.HcMenuID
where a.Flag=1 and a.NewsSideNavigationID<>b.HcMenuItemID
and   a.HchubcitiID=@HubcitiID and HcMenu.HchubcitiID=@HubcitiID
AND HcMenu.HcMenuID=@LinkID


		 DECLARE @UCount INT
		DECLARE @CatCount INT
		DECLARE @DCount INT

		SELECT NS.NewsCategoryID,NS.NewsCategoryColor,NS.HcHubCitiID
			INTO #Color
			FROM HcDefaultNewsFirstBookmarkedCategories D
			INNER JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID
			WHERE D.HchubcitiID = @HubCitiID 

		


		SELECT @UCount = Count(1) FROM HcUserNewsFirstSideNavigation WHERE Hcuserid= @HcuserID AND HchubcitiID = @HubCitiID AND Flag= 2
		SELECT @DCount = COUNT(1) FROM HcDefaultNewsFirstBookmarkedCategories D INNER JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID
		 WHERE D.HchubcitiID = @HubCitiID AND NS.HcHubCitiID=@HubCitiID

		SELECT @CatCount = Count(1) FROM NewsFirstSettings WHERE HcHubCitiID = @HubcitiID

				--SELECT @CatCount
		
					
		IF @Ucount = 0
			SELECT @Ucount = NULL


		 DECLARE @RegionApp INT
		 IF EXISTS (SELECT 1 FROM hchubciti WHERE HcHubCitiID = @HubcitiID AND Hcapplistid = 2 )
			SELECT @RegionApp = 1
		 ELSE 
			SELECT @RegionApp = 0

		DECLARE @GuestUser int
		DECLARE @CityPrefNotVisitedUser bit

		 CREATE TABLE #Temp 
		 (
		 parCatId INT,
		 parCatName VARCHAR(500),
		 subCatId INT,
		 subCatName VARCHAR(500),
		 isHubFunctn INT,
		 isAdded INT,
		 sortOrder INT,
		 isBkMark INT,
		 catColor VARCHAR(100),
		 isSubCategory BIT
		 )

		 CREATE TABLE #TempFN
		 (
		 parCatId INT,
		 parCatName VARCHAR(500),
		 subCatId INT,
		 subCatName VARCHAR(500),
		 isHubFunctn INT,
		 isAdded INT,
		 sortOrder INT,
		 isBkMark INT,
		 catColor VARCHAR(100),
		 isSubCategory BIT
		 )

		 
		 CREATE TABLE #TempFNSECTIONS
		 (
		 parCatId INT,
		 parCatName VARCHAR(500),
		 subCatId INT,
		 subCatName VARCHAR(500),
		 isHubFunctn INT,
		 isAdded INT,
		 sortOrder INT,
		 isBkMark INT,
		 catColor VARCHAR(100),
		 isSubCategory BIT
		 )
		 
		 
		 IF EXISTS (SELECT 1 FROM HcUserNewsFirstSideNavigation WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID  )
		 AND NOT EXISTS(select 1  from  HcUser  where username in('WelcomeScanSeeGuest','GuestLogin')  and hcuserid=@HcuserID) AND @RegionApp= 1
		 BEGIN

			SELECT @GuestUser = U.HcUserID
					FROM HcUser U
					INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
					WHERE UserName = 'GuestLogin'
					AND DA.HcHubCitiID = @HubCitiID
			
					IF NOT EXISTS (SELECT 1 FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HubCitiID AND HcUserID = @HcuserID and HcCityID is not null)
					BEGIN
						SET @CityPrefNotVisitedUser = 1
					END
					ELSE
					BEGIN
						SET @CityPrefNotVisitedUser = 0
					END
			
		--select 'a'

		--select @CityPrefNotVisitedUser,@GuestUser,@HcuserID

		IF @CityPrefNotVisitedUser=0 AND ISNULL(@GuestUser,0) <> @HcuserID
		BEGIN
		--SELECT '1'
		    INSERT INTO #TempFN
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,
			1  isHubFunctn,
			1 isAdded,UN.SortOrder sortOrder,1 isBkMark ,NULL  catColor,0 isSubCategory 
			from HcRegionAppMenuItemCityAssociation  RMC
			INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID
			AND RMC.HcHubcitiId=UC.HcHubcitiId
			INNER JOIN  HcMenuItem HM on HM.HcMenuItemID= RMC.HcMenuItemID
			INNER JOIN HcUserNewsFirstSideNavigation UN  on HM.HcMenuItemID=UN.NewsSideNavigationID
			where RMC.HCHubcitiID = @HubcitiID  AND UN.HcHubcitiId=@HubcitiID  and Flag= 1 
			and HM.HcMenuID = @LinkID AND UC.HcUserID=@HcuserID
				AND  UN.HcUserID=@HcuserID

			INSERT INTO #TempFNSECTIONS   
			SELECT DISTINCT M.HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			1 isHubFunctn,0 isAdded,(ISNULL(@CatCount,@DCount) + Position)  sortOrder,
			case when UN.NewsSideNavigationID is null then '0' else '1' end isBkMark,NULL  catColor, 0 isSubCategory
			from HcMenuItem M
			inner JOIN HcRegionAppMenuItemCityAssociation RMC on M.HcMenuItemID= RMC.HcMenuItemID and M.HcMenuID = @LinkID AND RMC.HcHubcitiID = @HubcitiID
			left JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND  RMC.HCHubcitiID = @HubcitiID  AND RMC.HcHubcitiId=UC.HcHubcitiId  AND UC.HcUserID=@HcuserID
			 LEFT JOIN HcUserNewsFirstSideNavigation UN  on M.HcMenuItemID=UN.NewsSideNavigationID AND UN.HcUserID=@HcuserID AND UN.HcHubcitiId=@HubcitiID AND  UN.Flag= 1  
				

			END
			ELSE

			BEGIN
			--SELECT '2'
		     INSERT INTO #TempFN  
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,
			1  isHubFunctn,
			1 isAdded,UN.SortOrder sortOrder,1 isBkMark ,NULL  catColor,0 isSubCategory 
			from HcUserNewsFirstSideNavigation UN 
	         INNER JOIN HcMenuItem HM on HM.HcMenuItemID=UN.NewsSideNavigationID
			WHERE UN.HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 1 and HM.HcMenuID = @LinkID
			AND  UN.HcUserID=@HcuserID

			INSERT INTO #TempFNSECTIONS   
			SELECT DISTINCT HM.HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			1 isHubFunctn,0 isAdded,(ISNULL(@CatCount,@DCount) + Position)  sortOrder,
			case when UN.NewsSideNavigationID is null then '0' else '1' end isBkMark,NULL  catColor, 0 isSubCategory
			from HcMenuItem HM
		    Left JOIN HcUserNewsFirstSideNavigation UN   on HM.HcMenuItemID=UN.NewsSideNavigationID AND  UN.HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID
			LEFT JOIN HcRegionAppMenuItemCityAssociation N   on HM.HcMenuItemID = N.HcMenuItemID  AND N.HcHubcitiId=@HubcitiID
			WHERE  HM.HcMenuID = @LinkID
			

			--from HcMenuItem HM
			----left JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND  RMC.HCHubcitiID = @HubcitiID  AND RMC.HcHubcitiId=UC.HcHubcitiId  AND UC.HcUserID=@HcuserID
			-- LEFT JOIN HcUserNewsFirstSideNavigation UN  on HM.HcMenuItemID=UN.NewsSideNavigationID AND UN.HcUserID=@HcuserID AND UN.HcHubcitiId=@HubcitiID AND  UN.Flag= 1 
			-- and HM.HcMenuID = @LinkID
			--AND  UN.HcUserID=@HcuserID 
			

			--SELECT   'HH',* FROM #TempFNSECTIONS


			END
			
			
		    INSERT INTO #Temp  

			SELECT DISTINCT*  FROM #TempFN
		  
		

		

			UNION

			--TO DISPLAY USER SELECTED CATEGORIES
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,0  isHubFunctn,
			1 isAdded,UN.Sortorder sortOrder,1 isBkMark, NS.NewsCategoryColor  catColor,0 isSubCategory
			FROM HcUserNewsFirstSideNavigation UN
			INNER join newsfirstsettings  ns on ns.NewsCategoryID=UN.NewsSideNavigationID  AND NS.HcHubCitiID = @HubcitiID
			WHERE HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 2

			--UNION

			----TO DISPLAY USER SELECTED SUB-CATEGORIES
			--SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,ISNULL(SB.NewsSideNavigationSubCategoryID,1) subCatId,ISNULL(SB.NewsSideNavigationSubCategoryDisplayValue,'All') subCatName,--SB.NewsSideNavigationSubCategoryID subCatId,SB.NewsSideNavigationSubCategoryDisplayValue subCatName,
			--0  isHubFunctn,
			--1 isAdded,UN.Sortorder sortOrder,1 isBkMark, NS.NewsCategoryColor  catColor,CASE WHEN SB.HcUserNewsFirstSideNavigationSubCategoriesID IS NULL THEN 0 ELSE 1 END isSubCategory 
			--FROM HcUserNewsFirstSideNavigation UN
			--INNER JOIN HcUserNewsFirstSideNavigationSubCategories SB ON SB.NewsFirstCategoryID = UN.NewsSideNavigationID
			--INNER JOIN newsfirstsettings  ns on ns.NewsCategoryID=UN.NewsSideNavigationID AND NS.HcHubCitiID = @HubcitiID
			--WHERE UN.HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and UN.Flag= 2  and SB.HcUserID=@HcuserID
			--and SB.HcHubcitiId=@HubcitiID
			----and  ns.HcHubcitiId=@HubcitiID
			
			UNION 

			--TO DISPLAY ALL FUNCTIONALITIES

			SELECT DISTINCT * FROM #TempFNSECTIONS
	
						
			UNION
			
			--TO DISPLAY ALL CATEGORIES
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,row_number()  over (order  by   cat.Sortorder ) sortOrder,case when hcus.NewsSideNavigationID is null then '0' else '1' end isBkMark , ns.NewsCategoryColor catColor,0 isSubCategory 
			FROM NewsFirstCategory cat 
			inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID
			LEFT  join HcUserNewsFirstSideNavigation hcus
			 on hcus.NewsSideNavigationID=cat.NewsCategoryID
			 and HcUserID= @HcuserID  AND hcus.HcHubcitiId=@HubcitiID
			 where  ns.HcHubcitiId=@HubcitiID AND cat.active=1
			
			--UNION

			----TO DISPLAY ALL SUB-CATEGORIES
			

			--SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,ISNULL(nsub.NewsSubCategoryID,1) subCatId,
			--ISNULL(nsub.NewsSubCategoryDisplayValue,'All') subCatName,--SB.NewsSideNavigationSubCategoryID subCatId,SB.NewsSideNavigationSubCategoryDisplayValue subCatName,--
			--0 isHubFunctn,0 isAdded, cat.Sortorder sortOrder ,case when SB.NewsFirstCategoryID is null then '0' else '1' end isBkMark , ns.NewsCategoryColor catColor,
			--CASE WHEN sub.NewsSubCategoryID IS NULL THEN 0 ELSE 1 END isSubCategory
			--FROM NewsFirstCategory cat 
			--inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID
			--inner join NewsFirstSubCategorySettings sub on sub.NewsCategoryID=ns.NewsCategoryID
			--inner join NewsFirstSubCategory nsub on nsub.NewsSubCategoryID= sub.NewsSubCategoryID
			----	LEFT  join HcUserNewsFirstSideNavigation hcus on hcus.NewsSideNavigationID=cat.NewsCategoryID and HcUserID= 12309 AND hcus.HcHubcitiId=2233
			--LEFT JOIN HcUserNewsFirstSideNavigationSubCategories SB ON SB.NewsFirstCategoryID = sub.NewsCategoryID AND SB.HchubcitiID= @HubcitiID and sb.HcUserID=@HcuserID
			--and SB.NewsSideNavigationSubCategoryID=sub.NewsSubCategoryID
			--where  ns.HcHubcitiId=@HubcitiID  
			--ORDER BY isAdded,sortOrder,isHubFunctn

	     
		 END

		ELSE IF EXISTS (SELECT 1 FROM HcUserNewsFirstSideNavigation WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID  )
		 AND NOT EXISTS(select 1  from  HcUser  where username in('WelcomeScanSeeGuest','GuestLogin')  and hcuserid=@HcuserID) AND @RegionApp= 0
		 BEGIN 

		--select 'b'
			
		 INSERT INTO #Temp  
		  
			--TO DISPLAY USER SELECTED FUNCTIONALITIES
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,
			1  isHubFunctn,
			1 isAdded,UN.SortOrder sortOrder,1 isBkMark ,NULL  catColor,0 isSubCategory
			FROM HcUserNewsFirstSideNavigation UN 
			--LEFT JOIN NewsFirstSettings  NS on ns.NewsCategoryID=UN.NewsSideNavigationID AND NS.HcHubCitiID = 2233
			INNER JOIN HcMenuItem HM on HM.HcMenuItemID=UN.NewsSideNavigationID
			WHERE HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 1 and HM.HcMenuID = @LinkID

			UNION

			--TO DISPLAY USER SELECTED CATEGORIES
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,0  isHubFunctn,
			1 isAdded,UN.Sortorder sortOrder,1 isBkMark, NS.NewsCategoryColor  catColor,0 isSubCategory
			FROM HcUserNewsFirstSideNavigation UN
			INNER join newsfirstsettings  ns on ns.NewsCategoryID=UN.NewsSideNavigationID  AND NS.HcHubCitiID = @HubcitiID
			WHERE HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 2

			--UNION

			----TO DISPLAY USER SELECTED SUB-CATEGORIES
			--SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,ISNULL(SB.NewsSideNavigationSubCategoryID,1) subCatId,ISNULL(SB.NewsSideNavigationSubCategoryDisplayValue,'All') subCatName,--SB.NewsSideNavigationSubCategoryID subCatId,SB.NewsSideNavigationSubCategoryDisplayValue subCatName,
			--0  isHubFunctn,
			--1 isAdded,UN.Sortorder sortOrder,1 isBkMark, NS.NewsCategoryColor  catColor,CASE WHEN SB.HcUserNewsFirstSideNavigationSubCategoriesID IS NULL THEN 0 ELSE 1 END isSubCategory 
			--FROM HcUserNewsFirstSideNavigation UN
			--INNER JOIN HcUserNewsFirstSideNavigationSubCategories SB ON SB.NewsFirstCategoryID = UN.NewsSideNavigationID
			--INNER JOIN newsfirstsettings  ns on ns.NewsCategoryID=UN.NewsSideNavigationID AND NS.HcHubCitiID = @HubcitiID
			--WHERE UN.HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and UN.Flag= 2
			----and  ns.HcHubcitiId=@HubcitiID
			
			UNION 

			--TO DISPLAY ALL FUNCTIONALITIES
			SELECT DISTINCT HcMenuItem.HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			1 isHubFunctn,0 isAdded,(ISNULL(@CatCount,@DCount) + Position) sortOrder,
			case when HN.NewsSideNavigationID is null then '0' else '1' end isBkMark,NULL  catColor, 0 isSubCategory
			FROM HcMenuItem 
			LEFT  JOIN HcUserNewsFirstSideNavigation HN on HN.NewsSideNavigationID=HcMenuItem.HcMenuItemID AND HcUserID = @HcuserID
			WHERE HcMenuID = @LinkID --AND CreatedUserID= @HcuserID 
						
			UNION
			
			--TO DISPLAY ALL CATEGORIES
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,row_number()  over (order  by   cat.Sortorder ) sortOrder,case when hcus.NewsSideNavigationID is null then '0' else '1' end isBkMark , ns.NewsCategoryColor catColor,0 isSubCategory 
			FROM NewsFirstCategory cat 
			inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID
			LEFT  join HcUserNewsFirstSideNavigation hcus
			 on hcus.NewsSideNavigationID=cat.NewsCategoryID
			 and HcUserID= @HcuserID  AND hcus.HcHubcitiId=@HubcitiID
			 where  ns.HcHubcitiId=@HubcitiID and cat.active=1
			
			--UNION

			

		--	SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,ISNULL(nsub.NewsSubCategoryID,1) subCatId,
		--		ISNULL(nsub.NewsSubCategoryDisplayValue,'All') subCatName,--SB.NewsSideNavigationSubCategoryID subCatId,SB.NewsSideNavigationSubCategoryDisplayValue subCatName,--
		--	0 isHubFunctn,0 isAdded,cat.Sortorder sortOrder ,case when SB.NewsFirstCategoryID is null then '0' else '1' end isBkMark , ns.NewsCategoryColor catColor,
		--	CASE WHEN sub.NewsSubCategoryID IS NULL THEN 0 ELSE 1 END isSubCategory
		--	FROM NewsFirstCategory cat 
		--	inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID
		--	inner join NewsFirstSubCategorySettings sub on sub.NewsCategoryID=ns.NewsCategoryID
		--	inner join NewsFirstSubCategory nsub on nsub.NewsSubCategoryID= sub.NewsSubCategoryID
		----	LEFT  join HcUserNewsFirstSideNavigation hcus on hcus.NewsSideNavigationID=cat.NewsCategoryID and HcUserID= 12309 AND hcus.HcHubcitiId=2233
		--	LEFT JOIN HcUserNewsFirstSideNavigationSubCategories SB ON SB.NewsFirstCategoryID = sub.NewsCategoryID AND SB.HchubcitiID= @HubcitiID and sb.HcUserID=@HcuserID
		--	and SB.NewsSideNavigationSubCategoryID=sub.NewsSubCategoryID
		--	where  ns.HcHubcitiId=@HubcitiID  
		--	ORDER BY isAdded,sortOrder,isHubFunctn
	     
		 END
		ELSE 
		 BEGIN

		-- select 'b'
		--SELECT @LinkID
		 select  distinct NewsCategoryID  into #tempdef  from   HcDefaultNewsFirstBookmarkedCategories  where HchubcitiID=@HubcitiID

			select  NewsCategoryID  into #other from NewsFirstCategory WHERE active=1
			except
			select NewsCategoryID  from #tempdef


		  INSERT INTO #Temp  
		  
			--TO DISPLAY ALL DEFAULT CATEGORIES
			SELECT DISTINCT def.NewsCategoryID parCatId, NewsCategoryDisplayValue parCatName,1 subCatId,'All' subCatName,0 isHubFunctn,
			1 isAdded,def.Sortorder sortOrder,1 isBkMark , n.NewsCategoryColor catColor,0 isSubCategory
			FROM HcDefaultNewsFirstBookmarkedCategories  def 
			INNER join #Color n on n.NewsCategoryID=def.NewsCategoryID  AND n.HcHubCitiID = def.HchubcitiID
			INNER join newsfirstsettings NS on NS.NewsCategoryID=def.NewscategoryID and NS.HcHubcitiId=@HubcitiID
			WHERE def.HchubcitiID = @HubcitiID

			UNION

			--TO DISPLAY ALL DEFAULT SUB-CATEGORIES
			SELECT DISTINCT def.NewsCategoryID parCatId, NewsCategoryDisplayValue parCatName,ISNULL(DS.NewsSubCategoryID,1) subCatId,ISNULL(DS.NewsSubCategoryDisplayValue,'All') subCatName,
			0 isHubFunctn, 
			1 isAdded,def.Sortorder sortOrder,1 isBkMark , n.NewsCategoryColor catColor,CASE WHEN DS.HcDefaultNewsFirstBookmarkedSubCategoriesID IS NULL THEN 0 ELSE 1 END isSubCategory
			FROM HcDefaultNewsFirstBookmarkedCategories  def 
			INNER JOIN HcDefaultNewsFirstBookmarkedSubCategories DS ON DEf.NewsCategoryID = DS.NewsFirstCategoryID
			INNER join #Color n on n.NewsCategoryID=def.NewsCategoryID  AND n.HcHubCitiID = def.HchubcitiID
		    inner join newsfirstsettings  ns on ns.NewsCategoryID=def.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			WHERE ds.HchubcitiID = @HubcitiID and def.HchubcitiID = @HubcitiID
			
			
			UNION 

			--TO DISPLAY ALL DEFAULT HUBCITI FUNCTIONALITIES
			SELECT DISTINCT HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			1 isHubFunctn,0 isAdded,(ISNULL(@DCount,@UCount) + Position) sortOrder,0 isBkMark, '#000000' catColor, 0 isSubCategory
			FROM HcMenuItem 
			WHERE HcMenuID = @LinkID --AND CreatedUserID= @HcuserID 
						
			UNION

			--TO DISPLAY ALL OTHER CATEGORIES
			

			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,cat.Sortorder sortOrder,
			1 isBkMark , n.NewsCategoryColor catColor, 0 isSubCategory
			FROM NewsFirstCategory cat 
			inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID AND ns.HchubcitiID = @HubcitiID
			inner join HcDefaultNewsFirstBookmarkedCategories def on def.NewscategoryID=cat.NewsCategoryID
			inner join #Color n on n.NewsCategoryID=def.NewsCategoryID  
			--and n.NewsCategoryID=cat.NewsCategoryID
			and def.HchubcitiID = n.HchubcitiID  where cat.Active=1--cat.NewsCategoryID=1
			union
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,cat.Sortorder sortOrder,
			0  isBkMark , null catColor, 0 isSubCategory
			FROM NewsFirstCategory cat  inner join #other  on #other.NewsCategoryID=cat.NewsCategoryID
			 inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID AND cat.Active=1

				

			UNION

			--TO DISPLAY ALL OTHER SUB_CATEGORIES
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,ISNULL(DS.NewsSubCategoryID,1) subCatId,ISNULL(DS.NewsSubCategoryDisplayValue,'All') subCatName,
			0 isHubFunctn,0 isAdded,cat.Sortorder sortOrder,
			0  isBkMark ,n.NewsCategoryColor catColor,
			CASE WHEN DS.HcDefaultNewsFirstBookmarkedSubCategoriesID IS NULL THEN 0 ELSE 1 END isSubCategory
			FROM NewsFirstCategory cat 
			--inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			inner join HcDefaultNewsFirstBookmarkedCategories def  on def.NewscategoryID=cat.NewsCategoryID
			 inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			inner JOIN HcDefaultNewsFirstBookmarkedSubCategories DS ON DEf.NewsCategoryID = DS.NewsFirstCategoryID
			inner join #Color n on n.NewsCategoryID=def.NewsCategoryID AND n.HcHubCitiID = def.HchubcitiID
			and n.NewsCategoryID=cat.NewsCategoryID
			and def.HchubcitiID = @HubcitiID  and DS.HchubcitiID = @HubcitiID AND cat.Active=1
			union

			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,ISNULL(DS.NewsSubCategoryID,1) subCatId,ISNULL(DS.NewsSubCategoryDisplayValue,'All') subCatName,
			0 isHubFunctn,0 isAdded,cat.Sortorder sortOrder,
			case when DS.NewsFirstCategoryID is null then '0' else '1' end isBkMark ,'#000000' catColor,
			CASE WHEN DS.HcDefaultNewsFirstBookmarkedSubCategoriesID IS NULL THEN 0 ELSE 1 END isSubCategory
			FROM NewsFirstCategory cat 
			inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			inner join #other  on #other.NewsCategoryID=cat.NewsCategoryID
			Left JOIN HcDefaultNewsFirstBookmarkedSubCategories DS ON #other.NewsCategoryID = DS.NewsFirstCategoryID
			  and DS.HchubcitiID = @HubcitiID
			WHERE cat.Active=1
			ORDER BY isAdded,sortOrder,isHubFunctn

			
			
		 END


		 SELECT @Bookmark = 'Favorites' ,@Section='Other Sections'
		 ----------------------------------------------------------------------------------

		 --SELECT *
		 --INTO #TEMP1
		 --FROM #TEMP 
		 --WHERE IsSubCategory = 0 AND ParcatID = 10

		 SELECT * FROM #temp ORDER BY isAdded,sortOrder
		 --EXCEPT
		 --SELECT * FROM #Temp1

		--Confirmation of Success
		SELECT @Status = 0	      	
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_DisplayNewsFirstSideNavigation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
