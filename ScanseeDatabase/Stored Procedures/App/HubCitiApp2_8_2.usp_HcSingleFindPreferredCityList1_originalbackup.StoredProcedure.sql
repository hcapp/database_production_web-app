USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcSingleFindPreferredCityList1_originalbackup]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcSingleFindPreferredCityList]
Purpose					: To display list of Cities which has Retailers for given HubCitiID(RegionApp).
Example					: [usp_HcSingleFindPreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  10/6/2014       SPAN              1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcSingleFindPreferredCityList1_originalbackup]
(
   
    --Input variable	  
	  @UserID int	
	, @HcHubCitiID int
	, @HcMenuItemID int
    , @HcBottomButtonID int
	, @CategoryName varchar(100)
    , @Latitude Decimal(18,6)
    , @Longitude  Decimal(18,6)
    , @Radius int
    , @SearchKey varchar(255)
	, @BusinessSubCategoryID int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @PostalCode varchar(10)
			, @UserOutOfRange bit 
			, @DefaultPostalCode varchar(50)
			, @DistanceFromUser float 
			, @Tomorrow DATETIME = GETDATE() + 1
			, @Yesterday DATETIME = GETDATE() - 1	
            , @RegionAppFlag INT
			, @HcAppListID int, @BusinessCategoryID Int

			--SearchKey implementation
			DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

			SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
					WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
					WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
					ELSE @SearchKey END)

						SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName

			SET @HcBottomButtonId = IIF(@HcBottomButtonId IS NOT NULL,0,Null)

			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppFlag = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiID = @HcHubCitiID                     
                            
			--To check whether user has preferred cities are not.
			DECLARE @UserPreferredCity bit 
			SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID AND HcCityID IS NULL THEN 0
												WHEN HcUserID = @UserID AND HcCityID IS NOT NULL THEN 1
												ELSE 0 END
			FROM HcUsersPreferredCityAssociation
			WHERE HcHubcitiID = @HcHubCitiID AND HcUserID = @UserID

			SELECT @UserPreferredCity = ISNULL(@UserPreferredCity,0)

			--To Fetch region app associated citylist
			CREATE TABLE #CityList(HcCityID int,CityName Varchar(200))
							  
			IF @UserPreferredCity = 0
			BEGIN
				
				INSERT INTO #CityList(HcCityID
									 ,CityName)
				SELECT DISTINCT C.HcCityID 
								,C.CityName					
				FROM HcHubciti H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID 
				INNER JOIN HcCity C ON C.HcCityID =  LA.HcCityID 
				WHERE H.HcHubcitiID = @HcHubCitiID
			END	

			ELSE IF @UserPreferredCity = 1
			BEGIN
				
				INSERT INTO #CityList(HcCityID
									 ,CityName)
				SELECT DISTINCT C.HcCityID 
								,C.CityName					
				FROM HcHubciti H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID 
				INNER JOIN HcCity C ON C.HcCityID = LA.HcCityID 
				INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HcHubCitiID
				WHERE H.HcHubcitiID = @HcHubCitiID
			END 

			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
			EXEC [HubCitiApp2_1].[usp_HcUserHubCitiRangeCheck] @UserID, @HCHubCitiID, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode) 

			--To fetch all the duplicate retailers.
			SELECT DISTINCT DuplicateRetailerID 
			INTO #DuplicateRet
			FROM Retailer 
			WHERE DuplicateRetailerID IS NOT NULL

			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
			BEGIN
					--If the postal code is passed then derive the co ordinates.
					IF @PostalCode IS NOT NULL
					BEGIN
							SELECT @Latitude = Latitude
								, @Longitude = Longitude
							FROM GeoPosition 
							WHERE PostalCode = @PostalCode
					END          
					ELSE
					BEGIN
							SELECT @Latitude = G.Latitude
								, @Longitude = G.Longitude
							FROM GeoPosition G
							INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
							WHERE U.HcUserID = @UserID 
					END                                                                               
			END
			
			--Get the user preferred radius.
			If (@Radius IS NULL)
			BEGIN

				SELECT @Radius = LocaleRadius
				FROM HcUserPreference 
				WHERE HcUserID = @UserID
			END
                           
			SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))  
                    
			SELECT DISTINCT F.BusinessCategoryID
			INTO #BusinessCategory
			FROM  BusinessCategory F				
			LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON FB.BusinessCategoryID=F.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemId
			WHERE F.BusinessCategoryName = @CategoryName AND ((FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID) OR 1=1)			


		
						    select distinct BusinessCategoryID  into #SubCategorytype  from HcBusinessSubCategorytype  WITH(NOLOCK) where BusinessCategoryID=@BusinessCategoryID
	
									 select distinct BusinessCategoryID into #NONSUB from BusinessCategory WITH(NOLOCK)   where BusinessCategoryID=@BusinessCategoryID  
									 except
									 select distinct BusinessCategoryID  from #SubCategorytype WITH(NOLOCK)

									 create  table #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int)

									 select * into #HcMenuFindRetailerBusinessCategories 
									  from HcMenuFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
									  AND HcBusinessSubCategoryID IS NOT NULL
									 and HcMenuItemID=@HcMenuItemId

									  select * into #HcMenuFindRetailerBusinessCategories1 
									  from HcMenuFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
									    AND HcBusinessSubCategoryID IS  NULL
									 and HcMenuItemID=@HcMenuItemId

									 select * into #RetailerBusinessCategory2  from RetailerBusinessCategory where  BusinessCategoryID=@BusinessCategoryID
									 create  index  ix_nn  on #HcMenuFindRetailerBusinessCategories(BusinessCategoryID,
									 HcBusinessSubCategoryID)
									  create clustered index  ix_nn1  on #HcMenuFindRetailerBusinessCategories(HcMenuItemID)

									-- select  count(*)  from #HcMenuFindRetailerBusinessCategories

									 if exists(select  1 from #SubCategorytype)

									 begin
									  
									 if @HcMenuItemId  is not null
									 BEGIN
									 INSERT  INTO #RetailerBusinessCategory1
									 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								
									 INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
									  ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								     AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID

									  INSERT  INTO #RetailerBusinessCategory1
									 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								
									 INNER JOIN #HcMenuFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
									  ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								  --   AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID
									 END

									 else
									 INSERT  INTO #RetailerBusinessCategory1
									 	 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 and RBC.BusinessCategoryID=@BusinessCategoryID
									inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									-- INNER JOIN HcMenuFindRetailerBusinessCategories HCB  ON HCB.BusinessCategoryID=st.BusinessCategoryID
									-- AND RBC.BusinessCategoryID= HCB.BusinessCategoryID
									-- AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID

									end

									else 

									begin

									  INSERT  INTO #RetailerBusinessCategory1
									  SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									  FROM RetailerBusinessCategory RBC
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 and RBC.BusinessCategoryID=@BusinessCategoryID

									 end

									 			
						

									 SELECT DISTINCT #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1

														-- SELECT DISTINCT #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1

									 SELECT distinct   R.RetailID     
														, R.RetailName
														, RL.RetailLocationID 
														, RL.Address1     
														, RL.City
														, RL.State
														, RL.PostalCode,Headquarters,Active
														,RetailLocationLatitude,RetailLocationLongitude,RetailLocationImagePath,WebsiteSourceFlag,RetailerImagePath
														, SaleFlag = 0 ,HL.HchubcitiID into #tempre
											FROM Retailer R 

											INNER JOIN #RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
											 AND RBC.BusinessCategoryID = @BusinessCategoryID
											 INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID  
											INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID 
											--INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
											--INNER JOIN #CityList CL ON  CL.CityID =HL.HcCityID 
				
			-- To identify Retailer that have products on Sale or any type of discount
			SELECT DISTINCT Retailid , RetailLocationid
			INTO #RetailItemsonSale
			FROM 
				(SELECT b.RetailID, a.RetailLocationID 
				FROM RetailLocationDeal a 
				INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID 
				INNER JOIN HcLocationAssociation HL ON b.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = B.RetailLocationID AND HR.HcHubCitiID = @HCHubCitiID AND Associated = 1                               
				--INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
				--                                                        and a.ProductID = c.ProductID
																		and GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) and ISNULL(a.SaleEndDate, @Tomorrow)
				UNION ALL 
				SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
				FROM Coupon C 
				INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				INNER JOIN RetailLocation RL ON RL.RetailID = CR.RetailID
				INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID 
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HCHubCitiID AND Associated = 1                                                                                                  
				LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				GROUP BY C.CouponID
							,NoOfCouponsToIssue
							,CR.RetailID
							,CR.RetailLocationID
				HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                                  
				UNION ALL  

				select  RR.RetailID, 0 as RetaillocationID  
				from Rebate R 
				INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
				INNER JOIN RetailLocation RL ON RL.RetailID = RR.RetailID
				INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID 
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HCHubCitiID AND Associated = 1                                                                                                                
				WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

				UNION ALL  

				SELECT  c.retailid, a.RetailLocationID 
				FROM  LoyaltyDeal a
				INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
				INNER JOIN RetailLocation c on a.RetailLocationID = c.RetailLocationID
				INNER JOIN HcLocationAssociation HL ON c.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID              
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = C.RetailLocationID AND HR.HcHubCitiID = @HCHubCitiID AND Associated = 1
				--INNER JOIN RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
				--                                                and b.ProductID = LDP.ProductID 
				Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

				UNION ALL 

				SELECT DISTINCT rl.RetailID, rl.RetailLocationID
				FROM ProductHotDeal p
				INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
				INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID
				INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID                                                                                   
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HCHubCitiID AND Associated = 1
				LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
				LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
				GROUP BY P.ProductHotDealID
							,NoOfHotDealsToIssue
							,rl.RetailID
							,rl.RetailLocationID
				HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
							ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

				UNION ALL 

				select q.RetailID, qa.RetailLocationID
				from QRRetailerCustomPage q
				INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID=qa.RetailLocationID
				INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID                     
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HCHubCitiID AND Associated = 1
				INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
				where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
				) Discount 

				DECLARE @GuestUser int
				DECLARE @CityPrefNotVisitedUser bit

				SELECT @GuestUser = U.HcUserID
				FROM HcUser U
				INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
				WHERE UserName = 'GuestLogin'
				AND DA.HcHubCitiID = @HcHubCitiID

			
				IF NOT EXISTS (SELECT HcUserID FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HCHubCitiID AND HcUserID = @UserID)
				BEGIN
					SET @CityPrefNotVisitedUser = 1
				END
				ELSE
				BEGIN
					SET @CityPrefNotVisitedUser = 0
				END

				CREATE TABLE #Temp(HcCityID int
								   ,CityName Varchar(100)
								   ,isCityChecked bit)

								  -- select 'gg', * from  #tempre
				INSERT INTO #Temp(HcCityID 
								 ,CityName
								 ,isCityChecked)
					      SELECT HcCityID
							    ,CityName
								,isCityChecked
					      FROM(
					      SELECT DISTINCT  CL.HcCityID
									      ,CL.CityName
										  ,isCityChecked = 1
									      ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
						  FROM Retailer R 
						 
						  INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
						  INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
						  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID    
						   inner join #tempre cat on cat.RetailID=r.RetailID
						  and cat.PostalCode=RL.PostalCode                               
						  INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID AND RL.HcCityID = HL.HcCityID 
						  INNER JOIN #CityList CL ON (@RegionAppFlag = 0) 
						  INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HCHubCitiID  AND Associated =1 
						  LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
						  LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
						  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
						  -----------to get retailer that have products on sale
						  LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
						  LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
						  LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
						  WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
						  AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
						  OR (@SearchKey IS NULL))
						  AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
											--AND ((S.RetailLocationID IS NOT NULL AND S.RetailLocationID = RL.RetailLocationID)
											--            OR
											--     (S.RetailLocationID IS NULL AND 1 = 1))

						 UNION ALL

						 SELECT DISTINCT  CL.HcCityID
									      ,CL.CityName
										  ,isCityChecked = 1
									      ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
						  FROM Retailer R 
						  INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
						  INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
						  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID    
						    inner join #tempre cat on cat.RetailID=r.RetailID
						  and cat.PostalCode=RL.PostalCode                                 
						  INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID AND RL.HcCityID = HL.HcCityID 
						  INNER JOIN #CityList CL ON  (@RegionAppFlag = 1 AND CL.HcCityID = HL.HcCityID) --OR (@RegionAppFlag = 0) 
						  INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HCHubCitiID  AND Associated =1 
						  LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
						  LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
						  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
						  -----------to get retailer that have products on sale
						  LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
						  LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
						  LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
						  WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
						  AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
						  OR (@SearchKey IS NULL))
						  AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
											--AND ((S.RetailLocationID IS NOT NULL AND S.RetailLocationID = RL.RetailLocationID)
											--            OR
											--     (S.RetailLocationID IS NULL AND 1 = 1))

						  ) Retailer
					      WHERE Distance <= @Radius

				SELECT DISTINCT HcCityID 
								,CityName
								,isCityChecked
				INTO #IsCityChecked
				FROM #Temp
				ORDER BY CityName

				SELECT HcCityID
					      ,cityname
						  ,isCityChecked
				INTO #Final
				FROM(
				SELECT DISTINCT I.HcCityID 
								,CityName
								, isCityChecked = CASE WHEN (@GuestUser = @UserID) OR (@CityPrefNotVisitedUser = 1) THEN 1
												WHEN UC.HcUsersPreferredCityAssociationID IS NOT NULL THEN 1
												WHEN UC.HcUserID = @UserID AND UC.HcHubcitiID = @hchubcitiid THEN 1						
												ELSE 0 END
								, Distance1 = 0
				FROM #IsCityChecked I					
				LEFT JOIN HcUsersPreferredCityAssociation UC ON  ((I.HcCityID = UC.HcCityID OR UC.HcCityID IS NULL) AND UC.HcHubcitiID = @HcHubCitiID AND UC.HcUserID = @UserID) OR (@GuestUser = @UserID) OR (@CityPrefNotVisitedUser = 1)
					
				UNION ALL
				SELECT DISTINCT   C.HcCityID
								    , C.CityName				
									, isCityChecked = 0
									 ,Distance1 = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
				FROM Retailer R 
				INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
				INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
				INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID    
				  inner join #tempre cat on cat.RetailID=r.RetailID
						  and cat.PostalCode=RL.PostalCode                                 
				INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID AND RL.HcCityID = HL.HcCityID 
				INNER JOIN HcHubCiti H ON HL.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID  = @HcHubCitiID
				INNER JOIN HcCity C ON  C.HcCityID =HL.HcCityID
				INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HCHubCitiID  AND Associated =1 
				LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
				LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
				LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
				-----------to get retailer that have products on sale
				LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
				LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
				LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
				WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
				--AND C.HcCityID NOT IN (SELECT HcCityID FROM HcUsersPreferredCityAssociation	WHERE HcUserID=@UserID and H.HcHubcitiID=@HcHubCitiID)
				AND C.HcCityID NOT IN (SELECT HcCityID FROM #IsCityChecked)
				AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
				OR (@SearchKey IS NULL))
				AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
				 )A
				 WHERE Distance1 <= @Radius

                SELECT DISTINCT HcCityID AS CityID
					    ,CityName
						,isCityChecked
				FROM #Final
				ORDER BY CityName

			    --Confirmation of Success.
			    SELECT @Status = 0						  
			
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcSingleFindPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




























GO
