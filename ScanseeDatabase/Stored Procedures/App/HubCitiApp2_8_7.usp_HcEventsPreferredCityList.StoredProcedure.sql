USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcEventsPreferredCityList]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcEventsPreferredCityList]
Purpose					: To display list of Cities which has Events for given HubCitiID(RegionApp).
Example					: [usp_HcEventsPreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  9/30/2014       SPAN              1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcEventsPreferredCityList]
(
   
    --Input variable	  
	  @UserID int	
	, @HcHubCitiID int
	, @HcMenuItemID int
    , @HcBottomButtonID int
	, @CategoryID Varchar(100)  --Comma Separated Category IDs
    , @SearchKey varchar(255)
	, @RetailID int
    , @RetailLocationID int
	, @FundRaisingID Int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT @CategoryID = ISNULL(@CategoryID,0)
			SELECT HcHubcitiID
			INTO #RHubcitiList
			FROM
				(SELECT HR.HcHubCitiID
				FROM HcHubCiti H
				LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
				WHERE HR.HcRegionAppID = @HcHubCitiID
				UNION ALL
				SELECT @HcHubCitiID)H
			
			DECLARE @HubCitis varchar(100)
			SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
			FROM #RHubcitiList
						
			IF(@HcMenuItemID IS NOT NULL)
			BEGIN
				--MenuItem	

				SELECT DISTINCT H.HcHubCitiID AS HubcitiID,EC.HcEventCategoryID AS EventCategoryID
								,EC.HcEventCategoryName AS EventCategoryName,E.HcEventID AS EventID
				INTO #AllEventCategories
				FROM HcMenuItemEventCategoryAssociation ME
				INNER JOIN HcEventsCategoryAssociation ECA ON  ME.HcEventCategoryID = ECA.HcEventCategoryID AND  ME.HcMenuItemID = @HcMenuItemID	
				INNER JOIN HcEventsCategory EC ON ECA.HcEventCategoryID = EC.HcEventCategoryID
				INNER JOIN HcEvents E ON ECA.HcEventID = E.HcEventID
				LEFT JOIN HcHubCiti H ON E.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
				WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
							
			END
			ELSE IF(@HcBottomButtonID IS NOT NULL)
			BEGIN
				--BottomButton
								
				SELECT DISTINCT H.HcHubCitiID AS HubcitiID,EC.HcEventCategoryID AS EventCategoryID
								,EC.HcEventCategoryName AS EventCategoryName,E.HcEventID AS EventID
				INTO #AllEventCategories1
				FROM HcBottomButtonEventCategoryAssociation ME
				INNER JOIN HcEventsCategoryAssociation ECA ON  ME.HcEventCategoryID = ECA.HcEventCategoryID AND  ME.HcBottomButtonID = @HcBottomButtonID	
				INNER JOIN HcEventsCategory EC ON ECA.HcEventCategoryID = EC.HcEventCategoryID
				INNER JOIN HcEvents E ON ECA.HcEventID = E.HcEventID
				LEFT JOIN HcHubCiti H ON E.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
				WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1

			END

				CREATE TABLE #Temp(HcCityID int
								,CityName Varchar(100)) 
                     
				--To display List of Events Cities
				IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
				BEGIN

						INSERT INTO #Temp(HcCityID 
										 ,CityName)
							SELECT HcCityID
								  ,CityName
						    FROM(
							SELECT DISTINCT  C.HcCityID
											,C.CityName 
							FROM HcEvents E 
							INNER JOIN #AllEventCategories A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
							INNER JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
							LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
							LEFT JOIN RetailLocation  RL1 ON HA.RetailLocationID = RL1.RetailLocationID OR (RL1.RetailLocationID =EP.RetailLocationID)
							LEFT JOIN HcCity C ON RL1.City = C.CityName
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1

							UNION ALL

							SELECT DISTINCT  C.HcCityID
											,C.CityName
							FROM HcEvents E 
							INNER JOIN #AllEventCategories A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
							INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
							LEFT JOIN HcCity C ON HL.City = C.CityName
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							
							UNION ALL

							SELECT DISTINCT  Hc.HcCityID
											,Hc.CityName
							FROM #AllEventCategories A 
							INNER JOIN HcEvents E on A.EventID = E.HcEventID
							INNER JOIN HcRetailerEventsAssociation RE on E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
							INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND RE.RetailLocationID = R.RetailLocationID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
							INNER JOIN RetailLocation  RL ON RE.RetailLocationID=RL.RetailLocationID AND RL.Active = 1 
							INNER JOIN HcCity HC ON RL.City = HC.CityName 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							
							UNION ALL

							SELECT DISTINCT  Hc.HcCityID
											,Hc.CityName
							FROM #AllEventCategories A 
							INNER JOIN HcEvents E on A.EventID = E.HcEventID AND E.HcHubCitiID IS NULL
							INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and A.EventCategoryID = c.HcEventCategoryID
							INNER JOIN HcEventLocation EL on E.HcEventID = EL.HcEventID
							INNER JOIN GeoPosition  RL1 ON EL.City =RL1.City AND EL.State =RL1.State AND RL1.PostalCode =EL.PostalCode
							INNER JOIN HcCity HC ON EL.City = HC.CityName 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							)E

							
																
				END
				ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
				BEGIN

							INSERT INTO #Temp(HcCityID 
											 ,CityName)
									SELECT  HcCityID
										   ,CityName
							FROM(
							SELECT DISTINCT  C.HcCityID
											,C.CityName 
							FROM HcEvents E 
							INNER JOIN #AllEventCategories1 A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
							INNER JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
							LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
							LEFT JOIN RetailLocation  RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (RL1.RetailLocationID =EP.RetailLocationID)
							LEFT JOIN HcCity C ON RL1.City = C.CityName
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1										   							
							
							UNION ALL

							SELECT DISTINCT  C.HcCityID
											,C.CityName
							FROM HcEvents E 
							INNER JOIN #AllEventCategories1 A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
							INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
							LEFT JOIN HcCity C ON HL.City = C.CityName
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							
							UNION ALL

							SELECT DISTINCT  HC.HcCityID
											,HC.CityName
							FROM #AllEventCategories1 A 
							INNER JOIN HcEvents E on A.EventID = E.HcEventID
							INNER JOIN HcRetailerEventsAssociation RE on E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
							INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND RE.RetailLocationID = R.RetailLocationID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
							INNER JOIN RetailLocation  RL ON RE.RetailLocationID=RL.RetailLocationID  AND RL.Active = 1
							INNER JOIN HcCity HC ON RL.City = HC.CityName 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							
							UNION ALL

							SELECT DISTINCT  HC.HcCityID
											,HC.CityName
							FROM #AllEventCategories1 A 
							INNER JOIN HcEvents E on A.EventID = E.HcEventID AND E.HcHubCitiID IS NULL
							INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
							INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and A.EventCategoryID = c.HcEventCategoryID
							INNER JOIN HcEventLocation EL on E.HcEventID = EL.HcEventID
							INNER JOIN GeoPosition  RL1 ON EL.City =RL1.City AND EL.State =RL1.State AND RL1.PostalCode =EL.PostalCode
							INNER JOIN HcCity HC ON EL.City = HC.CityName 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							)E

				END

				ELSE IF (@RetailID IS NOT NULL)
				BEGIN

					INSERT INTO #Temp(HcCityID 
									,CityName)
							SELECT  HcCityID
									,CityName
							FROM(
							SELECT DISTINCT  HC.HcCityID
											,HC.CityName 
							FROM HcEvents E 
							INNER JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
							INNER JOIN HcRetailerAssociation RA ON RE.RetailID = RA.RetailID AND RE.RetailLocationID = RA.RetailLocationID AND Associated = 1 AND RA.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))	
							INNER JOIN RetailLocation RL1 ON RE.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1
							INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
							INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 	
							INNER JOIN HcCity Hc ON RL1.City = Hc.CityName
							WHERE E.RetailID =@RetailID AND RE.RetailLocationID=@RetailLocationID
							AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							)E

				END

				ELSE IF(@FundRaisingID IS NOT NULL)
				BEGIN
					
					INSERT INTO #Temp(HcCityID 
									,CityName)
					SELECT  HcCityID
							,CityName
					FROM(
					SELECT DISTINCT  HC.HcCityID
									,HC.CityName 
					FROM HcFundraising HF
					INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
					INNER JOIN  HcEvents E ON HF.HcHubCitiID = E.HcHubCitiID AND F.HcEventID = E.HcEventID
					INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
					INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
					LEFT JOIN HcEventLocation HL ON E.HcEventID =HL.HcEventID 
					LEFT JOIN HcEventAppsite EAP ON E.HcEventID = EAP.HcEventID
					LEFT JOIN HcAppSite HA ON EAP.HcAppSiteID = HA.HcAppsiteID
					LEFT JOIN HcEventPackage EP ON EA.HcEventID =EP.HcEventID
					LEFT JOIN RetailLocation RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (EP.RetailLocationID =RL1.RetailLocationID)	
					LEFT JOIN HcRetailerAssociation RA ON RL1.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated =1						                                    
					INNER JOIN HcCity Hc ON RL1.City = Hc.CityName OR HL.City = Hc.CityName
					WHERE F.HcFundRaisingID = @FundRaisingID
					AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1															 					   							
					
					UNION ALL
					SELECT DISTINCT  HC.HcCityID
									,HC.CityName 
					FROM HcFundraising HF
					INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
					INNER JOIN  HcEvents E ON HF.RetailID = E.RetailID AND F.HcEventID = E.HcEventID
					INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
					INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
					LEFT JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
					LEFT JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID 
					LEFT JOIN HcRetailerAssociation RA ON RE.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated =1							 
					LEFT JOIN RetailLocation RL1 ON RE.RetailLocationID =RL1.RetailLocationID AND RL1.Active = 1	
					INNER JOIN HcCity Hc ON RL1.City = Hc.CityName OR HL.City = Hc.CityName
					WHERE F.HcFundRaisingID = @FundRaisingID 
					AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
					)F

				END

				SELECT DISTINCT  HcCityID AS CityID
								,CityName
				FROM #Temp
				--WHERE HcCityID IS NOT NULL

			   --Confirmation of Success.
			   SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcEventsPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






















GO
