USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiAppSiteDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiAppSiteDetails
Purpose					: To display List of appsites.
Example					: usp_HcHubCitiAppSiteDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiAppSiteDetails]
(   
    --Input variable.	
	  @HcUserID Int
	, @HubCitiID int
	, @AppsiteID Int
	, @Latitude Float
	, @Longitude Float
	, @postalcode Varchar(100)
	--User Tracking Inputs 
    --, @LinkID Int
    , @MainMenuID Int
  
	--Output Variable 
	, @UserEmail varchar(1000) OUTPUT 
	, @claimFlag BIT OUTPUT
	, @claimBusiness varchar(1000) OUTPUT
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @RetailConfig varchar(500)
			DECLARE @RetailLocationAdvertisement VARCHAR(1000)--WelcomePage/SplashAd/BannerAd(previously)
			DECLARE @RibbonAdImagePath varchar(1000)--BannerAd
			DECLARE @RibbonAdURL VARCHAR(1000)--BannerAdURL
			DECLARE @RetailerPageQRURL VARCHAR(1000)
			DECLARE @Config VARCHAR(100)
			DECLARE @SplashAdID int
			DECLARE @BannerAdID int	 
			DECLARE @RetailerDetailsID INT
			DECLARE @RetailID Int
			DECLARE @RetailLocationID Int

			--To get server Configuration.
			SELECT @RetailConfig= ScreenContent  
			FROM AppConfiguration   
			WHERE ConfigurationType='Web Retailer Media Server Configuration'	

            --Retrieve the server configuration.
			SELECT @Config = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'QR Code Configuration'
			AND Active = 1 

	        IF @Latitude IS NULL AND @postalcode IS NOT NULL
			BEGIN
				SELECT @Latitude=Latitude
				      ,@Longitude=Longitude
				FROM GeoPosition
				WHERE PostalCode=@postalcode
			END
			IF @Latitude IS NULL AND @postalcode IS NULL
			BEGIN
				SELECT @Latitude=Latitude
				      ,@Longitude=Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON U.PostalCode=G.PostalCode AND U.HcUserID=@HcUserID				
			END
		

			SELECT @RetailID = R.RetailID 
			 , @RetailLocationID = RL.RetailLocationID
			FROM HcAppSite A
			INNER JOIN RetailLocation RL ON RL.RetailLocationID = A.RetailLocationID AND RL.Active=1			
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID AND R.RetailerActive=1
			WHERE A.HcAppSiteID =@AppsiteID
			
				
			--Get the associated QRPage URL.
			
			
			
			SELECT @RetailerPageQRURL = @Config + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') as varchar(10)) + '.htm?retId=' + CAST(QR.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRRCPA .RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))+ CASE WHEN QR.URL IS NOT NULL OR M.MediaPath LIKE '%pdf' OR M.MediaPath LIKE '%png' THEN '&EL=true' ELSE '&EL=false'	END		
			FROM QRRetailerCustomPage QR
			INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QR.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
			INNER JOIN QRTypes Q ON QR.QRTypeID = Q.QRTypeID
			LEFT JOIN QRRetailerCustomPageMedia M ON M.QRRetailerCustomPageID = QR.QRRetailerCustomPageID
			WHERE QR.RetailID = @RetailID
			AND QRRCPA.RetailLocationID = @RetailLocationID
			AND Q.QRTypeName = 'Anything Page'

			--Get the associated Splash Ad/ Welcome Page Details.
			SELECT @RetailLocationAdvertisement =  @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.SplashAdImagePath
				 , @SplashAdID = RLA.AdvertisementSplashID
			FROM AdvertisementSplash RLA
			INNER JOIN RetailLocationSplashAd RL ON RL.RetailLocationID = RL.RetailLocationID
			WHERE RL.RetailLocationID = @RetailLocationID
			AND RLA.RetailID = @RetailID
			AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)
			AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementSplash ASP 
								 INNER JOIN  RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID = RLS.AdvertisementSplashID
								 WHERE RetailLocationID = @RetailLocationID)

			--Get the associated Banner Ad Details.
			SELECT @RibbonAdImagePath = @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
				 , @RibbonAdURL = RLA.BannerAdURL
				 , @BannerAdID = RLA.AdvertisementBannerID
			FROM AdvertisementBanner RLA
			INNER JOIN RetailLocationBannerAd RL ON RL.RetailLocationID = RL.RetailLocationID
			WHERE RL.RetailLocationID = @RetailLocationID
			AND RLA.RetailID = @RetailID
			AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1) --if the end date is set null then that ad is always active.
			AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementBanner ASP 
								 INNER JOIN  RetailLocationBannerAd RLS ON ASP.AdvertisementBannerID = RLS.AdvertisementBannerID
								 WHERE RetailLocationID = @RetailLocationID)
           

		    --Find if there is any sale or discounts associated with the Retail Location.
			select distinct Retailid , RetailLocationid
			into #RetailItemsonSale
			from 
			(select b.RetailID, a.RetailLocationID 
			from RetailLocationDeal a 
			inner join RetailLocation b on a.RetailLocationID = b.RetailLocationID AND b.Active=1
			inner join RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
											   and a.ProductID = c.ProductID 
											   AND A.RetailLocationID = @RetailLocationID
											   and GETDATE() between isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)
			union 
			SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
			FROM Coupon C 
			INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
			LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID
			WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
			AND CR.RetailLocationID = @RetailLocationID
			GROUP BY C.CouponID
					,NoOfCouponsToIssue
					,CR.RetailID
					,CR.RetailLocationID
			HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
					 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)								   
			union 
			select  RR.RetailID, RR.RetailLocationID  
			from Rebate R 
			inner join RebateRetailer RR ON R.RebateID=RR.RebateID
			where RR.RetailLocationID = @RetailLocationID
			AND GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
			union 
			select  c.retailid, a.RetailLocationID 
			from  LoyaltyDeal a
			INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
			inner join RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
										and a.ProductID = LDP.ProductID 
			inner join RetailLocation c on a.RetailLocationID = b.RetailLocationID AND c.Active=1
			Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
			AND A.RetailLocationID = @RetailLocationID
			union
			SELECT DISTINCT rl.RetailID, rl.RetailLocationID
			FROM ProductHotDeal p
			INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
			INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active=1
			LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
			LEFT JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
			WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
			AND rl.RetailLocationID = @RetailLocationID 
			GROUP BY P.ProductHotDealID
					,NoOfHotDealsToIssue
					,rl.RetailID
					,rl.RetailLocationID
			HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
					 ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)  
		
			union
			select q.RetailID, qa.RetailLocationID
			from QRRetailerCustomPage q
			inner join QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
			inner join QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
			where qa.RetailLocationID = @RetailLocationID 
			and GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE()+1)) a

			--To display List fo AppSites
			SELECT DISTINCT HcAppSiteID  appSiteId--
				  ,HcHubCitiID hubCitiId--
				  ,HcAppSiteName appSiteName--				
				  , R.RetailID retailerId
				 , R.RetailName retailerName
				 , RL.RetailLocationID retailLocationID
				 , RL.Address1+','+RL.City+','+RL.State+','+RL.PostalCode  retaileraddress1
				 , ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State )) retLatitude
				 , ISNULL(RL.RetailLocationLongitude, (SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) retLongitude
				 , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, (SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude,(SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958))))*6371) * 0.6214,1,1)   
				 , CASE WHEN RL.CorporateAndStore=1 THEN R.CorporatePhoneNo ELSE C.ContactPhone END contactPhone
				 , CASE WHEN RL.RetailLocationURL IS NULL THEN R.RetailURL ELSE RL.RetailLocationURL END retailerURL
				 , @RetailLocationAdvertisement bannerAdImagePath
				 , @RibbonAdImagePath ribbonAdImagePath
				 , @RibbonAdURL ribbonAdURL
				 , image = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
				 , @RetailerPageQRURL retailerPageQRURL 
				 , SaleFlag = CASE WHEN (SELECT COUNT(1) FROM #RetailItemsonSale)>0 THEN 1 ELSE 0 END
				 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')		
				 , @BannerAdID RibbonAdID
				 , @SplashAdID banAdID					  			
				  --,Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN RL.RetailLocationLongitude IS NULL THEN G.Longitude ELSE RL.RetailLocationLongitude END/ 57.2958))))*6371) * 0.6214 END				  		   
			FROM HcAppSite A
			INNER JOIN RetailLocation RL ON RL.RetailLocationID =A.RetailLocationID AND RL.Active=1
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID AND R.RetailerActive=1
			INNER JOIN GeoPosition G ON G.PostalCode=RL.PostalCode
			LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
		    LEFT JOIN Contact C ON C.ContactID = RC.ContactID
			WHERE HcHubCitiID= @HubCitiID AND HcAppSiteID =@AppsiteID 
			
			
			--UserTrcaking
			
			--Capture the AppSite click and impression
			INSERT INTO HubCitiReportingDatabase..AppsiteList(AppsiteID 
															 ,AppsiteClick 
															 ,MainMenuID 
															 ,DateCreated)
			SELECT @AppsiteID
			      ,1
			      ,@MainMenuID
			      ,GETDATE()

			SELECT @UserEmail =  ISNULL(Email,'support@hubcitiapp.com') FROM HcUser WHERE HcuserID = @HcUserID

			SELECT  @claimFlag = ISNULL(claimFlag,0) 
				  , @claimBusiness = CASE WHEN claimFlag = 0 OR ClaimFlag IS NULL THEN 'If this is your business, Claim It! - Free! <br/> Add or update your Website address, Phone <br/> Number, and ''''About Us''''. See more options…'
										  WHEN claimFlag = 1 THEN 'Already been Claimed' END
			FROM RetailLocation WHERE RetailLocationID = @RetailLocationID

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiAppSiteDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
