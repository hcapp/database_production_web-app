USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcCouponsBusinessCategoryDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcCouponsBusinessCategoryDisplay]
Purpose					: To Display All Coupons Category list
Example					: [usp_HcCouponsBusinessCategoryDisplay]

History
Version		 Date		     Author	           Change Description
------------------------------------------------------------------------------- 
1.0        12/26/2016    Shilpashree       Category List in Sort & Filter screen
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcCouponsBusinessCategoryDisplay]
(
	--Input Variables 	
	   @UserID int
	 , @HcHubCitiID int
	 , @RetailID int
	 , @SearchKey varchar(500)
	 , @Latitude Decimal(18,6)
	 , @Longitude Decimal(18,6)
	 , @PostalCode varchar(100)	
	 
	--Output Variables
	 , @Status bit output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
		--To get Media Server Configuration.  
		DECLARE @DistanceFromUser FLOAT
				, @Radius FLOAT
				, @LocCategory Int
				, @DefaultPostalCode Varchar(10)
				, @UserOutOfRange bit
		
		SET @RetailID = ISNULL(@RetailID,0)

		CREATE TABLE #RetailerBusinessCategory(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)
		CREATE TABLE #FilteredCoupons(CouponID int, HcHubCitiID int, RetailID int, RetailLocationID int,NoOfCouponsToIssue int,UsedCoupons int)
		
		SELECT @Radius = LocaleRadius
			FROM HcUserPreference 
				WHERE HcUserID = @UserID

		IF (@Radius IS NULL)
		BEGIN

			SELECT @Radius=ScreenContent 
				FROM AppConfiguration
					WHERE ScreenName = 'DefaultRadius' AND ConfigurationType = 'DefaultRadius'	
		END	

		EXEC [HubCitiapp2_8_7].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubCitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
						, @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
						, @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @UserID
			END
		END	

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
			FROM Retailer 
				WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive = 1

		SELECT DISTINCT BusinessCategoryID,HcBusinessSubCategoryID 
		INTO #UserPrefLocCategories
			FROM HcUserPreferredCategory 
				WHERE HcUserID = @UserID AND HcHubCitiID = @HcHubCitiID 

		SELECT @LocCategory = COUNT(1) FROM #UserPrefLocCategories 

		IF (@LocCategory = 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
					WHERE RA.HcHubCitiID = @HcHubCitiID
		END
		ELSE IF (@LocCategory > 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
				INNER JOIN #UserPrefLocCategories PC ON RBC.BusinessCategoryID = PC.BusinessCategoryID --AND RBC.BusinessSubCategoryID = PC.HcBusinessSubCategoryID
					WHERE RA.HcHubCitiID = @HcHubCitiID
		END
		
		--INSERT HubCiti Coupons
		INSERT INTO #FilteredCoupons(CouponID,HcHubCitiID,RetailID,RetailLocationID,NoOfCouponsToIssue,UsedCoupons)
		SELECT DISTINCT C.CouponID
						,C.HcHubCitiID
						,CR.RetailID
						,CR.RetailLocationID
						,NoOfCouponsToIssue
						,UsedCoupons = COUNT (HCUserCouponGalleryID)
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID 
			INNER JOIN HcRetailerAssociation RA ON CR.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND RA.Associated = 1 
			LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
				WHERE C.HcHubCitiID = @HcHubCitiID AND C.RetailID IS NULL
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
					GROUP BY C.CouponID
							,C.HcHubCitiID
							,CR.RetailID
							,CR.RetailLocationID
							,NoOfCouponsToIssue
						HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
	
		--INSERT HubCiti Associted Retailer Coupons
		INSERT INTO #FilteredCoupons(CouponID,RetailID,RetailLocationID,NoOfCouponsToIssue,UsedCoupons)
		SELECT DISTINCT C.CouponID
						,CR.RetailID
						,CR.RetailLocationID
						,NoOfCouponsToIssue
						,UsedCoupons = COUNT (HCUserCouponGalleryID)
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID AND C.RetailID = CR.RetailID
			INNER JOIN HcRetailerAssociation RA ON CR.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND RA.Associated = 1 
			LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
				WHERE C.HcHubcitiID IS NULL
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
					GROUP BY C.CouponID
							,CR.RetailID
							,CR.RetailLocationID
							,NoOfCouponsToIssue
						HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
		
		SELECT DISTINCT FC.CouponID
					,RBC.BusinessCategoryID
					--,RBC.BusinessSubCategoryID
					,Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
		INTO #RetailerSet
		FROM #FilteredCoupons FC
		INNER JOIN Coupon C ON FC.CouponID = C.CouponID
		INNER JOIN #RetailerBusinessCategory RBC  ON FC.RetailID = RBC.RetailerID
		INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1
		INNER JOIN Retaillocation RL ON R.RetailID = RL.RetailID AND FC.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
		INNER JOIN HcLocationAssociation HL ON RL.HcCityID = HL.HcCityID AND RL.PostalCode = HL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL
		AND ((@RetailID <> 0 AND FC.RetailID = @RetailID) OR @RetailID = 0)
		AND (C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
				OR C.KeyWords LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END)
		
		SELECT DISTINCT BC.BusinessCategoryID AS busCatId   
				,BC.BusinessCategoryDisplayValue AS busCatName  
			FROM #RetailerSet RS
			INNER JOIN BusinessCategory BC ON RS.BusinessCategoryID = BC.BusinessCategoryID
				WHERE Distance <= @Radius
					ORDER BY BusinessCategoryDisplayValue

		--Confirmation of Success.
		SELECT @Status = 0
				
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcCouponsBusinessCategoryDisplay].'		
			EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
			--Confirmation of failure.
			SELECT @Status = 1
		
		END;
		 
	END CATCH;
END;




GO
