USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserTrackingBandShare]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcUserTrackingBandShare 
Purpose               : To capure the deails of the Bands share.  
Example               : usp_HcUserTrackingBandShare 
  
History  
Version    Date				Author				Change Description  
---------------------------------------------------------------   
1.0        7/18/2016		Bindu T A			Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserTrackingBandShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @BandID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         --Display Module Details
		 INSERT INTO HubCitiReportingDatabase..ShareBand(MainMenuID
														   ,ShareTypeID
														   ,TargetAddress
														   ,BandID  
														   ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@BandID  
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0       							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingBandShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1     
  END;  
     
 END CATCH;  
END;














































GO
