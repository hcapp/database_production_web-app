USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcCouponShare]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcCouponShare
Purpose					: 
Example					: usp_HcCouponShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13thOct 2013    Dhananjaya TR	    Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcCouponShare]
(
	  @CouponID int
    , @HcHubcitiID int 
	, @UserID int
	
	--Output Variable 
	--, @CouponExpired bit Output
	, @ShareText varchar(500) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		    --To get Server Configuration 
			 DECLARE @RetailConfig varchar(50)
			 DECLARE @Config varchar(100)--, @RetailerConfig varchar(1000),@Configg varchar(1000)
	DECLARE @Configgg varchar(50) 
			 , @RetailerConfiggg VARCHAR(500) 



			
		SELECT @Configgg=ScreenContent  
			FROM AppConfiguration   
				WHERE ConfigurationType='Hubciti Media Server Configuration' 

		SELECT @RetailerConfiggg= ScreenContent  
			FROM AppConfiguration   
				 WHERE ConfigurationType='Web Retailer Media Server Configuration'



			
		SELECT @Config=ScreenContent  
			FROM AppConfiguration   
				WHERE ConfigurationType='Hubciti Media Server Configuration' 

		  
			 SELECT @RetailConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'

		   --Retrieve the server configuration.
            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 

			--To get the text used for share.         
            DECLARE @HubCitiName varchar(100)
		 
			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubcitiID
		       
			SELECT @ShareText = 'I found this Deal in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti Coupon Share Text'    
			 
		    --SET @CouponExpired = 0

			

			SELECT DISTINCT CouponName
				   --,CouponURL
				   --,CouponStartDate
				   --,CouponExpireDate
				  -- ,CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp2_8_2.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
						--																				CASE WHEN WebsiteSourceFlag = 1 
						--																					THEN @RetailConfig
						--																					+CONVERT(VARCHAR(30),CR.RetailID)+'/'
						--																					+CouponImagePath 
						--																			    ELSE CouponImagePath 
						--																			    END
						--																		 END 
					 --END
									, CouponImagePath = CASE WHEN C.HcHubCitiID IS NOT NULL THEN @Configgg + CAST(@HcHubCitiID as varchar(10))+'/' + CouponDetailImage 
											 WHEN C.RetailID IS NOT NULL AND C.HcHubCitiID IS NULL AND CouponDetailImage  = 'uploadIconSqr.png' THEN @RetailerConfiggg + CouponDetailImage ELSE @RetailerConfiggg + CAST(C.RetailID as varchar(10))+'/' + CouponDetailImage 
											 END
					 
				
				   ,QRUrl = @Config+'2700.htm?couponId='+ CAST(@CouponID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HcHubCitiId AS VARCHAR(10))  			
			FROM Coupon c
			LEFT JOIN CouponRetailer cr on c.CouponID = cr.CouponID
			WHERE c.CouponID=@CouponID 
			--AND GETDATE() Between CouponStartDate AND CouponExpireDate 
			
			--SELECT @CouponExpired=CASE WHEN GETDATE()>CouponExpireDate Then 1 else 0 END 
			--FROM Coupon
			--WHERE CouponID=@CouponID		
			
			SET @Status = 0 
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcCouponShare.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status = 1
			
		END;
		 
	END CATCH;
END;














































GO
