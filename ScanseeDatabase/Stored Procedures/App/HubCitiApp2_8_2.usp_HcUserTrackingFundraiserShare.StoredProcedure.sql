USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserTrackingFundraiserShare]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_HcUserTrackingFundraiserShare 
Purpose					: To track fundraiser share details.  
Example					: usp_HcUserTrackingFundraiserShare
  
History  
Version    Date				Author			Change         Description  
----------------------------------------------------------------------   
1.0        5th Feb 2014		Mohith H R		Initial Version  
---------------------------------------------------------------------- 
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserTrackingFundraiserShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @FundraiserID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         
		 INSERT INTO HubCitiReportingDatabase..ShareFundraiserPage(MainMenuID
														  ,ShareTypeID
														  ,TargetAddress
														  ,FundraiserID
														  ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@FundraiserID 
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0           							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingFundraiserShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1 
  END;            
 END CATCH;  
END;











































GO
