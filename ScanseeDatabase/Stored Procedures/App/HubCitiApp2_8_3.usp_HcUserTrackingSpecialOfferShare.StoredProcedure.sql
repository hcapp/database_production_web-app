USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserTrackingSpecialOfferShare]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_HcUserTrackingAnythingSpecialShare 
Purpose					: To track special offer share details.  
Example					: usp_HcUserTrackingAnythingSpecialShare
  
History  
Version    Date				Author			Change         Description  
----------------------------------------------------------------------   
1.0        7th Nov 2013		Mohith H R		Initial Version  
---------------------------------------------------------------------- 
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserTrackingSpecialOfferShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @SpecialPageID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         
		 INSERT INTO HubCitiReportingDatabase..ShareSpecialOffer(MainMenuID
														  ,ShareTypeID
														  ,TargetAddress
														  ,SpecialOfferID
														  ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@SpecialPageID 
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0           							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingAnythingSpecialShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1 
  END;            
 END CATCH;  
END;










































GO
