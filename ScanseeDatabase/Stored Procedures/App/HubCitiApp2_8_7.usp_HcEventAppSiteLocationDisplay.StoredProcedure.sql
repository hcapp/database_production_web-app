USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcEventAppSiteLocationDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcEventAppSiteLocationDisplay
Purpose					: To display List fo Event and it's location details.
Example					: usp_HcEventAppSiteLocationDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04/02/2014	    SPAN	1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcEventAppSiteLocationDisplay]
(   
    --Input variable.	  
	  @HcEventID int
	, @UserID int
	, @Latitude float
	, @Longitude float
	, @HubCitiID int
	
	--Output Variable 	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output  
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @RetailConfig Varchar(100)

			CREATE TABLE #RetailerList
			(appSiteId int,
			 appSiteName VARCHAR(1000),
			 address VARCHAR(1000),
			 City varchar(1000),
			 State varchar(1000),
			 PostalCode varchar(1000),
			 latitude float,
			 longitude float,
			 Distance float,
			 logoImagePath varchar(1000),
			 retailerId int,
			 retailLocationId int 
			)

			--derive Latitude and Longitude if its not passed in the input
			IF @Latitude IS NULL AND @Longitude IS NULL
			BEGIN
				SELECT @Latitude = Latitude
					 , @Longitude = Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON U.PostalCode = G.PostalCode
				WHERE U.HcUserID = @UserID
			END

			--derive Latitude and Longitude if user has not set postal code.
			IF @Latitude IS NULL AND @Longitude IS NULL
			BEGIN
				
				SELECT @Latitude = Latitude
					 , @Longitude = Longitude
				FROM GeoPosition G
				INNER JOIN HcHubCiti H ON H.DefaultPostalCode = G.PostalCode
				WHERE H.HcHubCitiID = @HubCitiID
			END 


			SELECT @RetailConfig = ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'

			CREATE TABLE #RHubcitiList(HchubcitiID Int) 
					INSERT INTO #RHubcitiList( HchubcitiID)
					SELECT HcHubCitiID
					FROM
						(SELECT RA.HcHubCitiID
						FROM HcHubCiti H
						LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
						WHERE H.HcHubCitiID  = @HubcitiID
						UNION ALL
						SELECT @HubcitiID)A

			DECLARE @HubCitis varchar(100)
			SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
			FROM #RHubcitiList

			IF EXISTS(SELECT 1 
			              FROM HcEvents E
						  INNER JOIN HcEventAppsite EA ON EA.HcEventID =E.HcEventID AND EA.HcEventID=@HcEventID 
						                              AND EA.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,',')))
            BEGIN
				
				insert into #RetailerList
				SELECT DISTINCT HA.HcAppSiteID appSiteId
					  ,HA.HcAppSiteName appSiteName
					  ,RL.Address1 address
					  ,RL.City
					  ,RL.State
					  ,RL.PostalCode
					  ,RL.RetailLocationLatitude latitude
					  ,RL.RetailLocationLongitude longitude
					  ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
					  --,logoImagePath = IIF( RetailerImagePath like '%www.%' , RetailerImagePath , @RetailConfig + CONVERT(VARCHAR(30),R.RetailID) + '/' + RetailerImagePath)
					  ,logoImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 	
					  ,R.RetailID retailerId
					  ,RL.RetailLocationID retailLocationId
				
				FROM HcAppsite HA
				INNER JOIN RetailLocation RL ON HA.RetailLocationID = RL.RetailLocationID	
				INNER JOIN Retailer R ON RL.RetailID = R.RetailID	
				INNER JOIN HcEventAppsite HL ON HA.HcAppSiteID = HL.HcAppsiteID AND HL.HcEventID = @HcEventID AND HL.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,',')) 
			    LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode	
				WHERE RL.Active = 1 AND R.RetailerActive = 1
				ORDER BY Distance			    
            END


			ELSE IF EXISTS(SELECT 1 
			              FROM HcEvents E						 
						  INNER JOIN HcRetailerEventsAssociation REA ON REA.HcEventID =E.HcEventID AND REA.HcEventID=@HcEventID)
            BEGIN

				insert into #RetailerList
				SELECT DISTINCT RL.RetailLocationID appSiteId
						  ,R.RetailName appSiteName
						  ,RL.Address1 address
						  ,RL.City
						  ,RL.State
						  ,RL.PostalCode
						  ,RL.RetailLocationLatitude latitude
						  ,RL.RetailLocationLongitude longitude
						  ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
						  --,logoImagePath = IIF( RetailerImagePath like '%www.%' , RetailerImagePath , @RetailConfig + CONVERT(VARCHAR(30),R.RetailID) + '/' + RetailerImagePath)
						  ,logoImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 	
						  ,R.RetailID retailerId
						  ,RL.RetailLocationID retailLocationId
					
					FROM  RetailLocation RL 	
					INNER JOIN Retailer R ON RL.RetailID = R.RetailID				
					INNER JOIN HcRetailerEventsAssociation REA ON REA.HcEventID =@HcEventID  AND RL.RetailLocationID =REA.RetailLocationID 
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode 
					WHERE RL.Active = 1	AND R.RetailerActive = 1
					ORDER BY Distance 				
			 END


			 SELECT appSiteId ,
					 appSiteName ,
					 address ,
					 City ,
					 State ,
					 PostalCode ,
					 latitude ,
					 longitude ,
					 Distance ,
					 logoImagePath ,
					 retailerId ,
					 retailLocationId  
			 FROM #RetailerList

			---------------Capture the impressions of the Retailer list.---------------- 3/21/2016-------------
            INSERT INTO HubCitiReportingDatabase..RetailerList(--MainMenuID	,																
																	 RetailID
																	, RetailLocationID
																	--, HcCitiExperienceID
																	--, FindCategoryID 
																	, DateCreated)
			--OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)							
													SELECT --@MainMenuId,
														    retailerId
														 , RetailLocationID
														--, @CityExperienceID 
														 --, CASE WHEN @CategoryID IS NOT NULL THEN NULL ELSE 0 END  --CASE WHEN @CategoryID IS NOT NULL THEN P.Param ELSE NULL END
														 , GETDATE()
													FROM #RetailerList
			----------------------------------------------------------------------------------------------------

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcEventAppSiteLocationDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
