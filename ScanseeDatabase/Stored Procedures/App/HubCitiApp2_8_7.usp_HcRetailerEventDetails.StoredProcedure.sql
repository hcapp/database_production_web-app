USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcRetailerEventDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcRetailerEventDetails
Purpose					: To display Retailer Event Details.
Example					: usp_HcRetailerEventDetails

History
Version		  Date			 Author		Change Description
--------------------------------------------------------------- 
1.0			2nd Aug 2014     SPAN          1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcRetailerEventDetails]
(   
    
	--Input variable	  
	  @HcEventID Int

	--User Tracking
	, @EventsListID Int
  
	--Output Variable 
	, @RetailLocationFlag bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @HcEventCategoryID Varchar(2000)
			DECLARE @RetailLocationID Varchar(2000)
			DECLARE @Config VARCHAR(500)
			DECLARE @WeeklyDays Varchar(2000)
			DECLARE @CategoryNames VARCHAR(1000)

	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT ID, Day
			INTO #DayLookup
			FROM 			
			(
				SELECT 1 AS 'ID', 'Sunday' 'Day'
				UNION
				SELECT 2, 'Monday'
				UNION
				SELECT 3, 'Tuesday'
				UNION
				SELECT 4, 'Wednesday'
				UNION
				SELECT 5, 'Thursday'
				UNION
				SELECT 6, 'Friday'
				UNION
				SELECT 7, 'Saturday')A
			
			SELECT @HcEventCategoryID= COALESCE(@HcEventCategoryID+',', '')+CAST(A.HcEventCategoryID AS VARCHAR)
				 , @CategoryNames = COALESCE(@CategoryNames+',', '')+B.HcEventCategoryName
			FROM HcEventsCategoryAssociation A
			INNER JOIN HcEventsCategory B ON A.HcEventCategoryID = B.HcEventCategoryID
			WHERE HcEventID = @HcEventID 
		
			SELECT @WeeklyDays = COALESCE(@WeeklyDays+',', '')+CAST([DayName] AS VARCHAR)
			FROM HcEventInterval
			--INNER JOIN HcEventInterval E ON H.HcEventID = E.HcEventID
			WHERE HcEventID = @HcEventID 

			SELECT @RetailLocationFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
			FROM HcRetailerEventsAssociation 
			WHERE HcEventID = @HcEventID  

			--To get Appsite when Location selected sending RetailID & RetailLocationID.
			DECLARE @RetailID int
			DECLARE @EventRetailLocationID int

			IF @RetailLocationFlag = 1
			BEGIN
				SELECT @RetailID = RetailID
					  ,@EventRetailLocationID = RetailLocationID
				FROM HcRetailerEventsAssociation
				WHERE HcEventID = @HcEventID  
			END

			--To display Event Details.
			SELECT DISTINCT HcEventName eventName 
			               ,ShortDescription shortDes
                           ,LongDescription longDes								
						   ,E.HcHubCitiID  hubCitiId
						   ,ImgPath=@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath 
						   ,StartDate =CAST(StartDate AS DATE)
						   ,EndDate =CAST(EndDate AS DATE)
						   ,StartTime =CAST(StartDate AS Time)
						   ,EndTime =CAST(EndDate AS Time)
						   ,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		ELSE EL.Address 
										END
							,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcRetailerEventsAssociation A 
																		INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																		WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		ELSE EL.City 
									END
							,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcRetailerEventsAssociation A 
																		INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																		WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		ELSE EL.State 
									END
							,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		ELSE EL.PostalCode 
									END  	
						
						    ,latitude = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.RetailLocationLatitude FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		ELSE EL.Latitude 
									END  	
							,longitude = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.RetailLocationLongitude FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID AND RL.Active=1)
																		ELSE EL.Longuitude 
									END 
						   ,@HcEventCategoryID  eventCatIds		
						   ,MoreInformationURL moreInfoURL
						   ,ISNULL(OnGoingEvent,0) isOnGoing
						   ,recurringDays = CASE WHEN R.RecurrencePattern = 'Daily'
										 THEN CASE WHEN EI.HcEventIntervalID IS NULL 
																THEN 'Occurs Every ' +IIF(E.RecurrenceInterval = 1, '',  CAST(E.RecurrenceInterval AS VARCHAR(100))+' ') + 'Day(s)' 	
															  ELSE 'Occurs Every Weekday' 
											  END
									    WHEN R.RecurrencePattern = 'Weekly'
											THEN 'Occurs Every '+IIF(RecurrenceInterval = 1, '', CAST(RecurrenceInterval AS VARCHAR(10))+' ') + 'Week(s) on ' + STUFF((SELECT ', ' + [DayName]
																																										FROM HcEventInterval F			
																																										LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																																										WHERE F.HcEventID = E.HcEventID
																																										ORDER BY L.ID
																																										FOR XML PATH('')), 1, 2, '')
										WHEN R.RecurrencePattern = 'Monthly'
											THEN CASE WHEN [DayName] IS NULL THEN 'Day '+ CAST(DayNumber AS VARCHAR(10)) + ' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
													  ELSE 'The ' + CASE WHEN DayNumber = 1 THEN 'First '
																		 WHEN DayNumber = 2 THEN 'Second '
																		 WHEN DayNumber = 3 THEN 'Third '
																		 WHEN DayNumber = 4 THEN 'Fourth '
																		 WHEN DayNumber = 5 THEN 'Last ' END													  
													   +  STUFF((SELECT ', ' + [DayName]
																 FROM HcEventInterval F			
																 LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																 WHERE F.HcEventID = E.HcEventID
																 ORDER BY L.ID
																 FOR XML PATH('')), 1, 2, '') +' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
												 END
								  END
							,@RetailID RetailID
							,@EventRetailLocationID RetailLocationID							
													  	  											
			FROM HcEvents E
			LEFT JOIN HcRetailerEventsAssociation RE ON RE.HcEventID = E.HcEventID
			LEFT JOIN RetailLocation RL ON RE.RetailLocationID = RL.RetailLocationID AND RL.Active=1
			LEFT JOIN HcEventLocation EL ON E.HcEventID = EL.HcEventID
			LEFT JOIN HcEventInterval EI ON EL.HcEventID = EI.HcEventID
			LEFT JOIN HcEventRecurrencePattern R ON R.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
			WHERE E.HcEventID = @HcEventID				  	  														

			--User Tracking

			Update HubCitiReportingDatabase..EventsList SET EventClick=1
			WHERE EventsListID = @EventsListID
				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcRetailerEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







































GO
