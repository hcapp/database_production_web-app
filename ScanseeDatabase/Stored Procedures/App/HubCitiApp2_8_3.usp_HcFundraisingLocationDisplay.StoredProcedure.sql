USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFundraisingLocationDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : usp_HcFundraisingLocationDisplay
Purpose                    : To display List of FundraisingEvents.
Example                    : usp_HcFundraisingLocationDisplay

History
Version              Date               Author     Change Description
--------------------------------------------------------------------------------- 
1.0                  20/07/2015         SPAN        1.0
---------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFundraisingLocationDisplay]
(
		@UserID INT
	   ,@HubcitiID INT
	   ,@Latitude FLOAT
	   ,@Longitude FLOAT
	   ,@Postalcode VARCHAR(10)
	   ,@FundraisingID INT
       --,@AppSiteFlag BIT 

       --Output Variable

	   , @HcAppSiteFlag BIT output  -- (1-HubcitiAppsite, 0-Retaillocation in retailer admin, NULL-Organization)
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

		BEGIN TRY
					 DECLARE @UpperLimit int
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @UserLatitude DECIMAL(16,9)
                     DECLARE @UserLongitude DECIMAL(16,9) 
                     DECLARE @RetailConfig Varchar(1000)
					 DECLARE @UserOutOfRange bit 
					 DECLARE @DefaultPostalCode VARCHAR(10) 

                     SELECT @RetailConfig = ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Retailer Media Server Configuration'   

					  IF @Latitude IS NULL AND @Postalcode IS NOT NULL
					 BEGIN
						SELECT @Latitude =Latitude 
						      ,@Longitude =Longitude
						FROM GeoPosition
						WHERE PostalCode =@Postalcode 
						
					 END

                     SELECT @UserLatitude = @Latitude
                           ,@UserLongitude = @Longitude                  

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                 ,@UserLongitude = Longitude
                           FROM HcUser A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE HcUserID = @UserID 
                     END
                     --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                 ,@UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HubCitiID
                     END

                     print @UserLatitude
                     print @UserLongitude
					    

                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)

                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @Postalcode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                  WHERE U.HcUserID = @UserID
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @Postalcode
                           END
                     END

                     DECLARE @Config VARCHAR(500)
					 SELECT @Config = ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'

					  CREATE TABLE #RHubcitiList(HchubcitiID Int) 
					INSERT INTO #RHubcitiList( HchubcitiID)
					SELECT HcHubCitiID
					FROM
						(SELECT RA.HcHubCitiID
						FROM HcHubCiti H
						LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
						WHERE H.HcHubCitiID  = @HubcitiID
						UNION ALL
						SELECT @HubcitiID)A

			DECLARE @HubCitis varchar(100)
			SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
			FROM #RHubcitiList
          
            --Fundraisers in Hubciti admin having APPSITES
			IF EXISTS(SELECT 1 
			          FROM HcFundraising WHERE HcFundraisingID=@FundraisingID AND FundraisingOrganizationName IS NULL AND HcHubCitiID IS NOT NULL)-- AND @AppSiteFlag = 1)
			BEGIN			
			
			--Table to insert fundraisers
			CREATE TABLE #R1(HcAppsiteID INT
							,HcAppSiteName VARCHAR(1000)
							,CompleteAddress VARCHAR(1000)
							,ImagePath VARCHAR(1000)
							,RetailID INT
							,RetailLocationID INT
							,RetailLocationLatitude FLOAT
							,RetailLocationLongitude FLOAT
							)
			
			INSERT INTO #R1							
			SELECT DISTINCT  B.HcAppsiteID AS appSiteId
							,C.HcAppSiteName AS appSiteName
							--,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
							,completeAddress = CAST((ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) AS VARCHAR(100))+'mi'+','+SPACE(1)+RL.Address1+','+SPACE(1)+RL.City+','+SPACE(1)+RL.State+','+SPACE(1)+RL.PostalCode
							,imagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 	
							,R.RetailID retailId
							,RL.RetailLocationID retailLocationId
							,RL.RetailLocationLatitude latitude
							,RL.RetailLocationLongitude longitude
			FROM HcFundraising A 
			INNER JOIN HcFundraisingAppsiteAssociation B ON A.HcFundraisingID=B.HcFundraisingID --AND B.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,',')) 
			INNER JOIN HcAppSite C ON C.HcAppSiteID=B.HcAppsiteID 
			INNER JOIN Retaillocation RL ON RL.RetailLocationID=C.RetailLocationID AND RL.Active = 1
			INNER JOIN Retailer R ON R.RetailID=RL.RetailID AND R.RetailerActive = 1
			LEFT JOIN GeoPosition G ON G.PostalCode=RL.PostalCode
			WHERE A.Active = 1 AND A.HcFundraisingID=@FundraisingID
 
			SET @HcAppSiteFlag = 1

			--To display Fundraisers
			SELECT	 HcAppsiteID appSiteId
					,HcAppSiteName appSiteName
					,CompleteAddress completeAddress
					,ImagePath imagePath
					,RetailID retailId
					,RetailLocationID retailLocationId
					,RetailLocationLatitude latitude
					,RetailLocationLongitude longitude
			FROM #R1

			--Retaillocation impressions/clicks tracking
			INSERT INTO HubCitiReportingDatabase..RetailerList(--MainMenuID
													          RetailID
													        , RetailLocationID
													        , DateCreated
															, FundraisingID
													        )
			 
													   SELECT --@MainMenuID
															  RetailID
															, RetailLocationID
															, GETDATE()
															, @FundraisingID
													   FROM #R1
	   			
			END

			--Fundraisers in Retailer admin having RetailLocations
			ELSE IF EXISTS(SELECT 1 
						   FROM HcFundraising WHERE HcFundraisingID=@FundraisingID AND FundraisingOrganizationName IS NULL AND RetailID IS NOT NULL) --AND @AppSiteFlag = 1)
            BEGIN

			--Table to insert fundraisers
			CREATE TABLE #R2(RetailName VARCHAR(1000)
							,CompleteAddress VARCHAR(1000)
							,ImagePath VARCHAR(1000)
							,RetailID INT
							,RetailLocationID INT
							,RetailLocationLatitude FLOAT
							,RetailLocationLongitude FLOAT
							)

			INSERT INTO #R2
			SELECT DISTINCT  --RFA.RetailLocationID 
							 R.RetailName AS retailerName
							--,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
							,completeAddress = CAST((ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)) AS VARCHAR(100))+'mi'+','+SPACE(1)+RL.Address1+','+SPACE(1)+RL.City+','+SPACE(1)+RL.State+','+SPACE(1)+RL.PostalCode
							,imagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@RetailConfig+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)  	
							,R.RetailID retailId
							,RL.RetailLocationID retailLocationId
							,RL.RetailLocationLatitude latitude
							,RL.RetailLocationLongitude longitude
			FROM HcFundraising A 
			INNER JOIN HcRetailerFundraisingAssociation RFA ON A.HcFundraisingID=RFA.HcFundraisingID AND A.HcFundraisingID=@FundraisingID
			INNER JOIN Retaillocation RL ON RL.RetailLocationID=RFA.RetailLocationID  
			INNER JOIN Retailer R ON R.RetailID=RL.RetailID
			LEFT JOIN GeoPosition G ON G.PostalCode=RL.PostalCode
			WHERE A.Active = 1 AND RL.Active = 1 AND R.RetailerActive = 1
			
			SET @HcAppSiteFlag = 0

			--To display Fundraisers
			SELECT	 RetailName retailerName
					,CompleteAddress completeAddress
					,ImagePath imagePath
					,RetailID retailId
					,RetailLocationID retailLocationId
					,RetailLocationLatitude latitude
					,RetailLocationLongitude longitude
			FROM #R2

			--Retaillocation impressions/clicks tracking
			INSERT INTO HubCitiReportingDatabase..RetailerList(--MainMenuID
													          RetailID
													        , RetailLocationID
													        , DateCreated
															, FundraisingID
													        )
			 
													   SELECT --@MainMenuID
															  RetailID
															, RetailLocationID
															, GETDATE()
															, @FundraisingID
													   FROM #R2

			END

			--Fundraisers in Hubciti/Retailer associated to Organizations
			ELSE IF EXISTS(SELECT 1 FROM HcFundraising WHERE HcFundraisingID=@FundraisingID AND FundraisingOrganizationName IS NOT NULL) --AND @AppSiteFlag = 0)
			BEGIN
			SELECT DISTINCT  FundraisingOrganizationName
			                ,completeAddress = Address+','+SPACE(1)+City+','+SPACE(1)+State+','+SPACE(1)+PostalCode
							,B.Latitude latitude
							,B.Longitude longitude
							
			FROM HcFundraising A 
			INNER JOIN HcFundraisingLocation B ON A.HcFundraisingID=B.HcFundraisingID AND A.HcFundraisingID = @FundraisingID --AND B.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))  
			WHERE A.Active = 1 AND A.HcFundraisingID=@FundraisingID
			             
			SET @HcAppSiteFlag = NULL

			END
			
	   --Confirmation of Success
		 SELECT @Status = 0

	   END TRY
 
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in StoredProcedure [HubCitiApp2_3_3].[usp_HcFundraisingLocationDisplay]'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END
              
       END CATCH
END




















GO
