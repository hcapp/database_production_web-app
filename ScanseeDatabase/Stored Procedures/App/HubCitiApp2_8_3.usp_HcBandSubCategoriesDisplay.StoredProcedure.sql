USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcBandSubCategoriesDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_8_3].[usp_HcBandSubCategoriesDisplay]
Purpose					: To display Retailer Categories list.
Example					: [HubCitiApp2_8_3].[usp_HcBandSubCategoriesDisplay]

History
Version		  Date		      Author	 Change Description
--------------------------------------------------------------- 
1.0		   5th May 2016		Sagar Byali	      1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcBandSubCategoriesDisplay]
(
	--Input Variables
	  @HcHubCitiID int	
	, @Latitude float
	, @Longitude float
    , @UserID int
	, @PostalCode varchar(10)
	, @HcMenuItemID int
	, @HcBottomButtonID int
	, @SearchKey varchar(1000)--,
	--  @BandEventTypeID int,
		--@BandID int
	--Output Variables
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output
	, @Status int output
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
  
)
AS
BEGIN

	BEGIN TRY

		
		DECLARE @Radius int
		DECLARE @DistanceFromUser FLOAT

		 --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubCitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
		

		--Derive the Latitude and Longitude in the absence of the input.
        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
        BEGIN
                --If the postal code is passed then derive the co ordinates.
                IF @PostalCode IS NOT NULL
				BEGIN
                        SELECT @Latitude = Latitude
                            , @Longitude = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @PostalCode
                END		
				ELSE
				BEGIN
					SELECT @Latitude = G.Latitude
							, @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID	
				END												
        END    

		--To Get the user preferred radius.
        SELECT @Radius = LocaleRadius
        FROM HcUserPreference 
        WHERE HcUserID = @UserID
                           
        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))  

		DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
				WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
				WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
				ELSE @SearchKey END)

	
			
			IF (@HcMenuItemId IS NOT NULL)
			BEGIN 
				SELECT DISTINCT BandCategoryID,BandSubCategoryID 
				INTO #MenuItemBandCategory              
				FROM HcMenuItemBandCategories 
				WHERE HcMenuItemID=@HcMenuItemID --AND BandCategoryID = @BusinessCatID 
			END
			ELSe IF (@HcBottomButtonID IS NOT NULL)
			BEGIN
			
				SELECT DISTINCT BandCategoryID,BandSubCategoryID 
				INTO #BottomButtonBandCategory             
				FROM HcBottomButtonBandCategories 
				WHERE HcBottomButonID=@HcBottomButtonID --AND BandCategoryID = @BusinessCatID
			END

		--To display list of Dining Categories for given HubCiti.
		--IF (@HcBottomButtonID IS NULL AND @HcMenuItemID IS NOT NULL)
		--BEGIN


		--SELECT * FROM #BandCategory

		--select @SearchKey

		--select  *from #MenuItemBandCategory

				SELECT DISTINCT BC.BandCategoryID busCatId
							   ,BC.BandCategoryName busCatName
				FROM BandCategory BC 
				--INNER JOIN BandSubCategoryType BSCT ON BC.BandCategoryID = BSCT.BandCategoryID		
				--INNER JOIN BandSubCategory BSC ON BSCT.bandSubCategoryTypeID = BSC.bandSubCategoryTypeID
				INNER JOIN #MenuItemBandCategory FR ON FR.BandCategoryID=BC.BandCategoryID
				INNER JOIN BandCategoryAssociation RBC ON FR.BandCategoryID = RBC.BandCategoryID --AND FR.BandSubCategoryID = RBC.BandSubCategoryID
				INNER JOIN Band BBB ON BBB.BandID = RBC.BandID
				WHERE (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (BandCategoryName LIKE '%'+@SearchKey+'%' OR BBB.BandName LIKE '%'+@SearchKey+'%'))
				OR (@SearchKey IS NULL))


				--UNION 

				--SELECT DISTINCT BC.BandCategoryID catId
				--		       ,BC.BandCategoryName catName
				--FROM BandCategory BC 
				--INNER JOIN BandSubCategoryType BSCT ON BC.BandCategoryID = BSCT.BandCategoryID		
				--INNER JOIN BandSubCategory BSC ON BSCT.bandSubCategoryTypeID = BSC.bandSubCategoryTypeID
				--INNER JOIN #BandCategory FR ON FR.BandSubCategoryID=BSC.BandSubCategoryID
				----INNER JOIN HcMenuFindRetailerBusinessCategories FR ON FR.HcBusinessSubCategoryID=BSC.HcBusinessSubCategoryID AND HcMenuItemID =@HcMenuItemID 
				--INNER JOIN BandCategoryAssociation RBC ON FR.BandCategoryID = RBC.BandCategoryID AND FR.BandSubCategoryID = RBC.BandSubCategoryID

				--INNER JOIN HcRetailerSubCategory RSC ON FR.BusinessCategoryID = RSC.BusinessCategoryID AND RSC.HcBusinessSubCategoryID = FR.HcBusinessSubCategoryID AND RBC.RetailerID = RSC.RetailID
				--INNER JOIN RetailLocation RL ON RSC.RetailLocationID = RL.RetailLocationID AND Headquarters = 0 AND RL.Active = 1
				--INNER JOIN HcLocationAssociation LA ON RL.PostalCode = LA.PostalCode AND LA.HcHubCitiID = @HcHubCitiID--AND RL.City = LA.City AND LA.HcHubCitiID = @HcHubCitiID
				--INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID  AND Associated =1 
				--INNER JOIN GeoPosition G ON RL.PostalCode = G.PostalCode
				--WHERE  LA.HcHubCitiID = @HcHubCitiID AND BC.BusinessCategoryID = @BusinessCatID
				--AND ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) < = @Radius
				--ORDER BY BSC.BusinessSubCategoryName
		--END
		--ELSE IF (@HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
		--BEGIN
			
		--	SELECT DISTINCT BSC.HcBusinessSubCategoryID catId
		--					   ,BSC.BusinessSubCategoryName catName
		--		FROM BusinessCategory BC 
		--		INNER JOIN HcBusinessSubCategoryType BSCT ON BC.BusinessCategoryID = BSCT.BusinessCategoryID		
		--		INNER JOIN HcBusinessSubCategory BSC ON BSCT.HcBusinessSubCategoryTypeID = BSC.HcBusinessSubCategoryTypeID
		--		INNER JOIN HcBottomButtonFindRetailerBusinessCategories FR ON FR.HcBusinessSubCategoryID=BSC.HcBusinessSubCategoryID AND HcBottomButonID = @HcBottomButtonID 
		--		INNER JOIN HcRetailerSubCategory RSC ON BSC.HcBusinessSubCategoryID = RSC.HcBusinessSubCategoryID
		--		INNER JOIN RetailLocation RL ON RSC.RetailLocationID = RL.RetailLocationID
		--		INNER JOIN HcLocationAssociation LA ON RL.PostalCode = LA.PostalCode AND RL.City = LA.City AND LA.HcHubCitiID = @HcHubCitiID
		--		INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID  AND Associated =1 
		--		INNER JOIN GeoPosition G ON RL.PostalCode = G.PostalCode
		--		WHERE  LA.HcHubCitiID = @HcHubCitiID AND BC.BusinessCategoryID = @BusinessCatID
		--		AND ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) < = @Radius
		--		ORDER BY BSC.BusinessSubCategoryName	

		--END


	
		--Confirmation of Success
		SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcRetailerCategoriesDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
