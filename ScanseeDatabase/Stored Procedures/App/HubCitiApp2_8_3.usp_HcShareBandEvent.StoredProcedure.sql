USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcShareBandEvent]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_HcShareEvent
Purpose                 : To share event details.
Example                 : usp_HcShareEvent

History
Version           Date              Author			  Change Description
------------------------------------------------------------------------ 
1.0				7/18/2016			Bindu T A				1.0
------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcShareBandEvent]
(
        
		@HcEventID int
	  ,	@UserID int
	  , @HubCitiId int
    
      --Output Variable 
	  --, @AppsiteLink nvarchar(500) output
	  --, @Address nvarchar(500) output
      , @ShareText varchar(500) output
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
      
		BEGIN TRANSACTION
      
            DECLARE @RetailConfig varchar(50)
			DECLARE @Config VARCHAR(100)  
			DECLARE @Config1 VARCHAR(100)            
			DECLARE @Configuration VARCHAR(100)
            DECLARE @QRType INT     
			DECLARE @HcBandEventID INT 

			SET @HcBandEventID = @HcEventID

			
                     DECLARE @Configg VARCHAR(500),@RetailConfigg varchar(1000)
					 SELECT @Configg = 
					 ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'

					 
                     SELECT @RetailConfigg = 
					 ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Band Media Server Configuration'  
          
			--SET @Address = ''
			--SET @AppsiteLink = ''

			--Retrieve the server configuration.
  SELECT  @Config ='http://66.228.143.28:8080/Images/band/'

            --To get server Configuration.
            SELECT @RetailConfig= ScreenContent  
            FROM AppConfiguration   
            WHERE ConfigurationType='Web Retailer Media Server Configuration' 
            
            --To get the text used for share.         
            DECLARE @HubCitiName varchar(100)
		 
			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
		       
			SELECT @ShareText = 'I found this Band Event in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti Event Share Text'    
			
			SELECT @Config1 = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'  
			
			DECLARE @RetailerEventID int

			SELECT @RetailerEventID = BandID
			FROM HcBandEvents
			WHERE HcBandEventID = @HcBandEventID     
                  
   --         --Display the Basic details of the Retail location.   
			--IF @RetailerEventID IS NULL
			--BEGIN
			--	SELECT DISTINCT HcBandEventName eventName 
			--					,ShortDescription shortDes															
			--					,HcHubCitiID  hubCitiId
			--					,imgPath=(@Config1 + CAST(HCHubcitiID AS VARCHAR(100))+'/'+ImagePath)     
			--					,StartDate =CAST(StartDate AS DATE)
			--					,EndDate =CAST(EndDate AS DATE)
			--					,StartTime =CAST(StartDate AS Time)
			--					,EndTime =CAST(EndDate AS Time)             
			--					--,QRUrl = @Config+'2400.htm?key1='+ CAST(@HcEventID AS VARCHAR(10)) --+'&key2='+ CAST(@HubCitiId AS VARCHAR(10))                 							
			--					,QRUrl = @Config+'3001.htm?bndevtId='+ CAST(@HcBandEventID AS VARCHAR(10)) +'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10))--+'&isband=1'                 							

			--	FROM  HcBandEvents 
			--	WHERE HcBandEventID = @HcBandEventID --AND HcHubCitiID = @HubCitiId     
			--END
			--ELSE
			--BEGIN 
			--	SELECT DISTINCT HcBandEventName eventName 
			--					,ShortDescription shortDes															
			--					,HcHubCitiID  hubCitiId
			--					,imgPath=(@RetailConfig + CAST(BandID AS VARCHAR(100))+'/'+ImagePath)  								  
			--					,StartDate =CAST(StartDate AS DATE)
			--					,EndDate =CAST(EndDate AS DATE)
			--					,StartTime =CAST(StartDate AS Time)
			--					,EndTime =CAST(EndDate AS Time)             
			--					--,QRUrl = @Config+'2400.htm?key1='+ CAST(@HcEventID AS VARCHAR(10)) --+'&key2='+ CAST(@HubCitiId AS VARCHAR(10))                 							
			--					--,QRUrl = @Config+'3001.htm?bndevtId='+ CAST(@HcBandEventID AS VARCHAR(10)) +'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10))--+'&isband=1'                  							
			--												   ,QRUrl = @Config + '2000.htm?retId='+ cast(RetailID as varchar) +'&hubcitiId='+ cast(@HubCitiID as varchar)+'&retlocId='+cast(RetailLocationID as varchar)
				

			--					--http://66.228.143.27:8080/SSQR/qr/3002.
			--	FROM  HcBandEvents  E
			--	INNER JOIN HcBandRetailerEventsAssociation A ON A.HcBandEventID = E.HcBandEventID
			--	WHERE E.HcBandEventID = @HcBandEventID 
			--END	      
				declare @SSQRURL varchar(1000)

			select   @SSQRURL = 'http://66.228.143.27:8080/'





            
						SELECT DISTINCT --title = isnull(EventLocationTitle,BandName) 
								eventName = HcBandEventName  
							   ,shortDes = ShortDescription 
							   ,  hubCitiId= @hubcitiid
							   --,longDesc = LongDescription  								
							   ,imgPath = IIF(E.BandID IS NULL,(@Configg + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfigg+CAST(E.BandID AS VARCHAR)+'/'+ ImagePath) 
													
							   ,startDate =cast(startdate as date)
							   ,startTime = convert(varchar,cast(startdate as time),100)
							   ,endDate =cast(EndDate as date)
							   ,endTime = convert(varchar,cast(startdate as time),100)
							 --  ,bandID = e.BandID 
							 --  ,bandName = BandName 
							  -- ,bandVenueFlag =  E.BussinessEvent 
							
							  
							--   ,bandAddress = case when E.BussinessEvent = 0 then el.Address + ', ' +el.City + ', '+ el.State + ', '+el.PostalCode end 
							   ,QRUrl =   @ssqrURL + 'SSQR/qr/3001.htm?bndevtId='+ cast(e.HcBandEventID as varchar) +'&hubcitiId='+ cast(@HubCitiID as varchar)
							-- else @ssqrURL + 'SSQR/qr/2000.htm?retId='+ cast(RetailID as varchar) +'&hubcitiId='+ cast(@HubCitiID as varchar)+'&retlocId='+cast(RetailLocationID as varchar) end
				FROM HcBandEvents E
				LEFT JOIN HcBandEventLocation EL ON E.HcBandEventID =EL.HcBandEventID 
				LEFT JOIN HcBandEventInterval EI ON EL.HcbandEventID = EI.HcbandEventID
				LEFT JOIN Band BB ON BB.BandID = e.BandID  
				LEFT JOIN HcBandRetailerEventsAssociation BBB ON BBB.HcBandEventID = E.HcBandEventID AND BBB.HcBandEventID = @HcbandEventID
				WHERE E.HcbandEventID =@HcbandEventID   

			


			/*---------Share Email------------
		
				SELECT  @Address= HE.Address+','+HE.City+','+HE.State+','+HE.PostalCode+'||'+@Address
				FROM HcEventLocation HE
				INNER JOIN HcEvents HC ON HE.HcEventID = HC.HcEventID
				WHERE  HC.HcEventID = @HcEventID
			
				SELECT @Appsitelink = RL.RetailLocationURL+'||'+@Appsitelink
				FROM HcEventAppSite HC
				INNER JOIN HcAppSite HA ON HC.HcAppsiteID = HA.HcAppSiteID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = HA.RetailLocationID
				WHERE HC.HcEventID = @HcEventID
			
				IF(LEN(@Address)>0)
					SELECT @Address = LEFT(@Address,LEN(@Address)-2)
				IF(LEN(@AppsiteLink)>0)
					SELECT @Appsitelink = LEFT(@Appsitelink,LEN(@Appsitelink)-2)
				
		-------------------------------------	*/


           --Confirmation of Success.
		   SELECT @Status = 0 
        COMMIT TRANSACTION          
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_HcShareBandEvent.'       
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;
            
      END CATCH;
END;






GO
