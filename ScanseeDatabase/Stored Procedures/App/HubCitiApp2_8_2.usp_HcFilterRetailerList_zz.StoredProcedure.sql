USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcFilterRetailerList_zz]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFilterRetailerList]
Purpose					: To display Filter RetailerList.
Example					: [usp_HcFilterRetailerList]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/10/2013	    Pavan Sharma K	    1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcFilterRetailerList_zz]
(
    --Input variable.
       @UserID int 
     , @HCHubCitiID Int     
	 , @FilterID int
	 , @CategoryID Varchar(1000)  --Comma separated CategoryIDs
	 , @SearchKey varchar(255)
	 , @LowerLimit int  
	 , @ScreenName varchar(50)
	 , @Latitude decimal(18,6)    
	 , @Longitude decimal(18,6)    
	 , @ZipCode varchar(10)	 
	 , @SortColumn Varchar(200)
	 , @SortOrder Varchar(100)
	 , @LocalSpecials Bit	
	 
	 --Inputs for User Tracking
	 , @MainMenuId int
	 
	   
	 --OutPut Variable
	 , @NoRecordsMsg nvarchar(max) output
	 , @UserOutOfRange bit output
	 , @DefaultPostalCode VARCHAR(10) output
	 , @RetailGroupButtonImagePath varchar(1000) output
	 , @MaxCnt int  output
	 , @NxtPageFlag bit output 
	 , @Status int output
	 , @ErrorNumber int output  
	 , @ErrorMessage varchar(1000) output  
     , @Description varchar(2000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @RegionAppID int
			DECLARE @HcAppListID int

			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiID = @HCHubCitiID

			DECLARE @Config VARCHAR(100)
			DECLARE @RetailAffiliateCount INT
			DECLARE @CityExperienceID INT
			DECLARE @DistanceFromUser FLOAT
			--DECLARE @Latitude decimal(18,6)    
	  --      DECLARE @Longitude decimal(18,6)    
	  --      DECLARE @ZipCode varchar(10) 
			
			DECLARE @Globalimage varchar(50)
			DECLARE @CityExpDefaultConfig varchar(50)
			DECLARE @ModuleName varchar(20) = 'Interests'
			
			SELECT @Globalimage =ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType ='Image Not Found'
	        
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
			
			SELECT @CityExpDefaultConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'City Experience Default Image Path'
			AND Active = 1
			
			DECLARE @RetailConfig varchar(50)
			SELECT @RetailConfig=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Web Retailer Media Server Configuration'
			
			--To get the row count for pagination.  
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1   
			
			DECLARE @UserLatitude float
			DECLARE @UserLongitude float 

			SELECT @UserLatitude = @Latitude
				 , @UserLongitude = @Longitude

			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
					 , @UserLongitude = Longitude
				FROM HcUser A
				INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				WHERE HcUserID = @UserID 
			END	
			--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
			IF (@UserLatitude IS NULL) 
			BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID
			END


			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HCHubCitiID, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
			
			
			SELECT @CityExperienceID = HcCityExperienceID
			FROM HcFilter 
			WHERE HcFilterID = @FilterID
			
			-- To identify Retailer that have products on Sale or any type of discount
			SELECT DISTINCT Retailid , RetailLocationid
			INTO #RetailItemsonSale
			FROM 
				(SELECT b.RetailID, a.RetailLocationID 
				FROM RetailLocationDeal a 
				INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID AND b.Active = 1 
				INNER JOIN RetailLocationProduct c ON a.RetailLocationID = c.RetailLocationID			
												   AND a.ProductID = c.ProductID
												   AND GETDATE() BETWEEN isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =b.PostalCode AND HL.HcCityID=b.HcCityID AND HL.StateID=b.StateID AND HL.HcHubCitiID =@HcHubcitiID  
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				UNION 
				SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
				FROM Coupon C 
				INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				INNER JOIN RetailLocation RL ON RL.RetailID =CR.RetailID AND RL.Active = 1 
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID  
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				GROUP BY C.CouponID
						,NoOfCouponsToIssue
						,CR.RetailID
						,CR.RetailLocationID
				HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
						 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)								   
				
				UNION 
				SELECT  RR.RetailID, 0 AS RetaillocationID  
				FROM Rebate R 
				INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
				INNER JOIN RetailLocation RL ON RL.RetailID =RR.RetailID AND RL.Active = 1 
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
				
				UNION 
				SELECT  c.retailid, a.RetailLocationID 
				FROM  LoyaltyDeal a
				INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
				INNER JOIN RetailLocation c ON a.RetailLocationID = c.RetailLocationID AND c.Active = 1
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =c.PostalCode AND HL.HcCityID=c.HcCityID AND HL.StateID=c.StateID AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID
				INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID AND a.ProductID = LDP.ProductID 
				WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
				
				UNION
				SELECT DISTINCT rl.RetailID, rl.RetailLocationID
				FROM ProductHotDeal p
				INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
				INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active = 1
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=rl.HcCityID AND HL.StateID=rl.StateID AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
				LEFT JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
				GROUP BY P.ProductHotDealID
						,NoOfHotDealsToIssue
						,rl.RetailID
						,rl.RetailLocationID
				HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
						 ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)  
				
				UNION
				SELECT q.RetailID, qa.RetailLocationID
				FROM QRRetailerCustomPage q
				INNER JOIN QRRetailerCustomPageAssociation qa ON qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID =qa.RetailLocationID AND RL.Active = 1 
				INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND HL.HcCityID=RL.HcCityID AND HL.StateID=RL.StateID AND HL.HcHubCitiID =@HcHubcitiID
				INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = @HCHubCitiID AND Associated =1 
				INNER JOIN QRTypes qt ON qt.QRTypeID = q.QRTypeID AND qt.QRTypeName = 'Special Offer Page'
				WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') AND isnull(q.enddate,GETDATE()+1)) a			
									

		    --To get the image path of the given group.	
			SELECT @RetailGroupButtonImagePath = @Config + ISNULL(ButtonImagePath, @CityExpDefaultConfig)
			FROM HcCityExperience
			WHERE HcCityExperienceID = @CityExperienceID
			
			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
				IF @ZipCode IS NULL
				BEGIN
					SELECT @Latitude = G.Latitude
						 , @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID
				END
				ELSE
				BEGIN
					SELECT @Latitude = Latitude
						 , @Longitude = Longitude
					FROM GeoPosition 
					WHERE PostalCode = @ZipCode
				END
			END
			  
			
				--To get the image path of the given group.	
			--SELECT @RetailGroupButtonImagePath = @Config + ISNULL(ButtonImagePath, @CityExpDefaultConfig)
			--FROM HcFilter HCF
			--INNER JOIN HcFilterRetailLocation RA ON RA.HcFilterID = HCF.HcFilterID
			--WHERE RA.HcFilterID = @FilterID
			
			DECLARE @UserPreferredCity bit 
			SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID AND HcCityID IS NULL THEN 0
											WHEN HcUserID = @UserID AND HcCityID IS NOT NULL THEN 1
											ELSE 0 END
			FROM HcUsersPreferredCityAssociation
			WHERE HcHubcitiID = @HCHubCitiID AND HcUserID = @UserID

			SELECT @UserPreferredCity = ISNULL(@UserPreferredCity,0)

			SELECT Param BusCatIDs
			INTO #BusinessCategoryIDs
			FROM fn_SplitParam (@CategoryID,',')

			--Filter the Retailers based on the given Affiliate.						
								SELECT Row_Num = IDENTITY(INT,1,1) 
										, RetailID    
										, RetailName
										, RetailLocationID
										, Address1
										, Address2
										, Address3
										, Address4
										, City    
										, State
										, PostalCode									
										, RetailerImagePath    
										, Distance 	
										, DistanceActual
										, SaleFlag	
										, RetailLocationLatitude
										, RetailLocationLongitude
										--, HcCityID																																
							INTO #Retail
							FROM 
							(SELECT DISTINCT TOP 100 PERCENT R.RetailID     
														   , R.RetailName
														   , RL.RetailLocationID 
														   , RL.Address1     
														   , RL.Address2
														   , RL.Address3
														   , RL.Address4
														   , RL.City
														   , RL.State
														   , RL.PostalCode													   
														   --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)
														  , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
														  , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
														  , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) 
								
														 -- -- , Distance = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
														 -- , Distance = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
														 -- -- , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
														 --, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
														   , SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
														   , RetailLocationLatitude=CASE WHEN RL.RetailLocationLatitude IS NOT NULL THEN RL.RetailLocationLatitude ELSE G.Latitude END 
														   , RetailLocationLongitude=CASE WHEN RL.RetailLocationLongitude IS NOT NULL THEN RL.RetailLocationLongitude ELSE G.Longitude END  
														  -- , C.HcCityID 															  
							FROM Retailer R    
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0   
							INNER JOIN HcLocationAssociation HLA ON HLA.HcHubCitiID = @HCHubCitiID AND HLA.PostalCode = RL.PostalCode  
							INNER JOIN HcCity C ON HLA.HcCityID = C.HcCityID
							INNER JOIN HcFilterRetailLocation HL ON HL.HcFilterID=@FilterID AND HL.RetailLocationID=RL.RetailLocationID 
							INNER JOIN RetailerBusinessCategory RB ON R.RetailID =RB.RetailerID 		                
							LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode 
							WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND @UserPreferredCity = 0 AND RL.Active =1 AND R.RetailerActive = 1
							AND ((@CategoryID IS NULL AND 1 = 1) OR RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs))
							AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+ @SearchKey +'%' END

							UNION ALL

							SELECT DISTINCT TOP 100 PERCENT R.RetailID     
														   , R.RetailName
														   , RL.RetailLocationID 
														   , RL.Address1     
														   , RL.Address2
														   , RL.Address3
														   , RL.Address4
														   , RL.City
														   , RL.State
														   , RL.PostalCode													   
														   --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)
														  , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
														  , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)  			  
														  , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) 
								
														 -- -- , Distance = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
														 -- , Distance = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
														 -- -- , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
														 --, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)
														   , SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
														   , RetailLocationLatitude=CASE WHEN RL.RetailLocationLatitude IS NOT NULL THEN RL.RetailLocationLatitude ELSE G.Latitude END 
														   , RetailLocationLongitude=CASE WHEN RL.RetailLocationLongitude IS NOT NULL THEN RL.RetailLocationLongitude ELSE G.Longitude END  
														   --, C.HcCityID 															   
							FROM Retailer R    
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.Headquarters = 0   
							INNER JOIN HcLocationAssociation HLA ON  HLA.HcHubCitiID = @HCHubCitiID AND HLA.PostalCode = RL.PostalCode  
							INNER JOIN HcCity C ON HLA.HcCityID = C.HcCityID
						    INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HcHubcitiID
							INNER JOIN HcFilterRetailLocation HL ON HL.HcFilterID=@FilterID AND HL.RetailLocationID=RL.RetailLocationID 
							INNER JOIN RetailerBusinessCategory RB ON R.RetailID =RB.RetailerID 		                
							LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
							LEFT JOIN GeoPosition G ON G.PostalCode =RL.PostalCode 
							WHERE @RegionAppID = 1 AND @UserPreferredCity = 1 AND RL.Active = 1 AND R.RetailerActive = 1
							AND ((@CategoryID IS NULL AND 1 = 1) OR RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs))
							AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+ @SearchKey +'%' END
							)Retailer 
							--ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
							--			  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailName AS SQL_VARIANT) 
							--			  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
							--		  END ASC,Distance
									  --CASE WHEN @SortOrder = 'Desc' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
											--WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(retailName AS SQL_VARIANT) 
											--WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
									  --END DESC  
							--ORDER BY ISNULL(DistanceActual, Distance), RetailName ASC							
		

		SELECT Row_Num rowNumber  
						, RetailID retailerId    
						, RetailName retailerName
						, R.RetailLocationID retailLocationID
						, Address1 retaileraddress1
						, Address2 retaileraddress2
						, Address3 retaileraddress3
						, Address4 retaileraddress4
						, City City
						, State State
						, PostalCode PostalCode    
						, RetailerImagePath logoImagePath     
						, Distance  
						, DistanceActual    
						, S.BannerAdImagePath bannerAdImagePath    
						, B.RibbonAdImagePath ribbonAdImagePath    
						, B.RibbonAdURL ribbonAdURL    
						, B.RetailLocationAdvertisementID advertisementID  
						, S.SplashAdID splashAdID
						, SaleFlag
						, RetailLocationLatitude retLatitude
						, RetailLocationLongitude retLongitude  						
					 INTO #RetailerList	
					 FROM #Retail R
					 LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
																	ELSE SplashAdImagePath
																  END
									   , SplashAdID  = ASP.AdvertisementSplashID
									   , R.RetailLocationID 
								FROM #Retail R
									INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
									INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
									AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)) S
							ON R.RetailLocationID = S.RetailLocationID
					 LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
																ELSE AB.BannerAdImagePath
															END 
									  , RibbonAdURL = AB.BannerAdURL
									  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
									  , R.RetailLocationID 
								 FROM #Retail R
								 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
								 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
								 WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)) B
					 ON R.RetailLocationID = B.RetailLocationID
					 --ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
						--				  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailName AS SQL_VARIANT) 
						--				  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
						--			  END ASC,Distance
									  --CASE WHEN @SortOrder = 'Desc' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
											--WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(retailName AS SQL_VARIANT) 
											--WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
									  --END DESC  
					 --WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit 
				
				
				SELECT  rowNumber = ROW_NUMBER() OVER 
						(ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
										  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT) 
										  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
									  END ASC,Distance)
						, retailerId    
						, retailerName
						, retailLocationID
						, retaileraddress1
						, retaileraddress2
						, retaileraddress3
						, retaileraddress4
						, City
						, State
						, PostalCode PostalCode    
						, logoImagePath     
						, Distance  
						, DistanceActual    
						, bannerAdImagePath    
						, ribbonAdImagePath    
						, ribbonAdURL    
						, advertisementID  
						, splashAdID
						, SaleFlag
						, retLatitude
						, retLongitude
				INTO #FinalList
				FROM #RetailerList
				WHERE (@LocalSpecials = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))	
				--ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
				--						  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT) 
				--						  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
				--					  END ASC, Distance
									  --CASE WHEN @SortOrder = 'Desc' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
											--WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(retailerName AS SQL_VARIANT) 
											--WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
									  --END DESC  								
				
			
		--To capture max row number.  
		SELECT @MaxCnt = COUNT(rowNumber) FROM #FinalList
					 
		--This flag is a indicator to enable "More" button in the UI.   
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
				 
		--To display Short Description as output parameter
		SELECT @Description = FilterDescription 
		FROM HcFilter
		WHERE HcFilterID = @FilterID	
		
			 
					 --UserTracking.					 
					 
					 --Table to track the Retailer List.
					 CREATE TABLE #Temp(RetailerListID int
                                        ,MainMenuID int
                                        ,RetailID int
                                        ,RetailLocationID int
                                        ,HcCitiExperienceID int)  
                      
                      
                      --Capture the impressions of the Retailer list.
                      INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID																	
																	, RetailID
																	, RetailLocationID
																	, HcCitiExperienceID
																	, FindCategoryID 
																	, DateCreated)
					  OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)							
													SELECT @MainMenuId
														 , retailerId
														 , RetailLocationID
														 , @CityExperienceID 
														 , CASE WHEN @CategoryID IS NOT NULL THEN NULL ELSE 0 END  --CASE WHEN @CategoryID IS NOT NULL THEN P.Param ELSE NULL END
														 , GETDATE()
													FROM #FinalList
													--LEFT JOIN [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ',') P ON 1=1
						--Table to Track Cities
						
						--SELECT DISTINCT HcCityID
						--INTO #City
						--FROM #CityList
						
						--INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
						--												,CityID
						--												,DateCreated
						--												)
						--										SELECT @MainMenuId
						--											   ,HcCityID
						--											   ,Getdate()
						--										FROM #City
												
						--Display the Retailer along with the keys generated in the Tracking table.
					
						SELECT rowNumber
								, T.RetailerListID retListID
								, retailerId
								, retailerName
								, T.RetailLocationID
								, retaileraddress1
								, retaileraddress2
								, retaileraddress3
								, retaileraddress4
								, City
								, State
								, PostalCode
								, logoImagePath
								, Distance = ISNULL(DistanceActual, Distance)
								, bannerAdImagePath
								, ribbonAdImagePath
								, ribbonAdURL
								, advertisementID
								, splashAdID
								, SaleFlag	
								, retLatitude
								, retLongitude														 					 
						FROM #Temp T
						INNER JOIN #FinalList R ON  R.RetailLocationID = T.RetailLocationID
						WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
						ORDER BY rowNumber	
						--ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
						--				  WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT) 
						--				  WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)
						--			  END ASC, Distance
									  --CASE WHEN @SortOrder = 'Desc' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
											--WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(retailerName AS SQL_VARIANT) 
											--WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
									  --END DESC  	
						

						
					--To display bottom buttons								

					EXEC [HubCitiApp2_8_2].[usp_HcFunctionalityBottomButtonDisplay] 10, 'Interests', @UserID, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

				--To display message when no Retailers display for user preferred cities.
				 DECLARE @UserPrefCities NVarchar(MAX)

				 IF (@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0) AND (@SearchKey IS NULL) AND @CategoryID IS NULL AND @LocalSpecials = 0
				 BEGIN 
						 SELECT @UserPrefCities = COALESCE(@UserPrefCities+',' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName))) 
						 FROM HcUsersPreferredCityAssociation P
						 INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
						 WHERE HcHubcitiID = @HCHubCitiID AND HcUserID = @UserID
						
						SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
												'.\n\nUpdate your city preferences in the settings menu.'

				 END
				 ELSE IF ((@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0)) 
						 AND (@SearchKey IS NOT NULL OR @CategoryID IS NOT NULL OR @LocalSpecials = 1)
				 BEGIN
					
					SELECT @NoRecordsMsg = 'No Records Found.'

				 END
				 ELSE IF (@RegionAppID = 0) AND (ISNULL(@MaxCnt,0) = 0)
				 BEGIN
						SELECT @NoRecordsMsg = 'No Records Found.'
				 END
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcFilterRetailerList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



























GO
