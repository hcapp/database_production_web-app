USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserCityAssociation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcUserCityAssociation]
Purpose					: To Associate selected Cities to User.
Example					: [usp_HcUserCityAssociation]

History
Version		    Date		   Author		Change Description
--------------------------------------------------------------- 
1.0			11th Sept 2014	    SPAN  			1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserCityAssociation]
(
   
    --Input variable 	  
	  @HcHubcitiID int
	, @UserID int
	, @CitiID Varchar(500) --Comma separated CitiID's
  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			--To update Cities to users, delete existed preferred cities and then insert new set of preferred cities.
			DELETE FROM HcUsersPreferredCityAssociation
			WHERE HcHubcitiID = @HcHubcitiID AND HcUserID = @UserID

			INSERT INTO HcUsersPreferredCityAssociation(HcHubcitiID
														, HcUserID
														, HcCityID
														, CreatedUserID
														, DateCreated
														)
												SELECT @HcHubcitiID
													  , @UserID
													  , Param
													  , @UserID
													  , GETDATE()
												FROM fn_SplitParam (@CitiID, ',')
													
			--Confirmation of Success.
			SELECT @Status = 0

		COMMIT TRANSACTION
	          
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [HubCitiApp2_1].[usp_HcUserCityAssociation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
































GO
