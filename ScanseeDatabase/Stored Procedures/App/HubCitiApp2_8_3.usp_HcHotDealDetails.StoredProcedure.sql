USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHotDealDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HotDealDetails    
Purpose      : To get details of a Hot Deal.    
Example      : usp_HotDealDetails 1,1    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   28th Oct 2013 SPAN Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHotDealDetails]    
(    
   @UserID int    
 , @HotDealID int    
 , @HubCitiID int 
 --User Tracking inputs.
 , @HotDealListID int
     
 --OutPut Variable
  
 , @Status int output 
 , @ErrorNumber int output    
 , @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY    
 
	BEGIN TRANSACTION
	 --To get Server Configuration
	 DECLARE @ManufConfig varchar(50) 
	 DECLARE @RetailerConfig varchar(50)
	 DECLARE @Config Varchar(50)
	 DECLARE @RetailName VARCHAR(255) 
	 DECLARE @Username Varchar(255)
	 DECLARE @Email Varchar(1000) 
	 
	 SELECT @ManufConfig=(select ScreenContent  
						 FROM AppConfiguration   
						 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
		   ,@RetailerConfig=(select ScreenContent  
							 FROM AppConfiguration   
							 WHERE ConfigurationType='Web Retailer Media Server Configuration')
		   ,@Config=(select ScreenContent  
					 FROM AppConfiguration   
					 WHERE ConfigurationType='App Media Server Configuration')
	 FROM AppConfiguration
	  
	 SELECT @Username =UserName,@Email=Email 
	 FROM HcUser 
	 WHERE HcUserID =@UserID 
	  
	 DECLARE @RetaillocationIDs VARCHAR(500)
     DECLARE @RetailLocations VARCHAR(5000)
     
     SELECT DISTINCT @RetaillocationIDs = COALESCE(@RetaillocationIDs + ', ','') + CAST(RL.RetailLocationID AS VARCHAR(100))
		  , @RetailLocations = COALESCE(@RetailLocations + ' | ','') + RL.Address1 + ', ' +  RL.city + ', ' +  RL.[State] + ', ' +  RL.PostalCode
		  , @RetailName = R.RetailName
	 FROM ProductHotDeal P
	 INNER JOIN ProductHotDealRetailLocation PRL ON P.ProductHotDealID = PRL.ProductHotDealID
	 INNER JOIN RetailLocation RL ON PRl.RetailLocationID = RL.RetailLocationID AND RL.Active=1   
	 INNER JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode
	 INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
	 INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND R.RetailerActive=1
	 WHERE P.ProductHotDealID = @HotDealID
	  AND COALESCE(HotDealExpirationDate,HotDealEndDate,GETDATE()+1) >= GETDATE()
	 AND HL.HcHubCitiID = @HubCitiID
	 
	  
		 	    
	  SELECT DISTINCT P.ProductHotDealID    
			,ISNULL(C.ParentCategoryName, 'Uncategorized') AS Category    
			,HotDealName    
			,HotDealProvider    
			,Price hDPrice    
			,HotDealDiscountType hDDiscountType    
			,HotDealDiscountAmount hDDiscountAmount    
			,HotDealDiscountPct hDDiscountPct    
			,SalePrice hDSalePrice    
			,hDLognDescription =  HotDeaLonglDescription 
			,hDDesc=HotDealShortDescription	
			,hotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
														CASE WHEN P.WebsiteSourceFlag = 1 THEN 
																	CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
																		+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
																	   +HotDealImagePath  
																	ELSE @RetailerConfig  
																	+CONVERT(VARCHAR(30),P.RetailID)+'/'  
																	+HotDealImagePath  
																	END  					                         
														ELSE P.HotDealImagePath  
														END  
								END  
			,HotDealTermsConditions hDTermsConditions    
			,HotDealURL hdURL    
			,HotDealStartDate hDStartDate    
			,HotDealEndDate hDEndDate  
			,HotDealExpirationDate hDExpDate
			--,REPLACE(CONVERT(VARCHAR, HotDealExpirationDate, 111), '/', '-') hDExpDate
			
			,P.HotDealCode coupCode
			,@Config+A.APIPartnerImagePath apiPartnerImagePath    
			,A.APIPartnerName   
			,MIN(PHL.City)
			,@RetailName AS retName
			,@RetaillocationIDs AS retLocIDs
			,@RetailLocations AS retLocs 
			,extFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
			,ClaimFlag = CASE WHEN UG.Used IS NOT NULL THEN 1 ELSE 0 END
			,RedeemFlag = CASE WHEN Used = 1 THEN 1 ELSE 0 END
			--,UserName = CASE WHEN UG.UsedFlag IS NOT NULL THEN (SELECT UserName FROM Users WHERE UserID = @UserID) ELSE (SELECT Email FROM Users WHERE UserID = @UserID) END 
	        ,UserName = ISNULL(@Username,@Email)
	        ,Expired = CASE WHEN UG.Used = 0 AND GETDATE() > HotDealExpirationDate THEN 1 
							ELSE CASE WHEN GETDATE() > HotDealEndDate THEN 1 ELSE 0 END 
					   END	
		    ,PHL.HotDealLatitude latitude
			,PHL.HotDealLongitude longitude	
			,retLocImgLogo = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailerConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailerConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
			--,retLocImgLogo=@RetailerConfig+CAST(RL.RetailID AS VARCHAR) +'/'+'locationlogo'+'/'+RL.RetailLocationImagePath 
			,RL.RetailID retailId
			,RL.RetailLocationID retLocId											
	  FROM ProductHotDeal P 
	  LEFT JOIN  HcUserHotDealGallery UG ON P.ProductHotDealID = UG.HotDealID  AND UG.HcUserID = @UserID
	  LEFT JOIN Category C ON C.CategoryID = P.CategoryID
	  LEFT JOIN ProductHotDealLocation PHL ON PHL.ProductHotDealID = P.ProductHotDealID
	  LEFT JOIN Retailer R ON R.RetailID = P.RetailID AND R.RetailerActive=1
	  LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID    
	  LEFT JOIN ProductHotDealRetailLocation RHR ON RHR.ProductHotDealID=P.ProductHotDealID 
	  LEFT JOIN RetailLocation RL ON RL.RetailLocationID =RHR.RetailLocationID AND RL.Active=1
	  WHERE P.ProductHotDealID = @HotDealID
	     AND COALESCE(HotDealExpirationDate,HotDealEndDate,GETDATE()+1) >= GETDATE()
	   GROUP BY  P.ProductHotDealID   
			,ParentCategoryName   
			,HotDealName    
			,HotDealProvider    
			,Price      
			,HotDealDiscountType      
			,HotDealDiscountAmount      
			,HotDealDiscountPct      
			,SalePrice      
			,hotDealImagePath
			,P.WebsiteSourceFlag 
			,ManufacturerID
			,HotDealImagePath
			,P.RetailID									
		    ,P.HotDealShortDescription   
		    --,'<![CDATA['+ P.HotDeaLonglDescription +']]>' hDLognDescription
			,P.HotDeaLonglDescription  				
			,HotDealTermsConditions      
			,HotDealURL      
			,HotDealStartDate      
			,HotDealEndDate    
			--,HotDealExpirationDate hDExpDate
			,HotDealExpirationDate 
			,P.HotDealCode  
			,A.APIPartnerImagePath      
			,A.APIPartnerName  
			,UG.Used 
			,PHL.HotDealLatitude  
			,PHL.HotDealLongitude    
			,RL.RetailID 
			,RL.RetailLocationID 
			,RL.RetailLocationImagePath
			,R.WebsiteSourceFlag 
			,RetailerImagePath

	  
	  --User Tracking section.
	  UPDATE HubCitiReportingDatabase..HotDealList SET ProductHotDealClick = 1 
	  WHERE HotDealListID = @HotDealListID
	  
	  --Confirmation of Success.
	  SELECT @Status = 0
	COMMIT TRANSACTION  
 END TRY    
     
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_HotDealDetails.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'     
   ROLLBACK TRANSACTION;
   --Confirmation of failure.
   SELECT @Status = 1
  END;    
       
 END CATCH;    
END;
















































GO
