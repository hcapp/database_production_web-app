USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFAQDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFAQDisplay
Purpose					: Display list of FAQs.
Example					: usp_HcFAQDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10/09/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFAQDisplay]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int
    , @CategoryID int	  		
    , @SearchKey Varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(50)

	--UserTracking

	, @MainMenuID Int
	
	  
	--Output Variable 		
	, @MaxCnt int  output
	, @NxtPageFlag bit output
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

	            DECLARE @UpperLimit int

				--To get the row count for pagination.	 
				SELECT @UpperLimit = ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
	           
			 --   SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(C.SortOrder,10000),C.HcFAQCategoryID, C.CategoryName,Question,Answer ASC) rowNum
				----SELECT ROW_NUMBER() OVER (PARTITION BY CatId,CatName ORDER BY CatId) rowNum
				--     -- ,CatId
				--   --   ,CatName
				--	  ,faqId
				--	  ,Question
				--	  ,Answer 
				--FROM 
				--(

				
				
				--SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(C.SortOrder,10000),C.HcFAQCategoryID, C.CategoryName,Question,Answer ASC) rowNum 
				--     -- ,C.HcFAQCategoryID CatId
				--     -- ,C.CategoryName CatName
				--      ,F.HcFAQID faqId
				--	  ,Question
				--	  ,Answer	  				
				--FROM HcFAQ F   
				--INNER JOIN HcFAQCategory C ON C.HcFAQCategoryID =F.HcFAQCategoryID AND F.HcHubCitiID=@HcHubcitiID AND C.HcFAQCategoryID = @CategoryID
				--WHERE (@SearchKey IS NOT NULL AND CategoryName LIKE '%'+@SearchKey+'%')

				--UNION 

				--SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(C.SortOrder,10000),C.HcFAQCategoryID,C.CategoryName,Question,Answer ASC) rowNum 
				--    --  ,C.HcFAQCategoryID CatId
				--    --  ,C.CategoryName CatName
				--      ,F.HcFAQID faqId
				--	  ,Question
				--	  ,Answer
					  				
				--FROM HcFAQ F   
				--INNER JOIN HcFAQCategory C ON C.HcFAQCategoryID =F.HcFAQCategoryID AND F.HcHubCitiID=@HcHubcitiID AND C.HcFAQCategoryID = @CategoryID
				--WHERE (@SearchKey IS NOT NULL AND Question LIKE '%'+@SearchKey+'%' OR Answer LIKE '%'+@SearchKey+'%')
				----ORDER BY C.CategoryName,Question			
				
				--UNION 

				--SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(C.SortOrder,10000),C.HcFAQCategoryID, CategoryName,Question,Answer ASC) rowNum
				--     -- ,C.HcFAQCategoryID CatId
				--    --  ,C.CategoryName CatName
				--      ,F.HcFAQID faqId
				--	  ,Question
				--	  ,Answer
					  				
				--FROM HcFAQ F   
				--INNER JOIN HcFAQCategory C ON C.HcFAQCategoryID =F.HcFAQCategoryID AND F.HcHubCitiID=@HcHubcitiID AND C.HcFAQCategoryID = @CategoryID
				--WHERE @SearchKey IS NULL 
				
				--) AS FAQData


				--To display list of FAQ's for a category
				SELECT ROW_NUMBER() OVER(ORDER BY ISNULL(C.SortOrder,10000),C.HcFAQCategoryID, C.CategoryName,Question,Answer ASC) rowNum 
				     -- ,C.HcFAQCategoryID CatId
				     -- ,C.CategoryName CatName
				      ,F.HcFAQID faqId
					  ,Question
					  ,Answer	  				
				FROM HcFAQ F   
				INNER JOIN HcFAQCategory C ON C.HcFAQCategoryID =F.HcFAQCategoryID AND F.HcHubCitiID=@HcHubcitiID AND C.HcFAQCategoryID = @CategoryID
				WHERE (@SearchKey IS NOT NULL AND (CategoryName LIKE '%'+@SearchKey+'%' OR Question LIKE '%'+@SearchKey+'%' OR Answer LIKE '%'+@SearchKey+'%')) OR (@SearchKey IS NULL)
			   --  OR (@SearchKey IS NOT NULL AND (Question LIKE '%'+@SearchKey+'%' OR Answer LIKE '%'+@SearchKey+'%'))	
                ORDER BY rowNum
				

				--To capture max row number.  
				SELECT @MaxCnt =@@ROWCOUNT
				 
				--IF @LowerLimit IS NULL
				--BEGIN
				--	SET @LowerLimit=0
				--	SET @UpperLimit=@MaxCnt					
				--END 
				 
				--this flag is a indicator to enable "More" button in the UI.   
				--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@UpperLimit+@LowerLimit)) > 0 THEN 1 ELSE 0 END 

	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			SELECT ERROR_LINE()
			PRINT 'Error occured in Stored Procedure usp_HcFAQDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











































GO
