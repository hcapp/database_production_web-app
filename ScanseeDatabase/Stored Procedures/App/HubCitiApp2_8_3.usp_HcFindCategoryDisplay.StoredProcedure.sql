USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFindCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFindCategoryDisplay]
Purpose					: To display CategoryList for HubCiti in Find All module.
Example					: [usp_HcFindCategoryDisplay]

History
Version		   Date			Author				Change Description
---------------------------------------------------------------------------------------------------------------------- 
1.0			10/29/2013	    SPAN				1.0
1.0			03/03/2016      Sagar Byali			Find Caching implementation
3.0			12/10/2016		Sagar Byali			Performance optimization & Logical changes (Reviewed by: Sreeraj S V)
----------------------------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFindCategoryDisplay]
(
	--Input Variable
	  @PostalCode VARCHAR(5)
	, @UserConfiguredPostalCode VARCHAR(5)
	, @HubCitiID int
	, @CityIDs VARCHAR(1000)
	, @HcMenuItemID int
	, @HcBottmoButtonID int
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)
	, @SubmenuFlag bit
	, @Radius int  
	--Output Variables
	, @NoRecordsMsg nvarchar(MAX) output
	, @MenuColor VARCHAR(50) output
	, @MenuFontColor Varchar(50) output 
	, @UserOutOfRange bit output
	, @DefaultPostalCode VARCHAR(5) output
	, @Status bit output
	, @ErrorNumber int output
	, @ErrorMessage VARCHAR(1000) output 
)--with recompile
AS
BEGIN
	BEGIN TRY
		--Variable declaration
		DECLARE   @CityList TABLE (CityName VARCHAR(100), PostalCode VARCHAR(5));
		DECLARE   @ConfigirationType VARCHAR(50)
				, @NearestPostalCode VARCHAR(5)
				, @DistanceFromUser FLOAT
				, @RegionAppFlag BIT 
				, @Config Varchar(200) 									
			    , @DistanceSin FLOAT 
			    , @Distancecos FLOAT 
			    , @DistancecosLong FLOAT 
			    , @DecToRad FLOAT = 57.2958
				, @UserPrefCities VARCHAR(250)
				, @RowCount INT = 0
				, @RandomPostalCode VARCHAR(5)
				, @Distance FLOAT
		
		SET @UserOutOfRange = 0
		
		--To get the Configuration Details.	
		SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

		SELECT @ConfigirationType = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType = 'App Media Server Configuration'

		SELECT @MenuColor = SubMenuButtonColor 
			  ,@MenuFontColor = SubMenuButtonFontColor  
			FROM HcMenuCustomUI 
			WHERE HubCitiId = @HubCitiID
  
		SELECT @RegionAppFlag = CASE WHEN HcAppListID = '1' THEN HcAppListID ELSE '0' END
		      ,@DefaultPostalCode = DefaultPostalCode
			FROM HcHubCiti  
			WHERE HcHubCitiID = @HubCitiID 

        IF @RegionAppFlag = '0'
        BEGIN							

			INSERT INTO @CityList(CityName,PostalCode)	
				SELECT DISTINCT  LA.City	
								,PostalCode				
					FROM (SELECT HcHubCitiID 
						  FROM HcRegionAppHubcitiAssociation 
						  WHERE HcRegionAppID = @HubCitiID 
						  UNION ALL
				          SELECT @HubCitiID) AS H
					INNER JOIN HcLocationAssociation LA  ON LA.HcHubCitiID = H.HchubcitiID AND LA.HcHubCitiID = @HubCitiID   						
					INNER JOIN (SELECT Param FROM fn_SplitParam(@CityIds,',')) AS C ON LA.HcCityID = C.Param 
		END
        ELSE
        BEGIN

			INSERT INTO @CityList(CityName,PostalCode)
				SELECT DISTINCT City,PostalCode					
					FROM HcLocationAssociation  
					WHERE HcHubCitiID = @HubCitiID                                   
		END
		
		--Derive the Latitude and Longitude in the absence of the input.
		IF @Latitude IS NULL
		BEGIN
			 SELECT @Latitude = Latitude
				 ,@Longitude = Longitude
				 FROM GeoPosition 
				 WHERE PostalCode = COALESCE(@PostalCode,@UserConfiguredPostalCode,@DefaultPostalCode)				
		END	

		--To find nearest PostalCode to user
		;WITH CTE AS (SELECT G.PostalCode
						  ,Distance = ROUND((ACOS((SIN(G.Latitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(G.Latitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (G.Longitude / 57.2958))))*6371) * 0.6214 ,1,1)
						  FROM HcLocationAssociation LA  
						  INNER JOIN GeoPosition G  ON LA.PostalCode = G.PostalCode AND LA.City = G.City AND LA.State = G.State
						  WHERE LA.HcHubCitiID = @HubCitiID 
						)
						 SELECT DISTINCT @NearestPostalCode = PostalCode
							 ,@Distance = Distance
							 FROM CTE
							 WHERE Distance = (SELECT MIN(Distance) FROM CTE)
		
		--To check whether user is within HubCiti or Not
		IF ISNULL(@Distance, 0) >= (@Radius/2) 
		BEGIN
			 SELECT @Latitude = Latitude 
			     ,@Longitude = Longitude
				 FROM GeoPosition 
				 WHERE PostalCode = ISNULL(@NearestPostalCode,@DefaultPostalCode)

			  SET @UserOutOfRange = 1
		END

		--Set Constant calculations to a variable
		SET @DistanceSin = SIN (@Latitude / 57.2958)
		SET @Distancecos = COS (@Latitude / 57.2958)
		SET @DistancecosLong = (@Longitude / 57.2958)

		-- To collect Retailers belonging to a HubCiti
		SELECT DISTINCT  RB.BusinessCategoryID
			,RA.HcHubCitiID
			,Radius = ROUND((ACOS((RL.SINRetailLocationLatitude * @DistanceSin + RL.COSRetailLocationLatitude * @Distancecos * COS(@DistancecosLong - (RL.RetailLocationLongitude / @DecToRad))))*6371) * 0.6214 ,1,1) 				
			INTO 
				#HubCitiRetailers
			FROM @CityList CL   
			INNER JOIN RetailLocation RL ON RL.PostalCode = CL.PostalCode 
			INNER JOIN HcRetailerAssociation RA  ON RA.RetailLocationID = RL.RetailLocationID AND RA.RetailID = RL.RetailID 
			INNER JOIN RetailerBusinessCategory RB  ON RB.RetailerID = RA.RetailID 
			WHERE RL.Active = 1 AND RA.Associated = 1 AND RA.HcHubCitiID = @HubCitiID AND RL.Headquarters = 0
	 
			--To display Retailers based on Menuitem or Bottombutton
			IF @HcMenuItemID IS NULL 
			BEGIN
				SELECT  DISTINCT B.BusinessCategoryID AS catID
					,B.BusinessCategoryName AS CatDisName
					,CASE WHEN HcFindCategoryImageID IS NULL THEN @ConfigirationType + I.CategoryImagePath
						WHEN HcFindCategoryImageID IS NOT NULL AND IsUpdated = 0 THEN @ConfigirationType + I.CategoryImagePath
						WHEN HcFindCategoryImageID IS NOT NULL AND IsUpdated = 1 THEN @Config + CAST(@HubCitiID AS VARCHAR(100)) + '/' + I.CategoryImagePath
					END AS CatImgPth
					FROM #HubCitiRetailers RB
					INNER JOIN HcBottomButtonFindRetailerBusinessCategories C  ON C.BusinessCategoryID = RB.BusinessCategoryID
					INNER JOIN HcBottomButton M  ON C.HcBottomButonID = M.HcBottomButtonID 
					INNER JOIN BusinessCategory B  ON B.BusinessCategoryID = C.BusinessCategoryID
					LEFT JOIN HcFindCategoryImage I ON I.BusinessCategoryID = B.BusinessCategoryID 
					WHERE B.BusinessCategoryName <> 'Other' 
					AND M.HcBottomButtonID = @HcBottmoButtonID AND I.HcHubCitiID = @HubCitiID AND Radius <= @Radius
						ORDER BY CatDisName ASC				
			END
			ELSE 
			BEGIN
				SELECT  DISTINCT B.BusinessCategoryID AS catID
					,B.BusinessCategoryName AS CatDisName
					,CASE WHEN HcFindCategoryImageID IS NULL THEN @ConfigirationType + I.CategoryImagePath
						WHEN HcFindCategoryImageID IS NOT NULL AND IsUpdated = 0 THEN @ConfigirationType + I.CategoryImagePath
						WHEN HcFindCategoryImageID IS NOT NULL AND IsUpdated = 1 THEN @Config + CAST(@HubCitiID AS VARCHAR(100)) + '/' + I.CategoryImagePath
					END AS CatImgPth
					FROM #HubCitiRetailers RB
					INNER JOIN HcMenuFindRetailerBusinessCategories C  ON RB.BusinessCategoryID = C.BusinessCategoryID
					INNER JOIN HcMenuItem M  ON C.HcMenuItemID = M.HcMenuItemID 
					INNER JOIN BusinessCategory B  ON B.BusinessCategoryID = C.BusinessCategoryID
					LEFT JOIN HcFindCategoryImage I ON I.BusinessCategoryID = B.BusinessCategoryID 
					WHERE B.BusinessCategoryName <> 'Other' 
					AND M.HcMenuItemID = @HcMenuItemID AND I.HcHubCitiID = @HubCitiID AND Radius <= @Radius
						ORDER BY CatDisName ASC					
			END

			

			--To display Pop-up for all the User preferred cities with no data
			IF @@ROWCOUNT =  0   
			BEGIN
														
				SELECT @UserPrefCities = COALESCE(@UserPrefCities+',' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName)))
					FROM (SELECT DISTINCT CityName FROM @CityList) A	
						
				SELECT @NoRecordsMsg = CASE WHEN @RegionAppFlag = '0' 
											THEN 'There currently is no information for your city preferences.\n\n' + @UserPrefCities + '.\n\nUpdate your city preferences in the settings menu.'
											ELSE 'No Records Found.' 
										END															
			END
	
			--To display Bottom buttons for 'Find module'
			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			     , HM.HcHubCitiID 
				 , BB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On 
				        WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  
				   END AS bottomBtnImg
				 , CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off 
				        WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  
				   END  AS bottomBtnImgOff
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) AS btnLinkID
				 , (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID 
							  ) = 1  THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName 
																		FROM HcBottomButtonFindRetailerBusinessCategories A 
																		INNER JOIN BusinessCategory C ON C.BusinessCategoryID =A.BusinessCategoryID 
																		WHERE HcBottomButonID = BB.HcBottomButtonID
																) 
									ELSE BottomButtonLinkTypeName END) AS btnLinkTypeName					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB  ON HM.HcHubCitiID = FBB.HcHubCitiID 
			INNER JOIN HcBottomButton BB  ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT  ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT  ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = 'Find Category'
			LEFT JOIN HcBottomButtonImageIcons BI  ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 	
			WHERE FBB.HcHubCitiID = @HubCitiID 

			SET @Status = 0

	END TRY
		
	BEGIN CATCH
		SET @ErrorNumber = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @Status = 1 
	END CATCH;
END;












GO
