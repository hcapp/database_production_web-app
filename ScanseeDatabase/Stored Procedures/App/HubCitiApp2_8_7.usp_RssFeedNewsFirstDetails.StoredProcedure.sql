USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_RssFeedNewsFirstDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssFeedNewsDetails
Purpose					: 
Example					: usp_WebRssFeedNewsDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_RssFeedNewsFirstDetails]
(	
	  @RssFeedNewsID int
	  		
	--Output Variable 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			   

			SELECT RssNewsFirstFeedNewsID itemId
				,NewsType feedType
				,Title title
				,ImagePath image
				,ShortDescription sDesc
				,LongDescription lDesc
				,Link link
				,date = CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END
				,PubStrtDate= convert(datetime,PublishedDate,105) 
				,time = FORMAT(CAST(PublishedTime AS DATETIME),'hh:mm tt')
				,Classification
				,Section section
				,AdCopy adcopy
				,author
				,subcategory
				,VideoLink
			FROM RssNewsFirstFeedNews
			WHERE RssNewsFirstFeedNewsID = @RssFeedNewsID			 
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;














GO
