USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcGalleryAllCouponsByLocation]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcGalleryAllCouponsByLocation
Purpose					: To Display All Coupons By Location
Example					: usp_HcGalleryAllCouponsByLocation

History
Version		 Date		     Author	           Change Description
------------------------------------------------------------------------------- 
1.0		   21stNov2013	 Dhananjaya TR	   Initial Version
2.0        12/13/2016    Shilpashree       Introducing Sort & filter items
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcGalleryAllCouponsByLocation]
(
	--Input Variables 	
	   @HcUserID int
	 , @HcHubcitiID int
	 , @RetailID int
	 , @SearchKey varchar(500)
	 , @Latitude Decimal(18,6)
	 , @Longitude Decimal(18,6)
	 , @PostalCode varchar(100)	
	 , @LowerLimit int
	 , @ScreenName varchar(100)
	 , @SortColumn varchar(50)
	 , @SortOrder varchar(10)   
	 , @HcCityID Varchar(MAX)
	 , @BusinessCategoryIDs varchar(MAX)
	   
	--UserTracking Input Variables
	 , @MainMenuID int
	 
	--Output Variables
	 , @Label varchar(50) output
	 , @UserOutOfRange bit output
	 , @DefaultPostalCode varchar(50) output
	 , @Status bit output
	 , @MaxCnt int output
	 , @NxtPageFlag bit output	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
			
		--To get Media Server Configuration.  
		DECLARE  @DistanceFromUser FLOAT
				, @Config VARCHAR(500)
				, @RetailerConfig VARCHAR(500)
				, @LowerLimitLatest INT
				, @UpperLimit int 
				, @Radius FLOAT
				, @ModuleName Varchar(50)
				, @LocCategory Int
				, @UserLatitude float
				, @UserLongitude float 
		
		CREATE TABLE #RetailerBusinessCategory(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)
		CREATE TABLE #FilteredCoupons(CouponID int, HcHubCitiID int, RetailID int, RetailLocationID int,NoOfCouponsToIssue int,UsedCoupons int)
		CREATE TABLE #Coupons(CouponID INT
								, CouponName VARCHAR(500)
								, RetailID INT
								, RetailName VARCHAR(200)
								, CouponImagePath VARCHAR(500)
								, BannerTitle VARCHAR(1000)
								, Distance FLOAT
								, Counts INT
								, Featured BIT)

		SET @LowerLimitLatest = @LowerLimit + 1
		SET @ModuleName = 'Deals'
		SET @RetailID = ISNULL(@RetailID,0)
		SET @Label = ISNULL(@SortColumn,'Distance')
		SET @UserLatitude = @Latitude
        SET @UserLongitude = @Longitude
		
		SELECT Param CityID
		INTO #CityIDs
			FROM fn_SplitParam(@HcCityID,',')

		SELECT Param BusCatID
		INTO #BusCatIDs
			FROM fn_SplitParam(@BusinessCategoryIDs,',')

		SELECT @Config = ScreenContent
			FROM AppConfiguration
				WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT @RetailerConfig= ScreenContent  
			FROM AppConfiguration   
				 WHERE ConfigurationType='Web Retailer Media Server Configuration'

		SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
				WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1  
		
		SELECT @Radius = LocaleRadius
			FROM HcUserPreference 
				WHERE HcUserID = @HcUserID

		IF (@Radius IS NULL)
		BEGIN

			SELECT @Radius=ScreenContent 
				FROM AppConfiguration
					WHERE ScreenName = 'DefaultRadius' AND ConfigurationType = 'DefaultRadius'	
		END
		
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
		END
            --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID
		END

		EXEC [HubCitiapp2_8_7].[usp_HcUserHubCitiRangeCheck] @HcUserID, @HcHubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
		BEGIN
			IF @PostalCode IS NOT NULL
			BEGIN
				SELECT @Latitude = Latitude
						, @Longitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
			END
			ELSE 
			BEGIN
				SELECT @Latitude = G.Latitude
						, @Longitude = G.Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
				WHERE U.HcUserID = @HcUserID
			END
		END	

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
			FROM Retailer 
				WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive = 1

		SELECT DISTINCT BusinessCategoryID,HcBusinessSubCategoryID 
		INTO #UserPrefLocCategories
			FROM HcUserPreferredCategory 
				WHERE HcUserID = @HcUserID AND HcHubCitiID = @HcHubcitiID 

		SELECT @LocCategory = COUNT(1) FROM #UserPrefLocCategories 

		IF (@LocCategory = 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
					WHERE RA.HcHubCitiID = @HcHubcitiID
		END
		ELSE IF (@LocCategory > 0)
		BEGIN
			INSERT INTO #RetailerBusinessCategory(RetailerID,BusinessCategoryID,BusinessSubCategoryID)
			SELECT DISTINCT RBC.RetailerID, RBC.BusinessCategoryID, RBC.BusinessSubCategoryID
				FROM RetailerBusinessCategory RBC
				INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID AND Associated = 1
				INNER JOIN #UserPrefLocCategories PC ON RBC.BusinessCategoryID = PC.BusinessCategoryID --AND RBC.BusinessSubCategoryID = PC.HcBusinessSubCategoryID
					WHERE RA.HcHubCitiID = @HcHubcitiID
		END
		
		--INSERT HubCiti Coupons
		INSERT INTO #FilteredCoupons(CouponID,HcHubCitiID,RetailID,RetailLocationID,NoOfCouponsToIssue,UsedCoupons)
		SELECT DISTINCT C.CouponID
						,C.HcHubCitiID
						,CR.RetailID
						,CR.RetailLocationID
						,NoOfCouponsToIssue
						,UsedCoupons = COUNT (HCUserCouponGalleryID)
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID 
			INNER JOIN HcRetailerAssociation RA ON CR.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubcitiID AND RA.Associated = 1 
			LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
				WHERE C.HcHubCitiID = @HcHubcitiID AND C.RetailID IS NULL
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
					GROUP BY C.CouponID
							,C.HcHubCitiID
							,CR.RetailID
							,CR.RetailLocationID
							,NoOfCouponsToIssue
						HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
	
		--INSERT HubCiti Associted Retailer Coupons
		INSERT INTO #FilteredCoupons(CouponID,RetailID,RetailLocationID,NoOfCouponsToIssue,UsedCoupons)
		SELECT DISTINCT C.CouponID
						,CR.RetailID
						,CR.RetailLocationID
						,NoOfCouponsToIssue
						,UsedCoupons = COUNT (HCUserCouponGalleryID)
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID AND C.RetailID = CR.RetailID
			INNER JOIN HcRetailerAssociation RA ON CR.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubcitiID AND RA.Associated = 1 
			LEFT JOIN HcUserCouponGallery UG ON C.CouponID = UG.CouponID
				WHERE C.HcHubcitiID IS NULL
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
					GROUP BY C.CouponID
							,CR.RetailID
							,CR.RetailLocationID
							,NoOfCouponsToIssue
						HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
							ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
		
		SELECT DISTINCT FC.CouponID
					,R.RetailID
					,R.RetailName
					,FC.RetailLocationID
					,RL.HcCityID
					,RL.PostalCode
					,RL.RetailLocationLatitude
					,RL.RetailLocationLongitude
					,RBC.BusinessCategoryID
					--,RBC.BusinessSubCategoryID
					,Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) 
					,DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)    
		INTO #RetailerSet
		FROM #FilteredCoupons FC
		INNER JOIN #RetailerBusinessCategory RBC  ON FC.RetailID = RBC.RetailerID
		INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1
		INNER JOIN Retaillocation RL ON R.RetailID = RL.RetailID AND FC.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
		INNER JOIN HcLocationAssociation HL ON RL.HcCityID = HL.HcCityID AND RL.PostalCode = HL.PostalCode AND HL.HcHubCitiID = @HcHubcitiID
		LEFT JOIN GeoPosition G ON HL.PostalCode = G.Postalcode AND G.City = HL.City
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL
		AND ((@RetailID <> 0 AND FC.RetailID = @RetailID) OR @RetailID = 0)
	
		SELECT DISTINCT C.CouponID
					, C.CouponName
					, R.RetailID 
					, RetailName 
					, R.RetailLocationID
					, CouponImagePath = CASE WHEN C.HcHubCitiID IS NOT NULL THEN @Config + CAST(@HcHubCitiID as varchar(10))+'/' + CouponImagePath 
											 WHEN C.RetailID IS NOT NULL AND C.HcHubCitiID IS NULL AND CouponImagePath  = 'uploadIconSqr.png' THEN @RetailerConfig + CouponImagePath ELSE @RetailerConfig + CAST(C.RetailID as varchar(10))+'/' + CouponImagePath 
											 END
					, C.BannerTitle
					, Distance = CASE WHEN @Latitude IS NULL THEN  ISNULL(Distance,DistanceActual ) ELSE ISNULL(DistanceActual,Distance) END
					, Featured = IIF(F.Featured IS NULL,0,F.Featured)
		INTO #CouponsList
		FROM #RetailerSet R
		INNER JOIN Coupon C ON R.CouponID = C.CouponID
		LEFT JOIN HubCitiFeaturedCoupon F ON C.CouponID = F.CouponID AND F.HubCitiID = @HcHubcitiID  
			WHERE Distance <= @Radius
			AND (@BusinessCategoryIDs IS NULL OR (R.BusinessCategoryID IN (SELECT Buscatid FROM #BusCatIDs)))
			AND (@HcCityID IS NULL OR (R.HcCityID IN (SELECT CityID FROM #CityIDs)))
			AND (C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
				OR C.KeyWords LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END)
		
		SELECT DISTINCT CouponID,CouponName,RetailID,RetailName
		INTO #UniqueCoupons
		FROM #CouponsList

		SELECT  CL.CouponID 
				, CL.CouponName 
				, CL.RetailID 
				, CL.RetailName
				, Counts = COUNT(CR.RetailLocationID)
		INTO #CouponsCount
		FROM #UniqueCoupons CL
		INNER JOIN CouponRetailer CR ON CL.CouponID = CR.CouponID
		GROUP BY  CL.CouponID
				, CL.CouponName
				, CL.RetailID 
				, CL.RetailName 

		INSERT INTO #Coupons
		SELECT  CouponID 
				, CouponName 
				, RetailID 
				, RetailName
				, CouponImagePath 
				, BannerTitle 
				, Distance 
				, Counts 
				, Featured 
		FROM(
		SELECT DISTINCT CL.CouponID 
				, CL.CouponName 
				, CL.RetailID 
				, CL.RetailName
				, CL.CouponImagePath 
				, CL.BannerTitle 
				, Distance = MIN(Distance)
				, Counts
				, Featured 
		FROM #CouponsList CL
		INNER JOIN #CouponsCount CC ON CL.CouponID = CC.CouponID AND CL.RetailID = CC.RetailID
		GROUP BY CL.CouponID 
				, CL.CouponName 
				, CL.RetailID 
				, CL.RetailName
				, CL.CouponImagePath 
				, CL.BannerTitle 
				, Counts
				, Featured 
		)C
		
		-- User Tracking Section            
		CREATE TABLE #Temp5(CouponListID int
							,CouponID int)
		
        DECLARE @SQL VARCHAR(500) = 'ALTER TABLE #Temp5 ADD RowNum1 INT IDENTITY('+CAST(@LowerLimitLatest AS VARCHAR(10))+', 1)'							
		EXEC (@SQL)

		INSERT INTO HubCitiReportingDatabase..CouponsList (MainMenuID
													      ,CouponID
													      ,DateCreated)
		OUTPUT inserted.CouponsListID,inserted.CouponID INTO #Temp5(CouponListID,CouponID)
														  
		SELECT @MainMenuID 
			   ,CouponID 
			   ,GETDATE() 
		FROM #Coupons  	

		SELECT rowNum = ROW_NUMBER() OVER (ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn IS NULL) 
															THEN CAST(Distance AS SQL_VARIANT)
														 WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' 
															THEN CAST(CouponName AS SQL_VARIANT)
													END ASC ,Distance) 
				  , C.couponId
				  , couponName
				  , RetailID 
				  , RetailName 
				  , couponImagePath
				  , bannerTitle
				  , distance 
				  , counts
			INTO #FeaturedCoupons
			FROM #Coupons C
				WHERE Featured = 1
					ORDER BY RowNum

			SELECT rowNum
				  , couponListId
				  , C.couponId
				  , couponName
				  , RetailID retId
				  , RetailName retName
				  , couponImagePath
				  , bannerTitle
				  , distance 
				  , counts
			FROM #FeaturedCoupons C
			INNER JOIN #Temp5 T ON C.CouponID = T.CouponID
				ORDER BY RowNum

		SELECT RowNum = ROW_NUMBER() OVER (ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn IS NULL) 
															THEN CAST(Distance AS SQL_VARIANT)
														 WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' 
															THEN CAST(CouponName AS SQL_VARIANT)
													END ASC ,Distance) 
				  , CouponID
				  , CouponName
				  , RetailID
				  , RetailName
				  , CouponImagePath
				  , BannerTitle
				  , Distance 
				  , Counts
		INTO #NonFeaturedCoupons
			FROM #Coupons
				WHERE Featured = 0
					ORDER BY RowNum
									   
		--To capture max row number.  
		SELECT @MaxCnt = MAX(RowNum) FROM #NonFeaturedCoupons  
 
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 						

		SELECT DISTINCT RowNum
				  , CouponID
				  , CouponName
				  , RetailID retId
				  , RetailName retName
				  , CouponImagePath
				  , BannerTitle
				  , Distance
				  , Counts
		INTO #NonFeaturedCouponList
			FROM #NonFeaturedCoupons
				WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
					ORDER BY RowNum 
		
		SELECT	rowNum  
      	      , couponListId
			  , CL.couponId
			  , couponName
			  , retId
			  , retName
			  , couponImagePath
			  , bannerTitle
			  , distance 
			  , counts
		FROM #NonFeaturedCouponList CL
		INNER JOIN #Temp5 T ON CL.CouponID = T.CouponID--CL.RowNum = T.RowNum1
		ORDER BY RowNum

		--To get list of BottomButtons for this Module
		SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
				, BB.HcBottomButtonID AS bottomBtnID
				, bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HcHubcitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				, bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HcHubcitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				, BottomButtonLinkTypeID AS btnLinkTypeID
				, btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HcHubcitiID), BottomButtonLinkID) 
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
											INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
											WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubcitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
																																				INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																				INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																				WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubcitiID)
										WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
											WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HcHubcitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																							WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubcitiID)
			                      	 
								ELSE BottomButtonLinkTypeName END)					
		FROM HcMenu HM
		INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
		INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
		INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
		INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
		LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
		WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HcHubcitiID
		ORDER by HcFunctionalityBottomButtonID

		--Confirmation of Success.
		SET @Status = 0
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcGalleryAllCouponsByLocation.'		
			EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		
			--Confirmation of Failure.
			SET @Status = 1
		
		END;
		 
	END CATCH;
END;




GO
