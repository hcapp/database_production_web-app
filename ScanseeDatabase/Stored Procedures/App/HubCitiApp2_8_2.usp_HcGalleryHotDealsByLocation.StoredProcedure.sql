USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcGalleryHotDealsByLocation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_HcGalleryHotDealsByLocation]  
Purpose     : To display all the HotDeals related to the user in user gallery.  
Example     : [usp_HcGalleryHotDealsByLocation]  
  
History  
Version  Date           Author          Change Description  
---------------------------------------------------------------   
1.0      3rd JAN 2014	Dhananjaya TR 	Initial Version
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcGalleryHotDealsByLocation]  
(  

	   @UserID int
	 , @SearchKey varchar(100)
	 , @LowerLimit int
     , @ScreenName varchar(100)
	 , @HCHubCitiID Int
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(10)
	
	--UserTracking Input Variables
	 , @MainMenuID int
	 
	--Output Variables
	 , @UserOutOfRange bit output
	 , @DefaultPostalCode varchar(50) output 
	 , @MaxCnt int output
	 , @NxtPageFlag bit output	
	 , @Status bit output   
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)  
AS  
BEGIN  
  
	BEGIN TRY  

			DECLARE @DistanceFromUser FLOAT
 
			 --Derive the Latitude and Longitude in the absence of the input.
            IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
            BEGIN
                    --If the postal code is passed then derive the co ordinates.
                    IF @PostalCode IS NOT NULL
					BEGIN
                            SELECT @Latitude = Latitude
                                , @Longitude = Longitude
                            FROM GeoPosition 
                            WHERE PostalCode = @PostalCode
                    END		
					ELSE
					BEGIN
						SELECT @Latitude = G.Latitude
								, @Longitude = G.Longitude
						FROM GeoPosition G
						INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
						WHERE U.HcUserID = @UserID	
					END												
            END    
             
			
			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
		 
		 
		  --To get Media Server Configuration.  
		  DECLARE @RetailConfig varchar(50)    
		  SELECT @RetailConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Retailer Media Server Configuration'  
		       
		  --To get the row count for pagination.  
		  DECLARE @UpperLimit int   
		  SELECT @UpperLimit = @LowerLimit + ScreenContent   
		  FROM AppConfiguration   
		  WHERE ScreenName = @ScreenName   
		  AND ConfigurationType = 'Pagination'  
		  AND Active = 1  
		    
		 	--To fetch all the duplicate retailers.
			SELECT DISTINCT DuplicateRetailerID 
			INTO #DuplicateRet
			FROM Retailer 
			WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive=1
		 	
		  CREATE TABLE #Gallery(Row_Num INT IDENTITY(1, 1)
							  , UserHotDealGalleryID  int
							  , HotDealID INT 
							  , HotDealName VARCHAR(255)
							  , RetailID INT
							  , RetailName VARCHAR(200)
							  , RetailLocationID INT
							  , [Address] VARCHAR(2000)
							  , HotDealDiscountAmount MONEY
							  , HotDealDiscountPct FLOAT
							  , HotDealDescription VARCHAR(1000) 
							  , HotDealStartDate DATETIME
							  , HotDealEndDate DATETIME
							  , HotDealExpireDate DATETIME
							  , HotDealURL VARCHAR(1000)
							  , HotDealImagePath VARCHAR(1000)
							  , UsedFlag BIT
							  , CategoryID int
							  , CategoryName varchar(100)
							  , APIPartnerID INT
                              , APIPartnerName VARCHAR(50))
							  
	
		   INSERT INTO #Gallery(UserHotDealGalleryID
									,HotDealID   
									,HotDealName
									,RetailID
									,RetailName
									,RetailLocationID
									,[Address]  
									,HotDealDiscountAmount  
									,HotDealDiscountPct  
									,HotDealDescription 							
									,HotDealStartDate
									,HotDealEndDate  
									,HotDealExpireDate  
									,HotDealURL  
									,HotDealImagePath  
									,UsedFlag  
									,CategoryID
									,CategoryName
									,APIPartnerID
                                    ,APIPartnerName)
									
				   SELECT DISTINCT  HcUserHotDealGalleryID
									,P.ProductHotDealID   
									,HotDealName
									,RL.RetailID
									,RetailName
									,RL.RetailLocationID
									,[Address]= (RL.Address1+', '+RL.City+', '+RL.[State]+', '+RL.PostalCode)  
									,HotDealDiscountAmount  
									,HotDealDiscountPct  
									,CASE WHEN HotDealShortDescription  IS NOT NULL THEN HotDealShortDescription ELSE HotDeaLonglDescription END
									,HotDealStartDate  
									,HotDealEndDate
									,HotDealExpirationDate
									,HotDealURL  
									,HotDealImagePath=CASE WHEN HotDealImagePath IS NULL
														 THEN [HubCitiApp2_3_3].[fn_HotDealImagePath](P.ProductHotDealID)
													  ELSE 
														 CASE WHEN HotDealImagePath IS NOT NULL
															  THEN CASE WHEN P.WebsiteSourceFlag = 1 
																		THEN @RetailConfig +CONVERT(VARCHAR(30),P.RetailID)+'/'+HotDealImagePath 
																	ELSE HotDealImagePath 
																	END
														 END 
													 END  
									,Used 
									,CategoryID = CASE WHEN C.CategoryID IS NULL THEN 0 ELSE C.CategoryID END
									,CategoryName = CASE WHEN C.ParentCategoryName IS NULL THEN 'Others' ELSE C.ParentCategoryName +' - '+C.SubCategoryName END 
									,A.APIPartnerID
                                    ,APIPartnerName
					FROM ProductHotDeal P
					INNER JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
					LEFT JOIN ProductHotDealRetailLocation PHRL ON P.ProductHotDealID = PHRL.ProductHotDealID
					LEFT JOIN RetailLocation RL ON PHRL.RetailLocationID = RL.RetailLocationID AND RL.Active=1
					LEFT JOIN Retailer R ON RL.RetailID = R.RetailID AND R.RetailerActive=1
					LEFT JOIN Category C ON P.CategoryID = C.CategoryID
					LEFT JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
					LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
					WHERE GETDATE() BETWEEN ISNULL(P.HotDealStartDate,GETDATE()-1) AND ISNULL(P.HotDealExpirationDate,GETDATE()+1)  
					AND HcUserID = @UserID AND Used = 0 AND D.DuplicateRetailerID IS NULL
					AND HotDealName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
					ORDER BY RetailName,[Address],CategoryName,HotDealName ASC    		
				
				  --To capture max row number.  
				  SELECT @MaxCnt = MAX(Row_Num) FROM #Gallery  
				  --this flag is a indicator to enable "More" button in the UI.   
				  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
				   SELECT  Row_Num rowNumber 
							,UserHotDealGalleryID hDGallId
							,HotDealID   
							,HotDealName
							,RetailID retId
							,RetailName retName
							,RetailLocationID retLocId
							,[Address]    
							,HotDealDiscountAmount hDDiscountAmount  
							,HotDealDiscountPct hDDiscountPct  
							,HotDealDescription hDDesc
							,HotDealStartDate hDStartDate  
							,HotDealEndDate hDEndDate 
							,HotDealExpireDate hDExpDate  
							,HotDealURL  hdURL
							,HotDealImagePath  
							,UsedFlag 
							,CategoryID catId
							,CategoryName catName 
							,APIPartnerID
                            ,APIPartnerName
				  FROM #Gallery  
				  WHERE Row_Num BETWEEN (@LowerLimit + 1) AND  @UpperLimit  
				 ORDER BY RetailName,[Address],CategoryName,HotDealName ASC 		    
  
	END TRY  
    
	 BEGIN CATCH  
	   
	--Check whether the Transaction is uncommitable.  
	IF @@ERROR <> 0  
	BEGIN  
	PRINT 'Error occured in Stored Procedure [usp_HcGalleryHotDealsByLocation].'    
	--- Execute retrieval of Error info.  
	EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
	END;  
	 
	 END CATCH;  
END;








































GO
