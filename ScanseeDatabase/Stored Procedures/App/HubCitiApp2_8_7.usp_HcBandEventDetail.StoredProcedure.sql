USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcBandEventDetail]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiapp2_8_7].[usp_HcBandEventDetail]
Purpose					: To display Event Details.
Example					: [HubCitiapp2_8_7].[usp_HcBandEventDetail]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25thApr2016	    Sagar Byali			1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcBandEventDetail]
(   
    --Input variable.	  
	  @HcEventID Int

	--User Tracking
	--, @EventsListID Int
	, @HcHubCitiID Int
  
	--Output Variable 
	--, @EventLogisticSSQRPath Varchar(2000) Output
	, @AppSiteFlag bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @HcBandEventCategoryID Varchar(2000)
			DECLARE @AppSiteID Int
			DECLARE @RetailLocationID Varchar(2000)
			DECLARE @HotelFlag Bit
			DECLARE @Config VARCHAR(500)
			DECLARE @WeeklyDays Varchar(2000)
			DECLARE @CategoryNames VARCHAR(1000)
			DECLARE @RetailConfig Varchar(1000)
			DECLARE @EventsLogisticSSQR VARCHAR(2000)
			SET @AppSiteFlag=0

			SELECT @RetailConfig = ScreenContent
            FROM AppConfiguration 
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'	

			SELECT @EventsLogisticSSQR=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType ='QR Code Configuration'

			--IF EXISTS(SELECT 1 FROM HcEvents WHERE HcBandEventID =@HcBandEventID AND EventsLogisticFlag =1)
			--BEGIN			 

			--	SET @EventLogisticSSQRPath =  @EventsLogisticSSQR + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Events Logistic Page') as varchar(10)) + '.htm?eventId=' + CAST(@HcBandEventID AS VARCHAR(10)) + '&hubcitiId=' + CAST(@HcHubCitiID AS VARCHAR(10))     
			
			--END


	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT ID, Day
			INTO #DayLookup
			FROM 			
			(
				SELECT 1 AS 'ID', 'Sunday' 'Day'
				UNION
				SELECT 2, 'Monday'
				UNION
				SELECT 3, 'Tuesday'
				UNION
				SELECT 4, 'Wednesday'
				UNION
				SELECT 5, 'Thursday'
				UNION
				SELECT 6, 'Friday'
				UNION
				SELECT 7, 'Saturday')A
			
			SELECT @HcBandEventCategoryID=COALESCE(@HcBandEventCategoryID+',', '')+CAST(A.HcBandEventCategoryID AS VARCHAR)
				 , @CategoryNames =  COALESCE(@CategoryNames+',', '')+B.HcBandEventCategoryName
			FROM HcBandEventsCategoryAssociation A
			INNER JOIN HcBandEventsCategory B ON A.HcBandEventCategoryID = B.HcBandEventCategoryID
			WHERE HcBandEventID = @HcEventID 

			SELECT @AppSiteID=HcBandAppsiteID 
			FROM HcbandEventAppsite 
			WHERE HcBandEventID = @HcEventID 
            
			--Select Event Package if Exist
			--SELECT @RetailLocationID=COALESCE(@RetailLocationID+',', '')+CAST(RetailLocationID AS VARCHAR)
			--FROM HcEventPackage 
			--WHERE HcBandEventID = @HcBandEventID
			
			--SELECT @HotelFlag=CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
			--FROM HcBandEventPackage 
			--WHERE HcBandEventID = @HcBandEventID  

			SELECT @WeeklyDays=COALESCE(@WeeklyDays+',', '')+CAST([DayName] AS VARCHAR)
			FROM HcBandEventInterval
			--INNER JOIN HcEventInterval E ON H.HcBandEventID = E.HcBandEventID
			WHERE HcBandEventID = @HcEventID 

			SELECT @AppSiteFlag = IIF(HE.HcBandEventID IS NOT NULL , 1 , 0 )
			FROM HcAppSite HA
			LEFT JOIN HcBandEventAppsite HE ON HA.HcAppSiteID = HE.HcBandAppsiteID AND HE.HcBandEventID = @HcEventID --AND HE.HcHubCitiID =@HcHubCitiID
			--LEFT JOIN HcRetailerEventsAssociation REA ON REA.HcBandEventID =@HcBandEventID 
			WHERE (HE.HcBandEventID = @HcEventID )

			
			--DECLARE @AppsiteRetailID int
			--DECLARE @AppsiteRetailLocationID int

			--IF @AppSiteFlag = 1
			--BEGIN
			--	SELECT @AppsiteRetailID = RL.RetailID
			--		  ,@AppsiteRetailLocationID = RL.RetailLocationID
			--	FROM HcBAndEventAppsite EA
			--	INNER JOIN HcAppSite A ON EA.HcAppSiteID = A.HcAppsiteID
			--	INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID 
			--	WHERE HcBandEventID = @HcBandEventID  
			--END

			----To get Retailer Event RetailLocations
			--DECLARE @RetailLocationFlag bit
			--SELECT @RetailLocationFlag = CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
			--FROM HcRetailerEventsAssociation 
			--WHERE HcBandEventID = @HcBandEventID  

			--To check if given Event is Retailer Event or not.
			DECLARE @RetailerEvent bit
			SET @RetailerEvent = 0

			SELECT @RetailerEvent = 1
			FROM HcBandEvents 
			WHERE HcBandEventID = @HcEventID AND BandID IS NOT NULL

			--To get Appsite when Location selected sending RetailID & RetailLocationID.
			DECLARE @EventRetailID int
			DECLARE @EventRetailLocationID int

			--IF @RetailLocationFlag = 1
			--BEGIN
			--	SELECT @EventRetailID = RetailID
			--		  ,@EventRetailLocationID = RetailLocationID
			--	FROM HcRetailerEventsAssociation
			--	WHERE HcBandEventID = @HcBandEventID  
			--END

			


			IF @RetailerEvent = 0 --AND @AppSiteFlag = 1
			BEGIN

					--To display Event Details.
					SELECT DISTINCT E.HcBandEventID EventID
					               ,HcBandEventName eventName 
								   ,ShortDescription shortDes
								   ,LongDescription longDes								
								   ,E.HcHubCitiID  hubCitiId
								   ,imgPath=(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath)
					
								   ,BussinessEvent busEvent
								   ,PackageEvent pkgEvent								
								   ,StartDate =CAST(StartDate AS DATE)
								   ,EndDate =CAST(EndDate AS DATE)
								   ,StartTime =CAST(StartDate AS Time)
								   ,EndTime = CAST(EndDate AS Time) --ISNULL((CAST(EndDate AS Time)),'00:00:00')
								   ,Address
								   ,City
								   ,State
								   ,PostalCode
								   ,Latitude latitude
								   ,EL.Longitude longitude						
								   ,mItemExist =	CASE WHEN(SELECT COUNT(HcMenuItemID)
													  FROM HcMenuItem MI 
													  INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID											  
													  WHERE LinkTypeName = 'Events' 
													  AND MI.LinkID = @HcEventID )>0 THEN 1 ELSE 0 END
								  ,@HcBandEventCategoryID  eventCatIds		
								  ,PackageDescription  pkgDes
								  ,PackageTicketURL pkgTicketURL 
								  ,PackagePrice pkgPrice
								  ,@HotelFlag HotelFlag		
								  ,MoreInformationURL moreInfoURL
								  ,ISNULL(OnGoingEvent,0) isOnGoing
								  ,recurringDays = CASE WHEN R.RecurrencePattern = 'Daily'
												 THEN CASE WHEN EI.HcBandEventIntervalID IS NULL 
																		THEN 'Occurs Every ' +IIF(E.RecurrenceInterval = 1, '',  CAST(E.RecurrenceInterval AS VARCHAR(100))+' ') + 'Day(s)' 	
																	  ELSE 'Occurs Every Weekday' 
													  END
												WHEN R.RecurrencePattern = 'Weekly'
													THEN 'Occurs Every '+IIF(RecurrenceInterval = 1, '', CAST(RecurrenceInterval AS VARCHAR(10))+' ') + 'Week(s) on ' + STUFF((SELECT ', ' + [DayName]
																																												FROM HcBandEventInterval F			
																																												LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																																												WHERE F.HcBandEventID = E.HcBandEventID
																																												ORDER BY L.ID
																																												FOR XML PATH('')), 1, 2, '')
												WHEN R.RecurrencePattern = 'Monthly'
													THEN CASE WHEN [DayName] IS NULL THEN 'Day '+ CAST(DayNumber AS VARCHAR(10)) + ' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
															  ELSE 'The ' + CASE WHEN DayNumber = 1 THEN 'First '
																				 WHEN DayNumber = 2 THEN 'Second '
																				 WHEN DayNumber = 3 THEN 'Third '
																				 WHEN DayNumber = 4 THEN 'Fourth '
																				 WHEN DayNumber = 5 THEN 'Last ' END													  
															   +  STUFF((SELECT ', ' + [DayName]
																		 FROM HcBandEventInterval F			
																		 LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																		 WHERE F.HcBandEventID = E.HcBandEventID
																		 ORDER BY L.ID
																		 FOR XML PATH('')), 1, 2, '') +' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
														 END
										  END
									--,@AppsiteRetailID RetailID
									--,@AppsiteRetailLocationID RetailLocationID	
									,EventsLogisticFlag 
									,EL.EventLocationTitle evtLocTitle
							
											
									 ,geoError = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(EL.GeoErrorFlag,0) END 
									 ,E.HcEventRecurrencePatternID recurrencePatternID
									 ,R.RecurrencePattern recurrencePatternName	
									 ,[isWeekDay] = CASE WHEN R.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
									 ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
									 ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
									 ,ByDayNumber = CASE WHEN R.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
									 ,DayNumber
									--,MonthInterval	
									 ,EventFrequency AS EndAfter															  	  											
					FROM HcBandEvents E
					LEFT JOIN HcBandEventLocation EL ON E.HcBandEventID =EL.HcBandEventID
					LEFT JOIN HcBandEventInterval EI ON E.HcBandEventID = EI.HcBandEventID
					LEFT JOIN HcEventRecurrencePattern R ON R.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
					--LEFT JOIN HcRetailerEventsAssociation REA ON REA.HcBandEventID =E.HcBandEventID 
					WHERE E.HcBandEventID = @HcEventID	
			END			  	  														


			IF @RetailerEvent = 1
			BEGIN
					--To display Event Details.
			SELECT DISTINCT HcBandEventName eventName 
			               ,ShortDescription shortDes
                           ,LongDescription longDes								
						   ,E.HcHubCitiID  hubCitiId
						   ,imgPath=@RetailConfig+CAST(E.BandID AS VARCHAR)+'/'+E.ImagePath 
						   ,StartDate =CAST(StartDate AS DATE)
						   ,EndDate =CAST(EndDate AS DATE)
						   ,StartTime =CAST(StartDate AS Time)
						   ,EndTime = CAST(EndDate AS Time)--ISNULL((CAST(EndDate AS Time)),'00:00:00')
						   ,Address = 
																		 EL.Address 
										
							,City =  EL.City 
									
							,State = EL.State 
									
							,PostalCode =  EL.PostalCode 
									  	
						
						    ,latitude = EL.Latitude 
									  	
							,longitude =  EL.Longitude 
									 
						   ,@HcBandEventCategoryID  eventCatIds		
						   ,MoreInformationURL moreInfoURL
						   ,ISNULL(OnGoingEvent,0) isOnGoing
						   ,recurringDays = CASE WHEN R.RecurrencePattern = 'Daily'
										 THEN CASE WHEN EI.HcBandEventIntervalID IS NULL 
																THEN 'Occurs Every ' +IIF(E.RecurrenceInterval = 1, '',  CAST(E.RecurrenceInterval AS VARCHAR(100))+' ') + 'Day(s)' 	
															  ELSE 'Occurs Every Weekday' 
											  END
									    WHEN R.RecurrencePattern = 'Weekly'
											THEN 'Occurs Every '+IIF(RecurrenceInterval = 1, '', CAST(RecurrenceInterval AS VARCHAR(10))+' ') + 'Week(s) on ' + STUFF((SELECT ', ' + [DayName]
																																										FROM HcBandEventInterval F			
																																										LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																																										WHERE F.HcBandEventID = E.HcBandEventID
																																										ORDER BY L.ID
																																										FOR XML PATH('')), 1, 2, '')
										WHEN R.RecurrencePattern = 'Monthly'
											THEN CASE WHEN [DayName] IS NULL THEN 'Day '+ CAST(DayNumber AS VARCHAR(10)) + ' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
													  ELSE 'The ' + CASE WHEN DayNumber = 1 THEN 'First '
																		 WHEN DayNumber = 2 THEN 'Second '
																		 WHEN DayNumber = 3 THEN 'Third '
																		 WHEN DayNumber = 4 THEN 'Fourth '
																		 WHEN DayNumber = 5 THEN 'Last ' END													  
													   +  STUFF((SELECT ', ' + [DayName]
																 FROM HcBandEventInterval F			
																 LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																 WHERE F.HcBandEventID = E.HcBandEventID
																 ORDER BY L.ID
																 FOR XML PATH('')), 1, 2, '') +' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
												 END
								  END
							--,@EventRetailID BandID
							--,@EventRetailLocationID RetailLocationID	
							,EventsLogisticFlag						
							,EL.EventLocationTitle evtLocTitle	
							
							
							 ,geoError = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(EL.GeoErrorFlag,0) END 
									 ,E.HcEventRecurrencePatternID recurrencePatternID
									 ,R.RecurrencePattern recurrencePatternName	
									 ,[isWeekDay] = CASE WHEN R.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
									 ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
									 ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
									 ,ByDayNumber = CASE WHEN R.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
									 ,DayNumber
									--,MonthInterval	
									 ,EventFrequency AS EndAfter					  	  											
			FROM HcBandEvents E
			--LEFT JOIN HcRetailerEventsAssociation RE ON RE.HcBandEventID = E.HcBandEventID
			--LEFT JOIN RetailLocation RL ON RE.RetailLocationID = RL.RetailLocationID
			LEFT JOIN HcBandEventLocation EL ON E.HcBandEventID = EL.HcBandEventID
			LEFT JOIN HcBandEventInterval EI ON E.HcBandEventID = EI.HcBandEventID
			LEFT JOIN HcEventRecurrencePattern R ON R.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
			WHERE E.HcBandEventID = @HcEventID	

			END

			--User Tracking

			--Update HubCitiReportingDatabase..EventsList SET EventClick=1
			--WHERE EventsListID =@EventsListID				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
