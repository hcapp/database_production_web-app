USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcGallaryHotDealClip]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcGallaryHotDealClip] 
Purpose					:
Example					: [usp_HcGallaryHotDealClip]  

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd JAN 2013	Dhananjaya TR 	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcGallaryHotDealClip]
(
    --Input Variables
	  @UserID int
	, @HotDealId int
	
	--User Tracking inputs
	, @MainMenuId int 
	
	--Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		 BEGIN TRANSACTION
		 
		 --To Get RetailLocationID for given HotDeal
		 DECLARE @UserHotDealGalleryID int
		 DECLARE @RetailLocationID int
		 SELECT @RetailLocationID = PRL.RetailLocationID
		 FROM ProductHotDeal P
		 INNER JOIN ProductHotDealRetailLocation PRL ON P.ProductHotDealID = PRL.ProductHotDealID
		 WHERE P.ProductHotDealID = @HotDealId
		 
		 --To Check whether user has already claimed HotDeal
		 DECLARE @DuplicateUser int
		 DECLARE @Used int
		 SELECT @DuplicateUser = 1 FROM HcUserHotDealGallery WHERE HcUserID = @UserID AND HotDealID = @HotDealId
		 SELECT @Used = CASE WHEN @DuplicateUser = 1 THEN 1 ELSE 0 END
		
		 
		 
		 --To Check whether HotDeal has crossed Limit
		 DECLARE @NoOfHotDealsToIssue INT
		 DECLARE @Count INT
		 DECLARE @Limit INT
		 SELECT @NoOfHotDealsToIssue = ISNULL(NoOfHotDealsToIssue, 0)
		 FROM ProductHotDeal p
		 WHERE ProductHotDealID = @HotDealId
		 
		 SELECT @Count = ISNULL(Count(DISTINCT HcUserHotDealGalleryID), 0)
		 FROM HcUserHotDealGallery 
		 WHERE HotDealID = @HotDealId
		 
		 SELECT @Limit = CASE WHEN (CASE WHEN @NoOfHotDealsToIssue = 0 THEN @Count+1 ELSE @NoOfHotDealsToIssue END) > @Count THEN 0 ELSE 1 END
		 PRINT @Limit
		 PRINT @Used
		 --If HotDeal has not crossed Limit and User has not Claimed HotDeal before Then user can Claim HotDeal
		 IF @Used = 0 AND @Limit = 0
		 BEGIN
		 INSERT INTO HcUserHotDealGallery( HotDealID
										,HCuserID
										,RetailLocationID
										,ClaimDate
										,Used
									   )
								VALUES( @HotDealId
									   ,@UserID
									   ,@RetailLocationID 
									   ,GETDATE()
									   ,0
									   )	
		
		 --To Capture UserHotDealGalleryID
		 SET @UserHotDealGalleryID = SCOPE_IDENTITY();	
		 SELECT UserHotDealGalleryID = @UserHotDealGalleryID
		 END	
		 					
		 --User Tracking Session
		 UPDATE HubCitiReportingDatabase..HotDealList SET HotDealClipped = 1
		 WHERE ProductHotDealID = @HotDealId AND MainMenuID = @MainMenuId	
													
		 --Confirmation of Success.
		 SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcGallaryHotDealClip] .'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








































GO
