USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFAQDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFAQDetails
Purpose					: Display FAQ Details.
Example					: usp_HcFAQDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFAQDetails]
(
   
    --Input variable. 	  
	  @HcHubcitiID Int	
	, @HcFAQID Int

	--User Tracking
	, @FAQListID Int
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	               
				--Display the FAQ details
								
				SELECT --C.HcFAQCategoryID categoryId
				      --,C.CategoryName categoryName
				      F.HcFAQID faqId
					  --,Question
					  ,Answer							
				FROM HcFAQ F   
				INNER JOIN HcFAQCategory C ON C.HcFAQCategoryID =F.HcFAQCategoryID AND F.HcHubCitiID=@HcHubcitiID AND HcFAQID =@HcFAQID	
				
				--UserTracking
				
				UPDATE HubCitiReportingDatabase..FAQList SET Click = 1 WHERE FAQListID = @FAQListID					
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
