USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcProductSmartSearchProductList]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcProductSmartSearchProductList
Purpose					: To display the product list that matches with the given name & category.
Example					: usp_HcProductSmartSearchProductList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29thOct2013 	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcProductSmartSearchProductList]
(
	
   --Input Variables 
	 @ProdSearch varchar(500)
   , @ParentCategoryID varchar(10)
   , @LowerLimit int
   , @ScreenName varchar(100)
   , @HcHubCitiID Int
   , @UserID Int
     
	--Output Variable 
	, @NxtPageFlag bit output
	, @MaxCnt int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
	
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		DECLARE @Tomorrow DATETIME = GETDATE()+1  
		
		--To get the row count for pagination.
		DECLARE @UpperLimit int 
		SELECT @UpperLimit = @LowerLimit + ScreenContent 
		FROM AppConfiguration 
		WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1		
		
		CREATE TABLE #Temp (
							 RowNum INT IDENTITY(1, 1)
						   , ProductID int
						   , ScanCode varchar(50)
						   , ISBN varchar(50)
						   , ProductName varchar(500)
						   , ProductDescription varchar(MAX)
						   , ProductImagePath varchar(1000)
						   )
						   
		CREATE TABLE #Prod(ProductID INT)	
		
					   
		--List the products irrespective of the category.
		IF @ParentCategoryID = '0' 
			BEGIN
				
				--Match Product Name
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID						 
				FROM Product P
				WHERE (ProductName = @ProdSearch) 
				
				--Match Product Keywords.
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID					 
				FROM ProductKeywords P						
				WHERE  (ProductKeyword = @ProdSearch )
				 
				--Match Product Scan Code.
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID					 
				FROM Product P
				WHERE (ScanCode = @ProdSearch)							
				
				--Match Product ISBN
			    INSERT INTO #Prod(ProductID)
				SELECT P.ProductID						 
				FROM Product P  
				WHERE (ISBN = @ProdSearch)	
				
				--Get the details of the product.
				INSERT INTO #Temp(ProductID
									, ScanCode
									, ISBN
									, ProductName
									, ProductDescription
									, ProductImagePath)
				
				SELECT DISTINCT P.ProductID
					 , P.ScanCode
					 , P.ISBN
					 , P.ProductName
					 , ISNULL(P.ProductShortDescription, P.ProductLongDescription)
					 , CASE WHEN P.WebsiteSourceFlag = 1 
								THEN @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath
							ELSE P.ProductImagePath
					  END
				FROM #Prod Pr
				INNER JOIN Product P ON Pr.ProductID = P.ProductID
				WHERE GETDATE() <= ISNULL(ProductExpirationDate, @Tomorrow)
				
			END
			
			--If the Others (Uncategorized) Section is selected then search against the Uncategorised products.
			ELSE IF @ParentCategoryID = '-1'
			BEGIN
				
				--Match Product Name
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID						 
				FROM Product P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE (ProductName = @ProdSearch )
				AND (ParentCategoryID IS NULL
					OR
					ParentCategoryName = 'Other')
				
				--Match Keywords.
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID					 
				FROM ProductKeywords P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE (ProductKeyword = @ProdSearch )
				AND (ParentCategoryID IS NULL
					OR
					ParentCategoryName = 'Other')
				 
				--Match ScanCode.
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID					 
				FROM Product P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE (ScanCode = @ProdSearch)
				AND (ParentCategoryID IS NULL
					OR
					ParentCategoryName = 'Other')						
				
				--Match Product ISBN
			    INSERT INTO #Prod(ProductID)
				SELECT P.ProductID						 
				FROM Product P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE (ISBN = @ProdSearch)
				AND (ParentCategoryID IS NULL
					OR
					ParentCategoryName = 'Other')
					
				--Get the details of the Products.
				INSERT INTO #Temp(ProductID
									, ScanCode
									, ISBN
									, ProductName
									, ProductDescription
									, ProductImagePath)
					
				SELECT DISTINCT P.ProductID
					 , P.ScanCode
					 , P.ISBN
					 , P.ProductName
					 , ISNULL(P.ProductShortDescription, P.ProductLongDescription)
					 , CASE WHEN P.WebsiteSourceFlag = 1 
								THEN @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath
							ELSE P.ProductImagePath
					  END
				FROM #Prod Pr
				INNER JOIN Product P ON P.ProductID = Pr.ProductID
				WHERE GETDATE() <= ISNULL(ProductExpirationDate, @Tomorrow)
				
			END
			
			--When category is selected.
			ELSE IF @ParentCategoryID > 0
			BEGIN			
				
				--Match Product Name
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID						 
				FROM Product P
				INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				INNER JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE ParentCategoryID = @ParentCategoryID 
				AND (ProductName = @ProdSearch)
				
				--Match Keywords.
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID					 
				FROM ProductKeywords P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE ParentCategoryID = @ParentCategoryID 
				AND (ProductKeyword = @ProdSearch)
				 
				--Match ScanCode.
				INSERT INTO #Prod(ProductID)
				SELECT P.ProductID					 
				FROM Product P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE ParentCategoryID = @ParentCategoryID 
				AND (ScanCode = @ProdSearch)						
				
				--Match Product ISBN
			    INSERT INTO #Prod(ProductID)
				SELECT P.ProductID						 
				FROM Product P
				LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
				LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
				WHERE ParentCategoryID = @ParentCategoryID 
				AND (ISBN = @ProdSearch)
					
				--Get the details of the Products.
				INSERT INTO #Temp(ProductID
									, ScanCode
									, ISBN
									, ProductName
									, ProductDescription
									, ProductImagePath)
					
				SELECT DISTINCT P.ProductID
					 , P.ScanCode
					 , P.ISBN
					 , P.ProductName
					 , ISNULL(P.ProductShortDescription, P.ProductLongDescription)
					 , CASE WHEN P.WebsiteSourceFlag = 1 
								THEN @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath
							ELSE P.ProductImagePath
					  END
				FROM #Prod Pr
				INNER JOIN Product P ON P.ProductID = Pr.ProductID
				WHERE GETDATE() <= ISNULL(ProductExpirationDate, @Tomorrow)
			END
			
			--Calculate the total number of records.
			SELECT @MaxCnt = MAX(RowNum) FROM #Temp	
		
			--Check if the next page exixts.
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
			
			SELECT RowNum rowNumber
				 , ProductID productId
				 , ScanCode scanCode
				 , ISBN prdISBN
				 , ProductName productName
				 , ProductDescription productDescription
				 , ProductImagePath productImagePath
			FROM #Temp
			WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcProductSmartSearchProductList.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;














































GO
