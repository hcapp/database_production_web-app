USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHotDealSearchPagination]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name   : usp_HcHotDealSearchPagination
Purpose                             : To get the list of Hotdeals available.
Example                             : usp_HcHotDealSearchPagination 1, 'o', 0,50

History
Version           Date              Author                  Change Description
--------------------------------------------------------------- 
1.0               9th Aug 2011      SPAN  Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHotDealSearchPagination]
(
        @UserID int
      --, @SortBy char(1)
      , @Category varchar(max)
      , @Search varchar(255)
      , @LowerLimit int
      , @ScreenName varchar(50)
      , @Latitude decimal(18,6)    
      , @Longitude decimal(18,6) 
      --, @PopulationCentreID int  
      --, @Radius int
      , @PostalCode varchar(10) 
      , @HubCitiID int
      
      --User Tracking Inputs  
      , @MainMenuID Int
      
      --OutPut Variable
	  , @UserOutOfRange bit output
	  , @DefaultPostalCode varchar(50) output 
      , @MaxCnt int output 
      , @ByCategoryFlag bit output
      , @FavCatFlag bit output
      , @NxtPageFlag bit output
	  --, @HotDealMarbleFlag bit output
	  , @Status bit output  
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output
)
AS
BEGIN

      
      BEGIN TRY
      
            DECLARE @Today datetime = GETDATE()
            DECLARE @CategorySet bit
            DECLARE @SearchID Int                                 
            DECLARE @HotDealBycityID Int 
			DECLARE @DistanceFromUser FLOAT
            --DECLARE @PostalCode varchar(10)
            --DECLARE @Latitude decimal(18,6)    
            --DECLARE @Longitude decimal(18,6)  
            
            --Check if the User has selected the any categories
            SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
            FROM HcUserCategory 
            WHERE HcUserID = @UserID            
           
		    DECLARE @LocCategory Int

			SELECT DISTINCT BusinessCategoryID 
			INTO #UserLocBusCat
			FROM HcUserPreferredCategory 
			WHERE HcUserID=@UserID --AND HcHubCitiID =@HubCitiID 

			SELECT @LocCategory=COUNT(1) FROM #UserLocBusCat 

			--To get Bottom Button Image URl
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_2].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
            
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
					--If the user has not passed the co ordinates & postal code then cosidered the user configured postal code.
						IF @PostalCode IS NULL
						BEGIN
							SELECT @Latitude = G.Latitude
									, @Longitude = G.Longitude
							FROM GeoPosition G
							INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
							WHERE U.HcUserID = @UserID
						END
						ELSE
						--If the postal code is passed then derive the co ordinates.
						BEGIN
							SELECT @Latitude = Latitude
									, @Longitude = Longitude
							FROM GeoPosition 
							WHERE PostalCode = @PostalCode
						END
			END            
		

            SELECT @FavCatFlag = CASE WHEN @CategorySet = 1 THEN 0 ELSE 1 END 
      
            --To get Server Configuration
            DECLARE @ManufConfig varchar(50) 
            DECLARE @RetailerConfig varchar(50) 
               
            SELECT @ManufConfig=(SELECT ScreenContent  
                                FROM AppConfiguration   
                                        WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
                  ,@RetailerConfig=(SELECT ScreenContent  
                                    FROM AppConfiguration   
                                    WHERE ConfigurationType='Web Retailer Media Server Configuration')
			FROM AppConfiguration  
            
            DECLARE @Radius INT
				
			SELECT @Radius = LocaleRadius
			FROM HcUserPreference
			WHERE HcUserID = @UserID
				
			IF @Radius IS NULL
			BEGIN
				SELECT @Radius = ScreenContent
				FROM AppConfiguration
				WHERE ScreenName = 'DefaultRadius'
			END
      
            --To get the row count for pagination.
            DECLARE @UpperLimit int 
            SELECT @UpperLimit = @LowerLimit + ScreenContent 
            FROM AppConfiguration 
            WHERE ScreenName = @ScreenName 
                  AND ConfigurationType = 'Pagination'
                  AND Active = 1                                                                                                                
            --DECLARE @MaxCnt int          
          							        
			--To capture hotdeals
            CREATE TABLE #HotDeal(ProductHotDealID INT
                                , HotDealName VARCHAR(300)
                                , City VARCHAR(50)
                                , Category VARCHAR(100)
                                , APIPartnerID INT
                                , APIPartnerName VARCHAR(50)
                                , Price MONEY
                                , SalePrice MONEY
                                , HotDealShortDescription VARCHAR(2000)
                                , HotDealImagePath VARCHAR(1000)
                                , HotDealURL VARCHAR(1000)    
                                --, Distance FLOAT
                                , CategoryID int
                                , HotDealStartdate DATETIME
                                , HotDealEndDate DATETIME
                                , HotDealExpirationDate DATETIME
                                , NewFlag BIT
                                , ExternalFlag BIT
                                )  

			 CREATE TABLE #NoCity(ProductHotDealID INT
                                , HotDealName VARCHAR(300)
                                , City VARCHAR(50)
                                , Category VARCHAR(100)
                                , APIPartnerID INT
                                , APIPartnerName VARCHAR(50)
                                , Price MONEY
                                , SalePrice MONEY
                                , HotDealShortDescription VARCHAR(2000)
                                , HotDealImagePath VARCHAR(1000)
                                , HotDealURL VARCHAR(1000)    
                                --, Distance FLOAT
                                , CategoryID int
                                , HotDealStartdate DATETIME
                                , HotDealEndDate DATETIME
                                , HotDealExpirationDate DATETIME
                                , NewFlag BIT
                                , ExternalFlag BIT
                                ) 
								
            
                  CREATE TABLE #NoCityUnCategorized(ProductHotDealID INT
                                , HotDealName VARCHAR(300)
                                , City VARCHAR(50)
                                , Category VARCHAR(100)
                                , APIPartnerID INT
                                , APIPartnerName VARCHAR(50)
                                , Price MONEY
                                , SalePrice MONEY
                                , HotDealShortDescription VARCHAR(2000)
                                , HotDealImagePath VARCHAR(1000)
                                , HotDealURL VARCHAR(1000)    
                                --, Distance FLOAT
                                , CategoryID int
                                , HotDealStartdate DATETIME
                                , HotDealEndDate DATETIME
                                , HotDealExpirationDate DATETIME
                                , NewFlag BIT
                                , ExternalFlag BIT
                                ) 
         
        --To get the HotDeals for which Categories are not mapped, ie. CategoryID is null. 
        --These HotDeal's are grouped under a section called "Others".
            SELECT DISTINCT ProductHotDealID
						  , HotDealName
						  , MIN(City) City
						  , Category
						  , APIPartnerID
						  , APIPartnerName
						  , Price
						  , SalePrice
						  , HotDealShortDescription
						  , HotDealImagePath
						  , HotDealURL
						  --, Distance
						  , CategoryID categoryId
						  , HotDealStartdate
						  , HotDealEndDate  
						  , HotDealExpirationDate           
            INTO #Others
            FROM (SELECT TOP 100 PERCENT p.ProductHotDealID
                              , HotDealName
                              , 'Others' Category
                              , ISNULL(p.APIPartnerID,0) APIPartnerID
                              , APIPartnerName
                              , isnull(Price,0) Price
                              , SalePrice
                              , HotDealShortDescription
                              , HotDealImagePath = CASE WHEN P.WebsiteSourceFlag = 1 THEN
                                                                                          CASE WHEN P.ManufacturerID IS NOT NULL 
                                                                                          THEN @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +HotDealImagePath
                                                                                          ELSE 
                                                                                             CASE WHEN P.RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
                                                                                             THEN CASE WHEN P1.WebsiteSourceFlag = 1 THEN  @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +ProductImagePath ELSE ProductImagePath END
                                                                                             ELSE @RetailerConfig + CONVERT(VARCHAR(30),P.RetailID)+'/' +HotDealImagePath
                                                                                             END   
                                                                                          END
                                                                                    ELSE HotDealImagePath
                                                                           END
                              , HotDealURL
                              , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                              , CategoryID categoryId
                              , PL.City
                              , PL.state
                              --, PCC.PopulationCenterID
                              , HotDealStartdate
                              , HotDealEndDate
                              , HotDealExpirationDate
                  FROM ProductHotDeal P
                  INNER JOIN ProductHotDealLocation PL on PL.ProductHotDealID=P.ProductHotDealID
                  LEFT JOIN HcLocationAssociation HL ON HL.PostalCode =PL.PostalCode AND HL.HCHubCitiID=@HubCitiID
				  INNER JOIN HcRetailerAssociation RLC ON HL.HcHubCitiID = RLC.HcHubCitiID AND RLC.HcHubCitiID =@HubCitiID AND Associated =1  --AND RLC.RetailLocationID =PL.RetailLocationID 
                  INNER JOIN Geoposition G ON G.Postalcode=HL.Postalcode                                   
				  LEFT JOIN APIPartner A on A.APIPartnerID=P.APIPartnerID
				  LEFT JOIN HCProductHotDealInterest PHI ON P.ProductHotDealID = PHI.ProductHotDealID AND PHI.HcUserID = @UserID AND Interested = 0	
				  LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = P.ProductHotDealID
				  LEFT JOIN Product P1 ON P1.ProductID = HDP.ProductID
                  WHERE CategoryID IS NULL AND PHI.ProductHotDealID IS NULL
                        AND P.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END)                         
                        AND GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(P.HotDealEndDate, P.HotDealExpirationDate) , GETDATE() + 1)
                        AND (@Category = 0 OR @Category = '-1')     
				  ORDER BY --CASE WHEN  HL.PostalCode = Pl.PostalCode AND HL.City = 'MARBLE FALLS' AND HL.State = 'TX' THEN 1 ELSE 0 END,
							 CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
                  ) Others             
            WHERE Distance <= @Radius   
            GROUP BY ProductHotDealID
                  , HotDealName 
                  , Category
                  , APIPartnerID
                  , APIPartnerName
                  , Price
                  , SalePrice
                  , HotDealShortDescription
                  , HotDealImagePath
                  , HotDealURL
                  --, Distance
                  , CategoryID
                  , HotDealStartdate
                  , HotDealExpirationDate
                  , HotDealEndDate      
            ORDER BY APIPartnerName,Category, HotDealStartdate ,HotDealName     
            
			IF  EXISTS (SELECT 1 FROM HcHubcitiNationWideDeals WHERE HcHubCitiID=@HubCitiID AND NationWideDealsDisplayFlag =1)
			BEGIN

						
						INSERT INTO #NoCity( ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  , HotDealShortDescription
							  , HotDealImagePath
							  , HotDealURL
							  --, Distance
							  , CategoryID 
							  , HotDealStartdate
							  , HotDealEndDate 
							  , HotDealExpirationDate)
						
						
						
						--To get the deals that are not associated to city.
						SELECT ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  , HotDealShortDescription
							  , HotDealImagePath
							  , HotDealURL
							  --, Distance
							  , CategoryID 
							  , HotDealStartdate
							  , HotDealEndDate 
							  , HotDealExpirationDate               
						FROM (
								SELECT DISTINCT TOP 100 PERCENT p.ProductHotDealID
												  , HotDealName
												  , Category = ISNULL(C.ParentCategoryName, P.Category)
												  , 'No City' City
												  , ISNULL(p.APIPartnerID,0) APIPartnerID
												  , APIPartnerName
												  , isnull(Price,0) Price
												  , SalePrice
												  , HotDealShortDescription
												  , HotDealImagePath = CASE WHEN P.WebsiteSourceFlag = 1 
																			THEN
																					CASE WHEN P.ManufacturerID IS NOT NULL 
																						THEN @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +HotDealImagePath
																					ELSE 
																							CASE WHEN RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
																								THEN CASE WHEN P1.WebsiteSourceFlag = 1 THEN  @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +ProductImagePath ELSE ProductImagePath END
																						ELSE @RetailerConfig + CONVERT(VARCHAR(30),P.RetailID)+'/' +HotDealImagePath
																						END  
																				END
																				ELSE HotDealImagePath
																		END
												  , HotDealURL
												  --, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
												  , P.CategoryID 
												 , HotDealStartdate
												 , HotDealEndDate
												 , HotDealExpirationDate
								FROM ProductHotDeal P
								INNER JOIN HcHubcitiNationWideDeals ND ON P.APIPartnerID = ND.APIPartnerID AND ND.HcHubCItiID = @HubCitiID
								INNER JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
								--INNER JOIN UserCategory UC ON UC.CategoryID = P.CategoryID
								INNER JOIN Category C ON P.CategoryID = C.CategoryID
								LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN Product P1 ON HDP.ProductID = P1.ProductID
								WHERE GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(P.HotDealEndDate, P.HotDealExpirationDate) , GETDATE() + 1)
								--(ISNULL(P.HotDealStartDate,@Today)<= @Today AND ISNULL(P.HotDealEndDate,@Today) >= @Today)
								AND P.HotDealName LIKE CASE WHEN @Search IS NULL THEN '%' ELSE '%'+@Search+'%' END
								AND (((@CategorySet = 1) AND P.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID))
										 OR
									  (@CategorySet = 0 AND 1 = 1))
								AND PL.City IS NULL
								AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM HcProductHotDealInterest WHERE HcUserID = @UserID AND Interested = 0)					
								) Deals    
								ORDER BY CASE WHEN APIPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
		            
            
						PRINT 'A'
						--To get the deals that are not associated to city & categories.
						
						INSERT INTO #NoCityUnCategorized (ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  , HotDealShortDescription
							  , HotDealImagePath
							  , HotDealURL
							  --, Distance
							  , CategoryID
							  , HotDealStartdate
							  , HotDealEndDate   
							  , HotDealExpirationDate)
						
						
						
						SELECT ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  , HotDealShortDescription
							  , HotDealImagePath
							  , HotDealURL
							  --, Distance
							  , CategoryID
							  , HotDealStartdate
							  , HotDealEndDate   
							  , HotDealExpirationDate 
						FROM (
								SELECT DISTINCT TOP 1000 p.ProductHotDealID
												  , HotDealName
												  , 'Others' Category
												  , 'No City' City
												  , ISNULL(p.APIPartnerID,0) APIPartnerID
												  , APIPartnerName
												  , isnull(Price,0) Price
												  , SalePrice
												  , HotDealShortDescription
												  , HotDealImagePath = CASE WHEN P.WebsiteSourceFlag = 1 
																												 THEN
																														  CASE WHEN P.ManufacturerID IS NOT NULL 
																																THEN @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +HotDealImagePath
																														  ELSE 
																																 CASE WHEN RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
																																	   THEN CASE WHEN P1.WebsiteSourceFlag = 1 THEN  @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +ProductImagePath ELSE ProductImagePath END
																																ELSE @RetailerConfig + CONVERT(VARCHAR(30),P.RetailID)+'/' +HotDealImagePath
																																END  
																														END
																													ELSE HotDealImagePath
																												END
												  , HotDealURL
												  --, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
												  , P.CategoryID
												  , HotDealStartdate
												  , HotDealEndDate 
												  , HotDealExpirationDate
								FROM ProductHotDeal P
								INNER JOIN HcHubcitiNationWideDeals ND ON P.APIPartnerID = ND.APIPartnerID AND ND.HcHubCItiID = @HubCitiID
								--INNER JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
								--INNER JOIN UserCategory UC ON UC.CategoryID = P.CategoryID            
								LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
								LEFT JOIN HcProductHotDealInterest PHI ON P.ProductHotDealID = PHI.ProductHotDealID AND PHI.HcUserID = @UserID AND Interested = 0
								LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN Product P1 ON P1.ProductID = HDP.ProductID
								WHERE P.CategoryID IS NULL AND PHI.ProductHotDealID IS NULL
								AND GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(P.HotDealEndDate, P.HotDealExpirationDate) , GETDATE() + 1)
								--AND (ISNULL(P.HotDealStartDate,@Today)<= @Today AND ISNULL(P.HotDealEndDate,@Today) >= @Today)
								AND P.HotDealName LIKE CASE WHEN @Search IS NULL THEN '%' ELSE '%'+@Search+'%' END
								AND (((@CategorySet = 1) AND P.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID))
										 OR
									  (@CategorySet = 0 AND 1 = 1)
										 OR
										(@CategorySet = 1 AND P.CategoryID IS NULL))
								AND PL.City IS NULL
								AND (@Category = 0 OR @Category = '-1')
								--AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)					
								) Deals  
								ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
				END
                  

        --To enable/disable "By Category" button.       
			IF EXISTS( SELECT 1 FROM Category C
						INNER JOIN ProductHotDeal P on p.CategoryID=C.CategoryID
						INNER JOIN HcUserCategory UC ON UC.CategoryID=C.CategoryID
						WHERE P.HotDealStartDate<= @Today AND P.HotDealEndDate >= @Today
						AND HcUserID=@UserID)
            BEGIN
                  SET @ByCategoryFlag = 0 
            END
            ELSE 
                  SET @ByCategoryFlag = 1
                  
                   
            
                                                                          
                        --To get Hot Deal info
                        SELECT  Row_Num = ROW_NUMBER() OVER( ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
                                                                                                                                    ,apiPartnerName
                                                                                                                                    ,Category
                                                                                                                                  ,HotDealStartdate
                                                                                                                                  ,HotDealName)
                                    , ProductHotDealID
                                    , HotDealName
                                    , MIN(City) City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL
                                    --, Distance
                                    , CategoryID categoryId
                                    , HotDealStartdate
                                    , HotDealEndDate
                                    , HotDealExpirationDate
                        INTO #HD
                        FROM (SELECT DISTINCT HD.ProductHotDealID 
                                          , HD.HotDealName
                                          , ISNULL(C.ParentCategoryName, 'Others') AS Category
                                          , PL.City 
                                          , ISNULL(AP.APIPartnerID,0) APIPartnerID
                                          , AP.APIPartnerName 
                                          , isnull(Price,0) Price
                                          , SalePrice
                                          , HotDealShortDescription 
                                          , HotDealImagePath = CASE WHEN HD.WebsiteSourceFlag = 1 
                                                                                             THEN
                                                                                                      CASE WHEN HD.ManufacturerID IS NOT NULL 
                                                                                                            THEN @ManufConfig +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/' +HotDealImagePath
                                                                                                      ELSE 
                                                                                                             CASE WHEN HD.RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
                                                                                                                   THEN CASE WHEN P.WebsiteSourceFlag = 1 THEN  @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +ProductImagePath ELSE ProductImagePath END
                                                                                                            ELSE @RetailerConfig + CONVERT(VARCHAR(30),HD.RetailID)+'/' +HotDealImagePath
                                                                                                            END  
                                                                                                    END
                                                                                                  ELSE HotDealImagePath
                                                                                            END
                                          , HotDealURL
                                          , C.CategoryID categoryId
                                          , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                                         , HotDealStartdate
                                         , HotDealEndDate
                                         , HotDealExpirationDate
                                FROM ProductHotDeal HD
                                INNER JOIN Category C ON C.CategoryID = HD.CategoryID 
                                INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID
                                LEFT JOIN HcLocationAssociation HL ON HL.HCHubCitiID=@HubCitiID AND PL.City=HL.City AND PL.State=HL.State  
								LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID = HL.HcHubCitiID AND RLC.HcHubCitiID = @HubCitiID AND Associated =1 --AND RLC.RetailLocationID =PL.RetailLocationID 
                                INNER JOIN Geoposition G ON G.Postalcode=HL.Postalcode 								
                                INNER JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID  
								LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = HD.ProductHotDealID
								LEFT JOIN Product P ON P.ProductID = HDP.ProductID                            
        --                        LEFT JOIN RetailerBusinessCategory RB ON RB.RetailerID =HD.RetailID 
								--LEFT JOIN #UserLocBusCat US ON (US.BusinessCategoryID =RB.BusinessCategoryID )                            
								WHERE  HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END)
								--AND (@LocCategory =0 OR (US.BusinessCategoryID IS NOT NULL AND US.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID =HD.RetailID))
                                          AND (
                                                      ((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
                                                      OR 
                                                      ((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
                                                )
                                          AND (((@CategorySet = 1) AND HD.CategoryID IN(SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID))
                                                      OR
                                                ((@CategorySet = 0) AND 1 = 1))
                                      -- AND (
                                                --(LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP))
                                                
                                          --  )
                                       AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM HcProductHotDealInterest WHERE HcUserID = @UserID AND Interested = 0)
                                       AND GETDATE() BETWEEN ISNULL(HD.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(HD.HotDealEndDate, HD.HotDealExpirationDate) , GETDATE() + 1)
                                       --AND ISNULL(HD.HotDealStartDate,@Today)<= @Today AND ISNULL(HD.HotDealEndDate,@Today) >= @Today 
                                ) HotDeal
																						
                        WHERE Distance <= @Radius         --To Filter Koala Hot Deals.
                                    --AND APIPartnerID != @KoalaPartnerID
                        GROUP BY ProductHotDealID
                                    , HotDealName 
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL
                                    --, Distance
                                    , CategoryID
                                    , HotDealStartdate
									, HotDealEndDate  
									, HotDealExpirationDate                
                        ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
                        print 'b'
                        IF ISNULL(@Category,0) <> '0' AND ISNULL(@Category,0) <> '-1'
                        BEGIN
                              print 'Lat Long, Category'
                              INSERT INTO #HotDeal (ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            , HotDealShortDescription  
                                                            , HotDealImagePath  
                                                            , HotDealURL      
                                                            --, Distance  
                                                            , CategoryID
                                                            , HotDealStartdate 
                                                            , HotDealEndDate 
                                                            , HotDealExpirationDate
                                                            , NewFlag
                                                            , ExternalFlag)
                             SELECT 
                                           ProductHotDealID
                                          , HotDealName
                                          , City
                                          , Category
                                          , APIPartnerID
                                          , APIPartnerName
                                          , Price
                                          , SalePrice
                                          , HotDealShortDescription
                                          , HotDealImagePath
                                          , HotDealURL                                    
                                          --, Distance 
                                          , CategoryID categoryId
                                          , HotDealStartdate
                                          , HotDealEndDate
                                          , HotDealExpirationDate
                                          , NewFlag = CASE WHEN (DATEDIFF(DD, HotDealStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
                                                              , ExternalFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
                              FROM (
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #HD                                              
                                          UNION                                                 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCity N
                                          INNER JOIN [Version8].fn_SplitParam(@Category, ',')C ON C.Param = N.CategoryID) HD
                              
                        END 
                                                        
                        IF ISNULL(@Category,0) = '0'
                        BEGIN
                              print 'Lat Long, No Category'
                              --To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
                              INSERT INTO #HotDeal (ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            , HotDealShortDescription  
                                                            , HotDealImagePath  
                                                            , HotDealURL      
                                                            --, Distance  
                                                            , CategoryID
                                                            , HotDealStartdate
                                                            , HotDealEndDate
                                                            , HotDealExpirationDate
                                                            , NewFlag
                                                            , ExternalFlag)
                              SELECT ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL                                    
                                    --, Distance 
                                    , CategoryID categoryId
                                    , HotDealStartdate
                                    , HotDealEndDate
                                    , HotDealExpirationDate
                                    , NewFlag = CASE WHEN (DATEDIFF(DD, HotDealStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
                                                      , ExternalFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
                              FROM 
                                    (SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                , CategoryID categoryId
                                                --, Distance
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #HD
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #Others
                                          UNION
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCity
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCityUnCategorized
                                    ) HotDeals 
                             
                        END                           
                        IF ISNULL(@Category,0) = '-1'
                        BEGIN
                              print 'Uncategorised only'
                              --To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
                              INSERT INTO #HotDeal (ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            , HotDealShortDescription  
                                                            , HotDealImagePath  
                                                            , HotDealURL      
                                                            --, Distance  
                                                            , CategoryID
                                                            , HotDealStartdate
                                                            , HotDealEndDate
                                                            , HotDealExpirationDate
                                                            , NewFlag
                                                            , ExternalFlag)
                              SELECT ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL                                    
                                    --, Distance 
                                    , CategoryID categoryId
                                    , HotDealStartdate
                                    , HotDealEndDate
                                    , HotDealExpirationDate
                                    , NewFlag = CASE WHEN (DATEDIFF(DD, HotDealStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
                                                      , ExternalFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
                              FROM  
                                    (SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #Others                                         
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCityUnCategorized
                                    ) HotDeals 
                        END          
                                                  
                                 SELECT DISTINCT H.ProductHotDealID
                                                      ,Used = COUNT(HcUserHotDealGalleryID)
                                 INTO #TotalCounts
                                 FROM #HotDeal H
                                 LEFT JOIN HcUserHotDealGallery UG ON H.ProductHotDealID = UG.HotDealID
                                 GROUP BY H.ProductHotDealID
                                  
                     
									 
					   SELECT rowNumber 
							, hotDealId
							, hotDealName
							, City
							, categoryName
							, apiPartnerId
							, apiPartnerName
							, hDPrice
							, hDSalePrice
							, hDshortDescription
							, hotDealImagePath 
							, hdURL
							--, Distance      distance
							, categoryId
							, hDStartDate
							, hDEndDate
							, NewFlag
							, extFlag
							, Used
                        INTO #Hdeals
                        FROM(                        
                                       SELECT  rowNumber = ROW_NUMBER() OVER(ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASc
                                                                                                                                    ,apiPartnerName
                                                                                                                                    ,H.Category
                                                                                                                                    ,NewFlag DESC
                                                                                                                                    ,H.HotDealName)
                                                , H.ProductHotDealID hotDealId
                                                , H.HotDealName hotDealName
                                                , City
                                                , H.Category categoryName
                                                , H.APIPartnerID apiPartnerId
                                                , APIPartnerName apiPartnerName
                                                , H.Price hDPrice
                                                , H.SalePrice hDSalePrice
                                                , H.HotDealShortDescription hDshortDescription
                                                , H.HotDealImagePath hotDealImagePath 
                                                , H.HotDealURL  hdURL
                                                --, Distance      distance
                                                , H.CategoryID categoryId
                                                , H.HotDealStartdate hDStartDate
                                                , H.HotDealEndDate hDEndDate
                                                , NewFlag
                                                , ExternalFlag extFlag
                                                --,COUNT(UserHotDealGalleryID) Used
                                                ,T.Used
                                          FROM #HotDeal H
                                          INNER JOIN #TotalCounts T ON H.ProductHotDealID = T.ProductHotDealID
                                          LEFT JOIN ProductHotDeal P ON H.ProductHotDealID = P.ProductHotDealID 
                                          LEFT JOIN HcUserHotDealGallery UH ON H.ProductHotDealID = UH.HotDealID AND UH.HcUserID = @UserID
										  --LEFT JOIN RetailerBusinessCategory RB ON RB.RetailerID =P.RetailID 
										  --LEFT JOIN #UserLocBusCat US ON (US.BusinessCategoryID =RB.BusinessCategoryID )
                                          WHERE ISNULL(ISNULL(H.HotDealEndDate, H.HotDealExpirationDate), GETDATE() + 1) >= GETDATE()
                                          AND ISNULL(UH.Used, 0) = 0 
                                          AND ((NOT EXISTS(SELECT 1 FROM HcUserHotDealGallery WHERE UH.HotDealID = P.ProductHotDealID AND UH.HcUserID = @UserID AND Used = 1) AND ISNULL(P.NoOfHotDealsToIssue, T.Used+1) > T.Used)
                                                      OR (EXISTS(SELECT 1 FROM HcUserHotDealGallery WHERE UH.HotDealID = P.ProductHotDealID AND UH.HcUserID = @UserID AND Used = 0)))
										  --AND (@LocCategory =0 OR (US.BusinessCategoryID IS NOT NULL AND US.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID =P.RetailID))
									      GROUP BY  H.ProductHotDealID 
                                                            , H.HotDealName 
                                                            , City
                                                            , H.Category 
                                                            , H.APIPartnerID 
                                                            , APIPartnerName 
                                                            , H.Price 
                                                            , H.SalePrice 
                                                            , H.HotDealShortDescription 
                                                            , H.HotDealImagePath 
                                                            , H.HotDealURL  
                                                            , H.CategoryID 
                                                            , H.HotDealStartdate
                                                            , H.HotDealEndDate
                                                            , NewFlag
                                                            , ExternalFlag
                                                            , P.NoOfHotDealsToIssue
                                                            , T.Used
                                          --HAVING 1= (CASE ExternalFlag WHEN 0 THEN 1 ELSE 2 END)
                                          )HDeals
                                    
                                    --To capture max row number.
                                    SELECT @MaxCnt = MAX(rowNumber) FROM #HDeals
                                    --this flag is a indicator to enable "More" button in the UI. 
                        --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
                                    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
                       
                         SELECT rowNumber 
                                                            , hotDealId
                                                            , hotDealName
                                                            , City
                                                            , categoryName
                                                            , apiPartnerId
                                                            , apiPartnerName
                                                            , hDPrice
                                                            , hDSalePrice
                                                            , hDshortDescription
                                                            , hotDealImagePath 
                                                            , hdURL
                                                            --, Distance      distance
                                                            , categoryId
                                                            , hDStartDate
                                                            , hDEndDate
                                                            , NewFlag
                                                            , extFlag
                                                            , Used
                                    INTO #Hdeals2  
                                    FROM #Hdeals  
                                    WHERE rowNumber BETWEEN (@LowerLimit + 1) AND @UpperLimit
                                  ORDER BY rowNumber 

								
                                                                              
                        --User Tracking Section.                        
                        --Check if the search is happening on the Hotdeal name.
                        IF @Search IS NOT NULL
                        BEGIN
                        
                              INSERT INTO HubCitiReportingDatabase..HotDealSearch(MainMenuID
                                                                                 ,SearchKey
                                                                                 ,DateCreated)
                                                      
                              VALUES(@MainMenuID 
                                      ,@Search 
                                      ,GETDATE())     
                              SELECT @SearchID = SCOPE_IDENTITY()                                                                            
                                                                                                   
                        END
                        --Capture the impression of the hot deals.
                        CREATE Table #Temp3(HotDealListID Int
                                           ,ProductHotDealID Int)
                        
                        INSERT INTO HubCitiReportingDatabase..HotDealList(MainMenuID
                                                                         ,HotDealSearchID                                                                        
                                                                         ,ProductHotDealID                                                                        
                                                                         ,DateCreated)
                        
                        OUTPUT inserted.HotDealListID,inserted.ProductHotDealID INTO #Temp3(HotDealListID,ProductHotDealID)
                        
                        SELECT @MainMenuID 
                              ,@SearchID                               
                              ,hotDealId 
                              ,GETDATE()                          
                        FROM #Hdeals2
                        
                      
                        --Display the list along with the primary key of the tracking table.
                        SELECT DISTINCT rowNumber
                             , HotDealListID
                              , hotDealId
                              , hotDealName
                              , H.City
                              , categoryName
                              , apiPartnerId
                              , apiPartnerName
                              , hDPrice
                              , hDSalePrice
                              , hDshortDescription
                              , hotDealImagePath     
                              , hdURL
                              --, Distance      distance
                              , categoryId
                              , hDStartDate
                              , hDEndDate
                              , NewFlag
                              , extFlag
							  , DealType =CASE WHEN PL.ProductHotDealID IS NULL THEN 'Nationwide' ELSE 'Local' END
                        INTO #DealTypeSort
						FROM #Hdeals2 H
                        INNER JOIN #Temp3 T ON H.hotDealId = T.ProductHotDealID 
						LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID =H.hotDealId  
                        ORDER BY rowNumber 

						SELECT * FROM #DealTypeSort ORDER BY DealType
                        
                        --Display the hot deal categories that fall in the current search criteria.
                        --SELECT DISTINCT Category,                 
                        --                              ISNULL(STUFF(REPLACE(REPLACE((
                        --                              SELECT DISTINCT  ',' + CAST(CategoryID AS VARCHAR(10)) CategoryID
                        --                              FROM #HotDeal H
                        --                              WHERE H.Category = H1.Category
                        --                              FOR XML PATH('')), '<CategoryID>', ''), '</CategoryID>', ''), 1, 1, ''), '-1')catID                       
                        --FROM #HotDeal H1

							   
						--To get list of BottomButtons for this Module
						DECLARE @ModuleName Varchar(50) 
						SET @ModuleName = 'Deals'					
               
						SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
							 , BB.HcBottomButtonID AS bottomBtnID
							 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
							 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
							 , BottomButtonLinkTypeID AS btnLinkTypeID
							 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
							 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
														  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
														  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
																																							   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																							   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																							   WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
													  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
															WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																											INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																											WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
											   ELSE BottomButtonLinkTypeName END)					
						FROM HcMenu HM
						INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
						INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
						INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
						INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
						LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
						WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HubCitiID
						ORDER by HcFunctionalityBottomButtonID
            
            
            
      END TRY
      
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_HcHotDealSearchPagination.'        
                  --- Execute retrieval of Error info.
                  EXEC [HubCitiApp2_3_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
            END;
            
      END CATCH;
END;















GO
