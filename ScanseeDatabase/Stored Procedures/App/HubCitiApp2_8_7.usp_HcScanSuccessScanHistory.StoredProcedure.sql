USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcScanSuccessScanHistory]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcScanSuccessScanHistory  
Purpose     : To set LookUpMatch flag in ScanHistory table and User Product Hit   
Example     : EXEC usp_HcScanSuccessScanHistory NULL, 35, 1, NULL, NULL, '0.00', '0.00', NULL, '6/2/2011', NULL, '38000391200', 1, 1   
  
History  
Version  Date             Author             Change Description  
---------------------------------------------------------------   
1.0      30thOct2013      SPAN               Initial version   
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcScanSuccessScanHistory]  
(  
 --ScanHistory Variables  
 --@RetailLocationID int    
  @UserID int    
 , @ProductID int
 , @RetailLocationID int    
 , @RebateID int    
 , @DeviceID varchar(60)    
 , @ScanLongitude float    
 , @ScanLatitude float    
 , @FieldAgentRequest bit    
 , @Date datetime   
 , @ScanCode varchar(20)    
 , @ScanTypeID tinyint    
 , @LookupMatch bit   
   
 --, @ScanSearch char(6)  
   
 --Output Variable   
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY 
  BEGIN TRANSACTION  
      
    --To capture scan details in ScanHistory Table.  
     INSERT INTO [HcScanHistory]  
        ([RetailLocationID]  
        ,[HcUserID]  
        ,[ProductID]  
        ,[RebateID]  
        ,[DeviceID]  
        ,[ScanLongitude]  
        ,[ScanLatitude]  
        ,[FieldAgentRequest]  
        ,[ScanDate]  
        ,[ScanCode]  
        ,[ScanTypeID]  
        ,[LookupMatch])  
     VALUES  
        (@RetailLocationID   
        ,@UserID   
        ,@ProductID   
        ,@RebateID  
        ,@DeviceID   
        ,@ScanLongitude   
        ,@ScanLatitude   
        ,@FieldAgentRequest   
        ,ISNULL(@Date,Getdate())   
        ,@ScanCode   
        ,@ScanTypeID   
        ,@LookupMatch)           
   
   --Confirmation of Success.  
   SELECT @Status = 0  
  COMMIT TRANSACTION  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcScanSuccessScanHistory.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;














































GO
