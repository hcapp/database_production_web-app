USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserTrackingCouponShare]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_HcUserTrackingCouponShare 
Purpose					: To track coupon share details.  
Example					: usp_HcUserTrackingCouponShare
  
History  
Version    Date				Author			Change         Description  
----------------------------------------------------------------------   
1.0        7th Nov 2013		Mohith H R		Initial Version  
---------------------------------------------------------------------- 
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserTrackingCouponShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @CouponID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         
		 INSERT INTO HubCitiReportingDatabase..ShareCoupon(MainMenuID
														  ,ShareTypeID
														  ,TargetAddress
														  ,CouponID
														  ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@CouponID 
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0           							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingCouponShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1 
  END;            
 END CATCH;  
END;










































GO
