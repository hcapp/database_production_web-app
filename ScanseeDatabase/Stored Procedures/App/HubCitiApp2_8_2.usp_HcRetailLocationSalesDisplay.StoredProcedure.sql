USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailLocationSalesDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_HcRetailLocationSalesDisplay
Purpose					: To display retail location sales.
Example					: usp_HcRetailLocationSalesDisplay  

History  
Version		Date		Author		Change Description  
---------------------------------------------------------------   
1.0			8/9/2011	SPAN		Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailLocationSalesDisplay]  
(  
   @UserID int 
 , @RetailerID int  
 , @RetailLocationID int    
 , @HubCitiID int  
 , @LowerLimit int
 , @ScreenName varchar(50)
  
 --User Tracking Inputs
 , @RetailerListID int
 , @MainMenuID int  
 
 --OutPut Variable 
 , @MaxCnt int output 
 , @NxtPageFlag bit output 
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
	BEGIN TRANSACTION
	
		 --To get Server Configuration
		 DECLARE @ManufConfig varchar(50) 
		 DECLARE @RetailerConfig varchar(50) 
		   
		 SELECT @ManufConfig=(SELECT ScreenContent  
							 FROM AppConfiguration   
							 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
			   ,@RetailerConfig=(SELECT ScreenContent  
								 FROM AppConfiguration   
								 WHERE ConfigurationType='Web Retailer Media Server Configuration')
		 FROM AppConfiguration  
		
		--To get the row count for pagination.
		 DECLARE @UpperLimit int 
		 SELECT @UpperLimit = @LowerLimit + ScreenContent 
		 FROM AppConfiguration 
		 WHERE ScreenName = @ScreenName 
		 AND ConfigurationType = 'Pagination'
		 AND Active = 1	
		
		 SELECT DISTINCT P.ProductID
					, Row_Num=IDENTITY(int,1,1)
					, P.ProductName   
					, P.ManufacturerID 
					, ManufName manufacturersName     
					, P.ModelNumber   
					, P.ProductLongDescription productDescription  
					, P.ProductExpirationDate productExpDate
					, ImagePath = CASE WHEN RD.ThumbNailProductImagePath IS NULL THEN
                                                            (CASE WHEN P.ProductImagePath IS NOT NULL
                                                            THEN (CASE WHEN P.WebsiteSourceFlag = 1  THEN (@ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + P.ProductImagePath) ELSE P.ProductImagePath END)
                                                            ELSE 
                                                                (CASE WHEN RL.RetailLocationImagePath IS NOT NULL AND RR.RetailerImagePath IS NULL                  
                                                                THEN (@RetailerConfig + CAST (RR.Retailid AS varchar(50)) + '/' + CONVERT(VARCHAR(30),RL.RetailLocationImagePath))
                                                                ELSE (CASE WHEN RR.RetailerImagePath IS NOT NULL AND RR.WebsiteSourceFlag =1 THEN (@RetailerConfig + CAST (RR.Retailid AS VARCHAR(50)) + '/' + CONVERT(VARCHAR(50),RR.RetailerImagePath)) ELSE RR.RetailerImagePath END)
                                                                END)  
                                                            END)
                                                    ELSE (@ManufConfig +CAST(RD.ThumbNailProductImagePath AS VARCHAR(30))+'/' + RD.ThumbNailProductImagePath)
                                            END
                        
					, P.SuggestedRetailPrice   
					, P.Weight productWeight 
					, P.WeightUnits  
					, RD.SalePrice   
					, RD.Price regularPrice 
					--, DiscountAmount		
					, RetailLocationDealID
					, @RetailerListID RetailerListID  					
		INTO #Product   
		FROM Product P 
		INNER JOIN RetailLocationDeal RD ON P.ProductID = RD.ProductID AND RD.RetailLocationID = @RetailLocationID
		INNER JOIN RetailLocation RL ON RD.RetailLocationID = RL.RetailLocationID AND RL.RetailLocationID = @RetailLocationID AND RetailID = @RetailerID AND RL.Active=1
		LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		LEFT JOIN HcLocationAssociation HA ON RL.PostalCode = HA.PostalCode AND RLC.HcHubCitiID = @HubCitiID
		LEFT JOIN Manufacturer M ON P.ManufacturerID = M.ManufacturerID
		LEFT JOIN Retailer RR ON RR.RetailID = RL.RetailID AND RR.RetailerActive=1 
		WHERE GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
		

	   --To capture max row number.
	   SELECT @MaxCnt = MAX(Row_Num) FROM #Product
	
	   --This flag is a indicator to enable "More" button in the UI. 
	   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
	   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
	   
	   
	   SELECT DISTINCT ProductID
				, Row_Num
				, ProductName   
				, ManufacturerID 
				, manufacturersName     
				, ModelNumber   
				, productDescription  
				, productExpDate  
				, imagePath
				, SuggestedRetailPrice   
				, productWeight 
				, WeightUnits  
				, SalePrice   
				, regularPrice 
				--, DiscountAmount		
				, RetailLocationDealID
				, @RetailerListID RetailerListID  
		INTO #ProductList			
		FROM #Product	
		WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
	    ORDER BY Row_Num		
		
		--User Tracking Section.

		--Capture the impressions of the Product on Sale.
		CREATE TABLE #Temp(SaleListID INT
		, RetailerListID INT
		, RetailLoactionDealID INT)

		INSERT INTO HubCitiReportingDatabase..SaleList(RetailerListID
								 , RetailLocationDealID
								 , MainMenuID 
								 , DateCreated
								 , SalePrice)
								 
		OUTPUT inserted.SaleListID, inserted.RetailerListID, inserted.RetailLocationDealID INTO #Temp(SaleListID, RetailerListID, RetailLoactionDealID)												 

		SELECT @RetailerListID
		, RetailLocationDealID
		, @MainMenuID 
		, GETDATE()
		, SalePrice 
		FROM #ProductList
		
		SELECT Row_Num 
		, T.SaleListID
		, T.RetailerListID retListID
		, P.productId
		, P.productName
		, P.manufacturersName
		, P.modelNumber
		, P.productDescription
		, P.productExpDate
		, P.imagePath
		, P.suggestedRetailPrice
		, P.productWeight
		, P.weightUnits
		, P.salePrice
		, P.regularPrice
		, P.RetailLocationDealID
		--, P.discount
		FROM #ProductList P
		INNER JOIN #Temp T ON P.RetailLocationDealID = T.RetailLoactionDealID
		
	  --Confirmation of Success.
	  SELECT @Status = 0	
	COMMIT TRANSACTION
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcRetailLocationSalesDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.
   SELECT @Status = 1
  END;  
     
 END CATCH;  
END;















































GO
