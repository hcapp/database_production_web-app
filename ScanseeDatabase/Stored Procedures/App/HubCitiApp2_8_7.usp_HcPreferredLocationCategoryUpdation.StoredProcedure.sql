USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcPreferredLocationCategoryUpdation]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcPreferredLocationCategoryUpdation
Purpose					: To update user preferred location Categories.
Example					: usp_HcPreferredLocationCategoryUpdation

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			2/26/2015       SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcPreferredLocationCategoryUpdation]
(
	--Input variable
	  @HcUserID int
	, @HubCitiID int
	, @BusinessCategoryIDs Varchar(1000) --Comma separated Business category ids
	, @SubCategoryIDs Varchar(MAX)  --Comma separated sub category ids
		
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			

			SELECT @BusinessCategoryIDs = IIF(@BusinessCategoryIDs = '', NULL, @BusinessCategoryIDs)
			SELECT @SubCategoryIDs = IIF(@SubCategoryIDs = '', NULL, @SubCategoryIDs)

			--To delete existing records from HcUserPreferredCategory table for given userid  
			DELETE FROM HcUserPreferredCategory 
			WHERE HcUserID = @HcUserID --AND HcHubCitiID = @HubCitiID

			--To Replace Null values with 0 in  input values
			SELECT @BusinessCategoryIDs = REPLACE(@BusinessCategoryIDs, 'NULL', '0')  
			SELECT @SubCategoryIDs = REPLACE(@SubCategoryIDs, 'NULL', '0')

			--Capture Category and SubCategory ID's
			SELECT RowNum = IDENTITY(INT, 1, 1)
					,CategoryID = Param
			INTO #CategoryID
			FROM dbo.fn_SplitParamMultiDelimiter(@BusinessCategoryIDs, ',')                     

			SELECT RowNum = IDENTITY(INT, 1, 1)
					, SubCategoryID = Param
			INTO #SubCategoryID
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCategoryIDs, '|')  

			SELECT CategoryID 
					,SubCategoryID      
			INTO #Pref_CatSubCat
			FROM #CategoryID C
			INNER JOIN #SubCategoryID S ON C.RowNum = S.RowNum 	


				IF CURSOR_STATUS('global','CategoryList')>=-1
					BEGIN
					DEALLOCATE CategoryList
					END
                                                
					DECLARE @CategoryID1 varchar(1000)
					DECLARE @SubCategoryID1 varchar(MAX)
					DECLARE CategoryList CURSOR STATIC FOR
					SELECT CategoryID
						  ,SubCategoryID          
					FROM #Pref_CatSubCat 
                           
					OPEN CategoryList
                           
					FETCH NEXT FROM CategoryList INTO @CategoryID1, @SubCategoryID1
                           
					CREATE TABLE #TempBiz(BusinnessCategoryID INT)


					WHILE @@FETCH_STATUS = 0
					BEGIN   
                            
						INSERT INTO #TempBiz(BusinnessCategoryID) 			                                
						OUTPUT inserted.BusinnessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT F.[Param]						     
						FROM dbo.fn_SplitParam(@CategoryID1, '|') F

						--Inserting categoryids into HcUserPreferredCategory table
						INSERT INTO HcUserPreferredCategory(HcUserID
															, HcHubCitiID
															, BusinessCategoryID
															, HcBusinessSubCategoryID
															, DateModified)
									OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinnessCategoryID)
														SELECT @HcUserID										
															 , @HubCitiID
															 , @CategoryID1
															 , IIF([Param] = 0,NULL,[Param])
															 , GETDATE()
														FROM dbo.fn_SplitParam(@SubCategoryID1, ',')
					
									DECLARE @Cnt INT 
									SELECT @Cnt = 0
									SELECT @Cnt = COUNT(Param)
									FROM dbo.fn_SplitParam(@SubCategoryID1, ',')
                                   
                               
						TRUNCATE TABLE #TempBiz
		
						FETCH NEXT FROM CategoryList INTO @CategoryID1, @SubCategoryID1
                                  
					END                        

				CLOSE CategoryList
				DEALLOCATE CategoryList

				--Confirmation of Success.
				SELECT @Status = 0

	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcPreferredLocationCategoryUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




























GO
