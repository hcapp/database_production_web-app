USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFAQCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcFAQCategoryDisplay
Purpose					: Display list of FAQ Category.
Example					: usp_HcFAQCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14/02/2014	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFAQCategoryDisplay]
(
   
    --Input variable. 	  
	  @HcHubCitiID Int	
    , @SearchKey varchar(1000)
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @UserID int 
	  
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION

	 		--set transaction   isolation level read uncommitted

	            DECLARE @UpperLimit int
				DECLARE @ModuleName varchar(20) = 'FAQ'
				
				--To get the row count for pagination.	 
				SELECT @UpperLimit = ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
	               
				--Display list FAQ Categories				
				SELECT  ROW_NUMBER() OVER(ORDER BY ISNULL(C.SortOrder,10000),C.HcFAQCategoryID, C.CategoryName ASC) rowNum
				               ,C.HcFAQCategoryID categoryId
				               ,CategoryName categoryName 							   
				--INTO #FAQCategories
				FROM HcFAQCategory C
				INNER JOIN HcFAQ F ON F.HcFAQCategoryID =C.HcFAQCategoryID 
				WHERE C.HcHubCitiID=@HcHubcitiID --AND (F.HcFAQCategoryID IS NOT NULL OR C.CategoryName='General')
				AND ((@SearchKey IS NULL) OR (@SearchKey IS NOT NULL AND (CategoryName LIKE '%'+@SearchKey+'%' OR Question LIKE '%'+@SearchKey+'%' OR Answer LIKE '%'+@SearchKey+'%')))	
				GROUP BY C.HcFAQCategoryID,CategoryName,F.HcFAQCategoryID,C.SortOrder
				ORDER BY rowNum

				--To capture max row number.  
				SELECT @MaxCnt =@@ROWCOUNT

				--IF @LowerLimit IS NULL
				--BEGIN
				--	SET @LowerLimit=0
				--	--SET @UpperLimit=@MaxCnt					
				--END
				

				----this flag is a indicator to enable "More" button in the UI.   
				----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@UpperLimit+@LowerLimit)) > 0 THEN 1 ELSE 0 END 
				
				--SELECT Rownum rowNum
				--      ,categoryid categoryId
				--      ,CategoryName categoryName 					  
				--FROM #FAQCategories
				--ORDER BY rowNum
				--OFFSET @LowerLimit ROWS
				--FETCH NEXT 50 ROWS ONLY	
				--FETCH NEXT @UpperLimit ROWS ONLY
				
				--To display bottom buttons								

				EXEC [HubCitiApp2_3_3].[usp_HcFunctionalityBottomButtonDisplay] @HcHubcitiID, @ModuleName, @UserID, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT
						
				
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcFAQCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








































GO
