USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcShareApp]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcShareApp
Purpose					: usp_HcShareApp.
Example					: usp_HcShareApp

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd Dec 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcShareApp]
(
	  @UserID int
	, @HcHubCitiID int
	
	--Output Variable 
	, @UserName varchar(100) output	
	, @ItunesImage varchar(1000) output
	, @GooglePlayImage varchar(1000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
						
		BEGIN TRANSACTION	
		
			SELECT @UserName = Email
			FROM HcUser
			WHERE HcUserID = @UserID		
			
			SELECT @ItunesImage = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'General Image'
			AND ScreenName = 'Itunes Image'
			
			SELECT @GooglePlayImage = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'General Image'
			AND ScreenName = 'Google Play Image'
		
			SELECT HcHubCitiID hubCitiId
			      ,ItunesURL iTunesURL
			      ,GooglePlayURL
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID
			
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcShareApp.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
