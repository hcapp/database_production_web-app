USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiFirstUseImagesList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiFirstUseImagesList
Purpose					: To retrieve First use images for the HubCiti App.
Example					: usp_HcHubCitiFirstUseImagesList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/8/2016	    Shilpashree		       1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiFirstUseImagesList]
(
   --Input variable
      @HcHubCitiKey Varchar(250)

	--Output Variable 
	, @LogoPath Varchar(500) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @ImagePath Varchar(500)
			SELECT @ImagePath = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCitiFirstUseImagePath' 

			DECLARE @HubCitiConfig Varchar(500)
			SELECT @HubCitiConfig = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			
			DECLARE @HubCitiID int
			SELECT @HubCitiID = HcHubCitiID
				 ,@LogoPath = @HubCitiConfig + CAST(@HubCitiID AS VARCHAR(10))+ '/'+ LogoImage
			FROM HcHubCiti
			WHERE HcHubCitiKey = @HcHubCitiKey

			SELECT @ImagePath + FirstUseImage link
					,Type
					,AssociatePageURL isAnythingPageOrWebLinkAssociated 
					,CASE WHEN (AssociatePageURL = 1 AND AssociatePageURLTypeID = 2) THEN 'AnythingPage' WHEN (AssociatePageURL = 1 AND AssociatePageURLTypeID = 1) THEN 'WebLink' END isAnythingPageOrWebLink
					,AssociatePageURLValue isAnythingPageOrWebLinkValue
			FROM FirstUseLoginImage
			WHERE HcHubCitiID = @HubCitiID 

			--Confirmation of Success.
			SELECT @Status = 0
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		   
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiFirstUseImagesList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














GO
