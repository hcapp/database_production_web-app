USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_GetScreenContent]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GetScreenContent]
Purpose					: To get Image of Product Summary page.
Example					: [usp_GetScreenContent]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Oct 2011	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_GetScreenContent]
(
	@ConfigurationType varchar(50)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @Config varchar(50)
			--To get Email Template URL.
			IF @ConfigurationType = 'ShareURL'
			BEGIN
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Configuration of server'
			END
			--For all the other URL framing.
			ELSE
			BEGIN
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
			END

			SELECT  ConfigurationType
					,ScreenContent	=CASE WHEN ScreenName Like '%image%' THEN  @Config+ScreenContent 
										  WHEN ConfigurationType='ShareURL' THEN @Config+ScreenContent  Else ScreenContent END														
					,ScreenName 
					,Active 
					,DateCreated
					,DateModified
			FROM  AppConfiguration
			WHERE ConfigurationType=@ConfigurationType
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GetScreenContent].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;














































GO
