USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcFindNearByRetailer]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcFindNearByRetailer  
Purpose     : To get near by retailers list.  
Example     : EXEC usp_HcFindNearByRetailer 14, '12.94715', '77.57888', 1, 2    
  
History  
Version  Date           Author                Change Description  
---------------------------------------------------------------   
1.0      30thOct2013    SPAN Infotech India   Initial Version    
---------------------------------------------------------------  
*/  
  
 CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcFindNearByRetailer]  
(  
   @UserID INT  
 , @Latitude decimal(18,6)  
 , @Longitude decimal(18,6)  
 , @PostalCode varchar(10)  
 , @ProductId int  
 , @Radius int 
 , @HcHubCitiID Int
 
 --User Tracking
 ,@MainMenuID int
    
 --Output Variable   
  , @UserOutOfRange bit output  
  , @DefaultPostalCode varchar(50) output
  , @Status bit output  
  , @ErrorNumber int output   
  , @Result int OUTPUT   
  , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	  DECLARE @UserID1 INT = @UserID,
	  @HcHubCitiID1 INT = @HcHubCitiID,
	  @Latitude1 DECIMAL(18,6) = @Latitude,
	  @Longitude1 DECIMAL(18,6) = @Longitude
		
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)  
	  DECLARE @DistanceFromUser FLOAT  
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 

	   DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='App Media Server Configuration'
		
	    DECLARE @RetailConfig varchar(50)
		SELECT @RetailConfig=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Retailer Media Server Configuration'

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer
		WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive = 1

		DECLARE @UserLatitude float
		DECLARE @UserLongitude float 

		SELECT @UserLatitude = @Latitude1
				, @UserLongitude = @Longitude1

		IF (@UserLatitude IS NULL) 
		BEGIN
			SELECT @UserLatitude = Latitude
					, @UserLongitude = Longitude
			FROM HcUser A
			INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
			WHERE HcUserID = @UserID1 
		END
                    
        --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude1, @Longitude1, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
   
   --Derive the Latitude and Longitude in the absence of the input.
        IF (@Latitude1 IS NULL AND @Longitude1 IS NULL) OR (@UserOutOfRange=1)
        BEGIN
                --If the postal code is passed then derive the co ordinates.
                IF @PostalCode IS NOT NULL
				BEGIN
                        SELECT @Latitude1 = Latitude
                            , @Longitude1 = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @PostalCode
                END		
				ELSE 
				BEGIN
					SELECT @Latitude1 = G.Latitude
							, @Longitude1 = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID1	
				END												
        END  
		IF @Latitude1 IS NULL AND @Longitude1 IS NULL
		BEGIN
			SET @Result = -1
		END
    
  --To fetch User Preferred Radius.  
  IF @Radius IS NULL  
  BEGIN  
   SELECT @Radius = ISNULL(LocaleRadius, 5)   
   FROM UserPreference   
   WHERE UserID = @UserID1   
  END  
    
  SELECT RetailID   
   , RetailName   
   , RetailLocationID 
   , Address   
   , ContactMobilePhone   
   , RetailURL   
   , Distance   
   , DistanceActual
   , ProductId   
   , ProductName  
   , ProductImagePath   
   , Price   
   , UserID   
   , RetailLocationLatitude   
   , RetailLocationLongitude 
   , bannerAdImagePath
   , ribbonAdImagePath
   , ribbonAdURL
   , advertisementID
   , splashAdID
  INTO #RetailerLists 
  FROM   
  (  
   SELECT DISTINCT  R.RetailID   
    , R.RetailName
    , RL.RetailLocationID   
    , RL.Address1 +','+isnull(RL.Address2,'')+ isnull(RL.Address3,'')+isnull(RL.Address4,'') +RL.City +','+ RL.State+ ',' +RL.PostalCode Address  
    , RC.ContactMobilePhone   
    , R.RetailURL  
    , Distance = (ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude1 / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude1 / 57.2958) * COS((@Longitude1 / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
	, DistanceActual = (ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
    , P.ProductId   
    , P.ProductName  
    , ProductImagePath  = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END  
    , RP.Price  
    , @UserID1  AS UserID  
    , RL.RetailLocationLatitude   
    , RL.RetailLocationLongitude   
	, S.BannerAdImagePath bannerAdImagePath    
	, B.RibbonAdImagePath ribbonAdImagePath    
	, B.RibbonAdURL ribbonAdURL    
	, B.RetailLocationAdvertisementID advertisementID  
	, S.SplashAdID splashAdID 
   FROM Product P  
   INNER JOIN RetailLocationProduct RP ON RP.ProductID = P.ProductID   
   INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID AND RL.Active =1 
   INNER JOIN HcLocationAssociation HL ON HL.Postalcode=RL.Postalcode AND HL.Hchubcitiid=@HcHubCitiID1
   INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID1 AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
   LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID    
   INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND R.RetailerActive = 1
   LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
   LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
																	ELSE SplashAdImagePath
																  END
									   , SplashAdID  = ASP.AdvertisementSplashID
									   , R.RetailLocationID 
								FROM RetailLocation R
									INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID AND R.Active = 1
									INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
									AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)) S
							ON RL.RetailLocationID = S.RetailLocationID
					 LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
																ELSE AB.BannerAdImagePath
															END 
									  , RibbonAdURL = AB.BannerAdURL
									  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
									  , R.RetailLocationID 
								 FROM RetailLocation R
										 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID AND R.Active = 1 
										 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
								 WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)) B
							ON RL.RetailLocationID = B.RetailLocationID
   WHERE P.ProductID = @ProductId  AND D.DuplicateRetailerID IS NULL 
  )Retailer   
  --WHERE Distance <= ISNULL(@Radius, 5)
  ORDER BY Price, DistanceActual  

 
 --User Tracking
 
 CREATE TABLE #Temp(RetailerListID int,RetailLocationID int)
 
 INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
												   ,RetailID
												   ,RetailLocationID
												   ,DateCreated)
  OUTPUT inserted.RetailerListID,inserted.RetailLocationID INTO #Temp(RetailerListID,RetailLocationID)	
  											   
  SELECT @MainMenuID 
         ,RetailID  
         ,RetailLocationID
         ,GETDATE()
  FROM #RetailerLists       
 
 
  SELECT T.RetailerListID retListID
   , RetailID retailerId      
   , RetailName retailerName  
   , R.RetailLocationID retLocId
   , Address   
   , ContactMobilePhone phone  
   , RetailURL RetailerUrl  
   , distance = ISNULL(DistanceActual, Distance)
   , ProductId productId  
   , ProductName  
   , ProductImagePath imagePath  
   , Price productPrice  
   , UserID   
   , RetailLocationLatitude latitude  
   , RetailLocationLongitude longitude 
   , bannerAdImagePath
   , ribbonAdImagePath
   , ribbonAdURL
   , advertisementID
   , splashAdID 
  FROM #RetailerLists R
  INNER JOIN #Temp T ON R.RetailLocationID =T.RetailLocationID  
  ORDER BY distance ASC
 
 END TRY 
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_ShoppingNearByRetailer.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;















































GO
