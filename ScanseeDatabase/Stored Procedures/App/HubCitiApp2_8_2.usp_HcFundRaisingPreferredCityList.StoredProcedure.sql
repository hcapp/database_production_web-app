USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcFundRaisingPreferredCityList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcEventsPreferredCityList]
Purpose					: To display list of Cities which has Events for given HubCitiID(RegionApp).
Example					: [usp_HcEventsPreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  11/26/2014       SPAN              1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcFundRaisingPreferredCityList]
(
   
    --Input variable	  
	  @UserID int	
	, @HcHubCitiID int
	, @HcMenuItemID int
    , @HcBottomButtonID int
	, @CategoryID Varchar(100)  --Comma Separated Category IDs
    , @SearchKey varchar(255)
	, @RetailID int
    , @RetailLocationID int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
					--MenuItem
					SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID
					INTO #RegionCat
					FROM HcHubCiti H                     
					INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN HcMenuItemFundraisingCategoryAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcMenuItemID = @HcMenuItemID
					WHERE H.HcHubCitiID = @HcHubCitiID

					 
					CREATE TABLE #AllEventCategories(HubcitiID INT,FundraisingCategoryID INT,FundraisingCategoryName VARCHAR(100),FundraisingID INT)

					INSERT INTO #AllEventCategories(HubcitiID,FundraisingCategoryID,FundraisingCategoryName,FundraisingID)
					 
					SELECT DISTINCT HR.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID
					FROM HcHubCiti H
					LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
					INNER JOIN HcFundraisingCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN #RegionCat RC ON HE.HcFundraisingCategoryID = RC.HcFundraisingCategoryID
					INNER JOIN HcMenuItemFundraisingCategoryAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcMenuItemID = @HcMenuItemID 
					WHERE HR.HcRegionAppID = @HcHubCitiID 
                    
					UNION ALL

					SELECT HcHubCitiID,HcFundraisingCategoryID,FundraisingCategoryName,HcFundraisingID
					FROM #RegionCat

					--BottomButton
					SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID
					INTO #RegionCat1
					FROM HcHubCiti H                     
					INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN HcFundraisingBottomButtonAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID
					WHERE H.HcHubCitiID = @HcHubCitiID

 
					CREATE TABLE #AllEventCategories1(HubcitiID INT,FundraisingCategoryID INT,FundraisingCategoryName VARCHAR(100),FundraisingID INT)

					INSERT INTO #AllEventCategories1(HubcitiID,FundraisingCategoryID,FundraisingCategoryName,FundraisingID) 
					
					SELECT DISTINCT HR.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID
					FROM HcHubCiti H
					LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
					INNER JOIN HcFundraisingCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID
					INNER JOIN #RegionCat1 RC ON HE.HcFundraisingCategoryID = RC.HcFundraisingCategoryID
					INNER JOIN HcFundraisingBottomButtonAssociation ME ON HE.HcFundraisingCategoryID = ME.HcFundraisingCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID 
					WHERE HR.HcRegionAppID = @HcHubCitiID 
                    
					UNION ALL

					SELECT HcHubCitiID,HcFundraisingCategoryID,FundraisingCategoryName,HcFundraisingID
					FROM #RegionCat1

					CREATE TABLE #RHubcitiList(HchubcitiID Int) 
					INSERT INTO #RHubcitiList( HchubcitiID)
					SELECT HcHubCitiID
					FROM
						(SELECT RA.HcHubCitiID
						FROM HcHubCiti H
						LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
						WHERE H.HcHubCitiID  = @HcHubCitiID
						UNION ALL
						SELECT @HcHubCitiID)A

					DECLARE @HubCitis varchar(100)
					SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
					FROM #RHubcitiList

				    CREATE TABLE #Temp(HcCityID int
								      ,CityName Varchar(100)) 
                     
				--To display List of Events Cities
				IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
				BEGIN

						INSERT INTO #Temp(HcCityID 
										 ,CityName)
							SELECT HcCityID
								  ,CityName
						    FROM(
							SELECT DISTINCT  C.HcCityID
											,C.CityName 
							FROM HcFundraising F 
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID=FCA.HcFundraisingID
							INNER JOIN #AllEventCategories A ON F.HcFundraisingID = A.FundraisingID AND F.RetailID IS NULL
							INNER JOIN HcFundraisingAppsiteAssociation FA ON A.FundraisingID = FA.HcFundraisingID
							INNER JOIN HcAppSite HA ON FA.HcAppSiteID = HA.HcAppsiteID
							INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = A.HubCitiID AND Associated =1
							INNER JOIN RetailLocation RL1 ON HA.RetailLocationID = RL1.RetailLocationID  --OR (RA.RetailLocationID =RL1.RetailLocationID)
							INNER JOIN HcCity C ON (RL1.HcCityID = C.HcCityID)									                         
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 AND RL1.Active = 1
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchKey IS NOT NULL AND FundraisingName LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL AND 1=1))
						   
							UNION ALL

							SELECT DISTINCT  C.HcCityID
											,C.CityName
							FROM HcLocationAssociation HL
							INNER JOIN #AllEventCategories A ON HL.HcHubCitiID IN (SELECT param FROM fn_SplitParam(@HubCitis,','))
							INNER JOIN HcRetailerAssociation R ON A.HubcitiID = R.HcHubCitiID AND Associated=1
							INNER JOIN HcFundraising F ON R.RetailID = F.RetailID and F.RetailID IS NOT NULL
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID = FCA.HcFundraisingID AND FCA.HcHubCitiID IS NULL AND A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
							INNER JOIN HcRetailerFundraisingAssociation L ON F.HcFundraisingID = L.HcFundraisingID AND R.RetailID = L.RetailID
							INNER JOIN RetailLocation RL ON RL.RetailLocationID=L.RetailLocationID 
							INNER JOIN HcCity C ON RL.HcCityID = C.HcCityID
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 AND RL.Active = 1
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchKey IS NOT NULL AND FundraisingName LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL AND 1=1))
						  )E
								
				END
				ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
				BEGIN

							INSERT INTO #Temp(HcCityID 
											 ,CityName)
									SELECT  HcCityID
										   ,CityName
							FROM(
							SELECT DISTINCT  C.HcCityID
											,C.CityName  											   							
							FROM HcFundraising F 
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID=FCA.HcFundraisingID
							INNER JOIN #AllEventCategories1 A ON F.HcFundraisingID = A.FundraisingID AND F.RetailID IS NULL
							INNER JOIN HcFundraisingAppsiteAssociation FA ON A.FundraisingID = FA.HcFundraisingID
							INNER JOIN HcAppSite HA ON FA.HcAppSiteID = HA.HcAppsiteID
							INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = A.HubCitiID AND Associated =1
							INNER JOIN RetailLocation RL1 ON HA.RetailLocationID = RL1.RetailLocationID  --OR (RA.RetailLocationID =RL1.RetailLocationID)
							INNER JOIN HcCity C ON (RL1.HcCityID = C.HcCityID)									                         
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 AND RL1.Active = 1
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchKey IS NOT NULL AND FundraisingName LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL AND 1=1))
						  
							UNION ALL

							SELECT DISTINCT  C.HcCityID
											,C.CityName
							FROM HcLocationAssociation HL
							INNER JOIN #AllEventCategories1 A ON HL.HcHubCitiID in (SELECT param FROM fn_SplitParam(@HubCitis,','))
							INNER JOIN HcRetailerAssociation R ON A.HubcitiID = R.HcHubCitiID AND Associated=1
							INNER JOIN HcFundraising F ON R.RetailID = F.RetailID and F.RetailID is not null
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID = FCA.HcFundraisingID and FCA.HcHubCitiID IS NULL AND A.FundraisingCategoryID = FCA.HcFundraisingCategoryID
							INNER JOIN HcRetailerFundraisingAssociation L ON F.HcFundraisingID = L.HcFundraisingID and R.RetailID = L.RetailID
							INNER JOIN RetailLocation RL ON RL.RetailLocationID=L.RetailLocationID 
							INNER JOIN HcCity C ON RL.HcCityID = C.HcCityID
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 AND RL.Active = 1
							AND (( ISNULL(@CategoryID, '0') <> '0' AND A.FundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchKey IS NOT NULL AND FundraisingName LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL AND 1=1))
							)E

				END

				ELSE IF(@RetailID IS NOT NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NULL)
				BEGIN

							INSERT INTO #Temp(HcCityID 
											 ,CityName)
							SELECT  HcCityID
									,CityName
							FROM(
							SELECT DISTINCT  C.HcCityID
											,C.CityName   
							 FROM HcFundraising F 
							 INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID =FCA.HcFundraisingID 
							 INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = @HcHubCitiID AND Associated =1							 
							 INNER JOIN HcRetailerFundraisingAssociation RF ON RA.RetailLocationID = RF.RetailLocationID AND F.HcFundraisingID = RF.HcFundraisingID
							 INNER JOIN RetailLocation RL1 ON RL1.RetailLocationID =RF.RetailLocationID	
							 INNER JOIN HcCity C ON RL1.HcCityID = C.HcCityID
							 WHERE F.RetailID =@RetailID AND RF.RetailLocationID = @RetailLocationID
							 AND  GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 AND RL1.Active = 1
							 AND (( ISNULL(@CategoryID, '0') <> '0' AND FCA.HcFundraisingCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							 AND ((@SearchKey IS NOT NULL AND FundraisingName LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL AND 1=1))
							 )A
							 
				END

				ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NULL)
				BEGIN
							
							INSERT INTO #Temp(HcCityID 
									,CityName)
							
							
							SELECT DISTINCT C.HcCityID
									,CityName							                	
							FROM HcFundraising F 
							INNER JOIN #RHubcitiList HL ON HL.HchubcitiID=F.HcHubCitiID
							INNER JOIN HcFundraisingAppsiteAssociation FA ON FA.HcFundraisingID =F.HcFundraisingID
							INNER JOIN HcAppSite A ON A.HcAppSiteID=FA.HcAppsiteID
							INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID =A.RetailLocationID AND Associated =1 AND HL.HchubcitiID =RA.HcHubCitiID
							INNER JOIN RetailLocation RL ON RL.RetailLocationID =RA.RetailLocationID AND RL.Active = 1 
							INNER JOIN HcCity C ON C.HcCityID =RL.HcCityID
							LEFT JOIN HcRetailerFundraisingAssociation RF ON RF.HcFundraisingID=F.HcFundraisingID				
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1	AND RF.HcFundraisingID IS NULL														    	   
							
							UNION ALL				
						
							--To display List of Events for Retiler
							SELECT DISTINCT C.HcCityID
										,CityName													
							FROM HcFundraising F 
							INNER JOIN #RHubcitiList HL ON HL.HchubcitiID=F.HcHubCitiID													 
							INNER JOIN HcRetailerFundraisingAssociation RF ON F.HcFundraisingID = RF.HcFundraisingID --RA.RetailLocationID = RF.RetailLocationID
							INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID=RF.RetailLocationID AND Associated =1 AND RA.HcHubCitiID =HL.HchubcitiID
							INNER JOIN RetailLocation RL ON RL.RetailLocationID =RA.RetailLocationID AND RL.Active = 1
							INNER JOIN HcCity C ON C.HcCityID =RL.HcCityID							
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1 
				END


				SELECT DISTINCT  HcCityID AS CityID
								,CityName
				FROM #Temp
				WHERE HcCityID IS NOT NULL

			   --Confirmation of Success.
			   SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcEventsPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
































GO
