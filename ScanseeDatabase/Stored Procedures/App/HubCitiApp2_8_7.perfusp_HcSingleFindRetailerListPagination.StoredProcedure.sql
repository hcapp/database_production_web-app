USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[perfusp_HcSingleFindRetailerListPagination]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  usp_HcFindRetailerListPagination
Purpose                 :  To get the list of near by retailer for Find Module.
Example                 :  

History
Version      Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/17/2012            SPAN       Initial Version
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_7].[perfusp_HcSingleFindRetailerListPagination]
(

      --Input Parameter(s)
      
         @UserID int  
       --, @ApiPartnerName Varchar(50)
       , @CategoryName varchar(100)
       , @Latitude decimal(18,6)
       , @Longitude decimal(18,6)
       , @Radius int
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @HCHubCitiID Int
       , @HcMenuItemId Int
       , @SearchKey Varchar(100)
       , @BusinessSubCategoryID Varchar(2000)
       , @SortColumn varchar(50)
       , @SortOrder varchar(10)
       , @HcCityID Varchar(2000)
       , @FilterID Varchar(1000)
       , @FilterValuesID Varchar(1000)
	   , @LocalSpecials bit
	   , @Interests Varchar(1000)   

       --User tracking Inputs
       , @MainMenuID int
       
      --Output Variable
	   , @NoRecordsMsg nvarchar(max) output 
       , @UserOutOfRange bit output    
       , @DefaultPostalCode varchar(50) output           
	   , @MaxCnt int  output
       , @NxtPageFlag bit output  
       , @Status int output 
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY

      
              SET NOCOUNT ON           
                DECLARE @RowCount int 
                , @Count INT = 1
                , @MaxCount INT 
                , @SQL VARCHAR(1000) 
                , @PostalCode varchar(10)
                , @CategoryID INT  
                , @RetSearch VARCHAR(1000)
                , @Config varchar(50)                     
                , @RetailConfig varchar(50)
                , @UpperLimit int   
                , @Tomorrow DATETIME = GETDATE() + 1
                , @Yesterday DATETIME = GETDATE() - 1
                , @RowsPerPage int
                , @DistanceFromUser FLOAT
                --DECLARE @DefaultPostalCode varchar(50)
                , @UserLatitude decimal(18,6)
                , @UserLongitude decimal(18,6) 
                , @SpecialChars VARCHAR(1000)

                DECLARE @ModuleName Varchar(200)='Find'
                , @RegionAppFlag Bit
                , @GuestLoginFlag BIT=0
				, @BusinessCategoryID Int

				DECLARE @UserID1 INT = @UserID
				,@HCHubCitiID1 INT = @HCHubCitiID
				,@HcMenuItemId1 INT = @HcMenuItemId
				,@MainMenuID1 INT = @MainMenuID
				,@HcCityID1 INT = @HcCityID

				--SearchKey implementation
				DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

				SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
						WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
						WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
						ELSE @SearchKey END)



					SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName

					SELECT Param BusCatIDs
					INTO #BusinessSubCategoryIDs
					FROM fn_SplitParam (@BusinessSubCategoryID,',')

					SELECT Param IntIDs
					INTO #Interests
					FROM fn_SplitParam (@Interests,',')

         --                  CREATE TABLE #FilterRetaillocations(RetailID INT)

         --                         --Filter Implementation 
         --                         IF @FilterID IS NOT NULL
         --                         BEGIN
         --                                SELECT Param FilterIDs
         --                                INTO #Filters
         --                                FROM fn_SplitParam(@FilterID,',') P
         --                                LEFT JOIN AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
         --                                WHERE AF.AdminFilterID IS NULL


         --                                SELECT Param FilterValues
         --                                INTO #FilterValues
         --                                FROM fn_SplitParam(@FilterValuesID,',')


									--	 IF @FilterValuesID IS NULL
									--	 BEGIN
									--		 INSERT INTO #FilterRetaillocations(RetailID)
											 	
									--		 SELECT DISTINCT RF.RetailID                                     
									--		 FROM RetailerFilterAssociation RF                                                 
									--		 INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BusinessCategoryID =@BusinessCategoryID										

									--	 END

									--	 ELSE IF @FilterValuesID IS NOT NULL AND (SELECT COUNT(1) FROM #Filters)>0
									--	 BEGIN


									--		 SELECT DISTINCT  RF.RetailID 
									--		 INTO #Temp1                                   
									--		 FROM RetailerFilterAssociation RF
									--		 INNER JOIN #FilterValues FV ON FV.FilterValues =RF.AdminFilterValueID AND BusinessCategoryID =@BusinessCategoryID  										 


									--		 SELECT DISTINCT  RF.RetailID  
									--		 INTO #temp2                                  
									--		 FROM RetailerFilterAssociation RF											         
									--		 INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BusinessCategoryID =@BusinessCategoryID 

									--		 INSERT INTO #FilterRetaillocations(RetailID)

									--		 SELECT DISTINCT T.RetailID
									--		 FROM #temp2 T
									--		 INNER JOIN #Temp1 TT ON T.RetailID =TT.RetailID
									--	 END 

									--	 ELSE
									--	 BEGIN

									--		 INSERT INTO #FilterRetaillocations(RetailID)

									--		 SELECT DISTINCT  RF.RetailID 											                                  
									--		 FROM RetailerFilterAssociation RF
									--		 INNER JOIN #FilterValues FV ON FV.FilterValues =RF.AdminFilterValueID AND BusinessCategoryID =@BusinessCategoryID  

									--	 END

									--END

									 --Filter Implementation 
                                  IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
                                  BEGIN
                                         SELECT Param FilterIDs
                                         INTO #Filters
                                         FROM fn_SplitParam(@FilterID,',') P
                                         LEFT JOIN AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
                                         WHERE AF.AdminFilterID IS NULL


                                         SELECT Param FilterValues
                                         INTO #FilterValues
                                         FROM fn_SplitParam(@FilterValuesID,',')

                                  

                                         SELECT DISTINCT RetailID 
                                         INTO #FilterRetaillocations
                                         FROM(
                                         SELECT RF.RetailID                                    
                                         FROM RetailerFilterAssociation RF
                                         INNER JOIN #FilterValues F ON F.FilterValues =RF.AdminFilterValueID AND BusinessCategoryID =@BusinessCategoryID

                                         UNION

                                         SELECT RF.RetailID                                    
                                         FROM RetailerFilterAssociation RF
                                         INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BusinessCategoryID =@BusinessCategoryID
                                         )A

								 END



                CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
                                  
                           CREATE TABLE #CityList(CityID Int,CityName Varchar(200))

                           IF (SELECT 1 FROM HcUser WHERE UserName ='guestlogin' AND HcUserID =@UserID1) >0
                           BEGIN
                                  SET @GuestLoginFlag =1
                           END


                           IF(SELECT 1 from HcHubCiti H
                                  INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                                  AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
                                  
                                  BEGIN
                                                SET @RegionAppFlag =1    
                                                INSERT INTO #RHubcitiList(HchubcitiID)
                                                SELECT DISTINCT HcHubCitiID 
                                                FROM HcHubCiti WHERE HcHubCitiID =@HCHubCitiID1  
                                  END


                           ELSE
                                  BEGIN
                                                SET @RegionAppFlag =0

                                                INSERT INTO #RHubcitiList(HchubcitiID)
                                                SELECT DISTINCT HcHubCitiID 
                                                FROM HcHubCiti WHERE HcHubCitiID =@HCHubCitiID1                                     
                                  END

                           --To Fetch region app associated citylist                       
                           INSERT INTO #CityList(CityID,CityName)
                                  
                           SELECT DISTINCT C.HcCityID 
                                         ,C.CityName                              
                           FROM #RHubcitiList H
                           INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID 
                           INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID 
                           LEFT JOIN fn_SplitParam(@HcCityID1,',') S ON S.Param =C.HcCityID
                           LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1 AND UP.HcHubcitiID =H.HchubcitiID                                                             
                           WHERE (@HcCityID1 IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
                           OR (@HcCityID1 IS NOT NULL AND C.HcCityID =S.Param )
                           OR (@HcCityID1 IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID)          
                           OR (@HcCityID1 IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID =@UserID1)
						   UNION 
						   SELECT 0,'ABC'
               
                           DECLARE @Globalimage varchar(50)
                           SELECT @Globalimage =ScreenContent 
                           FROM AppConfiguration 
                           WHERE ConfigurationType ='Image Not Found'

                           SELECT @UserLatitude = @Latitude
                                  , @UserLongitude = @Longitude

                           IF (@UserLatitude IS NULL) 
                           BEGIN
                                  SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                                  FROM HcUser A
                                  INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                                  WHERE HcUserID = @UserID1 
                           END
                           --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                           IF (@UserLatitude IS NULL) 
                           BEGIN
                                  SELECT @UserLatitude = Latitude
                                                , @UserLongitude = Longitude
                                  FROM HcHubCiti A
                                  INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                                  WHERE A.HcHubCitiID = @HCHubCitiID1
                           END

                --Set the Next page flag to 0 initially.
                SET @NxtPageFlag = 0
                           
                           --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.            
                           EXEC [HubCitiApp2_1].[usp_HcUserHubCitiRangeCheck] @UserID1, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                           SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)                      
                                                      
                   
                --Derive the co ordinates if absent in the input.
                IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                BEGIN
                                         IF @PostalCode IS NOT NULL
                                         BEGIN
                                                SELECT @Latitude=Latitude
                                                              ,@Longitude=Longitude
                                                FROM GeoPosition 
                                                Where PostalCode=@postalCode
                                         END
                                         ELSE
                                                BEGIN
                                                       SELECT @Latitude = G.Latitude
                                                            , @Longitude = G.Longitude
                                                       FROM GeoPosition G
                                                       INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                                       WHERE U.HcUserID = @UserID1 
                                                END    

                END
                           
                SELECT @Config=ScreenContent
                FROM AppConfiguration 
                WHERE ConfigurationType='App Media Server Configuration'

                SELECT @RetailConfig=ScreenContent
                FROM AppConfiguration 
                WHERE ConfigurationType='Web Retailer Media Server Configuration'

                           --To fetch all the duplicate retailers.
                           SELECT DISTINCT DuplicateRetailerID 
                           INTO #DuplicateRet
                           FROM Retailer 
                           WHERE DuplicateRetailerID IS NOT NULL

     --           --Split the search string.
     --           SELECT Param
     --           INTO #RetSearch
     --           FROM [HubCitiApp2_3_3].[fn_SplitParam](LTRIM(RTRIM(@SearchKey)), ' ')
                           
     --           --Remove the noise(stopwords) from the input search string.
     --           SELECT @RetSearch = COALESCE(@RetSearch+' ','') + I.Param 
     --           FROM #RetSearch I
     --           LEFT JOIN sys.fulltext_system_stopwords B ON I.Param = B.stopword AND B.language_id = 1033
     --           WHERE B.stopword IS NULL

     --            --Replace the special characters.
                           --SELECT @SpecialChars = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@RetSearch,'!',''),'@',''),'#',''),'$',''),'%',''),'^',''),'&',''),'*','')
                           --SELECT @RetSearch = IIF(LEN(@SpecialChars) = 0 OR @SpecialChars = '', @RetSearch, @SpecialChars)
                           ----Logic to replace multiple white spaces with a single one.
                           --SELECT @RetSearch = REPLACE(REPLACE(REPLACE(@RetSearch, ' ', '!@'), '@!', ''), '!@', ' ')
                                                
     --            --Stuff "AND" between words of the input search string.
     --            SELECT @RetSearch = REPLACE(LTRIM(RTRIM(@RetSearch)), ' ', ' AND ')
                                         
                --To get the row count for pagination.  
                SELECT @UpperLimit = @LowerLimit + ScreenContent 
                        , @RowsPerPage = ScreenContent
                FROM AppConfiguration   
                WHERE ScreenName = @ScreenName 
                AND ConfigurationType = 'Pagination'
                AND Active = 1  
             
                           
                --Get the user preferred radius.
               
                        IF @Radius IS NULL
                        BEGIN
                         SELECT @Radius = LocaleRadius
                FROM HcUserPreference 
                WHERE HcUserID = @UserID1
               END              
                SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))
              
                        
                           --To get Find Category associated Business Category
                           --SELECT DISTINCT F.BusinessCategoryID
                           --INTO #BusinessCategory
                           --FROM  FindSourceCategory F
                           --INNER JOIN APIPartner A ON A.APIPartnerID = F.APIPartnerID
                           --LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON FB.BusinessCategoryID=F.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemId1
                           --WHERE ActiveFlag = 1 And APIPartnerName = @ApiPartnerName
                           --AND CategoryDisplayName = @CategoryName AND ((FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID) OR 1=1)         
                           
                           SELECT DISTINCT F.BusinessCategoryID
                           INTO #BusinessCategory
                           FROM  BusinessCategory F                        
                           LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON FB.BusinessCategoryID=F.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemId1
                           WHERE F.BusinessCategoryName = @CategoryName AND ((FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID) OR 1=1)               
                           
                           SELECT @CategoryID = BusinessCategoryID
                           FROM #BusinessCategory

						    select distinct BusinessCategoryID  into #SubCategorytype  from HcBusinessSubCategorytype  WITH(NOLOCK)
	
									 select distinct BusinessCategoryID into #NONSUB from BusinessCategory WITH(NOLOCK)
									 except
									 select distinct BusinessCategoryID  from #SubCategorytype WITH(NOLOCK)

									 create  table #RetailerBusinessCategory(RetailerID int,BusinessCategoryID  int)

									 if @HcMenuItemId  is not null
									 INSERT  INTO #RetailerBusinessCategory
									 SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 INNER JOIN HcMenuFindRetailerBusinessCategories HCB  ON HCB.BusinessCategoryID=st.BusinessCategoryID
									 AND RBC.BusinessCategoryID= HCB.BusinessCategoryID
									 AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId
									 else
									 INSERT  INTO #RetailerBusinessCategory
									 	 SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID  
									FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 INNER JOIN HcMenuFindRetailerBusinessCategories HCB  ON HCB.BusinessCategoryID=st.BusinessCategoryID
									 AND RBC.BusinessCategoryID= HCB.BusinessCategoryID
									 AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID


									  INSERT  INTO #RetailerBusinessCategory
									  SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID  
									  FROM RetailerBusinessCategory RBC
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID

                                                                     
                                  CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                                            , RetailID INT 
                                                            , RetailName VARCHAR(1000)
                                                            , RetailLocationID INT
                                                            , Address1 VARCHAR(1000)
                                                            --, Address2 VARCHAR(1000)
                                                            --, Address3 VARCHAR(1000)
                                                            --, Address4 VARCHAR(1000)
                                                            , City VARCHAR(1000)    
                                                            , State CHAR(2)
                                                            , PostalCode VARCHAR(20)
                                                            , retLatitude FLOAT
                                                            , retLongitude FLOAT
                                                            , RetailerImagePath VARCHAR(1000)      
                                                            , Distance FLOAT  
                                                             , DistanceActual FLOAT                                                  
                                                            , SaleFlag BIT)
                                                            --, HcBusinessSubCategoryID INT
                                                            --, BusinessSubCategoryName VARCHAR(1000))   
                           IF (@FilterID IS NULL AND @FilterValuesID IS NULL)
                           BEGIN
                                  IF (@SearchKey IS NOT NULL AND @SearchKey <> '')
                                  BEGIN                                     

                                                INSERT INTO #Retail(RetailID   
                                                            , RetailName  
                                                            , RetailLocationID  
                                                            , Address1  
                                                            --, Address2  
                                                            --, Address3  
                                                            --, Address4  
                                                            , City  
                                                            , State  
                                                            , PostalCode  
                                                            , retLatitude  
                                                            , retLongitude  
                                                            , RetailerImagePath  
                                                            , Distance 
                                                            , DistanceActual                                                 
                                                            , SaleFlag ) 
                                                            --, HcBusinessSubCategoryID  
                                                            --, BusinessSubCategoryName)
                                                                SELECT  RetailID    
                                                                           , RetailName
                                                                           , RetailLocationID
                                                                           , Address1
                                                                           --, Address2
                                                                           --, Address3
                                                                           --, Address4
                                                                           , City    
                                                                           , State
                                                                           , PostalCode
                                                                           , retLatitude
                                                                           , retLongitude
                                                                           , RetailerImagePath    
                                                                           , Distance    
                                                                           , DistanceActual                                             
                                                                           , SaleFlag
                                                                           --, HcBusinessSubCategoryID 
                                                                           --, BusinessSubCategoryName
                                                       FROM 
                                                   (SELECT DISTINCT R.RetailID     
                                                                            , R.RetailName
                                                                           , RL.RetailLocationID 
                                                                            , RL.Address1     
                                                                           -- , RL.Address2
                                                                           --, RL.Address3
                                                                           --, RL.Address4
                                                                           , RL.City
                                                                           , RL.State
                                                                           , RL.PostalCode
                                                                           , RL.RetailLocationLatitude  retLatitude
                                                                           , RL.RetailLocationLongitude retLongitude
                                                                           -- , RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
                                                                           --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),@Config+@Globalimage)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)  
                                                                          , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                                           , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                                            , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                        
                                                                            --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                                           , SaleFlag = 0--CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
                                                                           -- , HcBusinessSubCategoryID = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, 0, SB.HcBusinessSubCategoryID)
                                                                           --, BusinessSubCategoryName = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, '0', BusinessSubCategoryName)
                                                FROM Retailer R 
												inner join #RetailerBusinessCategory  on #RetailerBusinessCategory.RetailerID=r.RetailID
                                                INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
                                                INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                                INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
                                                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 --AND RL.HcCityID = HL.HcCityID
                                                INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
                                                INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID ) OR (@RegionAppFlag =0 AND CL.CityID=0)
                                                INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
                                                
                                                --INNER JOIN #Search S ON R.RetailID = S.RetailID
                                                LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
												LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                                -----------to get retailer that have products on sale
                                                --LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
                                                LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
                                                LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID 
                                                 WHERE Headquarters = 0    AND D.DuplicateRetailerID IS NULL AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')                                                                                 
                                                AND (@BusinessSubCategoryID IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM #BusinessSubCategoryIDs)) 
												AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
											
												--AND ((S.RetailLocationID IS NOT NULL AND S.RetailLocationID = RL.RetailLocationID)
                                                --                         OR
                                                --            (S.RetailLocationID IS NULL AND 1=1))
                                                --AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                                ) Retailer
                                                WHERE Distance <= @Radius                             
                                                --ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                                --                     WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(RetailName AS SQL_VARIANT)
                                                --                     WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
                                                --       END ASC ,Distance
                                                       --CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --              WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(RetailName AS SQL_VARIANT) 
                                                       --              WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --END DESC
                                  END
              
                                  IF @SearchKey IS NULL
                                  BEGIN
                                            INSERT INTO #Retail(RetailID   
                                            , RetailName  
                                            , RetailLocationID  
                                            , Address1  
                                            --, Address2  
                                            --, Address3  
                                            --, Address4  
                                            , City  
                                            , State  
                                            , PostalCode  
                                            , retLatitude  
                                            , retLongitude  
                                            , RetailerImagePath  
                                            , Distance    
                                             , DistanceActual                                              
                                            , SaleFlag ) 
                                            --, HcBusinessSubCategoryID  
                                            --, BusinessSubCategoryName)
                                                         SELECT  RetailID    
                                                                           , RetailName
                                                                           , RetailLocationID
                                                                           , Address1
                                                                           --, Address2
                                                                           -- , Address3
                                                                           --, Address4
                                                                           , City    
                                                                           , State
                                                                           , PostalCode
                                                                           , retLatitude
                                                                           , retLongitude
                                                                           , RetailerImagePath    
                                                                           , Distance    
                                                                           , DistanceActual                                             
                                                                            , SaleFlag
                                                                           --, HcBusinessSubCategoryID 
                                                                           --, BusinessSubCategoryName
                                                       FROM 
                                                   (SELECT DISTINCT R.RetailID     
                                                                            , R.RetailName
                                                                           , RL.RetailLocationID 
                                                                            , RL.Address1     
                                                                           -- , RL.Address2
                                                                           --, RL.Address3
                                                                           --, RL.Address4
                                                                           , RL.City
                                                                           , RL.State
                                                                           , RL.PostalCode
                                                                           --, RL.RetailLocationLatitude  retLatitude
                                                                           --, RL.RetailLocationLongitude retLongitude
                                                                           , ISNULL(RL.RetailLocationLatitude, G.Latitude)  retLatitude
                                                                           , ISNULL(RL.RetailLocationLongitude, G.Longitude) retLongitude
                                                                           --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
                                                                            --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),@Config+@Globalimage)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)  
                                                                            , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                                           , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                                            , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                        
                                                                            --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                                           , SaleFlag = 0--CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
                                                                           -- , HcBusinessSubCategoryID = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, 0, SB.HcBusinessSubCategoryID)
                                                                           --, BusinessSubCategoryName = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, '0', BusinessSubCategoryName)
                                                FROM Retailer R 
												inner join #RetailerBusinessCategory  on #RetailerBusinessCategory.RetailerID=r.RetailID
                                                INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
                                                INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                                INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                               
                                                --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
                                                --INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HCHubCitiID1 AND Associated =1 
                                                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1  --AND RL.HcCityID = HL.HcCityID--AND HL.HcHubCitiID=@HCHubCitiID1 
                                                INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
                                                INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0)
                                                INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
                                                LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
												LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                                -----------to get retailer that have products on sale                                           
                                                                                  LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID              
                                                WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL                              
                                                AND (@BusinessSubCategoryID IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM #BusinessSubCategoryIDs)) 
												AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
											
												 --FROM Retailer R 
                                                 --INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID
                                                --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                                 --INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                 
                                                 --INNER JOIN HcRetailerAssociation HL ON RL.RetailLocationID = HL.RetailLocationID AND HL.HcHubCitiID = @HCHubCitiID1 AND Associated = 1
                                                
                                                 --INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
                                                 --INNER JOIN HcLocationAssociation LA ON RL.PostalCode=LA.PostalCode AND LA.HcHubCitiID =RH.HchubcitiID 
                                     --      INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityName =LA.City) OR (@RegionAppFlag =0)

                                                --LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                --LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                 --LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
                                  
                                                 -------------to get retailer that have products on sale
                                                --LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
                                                --WHERE Headquarters = 0                                           
                                                 --AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                                ) Retailer
                                                WHERE Distance <= @Radius                             
                                             --ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                             --                        WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(RetailName AS SQL_VARIANT)
                                             --                        WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
                                             --          END ASC ,Distance
                                                       --CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --              WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(RetailName AS SQL_VARIANT) 
                                                       --              WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --END DESC
                                  END
                           END

                           IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
                           BEGIN
                                  IF (@SearchKey IS NOT NULL AND @SearchKey <> '')
                                  BEGIN
                                         INSERT INTO #Retail(RetailID   
                                            , RetailName  
                                            , RetailLocationID  
                                            , Address1  
                                            --, Address2  
                                            --, Address3  
                                            --, Address4  
                                            , City  
                                            , State  
                                            , PostalCode  
                                            , retLatitude  
                                            , retLongitude  
                                            , RetailerImagePath  
                                            , Distance 
                                            , DistanceActual                                                 
                                            , SaleFlag )
                                            --, HcBusinessSubCategoryID  
                                            --, BusinessSubCategoryName)
                                        SELECT  RetailID    
                                                    , RetailName
                                                    , RetailLocationID
                                                    , Address1
                                                    --, Address2
                                                    --, Address3
                                                    --, Address4
                                                    , City    
                                                    , State
                                                    , PostalCode
                                                    , retLatitude
                                                    , retLongitude
                                                    , RetailerImagePath    
                                                    , Distance    
                                                    , DistanceActual                                             
                                                    , SaleFlag
                                                    --, HcBusinessSubCategoryID 
                                                    --, BusinessSubCategoryName
                                                       FROM 
                                                   (SELECT DISTINCT R.RetailID     
                                                                             , R.RetailName
                                                                           , RL.RetailLocationID 
                                                                            , RL.Address1     
                                                                           -- , RL.Address2
                                                                           --, RL.Address3
                                                                           --, RL.Address4
                                                                           , RL.City
                                                                           , RL.State
                                                                           , RL.PostalCode
                                                                           , RL.RetailLocationLatitude  retLatitude
                                                                           , RL.RetailLocationLongitude retLongitude
                                                                           -- , RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
                                                                           --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),@Config+@Globalimage)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)  
                                                                          , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                                           , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                                            , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                        
                                                                            --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                                           , SaleFlag = 0--CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
                                                                           -- , HcBusinessSubCategoryID = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, 0, SB.HcBusinessSubCategoryID)
                                                                           --, BusinessSubCategoryName = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, '0', BusinessSubCategoryName)
                                                FROM  #FilterRetaillocations RF                                                   
                                                INNER JOIN Retailer R ON R.RetailID =RF.RetailID   AND RetailerActive = 1
												inner join #RetailerBusinessCategory  on #RetailerBusinessCategory.RetailerID=r.RetailID
                                                INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID
                                                INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                                INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
                                                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1  --AND RL.HcCityID = HL.HcCityID--AND HL.HcHubCitiID=@HCHubCitiID1 
                                                INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
                                                INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID ) OR (@RegionAppFlag =0 AND CL.CityID=0)
                                                INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
                                                
                                                --INNER JOIN #Search S ON R.RetailID = S.RetailID
                                                LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
												LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                                -----------to get retailer that have products on sale
                                                --LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
                                                LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
                                                LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID 
                                                 WHERE Headquarters = 0    AND D.DuplicateRetailerID IS NULL AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')                                                                                 
                                                AND (@BusinessSubCategoryID IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM #BusinessSubCategoryIDs)) 
												AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
											
												--AND ((S.RetailLocationID IS NOT NULL AND S.RetailLocationID = RL.RetailLocationID)
                                                --                         OR
                                                --            (S.RetailLocationID IS NULL AND 1=1))
                                               -- AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                                ) Retailer
                                                WHERE Distance <= @Radius                             
                                                --ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                                --                     WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(RetailName AS SQL_VARIANT)
                                                --                     WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
                                                --       END ASC ,Distance
                                                       --CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --              WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(RetailName AS SQL_VARIANT) 
                                                       --              WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --END DESC
                                  END
              
                                  IF @SearchKey IS NULL
                                  BEGIN
                                            INSERT INTO #Retail(RetailID   
                                            , RetailName  
                                            , RetailLocationID  
                                            , Address1  
                                            --, Address2  
                                            --, Address3  
                                            --, Address4  
                                            , City  
                                            , State  
                                            , PostalCode  
                                            , retLatitude  
                                            , retLongitude  
                                            , RetailerImagePath  
                                            , Distance    
                                            , DistanceActual                                              
                                            , SaleFlag  )
                                            --, HcBusinessSubCategoryID  
                                            --, BusinessSubCategoryName)
                                    SELECT  RetailID    
                                                    , RetailName
                                                    , RetailLocationID
                                                    , Address1
                                                    --, Address2
                                                    --, Address3
                                                    --, Address4
                                                    , City    
                                                    , State
                                                    , PostalCode
                                                    , retLatitude
                                                    , retLongitude
                                                    , RetailerImagePath    
                                                    , Distance    
                                                    , DistanceActual                                             
                                                    , SaleFlag
                                                    --, HcBusinessSubCategoryID 
                                                    --, BusinessSubCategoryName
                                                       FROM 
                                                   (SELECT DISTINCT R.RetailID     
                                                                            , R.RetailName
                                                                           , RL.RetailLocationID 
                                                                            , RL.Address1     
                                                                           -- , RL.Address2
                                                                           --, RL.Address3
                                                                           --, RL.Address4
                                                                           , RL.City
                                                                           , RL.State
                                                                           , RL.PostalCode
                                                                           --, RL.RetailLocationLatitude  retLatitude
                                                                           --, RL.RetailLocationLongitude retLongitude
                                                                           , ISNULL(RL.RetailLocationLatitude, G.Latitude)  retLatitude
                                                                           , ISNULL(RL.RetailLocationLongitude, G.Longitude) retLongitude
                                                                           --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
                                                                            --, RetailerImagePath = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),@Config+@Globalimage)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)  
                                                                            , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                                           , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                                            , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                        
                                                                            --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                                           , SaleFlag = 0--CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
                                                                           -- , HcBusinessSubCategoryID = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, 0, SB.HcBusinessSubCategoryID)
                                                                           --, BusinessSubCategoryName = IIF(ISNULL(@BusinessSubCategoryID, 0) = 0, '0', BusinessSubCategoryName)
                                                FROM  #FilterRetaillocations RF                                                          
                                                INNER JOIN Retailer R ON R.RetailID =RF.RetailID  AND RetailerActive = 1
												inner join #RetailerBusinessCategory  on #RetailerBusinessCategory.RetailerID=r.RetailID
                                                INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID
                                                INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                                INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                               
                                                --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
                                                --INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HCHubCitiID1 AND Associated =1 
                                                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 --AND RL.HcCityID = HL.HcCityID--AND HL.HcHubCitiID=@HCHubCitiID1 
                                                INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
                                                INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0)
                                                INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
                                                LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
												LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                                -----------to get retailer that have products on sale                                           
                                                                                  LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID              
                                                WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL                              
                                                AND (@BusinessSubCategoryID IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM #BusinessSubCategoryIDs)) 
												AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
												 --FROM Retailer R 
                                                 --INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID
                                                --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                                 --INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                 
                                                 --INNER JOIN HcRetailerAssociation HL ON RL.RetailLocationID = HL.RetailLocationID AND HL.HcHubCitiID = @HCHubCitiID1 AND Associated = 1
                                                
                                                 --INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
                                                 --INNER JOIN HcLocationAssociation LA ON RL.PostalCode=LA.PostalCode AND LA.HcHubCitiID =RH.HchubcitiID 
                                     --      INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityName =LA.City) OR (@RegionAppFlag =0)

                                                --LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                --LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                 --LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
                                  
                                                 -------------to get retailer that have products on sale
                                                --LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
                                                --WHERE Headquarters = 0                                           
                                                 --AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                                ) Retailer
                                                WHERE Distance <= @Radius                             
                                             --ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                             --                        WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(RetailName AS SQL_VARIANT)
                                             --                        WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
                                             --          END ASC ,Distance
                                                       --CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --              WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(RetailName AS SQL_VARIANT) 
                                                       --              WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
                                                       --END DESC
                                  END
                           
                           END
                           
                           SELECT DISTINCT Retailid , RetailLocationid
                                  INTO #RetailItemsonSale1
                                  FROM 
                                  (SELECT DISTINCT R.RetailID, a.RetailLocationID 
                                  FROM RetailLocationDeal a 
                                  INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
                                  INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                    and a.ProductID = c.ProductID
                                    and GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) and ISNULL(a.SaleEndDate, @Tomorrow)
                                  


                                  UNION ALL 
                                  SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                                  FROM Coupon C 
                                  INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
                                  INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
                                  LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                                  WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                                  GROUP BY C.CouponID
                                                       ,NoOfCouponsToIssue
                                                       ,CR.RetailID
                                                       ,CR.RetailLocationID
                                  HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                                                       ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                          
                                  UNION ALL  

                                  SELECT DISTINCT  RR.RetailID, 0 as RetaillocationID  
                                  from Rebate R 
                                  INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
                                  INNER JOIN #Retail RE ON RE.RetailLocationID =RR.RetailLocationID
                                  WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

                                  UNION ALL  

                                  SELECT DISTINCT R.retailid, a.RetailLocationID 
                                  FROM  LoyaltyDeal a
                                   INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
                                  INNER JOIN #Retail R ON R.RetailLocationID =a.RetailLocationID
                                  INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID 
                                                                                                                     AND b.ProductID = LDP.ProductID 

                                  WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                                  UNION ALL 

                                  SELECT DISTINCT R.RetailID, R.RetailLocationID
                                  FROM ProductHotDeal p
                                  INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                                  INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
                                  LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                                  LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                                  WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
                                  GROUP BY P.ProductHotDealID
                                                       ,NoOfHotDealsToIssue
                                                       ,R.RetailID
                                                       ,R.RetailLocationID
                                  HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                                                       ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                                  UNION ALL 

                                  select q.RetailID, qa.RetailLocationID
                                  from QRRetailerCustomPage q
                                  INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                                  INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
                                  INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                                  where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
                                  ) Discount 
                    



                            SELECT DISTINCT Row_Num = RowNum
                                                              , r.RetailID    
                                                              , RetailName
                                                              , rl.RetailLocationID
                                                              , r.Address1
                                                              --, r.Address2
                                                              --, r.Address3
                                                              --, r.Address4
                                                              , r.City    
                                                              , r.State
                                                              , r.PostalCode
                                                              , retLatitude
                                                              , retLongitude
                                                              , RetailerImagePath    
                                                              , Distance           
                                                              , DistanceActual                                       
                                                              , SaleFlag= CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
                                                              --, HcBusinessSubCategoryID 
                                                              --, BusinessSubCategoryName
                                  INTO #Ret    
                                  FROM #Retail R
                                  INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
                                  LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
								  WHERE RL.Active = 1
                                   --ORDER BY RowNum ASC
                                                                                  
                                  
                                    
                                   SELECT DISTINCT Row_Num rowNumber  
                                                              , RetailID retailerId    
                                                              , RetailName retailerName
                                                              , R.RetailLocationID retailLocationID
                                                              , Address1 retaileraddress1
                                                              --, Address2 retaileraddress2
                                                              --, Address3 retaileraddress3
                                                              --, Address4 retaileraddress4
                                                              , City City
                                                              , State State
                                                              , PostalCode PostalCode
                                                              , retLatitude
                                                              , retLongitude    
                                                              , RetailerImagePath logoImagePath     
                                                              , Distance  
                                                              , DistanceActual    
                                                              , S.BannerAdImagePath bannerAdImagePath    
                                                              , B.RibbonAdImagePath ribbonAdImagePath    
                                                              , B.RibbonAdURL ribbonAdURL    
                                                              , B.RetailLocationAdvertisementID advertisementID  
                                                              , S.SplashAdID splashAdID
                                                              , SaleFlag 
                                                              --, HcBusinessSubCategoryID 
                                                              --, BusinessSubCategoryName  
                                   INTO #Retailer   
                                   FROM #Ret R
                                  LEFT JOIN (SELECT DISTINCT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
                                                                                                                     ELSE SplashAdImagePath
                                                                                                                END
                                                , SplashAdID  = ASP.AdvertisementSplashID
                                                , R.RetailLocationID 
                                FROM #Retail R
                                INNER JOIN RetailLocation RL ON RL.RetailLocationID=R.RetailLocationID
                                INNER JOIN RetailLocationSplashAd RS ON RL.RetailLocationID = RS.RetailLocationID
                                INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, @Tomorrow)) S ON R.RetailLocationID = S.RetailLocationID
                                  LEFT JOIN (SELECT DISTINCT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
                                                                                                            ELSE AB.BannerAdImagePath
                                                                                                    END 
                                            , RibbonAdURL = AB.BannerAdURL
                                            , RetailLocationAdvertisementID = AB.AdvertisementBannerID
                                            , R.RetailLocationID 
                                FROM #Retail R
                                INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
                                INNER JOIN RetailLocation RL ON RL.RetailLocationID=RB.RetailLocationID AND RL.Headquarters = 0
                                INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
                                WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, @Tomorrow)) B ON R.RetailLocationID = B.RetailLocationID                               
                                  
								SELECT DISTINCT  rowNumber = ROW_NUMBER() OVER --(ORDER BY Distance) rowNumber  
															 (ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
																		WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
																		WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
																	END ASC ,Distance)
                                                , retailerId    
                                                , retailerName
                                                , retailLocationID
                                                , retaileraddress1
                                                --, retaileraddress2
                                                --, retaileraddress3
                                                --, retaileraddress4
                                                , City
                                                , State
                                                , PostalCode
                                                , retLatitude
                                                , retLongitude    
                                                , logoImagePath     
                                                , Distance  
                                                , DistanceActual    
                                                , bannerAdImagePath    
                                                , ribbonAdImagePath    
                                                , ribbonAdURL    
                                                , advertisementID  
                                                , splashAdID
                                                , SaleFlag 
                                                --, HcBusinessSubCategoryID 
                                                --, BusinessSubCategoryName  
                                   INTO #Retailer1
								   FROM  #Retailer
                                   WHERE (ISNULL(@LocalSpecials,0) = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))
								   

								    --To capture max row number.  
                                   SELECT @MaxCnt = Count(1) FROM #Retailer1
                                  
                                   --this flag is a indicator to enable "More" button in the UI.   
                                   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                                   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

								   SELECT DISTINCT rowNumber  
                                                , retailerId    
                                                , retailerName
                                                , retailLocationID
                                                , retaileraddress1
                                                --, retaileraddress2
                                                --, retaileraddress3
                                                --, retaileraddress4
                                                , City
                                                , State
                                                , PostalCode
                                                , retLatitude
                                                , retLongitude    
                                                , logoImagePath     
                                                , Distance  
                                                , DistanceActual    
                                                , bannerAdImagePath    
                                                , ribbonAdImagePath    
                                                , ribbonAdURL    
                                                , advertisementID  
                                                , splashAdID
                                                , SaleFlag 
                                                --, HcBusinessSubCategoryID 
                                                --, BusinessSubCategoryName  
                                   INTO #RetailerList
								   FROM  #Retailer1
                                   WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit               
                                   ORDER BY rowNumber
                    --OFFSET (@LowerLimit) ROWS
                    --FETCH NEXT @RowsPerPage ROWS ONLY
                                  
                                  
                                  --User Tracking section.
                                  
                                   ---Region App

                                  INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
                                                                                                                ,CityID
                                                                                                                ,DateCreated)
                                  SELECT DISTINCT @MainMenuID1 
                                           ,CityID 
                                           ,GETDATE()
                                  FROM #CityList 

                                    --Table to track the Retailer List.
                                  CREATE TABLE #Temp(Rowno int IDENTITY(1,1)
													,RetailerListID int
													,LocationDetailID int
													,MainMenuID int
													,RetailID int
													,RetailLocationID int)  
                      --Capture the impressions of the Retailer list.
                      INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
                                                                                                                     , RetailID
                                                                                                                     , RetailLocationID
                                                                                                                     , FindCategoryID
                                                                                                                     , DateCreated)
                     
                      OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)                                             
                                                                                         SELECT DISTINCT @MainMenuID1
                                                                                                , retailerId
                                                                                                , RetailLocationID
                                                                                                , @CategoryID
                                                                                                , GETDATE()
                                                                                         FROM #RetailerList 
                                                                                         
                                                             
                           
                                  
                                  --Display the Retailer along with the keys generated in the Tracking table.
                                   SELECT Distinct rowNumber
                                                       , T.RetailerListID retListID
                                                       , retailerId
                                                       , retailerName
                                                       , T.RetailLocationID
                                                       , retaileraddress1
                                                       --, retaileraddress2
                                                       --, retaileraddress3
                                                       --, retaileraddress4
                                                       , City
                                                       , State
                                                       , PostalCode                                   
                                                        , retLatitude
                                                       , retLongitude
                                                       , Distance = ISNULL(DistanceActual, Distance)
                                                       , logoImagePath
                                                       , bannerAdImagePath
                                                       , ribbonAdImagePath
                                                       , ribbonAdURL
                                                       , advertisementID
                                                       , splashAdID
                                                       , SaleFlag   
                                                       --, HcBusinessSubCategoryID subCatId
                                                       --, BusinessSubCategoryName subCatName                         
                                  FROM #Temp T
                                  INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID
                                  --INNER JOIN #RetailerList R ON  R.rowNumber = T.Rowno
                                  
                                  --To display Find related Bottom Button list
                                  EXEC [HubCitiApp2_1].[usp_HcFunctionalityBottomButtonDisplay] @HCHubCitiID1,@ModuleName,@UserID1, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage =     @ErrorMessage output        
                                                              
                 --Confirmation of Success.
                 SELECT @Status = 0  
				 
				 --To display message when no Retailers display for user preferred cities.
				 DECLARE @UserPrefCities NVarchar(MAX)

				 IF (@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0) AND (@SearchKey IS NULL) AND @BusinessSubCategoryID IS NULL AND @HcCityID1 IS NULL 
										AND @FilterID IS NULL AND  @FilterValuesID IS NULL AND @LocalSpecials = 0 AND @Interests IS NULL
				 BEGIN 
						 SELECT @UserPrefCities = COALESCE(@UserPrefCities+',' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName))) 
						 FROM HcUsersPreferredCityAssociation P
						 INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
						 WHERE HcHubcitiID = @HCHubCitiID1 AND HcUserID = @UserID1
						
						SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
												'.\n\nUpdate your city preferences in the settings menu.'

				 END
				 ELSE IF ((@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0)) 
						 AND (@SearchKey IS NOT NULL OR @BusinessSubCategoryID IS NOT NULL OR @HcCityID1 IS NOT NULL OR @FilterID IS NOT NULL
						              OR  @FilterValuesID IS NOT NULL OR @LocalSpecials = 1 OR @Interests IS NOT NULL)
				 BEGIN
					
					SELECT @NoRecordsMsg = 'No Records Found.'

				 END
				 ELSE IF (@RegionAppFlag = 0) AND (ISNULL(@MaxCnt,0) = 0) 
				 BEGIN
					
					SELECT @NoRecordsMsg = 'No Records Found.'

				 END
			
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
             IF @@ERROR <> 0    
                       BEGIN    
                       PRINT 'Error occured in Stored Procedure usp_HcSingleFindRetailerListPagination.'      
                       --- Execute retrieval of Error info.  
                       EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
                       PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                       ROLLBACK TRANSACTION;
                       --Confirmation of failure.
                       SELECT @Status = 1
                       END;    
            
      END CATCH;
END;


































GO
