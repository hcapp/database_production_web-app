USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserTrackingShareAppsite]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcUserTrackingShareAppsite 
Purpose               : To track appsite share details.  
Example               : usp_HcUserTrackingShareAppsite 
  
History  
Version    Date            Author         Change          Description  
---------------------------------------------------------------   
1.0        7th Nov 2013    Mohith H R	  Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserTrackingShareAppsite]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @RetailLocationID int 
 , @TargetAddress Varchar(255)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
        
		 INSERT INTO HubCitiReportingDatabase..ShareAppsite(MainMenuID
															    ,ShareTypeID
															    ,TargetAddress
															    ,RetailLocationID														 
															    ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@RetailLocationID 
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0            							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingShareAppsite.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
    --Confirmation of failure.
    SELECT @Status = 1     
  END;  
     
 END CATCH;  
END;












































GO
