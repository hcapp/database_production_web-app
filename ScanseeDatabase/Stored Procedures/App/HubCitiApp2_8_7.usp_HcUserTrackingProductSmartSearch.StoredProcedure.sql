USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingProductSmartSearch]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserTrackingProductSmartSearch
Purpose					: To create an entry in the Prduct Smart search tracking table.
Example					: usp_HcUserTrackingProductSmartSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14thNov 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingProductSmartSearch]
(
	  @ProdSearch varchar(500)	
	  
	--User Tracking Inputs.	
	, @MainMenuID int	
	
	--Output Variable 
	, @ProductSmartSearchID int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION		
			
			--User Tracking Section.
			INSERT INTO HubCitiReportingDatabase..ProductSmartSearch(MainMenuID
																   , SearchKeyword
																   , CreatedDate)
														SELECT @MainMenuID
															 , @ProdSearch
															 , GETDATE()
			--Capture the identity value.
			SELECT @ProductSmartSearchID = SCOPE_IDENTITY()
						
		 --Confirmation of Success.
		 SELECT @Status = 0	
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserTrackingProductSmartSearch.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1			
		END;
		 
	END CATCH;
END;














































GO
