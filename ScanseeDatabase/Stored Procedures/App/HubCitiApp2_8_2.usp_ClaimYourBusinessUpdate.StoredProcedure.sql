USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_ClaimYourBusinessUpdate]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ClaimYourBusinessUpdate
Purpose					: To Update the Claim Details for the given RetailLocation and display the Example URLs's.
Example					: usp_ClaimYourBusinessUpdate
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		  8/23/2016			Bindu T A			Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_ClaimYourBusinessUpdate]
(
	  @RetailLocationID INT
	 ,@HcHubcitiID INT

	  --Output Variable 
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN 
	BEGIN TRAN

		BEGIN TRY
		
	DECLARE @HubCitiClaimURL varchar(max)	
		 
	SELECT @HubCitiClaimURL = HubCitiClaimURL
	FROM HcHubCiti
	WHERE HcHubCitiID = @HcHubcitiID

		 SELECT claimTxtMsg = 'Thank you! Welcome to your community''s all-in-one app. One of our Team Members will contact you shortly to upload your Free additions. 

In the meantime, would you like to see some examples of how we can help you get found and tell your story?' ,
		
		 claimCategoryURL =  @HubCitiClaimURL   
		 FROM RetailLocation WHERE RetailLocationID = @RetailLocationID

		 COMMIT TRAN
		
		 END TRY    	

BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_ClaimYourBusinessUpdate>.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			ROLLBACK TRANSACTION
			
		END;
		 
END CATCH;
END;





GO
