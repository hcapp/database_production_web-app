USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcOnlineRetailerInformation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcOnlineRetailerInformation
Purpose					: To display online store retailer information.
Example					: usp_HcOnlineRetailerInformation

History
Version		Date		Author		Change Description
--------------------------------------------------------------- 
1.0		10/31/2013 	    SPAN	     1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcOnlineRetailerInformation]
(
	
	 @UserID int
	,@ProductID int
	,@HcHubCitiID Int
	
	--User Tracking Inputs
    , @ProductListID int
    , @MainMenuID int
    
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive=1

		--User Tracking
	    DECLARE	@RowCount int
		
		SELECT DISTINCT RL.RetailID retailerId
			   ,RL.RetailLocationID
			   ,RetailName retailerName
			   ,RetailOnlineURL retailOnlineURL
			   ,Price
			   ,SalePrice
			   ,BuyURL buyURL
			   ,CASE WHEN ISNULL(ShipmentDetails, '0') = '0' OR ShipmentDetails = '0' OR ShipmentDetails LIKE '' THEN '$0.00' ELSE CAST(ShipmentDetails AS VARCHAR(255)) END ShipmentCost 
		INTO #Prod
		FROM Retailer R
		INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID AND R.RetailerActive=1 AND RL.Active=1
		INNER JOIN RetailLocationProduct RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=@ProductID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
		WHERE OnlineStoreFlag=1 AND D.DuplicateRetailerID IS NULL
		
		SELECT @RowCount =@@ROWCOUNT	
		
		--User Tracking		
		
		--Capture the online stores availble for the given product.
		CREATE TABLE #Temp(OnlineProductDetailID INT
						 , RetailLocationID INT
						 , ProductID INT)
				
		INSERT INTO HubCitiReportingDatabase..OnlineProductDetail(ProductListID
																  ,MainMenuID
																  ,RetailLocationID
																  ,ProductID
																  ,DateCreated)
		OUTPUT inserted.OnlineProductDetailID,INSERTED.RetailLocationID, inserted.ProductID INTO #Temp(OnlineProductDetailID,RetailLocationID,ProductID)
														SELECT @ProductListID
															 , @MainMenuID
															 , RetailLocationID
															 , @ProductID
															 , GETDATE()
													    FROM #Prod
		--Display along with the primary key of the tracking table.								    
		 SELECT T.OnlineProductDetailID onProdDetId
			   ,retailerId
			   ,p.RetailLocationID
			   ,retailerName
			   ,retailOnlineURL
			   ,Price
			   ,SalePrice
			   ,buyURL
			   ,ShipmentCost 
		 FROM #Prod P
		 INNER JOIN #Temp T ON T.RetailLocationID = P.RetailLocationID
		 AND T.ProductID = @ProductID	
		
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		
		select error_line()
			PRINT 'Error occured in Stored Procedure usp_HcOnlineRetailerInformation.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			SELECT @Status = 1
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;













































GO
