USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcShareProductDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_HcShareProductDetails  
Purpose                             : To fetch Product Details.  
Example                             : usp_HcShareProductDetails 2,1   
  
History  
Version           Date              Author            Change Description  
---------------------------------------------------------------   
1.0               13thOct 2013	    Dhananjaya TR	  Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcShareProductDetails]  
(  
        @ProductID int 
      , @HcHubcitiID int  
	  , @UserID int
      --OutPut Variable  
	  , @ShareText varchar(500) output
      , @Status int output
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY  
      
		  --To get Media Server Configuration.  
		  DECLARE @ManufConfig varchar(50)  
		  DECLARE @Config varchar(100)
		    
		  SELECT @ManufConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 

		  --Retrieve the server configuration.
           SELECT @Config = ScreenContent 
           FROM AppConfiguration 
           WHERE ConfigurationType LIKE 'QR Code Configuration'

		  --To get the text used for share.         
           DECLARE @HubCitiName varchar(100)
		 
		   SELECT @HubCitiName = HubCitiName
		   FROM HcHubCiti
		   WHERE HcHubCitiID = @HcHubcitiID
		       
		   SELECT @ShareText = 'I found this Product in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
           --FROM AppConfiguration 
           --WHERE ConfigurationType LIKE 'HubCiti Product Share Text' 
      
       SELECT --p.ProductID
        p.ProductName shareProdName  
       --, p.ManufacturerID
       --, p.ModelNumber
       , p.ProductShortDescription
       , p.ProductLongDescription shareProdLongDesc
       --, p.ProductExpirationDate productExpDate
       --, p.ProductAddDate 
       , imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																THEN @ManufConfig
																+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																+ProductImagePath ELSE ProductImagePath 
														  END   
                          ELSE ProductImagePath END 
		 , QRUrl = @Config+'1000.htm?prodId='+ CAST(@ProductID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HcHubcitiID AS VARCHAR(10))
	   --, QRUrl = @Config+'1000.htm?key1='+ CAST(@ProductID AS VARCHAR(10))
       --, p.SuggestedRetailPrice
       --, p.SKUNumber skuCode 
       --, p.Weight productWeight
       --, p.ScanCode barCode
       --, p.WeightUnits
       --, M.ManufName manufacturersName 
       FROM Product p 
       LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID 
       WHERE ProductID = @ProductID

	   SET @Status = 0 
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_HcShareProductDetails.'           
                  --- Execute retrieval of Error info.  
                  EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
				  SET @Status = 1   
            END;  
              
      END CATCH;  
END;













































GO
