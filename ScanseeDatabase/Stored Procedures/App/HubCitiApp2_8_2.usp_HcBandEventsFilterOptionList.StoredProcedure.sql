USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcBandEventsFilterOptionList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [HubCitiApp2_8_2].[usp_HcBandEventsFilterOptionList]
Purpose                 :  To get the list of filter options for Events (Filter and Sort)
Example                 :  [HubCitiApp2_8_2].[usp_HcBandEventsFilterOptionList]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          8/05/2015       SPAN              1.1
1.1         9/14/2016       Shilpashree       Changes w.r.t Hubregion.
1.2			10/26/2016		Bindu T A		  Playing Today changes
-------------------------------------------------------------------------------
*/


CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcBandEventsFilterOptionList]
(   
           
       --Input variable.        
         @HcHubCitiID int
       , @UserID Int
       , @HcMenuItemID int
       , @HcBottomButtonID int
	   , @SearchKey varchar(2000)	
	   , @Latitude float
	   , @Longitude float
	   , @Postalcode Varchar(200)

       --Output Variable 
        , @UserOutOfRange bit output
        , @DefaultPostalCode VARCHAR(10) output
        , @Status int output
        , @ErrorNumber int output
        , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY

			DECLARE @RegionApp Bit
			SELECT @RegionApp = IIF(HcAppListID=2,1,0)
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubCitiID 
              
			--To display Filter items
		   
		    DECLARE	@EventDateSort bit = 1	   
			DECLARe @Distance bit = 1
			DECLARE @Alphabetically bit = 1	
			DECLARE @EventDate bit = 1		 
			DECLARE @Category bit = 1						
			DECLARE @City bit = 1
			DECLARE @Venue bit = 1

			CREATE TABLE #Tempfilter (Type Varchar(50), Items Varchar(50),Flag bit)

			IF EXISTS (SELECT 1 FROM HcMenuItem WHERE HcMenuItemID = @HcMenuItemID AND HcLinkTypeID = (SELECT HcLinkTypeID FROM HcLinkType WHERE LinkTypeName = 'Playing Today')) 
			 BEGIN
				--INSERT INTO #Tempfilter VALUES ('Sort Items by','Date',@EventDateSort)
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Mileage',@Distance)
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Band',@Alphabetically)
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Venue',@Venue)
			
				--INSERT INTO #Tempfilter VALUES ('Filter Items by','Date',@EventDate)
				INSERT INTO #Tempfilter VALUES ('Filter Items by','Genre',@Category)
				INSERT INTO #Tempfilter VALUES ('Filter Items by','City',@City)
			 END
			ELSE
			 BEGIN
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Date',@EventDateSort)
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Mileage',@Distance)
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Band',@Alphabetically)
				INSERT INTO #Tempfilter VALUES ('Sort Items by','Venue',@Venue)
			

				INSERT INTO #Tempfilter VALUES ('Filter Items by','Date',@EventDate)
				INSERT INTO #Tempfilter VALUES ('Filter Items by','Genre',@Category)
				INSERT INTO #Tempfilter VALUES ('Filter Items by','City',@City)
			 END
			
			IF @RegionApp = 1
			BEGIN	
			
				SELECT Type AS fHeader
					  ,Items AS filterName
				FROM #Tempfilter
				--order by fHeader desc, filterName
			END
			ELSE
			BEGIN
				SELECT Type AS fHeader
					  ,Items AS filterName
				FROM #Tempfilter
				WHERE Items <> 'City'
				--ORDER BY fHeader,filterName desc
			END
				
			--Confirmation of Success.
            SELECT @Status = 0


	END TRY

    BEGIN CATCH
      
			--Check whether the Transaction is uncommitable.--usp_HcEventsOptionListDisplay
            IF @@ERROR <> 0     
            BEGIN    
            PRINT 'Error occured in Stored Procedure [HubCitiApp2_8_2].[usp_HcBandEventsFilterOptionList]'      
            --- Execute retrieval of Error info.  
            EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
            --Confirmation of failure.
            SELECT @Status = 1
            END;    
            
      END CATCH;
END;




GO
