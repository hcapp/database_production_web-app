USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUpdateUserPreferences]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUpdateUserPreferences
Purpose					: To Update the version of the App installed on the user's device.
Example					: usp_HcUpdateUserPreferences

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.4			4th Dec 2013				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUpdateUserPreferences]
(

	--Input Parameter(s)--
	  
	  @UserID INT
	, @PushNotify BIT
	, @WishListDefaultRadius Float
	, @DeviceID VARCHAR(100)
	, @HubCitiID int
	, @LocationService BIT
	
   	
	--Output Variable--		
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			--Update the LocaleRadius in  UserPreference TAble.
			--Check if the User Preferences exists.
			IF EXISTS(SELECT * FROM HcUserPreference WHERE HcUserID = @UserID)
			BEGIN
				UPDATE HcUserPreference SET LocaleRadius = @WishListDefaultRadius, EnablePushNotification = @PushNotify, LocationService = @LocationService
				WHERE HcUserID = @UserID 	
		    END
		    ELSE 
		    --Insert if not existing.
			BEGIN
				INSERT INTO HcUserPreference(HcUserID
										 , LocaleRadius
										 , EnablePushNotification
										 , LocationService)
									VALUES(@UserID
										 , @WishListDefaultRadius
										 , @PushNotify
										 , @LocationService)
			END
			
			
			UPDATE HcUserDeviceAppVersion SET PrimaryDevice = 0 WHERE HcUserID = @UserID
			UPDATE HcUserDeviceAppVersion SET PrimaryDevice = 1 WHERE HcUserID = @UserID AND DeviceID = @DeviceID

			IF NOT EXISTS (SELECT 1 FROM HcUserDeviceAppVersion WHERE HcUserID =@UserID AND HcHubCitiID = @HubCitiID AND DeviceID = @DeviceID)
			BEGIN
				INSERT INTO HcUserDeviceAppVersion (HcUserID,HcHubCitiID,DeviceID,DateCreated,DateModified)
				SELECT 
				@UserID,
				@HubCitiID,
				@DeviceID,
				GETDATE(),
				GETDATE()
			END

			--Update Pushnotify in the 	UserToken table	
				UPDATE HcUserToken SET PushNotify =@PushNotify 
				WHERE HcHubCitiID = @HubCitiID AND DeviceID = @DeviceID
			
		--Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUpdateUserPreferences.'		
			--Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
