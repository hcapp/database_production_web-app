USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcWishlistProductAttributeDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcWishlistProductAttributeDisplay
Purpose					: to display attributes of a product.
Example					: usp_HcWishlistProductAttributeDisplay

History
Version		Date			  Author			Change Description
--------------------------------------------------------------- 
1.0			20thNov2013       Dhananjaya TR  	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcWishlistProductAttributeDisplay]
(
	 @ProductID int
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		 SELECT AttributeName=CASE WHEN PA.AttributeName IS NULL THEN A.AttributeName ELSE PA.AttributeName END
				,DisplayValue
		 FROM ProductAttributes PA 
		   INNER JOIN Attribute A ON A.AttributeId = PA.AttributeID  
		 WHERE PA.ProductID  = @ProductID 
		
		--Confirmation of Success.
			SELECT @Status = 0
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_HcWishlistProductAttributeDisplay>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
