USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcSingleFindOptionsInterestsListDisplay1_originalbckup]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcSingleFindOptionsInterestsListDisplay]
Purpose                 :  To get the list of Interests for Find single category Module.
Example                 :  [usp_HcSingleFindOptionsInterestsListDisplay]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/05/2015       SPAN            1.1
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcSingleFindOptionsInterestsListDisplay1_originalbckup]
(
      --Input variable
         @UserID int  
       , @CategoryName varchar(100)
       , @Latitude Decimal(18,6)
       , @Longitude  Decimal(18,6)
       , @HcHubCitiID Int
       , @HcMenuItemID Int
       , @HcBottomButtonID Int
	   , @SearchKey Varchar(250)

      --Output Variable 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
          
				DECLARE @Radius int
                , @PostalCode varchar(10)
                , @Tomorrow DATETIME = GETDATE() + 1
                , @Yesterday DATETIME = GETDATE() - 1
                , @DistanceFromUser FLOAT
                , @UserLatitude decimal(18,6)
                , @UserLongitude decimal(18,6) 
                , @RegionAppFlag Bit
                , @GuestLoginFlag BIT=0
				, @BusinessCategoryID Int
				, @UserOutOfRange bit   
				, @DefaultPostalCode varchar(50) 
				
				DECLARE @UserID1 INT = @UserID,
				@HcHubCitiID1 INT = @HcHubCitiID,
				@HcMenuItemID1 INT = @HcMenuItemID,
				@HcBottomButtonID1 INT = @HcBottomButtonID


				--SearchKey implementation
				DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

				SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
						WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
						WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
						ELSE @SearchKey END)             

				SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName
					
					SET @HcBottomButtonID1 = IIF(@HcBottomButtonID1 IS NOT NULL,0,Null)

                    CREATE TABLE #CityList(CityID Int,CityName Varchar(200))

                    IF (SELECT 1 FROM HcUser WHERE UserName ='guestlogin' AND HcUserID = @UserID1) > 0
                    BEGIN
                    SET @GuestLoginFlag = 1
                    END

					CREATE TABLE #RHubCitiList(HcHubCitiID Int) 

                    IF(SELECT 1 from HcHubCiti H
						INNER JOIN HcAppList AL ON H.HcAppListID = AL.HcAppListID 
						AND H.HcHubCitiID = @HcHubCitiID1 AND AL.HcAppListName = 'RegionApp')>0
                            BEGIN
									SET @RegionAppFlag = 1
										    
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HcHubCitiID1 
									UNION ALL
									SELECT  @HcHubCitiID1 AS HcHubCitiID
									)
                            END
                    ELSE
                            BEGIN
									SET @RegionAppFlag =0
									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HcHubCitiID1                                     
                            END

                           --To Fetch region app associated citylist
						DECLARE @HcCityID varchar(100) = null                   
						INSERT INTO #CityList(CityID,CityName)
                                  
						SELECT DISTINCT C.HcCityID 
										,C.CityName                              
						FROM #RHubcitiList H
						INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HcHubCitiID1 
						INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID 
						LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =C.HcCityID
						LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1  AND UP.HcHubcitiID =H.HchubcitiID                                                      
						WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
						OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param )
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1)                                        
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID1)
						UNION
						SELECT 0,'ABC'


                        SELECT @UserLatitude = @Latitude
                            , @UserLongitude = @Longitude
               
                         IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcUser A
                                    INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                                    WHERE HcUserID = @UserID1 
                        END
                        --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                        IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcHubCiti A
                                    INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                                    WHERE A.HcHubCitiID = @HcHubCitiID1
                        END

                                                      
                        --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
						EXEC [HubCitiApp8].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
						SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
 
						--To fetch all the duplicate retailers.
						SELECT DISTINCT DuplicateRetailerID 
						INTO #DuplicateRet
						FROM Retailer 
						WHERE DuplicateRetailerID IS NOT NULL
                           
                        --Derive the Latitude and Longitude in the absence of the input.
                        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
                        BEGIN
                                    --If the postal code is passed then derive the co ordinates.
                                    IF @PostalCode IS NOT NULL
                                    BEGIN
                                            SELECT @Latitude = Latitude
                                                    , @Longitude = Longitude
                                            FROM GeoPosition 
                                            WHERE PostalCode = @PostalCode
                                    END          
                                    ELSE
                                    BEGIN
                                            SELECT @Latitude = G.Latitude
                                                    , @Longitude = G.Longitude
                                            FROM GeoPosition G
                                            INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                            WHERE U.HcUserID = @UserID1 
                                    END                                                                               
                        END    
                     
                    --Get the user preferred radius.
					SELECT @Radius = LocaleRadius
                    FROM HcUserPreference 
                    WHERE HcUserID = @UserID1
                                         
                    SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
														WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 

                    --To get Find Category associated Business Category
                    SELECT DISTINCT F.BusinessCategoryID
                    INTO #BusinessCategory
                    FROM  BusinessCategory F                        
                    LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON FB.BusinessCategoryID=F.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemID1
                    WHERE F.BusinessCategoryName = @CategoryName AND ((FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID) OR 1=1)               
					DECLARE @CategoryID INT
					 SELECT @CategoryID = BusinessCategoryID
                           FROM #BusinessCategory

						    select distinct BusinessCategoryID  into #SubCategorytype  from HcBusinessSubCategorytype  WITH(NOLOCK) where BusinessCategoryID=@BusinessCategoryID
	
									 select distinct BusinessCategoryID into #NONSUB from BusinessCategory WITH(NOLOCK)   where BusinessCategoryID=@BusinessCategoryID  
									 except
									 select distinct BusinessCategoryID  from #SubCategorytype WITH(NOLOCK)

									 create  table #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int)

									 select * into #HcMenuFindRetailerBusinessCategories 
									  from HcMenuFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
									  AND HcBusinessSubCategoryID IS NOT NULL
									 and HcMenuItemID=@HcMenuItemId

									  select * into #HcMenuFindRetailerBusinessCategories1 
									  from HcMenuFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
									    AND HcBusinessSubCategoryID IS  NULL
									 and HcMenuItemID=@HcMenuItemId

									 select * into #RetailerBusinessCategory2  from RetailerBusinessCategory where  BusinessCategoryID=@BusinessCategoryID
									 create  index  ix_nn  on #HcMenuFindRetailerBusinessCategories(BusinessCategoryID,
									 HcBusinessSubCategoryID)
									  create clustered index  ix_nn1  on #HcMenuFindRetailerBusinessCategories(HcMenuItemID)

									-- select  count(*)  from #HcMenuFindRetailerBusinessCategories

									 if exists(select  1 from #SubCategorytype)

									 begin
									  
									 if @HcMenuItemId  is not null
									 BEGIN
									 INSERT  INTO #RetailerBusinessCategory1
									 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								
									 INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
									  ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								     AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID

									  INSERT  INTO #RetailerBusinessCategory1
									 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								
									 INNER JOIN #HcMenuFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
									  ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								  --   AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID
									 END

									 else
									 INSERT  INTO #RetailerBusinessCategory1
									 	 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 and RBC.BusinessCategoryID=@BusinessCategoryID
									inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									-- INNER JOIN HcMenuFindRetailerBusinessCategories HCB  ON HCB.BusinessCategoryID=st.BusinessCategoryID
									-- AND RBC.BusinessCategoryID= HCB.BusinessCategoryID
									-- AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID

									end

									else 

									begin

									  INSERT  INTO #RetailerBusinessCategory1
									  SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									  FROM RetailerBusinessCategory RBC
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 and RBC.BusinessCategoryID=@BusinessCategoryID

									 end

									 			
						

									 SELECT DISTINCT #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1

									 SELECT distinct   R.RetailID     
														, R.RetailName
														, RL.RetailLocationID 
														, RL.Address1     
														, RL.City
														, RL.State
														, RL.PostalCode,Headquarters,Active
														,RetailLocationLatitude,RetailLocationLongitude,RetailLocationImagePath,WebsiteSourceFlag,RetailerImagePath
														, SaleFlag = 0 ,HL.HchubcitiID into #tempre
											FROM Retailer R 

											INNER JOIN #RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
											 AND RBC.BusinessCategoryID = @BusinessCategoryID
											 INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID  
											--INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
											INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode  AND HL.HcHubCitiID=@HCHubCitiID1 
											INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
											INNER JOIN #CityList CL ON  CL.CityID =HL.HcCityID 
											--and RL.HcCityID=HL.HcCityID 

                                          
                    CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                            , RetailID INT 
                                            , RetailName VARCHAR(1000)
                                            , RetailLocationID INT
                                            , Address1 VARCHAR(1000)
                                            , City VARCHAR(1000)    
                                            , State CHAR(2)
                                            , PostalCode VARCHAR(20)
                                            , retLatitude FLOAT
                                            , retLongitude FLOAT
                                            , Distance FLOAT  
                                            , DistanceActual FLOAT                                                  
                                            , SaleFlag BIT)   

                      INSERT INTO #Retail(RetailID   
											, RetailName  
											, RetailLocationID  
											, Address1  
											, City  
											, State  
											, PostalCode  
											, retLatitude  
											, retLongitude  
											, Distance 
											, DistanceActual                                                 
											, SaleFlag  )
                                SELECT  RetailID    
                                            , RetailName
                                            , RetailLocationID
                                            , Address1
                                            , City    
                                            , State
                                            , PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , Distance    
                                            , DistanceActual                                             
                                            , SaleFlag
									FROM 
									(SELECT DISTINCT RL.RetailID     
                                                    , RL.RetailName
                                                    , RL.RetailLocationID 
                                                    , RL.Address1     
                                                    , RL.City
                                                    , RL.State
                                                    , RL.PostalCode
                                                    , RL.RetailLocationLatitude  retLatitude
                                                    , RL.RetailLocationLongitude retLongitude
													, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                    , DistanceActual = 0
                                                    --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                    , SaleFlag = 0   
									FROM #tempre RL 
									--INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
									--INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
									--INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
									--INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HcHubCitiID1 AND RL.HcCityID = HL.HcCityID
									--INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
									--INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID ) OR (@RegionAppFlag =0 AND CL.CityID=0)
									INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RL.HchubcitiID  AND Associated =1 
                                    LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                    LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                    LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                    LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
                                    LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID 
									WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
                                    AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
									) Retailer
                                    WHERE Distance <= @Radius                             
                         
                           SELECT DISTINCT Retailid , RetailLocationid
                           INTO #RetailItemsonSale1
                           FROM 
								(SELECT DISTINCT R.RetailID, a.RetailLocationID 
								FROM RetailLocationDeal a 
								INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
								INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
									AND a.ProductID = c.ProductID
									AND GETDATE() BETWEEN ISNULL(a.SaleStartDate, @Yesterday) AND ISNULL(a.SaleEndDate, @Tomorrow)
								UNION ALL 
								SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
								FROM Coupon C 
								INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
								INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
								LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
								WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
								GROUP BY C.CouponID
										,NoOfCouponsToIssue
										,CR.RetailID
										,CR.RetailLocationID
								HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
													ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
								UNION ALL  
								SELECT DISTINCT  RR.RetailID, 0 as RetaillocationID  
								from Rebate R 
								INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
								INNER JOIN #Retail RE ON RE.RetailLocationID =RR.RetailLocationID
								WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

								UNION ALL  
								SELECT DISTINCT R.retailid, a.RetailLocationID 
								FROM  LoyaltyDeal a
								INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
								INNER JOIN #Retail R ON R.RetailLocationID =a.RetailLocationID
								INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID AND b.ProductID = LDP.ProductID 
								WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)
                                  
								UNION ALL 
								SELECT DISTINCT R.RetailID, R.RetailLocationID
								FROM ProductHotDeal p
								INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
								INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
								LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
								LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
								WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
								GROUP BY P.ProductHotDealID
											,NoOfHotDealsToIssue
											,R.RetailID
											,R.RetailLocationID
								HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
													ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  
								UNION ALL 
								SELECT q.RetailID, qa.RetailLocationID
								FROM QRRetailerCustomPage q
								INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
								INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
								INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
								where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
								) Discount 
                    
                            SELECT DISTINCT Row_Num = RowNum
                                            , r.RetailID    
                                            , RetailName
                                            , rl.RetailLocationID
                                            , r.Address1
                                            , r.City    
                                            , r.State
                                            , r.PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , Distance           
                                            , DistanceActual                                       
                                            , SaleFlag= CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
							INTO #Retailer    
							FROM #Retail R
							INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
							LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
							WHERE RL.Active = 1
							ORDER BY RowNum ASC
                                                                          
                        --To display List of Interests
						
						SELECT DISTINCT  F.HcFilterID AS filterId 
										, FilterName AS filterName                   
						FROM #Retailer R
						INNER JOIN HcFilterRetailLocation RF ON R.RetailLocationID = RF.RetailLocationID
						INNER JOIN HcFilter F ON RF.HcFilterID = F.HcFilterID
						WHERE F.HcHubCitiID = @HcHubCitiID1
						ORDER BY FilterName
                                           
                 --Confirmation of Success.
                 SELECT @Status = 0      
            
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
             IF @@ERROR <> 0    
                BEGIN    
                PRINT 'Error occured in Stored Procedure [usp_HcSingleFindOptionsInterestsListDisplay].'      
                --- Execute retrieval of Error info.  
                EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
                --Confirmation of failure.
                SELECT @Status = 1
                END;    
            
      END CATCH;
END;


























GO
