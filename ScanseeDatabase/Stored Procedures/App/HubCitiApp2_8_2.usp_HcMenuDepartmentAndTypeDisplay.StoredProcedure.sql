USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcMenuDepartmentAndTypeDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcMenuDepartmentAndTypeDisplay
Purpose					: usp_HcMenuDepartmentAndTypeDisplay.
Example					: usp_HcMenuDepartmentAndTypeDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd Dec 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcMenuDepartmentAndTypeDisplay]
(
	  @MenuID int
	, @HcHubCitiID int
	, @FilterName Varchar(100)
	
	--Output Variable 		
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
			SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
			      ,MI.HcDepartmentID 
				  ,D.HcDepartmentName 
			INTO #Dep
			FROM HcMenuItem MI	
			INNER JOIN HcMenu M ON MI.HcMenuID = M.HcMenuID	
			LEFT JOIN HcDepartments D ON MI.HcDepartmentID = D.HcDepartmentID
			WHERE M.HcHubCitiID = @HcHubCitiID AND M.HcMenuID = @MenuID
			AND  (MI.HcDepartmentID IS NOT NULL
			OR D.HcDepartmentName IS NOT NULL)
			ORDER BY HcDepartmentName
			
			
			SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
			      ,MI.HcMenuItemTypeID 
				  ,D.HcMenuItemTypeName 
			INTO #Type
			FROM HcMenuItem MI	
			INNER JOIN HcMenu M ON MI.HcMenuID = M.HcMenuID	
			LEFT JOIN HcMenuItemType D ON MI.HcMenuItemTypeID = D.HcMenuItemTypeID
			WHERE M.HcHubCitiID = @HcHubCitiID AND M.HcMenuID = @MenuID
			AND  (MI.HcMenuItemTypeID IS NOT NULL
				  OR D.HcMenuItemTypeName IS NOT NULL)
			ORDER BY HcMenuItemTypeName	  
			

			IF  (@FilterName = 'dept')
			BEGIN
				SELECT HcDepartmentID AS  departmentId
					, HcDepartmentName AS departmentName
				FROM #Dep
				ORDER BY HcDepartmentName
			END

			ELSE IF  (@FilterName = 'Type')
			BEGIN
				SELECT HcMenuItemTypeID  AS mItemTypeId
					, HcMenuItemTypeName AS mItemTypeName
				FROM #Type
				ORDER BY HcMenuItemTypeName
			END

			--SELECT departmentId
			--     , departmentName
			--     , mItemTypeId
			--     , mItemTypeName
			--FROM #Dep D
			--FULL OUTER JOIN #Type T ON D.RowNum = T.RowNum
			--ORDER BY departmentName ASC, mItemTypeName ASC

		
		--Confirmation of Success.
		SELECT @Status = 0
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcMenuDepartmentAndTypeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
