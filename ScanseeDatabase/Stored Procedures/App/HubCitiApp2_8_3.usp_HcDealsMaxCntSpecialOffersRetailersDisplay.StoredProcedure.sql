USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcDealsMaxCntSpecialOffersRetailersDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcRetailerLocationSpecialOffersDisplay
Purpose					: To List the Special Offers (Special Offer Page) for the given Retailer Location.
Example					: usp_HcRetailerLocationSpecialOffersDisplay

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			4/30/2015      Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcDealsMaxCntSpecialOffersRetailersDisplay]
(
	  
	  @UserID int
    , @HcHubCitiID Int
	, @Latitude Decimal(18,6)
	, @Longitude Decimal(18,6)
	, @PostalCode varchar(20)
   
	--Output Variable 
	, @MaxCnt int output

)
AS
BEGIN

	BEGIN TRY
				
				DECLARE @UserIDs int 
				SELECT @UserIDs = @UserID
				DECLARE @HcHubCitiIDs Int
				SELECT @HcHubCitiIDs = @HcHubCitiID
				DECLARE @Latitudes Decimal(18,6)
				SELECT @Latitudes = @Latitude
				DECLARE @Longitudes Decimal(18,6)
				SELECT @Longitudes = @Longitude
				DECLARE  @PostalCodes varchar(20)
				SELECT @PostalCodes = @PostalCode

				DECLARE @QRType INT
				DECLARE @QRTypeCode INT			      
				DECLARE @Radius INT
				DECLARE @DistanceFromUser FLOAT
				DECLARE @UserLatitude float
				DECLARE @UserLongitude float 
				DECLARE @UserOutOfRange bit 
				DECLARE @DefaultPostalCode varchar(50)   
				DECLARE @ErrorNumber int 
				DECLARE @ErrorMessage varchar(1000)
				DECLARE @Status int 

				SELECT @UserLatitude = @Latitudes
					 , @UserLongitude = @Longitudes

				IF (@UserLatitude IS NULL) 
				BEGIN
					SELECT @UserLatitude = Latitude
						 , @UserLongitude = Longitude
					FROM HcUser A
					INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
					WHERE HcUserID = @UserIDs 
				END

				--Capture the ID for Special Offers.
				SELECT @QRType = QRTypeID 
					 , @QRTypeCode = QRTypeCode
				FROM QRTypes WHERE QRTypeName LIKE 'Special Offer Page'	

				EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @UserIDs, @HcHubCitiIDs, @Latitudes, @Longitudes, @PostalCodes, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
				SELECT @PostalCodes = ISNULL(@DefaultPostalCode, @PostalCodes) 

				IF (@Latitudes IS NULL AND @Longitudes IS NULL) OR (@UserOutOfRange = 1)
				BEGIN
					IF @PostalCodes IS NOT NULL
					BEGIN
						SELECT @Latitudes = G.Latitude
							 , @Longitudes = G.Longitude
						FROM GeoPosition G
						WHERE PostalCode = @PostalCodes
					END
					ELSE
					BEGIN
						SELECT @Latitudes = G.Latitude
							 , @Longitudes = G.Longitude
						FROM GeoPosition G
						INNER JOIN HCUSER H ON H.PostalCode = G.PostalCode
						WHERE H.HcUserID = @UserIDs
					END
				END
				
				--DECLARE @LocCategory Int		
				--SELECT DISTINCT BusinessCategoryID 
				--INTO #UserLocCategories
				--FROM HcUserPreferredCategory 
				--WHERE HcUserID=@UserIDs AND HcHubCitiID =@HcHubCitiIDs 

				--SELECT @LocCategory=COUNT(1) FROM #UserLocCategories 

				SELECT @Radius = ScreenContent
				FROM AppConfiguration
				WHERE ScreenName = 'DefaultRadius'

				SELECT RowNum = IDENTITY (int,1,1)
					 , RetailID
					 , RetailName
					 , RetailLocationID
					 , Address1
					 , City
					 , State
					 , PostalCode
				INTO #SpecialOffers
				FROM  
				(
					SELECT DISTINCT  R.RetailID
								  , R.RetailName
								  , RL.RetailLocationID
								  , RL.Address1
								  , RL.City
								  , RL.State
								  , RL.PostalCode
					FROM QRRetailerCustomPage QR
					INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QR.QRRetailerCustomPageID
					INNER JOIN RetailLocation RL ON RL.RetailLocationID=QRRCPA.RetailLocationID AND RL.Active = 1
					INNER JOIN Retailer R ON R.RetailID = QRRCPA.RetailID AND R.RetailerActive = 1
					INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
					INNER JOIN HcLocationAssociation HL ON HL.Postalcode=RL.Postalcode AND HL.HcHubCitiID = @HcHubCitiIDs
					INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiIDs AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
					LEFT JOIN RetailerBusinessCategory RB ON RB.RetailerID =QRRCPA.RetailID 
					--LEFT JOIN HcUserPreferredCategory US ON US.BusinessCategoryID =RB.BusinessCategoryID 
					LEFT JOIN HcUserPreferredCategory UP ON UP.HcUserID=@UserIDs --AND UP.HcHubCitiID =@HcHubCitiIDs  
					WHERE QRTypeID = @QRType AND RL.Active = 1 
					AND (GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1))
					--AND (( /*UP.HcUserPreferredCategoryID IS NOT NULL AND */ UP.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID=QR.RetailID) OR UP.HcUserPreferredCategoryID IS NULL)
					--AND ((StartDate IS NULL AND EndDate IS NULL) OR (GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)))
					--AND ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitudes / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitudes / 57.2958) * COS((@Longitudes / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius
					--AND (@LocCategory=0 OR (US.BusinessCategoryID IS NOT NULL AND US.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID =QRRCPA.RetailID))
				)A
		

				SELECT RowNum
				     , S.RetailID
					 , S.RetailName
					 , S.RetailLocationID
					 , S.Address1
					 , S.City
					 , S.State
					 , S.PostalCode
				INTO #Specials
				FROM #SpecialOffers S
				INNER JOIN RetailLocation RL ON S.RetailLocationID=rl.RetailLocationID AND RL.Active = 1
				INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
				WHERE ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitudes / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitudes / 57.2958) * COS((@Longitudes / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius					

				--To capture max row number.        
				SELECT @MaxCnt = MAX(RowNum) FROM #Specials
						
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
		END;
		 
	END CATCH;
END;





















GO
