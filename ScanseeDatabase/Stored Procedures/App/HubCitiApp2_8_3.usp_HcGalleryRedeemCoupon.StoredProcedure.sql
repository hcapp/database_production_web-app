USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcGalleryRedeemCoupon]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcGalleryRedeemCoupon
Purpose					: To add Coupon to User Gallery And making used flag as 1 when they tap on redeem button.
Example					: usp_HcGalleryRedeemCoupon 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		  17thDec2013 	Dhananjaya TR	  Initial Version
2.0       12/27/2016     Shilpashree      Changed input variables
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcGalleryRedeemCoupon]
(
      @CouponID int
	, @HcUserID int
	
	--User Tracking
    , @CouponListID int	
 
	--Output Variable 
	, @UsedFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			IF EXISTS(SELECT 1 FROM HCUserCouponGallery WHERE CouponID = @CouponID AND HcUserID=@HcUserID AND UsedFlag=1)
			BEGIN
				SET @UsedFlag=1
			END
			
			ELSE
			BEGIN
				SET @UsedFlag=0

				IF  EXISTS (SELECT 1 FROM HcUserCouponGallery WHERE CouponID = @CouponID AND HcUserID=@HcUserID AND UsedFlag=0)
				BEGIN
					UPDATE HcUserCouponGallery
					SET UsedFlag=1
					Where HcUserID=@HcUserID AND CouponID=@CouponID
				END
				
				IF NOT EXISTS (SELECT 1 FROM HcUserCouponGallery WHERE CouponID = @CouponID AND HcUserID=@HcUserID)
				BEGIN
					INSERT INTO [HCUserCouponGallery](CouponID
													   ,HcUserID
													   ,Claim
													   ,ClaimDate
													   ,UsedFlag
													   ,RedeemDate
													   ,DateCreated
													   )
												VALUES (@CouponID
													   ,@HcUserID
													   ,1
													   ,GETDATE()
													   ,1	
													   ,GETDATE()
													   ,GETDATE()
													   )
				END
			END
			
			--User Tracking
			UPDATE HubCitiReportingDatabase..CouponsList SET CouponUsed =1
			WHERE CouponsListID = @CouponListID 
			
			--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcGalleryRedeemCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










































GO
