USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubRegionGetMainMenu_backupbefopt]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcHubRegionGetMainMenu]
Purpose					: To disply the list of the Main Menu Button Details associated to the HubRegion.
Example					: [usp_HcHubRegionGetMainMenu]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			01/22/2016	    Sagar Byali			 1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubRegionGetMainMenu_backupbefopt]

(
    --Input variable.
      @LinkID Int
    , @HubCitiID Varchar(100)
    , @LevelID Int      
    , @SortOrder Varchar(100)  
    , @TypeID int
    , @DepartmentID int
	, @HcCityID varchar(max)
	, @DateCheck datetime
	, @DeviceType varchar(100) --if input is Ipad then only send ipad image values
    
    ----User Tracking Inputs     
    --, @MainMenuID Int	 
	
	 
	--Output Variable 
	, @NoRecordsMsg nvarchar(max) output
	, @FilterID int output
	, @FilterName varchar(255) output
	, @FilterCount int output
	, @DownLoadLinkIOS Varchar(1000) output
	, @DownLoadLinkAndroid Varchar(1000) output
	, @RetailGroupButtonImagePath varchar(1000) output
	, @AppIconImagePath varchar(1000) output
	, @HCMenuBannerImage Varchar(1000) output
	, @HcDepartmentFlag bit output
	, @HcTypeFlag bit output
	, @NoOfColumns int output
	, @MenuName varchar(255) output
	, @IsRegionApp bit output
	, @TemplateChanged bit output
	, @TempleteBackgroundImage VARCHAR(1000) OUTPUT
	, @DisplayLabel BIT OUTPUT
	, @LabelBckGndColor Varchar(250) output
	, @LabelFontColor Varchar(250) output
	, @homeImgPath varchar(250) output
	, @bkImgPath varchar(250) output
	, @titleBkGrdColor varchar(250) output
	, @titleTxtColor varchar(250) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN


	BEGIN TRY


	 IF EXISTS(SELECT 1 FROM HcMenu H
			INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
			WHERE HM.DateCreated = CONVERT(date,@DateCheck) AND HcHubCitiID = @HubCitiID) AND @LinkID = 0
	 BEGIN
		SELECT @TemplateChanged = 0, @Status = 0
	 END

     ELSE IF EXISTS(SELECT 1 FROM HcMenu H
			   INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
			   WHERE HM.DateCreated = CONVERT(date,@DateCheck) AND HcHubCitiID = @HubCitiID) AND @LinkID <> 0
		BEGIN
			SELECT @TemplateChanged = 0, @Status = 0
		END

	  ELSE
	 
		BEGIN	
			
			DECLARE @Template Varchar(100)
			DECLARE @CityConfig VARCHAR(1000)
			DECLARE @CityExpDefaultConfig VARCHAR(1000)			
			DECLARE @GroupedTabTextColor VARCHAR(100)
			DECLARE @GroupedTabTextFontColor VARCHAR(100)
			DECLARE @Config VARCHAR(500)
			DECLARE @HcAppListName VARCHAR(10)
			DECLARE @RegionAppID int
			DECLARE @HcAppListID int

			SET @LinkID =IIF(@LinkID IS NULL OR @LinkID=0 ,Null,@LinkID)
			SET @TypeID =IIF(@TypeID IS NULL OR @typeID=0,Null,@TypeID)
			SET @DepartmentID =IIF(@DepartmentID IS NULL OR @DepartmentID=0,Null,@DepartmentID)		


			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiID = @HubCitiID


			DECLARE @HcLinkTypeIDS VARCHAR(100)
			SELECT @HcLinkTypeIDS = COALESCE(@HcLinkTypeIDS+',','') + CAST(HcLinkTypeID AS VARCHAR(100))
			FROM HcLinkType WHERE LinkTypeName IN ('Text','Label')


			SELECT @MenuName = MenuName
			FROM HcMenu
			WHERE HcMenuID = @LinkID AND ISNULL(@LinkID, 0) <> 0
			OR ISNULL(@LinkID, 0) = 0 AND Level = 1
			
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT @HcAppListName = (SELECT HcAppListName 
								     FROM HcHubCiti H 
								     INNER JOIN HcAppList A ON H.HcAppListID = A.HcAppListID	
								     WHERE HcHubCitiID=@HubCitiID)

			SELECT @IsRegionApp = IIF(@HcAppListName = 'RegionApp',1,0)
			
			IF @DepartmentID =0
			BEGIN

				SET @DepartmentID = NULL
			END

			IF @TypeID =0
			BEGIN

				SET @TypeID = NULL
			END
			 
			--To get the Filter Details and Count of filters for a given hub Citi.
			SELECT @FilterID = F.HcFilterID
				 , @FilterName = F.FilterName
			FROM HcCityExperience H
			INNER JOIN HcFilter F ON H.HcCityExperienceID = F.HcCityExperienceID AND H.HcHubCitiID = @HubCitiID
			
			
			SELECT @FilterCount = COUNT(1)
			FROM HcFilter 
			WHERE HcHubCitiID = @HubCitiID
           
		   --------------------------------------------------------
			SELECT DISTINCT HM.HcTemplateID
			INTO #Templates
			FROM HcMenuItem MI
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			INNER JOIN HcTemplate T ON HM.HcTemplateID = T.HcTemplateID
			WHERE HM.HCHubcitiID=@HubCitiID AND	(@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID ))
			AND T.TemplateName IN ('Combo Template','Grouped Tab','Grouped Tab With Image') 
			    
		   CREATE TABLE #Groups(HcMenuItemID int)
		   IF EXISTS (SELECT 1 FROM #Templates)
		   BEGIN
		   
			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc) 
			      ,H.HcMenuItemID				  
			INTO #group1
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE  (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID 	
				  , MenuItemName			 
			INTO #group2
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group3
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where  (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group4
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			where (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 
			
            --Select menuitems based on the Department and Type values
			SELECT H.HcMenuItemID 
				  , H.MenuItemName
			INTO #menuitem
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID
			WHERE ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND H.HcMenuItemTypeID =@TypeID )) 
						AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND H.HcDepartmentID  =@DepartmentID))))
						
			ORDER BY H.HcMenuItemID
	
			Declare @maxGroup Int
			DECLARE @minGroup Int

			--Select maximum menuitemid in the selected group template
			SELECT @maxGroup = MAX(HcMenuItemID) From #group1
			SELECT @minGroup = Min(HcMenuItemID) From #group1
		
			--Here based on the temporary table data, selecting menuitemid under a group and storing in temp table
			
			INSERT INTO #Groups
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )	--here check the menuitem is belong to first group			
			OR
			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID 
								AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID) 
			OR 
			(@maxGroup <M.HcMenuItemID AND @maxGroup =A.HcMenuItemID)
			


			SELECT DISTINCT A.HcMenuItemID
			              
			              ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
			INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )				
			OR
			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID 
							AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID)
			OR 
			(@maxGroup <M.HcMenuItemID AND @maxGroup =A.HcMenuItemID)


		  

			SELECT DISTINCT M.GroupIDs
			      ,MenuitemsID
				  ,M.MenuItemName GroupName
			INTO #Sort
			FROM
			(SELECT DISTINCT HcMenuItemID GroupIDs
			                ,menuitemid MenuitemsID
			FROM #SortOrders
			UNION ALL
			SELECT DISTINCT HcMenuItemID GroupIDs
			               ,HcMenuItemID MenuitemsID
			FROM #SortOrders)A
			INNER JOIN HcMenuItem M ON M.HcMenuItemID =A.GroupIDs
			ORDER BY MenuitemsID,M.GroupIDs Asc

			
			END
			
			--For the Grouped tab template send the button & the font colors from the configuration table.
			SELECT @GroupedTabTextColor = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti Grouped Tab Text Color'
			AND ScreenName = 'HubCiti Grouped Tab Text Color'
			
			
			SELECT @GroupedTabTextFontColor = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti Grouped Tab Text Font Color'
			AND ScreenName = 'HubCiti Grouped Tab Text Font Color'

			SELECT @CityConfig=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
		
			SELECT @CityExpDefaultConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'City Experience Default Image Path'
			AND Active = 1
			
			SELECT Param LinkTypeIDs
			INTO #LinkTypeIDs
			FROM fn_SplitParam(@HcLinkTypeIDS,',')

			SELECT @DownLoadLinkIOS=ItunesURL
			FROM HcHubCiti HC
			WHERE HC.HcHubCitiID=@HubCitiID  
						
			
			SELECT @DownLoadLinkAndroid=GooglePlayURL
			FROM HcHubCiti HC
			WHERE HC.HcHubCitiID=@HubCitiID 
			
			SELECT @RetailGroupButtonImagePath = @CityConfig + ISNULL(R.ButtonImagePath, @CityExpDefaultConfig)
			FROM RetailGroup R
			INNER JOIN HcCityExperience C ON C.HcCityExperienceID=R.RetailGroupID AND HcHubCitiID =@HubCitiID 
			
			SELECT @RetailGroupButtonImagePath = ISNULL(@RetailGroupButtonImagePath, @CityConfig + @CityExpDefaultConfig)
			
			--Send AppIcon
			SELECT @AppIconImagePath = @Config + CAST(HcHubCitiID AS VARCHAR(10)) + '/' + AppIcon
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
			
			SELECT DISTINCT @HCMenuBannerImage= IIF(HCMenuBannerImage IS NOT NULL AND HCMenuBannerImage <> '',@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HCMenuBannerImage,null )
				  ,@HcDepartmentFlag = HcDepartmentFlag
				  ,@HcTypeFlag = HcTypeFlag	
				  ,@NoOfColumns = NoOfColumns
			FROM HcMenuItem MI
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			INNER JOIN HCTemplate HCT on HCT.HCTemplateID = HM.HCTemplateID
			INNER JOIN HCLinkType N ON N.HcLinkTypeID = MI.HcLinkTypeID
			WHERE HM.HCHubcitiID=@HubCitiID AND --(MI.HcMenuID=ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 AND Level=1))   
			(@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID ))   

			
			CREATE TABLE #MenuItems(RowNum INT IDENTITY(1,1)
									,MenuID INT
									,HubCitiId INT
									,templateName VARCHAR(250)
									,Level INT
									,mItemID INT
									,mItemName VARCHAR(250)
									,LinkTypeName VARCHAR(250)
									,LinkTypeID INT
									,LinkID INT
									,Position INT
									,mItemImg VARCHAR(1000)
									,mBkgrdColor VARCHAR(100)
									,mBkgrdImage VARCHAR(1000)
									,mBtnColor VARCHAR(100)
									,mBtnFontColor VARCHAR(100)
									,smBkgrdColor VARCHAR(100)
									,smBkgrdImage VARCHAR(1000)
									,smBtnColor VARCHAR(100)
									,smBtnFontColor VARCHAR(100)
									,HcDepartmentID INT
									,HcMenuItemTypeID INT			
									,departmentName VARCHAR(250)
									,mItemTypeName VARCHAR(250)
									,HcMenuItemShapeID INT
									,HcMenuItemShape  VARCHAR(100)
									,mGrpBkgrdColor VARCHAR(100)
									,mGrpFntColor VARCHAR(100)
									,smGrpBkgrdColor VARCHAR(100)
									,smGrpFntColor VARCHAR(100)
									,MenuName VARCHAR(250)
									,mFontColor VARCHAR(100)
									,smFontColor VARCHAR(100)
									,dateModified datetime
									,TemplateBackgroundColor VARCHAR(100)
									--,NoOfColumns INT
									)

			SELECT DISTINCT HcMenuItemID
						,MI.HcMenuID
						,MenuItemName
						,HcLinkTypeID
						,LinkID
						,Position						
						,HcMenuItemImagePath
						,HcMenuItemIpadImagePath
						,HcDepartmentID
						,HcMenuItemTypeID
						,HcMenuItemShapeID
						,MI.DateCreated
						,Mi.DateModified
						,MI.CreatedUserID
						,MI.ModifiedUserID
						,MenuTypeID
						,HcHubCitiID
						,HcTemplateID
						,Level
						,Hm.DateCreated MenuCreatedDate
						,MenuName
						,HCMenuBannerImage
						,HcDepartmentFlag
						,HcTypeFlag
						,NoOfColumns
						,TemplateBackgroundColor
				INTO #Menuitemss
				FROM HcMenuItem MI
				INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID AND HM.HcHubCitiID = @HubCitiID 
				AND ((@LinkID IS NULL AND Level =1) OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID))
			
			
		  IF (@RegionAppID = 1 AND (@HcCityID IS NOT NULL) )	
		  BEGIN
				INSERT INTO #MenuItems (MenuID
										,HubCitiId
										,templateName
										,Level
										,mItemID
										,mItemName
										,LinkTypeName
										,LinkTypeID
										,LinkID
										,Position
										,mItemImg
										,mBkgrdColor
										,mBkgrdImage
										,mBtnColor
										,mBtnFontColor
										,smBkgrdColor
										,smBkgrdImage
										,smBtnColor
										,smBtnFontColor
										,HcDepartmentID
										,HcMenuItemTypeID			
										,departmentName
										,mItemTypeName
										,HcMenuItemShapeID 
										,HcMenuItemShape 
										,mGrpBkgrdColor
										,mGrpFntColor
										,smGrpBkgrdColor
										,smGrpFntColor
										,MenuName
										,mFontColor
										,smFontColor
										,dateModified
										,TemplateBackgroundColor
										--,NoOfColumns
										)
			          SELECT MenuID
							,HubCitiId
							,templateName
							,Level
							,mItemID
							,mItemName
							,LinkTypeName
							,LinkTypeID
							,LinkID
							,Position
							,mItemImg
							,mBkgrdColor
							,mBkgrdImage
							,mBtnColor
							,mBtnFontColor
							,smBkgrdColor
							,smBkgrdImage
							,smBtnColor
							,smBtnFontColor
							,HcDepartmentID
							,HcMenuItemTypeID			
							,departmentName
							,mItemTypeName
							,HcMenuItemShapeID 
							,HcMenuItemShape 
							,mGrpBkgrdColor
							,mGrpFntColor
							,smGrpBkgrdColor
							,smGrpFntColor
							,MenuName
							,mFontColor
							,smFontColor
							,dateModified
							,TemplateBackgroundColor
							--,NoOfColumns
					FROM
					(SELECT DISTINCT HM.HcMenuID as MenuID			
						  ,HM.HCHubCitiID as HubCitiId
						  ,HCT.TemplateName as templateName
						  ,HM.Level
						  ,MI.HcMenuItemID as mItemID
						  ,MenuItemName	as mItemName	
						  ,LinkTypeName,N.HcLinkTypeID as LinkTypeID,LinkID	 		      
						  ,Position	


						     ,mItemImg= CASE WHEN @DeviceType = 'Ipad' AND (HCT.TemplateName = '4X4 Grid' OR HCT.TemplateName = 'Rectangular Grid') 
								  THEN @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(MI.HcMenuItemIpadImagePath,MI.HcMenuItemImagePath)
								  WHEN @DeviceType = 'Ipad' AND (HCT.TemplateName != '4X4 Grid' OR HCT.TemplateName<> 'Rectangular Grid') 
								  THEN @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath
								  ELSE @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+MI.HcMenuItemImagePath 
							END
						  --,mItemImg = IIF(@DeviceType = 'Ipad',@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(HcMenuItemIpadImagePath,HcMenuItemImagePath), @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath)
						 --,mItemImg = @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath
						  ,mBkgrdColor = IIF(HM.Level = 1, MenuBackgroundColor, NULL)
						  ,mBkgrdImage = IIF(HM.Level = 1, @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+MenuBackgroundImage, NULL)
						  ,mBtnColor = IIF(HM.Level = 1, MenuButtonColor, NULL)
						  ,mBtnFontColor = IIF(HM.Level = 1, MenuButtonFontColor, NULL)
						  ,smBkgrdColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuBackgroundColor, NULL)
						  ,smBkgrdImage = IIF(ISNULL(HM.Level, 0) <> 1,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+SubMenuBackgroundImage, NULL)				  
						  ,smBtnColor = IIF(ISNULL(HM.Level, 0) <> 1, CASE WHEN N.LinkTypeName = 'Text' OR N.LinkTypeName = 'Label' THEN @GroupedTabTextColor ELSE SubMenuButtonColor END, NULL)
						  ,smBtnFontColor = IIF(ISNULL(HM.Level, 0) <> 1, CASE WHEN N.LinkTypeName = 'Text' OR N.LinkTypeName = 'Label' THEN @GroupedTabTextFontColor ELSE SubMenuButtonFontColor END, NULL) 
						  ,MI.HcDepartmentID
						  ,MI.HcMenuItemTypeID			
						  ,D.HcDepartmentName departmentName
						  ,T.HcMenuItemTypeName mItemTypeName
						  ,S.HcMenuItemShapeID 
						  ,S.HcMenuItemShape 
						  ,mGrpBkgrdColor = IIF(HM.Level = 1, MenuGroupBackgroundColor, NULL)
						  ,mGrpFntColor = IIF(HM.Level = 1, MenuGroupFontColor, NULL)
						  ,smGrpBkgrdColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuGroupBackgroundColor, NULL)
						  ,smGrpFntColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuGroupFontColor, NULL)
						  ,HM.MenuName
						  ,MenuIconicFontColor mFontColor
						  ,SubMenuIconicFontColor smFontColor
						  ,IIF(@LinkID = 0,HM.DateCreated,MI.DateCreated) dateModified
						  ,TemplateBackgroundColor
						  --,NoOfColumns
					FROM HcMenuItem MI  
					INNER JOIN HcRegionAppMenuItemCityAssociation RMC ON MI.HcMenuItemID = RMC.HcMenuItemID AND RMC.HCHubcitiID=@HubCitiID 
					LEFT JOIN HcCity C ON RMC.HcCityID = C.HcCityID		
					INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
					INNER JOIN HCTemplate HCT on HCT.HCTemplateID = HM.HCTemplateID
					INNER JOIN HCLinkType N ON N.HcLinkTypeID = MI.HcLinkTypeID
					LEFT JOIN HcMenuCustomUI CI ON HM.HcHubCitiID = CI.HubCitiId
					LEFT JOIN HCDepartments D ON D.HcDepartmentID = MI.HcDepartmentID
					LEFT JOIN HcMenuItemType T ON T.HcMenuItemTypeID = MI.HcMenuItemTypeID
					LEFT JOIN HcMenuItemShape S ON S.HcMenuItemShapeID =MI.HcMenuItemShapeID
					LEFT JOIN #Groups G ON G.HcMenuItemID =MI.HcMenuItemID  
					WHERE (C.HcCityID IN (SELECT Param FROM fn_SplitParam(@HcCityID,',')) 
									OR (RMC.HcCityID IS NULL AND MI.HcLinkTypeID IN (SELECT PARAM FROM fn_SplitParam(@HcLinkTypeIDS,','))) )
					AND (MI.HcMenuID=ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 AND Level=1)) 
					AND ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND MI.HcMenuItemTypeID =@TypeID ))
						AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND MI.HcDepartmentID  =@DepartmentID))) OR G.HcMenuItemID =MI.HcMenuItemID)
					)C
			END
			
		
			
			SELECT @TemplateChanged = 1	
			
			--Confirmation of Success
			SELECT @Status = 0
			
			 --To display message when no Retailers display for user preferred cities.
				 DECLARE @MaxCnt int
				 DECLARE @UserPrefCities NVarchar(MAX)

				 SELECT @MaxCnt = Count(1) FROM #MenuItems

				 DECLARE @Count INT
				 SET @Count=(SELECT Count(LinkTypeName) from #MenuItems
				             WHERE LinkTypeName = 'Label' OR LinkTypeName ='Text')

				
				 IF (@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0 OR (@Count=@MaxCnt)) AND (@HcCityID IS NOT NULL )
				 BEGIN 

				 
					SELECT Param LinkTypeIDs
					INTO #CityIDs
					FROM fn_SplitParam(@HcCityID,',')
	                  
					  select CityName into #CityIDsess From #CityIDs I
					  inner join HcCity c on i.LinkTypeIDs=HcCityID

					 DECLARE  @List varchar(1000)
					SELECT @List = COALESCE(@List + ';', '') + 
					CAST(CityName AS nvarchar(max))
					FROM #CityIDsess

						


						SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @List +
												'.\n\nUpdate your city preferences in the settings menu.'

				 END
				 --ELSE IF ((@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0)  AND (@HcCityID IS NOT NULL OR @TypeID IS NOT NULL OR @DepartmentID IS NOT NULL ))
				 --BEGIN

				 --select 'aa'


					--	SELECT @NoRecordsMsg = 'No Records Found.'
				 --END

				 --ELSE IF (@RegionAppID = 0) AND (ISNULL(@MaxCnt,0) = 0)
				 --BEGIN
					--	SELECT @NoRecordsMsg = 'No Records Found.'
				 --END



				 
	

		--select  *From #MenuItems
	

		
			DECLARE @TemplateName Varchar(100)
			SELECT @TemplateName=templateName  FROM #MenuItems 
			

			IF @SortOrder ='ASC' AND (@TemplateName IN ('Grouped Tab','Combo Template','Grouped Tab With Image'))
			BEGIN	


				
				 SELECT RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName

					  ,LinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
													  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
													  WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID ) = 1 AND M.LinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																						   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																						   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																						   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
												WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
													  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							   INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID
																																							   WHERE HcMenuItemID = M.mItemID AND HcHubCitiID =@HubCitiID)
										   ELSE LinkTypeName END)
						,LinkTypeID
						,LinkID =  CASE WHEN M.LinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID) 
										  WHEN M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																			 INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
																			 WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 AND M.LinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
																																							INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
																																							WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
										  WHEN M.LinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
																			  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN (SELECT HcEventCategoryID FROM HcMenuItemEventCategoryAssociation
																																							WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID)	

									 ELSE LinkID END	
					  ,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,	GroupName 	
					  , SortD = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='DESC' THEN 'Z'+ mItemName ELSE mItemName END
					  , SortA = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='ASC' THEN ' ' ELSE mItemName END
					  , mGrpBkgrdColor 
					  , mGrpFntColor                        
					  , smGrpBkgrdColor 
					  , smGrpFntColor 
					  , MenuName
					  , mFontColor
					  , smFontColor
					  , dateModified	
					  , TemplateBackgroundColor AS templateBgColor 			                
		        FROM #MenuItems M
				INNER JOIN #Sort S ON M.mItemID=S.MenuitemsID 
				WHERE @Count!=@MaxCnt 
			    ORDER BY GroupName ,SortA ASC
						
			END
			
		   ELSE	IF @SortOrder ='DESC' AND (@TemplateName = 'Grouped Tab' OR  @TemplateName = 'Combo Template' OR  @TemplateName = 'Grouped Tab With Image')
			BEGIN	
			
				 SELECT RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName

					  ,LinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
													  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
													  WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID ) = 1 AND M.LinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																						   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																						   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																						   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
												WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
													  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							   INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID
																																							   WHERE HcMenuItemID = M.mItemID AND HcHubCitiID =@HubCitiID)
										   ELSE LinkTypeName END)
						,LinkTypeID
						,LinkID =  CASE WHEN M.LinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID) 
										  WHEN M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																			 INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
																			 WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 AND M.LinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
																																							INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
																																							WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
										  WHEN M.LinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociation 
																			  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN (SELECT HcEventCategoryID FROM HcMenuItemEventCategoryAssociation
																																							WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID)	

									 ELSE LinkID END 
					  ,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,	GroupName 	
					  , SortD = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='DESC' THEN 'Z'+ mItemName ELSE mItemName END
					  , SortA = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') and @SortOrder ='ASC' THEN ' ' ELSE mItemName END		
					  , mGrpBkgrdColor 
					  , mGrpFntColor                        
					  , smGrpBkgrdColor 
					  , smGrpFntColor 
					  , MenuName
					  , mFontColor
					  , smFontColor	
					  , dateModified
					  , TemplateBackgroundColor	AS templateBgColor 	                 
			
		        FROM #MenuItems M
				INNER JOIN #Sort S ON M.mItemID=S.MenuitemsID  
				WHERE @Count!=@MaxCnt
				ORDER BY GroupName desc ,SortD Desc						
			END			    
						 
           ELSE
		   BEGIN
		  			
				 SELECT RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName

					 ,LinkTypeName = IIF((SELECT COUNT(DISTINCT A.BusinessCategoryID)  FROM HcMenuFindRetailerBusinessCategories A 
										INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
										WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 , 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																		   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																		   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																		   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID), 
								     LinkTypeName)
					,LinkTypeID
					,LinkID = IIF((M.LinkTypeName IN ('Filters','City Experience')),
											(SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID),
								IIF( ( M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																	INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
																	WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID) = 1),
										
															(SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
															INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID = A.BusinessCategoryID 
															WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID)
									 ,LinkID)) 	
					  ,Position	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,mGrpBkgrdColor 
					  ,mGrpFntColor                        
					  ,smGrpBkgrdColor 
					  ,smGrpFntColor 
					  ,MenuName
					  ,mFontColor
					  ,smFontColor	
					  ,dateModified
					  ,TemplateBackgroundColor AS templateBgColor 
										                
		        FROM #MenuItems M	
				WHERE @Count!=@MaxCnt			 
				ORDER BY CASE WHEN ISNULL(@SortOrder, 'None') = 'NONE' THEN Position END ASC,
				         CASE WHEN @SortOrder = 'ASC' AND (TemplateName = 'Two Column Tab' OR  TemplateName = 'Two Column Tab with Banner Ad') AND LinkTypeName = 'City Experience' THEN '0' ELSE mItemName END ASC, 
			             CASE WHEN @SortOrder LIKE 'DESC' AND (TemplateName = 'Two Column Tab' OR  TemplateName = 'Two Column Tab with Banner Ad') AND LinkTypeName = 'City Experience' THEN '0' ELSE mItemName END DESC						
			END	
				
				
			
			
			SELECT DISTINCT HM.HcMenuID as MenuID			
				 , BB.HcBottomButtonID as bottomBtnID
				 , BottomButtonName as bottomBtnName
				 , bottomBtnImg = IIF(BottomButtonImage_On IS NOT NULL,@Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On,@Config + HcBottomButtonImageIcon)
				 
				 --CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On ELSE @Config + HcBottomButtonImageIcon END 
				 , bottomBtnImgOff = IIF(BottomButtonImage_Off IS NOT NULL,@Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off , @Config + HcBottomButtonImageIcon_Off)
				 --CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off ELSE @Config + HcBottomButtonImageIcon_Off END 
				 --, BottomButtonLinkTypeName as btnLinkTypeName
				 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
										  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
								                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																					            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																					            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
			                       ELSE BottomButtonLinkTypeName END)
				 , BottomButtonLinkTypeID as btnLinkTypeID				
				 --, btnLinkID = IIF(BT.BottomButtonLinkTypeName = 'Filters', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID),
					--						IIF(BT.BottomButtonLinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
					--																		 WHERE HcBottomButtonID = BB.HcBottomButtonID AND HcHubCitiID = @HubCitiID) = 1,
					--											(SELECT HcEventCategoryID FROM HcBottomButtonEventCategoryAssociation
					--											 WHERE HcBottomButtonID = BB.HcBottomButtonID AND HcHubCitiID = @HubCitiID), BottomButtonLinkID))

				 , btnLinkID = (CASE WHEN BT.BottomButtonLinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID)
									 WHEN BT.BottomButtonLinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
								                                     INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
								                                     WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID) = 1 AND BT.BottomButtonLinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcBottomButtonFindRetailerBusinessCategories A 
								                                                                                                                    INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
								                                                                                                                    WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID) 
								  
									WHEN BT.BottomButtonLinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
																					 WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN (SELECT DISTINCT HcEventCategoryID FROM HcBottomButtonEventCategoryAssociation
																																										  WHERE HcBottomButtonID = BB.HcBottomButtonID AND HcHubCitiID = @HubCitiID)
								ELSE BottomButtonLinkID	END)													 
				 , BM.Position as position		
				-- , HM.DateCreated dateModified		
				-- , CASE WHEN HcBottomButtonImageIconID IS NOT NULL THEN HcBottomButtonImageIconID ELSE @Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage	END	HcBottomButtonImageIcon	
			FROM #Menuitemss HM


			INNER JOIN HcMenuBottomButton BM ON BM.HcMenuID=HM.HcMenuID
			INNER JOIN HcBottomButton BB ON BB.HcBottomButtonID=BM.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BT ON BT.HcBottomButtonLinkTypeID=BB.BottomButtonLinkTypeID
			INNER JOIN HcHubciti HC ON HC.HcHubcitiID=HM.HcHubcitiID 
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID=BB.HcBottomButtonImageIconID
			--LEFT JOIN HubCitiReportingDatabase.dbo.RequestPlatforms RP ON RP.RequestPlatformtype=@RequestPlatformtype
			WHERE ((@LinkID IS NOT NULL AND HM.HcMenuID=@LinkID) OR (@LinkID IS NULL AND @LevelID=Level AND @HubCitiID=HM.HcHubCitiID ))
			ORDER BY position				
						
			
			----------------Two Image Template Changes 3/2/2106---------------------

			SELECT  @TempleteBackgroundImage= @config+ CAST(H.HCHubcitiID AS VARCHAR(100))+'/'+CAST(H.TemplateBackgroundImage AS VARCHAR(100)) --@config
			       ,@DisplayLabel=H.DisplayLabel
				   ,@LabelBckGndColor= H.LabelBackGroundColor
				   ,@LabelFontColor= H.LabelFontColor
			FROM HcMenu H
			WHERE HcHubCitiID=@HubCitiID AND (@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =h.HcMenuID )) 
			---------------------------------END--------------------------
			------------- Custom Navigation Bar changes -------------------------------
			DECLARE @ServerConfig VARCHAR(500)

			SELECT @ServerConfig = ScreenContent FROM AppConfiguration WHERE ConfigurationType= 'Hubciti Media Server Configuration'
			
			SELECT	@homeImgPath = 	CASE WHEN HU.homeIconName IS NULL THEN @ServerConfig+'customhome.png' ELSE  @Config +CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName END
					 ,@bkImgPath = 	CASE WHEN HU.backButtonIconName IS NULL THEN @ServerConfig+'customback.png' ELSE @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName END
					 ,@titleBkGrdColor =  ISNULL( HU.backGroundColor ,'#000000')
					 ,@titleTxtColor =  ISNULL(HU.titleColor ,'#ffffff')
				FROM HcHubCiti HC
				LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
				LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
				WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					 
			
			-------------------------------------------------------------------------
								 
				 	      
	  END

	  --SET @EndTime = GETDATE()	  
	  --SET @DiffTime = (SELECT CONVERT(VARCHAR(12), DATEADD(MS, DATEDIFF(MS,@StartTime,@EndTime), 0), 114))

	  --DROP TABLE ResponseTime
	  --CREATE TABLE ResponseTime(StartTime datetime,Endtime datetime,DiffTime varchar(100))

	  --INSERT INTO ResponseTime(StartTime,Endtime,DiffTime)
	  --SELECT @StartTime,@EndTime,@DiffTime

	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiMenuDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





























GO
