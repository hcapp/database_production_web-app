USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcProductSmartSearchResults]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcProductSmartSearchResults
Purpose					: To display the products grouped by category names.
Example					: usp_HcProductSmartSearchResults

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29thOct2013 	Pavan Sharma K    Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcProductSmartSearchResults]
(
	
   --Input Variables 
	 @ProdSearch varchar(1000)
   , @ParentCategoryID varchar(10)
   , @LowerLimit int
   , @ScreenName varchar(100)
   , @HcHubCitiID Int
    --User tracking Inputs.
   , @MainMenuID int
   , @ProductSmartSearchID int
   
	--Output Variable 
    , @TimeTaken float output
	, @NxtPageFlag bit output
	, @MaxCnt int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		SET NOCOUNT ON
		DECLARE @StartTime DATETIME = GETDATE()

	    DECLARE @Config varchar(50)  
	    DECLARE @Tomorrow DATETIME = GETDATE()+1 
		DECLARE @RowsPerPage int 			
		DECLARE @STRREV VARCHAR(100)
		DECLARE @STRRES VARCHAR(100) 
		DECLARE @ProdSearchInp varchar(5000)
		DECLARE @SpecialChars VARCHAR(1000)			

		SELECT @ProdSearchInp = @ProdSearch
		
		SELECT @config=ScreenContent
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		--To get the row count for pagination.
		SELECT @RowsPerPage = ScreenContent 
		FROM AppConfiguration 
		WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1		
						
		--To avoid Parameter sniffing.
		SET @STRREV = REVERSE(LTRIM(RTRIM(@ProdSearchInp)))
		
		IF CHARINDEX('S', @STRREV)=1
		BEGIN
			SET @ProdSearchInp=''
			SET @STRRES = SUBSTRING(@STRREV,2,LEN(@STRREV))			
			SET @ProdSearchInp = REVERSE(@STRRES)		
		END				

		--Split the search string.
        SELECT Param
        INTO #ProdSearch
        FROM [HubCitiApp2_1].[fn_SplitParam](LTRIM(RTRIM(@ProdSearchInp)), ' ') 
			
		 --Remove the noise(stopwords) from the input search string.
		SET @ProdSearchInp = NULL
        SELECT @ProdSearchInp = COALESCE(@ProdSearchInp+' ','') + I.Param 
        FROM #ProdSearch I
        LEFT JOIN sys.fulltext_system_stopwords B ON I.Param = B.stopword AND B.language_id = 1033
        WHERE B.stopword IS NULL 
			
		SELECT @ProdSearchInp = REPLACE(REPLACE(@ProdSearchInp, '''', ''''''), ',', '')		

		--Replace the special characters.
		SELECT @SpecialChars = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ProdSearchInp,'!',''),'@',''),'#',''),'$',''),'%',''),'^',''),'&',''),'*',''), '-', ''), '_', '')
		SELECT @ProdSearchInp = IIF(LEN(@SpecialChars) = 0 OR @SpecialChars = '', @ProdSearchInp, @SpecialChars)
		--Logic to replace multiple white spaces with a single one.
		SELECT @ProdSearchInp = REPLACE(REPLACE(REPLACE(@ProdSearchInp, ' ', '!@'), '@!', ''), '!@', ' ')

		--For Full Text Search
		SELECT @ProdSearchInp = LTRIM(RTRIM(REPLACE(@ProdSearchInp, ' ', ' AND ')))		
		PRINT @ProdSearchInp

		--First ProductID in a category is displayed for all the categories. 
		--If the count = 1, this productid is used to get the product details 
		--else if the count>1 then another procedure is called to get the list of products.
		
		CREATE TABLE #Temp(RowNum INT IDENTITY(1, 1)
						   , ProductID int
						   , ProductName VARCHAR(500)
						   , Counts INT 
						   , ProductDescription VARCHAR(1000)
						   , ProductImagePath VARCHAR(1000))
						 					   
		CREATE TABLE #Prod(ProductID INT, ProductName VARCHAR(500), ExpDate DATETIME, RankValue FLOAT)
		
		--Display the number of Products in given Category group by Product Name(Show the same product name in single cell).
		IF @ParentCategoryID = '0' 
		BEGIN		

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT DISTINCT ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow) 
				  , 10000
			FROM Product
			WHERE ScanCode LIKE @ProdSearchInp + '%'

			UNION ALL

			SELECT DISTINCT ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product
			WHERE ISBN LIKE @ProdSearchInp + '%'

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT TOP 1000 ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product 
			WHERE ProductName = @ProdSearch  
			UNION ALL
			SELECT ProductID
				 , ProductName
				 , ProductExpirationDate
				 , A.RankValue
			FROM 
			(SELECT DISTINCT TOP 1000  ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)ProductExpirationDate
				  , 1000 + RANK RankValue
			FROM Product P
			INNER JOIN CONTAINSTABLE(Product, ProductName, @ProdSearchInp)F ON F.[Key] = P.ProductID
			ORDER BY RankValue DESC)A

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT TOP 1000 P.ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product P
			INNER JOIN ProductKeywords PK ON P.ProductID = PK.ProductID AND PK.ProductKeyword = @ProdSearch 
			UNION ALL 
			SELECT ProductID
				 , ProductName
				 , ProductExpirationDate
				 , A.RankValue
			FROM 
			(SELECT DISTINCT TOP 1000 P.ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)ProductExpirationDate
				  , RANK RankValue
			FROM ProductKeywords PK
			INNER JOIN Product P ON P.ProductID = PK.ProductID
			INNER JOIN CONTAINSTABLE(ProductKeywords, ProductKeyword, @ProdSearchInp)F ON F.[Key] = PK.ProductKeywordsID
			ORDER BY RankValue DESC)A
			
			--To remove the duplicate Product IDs.
			;WITH ProdCTE(ProductID, ProductName, ExpDate, RankValue)
			AS 
			(
				SELECT DISTINCT ProductID, ProductName, ExpDate, RankValue
				FROM #Prod
			)
			
			INSERT INTO #Temp(ProductID  
						   , ProductName  
						   , Counts   
						   , ProductDescription  
						   , ProductImagePath)
			SELECT P.ProductID
				 , A.ProductName
				 , A.Counts
				 , ISNULL(P.ProductShortDescription, P.ProductLongDescription)
				 , CASE WHEN P.ProductImagePath IS NOT NULL
                        THEN (CASE WHEN P.WebsiteSourceFlag = 1  THEN (@Config +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + P.ProductImagePath) ELSE P.ProductImagePath END) END
				-- , IIF(P.WebsiteSourceFlag = 1, @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath,  P.ProductImagePath) 
			FROM Product P
			INNER JOIN (SELECT MIN(ProductID)ProductID
							 , ProductName
							 , COUNT(DISTINCT ProductID)Counts
							 , MAX(RankValue)RankValue
						FROM ProdCTE
						WHERE GETDATE() <= ExpDate
						GROUP BY ProductName)A
			ON P.ProductID = A.ProductID
			ORDER BY RankValue DESC 
			
		END

		--If the Others (Uncategorized) Section is selected then search against the Uncategorised products.
		IF @ParentCategoryID = '-1'
		BEGIN

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow) 
				  , 10000
			FROM Product
			WHERE ScanCode LIKE @ProdSearchInp + '%'

			UNION ALL

			SELECT ProductID
				  , ProductName
				 , ISNULL(ProductExpirationDate, @Tomorrow)
				 , 10000
			FROM Product
			WHERE ISBN LIKE @ProdSearchInp + '%'

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT TOP 1000 ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product 
			WHERE ProductName = @ProdSearch  
			UNION ALL
			SELECT ProductID
				 , ProductName
				 , ProductExpirationDate
				 , A.RankValue
			FROM 
			(SELECT DISTINCT TOP 1000  ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)ProductExpirationDate
				  , 1000 + RANK RankValue
			FROM Product P
			INNER JOIN CONTAINSTABLE(Product, ProductName, @ProdSearchInp)F ON F.[Key] = P.ProductID
			ORDER BY RankValue DESC)A

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT TOP 1000 P.ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product P
			INNER JOIN ProductKeywords PK ON P.ProductID = PK.ProductID AND PK.ProductKeyword = @ProdSearch 
			UNION ALL 
			SELECT ProductID
				 , ProductName
				 , ProductExpirationDate
				 , A.RankValue
			FROM 
			(SELECT DISTINCT TOP 1000 P.ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)ProductExpirationDate
				  , RANK RankValue
			FROM ProductKeywords PK
			INNER JOIN Product P ON P.ProductID = PK.ProductID
			INNER JOIN CONTAINSTABLE(ProductKeywords, ProductKeyword, @ProdSearchInp)F ON F.[Key] = PK.ProductKeywordsID
			ORDER BY RANK DESC)A 


			;WITH ProdCTE(ProductID, ProductName, ExpDate, RankValue)
			AS 
			(SELECT DISTINCT P.ProductID, ProductName, ExpDate, RankValue
			FROM #Prod P
			LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
			LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
			WHERE (PC.ProductCategoryID IS NULL
					OR
				   C.ParentCategoryName = 'Other') 
			)
			
			INSERT INTO #Temp(ProductID  
						   , ProductName  
						   , Counts   
						   , ProductDescription  
						   , ProductImagePath)
			SELECT P.ProductID
				 , A.ProductName
				 , A.Counts
				 , ISNULL(P.ProductShortDescription, P.ProductLongDescription)
				 , IIF(P.WebsiteSourceFlag = 1, @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath,  P.ProductImagePath) 
			FROM Product P
			INNER JOIN (SELECT MIN(ProductID)ProductID
							 , ProductName
							 , COUNT(DISTINCT ProductID)Counts
							 , MAX(RankValue)RankValue
						FROM ProdCTE
						WHERE GETDATE() <= ExpDate
						GROUP BY ProductName)A
			ON P.ProductID = A.ProductID
			ORDER BY RankValue DESC, ProductName ASC
			
		END
		
		--When category is selected.
		IF @ParentCategoryID > 0
		BEGIN		
			--This Logic is split into 4 different blocks of code to avoid or conditions and joins.
			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow) 
				  , 10000
			FROM Product
			WHERE ScanCode LIKE @ProdSearchInp + '%'

			UNION ALL

			SELECT ProductID
				  , ProductName
				 , ISNULL(ProductExpirationDate, @Tomorrow)
				 , 10000
			FROM Product
			WHERE ISBN LIKE @ProdSearchInp + '%'

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT TOP 1000 ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product 
			WHERE ProductName = @ProdSearch  
			UNION ALL
			SELECT ProductID
				 , ProductName
				 , ProductExpirationDate
				 , A.RankValue
			FROM 
			(SELECT DISTINCT TOP 1000  ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)ProductExpirationDate
				  , 1000 + RANK RankValue
			FROM Product P
			INNER JOIN CONTAINSTABLE(Product, ProductName, @ProdSearchInp)F ON F.[Key] = P.ProductID
			ORDER BY RankValue DESC)A

			INSERT INTO #Prod(ProductID, ProductName, ExpDate, RankValue)
			SELECT TOP 1000 P.ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)
				  , 10000
			FROM Product P
			INNER JOIN ProductKeywords PK ON P.ProductID = PK.ProductID AND PK.ProductKeyword = @ProdSearch 
			UNION ALL 
			SELECT ProductID
				 , ProductName
				 , ProductExpirationDate
				 , A.RankValue
			FROM 
			(SELECT DISTINCT TOP 1000 P.ProductID
				  , ProductName
				  , ISNULL(ProductExpirationDate, @Tomorrow)ProductExpirationDate
				  , RANK RankValue
			FROM ProductKeywords PK
			INNER JOIN Product P ON P.ProductID = PK.ProductID
			INNER JOIN CONTAINSTABLE(ProductKeywords, ProductKeyword, @ProdSearchInp)F ON F.[Key] = PK.ProductKeywordsID
			ORDER BY RANK DESC)A 

			;WITH ProdCTE(ProductID, ProductName, ExpDate, RankValue)
			AS 
			(SELECT DISTINCT P.ProductID, ProductName, ExpDate, RankValue
			FROM #Prod P
			INNER JOIN ProductCategory PC ON P.ProductID = PC.ProductID
			INNER JOIN Category C ON C.CategoryID = PC.CategoryID
			WHERE C.ParentCategoryID = @ParentCategoryID)

			INSERT INTO #Temp(ProductID  
						   , ProductName  
						   , Counts   
						   , ProductDescription  
						   , ProductImagePath)
			SELECT P.ProductID
				 , A.ProductName
				 , A.Counts
				 , ISNULL(P.ProductShortDescription, P.ProductLongDescription)
				 , IIF(P.WebsiteSourceFlag = 1, @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath,  P.ProductImagePath) 
			FROM Product P
			INNER JOIN (SELECT MIN(ProductID)ProductID
							 , ProductName
							 , COUNT(DISTINCT ProductID)Counts
							 , MAX(RankValue)RankValue
						FROM ProdCTE
						WHERE GETDATE() <= ExpDate
						GROUP BY ProductName)A
			ON P.ProductID = A.ProductID
			ORDER BY RankValue DESC, ProductName ASC
			 
		END
		
		--Calculate the total number of records.
		SELECT @MaxCnt = MAX(RowNum) FROM #Temp	
		
		--Check if the next page exixts.
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - (@LowerLimit+@RowsPerPage)) > 0 THEN 1 ELSE 0 END  
	
		SELECT RowNum rowNumber
			 , ProductID productId
			 , ProductName productName
			 , ProductDescription  
			 , ProductImagePath productImagePath 
			 , Counts prdCount		
		INTO #ProductList	  
		FROM #Temp
		ORDER BY RowNum
		OFFSET @LowerLimit ROWS
		FETCH NEXT @RowsPerPage ROWS ONLY

			--User Tracking Section.
			
			--Capture the category that the user has selected.
			INSERT INTO HubCitiReportingDatabase..ProductSmartSearchCategory(ProductSmartSearchID
																		   , ParentCategoryID
																		   , CreatedDate)
																SELECT @ProductSmartSearchID
																	 , @ParentCategoryID
																	 , GETDATE()
																	 
			
			CREATE TABLE #Temp1(ProductListID INT								
							  , ProductSmartSearchID INT
							  , ProductID INT)
			
			INSERT INTO HubCitiReportingDatabase..ProductList(ProductSmartSearchID 
													        , MainMenuID
													        , ProductID
													        , DateCreated
													        )
							OUTPUT inserted.ProductListID, inserted.ProductSmartSearchID, inserted.ProductID INTO #Temp1(ProductListID, ProductSmartSearchID, ProductID)
													   SELECT @ProductSmartSearchID
															, @MainMenuID
															, productId
															, GETDATE()
													   FROM #ProductList
			--Display along with the primary key of the tracking table.
			SELECT rowNumber
			 , T.ProductListID prodListID
			 , P.productId
			 , productName
			 , ProductDescription  
			 , ProductImagePath productImagePath 
			 , prdCount	
			FROM #ProductList P
			INNER JOIN #Temp1 T ON T.ProductID = P.productId
													  
		SELECT @TimeTaken = DATEDIFF(MILLISECOND, @StartTime, GETDATE())
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcProductSmartSearchResults.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;













































GO
