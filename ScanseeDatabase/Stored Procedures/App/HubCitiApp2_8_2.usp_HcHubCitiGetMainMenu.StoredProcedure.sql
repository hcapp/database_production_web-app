USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiGetMainMenu]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcHubCitiGetMainMenu]
Purpose					: To disply the list of the Main Menu Button Details associated to Hubciti only.
Example					: [usp_HcHubCitiGetMainMenu]

HIStory
VersiON		Date			Author			Change DescriptiON
--------------------------------------------------------------- 
1.0			01/14/2016	    Dhananjaya TR	1.0
2.0         10/05/2016     Shilpashree      News Ticker changes 
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiGetMainMenu]
(
    --Input variable.
      @LinkID INT
    , @HubCitiID Varchar(100)
    , @LevelID INT  
    , @SortOrder Varchar(100)  
    , @TypeID INT
    , @DepartmentID INT
	, @DateCheck datetime -- Varchar(100)
	, @DeviceType varchar(100) --IF input is Ipad then only send ipad image values

	--Output Variable 
	, @HamburgerImagePath varchar(1000) output
	, @NoRecordsMsg varchar(max) output
	, @FilterID INT output
	, @FilterName varchar(255) output
	, @FilterCount INT output
	, @DownLoadLinkIOS Varchar(1000) output
	, @DownLoadLinkandroid Varchar(1000) output
	, @RetailGroupButtonImagePath varchar(1000) output
	, @AppiconimagePath varchar(1000) output
	, @HCMenuBannerImage Varchar(1000) output
	, @HcDepartmentFlag BIT output
	, @HcTypeFlag BIT output
	, @NoOfColumns INT output
	, @MenuName varchar(255) output
	, @IsRegionapp BIT output
	, @TemplateChanged BIT output
	, @TemplateName varchar(100) output
	, @ModifiedDate varchar(100) output
	, @TempleteBackgroundImage VARCHAR(1000) output
	, @DisplayLabel BIT output
	, @LabelBckGndColor Varchar(250) output
	, @LabelFontColor Varchar(250) output
	, @homeImgPath varchar(250) output
	, @bkImgPath varchar(250) output
	, @titleBkGrdColor varchar(250) output
	, @titleTxtColor varchar(250) output
	, @IsNewsTicker BIT output
	, @TickerBckGrdColor Varchar(250) output
	, @NewsTextColor Varchar(250) output
	, @NewsTickerMode Varchar(250) output
	, @NewsDirection Varchar(250) output 
    , @Status INT output
	, @ErrorNumber INT output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		
		SET @ModifiedDate=replace(@ModifiedDate,'  ',' ')
		--SET @DateCheck=replace(@DateCheck,'  ',' ')	
		SET @ModifiedDate=replace(@ModifiedDate,'  ',' ')
		--SET @DateCheck=replace(@DateCheck,'  ',' ')	
		SET @ModifiedDate=CONVERT(DATETIME,@ModifiedDate)
		--SET @DateCheck=CONVERT(DATETIME,@DateCheck)


		DECLARE @NewsImage varchar(1000)
		SELECT @NewsImage = ScreenContent + CONVERT(VARCHAR,@HubCitiID)+ '/'
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'
		
		DECLARE @DefaultHamburgerIcon VARCHAR(1000)
		select @DefaultHamburgerIcon = ScreenContent +  'images/hamburger.png' 
		from AppConfiguration 
		where ScreenName = 'Base_URL'


	   ----News Ticker Implementation
		SELECT @IsNewsTicker = HM.IsNewsTicker
			, @NewsTickerMode = HcNewsTickerMode
			, @NewsDirection = HcNewsTickerDirection
		FROM HcMenu HM
		LEFT JOIN HcNewsTickermode M ON HM.HcNewsTickermodeID = M.HcNewsTickerModeID
		LEFT JOIN HcNewsTickerDirection D ON HM.HcNewsTickerDirectionID = D.HcNewsTickerDirectionID
		WHERE HM.HcHubCitiID = @HubCitiID 
		AND (((@LinkID IS NULL OR @LinkID = 0) AND Level =1) OR (@LinkID IS NOT NULL AND @LinkID = HcMenuID))

		SELECT @TickerBckGrdColor = tickerBackground
				,@NewsTextColor = newsFeedText
				 ,@HamburgerImagePath = ISNULL(@NewsImage + NewsHamburgerIcon,@DefaultHamburgerIcon)
		FROM HcMenuCustomUI
		WHERE HubCitiID = @HubCitiID

		IF(@IsNewsTicker IS NULL)
		BEGIN
			SET @IsNewsTicker = 0
		END
		
		IF(@IsNewsTicker = 0)
		BEGIN

			SET @NewsTickerMode = NULL
			SET @NewsDirection = NULL
			SET @TickerBckGrdColor = NULL
			SET @NewsTextColor = NULL

		END

		SELECT RowNum = Row_Number() OVER (PARTITION BY NewsType ORDER BY NewsType,R.DateCreated desc)
						,Title,NewsType,N.NewsCategoryID,R.DateCreated,R.RssNewsFirstFeedNewsID
		INTO #NewsList
		FROM NewsFirstSettings N
		INNER JOIN NewsFirstcategory D ON  N.NewsCategoryID = D.NewsCategoryID AND N.HcHubCitiID = @HubCitiID 
		INNER JOIN RssNewsFirstFeedNews R ON D.NewsCategoryDisplayValue = R.NewsType AND R.HcHubCitiID = @HubCitiID
		WHERE N.NewsTicker = 1 AND N.HcHubCitiID = @HubCitiID AND R.Title IS NOT NULL
		GROUP BY NewsType,Title,N.NewsCategoryID,R.DateCreated,R.RssNewsFirstFeedNewsID
		ORDER BY NewsType,R.DateCreated desc
		
		SELECT DISTINCT L.NewsCategoryID catId
						,L.NewsType catName
						,L.Title title 
						,L.RssNewsFirstFeedNewsID [rowCount]
		FROM #NewsList L
		INNER JOIN NewsFirstSettings N on L.NewsCategoryID = N.NewsCategoryID ANd N.HcHubCitiID = @HubCitiID
		INNER JOIN HcMenu M ON N.HcHubCitiID = M.HcHubCitiID AND IsNewsTicker = 1
		WHERE RowNum <= N.NewsStories 
		ORDER BY NewsType

	  -----------------------------------------

		 IF  ISNULL(@Levelid,1)<>1
         BEGIN
			IF NOT EXISTS(SELECT 1 FROM HcNewsFirstTempFlag H WHERE HcHubCitiID = @HubCitiID  AND ISNULL(Level,1)=0 AND linkid=@LinkID) AND ISNULL(@LevelID,1)<>1 
			BEGIN
				 INSERT INTO HcNewsFirstTempFlag(HcHubCitiID,level,linkid) 
				 SELECT @HubCitiID,0,@LinkID
			END
		 END
		 IF NOT EXISTS(SELECT 1 FROM HcNewsFirstTempFlag H WHERE HcHubCitiID = @HubCitiID  AND ISNULL(Level,1)=1)  AND ISNULL(@LevelID,1)=1 
         BEGIN
				 INSERT INTO HcNewsFirstTempFlag(HcHubCitiID,level) 
				 SELECT @HubCitiID,1
		 END

		 UPDATE  HcNewsFirstTempFlag SET level=1  WHERE level IS NULL AND HcHubCitiID = @HubCitiID

		IF EXISTS(SELECT 1 FROM HcMenu H INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
			     WHERE HM.DateCreated = @DateCheck
				 --WHERE LEFT(CONVERT(DATETIME,H.DateCreated),19)  = LEFT(CONVERT(DATETIME,@DateCheck),19) 
				 AND HcHubCitiID = @HubCitiID) AND @LinkID = 0
			BEGIN
				SELECT @TemplateChanged = 0, @Status = 0, @ModifiedDate = @DateCheck
			END
		ELSE IF EXISTS(SELECT 1 FROM HcMenu H INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
							WHERE HM.DateCreated = @DateCheck
							--WHERE LEFT(CONVERT(DATETIME,HM.DateCreated),19)  = LEFT(CONVERT(DATETIME,@DateCheck),19) 
							AND HcHubCitiID = @HubCitiID  AND H.HcMenuID=@LinkID ) AND @LinkID <> 0
			BEGIN
				SELECT @TemplateChanged = 0, @Status = 0, @ModifiedDate = @DateCheck
			END

		ELSE
			BEGIN	
				SET @TemplateChanged=1
				DECLARE @Flag BIT
				DECLARE @FlagExist BIT
				DECLARE @FlagExist1 BIT

				IF ISNULL(@LevelID,1)=1
					BEGIN
						 IF EXISTS(SELECT 1 FROM HcMenu H WHERE HcHubCitiID = @HubCitiID AND ISAssociateNews=1 AND Level=1)
							 BEGIN 
									SET @Flag=1
									UPDATE HcNewsFirstTempFlag SET NewsFlag=1,level=1,linkid=@LinkID 
									WHERE HcHubCitiID=@HubCitiID AND NewsFlag IS NULL AND ISNULL(Level,1)=1
							 END
						 ELSE
							 BEGIN
									SET @Flag=0
									UPDATE HcNewsFirstTempFlag SET NewsFlag=0,level=1,linkid=@LinkID 
									WHERE HcHubCitiID=@HubCitiID AND NewsFlag IS NULL AND ISNULL(Level,1)=1
							 END
					END
				ELSE
				BEGIN
					IF EXISTS(SELECT 1 FROM HcMenu H WHERE HcHubCitiID = @HubCitiID AND ISAssociateNews=1 AND HcMenuID=@LinkID)
		  				BEGIN 
							 SET @Flag=1
							 UPDATE HcNewsFirstTempFlag SET NewsFlag=1,level=0,linkid=@LinkID 
							 WHERE HcHubCitiID=@HubCitiID AND NewsFlag IS NULL AND ISNULL(Level,1)=0
						END
				   ELSE
						BEGIN
							SET @Flag=0
							UPDATE HcNewsFirstTempFlag SET NewsFlag=0,level=0,linkid=@LinkID WHERE HcHubCitiID=@HubCitiID AND NewsFlag IS NULL AND ISNULL(Level,1)=0
						END
                END

			SELECT @FlagExist=NewsFlag  
			FROM HCNewsFirstTempFlag  WHERE HcHubCitiID=@HubCitiID AND  ISNULL(level,1)= case when @LevelID=1 then '1' ELSE 0 END
			--AND ISNULL(linkid,0)=case when @LevelID=1 then 0 ELSE @LinkID END 
			AND ISNULL(linkid,0)=ISNULL(@LinkID,0)

			SET  @FlagExist1 = @FlagExist

			IF @FlagExist  IS NULL
			BEGIN
			IF @LevelID=1
					BEGIN 
						SELECT @FlagExist=NewsFlag  FROM HCNewsFirstTempFlag  WHERE HcHubCitiID=@HubCitiID 
						AND  ISNULL(level,1)=case when @LevelID=1 then '1' ELSE 0 END
						--AND ISNULL(linkid,0)=case when @LevelID=1 then 0 ELSE @LinkID END 
					END
			END

			IF @FlagExist=@Flag
			BEGIN
				DECLARE @Template Varchar(100)
					, @CityConfig VARCHAR(1000)
					, @CityExpDefaultConfig VARCHAR(1000)			
					, @GroupedTabTextColor VARCHAR(100)
					, @GroupedTabTextFontColor VARCHAR(100)
					, @Config VARCHAR(500)
					, @HcAppListName VARCHAR(10)

				SET @LinkID =IIF(@LinkID IS NULL OR @LinkID=0 ,NULL,@LinkID)
				SET @TypeID =IIF(@TypeID IS NULL OR @typeID=0,NULL,@TypeID)
				SET @DepartmentID =IIF(@DepartmentID IS NULL OR @DepartmentID=0,NULL,@DepartmentID)		

				DECLARE @HcLinkTypeIDS VARCHAR(100)
				SELECT @HcLinkTypeIDS = COALESCE(@HcLinkTypeIDS+',','') + CAST(HcLinkTypeID AS VARCHAR(100))
				FROM HcLinkType WHERE LinkTypeName IN ('Text','Label')
			
				SELECT @MenuName = MenuName
				FROM HcMenu
				WHERE HcMenuID = @LinkID AND ISNULL(@LinkID, 0) <> 0 OR ISNULL(@LinkID, 0) = 0 AND Level = 1
			
				SELECT @Config = ScreenCONtent
				FROM AppConfiguratiON WHERE ConfiguratiONType = 'Hubciti Media Server ConfiguratiON'

				IF @DepartmentID =0
				BEGIN
					SET @DepartmentID = NULL
				END

				IF @TypeID =0
				BEGIN
					SET @TypeID = NULL
				END
			 
				--To get the Filter Details AND Count of filters for a given hub Citi.
				SELECT @FilterID = F.HcFilterID
					 , @FilterName = F.FilterName
				FROM HcCityExperience H
				INNER JOIN HcFilter F ON H.HcCityExperienceID = F.HcCityExperienceID AND H.HcHubCitiID = @HubCitiID

				SELECT @FilterCount = COUNT(1)
				FROM HcFilter 
				WHERE HcHubCitiID = @HubCitiID
           
		   --------------------------------------------------------
			SELECT DISTINCT HM.HcTemplateID
			INTO #Templates
			FROM HcMenuItem MI
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			INNER JOIN HcTemplate T ON HM.HcTemplateID = T.HcTemplateID
			WHERE HM.HCHubcitiID=@HubCitiID AND	(@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID ))
			AND T.TemplateName IN ('Combo Template','Grouped Tab','Grouped Tab With Image') 
			    
			CREATE TABLE #Groups(HcMenuItemID INT)
			IF EXISTS (SELECT 1 FROM #Templates)
			BEGIN
		   
			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc) 
			      ,H.HcMenuItemID				  
			INTO #group1
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE  (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID 	
				  , MenuItemName			 
			INTO #group2
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group3
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID 
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE  (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label') 
			ORDER BY H.HcMenuItemID 

			--Fetch MenuItemID related GroupsID
			SELECT Rownum=Row_number() Over(Order by H.HcMenuItemID Asc)
			      , H.HcMenuItemID   
			INTO #group4
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID 
			WHERE (L.LinkTypeName ='Text' OR L.LinkTypeName ='Label')
			ORDER BY H.HcMenuItemID 
			
            --SELECT menuitems based ON the Department AND Type values
			SELECT H.HcMenuItemID 
				  , H.MenuItemName
			INTO #menuitem
			FROM HcMenuItem H 
			INNER JOIN HCmenu M ON H.HcMenuID =M.HcMenuID AND HcHubCitiID = @HubCitiID
			INNER JOIN HcLinkType L ON H.HcLinkTypeID =L.HcLinkTypeID
			WHERE ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND H.HcMenuItemTypeID =@TypeID )) 
						AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND H.HcDepartmentID  =@DepartmentID))))
						
			ORDER BY H.HcMenuItemID
	
			DECLARE @maxGroup INT
			DECLARE @minGroup INT

			--SELECT maximum menuitemid in the SELECTed group template
			SELECT @maxGroup = MAX(HcMenuItemID) FROM #group1
			SELECT @minGroup = Min(HcMenuItemID) FROM #group1
		
			--Here based ON the temporary table data, SELECTing menuitemid under a group AND storing in temp table
			
			INSERT INTO #Groups
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )	--here check the menuitem IS belONg to first group			
			UNION
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE

			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID 
								AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID) 
			UNION
			SELECT DISTINCT A.HcMenuItemID     
			--INTO #Groups
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
		--	FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (@maxGroup <M.HcMenuItemID AND @maxGroup =A.HcMenuItemID)
			
			SELECT DISTINCT A.HcMenuItemID
			              ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
			INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE (	A.HcMenuItemID <M.HcMenuItemID AND A.Rownum =1 AND ((C.Rownum =2 AND C.HcMenuItemID >M.HcMenuItemID) OR @maxGroup=1) )				
			UNION
			SELECT DISTINCT A.HcMenuItemID
			              ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
			--INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE

			( M.HcMenuItemID > A.HcMenuItemID AND C.Rownum > A.Rownum AND (A.Rownum + 1)=C.Rownum  AND C.HcMenuItemID > M.HcMenuItemID 
							AND (A.Rownum -1)=D.Rownum  AND A.Rownum > D.Rownum AND M.HcMenuItemID >D.HcMenuItemID)
			UNION
				SELECT DISTINCT A.HcMenuItemID
			              
			              ,menuitemid= CASE WHEN M.HcMenuItemID IS NOT NULL THEN M.HcMenuItemID ELSE A.HcMenuItemID END   
		--	INTO #SortOrders
			FROM #menuitem M
			FULL OUTER JOIN #group1 A ON 1=1
			FULL OUTER JOIN #group2 B ON 1=1
			FULL OUTER JOIN #group3 C ON 1=1
			FULL Outer JOIN #group4 D ON 1=1
			WHERE
			 
			(@maxGroup <M.HcMenuItemID AND @maxGroup =A.HcMenuItemID)

			SELECT DISTINCT M.GroupIDs
			      ,MenuitemsID
				  ,M.MenuItemName GroupName
			INTO #Sort
			FROM
			(SELECT DISTINCT HcMenuItemID GroupIDs
			                ,menuitemid MenuitemsID
			FROM #SortOrders
			UNION ALL
			SELECT DISTINCT HcMenuItemID GroupIDs
			               ,HcMenuItemID MenuitemsID
			FROM #SortOrders)A
			INNER JOIN HcMenuItem M ON M.HcMenuItemID =A.GroupIDs
			ORDER BY MenuitemsID,M.GroupIDs Asc
			
			END
			
			--for the grouped tab template send the button & the font colors from the configuration table.
			SELECT @GroupedTabTextColor = ScreenCONtent
			FROM AppConfiguratiON
			WHERE ConfiguratiONType = 'HubCiti Grouped Tab Text Color'
			AND ScreenName = 'HubCiti Grouped Tab Text Color'
			
			SELECT @GroupedTabTextFontColor = ScreenCONtent
			FROM AppConfiguratiON
			WHERE ConfiguratiONType = 'HubCiti Grouped Tab Text Font Color'
			AND ScreenName = 'HubCiti Grouped Tab Text Font Color'

			SELECT @CityConfig=ScreenCONtent
			FROM AppConfiguratiON 
			WHERE ConfiguratiONType='App Media Server ConfiguratiON'
		
			SELECT @CityExpDefaultConfig = ScreenCONtent
			FROM AppConfiguratiON 
			WHERE ConfiguratiONType = 'City Experience Default Image Path' AND Active = 1
			
			SELECT Param LinkTypeIDs
			INTO #LinkTypeIDs
			FROM fn_SplitParam(@HcLinkTypeIDS,',')

			SELECT @DownLoadLinkIOS=ItunesURL
			FROM HcHubCiti HC
			WHERE HC.HcHubCitiID=@HubCitiID  
						
			SELECT @DownLoadLinkANDroid=GooglePlayURL
			FROM HcHubCiti HC
			WHERE HC.HcHubCitiID=@HubCitiID 
			
			SELECT @RetailGroupButtONImagePath = @CityConfig + ISNULL(R.ButtONImagePath, @CityExpDefaultConfig)
			FROM RetailGroup R
			INNER JOIN HcCityExperience C ON C.HcCityExperienceID=R.RetailGroupID AND HcHubCitiID =@HubCitiID 
			
			SELECT @RetailGroupButtONImagePath = ISNULL(@RetailGroupButtONImagePath, @CityConfig + @CityExpDefaultConfig)
			
			--SEND Appicon
			SELECT @AppIcONImagePath = @Config + CAST(HcHubCitiID AS VARCHAR(10)) + '/' + AppIcON
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID
			
			SELECT DISTINCT @HCMenuBannerImage= IIF(HCMenuBannerImage IS NOT NULL AND HCMenuBannerImage <> '',@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HCMenuBannerImage,NULL )
				  ,@HcDepartmentFlag = HcDepartmentFlag
				  ,@HcTypeFlag = HcTypeFlag	
				  ,@NoOfColumns = NoOfColumns
			FROM HcMenuItem MI
			INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID
			INNER JOIN HCTemplate HCT ON HCT.HCTemplateID = HM.HCTemplateID
			INNER JOIN HCLinkType N ON N.HcLinkTypeID = MI.HcLinkTypeID
			WHERE HM.HCHubcitiID=@HubCitiID AND --(MI.HcMenuID=ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 AND Level=1))   
			(@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID ))   
			
			IF(@IsNewsTicker = 1)
			BEGIN

				SET @HCMenuBannerImage = NULL
			
			END
			
			CREATE TABLE #MenuItems(RowNum INT IDENTITY(1,1)
									,MenuID INT
									,HubCitiId INT
									,templateName VARCHAR(250)
									,Level INT
									,mItemID INT
									,mItemName VARCHAR(250)
									,LinkTypeName VARCHAR(250)
									,LinkTypeID INT
									,LinkID INT
									,PositiON INT
									,mItemImg VARCHAR(1000)
									,mBkgrdColor VARCHAR(100)
									,mBkgrdImage VARCHAR(1000)
									,mBtnColor VARCHAR(100)
									,mBtnFontColor VARCHAR(100)
									,smBkgrdColor VARCHAR(100)
									,smBkgrdImage VARCHAR(1000)
									,smBtnColor VARCHAR(100)
									,smBtnFontColor VARCHAR(100)
									,HcDepartmentID INT
									,HcMenuItemTypeID INT			
									,departmentName VARCHAR(250)
									,mItemTypeName VARCHAR(250)
									,HcMenuItemShapeID INT
									,HcMenuItemShape  VARCHAR(100)
									,mGrpBkgrdColor VARCHAR(100)
									,mGrpFntColor VARCHAR(100)
									,smGrpBkgrdColor VARCHAR(100)
									,smGrpFntColor VARCHAR(100)
									,MenuName VARCHAR(250)
									,mFontColor VARCHAR(100)
									,smFontColor VARCHAR(100)
									,dateModIFied DATETIME
									,TemplateBackgroundColor VARCHAR(100)
									--,NoOfColumns INT
									)

			SELECT DISTINCT HcMenuItemID
						,MI.HcMenuID
						,MenuItemName
						,HcLinkTypeID
						,LinkID
						,PositiON						
						,HcMenuItemImagePath
						,HcMenuItemIpadImagePath
						,HcDepartmentID
						,HcMenuItemTypeID
						,HcMenuItemShapeID
						,MI.DateCreated
						,Mi.DateModIFied
						,MI.CreatedUserID
						,MI.ModIFiedUserID
						,MenuTypeID
						,HcHubCitiID
						,HcTemplateID
						,Level
						,Hm.DateCreated MenuCreatedDate
						,MenuName
						,HCMenuBannerImage
						,HcDepartmentFlag
						,HcTypeFlag
						,NoOfColumns
						,TemplateBackgroundColor
				INTO #Menuitemss
				FROM HcMenuItem MI
				INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID AND HM.HcHubCitiID = @HubCitiID 
				AND ((@LinkID IS NULL AND Level =1) OR (@LinkID IS NOT NULL AND @LinkID =MI.HcMenuID))
			
				 INSERT INTO #MenuItems (MenuID
										,HubCitiId
										,templateName
										,Level
										,mItemID
										,mItemName
										,LinkTypeName
										,LinkTypeID
										,LinkID
										,PositiON
										,mItemImg
										,mBkgrdColor
										,mBkgrdImage
										,mBtnColor
										,mBtnFontColor
										,smBkgrdColor
										,smBkgrdImage
										,smBtnColor
										,smBtnFontColor
										,HcDepartmentID
										,HcMenuItemTypeID			
										,departmentName
										,mItemTypeName
										,HcMenuItemShapeID 
										,HcMenuItemShape 
										,mGrpBkgrdColor
										,mGrpFntColor
										,smGrpBkgrdColor
										,smGrpFntColor
										,MenuName
										,mFontColor
										,smFontColor
										,dateModIFied
										,TemplateBackgroundColor
										--,NoOfColumns
										)
			         SELECT DISTINCT MenuID
							,HubCitiId
							,templateName
							,Level
							,mItemID
							,mItemName
							,LinkTypeName
							,LinkTypeID
							,LinkID
							,PositiON
							,mItemImg
							,mBkgrdColor
							,mBkgrdImage
							,mBtnColor
							,mBtnFontColor
							,smBkgrdColor
							,smBkgrdImage
							,smBtnColor
							,smBtnFontColor
							,HcDepartmentID
							,HcMenuItemTypeID			
							,departmentName
							,mItemTypeName
							,HcMenuItemShapeID 
							,HcMenuItemShape 
							,mGrpBkgrdColor
							,mGrpFntColor
							,smGrpBkgrdColor
							,smGrpFntColor
							,MenuName
							,mFontColor
							,smFontColor
							,dateModIFied
							,TemplateBackgroundColor
							--,NoOfColumns
					FROM
					(SELECT DISTINCT HM.HcMenuID as MenuID			
						  ,HM.HCHubCitiID as HubCitiId
						  ,HCT.TemplateName as templateName
						  ,HM.Level
						  ,MI.HcMenuItemID as mItemID
						  ,MenuItemName	as mItemName
						  ,LinkTypeName,N.HcLinkTypeID as LinkTypeID,LinkID	      
						  ,PositiON	
						  ,mItemImg= CASE WHEN @DeviceType = 'Ipad' AND (HCT.TemplateName = '4X4 Grid' OR HCT.TemplateName = 'Rectangular Grid') 
								  THEN @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(MI.HcMenuItemIpadImagePath,MI.HcMenuItemImagePath)
								  WHEN @DeviceType = 'Ipad' AND (HCT.TemplateName != '4X4 Grid' OR HCT.TemplateName<> 'Rectangular Grid') 
								  THEN @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath
								  ELSE @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+MI.HcMenuItemImagePath 
							END
						  --,mItemImg = IIF(@DeviceType = 'Ipad',@Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(HcMenuItemIpadImagePath,HcMenuItemImagePath), @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath)
						 --,mItemImg = @Config + CAST(HM.HCHubcitiID AS VARCHAR(100))+'/'+HcMenuItemImagePath
						  ,mBkgrdColor = IIF(HM.Level = 1, MenuBackgroundColor, NULL)
						  ,mBkgrdImage = IIF(HM.Level = 1, @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+MenuBackgroundImage, NULL)
						  ,mBtnColor = IIF(HM.Level = 1, MenuButtONColor, NULL)
						  ,mBtnFontColor = IIF(HM.Level = 1, MenuButtONFontColor, NULL)
						  ,smBkgrdColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuBackgroundColor, NULL)
						  ,smBkgrdImage = IIF(ISNULL(HM.Level, 0) <> 1,@Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+SubMenuBackgroundImage, NULL)				  
						  ,smBtnColor = IIF(ISNULL(HM.Level, 0) <> 1, CASE WHEN N.LinkTypeName = 'Text' OR N.LinkTypeName = 'Label' THEN @GroupedTabTextColor ELSE SubMenuButtONColor END, NULL)
						  ,smBtnFontColor = IIF(ISNULL(HM.Level, 0) <> 1, CASE WHEN N.LinkTypeName = 'Text' OR N.LinkTypeName = 'Label' THEN @GroupedTabTextFontColor ELSE SubMenuButtONFontColor END, NULL) 
						  ,MI.HcDepartmentID
						  ,MI.HcMenuItemTypeID			
						  ,D.HcDepartmentName departmentName
						  ,T.HcMenuItemTypeName mItemTypeName
						  ,S.HcMenuItemShapeID 
						  ,S.HcMenuItemShape 
						  ,mGrpBkgrdColor = IIF(HM.Level = 1, MenuGroupBackgroundColor, NULL)
						  ,mGrpFntColor = IIF(HM.Level = 1, MenuGroupFontColor, NULL)
						  ,smGrpBkgrdColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuGroupBackgroundColor, NULL)
						  ,smGrpFntColor = IIF(ISNULL(HM.Level, 0) <> 1, SubMenuGroupFontColor, NULL)
						  ,HM.MenuName
						  ,MenuIcONicFontColor mFontColor
						  ,SubMenuIcONicFontColor smFontColor
						  ,IIF(@LinkID = 0,HM.DateCreated,MI.DateCreated) dateModIFied
						  ,TemplateBackgroundColor
						  --,NoOfColumns
					FROM HcMenuItem MI			
					INNER JOIN HcMenu HM ON MI.HCMenuID = HM.HCMenuID AND (ISAssociateNews IS NULL or ISAssociateNews = 0)
					INNER JOIN HCTemplate HCT ON HCT.HCTemplateID = HM.HCTemplateID
					INNER JOIN HCLinkType N ON N.HcLinkTypeID = MI.HcLinkTypeID
					LEFT JOIN HcMenuCustomUI CI ON HM.HcHubCitiID = CI.HubCitiId
					LEFT JOIN HCDepartments D ON D.HcDepartmentID = MI.HcDepartmentID
					LEFT JOIN HcMenuItemType T ON T.HcMenuItemTypeID = MI.HcMenuItemTypeID
					LEFT JOIN HcMenuItemShape S ON S.HcMenuItemShapeID =MI.HcMenuItemShapeID
					LEFT JOIN #Groups G ON G.HcMenuItemID =MI.HcMenuItemID  
					WHERE  HM.HCHubcitiID=@HubCitiID AND (MI.HcMenuID=ISNULL(@LinkID, 0) OR (ISNULL(@LinkID, 0) = 0 AND Level=1))
					AND ((((@TypeID IS NULL) OR (@TypeID IS NOT NULL AND MI.HcMenuItemTypeID =@TypeID )) 
					AND ((@DepartmentID IS NULL) OR (@DepartmentID IS NOT NULL AND MI.HcDepartmentID  =@DepartmentID))) OR G.HcMenuItemID =MI.HcMenuItemID)  
					)A

			--Confirmation of Success
			SELECT @Status = 0
			
			 --To dISplay message when no Retailers dISplay for user preferred cities.
			DECLARE @MaxCnt INT
			DECLARE @UserPrefCities NVarchar(MAX)
			DECLARE @Count INT
			DECLARE @Cnt INT = 0
			DECLARE @TemplateName1 Varchar(100)

			SELECT @MaxCnt = Count(1) FROM #MenuItems
			SET @Count=(SELECT Count(LinkTypeName) FROM #MenuItems WHERE LinkTypeName = 'Label' OR LinkTypeName ='Text')
			SELECT @Cnt=Count(1) FROM #MenuItems
			SELECT @TemplateName1=templateName  FROM #MenuItems
			 	 
			IF (@Cnt <= 0)
			BEGIN
				SELECT @NoRecordsMsg = 'No Records Found.'
			END
		
			IF @SortOrder ='ASC' AND (@TemplateName1 IN ('Grouped Tab','Combo Template','Grouped Tab With Image'))
			BEGIN	
				 SELECT RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					  --,LinkTypeName			
					  --,LinkTypeID
					  --,LinkID 
					  ,LinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
													  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
													  WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID ) = 1 AND M.LinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																						   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																						   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																						   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
												WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociatiON 
													  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							   INNER JOIN HcMenuItemEventCategoryAssociatiON MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID
																																							   WHERE HcMenuItemID = M.mItemID AND HcHubCitiID =@HubCitiID)
										   ELSE LinkTypeName END)
						,LinkTypeID
						,LinkID =  CASE WHEN M.LinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID) 
										  WHEN M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																			 INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
																			 WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 AND M.LinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
																																							INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
																																							WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
										  WHEN M.LinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociatiON 
																			  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN (SELECT HcEventCategoryID FROM HcMenuItemEventCategoryAssociatiON
																																							WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID)	

									 ELSE LinkID END	
					  ,PositiON	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,	GroupName 	
					  , SortD = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') AND @SortOrder ='DESC' THEN 'Z'+ mItemName ELSE mItemName END
					  , SortA = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') AND @SortOrder ='ASC' THEN ' ' ELSE mItemName END
					  , mGrpBkgrdColor 
					  , mGrpFntColor                        
					  , smGrpBkgrdColor 
					  , smGrpFntColor 
					  , MenuName
					  , mFontColor
					  , smFontColor
					  , dateModIFied	
					  , TemplateBackgroundColor AS templateBgColor 
					  --, NoOfColumns				                
		        FROM #MenuItems M
				INNER JOIN #Sort S ON M.mItemID=S.MenuitemsID 
				WHERE @Count!=@MaxCnt 
			    ORDER BY GroupName ,SortA ASC
						
			END
			
		   ELSE	IF @SortOrder ='DESC' AND (@TemplateName1 = 'Grouped Tab' OR  @TemplateName1 = 'Combo Template' OR  @TemplateName1 = 'Grouped Tab With Image')
			BEGIN	
			
				 SELECT RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					  --,LinkTypeName			
					  --,LinkTypeID
					  --,LinkID
					  ,LinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
													  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
													  WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID ) = 1 AND M.LinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																						   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																						   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																						   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
												WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociatiON 
													  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																							   INNER JOIN HcMenuItemEventCategoryAssociatiON MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID
																																							   WHERE HcMenuItemID = M.mItemID AND HcHubCitiID =@HubCitiID)
										   ELSE LinkTypeName END)
						,LinkTypeID
						,LinkID =  CASE WHEN M.LinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID) 
										  WHEN M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																			 INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
																			 WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 AND M.LinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
																																							INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
																																							WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) 
										  WHEN M.LinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcMenuItemEventCategoryAssociatiON 
																			  WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID) = 1 THEN (SELECT HcEventCategoryID FROM HcMenuItemEventCategoryAssociatiON
																																							WHERE HcMenuItemID = M.mItemID AND HcHubCitiID = @HubCitiID)	

									 ELSE LinkID END 
					  ,PositiON	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,	GroupName 	
					  , SortD = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') AND @SortOrder ='DESC' THEN 'Z'+ mItemName ELSE mItemName END
					  , SortA = CASE WHEN (LinkTypeName = 'Text' OR LinkTypeName = 'Label') AND @SortOrder ='ASC' THEN ' ' ELSE mItemName END		
					  , mGrpBkgrdColor 
					  , mGrpFntColor                        
					  , smGrpBkgrdColor 
					  , smGrpFntColor 
					  , MenuName
					  , mFontColor
					  , smFontColor	
					  , dateModIFied
					  , TemplateBackgroundColor	AS templateBgColor 	                 
					  --, NoOfColumns
		        FROM #MenuItems M
				INNER JOIN #Sort S ON M.mItemID=S.MenuitemsID  
				WHERE @Count!=@MaxCnt
				ORDER BY GroupName desc ,SortD Desc				
			END			    
						 
           ELSE
		   BEGIN
				 SELECT RowNum  
					  ,MenuID			
					  ,HubCitiId
					  ,templateName
					  ,Level
					  ,mItemID
					  ,mItemName
					  --,LinkTypeName	= 'a'		
					  --,LinkTypeID
					  --,LinkID = '1'
					 ,LinkTypeName = IIF((SELECT COUNT(DISTINCT A.BusinessCategoryID)  FROM HcMenuFindRetailerBusinessCategories A 
										INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
										WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID) = 1 , 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcMenuFindRetailerBusinessCategories A 
																																		   INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																		   INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																		   WHERE HcMenuItemID = M.mItemID AND M.HubCitiID =@HubCitiID), 
								     LinkTypeName)
					,LinkTypeID
					,LinkID = IIF((M.LinkTypeName IN ('Filters','City Experience')),
											(SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID),
								IIF( ( M.LinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcMenuFindRetailerBusinessCategories A 
																	INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID = A.BusinessCategoryID 
																	WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID) = 1),
										
															(SELECT DISTINCT A.BusinessCategoryID FROM HcMenuFindRetailerBusinessCategories A 
															INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID = A.BusinessCategoryID 
															WHERE HcMenuItemID = M.mItemID AND M.HubCitiID = @HubCitiID)
									 ,LinkID)) 	
					  ,PositiON	
					  ,mItemImg
					  ,mBkgrdColor
					  ,mBkgrdImage
					  ,mBtnColor
					  ,mBtnFontColor
					  ,smBkgrdColor
					  ,smBkgrdImage 		  
					  ,smBtnColor
					  ,smBtnFontColor   
					  ,HcDepartmentID
					  ,HcMenuItemTypeID			
					  ,departmentName
					  ,mItemTypeName
					  ,HcMenuItemShapeID mShapeId
				      ,HcMenuItemShape mShapeName
					  ,mGrpBkgrdColor 
					  ,mGrpFntColor                        
					  ,smGrpBkgrdColor 
					  ,smGrpFntColor 
					  ,MenuName
					  ,mFontColor
					  ,smFontColor	
					  ,dateModIFied
					  ,TemplateBackgroundColor AS templateBgColor 
					  --,NoOfColumns							                
		        FROM #MenuItems M	
				WHERE @Count!=@MaxCnt			 
				ORDER BY CASE WHEN ISNULL(@SortOrder, 'NONe') = 'NONE' THEN PositiON END ASC,
				         CASE WHEN @SortOrder = 'ASC' AND (TemplateName = 'Two Column Tab' OR  TemplateName = 'Two Column Tab with Banner Ad') AND LinkTypeName = 'City Experience' THEN '0' ELSE mItemName END ASC, 
			             CASE WHEN @SortOrder LIKE 'DESC' AND (TemplateName = 'Two Column Tab' OR  TemplateName = 'Two Column Tab with Banner Ad') AND LinkTypeName = 'City Experience' THEN '0' ELSE mItemName END DESC						
			END	
				
				        -- CASE WHEN @SortOrder = 'DESC' AND (TemplateName = 'Combo Template') AND LinkTypeName = 'Text' THEN '0' ELSE mItemName END DESC
				         
				   
			       -- CASE WHEN @SortOrder = 'ASC' AND (TemplateName = 'Two Column Tab' OR  TemplateName = 'Two Column Tab with Banner Ad') AND LinkTypeName = 'City Experience' THEN '0' ELSE mItemName END ASC, 
			       -- CASE WHEN @SortOrder LIKE 'DESC' AND (TemplateName = 'Two Column Tab' OR  TemplateName = 'Two Column Tab with Banner Ad') AND LinkTypeName = 'City Experience' THEN '0' ELSE mItemName END DESC,
			       ---- CASE WHEN @SortOrder = 'ASC' AND (TemplateName = 'Grouped Tab' OR  TemplateName = 'Combo Template') AND LinkTypeName = 'Text' THEN '0'  ELSE '1' END ASC,mItemName Asc,
			       --  CASE WHEN ISNULL(@SortOrder, 'NONe') = 'NONE' THEN PositiON END ASC
					
			SELECT DISTINCT HM.HcMenuID as MenuID			
				 , BB.HcBottomButtONID as bottomBtnID
				 , BottomButtONName as bottomBtnName
				 , bottomBtnImg = IIF(BottomButtONImage_ON IS NOT NULL,@Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtONImage_ON,@Config + HcBottomButtONImageIcON)
				 
				 --CASE WHEN BottomButtONImage_ON IS NOT NULL THEN @Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtONImage_ON ELSE @Config + HcBottomButtONImageIcON END 
				 , bottomBtnImgOff = IIF(BottomButtONImage_Off IS NOT NULL,@Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtONImage_Off , @Config + HcBottomButtONImageIcON_Off)
				 --CASE WHEN BottomButtONImage_Off IS NOT NULL THEN @Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtONImage_Off ELSE @Config + HcBottomButtONImageIcON_Off END 
				 --, BottomButtONLinkTypeName as btnLinkTypeName
				 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtONFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButONID = BB.HcBottomButtONID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BT.BottomButtONLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtONFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButONID = BB.HcBottomButtONID AND HM.HcHubCitiID =@HubCitiID)
										  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtONEventCategoryAssociatiON 
								                WHERE HcBottomButtONID = BB.HcBottomButtONID AND HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																					            INNER JOIN HcBottomButtONEventCategoryAssociatiON BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																					            WHERE HcBottomButtONID = BB.HcBottomButtONID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
			                       ELSE BottomButtONLinkTypeName END)
				 , BottomButtONLinkTypeID as btnLinkTypeID				
				 --, btnLinkID = IIF(BT.BottomButtONLinkTypeName = 'Filters', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID),
					--						IIF(BT.BottomButtONLinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtONEventCategoryAssociatiON 
					--																		 WHERE HcBottomButtONID = BB.HcBottomButtONID AND HcHubCitiID = @HubCitiID) = 1,
					--											(SELECT HcEventCategoryID FROM HcBottomButtONEventCategoryAssociatiON
					--											 WHERE HcBottomButtONID = BB.HcBottomButtONID AND HcHubCitiID = @HubCitiID), BottomButtONLinkID))

				 , btnLinkID = (CASE WHEN BT.BottomButtONLinkTypeName IN ('Filters','City Experience') THEN (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID)
									 WHEN BT.BottomButtONLinkTypeName = 'Find' AND (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtONFindRetailerBusinessCategories A 
								                                     INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
								                                     WHERE HcBottomButONID = BB.HcBottomButtONID AND HM.HcHubCitiID =@HubCitiID) = 1 AND BT.BottomButtONLinkTypeName <> 'Dining' THEN (SELECT DISTINCT A.BusinessCategoryID FROM HcBottomButtONFindRetailerBusinessCategories A 
								                                                                                                                    INNER JOIN RetailerBusinessCategory B ON B.BusinessCategoryID =A.BusinessCategoryID 
								                                                                                                                    WHERE HcBottomButONID = BB.HcBottomButtONID AND HM.HcHubCitiID =@HubCitiID) 
								  
									WHEN BT.BottomButtONLinkTypeName = 'Events' AND (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtONEventCategoryAssociatiON 
																					 WHERE HcBottomButtONID = BB.HcBottomButtONID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN (SELECT DISTINCT HcEventCategoryID FROM HcBottomButtONEventCategoryAssociatiON
																																										  WHERE HcBottomButtONID = BB.HcBottomButtONID AND HcHubCitiID = @HubCitiID)
								ELSE BottomButtONLinkID	END)													 
				 , BM.PositiON as positiON		
				-- , HM.DateCreated dateModIFied		
				-- , CASE WHEN HcBottomButtONImageIcONID IS NOT NULL THEN HcBottomButtONImageIcONID ELSE @Config+CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtONImage	END	HcBottomButtONImageIcON	
			FROM #Menuitemss HM
			--HcMenu HM
			--INNER JOIN HcMenuItem MI ON MI.HCMenuID = HM.HCMenuID 

			INNER JOIN HcMenuBottomButtON BM ON BM.HcMenuID=HM.HcMenuID
			INNER JOIN HcBottomButtON BB ON BB.HcBottomButtONID=BM.HcBottomButtONID
			INNER JOIN HcBottomButtONLinkType BT ON BT.HcBottomButtONLinkTypeID=BB.BottomButtONLinkTypeID
			INNER JOIN HcHubciti HC ON HC.HcHubcitiID=HM.HcHubcitiID 
			LEFT JOIN HcBottomButtONImageIcONs BI ON BI.HcBottomButtONImageIcONID=BB.HcBottomButtONImageIcONID
			--LEFT JOIN HubCitiReportingDatabase.dbo.RequestPlatforms RP ON RP.RequestPlatformtype=@RequestPlatformtype
			WHERE ((@LinkID IS NOT NULL AND HM.HcMenuID=@LinkID) OR (@LinkID IS NULL AND @LevelID=Level AND @HubCitiID=HM.HcHubCitiID )) 
			ORDER BY positiON				
			
			----------------Two Image Template Changes 3/2/2106---------------------
										
			SELECT  @TempleteBackgroundImage= @Config+ CAST(H.HCHubcitiID AS VARCHAR(100))+'/'+CAST(H.TemplateBackgroundImage AS VARCHAR(100)) --@Config
			       ,@DisplayLabel=H.DisplayLabel
				   ,@LabelBckGndColor = H.LabelBackGroundColor
				   ,@LabelFontColor = H.LabelFontColor
			FROM HcMenu H
			WHERE HcHubCitiID=@HubCitiID AND (@LinkID IS NULL AND Level =1 OR (@LinkID IS NOT NULL AND @LinkID =h.HcMenuID )) 
			---------------------------------END--------------------------
			------------- Custom NavigatiON Bar changes -------------------------------
			DECLARE @ServerConfig VARCHAR(500)

			SELECT @ServerConfig = ScreenCONtent FROM AppConfiguratiON WHERE ConfiguratiONType= 'Hubciti Media Server ConfiguratiON'
			
			SELECT	@homeImgPath = 	CASE WHEN HU.homeIcONName IS NULL THEN @ServerConfig+'customhome.png' ELSE  @Config +CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIcONName END
					 ,@bkImgPath = 	CASE WHEN HU.backButtONIcONName IS NULL THEN @ServerConfig+'customback.png' ELSE @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtONIcONName END
					 ,@titleBkGrdColor =  ISNULL( HU.backGroundColor ,'#000000')
					 ,@titleTxtColor =  ISNULL(HU.titleColor ,'#ffffff')
				FROM HcHubCiti HC
				LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
				LEFT JOIN HcBottomButtONTypes HB ON HU.HcBottomButtONTypeID = HB.HcBottomButtONTypeId			
				WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					 
			
			-------------------------------------------------------------------------

			IF ISNULL(@LevelID,1)=1
					BEGIN
						SELECT @TemplateName = ISNULL(T.TemplateName,0)
						,@ModifiedDate = CONVERT(DATETIME,H.DateCreated)
						FROM HcTemplate  T 
						INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
						WHERE HcHubCitiID = @HubCitiID AND Level=1       
					END
					ELSE
					BEGIN
						SELECT @TemplateName = ISNULL(T.TemplateName,0)
					,@ModifiedDate = CONVERT(DATETIME,HM.DateCreated)
					FROM HcTemplate  T 
					INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
					INNER JOIN HcMenuItem HM ON HM.HcMenuID=H.HcMenuID
					WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

					END

			 	-- UPDATE HCNewsFirstTempFlag SET NewsFlag=@Flag WHERE HcHubCitiID=@HubCitiID

					UPDATE HcNewsFirstTempFlag  
					SET NewsFlag=@Flag,linkid=@LinkID
					WHERE  HcHubCitiID=@HubCitiID
					AND ISNULL(level,1)=case when @LevelID=1 then '1' ELSE 0 END
					 AND ISNULL(linkid,0)=ISNULL(@LinkID,0)
					SELECT @Status=0
			END
			ELSE
			BEGIN
					IF ISNULL(@LevelID,1)=1
					BEGIN
						SELECT @TemplateName = ISNULL(T.TemplateName,0)
								,@ModifiedDate = CONVERT(DATETIME,H.DateCreated)
						FROM HcTemplate  T 
						INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
						WHERE HcHubCitiID = @HubCitiID AND Level=1       
					END
					ELSE
					BEGIN
						SELECT @TemplateName = ISNULL(T.TemplateName,0)
								,@ModifiedDate = CONVERT(DATETIME,HM.DateCreated)
						FROM HcTemplate  T 
						INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
						INNER JOIN HcMenuItem HM ON HM.HcMenuID=H.HcMenuID
						WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID
					END

					IF @FlagExist1  IS NULL
					BEGIN
						IF @LevelID=1
							BEGIN 
								UPDATE HcNewsFirstTempFlag  
								SET NewsFlag=@Flag,linkid=@LinkID
								WHERE  HcHubCitiID=@HubCitiID
								AND ISNULL(level,1)=case when @LevelID=1 then '1' ELSE 0 END
								-- AND ISNULL(linkid,0)=ISNULL(@LinkID,0)
							END
					END

				    UPDATE HcNewsFirstTempFlag  
					SET NewsFlag=@Flag,linkid=@LinkID
					WHERE  HcHubCitiID=@HubCitiID
					AND ISNULL(level,1)=case when @LevelID=1 then '1' ELSE 0 END
					AND ISNULL(linkid,0)=ISNULL(@LinkID,0)
			END
			SELECT @Status=0	 	      
			IF @TemplateChanged = 1 AND @Flag=1
			SET @NoRecordsMsg=NULL	
			      
	  END

	  --SELECT @TemplateChanged,@TemplateName,@ModifiedDate

	  --SET @ENDTime = GETDATE()	  
	  --SET @DIFfTime = (SELECT CONVERT(VARCHAR(12), DATEADD(MS, DATEDIFF(MS,@StartTime,@ENDTime), 0), 114))

	  --DROP TABLE RespONSETime
	  --CREATE TABLE RespONSETime(StartTime DATETIME,ENDtime DATETIME,DIFfTime varchar(100))

	  --INSERT INTO RespONSETime(StartTime,ENDtime,DIFfTime)
	  --SELECT @StartTime,@ENDTime,@DIFfTime

	END TRY
		
	BEGIN CATCH
	  
		--Check whether the TransactiON IS uncommitable.
		IF @@ERROR <> 0
		BEGIN	
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiMenuDISplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--CONfirmatiON of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
