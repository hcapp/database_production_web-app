USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcCityExperiencePreferredCityList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcCityExperiencePreferredCityList]
Purpose					: To display list of Cities which has Retailers for given HubCitiID(RegionApp).
Example					: [usp_HcCityExperiencePreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  9/30/2014       SPAN              1.0
---------------------------------------------------------------
*/

CREATE  PROCEDURE [HubCitiApp2_8_2].[usp_HcCityExperiencePreferredCityList]
(
   
    --Input variable	  
	  @UserID int	
	, @HcHubCitiID int
	, @CityExperienceID int
	, @CategoryID Varchar(100)  --Comma Separated Category IDs
    , @SearchKey varchar(255)

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			

			DECLARE @UserID1 int = @UserID
			DECLARE @HcHubCitiID1 int = @HcHubCitiID
			DECLARE @CityExperienceID1 int = @CityExperienceID

			DECLARE @RegionAppID int
			DECLARE @HcAppListID int

			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiID = @HcHubCitiID1

			DECLARE @Tomorrow DATETIME = GETDATE() + 1
			DECLARE @Yesterday DATETIME = GETDATE() - 1


	

			  -- To identify Retailer that have products on Sale or any type of discount
                 create  table #RetailItemsonSale(RetailID  bigint,RetailLocationID bigint)

				 insert  into #RetailItemsonSale
				 SELECT b.RetailID, a.RetailLocationID 
                    FROM RetailLocationDeal a 
                    INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID AND b.Active = 1
                    INNER JOIN HcLocationAssociation HL ON b.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = B.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                               
                    INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                                                            and a.ProductID = c.ProductID
                                                                            and GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) and ISNULL(a.SaleEndDate, @Tomorrow)
                    insert  into #RetailItemsonSale 
                    SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                    FROM Coupon C 
                    INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
                    INNER JOIN RetailLocation RL ON RL.RetailID = CR.RetailID AND RL.Active = 1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                   
                    LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                    WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                    GROUP BY C.CouponID
                                ,NoOfCouponsToIssue
                                ,CR.RetailID
                                ,CR.RetailLocationID
                    HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                                ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                          
                     insert  into #RetailItemsonSale

                    select  RR.RetailID, 0 as RetaillocationID  
                    from Rebate R 
                    INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
                    INNER JOIN RetailLocation RL ON RL.RetailID = RR.RetailID ANd RL.Active = 1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                                
                    WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

                     insert  into #RetailItemsonSale  

                    SELECT  c.retailid, a.RetailLocationID 
                    FROM  LoyaltyDeal a
                    INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
                    INNER JOIN RetailLocation c on a.RetailLocationID = c.RetailLocationID AND c.Active = 1
                    INNER JOIN HcLocationAssociation HL ON c.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1              
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = C.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    INNER JOIN RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
                                                                    and b.ProductID = LDP.ProductID 
                    WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                    insert  into #RetailItemsonSale 

                    SELECT DISTINCT rl.RetailID, rl.RetailLocationID
                    FROM ProductHotDeal p
                    INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                    INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active = 1 
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                                                                                   
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                    LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                    WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
                    GROUP BY P.ProductHotDealID
                                ,NoOfHotDealsToIssue
                                ,rl.RetailID
                                ,rl.RetailLocationID
                    HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                                ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                    insert  into #RetailItemsonSale

                    select q.RetailID, qa.RetailLocationID
                    from QRRetailerCustomPage q
                    INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                    INNER JOIN RetailLocation RL ON RL.RetailLocationID=qa.RetailLocationID AND RL.Active =1
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                     
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                    where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
                   -- ) Discount    
                     

			 

				--To check whether user has preferred cities are not.
				DECLARE @UserPreferredCity bit 
				SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID1 AND HcCityID IS NULL THEN 0
													WHEN HcUserID = @UserID1 AND HcCityID IS NOT NULL THEN 1
													ELSE 0 END
				FROM HcUsersPreferredCityAssociation
				WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1

				SELECT @UserPreferredCity = ISNULL(@UserPreferredCity,0)

				DECLARE @GuestUser int
				DECLARE @CityPrefNotVisitedUser bit

				SELECT @GuestUser = U.HcUserID
				FROM HcUser U
				INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
				WHERE UserName = 'GuestLogin'
				AND DA.HcHubCitiID = @HcHubCitiID1

			
				IF NOT EXISTS (SELECT HcUserID FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1)
				BEGIN
					SET @CityPrefNotVisitedUser = 1
				END
				ELSE
				BEGIN
					SET @CityPrefNotVisitedUser = 0

				END

				SELECT Param into #cat FROM [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ',')

				
				--select * into RetailItemsonSale from #RetailItemsonSale

				CREATE  TABLE #Retail(HcCityID  BIGINT,CityName VARCHAR(500))
				--To get a list of City names
				
						INSERT INTO #Retail
						SELECT DISTINCT  C.HcCityID
							            , C.CityName										
						FROM Retailer R    
						INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND R.RetailerActive = 1 AND RL.Active = 1  
						INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND RL.City = HL.City 
						INNER JOIN HcCity C ON HL.HcCityID = C.HcCityID                               
						INNER JOIN HcCityExperienceRetailLocation CE ON CE.RetailLocationID = RL.RetailLocationID AND Headquarters = 0                                                                                  
						INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND @UserPreferredCity = 0 
						AND CE.HcCityExperienceID = @CityExperienceID
						AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1))
								--OR
							--(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ',')))
							
						--AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END
						
						if ISNULL(@CategoryID, 0) <> 0
						begin
						INSERT INTO #Retail
                       SELECT DISTINCT  C.HcCityID
							            , C.CityName										
						FROM Retailer R    
						INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND R.RetailerActive = 1 AND RL.Active = 1  
						INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND RL.City = HL.City 
						INNER JOIN HcCity C ON HL.HcCityID = C.HcCityID                               
						INNER JOIN HcCityExperienceRetailLocation CE ON CE.RetailLocationID = RL.RetailLocationID AND Headquarters = 0                                                                                  
						INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						inner join #cat on #cat.Param=RB.BusinessCategoryID
						WHERE (@RegionAppID = 0 OR @RegionAppID = 1) AND @UserPreferredCity = 0
						 AND CE.HcCityExperienceID = @CityExperienceID
						--AND-- ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1)
							--	OR
							--(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM #cat))
						--AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END
						end

							

						INSERT INTO #Retail

						SELECT DISTINCT  C.HcCityID
							            , C.CityName										
						FROM HcHubCiti H
						INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
						INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
						INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID
						 AND UC.HcHubcitiID = @HcHubCitiID						
						INNER JOIN RetailLocation RL ON LA.PostalCode =RL.PostalCode AND Headquarters = 0 AND RL.City = LA.City 
						INNER JOIN Retailer R ON RL.RetailID = R.RetailID AND RL.Active = 1 AND R.RetailerActive = 1   
						INNER JOIN HcCityExperienceRetailLocation CER ON CER.RetailLocationID = RL.RetailLocationID 
						AND CER.HcCityExperienceID = @CityExperienceID                                                                                    
						INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						WHERE @RegionAppID = 1 AND @UserPreferredCity = 1 AND H.HcHubCitiID  = @HcHubCitiID
						AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1))
									--	OR
						--(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ','))))
						AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END

						
							

						if ISNULL(@CategoryID, 0) <> 0
						begin
						INSERT INTO #Retail

						SELECT DISTINCT  C.HcCityID
							            , C.CityName										
						FROM HcHubCiti H
						INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
						INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
						INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HcHubCitiID						
						INNER JOIN RetailLocation RL ON LA.PostalCode =RL.PostalCode AND Headquarters = 0 AND RL.City = LA.City 
						INNER JOIN Retailer R ON RL.RetailID = R.RetailID AND RL.Active = 1 AND R.RetailerActive = 1   
						INNER JOIN HcCityExperienceRetailLocation CER ON CER.RetailLocationID = RL.RetailLocationID AND CER.HcCityExperienceID = @CityExperienceID                                                                                   
						INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
						INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
						LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
						inner join #cat on #cat.Param=RB.BusinessCategoryID
						WHERE @RegionAppID = 1 AND @UserPreferredCity = 1 AND H.HcHubCitiID  = @HcHubCitiID
						--AND ((ISNULL(@CategoryID, 0) = 0 AND 1 = 1))
						--and (ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM #cat))
								--		OR
						--(ISNULL(@CategoryID, 0) <> 0 AND RB.BusinessCategoryID IN (SELECT Param FROM [HubCitiApp2_8_2].fn_SplitParam(@CategoryID, ','))))
						AND R.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END
						
					--	)Retailer
					ORDER BY CityName ASC
					end

					

					--To display list of cities in sorting
					SELECT DISTINCT HcCityID 
							       , CityName								   
					INTO #IsCityChecked 
					FROM #Retail
					
					--select * from #IsCityChecked
					SELECT HcCityID
					      ,cityname
						  ,isCityChecked
				    INTO #Final
					FROM(
					--SELECT DISTINCT I.HcCityID 
					--			   ,CityName
					--			  --, isCityChecked = CASE WHEN (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1) THEN 1
					--					--			WHEN UC.HcUsersPreferredCityAssociationID IS NOT NULL THEN 1
					--					--			WHEN UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1 THEN 1						
					--					--			ELSE 0 END
					--				, isCityChecked = CASE WHEN (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1) THEN 1
					--								WHEN UC.HcUsersPreferredCityAssociationID IS NOT NULL AND UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1 THEN 1
																	
					--								ELSE 0 END

					--FROM #IsCityChecked I					
					--LEFT JOIN HcUsersPreferredCityAssociation UC ON  ((I.HcCityID = UC.HcCityID OR UC.HcCityID IS NULL) AND UC.HcHubcitiID = @HcHubCitiID1 AND UC.HcUserID = @UserID1) OR (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1)
					
					--UNION ALL

					SELECT DISTINCT   C.HcCityID
								    , C.CityName				
									, isCityChecked = CASE WHEN ((SELECT 1 FROM #IsCityChecked WHERE CityName = C.CityName)=1) THEN 1
													ELSE 0 END	
					--FROM Retailer R    
					--	  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID   
					--	  INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND  HL.HcHubCitiID = @HcHubCitiID1
					--	  INNER JOIN HcCity C ON HL.City = C.CityName                                   
					--	  INNER JOIN HcCityExperienceRetailLocation CE ON CE.RetailLocationID = RL.RetailLocationID AND Headquarters = 0                                                                                  
					--	  INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
					--	  INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
					--	  LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
					--	  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
					FROM HcHubCiti H
					INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID AND  LA.HcHubCitiID = @HcHubCitiID1
					INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
					LEFT JOIN HcUsersPreferredCityAssociation UP on C.HcCityID = UP.HcCityID 
					INNER JOIN RetailLocation RL ON LA.PostalCode =RL.PostalCode AND Headquarters = 0 
					INNER JOIN Retailer R ON RL.RetailID = R.RetailID 
					INNER JOIN HcCityExperienceRetailLocation CER ON CER.RetailLocationID = RL.RetailLocationID AND CER.HcCityExperienceID = @CityExperienceID1                                                                                    
					INNER JOIN RetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
					INNER JOIN BusinessCategory BC ON BC.BusinessCategoryID = RB.BusinessCategoryID 
					LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
					WHERE  @RegionAppID = 1 AND H.HcHubCitiID  = @HcHubCitiID1  --AND C.HcCityID NOT IN (SELECT HcCityID FROM HcUsersPreferredCityAssociation
					--WHERE HcUserID=@UserID1 and HcHubcitiID=@HcHubCitiID1)
					AND R.RetailerActive = 1 AND RL.Active = 1
					
					)F
					
                    SELECT DISTINCT HcCityID AS cityId
					      ,CityName
						  ,isCityChecked
					FROM #final
					ORDER BY CityName

	       --Confirmation of Success.
		   SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcCityExperiencePreferredCityList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;































GO
