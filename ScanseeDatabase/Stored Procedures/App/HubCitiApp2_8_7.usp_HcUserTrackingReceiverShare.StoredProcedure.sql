USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingReceiverShare]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserTrackingReceiverShare
Purpose					: To track shared page information and the respective receiver user details.
Example					: usp_HcUserTrackingReceiverShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th Jan 2015	Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingReceiverShare]
(
	  
	  @UserID int
	, @QRTypeCode int  
	, @PageID int  
	, @HubCitiID int
	, @RetailID int
	, @RetailLocationID int
    
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @QRTypeName varchar(500)

			SELECT @QRTypeName = QRTypeName
			FROM QRTypes
			WHERE QRTypeCode = @QRTypeCode

			IF @QRTypeName = 'Product Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverProductShare(UserID
										,QRTypeCode
										,ProductID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'Main Menu Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverAppsiteShare(UserID
										,QRTypeCode
										,RetailLocationID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'Anything Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverAnythingShare(UserID
										,QRTypeCode
										,AnythingPageID
										,HubCitiID 
										,DateCreated
										,RetailID
										,RetailLocationID)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()
									  ,@RetailID
									  ,@RetailLocationID	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'Special Offer Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverSpecialOfferShare(UserID
										,QRTypeCode
										,SpecialOfferID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'Event Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverEventShare(UserID
										,QRTypeCode
										,EventID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'Fundraiser Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverFundraiserShare(UserID
										,QRTypeCode
										,FundraiserID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'HotDeal Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverHotDealShare(UserID
										,QRTypeCode
										,HotDealID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE IF @QRTypeName = 'Coupon Page'
			BEGIN
				INSERT INTO HubCitiReportingDatabase..ReceiverCouponShare(UserID
										,QRTypeCode
										,CouponID
										,HubCitiID 
										,DateCreated)
								SELECT @UserID
									  ,@QRTypeCode
									  ,@PageID
									  ,@HubCitiID
									  ,GETDATE()	
				SET @Status = 0	
			END
			ELSE
			BEGIN
				SET @Status = 1
			END
													
	    
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserTrackingReceiverShare.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 

			SET @Status = 1
			
		END;
		 
	END CATCH;
END;



 




























GO
