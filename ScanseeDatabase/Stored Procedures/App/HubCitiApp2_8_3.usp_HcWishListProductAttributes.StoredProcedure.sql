USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcWishListProductAttributes]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcWishListProductAttributes  
Purpose               : To dispaly Product Attributes in Wish List  
Example               : usp_HcWishListProductAttributes  
  
History  
Version       Date             Author          Change Description  
---------------------------------------------------------------   
1.0           20thNov2013      Dhananjaya TR   
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcWishListProductAttributes]  
(  
 @ProductID int  
   
 --Output Variable   
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
   --To get Media Server Configuration. 
		 DECLARE @ManufConfig varchar(50) 
		  
		 SELECT @ManufConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
  -- To capture the existance of the media for the specified product.  
            DECLARE @VideoFlag bit  
            DECLARE @AudioFlag bit  
            DECLARE @FileFlag bit  
            SELECT @VideoFlag = SUM(CASE WHEN PT.ProductMediaType = 'Video Files' THEN 1 ELSE 0 END)  
                  , @AudioFlag = SUM(CASE WHEN PT.ProductMediaType = 'Audio Files' THEN 1 ELSE 0 END)  
                  , @FileFlag = SUM(CASE WHEN PT.ProductMediaType = 'Other Files' THEN 1 ELSE 0 END)  
            FROM ProductMedia PM  
            INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
            WHERE PM.ProductID = @ProductID 
             
   IF @ProductID in (SELECT ProductID FROM ProductMedia P 
						INNER JOIN ProductMediaType PM on P.ProductMediaTypeID =PM.ProductMediaTypeID WHERE ProductMediaType='Image Files')
	   BEGIN
			   --To get Product Info       
			   SELECT P.ProductID   
				, P.ProductName   
				, CASE WHEN  p.ProductLongDescription = P.ProductShortDescription THEN NULL ELSE P.ProductLongDescription END ProductLongDescription
				, P.ProductShortDescription
				, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																										THEN @ManufConfig
																										+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																										+ProductImagePath ELSE ProductImagePath 
																								  END   
									  ELSE ProductImagePath END          
				, WarrantyServiceInformation warrantyServiceInfo
				, ModelNumber
				, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																										THEN @ManufConfig
																										+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																										+ProductMediaPath  END
									  ELSE ProductMediaPath END                    
				--, PA.AttributeName  
				--, PA.DisplayValue  
				, VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
							, AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
							, FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END  
			    
			   FROM Product P  
			   INNER JOIN ProductMedia PM ON P.ProductID = PM.ProductID
			   INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID 
			   --LEFT JOIN ProductAttributes PA ON PA.ProductID = P.ProductID
			   --LEFT JOIN Attribute A ON A.AttributeId = PA.AttributeID  
			   WHERE P.ProductID  = @ProductID AND  ProductMediaType='Image Files'  
	  END
	   
	  ELSE
		BEGIN
				SELECT P.ProductID   
				, P.ProductName   
				, CASE WHEN  p.ProductLongDescription = P.ProductShortDescription THEN NULL ELSE P.ProductLongDescription END ProductLongDescription   
				, P.ProductShortDescription
				, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																										THEN @ManufConfig
																										+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																										+ProductImagePath ELSE ProductImagePath 
																								  END   
									  ELSE ProductImagePath END          
				, WarrantyServiceInformation warrantyServiceInfo
				, ModelNumber
				, NULL ProductMediaPath
				--, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
							--																			THEN @ManufConfig
							--																			+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
							--																			+ProductMediaPath  END
				--                      ELSE ProductMediaPath END                    
				--, PA.AttributeName  
				--, PA.DisplayValue  
				, VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
				, AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
				, FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END  
			    
			   FROM Product P  
			   --LEFt JOIN ProductMedia PM ON P.ProductID = PM.ProductID
			   --LEFT JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
			   --LEFT JOIN ProductAttributes PA ON PA.ProductID = P.ProductID
			   --LEFT JOIN Attribute A ON A.AttributeId = PA.AttributeID  
			   WHERE P.ProductID  = @ProductID 
		END	 
     
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure <>.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
     
  END;  
     
 END CATCH;  
END;













































GO
