USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcCityList]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcCityList
Purpose					: To display list of Cities.
Example					: usp_HcCityList

History
Version		    Date		   Author		Change Description
--------------------------------------------------------------- 
1.0			9/11/2014	      SPAN  		 1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcCityList]
(
   
    --Input variable 	  
	  @HcHubcitiID int
	, @UserID int
  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		 DECLARE @RegionAppID int
		 DECLARE @GuestUserID int

		 SELECT @RegionAppID = HcAppListID
		 FROM HcAppList
		 WHERE HcAppListName = 'RegionApp'

		 SELECT @GuestUserID = HcUserID
		 FROM HcUser
		 WHERE UserName = 'guestlogin'

		 --To display list of all cities associated to given RegionApp.
		 IF EXISTS (SELECT 1 FROM HcUsersPreferredCityAssociation WHERE HcUserID = @UserID AND HcCityID IS NOT NULL)
		 BEGIN
				
				SELECT DISTINCT  C.HcCityID cityId
									,C.CityName cityName
									,isCityChecked = CASE WHEN UC.HcUserID = @UserID AND UC.HcCityID IS NOT NULL THEN 1
														ELSE 0
													END
					FROM HcHubCiti H
					INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
					INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
					LEFT JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HcHubcitiID
					WHERE H.HcHubCitiID  = @HcHubcitiID AND H.HcAppListID = @RegionAppID
					ORDER BY C.CityName

		 END

		 ELSE
		 BEGIN
				 
				SELECT DISTINCT  C.HcCityID cityId
									,C.CityName cityName
									,isCityChecked = CASE WHEN @UserID IS NULL OR @UserID = @GuestUserID THEN 1
														WHEN UC.HcUserID = @UserID AND UC.HcCityID IS NULL THEN 1
														WHEN @UserID <> UC.HcUserID THEN 1
														ELSE 1
													END
					FROM HcHubCiti H
					INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
					INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
					LEFT JOIN HcUsersPreferredCityAssociation UC ON  UC.HcHubcitiID = @HcHubcitiID
					WHERE H.HcHubCitiID  = @HcHubcitiID AND H.HcAppListID = @RegionAppID
					ORDER BY C.CityName

		 END


	    --Confirmation of Success.
		SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [HubCitiApp2_3_3].[usp_HcCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




























GO
