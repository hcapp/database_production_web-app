USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHotDealShare]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHotDealShare
Purpose					: 
Example					: usp_HcHotDealShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13thOct 2013    Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHotDealShare]
(
	  @HotDealID int
	, @HcHubcitiID int 
	, @UserID int
	--Output Variable 
	--, @HotDealExpired bit output
	, @ShareText varchar(500) output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			--Retrieve the server configuration.
			DECLARE @Config varchar(100)
			DECLARE @RetailConfig varchar(50)
			DECLARE @ManufConfig varchar(50)

            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 

			--To get the text used for share.         
            DECLARE @HubCitiName varchar(100)
		 
			SELECT @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiID = @HcHubcitiID
		       
			SELECT @ShareText = 'I found this HotDeal in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
            --FROM AppConfiguration 
            --WHERE ConfigurationType LIKE 'HubCiti HotDeal Share Text'  
			
			--To get server Configuration.
            SELECT @RetailConfig= ScreenContent  
            FROM AppConfiguration   
            WHERE ConfigurationType='Web Retailer Media Server Configuration'  

			SELECT @ManufConfig=(select ScreenContent  
						 FROM AppConfiguration   
						 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')

			--SET @HotDealExpired = 0

			SELECT  HotDealName hotDealName
				   --,HotDealURL hdURL
				   --,HotDealStartDate hDStartDate
				   --,HotDealEndDate hDEndDate
				   ,hotDealImagePath = CASE WHEN HotDealImagePath IS NOT NULL THEN   
														CASE WHEN WebsiteSourceFlag = 1 THEN 
																	CASE WHEN ManufacturerID IS NOT NULL THEN @ManufConfig  
																		+CONVERT(VARCHAR(30),ManufacturerID)+'/'  
																	   +HotDealImagePath  
																	ELSE @RetailConfig  
																	+CONVERT(VARCHAR(30),RetailID)+'/'  
																	+HotDealImagePath  
																	END  					                         
														ELSE HotDealImagePath  
														END  
								END 
				  --,QRUrl = @Config+'2600.htm?key1='+ CAST(@HotDealID AS VARCHAR(10))
				  ,QRUrl = @Config+'2600.htm?hotdealId='+ CAST(@HotDealID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HcHubCitiId AS VARCHAR(10))
			FROM ProductHotDeal
			WHERE ProductHotDealID=@HotDealID
			--AND GETDATE() Between HotDealStartDate AND HotDealEndDate
			
			--SELECT @HotDealExpired=ISNULL(CASE WHEN GETDATE()>HotDealEndDate Then 0 
			--							WHEN GETDATE()<=HotDealEndDate THEN 0 END, 1) FROM ProductHotDeal
			--WHERE ProductHotDealID=@HotDealID

			SET @Status = 0
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcHotDealShare.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status = 1
			
		END;
		 
	END CATCH;
END;











































GO
