USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserPushNotify]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserPushNotify
Purpose					: To send push notifications to respective users.
Example					: usp_HcUserPushNotify

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			07 Jan 2015		Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserPushNotify]
(
	  --@URL varchar(2000) output
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			
			
		BEGIN TRANSACTION	
		
			--SELECT @URL=(SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType='PushNotifyRSSFeedURL')

			DECLARE @TaylorHubCitiID INT
			SELECT @TaylorHubCitiID=HcHubCitiID FROM HcHubCiti WHERE HubCitiName LIKE 'Tyler'
		
			--To fetch news information.
			SELECT  Title title
				  --,Description mediaText
				  --,ImagePath imgPath
				  --, URL link
				  --, NS.PublishedDate pubDate 		
				  , NT.NewsType	type
				  , NT.NewsLink link			  	  
			FROM HcNewsStaging NS
			INNER JOIN HcNewsType NT ON NS.HcNewsTypeID = NT.HcNewsTypeID AND NT.NewsType IN ('Trending')  

			ORDER BY type desc	

			--To fetch deals information.
			SELECT  TOP 1 S.DealOfTheDayID dealId
				  , S.DealName dealName
				  , D.DealDescription type
				  , S.SpecialsQRURL splUrl
				  , H.HubCitiName hcName
				 -- , D.DealDescription dealDesc
				  ,(CASE WHEN (QT.QRTypeName = 'Special Offer Page' AND S.SpecialsQRURL IS NULL AND QA.URL IS NULL AND D.DealDescription = 'SpecialOffers') THEN QR.RetailID END) AS retailerId   
				  ,(CASE WHEN (QT.QRTypeName = 'Special Offer Page' AND S.SpecialsQRURL IS NULL AND QA.URL IS NULL AND D.DealDescription = 'SpecialOffers') THEN QR.RetailLocationID END)  retailLocationId
			FROM HcDeals D 
			INNER JOIN HcDealOfTheDayStaging S ON D.HcDealsID = S.HcDealsID 
			INNER JOIN HcHubCiti H ON S.HcHubCitiID = H.HcHubCitiID
			LEFT JOIN QRRetailerCustomPageAssociation QR ON QR.QRRetailerCustomPageID = S.DealOfTheDayID
			LEFT JOIN QRRetailerCustomPage QA ON QR.QRRetailerCustomPageID=QA.QRRetailerCustomPageID
			LEFT JOIN QRTypes QT ON QT.QRTypeID=QA.QRTypeID
			LEFT JOIN RetailLocation RL ON RL.Retailid = QR.RetailID AND RL.RetailLocationID = QR.RetailLocationID
			WHERE H.HcHubCitiID IN (@TaylorHubCitiID) AND CAST(Getdate() as date) BETWEEN CAST(S.DealScheduleStartDate as date) AND CAST(S.DealScheduleEndDate as date)
			AND DealScheduleStartDate IS NOT NULL AND DealScheduleEndDate IS NOT NULL
			ORDER BY s.DateCreated desc


			--To store News History
			INSERT INTO HcNews(	Title
								,PublishedDate
								,URL
								,HcNewsTypeID
								,Description
								,ImagePath
								,NotificationSentDate
								,DateCreated
								,DateModified
								,CreatedUserID
								,ModifiedUserID)
						
						SELECT 	Title
								,PublishedDate
								,URL
								,HcNewsTypeID
								,Description
								,ImagePath
								,GETDATE()
								,DateCreated
								,DateModified
								,CreatedUserID
								,ModifiedUserID
					FROM HcNewsStaging 

			 INSERT INTO HcDealOfTheDay(HcHubCitiID
										,DealOfTheDayID
										,DealName
										,DealDescription
										,Price
										,SalePrice
										,DealStartDate
										,DealEndDate
										,HcDealsID
										,DateCreated
										,DateModified
										,CreatedUserID
										,ModifiedUserID)

								SELECT	 HcHubCitiID
										,DealOfTheDayID
										,DealName
										,DealDescription
										,Price
										,SalePrice
										,DealStartDate
										,DealEndDate
										,HcDealsID
										,DateCreated
										,DateModified
										,CreatedUserID
										,ModifiedUserID
							FROM HcDealOfTheDayStaging 
							WHERE HcHubCitiID IN (@TaylorHubCitiID)
							AND cast(DealScheduleEndDate as date) < cast(GETDATE() as date)
							

			--To send pushnotifications to respective devices.
			SELECT DISTINCT UserToken deviceId
				  ,'Android' platform					  		
				  ,HubCitiName hcName
			FROM HcUserToken U
			INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
			WHERE PushNotify = 1 AND H.HcHubCitiID IN (@TaylorHubCitiID) AND HcRequestPlatformID=2
								-- AND (DeviceID = 'c14e4993c33c2fa2' OR DeviceID = '29a3832895c3c01')
			 
			UNION ALL

			SELECT DISTINCT UserToken deviceId
				  ,'IOS' platform					  		
				  ,HubCitiName hcName			 
			FROM HcUserToken U
			INNER JOIN HcHubCiti H ON U.HcHubCitiID = H.HcHubCitiID 
			INNER JOIN HcUserDeviceAppVersion D ON D.DeviceID = U.DeviceID
			WHERE PushNotify = 1 AND H.HcHubCitiID IN (@TaylorHubCitiID) AND U.HcRequestPlatformID=1 --AND D.AppVersion = '2.3.2'
					--AND (U.DeviceID = '8C6A4AD0-FF2F-4BBE-B78F-5C5F18BA9E15')  
					--OR U.DeviceID = 'BAC2571B-BEDE-4FC4-B317-DDDC75E2DB8F'
					--OR U.DeviceID = '5BD163B8-0380-46EB-9E94-4F54028CD80F')
					ORDER BY platform


	--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserPushNotify.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



























GO
