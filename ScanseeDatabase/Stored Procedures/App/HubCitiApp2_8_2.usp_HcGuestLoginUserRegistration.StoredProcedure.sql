USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcGuestLoginUserRegistration]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcGuestLoginUserRegistration]
Purpose					: For Hub Citi Guest login User Registration.
Example					: [usp_HcGuestLoginUserRegistration]

History
Version		  Date			  Author	Change Description
--------------------------------------------------------------- 
1.0		   14th May 2014       SPAN         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcGuestLoginUserRegistration]
(
	
	--Input Variable
	  @HubCitiKey varchar(100)
	, @Platform Varchar(50)

	--Output Variable
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(10) output  
	, @HubCitiID int output
	, @HubCitiName varchar(255) output
	, @FirstUserID int output
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			SET @Result = 0
			
			DECLARE @NearestPostalCode varchar(10)
			DECLARE @RandomPostalCode varchar(10)
			DECLARE @Distance float
			DECLARE @Radius float
			DECLARE @UserName Varchar(50)
			DECLARE @Password Varchar(50)
			DECLARE @AppVersion Varchar(10)
			--DECLARE @DeviceID Varchar(50)
			DECLARE @PostalCode Varchar(10)
			DECLARE @UserLatitude Float
			DECLARE @UserLongitude Float

			SELECT @UserName = '<' + HubCitiName + '>' + '_' + 'GuestLogin' + '_' + 'User'
			FROM HcHubCiti
			WHERE HcHubCitiKey = @HubCitiKey

			SELECT @Password = 'BA3mgdIxHpY='

		   	SELECT @AppVersion = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = CASE WHEN  @Platform = 'IOS' THEN 'CurrentHubCitiAppVersionIOS' 
										   WHEN  @Platform = 'Android' THEN 'CurrentHubCitiAppVersionAndroid'
									  END

			SELECT @PostalCode = DefaultPostalCode
			FROM HcHubCiti
			WHERE HcHubCitiKey = @HubCitiKey

			SELECT  @UserLatitude = Latitude
				   , @UserLongitude = Longitude
			FROM GeoPosition
			WHERE PostalCode = @PostalCode

			SELECT @Radius = ScreenContent
			FROM AppConfiguration
			WHERE ScreenName = 'DefaultRadius'			

			--Derive the hub citi ID and  Name from HubCitiKey.
			SELECT @HubCitiID = HcHubCitiID
				 , @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiKey = @HubCitiKey

			-- Check for existance of the Email Address. 
			--If already found, return -1
			IF EXISTS (SELECT 1 FROM HcUser H
					   INNER JOIN HcUserDeviceAppVersion HD ON H.HcUserID = HD.HcUserID AND HD.HcHubCitiID = @HubCitiID
					   WHERE(UserName = @UserName) AND FacebookAuthenticatedUser = 0)
			BEGIN
				SELECT @Result = -1
			END
			--If not found, insert into Users table. ie,allow user to register.
			ELSE IF @HubCitiID IS NOT NULL
			BEGIN
				INSERT INTO HcUser 
					(
					  UserName
					 ,Password
					 ,FirstUseComplete
					 ,DateCreated
					)
				VALUES
					(
					  @UserName 
					 ,@Password 
					 ,0
					 ,GETDATE() 
					)
				
				DECLARE @UserID INT
				SET @UserID = SCOPE_IDENTITY()
				
				INSERT INTO HcUserDeviceAppVersion(HcUserID
												 , HcHubCitiID
												 --, DeviceID
												 , AppVersion 
												 , DateCreated)
				VALUES(@UserID
					 , @HubCitiID
					 --, @DeviceID
					 , @AppVersion
					 , GETDATE())
				
				INSERT INTO HcUserLocation 
					(
						 HcUserID 
						--,DeviceID
						,UserLongitude
						,UserLatitude
						,PostalCode
						,UserLocationDate
					)
				VALUES
					(
						 @UserID 
						--,@DeviceID 
						,@UserLongitude 
						,@UserLatitude
						,@PostalCode 
						,GETDATE() 
					)
								 
			/*Create a dummy entry for the user & default it 
			to the global radius so that for the first time 
			it does not display "No Records Found" message 
			in the App when the user goes to this screen.*/
			
			INSERT INTO HcUserPreference(HcUserID, LocaleRadius)
			SELECT	 @UserID
				   , ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'DefaultRadius'
			
			--For the first time user default the categories to "All".
			INSERT INTO HcUserCategory(
									 HcUserID
								   , CategoryID
								   , DateCreated)
							SELECT @UserID
								 , CategoryID
								 , GETDATE()
							FROM Category 
			
			--Insert guest user preferred category values into HcUserPreferredCategory table
			INSERT INTO HcUserPreferredCategory(HcUserID
												, BusinessCategoryID
												, HcBusinessSubCategoryID
												, DateCreated
												)
									SELECT  @UserID
											, BC.BusinessCategoryID
											, SC.HcBusinessSubCategoryID
											, GETDATE()
									FROM BusinessCategory BC
									LEFT JOIN HcBusinessSubCategoryType SCT ON BC.BusinessCategoryID = SCT.BusinessCategoryID
									LEFT JOIN HcBusinessSubCategory SC ON SCT.HcBusinessSubCategoryTypeID = SC.HcBusinessSubCategoryTypeID
									WHERE Active = 1
									
			
			-- To capture User First Use Login.
			 INSERT INTO [HcUserFirstUseLogin]
				   ([UserID]
				   ,[FirstUseLoginDate])
			 VALUES										 
				   (@UserID 
				   ,GETDATE())	   
			        	   
				   		
			--To Pass generated UserID.
			SELECT @FirstUserID = @UserID
		END
		ELSE 
		BEGIN
			--Invalid Hub Citi
			SELECT @Result = -2
		END

			
			--To fetch Random PostalCode when user has not configured PostalCode
			SELECT @RandomPostalCode = PostalCode
			FROM HcLocationAssociation
			WHERE HcHubCitiID = @HubCitiID

			--To find nearest PostalCode to user
			SELECT G.PostalCode
				  ,Distance = ROUND((ACOS((SIN(G.Latitude / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(G.Latitude / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (G.Longitude / 57.2958))))*6371) * 0.6214 ,1,1)
			INTO #Temp
			FROM HcLocationAssociation LA
			INNER JOIN GeoPosition G ON LA.PostalCode = G.PostalCode
			WHERE LA.HcHubCitiID = @HubCitiID
			
			SELECT Distinct @NearestPostalCode = PostalCode
							,@Distance = Distance
			FROM #Temp
			WHERE Distance = (SELECT MIN(Distance) FROM #Temp)

			--To check whether user is within HubCiti or Not
			IF(@Distance < @Radius)
			BEGIN
				SELECT @UserOutOfRange = 0
			END

			ELSE
			BEGIN
				SELECT @UserOutOfRange = 1
				SELECT @DefaultPostalCode =ISNULL(@NearestPostalCode,@RandomPostalCode)
			END


			--Confirmation of Success.
			SELECT @Status = 0			
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcGuestLoginUserRegistration].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
