USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcRetailerBusinessSubCategoriesDisplay1_originalbckup]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcRetailerCategoriesDisplay]
Purpose					: To display Retailer Categories list.
Example					: [usp_HcRetailerCategoriesDisplay]

History
Version		  Date		      Author	 Change Description
--------------------------------------------------------------- 
1.0		   16th Jan 2014	   SPAN	           1.1
---------------------------------------------------------------
*/

--exec [HubCitiApp2_8_3].[usp_HcRetailerBusinessSubCategoriesDisplay] null,null,null,null,null,null,null,null,null,null,null,null,null
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcRetailerBusinessSubCategoriesDisplay1_originalbckup]
(
	--Input Variables
	  @HubCitiID int	
	, @BusinessCatID int
	, @Latitude float
	, @Longitude float
    , @UserID int
	, @PostalCode varchar(10)
	, @HcMenuItemID int
	, @BottomButtonID int
	, @SearchKey varchar(1000)

	--Output Variables
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output
	, @Status int output
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
  
)
AS
BEGIN

	BEGIN TRY

		DECLARE @Radius int
		DECLARE @DistanceFromUser FLOAT


		--if @SearchKey is not null and @BusinessCatID is null
		--set @BusinessCatID =
		--(select  distinct BusinessCategoryid from RetailerBusinessCategory
		--where RetailerID in (
		--select distinct RetailID from retailer where retailname like @SearchKey))

				
		--SearchKey implementation
		DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
				WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
				WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
				ELSE @SearchKey END)

		 --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
		EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
		

		--Derive the Latitude and Longitude in the absence of the input.
        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
        BEGIN
                --If the postal code is passed then derive the co ordinates.
                IF @PostalCode IS NOT NULL
				BEGIN
                        SELECT @Latitude = Latitude
                            , @Longitude = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @PostalCode
                END		
				ELSE
				BEGIN
					SELECT @Latitude = G.Latitude
							, @Longitude = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID	
				END												
        END    

		--To Get the user preferred radius.
        SELECT @Radius = LocaleRadius
        FROM HcUserPreference 
        WHERE HcUserID = @UserID
                           
        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))  
	
			CREATE TABLE #BusinessCategory(BusinessCategoryID int , HcBusinessSubCategoryID int)
			IF (@HcMenuItemId IS NOT NULL)
			BEGIN 

				INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
				SELECT DISTINCT BusinessCategoryID,HcBusinessSubCategoryID               
				FROM  HcMenuFindRetailerBusinessCategories 
				WHERE HcMenuItemID=@HcMenuItemID AND BusinessCategoryID = @BusinessCatID 
			END
			ELSe IF (@BottomButtonID IS NOT NULL)
			BEGIN
			
				INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
				SELECT DISTINCT BusinessCategoryID,HcBusinessSubCategoryID               
				FROM  HcBottomButtonFindRetailerBusinessCategories 
				WHERE HcBottomButonID=@BottomButtonID AND BusinessCategoryID = @BusinessCatID
			END

		
			 select distinct BusinessCategoryID  into #SubCategorytype  from HcBusinessSubCategorytype  WITH(NOLOCK) where BusinessCategoryID=@BusinessCatID
	
			select distinct BusinessCategoryID into #NONSUB from BusinessCategory WITH(NOLOCK)   where BusinessCategoryID=@BusinessCatID  
			except
			select distinct BusinessCategoryID  from #SubCategorytype WITH(NOLOCK)

			CREATE  TABLE #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)

					SELECT DISTINCT * INTO #HcMenuFindRetailerBusinessCategories 
					FROM HcMenuFindRetailerBusinessCategories  WHERE  BusinessCategoryID=@BusinessCatID
					AND HcBusinessSubCategoryID IS NOT NULL
					AND HcMenuItemID=@HcMenuItemId

					--SELECT DISTINCT * INTO #HcMenuFindRetailerBusinessCategories1 
					--FROM HcMenuFindRetailerBusinessCategories  WHERE  BusinessCategoryID=@BusinessCatID
					--AND HcBusinessSubCategoryID IS  NULL
					--AND HcMenuItemID=@HcMenuItemId

					SELECT DISTINCT * INTO #HcBottomButtonFindRetailerBusinessCategories 
					from HcBottomButtonFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCatID
					AND HcBusinessSubCategoryID IS NOT NULL
					and HcBottomButonID=@BottomButtonID

					--SELECT DISTINCT * INTO #HcBottomButtonFindRetailerBusinessCategories1 
					--from HcBottomButtonFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCatID
					--AND HcBusinessSubCategoryID IS  NULL
					--and HcBottomButonID=@BottomButtonID
					
					SELECT DISTINCT * INTO #RetailerBusinessCategory2  FROM RetailerBusinessCategory WHERE  BusinessCategoryID=@BusinessCatID

					CREATE  INDEX  ix_nn  on #HcMenuFindRetailerBusinessCategories(BusinessCategoryID,
					HcBusinessSubCategoryID)

					CREATE CLUSTERED INDEX  ix_nn1  on #HcMenuFindRetailerBusinessCategories(HcMenuItemID)
				
                    IF EXISTS(SELECT  1 FROM #SubCategorytype)
                    BEGIN
                            IF @HcMenuItemID  IS NOT NULL
								BEGIN
									INSERT  INTO #RetailerBusinessCategory1
									SELECT DISTINCT  RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
									FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
									INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
											ON HCB.BusinessCategoryID=RBC.BusinessCategoryID AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID 
									INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID 
											AND RA.HcHubCitiID = @HubCitiID AND Associated = 1
									WHERE HCB.HcMenuItemID=@HcMenuItemID

										 --INSERT  INTO #RetailerBusinessCategory1
										 --SELECT DISTINCT  RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
										 --FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										 --INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
										 --ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										 --AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
										 --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										 --inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										 --WHERE HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCatID
										

										 --INSERT  INTO #RetailerBusinessCategory1
										 --SELECT  DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
										 --FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
										 --INNER JOIN #HcMenuFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
										 --ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
										 --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
										 --inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
										 --WHERE HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCatID
									 END
							ELSE IF @BottomButtonID  is not null
								BEGIN
								INSERT  INTO #RetailerBusinessCategory1
									SELECT DISTINCT  RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
									FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
									INNER JOIN #HcBottomButtonFindRetailerBusinessCategories HCB WITH(NOLOCK)
											ON HCB.BusinessCategoryID=RBC.BusinessCategoryID AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID 
									INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID 
											AND RA.HcHubCitiID = @HubCitiID AND Associated = 1
									WHERE HCB.HcBottomButonID=@BottomButtonID
								
								--INSERT  INTO #RetailerBusinessCategory1
								--SELECT  distinct RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
								--FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
								--INNER JOIN #HcBottomButtonFindRetailerBusinessCategories HCB WITH(NOLOCK)
								--ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								--AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
								--INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
								--inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
								--where HCB.HcBottomButonID=@BottomButtonID and RBC.BusinessCategoryID=@BusinessCatID

								--INSERT  INTO #RetailerBusinessCategory1
								--SELECT  distinct RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
								--FROM #RetailerBusinessCategory2 RBC WITH(NOLOCK)
								--INNER JOIN #HcBottomButtonFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
								--ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								--INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
								--inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
								--where HCB.HcBottomButonID=@BottomButtonID and RBC.BusinessCategoryID=@BusinessCatID
								END

								--ELSE
								--BEGIN
								--	SELECT DISTINCT BusinessCategoryID INTO #BusinessCategory1 FROM #BusinessCategory

								--	INSERT  INTO #RetailerBusinessCategory1
								--	SELECT  DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
								--	FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								--	INNER JOIN #BusinessCategory1 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
								--	WHERE RBC.BusinessCategoryID=@BusinessCatID
								--END
					END
					--ELSE 
					--BEGIN
					--		SELECT DISTINCT BusinessCategoryID INTO #BusinessCategory2 FROM #BusinessCategory

					--		INSERT  INTO #RetailerBusinessCategory1
					--		SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
					--		FROM RetailerBusinessCategory RBC
					--		INNER JOIN #BusinessCategory2 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
					--		INNER JOIN #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID
					--		AND RBC.BusinessCategoryID=@BusinessCatID

					--END
					SELECT DISTINCT #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1

		;WITH CTE AS(
		SELECT DISTINCT BSC.HcBusinessSubCategoryID catId
						,BSC.BusinessSubCategoryName catName
						, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
		FROM BusinessCategory BC 
		INNER JOIN HcBusinessSubCategoryType BSCT ON BC.BusinessCategoryID = BSCT.BusinessCategoryID AND BC.BusinessCategoryID = @BusinessCatID	
		INNER JOIN HcBusinessSubCategory BSC ON BSCT.HcBusinessSubCategoryTypeID = BSC.HcBusinessSubCategoryTypeID
		INNER JOIN #BusinessCategory FR ON FR.HcBusinessSubCategoryID=BSC.HcBusinessSubCategoryID
		--INNER JOIN HcMenuFindRetailerBusinessCategories FR ON FR.HcBusinessSubCategoryID=BSC.HcBusinessSubCategoryID AND HcMenuItemID =@HcMenuItemID 
		--INNER JOIN RetailerBusinessCategory RBC ON FR.BusinessCategoryID = RBC.BusinessCategoryID 
		--		AND FR.HcBusinessSubCategoryID = RBC.BusinessSubCategoryID
		INNER JOIN #RetailerBusinessCategory RBC ON  FR.BusinessCategoryID=RBC.BusinessCategoryID
				AND FR.HcBusinessSubCategoryID=RBC.BusinessSubCategoryID
		INNER JOIN Retailer R On R.Retailid  = RBC.RetailerID
		LEFT JOIN RetailerKeywords RK on RK.RetailID = R.RetailID
		LEFT JOIN HcRetailerSubCategory RSC ON FR.BusinessCategoryID = RSC.BusinessCategoryID AND RSC.HcBusinessSubCategoryID = FR.HcBusinessSubCategoryID AND RBC.RetailerID = RSC.RetailID
		LEFT JOIN RetailLocation RL ON RSC.RetailLocationID = RL.RetailLocationID AND Headquarters = 0 AND RL.Active = 1
		LEFT JOIN GeoPosition G on G.PostalCode = RL.PostalCode
		WHERE ((@SearchKey IS NOT NULL AND @SearchKey <> '') 
		AND (RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey)) OR (@SearchKey IS NULL)
				
		--UNION ALL

		--SELECT DISTINCT BSC.HcBusinessSubCategoryID catId
		--				,BSC.BusinessSubCategoryName catName
		--				, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       

		--FROM BusinessCategory BC 
		--INNER JOIN HcBusinessSubCategoryType BSCT ON BC.BusinessCategoryID = BSCT.BusinessCategoryID AND BC.BusinessCategoryID = @BusinessCatID			
		--INNER JOIN HcBusinessSubCategory BSC ON BSCT.HcBusinessSubCategoryTypeID = BSC.HcBusinessSubCategoryTypeID
		--INNER JOIN #BusinessCategory FR ON FR.HcBusinessSubCategoryID=BSC.HcBusinessSubCategoryID
		----INNER JOIN HcMenuFindRetailerBusinessCategories FR ON FR.HcBusinessSubCategoryID=BSC.HcBusinessSubCategoryID AND HcMenuItemID =@HcMenuItemID 
		--INNER JOIN RetailerBusinessCategory RBC ON FR.BusinessCategoryID = RBC.BusinessCategoryID 
		--AND FR.HcBusinessSubCategoryID = RBC.BusinessSubCategoryID
		--INNER JOIN retailer Rrr On rrr.retailid  = rbc.RetailerID
		--INNER JOIN #RetailerBusinessCategory  on #RetailerBusinessCategory.RetailerID=RBC.RetailerID
		--and #RetailerBusinessCategory.BusinessCategoryID=RBC.BusinessCategoryID
		--LEFT JOIN RetailerKeywords rK on rk.RetailID = rrr.RetailID
		--LEFT JOIN HcRetailerSubCategory RSC ON FR.BusinessCategoryID = RSC.BusinessCategoryID AND RSC.HcBusinessSubCategoryID = FR.HcBusinessSubCategoryID AND RBC.RetailerID = RSC.RetailID
		---- INNER JOIN HcRetailerSubCategory RSC ON FR.BusinessCategoryID = RSC.BusinessCategoryID AND RSC.HcBusinessSubCategoryID = FR.HcBusinessSubCategoryID AND RBC.RetailerID = RSC.RetailID
		--LEFT JOIN RetailLocation RL ON RSC.RetailLocationID = RL.RetailLocationID AND Headquarters = 0 AND RL.Active = 1
		--LEFT JOIN GeoPosition G on G.PostalCode = RL.PostalCode
		--WHERE ((@SearchKey IS NOT NULL AND @SearchKey <> '') 
		--AND (RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey)) OR (@SearchKey IS NULL)
				
		) SELECT  * INTO #CC FROM CTE
		
		select distinct catId, catName from #CC
		where distance < @Radius
		order by catName 

	
		--Confirmation of Success
		SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcRetailerCategoriesDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
