USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserRegistration_Version2]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FirstUse
Purpose					: For Hub Citi User Registration.
Example					:  
DECLARE	@return_value int
EXEC @return_value =  [usp_FirstUse] 'John@em.com', 'Mitter','6/28/2011', '123234234', null, null, @Result = @return_value OUTPUT
SELECT @return_value as return_value
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		   16th	Sept 2013      Pavan Sharma K	Initial Version
2.0			2nd July 2016	Bindu T A		Changes with respect to Forgot Password
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserRegistration_Version2]
(
	  @UserName varchar(100)
	, @Password varchar(60)
	, @HubCitiKey varchar(100)
	, @AppVersion varchar(10)
	, @Email Varchar(500)
	
	
	--UserLocation Variables.
	, @DeviceID varchar(60)
	, @UserLongitude float
	, @UserLatitude float
	
	--Output Variable
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(10) output  
	, @HubCitiID int output
	, @HubCitiName varchar(255) output
	, @FirstUserID int output
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			SET @Result = 0
			
			DECLARE @NearestPostalCode varchar(10)
			, @RandomPostalCode varchar(10)
			, @Distance float
			, @Radius float

			SELECT @Radius = ScreenContent
			FROM AppConfiguration
			WHERE ScreenName = 'DefaultRadius'			

			--Derive the hubciti ID from Name.
			SELECT @HubCitiID = HcHubCitiID
				 , @HubCitiName = HubCitiName
			FROM HcHubCiti
			WHERE HcHubCitiKey = @HubCitiKey
						
			-- Check for existance of the Email Address. 

			--If already found, return -1
			IF EXISTS (SELECT 1 FROM HcUser H
					   INNER JOIN HcUserDeviceAppVersion HD ON H.HcUserID = HD.HcUserID AND HD.HcHubCitiID = @HubCitiID
					    WHERE(UserName = @UserName ) AND (Email = @Email) AND FacebookAuthenticatedUser = 0)	  
			BEGIN
				SELECT @Result = -3
			END
			ELSE IF EXISTS (SELECT 1 FROM HcUser H
					   INNER JOIN HcUserDeviceAppVersion HD ON H.HcUserID = HD.HcUserID AND HD.HcHubCitiID = @HubCitiID
					   WHERE (Email = @Email) AND FacebookAuthenticatedUser = 0)
			BEGIN
				SELECT @Result = -2
			END
			ELSE IF EXISTS (SELECT 1 FROM HcUser H
					   INNER JOIN HcUserDeviceAppVersion HD ON H.HcUserID = HD.HcUserID AND HD.HcHubCitiID = @HubCitiID
					    WHERE(UserName = @UserName ) AND FacebookAuthenticatedUser = 0)
			BEGIN 
				SELECT @Result = -1
			END

			--If not found, insert into Users table. ie,allow user to register.
			ELSE IF @HubCitiID IS NOT NULL
			BEGIN
				INSERT INTO HcUser 
					(
					  UserName
					 ,Password
					 ,FirstUseComplete
					 ,DateCreated
					 ,Email
					)
				VALUES
					(
					  @UserName 
					 ,@Password 
					 ,0
					 ,GETDATE() 
					 ,@Email
					)
				
				DECLARE @UserID INT
				SET @UserID = SCOPE_IDENTITY()
				
				INSERT INTO HcUserDeviceAppVersion(HcUserID
												 , HcHubCitiID
												 , DeviceID
												 , AppVersion 
												 , DateCreated)
				VALUES(@UserID
					 , @HubCitiID
					 , @DeviceID
					 , @AppVersion
					 , GETDATE())
				
				INSERT INTO HcUserLocation 
					(
						 HcUserID 
						,DeviceID
						,UserLongitude
						,UserLatitude
						,UserLocationDate
					)
				VALUES
					(
						 @UserID 
						,@DeviceID 
						,@UserLongitude 
						,@UserLatitude 
						,GETDATE() 
					)
								 
			/*Create a dummy entry for the user & default it 
			to the global radius so that for the first time 
			it does not display "No Records Found" message 
			in the App when the user goes to this screen.*/
			
			INSERT INTO HcUserPreference(HcUserID, LocaleRadius)
			SELECT	 @UserID
				   , ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'DefaultRadius'
			
			--For the first time user default the categories to "All".
			INSERT INTO HcUserCategory(
									 HcUserID
								   , CategoryID
								   , DateCreated)
							SELECT @UserID
								 , CategoryID
								 , GETDATE()
							FROM Category 
			
			--Insert user preferred category values into HcUserPreferredCategory table
			INSERT INTO HcUserPreferredCategory(HcUserID
												, BusinessCategoryID
												, HcBusinessSubCategoryID
												, DateCreated
												)
										SELECT @UserID
											   , BC.BusinessCategoryID
											   , SC.HcBusinessSubCategoryID
											   , GETDATE()
										FROM BusinessCategory BC
										LEFT JOIN HcBusinessSubCategoryType SCT ON BC.BusinessCategoryID = SCT.BusinessCategoryID
										LEFT JOIN HcBusinessSubCategory SC ON SCT.HcBusinessSubCategoryTypeID = SC.HcBusinessSubCategoryTypeID
										WHERE Active = 1
										
			-- To capture User First Use Login.
			 INSERT INTO [HcUserFirstUseLogin]
				   ([UserID]
				   ,[FirstUseLoginDate])
			 VALUES										 
				   (@UserID 
				   ,GETDATE())	   
			        	   
				   		
			--To Pass generated UserID.
			SELECT @FirstUserID = @UserID
		END
		ELSE 
		BEGIN
			--Invalid Hub Citi
			SELECT @Result = -4
		END

			
			--To fetch Random PostalCode when user has not configured PostalCode
			SELECT @RandomPostalCode = PostalCode
			FROM HcLocationAssociation
			WHERE HcHubCitiID = @HubCitiID

			--To find nearest PostalCode to user
			SELECT G.PostalCode
				  ,Distance = ROUND((ACOS((SIN(G.Latitude / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(G.Latitude / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (G.Longitude / 57.2958))))*6371) * 0.6214 ,1,1)
			INTO #Temp
			FROM HcLocationAssociation LA
			INNER JOIN GeoPosition G ON LA.PostalCode = G.PostalCode
			WHERE LA.HcHubCitiID = @HubCitiID
			
			SELECT Distinct @NearestPostalCode = PostalCode
							,@Distance = Distance
			FROM #Temp
			WHERE Distance = (SELECT MIN(Distance) FROM #Temp)

			--To check whether user is within HubCiti or Not
			IF(@Distance < @Radius)
			BEGIN
				SELECT @UserOutOfRange = 0
			END

			ELSE
			BEGIN
				SELECT @UserOutOfRange = 1
				SELECT @DefaultPostalCode =ISNULL(@NearestPostalCode,@RandomPostalCode)
			END

			--inserting record to usersPreferred city association table bcz if
			--user chooses get started as soon as registration without going to 
			--set preference all cities should be selected.

			IF EXISTS (SELECT 1 FROM HcHubCiti WHERE HcAppListID = 2 AND HcHubCitiID = @HubCitiID)
			BEGIN

				INSERT INTO HcUsersPreferredCityAssociation(HcHubcitiID
															,HcUserID
															,HcCityID
															,CreatedUserID
															,ModifiedUserID
															,DateCreated
															,DateModified)
													VALUES(@HubCitiID
															,@UserID
															,NULL
															,@UserID
															,NULL
															,GETDATE()
															,NULL)
			END

			--Confirmation of Success.
			SELECT @Status = 0			
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FirstUse.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
