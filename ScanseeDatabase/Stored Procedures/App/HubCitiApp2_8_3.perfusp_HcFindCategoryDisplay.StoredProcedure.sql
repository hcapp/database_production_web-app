USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[perfusp_HcFindCategoryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFindCategoryDisplay]
Purpose					: To display CategoryList for HubCiti.
Example					: [usp_HcFindCategoryDisplay]

History
Version		   Date			Author	 Change Description
--------------------------------------------------------------- 
1.0			10/29/2013	    SPAN	      1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[perfusp_HcFindCategoryDisplay]
(
	--Input Variable
	  @HubCitiID int
	, @UserID int
	, @HcMenuItemID int
	, @HcBottmoButtonID int
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)
	, @PostalCode VARCHAR(10)
	, @SubmenuFlag bit
		  
	--Output Variable
	, @NoRecordsMsg nvarchar(max) output
	, @MenuColor Varchar(500) output
	, @MenuFontColor Varchar(500) output 
	, @UserOutOfRange bit output
	, @DefaultPostalCode VARCHAR(10) output
	, @Status bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @HubCitiID1 int = @HubCitiID
			, @UserID1 int = @UserID
			, @HcMenuItemID1 int = @HcMenuItemID
			, @HcBottmoButtonID1 int = @HcBottmoButtonID
			, @Latitude1 decimal(18,6) =  @Latitude
			, @Longitude1 decimal(18,6) = @Longitude
			, @PostalCode1 VARCHAR(10) = @PostalCode
			, @SubmenuFlag1 bit = @SubmenuFlag

			DECLARE @ConfigirationType varchar(50)
			, @NearestPostalCode varchar(50)
			, @DistanceFromUser FLOAT
			, @ModuleName Varchar(200)='Find'
			, @RegionAppFlag Bit
			, @GuestLoginFlag BIT=0

			SELECT @MenuColor = SubMenuButtonColor --IIF(@SubmenuFlag1=0,MenuButtonColor,SubMenuButtonColor)
			      ,@MenuFontColor = SubMenuButtonFontColor  --IIF(@SubmenuFlag1=0,MenuButtonFontColor,SubMenuButtonFontColor)
			FROM HcMenuCustomUI 
			WHERE HubCitiId=@HubCitiID1 

			CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
                                  
			IF (SELECT 1 FROM HcUser WHERE UserName ='guestlogin' AND HcUserID =@UserID1) >0
			BEGIN
				SET @GuestLoginFlag =1
			END


			IF(SELECT 1 from HcHubCiti H
				INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
				AND H.HcHubCitiID =@HubCitiID1 AND AL.HcAppListName ='RegionApp')>0
					
				BEGIN
						SET @RegionAppFlag = 1
										    
						INSERT INTO #RHubcitiList(HcHubCitiID)
						(SELECT DISTINCT HcHubCitiID 
						FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HubCitiID1
						UNION ALL
						SELECT  @HubCitiID1 AS HcHubCitiID
						)
				END
			ELSE
				BEGIN                                           
						SET @RegionAppFlag =0
						INSERT INTO #RHubcitiList(HchubcitiID)
						SELECT @HubCitiID1                                    
				END


			--To Fetch region app associated citylist	
					  
			--CREATE TABLE #CityList(CityID Int,CityName Varchar(200))
			DECLARE @CityList TABLE(CityID Int,CityName Varchar(200))
			INSERT INTO @CityList(CityID,CityName)
					
			SELECT DISTINCT C.HcCityID 
					,C.CityName					
			FROM #RHubcitiList H
			INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HubCitiID1  
			INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID 						
			LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1	AND UP.HcHubcitiID =H.HchubcitiID					        
			WHERE (UP.HcUsersPreferredCityAssociationID IS NULL) 
			OR (UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID)	
			OR ( UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID =@UserID1)					  
			UNION
			SELECT 0,'ABC'

			CREATE TABLE #cityname (cityname VARCHAR(30))
			INSERT INTO #cityname
			SELECT CityName FROM @CityList  

			--To get the Configuration Details.	
			DECLARE @Config Varchar(500) 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			SELECT @ConfigirationType = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType = 'App Media Server Configuration'
			
			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp8].[usp_HcUserHubCitiRangeCheck] @UserID1, @HubCitiID1, @Latitude1, @Longitude1, @PostalCode1, 1, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode1 = ISNULL(@DefaultPostalCode, @PostalCode1)
			
			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude1 IS NULL AND @Longitude1 IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
				IF @PostalCode1 IS NULL
				BEGIN
					SELECT @Latitude1 = G.Latitude
						 , @Longitude1 = G.Longitude
					FROM GeoPosition G
					INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
					WHERE U.HcUserID = @UserID1	
				END
				ELSE
				BEGIN
					SELECT @Latitude1 = G.Latitude
						 , @Longitude1 = G.Longitude
					FROM GeoPosition G
					WHERE PostalCode = @PostalCode1
				END			

			END	
									
			DECLARE @Radius INT
			
			SELECT @Radius = LocaleRadius
			FROM HcUserPreference
			WHERE HcUserID = @UserID1
			
			IF @Radius IS NULL
			BEGIN
				SELECT @Radius = ScreenContent
				FROM AppConfiguration
				WHERE ScreenName = 'DefaultRadius'
			END

			----Varibles for calculating distance
			DECLARE @DistanceSin float = SIN(@Latitude1 / 57.2958)
			DECLARE @Distancecos float = COS(@Latitude1 / 57.2958)
			DECLARE @DistancecosLong float = (@Longitude1 / 57.2958)
			DECLARE @DecToRad float = 57.2958

			--select getdate() beforedistance

			CREATE TABLE #RetailLocation(RetailLocationID int
										, RetailID int
										--, HcCityID int
										--, StateID int
										, PostalCode varchar(10))

			CREATE NONCLUSTERED INDEX [IX_scansee_RetailLocation_RetailID] ON [dbo].[#RetailLocation]
			(
				  [RetailID] ASC
				  ,[Postalcode] ASC
			)

			INSERT INTO #RetailLocation(RetailLocationID
										, RetailID
										--, HcCityID
										--, StateID
										, PostalCode
										) 

							SELECT RetailLocationID
										, RetailID
										--, HcCityID
										--, StateID
										, PostalCode
			FROM RetailLocation  
			WHERE ROUND((ACOS((SINRetailLocationLatitude * @DistanceSin + COSRetailLocationLatitude
				* @Distancecos * COS(@DistancecosLong - COSRetailLocationLongitude)))*6371) * 0.6214 ,1,1) <= 99

			


				--select getdate() afterdistance

			CREATE TABLE #Temp(BusinessCategoryID int
								,BusinessCategoryName Varchar(250)
								,CategoryImagePath Varchar(1000)
								,RetailLocationLatitude float
								,RetailLocationLongitude float
								, CITY VARCHAR(30)
								)

			IF @HcMenuItemID1 IS NOT NULL AND @HcBottmoButtonID1 IS NULL
			BEGIN
		
				INSERT INTO #Temp(BusinessCategoryID
									,BusinessCategoryName
									,CategoryImagePath
									,RetailLocationLatitude
									,RetailLocationLongitude
									,CITY
									)
							SELECT BusinessCategoryID
									,BusinessCategoryName
									,CategoryImagePath
									,RetailLocationLatitude
									,RetailLocationLongitude
									,CITY
							FROM(
							SELECT 	DISTINCT B.BusinessCategoryID 
										  , B.BusinessCategoryName 
										  , B.CategoryImagePath 
										  , RL.RetailLocationLatitude
										  , RL.RetailLocationLongitude
										  , HC.CITY
							FROM BusinessCategory B 		
							INNER JOIN HcMenuFindRetailerBusinessCategories C ON B.BusinessCategoryID = C.BusinessCategoryID
							INNER JOIN RetailerBusinessCategory RB ON C.BusinessCategoryID = RB.BusinessCategoryID				
							INNER JOIN RetailLocation RL ON RB.RetailerID = RL.RetailID
							INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID1 --AND  Hc.City= @cityname
							INNER JOIN #cityname CN ON CN.cityname = HC.City
							INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
							INNER JOIN @CityList CL ON  (@RegionAppFlag=1 AND CL.CityID =HC.HcCityID) OR (@RegionAppFlag=0)-- AND Cl.CityID=0)
							INNER JOIN HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND Associated =1 AND HR.HcHubCitiID = @HubCitiID1
							INNER JOIN HcMenuItem M ON C.HcMenuItemID = M.HcMenuItemID 
							WHERE M.HcMenuItemID = @HcMenuItemID1 AND B.BusinessCategoryName <> 'Other'
							--AND ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude1 / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958)
							-- * COS(@Latitude1 / 57.2958) * COS((@Longitude1 / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius				
							 
							-- union all

							-- SELECT 	DISTINCT B.BusinessCategoryID 
							--			  , B.BusinessCategoryName 
							--			  , B.CategoryImagePath 
							--			  , RL.RetailLocationLatitude
							--			  , RL.RetailLocationLongitude
							--FROM BusinessCategory B 		
							--INNER JOIN HcMenuFindRetailerBusinessCategories C ON B.BusinessCategoryID = C.BusinessCategoryID
							--INNER JOIN RetailerBusinessCategory RB ON C.BusinessCategoryID = RB.BusinessCategoryID				
							--INNER JOIN RetailLocation RL ON RB.RetailerID = RL.RetailID
							--INNER JOIN HcLocationAssociation HC ON HC.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID1
							--INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
							--INNER JOIN @CityList CL ON (@RegionAppFlag=0 AND Cl.CityID=0) --OR (@RegionAppFlag=1 AND CL.CityID =HC.HcCityID)
							--INNER JOIN HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND Associated =1 AND HR.HcHubCitiID = @HubCitiID1
							--INNER JOIN HcMenuItem M ON C.HcMenuItemID = M.HcMenuItemID 
							--WHERE M.HcMenuItemID = @HcMenuItemID1 AND B.BusinessCategoryName <> 'Other'
							--AND ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude1 / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958)
							-- * COS(@Latitude1 / 57.2958) * COS((@Longitude1 / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius				
							 



							)A
							
			END
			ELSE
			BEGIN
		--SELECT GETDATE() btmbtnblockStart

				INSERT INTO #Temp(BusinessCategoryID
									,BusinessCategoryName
									,CategoryImagePath
									,CITY
									--,RetailLocationLatitude
									--,RetailLocationLongitude
									)
				SELECT BusinessCategoryID
						,BusinessCategoryName
						,CategoryImagePath
						, CITY
						--,RetailLocationLatitude
						--,RetailLocationLongitude
				FROM(
				SELECT 	DISTINCT B.BusinessCategoryID 
								, B.BusinessCategoryName 
								, B.CategoryImagePath 
								, HC.CITY
								--, RL.RetailLocationLatitude
								--, RL.RetailLocationLongitude
				FROM BusinessCategory B 	
				INNER JOIN HcBottomButtonFindRetailerBusinessCategories C ON B.BusinessCategoryID = C.BusinessCategoryID ANd c.HcBottomButonID = @HcBottmoButtonID1 
				INNER JOIN RetailerBusinessCategory RB ON C.BusinessCategoryID = RB.BusinessCategoryID
				INNER JOIN #RetailLocation RL ON RB.RetailerID = RL.RetailID
				INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID1 --AND  Hc.City= @cityname
				INNER JOIN #cityname CN ON CN.cityname = HC.City
				INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
			    INNER JOIN @CityList CL ON (@RegionAppFlag=0)-- AND Cl.CityID=0) 
				 OR (@RegionAppFlag=1 AND CL.CityID =HC.HcCityID)  
				INNER JOIN HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND Associated =1 AND HR.HcHubCitiID = @HubCitiID1
				INNER JOIN HcBottomButton M ON C.HcBottomButonID = M.HcBottomButtonID	
				WHERE (M.HcBottomButtonID = @HcBottmoButtonID1 ) AND B.BusinessCategoryName <> 'Other' 
					--AND ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude1 / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958)
					--		 * COS(@Latitude1 / 57.2958) * COS((@Longitude1 / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius				
							 

				--UNION ALL

				--SELECT 	DISTINCT B.BusinessCategoryID 
				--				, B.BusinessCategoryName 
				--				, B.CategoryImagePath 
				--				--, RL.RetailLocationLatitude
				--				--, RL.RetailLocationLongitude
				--FROM BusinessCategory B 	
				--INNER JOIN HcBottomButtonFindRetailerBusinessCategories C ON B.BusinessCategoryID = C.BusinessCategoryID ANd c.HcBottomButonID = @HcBottmoButtonID1 
				--INNER JOIN RetailerBusinessCategory RB ON C.BusinessCategoryID = RB.BusinessCategoryID
				--INNER JOIN #RetailLocation RL ON RB.RetailerID = RL.RetailID
				--INNER JOIN HcLocationAssociation HC ON HC.PostalCode = RL.PostalCode AND HcHubCitiID = @HubCitiID1
				--INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
			 --   INNER JOIN @CityList CL ON (@RegionAppFlag=1 AND CL.CityID =HC.HcCityID) --OR (@RegionAppFlag=0 AND Cl.CityID=0)  
				--INNER JOIN HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND Associated =1 AND HR.HcHubCitiID = @HubCitiID1
				--INNER JOIN HcBottomButton M ON C.HcBottomButonID = M.HcBottomButtonID	
				--WHERE (M.HcBottomButtonID = @HcBottmoButtonID1 ) AND B.BusinessCategoryName <> 'Other'

				--AND ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude1 / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958)
				--			 * COS(@Latitude1 / 57.2958) * COS((@Longitude1 / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius				
							 
				--AND RL.Distance <= @Radius
				 
				--AND ROUND((ACOS((SINRetailLocationLatitude * @DistanceSin + COSRetailLocationLatitude
				--* @Distancecos * COS(@DistancecosLong - COSRetailLocationLongitude)))*6371) * 0.6214 ,1,1) <= @Radius	

				--AND ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude1 / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958)
				--			 * COS(@Latitude1 / 57.2958) * COS((@Longitude1 / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1) <= @Radius				
				
				
				
							 
				--AND ((ACOS((SIN(RL.RetailLocationLatitude / @DecToRad) * @DistanceSin + COS(RL.RetailLocationLatitude / @DecToRad)
				-- * @Distancecos * COS(@DistancecosLong - (RL.RetailLocationLongitude / @DecToRad))))*6371) * 0.6214,1,1)  <= @Radius	 --,1,1)			 
				)B
				--SELECT GETDATE() btmbtnblockEnd
			END	
			
			SELECT DISTINCT T.BusinessCategoryID AS catID
					,T.BusinessCategoryName AS CatDisName
					,CatImgPth = CASE WHEN HcFindCategoryImageID IS NULL THEN @ConfigirationType + T.CategoryImagePath
											    WHEN HcFindCategoryImageID IS NOT NULL AND IsUpdated = 0 THEN @ConfigirationType + T.CategoryImagePath
											    WHEN HcFindCategoryImageID IS NOT NULL AND IsUpdated = 1 THEN @Config + CAST(@HubCitiID1 AS VARCHAR(100)) + '/' + I.CategoryImagePath
										   END
					--,T.CITY
			FROM #Temp T
			LEFT JOIN HcFindCategoryImage I	ON T.BusinessCategoryID = I.BusinessCategoryID AND I.HcHubCitiID = @HubCitiID1
			ORDER BY CatDisName ASC
		
			--To display Find related Bottom Button list
			--EXEC [HubCitiApp2_1].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID1,@ModuleName,@UserID1, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage =     @ErrorMessage output
			
			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			     , HM.HcHubCitiID 
				 , BB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID1 AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID1 AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID1), BottomButtonLinkID) 
				 --, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID1 ) = 1  THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       --INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =A.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID) -- AND HM.HcHubCitiID =@HubCitiID1
			                      	 
			                       ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID AND FBB.HcHubCitiID =@HubCitiID1 
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = 'Find Category'
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 

				--To display message when no Retailers display for user preferred cities.
				 DECLARE @MaxCnt int
				 DECLARE @UserPrefCities NVarchar(MAX)

				 SELECT @MaxCnt = Count(1) FROM #Temp

				 IF (@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0)
				 BEGIN 
						 SELECT @UserPrefCities = COALESCE(@UserPrefCities+',' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName))) 
						 FROM HcUsersPreferredCityAssociation P
						 INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
						 WHERE HcHubcitiID = @HubCitiID1 AND HcUserID = @UserID1
						
						SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
												'.\n\nUpdate your city preferences in the settings menu.'
						--DECLARE @strPrint VARCHAR(MAX)
						--SET @strPrint = 'There currently is no information for your city preferences.'
						--SET @strPrint = @strPrint + CHAR(13)
						--SET @strPrint = @strPrint + @UserPrefCities
						--SET @strPrint = @strPrint + CHAR(13)
						--SET @strPrint = @strPrint + 'Update your city preferences in the settings menu.'
						--PRINT @strPrint
						--SELECT @NoRecordsMsg = @strPrint

				 END
				 ELSE IF (@RegionAppFlag = 0) AND (ISNULL(@MaxCnt,0) = 0)
				 BEGIN
						SELECT @NoRecordsMsg = 'No Records Found.'
				 END


	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcFindCategoryDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;













GO
