USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailerLocationSpecialOffersDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerLocationSpecialOffersDisplay
Purpose					: To List the Special Offers for the given Retailer Location.
Example					: usp_RetailerLocationSpecialOffersDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th May 2012	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailerLocationSpecialOffersDetails]
(
	  @HcUserID int  
	, @RetailID int    
	, @RetailLocationID int
	, @PageID int
	, @HubCitiID int
	
	 --User Tracking Imputs
    , @ScanTypeID bit
    , @MainMenuID int
    
	--Output Variable	
	, @ExternalLinkFlag bit output
	, @ExternalLink varchar(1000) output
	, @Status int output 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	 
	
		BEGIN TRANSACTION
		

			 DECLARE @RetailerConfig varchar(50)
			 DECLARE @MediaTypes VARCHAR(1000) = ''
			 DECLARE @MediaPath VARCHAR(MAX) = ''
			 DECLARE @ExternalFlag varchar(100) = ''
			 DECLARE @RetailLocations VARCHAR(1000) = ''
			 DECLARE @ScanType int
			 DECLARE @SpecialListID INT

			 --User Tracking
			 DECLARE @Rowcount int
			

			 SET @SpecialListID =( SELECT MAX(SpecialsListID)
			 FROM HubCitiReportingDatabase..SpecialsList where SpecialOfferID =@PageID) 
		
		     Update HubCitiReportingDatabase..SpecialsList SET SpecialOfferClick =1 
			 WHERE SpecialsListID  =@SpecialListID
			
			 SELECT @RetailerConfig= ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'
			 
			 DECLARE @RetailConfig varchar(50)
			 SELECT @RetailConfig= ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='QR Code Configuration'
			 AND Active = 1
			 
			 SELECT @ScanType = ScanTypeID
		     FROM HubCitiReportingDatabase..ScanType
		     WHERE ScanType = 'Retailer Special Offer QR Code'
			 
			 
		     SELECT @ExternalLinkFlag = CASE WHEN A.URL IS NOT NULL THEN 1
										ELSE 0 END
		     FROM QRRetailerCustomPage A
		     LEFT JOIN QRRetailerCustomPageMedia B ON A.QRRetailerCustomPageID = B.QRRetailerCustomPageID
		     WHERE A.QRRetailerCustomPageID = @PageID
		     
							 
		     CREATE TABLE #SpecialOffers(pageID INT
									  , Pagetitle VARCHAR(1000)
									  --, PageDescription VARCHAR(Max)
									  , ShortDescription VARCHAR(Max)
									  , LongDescription VARCHAR(Max)
									  --, ImageName VARCHAR(1000)
									  , ImagePath VARCHAR(1000)
									  --, MediaType VARCHAR(10)
									  --, MediaPath VARCHAR(1000)
									  --, ExternalFlag BIT
									  , RetailID INT	 
									  --, QRType VARCHAR(100)
									  , StartDate DATETIME
									  , EndDate DATETIME
									  --, Expired BIT
									  --, QRUrl VARCHAR(1000)
									  )

			 SELECT 
				   QRTypeCode
				 , QRRCP.RetailID		       
				 , QRRCP.QRRetailerCustomPageID  
				 , COUNT(QRRCPA.RetailLocationID) Counts
			 INTO #Temp
			 FROM QRRetailerCustomPage QRRCP  
			 INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
			 INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCP.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
			 WHERE QRRCPA.QRRetailerCustomPageID = @PageID
			 AND QRRCP.RetailID = @RetailID
			 GROUP BY QRTypeCode
					, QRRCP.RetailID		     
					, QRRCP.QRRetailerCustomPageID									
									
		     
		      --Send the details of the Special Offers if the page is found to be an internal(Make your own).
			 IF @ExternalLinkFlag = 0
			 BEGIN
					 --Get Associated Media Details
					 SELECT  @MediaTypes = COALESCE(@MediaTypes+',','') + CAST(PMT.ProductMediaType AS VARCHAR(10))
						   , @MediaPath = COALESCE(@MediaPath+',','') +  @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + CAST(QR.MediaPath AS VARCHAR(100))
						   , @ExternalFlag = COALESCE(@ExternalFlag+',','') + CAST(ExternalFlag AS VARCHAR(10))
					 FROM QRRetailerCustomPageMedia QR
					 INNER JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = QR.MediaTypeID
					 WHERE QRRetailerCustomPageID = @PageID	 
					 
					--Get Page Details 
					
					INSERT INTO #SpecialOffers(pageID  
									  , Pagetitle  
									  --, PageDescription  
									  , ShortDescription  
									  , LongDescription  
									  --, ImageName  
									  , ImagePath  
									  --, MediaType  
									  --, MediaPath  
									  --, ExternalFlag  
									  , RetailID
									  --, QRType  	 
									  , StartDate 
									  , EndDate  
									  --, Expired
									  --, QRUrl
									  )
					SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
						 , QRRCP.Pagetitle
						 --, QRRCP.PageDescription
						 , QRRCP.ShortDescription 
						 , QRRCP.LongDescription  
						 --, CASE WHEN QRRCP.Image IS NULL THEN (SELECT QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID)
							--	ELSE QRRCP.Image END  ImageName
						 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT @RetailerConfig + CAST(QRRetailerCustomPageIconImagePath AS VARCHAR(100)) FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID) 
								ELSE @RetailerConfig + CAST(QRRCP.RetailID AS VARCHAR(10)) + '/'+ CAST(Image AS VARCHAR(100)) END	ImagePath
						 --, SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
						 --, SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
						 --, SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
						 , QRRCP.RetailID
						 --, QRT.QRTypeName QRType			 
						 , QRRCP.StartDate
						 , QRRCP.EndDate
						 --, Expired = CASE WHEN GETDATE() > EndDate THEN 1
							--		 ELSE 0 END
						 --, QRUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL									 
							--		 THEN CASE WHEN Counts > 1 THEN @RetailConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10)) +'&EL=true'
							--				   WHEN COUNTS = 1 THEN @RetailConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10))+ '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=true'
							--			  END 
							--		 ELSE			
							--		 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
							--			  CASE WHEN Counts > 1 THEN @RetailConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+ '&EL=false'
							--				   WHEN Counts = 1 THEN @RetailConfig + CAST(T.QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.RetailID AS VARCHAR(10)) + '&retlocId='  + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRRetailerCustomPageID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubcitiID AS VARCHAR(10))+'&EL=false'
							--			  END
							--		 END	
					FROM QRRetailerCustomPage QRRCP
					INNER JOIN #Temp T ON QRRCP.QRRetailerCustomPageID = T.QRRetailerCustomPageID
					INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID	
					INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
					LEFT OUTER JOIN QRRetailerCustomPageMedia QRRPM ON QRRPM.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
					WHERE QRRCP.RetailID = @RetailID
					AND  QRRCP.QRRetailerCustomPageID = @PageID
					AND ((@RetailLocationID <> '0' AND QRRCPA.RetailLocationID = @RetailLocationID)
						 OR
						 (@RetailLocationID = '0' AND 1 = 1))
	
					
		   --User Tracking
				SELECT @Rowcount=@@ROWCOUNT
			END
						
			--send the link if the page is found to be an external one(Link to existing or Upload PDF or image).
		   ELSE IF @ExternalLinkFlag = 1
		   BEGIN
				 INSERT INTO #SpecialOffers(pageID)
				 VALUES(@PageID)
				 
				 SELECT @ExternalLink = QRRCP.URL
				 FROM QRRetailerCustomPage QRRCP  
				 INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
				 WHERE QRRCP.QRRetailerCustomPageID = @PageID
				--AND QRRCP.RetailID = @RetailID
		   END
		   	
		   --User Tracking
			
		--	Set to 1 if it is null because @ScanTypeID will be null when scanned from external source.
		   IF ISNULL(@ScanTypeID, 1)  = 1
		   BEGIN
		   INSERT INTO HubCitiReportingDatabase..scan(MainMenuID
		     										 ,ScanTypeID
													 ,Success
													 ,CreatedDate
													 , ID)
				SELECT @MainMenuID 
					  ,@ScanType 
					  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
					  ,GETDATE()
					  , pageID
				FROM #SpecialOffers	
					  
			--INSERT INTO HubCitiReportingDatabase..SpecialsList(SpecialOfferID, MainMenuID, SpecialOfferClick, CreatedDate)
			--SELECT pageID
			--	 , @MainMenuID
			--	 , 1
			--	 , GETDATE()
			--FROM #SpecialOffers							  
														
		    END 
			--MM/DD/YYYY Time:  HH:MM
		     --Display the result set if it is an internal page.
		   --select * from #SpecialOffers
		   IF @ExternalLinkFlag = 0
		   BEGIN
				SELECT  DISTINCT pageID
					 , Pagetitle
					 --, PageDescription
					 ,  ShortDescription shortDesc
					 ,  LongDescription longDesc
					 --, ImageName
					 , ImagePath retImagePath
					 --, MediaType
					 --, MediaPath	
					 --, ExternalFlag 
					 , RetailID	retailerId
					 --, QRType		 
					 , startDate = FORMAT(StartDate, 'MMMM dd') + 'th at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), StartDate, 100), 7))--FORMAT(StartDate, 'HH:mm tt')
					 , endDate = FORMAT(EndDate, 'MMMM dd') + 'th at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), EndDate, 100), 7))
					 --,StartDate =FORMAT(StartDate, 'MM/dd/yyyy') + ' Time: ' + FORMAT(StartDate, 'HH:MM ')
					 --,EndDate =FORMAT(EndDate, 'MM/dd/yyyy') + ' Time: ' + FORMAT(EndDate, 'HH:MM ')
					 --, Expired
					-- , QRUrl retailerPageQRURL
				FROM #SpecialOffers
			 END
			 
			 --SET @SpecialListID =( SELECT TOP 1 SpecialsListID 
			 --FROM HubCitiReportingDatabase..SpecialsList where SpecialOfferID =@PageID) 


			 --Update HubCitiReportingDatabase..SpecialsList SET SpecialOfferClick =1 
			 --WHERE SpecialOfferID =@PageID
			 			 
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
























GO
