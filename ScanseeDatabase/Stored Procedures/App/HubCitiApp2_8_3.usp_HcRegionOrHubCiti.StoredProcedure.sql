USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcRegionOrHubCiti]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcRegionOrHubCiti]
Purpose					: To identify if it is a region app or a hubciti app.
Example					: [usp_HcRegionOrHubCiti] 

History
Version		     Date		      Author	   Change Description
----------------------------------------------------------------- 
1.0				 29 Sep 2014	  Mohith H R   1.0
-----------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcRegionOrHubCiti]
(
	 
      @HcHubCitiID INT	
    
    --OutPut Variable
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
					
			--To identify if it is a region app or a hubciti app.
			SELECT   HcHubCitiID
			       , HubCitiName
			       , HcAppListName
			FROM HcHubCiti H
			INNER JOIN HcAppList HA ON H.HcAppListID = HA.HcAppListID
			WHERE H.HcHubCitiID = @HcHubCitiID

			--Confirmation of Success
			SELECT @Status = 0
		    COMMIT TRANSACTION
		
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcRegionOrHubCiti].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
