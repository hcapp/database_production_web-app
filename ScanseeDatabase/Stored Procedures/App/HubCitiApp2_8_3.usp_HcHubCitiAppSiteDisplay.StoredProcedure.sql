USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiAppSiteDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiAppSiteDisplay
Purpose					: To display List of appsites.
Example					: usp_HcHubCitiAppSiteDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiAppSiteDisplay]
(   
    --Input variable.	
	  @HcUserID Int
	, @HubCitiID int
	, @AppsiteID Int
	, @Latitude Float
	, @Longitude Float
	, @postalcode Varchar(100)
	--User Tracking Inputs 
    , @LinkID Int
    , @MainMenuID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			--To fetch all the duplicate retailers.
			SELECT DISTINCT DuplicateRetailerID 
			INTO #DuplicateRet
			FROM Retailer 
			WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive=1

	        IF @Latitude IS NULL AND @postalcode IS NOT NULL
			BEGIN
				SELECT @Latitude=Latitude
				      ,@Longitude=Longitude
				FROM GeoPosition
				WHERE PostalCode=@postalcode
			END
			IF @Latitude IS NULL AND @postalcode IS NULL
			BEGIN
				SELECT @Latitude=Latitude
				      ,@Longitude=Longitude
				FROM GeoPosition G
				INNER JOIN HcUser U ON U.PostalCode=G.PostalCode AND U.HcUserID=@HcUserID				
			END



			--To display List fo AppSites
			SELECT HcAppSiteID
				  ,HcHubCitiID
				  ,HcAppSiteName 
				  ,A.RetailLocationID	
				  ,R.RetailName 
				  ,RL.Address1
				  ,RL.City
				  ,RL.State
				  ,RL.PostalCode
				   , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN RL.RetailLocationLatitude IS NULL THEN G.Latitude ELSE RL.RetailLocationLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN RL.RetailLocationLongitude IS NULL THEN G.Longitude ELSE RL.RetailLocationLongitude END/ 57.2958))))*6371) * 0.6214 END				  		   
			FROM HcAppSite A
			INNER JOIN RetailLocation RL ON RL.RetailLocationID =A.RetailLocationID AND RL.Active=1
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID AND R.RetailerActive=1
			INNER JOIN GeoPosition G ON G.PostalCode=RL.PostalCode
			LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
			WHERE HcHubCitiID= @HubCitiID AND HcAppSiteID =@AppsiteID AND D.DuplicateRetailerID IS NULL
			
			
			--UserTrcaking
			
			--Capture the AppSite click and impression
			INSERT INTO HubCitiReportingDatabase..AppsiteList(AppsiteID 
															 ,AppsiteClick 
															 ,MainMenuID 
															 ,DateCreated)
			SELECT @LinkID
			      ,1
			      ,@MainMenuID
			      ,GETDATE()

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiAppSiteDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
