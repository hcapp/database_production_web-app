USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserLogin]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserLogin
Purpose					: To Validate User Login.
Example					: usp_HcUserLogin

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th Sept 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserLogin]
(
	  @UserName varchar(100)
	, @Password char(60)
	, @DeviceID varchar(60)
	, @AppVersion varchar(100)
	, @HubCitiKey varchar(100)
	, @RequestPlatformType varchar(100)
		
	--Output Variable 
	, @HubCitiID Int Output
	, @HubCitiName varchar(255) output
	, @UserID int output
	, @Login int output
	, @HubCitiDeactivated bit output
	--, @InvalidUser int output
	, @InvalidPassword int output --@InvalidPassword output parameter will be set when login fails
	, @Result int output
	, @AddDeviceToUser bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @CNT int		
			
		BEGIN TRANSACTION			
		
			DECLARE @User INT
			DECLARE @RequestPlatformID int
			

			SET @AddDeviceToUser = 0
			SET @InvalidPassword = 0
			SET @HubCitiDeactivated = 0
			--SET @InvalidUser = 0

			SELECT @RequestPlatformID = HcRequestPlatformID
			FROM HcRequestPlatforms
			WHERE RequestPlatformtype = @RequestPlatformType
			
			SELECT @HubCitiID = HcHubCitiID
				, @HubCitiName = HubCitiName
			FROM HcHubCiti 
			WHERE HcHubCitiKey = @HubCitiKey AND Active = 1
			
			--Check if the Hub Citi that the user is accessing is active and notify him accordingly.
			IF EXISTS(SELECT 1 FROM HcHubCiti WHERE HcHubCitiID = @HubCitiID AND Active = 1)		
			BEGIN
			
				--To validate user login			
				SELECT @UserID = H.HcUserID 
				FROM HcUser H
				INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = H.HcUserID
				INNER JOIN HcHubCiti HC ON HC.HcHubCitiID = UD.HcHubCitiID
				WHERE BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName)
				--OR BINARY_CHECKSUM(Email) = BINARY_CHECKSUM(@UserName) 			 
				AND BINARY_CHECKSUM(Password) = BINARY_CHECKSUM(@Password)
				AND FaceBookAuthenticatedUser=0	
				AND HC.HcHubCitiKey = @HubCitiKey
				
				--If the DeviceID is already registered with the user check if it is primary device for the User and alert him if he wants to make it as primary device.	
				IF @DeviceID IN (SELECT DeviceID FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID)					
				BEGIN 
					IF (SELECT Distinct PrimaryDevice FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID AND DeviceID = @DeviceID AND PrimaryDevice = 1) <> 1
					BEGIN
						SET @AddDeviceToUser = 1
					END
				END
				
				--If the device is not registered to the user then alert him if he wants to add & make it as primary device.
				IF @DeviceID NOT IN (SELECT DeviceID FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID AND HcHubCitiID = @HubCitiID)		
				BEGIN
					INSERT INTO HcUserDeviceAppVersion(HcUserID, DeviceID, AppVersion, HcHubCitiID, DateCreated, PrimaryDevice, HcRequestPlatformID)
					VALUES(@UserID, @DeviceID, @AppVersion, @HubCitiID , GETDATE(), 0, @RequestPlatformID)
					
					SET @AddDeviceToUser = 1
				END			
				
				--If Login is success, then capture user login.		
				IF ISNULL(@UserID, 0) <> 0
				BEGIN
					-- To capture User Login.
					INSERT INTO [HcUserFirstUseLogin]
						   ([UserID]
						   ,[FirstUseLoginDate])
					 VALUES
						   (@UserID 
						   ,GETDATE())	
						   
					SELECT @Login = 0 				  					  
				END
				ELSE
				BEGIN			
					SELECT @Login = -1
				END
				
				IF EXISTS(SELECT TOP 1 HcUserID  FROM HcUser WHERE HcUserID = @UserID AND FirstUseComplete = 0)
				BEGIN
					SELECT @Result = CASE WHEN (COUNT(UserID)) % 5 = 0 THEN -1 ELSE 0 END
					FROM HcUserFirstUseLogin 
					WHERE UserID = @UserID
				END
				-- BELOW CODED TO HAVE DEFAULT VALUE OF FIRSTUSE STATUS
				ELSE
				BEGIN
				SELECT @Result =0
				END
				
				--Check the reason for failure.
				IF @Login = -1
				BEGIN	
					--If only user name exists then confirm Password is wrong						
					IF EXISTS(SELECT 1 FROM HcUser WHERE BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName))
					BEGIN
							SET @InvalidPassword = 1				
					END						
					ELSE
					--If username doesnt exist then confirm user is invalid.	
					BEGIN
							SET @InvalidPassword = 0
					END				
				END
			END
			ELSE
			BEGIN
				SELECT @HubCitiDeactivated = 1
			END
			
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserLogin.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
















































GO
