USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserGalleryDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcUserGalleryDisplay]
Purpose					: To dispaly gallery screen.
Example					: [usp_HcUserGalleryDisplay]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			3/4/2015       SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserGalleryDisplay]
(
	 --Input variable
	  @HcUserID int
	, @HubCitiID int
	, @Type Varchar(20)
	, @SubmenuFlag Bit
		
	--Output Variable
	, @MenuBckgrndColor Varchar(500) output
	, @MenuColor Varchar(500) output
	, @MenuFontColor Varchar(500) output 
	, @SectionColor Varchar(500) output		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Deals Bit = 0
			DECLARE @Coupons Bit = 0

			--To get ButtonImage URl
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To get deals image URL
			DECLARE @ImagePath VARCHAR(500)
			SELECT @ImagePath = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti Deals imagepath'

			--To get Coupons image
			DECLARE @CouponsImage varchar(100)
			SELECT @CouponsImage = @ImagePath + ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'HubCiti Deals coupons image'

			--To get deals image
			DECLARE @DealsImage varchar(100)
			SELECT @DealsImage = @ImagePath + ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'HubCiti Deals deals image'

			
			--To get menu background color, menu font color and group section color for Deals.
			SELECT @MenuBckgrndColor = IIF(@SubmenuFlag=0,MenuBackgroundColor,SubMenuBackgroundColor)
				  ,@MenuColor =  IIF(@SubmenuFlag=0,MenuButtonColor,SubMenuButtonColor)
			      ,@MenuFontColor = IIF(@SubmenuFlag=0,MenuButtonFontColor,SubMenuButtonFontColor)
				  ,@SectionColor = IIF(@SubmenuFlag=0,MenuGroupBackgroundColor,SubMenuGroupBackgroundColor)
			FROM HcMenuCustomUI 
			WHERE HubCitiID = @HubCitiID 

			--To check whether User has Claimed HotDeals/Coupons.

			IF (@Type = 'Claimed')
			BEGIN 
				
				SELECT @Deals = 1
				FROM HcUserHotDealGallery G
				INNER JOIN ProductHotDeal P ON G.HotDealID = P.ProductHotDealID
				WHERE HCuserID = @HcUserID AND Used = 0
				AND GETDATE() BETWEEN ISNULL(P.HotDealStartDate,GETDATE()-1) AND ISNULL(P.HotDealExpirationDate,GETDATE()+1)
								
				SELECT @Coupons = 1
				FROM HcUserCouponGallery G
				INNER JOIN Coupon C ON G.CouponID = C.CouponID
				WHERE HCuserID = @HcUserID AND UsedFlag = 0
				AND GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)

			END
			
			--To check whether User has Used HotDeals/Coupons.
			IF (@Type = 'Used')
			BEGIN 
				
				SELECT @Deals = 1
				FROM HcUserHotDealGallery G
				INNER JOIN ProductHotDeal P ON G.HotDealID = P.ProductHotDealID
				WHERE HCuserID = @HcUserID AND Used = 1
				AND CASE WHEN HotDealExpirationDate < GETDATE() THEN DATEDIFF(DAY,GETDATE(),HotDealExpirationDate) 
							ELSE DATEPART(DAY,GETDATE()) END <=90  

				SELECT @Coupons = 1
				FROM HcUserCouponGallery G
				INNER JOIN Coupon C ON G.CouponID = C.CouponID
				WHERE HCuserID = @HcUserID AND UsedFlag = 1
				AND (CASE WHEN CouponExpireDate < GETDATE() THEN DATEDIFF(DAY,GETDATE(),CouponExpireDate) 
									ELSE DATEPART(MONTH,GETDATE()) END <=90)

			END
			
			--To check whether User has any Expired HotDeals/Coupons.
			IF (@Type = 'Expired')
			BEGIN 
				
					SELECT @Deals = 1
					FROM HcUserHotDealGallery H
					INNER JOIN ProductHotDeal P ON H.HotDealID = P.ProductHotDealID
					WHERE HCuserID = @HcUserID AND Used = 0
					AND HotDealExpirationDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),HotDealExpirationDate)<=90

					SELECT @Coupons = 1
					FROM HcUserCouponGallery G
					INNER JOIN Coupon C ON G.CouponID = C.CouponID
					WHERE HcUserID = @HcUserID AND UsedFlag = 0
					AND CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
				
			END

			--Final Result Set
			CREATE TABLE #Temp( ImagePath Varchar(500)
								, Name Varchar(100)
								, Flag bit)
				INSERT INTO #Temp(	ImagePath
								  , Name
								  , Flag)
							SELECT ImagePath
								  , Name
								  , Flag
							FROM 
							(SELECT @DealsImage ImagePath
									, 'Deals' Name
									, @Deals Flag
							 UNION ALL
							 SELECT @CouponsImage ImagePath
									, 'Coupons' Name
									, @Coupons Flag
							)A
			
			SELECT ImagePath
					, Name
					, Flag
			FROM #Temp
			WHERE Flag = 1


		    --To get list of BottomButtons for this Module
            DECLARE @ModuleName Varchar(50) 
            SET @ModuleName = 'Deals'					
               

			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
				 , BB.HcBottomButtonID AS bottomBtnID
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
				 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
										  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
								                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																					            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																					            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
			                       ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HubCitiID
			ORDER by HcFunctionalityBottomButtonID


            --Confirmation of Success
            SELECT @Status = 0    
		
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcUserGalleryDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


























GO
