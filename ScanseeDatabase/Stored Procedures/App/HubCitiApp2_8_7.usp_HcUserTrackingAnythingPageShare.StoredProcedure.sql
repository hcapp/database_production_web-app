USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingAnythingPageShare]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_HcUserTrackingAnythingSpecialShare 
Purpose					: To track anything page share details.  
Example					: usp_HcUserTrackingAnythingSpecialShare
  
History  
Version    Date				Author			Change         Description  
----------------------------------------------------------------------   
1.0        7th Nov 2013		Mohith H R		Initial Version  
---------------------------------------------------------------------- 
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingAnythingPageShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @AnythingPageID int 
 , @TargetAddress Varchar(1000)   
 , @RetailID int
 , @RetailLocationID int
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         
		 INSERT INTO HubCitiReportingDatabase..ShareAnythingPage(MainMenuID
														  ,ShareTypeID
														  ,TargetAddress
														  ,AnythingPageID
														  ,DateCreated
														  ,RetailID
														  ,RetailLocationID)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@AnythingPageID 
		        ,GETDATE()
				,@RetailID
				,@RetailLocationID)	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0           							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingAnythingSpecialShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1 
  END;            
 END CATCH;  
END;












































GO
