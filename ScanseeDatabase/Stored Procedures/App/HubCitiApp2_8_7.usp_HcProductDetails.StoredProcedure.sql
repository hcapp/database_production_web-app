USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcProductDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_HcProductDetails  
Purpose                 : To fetch Product Details.  
Example                 : usp_HcProductDetails 2,1   
  
History  
Version           Date              Author            Change Description  
---------------------------------------------------------------   
1.0          30thOct2013       SPAN Infotech India    Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcProductDetails]  
(  
        @UserID int  
      , @ProductID int  
      , @RetailID int  
      , @ScanLongitude float    
	  , @ScanLatitude float
	  , @HcHubCitiID Int
	  
	  --User Tracking Inputs
	  , @SaleListID int
	  ,	@ProductListID int 
      , @ScanTypeID int --0=Not Scanned, 1=Product Bar Code, 2=Product QR Code
      , @MainMenuID int 
        
      --OutPut Variable  
      , @Status int output
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN    
      BEGIN TRY  
      
			BEGIN TRANSACTION
      
		   --To get Media Server Configuration.  
		   DECLARE @ManufConfig varchar(50)    
		   SELECT @ManufConfig=ScreenContent    
		   FROM AppConfiguration     
		   WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
		   
		   SELECT @ScanTypeID = ScanTypeID
		   FROM HubCitiReportingDatabase..ScanType
		   WHERE (@ScanTypeID = 1 AND ScanType = 'Product Bar Code')
		   OR (@ScanTypeID = 2 AND ScanType = 'Product QR')
		    
		   DECLARE @RetailLocationID int   
		   --DECLARE @Status int    
		   -- If GPS is ON and the Scan Latitude and Longitude is passed, fetching Retailer location whose coordinates matches.    
		   IF @RetailLocationID IS NULL AND (@ScanLatitude IS NOT NULL AND @ScanLongitude IS NOT NULL)    
		   BEGIN    
				SELECT RL.RetailLocationID    
				INTO #RetailLocation    
				FROM RetailLocationProduct RLP     
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID AND RL.Active=1    
				WHERE RLP.ProductID = @ProductID     
				AND (RL.RetailLocationLatitude = @ScanLatitude AND RL.RetailLocationLongitude = @ScanLongitude)  
				  
			-- If Single Retail locations matched then capture.    
			DECLARE @RetailLoc int    
			SELECT @RetailLoc = CASE WHEN COUNT(RetailLocationID) = 1 THEN MIN(RetailLocationID) END    
			FROM #RetailLocation  
			END        
			
			--EXEC [HubCitiapp2_8_7].usp_UserProductHit @UserID, @ProductID, @RetailLocationID, @Status = @Status output , @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
		     
		     
			-- To capture the existance of the Media for the specified product.  
			DECLARE @VideoFlag bit  
			DECLARE @AudioFlag bit  
			DECLARE @FileFlag bit  
			DECLARE @Coupon bit  
			DECLARE @Loyalty bit  
			DECLARE @Rebate bit  				
		              
			--To fetch product info.  		              
			SELECT  P.ProductID   
					, ProductName   
					, ProductLongDescription 
					, ProductShortDescription
					, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN 
											CASE WHEN P.WebsiteSourceFlag = 1 
												THEN @ManufConfig+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+ProductImagePath ELSE ProductImagePath 
										    END   
								   ELSE ProductImagePath 
								   END 
					, UR.Rating  
					, Coupon.Coupon
					, Loyalty.Loyalty  
					, Rebate.Rebate   
		   INTO #Product   
		   FROM Product P   
		   LEFT JOIN UserRating UR ON UR.ProductID = P.ProductID AND UR.UserID = @UserID                                 
		   OUTER APPLY fn_CouponDetails(@ProductID, @RetailID) Coupon  
		   OUTER APPLY fn_LoyaltyDetails(@ProductID, @RetailID) Loyalty  
		   OUTER APPLY fn_RebateDetails(@ProductID, @RetailID) Rebate  
		   WHERE P.ProductID = @ProductID   
		   AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
		                    
		                    
		   SELECT  p.ProductID   
				   , ProductName   
				   , ProductShortDescription
				   , ProductLongDescription 
				   , imagePath		
				   , Rating      
				   , CLRFlag = CASE WHEN (COUNT(p.Coupon)+COUNT(p.Loyalty)+COUNT(p.Rebate)) > 0 THEN 1 ELSE 0 END  
				   , coupon_Status = CASE WHEN COUNT(p.Coupon) = 0 THEN 'Grey'   
										ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
									 END  
				   , loyalty_Status = CASE WHEN COUNT(p.Loyalty) = 0 THEN 'Grey'  
										ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
									  END  
				   , rebate_Status = CASE WHEN COUNT(p.Rebate) = 0 THEN 'Grey'  
										ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
									 END  
		   FROM #Product p  
				LEFT JOIN UserCouponGallery UC ON UC.UserID = @UserID AND UC.CouponID = p.Coupon  
				LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = @UserID AND UL.LoyaltyDealID = p.Loyalty  
				LEFT JOIN UserRebateGallery UR ON UR.UserID = @UserID AND UR.RebateID = p.Rebate   
		  GROUP BY  p.ProductID   
				   , ProductName   
				   , ProductLongDescription 
				   , ProductShortDescription  
				   , imagePath 
				   , Rating     
		        
			--User Tracking
		
			--Capture the click on the productDEAL.
			
			UPDATE HubCitiReportingDatabase..SaleList 
			SET RetailLocationDealClick = 1
			WHERE SaleListID = @SaleListID
			
			UPDATE HubCitiReportingDatabase..ProductList 
			SET ProductClick = 1
			WHERE ProductListID = @ProductListID
			
			--Capture from the scan .
			IF ISNULL(@ScanTypeID, 0) <> 0
			BEGIN
				INSERT INTO HubCitiReportingDatabase..Scan(MainMenuID
														  , ScanTypeID
														  , Success
														  , ID
														  , CreatedDate)
													SELECT  @MainMenuID 
														  , @ScanTypeID 
														  , (CASE WHEN (SELECT COUNT(1) FROM #Product) >0 THEN 1 ELSE 0 END)
														  , ProductID
														  , GETDATE()
													FROM #Product	  		
				
				INSERT INTO HubCitiReportingDatabase..ProductList(ProductID
																 , MainMenuID
																 , DateCreated
																 , ProductClick)
														  VALUES(  @ProductID
																 , @MainMenuID
																 , GETDATE()
																 , 1)								  
														
			END 
				
			--Confirmation of Success.
			SELECT @Status = 0
			COMMIT TRANSACTION
        
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure <>.'           
                  --- Execute retrieval of Error info.  
                  EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;  
              
      END CATCH;  
END;















































GO
