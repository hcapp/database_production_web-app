USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcBandEventCategoriesDisplay_backup]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name					 : [HubCitiapp2_8_7].[usp_HcBandEventsDisplay]
Purpose                                  : To display List fo Events.
Example                                  : [HubCitiapp2_8_7].[usp_HcBandEventsDisplay]

History
Version              Date                 Author               Change Description
------------------------------------------------------------------------------------ 
1.0               04/21/2016			Bindu T A				Initial Version - usp_HcBandEventsDisplay
------------------------------------------------------------------------------------
*/



--exec [HubCitiapp2_8_7].[usp_HcBandEventsDisplay] 2162,10533,null,null,null,null,null,null,0,'clr screen',null,null,null,null,null,null,null,null,null,1 ,null,null,null,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcBandEventCategoriesDisplay_backup]
(   
    --Input variable.        
         @HcHubCitiID int
       , @UserID Int
      -- , @CategoryID VARCHAR(1000)
       , @Latitude Float
       , @Longitude Float
	   
       , @Postalcode Varchar(200)
       , @SearchKey Varchar(1000)
     
      
      -- , @GroupColumn varchar(50)
       , @HcMenuItemID int
       , @HcBottomButtonID int,
	    @BandEventTypeID int,
		@BandID int
      
      -- , @RetailLocationID int
	  -- , @FundRaisingID Int
	  -- , @EventDate Date
	   

       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY


	

	   
       --User Tracking
       declare @MainMenuID Int
	  -- , @BandEventTypeID INT
	   , @Radius INT 
	   
	     , @LowerLimit int  
       , @ScreenName varchar(50)
	    , @SortColumn Varchar(200)
       , @SortOrder Varchar(100)
  
       --Output Variable 
       , @UserOutOfRange bit 
       , @DefaultPostalCode VARCHAR(10) 
       , @MaxCnt int  
       , @NxtPageFlag bit  


	   DECLARE   @RetailID int , @CategoryID int
               
                     DECLARE @UpperLimit int
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     DECLARE @RetailConfig Varchar(1000)
					-- DECLARE @BandUserID Int
					 DECLARE @BandCategoryID VARCHAR(1000)
					 --DECLARE @BandID INT

					-- SET @BandID = @RetailID
					 --SET @BandUserID = @UserID
					 SET @BandCategoryID = @CategoryID

					 DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

					 SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
											   WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
											   WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
											   ELSE @SearchKey END)
											   
					 IF (@Radius IS NULL)
					 BEGIN
					 SELECT @Radius = ISNULL(LocaleRadius, 5)   
					 FROM UserPreference   
					 WHERE UserID = @UserID
					 END

                     SELECT @RetailConfig = 
					 ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Band Media Server Configuration'   

					  IF @Latitude IS NULL AND @Postalcode IS NOT NULL
					 BEGIN
						SELECT @Latitude =Latitude 
						      ,@Longitude =Longitude
						FROM GeoPosition
						WHERE PostalCode =@Postalcode 
						
					 END

                     SELECT @UserLatitude = @Latitude
                           , @UserLongitude = @Longitude                  

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                  , @UserLongitude = Longitude
                           FROM BandUsers A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE BandUserID = @UserID 
                     END
                     --Pick the co ordinates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HcHubCitiID
                     END

                     --print @UserLatitude
                     --print @UserLongitude

					                           
      --               --To get the row count for pagination.   
      --               SELECT @UpperLimit = @LowerLimit +7
					 ----+ScreenContent  
      ----               FROM AppConfiguration   
      ----               WHERE ScreenName = @ScreenName 
      ----               AND ConfigurationType = 'Pagination'
      ----               AND Active = 1

                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HcHubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)

                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @Postalcode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN Users U ON G.PostalCode = U.PostalCode
                                  WHERE U.UserID = @UserID
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @Postalcode
                           END
                     END

                     DECLARE @Config VARCHAR(500)
					 SELECT @Config = 
					 ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'


					 SELECT  HcBandEventID
							,HcBandEventName
							,ShortDescription
							,LongDescription
							,ImagePath
							,EventListingImagePath
							,HcHubCitiID
							,StartDate
							,EndDate
							,HE.DateCreated
							,HE.DateModified
							,CreatedUserID
							,ModifiedUserID
							,MoreInformationURL
							,HcEventRecurrencePatternID
							,RecurrenceInterval
							,EventFrequency
							,OnGoingEvent
							,B.BandID
							,B.BandName
							,Active
					INTO #BandEvents
					FROM hcBandEvents HE
					JOIN Band B ON HE.BandID = B.BandID 
					WHERE (HcHubCitiID =@HcHubCitiID OR HcHubCitiID IS NULL)
				--	AND GETDATE() < ISNULL(EndDate, GETDATE()+1) 
					AND Active = 1			
					
					--SELECT * FROM #BandEvents		

					SELECT Param CatID
					INTO #CategoryList
					FROM fn_SplitParam(@BandCategoryID,',')

					CREATE TABLE #Temp1(Bandid int,EventID Int
                                            ,EventName Varchar(2000) 
											,BandName Varchar(2000)
											,ShortDescription Varchar(2000) 
											,LongDescription Varchar(5000)                                                     
                                            ,HcHubCitiID Int
											,EventAddress Varchar(2000)
											,EventLatitude Float
											,EventLongitude Float
                                            ,ImagePath Varchar(2000) 
                                            ,MoreInformationURL Varchar(2000)                                                    
                                            ,StartDate Date
                                            ,EndDate Date
                                            ,StartTime VARCHAR(15)
                                            ,EndTime VARCHAR(15)                                   
                                            ,MenuItemExist Int
                                            ,EventCategoryID INT
                                            ,EventCategoryName Varchar(5000)
										    ,Distance Int                                                                            
										    ,DistanceActual	Int									  
                                            ,OnGoingEvent Int
										    ,RetailLocationID Int
											,Latitude Float
											,Longitude Float
											,SignatureEvent bit)

					
                      INSERT INTO #Temp1(Bandid,EventID 
												,EventName
												,BandName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,EventAddress
												,EventLatitude
												,EventLongitude
												,ImagePath 
												,MoreInformationURL                                               
												,StartDate
												,EndDate 
												,StartTime
												,EndTime                                
												,MenuItemExist
												,EventCategoryID
												,EventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,Latitude 
												,Longitude
												,SignatureEvent)
							 SELECT bandid,EventID 
												,EventName
												,BandName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,EventAddress
												,EventLatitude
												,EventLongitude
												,ImagePath 
												,MoreInformationURL                                               
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,Latitude 
												,Longitude
												,SignatureEvent
							FROM(

							SELECT DISTINCT  E.bandid, E.HcbandEventID AS EventID
													,E.hcBandEventName  AS EventName
													,E.BandName
													,ShortDescription ShortDescription
													,LongDescription   LongDescription                                                  
													,E.HcHubCitiID  HcHubCitiID
													,isnull(HL.Address,'')+','+ isnull(HL.city,'')+','+ isnull(HL.State,'')+','+ isnull(HL.PostalCode,'') EventAddress
													,HL.Latitude EventLatitude
													,HL.Longitude EventLongitude
													,ImagePath=IIF(E.BandID IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.BandID AS VARCHAR)+'/'+ ISNULL(EventListingImagePath,ImagePath)) 
													,E.MoreInformationURL                                                  
													,StartDate =CAST(StartDate AS DATE)
													,EndDate =CAST(EndDate AS DATE)
													,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(StartDate AS Time),100),'AM',' AM') ,'PM',' PM')StartTime
													,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(EndDate AS Time),100),'AM',' AM') ,'PM',' PM') EndTime
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Events' 
																					 AND MI.LinkID = E.HcBandEventID)>0 THEN 1 ELSE 0 END  
												   ,EC.HcBandEventCategoryID  AS HcEventCategoryID
												   ,EC.HcBandEventCategoryName AS HcEventCategoryName
												    						     ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
								                    ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
												   ,E.OnGoingEvent  OnGoingEvent
												   ,NULL RetailLocationID 
												   ,Latitude = ISNULL(HL.Latitude,NULL)
												   ,Longitude = ISNULL(HL.Longitude,NULL)
												   ,SignatureEvent = CASE WHEN (EC.HcBandEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END
							 FROM #BandEvents  E 
							 INNER JOIN HcBandEventsCategoryAssociation EA ON EA.HcBandEventID =E.hcBandEventID AND (E.HcHubCitiID =@HcHubCitiID OR E.HcHubCitiID IS NULL)
							 INNER JOIN HcBandEventsCategory EC ON EC.HcBandEventCategoryID =EA.HcBandEventCategoryID
							 --INNER JOIN HcMenuItemBandCategories MIC ON EC.HcBandEventCategoryID = MIC.HcEventCategoryID AND MIC.HcHubCitiID =@HcHubCitiID AND MIC.HcMenuItemID = @HcMenuItemID 
							 LEFT JOIN HcBandEventLocation HL ON HL.HcBandEventID =E.HcBandEventID 
							 LEFT JOIN HcBandEventAppsite A ON E.hcBandEventID = A.HcBandEventID AND A.HcHubCitiID = @HcHubCitiID AND A.HcBandEventID IS NULL
							 LEFT JOIN HcBandAppSite HA ON A.HcBandAppSiteID = HA.HcBandAppSiteID AND HA.HcHubCitiID = @HcHubCitiID 
							    LEFT JOIN Geoposition G ON (G.Postalcode=HL.Postalcode)                     
							 --LEFT JOIN HcEventPackage EP ON E.HcEventID =EP.HcEventID AND EP.HcHubCitiID = @HcHubCitiID                                      
							 --LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HcHubCitiID AND Associated = 1 AND (E.RetailID = RA.RetailID OR 1=1)
							 --LEFT JOIN #RLListss RRR ON RRR.RetailLocationID =RA.RetailLocationID 
							 --LEFT JOIN RetailLocation RL1 ON (RL1.RetailLocationID = RRR.RetailLocationID) AND RL1.Active = 1 --OR (RL1.RetailLocationID = HA.RetailLocationID) OR (RRR.RetailLocationID =RL1.RetailLocationID)                
							 LEFT JOIN HcBandEventsAssociation RE ON  E.HcBandEventID = RE.HcBandEventID --AND  RE.RetailLocationID = RA.RetailLocationID--AND  RE.HcEventID IS NULL							 
							 WHERE ((EA.HcHubCitiID =@HcHubCitiID) OR (E.HcHubCitiID IS NULL)) --AND E.BandID = RA.BandID))
							 AND ((ISNULL(@CategoryID, '0') <> '0' 
							 AND EA.HcBandEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))
							 --AND GETDATE() < ISNULL(EndDate, GETDATE()+1)
							 			
							  AND E.Active = 1							
							 GROUP BY E.hcbandEventID 
									  , hcbandEventName
									  , E.BandID
									  , BandName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  , isnull(HL.Address,'')+','+ isnull(HL.city,'')+','+ isnull(HL.State,'')+','+ isnull(HL.PostalCode,'')
									  , HL.Latitude
									  , HL.Longitude   
									  , ImagePath
									  , E.MoreInformationURL
									  , EventListingImagePath
									  , StartDate
									  , EndDate
									  , EC.HcBandEventCategoryID
									  , EC.HcBandEventCategoryName
									  , OnGoingEvent
									  , E.BandID
									  , RE.HcBandEventID
									  --, RL1.RetailLocationID
									  , hl.Latitude
									  , hl.Longitude
									  ) E

						--SELECT * FROM #Temp1

                 CREATE TABLE #Events(RowNum INT IDENTITY(1, 1)
                                    ,HcEventID Int
                                    ,HcEventName  Varchar(2000)
									,BandID INT
									,BandName VARCHAR(2000)
                                    ,ShortDescription Varchar(2000)
                                    ,LongDescription  Varchar(8000)                                                
                                    ,HcHubCitiID Int
									,EventAddress Varchar(2000)
									,EventLatitude Float
									,EventLongitude Float
                                    ,ImagePath Varchar(2000) 
									,MoreInformationURL Varchar(2000)                                             
                                    ,StartDate date
                                    ,EndDate date
                                    ,StartTime VARCHAR(15)
                                    ,EndTime VARCHAR(15)                                        
                                    ,MenuItemExist bit
                                    ,HcEventCategoryID INT
                                    ,HcEventCategoryName Varchar(2000)
                                    ,Distance Float
                                    ,DistanceActual float
                                    ,OnGoingEvent bit
									,SignatureEvent bit)


					if (@BandID is not null and  @BandEventTypeID is  null)			
					begin
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName  
										,BandID
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath        
										,MoreInformationURL                                         
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,BandID
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath     
										   ,MoreInformationURL                                                
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
								 FROM #Temp1        where  bandid=@BandID 
								 GROUP BY EventID 
										 ,EventName  
										 ,BandID
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath   
										 ,MoreInformationURL                                                  
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC

									end

					if (@BandID is  null and @BandEventTypeID is not null)         

					BEGIN

					if @BandEventTypeID=2---Near by EVENT
					
					  begin

                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
								 FROM #Temp1  --where  Distance <= ISNULL(@Radius, 5) 
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									end

									
								

									if @BandEventTypeID=1---ON GOING  EVENT
					
					  begin

					 
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
								 FROM #Temp1 WHERE CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)) between
								   CONVERT(DATETIME,CONVERT(VARCHAR,StartDate,101))
						 AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,StartDate),101))
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									END


									if @BandEventTypeID=3---Up coming EVENT
					
					  begin

			

                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
								 FROM #Temp1 WHERE ( CONVERT(DATETIME,CONVERT(VARCHAR,StartDate,101))>CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)) 
								 and  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)))
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									enD

									enD

									
					if (@BandID is null AND @BandEventTypeID is null AND ISNULL(@SearchKey,'')= '')   
					begin
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                   
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                   
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
								 FROM #Temp1       -- where  Distance <= ISNULL(@Radius, 5) 
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                               
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC

					end

					if (@BandEventTypeID is  null  and @BandID is null AND ISNULL(@SearchKey,'') <> '')   -- Search Button
					BEGIN
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName  
										,BandID
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath         
										,MoreInformationURL                                          
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent)
								 SELECT   EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                    
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
								 FROM #Temp1 -- WHERE ( isnull(Eventname,'') LIKE '%'+@SearchKey+'%' OR isnull(EventAddress,'') LIKE '%'+@SearchKey+'%')
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                             
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC

					end



					--select * from #Events


					--SELECT * FROM #Temp1
											
                     --To capture max row number.  
                     --SELECT @MaxCnt = count(distinct hcEventCategoryID) FROM #Events
                           

                     --this flag is a indicator to enable "More" button in the UI.   
                     --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                     --SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

                     ---User Tracking

           
                  --   CREATE TABLE #Temp2(EventsListID INT, HcEventID Int)
                     
                  --   INSERT INTO HubCitiReportingDatabase..EventsList(MainMenuID
                  --                                                   ,HcEventID 
                  --                                                   ,EventClick 
                  --                                                   ,DateCreated)

                  --   OUTPUT Inserted.EventsListID,inserted.HCEventID INTO #Temp2(EventsListID ,HcEventID)

                  --   SELECT @MainMenuID
                  --         ,HcEventID
                  --         ,0
                  --         ,GETDATE()
                  --FROM #Events  
				  
                     
                  --   DECLARE @Seed INT
                  --   SELECT @Seed = ISNULL(MIN(EventsListID), 0)
                  --   FROM #Temp2                
                     
                     SELECT DISTINCT RowNum = Row_Number() over(order by Distance asc)
                              ,HcEventID  
                              ,HcEventName  
							  ,BandID
							  ,BandName
                              ,ShortDescription 
                              ,LongDescription                                                  
                              ,HcHubCitiID 
							  ,EventAddress
							 ,EventLatitude
							 ,EventLongitude
                              ,ImagePath 
							  ,MoreInformationURL                                                 
                              ,StartDate 
                              ,EndDate 
                              ,StartTime 
                              ,EndTime                                       
                              ,MenuItemExist 
                              ,HcEventCategoryID 
                              ,HcEventCategoryName
                              ,Distance
                              ,DistanceActual
                              ,OnGoingEvent 
                     INTO #Event
                     FROM #Events
					 where (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (HcEventName LIKE '%'+@SearchKey+'%' OR EventAddress LIKE '%'+@SearchKey+'%'))
							OR (@SearchKey IS NULL))

							
					

					--SELECT * FROM #Event

					-- WHERE ( isnull(hcEventname,'') LIKE '%'+@SearchKey+'%' OR isnull(EventAddress,'') LIKE '%'+@SearchKey+'%')
										
                     --DECLARE @SQL VARCHAR(1000) = 'ALTER TABLE #Event ADD EventsListID1 INT IDENTITY('+CAST(@Seed AS VARCHAR(100))+', 1)'
                     --EXEC (@SQL)
                     
                     SELECT DISTINCT --Rownum rowNum
                           --,T.EventsListID eventListId                                    
       --                    ,E.HcEventID eventId
       --                    ,HcEventName eventName
						 --  ,BandID 
						 --  ,BandName 
       --                    ,ShortDescription shortDes
							--,LongDescription longDes                                                      
       --                      ,HcHubCitiID hubCitiId
							-- ,EventAddress Address
							-- ,EventLatitude Latitude
							-- ,EventLongitude Longitude
       --                      ,ImagePath imgPath
							-- ,MoreInformationURL moreInfoURL                                             
       --                      ,StartDate 
       --                      ,EndDate 
       --                      ,StartTime
                             --,EndTime                        
                            -- ,MenuItemExist mItemExist
                              HcEventCategoryID busCatId
                             ,HcEventCategoryName busCatName      
                            -- ,Distance   = ISNULL(DistanceActual, Distance) 
                            -- ,ISNULL(OnGoingEvent, 0) isOnGoing                                                     
                     FROM #Event E
                     --INNER JOIN #Temp1 T ON T.EventsListID=E.EventsListID1
                    -- WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  
                     --ORDER BY  RowNum asc

                     --To get list of BottomButtons for this Module
                     DECLARE @ModuleName Varchar(500) 
                     SET @ModuleName = 'Event'

                     --EXEC [HubCitiApp7].[usp_HcFunctionalityBottomButtonDisplay] @HcHubCitiID,@ModuleName,@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                

/*
					 SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			     , HM.HcHubCitiID 
				 , BB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 --, btnLinkID = BottomButtonLinkID
				 --, btnLinkTypeName = (CASE  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
					--			                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HcHubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
					--																																            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
					--																																            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID)
			                      	 
			  --                     ELSE BottomButtonLinkTypeName END)
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HcHubCitiID), BottomButtonLinkID) 
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                                                                                       INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                                                                                       INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                                                                                       WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID)
										  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
								                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HcHubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																					            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																					            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HcHubCitiID)
			                      	 
			                       ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			WHERE LT.LinkTypeDisplayName = 'event' AND FBB.HcHubCitiID = @HcHubCitiID
			ORDER by HcFunctionalityBottomButtonID
             */
			 
			--Confirmation of Success
			SELECT @Status = 0    
			
					 
       select 'rr11'     
       END TRY
              
       BEGIN CATCH

		
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN  
			     select ERROR_LINE()       
                     PRINT 'Error occured in Stored Procedure usp_HcEventsDisplay.'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;









GO
