USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcEventsOptionEventDateDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name					 : [HubCitiApp2_3_3].[usp_HcEventsOptionEventDateDisplay]
Purpose                                  : To display List fo Event dates wrt Region and associated HubCities.
Example                                  : [HubCitiApp2_3_3].[usp_HcEventsOptionEventDateDisplay]

History
Version              Date                 Author               Change Description
--------------------------------------------------------------- 
1.0                  5th Feb 2014		  Pavan Sharma K       1.0
---------------------------------------------------------------
*/


CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcEventsOptionEventDateDisplay]
(   
    --Input variable.        
         @HubCitiID int
       , @UserID Int
       
       , @Latitude Float
       , @Longitude Float
       , @Postalcode Varchar(200)
       
       , @HcMenuItemID int
       , @HcBottomButtonID int
       , @SearchParameter Varchar(1000)

       --User Tracking
      
       --Output Variable 
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
               
                     DECLARE @UpperLimit int
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     DECLARE @RetailConfig Varchar(1000)
					 DECLARE @CategoryID Int
					 
                     DECLARE @LowerLimit int  
                     DECLARE @ScreenName varchar(50)
                     DECLARE @SortColumn Varchar(200)
                     DECLARE @SortOrder Varchar(100)
                     DECLARE @GroupColumn varchar(50)



					 DECLARE @RetailID int
					 DECLARE @RetailLocationID int
					 DECLARE @CityIDs Varchar(2000)
					 DECLARE  @FundRaisingID Int	
					 DECLARE @MaxCnt int  
					  DECLARE @NxtPageFlag bit 
					  DECLARE @MainMenuID Int
  


                     SELECT @RetailConfig = ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Retailer Media Server Configuration'   

					 
                           IF @Postalcode IS NOT NULL AND @Latitude IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                WHERE   G.PostalCode = @Postalcode 
                                 
                           END
                           


                     SELECT @UserLatitude = @Latitude
                           , @UserLongitude = @Longitude                  

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                  , @UserLongitude = Longitude
                           FROM HcUser A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE HcUserID = @UserID 
                     END
                     --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HubCitiID
                     END

                     print @UserLatitude
                     print @UserLongitude
                           
                     --To get the row count for pagination.   
                     SELECT @UpperLimit = @LowerLimit + ScreenContent   
                     FROM AppConfiguration   
                     WHERE ScreenName = @ScreenName 
                     AND ConfigurationType = 'Pagination'
                     AND Active = 1

					
                   

                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp7].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)

                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @Postalcode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                  WHERE U.HcUserID = @UserID
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @Postalcode
                           END
                     END


                     DECLARE @Config VARCHAR(500)
					 SELECT @Config = ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'


					 --IF(@HubCitiID =19)
					 --BEGIN

					 IF EXISTS(SELECT 1 from HcRegionAppHubcitiAssociation WHERE HcRegionAppID=@HubCitiID)
			 BEGIN


															 IF (@RetailID IS NULL AND @FundRaisingID IS NULL)
															 BEGIN
					 
															--MenuItem
															SELECT DISTINCT H.HcHubCitiID,HE.HcEventCategoryID,E.HcEventCategoryName,HE.HcEventID
															INTO #RegionCatt
															FROM HcHubCiti H                     
															INNER JOIN HcEventsCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID AND HE.HcHubCitiID=@HubCitiID
															INNER JOIN HcEventsCategory E ON HE.HcEventCategoryID = E.HcEventCategoryID
															INNER JOIN HcMenuItemEventCategoryAssociation ME ON HE.HcEventCategoryID = ME.HcEventCategoryID AND ME.HcMenuItemID = @HcMenuItemID					

 
															CREATE TABLE #AllEventCategoriess(HubcitiID INT,EventCategoryID INT,EventCategoryName VARCHAR(100),EventID INT)

															INSERT INTO #AllEventCategoriess(HubcitiID,EventCategoryID,EventCategoryName,EventID) 
															SELECT DISTINCT HR.HcHubCitiID,HE.HcEventCategoryID,E.HcEventCategoryName,HE.HcEventID
															FROM HcHubCiti H
															LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
															--INNER JOIN HcEvents E ON HR.HcHubCitiID = E.HcHubCitiID  
															INNER JOIN HcEventsCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
															INNER JOIN HcEventsCategory E ON HE.HcEventCategoryID = E.HcEventCategoryID
															INNER JOIN #RegionCatt RC ON HE.HcEventCategoryID = RC.HcEventCategoryID
															INNER JOIN HcMenuItemEventCategoryAssociation ME ON HE.HcEventCategoryID = ME.HcEventCategoryID AND ME.HcMenuItemID = @HcMenuItemID 
															WHERE HR.HcRegionAppID = @HubCitiID 
                    
															UNION ALL

															SELECT HcHubCitiID,HcEventCategoryID,HcEventCategoryName,HcEventID
															FROM #RegionCatt

															--BottomButton
															SELECT DISTINCT H.HcHubCitiID,HE.HcEventCategoryID,E.HcEventCategoryName,HE.HcEventID
															INTO #RegionCat11
															FROM HcHubCiti H                     
															INNER JOIN HcEventsCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
															INNER JOIN HcEventsCategory E ON HE.HcEventCategoryID = E.HcEventCategoryID
															INNER JOIN HcBottomButtonEventCategoryAssociation ME ON HE.HcEventCategoryID = ME.HcEventCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID
															WHERE H.HcHubCitiID = @HubCitiID

 
															CREATE TABLE #AllEventCategories11(HubcitiID INT,EventCategoryID INT,EventCategoryName VARCHAR(100),EventID INT)

															INSERT INTO #AllEventCategories11(HubcitiID,EventCategoryID,EventCategoryName,EventID) 
															SELECT DISTINCT HR.HcHubCitiID,HE.HcEventCategoryID,E.HcEventCategoryName,HE.HcEventID
															FROM HcHubCiti H
															LEFT JOIN HcRegionAppHubcitiAssociation HR ON H.HcHubCitiID = HR.HcRegionAppID
															--INNER JOIN HcEvents E ON HR.HcHubCitiID = E.HcHubCitiID  
															INNER JOIN HcEventsCategoryAssociation HE ON HR.HcHubcitiID = HE.HcHubCitiID
															INNER JOIN HcEventsCategory E ON HE.HcEventCategoryID = E.HcEventCategoryID
															INNER JOIN #RegionCat11 RC ON HE.HcEventCategoryID = RC.HcEventCategoryID
															INNER JOIN HcBottomButtonEventCategoryAssociation ME ON HE.HcEventCategoryID = ME.HcEventCategoryID AND ME.HcBottomButtonID = @HcBottomButtonID 
															WHERE HR.HcRegionAppID = @HubCitiID 
                    
															UNION ALL

															SELECT HcHubCitiID,HcEventCategoryID,HcEventCategoryName,HcEventID
															FROM #RegionCat11

															CREATE TABLE #RHubcitiListt(HchubcitiID Int) 
															INSERT INTO #RHubcitiListt( HchubcitiID)
															SELECT HcHubCitiID
															FROM
																(SELECT RA.HcHubCitiID
																FROM HcHubCiti H
																LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
																WHERE H.HcHubCitiID  = @HubcitiID
																UNION ALL
																SELECT @HubcitiID)A

															DECLARE @HubCitis varchar(100)
															SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
															FROM #RHubcitiListt

															END

															 SELECT Param CityID
															 INTO #Events111
															 from fn_SplitParam(@CityIDs,',')

															CREATE TABLE #Temp1111(HcEventID Int
																		,HcEventName Varchar(2000) 
																		,ShortDescription Varchar(2000) 
																		,LongDescription Varchar(2000)                                                     
																		,HcHubCitiID Int
																		,ImagePath Varchar(2000) 
																		,BussinessEvent Int
																		,PackageEvent Int								                                               
																		,StartDate Date
																		,EndDate Date
																		,StartTime Time
																		,EndTime Time                                   
																		,MenuItemExist Int
																		,HcEventCategoryID Int 
																		,HcEventCategoryName Varchar(500)
																		,Distance Int                                                                            
																		,DistanceActual	Int									  
																		,OnGoingEvent Int
																		,RetailLocationID Int
																		,City Varchar(100)
																		,Lat Float
																		,Long Float
																		,SignatureEvent bit)
								
															IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
															BEGIN
					
															INSERT INTO #Temp1111(HcEventID 
																				,HcEventName
																				,ShortDescription 
																				,LongDescription                                                    
																				,HcHubCitiID 
																				,ImagePath 
																				,BussinessEvent
																				,PackageEvent   									                                              
																				,StartDate
																				,EndDate 
																				,StartTime 
																				,EndTime                                
																				,MenuItemExist
																				,HcEventCategoryID
																				,HcEventCategoryName 
																				,Distance                                                                        
																				,DistanceActual									  
																				,OnGoingEvent 
																				,RetailLocationID
																				,City
																				,Lat 
																				,Long
																				,SignatureEvent)
																SELECT   HcEventID 
																		,HcEventName
																		,ShortDescription 
																		,LongDescription                                                    
																		,HcHubCitiID 
																		,ImagePath 
																		,BussinessEvent
																		,PackageEvent 							                                           
																		,StartDate
																		,EndDate 
																		,StartTime 
																		,EndTime                                
																		,MenuItemExist
																		,EventCategoryID
																		,EventCategoryName 
																		,Distance                                                                        
																		,DistanceActual									  
																		,OnGoingEvent 
																		,RetailLocationID
																		,CityName
																		,RetailLocationLatitude 
																		,RetailLocationLongitude
																		,SignatureEvent
															FROM(
															SELECT DISTINCT  E.HcEventID 
																			,HcEventName  
																			,ShortDescription
																			,LongDescription                                                     
																			,E.HcHubCitiID 	
																			,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																			,BussinessEvent 
																			,PackageEvent                                                     
																			,StartDate =CAST(StartDate AS DATE)
																			,EndDate =CAST(EndDate AS DATE)
																			,StartTime =CAST(StartDate AS Time)
																			,EndTime =CAST(EndDate AS Time)                                     
																			,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
																								FROM HcMenuItem MI 
																								INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																								WHERE LinkTypeName = 'Events' 
																								AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
																			,A.EventCategoryID 
																			,A.EventCategoryName 
																			,Distance=0                                           				
																			,DistanceActual =0				
																			,E.OnGoingEvent 
																			,RL1.RetailLocationID 
																			,C.CityName	
																			,RL1.RetailLocationLatitude 
																			,RL1.RetailLocationLongitude
																			,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 															 					   							
															FROM HcEvents E 
															INNER JOIN #AllEventCategoriess A ON E.HcEventID = A.EventID AND E.RetailID IS NULL
															INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID
															LEFT JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
															LEFT JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
															LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
															LEFT JOIN RetailLocation  RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (RL1.RetailLocationID =EP.RetailLocationID)
															--LEFT JOIN RetailLocation RL1 ON HA.RetailLocationID = RL1.RetailLocationID OR (RL1.RetailLocationID =EP.RetailLocationID) OR (RA.RetailLocationID =RL1.RetailLocationID)
															--LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID AND  RE.HcEventID IS NULL	
															LEFT JOIN HcCity C ON HL.HcHubCitiID = C.HcCityID-- OR RL1.City = C.CityName)
															LEFT JOIN #Events111 EE ON EE.CityID =C.HcCityID 									                         
															WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
															AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =EE.CityID))
															AND (( ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
															AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
															GROUP BY E.HcEventID 
																	, HcEventName
																	, ShortDescription
																	, LongDescription                                                     
																	, E.HcHubCitiID 
																	, BussinessEvent
																	, PackageEvent
																	, ImagePath    		
																	, StartDate
																	, EndDate
																	, A.EventCategoryID 
																	, A.EventCategoryName
																	, OnGoingEvent
																	, E.RetailID
																	--, RE.HcEventID
																	, RL1.RetailLocationID
																	, C.CityName
																	,RL1.RetailLocationLatitude 
																			,RL1.RetailLocationLongitude 

																UNION ALL

																SELECT DISTINCT TOP 100 Percent E.HcEventID 
																			,HcEventName  
																			,ShortDescription
																			,LongDescription                                                     
																			,E.HcHubCitiID 	
																			,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																			,BussinessEvent 
																			,PackageEvent                                                     
																			,StartDate =CAST(StartDate AS DATE)
																			,EndDate =CAST(EndDate AS DATE)
																			,StartTime =CAST(StartDate AS Time)
																			,EndTime =CAST(EndDate AS Time)                                     
																			,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
																								FROM HcMenuItem MI 
																								INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																								WHERE LinkTypeName = 'Events' 
																								AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
								
																			,A.EventCategoryID 
																			,A.EventCategoryName 
																			,Distance=0                                           				
																			,DistanceActual =0				
																			,E.OnGoingEvent 
																			,RetailLocationID=0 
																			,C.CityName	
																			,RL1.Latitude  RetailLocationLatitude
																			,RL1.Longitude 	RetailLocationLongitude
																			,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 														 					   							
															FROM HcEvents E 
															INNER JOIN #AllEventCategoriess A ON E.HcEventID = A.EventID AND E.RetailID IS NULL
															INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
															INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
															LEFT JOIN HcCity C ON HL.City = C.CityName-- OR RL1.City = C.CityName)
															LEFT JOIN #Events111 EE ON EE.CityID =C.HcCityID 									                         
															WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
															AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =EE.CityID))
															AND (( ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
															AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
															GROUP BY E.HcEventID 
																	, HcEventName
																	, ShortDescription
																	, LongDescription                                                     
																	, E.HcHubCitiID 
																	, BussinessEvent
																	, PackageEvent
																	, ImagePath    		
																	, StartDate
																	, EndDate
																	, A.EventCategoryID 
																	, A.EventCategoryName
																	, OnGoingEvent
																	, E.RetailID
																	--, RE.HcEventID
																					, C.CityName
																	,RL1.Latitude  
																			,RL1.Longitude
							
																UNION ALL
						
																SELECT DISTINCT  E.HcEventID 
																			,HcEventName  
																			,ShortDescription
																			,LongDescription                                                     
																			,E.HcHubCitiID 	
																			,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																			,BussinessEvent 
																			,PackageEvent                                                     
																			,StartDate =CAST(StartDate AS DATE)
																			,EndDate =CAST(EndDate AS DATE)
																			,StartTime =CAST(StartDate AS Time)
																			,EndTime =CAST(EndDate AS Time)                                     
																			,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
																								FROM HcMenuItem MI 
																								INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																								WHERE LinkTypeName = 'Events' 
																								AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
																			,A.EventCategoryID 
																			,A.EventCategoryName 
																			,Distance=0                                           				
																			,DistanceActual =0				
																			,E.OnGoingEvent 
																			,RL.RetailLocationID 
																			,HC.CityName
																			,RL.RetailLocationLatitude 
																			,RL.RetailLocationLongitude
																			,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END  			
																FROM HcLocationAssociation hl
																INNER JOIN #AllEventCategoriess a on hl.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
																INNER JOIN HcRetailerAssociation r on a.HubcitiID = r.HcHubCitiID and associated=1
																INNER JOIN HcEvents e on r.RetailID = e.RetailID and e.RetailID is not null
																INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
																--INNER JOIN HcEventsCategory ec on c.HcEventCategoryID=ec.HcEventCategoryID
															INNER JOIN HcEventLocation el on e.HcEventID = el.HcEventID
																LEFT JOIN HcRetailerEventsAssociation l on e.HcEventID = l.HcEventID and r.RetailID = l.RetailID
																LEFT JOIN RetailLocation rl on rl.RetailLocationID=l.RetailLocationID AND rl.Active = 1
																INNER JOIN HcCity Hc ON RL.City = Hc.CityName OR el.City = Hc.CityName
																LEFT JOIN #Events111 EE ON EE.CityID =HC.HcCityID 	
																WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID =EE.CityID))
																AND (( ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
																AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
																GROUP BY E.HcEventID 
																	, HcEventName
																	, ShortDescription
																	, LongDescription                                                     
																	, E.HcHubCitiID 
																	, BussinessEvent
																	, PackageEvent
																	, ImagePath    		
																	, StartDate
																	, EndDate
																	, A.EventCategoryID 
																	, A.EventCategoryName
																	, OnGoingEvent
																	, E.RetailID
																	--, RE.HcEventID
																	, RL.RetailLocationID
																	, HC.CityName
																	, RL.RetailLocationLatitude 
																	,RL.RetailLocationLongitude 	
																	) E
															END
															ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
															BEGIN
																INSERT INTO #Temp1111(HcEventID 
																				,HcEventName
																				,ShortDescription 
																				,LongDescription                                                    
																				,HcHubCitiID 
																				,ImagePath 
																				,BussinessEvent
																				,PackageEvent   									                                              
																				,StartDate
																				,EndDate 
																				,StartTime 
																				,EndTime                                
																				,MenuItemExist
																				,HcEventCategoryID
																				,HcEventCategoryName 
																				,Distance                                                                        
																				,DistanceActual									  
																				,OnGoingEvent 
																				,RetailLocationID
																				,City
																				,SignatureEvent
																				)
																SELECT   HcEventID 
																		,HcEventName
																		,ShortDescription 
																		,LongDescription                                                    
																		,HcHubCitiID 
																		,ImagePath 
																		,BussinessEvent
																		,PackageEvent 							                                           
																		,StartDate
																		,EndDate 
																		,StartTime 
																		,EndTime                                
																		,MenuItemExist
																		,EventCategoryID
																		,EventCategoryName 
																		,Distance                                                                        
																		,DistanceActual									  
																		,OnGoingEvent 
																		,RetailLocationID
																		,CityName
																		,SignatureEvent
															FROM(
															SELECT DISTINCT  E.HcEventID 
																			,HcEventName  
																			,ShortDescription
																			,LongDescription                                                     
																			,E.HcHubCitiID 	
																			,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																			,BussinessEvent 
																			,PackageEvent                                                     
																			,StartDate =CAST(StartDate AS DATE)
																			,EndDate =CAST(EndDate AS DATE)
																			,StartTime =CAST(StartDate AS Time)
																			,EndTime =CAST(EndDate AS Time)                                     
																			,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
																								FROM HcMenuItem MI 
																								INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																								WHERE LinkTypeName = 'Events' 
																								AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
																			,A.EventCategoryID 
																			,A.EventCategoryName 
																			,Distance=0                                           				
																			,DistanceActual =0				
																			,E.OnGoingEvent 
																			,RL1.RetailLocationID 
																			,C.CityName
																			,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 																 					   							
															FROM HcEvents E 
															INNER JOIN #AllEventCategories11 A ON E.HcEventID = A.EventID
															INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID
															LEFT JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
															LEFT JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
															LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID	
															LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = A.HubCitiID AND Associated =1
															LEFT JOIN RetailLocation RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (RL1.RetailLocationID =EP.RetailLocationID) --OR (RA.RetailLocationID =RL1.RetailLocationID)
															--LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID AND  RE.HcEventID IS NULL	
															LEFT JOIN HcCity C ON (HL.City = C.CityName OR RL1.HcCityID = C.HcCityID)									                         
															WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
															AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
															AND (( ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
															AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
															GROUP BY E.HcEventID 
																	, HcEventName
																	, ShortDescription
																	, LongDescription                                                     
																	, E.HcHubCitiID 
																	, BussinessEvent
																	, PackageEvent
																	, ImagePath    		
																	, StartDate
																	, EndDate
																	, A.EventCategoryID 
																	, A.EventCategoryName
																	, OnGoingEvent
																	, E.RetailID
																	--, RE.HcEventID
																	, RL1.RetailLocationID
																	, C.CityName
							
																UNION ALL
						
																SELECT DISTINCT  E.HcEventID 
																			,HcEventName  
																			,ShortDescription
																			,LongDescription                                                     
																			,E.HcHubCitiID 	
																			,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																			,BussinessEvent 
																			,PackageEvent                                                     
																			,StartDate =CAST(StartDate AS DATE)
																			,EndDate =CAST(EndDate AS DATE)
																			,StartTime =CAST(StartDate AS Time)
																			,EndTime =CAST(EndDate AS Time)                                     
																			,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
																								FROM HcMenuItem MI 
																								INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																								WHERE LinkTypeName = 'Events' 
																								AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
																			,A.EventCategoryID 
																			,A.EventCategoryName 
																			,Distance=0                                           				
																			,DistanceActual =0				
																			,E.OnGoingEvent 
																			,RL.RetailLocationID 
																			,HC.CityName
																			,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 			
																FROM HcLocationAssociation hl
																INNER JOIN #AllEventCategories11 a on hl.HcHubCitiID in (select param from fn_SplitParam(@HubCitis,','))
																INNER JOIN HcRetailerAssociation r on a.HubcitiID = r.HcHubCitiID and associated=1
																INNER JOIN HcEvents e on r.RetailID = e.RetailID and e.RetailID is not null
																INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
																INNER JOIN HcEventsCategory ec on c.HcEventCategoryID=ec.HcEventCategoryID
																INNER JOIN HcEventLocation el on e.HcEventID = el.HcEventID
																LEFT JOIN HcRetailerEventsAssociation l on e.HcEventID = l.HcEventID and r.RetailID = l.RetailID
																LEFT JOIN RetailLocation rl on rl.RetailLocationID=l.RetailLocationID AND rl.Active = 1 
																INNER JOIN HcCity Hc ON RL.HcCityID = Hc.HcCityID OR el.City = Hc.CityName
																WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
																AND (( ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
																AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
																GROUP BY E.HcEventID 
																	, HcEventName
																	, ShortDescription
																	, LongDescription                                                     
																	, E.HcHubCitiID 
																	, BussinessEvent
																	, PackageEvent
																	, ImagePath    		
																	, StartDate
																	, EndDate
																	, A.EventCategoryID 
																	, A.EventCategoryName
																	, OnGoingEvent
																	, E.RetailID
																	--, RE.HcEventID
																	, RL.RetailLocationID
																	, HC.CityName	
																	) E

															END
															ELSE IF (@RetailID IS NOT NULL)
															BEGIN
																	 INSERT INTO #Temp1111(HcEventID 
																						,HcEventName
																						,ShortDescription 
																						,LongDescription                                                    
																						,HcHubCitiID 
																						,ImagePath 
																						,BussinessEvent
																						,PackageEvent                                                 
																						,StartDate
																						,EndDate 
																						,StartTime 
																						,EndTime                                
																						,MenuItemExist
																						,HcEventCategoryID
																						,HcEventCategoryName 
																						,Distance                                                                        
																						,DistanceActual									  
																						,OnGoingEvent 
																						,RetailLocationID
																						,City
																						,SignatureEvent)
																	 SELECT HcEventID 
																						,HcEventName
																						,ShortDescription 
																						,LongDescription                                                    
																						,HcHubCitiID 
																						,ImagePath 
																						,BussinessEvent
																						,PackageEvent                                                 
																						,StartDate
																						,EndDate 
																						,StartTime 
																						,EndTime                                
																						,MenuItemExist
																						,HcEventCategoryID
																						,HcEventCategoryName 
																						,Distance                                                                        
																						,DistanceActual									  
																						,OnGoingEvent 
																						,RetailLocationID
																						,CityName
																						,SignatureEvent
																	FROM
																	(SELECT DISTINCT E.HcEventID 
																							,HcEventName  
																							,ShortDescription
																							,LongDescription                                                     
																							,E.HcHubCitiID 
																							,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 
																							,BussinessEvent 
																							,PackageEvent                                                     
																							,StartDate =CAST(StartDate AS DATE)
																							,EndDate =CAST(EndDate AS DATE)
																							,StartTime =CAST(StartDate AS Time)
																							,EndTime =CAST(EndDate AS Time)                                     
																							,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																															 FROM HcMenuItem MI 
																															 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																															 WHERE LinkTypeName = 'Events' 
																															 AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
																						   ,EC.HcEventCategoryID 
																						   ,EC.HcEventCategoryName 
																						   ,Distance=0                                           
																				   --,Distance = IIF(RE.HcEventID IS NULL,(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)),(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)))
																					   --,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
																						   ,DistanceActual =0
																						   --= CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
																						   ,E.OnGoingEvent 
																						   ,RL1.RetailLocationID 	
																						   ,HC.CityName	
																						   ,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 
												   				 
																	 FROM HcEvents E 
																	 INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
																	 INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
																	 INNER JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 							                                    
																	 INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated =1							 
																	 INNER JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID
																	 INNER JOIN RetailLocation RL1 ON RL1.RetailLocationID =RE.RetailLocationID	AND RL1.Active = 1
																	 INNER JOIN HcCity Hc ON RL1.HcCityID = Hc.HcCityID OR HL.City = Hc.CityName
																	 --LEFT JOIN HcFundraisingEventsAssociation F ON F.HcEventID=E.HcEventID	
																	 --LEFT JOIN HcFundraisingCategoryAssociation FC ON FC.HcFundraisingID =F.HcFundraisingID 						
																	 --WHERE ((E.RetailID =@RetailID AND @FundRaisingID IS NULL)
																	 --OR (@FundRaisingID IS NOT NULL AND F.HcFundRaisingID=@FundRaisingID AND F.HcEventID=E.HcEventID)
																	 --)						
																	 WHERE E.RetailID =@RetailID AND RE.RetailLocationID=@RetailLocationID
																	 AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
																	 AND (( ISNULL(@CategoryID, '0') <> '0' AND EC.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
																	 AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
																	 AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
																	 GROUP BY E.HcEventID 
																			  , HcEventName
																			  , ShortDescription
																			  , LongDescription                                                     
																			  , E.HcHubCitiID 
																			  , BussinessEvent
																			  , PackageEvent    
																			  , ImagePath
																			  , StartDate
																			  , EndDate
																			  , EC.HcEventCategoryID
																			  , EC.HcEventCategoryName
																			  , OnGoingEvent
																			  , E.RetailID
																			  , RE.HcEventID
																			  , RL1.RetailLocationID
																			  , HC.CityName	) E


															END					
															ELSE IF(@FundRaisingID IS NOT NULL)
															BEGIN
					
															INSERT INTO #Temp1111(HcEventID 
																				,HcEventName
																				,ShortDescription 
																				,LongDescription                                                    
																				,HcHubCitiID 
																				,ImagePath 
																				,BussinessEvent
																				,PackageEvent   									                                              
																				,StartDate
																				,EndDate 
																				,StartTime 
																				,EndTime                                
																				,MenuItemExist
																				,HcEventCategoryID
																				,HcEventCategoryName 
																				,Distance                                                                        
																				,DistanceActual									  
																				,OnGoingEvent 
																				,RetailLocationID
																				,City
																				,SignatureEvent
																				)
																SELECT   HcEventID 
																		,HcEventName
																		,ShortDescription 
																		,LongDescription                                                    
																		,HcHubCitiID 
																		,ImagePath 
																		,BussinessEvent
																		,PackageEvent 							                                           
																		,StartDate
																		,EndDate 
																		,StartTime 
																		,EndTime                                
																		,MenuItemExist
																		,HcEventCategoryID
																		,HcEventCategoryName 
																		,Distance                                                                        
																		,DistanceActual									  
																		,OnGoingEvent 
																		,RetailLocationID
																		,CityName
																		,SignatureEvent
															FROM(
															SELECT DISTINCT  E.HcEventID 
																			,HcEventName  
																			,E.ShortDescription
																			,E.LongDescription                                                     
																			,E.HcHubCitiID 	
																			,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																			,BussinessEvent 
																			,PackageEvent                                                     
																			,StartDate =CAST(E.StartDate AS DATE)
																			,EndDate =CAST(E.EndDate AS DATE)
																			,StartTime =CAST(E.StartDate AS Time)
																			,EndTime =CAST(E.EndDate AS Time)                                     
																			,MenuItemExist = 0   
																			,EA.HcEventCategoryID 
																			,EC.HcEventCategoryName 
																			,Distance=0                                           				
																			,DistanceActual =0				
																			,E.OnGoingEvent 
																			,RL1.RetailLocationID 
																			,HC.CityName
																			,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END	
															FROM HcFundraising HF
															INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
															INNER JOIN  HcEvents E ON HF.HcHubCitiID = E.HcHubCitiID AND F.HcEventID = E.HcEventID
															INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
															INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
															INNER JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
															LEFT JOIN HcEventAppsite EAP ON EA.HcEventID = EAP.HcEventID
															LEFT JOIN HcAppSite HA ON EAP.HcAppSiteID = HA.HcAppsiteID
															LEFT JOIN HcEventPackage EP ON EA.HcEventID =EP.HcEventID
															LEFT JOIN HcRegionAppHubcitiAssociation RHA ON RHA.HcRegionAppID = HF.HcHubCitiID	
															LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = @HubCitiID OR RHA.HcHubCitiID = RA.HcHubCitiID AND Associated =1
															LEFT JOIN RetailLocation RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1) OR (EP.RetailLocationID =RL1.RetailLocationID)							                                    
															INNER JOIN HcCity Hc ON RL1.HcCityID = Hc.HcCityID OR HL.City = Hc.CityName
															WHERE F.HcFundRaisingID = @FundRaisingID
															AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1															 					   							
															AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
															AND (( ISNULL(@CategoryID, '0') <> '0' AND EA.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
															AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					
															UNION ALL
						
															SELECT DISTINCT  E.HcEventID 
																		,HcEventName  
																		,E.ShortDescription
																		,E.LongDescription                                                     
																		,E.HcHubCitiID 	
																		,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
																		,BussinessEvent 
																		,PackageEvent                                                     
																		,StartDate =CAST(E.StartDate AS DATE)
																		,EndDate =CAST(E.EndDate AS DATE)
																		,StartTime =CAST(E.StartDate AS Time)
																		,EndTime =CAST(E.EndDate AS Time)                                     
																		,MenuItemExist =0  
																		,EA.HcEventCategoryID 
																		,EC.HcEventCategoryName 
																		,Distance=0                                           				
																		,DistanceActual =0				
																		,E.OnGoingEvent 
																		,RL1.RetailLocationID 
																		,HC.CityName
																		,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END				
															FROM HcFundraising HF
															INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
															INNER JOIN  HcEvents E ON HF.RetailID = E.RetailID AND F.HcEventID = E.HcEventID
															INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
															INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
															INNER JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
															LEFT JOIN HcRegionAppHubcitiAssociation RHA ON RHA.HcRegionAppID = @HubCitiID	
															INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = @HubCitiID OR RA.HcHubCitiID = RHA.HcHubCitiID AND Associated =1							 
															LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID
															LEFT JOIN RetailLocation RL1 ON RE.RetailLocationID =RL1.RetailLocationID AND RL1.Active = 1	
															INNER JOIN HcCity Hc ON RL1.HcCityID = Hc.HcCityID OR HL.City = Hc.CityName
															WHERE F.HcFundRaisingID = @FundRaisingID 
															AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
															AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
															AND (( ISNULL(@CategoryID, '0') <> '0' AND EA.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
															AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
															) E
						
															END
					
															SELECT DISTINCT HcEventID 
																			,HcEventName  
																			,ShortDescription
																			,LongDescription                                                     
																			,T.HcHubCitiID 
																			,ImagePath
																			,BussinessEvent 
																			,PackageEvent 						                                                   
																			,StartDate
																			,EndDate 
																			,StartTime 
																			,EndTime                             
																			,MenuItemExist
																			,HcEventCategoryID 
																			,HcEventCategoryName 										                                                                    
																			,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(Long) IS NULL THEN MIN(G.Longitude) ELSE MIN(Long) END/ 57.2958))))*6371) * 0.6214 END, 0))
																			,DistanceActual =(ISNULL(CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(Long) IS NULL THEN MIN(G.Longitude) ELSE MIN(Long) END/ 57.2958))))*6371) * 0.6214, 1, 1) END ,0))                           
							 											    ,OnGoingEvent 	
																			--,T.RetailLocationID
																			,T.City
																			,SignatureEvent									  
																INTO #Temppp
																FROM #Temp1111 T
																--LEFT JOIN RetailLocation RL1 ON RL1.RetailLocationID =T.RetailLocationID 
																--LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = T.HcHubCitiID AND Associated =1 AND RA.RetailLocationID = RL1.RetailLocationID                  
																LEFT JOIN Geoposition G ON T.City =G.City                              
																GROUP BY HcEventID 
																		,HcEventName  
																		,ShortDescription
																		,LongDescription                                                     
																		,T.HcHubCitiID 
																		,ImagePath
																		,BussinessEvent 
																		,PackageEvent   				                                            
																		,StartDate
																		,EndDate 
																		,StartTime 
																		,EndTime                             
																		,MenuItemExist
																		,HcEventCategoryID 
																		,HcEventCategoryName 
																		,Distance
																		,DistanceActual
																		,OnGoingEvent
																		--,T.RetailLocationID
																		,T.City
																		,SignatureEvent



										Select DISTINCT StartDate AS eventDate
								        from #Temppp
										WHERE StartDate IS NOT NULL


            END


			ELSE


			BEGIN
			 SELECT HcEventID
							,HcEventName
							,ShortDescription
							,LongDescription
							,ImagePath
							,BussinessEvent
							,PackageEvent
							,HcHubCitiID
							,StartDate
							,EndDate
							,DateCreated
							,DateModified
							,CreatedUserID
							,ModifiedUserID
							,PackageDescription
							,PackageTicketURL
							,PackagePrice
							,HotelEvent
							,MoreInformationURL
							,HcEventRecurrencePatternID
							,RecurrenceInterval
							,EventFrequency
							,OnGoingEvent
							,RetailID
							,Active
					INTO #Eventsss
					FROM HcEvents 
					WHERE (HcHubCitiID =@HubCitiID OR HcHubCitiID IS NULL)					
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))

					SELECT DISTINCT RetailLocationID
					INTO #RLListss 
					FROM (
					
					SELECT DISTINCT RL.RetailLocationID 					
					FROM #Eventsss E
					INNER JOIN HcEventLocation EL ON EL.HcEventID =E.HcEventID 
					INNER JOIN RetailLocation RL ON RL.Address1 =EL.Address AND RL.State =EL.State AND RL.City =EL.City AND RL.Active = 1 

					UNION ALL

					SELECT RL.RetailLocationID 					
					FROM #Eventsss E
					INNER JOIN HcRetailerAssociation RA ON RA.RetailID =E.RetailID AND Associated =1					
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =RA.RetailLocationID AND RL.Active = 1 )A 

     
					CREATE TABLE #Temp111(HcEventID Int
                                            ,HcEventName Varchar(2000) 
											,ShortDescription Varchar(2000) 
											,LongDescription Varchar(2000)                                                     
                                            ,HcHubCitiID Int
                                            ,ImagePath Varchar(2000) 
                                            ,BussinessEvent Int
                                            ,PackageEvent Int                                                     
                                            ,StartDate Date
                                            ,EndDate Date
                                            ,StartTime Time
                                            ,EndTime Time                                   
                                            ,MenuItemExist Int
                                            ,HcEventCategoryID Int 
                                            ,HcEventCategoryName Varchar(500)
										    ,Distance Int                                                                            
										    ,DistanceActual	Int									  
                                            ,OnGoingEvent Int
										    ,RetailLocationID Int
											,Latitude Float
											,Longitude Float
											,SignatureEvent bit)
                     
                     --To display List of Events
					 IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
					 BEGIN
					 print 'begin of mm'
							 INSERT INTO #Temp111(HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,Latitude 
												,Longitude
												,SignatureEvent)
							 SELECT HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,Latitude 
												,Longitude
												,SignatureEvent
							FROM(
							SELECT DISTINCT E.HcEventID 
													,HcEventName  
													,ShortDescription
													,LongDescription                                                     
													,E.HcHubCitiID 
													,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 
													,BussinessEvent 
													,PackageEvent                                                     
													,StartDate =CAST(StartDate AS DATE)
													,EndDate =CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Events' 
																					 AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
												   ,EC.HcEventCategoryID 
												   ,EC.HcEventCategoryName 
												   ,Distance=0                                           
										   --,Distance = IIF(RE.HcEventID IS NULL,(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)),(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)))
											   --,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
												   ,DistanceActual =0
												   --= CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
												   ,E.OnGoingEvent 
												   ,RL1.RetailLocationID 
												   ,Latitude=ISNULL(HL.Latitude,RL1.RetailLocationLatitude)
												   ,Longitude=ISNULL(HL.Longuitude,RL1.RetailLocationLongitude)
												   ,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END
							 --INTO #temp111					   							
							 FROM HcEvents  E 
							 LEFT JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID AND (EA.HcHubCitiID =@HubCitiID OR E.HcHubCitiID IS NULL)
							 INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID  
							 INNER JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
							 INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID AND MIC.HcHubCitiID =@HubCitiID AND MIC.HcMenuItemID = @HcMenuItemID 
							 --LEFT JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID AND BBC.HcHubCitiID =@HubCitiID AND BBC.HcBottomButtonID = @HcBottomButtonID 
							 LEFT JOIN HcEventAppsite A ON A.HcEventID = E.HcEventID AND A.HcHubCitiID =@HubCitiID AND A.HcEventID IS NULL
							 LEFT JOIN HcAppSite HA ON HA.HcAppSiteID = A.HcAppsiteID AND HA.HcHubCitiID =@HubCitiID                     
							 LEFT JOIN HcEventPackage EP ON EP.HcEventID =E.HcEventID AND EP.HcHubCitiID =@HubCitiID                                      
							 LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated = 1 AND ((E.HcHubCitiID IS NULL AND E.RetailID =RA.RetailID) OR 1=1)
							 LEFT JOIN #RLListss RRR ON RRR.RetailLocationID =RA.RetailLocationID 
							 LEFT JOIN RetailLocation RL1 ON (RL1.RetailLocationID =EP.RetailLocationID AND RL1.Active = 1) --OR (RL1.RetailLocationID = HA.RetailLocationID) OR (RRR.RetailLocationID =RL1.RetailLocationID)                
							 LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID AND  RE.HcEventID IS NULL							 
							                  
							 WHERE ((EA.HcHubCitiID =@HubCitiID)) --OR(  (  E.HcHubCitiID IS NULL AND E.RetailID =RA.RetailID )   ) 										          
							AND (( ISNULL(@CategoryID, '0') <> '0' AND EC.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
							AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							GROUP BY E.HcEventID 
									  , HcEventName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  , BussinessEvent
									  , PackageEvent    
									  , ImagePath
									  , StartDate
									  , EndDate
									  , EC.HcEventCategoryID
									  , EC.HcEventCategoryName
									  , OnGoingEvent
									  , E.RetailID
									  , RE.HcEventID
									  , RL1.RetailLocationID
									  ,HL.Latitude,RL1.RetailLocationLatitude
									  ,HL.Longuitude,RL1.RetailLocationLongitude
									  ) E

						print 'end of mm' 
					END
					ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
					BEGIN
					print 'inside bb'
							 INSERT INTO #Temp111(HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,SignatureEvent)
							 SELECT HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,SignatureEvent
							FROM(
							SELECT DISTINCT E.HcEventID 
													,HcEventName  
													,ShortDescription
													,LongDescription                                                     
													,E.HcHubCitiID 
													,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 
													,BussinessEvent 
													,PackageEvent                                                     
													,StartDate =CAST(StartDate AS DATE)
													,EndDate =CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Events' 
																					 AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
												   ,EC.HcEventCategoryID 
												   ,EC.HcEventCategoryName 
												   ,Distance=0                                           
										   --,Distance = IIF(RE.HcEventID IS NULL,(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)),(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)))
											   --,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
												   ,DistanceActual =0
												   --= CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
												   ,E.OnGoingEvent 
												   ,RL1.RetailLocationID
												   ,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 
							 --INTO #temp111					   							
							 FROM #Eventsss E 
							 INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
							 INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
							 INNER JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
							 --INNER JOIN HcMenuItemEventCategoryAssociation MIC ON EC.HcEventCategoryID = MIC.HcEventCategoryID AND MIC.HcHubCitiID =@HubCitiID AND MIC.HcMenuItemID = @HcMenuItemID --AND @HcMenuItemID IS NOT NULL
							 INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID AND BBC.HcHubCitiID =@HubCitiID AND BBC.HcBottomButtonID = @HcBottomButtonID 
							 LEFT JOIN HcEventAppsite A ON A.HcEventID = E.HcEventID AND A.HcHubCitiID =@HubCitiID 
							 LEFT JOIN HcAppSite HA ON HA.HcAppSiteID = A.HcAppsiteID AND HA.HcHubCitiID =@HubCitiID                     
							 LEFT JOIN HcEventPackage EP ON EP.HcEventID =E.HcEventID AND EP.HcHubCitiID =@HubCitiID                                      
							 LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated =1
							 LEFT JOIN #RLListss RR ON RR.RetailLocationID =RA.RetailLocationID 
							 LEFT JOIN RetailLocation RL1 ON (RL1.RetailLocationID =EP.RetailLocationID AND RL1.Active = 1) OR (RL1.RetailLocationID = HA.RetailLocationID) OR (RR.RetailLocationID =RL1.RetailLocationID)                
							 LEFT JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID
							 LEFT JOIN Geoposition G ON (G.Postalcode=RL1.Postalcode) OR (HL.PostalCode =G.PostalCode)                    
							 --WHERE ((MIC.HcMenuItemID = @HcMenuItemID AND @HcMenuItemID IS NOT NULL AND EA.HcHubCitiID =@HubCitiID)                   
							 WHERE((BBC.HcBottomButtonID = @HcBottomButtonID AND @HcBottomButtonID IS NOT NULL AND EA.HcHubCitiID =@HubCitiID)                             
							 OR (@RetailLocationID IS NULL AND RE.HcEventID =E.HcEventID And RE.HcRetailerEventsAssociationID  IS NOT NULL)                       
							 OR (RE.RetailLocationID = @RetailLocationID AND @RetailLocationID IS NOT NULL AND RE.RetailID =@RetailID)
							 OR (E.HcHubCitiID IS NULL AND E.RetailID IS NOT NULL AND A.HcEventID IS NULL AND RE.HcEventID IS NULL AND @RetailLocationID IS NULL AND E.RetailID =RA.RetailID )
							 )             
                     
                     
							AND (( ISNULL(@CategoryID, '0') <> '0' AND EC.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
							AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							GROUP BY E.HcEventID 
									  , HcEventName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  , BussinessEvent
									  , PackageEvent    
									  , ImagePath
									  , StartDate
									  , EndDate
									  , EC.HcEventCategoryID
									  , EC.HcEventCategoryName
									  , OnGoingEvent
									  , E.RetailID
									  , RE.HcEventID
									  , RL1.RetailLocationID
									  ) E
					END
					ELSE
					BEGIN
							 print 'inside else'
							 INSERT INTO #Temp111(HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,SignatureEvent)
							 SELECT HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,SignatureEvent
							FROM
							(SELECT DISTINCT E.HcEventID 
													,HcEventName  
													,ShortDescription
													,LongDescription                                                     
													,E.HcHubCitiID 
													,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 
													,BussinessEvent 
													,PackageEvent                                                     
													,StartDate =CAST(StartDate AS DATE)
													,EndDate =CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																					 FROM HcMenuItem MI 
																					 INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																					 WHERE LinkTypeName = 'Events' 
																					 AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
												   ,EC.HcEventCategoryID 
												   ,EC.HcEventCategoryName 
												   ,Distance=0                                           
										   --,Distance = IIF(RE.HcEventID IS NULL,(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)),(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)))
											   --,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
												   ,DistanceActual =0
												   --= CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
												   ,E.OnGoingEvent 
												   ,RL1.RetailLocationID
												   ,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 							 
							 FROM HcEvents E 
							 INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
							 INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
							 INNER JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 							                                     
							 INNER JOIN HcRetailerAssociation RA ON RA.HcHubCitiID =@HubCitiID AND Associated =1							 
							 INNER JOIN HcRetailerEventsAssociation RE ON RE.RetailLocationID = RA.RetailLocationID AND E.HcEventID = RE.HcEventID
							 INNER JOIN RetailLocation RL1 ON RL1.RetailLocationID =RE.RetailLocationID	AND RL1.Active = 1					
							 LEFT JOIN HcFundraisingEventsAssociation F ON F.HcEventID=E.HcEventID	
							 LEFT JOIN HcFundraisingCategoryAssociation FC ON FC.HcFundraisingID =F.HcFundraisingID 						
							 WHERE ((E.RetailID =@RetailID AND @FundRaisingID IS NULL)
							 OR (@FundRaisingID IS NOT NULL AND F.HcFundRaisingID=@FundRaisingID AND F.HcEventID=E.HcEventID)
							 )

							-- OR (E.HcHubCitiID IS NULL AND E.RetailID IS NOT NULL AND RE.HcEventID IS NULL AND @RetailLocationID IS NULL AND E.RetailID =RA.RetailID )
							-- )             
                     
                     
							AND (( ISNULL(@CategoryID, '0') <> '0' AND EC.HcEventCategoryID=@CategoryID) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  					                         
							AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
							AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
							GROUP BY E.HcEventID 
									  , HcEventName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  , BussinessEvent
									  , PackageEvent    
									  , ImagePath
									  , StartDate
									  , EndDate
									  , EC.HcEventCategoryID
									  , EC.HcEventCategoryName
									  , OnGoingEvent
									  , E.RetailID
									  , RE.HcEventID
									  , RL1.RetailLocationID) E

							
					END

				

				print 'b4 #temp'
					SELECT DISTINCT HcEventID 
                                   ,HcEventName  
                                   ,ShortDescription
								   ,LongDescription                                                     
                                   ,HcHubCitiID 
                                   ,ImagePath
                                   ,BussinessEvent 
                                   ,PackageEvent                                                     
                                   ,StartDate
                                   ,EndDate 
                                   ,StartTime 
                                   ,EndTime                             
                                   ,MenuItemExist
                                   ,HcEventCategoryID 
                                   ,HcEventCategoryName 
										                                            
                                --,Distance = IIF(RE.HcEventID IS NULL,(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)),(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0)))
           --                        ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
								   --,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(RL1.RetailLocationLatitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(RL1.RetailLocationLatitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(RL1.RetailLocationLongitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(RL1.RetailLocationLongitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
                                  
								  
								   ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(T.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(T.Longitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
								   ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(T.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(T.Latitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(T.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(T.Longitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END                            
                                  
								  
								   ,OnGoingEvent
								   ,SignatureEvent 
										  
                     into #Temp
					 FROM #Temp111 T                    
                     LEFT JOIN RetailLocation RL1 ON RL1.RetailLocationID =T.RetailLocationID AND RL1.Active = 1                     
                     LEFT JOIN Geoposition G ON (G.Postalcode=RL1.Postalcode)                  
                     GROUP BY HcEventID 
					 ,HcEventName  
                     ,ShortDescription
                     ,LongDescription                                                     
                     ,HcHubCitiID 
                     ,ImagePath
                     ,BussinessEvent 
                     ,PackageEvent                                                     
                     ,StartDate
                     ,EndDate 
                     ,StartTime 
                     ,EndTime                             
                     ,MenuItemExist
                     ,HcEventCategoryID 
                     ,HcEventCategoryName 
					 ,Distance
					 ,DistanceActual
					 ,OnGoingEvent
					 ,SignatureEvent



					 select DISTINCT StartDate AS eventDate 
					 from #Temp
					 WHERE StartDate IS NOT NULL

					 END


					--Confirmation of Success
					SELECT @Status = 0   
													
			
					 					  
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure [HubCitiApp2_3_3].[usp_HcEventsOptionEventDateDisplay]'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;

























GO
