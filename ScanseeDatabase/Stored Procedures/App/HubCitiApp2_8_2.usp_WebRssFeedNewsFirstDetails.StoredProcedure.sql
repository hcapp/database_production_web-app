USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_WebRssFeedNewsFirstDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssFeedNewsDetails
Purpose					: 
Example					: usp_WebRssFeedNewsDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_WebRssFeedNewsFirstDetails]
(	
	  @RssFeedNewsID int
	  		
	--Output Variable 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			   

			SELECT RssFeedNewsID Id
				,NewsType
				,Title
				,ImagePath
				,ShortDescription shortDesc
				,LongDescription description
				,Link
				,PublishedDate
				,Classification
				,Section
				,AdCopy
			FROM RssFeedNews
			WHERE RssFeedNewsID = @RssFeedNewsID			 
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;












GO
