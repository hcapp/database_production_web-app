USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[1usp_HcSingleFindRetailerListPagination]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  usp_HcFindRetailerListPagination
Purpose                 :  To get the list of near by retailer for Find Module.


History
Version     Date         Author          Change Description
------------------------------------------------------------------------------- 
1.0      5/17/2012      SPAN            Initial Version
2.0		03/03/2016     Sagar Byali	   Revised Version - Find Performance Changes
3.0		08/23/2016     Prakash C 	   Rewrite full code for Optimization
4.0     12/1/2016      Shilpashree     Optimized and handled RetailLocation SubCat filtering.
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_7].[1usp_HcSingleFindRetailerListPagination]
(
	--Input Parameters 
	  @Postalcode varchar(100)
	, @UserConfiguredPostalCode Varchar(10)
	, @CategoryName varchar(2000)
	, @Latitude Decimal(18,6)
	, @Longitude  Decimal(18,6)
	, @Radius int
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @HCHubCitiID Int
	, @HcMenuItemId Int
	, @HcBottomButtonId Int
	, @SearchKey Varchar(100)
	, @BusinessSubCategoryID Varchar(2000) 
	, @SortColumn varchar(50)
	, @SortOrder varchar(10)   
	, @HcCityID Varchar(3000)
	, @FilterID Varchar(1000)
	, @FilterValuesID Varchar(1000) 
	, @LocalSpecials bit 
	, @Interests Varchar(1000) 
	, @requestedTime Time
	, @MainMenuID int
	
	--Output Parameters
	, @NoRecordsMsg nvarchar(max) output
	, @UserOutOfRange bit output  
	, @DefaultPostalCode varchar(50) output             
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
	, @Status bit output  
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
) --with recompile
AS
BEGIN
    BEGIN TRY      
       SET NOCOUNT ON
		
		DECLARE  @MaxCount INT 
			, @RetSearch VARCHAR(1000)
			, @Config VARCHAR(50)                     
			, @RetailConfig VARCHAR(50)
			, @UpperLimit INT   
			, @Tomorrow DATETIME = GETDATE() + 1
			, @Yesterday DATETIME = GETDATE() - 1
			, @RowsPerPage INT
			, @NearestPostalCode VARCHAR(50)
			, @DistanceFromUser FLOAT
			, @UserLatitude FLOAT
			, @UserLongitude FLOAT 
			, @SpecialChars VARCHAR(1000)
			, @ModuleName VARCHAR(100) = 'Find'
			, @RegionAppFlag Bit
			, @BusinessCategoryID INT
			, @Length INT
			, @Globalimage VARCHAR(50)
			, @CategoryName1 VARCHAR(100) = @CategoryName
			, @HcMenuItemId1 INT = @HcMenuItemId 
			, @HCHubCitiID1 INT =  @HCHubCitiID
			, @BusinessSubCategoryID1 VARCHAR(1000) = @BusinessSubCategoryID
			, @HcBottomButtonId1 INT = @HcBottomButtonId
			, @Latitude11 bigint = @Latitude

		DECLARE @Interestss TABLE (ID int identity(1,1), IntIDs int)
		DECLARE @Filter TABLE (ID int identity(1,1), FilterIDs int)
		DECLARE @FilterValues TABLE (ID int identity(1,1), FilterValuesIDs int)
		DECLARE @BusinessSubCategoryID1s Table (ID int identity(1,1), BusCatIDs int)

		CREATE TABLE #BusinessCategory(BusinessCategoryID int , HcBusinessSubCategoryID int)
		CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
		CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100),HcHubCitiID int)
		CREATE TABLE #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int,BusinessSubCategoryID int)
		CREATE TABLE #RetailItemsonSale1(Retailid bigint , RetailLocationid bigint)
        CREATE TABLE #Retail(--RowNum INT IDENTITY(1, 1)
                             RetailID INT 
                            , RetailName VARCHAR(1000)
                            , RetailLocationID INT
                            , Address1 VARCHAR(1000)
							, Address2 varchar(1000)
                            , City VARCHAR(1000)    
                            , State CHAR(2)
                            , PostalCode VARCHAR(20)
                            , retLatitude FLOAT
                            , retLongitude FLOAT
                            , RetailerImagePath VARCHAR(1000)      
                            , Distance FLOAT  
                            , DistanceActual FLOAT                                                   
                            , BusinessSubCategoryID int
							, SaleFlag BIT
							, locationOpen VARCHAR(10))
					 
		SET @UserLatitude = @Latitude
		SET @UserLongitude = @Longitude
		SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
								WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
								WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
								ELSE @SearchKey END)
			
		SET @Globalimage=[dbo].[fn_ConfigvalueforScreencontent]('Image Not Found')
		SET @Config=[dbo].[fn_ConfigvalueforScreencontent]('App Media Server Configuration')
		SET @RetailConfig=[dbo].[fn_ConfigvalueforScreencontent]('Web Retailer Media Server Configuration')
		--SET @RowsPerPage=[dbo].[fn_ConfigvalueforScreencontent]('Pagination')
		--SET @UpperLimit=isnull(@UpperLimit,0)+convert(int,[dbo].[fn_ConfigvalueforScreencontent]('Pagination'))

		SELECT @RowsPerPage = ScreenContent
			  ,@UpperLimit = @LowerLimit + ScreenContent 
		FROM AppConfiguration   
		WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination'AND Active = 1   

		SELECT @BusinessCategoryID=BusinessCategoryID 
		FROM BusinessCategory 
		WHERE BusinessCategoryName LIKE @CategoryName1

		INSERT INTO @BusinessSubCategoryID1s (BusCatIDs)
		SELECT Param
		FROM fn_SplitParam (@BusinessSubCategoryID1,',')

		INSERT INTO @Interestss (IntIDs)
		SELECT Param
		FROM fn_SplitParam (@Interests,',')

		SELECT DISTINCT * 
		INTO #RetailerBusinessCategory 
			FROM RetailerBusinessCategory 
				WHERE BusinessCategoryID=@BusinessCategoryID

		--Filter Implementation 
		IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
		BEGIN
				SELECT Param FilterIDs
				INTO #Filters
				FROM fn_SplitParam(@FilterID,',') P
				LEFT JOIN AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
				WHERE AF.AdminFilterID IS NULL

				SELECT Param FilterValues
				INTO #FilterValues
				FROM fn_SplitParam(@FilterValuesID,',')

				SELECT DISTINCT RetailLocationID 
				INTO #FilterRetaillocations
				FROM(
				SELECT RF.RetailLocationID                                    
				FROM RetailerFilterAssociation RF
				INNER JOIN #FilterValues F ON F.FilterValues =RF.AdminFilterValueID AND BusinessCategoryID =@BusinessCategoryID
				UNION
				SELECT RF.RetailLocationID                                    
				FROM RetailerFilterAssociation RF
				INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BusinessCategoryID =@BusinessCategoryID
				)A

		END

		--To collect Cities passed as input for RegionApp
		SELECT Param CityID
		INTO #CityIDs
		FROM fn_SplitParam(@HcCityID,',')

		IF(SELECT 1 FROM HcHubCiti H
                    INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                    AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
        BEGIN
				SET @RegionAppFlag = 1
									
				INSERT INTO #RHubcitiList(HcHubCitiID)
				(SELECT DISTINCT HcHubCitiID 
				FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
				UNION ALL
				SELECT  @HCHubCitiID1 AS HcHubCitiID
				)

				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)		
				SELECT DISTINCT LA.HcCityID 
								,LA.City	
								,LA.PostalCode
								,H.HcHubCitiID				
				FROM #RHubcitiList H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID AND LA.HcHubCitiID = @HCHubCitiID1   						
				INNER JOIN #CityIDs C ON LA.HcCityID=C.CityID 
        END
		ELSE
        BEGIN
				SET @RegionAppFlag =0

				INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID1 
				 
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)
				SELECT DISTINCT HcCityID 
								,City
								,PostalCode	
								,HcHubCitiID				
				FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID1                                   
		END
					
		SELECT @UserLatitude = @Latitude
			 , @UserLongitude = @Longitude

		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
		END
         
		--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID1
		END

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
		EXEC [HubCitiapp2_8_7].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFROMUser OUTPUT
		--select @DefaultPostalCode = defaultpostalcode from hchubciti where HcHubCitiid = @HcHubCitiID  and @PostalCode is null
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Set the Next page flag to 0 initially.
		SET @NxtPageFlag = 0

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
		BEGIN
			SELECT @Latitude = Latitude
					, @Longitude = Longitude
			FROM GeoPosition 
			WHERE PostalCode = @PostalCode                                                                             
		END  
					
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL

		--#SubCategorytype will have BusCategoryID which has SubCategories. eg:dining-11
		SELECT DISTINCT ST.BusinessCategoryID  
		INTO #SubCategorytype  
			FROM HcBusinessSubCategorytype ST
			INNER JOIN HcBusinessSubCategory SC ON ST. HcBusinessSubCategorytypeID = SC.HcBusinessSubCategorytypeID
				WHERE ST.BusinessCategoryID = @BusinessCategoryID
	
		--#NONSUB will have BusCategoryID which does not have SubCategories. eg:Bars-8
		SELECT DISTINCT BusinessCategoryID INTO #NONSUB FROM BusinessCategory WHERE BusinessCategoryID=@BusinessCategoryID  
		EXCEPT
		SELECT DISTINCT BusinessCategoryID FROM #SubCategorytype
					        
        SELECT DISTINCT * 
		INTO #SUBHcMenuFindRetailerBusinessCategories 
			FROM HcMenuFindRetailerBusinessCategories 
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcMenuItemID=@HcMenuItemID
				AND HcBusinessSubCategoryID IS NOT NULL	

		SELECT DISTINCT * 
		INTO #NONSUBHcMenuFindRetailerBusinessCategories 
			FROM HcMenuFindRetailerBusinessCategories  
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcMenuItemID=@HcMenuItemID
				AND HcBusinessSubCategoryID IS NULL 

		SELECT DISTINCT * 
		INTO #SUBHcBottomButtonFindRetailerBusinessCategories 
			FROM HcBottomButtonFindRetailerBusinessCategories  
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcBottomButonID=@HcBottomButtonID
				AND HcBusinessSubCategoryID IS NOT NULL

		SELECT DISTINCT * 
		INTO #NONSUBHcBottomButtonFindRetailerBusinessCategories 
			FROM HcBottomButtonFindRetailerBusinessCategories  
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcBottomButonID=@HcBottomButtonID
				AND HcBusinessSubCategoryID IS NULL
		

		CREATE INDEX ix_nn ON #SUBHcMenuFindRetailerBusinessCategories(BusinessCategoryID,HcBusinessSubCategoryID)

		CREATE CLUSTERED INDEX ix_nn1 ON #SUBHcMenuFindRetailerBusinessCategories(HcMenuItemID)

		SELECT DISTINCT RS.*
		INTO #HcRetailerSubCategoryMI
		FROM #SUBHcMenuFindRetailerBusinessCategories HCB
		INNER JOIN HcRetailerSubCategory RS ON HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID

		SELECT DISTINCT RS.*
		INTO #HcRetailerSubCategoryBB
		FROM #SUBHcBottomButtonFindRetailerBusinessCategories HCB
		INNER JOIN HcRetailerSubCategory RS ON HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID

        IF @HcMenuItemID IS NOT NULL
		BEGIN

			INSERT INTO #RetailerBusinessCategory1
			SELECT DISTINCT  RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
				FROM #RetailerBusinessCategory RBC
				INNER JOIN #SUBHcMenuFindRetailerBusinessCategories HCB	ON HCB.BusinessCategoryID = RBC.BusinessCategoryID
															AND RBC.BusinessSubCategoryID = HCB.HcBusinessSubCategoryID
				INNER JOIN #HcRetailerSubCategoryMI RS ON HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID
			
			INSERT INTO #RetailerBusinessCategory1
			SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID , RBC.BusinessSubCategoryID 
				FROM #RetailerBusinessCategory RBC
				INNER JOIN #NONSUBHcMenuFindRetailerBusinessCategories HCB ON HCB.BusinessCategoryID = RBC.BusinessCategoryID

		END
		ELSE IF @HcBottomButtonID IS NOT NULL
		BEGIN
			
			INSERT INTO #RetailerBusinessCategory1
			SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID ,RBC.BusinessSubCategoryID 
				FROM #RetailerBusinessCategory RBC 
				INNER JOIN #SUBHcBottomButtonFindRetailerBusinessCategories HCB ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
													AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
				INNER JOIN #HcRetailerSubCategoryBB RS ON HCB.HcBusinessSubCategoryID = RS.HcBusinessSubCategoryID

			INSERT INTO #RetailerBusinessCategory1
			SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID, RBC.BusinessSubCategoryID 
				FROM #RetailerBusinessCategory RBC
				INNER JOIN #NONSUBHcBottomButtonFindRetailerBusinessCategories HCB ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
		END
		
		SELECT DISTINCT R.RetailID ,RetailName,WebsiteSourceFlag,RetailerImagePath,RBC.BusinessSubCategoryID
		INTO #RetailerInfo
			FROM Retailer R 
			INNER JOIN #RetailerBusinessCategory1 RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
						AND RBC.BusinessCategoryID = @BusinessCategoryID

		; WITH CTE AS(
					SELECT DISTINCT RL.RetailID   
								, R.RetailName
								, RL.RetailLocationID 
								, WebsiteSourceFlag
								, RetailerImagePath
								, saleflag= 0  
								, HL.HcHubCitiID 
								, BusinessSubCategoryID	
					FROM #RetailerInfo R
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID 
					INNER JOIN #CityList HL ON RL.PostalCode = HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID AND RL.HcCityID = HL.CityID
					INNER JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.Associated = 1 AND RA.HcHubCitiID = HL.HcHubCitiID
					)

		SELECT * INTO #CTETemp FROM CTE
		
		SELECT * INTO #CTEResults FROM #CTETemp  WHERE 1<>1

		IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
		BEGIN
			INSERT INTO #CTEResults
			SELECT T.*  
			FROM #CTETemp T  
			INNER JOIN #FilterRetaillocations F ON F.RetailLocationID = T.RetailLocationID
		END
		ELSE
		BEGIN
			INSERT INTO #CTEResults
			SELECT * FROM #CTETemp
		END

		SELECT DISTINCT  C.RetailID
						, RetailName
						, RL.RetailLocationID 
						, WebsiteSourceFlag
						, RetailerImagePath
						, saleflag= 0  
						, HchubcitiID 
						, RL.Address1    
						, RL.Address2     
						, RL.City
						, RL.State
						, RL.PostalCode
						, Headquarters
						, Active
						, RetailLocationLatitude
						, RetailLocationLongitude
						, RetailLocationImagePath
						, BusinessSubCategoryID
						, locationOpen  = 
							CASE WHEN RL.StartTimeUTC < RL.ENDTimeUTC THEN 
									(CASE WHEN CAST(@requestedTime as time) >= RL.StartTimeUTC AND CAST(@requestedTime as time) < RL.ENDTimeUTC THEN 'Open'
								WHEN RL.StartTimeUTC IS NULL OR RL.ENDTimeUTC IS NULL THEN 'N/A' ELSE 'Close' END)
								WHEN RL.StartTimeUTC >= RL.ENDTimeUTC THEN 
									(CASE WHEN CAST(@requestedTime AS TIME) >= RL.StartTimeUTC AND convert(int,substring(convert(varchar(20),@requestedTime),1,2))<convert(int,substring(convert(varchar(20),RL.ENDTimeUTC),1,2))+24 THEN 'Open'
								WHEN CAST(@requestedTime AS TIME) < RL.ENDTimeUTC THEN 'Open' ELSE 'Close' END)
								WHEN RL.StartTimeUTC IS NULL OR RL.ENDTimeUTC IS NULL THEN 'N/A'ELSE 'Close' END
		INTO #RetLocDetails
		FROM RetailLocation RL INNER JOIN #CTEResults C on C.RetailLocationID=RL.RetailLocationID
		WHERE Headquarters = 0  AND RL.Active = 1
		
		SELECT DISTINCT Rl.RetailID     
				, Rl.RetailName
				, RL.RetailLocationID 
				, RL.Address1   , RL.Address2     
				, RL.City
				, RL.State
				, RL.PostalCode
				, ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
				, ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
				, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
				, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
				, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
				--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available AND 1 = Sale available
				, RL.BusinessSubCategoryID
				, SaleFlag = 0  
				, locationOpen
		INTO #FilteredRetLocs
		FROM #RetLocDetails RL
		LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode AND G.City = RL.City                                   
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID
		LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID 
		LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
			WHERE D.DuplicateRetailerID IS NULL 
			AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
					OR (@SearchKey IS NULL))
			AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM @Interestss))

		IF EXISTS (SELECT 1 FROM #SubCategorytype) 					
		BEGIN
						
			INSERT INTO #Retail(RetailID
						, RetailName
						, RetailLocationID 
						, Address1   
						, Address2     
						, City
						, State
						, PostalCode
						, retLatitude
						, retLongitude
						, RetailerImagePath
						, Distance
						, DistanceActual
						, SaleFlag
						, locationOpen)
			SELECT DISTINCT R.RetailID
						, R.RetailName
						, R.RetailLocationID 
						, R.Address1   
						, R.Address2     
						, R.City
						, R.State
						, R.PostalCode
						, R.retLatitude
						, R.retLongitude
						, R.RetailerImagePath
						, R.Distance
						, R.DistanceActual
						, R.SaleFlag
						, R.locationOpen   
			FROM #FilteredRetLocs R
			INNER JOIN HcRetailerSubCategory S ON R.RetailLocationID = S.RetailLocationID AND R.BusinessSubCategoryID = S.HcBusinessSubCategoryID	
			WHERE Distance <= @Radius
			AND (@BusinessSubCategoryID1 IS NULL OR S.HcBusinessSubCategoryID IN (SELECT BusCatIDs FROM @BusinessSubCategoryID1s)) 
		END
		ELSE IF EXISTS (SELECT 1 FROM #NONSUB)
		BEGIN
			INSERT INTO #Retail(RetailID
							, RetailName
							, RetailLocationID 
							, Address1   
							, Address2     
							, City
							, State
							, PostalCode
							, retLatitude
							, retLongitude
							, RetailerImagePath
							, Distance
							, DistanceActual
							, SaleFlag
							, locationOpen)
				SELECT DISTINCT R.RetailID
							, R.RetailName
							, R.RetailLocationID 
							, R.Address1   
							, R.Address2     
							, R.City
							, R.State
							, R.PostalCode
							, R.retLatitude
							, R.retLongitude
							, R.RetailerImagePath
							, R.Distance
							, R.DistanceActual
							, R.SaleFlag
							, R.locationOpen  
				FROM #FilteredRetLocs R
				WHERE Distance <= @Radius
		END

		INSERT  INTO  #RetailItemsonSale1
		SELECT DISTINCT R.RetailID, A.RetailLocationID 
			FROM RetailLocationDeal A 
			INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
			INNER JOIN RetailLocationProduct C ON A.RetailLocationID = C.RetailLocationID AND A.ProductID = c.ProductID
			AND GETDATE() BETWEEN ISNULL(A.SaleStartDate, @Yesterday) AND ISNULL(A.SaleEndDate, @Tomorrow)
		
		INSERT  INTO  #RetailItemsonSale1
		SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  AS RetaillocationID 
			FROM Coupon C 
			INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
			INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
			LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
					GROUP BY C.CouponID
							,NoOfCouponsToIssue
							,CR.RetailID
							,CR.RetailLocationID
					HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
					ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   

		
		INSERT  INTO  #RetailItemsonSale1
		SELECT DISTINCT R.RetailID, R.RetailLocationID
			FROM ProductHotDeal p
			INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
			INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
			LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
			LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealENDDate, @Tomorrow)
					GROUP BY P.ProductHotDealID
							,NoOfHotDealsToIssue
							,R.RetailID
							,R.RetailLocationID
					HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
					ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

		INSERT  INTO  #RetailItemsonSale1
		SELECT q.RetailID, qa.RetailLocationID
			FROM QRRetailerCustomPage q
			INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
			INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
			INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID AND qt.QRTypeName = 'Special Offer Page'
				WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') AND isnull(q.ENDdate,@Tomorrow)
					

		SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
									ELSE SplashAdImagePath END
									, SplashAdID  = ASP.AdvertisementSplashID
									, R.RetailLocationID  
		INTO #BannerAd
		FROM #Retail R 
		INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID=RS.RetailLocationID
		INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.ENDDate, GETDATE() + 1)

		SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
									ELSE AB.BannerAdImagePath END 
									, RibbonAdURL = AB.BannerAdURL
									, RetailLocationAdvertisementID = AB.AdvertisementBannerID
									, R.RetailLocationID
		INTO #RibbonAd
		FROM #Retail R 
		INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID=RB.RetailLocationID
		INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
		WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.ENDDate, GETDATE() + 1)
					
		SELECT DISTINCT  R.RetailID retailerId    
						, RetailName retailerName
						, R.RetailLocationID retailLocationID
						, Address1 retaileraddress1
						, Address2 retaileraddress2
						, City City
						, State State
						, PostalCode PostalCode
						, retLatitude
						, retLongitude    
						, RetailerImagePath logoImagePath     
						, Distance
						, DistanceActual                         
						, S.BannerAdImagePath bannerAdImagePath    
						, B.RibbonAdImagePath ribbonAdImagePath    
						, B.RibbonAdURL ribbonAdURL    
						, B.RetailLocationAdvertisementID advertisementID  
						, S.SplashAdID splashAdID
						, SaleFlag = CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
						, locationOpen
		INTO #Retailer    
		FROM #Retail R
		LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
		LEFT JOIN #BannerAd S ON R.RetailLocationID = S.RetailLocationID
		LEFT JOIN #RibbonAd B ON R.RetailLocationID = B.RetailLocationID
	
		SELECT rowNumber =  ROW_NUMBER() OVER (ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance') THEN CAST(DistanceActual AS SQL_VARIANT)
				WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
				WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
				END ASC ,Distance)  
				, retailerId    
				, retailerName
				, retailLocationID
				, retaileraddress1
				, retaileraddress2
				, City
				, State
				, PostalCode
				, retLatitude
				, retLongitude    
				, logoImagePath     
				, Distance
				, DistanceActual                         
				, bannerAdImagePath    
				, ribbonAdImagePath    
				, ribbonAdURL    
				, advertisementID  
				, splashAdID
				, SaleFlag 
				, locationOpen
		INTO #Retailer1 
		FROM #Retailer
		WHERE (ISNULL(@LocalSpecials,0) = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))
		
		--To capture total noumber of rows.  
		SELECT @MaxCnt = count(1) FROM #Retailer1

		--this flag is a indicator to enable "More" button in the UI.   
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

		SELECT DISTINCT   rowNumber  
						, retailerId    
						, retailerName
						, retailLocationID
						, retaileraddress1
						, retaileraddress2
						, City
						, State
						, PostalCode
						, retLatitude
						, retLongitude    
						, logoImagePath     
						, Distance
						, DistanceActual                         
						, bannerAdImagePath    
						, ribbonAdImagePath    
						, ribbonAdURL    
						, advertisementID  
						, splashAdID
						, SaleFlag 
						, locationOpen  
		INTO #RetailerList 
		FROM #Retailer1
		WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
		ORDER BY rowNumber   
					              
		INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
														,CityID
														,DateCreated)
		SELECT DISTINCT @MainMenuID 
						,CityID 
						,GETDATE()
						FROM #CityList 

		--Table to track the Retailer List.
		CREATE TABLE #Temp(Rowno int IDENTITY(1,1)
							,RetailerListID int
							,LocationDetailID int
							,MainMenuID int
							,RetailID int
							,RetailLocationID int)  

		--Capture the impressions of the Retailer list.
		INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
														, RetailID
														, RetailLocationID
														, FindCategoryID
														, DateCreated)
        OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)                                             
		SELECT DISTINCT @MainMenuId
						, retailerId
						, RetailLocationID
						, @BusinessCategoryID
						, GETDATE()
		FROM #RetailerList  

		--Display the Retailer along with the keys generated in the Tracking table.
		SELECT  rowNumber
				, T.RetailerListID retListID
				, retailerId
				, retailerName
				, T.RetailLocationID
				, retaileraddress1
				, retaileraddress2
				, City
				, State
				, PostalCode                                   
				, retLatitude
				, retLongitude
				, Distance = CASE WHEN @Latitude11 is null THEN ISNULL(Distance,DistanceActual) ELSE ISNULL(DistanceActual,Distance)END
				, logoImagePath
				, bannerAdImagePath
				, ribbonAdImagePath
				, ribbonAdURL
				, advertisementID
				, splashAdID
				, SaleFlag   
				, locationOpen
		FROM #Temp T
		INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID 
		--ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(case when @Latitude11 is null then  ISNULL(Distance,DistanceActual ) else   ISNULL(DistanceActual,Distance) end AS SQL_VARIANT)
		--			WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
		--			WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
		--			END ASC    

		--To display Bottom Buttons                                                
		EXEC [HubCitiapp2_8_7].[Find_usp_HcFunctionalityBottomButtonDisplay] @HCHubCitiID1, @ModuleName, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT

		DECLARE @UserPrefCities NVarchar(MAX)

		IF (@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0) AND (@SearchKey IS NULL) AND @BusinessSubCategoryID IS NULL 
			AND @HcCityID IS NULL AND @FilterID IS NULL AND  @FilterValuesID IS NULL AND @LocalSpecials = 0 AND @Interests IS NULL
		BEGIN 
				SELECT DISTINCT cityname INTO #tempp FROM #CityList
				SELECT @UserPrefCities = COALESCE( @UserPrefCities+',' ,'') + UPPER(LEFT( CityName,1))+LOWER(SUBSTRING( CityName,2,LEN( CityName))) 
				FROM #tempp
						
				SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n' + @UserPrefCities +
				'.\n\nUpdate your city preferences in the settings menu.'

		END
		ELSE IF ((@RegionAppFlag =1) AND (ISNULL(@MaxCnt,0) = 0)) 
				AND (@SearchKey IS NOT NULL OR @BusinessSubCategoryID IS NOT NULL OR @HcCityID IS NOT NULL OR @FilterID IS NOT NULL
				OR  @FilterValuesID IS NOT NULL OR @LocalSpecials = 1 OR @Interests IS NOT NULL)
		BEGIN
				SELECT @NoRecordsMsg = 'No Records Found.'
		END
		ELSE IF (@RegionAppFlag = 0) AND (ISNULL(@MaxCnt,0) = 0) 
		BEGIN
				SELECT @NoRecordsMsg = 'No Records Found.'
		END
					
		--Confirmation of success
		SELECT @Status = 0 

	END TRY
            
	BEGIN CATCH
			INSERT  INTO SCANSEEVALIDATIONERRORS(ERRORCODE,ERRORLINE,ERRORDESCRIPTION,ERRORPROCEDURE,Inputon)
			VALUES(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE(),getdate())
			SET @ErrorMessage=ERROR_MESSAGE()  
			SET @ErrorNumber=ERROR_NUMBER()
			
			--Confirmation of Failure
			SELECT @Status = 1    
	END CATCH;
END;





GO
