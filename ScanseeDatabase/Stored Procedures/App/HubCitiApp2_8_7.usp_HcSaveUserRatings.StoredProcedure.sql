USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcSaveUserRatings]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_HcSaveUserRatings]  
Purpose     : Retrieve Average User Rating for product in context 
Example     : EXEC [HubCitiApp2_1].[usp_HcSaveUserRatings] ''  
  
History  
Version  Date           Author         Change Description  
---------------------------------------------------------------  
1.0      13thOct 2013	Dhananjaya TR  Initial Version  
---------------------------------------------------------------   
*/ 

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcSaveUserRatings]
(
	@ProductID int,
	@UserID	int,
	@Rating	Int,
		
	--Output Variable 
	@ErrorNumber	int output,
	@ErrorMessage	varchar(1000) output 	
)	
AS

BEGIN
	
	BEGIN TRY		
				
		-- Save User Rating
		If Exists (Select 1 from [UserRating] where UserID = @UserID and ProductID = @ProductID)
			Begin
				If @Rating != 0 -- To check if User Rating is > or = zero
					Begin
						-- For an existing record the rating is updated if User Rating > zero
						Update UserRating Set Rating = @Rating
						Where UserID = @UserID 
						and ProductID = @ProductID
					End
				Else
					Begin
						-- If User Rating is zero, then the rating is deleted
						Delete from UserRating 
						where UserID = @UserID 
						and ProductID = @ProductID 
						and @Rating = 0
					End
			End			
		Else		
			Begin
				If @Rating != 0 -- If User Rating is zero, then the rating is NOT recorded
					Begin
						Insert Into UserRating (ProductID
						                      , UserID
						                      , Rating
						                      , ReviewDate)
						Values (@ProductID
						      , @UserID
						      , @Rating
						      , GETDATE())
					End
			End	
		
	End Try

Begin Catch

	--Check whether the Transaction is uncommitable.
	If @@ERROR <> 0
	Begin
		Print 'Error occured in Stored Procedure usp_HcSaveUserRatings.'		
		--- Execute retrieval of Error info.
		EXEC [HubCitiapp2_8_7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
	End;
	 
End Catch;
	
End














































GO
