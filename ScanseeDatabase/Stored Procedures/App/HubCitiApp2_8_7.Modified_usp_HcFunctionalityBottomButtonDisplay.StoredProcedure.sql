USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[Modified_usp_HcFunctionalityBottomButtonDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: Modified_usp_HcFunctionalityBottomButtonDisplay
Purpose					: To display list of BottomButtons for given Module.
Example					: Modified_usp_HcFunctionalityBottomButtonDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22/4/2014	    SPAN     	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[Modified_usp_HcFunctionalityBottomButtonDisplay]
(   
    --Input variable.	  
	  @HubCitiID Int	
	, @ModuleName Varchar(50)
	--, @UserID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config VARCHAR(500)
			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			     , HM.HcHubCitiID 
				 , BB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
				 --, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1  
											  THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                       ELSE BottomButtonLinkTypeName END)
			--INTO #Temp1
			FROM HcMenu HM
			--INNER JOIN HcMenuItem MI ON MI.HCMenuID = HM.HCMenuID 
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID AND FBB.HcHubCitiID =@HubCitiID 
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = @ModuleName
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			--WHERE LT.LinkTypeDisplayName = @ModuleName --AND FBB.HcHubCitiID = @HubCitiID
			ORDER by HcFunctionalityBottomButtonID

			--SELECT HcHubCitiID 
			--	 , bottomBtnID
			--   , bottomBtnName
			--	 , bottomBtnImg
			--	 , bottomBtnImgOff
			--	 , btnLinkTypeID
			--	 , btnLinkID 
			--	 --, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
			--	 , btnLinkTypeName 				
			--FROM #Temp
			--ORDER by HcFunctionalityBottomButtonID

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
		   SELECT ERROR_LINE()
			PRINT 'Error occured in Stored Procedure [usp_HcFunctionalityBottomButtonDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
