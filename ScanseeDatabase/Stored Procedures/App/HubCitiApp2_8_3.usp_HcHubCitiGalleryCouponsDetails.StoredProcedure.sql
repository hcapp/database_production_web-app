USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiGalleryCouponsDetails]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcHubCitiGalleryCouponsDetails]
Purpose					: To display details of coupon related to the user.
Example					: [usp_HcHubCitiGalleryCouponsDetails]

History
Version		   Date			Author	  Change Description
-----------------------------------------------------------
1.0			6/11/2013	    SPAN	       1.0
-----------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiGalleryCouponsDetails]
(
	  @Userid int
	, @Couponid int
	, @HcHubCitiID Int  
	
	--Inputs for User Tracking. 
	, @CouponListID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY

		--To get Media Server Configuration.  
		DECLARE @Config varchar(50) 
			 , @RetailerConfig VARCHAR(500) 
			
		SELECT @Config=ScreenContent  
			FROM AppConfiguration   
				WHERE ConfigurationType='Hubciti Media Server Configuration' 

		SELECT @RetailerConfig= ScreenContent  
			FROM AppConfiguration   
				 WHERE ConfigurationType='Web Retailer Media Server Configuration'
	   
			SELECT DISTINCT	C.CouponID
					,CouponName
					,CouponShortDescription couponDesc
					,couponStartDate = CASE WHEN CouponExpireDate IS NULL THEN  FORMAT(CouponStartDate, 'MMM dd') +', '+LTRIM(RIGHT(CONVERT(VARCHAR(20), CouponStartDate, 100), 7))
					ELSE  FORMAT(CouponStartDate, 'MMM dd') + ' - ' + FORMAT(CouponExpireDate, 'MMM dd') +', '+
						 LTRIM(RIGHT(CONVERT(VARCHAR(20), CouponStartDate, 100), 7)) + ' - ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), CouponExpireDate, 100), 7))  -- FORMAT(CouponExpireDate, 'HH:mm tt')  
						END
					--,couponStartDate = FORMAT(CouponStartDate, 'MMMM dd') + 'th at ' + FORMAT(CouponStartDate, 'HH:mm tt') 
					,couponExpireDate = CASE WHEN claim = 0 THEN FORMAT(CouponExpireDate, 'MMM dd') + 'th at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), CouponExpireDate, 100), 7))
											 ELSE FORMAT(ActualCouponExpirationDate, 'MMM dd') + 'th at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), ActualCouponExpirationDate, 100), 7))end
					
					,CouponURL
					,case when CouponTermsAndConditions = ' ' then null else CouponTermsAndConditions end termAndConditions 
					,ViewableOnWeb viewOnWeb
					,UsedFlag=CASE WHEN C.CouponID=UC.CouponID AND UC.HcUserID = @Userid AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.HcUserID = @Userid AND UC.UsedFlag=0 THEN 1 ELSE 0 END
					,CASE WHEN C.CouponExpireDate<GETDATE() THEN 1 ELSE 0 END expireFlag
					,prodFlag = CASE WHEN CP.CouponProductId IS NOT NULL THEN 1 ELSE 0 END
					,locatnFlag = CASE WHEN CR.CouponRetailerID IS NOT NULL THEN 1 ELSE 0 END
					,C.CouponTermsAndConditions 
									, CouponImagePath = CASE WHEN C.HcHubCitiID IS NOT NULL THEN @Config + CAST(@HcHubCitiID as varchar(10))+'/' + CouponDetailImage 
											 WHEN C.RetailID IS NOT NULL AND C.HcHubCitiID IS NULL AND CouponDetailImage  = 'uploadIconSqr.png' THEN @RetailerConfig + CouponDetailImage ELSE @RetailerConfig + CAST(C.RetailID as varchar(10))+'/' + CouponDetailImage 
											 END
					--,CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp2_8_1.fn_CouponImage(C.CouponID)
					--								  ELSE CASE WHEN CouponImagePath IS NOT NULL
					--										THEN CASE WHEN C.WebsiteSourceFlag = 1 
					--												THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
					--											 ELSE CouponImagePath 
					--											 END
					--										END 
					--								  END
					,BannerTitle AS bannerName
			FROM Coupon C
		    LEFT JOIN CouponProduct CP ON C.CouponID=CP.CouponID
			LEFT JOIN Product P ON P.ProductID=CP.ProductID
			LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID	
			LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active=1  
			LEFT JOIN HcLocationAssociation HL ON HL.PostalCode = RL.PostalCode
			LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
			LEFT JOIN Retailer R ON R.RetailID = CR.RetailID AND R.RetailerActive=1
			LEFT JOIN HCUserCouponGallery UC ON UC.CouponID = C.CouponID 
			AND UC.HcUserID = @Userid
			WHERE  C.CouponID=@couponid --AND ISNULL(CouponDisplayExpirationDate, CouponExpireDate) >= GETDATE()
 
			
			--User Tracking section.
			
			--Capture the Click
			UPDATE HubCitiReportingDatabase..CouponsList SET CouponClick = 1
			WHERE CouponsListID = @CouponListID
			
		 --Confirmation of Success.
		 SET @Status = 0	
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [HubCitiApp2_8_3].[usp_HcHubCitiGalleryCouponsDetails].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			
			--Confirmation of failure.
			SET @Status = 1
					
		END;
		 
	END CATCH;
END;




GO
