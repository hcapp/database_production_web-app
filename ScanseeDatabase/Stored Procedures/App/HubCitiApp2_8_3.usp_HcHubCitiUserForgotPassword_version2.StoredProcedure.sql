USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcHubCitiUserForgotPassword_version2]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiUserForgotPassword
Purpose					: To Display Password For selected Username.
Example					: usp_HcHubCitiUserForgotPassword

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/10/2013	    Dhananjaya TR	1.0
2.0			04/07/2016		Bindu T A		Changes with respect to Forget Password Implimentation
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcHubCitiUserForgotPassword_version2]
(
    --Input variable.
	  @HubCitiID int
    , @HcUserName Varchar(1000)    
	, @TPassword Varchar(100)
		  
	--Output Variable 
	, @message Varchar(100) Output
	, @UserName Varchar(1000)  Output
	, @Email Varchar(1000) Output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		DECLARE @UserID INT
		SELECT @UserID= A.HcUserID FROM HcUser A INNER JOIN HcUserDeviceAppVersion B ON A.HcUserID = B.HcUserID AND B.HcHubCitiID = @HubCitiID 
		WHERE ( UserName = @HcUserName OR Email = @HcUserName )

		IF(@UserID IS NULL)
			SELECT @HcUserName = NULL
			
			--Select user details for selected username
		IF EXISTS (SELECT 1 FROM HcUser A 	WHERE (A.HcUserID= @UserID ) AND A.Email IS NOT NULL)
		BEGIN
			
			UPDATE A
			SET [Password] = @TPassword,
			TempPassword= 1,
			TempTime = GETDATE()
			FROM HcUser A
			WHERE HcUserID =@UserID
			
			SELECT @Email = A.Email , @UserName = UserName 
			FROM HcUser A
			WHERE HcUserID =@UserID

			SELECT @message = 'Temporary Password has been sent to the registered Email' 

		END
		ELSE IF EXISTS (SELECT 1 FROM HcUser A 	WHERE  A.HcUserID= ISNULL(@UserID,0) AND A.Email IS NULL)
		BEGIN 
			SELECT @message = 'EmailID is not present for the given UserName' 
		END
		ELSE
		BEGIN
			SELECT @message = 'UserName/ EmailID is Invalid' 
		END
		
		--Confirmation of	Success
			SELECT @Status = 0			
				
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiUserForgotPassword_version2.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
