USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserProductHit]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserProductHit
Purpose					: To capture User product Hit.
Example					: usp_HcUserProductHit

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30thOct2013     SPAN            Initial version 
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserProductHit]
(
	  @UserID int    
	, @ProductID int
	, @RetailLocationID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION  
		--To capture User Product Hit info.  
		INSERT INTO [HCProductUserHit]  
			([ProductID]  
			,[HCUserID]  
			,[RetailLocationID]  
			,[UserHitDate])  
		 VALUES  
			(@ProductID   
			,@UserID   
			,@RetailLocationID   
			,GETDATE())
			
		--Confirmation of Success.  
		 SELECT @Status = 0  
		COMMIT TRANSACTION   
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserProductHit.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;













































GO
