USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcGalleryUsedCouponsByLocation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GalleryUsedCouponsByLocation]
Purpose					: To display Used Coupons related to the user in user coupon gallery.
Example					: [usp_GalleryUsedCouponsByLocation]

History
Version		Date			 Author	 Change Description
--------------------------------------------------------------- 
1.0			26th JUN 2013	 SPAN	 Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcGalleryUsedCouponsByLocation]
(
	   @UserID int
	 , @SearchKey varchar(100)
	 , @LowerLimit int
     , @ScreenName varchar(100)
	 , @Latitude float
	 , @Longitude float
	 , @PostalCode varchar(500)	
	 , @HubCitiID int
	
	--UserTracking Input Variables
	 , @MainMenuID int
	 
	--Output Variables
	 , @Status bit output
	 , @UserOutOfRange bit output
	 , @DefaultPostalCode varchar(50) output
	 , @MaxCnt int output
	 , @NxtPageFlag bit output	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			--To get Media Server Configuration.  
			DECLARE @RetailConfig varchar(50)
			DECLARE @DistanceFromUser FLOAT
			
			    
			SELECT @RetailConfig=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration'  
			   
			--To get the row count for pagination.
			DECLARE @UpperLimit int   
			SELECT @UpperLimit = @LowerLimit + ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = @ScreenName   
			AND ConfigurationType = 'Pagination'  
			AND Active = 1 

			--To fetch all the duplicate retailers.
			SELECT DISTINCT DuplicateRetailerID 
			INTO #DuplicateRet
			FROM Retailer 
			WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive=1
			
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubcitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode) 
		
			CREATE TABLE #Gallery(Row_Num INT IDENTITY(1, 1)
									, UserCouponGalleryID  INT
									, BusinessCategoryID INT 
									, BusinessCategoryName VARCHAR(255)
									, RetailID INT
									, RetailName VARCHAR(200)
									, RetailLocationID INT
									, [Address] VARCHAR(2000)
									, CouponID INT
									, CouponName VARCHAR(500)
									, CouponDescription VARCHAR(1000)
									, CouponDiscountAmount MONEY
									, CouponDiscountPct FLOAT
									, CouponURL VARCHAR(500)
									, CouponImagePath VARCHAR(500)
									, CouponStartDate DATETIME
									, CouponExpireDate DATETIME
									, UsedFlag BIT 
									, ViewableOnWeb BIT
									)
				INSERT INTO #Gallery(UserCouponGalleryID 
									, BusinessCategoryID  
									, BusinessCategoryName
									, RetailID
									, RetailName
									, RetailLocationID
									, [Address]
									, CouponID 
									, CouponName 
									, CouponDescription
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
									)
							SELECT DISTINCT HcUserCouponGalleryID 
									, MIN(RBC.BusinessCategoryID)
									, MIN(BC.BusinessCategoryName)
									, CR.RetailID
									, R.RetailName
									, CR.RetailLocationID
									, [Address]= (RL.Address1+', '+RL.City+', '+RL.[State]+', '+RL.PostalCode)
									, C.CouponID 
									, C.CouponName 
									, CouponDescription = CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN HubCitiApp2_8_2.fn_CouponImage(C.CouponID)
												  ELSE CASE WHEN CouponImagePath IS NOT NULL
														THEN CASE WHEN C.WebsiteSourceFlag = 1 
																THEN @RetailConfig +CONVERT(VARCHAR(30),CR.RetailID)+'/'+CouponImagePath 
															 ELSE CouponImagePath 
															 END
														END 
												  END
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
							FROM Coupon C 
							INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
							INNER JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
							LEFT JOIN Retailer R ON C.RetailID = R.RetailID AND R.RetailerActive=1
							LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active=1
							LEFT JOIN HcRetailerAssociation HR ON HR. RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HubCitiID AND Associated =1 
							LEFT JOIN RetailerBusinessCategory RBC ON CR.RetailID = RBC.RetailerID
							LEFT JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID
							LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
							WHERE UsedFlag = 1 AND HcUserID = @UserID AND D.DuplicateRetailerID IS NULL
							AND (CASE WHEN CouponExpireDate < GETDATE() THEN DATEDIFF(DAY,GETDATE(),CouponExpireDate) 
									ELSE DATEPART(MONTH,GETDATE()) END <=90)
							AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
							GROUP BY HcUserCouponGalleryID 
									, CR.RetailID
									, RetailName
									, CR.RetailLocationID
									, RL.Address1
									, RL.City
									, RL.State
									, RL.PostalCode
									, C.CouponID 
									, CouponName 
									, C.CouponShortDescription
									, C.CouponLongDescription
									, CouponDiscountAmount
									, CouponDiscountPct
									, CouponURL
									, CouponImagePath
									, CouponStartDate
									, CouponExpireDate 
									, UsedFlag 
									, ViewableOnWeb
									, C.WebsiteSourceFlag
					  
			  --To capture max row number.  
			  SELECT @MaxCnt = MAX(Row_Num) FROM #Gallery  
			  --this flag is a indicator to enable "More" button in the UI.   
			  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
			  
			  SELECT Row_Num AS rowNumber
					, UserCouponGalleryID userCoupGallId
					, BusinessCategoryID busCatId 
					, BusinessCategoryName busCatName
					, RetailID retId
					, RetailName retName
					, RetailLocationID retLocId
					, [Address]
					, CouponID couponId
					, CouponName 
					, CouponDescription coupDesc
					, CouponDiscountAmount
					, CouponDiscountPct
					, CouponURL
					, CouponImagePath
					, CouponStartDate
					, CouponExpireDate 
					, UsedFlag 
					, ViewableOnWeb
			FROM #Gallery
			WHERE Row_Num BETWEEN (@LowerLimit + 1) AND  @UpperLimit
			ORDER BY RetailName,[Address],BusinessCategoryName,CouponName ASC
							
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GalleryUsedCouponsByLocation].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
				
		END;
		 
	END CATCH;
END;









































GO
