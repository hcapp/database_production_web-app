USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubRegionEventsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name					 : usp_HcHubRegionEventsDisplay
Purpose                                  : To display List fo Events wrt Region and associated HubCities.
Example                                  : usp_HcHubRegionEventsDisplay

History
Version          Date          Author		Change Description
--------------------------------------------------------------- 
1.0            5th Feb 2014		SPAN         1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubRegionEventsDisplay]
(   
    --Input variable.        
         @HubCitiID int
       , @UserID Int
       , @CategoryID Varchar(1000)
       , @Latitude Float
       , @Longitude Float
       , @Postalcode Varchar(200)
       , @SearchParameter Varchar(1000)
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @SortColumn Varchar(200)
       , @SortOrder Varchar(100)
       , @GroupColumn varchar(50)
       , @HcMenuItemID int
       , @HcBottomButtonID int
       , @RetailID int
       , @RetailLocationID int
	   , @CityIDs Varchar(2000)
	   , @FundRaisingID Int	
	   , @EventDate Date

       --User Tracking
       , @MainMenuID Int
  
       --Output Variable 
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
               
                     DECLARE @UpperLimit int
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     DECLARE @RetailConfig Varchar(1000)

                     SELECT @RetailConfig = ScreenContent
					 FROM AppConfiguration 
					 WHERE ConfigurationType = 'Web Retailer Media Server Configuration'   

					 
                    IF @Postalcode IS NOT NULL AND @Latitude IS NULL
                    BEGIN
                            SELECT @Latitude = G.Latitude
                                    , @Longitude = G.Longitude
                            FROM GeoPosition G
                        WHERE   G.PostalCode = @Postalcode 
                                 
                    END
                           

                     SELECT @UserLatitude = @Latitude
                           , @UserLongitude = @Longitude                  

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                  , @UserLongitude = Longitude
                           FROM HcUser A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE HcUserID = @UserID 
                     END
                     --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HubCitiID
                     END

                     print @UserLatitude
                     print @UserLongitude
                           
                     --To get the row count for pagination.   
                     SELECT @UpperLimit = @LowerLimit + ScreenContent   
                     FROM AppConfiguration   
                     WHERE ScreenName = @ScreenName 
                     AND ConfigurationType = 'Pagination'
                     AND Active = 1


                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp7].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)

                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @Postalcode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                  WHERE U.HcUserID = @UserID
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @Postalcode
                           END
                     END


                     DECLARE @Config VARCHAR(500)
					 SELECT @Config = ScreenContent
                     FROM AppConfiguration
                     WHERE ConfigurationType = 'Hubciti Media Server Configuration'

					SELECT HcHubCitiID
					INTO #RHubcitiList
					FROM
						(SELECT RA.HcHubCitiID
						FROM HcHubCiti H
						LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
						WHERE H.HcHubCitiID  = @HubcitiID
						UNION ALL
						SELECT @HubcitiID)A

					DECLARE @HubCitis varchar(100)
					SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
					FROM #RHubcitiList

					IF(@HcMenuItemID IS NOT NULL)
					BEGIN
						--MenuItem	

						SELECT DISTINCT H.HcHubCitiID AS HubcitiID,EC.HcEventCategoryID AS EventCategoryID
										,EC.HcEventCategoryName AS EventCategoryName,E.HcEventID AS EventID
						INTO #AllEventCategories
						FROM HcMenuItemEventCategoryAssociation ME
						INNER JOIN HcEventsCategoryAssociation ECA ON  ME.HcEventCategoryID = ECA.HcEventCategoryID AND  ME.HcMenuItemID = @HcMenuItemID	
						INNER JOIN HcEventsCategory EC ON ECA.HcEventCategoryID = EC.HcEventCategoryID
						INNER JOIN HcEvents E ON ECA.HcEventID = E.HcEventID
						LEFT JOIN HcHubCiti H ON E.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
							
					END
					ELSE IF(@HcBottomButtonID IS NOT NULL)
					BEGIN
						--BottomButton
								
						SELECT DISTINCT H.HcHubCitiID AS HubcitiID,EC.HcEventCategoryID AS EventCategoryID
										,EC.HcEventCategoryName AS EventCategoryName,E.HcEventID AS EventID
						INTO #AllEventCategories1
						FROM HcBottomButtonEventCategoryAssociation ME
						INNER JOIN HcEventsCategoryAssociation ECA ON  ME.HcEventCategoryID = ECA.HcEventCategoryID AND  ME.HcBottomButtonID = @HcBottomButtonID	
						INNER JOIN HcEventsCategory EC ON ECA.HcEventCategoryID = EC.HcEventCategoryID
						INNER JOIN HcEvents E ON ECA.HcEventID = E.HcEventID
						LEFT JOIN HcHubCiti H ON E.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						WHERE GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1

					END
						
				
						SELECT Param CityID
						INTO #Events111
						FROM fn_SplitParam(@CityIDs,',')

						SELECT Param CatID
						INTO #CategoryList
						FROM fn_SplitParam(@CategoryID,',')
												
					CREATE TABLE #Temp111(HcEventID Int
								,HcEventName Varchar(2000) 
								,ShortDescription Varchar(2000) 
								,LongDescription Varchar(2000)                                                     
								,HcHubCitiID Int
								,ImagePath Varchar(2000) 
								,BussinessEvent Int
								,PackageEvent Int								                                               
								,StartDate Date
								,EndDate Date
								,StartTime Time
								,EndTime Time                                   
								,MenuItemExist Int
								,HcEventCategoryID Int 
								,HcEventCategoryName Varchar(500)
								,Distance Int                                                                            
								,DistanceActual	Int									  
								,OnGoingEvent Int
								,RetailLocationID Int
								,City Varchar(100)
								,Lat Float
								,Long Float
								,SignatureEvent bit)
															
					

					IF(@RetailID IS NULL AND @HcMenuItemID IS NOT NULL AND @HcBottomButtonID IS NULL)
					BEGIN
																	
					INSERT INTO #Temp111(HcEventID 
										,HcEventName
										,ShortDescription 
										,LongDescription                                                    
										,HcHubCitiID 
										,ImagePath 
										,BussinessEvent
										,PackageEvent   									                                              
										,StartDate
										,EndDate 
										,StartTime 
										,EndTime                                
										,MenuItemExist
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual									  
										,OnGoingEvent 
										,RetailLocationID
										,City
										,Lat 
										,Long
										,SignatureEvent)
						SELECT   HcEventID 
								,HcEventName
								,ShortDescription 
								,LongDescription                                                    
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent
								,PackageEvent 							                                           
								,StartDate
								,EndDate 
								,StartTime 
								,EndTime                                
								,MenuItemExist
								,EventCategoryID
								,EventCategoryName 
								,Distance                                                                        
								,DistanceActual									  
								,OnGoingEvent 
								,RetailLocationID
								,CityName
								,RetailLocationLatitude 
								,RetailLocationLongitude
								,SignatureEvent
					FROM(
					SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RL1.RetailLocationID 
									,C.CityName	
									,RL1.RetailLocationLatitude 
									,RL1.RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 															 					   							
					FROM HcEvents E 
					INNER JOIN #AllEventCategories A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
					INNER JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
					LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
					LEFT JOIN RetailLocation  RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active=1) OR (RL1.RetailLocationID =EP.RetailLocationID AND RL1.Active=1)
					LEFT JOIN HcCity C ON RL1.HcCityID = C.HcCityID
					LEFT JOIN #Events111 EE ON EE.CityID =C.HcCityID 									                         
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
					AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =EE.CityID))
					AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))					                         
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath
							--, EventListingImagePath    		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, RL1.RetailLocationID
							, C.CityName
							,RL1.RetailLocationLatitude 
							,RL1.RetailLocationLongitude 
																	
						UNION ALL

						SELECT DISTINCT TOP 100 Percent E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
								
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RetailLocationID=0 
									,C.CityName	
									,RL1.Latitude  RetailLocationLatitude
									,RL1.Longitude 	RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 														 					   							
					FROM HcEvents E 
					INNER JOIN #AllEventCategories A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
					INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
					LEFT JOIN HcCity C ON HL.City = C.CityName
					LEFT JOIN #Events111 EE ON EE.CityID =C.HcCityID 									                         
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
					AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =EE.CityID))
					AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 	
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath  
							--, EventListingImagePath  		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, C.CityName
							,RL1.Latitude  
							,RL1.Longitude
							
						UNION ALL
						
						SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RL.RetailLocationID 
									,HC.CityName
									,RL.RetailLocationLatitude 
									,RL.RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END  			
						FROM #AllEventCategories A 
						INNER JOIN HcEvents E on A.EventID = E.HcEventID
						INNER JOIN HcRetailerEventsAssociation RE on E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
						INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND RE.RetailLocationID = R.RetailLocationID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
						INNER JOIN RetailLocation  RL ON RE.RetailLocationID=RL.RetailLocationID AND RL.Active=1  
						INNER JOIN HcCity HC ON RL.HcCityID = HC.HcCityID
						LEFT JOIN #Events111 EE ON EE.CityID =HC.HcCityID 	
						WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
						AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID =EE.CityID))
						AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 	
						--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath 
							--, EventListingImagePath   		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, RL.RetailLocationID
							, HC.CityName
							, RL.RetailLocationLatitude 
							,RL.RetailLocationLongitude 
							
							UNION ALL
						
						SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RetailLocationID=0 
									,HC.CityName
									,RL1.Latitude   RetailLocationLatitude 
									,RL1.Longitude  RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END  			
						FROM #AllEventCategories A 
						INNER JOIN HcEvents E on A.EventID = E.HcEventID AND E.HcHubCitiID IS NULL
						INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and A.EventCategoryID = c.HcEventCategoryID
						INNER JOIN HcEventLocation EL on E.HcEventID = EL.HcEventID
						INNER JOIN GeoPosition  RL1 ON EL.City =RL1.City AND EL.State =RL1.State AND RL1.PostalCode =EL.PostalCode
						INNER JOIN HcCity HC ON EL.City = HC.CityName 
						LEFT JOIN #Events111 EE ON EE.CityID =HC.HcCityID 	
						WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
						AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID =EE.CityID))
						AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 		
						--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath  
							--, EventListingImagePath  		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, HC.CityName
							, RL1.Latitude 
							, RL1.Longitude 	
							) E
							print 'Menuitem End'
					END
					ELSE IF(@RetailID IS NULL AND @HcMenuItemID IS NULL AND @HcBottomButtonID IS NOT NULL)
					BEGIN
						INSERT INTO #Temp111(HcEventID 
										,HcEventName
										,ShortDescription 
										,LongDescription                                                    
										,HcHubCitiID 
										,ImagePath 
										,BussinessEvent
										,PackageEvent   									                                              
										,StartDate
										,EndDate 
										,StartTime 
										,EndTime                                
										,MenuItemExist
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual									  
										,OnGoingEvent 
										,RetailLocationID
										,City
										,Lat
										,Long
										,SignatureEvent
										)
						SELECT   HcEventID 
								,HcEventName
								,ShortDescription 
								,LongDescription                                                    
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent
								,PackageEvent 							                                           
								,StartDate
								,EndDate 
								,StartTime 
								,EndTime                                
								,MenuItemExist
								,EventCategoryID
								,EventCategoryName 
								,Distance                                                                        
								,DistanceActual									  
								,OnGoingEvent 
								,RetailLocationID
								,CityName
								,RetailLocationLatitude
								,RetailLocationLongitude
								,SignatureEvent
					FROM(
					SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RL1.RetailLocationID 
									,C.CityName
									,RL1.RetailLocationLatitude 
									,RL1.RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 																 					   							
					FROM HcEvents E 
					INNER JOIN #AllEventCategories1 A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventAppsite EA ON A.EventID = EA.HcEventID
					INNER JOIN HcAppSite HA ON EA.HcAppSiteID = HA.HcAppsiteID
					LEFT JOIN HcEventPackage EP ON A.EventID =EP.HcEventID					
					LEFT JOIN RetailLocation  RL1 ON (HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active=1) OR (RL1.RetailLocationID =EP.RetailLocationID AND RL1.Active=1)
					LEFT JOIN HcCity C ON RL1.HcCityID = C.HcCityID
					LEFT JOIN #Events111 EE ON EE.CityID =C.HcCityID 									                         
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
					AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =EE.CityID))
					AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))					                         
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath  
							--, EventListingImagePath  		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, RL1.RetailLocationID
							, C.CityName
							,RL1.RetailLocationLatitude 
							,RL1.RetailLocationLongitude
						
						UNION ALL

						SELECT DISTINCT TOP 100 Percent E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
								
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RetailLocationID=0 
									,C.CityName	
									,RL1.Latitude  RetailLocationLatitude
									,RL1.Longitude 	RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 														 					   							
					FROM HcEvents E 
					INNER JOIN #AllEventCategories1 A ON E.HcEventID = A.EventID AND E.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
					INNER JOIN HcEventLocation HL ON E.HcEventID = HL.HcEventID				
					INNER JOIN GeoPosition  RL1 ON HL.City =RL1.City AND HL.State =RL1.State AND RL1.PostalCode =HL.PostalCode
					LEFT JOIN HcCity C ON HL.City = C.CityName
					LEFT JOIN #Events111 EE ON EE.CityID =C.HcCityID 									                         
					WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
					AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =EE.CityID))
					AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 	
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath   
							--, EventListingImagePath 		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, C.CityName
							,RL1.Latitude  
							,RL1.Longitude
								
						UNION ALL
						
						SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RL.RetailLocationID 
									,HC.CityName
									,RL.RetailLocationLatitude
									,RL.RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 			
						FROM #AllEventCategories1 A 
						INNER JOIN HcEvents E on A.EventID = E.HcEventID
						INNER JOIN HcRetailerEventsAssociation RE on E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
						INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND RE.RetailLocationID = R.RetailLocationID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and a.EventCategoryID = c.HcEventCategoryID
						INNER JOIN RetailLocation  RL ON RE.RetailLocationID=RL.RetailLocationID  AND RL.Active=1
						INNER JOIN HcCity HC ON RL.HcCityID = HC.HcCityID
						LEFT JOIN #Events111 EE ON EE.CityID =HC.HcCityID 	
						WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
						AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID =EE.CityID))
						AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 	
						--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath  
							--, EventListingImagePath  		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, RL.RetailLocationID
							, HC.CityName
							,RL.RetailLocationLatitude
							,RL.RetailLocationLongitude	
						UNION ALL
						
						SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(StartDate AS DATE)
									,EndDate =CAST(EndDate AS DATE)
									,StartTime =CAST(StartDate AS Time)
									,EndTime =CAST(EndDate AS Time)                                     
									,MenuItemExist =    CASE WHEN(SELECT COUNT(HcMenuItemID)
														FROM HcMenuItem MI 
														INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
														WHERE LinkTypeName = 'Events' 
														AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
									,A.EventCategoryID 
									,A.EventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RetailLocationID=0 
									,HC.CityName
									,RL1.Latitude   RetailLocationLatitude 
									,RL1.Longitude  RetailLocationLongitude
									,SignatureEvent = CASE WHEN (A.EventCategoryName = 'Signature Events') THEN 1 ELSE 0 END  			
						FROM #AllEventCategories1 A 
						INNER JOIN HcEvents E on A.EventID = E.HcEventID AND E.HcHubCitiID IS NULL
						INNER JOIN HcRetailerAssociation R on E.RetailID = R.RetailID AND Associated = 1 AND R.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))
						INNER JOIN HcEventsCategoryAssociation c on e.HcEventID = c.HcEventID and c.HcHubCitiID is null and A.EventCategoryID = c.HcEventCategoryID
						INNER JOIN HcEventLocation EL on E.HcEventID = EL.HcEventID
						INNER JOIN GeoPosition  RL1 ON EL.City =RL1.City AND EL.State =RL1.State AND RL1.PostalCode =EL.PostalCode
						INNER JOIN HcCity HC ON EL.City = HC.CityName 
						LEFT JOIN #Events111 EE ON EE.CityID =HC.HcCityID 	
						WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
						AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID =EE.CityID))
						AND ((ISNULL(@CategoryID, '0') <> '0' AND A.EventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 		
						--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
						GROUP BY E.HcEventID 
							, HcEventName
							, ShortDescription
							, LongDescription                                                     
							, E.HcHubCitiID 
							, BussinessEvent
							, PackageEvent
							, ImagePath
							--, EventListingImagePath    		
							, StartDate
							, EndDate
							, A.EventCategoryID 
							, A.EventCategoryName
							, OnGoingEvent
							, E.RetailID
							--, RE.HcEventID
							, HC.CityName
							, RL1.Latitude 
							, RL1.Longitude 	
							) E

					END
					ELSE IF (@RetailID IS NOT NULL)
					BEGIN
								INSERT INTO #Temp111(HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,City
												,Lat
												,Long
												,SignatureEvent)
								SELECT HcEventID 
												,HcEventName
												,ShortDescription 
												,LongDescription                                                    
												,HcHubCitiID 
												,ImagePath 
												,BussinessEvent
												,PackageEvent                                                 
												,StartDate
												,EndDate 
												,StartTime 
												,EndTime                                
												,MenuItemExist
												,HcEventCategoryID
												,HcEventCategoryName 
												,Distance                                                                        
												,DistanceActual									  
												,OnGoingEvent 
												,RetailLocationID
												,CityName
												,RetailLocationLatitude
												,RetailLocationLongitude
												,SignatureEvent
							FROM
							(SELECT DISTINCT E.HcEventID 
													,HcEventName  
													,ShortDescription
													,LongDescription                                                     
													,E.HcHubCitiID 
													--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 
													,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
													,BussinessEvent 
													,PackageEvent                                                     
													,StartDate =CAST(StartDate AS DATE)
													,EndDate =CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																						FROM HcMenuItem MI 
																						INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																						WHERE LinkTypeName = 'Events' 
																						AND MI.LinkID = E.HcEventID)>0 THEN 1 ELSE 0 END  
													,EC.HcEventCategoryID 
													,EC.HcEventCategoryName 
													,Distance=0                                           
													,DistanceActual =0
													,E.OnGoingEvent 
													,RL1.RetailLocationID 	
													,HC.CityName
													,RL1.RetailLocationLatitude
													,RL1.RetailLocationLongitude	
													,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END 
								FROM HcEvents E 
								INNER JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID
								INNER JOIN HcRetailerAssociation RA ON RE.RetailID = RA.RetailID AND RE.RetailLocationID = RA.RetailLocationID AND Associated = 1 AND RA.HcHubCitiID IN (select param from fn_SplitParam(@HubCitis,','))	
								INNER JOIN RetailLocation RL1 ON RE.RetailLocationID = RL1.RetailLocationID AND RL1.Active=1
								INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
								INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 	
								INNER JOIN HcCity Hc ON RL1.HcCityID = Hc.HcCityID
								WHERE E.RetailID =@RetailID AND RE.RetailLocationID=@RetailLocationID
								AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
								AND (( ISNULL(@CategoryID, '0') <> '0' AND EC.HcEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))  
								--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
								AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active = 1
								GROUP BY E.HcEventID 
										, HcEventName
										, ShortDescription
										, LongDescription                                                     
										, E.HcHubCitiID 
										, BussinessEvent
										, PackageEvent    
										, ImagePath
										--, EventListingImagePath
										, StartDate
										, EndDate
										, EC.HcEventCategoryID
										, EC.HcEventCategoryName
										, OnGoingEvent
										, E.RetailID
										, RE.HcEventID
										, RL1.RetailLocationID
										, HC.CityName
										,RL1.RetailLocationLatitude
										,RL1.RetailLocationLongitude	
										) E


					END					
					ELSE IF(@FundRaisingID IS NOT NULL)
					BEGIN
					
					INSERT INTO #Temp111(HcEventID 
										,HcEventName
										,ShortDescription 
										,LongDescription                                                    
										,HcHubCitiID 
										,ImagePath 
										,BussinessEvent
										,PackageEvent   									                                              
										,StartDate
										,EndDate 
										,StartTime 
										,EndTime                                
										,MenuItemExist
										,HcEventCategoryID
										,HcEventCategoryName 
										,Distance                                                                        
										,DistanceActual									  
										,OnGoingEvent 
										,RetailLocationID
										,City
										,Lat
										,Long
										,SignatureEvent
										)
						SELECT   HcEventID 
								,HcEventName
								,ShortDescription 
								,LongDescription                                                    
								,HcHubCitiID 
								,ImagePath 
								,BussinessEvent
								,PackageEvent 							                                           
								,StartDate
								,EndDate 
								,StartTime 
								,EndTime                                
								,MenuItemExist
								,HcEventCategoryID
								,HcEventCategoryName 
								,Distance                                                                        
								,DistanceActual									  
								,OnGoingEvent 
								,RetailLocationID
								,CityName
								,RetailLocationLatitude
								,RetailLocationLongitude
								,SignatureEvent
					FROM(
					SELECT DISTINCT  E.HcEventID 
									,HcEventName  
									,E.ShortDescription
									,E.LongDescription                                                     
									,E.HcHubCitiID 	
									--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
									,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
									,BussinessEvent 
									,PackageEvent                                                     
									,StartDate =CAST(E.StartDate AS DATE)
									,EndDate =CAST(E.EndDate AS DATE)
									,StartTime =CAST(E.StartDate AS Time)
									,EndTime =CAST(E.EndDate AS Time)                                     
									,MenuItemExist = 0   
									,EA.HcEventCategoryID 
									,EC.HcEventCategoryName 
									,Distance=0                                           				
									,DistanceActual =0				
									,E.OnGoingEvent 
									,RL1.RetailLocationID 
									,HC.CityName
									,RL1.RetailLocationLatitude
									,RL1.RetailLocationLongitude
									,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END	
					FROM HcFundraising HF
					INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
					INNER JOIN  HcEvents E ON HF.HcHubCitiID = E.HcHubCitiID AND F.HcEventID = E.HcEventID
					INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
					INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
					LEFT JOIN HcEventLocation HL ON E.HcEventID =HL.HcEventID 
					LEFT JOIN HcEventAppsite EAP ON E.HcEventID = EAP.HcEventID
					LEFT JOIN HcAppSite HA ON EAP.HcAppSiteID = HA.HcAppsiteID
					LEFT JOIN HcEventPackage EP ON EA.HcEventID =EP.HcEventID
					LEFT JOIN RetailLocation RL1 ON (HA.RetailLocationID = RL1.RetailLocationID OR EP.RetailLocationID =RL1.RetailLocationID) AND (RL1.Active=1)
					LEFT JOIN HcRetailerAssociation RA ON RL1.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HubCitiID AND Associated =1						                                    
					INNER JOIN HcCity Hc ON RL1.HcCityID = Hc.HcCityID OR HL.City = Hc.CityName
					WHERE F.HcFundRaisingID = @FundRaisingID
					AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1															 					   							
					AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
					AND (( ISNULL(@CategoryID, '0') <> '0' AND EA.HcEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					
					UNION ALL
						
					SELECT DISTINCT  E.HcEventID 
								,HcEventName  
								,E.ShortDescription
								,E.LongDescription                                                     
								,E.HcHubCitiID 	
								--,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ISNULL(EventListingImagePath,ImagePath)),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+ISNULL(EventListingImagePath,ImagePath)) 			
								,ImagePath=IIF(E.retailid IS NULL,(@Config + CAST(E.HCHubcitiID AS VARCHAR(100))+'/'+ImagePath),@RetailConfig+CAST(E.RetailID AS VARCHAR)+'/'+E.ImagePath) 			
								,BussinessEvent 
								,PackageEvent                                                     
								,StartDate =CAST(E.StartDate AS DATE)
								,EndDate =CAST(E.EndDate AS DATE)
								,StartTime =CAST(E.StartDate AS Time)
								,EndTime =CAST(E.EndDate AS Time)                                     
								,MenuItemExist =0  
								,EA.HcEventCategoryID 
								,EC.HcEventCategoryName 
								,Distance=0                                           				
								,DistanceActual =0				
								,E.OnGoingEvent 
								,RL1.RetailLocationID 
								,HC.CityName
								,RL1.RetailLocationLatitude
								,RL1.RetailLocationLongitude
								,SignatureEvent = CASE WHEN (EC.HcEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END				
					FROM HcFundraising HF
					INNER JOIN HcFundraisingEventsAssociation F ON HF.HcFundraisingID = F.HcFundraisingID
					INNER JOIN  HcEvents E ON HF.RetailID = E.RetailID AND F.HcEventID = E.HcEventID
					INNER JOIN HcEventsCategoryAssociation EA ON EA.HcEventID =E.HcEventID 
					INNER JOIN HcEventsCategory EC ON EC.HcEventCategoryID =EA.HcEventCategoryID 
					LEFT JOIN HcEventLocation HL ON HL.HcEventID =E.HcEventID 
					LEFT JOIN HcRetailerEventsAssociation RE ON E.HcEventID = RE.HcEventID AND E.RetailID = RE.RetailID 
					LEFT JOIN HcRetailerAssociation RA ON RE.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HubCitiID AND Associated =1							 
					LEFT JOIN RetailLocation RL1 ON RE.RetailLocationID =RL1.RetailLocationID AND RL1.Active=1	
					INNER JOIN HcCity Hc ON RL1.HcCityID = Hc.HcCityID OR HL.City = Hc.CityName
					WHERE F.HcFundRaisingID = @FundRaisingID 
					AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1) AND E.Active = 1
					AND ((@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND HC.HcCityID IN (SELECT Param FROM fn_SplitParam(@CityIDs,','))))
					AND (( ISNULL(@CategoryID, '0') <> '0' AND EA.HcEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1)) 
					--AND ((@SearchParameter IS NOT NULL AND HcEventName LIKE '%'+@SearchParameter+'%') OR (@SearchParameter IS NULL AND 1=1))
					) E
						
					END
					
					SELECT DISTINCT HcEventID 
									,HcEventName  
									,ShortDescription
									,LongDescription                                                     
									,T.HcHubCitiID 
									,ImagePath
									,BussinessEvent 
									,PackageEvent 						                                                   
									,StartDate
									,EndDate 
									,StartTime 
									,EndTime                             
									,MenuItemExist
									,HcEventCategoryID 
									,HcEventCategoryName 										                                                                    
									,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(Long) IS NULL THEN MIN(G.Longitude) ELSE MIN(Long) END/ 57.2958))))*6371) * 0.6214 END, 0))
									,DistanceActual =(ISNULL(CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(Lat) IS NULL THEN MIN(G.Latitude) ELSE MIN(Lat) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(Long) IS NULL THEN MIN(G.Longitude) ELSE MIN(Long) END/ 57.2958))))*6371) * 0.6214, 1, 1) END ,0))                           
							 		,OnGoingEvent 	
									--,T.RetailLocationID
									,T.City
									,SignatureEvent									  
						INTO #Temp
						FROM #Temp111 T
						--LEFT JOIN RetailLocation RL1 ON RL1.RetailLocationID =T.RetailLocationID 
						--LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = T.HcHubCitiID AND Associated =1 AND RA.RetailLocationID = RL1.RetailLocationID                  
						LEFT JOIN Geoposition G ON T.City =G.City 
						WHERE ((@EventDate IS NOT NULL AND StartDate = @EventDate) OR (@EventDate IS NULL AND 1=1))                             
						GROUP BY HcEventID 
								,HcEventName  
								,ShortDescription
								,LongDescription                                                     
								,T.HcHubCitiID 
								,ImagePath
								,BussinessEvent 
								,PackageEvent   				                                            
								,StartDate
								,EndDate 
								,StartTime 
								,EndTime                             
								,MenuItemExist
								,HcEventCategoryID 
								,HcEventCategoryName 
								,Distance
								,DistanceActual
								,OnGoingEvent
								--,T.RetailLocationID
								,T.City
								,SignatureEvent

				CREATE TABLE #Events(RowNum INT IDENTITY(1, 1)
						,HcEventID Int
						,HcEventName  Varchar(2000)
						,ShortDescription Varchar(2000)
						,LongDescription  Varchar(2000)                                                
						,HcHubCitiID Int
						,ImagePath Varchar(2000) 
						,BussinessEvent Bit
						,PackageEvent Bit       						                                     
						,StartDate date
						,EndDate date
						,StartTime Time
						,EndTime Time                                         
						,MenuItemExist bit
						,HcEventCategoryID int
						,HcEventCategoryName Varchar(2000)
						,Distance Float
						,DistanceActual float
						,OnGoingEvent bit
						--,RetailLocationID int
						,City Varchar(100)
						,SignatureEvent bit)


		INSERT INTO #Events(HcEventID  
						,HcEventName  
						,ShortDescription 
						,LongDescription                                                 
						,HcHubCitiID 
						,ImagePath 
						,BussinessEvent 
						,PackageEvent 						                                               
						,StartDate 
						,EndDate 
						,StartTime 
						,EndTime                                       
						,MenuItemExist 
						,HcEventCategoryID 
						,HcEventCategoryName
						,Distance
						,DistanceActual
						,OnGoingEvent
						--,RetailLocationID
						,City
						,SignatureEvent)

		SELECT   HcEventID 
				,HcEventName  
				,ShortDescription
				,LongDescription                                                   
				,HcHubCitiID 
				,ImagePath
				,BussinessEvent 
				,PackageEvent				                                                   
				,StartDate 
				,EndDate 
				,StartTime
				,EndTime                        
				,MenuItemExist
				--,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
				--,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
				,HcEventCategoryID
				,HcEventCategoryName
				,Distance   
				,DistanceActual
				,OnGoingEvent 
				--,RetailLocationID  
				,City 
				,SignatureEvent                                                                                            
			FROM #Temp           
			GROUP BY HcEventID 
					,HcEventName  
					,ShortDescription
					,LongDescription                                                   
					,HcHubCitiID 
					,ImagePath
					,BussinessEvent 
					,PackageEvent  					                                                   
					,StartDate 
					,EndDate 
					,StartTime
					,EndTime                        
					,MenuItemExist     
					--,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
					--,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
					,HcEventCategoryID
					,HcEventCategoryName
					,Distance
					,DistanceActual
					,OnGoingEvent
					--,RetailLocationID 
					,City
					,SignatureEvent
			ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
					CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Date' OR @SortColumn = 'City') THEN CAST(StartDate AS SQL_VARIANT)
							WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
							WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN HcEventName
							WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(StartDate AS SQL_VARIANT)                                                       
					END ASC ,StartDate
					--CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'StartDate' OR @SortColumn = 'City') THEN CAST(StartDate AS SQL_VARIANT)
					--		WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS SQL_VARIANT)
					--		WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
					--		WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(StartDate AS SQL_VARIANT)
					--END DESC

	
			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Events
                           
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			---User Tracking
		
			CREATE TABLE #Temp1(EventsListID INT, HcEventID Int)
                     
			INSERT INTO HubCitiReportingDatabase..EventsList(MainMenuID
															,HcEventID 
															,EventClick 
															,DateCreated)

			OUTPUT Inserted.EventsListID,inserted.HCEventID INTO #Temp1(EventsListID ,HcEventID)

			SELECT @MainMenuID
				, HcEventID
					,0
					,GETDATE()
			FROM #Events           
                     
			DECLARE @Seed INT
			SELECT @Seed = ISNULL(MIN(EventsListID), 0)
			FROM #Temp1                
                     
			SELECT DISTINCT RowNum = Row_Number() over(order by RowNum asc)
					,HcEventID  
					,HcEventName  
					,ShortDescription 
					,LongDescription                                                  
					,HcHubCitiID 
					,ImagePath 
					,BussinessEvent 
					,PackageEvent   					                                              
					,StartDate 
					,EndDate 
					,StartTime 
					,EndTime                                       
					,MenuItemExist 
					,HcEventCategoryID 
					,HcEventCategoryName
					,Distance
					,DistanceActual
					,OnGoingEvent 
					--,RetailLocationID 
					,City
			INTO #Event
			FROM #Events
                
			DECLARE @SQL VARCHAR(1000) = 'ALTER TABLE #Event ADD EventsListID1 INT IDENTITY('+CAST(@Seed AS VARCHAR(100))+', 1)'
			EXEC (@SQL)
               
			SELECT DISTINCT Rownum rowNum
					,T.EventsListID eventListId                                    
					,E.HcEventID eventId
					,HcEventName eventName
					,ShortDescription shortDes
					,LongDescription longDes                                                      
					,HcHubCitiID hubCitiId
					,ImagePath imgPath
					,BussinessEvent busEvent
					,PackageEvent pkgEvent  					                                         
					,StartDate 
					,EndDate 
					,StartTime
					,EndTime                        
					,MenuItemExist mItemExist
					,HcEventCategoryID eventCatId
					,HcEventCategoryName eventCatName      
					,Distance   = ISNULL(DistanceActual, Distance) 
					,ISNULL(OnGoingEvent, 0) isOnGoing   
					--,RetailLocationID 
					,City                                                  
			FROM #Event E
			INNER JOIN #Temp1 T ON T.EventsListID=E.EventsListID1
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  
			ORDER BY RowNum 
              
			--To get list of BottomButtons for this Module
			DECLARE @ModuleName Varchar(50) 
			SET @ModuleName = 'Event'					
						--EXEC [HubCitiApp7].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,@ModuleName,@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                

			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
					, HM.HcHubCitiID 
					, BB.HcBottomButtonID AS bottomBtnID
					, ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
					, bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
					, bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
					, BottomButtonLinkTypeID AS btnLinkTypeID
					--, btnLinkID = BottomButtonLinkID
					--, btnLinkTypeName = (CASE  WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
					--			                WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
					--																																            INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
					--																																            WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
				--                     ELSE BottomButtonLinkTypeName END)
					, btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
												INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
												WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1 AND BLT.BottomButtonLinkTypeName <> 'Dining' THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
																																					INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
																																					INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
																																					WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
											WHEN (SELECT COUNT(DISTINCT HcEventCategoryID) FROM  HcBottomButtonEventCategoryAssociation 
												WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID = @HubCitiID) = 1 THEN 'EventSingleCategory-' + (SELECT DISTINCT HcEventCategoryName FROM HcEventsCategory EC
																																								INNER JOIN HcBottomButtonEventCategoryAssociation BBC ON EC.HcEventCategoryID = BBC.HcEventCategoryID
																																								WHERE HcBottomButtonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                      	 
									ELSE BottomButtonLinkTypeName END)					
			FROM HcMenu HM
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			WHERE LT.LinkTypeDisplayName = @ModuleName AND FBB.HcHubCitiID = @HubCitiID
			ORDER by HcFunctionalityBottomButtonID

			--Confirmation of Success
			SELECT @Status = 0   
					 					  
       
      END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_HcHubRegionEventsDisplay.'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;





















GO
