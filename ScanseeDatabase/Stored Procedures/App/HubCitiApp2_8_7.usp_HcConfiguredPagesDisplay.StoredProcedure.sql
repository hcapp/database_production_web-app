USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcConfiguredPagesDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcConfiguredPagesDisplay]
Purpose					: To display the Configured Pages for the Hub Citi.
Example					: [usp_HcConfiguredPagesDisplay] 

History
Version		     Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			9th sept 2013	  SPAN	     Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE[HubCitiApp2_8_7].[usp_HcConfiguredPagesDisplay]
(
	
	--Input Variable 
      @PageType varchar(100)
    , @HubCitiKey varchar(100)
   
	--OutPut Variable
	, @HubCitiID Int output
	, @HubCitiName varchar(255) output
	, @VersionNumber varchar(20) output
	, @SplashImage varchar(1000) output
	, @SmallLogo varchar(1000) output
	, @ScanSeeLogo varchar(1000) output
	, @Logo varchar(1000) output
	, @homeImgPath varchar(1000) output
	, @bkImgPath varchar(1000) output
	, @titleBkGrdColor varchar(20) output
	, @titleTxtColor varchar(20) output
	, @templatetype varchar(50) output
	, @MaxCnt int output
	, @HamburgerImagePath varchar(1000) output
	, @domainName varchar(1000) output

	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT
)
AS
BEGIN

	BEGIN TRY
			
		DECLARE @HubCitiImageConfig VARCHAR(100)
		DECLARE @HTMLConfig VARCHAR(100), @NewsImage varchar(1000)

		SELECT @domainName = 'https://www.scansee.net/' -- 'https://hubcitiweb.com'
					
		DECLARE @DefaultHamburgerIcon VARCHAR(1000)
		select @DefaultHamburgerIcon = ScreenContent +  'images/hamburger.png' 
		from AppConfiguration 
		where ScreenName = 'Base_URL'
			
			SELECT @HubCitiImageConfig = 
			ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'


			SELECT @HubCitiID = HcHubCitiId 
			FROM HcHubCiti 
			where HcHubCitiKey = @HubCitiKey

			SELECT @NewsImage = ScreenContent + CONVERT(VARCHAR,@HubCitiID)+ '/'
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

	

			SELECT @HTMLConfig = 
			ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti html path'
			
			SELECT @HubCitiID = 
			HcHubCitiID
				 , @HubCitiName = 
				 HubCitiName
			FROM HcHubCiti 
			WHERE HcHubCitiKey = @HubCitiKey AND Active = 1
			
			
			DECLARE @DefaultBackButtonImage varchar(1000)
				   ,@DefaultSmallLogoImage varchar(1000)
				   ,@DefaultHomeLogoImage varchar(1000)

			select @DefaultBackButtonImage = @HubCitiImageConfig + ScreenContent	
			FROM AppConfiguration
			WHERE ConfigurationType = 'Back Button'

			select @DefaultSmallLogoImage = @HubCitiImageConfig + ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Small Logo'

			select @DefaultHomeLogoImage = @HubCitiImageConfig + ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Home Logo'	
					
			
			SELECT @Logo = @HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+ '/'+ LogoImage
				 , @SplashImage = @HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+'/'+ SplashImage
				 , @SmallLogo = ISNULL(@HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+'/'+ SmallLogo, @DefaultSmallLogoImage)
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID

			SELECT @ScanSeeLogo =
			 ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'HubCiti'
			AND ScreenName = 'ScanSee PoweredBy Logo'


			

		SELECT  distinct @homeImgPath = ISNULL(@HubCitiImageConfig + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName , @DefaultHomeLogoImage)
				,@bkImgPath =  ISNULL(@HubCitiImageConfig + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName  , @DefaultBackButtonImage)
				,@titleBkGrdColor =   HU.backGroundColor 
				,@titleTxtColor =  HU.titleColor 
				,@templatetype = CASE WHEN HM.IsAssociateNews= 1 AND Level = 1 THEN HT.TemplateName else NUll END
				,@HamburgerImagePath = ISNULL(@NewsImage + NewsHamburgerIcon,@DefaultHamburgerIcon)
					
				FROM HcHubCiti HC
				LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
				LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId	
				INNER JOIN HcMenu HM ON HM.HcHubCitiID = HC.HcHubCitiID 	
				INNER JOIN HcTemplate HT ON HT.HcTemplateID = HM.HcTemplateID	
				WHERE HC.HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL) AND HM.HcHubCitiID = @HubCitiID 
			
			--Get the latest version of the Hub Citi app.
		   -- SELECT @VersionNumber = 'Version ' + ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCiti' AND ScreenName = 'Hub Citi Version Number'
		    SELECT @VersionNumber = 'Version ' + ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'CurrentHubCitiAppVersionIOS' AND ScreenName = 'CurrentHubCitiAppVersionIOS'
		
			SELECT  DISTINCT HC.HcHubCitiID hubCitiId
			        ,HPT.PageType 
				    ,BackgroundColor bkgndColor
				    ,FontColor
					,Title
					,[Description]
					,ButtonColor btnColor
					,ButtonFontColor btnFontColor
				    ,LogoImg = @HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+ '/'+ LogoImage
				    ,SmallLogo = ISNULL(@HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+'/'+ SmallLogo, @DefaultSmallLogoImage)
				    ,HubCitiImg = @HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+'/'+[Image]
				    ,PageLink = CASE WHEN PageType = 'About Us Page' OR PageType = 'Privacy Policy' THEN @HTMLConfig + CAST(HC.HcHubCitiID AS VARCHAR(10)) + '/' + HPT.HTMLPageType+'.html' ELSE NULL END 
					-- ,SplashImage = @HubCitiImageConfig + CAST(@HubCitiID AS VARCHAR(10))+'/'+ SplashImage
			FROM HcHubCiti  HC
			LEFT JOIN HcPageConfiguration HPC ON HPC.HcHubCitiID = HC.HcHubCitiID
			LEFT JOIN HcPageType HPT ON HPT.HcPageTypeID = HPC.HcPageTypeID 
			WHERE HC.HcHubCitiID = @HubCitiID 
			AND HPT.PageType = @PageType 

			SELECT @MaxCnt =  @@ROWCOUNT 
			
			
		--Confirmation of Success
		SELECT @Status = 0
		
	END TRY		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcConfiguredPagesDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			--Confirmation of Failure
			SELECT @Status = 1 
		END;
		 
	END CATCH;
END;










GO
