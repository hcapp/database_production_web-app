USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcQRDisplayRetailerCreatedPages]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: usp_HcQRDisplayRetailerCreatedPages
Purpose					: To Retrieve the list of Custom pages(Anything Page) associated with the retailer location.
Example					: usp_HcQRDisplayRetailerCreatedPages

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24th Oct 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcQRDisplayRetailerCreatedPages]
(
	  
	  @UserID int
	, @RetailID int
    , @RetailLocationID int
	, @AppsiteID int
	, @HcHubCitiID int
    
    --User Tracking Inputs
    , @RetailerListID int
    , @MainMenuID int
    , @ScanTypeID bit
    
	--Output Variable 
	, @EventExists bit output
	, @FundRaisingExists bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Configuration VARCHAR(100)
		DECLARE @QRType INT
		DECLARE @RetailerConfig varchar(50)
		DECLARE @ScanType int
		DECLARE	@RowCount int		
		DECLARE @RetailEventsIconLogo Varchar(1000)
		DECLARE @RetailFundRaisingIconLogo Varchar(1000)

		SET @EventExists = 0
		SET @FundRaisingExists = 0

		
		SELECT @RetailLocationID = ISNULL(@RetailLocationID , A.RetailLocationID)
			   ,@RetailID = ISNULL(@RetailID, RL.RetailID)
		FROM HcAppSite A
		INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
		WHERE HcAppSiteID = @AppsiteID

		
		SELECT @EventExists = 1 
		FROM HcRetailerEventsAssociation RE 
		INNER  JOIN HcEvents E ON RE.HcEventID = E.HcEventID
		WHERE RE.RetailID = @RetailID AND RetailLocationID = @RetailLocationID
		--AND E.EndDate > GETDATE()
		AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND E.Active=1

		--Verify If FundRaising Exist For A retailer
		SELECT @FundRaisingExists = 1 
		FROM HcRetailerFundraisingAssociation RF 
		INNER  JOIN HcFundraising F ON F.HcFundraisingID =RF.HcFundraisingID 
		WHERE RF.RetailID = @RetailID AND RetailLocationID = @RetailLocationID
		--AND E.EndDate > GETDATE()
		AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active=1

		SELECT @ScanType = ScanTypeID
		FROM HubCitiReportingDatabase..ScanType
		WHERE ScanType = 'Retailer Summary QR Code'
			   
		SELECT @RetailerConfig= ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		--Get the Server Configuration.
		SELECT @Configuration = ScreenContent FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'QR Code Configuration'
		AND Active = 1		
				 
		SELECT @QRType = QRTypeID 
		FROM QRTypes 
		WHERE QRTypeName LIKE 'Anything Page' 

		SELECT @RetailEventsIconLogo=@RetailerConfig+ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Retailer Events Icon Logo'

		SELECT @RetailFundRaisingIconLogo=@RetailerConfig+ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Retailer Fundraiser Icon Logo'

		--Fetch all the Created Pages for the given Retailer Location		
		SELECT  DISTINCT QR.QRRetailerCustomPageID pageID
					   , QR.Pagetitle pageTitle
					   , CASE WHEN [Image] IS NULL THEN (SELECT @RetailerConfig + QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QR.QRRetailerCustomPageIconID) 
								ELSE @RetailerConfig + CAST(ISNULL(@RetailID, (SELECT RetailID FROM RetailLocation RL WHERE RL.RetailLocationID = H.RetailLocationID AND RL.Active=1)) AS VARCHAR(10))+ '/' + [Image] END pageImage		 

					  , PageLink = @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') as varchar(10)) + '.htm?retId=' + CAST(QRCPA.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))+ CASE WHEN QR.URL IS NOT NULL OR M.MediaPath LIKE '%pdf' OR M.MediaPath LIKE '%png' THEN '&EL=true'WHEN M.MediaPath LIKE '%html'THEN '&EL=true&source=HubCiti' ELSE '&EL=false'END --+ '&oType=' + MapDisplayTypeName
					  , ISNULL(SortOrder, 100000) SortOrder	
					  , CASE WHEN ExternalFlag = 1 THEN MediaPath ELSE (SELECT @RetailerConfig + CAST(QR.RetailID AS VARCHAR(10))+ '/' + MediaPath FROM QRRetailerCustomPageMedia WHERE QRRetailerCustomPageMediaID = M.QRRetailerCustomPageMediaID) END pageTempLink
		INTO #AnythingPages	  
		FROM QRRetailerCustomPage QR
		INNER JOIN QRRetailerCustomPageAssociation QRCPA ON QR.QRRetailerCustomPageID = QRCPA.QRRetailerCustomPageID
	 
		LEFT JOIN QRRetailerCustomPageMedia M ON M.QRRetailerCustomPageID = QR.QRRetailerCustomPageID	
		LEFT JOIN HcAppSite H ON H.RetailLocationID = QRCPA.RetailLocationID
		
		WHERE ((ISNULL(@RetailLocationID, 0) <> 0 AND QRCPA.RetailLocationID = @RetailLocationID)
				OR
			 (ISNULL(@AppsiteID, 0) <> 0 AND H.HcAppSiteID = @AppsiteID))
		AND QR.QRTypeID = @QRType
		AND ((QR.StartDate IS NULL AND QR.EndDate IS NULL) 
		      OR 
		     (CAST(GETDATE() AS DATE) BETWEEN QR.StartDate AND ISNULL(QR.EndDate, GETDATE() + 1))
		    )
		ORDER BY ISNULL(SortOrder, 100000) ,Pagetitle  

		
		--Check for the existance of the Anything Pages. 
		SELECT @RowCount =@@ROWCOUNT 

		--Appsite Logistic implementation
		DECLARE @AppsiteConfig Varchar(500) 
		--DECLARE @HubCitiID int

		SELECT @AppsiteConfig = ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'



		SELECT HcAppsiteLogisticID pageID
				,LogisticName pageTitle
				,pageImage = @AppsiteConfig + CAST(RL.HcHubCitiID AS VARCHAR)+ '/'+ LogisticImage
				,PageLink = @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Appsite Logistic Page') AS VARCHAR(10)) 
					+ '.htm?logisticsId=' + CAST(HcAppsiteLogisticID AS VARCHAR(100)) + '&hubcitiId=' +CAST(RL.HcHubCitiID AS VARCHAR(100)) + '&oType=' + MapDisplayTypeName
		INTO #AppsiteLogisticMap
		FROM HcAppsiteLogistic RL
		INNER JOIN MapDisplayType TT ON TT.MapDisplayTypeID = RL.MapDisplayTypeID
		INNER JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated = 1
		WHERE RL.RetailLocationID = @RetailLocationID AND RL.HcHubCitiID = @HcHubCitiID
		AND (CAST(GETDATE() AS DATE) BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1))
		
		--User Tracking	




		--UPDATE HubCitiReportingDatabase..RetailerList
		--SET RetailLocationClick = 1 
		--WHERE retailid = @RetailID and retaillocationid= @RetailLocationID AND RetailerListID = @RetailerListID
		

				
			--Capture the impressions of the Anything pages.
		
			CREATE TABLE #Temp(AnythingPageListID INT, RetailerListID int, PageID int)
			
			INSERT INTO HubCitiReportingDatabase..AnythingPageList(RetailerListID
																 , AnythingPageID
																 , AppsiteID
																 , MainMenuID
																 , DateCreated)
													
			OUTPUT inserted.AnythingPageListID, inserted.RetailerListID, inserted.AnythingPageID INTO #Temp(AnythingPageListID, RetailerListID, PageID)
														
			SELECT DISTINCT @RetailerListID
					, pageID
					, @AppsiteID
					, @MainMenuID
					, GETDATE()
			FROM #AnythingPages
														
			IF @AppsiteID IS NOT NULL
			BEGIN
				CREATE TABLE #Temp1(AppsiteListID INT, AppsiteID int)
				
				INSERT INTO HubCitiReportingDatabase..AppsiteList(AppsiteID
																	 , MainMenuID
																	 , DateCreated)
														
				OUTPUT inserted.AppsiteListID, inserted.AppsiteID INTO #Temp1(AppsiteListID, AppsiteID)
															
															SELECT @AppsiteID															 
																 , @MainMenuID
																 , GETDATE()
															FROM #AnythingPages
			END												
			
				
			--Display the Anything page list along with the primary key of the Tracking table. Also to list events first followed by anything pages.					
			
			IF @EventExists = 1 AND @FundRaisingExists =1
			BEGIN
					SELECT 99999 AnythingPageListID
						   ,@RetailerListID retListID
						   ,9999 pageID
						   ,'Events' pageTitle
						   ,@RetailEventsIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder	

					UNION ALL

					SELECT 99999 AnythingPageListID
						   ,@RetailerListID retListID
						   ,9999 pageID
						   ,'Fundraisers' pageTitle
						   ,@RetailFundRaisingIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder	
					UNION ALL
					
					SELECT 99999 AnythingPageListID
							,@RetailerListID retListID
							,pageID
							,pageTitle
							,pageImage
							,PageLink
							,NULL pageTempLink	
						    ,0 SortOrder	
					FROM #AppsiteLogisticMap

					UNION ALL

					SELECT T.AnythingPageListID
							 , @RetailerListID retListID
							 , A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink	
							 , A.SortOrder SortOrder		 
					FROM #AnythingPages A
					INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle							
			END

			ELSE IF @EventExists = 1 AND @FundRaisingExists =0
			BEGIN
					SELECT 99999 AnythingPageListID
						   ,@RetailerListID retListID
						   ,9999 pageID
						   ,'Events' pageTitle
						   ,@RetailEventsIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder
					
					UNION ALL
					
					SELECT 99999 AnythingPageListID
							,@RetailerListID retListID
							,pageID
							,pageTitle
							,pageImage
							,PageLink
							,NULL pageTempLink	
						    ,0 SortOrder	
					FROM #AppsiteLogisticMap

					UNION ALL

					SELECT T.AnythingPageListID
							 , @RetailerListID retListID
							 , A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink	
							 , A.SortOrder SortOrder		 
					FROM #AnythingPages A
					INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle						
			END


			ELSE IF @EventExists = 0 AND @FundRaisingExists =1
			BEGIN
					SELECT 99999 AnythingPageListID
						   ,@RetailerListID retListID
						   ,9999 pageID
						   ,'Fundraisers' pageTitle
						   ,@RetailFundRaisingIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder	
					UNION ALL
					
					SELECT 99999 AnythingPageListID
							,@RetailerListID retListID
							,pageID
							,pageTitle
							,pageImage
							,PageLink
							,NULL pageTempLink	
						    ,0 SortOrder	
					FROM #AppsiteLogisticMap

					UNION ALL

					SELECT T.AnythingPageListID
							 , @RetailerListID retListID
							 , A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink	
							 , A.SortOrder SortOrder		 
					FROM #AnythingPages A
					INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle				
			END
			
			ELSE 
			BEGIN
					SELECT 99999 AnythingPageListID
							,@RetailerListID retListID
							,pageID
							,pageTitle
							,pageImage
							,PageLink
							,NULL pageTempLink	
						    ,0 SortOrder	
					FROM #AppsiteLogisticMap
					
					UNION ALL

					SELECT T.AnythingPageListID
							 , @RetailerListID retListID
							 , A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink
							 , A.SortOrder SortOrder			 
					FROM #AnythingPages A
					INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle	
			END

		
			--Check if this SP is called when scanned and update associated tables.
			IF ISNULL(@ScanTypeID, 0) <> 0
			BEGIN
				
				INSERT INTO HubCitiReportingDatabase..Scan(MainMenuID
														  , ScanTypeID
														  , Success
														  , ID
														  , CreatedDate)
												SELECT  @MainMenuID 
													  , @ScanType
													  , (CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
													  , pageID
													  , GETDATE()
												FROM #AnythingPages
														
			END 	

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcQRDisplayRetailerCreatedPages.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




















GO
