USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailLocationHotDealsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_HcRetailLocationHotDealsDisplay
Purpose					: To get the list of Hotdeals available.
Example					: usp_HcRetailLocationHotDealsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.4			12th Oct 2011	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailLocationHotDealsDisplay]
(
	  @UserID INT
	, @RetailID INT
	, @RetailLocationID INT
	, @LowerLimit int
	, @ScreenName varchar(50)
	, @HcHubCitiID Int  	
	
	
    --User Tracking inputs.
    , @MainMenuID int    
	
	--OutPut Variable	
	, @MaxCnt int output 
	, @NxtPageFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN
	
	BEGIN TRY
	
		BEGIN TRANSACTION
	
			 --To get Server Configuration
			 DECLARE @ManufConfig varchar(50) 
			 DECLARE @RetailerConfig varchar(50) 
			   
			 SELECT @ManufConfig=(SELECT ScreenContent  
								 FROM AppConfiguration   
								 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
				   ,@RetailerConfig=(SELECT ScreenContent  
									 FROM AppConfiguration   
									 WHERE ConfigurationType='Web Retailer Media Server Configuration')
			 FROM AppConfiguration  
		
			--To get the row count for pagination.
			 DECLARE @UpperLimit int 
			 SELECT @UpperLimit = @LowerLimit + ScreenContent 
			 FROM AppConfiguration 
			 WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1	
		      
			  Select Row_Num=IDENTITY(int,1,1)
					 ,PH.ProductHotDealID 
					 ,PH.HotDealName 
					 ,RL.City
					 ,Isnull(PH.Category, 'Others') categoryName
					 ,A.APIPartnerID 
					 ,A.APIPartnerName 
					 ,PH.Price 
					 ,PH.SalePrice 
					 ,PH.HotDealShortDescription 
					 ,HotDealImagePath = CASE WHEN WebsiteSourceFlag = 1 THEN
															CASE WHEN ManufacturerID IS NOT NULL 
															THEN @ManufConfig +CONVERT(VARCHAR(30),PH.ManufacturerID)+'/' +HotDealImagePath
															ELSE 
															   CASE WHEN PH.RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
															   THEN [HubCitiApp2_3_3].fn_HotDealImagePath(PH.ProductHotDealID)
															   ELSE @RetailerConfig + CONVERT(VARCHAR(30),PH.RetailID)+'/' +HotDealImagePath
															   END	 
															END
														ELSE HotDealImagePath
												   END
					 ,PH.HotDealURL 
					 ,PH.CategoryID	             	      
			  INTO #Product
			  FROM ProductHotDealRetailLocation PRL
			  INNER JOIN ProductHotDeal PH ON PRL.ProductHotDealID = PH.ProductHotDealID
			  INNER JOIN RetailLocation RL ON RL.RetailLocationID = PRL.RetailLocationID AND RL.Active=1
			  INNER JOIN HcLocationAssociation HL ON HL.Postalcode=RL.Postalcode AND HL.HcHubCitiID=@HcHubCitiID
			  INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
			  INNER JOIN APIPartner A ON A.APIPartnerID = PH.APIPartnerID
			  LEFT JOIN UserHotDealGallery UHG ON PH.ProductHotDealID = UHG.HotDealID
			  Where RL.RetailID = @RetailID 
			  AND RL.RetailLocationID = @RetailLocationID 
			  AND GETDATE() Between ISNULL(PH.HotDealStartDate, GETDATE() - 1) AND ISNULL(PH.HotDealEndDate, GETDATE()+1)
			  GROUP BY PH.ProductHotDealID 
					 ,PH.HotDealName 
					 ,RL.City
					 ,PH.Category
					 ,A.APIPartnerID 
					 ,A.APIPartnerName 
					 ,PH.Price 
					 ,PH.SalePrice 
					 ,PH.HotDealShortDescription 
					 ,PH.HotDealImagePath 
					 ,PH.HotDealURL 
					 ,PH.CategoryID
					 ,PH.NoOfHotDealsToIssue
					 , WebsiteSourceFlag
					 ,ManufacturerID
					 ,PH.RetailID
			  HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
					ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)	
			
			  --To capture max row number.
			  SELECT @MaxCnt = MAX(Row_Num) FROM #Product
			
			  --this flag is a indicator to enable "More" button in the UI. 
			  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
			  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			  SELECT  Row_Num  rowNumber
					, ProductHotDealID hotDealId
					, HotDealName hotDealName
					, City city
					, Isnull(categoryName, 'Others') categoryName
					, APIPartnerID apiPartnerId
					, APIPartnerName apiPartnerName
					, Price hDPrice
					, SalePrice hDSalePrice
					, HotDealShortDescription hDshortDescription
					, HotDealImagePath hotDealImagePath	
					, HotDealURL  hdURL
					--, Distance 	distance
					, CategoryID categoryId
			  INTO #HotDeals
			  FROM #Product
			  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
			  ORDER BY Row_Num
			  
			  --User Tracking section.
			  
			  --Capture the impression of the Hot deals.
			  CREATE TABLE #Temp(HotDealListID int
							   , ProductHotDealID int
							   , RetailerListID int)
			  
			  INSERT INTO HubCitiReportingDatabase..HotDealList(MainMenuID
															  , ProductHotDealID															  
															  , DateCreated)
											
				OUTPUT inserted.HotDealListID, inserted.ProductHotDealID INTO #Temp(HotDealListID, ProductHotDealID)
										
											SELECT @MainMenuID
												 , hotDealId												
												 , GETDATE()
											FROM #HotDeals H
											
			 --Display the hot deal list along with the primary key of the tracking table.
			 SELECT rowNumber
				  , T.HotDealListID
				  , T.RetailerListID retListID
				  , hotDealId
				  , hotDealName
				  , city
				  , categoryName
				  , apiPartnerId
				  , apiPartnerName
				  , hDPrice
				  , hDSalePrice
				  , hDshortDescription
				  , hotDealImagePath
				  , categoryId
			 FROM #Temp T
			 INNER JOIN #HotDeals H ON T.ProductHotDealID = H.hotDealId
			 
			--Confirmation of Success.
			SELECT @Status = 0										  
		COMMIT TRANSACTION		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcRetailLocationHotDealsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION; 
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
