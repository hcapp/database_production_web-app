USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserTrackingShareTypeDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcUserTrackingShareTypeDisplay 
Purpose               : To capure the deails of the prouct share.  
Example               : usp_UserTrackingRebateShare 
  
History  
Version    Date				Author				Change Description  
---------------------------------------------------------------   
1.0      17th March 2013	SPAN			Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserTrackingShareTypeDisplay]  
(    
    
 --OutPut Variable 
   @Status bit output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
        SELECT ShareTypeID shrTypID
			 , ShareTypeName shrTypNam
        FROM HubCitiReportingDatabase..ShareType	
		
		SET @Status=0			     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingShareTypeDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
   
   SET @Status=1 
  END;  
     
 END CATCH;  
END;











































GO
