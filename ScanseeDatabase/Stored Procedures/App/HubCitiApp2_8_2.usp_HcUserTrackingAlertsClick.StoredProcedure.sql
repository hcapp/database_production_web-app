USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserTrackingAlertsClick]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserTrackingAlertsClick
Purpose					: To display Alert Details.
Example					: usp_HcUserTrackingAlertsClick

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18/11/2013	    Mohith H R	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserTrackingAlertsClick]
(   
   
	--UserTracking  
      @AlertListID int     
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY			
		
			--User Tracking Section.
		
			--Capture the click of the alerts.			
		    UPDATE HubCitiReportingDatabase..AlertList  SET AlertClick  =  1 
			WHERE AlertListID  = @AlertListID 		
			
				
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiAlertsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
