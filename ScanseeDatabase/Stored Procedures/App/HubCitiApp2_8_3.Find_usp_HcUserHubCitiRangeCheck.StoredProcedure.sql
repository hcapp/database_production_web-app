USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck]
Purpose					: To check whether user is within HubCiti Range or not.
Example					: [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck]

History
Version		   Date			Author	 Change Description
--------------------------------------------------------------- 
1.0			02/11/2014	    SPAN	      1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck]
(
	--Input Variable
	  @Radius int
	, @HubCitiID int
	, @Latitude Decimal(18,6)    
	, @Longitude Decimal(18,6)  
	, @PostalCode VARCHAR(10)
	, @UserRadius bit = 0
	, @UserConfiguredPostalCode Varchar(10)
	  
	--Output Variable
	, @UserOutOfRange bit output
	, @Distance float output
	, @DefaultPostalCode varchar(10) output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @NearestPostalCode varchar(10)
				  , @RandomPostalCode varchar(10)

		    SELECT @PostalCode=PostalCode
			FROM GeoPosition 
			WHERE Postalcode = ISNULL(@PostalCode,@UserConfiguredPostalCode)
		
			--Derive the Latitude and Longitude in the absence of the input.
			IF @Latitude IS NULL AND @Longitude IS NULL
			BEGIN
				--Derive Latitude and Longitude using Postalcode
					SELECT @Latitude=Latitude
						  ,@Longitude=Longitude
					FROM GeoPosition 
					Where PostalCode = @PostalCode				
			END	
		
			--To fetch Random PostalCode when user has not configured PostalCode
			SELECT @RandomPostalCode = DefaultPostalCode
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID


			
			--To find nearest PostalCode to user
			SELECT G.PostalCode
				  ,Distance = ROUND((ACOS((SIN(G.Latitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(G.Latitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (G.Longitude / 57.2958))))*6371) * 0.6214 ,1,1)
			INTO #Temp
			FROM HcLocationAssociation LA
			INNER JOIN GeoPosition G ON LA.PostalCode = G.PostalCode AND LA.City = G.City AND LA.State = G.State
			WHERE LA.HcHubCitiID = @HubCitiID


			
			
			
			SELECT Distinct @NearestPostalCode = PostalCode
							,@Distance = Distance
			FROM #Temp
			WHERE Distance = (SELECT MIN(Distance) FROM #Temp)
			
			--To check whether user is within HubCiti or Not
			IF(ISNULL(@Distance, 0) < (@Radius/2)) AND @Latitude IS NOT NULL AND @Longitude IS NOT NULL
			BEGIN
				SELECT @UserOutOfRange = 0
					 , @DefaultPostalCode = NULL
			END

			ELSE
			BEGIN
				SELECT @UserOutOfRange = 1
				SELECT @DefaultPostalCode = ISNULL(@NearestPostalCode,@RandomPostalCode)
			END

			--Confirmation of Success.
		    SELECT @Status = 0
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			throw;
		END;
		 
	END CATCH;
END;














































GO
