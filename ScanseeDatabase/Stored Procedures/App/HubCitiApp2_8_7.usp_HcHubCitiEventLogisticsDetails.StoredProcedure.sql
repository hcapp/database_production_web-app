USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcHubCitiEventLogisticsDetails]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiEventLogisticsDetails
Purpose					: To display event logistic details.
Example					: usp_HcHubCitiEventLogisticsDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10 Apr 2015	    Mohith H R		1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcHubCitiEventLogisticsDetails]
(   
    --Input variable.	  
	  @HubCitiID int
	, @HcEventID int
	, @UserID int
  
	--Output Variable 
	, @EventLogisticMapURL Varchar(500) Output
	, @EventLogisticImgPath varchar(1000) output
	, @EventsIsOverLayFlag bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			DECLARE @Config VARCHAR(500)
			DECLARE @SSQRConfig VARCHAR(500)

			SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			SELECT @SSQRConfig=ScreenContent
			FROM AppConfiguration			
			WHERE ConfigurationType = 'QR Code Configuration'


			SET @EventLogisticMapURL = @SSQRConfig + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'EventLogisticMapPath') as varchar(10)) + '.htm?eventId=' + CAST(@HcEventID AS VARCHAR(10)) + '&hubcitiId=' + CAST(@HubCitiID AS VARCHAR(10))
			
			SELECT @EventLogisticImgPath = @Config + CAST(HcHubCitiID AS VARCHAR(100)) +'/events/' + CAST(HcEventID AS VARCHAR(100)) + '/'  +EventsLogisticImagePath
				  ,@EventsIsOverLayFlag = EventsIsOverLayFlag
			FROM HcEventsLogistic
			WHERE HcEventID = @HcEventID AND HcHubCitiID = @HubCitiID

			--SELECT DISTINCT HcEventsLogisticButtonID ButtonId
			--	  ,EL.HcEventID
			--	  ,EL.HcHubCitiID
		    SELECT DISTINCT ButtonName btnName
				  ,ButtonLink btnLink
			FROM HcEventsLogisticButtons EL
			INNER JOIN HcEvents E ON EL.HcEventID = E.HcEventID
			WHERE EL.HcEventID = @HcEventID AND EL.HcHubCitiID = @HubCitiID AND E.Active = 1 
			ORDER BY ButtonName 


						INSERT INTO HubCitiReportingDatabase..RetailerList(--MainMenuID
													         -- RetailID
													       -- , RetailLocationID,
													         DateCreated
															, HcEventID
													        )
			 
													   SELECT --@MainMenuID
															 -- RetailID
															--, RetailLocationID,
															 GETDATE()
															, @HcEventID
										
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiEventLogisticsDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;































GO
