USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserInfoDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcUserInfoDisplayDisplay]
Purpose					: To display the details of the hub citi user.
Example					: [usp_HcUserInfoDisplay] 

History
Version		     Date		  Author	 Change Description
--------------------------------------------------------------- 
1.0			16th sept 2013	  Pavan Sharma K	     Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcUserInfoDisplay]
(
	 @UserID INT
    
    --OutPut Variable
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
					
			SELECT H.FirstName
				 , H.LastName
				 , H.Email
				 , H.PostalCode
				 , H.PhoneNO phoneNum
				 , H.Gender
			   --, U.State
			   --, U.UniversityName univName
			FROM HcUser H
			LEFT JOIN HcUserUniversity HU ON H.HcUserID = HU.HcUserID
			LEFT JOIN University U ON HU.UniversityID = U.UniversityID
			WHERE H.HcUserID = @UserID
			        
			--Confirmation of Success
			SELECT @Status = 0
		    COMMIT TRANSACTION
		
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcUserInfoDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;















































GO
