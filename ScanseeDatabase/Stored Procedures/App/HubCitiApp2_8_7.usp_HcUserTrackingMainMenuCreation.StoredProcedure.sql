USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingMainMenuCreation]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_UserTrackingMainMenuCreaion  
Purpose				  : To capture the module details & the location details which the user is using.  
Example				  : usp_UserTrackingMainMenuCreaion  
  
History  
Version    Date          Author        Change         Description  
---------------------------------------------------------------   
1.0        18th Jan 2013 Dhananjaya TR Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingMainMenuCreation]  
(  
   @UserID INT
 , @HubCitiID INT
 , @MenuItemID INT
 , @BottomButtonID INT 
 , @RequestPlatformtype VARCHAR(100)
 --OutPut Variable  
 , @MainMenuID int output
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	BEGIN TRANSACTION
	
	DECLARE @IPhoneRequest INT
	
	SELECT @IPhoneRequest=RequestPlatformID
	FROM HubCitiReportingDatabase.dbo.RequestPlatforms 
	WHERE RequestPlatformtype=@RequestPlatformtype
	
         --Display Main Menu Details
	Insert into HubCitiReportingDatabase..MainMenu (HcUserID
													  ,HcHubCitiID
													  ,MenuItemID
													  ,PostalCode
													  ,BottomButtonID
													  ,DateCreated
													  ,RequestPlatformID)
												SELECT @UserID
													 , @HubCitiID
													 , @MenuItemID
													 , (SELECT PostalCode FROM HcUser WHERE HcUserID = @UserID)
													 , @BottomButtonID
													 , GETDATE()
													 , @IPhoneRequest
							
							SELECT @MainMenuID = SCOPE_IDENTITY()
		
		 
     
       --Confirmation of Success.
       SELECT @Status=0
	COMMIT TRANSACTION
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_UserTrackingMainMenuCreaion.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
   ROLLBACK TRANSACTION;
   --Confirmation of Failure.
       SELECT @Status=1 
  END;  
     
 END CATCH;  
END;














































GO
