USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_FindNearByBandEvent]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_FindNearByBandEvent  
Purpose     : To get near by retailers list.  
Example     : EXEC [HubCitiApp2_8_2].[usp_FindNearByBandEvent]   66,'',1, '12.94715', '77.57888','', 1, 2 ,NULL,NULL,NULL   
  
History  
Version  Date  Author   Change Description  
---------------------------------------------------------------   
  
---------------------------------------------------------------  
*/  
  
 CREATE PROCEDURE [HubCitiApp2_8_2].[usp_FindNearByBandEvent]  
(  
	@HubCitiID Int	
, @HcMenuItemID INT
, @UserID INT  
 , @Latitude decimal(18,6)  
 , @Longitude decimal(18,6)  
 , @PostalCode varchar(10)  
 , @Radius int 
 --User Tracking
 ,@MainMenuID int
    
 --Output Variable  
 , @NoRecordsMsg nvarchar(max) output
 , @Result int OUTPUT  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  	  DECLARE @ModuleName varchar(100)
	  
	  	select @ModuleName=MenuItemName  from HcMenuItem    where HcMenuItemID=@HcMenuItemID
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
  
	  DECLARE @RetailConfig varchar(50)    
 	  SELECT @RetailConfig=ScreenContent
	  FROM AppConfiguration 
	  WHERE ConfigurationType='Web Retailer Media Server Configuration'	 
	  
	  DECLARE @Config varchar(50) 
	  SELECT @Config=ScreenContent
	  FROM AppConfiguration 
	  WHERE ConfigurationType='App Media Server Configuration'
   
  --Latitude and longitude values are null, but Postal Code is passed.  
  IF (@Latitude IS NULL OR @Longitude IS NULL) AND @PostalCode IS NOT NULL  
  BEGIN  
   SELECT @Latitude = Latitude   
    , @Longitude = Longitude   
   FROM GeoPosition   
   WHERE PostalCode = @PostalCode   
  END  
    
  --Latitude, longitude and Postal Code values are not passed.  
  IF (@Latitude IS NULL OR @Longitude IS NULL) AND @PostalCode IS NULL  
  BEGIN  
   SELECT @PostalCode = PostalCode FROM Users WHERE UserID = @UserID  
   IF @PostalCode IS NOT NULL  
   BEGIN  
    SELECT @Latitude = Latitude, @Longitude = Longitude   
    FROM GeoPosition   
    WHERE PostalCode = @PostalCode  
   END  
   ELSE   
   BEGIN  
    SELECT @Result = -1  
   END  
     
  END  
    
    
    
  --To fetch User Preferred Radius.  
  IF @Radius IS NULL  
  BEGIN  
   SELECT @Radius = ISNULL(LocaleRadius, 5)   
   FROM UserPreference   
   WHERE UserID = @UserID   
  END  
    
  SELECT HcBandEventID
  INTO #RetailerLists 
  FROM   
  (  
   SELECT E.HcBandEventID   

    , Distance = (ACOS((SIN(L.Longitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(L.Longitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (L.Latitude / 57.2958))))*6371) * 0.6214  
   
   FROM HcBandEvents E    INNER JOIN HcBandEventLocation  L  ON E.HcBandEventID=L.HcBandEventID

  )Retailer   
 -- WHERE Distance <= ISNULL(@Radius, 5)

  
  --IF EXISTS(SELECT  *  FROM #RetailerLists)
  SELECT  BandEventTypeName as evtTypeName,BandEventTypeID  as evtTypeID from BandEventType  --where BandEventTypeName='Events Near By'
  --ELSE
 -- SELECT BandEventTypeName as evtTypeName,BandEventTypeID  as evtTypeID from BandEventType  where 1<>1


  IF NOT EXISTS (SELECT 1 FROM #RetailerLists)
  SET @NoRecordsMsg = 'No Records Found'
 
 --if @ModuleName='Band'
 ------exec  [HubCitiApp2_8_2].[usp_HcFunctionalityBottomButtonDisplay] 10,'Find',1,null,null,null

 --DECLARE 
 --@Config Varchar(500) 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

--SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
--			     , HM.HcHubCitiID 
--				 , BB.HcBottomButtonID AS bottomBtnID
--			     ,BLT.BottomButtonLinkTypeDisplayName as bottomBtnName
--				 , bottomBtnImg=@Config + BI.HcBottomButtonImageIcon
--				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(10 AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
--				 , BottomButtonLinkTypeID AS btnLinkTypeID
--				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = 10), BottomButtonLinkID) 
--				 , btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
--			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
--			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =10 ) = 1  THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
--INNER JOIN BusinessCategory C ON C.BusinessCategoryID =A.BusinessCategoryID 
--WHERE HcBottomButonID = BB.HcBottomButtonID) 
			                      	 
--			                       ELSE BottomButtonLinkTypeName END)					
--			FROM HcMenu HM
--			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID AND FBB.HcHubCitiID =10 
--			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
--			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
--			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = 'Find'
--			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 


      --  EXEC [HubCitiApp2_8_2].[Find_usp_HcFunctionalityBottomButtonDisplay] @HubCitiID, 'Band Events', @Status = @Result OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT
                                    
        
		
			SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			     , HM.HcHubCitiID 
				 , BB.HcBottomButtonID AS bottomBtnID
			     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
				 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
				 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@HubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
				 , BottomButtonLinkTypeID AS btnLinkTypeID
				 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @HubCitiID), BottomButtonLinkID) 
				 --, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
				, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID ) = 1  
											  THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			                                                                INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			                                                                INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			                                                                WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@HubCitiID)
			                       ELSE BottomButtonLinkTypeName END)
			--INTO #Temp1
			FROM HcMenu HM
			--INNER JOIN HcMenuItem MI ON MI.HCMenuID = HM.HCMenuID 
			INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID AND FBB.HcHubCitiID =@HubCitiID 
			INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = 'Band Events'
			LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
		--	WHERE LT.LinkTypeDisplayName = 'Band Events' --AND FBB.HcHubCitiID = @HubCitiID
			ORDER by btnLinkTypeID asc
		     
 
 END TRY 
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_ShoppingNearByRetailer.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
			values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())
  END;  
     
 END CATCH;  
END;  








GO
