USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcBandListPagination_bak]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcBandListPagination]
Purpose                 :  To get the list of near by Band for Find Module.
Example                 :  

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/17/2012       SPAN            Initial Version
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcBandListPagination_bak]
(

      --Input Parameter 
		 @Postalcode varchar(100)
	   , @UserConfiguredPostalCode Varchar(10)
       --, @CategoryName varchar(2000)
       --, @Latitude Decimal(18,6)
       --, @Longitude  Decimal(18,6)
       , @Radius int
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @HCHubCitiID Int
       , @HcMenuItemId Int
       , @HcBottomButtonId Int
       , @SearchKey Varchar(100)
       , @BandSubCategoryID Varchar(2000) 
       , @SortColumn varchar(50)
       , @SortOrder varchar(10)   
       , @HcCityID Varchar(3000)
    --   , @FilterID Varchar(1000)
    --   , @FilterValuesID Varchar(1000) 
	   --, @LocalSpecials bit 
	   --, @Interests Varchar(1000) 
	  , @groupBy Varchar(10)
	 
        
       --User tracking Inputs
       , @MainMenuID int
       
      --Output Variable
       , @UserOutOfRange bit output  
       , @DefaultPostalCode varchar(50) output             
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY      
                    SET NOCOUNT ON
                                      
                    DECLARE	  @CategoryID INT  
							, @RowCount int 
							, @Count INT = 1
							, @MaxCount INT 
							, @SQL VARCHAR(1000)                     
							, @RetSearch VARCHAR(1000)
							, @Config varchar(50)                     
							, @BandConfig varchar(50)
							, @UpperLimit int   
							, @Tomorrow DATETIME = GETDATE() + 1
							, @Yesterday DATETIME = GETDATE() - 1
							, @RowsPerPage int
							, @NearestPostalCode varchar(50)
							, @DistanceFromUser FLOAT
							, @UserLatitude float
							, @UserLongitude float 
							, @SpecialChars VARCHAR(1000)
							, @ModuleName varchar(100) = 'Band'
							, @RegionAppFlag Bit
							, @GuestLoginFlag BIT=0
							, @BandCategoryID Int
							--, @BandSubCategoryID Varchar(2000) 


					--Save Copy of input PostalCode 
					DECLARE @PostalCodeCopy VARCHAR(10)
					SET @PostalCodeCopy = @Postalcode

					--SearchKey implementation
					DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

					SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
							WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
							WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
							ELSE @SearchKey END)

					--SELECT @BandCategoryID=BandCategoryID 
					--FROM BandCategory 
					--WHERE BandCategoryName LIKE @CategoryName

					DECLARE @BandSubCategoryIDs Table (ID int identity(1,1), BusCatIDs int)
					
					INSERT INTO @BandSubCategoryIDs (BusCatIDs)
					SELECT Param
					FROM fn_SplitParam (@BandSubCategoryID,',')

					--DECLARE @Interestss Table (ID int identity(1,1), IntIDs int)
					--INSERT INTO @Interestss (IntIDs)
					--SELECT Param
					--FROM fn_SplitParam (@Interests,',')


				--Filter Implementation 
    --            IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
    --            BEGIN
    --                    SELECT Param FilterIDs
    --                    INTO #Filters
    --                    FROM fn_SplitParam(@FilterID,',') P
    --                    LEFT JOIN AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
    --                    WHERE AF.AdminFilterID IS NULL

    --                    SELECT Param FilterValues
    --                    INTO #FilterValues
    --                    FROM fn_SplitParam(@FilterValuesID,',')

    --                    SELECT DISTINCT BandID 
    --                    INTO #FilterRetaillocations
    --                    FROM(
    --                    SELECT RF.BandID                                    
    --                    FROM BandFilterAssociation RF
    --                    INNER JOIN #FilterValues F ON F.FilterValues =RF.AdminFilterValueID AND BandCategoryID =@BandCategoryID

    --                    UNION

    --                    SELECT RF.BandID                                    
    --                    FROM BandFilterAssociation RF
    --                    INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BandCategoryID =@BandCategoryID
    --                    )A

				--END

							SELECT Param CityID
							INTO #CityIDs
							FROM fn_SplitParam(@HcCityID,',')

				
				CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
                DECLARE @CityList TABLE(CityID Int,CityName Varchar(200),PostalCode varchar(100))                  

                IF(SELECT 1 from HcHubCiti H
                        INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                        AND H.HcHubCitiID =@HCHubCitiID AND AL.HcAppListName ='RegionApp')>0
                       BEGIN
									SET @RegionAppFlag = 1
										    
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID 
									UNION ALL
									SELECT  @HCHubCitiID AS HcHubCitiID
									)

								INSERT INTO @CityList(CityID,CityName,PostalCode)		
								SELECT DISTINCT LA.HcCityID 
												,LA.City	
												,PostalCode				
								FROM #RHubcitiList H
								INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID   						
								INNER JOIN #CityIDs C ON LA.HcCityID=C.CityID 
                            END
                    ELSE
                            BEGIN
									SET @RegionAppFlag =0
									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID  
	
								INSERT INTO @CityList(CityID,CityName,PostalCode)
								SELECT DISTINCT HcCityID 
											   ,City
											   ,PostalCode					
								FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID                                   
                            END


         

                DECLARE @Globalimage varchar(50)
                SELECT @Globalimage =ScreenContent 
                FROM AppConfiguration 
                WHERE ConfigurationType ='Image Not Found'

   --             SELECT @UserLatitude = @Latitude
   --                  , @UserLongitude = @Longitude


			--	IF (@UserLatitude IS NULL) 
   --             BEGIN
   --                     SELECT @UserLatitude = Latitude
   --                             , @UserLongitude = Longitude
   --                     FROM GeoPosition 
   --                     WHERE PostalCode = @PostalCode
   --             END
   --             --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
   --             IF (@UserLatitude IS NULL) 
   --             BEGIN
   --                     SELECT @UserLatitude = Latitude
   --                             , @UserLongitude = Longitude
   --                     FROM HcHubCiti A
   --                     INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
   --                     WHERE A.HcHubCitiID = @HCHubCitiID1
   --             END

                             


			----To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
   --             EXEC [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
   --             SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

			
			--	--SElect @PostalCode = DefaultPostalCode from hchubciti where hchubcitiid = @HCHubCitiID 
				

			--	--Set the Next page flag to 0 initially.
   --             SET @NxtPageFlag = 0

			--	--Derive the Latitude and Longitude in the absence of the input.
   --            IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
   --            BEGIN
   --                 SELECT @Latitude = Latitude
   --                         , @Longitude = Longitude
   --                 FROM GeoPosition 
   --                 WHERE PostalCode = ISNULL(@PostalCode,@PostalCodeCopy)                                                                              
   --             END  
				

                SELECT @Config=ScreenContent
                FROM AppConfiguration 
                WHERE ConfigurationType='App Media Server Configuration'

                SELECT @BandConfig=ScreenContent
                FROM AppConfiguration 
                WHERE ConfigurationType='Web Band Media Server Configuration'

                --To fetch all the duplicate Bands.
                --SELECT DISTINCT DuplicateBandID 
                --INTO #DuplicateRet
                --FROM Band 
                --WHERE DuplicateBandID IS NOT NULL

                --To get the row count for pagination.  
                SELECT @UpperLimit = @LowerLimit + 20 
                     , @RowsPerPage = ScreenContent
                FROM AppConfiguration   
                WHERE ScreenName = @ScreenName 
                AND ConfigurationType = 'Pagination'
                AND Active = 1   
                           
               
           
     	--				CREATE TABLE #BandCategory(BandCategoryID int , BandSubCategoryID int, BandCategoryName varchar(1000))
						--IF (@HcMenuItemId IS NOT NULL)
						--BEGIN 

						--	INSERT INTO #BandCategory(BandCategoryID,BandSubCategoryID,BandCategoryName) 
						--	SELECT DISTINCT F.BandCategoryID,BandSubCategoryID,BandCategoryName               
						--	FROM BandCategory BC
						--	INNER JOIN HcMenuItemBandCategories F ON BC.BandCategoryID = F.BandCategoryID
						--	WHERE --BC.BandCategoryName = @CategoryName AND 
						--	HcMenuItemID = @HcMenuItemId
						--END
						--ELSe IF (@HcBottomButtonID IS NOT NULL)
						--BEGIN
			
						--	INSERT INTO #BandCategory(BandCategoryID,BandSubCategoryID,BandCategoryName) 
						--	SELECT DISTINCT F.BandCategoryID,BandSubCategoryID,BandCategoryName               
						--	FROM BandCategory BC
						--	INNER JOIN HcBottomButtonBandCategories F ON BC.BandCategoryID = F.BandCategoryID 
						--	WHERE --BC.BandCategoryName = @CategoryName AND 
						--	HcBottomButonID = @HcBottomButtonID
						--END
        
						--SELECT @CategoryID = BandCategoryID
						--FROM #BandCategory   
						
						
						--select * from @CityList                    
                                           
							CREATE TABLE #Band (RowNum INT IDENTITY(1, 1),BandCategoryID INT,BandCategoryName VARCHAR(1000),BandID INT,BandName VARCHAR(1000),BandImagePath VARCHAR(1000))

							IF (@HcMenuItemId is not null)
							BEGIN

							INSERT INTO #Band (BandCategoryID,BandCategoryName,BandID,BandName,BandImagePath)

							SELECT BandCategoryID,BandCategoryName,BandID,BandName,BandImagePath FROM



							(
							SELECT DISTINCT   B.BandCategoryID 
										, B.BandCategoryName 
										, BB.BandID
										, BB.BandName
										, CASE WHEN BandImagePath IS NOT NULL THEN @BandConfig+CONVERT(VARCHAR(30),BB.BandID)+'/'+ BandImagePath 
											   ELSE 'http://66.228.143.28:8080/Images/band/default-logo.jpg' END  BandImagePath
							FROM BandCategory B 		
							INNER JOIN HcMenuItemBandCategories C ON B.BandCategoryID = C.BandCategoryID 
							INNER JOIN BandCategoryAssociation RB ON C.BandCategoryID = RB.BandCategoryID	
							INNER JOIN Band BB ON BB.BandID = RB.BandID		
							INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = BB.PostalCode AND HcHubCitiID = @hcHubCitiID 
							INNER JOIN @CityList CN ON CN.CityID = HC.HcCityID
							INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
							INNER JOIN HcMenuItem M ON C.HcMenuItemID = M.HcMenuItemID 
							WHERE M.HcMenuItemID = @HcMenuItemID AND RB.BandSubCategoryID IS NULL
							AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (BandName LIKE '%'+@SearchKey+'%' OR BandCategoryName LIKE '%'+@SearchKey+'%'))
							OR (@SearchKey IS NULL))
							AND (@BandSubCategoryID IS NULL OR RB.BandCategoryID IN (SELECT BusCatIDs FROM @BandSubCategoryIDs))
							AND  BB.BandActive=1
							


							UNION ALL

							SELECT DISTINCT   B.BandCategoryID 
										, B.BandCategoryName 
										, BB.BandID
										, BB.BandName
										, CASE WHEN BandImagePath IS NOT NULL THEN @BandConfig+CONVERT(VARCHAR(30),BB.BandID)+'/'+ BandImagePath 
											   ELSE 'http://66.228.143.28:8080/Images/band/default-logo.jpg' END  BandImagePath
							FROM BandCategory B 		
							INNER JOIN HcMenuItemBandCategories C ON B.BandCategoryID = C.BandCategoryID 
							INNER JOIN BandCategoryAssociation RB ON C.BandCategoryID = RB.BandCategoryID AND RB.BandSubCategoryID = C.BandSubCategoryID	
							INNER JOIN Band BB ON BB.BandID = RB.BandID		
							INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = BB.PostalCode AND HcHubCitiID = @hcHubCitiID 
							INNER JOIN @CityList CN ON CN.CityID = HC.HcCityID
							INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
							INNER JOIN HcMenuItem M ON C.HcMenuItemID = M.HcMenuItemID 
							WHERE M.HcMenuItemID = @HcMenuItemID AND RB.BandSubCategoryID IS NOT NULL
							AND  BB.BandActive=1
							AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (BandName LIKE '%'+@SearchKey+'%' OR BandCategoryName LIKE '%'+@SearchKey+'%'))
							OR (@SearchKey IS NULL))
							AND (@BandSubCategoryID IS NULL OR RB.BandCategoryID IN (SELECT BusCatIDs FROM @BandSubCategoryIDs))

							) MenuItem

							ORDER BY BandName

							END

							IF (@HcBottomButtonId is not null)
							BEGIN

							INSERT INTO #Band (BandCategoryID,BandCategoryName,BandID,BandName,BandImagePath)

							SELECT BandCategoryID,BandCategoryName,BandID,BandName,BandImagePath FROM

							(
							SELECT DISTINCT   B.BandCategoryID 
										, B.BandCategoryName 
										, BB.BandID
										, BB.BandName
											, CASE WHEN BandImagePath IS NOT NULL THEN @BandConfig+CONVERT(VARCHAR(30),BB.BandID)+'/'+ BandImagePath 
											   ELSE 'https://app.scansee.net/Images/band/banner-default-one.jpg' END  BandImagePath
							FROM BandCategory B 		
							INNER JOIN HcBottomButtonBandCategories C ON B.BandCategoryID = C.BandCategoryID 
							INNER JOIN BandCategoryAssociation RB ON C.BandCategoryID = RB.BandCategoryID	
							INNER JOIN Band BB ON BB.BandID = RB.BandID		
							INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = BB.PostalCode AND HcHubCitiID = @hcHubCitiID 
							INNER JOIN @CityList CN ON CN.CityID = HC.HcCityID
							INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
							INNER JOIN HcBottomButton M ON C.HcBottomButonID = M.HcBottomButtonID 
							WHERE M.HcBottomButtonID = @HcBottomButtonId AND RB.BandSubCategoryID IS NULL
							AND  BB.BandActive=1


							UNION ALL

							SELECT DISTINCT   B.BandCategoryID 
										, B.BandCategoryName 
										, BB.BandID
										, BB.BandName
										,  CASE WHEN BandImagePath IS NOT NULL THEN @BandConfig+CONVERT(VARCHAR(30),BB.BandID)+'/'+ BandImagePath 
											   ELSE 'https://app.scansee.net/Images/band/banner-default-one.jpg' END  BandImagePath
							FROM BandCategory B 		
							INNER JOIN HcMenuItemBandCategories C ON B.BandCategoryID = C.BandCategoryID 
							INNER JOIN BandCategoryAssociation RB ON C.BandCategoryID = RB.BandCategoryID AND RB.BandSubCategoryID = C.BandSubCategoryID	
							INNER JOIN Band BB ON BB.BandID = RB.BandID		
							INNER JOIN HcLocationAssociation HC ON  HC.PostalCode = BB.PostalCode AND HcHubCitiID = @hcHubCitiID 
							INNER JOIN @CityList CN ON CN.CityID = HC.HcCityID
							INNER JOIN #RHubcitiList H ON H.HcHubcitiID =HC.HcHubcitiID
							INNER JOIN HcBottomButton M ON C.HcBottomButonID = M.HcBottomButtonID 
							WHERE M.HcBottomButtonID = @HcBottomButtonId AND RB.BandSubCategoryID IS NOT NULL
							AND  BB.BandActive=1
							) BottomButton
							ORDER BY BandName
							END
							


							select BandId,BandName,BandCategoryName,BandCategoryID,BandImagePath
							into #bandlist1
							from #Band
							group by BandId,BandName,BandCategoryName,BandCategoryID,BandImagePath
							order by BandName,BandCategoryName
							
							--SELECT * FROM #bandlist1


							CREATE TABLE bandlist
							(
								BandId INT
								,BandName VARCHAR(5000)
								,BandImagePath VARCHAR(1000)
								,BandCategoryIDs VARCHAR(5000)
								,BandCategoryName VARCHAR(5000)
								
								
							)
								

							IF (@groupBy = 'name')
							 BEGIN
								INSERT INTO bandlist (BandId,BandName,BandImagePath,BandCategoryIDs,BandCategoryName)
								select distinct  BandId,BandName,BandImagePath
									,BandCategoryIDs = 	STUFF((SELECT ', ' + CAST(BandCategoryID AS VARCHAR(500))
														FROM #bandlist1 t
														WHERE b.BandId = t.BandId
														FOR XML PATH('')),1,1,'')
									,BandCategoryName = REPLACE(STUFF((SELECT ', ' + BandCategoryName
														FROM #bandlist1 t
														WHERE b.BandId = t.BandId
														FOR XML PATH('')),1,1,''),'&amp;','&')
								FROM #bandlist1 b
								group by BandId,BandName,BandImagePath,BandCategoryID,BandCategoryName
							 END
							ELSE 
							 BEGIN
								INSERT INTO bandlist (BandId,BandName,BandImagePath,BandCategoryIDs,BandCategoryName)
								SELECT BandId,BandName,BandImagePath,BandCategoryID BandCategoryIDs,BandCategoryName
								FROM #bandlist1
								group by BandId,BandName,BandImagePath,BandCategoryID,BandCategoryName
							 END

                                   ---End Find Imp

								  -- SELECT * FROM bandlist

								    SELECT  IDENTITY(int, 1, 1) AS RowNumber   
                                    , R.BandID BandId    
                                    , BandName BandName
                                    --, R.RetailLocationID retailLocationID
                                    --, Address1 Bandaddress1
                                    --, Address2 Bandaddress2
                                    --, Address3 Bandaddress3
                                    --, Address4 Bandaddress4
                                    --, City City
                                    --, State State
                                    --, PostalCode PostalCode
                                    --, retLatitude
                                    --, retLongitude    
                                   -- , BandImagePath logoImagePath     
                                    --, Distance
                                    --, DistanceActual                         
                                    --, S.BannerAdImagePath bannerAdImagePath    
                                    --, B.RibbonAdImagePath ribbonAdImagePath    
                                    --, B.RibbonAdURL ribbonAdURL    
                                    --, B.RetailLocationAdvertisementID advertisementID  
                                    --, S.SplashAdID splashAdID
                                    --, SaleFlag = CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
                                    --, HcBusinessSubCategoryID 
                                    --, BusinessSubCategoryName 
									,BandCategoryName 
									,BandCategoryIDs
									,BandImagePath
                        INTO #Bands    
                        FROM bandlist R
     --                  -- LEFT JOIN #RetailItemsonSale1 SS ON SS.BandID =R.BandID AND R.RetailLocationID =SS.RetailLocationID 
     --                  -- LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @BandConfig+CONVERT(VARCHAR(30),R.BandID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
     --                                                                                                                                                                   --ELSE SplashAdImagePath
     --                                                                                                                                                   --END
     --                               --        , SplashAdID  = ASP.AdvertisementSplashID
     --                               --        , R.RetailLocationID 
     --                               --FROM #Retail R
     --                               --INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
     --                               --INNER JOIN RetailLocation RL ON RL.RetailLocationID=RS.RetailLocationID
     --                               --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1   
     --                               --INNER JOIN HcBandAssociation RLC ON RLC.HcHubCitiID =@HCHubCitiID1 AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
     --                               --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode --AND HL.HcHubCitiID=@HCHubCitiID1 
     --                               --INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
     --                               --INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)
     --                               --INNER JOIN HcBandAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
     --                               --INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
     --                                                                                                                   --AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)
     --                   --          ) S    ON R.RetailLocationID = S.RetailLocationID
     --                   --LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @BandConfig+CONVERT(VARCHAR(30),R.BandID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
     --                   --                                                                                                                                ELSE AB.BannerAdImagePath
     --                   --                                                                                                                    END 
     --                   --                , RibbonAdURL = AB.BannerAdURL
     --                   --                , RetailLocationAdvertisementID = AB.AdvertisementBannerID
     --                   --                , R.RetailLocationID 
     --                   --            FROM #Retail R
     --                   --                                INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
     --                   --                                INNER JOIN RetailLocation RL ON RL.RetailLocationID=RB.RetailLocationID
     --                   --                                --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
     --                   --                                --INNER JOIN HcBandAssociation RLC ON RLC.HcHubCitiID =@HCHubCitiID1 AND RLC.RetailLocationID =RL.RetailLocationID AND Associated =1 
     --                                                   --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode --AND HL.HcHubCitiID=@HCHubCitiID1 
     --                                                   --INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
     --                                                   ----INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)
     --                                                   --INNER JOIN HcBandAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
     --                                       --            INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
     --                                       --            WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)) B
     --                                       --ON R.RetailLocationID = B.RetailLocationID
       --                 select 

						 --BandCategoryID 
							--			, BandCategoryName 
							--			, BandID
							--			, BandName
							--			, BandImagePath  
						
						 --from #Band

						 --group by YYZ
						 -- BandCategoryID 
							--			, BandCategoryName 
							--			, BandID
							--			, BandName
							--			, BandImagePath  
						
						 --(ISNULL(@LocalSpecials,0) = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))
						 ORDER BY CASE --WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
                                        WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'BandName' THEN CAST(BandName AS SQL_VARIANT)
                                       -- WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
                            END ASC  ,BandName


							 --select * from #Band
							 --order by 
        --                    CASE --WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
        --                                WHEN @SortOrder = 'DESC' AND @SortColumn = 'atoz' THEN CAST(BandName AS SQL_VARIANT) 
        --                              --  WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
        --                    END DESC ,BandCategoryName , BandName
					
					--To capture max row number.  
                    SELECT @MaxCnt = count(1) FROM #Bands
                                  
     --               --this flag is a indicator to enable "More" button in the UI.   
     --               --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

				
										               
						SELECT   rowNumber
									, BandCategoryIDs catIds
									, BandCategoryName catName
                                    , BandId bandID
                                    , BandName bandName
                                    --, retailLocationID
                                    --, Bandaddress1
                                    --, Bandaddress2
                                    --, Bandaddress3
                                    --, Bandaddress4
                                    --, City
                                    --, State
                                    --, PostalCode
                                    --, retLatitude
                                    --, retLongitude    
                                    , BandImagePath bandImgPath  
                                    --, Distance
                                    --, DistanceActual                         
                                    --,  bannerAdImagePath    
                                    --, ribbonAdImagePath    
                                    --, ribbonAdURL    
                                    --, advertisementID  
                                    --, splashAdID
                                    --, SaleFlag 
                                    --, HcBusinessSubCategoryID 
                                    --, BusinessSubCategoryName 
                        --INTO #BandList 
						FROM #Bands
						WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
                       -- ORDER BY rowNumber
     --                   --OFFSET (@LowerLimit) ROWS
     --                   --FETCH NEXT @UpperLimit ROWS ONLY      
                                                
                                         
                        --select * from #BandList
                                  
     --                      --Display the Band along with the keys generated in the Tracking table.
     --                      --SELECT Distinct rowNumber
     --                      --                            , T.BandListID retListID
     --                      --                            , BandId
     --                      --                            , BandName
     --                      --                            --, T.RetailLocationID
     --                      --                            --, Bandaddress1
     --                      --                            --, Bandaddress2
     --                      --                            --, Bandaddress3
     --                      --                            --, Bandaddress4
     --                      --                            , City
     --                      --                            , State
     --                      --                            , PostalCode                                   
     --                      --                            --, retLatitude
     --                      --                            --, retLongitude
     --                      --                            --, Distance = ISNULL(DistanceActual, Distance)
     --                      --                            , logoImagePath
     --                      --                            --, bannerAdImagePath
     --                      --                            --, ribbonAdImagePath
     --                      --                            --, ribbonAdURL
     --                      --                            --, advertisementID
     --                      --                            --, splashAdID
     --                      --                            --, SaleFlag   
     --                      --                            --, HcBusinessSubCategoryID subCatId
     --                      --                            --, BusinessSubCategoryName subCatName                         
     --                      --FROM #Temp T
     --                      --INNER JOIN #BandList R ON  R.RetailLocationID = T.RetailLocationID     
					--	   --INNER JOIN #BandList R ON  R.rowNumber = T.Rowno
                           
     --                      --To display bottom buttons 
	 
	 	--		SELECT DISTINCT FBB.HcFunctionalityBottomButtonID 
			--     , HM.HcHubCitiID 
			--	 , BB.HcBottomButtonID AS bottomBtnID
			--     , ISNULL(BottomButtonName,BLT.BottomButtonLinkTypeDisplayName) AS bottomBtnName
			--	 , bottomBtnImg=CASE WHEN BottomButtonImage_On IS NOT NULL THEN @Config + CAST(@hcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_On WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon  END
			--	 , bottomBtnImgOff=CASE WHEN BottomButtonImage_Off IS NOT NULL THEN @Config + CAST(@hcHubCitiID AS VARCHAR(1000))+'/'+BottomButtonImage_Off WHEN BB.HcBottomButtonImageIconID IS NOT NULL THEN @Config + BI.HcBottomButtonImageIcon_Off  END 
			--	 , BottomButtonLinkTypeID AS btnLinkTypeID
			--	 , btnLinkID = IIF(BLT.BottomButtonLinkTypeName = 'Filters' OR BLT.BottomButtonLinkTypeName ='City Experience', (SELECT HcCityExperienceID FROM HcCityExperience WHERE HcHubCitiID = @hcHubCitiID), BottomButtonLinkID) 
			--	 --, HM.BottomButtonLinkTypeName AS btnLinkTypeName	
			--	, btnLinkTypeName = (CASE WHEN (SELECT COUNT(DISTINCT A.BusinessCategoryID) FROM HcBottomButtonFindRetailerBusinessCategories A 
			--                                  INNER JOIN RetailerBusinessCategory RB ON RB.BusinessCategoryID =A.BusinessCategoryID 
			--                                  WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@hcHubCitiID ) = 1  
			--								  THEN 'FindSingleCategory-'+(SELECT DISTINCT BusinessCategoryName FROM HcBottomButtonFindRetailerBusinessCategories A 
			--                                                                INNER JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID 
			--                                                                INNER JOIN BusinessCategory C ON C.BusinessCategoryID =B.BusinessCategoryID 
			--                                                                WHERE HcBottomButonID = BB.HcBottomButtonID AND HM.HcHubCitiID =@hcHubCitiID)
			--                       ELSE BottomButtonLinkTypeName END)
			----INTO #Temp1
			--FROM HcMenu HM
			----INNER JOIN HcMenuItem MI ON MI.HCMenuID = HM.HCMenuID 
			--INNER JOIN HcFunctionalityBottomButton FBB ON HM.HcHubCitiID = FBB.HcHubCitiID AND FBB.HcHubCitiID =@hcHubCitiID 
			--INNER JOIN HcBottomButton BB ON FBB.HcBottomButtonID = BB.HcBottomButtonID
			--INNER JOIN HcBottomButtonLinkType BLT ON BLT.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
			--INNER JOIN HcLinkType LT ON FBB.HcFunctionalityID = LT.HcLinkTypeID AND LT.LinkTypeDisplayName = 'deals'
			--LEFT JOIN HcBottomButtonImageIcons BI ON BI.HcBottomButtonImageIconID =BB.HcBottomButtonImageIconID 
			----WHERE LT.LinkTypeDisplayName = @ModuleName --AND FBB.HcHubCitiID = @hcHubCitiID
			--ORDER by HcFunctionalityBottomButtonID                                               

        EXEC [HubCitiApp2_8_3].[Find_usp_HcFunctionalityBottomButtonDisplay] @HCHubCitiID, @ModuleName, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT
                                    
          DROP TABLE bandlist                        
            
       END TRY
            
       BEGIN CATCH
      
       --Check whether the Transaction is uncommitable.
       IF @@ERROR <> 0
       BEGIN

               SELEct ERROR_LINE()
                     PRINT 'Error occured in Stored Procedure usp_HcFindBandListPagination.'                            
                     --Execute retrieval of Error info.
                     EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                     PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
       END;
            
       END CATCH;
       END;
       


































GO
