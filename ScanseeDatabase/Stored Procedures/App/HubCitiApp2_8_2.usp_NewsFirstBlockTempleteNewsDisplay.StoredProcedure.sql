USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_NewsFirstBlockTempleteNewsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [HubCitiApp2_5].[usp_NewsFirstScrollingNewsDisplay]
Purpose					: To display news as per Scrolling templete
Example					: [HubCitiApp2_5].[usp_NewsFirstScrollingNewsDisplay]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26 May 2016		Sagar Byali		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_NewsFirstBlockTempleteNewsDisplay]
(	
	  @HubCitiID int 
	, @LowerLimit INT
	, @ScreenName VARCHAR(1000)
	
	, @HcUserID INT
	, @DateCheck varchar(1000)		
	, @LinkID int
	, @Level int
			
	--Output Variable 
	, @bannerImg VARCHAR(1000) Output
	, @weatherurl varchar(1000) Output
	, @homeImgPath VARCHAR(1000) Output
	, @bkImgPath VARCHAR(1000) Output
	, @titleBkGrdColor VARCHAR(1000) Output
	, @titleTxtColor VARCHAR(1000) Output
	--, @MaxCnt int Output
	--, @NxtPageFlag bit Output
	, @TemplateChanged int output
	, @TemplateName varchar(100) output
	--, @LowerLimitFlag int output
	, @ModifiedDate varchar(100) output
	--, @SubPage varchar(100) output

	, @HamburgerImagePath varchar(1000) output
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
        DECLARE @thumbnailposition varchar(1000), @newscategoryimage varchar(1000)
		DECLARE @ServerConfig VARCHAR(500),@Config varchar(1000)
		DECLARE @TempID int, @ExistTempID int, @NewsFlag int
		declare @master bigint  , @NewsImage varchar(max)

				
		DECLARE @DefaultHamburgerIcon VARCHAR(1000)
		select @DefaultHamburgerIcon = ScreenContent +  'images/hamburger.png' 
		from AppConfiguration 
		where ScreenName = 'Base_URL'

		SELECT @NewsImage = ScreenContent + CONVERT(VARCHAR,@HubCitiID)+ '/'
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT    @newscategoryimage = @NewsImage + NewsCategoryImage
				, @bannerImg = @NewsImage + NewsBannerImage
				, @weatherurl =  WeatherURL 
				, @thumbnailposition = NewsThumbnailPosition
		FROM HcHubCiti
		WHERE HcHubCitiID = @HubCitiID 

											------------- Custom Navigation Bar changes -------------------------------

		SELECT @Config = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT @ServerConfig = ScreenContent FROM AppConfiguration WHERE ConfigurationType= 'Hubciti Media Server Configuration'
			
		SELECT	@homeImgPath = 	CASE WHEN HU.homeIconName IS NULL THEN @ServerConfig+'customhome.png' ELSE  @Config +CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.homeIconName END
		,@bkImgPath = 	CASE WHEN HU.backButtonIconName IS NULL THEN @ServerConfig+'customback.png' ELSE @Config + CONVERT(VARCHAR(100),@HubCitiID)+'/'+HU.backButtonIconName END
		,@titleBkGrdColor =  ISNULL( HU.backGroundColor ,'#000000')
		,@titleTxtColor =  ISNULL(HU.titleColor ,'#ffffff')
		,@HamburgerImagePath = ISNULL(@NewsImage + NewsHamburgerIcon,@DefaultHamburgerIcon)
		FROM HcHubCiti HC
		LEFT JOIN HcMenuCustomUI HU ON HC.HcHubCitiID = HU.HubCitiId	
		LEFT JOIN HcBottomButtonTypes HB ON HU.HcBottomButtonTypeID = HB.HcBottomButtonTypeId			
		WHERE HcHubCitiID = @HubCitiID AND (SmallLogo IS NOT NULL)					 
			
			-------------------------------------------------------------------------
  
	SET @ModifiedDate=replace(@ModifiedDate,'  ',' ')
	SET @DateCheck=replace(@DateCheck,'  ',' ')
	set  @ModifiedDate=convert(datetime,@ModifiedDate)
	set  @DateCheck=convert(datetime,@DateCheck)

    
		
	  if  isnull(@Level,1)<>1
         BEGIN
		    if not exists(SELECT 1 FROM HcNewsFirstTempFlag H where HcHubCitiID = @HubCitiID  and isnull(Level,1)=0 and linkid=@LinkID) and isnull(@Level,1)<>1 
			begin
				 insert into HcNewsFirstTempFlag(HcHubCitiID,level,linkid) 
				 select @HubCitiID,0,@LinkID
			end
		 END
		  if not exists(SELECT 1 FROM HcNewsFirstTempFlag H where HcHubCitiID = @HubCitiID  and isnull(Level,1)=1)  and isnull(@Level,1)=1 
         BEGIN
				 insert into HcNewsFirstTempFlag(HcHubCitiID,level) 
				 select @HubCitiID,1
		 END

			 update   HcNewsFirstTempFlag set level=1  where level is null and HcHubCitiID = @HubCitiID

		 if isnull(@Level,1)=1
		 begin
		  SELECT top 1 @tempid = HcTemplateID FROM HcMenu H where HcHubCitiID = @HubCitiID and Level=1

				 if exists(SELECT 1 FROM HcMenu H where HcHubCitiID = @HubCitiID and IsAssociateNews=1 and Level=1)
				 BEGIN 
						 SET @NewsFlag=1
						 update HcNewsFirstTempFlag set NewsFlag=1,level=1,linkid=@LinkID where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=1
				 END
				 else
				 BEGIN
						SET @NewsFlag=0
						update HcNewsFirstTempFlag set NewsFlag=0,level=1,linkid=@LinkID where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=1
				 END
		 end
		 else
		    BEGIN
			 SELECT top 1 @tempid = HcTemplateID FROM HcMenu H where HcHubCitiID = @HubCitiID and  HcMenuID=@LinkID
		      if exists(SELECT 1 FROM HcMenu H where HcHubCitiID = @HubCitiID and IsAssociateNews=1 and HcMenuID=@LinkID)
		  		 BEGIN 
						 SET @NewsFlag=1
						 update HcNewsFirstTempFlag set NewsFlag=1,level=0 where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=0
				 END
				 else
				 BEGIN
						SET @NewsFlag=0
						update HcNewsFirstTempFlag set NewsFlag=0,level=0 where HcHubCitiID=@HubCitiID and NewsFlag is null and isnull(Level,1)=0
				 END
            end

			 
		 update HcNewsFirstTempFlag  
		 set TemplateID=@tempid  
		 where TemplateID is null and HcHubCitiID=@HubCitiID
		 and isnull(level,1)=case when @Level=1 then '1' else 0 end
		 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 

		select top 1 @Existtempid = TemplateID  
		from HcNewsFirstTempFlag  
		where HcHubCitiID=@HubCitiID
		and isnull(level,1)=case when @Level=1 then '1' else 0 end
		 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 

	  IF ( @NEWSflag=0)
		BEGIN
		
				update HCNewsFirstTempFlag 
				set NewsFlag=@NEWSflag,TemplateID=@tempid ,linkid=@LinkID
				where HcHubCitiID=@HubCitiID and level=case when @Level=1 then '1' else 0 end
				 and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end 
				
				 SET @TemplateChanged = 1
				
		          if isnull(@Level,1)=1
					begin
							SELECT @TemplateName = isnull(T.TemplateName,0)
							,@ModifiedDate = convert(datetime,H.DateCreated)
							FROM HcTemplate  T 
							INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
							WHERE HcHubCitiID = @HubCitiID AND Level=1       
					end
					else
					begin
							SELECT @TemplateName = isnull(T.TemplateName,0)
							,@ModifiedDate = convert(datetime,H.DateCreated)
							FROM HcTemplate  T 
							INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
							WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

					end

				
				SELECT  @Status = 0
				 
	    end

		ELSE

	    begin
		
		  SET @LinkID =IIF(@LinkID IS NULL OR @LinkID=0 ,Null,@LinkID)
		
			
			DECLARE @MenuName varchar(1000), @hcmenuid int 
			
			SELECT @MenuName = MenuName 
			FROM HcMenu
			WHERE HcMenuID = @LinkID AND ISNULL(@LinkID, 0) <> 0
			OR ISNULL(@LinkID, 0) = 0 AND Level = 1	

			IF @Level = 1 
			BEGIN
			select  @HcMenuID = HcMenuID
			FROM HCmenu where Level = 1 AND HcHubCitiID = @HubCitiID AND IsAssociateNews = 1
			END


			 -------------------------------------------------
	
			DECLARE @UpperLimit VARCHAR(1000)
				  , @RowsPerPage varchar(1000)
				  , @Exists BIT
				  , @BookMarkExists BIT

			--To get the row count for pagination.  
            SELECT @UpperLimit = @LowerLimit + ScreenContent 
                 , @RowsPerPage = ScreenContent
            FROM AppConfiguration   
            WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1	


		create table #TEMP(parCatId int, parCatName varchar(1000),subCatFlag bit,SortOrder INT,NonFeedNewsLink varchar(1000))--,backButtonColor varchar(1000))

		IF EXISTS (SELECT 1 FROM HcUserNewsFirstBookmarkedCategories WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID )
		AND NOT EXISTS(select 1  from  HcUser  where username='WelcomeScanSeeGuest'  and hcuserid=@HcuserID)
		BEGIN 
			
			--select 'a'

			INSERT INTO #TEMP
			--TO DISPLAY THE USER BOOKMARKED CATEGORIES
			SELECT DISTINCT cat.NewscategoryID parCatId
						  , NewscategoryDisplayValue parCatName
						  , case when NewsSubCategoryID is not null then '1' else '0' end
						  , cat.SortOrder
						  , ns.NewsFeedURL
						 --- , ns.backButtonColor
			FROM HcUserNewsFirstBookmarkedCategories  cat 
			INNER join newsfirstsettings ns on ns.NewsCategoryID=cat.NewscategoryID
			left join NewsFirstSubCategorySettings nss on nss.NewsCategoryID = ns.NewsCategoryID and nss.HcHubCitiID = cat.HchubcitiID
			WHERE cat.HcUserID=@HcuserID AND cat.HcHubcitiId=@HubcitiID  AND ns.IsNewsFeed != 0 
			
			UNION
			
			SELECT DISTINCT null parCatId
						  , ns.NewsLinkCategoryName parCatName
						  ,'0' 
						  , cat.SortOrder
						  , ns.NewsFeedURL
						 -- , ns.backButtonColor
			FROM HcUserNewsFirstBookmarkedCategories  cat 
			INNER join newsfirstsettings ns on ns.NewsLinkCategoryName=cat.NewsLinkCategoryName and ns.HcHubCitiID = cat.HchubcitiID 
			WHERE cat.HcUserID=@HcuserID AND cat.HcHubcitiId=@HubcitiID AND ns.IsNewsFeed = 0

				
		 END
		ELSE 
		 BEGIN
		 
			INSERT INTO  #TEMP
			--TO DISPLAY DEFAULT BOOKMARK CATEGORIES		
			SELECT DISTINCT def.NewscategoryID parCatId
						  , NewscategoryDisplayValue parCatName
						  , case when NewsSubCategoryID is not null then '1' else '0' end 
						  , def.SortOrder
						  , ns.NewsFeedURL
						 -- , ns.backButtonColor
			FROM HcDefaultNewsFirstBookmarkedCategories def 
			inner join newsfirstsettings ns on ns.NewsCategoryID=def.NewscategoryID and ns.HcHubCitiID = def.HchubcitiID 
			LEFT JOIN NewsFirstSubCategorySettings nss on nss.NewsCategoryID = ns.NewsCategoryID and def.NewsCategoryID = nss.NewsCategoryID --and nss.HcHubCitiID = def.HchubcitiID
			where def.HcHubcitiId=@HubcitiID  AND ns.IsNewsFeed != 0 

			union all

			SELECT DISTINCT null parCatId
						  , ns.NewsLinkCategoryName parCatName			 
						  , '0' 
						  , def.SortOrder
						  , ns.NewsFeedURL
						 -- , ns.backButtonColor
			FROM HcDefaultNewsFirstBookmarkedCategories def 
			inner join newsfirstsettings ns on ns.NewsLinkCategoryName=def.NewsLinkCategoryName and ns.HcHubCitiID = def.HchubcitiID 
			where def.HcHubcitiId=@HubcitiID AND ns.IsNewsFeed = 0 

		 END


		--SELECT * FROM #TEMP where parCatId = 62

       IF @Level <> 1
		 SELECT @newscategoryimage = NewsCategoryImage from hcmenu where HcMenuID = @hcmenuid

                             	--To fetch LatestNews---
						SELECT   Newstype
								,HcHubCitiID
								,max(datecreated) as MaxDate
                     	INTO #2DaysNews 
                        FROM RssNewsFirstFeedNews WHERE Title is not null
                        GROUP BY Newstype,HcHubCitiID
                      

			SELECT R.* 
			INTO #HubCitiNewss
			FROM RssNewsFirstFeedNews R 
			INNER JOIN #2DaysNews T on T.Newstype=R.Newstype AND T.HcHubCitiID=R.HcHubCitiID AND DATEDIFF(Day,R.DateCreated,T.MaxDate) <= 15


			



			SELECT  *
			INTO #HubCitiNews
			FROM #HubCitiNewss
			WHERE HcHubCitiID = @HubCitiID and title is not null order by RssNewsFirstFeedNewsID 
			


		; with cte as(
		 SELECT row_number() over (partition by newstype ORDER BY CASE WHEN PublishedDate is null THEN 1  
																			  WHEN PublishedDate is not null THEN 1 END, PublishedDate desc, CAST(PublishedTime AS time) desc )  rn, ImagePath, NewsType, HcHubCitiID, PublishedDate, Title, RssNewsFirstFeedNewsID
		 from #HubCitiNews
		 )

		 		

		 select * into #temppp from cte where rn = 1 

		 	update #temppp
			set PublishedDate = null
			where left(replace(getdate(),'  ,',','),11) = replace(left(replace(publisheddate,'  ,',','),12),',','')

	
		 select distinct parCatId catID
			 , parCatName catName
			 , subCatFlag 
			 , catImgPath = case when ImagePath is NULL OR ImagePath = ' ' THEN @newscategoryimage ELSE ImagePath END
			 , FS.NewsCategoryFontColor cattxtColor
			 , Fs.NewsCategoryColor catColor
			 , T.SortOrder
			 , null nonfeedlink
			 , fs.backButtonColor backButtonColor
		 
		 from #TEMP T
		 INNER JOIN NewsFirstCategory FN ON FN.NewsCategoryID = T.parCatId
		 inner JOIN NewsFirstSettings FS ON FS.NewsCategoryID = FN.NewsCategoryID AND FS.HcHubCitiID = @HubCitiID
		 inner JOIN #temppp NN ON NN.NewsType = FN.NewsCategoryName
		 WHERE FN.Active = 1 AND FN.NewsCategoryName IN (select NewsType from #temppp)

		 union

		  select distinct null catID
			 , parCatName catName
			 , subCatFlag 
			 , catImgPath = @newscategoryimage
			 , FS.NewsCategoryFontColor cattxtColor
			 , Fs.NewsCategoryColor catColor
			 , T.SortOrder
			,T.NonFeedNewsLink nonfeedlink
			,fs.backButtonColor backButtonColor
		 
		 from #TEMP T

		 inner JOIN NewsFirstSettings FS ON FS.NewsLinkCategoryName = t.parCatName AND FS.HcHubCitiID = @HubCitiID
		 and IsNewsFeed = 0
	





		 order by T.SortOrder
	
                      
   end
				--Temlete changed implementation----START------
				
if isnull(@Level,1)=1

begin
			IF EXISTS(SELECT 1 FROM HcMenu H
						INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
						WHERE left(convert(datetime,h.DateCreated),19)  = left(convert(datetime,@DateCheck),19) AND HcHubCitiID = @HubCitiID 
						and Level=1) or @DateCheck  is null
			BEGIN
						SELECT @TemplateChanged = 0 
			END
			ELSE
			BEGIN
					SELECT  @TemplateChanged = 1
            END

end
else

begin
			IF EXISTS(SELECT 1 FROM HcMenu H
						INNER JOIN HcMenuItem HM ON H.HcMenuID = HM.HcMenuID	
						WHERE left(convert(datetime,h.DateCreated),19)  = left(convert(datetime,@DateCheck),19) AND HcHubCitiID = @HubCitiID 
						and h.HcMenuID=@LinkID) or @DateCheck  is null
			BEGIN
						SELECT @TemplateChanged = 0 
			END
			ELSE
			BEGIN
					SELECT  @TemplateChanged = 1
            END

end


	if isnull(@Level,1)=1
	begin
			SELECT @TemplateName = isnull(T.TemplateName,0)
					,@ModifiedDate = convert(datetime,H.DateCreated)
			FROM HcTemplate  T 
			INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
			WHERE HcHubCitiID = @HubCitiID AND Level=1       
	end
	else
	begin
	SELECT @TemplateName = isnull(T.TemplateName,0)
					,@ModifiedDate = convert(datetime,H.DateCreated)
			FROM HcTemplate  T 
			INNER JOIN HcMenu H ON T.HcTemplateID=H.HcTemplateID 
			WHERE HcHubCitiID = @HubCitiID AND h.HcMenuID=@LinkID

	end

	update HCNewsFirstTempFlag 
	set NewsFlag=@NEWSflag,TemplateID=@tempid ,linkid=@LinkID
	where HcHubCitiID=@HubCitiID and level=case when @Level=1 then '1' else 0 end
	and isnull(linkid,0)=case when @Level=1 then 0 else @LinkID end
			 
		SET @Status=0	

		
		select top 1 @master=HcTemplateID  from HcTemplate  where  TemplateName='News Tile Template'
		IF @DateCheck IS  NULL
		set @TemplateChanged = 0
			
			if (@Existtempid<>@tempid  or @tempid<>@master )
			SET @TemplateChanged = 1
			 
		  
	  					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN

		throw;
			
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;









GO
