USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_DisplaySingleNewsFirstSideNavigation]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_DisplaySingleNewsFirstSideNavigation]
Purpose					: To display list of News categories on a Single Side Navigation.
Example					: [usp_DisplaySingleNewsFirstSideNavigation]

History
Version		   Date			 Author					Change Description
-------------------------------------------------------------------------------------------
1.0			26 May 2016	     Bindu T A			[usp_DisplaySingleNewsFirstSideNavigation]
----------------------------------------------------------------------------------------------
*/

--exec [HubCitiApp2_8_1].[usp_DisplayNewsFirstSideNavigation] 5139,2217,12645,null,null,null,null,null
--exec [HubCitiApp2_8_1].[usp_DisplayNewsFirstSideNavigation_dev] null,0,2113,12701,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_DisplaySingleNewsFirstSideNavigation]
(   

	--Input variable
	  @LinkID INT,
	  @LevelID INT,
	  @HubcitiID INT 
	 ,@HcuserID INT
	--Output Variable 
	 ,@BookMark Varchar(100) output
	 ,@Section Varchar(100) output
     ,@Status int output
	 ,@ErrorNumber int output
	 ,@ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		 --------------------------------Side Navigation List--------------------------

		IF @LinkID IS NULL OR  @LinkID=0
	    	SELECT @LinkID = (SELECT TOP 1 HCMenuID FROM HcMenu WHERE  HcHubcitiId=@HubcitiID AND IsDefaultHubCitiSideMenu=1  )
		

		UPDATE a set NewsSideNavigationID=b.HcMenuItemID  from HcUserNewsFirstSingleSideNavigation a 
		INNER JOIN HcMenuItem b on b.MenuItemName=a.NewsSideNavigationDisplayValue
		INNER JOIN  HcMenu  on HcMenu.HcMenuID=b.HcMenuID
		WHERE a.Flag=1 and a.NewsSideNavigationID<>b.HcMenuItemID
		AND   a.HchubcitiID=@HubcitiID and HcMenu.HchubcitiID=@HubcitiID
		AND	 HcMenu.HcMenuID=@LinkID

		DECLARE @IsSideNavigationChanged BIT
		SELECT @IsSideNavigationChanged =  IsSideNavigationChanged FROM HcUserNewsFirstSingleSideNavigation WHERE HcHubCitiID = @HubCitiID AND HcUserID = @HcuserID

		IF  @IsSideNavigationChanged=1
		 BEGIN
			DELETE FROM HcUserNewsFirstSingleSideNavigation WHERE HchubcitiID = @HubcitiID AND HcUserID = @HcuserID
		 END

		DECLARE @UCount INT
		DECLARE @CatCount INT
		DECLARE @DCount INT

		SELECT NS.NewsCategoryID,NS.NewsCategoryColor,NS.HcHubCitiID
		INTO #Color
		FROM HcDefaultNewsFirstBookmarkedCategories D
		INNER JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID
		WHERE D.HchubcitiID = @HubcitiID 

		SELECT @UCount = Count(1) FROM HcUserNewsFirstSingleSideNavigation WHERE Hcuserid= @HcuserID AND HchubcitiID = @HubCitiID AND Flag= 2
		SELECT @DCount = COUNT(1) FROM NewsFirstSettings WHERE HchubcitiID = @HubCitiID 
		--FROM HcDefaultNewsFirstBookmarkedCategories D 
		--LEFT JOIN newsfirstsettings NS ON D.HchubcitiID = NS.HcHubCitiID AND D.NewsCategoryID= NS.NewsCategoryID AND NS.HcHubCitiID=@HubCitiID
		--WHERE D.HchubcitiID = @HubCitiID 

		SELECT @CatCount = Count(1) FROM NewsFirstSettings WHERE HcHubCitiID = @HubcitiID

		IF @Ucount = 0
			SELECT @Ucount = NULL


		DECLARE @RegionApp INT
		 IF EXISTS (SELECT 1 FROM hchubciti WHERE HcHubCitiID = @HubcitiID AND Hcapplistid = 2 )
			SELECT @RegionApp = 1
		 ELSE 
			SELECT @RegionApp = 0

		DECLARE @GuestUser int
		DECLARE @CityPrefNotVisitedUser bit

		 CREATE TABLE #Temp 
		 (
		 parCatId INT,
		 parCatName VARCHAR(500),
		 subCatId INT,
		 subCatName VARCHAR(500),
		 isHubFunctn INT,
		 isAdded INT,
		 sortOrder INT,
		 isBkMark INT,
		 catColor VARCHAR(100),
		 isSubCategory BIT
		 )

		 CREATE TABLE #TempFN
		 (
		 parCatId INT,
		 parCatName VARCHAR(500),
		 subCatId INT,
		 subCatName VARCHAR(500),
		 isHubFunctn INT,
		 isAdded INT,
		 sortOrder INT,
		 isBkMark INT,
		 catColor VARCHAR(100),
		 isSubCategory BIT
		 )

		 CREATE TABLE #TempFNSECTIONS
		 (
		 parCatId INT,
		 parCatName VARCHAR(500),
		 subCatId INT,
		 subCatName VARCHAR(500),
		 isHubFunctn INT,
		 isAdded INT,
		 sortOrder INT,
		 isBkMark INT,
		 catColor VARCHAR(100),
		 isSubCategory BIT
		 )
		 
		 
		 IF EXISTS (SELECT 1 FROM HcUserNewsFirstSingleSideNavigation WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID  )
		 AND NOT EXISTS(select 1  from  HcUser  where username in('WelcomeScanSeeGuest','GuestLogin')  and hcuserid=@HcuserID) AND @RegionApp= 1
		 BEGIN
		-- SELECT 'A'
			SELECT @GuestUser = U.HcUserID
					FROM HcUser U
					INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
					WHERE UserName = 'GuestLogin'
					AND DA.HcHubCitiID = @HubCitiID
			
			IF NOT EXISTS (SELECT 1 FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HubCitiID AND HcUserID = @HcuserID and HcCityID is not null)
			BEGIN
				SET @CityPrefNotVisitedUser = 1
			END
			ELSE
			BEGIN
				SET @CityPrefNotVisitedUser = 0
			END
			
		--SELECT @CityPrefNotVisitedUser,@CatCount,@DCount

		IF @CityPrefNotVisitedUser=0 AND ISNULL(@GuestUser,0) <> @HcuserID
		BEGIN
		
		    INSERT INTO #TempFN
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,
			1  isHubFunctn,
			1 isAdded,UN.SortOrder sortOrder,1 isBkMark ,NULL  catColor,0 isSubCategory 
			from HcRegionAppMenuItemCityAssociation  RMC
			INNER JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND RMC.HcHubcitiId=UC.HcHubcitiId
			INNER JOIN  HcMenuItem HM on HM.HcMenuItemID= RMC.HcMenuItemID
			INNER JOIN HcUserNewsFirstSingleSideNavigation UN  on HM.HcMenuItemID=UN.NewsSideNavigationID
			where RMC.HCHubcitiID = @HubcitiID  AND UN.HcHubcitiId=@HubcitiID  and Flag= 1 
			and HM.HcMenuID = @LinkID AND UC.HcUserID=@HcuserID
			AND  UN.HcUserID=@HcuserID

			INSERT INTO #TempFNSECTIONS   
			SELECT DISTINCT M.HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			1 isHubFunctn,0 isAdded,(ISNULL(@CatCount,@DCount) + Position)  sortOrder,
			case when ((UN.NewsSideNavigationID IS NULL) OR (M.HcMenuItemID IS NULL)) then '0' else '1' end isBkMark,NULL  catColor, 0 isSubCategory
			from HcMenuItem M
			inner JOIN HcRegionAppMenuItemCityAssociation RMC on M.HcMenuItemID= RMC.HcMenuItemID and M.HcMenuID = @LinkID AND RMC.HcHubcitiID = @HubcitiID
			left JOIN HcUsersPreferredCityAssociation UC  ON RMC.HcCityID=UC.HcCityID AND  RMC.HCHubcitiID = @HubcitiID  AND RMC.HcHubcitiId=UC.HcHubcitiId  AND UC.HcUserID=@HcuserID
			LEFT JOIN HcUserNewsFirstSingleSideNavigation UN  on M.HcMenuItemID=UN.NewsSideNavigationID AND UN.HcUserID=@HcuserID AND UN.HcHubcitiId=@HubcitiID AND  UN.Flag= 1  
			
		END
		ELSE
		BEGIN
			
		    INSERT INTO #TempFN  
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,
			1  isHubFunctn,
			1 isAdded,UN.SortOrder sortOrder,1 isBkMark ,NULL  catColor,0 isSubCategory 
			from HcUserNewsFirstSingleSideNavigation UN 
	        INNER JOIN HcMenuItem HM on HM.HcMenuItemID=UN.NewsSideNavigationID
			WHERE UN.HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 1 and HM.HcMenuID = @LinkID
			AND  UN.HcUserID=@HcuserID

			INSERT INTO #TempFNSECTIONS   
			SELECT DISTINCT HM.HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName
			,1 isHubFunctn,0 isAdded,(ISNULL(@CatCount,@DCount) + Position)  sortOrder,
			case when ((UN.NewsSideNavigationID IS NULL) OR (HM.HcMenuItemID IS NULL)) then '0' else '1' end isBkMark,NULL  catColor, 0 isSubCategory
			from HcMenuItem HM
		    Left JOIN HcUserNewsFirstSingleSideNavigation UN   on HM.HcMenuItemID=UN.NewsSideNavigationID AND  UN.HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID
			LEFT JOIN HcRegionAppMenuItemCityAssociation N   on HM.HcMenuItemID = N.HcMenuItemID  AND N.HcHubcitiId=@HubcitiID
			WHERE  HM.HcMenuID = @LinkID
		
		END
			
		INSERT INTO #Temp  
			
			SELECT DISTINCT *  FROM #TempFN
			
			UNION

			--TO DISPLAY USER SELECTED CATEGORIES
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,0  isHubFunctn,
			1 isAdded,UN.Sortorder sortOrder,1 isBkMark, NS.NewsCategoryColor  catColor,0 isSubCategory
			FROM HcUserNewsFirstSingleSideNavigation UN
			INNER join newsfirstsettings  ns on ns.NewsCategoryID=UN.NewsSideNavigationID  AND NS.HcHubCitiID = @HubcitiID
			WHERE HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 2

			UNION 

			--TO DISPLAY ALL FUNCTIONALITIES

			SELECT DISTINCT * FROM #TempFNSECTIONS
				
			UNION
			
			--TO DISPLAY ALL CATEGORIES
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,row_number() over (order by ns.Sortorder) sortOrder,
			case when hcus.NewsSideNavigationID is null  then '0' else '1' end isBkMark
			 , ns.NewsCategoryColor catColor,0 isSubCategory
			FROM NewsFirstCategory cat 
			inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID
			LEFT  join HcUserNewsFirstSingleSideNavigation hcus on hcus.NewsSideNavigationID=cat.NewsCategoryID
			and HcUserID= @HcuserID  AND hcus.HcHubcitiId=@HubcitiID
			LEFT JOIN HcMenuItem HM ON Hm.MenuItemName = cat.NewsCategoryName AND Hm.HcMenuID=@LinkID 
			WHERE  NS.HcHubcitiId=@HubcitiID --AND HM.HcMenuItemID IS NULL
			ORDER BY sortOrder
     
		END

		ELSE IF EXISTS (SELECT 1 FROM HcUserNewsFirstSingleSideNavigation WHERE HcUserID= @HcuserID AND HcHubcitiId=@HubcitiID  )
		AND NOT EXISTS(select 1  from  HcUser  where username in('WelcomeScanSeeGuest','GuestLogin')  and hcuserid=@HcuserID) AND @RegionApp= 0
		BEGIN 
		--SELECT 'B'

		 INSERT INTO #Temp  
		  
			--TO DISPLAY USER SELECTED FUNCTIONALITIES
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,
			1  isHubFunctn,
			1 isAdded,UN.SortOrder sortOrder,1 isBkMark ,NULL catColor,0 isSubCategory
			FROM HcUserNewsFirstSingleSideNavigation UN 
			--LEFT JOIN NewsFirstSettings  NS on ns.NewsCategoryID=UN.NewsSideNavigationID AND NS.HcHubCitiID = 2233
			INNER JOIN HcMenuItem HM on HM.HcMenuItemID=UN.NewsSideNavigationID
			WHERE HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 1 and HM.HcMenuID = @LinkID

			UNION

			--TO DISPLAY USER SELECTED CATEGORIES
			SELECT DISTINCT NewsSideNavigationID parCatId, NewsSideNavigationDisplayValue parCatName,1 subCatId,'All' subCatName,0  isHubFunctn,
			1 isAdded,UN.Sortorder sortOrder,1 isBkMark, NS.NewsCategoryColor  catColor,0 isSubCategory
			FROM HcUserNewsFirstSingleSideNavigation UN
			INNER join newsfirstsettings  ns on ns.NewsCategoryID=UN.NewsSideNavigationID  AND NS.HcHubCitiID = @HubcitiID
			WHERE HcUserID= @HcuserID AND UN.HcHubcitiId=@HubcitiID and Flag= 2

			
			UNION 

			--TO DISPLAY ALL FUNCTIONALITIES
			SELECT DISTINCT HcMenuItem.HcMenuItemID parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			1 isHubFunctn,0 isAdded,(ISNULL(@CatCount,@DCount) + Position) sortOrder,
			case when HN.NewsSideNavigationID is null then '0' else '1' end isBkMark,NULL  catColor, 0 isSubCategory
			FROM HcMenuItem 
			LEFT  JOIN HcUserNewsFirstSingleSideNavigation HN on HN.NewsSideNavigationID=HcMenuItem.HcMenuItemID AND HcUserID = @HcuserID
			WHERE HcMenuID = @LinkID --AND CreatedUserID= @HcuserID 
						
			UNION
			
			--TO DISPLAY ALL CATEGORIES
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,row_number() over (order by ns.Sortorder ) sortOrder,
			case when hcus.NewsSideNavigationID is null then '0' else '1' end isBkMark , ns.NewsCategoryColor catColor,0 isSubCategory
			FROM NewsFirstCategory cat 
			INNER JOIN newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID
			LEFT  join HcUserNewsFirstSingleSideNavigation hcus
			 on hcus.NewsSideNavigationID=cat.NewsCategoryID
			 and HcUserID= @HcuserID  AND hcus.HcHubcitiId=@HubcitiID
			 where  ns.HcHubcitiId=@HubcitiID
			
		 END
		
		ELSE 
		 BEGIN
		 --SELECT 'C'

		 SELECT  DISTINCT NewsCategoryID  
		 INTO #tempdef  
		 FROM HcDefaultNewsFirstBookmarkedCategories  
		 WHERE HchubcitiID=@HubcitiID

		SELECT NewsCategoryID  INTO #other FROM NewsFirstCategory
		EXCEPT
		SELECT NewsCategoryID  FROM #tempdef

		--SELECT * FROM #other
		
		DECLARE @DefCount INT
		SELECT @defCount = COUNT(1 )
		FROM HcMenuItem 
		WHERE HcMenuID = @LinkID


		INSERT INTO #Temp  

			--TO DISPLAY ALL DEFAULT HUBCITI FUNCTIONALITIES
			SELECT DISTINCT CASE WHEN NS.NewsCategoryID IS  NULL THEN Hm.HcMenuItemID  ELSE  NS.NewsCategoryID END
			parCatId, MenuItemName parCatName,1 subCatId,'All' subCatName,
			CASE WHEN NS.NewsCategoryID IS  NULL THEN 1 ELSE 0 END isHubFunctn,
			1 isAdded,Position sortOrder,1 isBkMark, '#000000' catColor, 0 isSubCategory
			FROM HcMenuItem HM
			LEFT JOIN NewsFirstCategory NS ON LTRIM(RTRIM(HM.MenuItemName))= LTRIM(RTRIM(NS.NewsCategoryDisplayValue)) AND Active= 1
			WHERE HcMenuID  = @LinkID

			--UNION

			----TO DISPLAY ALL DEFAULT HUBCITI Sub Categories 
			--SELECT DISTINCT CASE WHEN NS.NewsCategoryID IS  NULL THEN Hm.HcMenuItemID  ELSE  DS.NewsSubCategoryID END
			--parCatId, DS.NewsSubCategoryDisplayValue parCatName,1 subCatId,'All' subCatName,
			--CASE WHEN NS.NewsCategoryID IS  NULL THEN 1 ELSE 0 END isHubFunctn,1 isAdded,Position sortOrder,1 isBkMark, '#000000' catColor, 1 isSubCategory
			--FROM HcMenuItem HM
			--INNER JOIN HcDefaultNewsFirstBookmarkedCategories D ON D.NewsCategoryDisplayValue= HM.MenuItemName
			--INNER JOIN HcDefaultNewsFirstBookmarkedSubCategories DS ON DS.NewsFirstCategoryID= D.NewsCategoryID 
			--LEFT JOIN NewsFirstCategory NS ON LTRIM(RTRIM(HM.MenuItemName))= LTRIM(RTRIM(NS.NewsCategoryDisplayValue)) AND NS.Active= 1
			--WHERE HcMenuID = @LinkID
									
			UNION
		
			--TO DISPLAY ALL DEFAULT CATEGORIES
			SELECT DISTINCT def.NewsCategoryID parCatId, NewsCategoryDisplayValue parCatName,1 subCatId,'All' subCatName,0 isHubFunctn,
			1 isAdded,(@defCount+def.Sortorder) sortOrder,1 isBkMark , n.NewsCategoryColor catColor,0 isSubCategory
			FROM HcDefaultNewsFirstBookmarkedCategories  def 
			INNER join #Color n on n.NewsCategoryID=def.NewsCategoryID  AND n.HcHubCitiID = def.HchubcitiID
			INNER join newsfirstsettings NS on NS.NewsCategoryID=def.NewscategoryID and NS.HcHubcitiId=@HubcitiID
			LEFT JOIN HcMenuItem HM ON HM.MenuItemName = DEF.NewsCategoryDisplayValue AND HM.HcMenuID= @LinkID  
			WHERE HM.HcMenuItemID IS NULL AND
			 def.HchubcitiID = @HubcitiID
			--ORDER BY SortOrder

			UNION

			----TO DISPLAY ALL DEFAULT SUB-CATEGORIES
			--SELECT DISTINCT def.NewsCategoryID parCatId, NewsCategoryDisplayValue parCatName,ISNULL(DS.NewsSubCategoryID,1) subCatId,
			--ISNULL(DS.NewsSubCategoryDisplayValue,'All') subCatName,0 isHubFunctn, 
			--1 isAdded,(@defCount+def.Sortorder) sortOrder,1 isBkMark , n.NewsCategoryColor catColor,
			--1 isSubCategory
			--FROM HcDefaultNewsFirstBookmarkedCategories  def 
			--INNER JOIN HcDefaultNewsFirstBookmarkedSubCategories DS ON DEf.NewsCategoryID = DS.NewsFirstCategoryID
			--INNER join #Color n on n.NewsCategoryID=def.NewsCategoryID  AND n.HcHubCitiID = def.HchubcitiID
		 --   inner join newsfirstsettings  ns on ns.NewsCategoryID=def.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			--LEFT JOIN HcMenuItem HM ON HM.MenuItemName = DEF.NewsCategoryDisplayValue AND HM.HcMenuID= @LinkID  
			--WHERE ds.HchubcitiID = @HubcitiID and def.HchubcitiID = @HubcitiID AND HM.HcMenuItemID IS NULL
			----ORDER BY SortOrder
					
			--UNION 

			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,ns.Sortorder sortOrder,
			CASE WHEN HM.HcMenuItemID IS NULL  THEN 0 ELSE 1 END isBkMark , ns.NewsCategoryColor catColor, 0 isSubCategory
			FROM NewsFirstCategory cat  
			INNER JOIN #other  on #other.NewsCategoryID=cat.NewsCategoryID
			INNER JOIN newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID AND Cat.Active=1
			LEFT JOIN  HcDefaultNewsFirstBookmarkedCategories  def ON def.NewsCategoryID= NS.NewsCategoryID AND def.HchubcitiID= @HubcitiID
			LEFT JOIN hcMenuITEM HM ON Hm.MenuItemName = cat.NewsCategoryName AND HM.HcMenuID= @LinkID
			--ORDER BY SortOrder

			UNION

			--TO DISPLAY ALL OTHER CATEGORIES
			
			SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,1 subCatId,'All' subCatName,
			0 isHubFunctn,0 isAdded,ns.Sortorder sortOrder,
			CASE WHEN def.NewsCategoryID IS NULL THEN 0 ELSE 1 END isBkMark , n.NewsCategoryColor catColor, 0 isSubCategory
			FROM NewsFirstCategory cat 
			inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID AND ns.HchubcitiID = @HubcitiID
			inner join HcDefaultNewsFirstBookmarkedCategories def on def.NewscategoryID=cat.NewsCategoryID
			inner join #Color n on n.NewsCategoryID=def.NewsCategoryID  
			and n.NewsCategoryID=cat.NewsCategoryID
			LEFT JOIN HcMenuItem HM ON HM.MenuItemName = NS.NewsCategoryColor AND HM.HcMenuID= @LinkID  
			WHERE HM.HcMenuItemID IS NULL
			and def.HchubcitiID = n.HchubcitiID -- where cat.NewsCategoryID=1
			--ORDER BY SortOrder

			--UNION

			----TO DISPLAY ALL OTHER SUB_CATEGORIES
			--SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,ISNULL(DS.NewsSubCategoryID,1) subCatId,ISNULL(DS.NewsSubCategoryDisplayValue,'All') subCatName,
			--0 isHubFunctn,0 isAdded,NS.Sortorder sortOrder,
			--CASE WHEN def.NewsCategoryID IS NULL THEN 0 ELSE 1 END isBkMark ,n.NewsCategoryColor catColor,
			--1 isSubCategory
			--FROM NewsFirstCategory cat 
			--inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			--inner join HcDefaultNewsFirstBookmarkedCategories def  on def.NewscategoryID=cat.NewsCategoryID
			----inner join newsfirstsettings  ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			--inner JOIN HcDefaultNewsFirstBookmarkedSubCategories DS ON DEf.NewsCategoryID = DS.NewsFirstCategoryID
			--inner join #Color n on n.NewsCategoryID=def.NewsCategoryID AND n.HcHubCitiID = def.HchubcitiID
			--and n.NewsCategoryID=cat.NewsCategoryID
			--LEFT JOIN hcMenuItem HM ON HM.MenuItemName = cat.NewsCategoryName AND HM.HcMenuID= @LinkID
			--and def.HchubcitiID = @HubcitiID  and DS.HchubcitiID = @HubcitiID
			--WHERE HM.HcMenuItemID IS NULL
			----ORDER BY SortOrder
			
			--UNION

			--SELECT DISTINCT cat.NewsCategoryID,NewsCategoryName,ISNULL(DS.NewsSubCategoryID,1) subCatId,ISNULL(DS.NewsSubCategoryDisplayValue,'All') subCatName,
			--0 isHubFunctn,0 isAdded,ns.Sortorder sortOrder,
			--CASE WHEN DS.HcDefaultNewsFirstBookmarkedSubCategoriesID IS NULL THEN 0 ELSE 1 END isBkMark ,ns.NewsCategoryColor catColor,
			--CASE WHEN DS.HcDefaultNewsFirstBookmarkedSubCategoriesID IS NULL THEN 0 ELSE 1 END isSubCategory
			--FROM NewsFirstCategory cat 
			--INNER JOIN newsfirstsettings ns on ns.NewsCategoryID=cat.NewsCategoryID  AND ns.HchubcitiID = @HubcitiID
			--INNER JOIN #other  on #other.NewsCategoryID=cat.NewsCategoryID
			--LEFT JOIN  HcDefaultNewsFirstBookmarkedSubCategories DS ON #other.NewsCategoryID = DS.NewsFirstCategoryID
			--LEFT JOIN hcMenuItem HM ON HM.MenuItemName = cat.NewsCategoryName AND HM.HcMenuID= @LinkID
			--AND DS.HchubcitiID = @HubcitiID
			--WHERE HM.HcMenuItemID IS NULL
			ORDER BY isAdded,sortOrder,isHubFunctn
			
		 END


		 SELECT @Bookmark = 'Favorites' ,@Section='Other Sections'
		 ----------------------------------------------------------------------------------


		 SELECT * FROM #temp ORDER BY isAdded,sortOrder
		 
		--Confirmation of Success
		SELECT @Status = 0	      	
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_DisplaySingleNewsFirstSideNavigation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
