USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcShareAppsite]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_HcShareAppsite
Purpose                 : 
Example                 : usp_HcShareAppsite

History
Version           Date              Author    Change Description
--------------------------------------------------------------- 
1.0				  18th Nov 2013     SPAN        1.0
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcShareAppsite]
(
        
		@UserID int
	  , @RetailID int
	  , @RetailLocationID int
	  --, @ShareType VARCHAR(10)
	  , @HubCitiId int
    
      --Output Variable 
      , @ShareText varchar(500) output
	  , @HubCitiName varchar(100) output
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
      
		BEGIN TRANSACTION
      
            DECLARE @RetailConfig varchar(50)
			DECLARE @Config VARCHAR(100)              
			DECLARE @Configuration VARCHAR(100)
            DECLARE @QRType INT     
          
          --Retrieve the server configuration.
            SELECT @Config = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType LIKE 'QR Code Configuration'
            AND Active = 1 
             
            --To get server Configuration.
            SELECT @RetailConfig= ScreenContent  
            FROM AppConfiguration   
            WHERE ConfigurationType='Web Retailer Media Server Configuration' 
            
            --To get the text used for share.         
            --DECLARE @HubCitiName varchar(100)
		 
			 SELECT @HubCitiName = HubCitiName
			 FROM HcHubCiti
			 WHERE HcHubCitiID = @HubCitiID
		       
			 SELECT @ShareText = 'I found this Appsite in the ' +@HubCitiName+ ' mobile app and thought you might be interested:'
             --FROM AppConfiguration 
             --WHERE ConfigurationType LIKE 'HubCiti Appsite Share Text'           
                  
            --Display the Basic details of the Retail location.   
            SELECT DISTINCT --R.RetailID retailerId
                     R.RetailName retName
                 --, RL.RetailLocationID retailLocationID
                 --, RL.Address1 as retailerAddress
                 --, RL.Address2 as address2
                 --, RL.City as city
                 --, RL.State as state
                 --, RL.PostalCode as postalCode
                 --, C.ContactPhone contactPhone
                 --, CASE WHEN RL.RetailLocationURL IS NULL THEN R.RetailURL ELSE RL.RetailLocationURL END retailerURL
				   , IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) retImage   
                 --, IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) as retImage
                 --, QRUrl = @Config+'2000.htm?key1='+ CAST(RL.RetailID AS VARCHAR(10)) +'&key2='+ CAST(RL.RetailLocationID AS VARCHAR(10))
				   , QRUrl = @Config+'2000.htm?retId='+ CAST(@RetailID AS VARCHAR(10)) +'&retlocId='+ CAST(@RetailLocationID AS VARCHAR(10))+'&hubcitiId='+ CAST(@HubCitiId AS VARCHAR(10))
                 --,@ShareType ShareType
            FROM  RetailLocation RL 
            INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND RL.Active=1 AND R.RetailerActive=1     
            LEFT JOIN HcAppSite H ON H.RetailLocationID = RL.RetailLocationID AND H.HcHubCitiID = @HubCitiId 
            LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
            LEFT JOIN Contact C ON C.ContactID = RC.ContactID           
            WHERE RL.RetailID = @RetailID 
            AND RL.RetailLocationID = @RetailLocationID  
                   
            
           --Confirmation of Success.
		   SELECT @Status = 0 
        COMMIT TRANSACTION          
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_HcShareAppsite.'       
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;
            
      END CATCH;
END;












































GO
