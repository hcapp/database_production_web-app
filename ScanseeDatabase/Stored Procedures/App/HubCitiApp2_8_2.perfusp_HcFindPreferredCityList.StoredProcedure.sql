USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[perfusp_HcFindPreferredCityList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcFindPreferredCityList]
Purpose					: To display list of Cities which has Retailers for given HubCitiID(RegionApp).
Example					: [usp_HcFindPreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  10/6/2014       SPAN              1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[perfusp_HcFindPreferredCityList]
(
   
    --Input variable	  
	  @UserID int	
	, @HcHubCitiID int
	, @HcMenuItemID int
    , @HcBottomButtonID int
	, @CategoryName varchar(100)
    , @Latitude Decimal(18,6)
    , @Longitude  Decimal(18,6)
    , @Radius int
    , @SearchKey varchar(255)
	, @BusinessSubCategoryID int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

		BEGIN TRY

			DECLARE @PostalCode varchar(10)
			, @UserOutOfRange bit 
			, @DefaultPostalCode varchar(50)
			, @DistanceFromUser float 
			, @Tomorrow DATETIME = GETDATE() + 1
			, @Yesterday DATETIME = GETDATE() - 1	
            , @RegionAppFlag int
			, @HcAppListID int
			, @BusinessCategoryID Int

			DECLARE @UserID1 INT = @UserID,
			@HcHubCitiID1 INT = @HcHubCitiID,
			@HcMenuItemID1 INT = @HcMenuItemID,
			@HcBottomButtonID1 INT = @HcBottomButtonID,
			@BusinessSubCategoryID1 INT = @BusinessSubCategoryID



			--SearchKey implementation
			DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

			SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
					WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
					WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
					ELSE @SearchKey END)


			SELECT @BusinessCategoryID=BusinessCategoryID 
			FROM BusinessCategory 
			WHERE BusinessCategoryName LIKE @CategoryName

			--SET @HcBottomButtonID1 = IIF(@HcBottomButtonID1 IS NOT NULL,0,Null)

			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppFlag = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiID = @HcHubCitiID1                     
                            
			--To check whether user has preferred cities are not.
			DECLARE @UserPreferredCity bit = 0
			SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID1 AND HcCityID IS NULL THEN 0
												WHEN HcUserID = @UserID1 AND HcCityID IS NOT NULL THEN 1
												ELSE 0 END
			FROM HcUsersPreferredCityAssociation
			WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1

			--To Fetch region app associated citylist
			CREATE TABLE #CityList(HcCityID int,CityName Varchar(200))
							  
			IF @UserPreferredCity = 0
			BEGIN
				
				INSERT INTO #CityList(HcCityID
									 ,CityName)
				SELECT DISTINCT C.HcCityID 
								,C.CityName					
				FROM HcHubciti H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID 
				INNER JOIN HcCity C ON C.HcCityID =  LA.HcCityID 
				WHERE H.HcHubcitiID = @HcHubCitiID1
				--UNION
				--SELECT 0, 'ABC'

			END	

			ELSE IF @UserPreferredCity = 1
			BEGIN
				
				INSERT INTO #CityList(HcCityID
									 ,CityName)
				SELECT DISTINCT C.HcCityID 
								,C.CityName					
				FROM HcHubciti H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID 
				INNER JOIN HcCity C ON C.HcCityID = LA.HcCityID 
				INNER JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1
				WHERE H.HcHubcitiID = @HcHubCitiID1
				--UNION 
				--SELECT 0, 'ABC'

			END 

			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
			EXEC [HubCitiApp2_1].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode) 

			

			--To fetch all the duplicate retailers.
			SELECT DISTINCT DuplicateRetailerID 
			INTO #DuplicateRet
			FROM Retailer 
			WHERE DuplicateRetailerID IS NOT NULL

			--Derive the Latitude and Longitude in the absence of the input.
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
			BEGIN
					--If the postal code is passed then derive the co ordinates.
					IF @PostalCode IS NOT NULL
					BEGIN
							SELECT @Latitude = Latitude
								, @Longitude = Longitude
							FROM GeoPosition 
							WHERE PostalCode = @PostalCode
					END          
					ELSE
					BEGIN
							SELECT @Latitude = G.Latitude
								, @Longitude = G.Longitude
							FROM GeoPosition G
							INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
							WHERE U.HcUserID = @UserID1 
					END                                                                               
			END
			
			--Get the user preferred radius.
			If (@Radius IS NULL)
			BEGIN

				SELECT @Radius = LocaleRadius
				FROM HcUserPreference 
				WHERE HcUserID = @UserID1
			END
                           
			SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))  
            
			IF(@CategoryName IS NOT NULL) 
			BEGIN       
					SELECT DISTINCT F.BusinessCategoryID 
					INTO #BusinessCategory              
					FROM  BusinessCategory F                               
					LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON F.BusinessCategoryID=FB.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemID1
					LEFT JOIN HcBottomButtonFindRetailerBusinessCategories FBC ON F.BusinessCategoryID=FBC.BusinessCategoryID AND FBC.HcBottomButonID=@HcBottomButtonID1                             
					WHERE F.BusinessCategoryName = @CategoryName 
					AND ((@HcMenuItemID1 IS NOT NULL AND FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID)
									OR
						(@HcBottomButtonID1 IS NOT NULL AND FBC.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID = FBC.BusinessCategoryID)
						--			OR
						--(@HcBottomButtonID1 = 0)
						)
			END
			ELSE IF(@CategoryName IS NULL)
			BEGIN
				SELECT DISTINCT BusinessCategoryID = ISNULL(FB.BusinessCategoryID,FBC.BusinessCategoryID)
				INTO #BusinessSubCategory              
				FROM HcMenuFindRetailerBusinessCategories FB 
				LEFT JOIN HcBottomButtonFindRetailerBusinessCategories FBC ON FB.BusinessCategoryID=FBC.BusinessCategoryID                           
				WHERE (((@HcMenuItemID1 IS NOT NULL AND FB.HcMenuItemID=@HcMenuItemID1) OR 1=1)
								OR
					((@HcBottomButtonID1 IS NOT NULL AND FBC.HcBottomButonID=@HcBottomButtonID1) OR 1=1)
					--			OR
					--(@HcBottomButtonID1 = 0)
					)
			END
                                                      

			-- To identify Retailer that have products on Sale or any type of discount
                SELECT DISTINCT Retailid , RetailLocationid
                INTO #RetailItemsonSale
                FROM 
                (SELECT b.RetailID, a.RetailLocationID 
                FROM RetailLocationDeal a 
                INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID 
                INNER JOIN HcLocationAssociation HL ON b.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
                INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = B.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                               
                INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                                                        and a.ProductID = c.ProductID
                                                                        and GETDATE() between ISNULL(a.SaleStartDate, GETDATE() - 1	) and ISNULL(a.SaleEndDate, GETDATE() + 1)
                UNION ALL 
                SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                FROM Coupon C 
                INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
                INNER JOIN RetailLocation RL ON RL.RetailID = CR.RetailID
                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                   
                LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                GROUP BY C.CouponID
                            ,NoOfCouponsToIssue
                            ,CR.RetailID
                            ,CR.RetailLocationID
                HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                            ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                          
                --UNION ALL  

                --select  RR.RetailID, 0 as RetaillocationID  
                --from Rebate R 
                --INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
                --INNER JOIN RetailLocation RL ON RL.RetailID = RR.RetailID
                --INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                --INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                                
                --WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

                --UNION ALL  

                --SELECT  c.retailid, a.RetailLocationID 
                --FROM  LoyaltyDeal a
                --INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
                --INNER JOIN RetailLocation c on a.RetailLocationID = c.RetailLocationID
                --INNER JOIN HcLocationAssociation HL ON c.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1              
                --INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = C.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                --INNER JOIN RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
                --                                                and b.ProductID = LDP.ProductID 
                --Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                UNION ALL 

                SELECT DISTINCT rl.RetailID, rl.RetailLocationID
                FROM ProductHotDeal p
                INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID
                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                                                                                   
                INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
                GROUP BY P.ProductHotDealID
                            ,NoOfHotDealsToIssue
                            ,rl.RetailID
                            ,rl.RetailLocationID
                HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                            ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                UNION ALL 

                select q.RetailID, qa.RetailLocationID
                from QRRetailerCustomPage q
                INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                INNER JOIN RetailLocation RL ON RL.RetailLocationID=qa.RetailLocationID
                INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                     
                INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE() + 1)
                ) Discount 
				
				    
                CREATE TABLE #Temp(HcCityID int
								   ,CityName Varchar(100)
								   ,isCityChecked bit)
				
				IF(@CategoryName IS NULL)
				BEGIN
					INSERT INTO #Temp(HcCityID 
									 ,CityName
									 ,isCityChecked)
							  SELECT HcCityID
									,CityName
									,isCityChecked
							  FROM(
							  SELECT DISTINCT  CL.HcCityID
											  ,CL.CityName
											  ,isCityChecked = 1
											  ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
							  FROM Retailer R 
							  INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
							  INNER JOIN #BusinessSubCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
							  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
							  INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID1 
							  INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.HcCityID =HL.HcCityID ) OR (@RegionAppFlag =0)-- AND CL.HcCityID=0)
							  INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID1  AND Associated =1 
							  LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
							  LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
							  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
							  -----------to get retailer that have products on sale
							  LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
							  LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
							  LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
							  WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
							  AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')
							  ) Retailer
							  WHERE Distance <= @Radius
							 
				END
				
				ELSE
				BEGIN
					INSERT INTO #Temp(HcCityID 
									 ,CityName
									 ,isCityChecked)
							  SELECT HcCityID
									,CityName
									,isCityChecked
							  FROM(
							  SELECT DISTINCT  CL.HcCityID
											  ,CL.CityName
											  ,isCityChecked = 1
											  ,Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
							  FROM Retailer R 
							  INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1 AND RBC.BusinessCategoryID = @BusinessCategoryID
							  --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
							  INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
							  INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1  
							  INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.HcCityID =HL.HcCityID ) OR (@RegionAppFlag =0)-- AND CL.HcCityID=0)
							  INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID1  AND Associated =1 
							  LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
							  LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
							  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
							  -----------to get retailer that have products on sale
							  LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
							  LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
							  LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
							  WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
							  AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
							  OR (@SearchKey IS NULL))
							  AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID1 OR (@BusinessSubCategoryID1 IS NULL OR @BusinessSubCategoryID1 = 0 OR @BusinessSubCategoryID1 =-1))
												--AND ((S.RetailLocationID IS NOT NULL AND S.RetailLocationID = RL.RetailLocationID)
												--            OR
												--     (S.RetailLocationID IS NULL AND 1 = 1))
							  ) Retailer
							  WHERE Distance <= @Radius
				END

				DECLARE @GuestUser int
				DECLARE @CityPrefNotVisitedUser bit

				SELECT @GuestUser = U.HcUserID
				FROM HcUser U
				INNER JOIN HcUserDeviceAppVersion DA ON U.HcUserID = DA.HcUserID
				WHERE UserName = 'GuestLogin'
				AND DA.HcHubCitiID = @HcHubCitiID1

			
				IF NOT EXISTS (SELECT HcUserID FROM HcUsersPreferredCityAssociation WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1)
				BEGIN
					SET @CityPrefNotVisitedUser = 1
				END
				ELSE
				BEGIN
					SET @CityPrefNotVisitedUser = 0
				END
				
				SELECT DISTINCT HcCityID 
								,CityName
								,isCityChecked
				INTO #IsCityChecked
				FROM #Temp
				ORDER BY CityName

				IF(@CategoryName IS NOT NULL) 
				BEGIN 

					SELECT HcCityID
							  ,cityname
							  ,isCityChecked
					INTO #Final
					FROM(
					SELECT DISTINCT I.HcCityID 
									,CityName
									, isCityChecked = CASE WHEN (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1) THEN 1
													WHEN UC.HcUsersPreferredCityAssociationID IS NOT NULL THEN 1
													WHEN UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1 THEN 1						
													ELSE 0 END
									, Distance1 = 0
					FROM #IsCityChecked I					
					LEFT JOIN HcUsersPreferredCityAssociation UC ON  ((I.HcCityID = UC.HcCityID OR UC.HcCityID IS NULL) AND UC.HcHubcitiID = @HcHubCitiID1 AND UC.HcUserID = @UserID1) OR (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1)
					
					UNION ALL
					SELECT DISTINCT   C.HcCityID
										, C.CityName				
										, isCityChecked = 0
										 ,Distance1 = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					FROM Retailer R 
					INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1 AND RBC.BusinessCategoryID = @BusinessCategoryID
					--INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID 
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
					INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
					INNER JOIN HcHubCiti H ON HL.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID  = @HcHubCitiID1
					INNER JOIN HcCity C ON  C.HcCityID =HL.HcCityID
					INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID1  AND Associated =1 
					LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
					LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
					-----------to get retailer that have products on sale
					LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
					LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
					LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
					WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
					--AND C.HcCityID NOT IN (SELECT HcCityID FROM HcUsersPreferredCityAssociation	WHERE HcUserID=@UserID1 and H.HcHubcitiID=@HcHubCitiID1)
					AND C.HcCityID NOT IN (SELECT HcCityID FROM #IsCityChecked)
					AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
					OR (@SearchKey IS NULL))
					AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID1 OR (@BusinessSubCategoryID1 IS NULL OR @BusinessSubCategoryID1 = 0 OR @BusinessSubCategoryID1 =-1))
				 
					UNION ALL
					SELECT DISTINCT   C.HcCityID
									 ,C.CityName				
									 ,isCityChecked = 0
									 ,Distance1 = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					FROM Retailer R 
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID    AND RetailerActive = 1                                
					INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
					INNER JOIN HcHubCiti H ON HL.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID  = @HcHubCitiID1
					INNER JOIN HcCity C ON  C.HcCityID =HL.HcCityID
					INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID1  AND Associated =1 
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
					-----------to get retailer that have products on sale
					LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
					LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
					LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
					WHERE Headquarters = 0 AND @CategoryName IS NULL AND D.DuplicateRetailerID IS NULL 
					--AND C.HcCityID NOT IN (SELECT HcCityID FROM HcUsersPreferredCityAssociation	WHERE HcUserID=@UserID1 and H.HcHubcitiID=@HcHubCitiID1)
					AND C.HcCityID NOT IN (SELECT HcCityID FROM #IsCityChecked)
					AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%') 
					 )A
					 WHERE Distance1 <= @Radius

					SELECT DISTINCT HcCityID AS CityID
							,CityName
							,isCityChecked
					FROM #Final
					ORDER BY CityName
			END
			ELSE IF(@CategoryName IS NULL)
			BEGIN
				
				SELECT HcCityID
							  ,cityname
							  ,isCityChecked
					INTO #Final1
					FROM(
					SELECT DISTINCT I.HcCityID 
									,CityName
									, isCityChecked = CASE WHEN (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1) THEN 1
													WHEN UC.HcUsersPreferredCityAssociationID IS NOT NULL THEN 1
													WHEN UC.HcUserID = @UserID1 AND UC.HcHubcitiID = @HcHubCitiID1 THEN 1						
													ELSE 0 END
									, Distance1 = 0
					FROM #IsCityChecked I					
					LEFT JOIN HcUsersPreferredCityAssociation UC ON  ((I.HcCityID = UC.HcCityID OR UC.HcCityID IS NULL) AND UC.HcHubcitiID = @HcHubCitiID1 AND UC.HcUserID = @UserID1) OR (@GuestUser = @UserID1) OR (@CityPrefNotVisitedUser = 1)
					
					UNION ALL
					SELECT DISTINCT   C.HcCityID
										, C.CityName				
										, isCityChecked = 0
										 ,Distance1 = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					FROM Retailer R 
					INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
					INNER JOIN #BusinessSubCategory SC ON RBC.BusinessCategoryID = SC.BusinessCategoryID       
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
					INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
					INNER JOIN HcHubCiti H ON HL.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID  = @HcHubCitiID1
					INNER JOIN HcCity C ON  C.HcCityID =HL.HcCityID
					INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID1  AND Associated =1 
					LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
					LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
					-----------to get retailer that have products on sale
					LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
					LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
					LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
					WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
					--AND C.HcCityID NOT IN (SELECT HcCityID FROM HcUsersPreferredCityAssociation	WHERE HcUserID=@UserID1 and H.HcHubcitiID=@HcHubCitiID1)
					AND C.HcCityID NOT IN (SELECT HcCityID FROM #IsCityChecked)
					AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%'))
					OR (@SearchKey IS NULL))
					AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID1 OR (@BusinessSubCategoryID1 IS NULL OR @BusinessSubCategoryID1 = 0 OR @BusinessSubCategoryID1 =-1))
				 
					UNION ALL
					SELECT DISTINCT   C.HcCityID
									 ,C.CityName				
									 ,isCityChecked = 0
									 ,Distance1 = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					FROM Retailer R 
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID     AND RetailerActive = 1                                
					INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
					INNER JOIN HcHubCiti H ON HL.HcHubCitiID = H.HcHubCitiID AND H.HcHubCitiID  = @HcHubCitiID1
					INNER JOIN HcCity C ON  C.HcCityID =HL.HcCityID
					INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = @HcHubCitiID1  AND Associated =1 
					LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
					-----------to get retailer that have products on sale
					LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
					LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
					LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID                                                 
					WHERE Headquarters = 0 AND @CategoryName IS NULL AND D.DuplicateRetailerID IS NULL 
					--AND C.HcCityID NOT IN (SELECT HcCityID FROM HcUsersPreferredCityAssociation	WHERE HcUserID=@UserID1 and H.HcHubcitiID=@HcHubCitiID1)
					AND C.HcCityID NOT IN (SELECT HcCityID FROM #IsCityChecked)
					AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%') 
					 )A
					 WHERE Distance1 <= @Radius

					SELECT DISTINCT HcCityID AS CityID
							,CityName
							,isCityChecked
					FROM #Final1
					ORDER BY CityName


			END
			   --Confirmation of Success.
			   SELECT @Status = 0

	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcFindPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




























GO
