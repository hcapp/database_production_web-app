USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[1usp_HcRetailerBusinessSubCategoriesDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcRetailerBusinessSubCategoriesDisplay]
Purpose					: To display Retailer Business SubCategories list.
Example					: [usp_HcRetailerBusinessSubCategoriesDisplay]

History
Version		  Date			Author			Change Description
--------------------------------------------------------------- 
1.0		 16th Jan 2014		SPAN	           1.1
2.0		 12th sept 2016		Shilpashree		Optimized
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[1usp_HcRetailerBusinessSubCategoriesDisplay]
(
	--Input Variables
	  @HubCitiID int	
	, @BusinessCatID int
	, @Latitude Decimal(18,6)
	, @Longitude Decimal(18,6)
    , @UserID int
	, @PostalCode varchar(10)
	, @HcMenuItemID int
	, @BottomButtonID int
	, @SearchKey varchar(1000)

	--Output Variables
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output
	, @Status int output
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
  
)
AS
BEGIN
      BEGIN TRY
			SET NOCOUNT ON

		DECLARE @Radius int
			, @DistanceFromUser FLOAT
			, @UserLatitude decimal(18,6)
			, @UserLongitude decimal(18,6) 
			, @RegionAppFlag Bit
			, @Sin FLOAT = SIN(@Latitude / 57.2958)
			, @Coslat FLOAT =  COS(@Latitude / 57.2958) 
			, @CosLong FLOAT = (@Longitude / 57.2958) 
			, @Length INT
			, @UserConfiguredPostalCode varchar(50)
			, @IsSubCategory bit = 0

		DECLARE @UserID1 INT = @UserID,
				@HcHubCitiID1 INT = @HubCitiID,
				@HcMenuItemID1 INT = @HcMenuItemID,
				@HcBottomButtonID1 INT = @BottomButtonID
		
		SELECT @UserConfiguredPostalCode = PostalCode FROM HcUser WHERE HcUserID = @UserID1		
		SET @UserLatitude = @Latitude
		SET @UserLongitude = @Longitude
		SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
								WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
								WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
								ELSE @SearchKey END)

		CREATE TABLE #SubCategoryList(BusinessCategoryID int,SubCategoryID int)
		CREATE TABLE #HcRetailerSubCategory(RetailID int,RetailLocationID int,BusinessCategoryID int,HcBusinessSubCategoryID int)
		CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
		CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100),HcHubCitiID int)
		CREATE TABLE #RetailItemsonSale1(Retailid bigint , RetailLocationid bigint)
        CREATE TABLE #Retail(RetailID INT 
                            , RetailName VARCHAR(1000)
                            , RetailLocationID INT
                            , City VARCHAR(1000)    
                            , PostalCode VARCHAR(20)
                            , retLatitude FLOAT
                            , retLongitude FLOAT
                            , Distance FLOAT  
                            , DistanceActual FLOAT                                                   
							, HcBusinessSubCategoryID INT
                            , BusinessSubCategoryName VARCHAR(1000))

		SELECT @IsSubCategory = 1
			FROM HcBusinessSubCategoryType T
			INNER JOIN HcBusinessSubCategory S ON T.HcBusinessSubCategoryTypeID = S.HcBusinessSubCategoryTypeID
				WHERE T.BusinessCategoryID = @BusinessCatID
				
		SELECT DISTINCT RBC.*,R.RetailName 
		INTO #RetailerBusinessCategory 
			FROM RetailerBusinessCategory RBC
			INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1 
				WHERE BusinessCategoryID=@BusinessCatID
	
		IF (@IsSubCategory = 1)
		BEGIN
			
			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcMenuFindRetailerBusinessCategories 
				WHERE BusinessCategoryID= @BusinessCatID AND HcMenuItemID=@HcMenuItemID

			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcBottomButtonFindRetailerBusinessCategories  
				WHERE BusinessCategoryID = @BusinessCatID AND HcBottomButonID= @BottomButtonID

			INSERT INTO #HcRetailerSubCategory(RetailID,RetailLocationID,BusinessCategoryID,HcBusinessSubCategoryID)
			SELECT RS.RetailID,RS.RetailLocationID,RS.BusinessCategoryID,RS.HcBusinessSubCategoryID
			FROM HcRetailerSubCategory RS
			INNER JOIN #SubCategoryList SL ON RS.BusinessCategoryID = SL.BusinessCategoryID AND RS.HcBusinessSubCategoryID = SL.SubCategoryID
			
		END

		IF(SELECT 1 FROM HcHubCiti H
                    INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                    AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
        BEGIN
				SET @RegionAppFlag = 1
									
				INSERT INTO #RHubcitiList(HcHubCitiID)
				(SELECT DISTINCT HcHubCitiID 
				FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
				UNION ALL
				SELECT  @HCHubCitiID1 AS HcHubCitiID
				)
				
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)		
				SELECT DISTINCT LA.HcCityID 
								,LA.City	
								,LA.PostalCode
								,H.HcHubCitiID	                         
				FROM #RHubcitiList H
				INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HcHubCitiID1 
				LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1  AND UP.HcHubcitiID =H.HchubcitiID                                                      
				WHERE( UP.HcUsersPreferredCityAssociationID IS NOT NULL AND LA.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1)                                        
				OR (UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID1)
				
        END
		ELSE
        BEGIN
				SET @RegionAppFlag =0

				INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID1 
				 
				INSERT INTO #CityList(CityID,CityName,PostalCode,HcHubCitiID)
				SELECT DISTINCT HcCityID 
								,City
								,PostalCode	
								,HcHubCitiID				
				FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID1                                   
		END
				
		SELECT @UserLatitude = @Latitude
			 , @UserLongitude = @Longitude

		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM GeoPosition 
				WHERE PostalCode = @PostalCode
		END
         
		--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID1
		END

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
		EXEC [HubCitiapp2_8_7].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFROMUser OUTPUT
		--select @DefaultPostalCode = defaultpostalcode from hchubciti where HcHubCitiid = @HcHubCitiID  and @PostalCode is null
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
		BEGIN
			SELECT @Latitude = Latitude
					, @Longitude = Longitude
			FROM GeoPosition 
			WHERE PostalCode = @PostalCode                                                                             
		END  
					
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL
                   
		--Get the user preferred radius.
		SELECT @Radius = LocaleRadius
        FROM HcUserPreference 
        WHERE HcUserID = @UserID1
                                         
        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
											WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 
				
		SELECT DISTINCT RBC.RetailerID 
				, RBC.RetailName    
				, RL.RetailLocationID 
				, RL.City
				, RL.PostalCode
				, RetailLocationLatitude
				, RetailLocationLongitude
				, HL.HchubcitiID 
				, RS.HcBusinessSubCategoryID
		INTO #RetailInfo
		FROM #RetailerBusinessCategory RBC
		INNER JOIN RetailLocation RL ON RBC.RetailerID = RL.RetailID AND RL.Active = 1
		INNER JOIN #CityList HL ON RL.PostalCode=HL.PostalCode AND RL.HcCityID = HL.CityID AND HL.HcHubCitiID=@HCHubCitiID1 
		INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
		INNER JOIN HcRetailerAssociation RA ON RL.RetailID = RA.RetailID AND RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = RH.HchubcitiID
		INNER JOIN #HcRetailerSubCategory RS ON RL.RetailLocationID = RS.RetailLocationID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
	
		INSERT INTO #Retail 
        SELECT  *
		FROM 
		(SELECT DISTINCT RL.RetailerID 
					, RL.RetailName    
					, RL.RetailLocationID 
					, RL.City
					, RL.PostalCode
					, RL.RetailLocationLatitude  retLatitude
					, RL.RetailLocationLongitude retLongitude
					, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					, DistanceActual = 0
					, SB.HcBusinessSubCategoryID
					, BusinessSubCategoryName   
		FROM #RetailInfo RL
		INNER JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID = RL.HcBusinessSubCategoryID 
		LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode AND G.City = RL.City 
		LEFT JOIN RetailerKeywords RK ON RL.RetailerID = RK.RetailID                                 
        WHERE (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
		) Retailer
        WHERE Distance <= @Radius                             
        
		SELECT DISTINCT HcBusinessSubCategoryID AS catId
					  , BusinessSubCategoryName AS catName 
		FROM #Retail
		ORDER BY BusinessSubCategoryName 

		--Confirmation of Success
		SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcRetailerCategoriesDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
