USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_PushNotifyBadgeReset]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_PushNotifyBatchCount]
Purpose					: To Deal Of the Day for a Hubciti.
Example					: [usp_PushNotifyBatchCount]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
			15/12/2015      Sagar Byali   
---------------------------------------------------------------
*/

CREATE PROCEDURE[HubCitiApp2_8_7].[usp_PushNotifyBadgeReset]
(
    --Input variable
	  @UserID int
	, @HcHubCitiID Int
	, @DeviceID VARCHAR(100)
	--, @NewsType BIT

    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	


		   UPDATE HcUserToken
		   SET   PushNotifyBadgeCount = 0 
				,NewsTypeBreaking=0
				,NewsTypeTrending=0
				,NewsTypeDeals=0
				,NewsTypeTaylorBreakingNews = 0
				,BreakingNewsPushCount = 0
		   FROM HcUserToken T
		   INNER JOIN HcUserDeviceAppVersion D ON T.DeviceID = D.DeviceID AND T.HcHubCitiID = D.HcHubCitiID
		   WHERE T.DeviceID = @DeviceID AND T.HcHubCitiID = @HcHubCitiID AND D.HcUserID = @UserID
		
		   --UPDATE HcUserToken
		   --SET BreakingNewsPushCount = 0 
			  --,NewsTypeBreaking=0
			  --,NewsTypeTrending=0
			  --,NewsTypeDeals=0
			  --,NewsTypeTaylorBreakingNews = 0
		   --FROM HcUserToken T
		   --INNER JOIN HcUserDeviceAppVersion D ON T.DeviceID = D.DeviceID AND T.HcHubCitiID = D.HcHubCitiID
		   --WHERE T.DeviceID = @DeviceID AND T.HcHubCitiID= @HcHubCitiID AND D.HcUserID = @UserID


		   	if not exists(SELECT *  FROM HcNewsStaging  N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
						  WHERE convert(datetime,convert(varchar(100), DateCreated,101))=convert(datetime,convert(varchar(100),getdate(),101)) AND NewsType = 'Breaking News') 

			begin
			UPDATE HcUserToken 	SET NewsTypeBreaking = 0
			END
			if not exists(SELECT *  FROM HcNewsStaging  N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
						  WHERE convert(datetime,convert(varchar(100), DateCreated,101))=convert(datetime,convert(varchar(100),getdate(),101)) AND NewsType = 'Trending') 


			begin
			UPDATE HcUserToken 	SET NewsTypeTrending = 0
			END





			if not exists(SELECT *  FROM HcNewsStaging  N inner join HcNewsType T ON T.HcNewsTypeID = N.HcNewsTypeID
						  WHERE convert(datetime,convert(varchar(100), DateCreated,101))=convert(datetime,convert(varchar(100),getdate(),101)) AND NewsType = 'Taylor Breaking News') 

			begin
			UPDATE HcUserToken 	SET NewsTypeTaylorBreakingNews = 0
			END
		

	
	

	--Confirmation of Success
			SELECT @Status = 0
	
	END TRY

	
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcDelaoftheDayCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














GO
