USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcBandSummaryDetail]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcDisplayBandInfo]
Purpose					: To Display the basic infomration about the Retail Location.
Example					: [usp_HcDisplayBandInfo]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd May 2012	SPAN			Initial Version
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcBandSummaryDetail]
(
	  
	  @UserID int
	, @BandID int
    --, @RetailLocationID int
    , @Latitude Decimal(18,6)
	, @Longitude  Decimal(18,6)
    
	    
    
    --User Tracking Imputs
    --, @ScanTypeID int
    , @MainMenuID int
    --, @RetailerListID int
    
	--Output Variable 
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRAN

				BEGIN TRY		
				
				DECLARE @BandConfig varchar(50)
				DECLARE @RetailLocationAdvertisement VARCHAR(1000)--WelcomePage/SplashAd/BannerAd(previously)
				DECLARE @RibbonAdImagePath varchar(1000)--BannerAd
				DECLARE @RibbonAdURL VARCHAR(1000)--BannerAdURL
				DECLARE @RetailerPageQRURL VARCHAR(1000)
				DECLARE @Config VARCHAR(100)
				DECLARE @SplashAdID int
				DECLARE @BannerAdID int	 
				DECLARE @RetailerDetailsID INT
				--DECLARE @DefaultPostalCode varchar(50)
				DECLARE @HubCitiID INT
				--DECLARE @Latitude FLOAT
				--DECLARE @Longitude FLOAT
				DECLARE @ZipCode VARCHAR(10)	    
				--User Tracking
				DECLARE	@RowCount int 
				DECLARE @DistanceFromUser float
				DECLARE @UserLatitude float
				DECLARE @UserLongitude float 

				SELECT @UserLatitude = @Latitude
					 , @UserLongitude = @Longitude

				IF (@UserLatitude IS NULL) 
				BEGIN
					SELECT @UserLatitude = Latitude
						 , @UserLongitude = Longitude
					FROM HcUser A
					INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
					WHERE HcUserID = @UserID 
				END
			    
				--SELECT @HubCitiID = HcHubCitiID
				--FROM HubCitiReportingDatabase..MainMenu
				--WHERE MainMenuID = @MainMenuID
			    
				--Retrieve the server configuration.
				--SELECT @Config = ScreenContent 
				--FROM AppConfiguration 
				--WHERE ConfigurationType LIKE 'QR Code Configuration'
				--AND Active = 1 
				
				--To get server Configuration.
				SELECT @BandConfig= ScreenContent  
				FROM AppConfiguration   
				WHERE ConfigurationType='Web Band Media Server Configuration'	
				
				--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
				EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
				SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
				
				IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
				BEGIN
					 --If the postal code is passed then derive the co ordinates.
                            IF @ZipCode IS NOT NULL
							BEGIN
                                    SELECT @Latitude = Latitude
                                        , @Longitude = Longitude
                                    FROM GeoPosition 
                                    WHERE PostalCode = @ZipCode
                            END		
							ELSE
							BEGIN
								SELECT @Latitude = G.Latitude
								     , @Longitude = G.Longitude
								FROM GeoPosition G
								INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
								WHERE U.HcUserID = @UserID	
							END	
				END  	
				
				----Get the associated Splash Ad/ Welcome Page Details.
				--SELECT DISTINCT @RetailLocationAdvertisement =  @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.SplashAdImagePath
				--	 , @SplashAdID = RLA.AdvertisementSplashID
				--FROM AdvertisementSplash RLA
				--INNER JOIN RetailLocationSplashAd RL ON RL.RetailLocationID = RL.RetailLocationID
				--WHERE RL.RetailLocationID = @RetailLocationID
				--AND RLA.RetailID = @RetailID
				--AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)
				--AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementSplash ASP 
				--					 INNER JOIN  RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID = RLS.AdvertisementSplashID
				--					 WHERE RetailLocationID = @RetailLocationID)


			
									 --select @RibbonAdImagePath = 'http://66.228.143.28:8080/Images/band/banner-default.jpg'
				--Get the associated Banner Ad Details.
				SELECT DISTINCT @RibbonAdImagePath = 
				
				 @BandConfig + + CAST(@BandID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
				
					 , @RibbonAdURL = RLA.BannerAdURL
					 , @BannerAdID = RLA.bandAdvertisementBannerID
				FROM bandAdvertisementBanner RLA
				--INNER JOIN RetailLocationBannerAd RL ON RL.RetailLocationID = RL.RetailLocationID
				WHERE RLA.bandID = @bandID
				--AND RLA.RetailID = @RetailID
				AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1) --if the end date is set null then that ad is always active.
				AND RLA.StartDate = (SELECT MAX(StartDate) FROM bandAdvertisementBanner ASP 
									 --INNER JOIN  RetailLocationBannerAd RLS ON ASP.AdvertisementBannerID = RLS.AdvertisementBannerID
									 WHERE BandID = @bandID)

									 
				
				--Get the associated QRPage URL.
				SELECT DISTINCT @RetailerPageQRURL = CASE WHEN QR.QRBandCustomPageID IS NOT NULL THEN 
							@Config  + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(@BandID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(QR.QRBandCustomPageID AS VARCHAR(10)) 
							ELSE NULL END			
				FROM QRBandCustomPage QR
				left JOIN QRBandCustomPageAssociation QRRCPA ON QR.QRBandCustomPageID = QRRCPA.QRBandCustomPageID
				INNER JOIN QRTypes Q ON QR.QRTypeID = Q.QRTypeID
				WHERE QR.BandID = @BandID
				--AND QRRCPA.RetailLocationID = @RetailLocationID
				AND Q.QRTypeName = 'Main Menu Page'
				
				--SELECT @Latitude = B.Latitude
				--	 , @Longitude = B.Longitude
				--	 , @PostalCode = A.PostalCode
				--FROM HcUser A
				--INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				--WHERE HcUserID = @UserID
				
				----Find if there is any sale or discounts associated with the Retail Location.
				--SELECT DISTINCT Retailid , RetailLocationid
				--INTO #RetailItemsonSale
				--FROM 
				--(SELECT DISTINCT b.RetailID, a.RetailLocationID 
				--from RetailLocationDeal a 
				--inner join RetailLocation b on a.RetailLocationID = b.RetailLocationID AND b.Active = 1
				--inner join RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
				--								   and a.ProductID = c.ProductID 
				--								   AND A.RetailLocationID = @RetailLocationID
				--								   and GETDATE() between isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)

				--UNION 

				--SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
				--FROM Coupon C 
				--INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				--LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
				--WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				--AND CR.RetailLocationID = @RetailLocationID
				--GROUP BY C.CouponID
				--		,NoOfCouponsToIssue
				--		,CR.RetailID
				--		,CR.RetailLocationID
				--HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				--		 ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
						 								   
				--UNION
				 
				--SELECT DISTINCT  RR.RetailID, RR.RetailLocationID  
				--from Rebate R 
				--inner join RebateRetailer RR ON R.RebateID=RR.RebateID
				--where RR.RetailLocationID = @RetailLocationID
				--AND GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

				--UNION 

				--SELECT DISTINCT  c.retailid, a.RetailLocationID 
				--from  LoyaltyDeal a
				--INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
				--inner join RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
				--							and a.ProductID = LDP.ProductID 
				--inner join RetailLocation c on a.RetailLocationID = b.RetailLocationID 
				--Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
				--AND A.RetailLocationID = @RetailLocationID AND c.Active = 1

				--UNION

				--SELECT DISTINCT rl.RetailID, rl.RetailLocationID
				--FROM ProductHotDeal p
				--INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
				--INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID AND rl.Active = 1  
				--LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
				--LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
				--WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
				--AND rl.RetailLocationID = @RetailLocationID 
				--GROUP BY P.ProductHotDealID
				--		,NoOfHotDealsToIssue
				--		,rl.RetailID
				--		,rl.RetailLocationID
				--HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				--		 ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  
				
				--UNION

				--SELECT DISTINCT q.RetailID, qa.RetailLocationID
				--from QRRetailerCustomPage q
				--inner join QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
				--inner join QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
				--where qa.RetailLocationID = @RetailLocationID 
				--and GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE()+1)) a			
						 
			

	   SELECT DISTINCT R.BandId bandID
					 , R.BandName bandName
					 , R.City+','+R.State+','+R.PostalCode  bandaddress1
					 , R.CorporatePhoneNo AS contactPhone
					 , R.BandURL bandURL
					 , ISNULL(@RibbonAdImagePath,'http://66.228.143.28:8080/Images/band/banner-default.jpg') ribbonAdImagePath
					 , @RibbonAdURL ribbonAdURL
					 , bandURLImgPath = 'http://66.228.143.28:8080/Images/band/web.png'  --IIF(BandImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@BandConfig+CONVERT(VARCHAR(30),R.BandID)+'/'+BandImagePath),@BandConfig+BandImagePath)),BandImagePath)
					 , phoneImg =  CASE WHEN R.CorporatePhoneNo IS NOT NULL THEN 'http://66.228.143.28:8080/Images/band/contact.png' END--
					 , mailImg ='http://66.228.143.28:8080/Images/band/mail.png' 
					 , Email mail 
					 , @RetailerPageQRURL retailerPageQRURL 
					 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')		
					 , @BannerAdID RibbonAdID
					 , @SplashAdID BannerAdID
				FROM Band R
				WHERE BandId = @BandID	
				
				
				--Capture the rows affected(No of Locations)
				SELECT @RowCount =@@ROWCOUNT 
				
				--User Tracking
						
				--Capture the RetailLocation click.

				 

				--	UPDATE HubCitiReportingDatabase..RetailerList 
				--	SET RetailLocationClick = RetailLocationClick +  1 
				--	WHERE RetailerListID in 
					
				--							(SELECT top 1 RetailerListID from HubCitiReportingDatabase..RetailerList
				--							 where Retailid = @retailid 
				--							 AND Retaillocationid  = @RetailLocationID order by datecreated desc )
											 

					
				--	--Capture the impression of the Banner & the Ribbon Ads.
				--	CREATE TABLE #Temp(RetailerDetailsID int
				--					 , RetailerListID int
				--					 , SplashAdID int
				--					 , BannerAdID int)
					
				--	INSERT INTO HubCitiReportingDatabase..RetailerDetail(RetailerListID
				--														, SplashAdID
				--														, BannerAdID
				--														, DateCreated)
																		
						
				--										SELECT @RetailerListID 
				--											 , BannerAdID
				--											 , RibbonAdID
				--											 , GETDATE()
				--										FROM #RetailLocationDetails	
					
				--	--Capture the identity value inserted									
				--	SELECT @RetailerDetailsID = SCOPE_IDENTITY()			
					
				----Check is this is invoked from Scan Now module and update associated tables.
				--IF ISNULL(@ScanTypeID, 0) <> 0
				--BEGIN
				--	SELECT @ScanTypeID = ScanTypeID
				--	FROM HubCitiReportingDatabase..ScanType
				--	WHERE ScanType = 'Retailer Summary QR Code'
					
				--	INSERT INTO HubCitiReportingDatabase..Scan(MainMenuID
				--											  ,ScanTypeID
				--											  ,Success
				--											  ,ID
				--											  ,CreatedDate)
				--	SELECT DISTINCT @MainMenuID 
				--		  , @ScanTypeID 
				--		  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
				--		  , retailLocationID
				--		  ,GETDATE()
				--	FROM #RetailLocationDetails
					
				--	INSERT INTO HubCitiReportingDatabase..RetailerList(RetailID, RetailLocationID, RetailLocationClick, MainMenuID)
				--	SELECT retailerId
				--		 , retailLocationID
				--		 , 1
				--		 , @MainMenuID
				--	FROM #RetailLocationDetails
					
				--END
					----Send the Retailer details ID in the result set.
					--SELECT @RetailerDetailsID retDetID
					--	 , @RetailerListID retListID
					--	 , retailerId
					--	 , retailerName
					--	 , retailLocationID
					--	 , retaileraddress1
					--	 , retLatitude
					--	 , retLongitude
					--	 , Distance = ISNULL(DistanceActual, Distance)
					--	 , contactPhone
					--	 , retailerURL
					--	 , bannerAdImagePath
					--	 , ribbonAdImagePath
					--	 , ribbonAdURL
					--	 , image
					--	 , retailerPageQRURL 
					--	 , SaleFlag 
					--	-- , AppDownloadLink 
					--	 , RibbonAdID
					--	 , BannerAdID	
					--FROM #RetailLocationDetails												
	     

	  
		   --Confirmation of Success.
		   SELECT @Status = 0	
		COMMIT TRANSACTION  
      
 END TRY    	
BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;









GO
