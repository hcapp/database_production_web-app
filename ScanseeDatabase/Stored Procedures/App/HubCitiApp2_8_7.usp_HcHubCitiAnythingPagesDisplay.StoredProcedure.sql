USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcHubCitiAnythingPagesDisplay]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiAnythingPagesDisplay
Purpose					: To display List of Anything Page.
Example					: usp_HcHubCitiAnythingPagesDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcHubCitiAnythingPagesDisplay]
(  
     --Input variable.
      @HubCitiID Int
      
     --User Tracking Inputs 
    , @LinkID Int
    , @MainMenuID Int 
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To display List fo Anything Page
			SELECT HcAnythingPageID
				  ,AnythingPageName
				  ,ShortDescription
				  ,LongDescription
				  ,StartDate
				  ,EndDate
				  ,ImagePath
				  ,ImageIcon
				  ,HcHubCitiID			   
			FROM HcAnythingPage 
			WHERE HcHubCitiID=@HubCitiID  
			
			--UserTrcaking
			
			--Capture the Anything page click and impression
			INSERT INTO HubCitiReportingDatabase..AnythingPageList(AnythingPageID
																  ,AnythingPageClick
																  ,MainMenuID
																  ,DateCreated)
			SELECT @LinkID
			      ,1
			      ,@MainMenuID
			      ,GETDATE()
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiAnythingPagesDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
