USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcUserLogin_Version2]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcUserLogin
Purpose					: To Validate User Login.
Example					: usp_HcUserLogin

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th Sept 2013	Pavan Sharma K	Initial Version
2.0			2nd July 2016	Bindu T A		Changes with respect to Forgot Password
---------------------------------------------------------------
*/

CREATE PROCEDURE[HubCitiApp2_8_3].[usp_HcUserLogin_Version2]
(
	  @UserName varchar(100)
	, @Password char(60)
	, @DeviceID varchar(60)
	, @AppVersion varchar(100)
	, @HubCitiKey varchar(100)
	, @RequestPlatformType varchar(100)
		
	--Output Variable 
	, @NoEmailCount BIT Output
	, @IsTempUser BIT Output
	, @HubCitiID Int Output
	, @HubCitiName varchar(255) output
	, @UserID int output
	, @Login int output
	, @HubCitiDeactivated bit output
	--, @InvalidUser int output
	, @InvalidPassword int output --@InvalidPassword output parameter will be set when login fails
	, @Result int output
	, @AddDeviceToUser bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
	, @PostalCode varchar(5) output
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @CNT int		
			
		BEGIN TRANSACTION			
		
			DECLARE @User INT
			DECLARE @TEMPUser INT
			DECLARE @RequestPlatformID int
			DECLARE @TempTime INT
			DECLARE @RegionAppID int
			DECLARE @GuestUserID int
			
			SET @AddDeviceToUser = 0
			SET @InvalidPassword = 0
			SET @HubCitiDeactivated = 0
			SET @NoEmailCount = 0
			SET @Login = -1

			SELECT @RegionAppID = HcAppListID
			FROM HcAppList
			WHERE HcAppListName = 'RegionApp'

			SELECT @GuestUserID = HcUserID
			FROM HcUser
			WHERE UserName LIKE '%guest%'
		
			SELECT @RequestPlatformID = HcRequestPlatformID
			FROM HcRequestPlatforms
			WHERE RequestPlatformtype = @RequestPlatformType
			
			SELECT @HubCitiID = HcHubCitiID
				, @HubCitiName = HubCitiName
			FROM HcHubCiti 
			WHERE HcHubCitiKey = @HubCitiKey AND Active = 1

			CREATE TABLE #temp
			(cityId INT,
			 cityName VARCHAR(200))

			select distinct @PostalCode = PostalCode
			from hcuser h
			inner join HcUserDeviceAppVersion D on D.HcUserID = H.HcUserID
			where username = @username and D.HcHubCitiID = @HubCitiID

	
			SELECT @TEMPUser = H.HcUserID 
			FROM HcUser H
			INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = H.HcUserID
			INNER JOIN HcHubCiti HC ON HC.HcHubCitiID = UD.HcHubCitiID
			WHERE (BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName)
			OR BINARY_CHECKSUM(Email) = BINARY_CHECKSUM(@UserName)) 			 
			AND HC.HcHubCitiKey = @HubCitiKey
			--AND TempPassword= 1
				

	         IF EXISTS(SELECT *  FROM HcUser  WHERE HcUserID=@TEMPUser AND TempPassword=1)
			 BEGIN
			    SET @IsTempUser=1
			 END
			 ELSE
			 BEGIN
			      SET @IsTempUser = 0
			 END

			
              SELECT @UserID = H.HcUserID ,@TempTime=DATEDIFF ( MI , TempTime ,GETDATE() )  
				FROM HcUser H
				INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = H.HcUserID
				INNER JOIN HcHubCiti HC ON HC.HcHubCitiID = UD.HcHubCitiID
				WHERE (BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName)
				OR BINARY_CHECKSUM(Email) = BINARY_CHECKSUM(@UserName)) 			 
				AND BINARY_CHECKSUM(Password) = BINARY_CHECKSUM(@Password)
				AND FaceBookAuthenticatedUser=0	
				AND HC.HcHubCitiKey = @HubCitiKey


			IF EXISTS(SELECT 1 FROM HcUser WHERE HcUserID=@UserID  AND Email IS NULL AND UserName NOT LIKE '%guest%')
				 BEGIN 
						UPDATE HcUser
						SET NoEmailCount = ISNULL(NoEmailCount,0) +1
						WHERE HcUserID=@UserID 


						UPDATE HcUser  SET  NoemailCount=0  WHERE HcUserID=@UserID  AND  Email IS NOT NULL

						SELECT @NoEmailCount = CASE (ISNULL(NoemailCount,0) % 5) WHEN 0 THEN 1 ELSE 0 END
						FROM HcUser
						WHERE HcUserID=@UserID  

				END


			 IF @IsTempUser=1 AND ISNULL(@UserID,0)=0
			 BEGIN

			 SET @IsTempUser=0
			 END

			
			--Check if the Hub Citi that the user is accessing is active and notify him accordingly.
			IF EXISTS(SELECT 1 FROM HcHubCiti WHERE HcHubCitiID = @HubCitiID AND Active = 1)		
			BEGIN
				
				--If Login is success, then capture user login.		
				IF (ISNULL(@UserID, 0) <> 0 AND (@TempTime <= 30 OR @TempTime IS NULL))
				BEGIN
				--To validate user login	
				SET @Login = 0 		
				
				--If the DeviceID is already registered with the user check if it is primary device for the User and alert him if he wants to make it as primary device.	
				IF @DeviceID IN (SELECT DeviceID FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID)					
				BEGIN 
					IF (SELECT Distinct PrimaryDevice FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID AND DeviceID = @DeviceID AND PrimaryDevice = 1) <> 1
					BEGIN
						SET @AddDeviceToUser = 1
					END
				END
				
				--If the device is not registered to the user then alert him if he wants to add & make it as primary device.
				IF @DeviceID NOT IN (SELECT DeviceID FROM HcUserDeviceAppVersion WHERE HcUserID = @UserID)		
				BEGIN
					INSERT INTO HcUserDeviceAppVersion(HcUserID, DeviceID, AppVersion, DateCreated, PrimaryDevice, HcRequestPlatformID)
					VALUES(@UserID, @DeviceID, @AppVersion, GETDATE(), 0, @RequestPlatformID)
					
					SET @AddDeviceToUser = 1
				END			
				
				
					-- To capture User Login.
					INSERT INTO [HcUserFirstUseLogin]
						   ([UserID]
						   ,[FirstUseLoginDate])
					 VALUES
						   (@UserID 
						   ,GETDATE())	
						   
					
					 IF EXISTS (SELECT 1 FROM HcUsersPreferredCityAssociation WHERE HcUserID = @UserID AND HcCityID IS NOT NULL)
						 BEGIN
				                 INSERT INTO #temp
								 SELECT DISTINCT  C.HcCityID cityId
												 ,C.CityName cityName
												
								FROM HcHubCiti H
						INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
						INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
						inner JOIN HcUsersPreferredCityAssociation UC ON C.HcCityID = UC.HcCityID AND UC.HcUserID = @UserID AND UC.HcHubcitiID = @HubCitiID
						WHERE H.HcHubCitiID  = @HubCitiID AND H.HcAppListID = 2 AND UC.HcUserID = @UserID
						ORDER BY C.CityName

						--SELECT 'a'

						

						END

						ELSE
						BEGIN
				            INSERT INTO #temp
							SELECT DISTINCT  C.HcCityID cityId
											,C.CityName cityName
												
								FROM HcHubCiti H
								INNER JOIN HcLocationAssociation LA  ON H.HcHubCitiID = LA.HcHubCitiID 
								INNER JOIN HcCity C ON LA.HcCityID = C.HcCityID
								--LEFT JOIN HcUsersPreferredCityAssociation UC ON  UC.HcHubcitiID = @HubCitiID
								WHERE H.HcHubCitiID  = @HubCitiID 
							--	AND (@UserID IS NULL ) OR (UC.HcUserID = @UserID AND UC.HcCityID IS NULL) OR (@UserID <> UC.HcUserID)
								--OR ( UC.HCUserID = @GuestUserID)
								AND H.HcAppListID = 2
								ORDER BY C.CityName


								--SELECT 'b'

						END
				  					  
				ENd
				ELSE
				BEGIN			
					SELECT @Login = -1
				END
				
				IF EXISTS(SELECT TOP 1 HcUserID  FROM HcUser WHERE HcUserID = @UserID AND FirstUseComplete = 0)
				BEGIN
					SELECT @Result = CASE WHEN (COUNT(UserID)) % 5 = 0 THEN -1 ELSE 0 END
					FROM HcUserFirstUseLogin 
					WHERE UserID = @UserID
				END
				-- BELOW CODED TO HAVE DEFAULT VALUE OF FIRSTUSE STATUS
				ELSE
				BEGIN
				SELECT @Result =0
				END
				
				--Check the reason for failure.
				IF @Login = -1
				BEGIN	
					--If only user name exists then confirm Password is wrong	
						
								
					IF ISNULL(@TEMPUser,0)<>0
					BEGIN
							SET @InvalidPassword = 1				
					END						
					ELSE
					--If username doesnt exist then confirm user is invalid.	
					BEGIN
							SET @InvalidPassword = 0
					END				
				ENd
			  
				  IF (@TempTime > 30 AND @IsTempUser = 1 AND @UserID IS NOT NULL and @TempTime is not null)
				  BEGIN
				
					SET @Login = -2

				  END		
			END
			ELSE
			BEGIN
				SELECT @HubCitiDeactivated = 1
			END
			--select @Login
			SELECT * FROM #temp
			
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcUserLogin.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
