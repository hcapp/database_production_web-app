USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcDealsMaxCntHotDealSearchPagination]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name   : usp_HcHotDealSearchPagination
Purpose                  : To get the list of Hotdeals available.
Example                  : usp_HcHotDealSearchPagination 1, 'o', 0,50

History
Version           Date           Author     Change Description
--------------------------------------------------------------- 
1.0              5/22/2015	SPAN         1.1
---------------------------------------------------------------
*/
 
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcDealsMaxCntHotDealSearchPagination]
(
        @UserID int
      , @Category varchar(max)
      , @Latitude decimal(18,6)    
      , @Longitude decimal(18,6) 
      , @PostalCode varchar(10) 
      , @HubCitiID int
      
	  --OutPut Variable
      , @MaxCnt int output 
    
)
AS
BEGIN

      
      BEGIN TRY
      
            DECLARE @Today datetime = GETDATE()
            DECLARE @CategorySet bit
			DECLARE @UserOutOfRange bit 
			DECLARE @DefaultPostalCode varchar(50)
			DECLARE @DistanceFromUser FLOAT 
			DECLARE @Status bit 
			DECLARE @ErrorNumber int 
			DECLARE @ErrorMessage varchar(1000) 
            
            --Check if the User has selected the any categories
            SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
            FROM HcUserCategory 
            WHERE HcUserID = @UserID            
           
		    DECLARE @LocCategory Int

			SELECT DISTINCT BusinessCategoryID 
			INTO #UserLocBusCat
			FROM HcUserPreferredCategory 
			WHERE HcUserID=@UserID --AND HcHubCitiID =@HubCitiID 

			SELECT @LocCategory=COUNT(1) FROM #UserLocBusCat 


			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
			EXEC [HubCitiApp2_3_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @PostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
			SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
            
			IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
			BEGIN
					--If the user has not passed the co ordinates & postal code then cosidered the user configured postal code.
						IF @PostalCode IS NULL
						BEGIN
							SELECT @Latitude = G.Latitude
									, @Longitude = G.Longitude
							FROM GeoPosition G
							INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
							WHERE U.HcUserID = @UserID
						END
						ELSE
						--If the postal code is passed then derive the co ordinates.
						BEGIN
							SELECT @Latitude = Latitude
									, @Longitude = Longitude
							FROM GeoPosition 
							WHERE PostalCode = @PostalCode
						END
			END            
		
			DECLARE @ByCategoryFlag bit = 0
			DECLARE @FavCatFlag bit = 0
            SELECT @FavCatFlag = CASE WHEN @CategorySet = 1 THEN 0 ELSE 1 END 
            
            DECLARE @Radius INT
				
			SELECT @Radius = LocaleRadius
			FROM HcUserPreference
			WHERE HcUserID = @UserID
				
			IF @Radius IS NULL
			BEGIN
				SELECT @Radius = ScreenContent
				FROM AppConfiguration
				WHERE ScreenName = 'DefaultRadius'
			END
		        
			--To capture hotdeals
            CREATE TABLE #HotDeal(ProductHotDealID INT
                                , HotDealName VARCHAR(300)
                                , City VARCHAR(50)
                                , Category VARCHAR(100)
                                , APIPartnerID INT
                                , APIPartnerName VARCHAR(50)
                                , Price MONEY
                                , SalePrice MONEY
								--, distance float
                                , CategoryID int
                                , HotDealStartdate DATETIME
                                , HotDealEndDate DATETIME
                                , HotDealExpirationDate DATETIME
                                , NewFlag BIT
                                , ExternalFlag BIT
                                )  

			 CREATE TABLE #NoCity(ProductHotDealID INT
                                , HotDealName VARCHAR(300)
                                , City VARCHAR(50)
                                , Category VARCHAR(100)
                                , APIPartnerID INT
                                , APIPartnerName VARCHAR(50)
                                , Price MONEY
                                , SalePrice MONEY
								--, Distance float
                                , CategoryID int
                                , HotDealStartdate DATETIME
                                , HotDealEndDate DATETIME
                                , HotDealExpirationDate DATETIME
                                , NewFlag BIT
                                , ExternalFlag BIT
                                ) 
								
            
                  CREATE TABLE #NoCityUnCategorized(ProductHotDealID INT
                                , HotDealName VARCHAR(300)
                                , City VARCHAR(50)
                                , Category VARCHAR(100)
                                , APIPartnerID INT
                                , APIPartnerName VARCHAR(50)
                                , Price MONEY
                                , SalePrice MONEY
								--,Distance float
                                , CategoryID int
                                , HotDealStartdate DATETIME
                                , HotDealEndDate DATETIME
                                , HotDealExpirationDate DATETIME
                                , NewFlag BIT
                                , ExternalFlag BIT
                                ) 
         
        --To get the HotDeals for which Categories are not mapped, ie. CategoryID is null. 
        --These HotDeal's are grouped under a section called "Others".
            SELECT DISTINCT ProductHotDealID
						  , HotDealName
						  , MIN(City) City
						  , Category
						  , APIPartnerID
						  , APIPartnerName
						  , Price
						  , SalePrice
						  ,Distance
						  , CategoryID categoryId
						  , HotDealStartdate
						  , HotDealEndDate  
						  , HotDealExpirationDate           
            INTO #Others
            FROM (SELECT TOP 100 PERCENT p.ProductHotDealID
                              , HotDealName
                              , 'Others' Category
                              , ISNULL(p.APIPartnerID,0) APIPartnerID
                              , APIPartnerName
                              , isnull(Price,0) Price
                              , SalePrice
                              , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                              , CategoryID categoryId
                              , PL.City
                              , PL.state
                              --, PCC.PopulationCenterID
                              , HotDealStartdate
                              , HotDealEndDate
                              , HotDealExpirationDate
                  FROM ProductHotDeal P
                  INNER JOIN ProductHotDealLocation PL on PL.ProductHotDealID=P.ProductHotDealID
                  LEFT JOIN HcLocationAssociation HL ON HL.PostalCode =PL.PostalCode AND HL.HCHubCitiID=@HubCitiID
				  INNER JOIN HcRetailerAssociation RLC ON HL.HcHubCitiID = RLC.HcHubCitiID AND RLC.HcHubCitiID =@HubCitiID AND Associated =1  --AND RLC.RetailLocationID =PL.RetailLocationID 
                  INNER JOIN Geoposition G ON G.Postalcode=HL.Postalcode                                   
				  LEFT JOIN APIPartner A on A.APIPartnerID=P.APIPartnerID
				  LEFT JOIN HCProductHotDealInterest PHI ON P.ProductHotDealID = PHI.ProductHotDealID AND PHI.HcUserID = @UserID AND Interested = 0	
				  LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = P.ProductHotDealID
				  LEFT JOIN Product P1 ON P1.ProductID = HDP.ProductID
                  WHERE CategoryID IS NULL AND PHI.ProductHotDealID IS NULL
                        AND  GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(P.HotDealEndDate, P.HotDealExpirationDate) , GETDATE() + 1)
                        AND (@Category = 0 OR @Category = '-1')     
				  ORDER BY --CASE WHEN  HL.PostalCode = Pl.PostalCode AND HL.City = 'MARBLE FALLS' AND HL.State = 'TX' THEN 1 ELSE 0 END,
							 CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
                  ) Others             
            WHERE Distance <= @Radius   
            GROUP BY ProductHotDealID
                  , HotDealName 
                  , Category
                  , APIPartnerID
                  , APIPartnerName
                  , Price
                  , SalePrice
                 , Distance
                  , CategoryID
                  , HotDealStartdate
                  , HotDealExpirationDate
                  , HotDealEndDate      
            ORDER BY APIPartnerName,Category, HotDealStartdate ,HotDealName     
            
			IF  EXISTS (SELECT 1 FROM HcHubcitiNationWideDeals WHERE HcHubCitiID=@HubCitiID AND NationWideDealsDisplayFlag =1)
			BEGIN

						
						INSERT INTO #NoCity( ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  --, Distance
							  , CategoryID 
							  , HotDealStartdate
							  , HotDealEndDate 
							  , HotDealExpirationDate)
						
						
						
						--To get the deals that are not associated to city.
						SELECT ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  --, Distance
							  , CategoryID 
							  , HotDealStartdate
							  , HotDealEndDate 
							  , HotDealExpirationDate               
						FROM (
								SELECT DISTINCT TOP 100 PERCENT p.ProductHotDealID
												  , HotDealName
												  , Category = ISNULL(C.ParentCategoryName, P.Category)
												  , 'No City' City
												  , ISNULL(p.APIPartnerID,0) APIPartnerID
												  , APIPartnerName
												  , isnull(Price,0) Price
												  , SalePrice
												  --, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
												  , P.CategoryID 
												 , HotDealStartdate
												 , HotDealEndDate
												 , HotDealExpirationDate
								FROM ProductHotDeal P
								INNER JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
								--INNER JOIN UserCategory UC ON UC.CategoryID = P.CategoryID
								INNER JOIN Category C ON P.CategoryID = C.CategoryID
								LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN Product P1 ON HDP.ProductID = P1.ProductID
								WHERE GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(P.HotDealEndDate, P.HotDealExpirationDate) , GETDATE() + 1)
								--(ISNULL(P.HotDealStartDate,@Today)<= @Today AND ISNULL(P.HotDealEndDate,@Today) >= @Today)
								AND  (((@CategorySet = 1) AND P.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID))
										 OR
									  (@CategorySet = 0 AND 1 = 1))
								AND PL.City IS NULL
								AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM HcProductHotDealInterest WHERE HcUserID = @UserID AND Interested = 0)					
								) Deals    
								ORDER BY CASE WHEN APIPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
		            
            
						PRINT 'A'
						--To get the deals that are not associated to city & categories.
						
						INSERT INTO #NoCityUnCategorized (ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  --, Distance
							  , CategoryID
							  , HotDealStartdate
							  , HotDealEndDate   
							  , HotDealExpirationDate)
						
						
						
						SELECT ProductHotDealID
							  , HotDealName
							  , Category
							  , City
							  , APIPartnerID
							  , APIPartnerName
							  , Price
							  , SalePrice
							  --, Distance
							  , CategoryID
							  , HotDealStartdate
							  , HotDealEndDate   
							  , HotDealExpirationDate 
						FROM (
								SELECT DISTINCT TOP 1000 p.ProductHotDealID
												  , HotDealName
												  , 'Others' Category
												  , 'No City' City
												  , ISNULL(p.APIPartnerID,0) APIPartnerID
												  , APIPartnerName
												  , isnull(Price,0) Price
												  , SalePrice
												  --, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
												  , P.CategoryID
												  , HotDealStartdate
												  , HotDealEndDate 
												  , HotDealExpirationDate
								FROM ProductHotDeal P
								--INNER JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
								--INNER JOIN UserCategory UC ON UC.CategoryID = P.CategoryID            
								LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
								LEFT JOIN HcProductHotDealInterest PHI ON P.ProductHotDealID = PHI.ProductHotDealID AND PHI.HcUserID = @UserID AND Interested = 0
								LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = P.ProductHotDealID
								LEFT JOIN Product P1 ON P1.ProductID = HDP.ProductID
								WHERE P.CategoryID IS NULL AND PHI.ProductHotDealID IS NULL
								AND GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(P.HotDealEndDate, P.HotDealExpirationDate) , GETDATE() + 1)
								--AND (ISNULL(P.HotDealStartDate,@Today)<= @Today AND ISNULL(P.HotDealEndDate,@Today) >= @Today)
								AND(((@CategorySet = 1) AND P.CategoryID IN (SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID))
										 OR
									  (@CategorySet = 0 AND 1 = 1)
										 OR
										(@CategorySet = 1 AND P.CategoryID IS NULL))
								AND PL.City IS NULL
								AND (@Category = 0 OR @Category = '-1')
								--AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)					
								) Deals  
								ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
				END
                  

        --To enable/disable "By Category" button.       
			IF EXISTS( SELECT 1 FROM Category C
						INNER JOIN ProductHotDeal P on p.CategoryID=C.CategoryID
						INNER JOIN HcUserCategory UC ON UC.CategoryID=C.CategoryID
						WHERE P.HotDealStartDate<= @Today AND P.HotDealEndDate >= @Today
						AND HcUserID=@UserID)
            BEGIN
                  SET @ByCategoryFlag = 0 
            END
            ELSE 
                  SET @ByCategoryFlag = 1
                  
                   
            
                                                                          
                        --To get Hot Deal info
                        SELECT  Row_Num = ROW_NUMBER() OVER( ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
                                                                                                                                    ,apiPartnerName
                                                                                                                                    ,Category
                                                                                                                                  ,HotDealStartdate
                                                                                                                                  ,HotDealName)
                                    , ProductHotDealID
                                    , HotDealName
                                    , MIN(City) City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    --, Distance
                                    , CategoryID categoryId
                                    , HotDealStartdate
                                    , HotDealEndDate
                                    , HotDealExpirationDate
                        INTO #HD
                        FROM (SELECT DISTINCT HD.ProductHotDealID 
                                          , HD.HotDealName
                                          , ISNULL(C.ParentCategoryName, 'Others') AS Category
                                          , PL.City 
                                          , ISNULL(AP.APIPartnerID,0) APIPartnerID
                                          , AP.APIPartnerName 
                                          , isnull(Price,0) Price
                                          , SalePrice
                                          , C.CategoryID categoryId
                                          , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                                         , HotDealStartdate
                                         , HotDealEndDate
                                         , HotDealExpirationDate
                                FROM ProductHotDeal HD
                                INNER JOIN Category C ON C.CategoryID = HD.CategoryID 
                                INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID
                                LEFT JOIN HcLocationAssociation HL ON HL.HCHubCitiID=@HubCitiID AND PL.City=HL.City AND PL.State=HL.State  
								LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID = HL.HcHubCitiID AND RLC.HcHubCitiID = @HubCitiID AND Associated =1 --AND RLC.RetailLocationID =PL.RetailLocationID 
                                INNER JOIN Geoposition G ON G.Postalcode=HL.Postalcode 								
                                INNER JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID  
								LEFT JOIN HotDealProduct HDP ON HDP.ProductHotDealID = HD.ProductHotDealID
								LEFT JOIN Product P ON P.ProductID = HDP.ProductID
								--LEFT JOIN RetailerBusinessCategory RB ON RB.RetailerID =HD.RetailID 
								--LEFT JOIN #UserLocBusCat US ON (US.BusinessCategoryID =RB.BusinessCategoryID )                            
                                WHERE  -- (@LocCategory =0 OR (US.BusinessCategoryID IS NOT NULL AND US.BusinessCategoryID =RB.BusinessCategoryID AND RB.RetailerID =HD.RetailID))
									      (
                                                      ((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
                                                      OR 
                                                      ((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
                                                )
                                          AND (((@CategorySet = 1) AND HD.CategoryID IN(SELECT CategoryID FROM HcUserCategory WHERE HcUserID = @UserID))
                                                      OR
                                                ((@CategorySet = 0) AND 1 = 1))
                                      -- AND (
                                                --(LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP))
                                                
                                          --  )
                                       AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM HcProductHotDealInterest WHERE HcUserID = @UserID AND Interested = 0)
                                       AND GETDATE() BETWEEN ISNULL(HD.HotDealStartDate, GETDATE() - 1) AND ISNULL(ISNULL(HD.HotDealEndDate, HD.HotDealExpirationDate) , GETDATE() + 1)
                                       --AND ISNULL(HD.HotDealStartDate,@Today)<= @Today AND ISNULL(HD.HotDealEndDate,@Today) >= @Today 
                                ) HotDeal
																						
                        WHERE Distance <= @Radius         --To Filter Koala Hot Deals.
                                    --AND APIPartnerID != @KoalaPartnerID
                        GROUP BY ProductHotDealID
                                    , HotDealName 
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    --, Distance
                                    , CategoryID
                                    , HotDealStartdate
									, HotDealEndDate  
									, HotDealExpirationDate                
                        ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASC
                        print 'b'
                        IF ISNULL(@Category,0) <> '0' AND ISNULL(@Category,0) <> '-1'
                        BEGIN
                              print 'Lat Long, Category'
                              INSERT INTO #HotDeal (ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            --, Distance  
                                                            , CategoryID
                                                            , HotDealStartdate 
                                                            , HotDealEndDate 
                                                            , HotDealExpirationDate
                                                            , NewFlag
                                                            , ExternalFlag)
                             SELECT 
                                           ProductHotDealID
                                          , HotDealName
                                          , City
                                          , Category
                                          , APIPartnerID
                                          , APIPartnerName
                                          , Price
                                          , SalePrice
                                          --, Distance 
                                          , CategoryID categoryId
                                          , HotDealStartdate
                                          , HotDealEndDate
                                          , HotDealExpirationDate
                                          , NewFlag = CASE WHEN (DATEDIFF(DD, HotDealStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
                                                              , ExternalFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
                              FROM (
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #HD                                              
                                          UNION                                                 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCity N
                                          INNER JOIN [Version8].fn_SplitParam(@Category, ',')C ON C.Param = N.CategoryID) HD
                              
                        END 
                                                        
                        IF ISNULL(@Category,0) = '0'
                        BEGIN
                              print 'Lat Long, No Category'
                              --To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
                              INSERT INTO #HotDeal (ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            --, Distance  
                                                            , CategoryID
                                                            , HotDealStartdate
                                                            , HotDealEndDate
                                                            , HotDealExpirationDate
                                                            , NewFlag
                                                            , ExternalFlag)
                              SELECT ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    --, Distance 
                                    , CategoryID categoryId
                                    , HotDealStartdate
                                    , HotDealEndDate
                                    , HotDealExpirationDate
                                    , NewFlag = CASE WHEN (DATEDIFF(DD, HotDealStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
                                                      , ExternalFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
                              FROM 
                                    (SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , CategoryID categoryId
                                                --, Distance
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #HD
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #Others
                                          UNION
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCity
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCityUnCategorized
                                    ) HotDeals 
                              
                        END                           
                        IF ISNULL(@Category,0) = '-1'
                        BEGIN
                              print 'Uncategorised only'
                              --To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
                              INSERT INTO #HotDeal (ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            --, Distance  
                                                            , CategoryID
                                                            , HotDealStartdate
                                                            , HotDealEndDate
                                                            , HotDealExpirationDate
                                                            , NewFlag
                                                            , ExternalFlag)
                              SELECT ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    --, Distance 
                                    , CategoryID categoryId
                                    , HotDealStartdate
                                    , HotDealEndDate
                                    , HotDealExpirationDate
                                    , NewFlag = CASE WHEN (DATEDIFF(DD, HotDealStartDate, GETDATE()) <= 2) THEN 1 ELSE 0 END
                                                      , ExternalFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
                              FROM  
                                    (SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #Others                                         
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                --, Distance 
                                                , CategoryID categoryId
                                                , HotDealStartdate
                                                , HotDealEndDate
                                                , HotDealExpirationDate
                                          FROM #NoCityUnCategorized
                                    ) HotDeals 
                        END          
                                                  
                                 SELECT DISTINCT H.ProductHotDealID
                                                      ,Used = COUNT(HcUserHotDealGalleryID)
                                 INTO #TotalCounts
                                 FROM #HotDeal H
                                 LEFT JOIN HcUserHotDealGallery UG ON H.ProductHotDealID = UG.HotDealID
                                 GROUP BY H.ProductHotDealID
                                    
                     
									 
					   SELECT rowNumber 
							, hotDealId
							, hotDealName
							, City
							, categoryName
							, apiPartnerId
							, apiPartnerName
							, hDPrice
							, hDSalePrice
							--, Distance      distance
							, categoryId
							, hDStartDate
							, hDEndDate
							, NewFlag
							, extFlag
							, Used
                        INTO #Hdeals
                        FROM(                        
                                       SELECT  rowNumber = ROW_NUMBER() OVER(ORDER BY CASE WHEN apiPartnerName = 'ScanSee' THEN 0 ELSE 1 END ASc
                                                                                                                                    ,apiPartnerName
                                                                                                                                    ,H.Category
                                                                                                                                    ,NewFlag DESC
                                                                                                                                    ,H.HotDealName)
                                                , H.ProductHotDealID hotDealId
                                                , H.HotDealName hotDealName
                                                , City
                                                , H.Category categoryName
                                                , H.APIPartnerID apiPartnerId
                                                , APIPartnerName apiPartnerName
                                                , H.Price hDPrice
                                                , H.SalePrice hDSalePrice
                                                --, Distance      distance
                                                , H.CategoryID categoryId
                                                , H.HotDealStartdate hDStartDate
                                                , H.HotDealEndDate hDEndDate
                                                , NewFlag
                                                , ExternalFlag extFlag
                                                --,COUNT(UserHotDealGalleryID) Used
                                                ,T.Used
                                          FROM #HotDeal H
                                          INNER JOIN #TotalCounts T ON H.ProductHotDealID = T.ProductHotDealID
                                          LEFT JOIN ProductHotDeal P ON H.ProductHotDealID = P.ProductHotDealID 
                                          LEFT JOIN HcUserHotDealGallery UH ON H.ProductHotDealID = UH.HotDealID AND UH.HcUserID = @UserID
                                          WHERE ISNULL(ISNULL(H.HotDealEndDate, H.HotDealExpirationDate), GETDATE() + 1) >= GETDATE()
                                          AND ISNULL(UH.Used, 0) = 0 
                                          AND ((NOT EXISTS(SELECT 1 FROM HcUserHotDealGallery WHERE UH.HotDealID = P.ProductHotDealID AND UH.HcUserID = @UserID AND Used = 1) AND ISNULL(P.NoOfHotDealsToIssue, T.Used+1) > T.Used)
                                                      OR (EXISTS(SELECT 1 FROM HcUserHotDealGallery WHERE UH.HotDealID = P.ProductHotDealID AND UH.HcUserID = @UserID AND Used = 0)))
										  GROUP BY  H.ProductHotDealID 
                                                            , H.HotDealName 
                                                            , City
                                                            , H.Category 
                                                            , H.APIPartnerID 
                                                            , APIPartnerName 
                                                            , H.Price 
                                                            , H.SalePrice 
                                                            , H.CategoryID 
                                                            , H.HotDealStartdate
                                                            , H.HotDealEndDate
                                                            , NewFlag
                                                            , ExternalFlag
                                                            , P.NoOfHotDealsToIssue
                                                            , T.Used
                                          )HDeals
                                    
                --To capture max row number.
                SELECT @MaxCnt = MAX(rowNumber) FROM #HDeals

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_usp_HcDealsMaxCntHotDealSearchPagination].'		
		END;
		 
	END CATCH;
END;





















GO
