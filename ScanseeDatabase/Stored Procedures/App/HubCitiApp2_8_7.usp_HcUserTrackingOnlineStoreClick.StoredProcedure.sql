USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingOnlineStoreClick]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcUserTrackingOnlineStoreClick  
Purpose               : Capture the CLick on the Google Retailer.
Example               : usp_HcUserTrackingOnlineStoreClick  
  
History  
Version    Date            Author            Change Description  
---------------------------------------------------------------   
1.0        9thDec2013      Dhananjaya TR     Initial Version
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingOnlineStoreClick]  
(  
   @OnlineProductDetailID INT 
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY
 
	BEGIN TRANSACTION 
	 
         --Update Google Retailer Click Information. 
		 UPDATE HubCitiReportingDatabase..OnlineProductDetail  SET WebClick =1
		 WHERE OnlineProductDetailID  = @OnlineProductDetailID 
		 
	  --Confirmation of Success.
	  SELECT @Status=0	 
	COMMIT TRANSACTION	 	
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingOnlineStoreClick.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
   ROLLBACK TRANSACTION;
   --Confirmation of failure.
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;














































GO
