USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiUserForgotPassword]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHubCitiUserForgotPassword
Purpose					: To Display Password For selected Username.
Example					: usp_HcHubCitiUserForgotPassword

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15/10/2013	    Dhananjaya TR	1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiUserForgotPassword]
(
    --Input variable.
	  @HubCitiID int
    , @UserName Varchar(1000)    
	  
	--Output Variable 
    , @Status int output
    , @Success int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Select user details for selected username
		IF EXISTS (SELECT 1 FROM HcUser A INNER JOIN HcUserDeviceAppVersion B ON A.HcUserID = B.HcUserID AND B.HcHubCitiID = @HubCitiID WHERE UserName = @UserName)
		BEGIN
			SELECT Password
			      ,USerName
			      ,Email			
			From HcUser A
			INNER JOIN HcUserDeviceAppVersion B ON A.HcUserID = B.HcUserID AND B.HcHubCitiID = @HubCitiID
			WHERE UserName like @UserName 
			--Confirmation of	Success
			SELECT @Success = 0
		END
		ELSE
		BEGIN
			SELECT @Success = 1
		END		
		
		--Confirmation of	Success
		SELECT @Status = 0				
				
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_HcHubCitiUserForgotPassword.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











































GO
