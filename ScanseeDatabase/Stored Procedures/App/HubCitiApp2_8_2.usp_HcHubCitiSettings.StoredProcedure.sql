USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiSettings]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcHubCitiSettings]
Purpose					: To provide setting information.
Example					: [usp_HcHubCitiSettings]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			12/05/2014	    Span            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiSettings]
(   
    --Input variable.	
	  @HcUserID int
	, @HubCitiID int
	
	--Output Variable 
	, @FaqExists int output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			SET @FaqExists = 0

			IF (SELECT DISTINCT 1 FROM HcFAQ WHERE HcHubCitiID = @HubCitiID) > 0
			BEGIN

					SET @FaqExists = 1

			END

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcHubCitiSettings].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





























GO
