USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFindOptionsListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcFindOptionsListDisplay]
Purpose                 :  To get the list of filter options for Find Module.
Example                 :  [usp_HcFindOptionsListDisplay]
------------------------------------------------------------------------------- 
History
Version       Date           Author          Change Description
1.0          5/05/2015       SPAN            1.1
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFindOptionsListDisplay]
(

      --Input variable
         @UserID INT  
       , @CategoryName varchar(250)
       , @Latitude Decimal(18,6)
       , @Longitude  Decimal(18,6)
       , @HcHubCitiID INT
       , @HcMenuItemID INT
       , @HcBottomButtonID INT
	   , @SearchKey Varchar(1000)

      --Output Variable 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY      
				
			--	select getdatE()
				    DECLARE @CategoryName1 VARCHAR(25) = @CategoryName,
					 @HcMenuItemID1 INT = @HcMenuItemID,
					 @HcBottomButtonID1 INT =  @HcBottomButtonID,
					 @HcHubCitiID1 INT =  @HcHubCitiID,
					 @UserID1 INT = @UserID


					DECLARE @GetDate DATETIME = GETDATE()

					DECLARE @Radius int                  
                    , @Tomorrow DATETIME = @GetDate + 1
                    , @Yesterday DATETIME = @GetDate - 1
                    , @PostalCode varchar(10)
                    , @DistanceFromUser FLOAT
                    , @UserLatitude float
                    , @UserLongitude float 
                    , @RegionAppFlag Bit
                    , @GuestLoginFlag BIT=0
					, @BusinessCategoryID Int
					, @UserOutOfRange bit   
				    , @DefaultPostalCode varchar(50)              
				    , @MaxCnt int


					--SearchKey implementation
					DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

					SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
							WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
							WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
							ELSE @SearchKey END)


					SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName1
					
					DECLARE @IsSubCategory bit = 0
					SELECT @IsSubCategory = 1
					FROM HcBusinessSubCategoryType T
					INNER JOIN HcBusinessSubCategory S ON T.HcBusinessSubCategoryTypeID = S.HcBusinessSubCategoryTypeID
					WHERE T.BusinessCategoryID = @BusinessCategoryID

					--SET @HcBottomButtonID1 = IIF(@HcBottomButtonID1 IS NOT NULL,0,Null)

                    CREATE TABLE #CityList(CityID Int,CityName Varchar(200))

                    IF (SELECT 1 FROM HcUser WHERE UserName ='guestlogin' AND HcUserID = @UserID1) > 0
                    BEGIN
                    SET @GuestLoginFlag = 1
                    END

					CREATE TABLE #RHubCitiList(HcHubCitiID Int) 

                    IF(SELECT 1 from HcHubCiti H
						INNER JOIN HcAppList AL ON H.HcAppListID = AL.HcAppListID 
						AND H.HcHubCitiID = @HcHubCitiID1 AND AL.HcAppListName = 'RegionApp')>0
                            BEGIN
									SET @RegionAppFlag = 1
										    
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HcHubCitiID1 
									UNION ALL
									SELECT  @HcHubCitiID1 AS HcHubCitiID
									)
                            END
                    ELSE
                            BEGIN
									SET @RegionAppFlag =0
									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HcHubCitiID1                                     
                            END

                     --To Fetch region app associated citylist
						DECLARE @HcCityID varchar(100) = null                   
						INSERT INTO #CityList(CityID,CityName)
                                  
						SELECT DISTINCT  C.HcCityID 
										,C.CityName                              
						FROM #RHubcitiList H
						INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HcHubCitiID1 
						INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID 
						LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =C.HcCityID
						LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1  AND UP.HcHubcitiID =H.HchubcitiID                                                      
						--WHERE --(@HcCityID IS NULL AND (UP.HcUsersPreferredCityAssociationID IS NULL)  
						----OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1)                                        
						----OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID1)
                        
						--(@HcCityID IS NULL AND (UP.HcUsersPreferredCityAssociationID IS NOT NULL AND ((C.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1) OR (UP.HcCityID IS NULL AND UP.HcUserID = @UserID1))) OR (UP.HcUsersPreferredCityAssociationID IS NULL)) 
						--OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param 
						
							WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
						OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param )
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID)                                        
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID)
					
						UNION
						SELECT 0,'ABC'

                        SELECT @UserLatitude = @Latitude
                            , @UserLongitude = @Longitude

                        IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcUser A
                                    INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                                    WHERE HcUserID = @UserID1 
                        END
                        --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                        IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcHubCiti A
                                    INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                                    WHERE A.HcHubCitiID = @HcHubCitiID1
                        END

                                                      
                        --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
						EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
						SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
 
						--To fetch all the duplicate retailers.
						SELECT DISTINCT DuplicateRetailerID 
						INTO #DuplicateRet
						FROM Retailer 
						WHERE DuplicateRetailerID IS NOT NULL
                           
                        --Derive the Latitude and Longitude in the absence of the input.
                        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
                        BEGIN
                                    --If the postal code is passed then derive the co ordinates.
                                    IF @PostalCode IS NOT NULL
                                    BEGIN
                                            SELECT @Latitude = Latitude
                                                    , @Longitude = Longitude
                                            FROM GeoPosition 
                                            WHERE PostalCode = @PostalCode
                                    END          
                                    ELSE
                                    BEGIN
                                            SELECT @Latitude = G.Latitude
                                                    , @Longitude = G.Longitude
                                            FROM GeoPosition G
                                            INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                            WHERE U.HcUserID = @UserID1 
                                    END                                                                               
                        END    
                     
                        --Get the user preferred radius.
						SELECT @Radius = LocaleRadius
                        FROM HcUserPreference 
                        WHERE HcUserID = @UserID1
                                         
                        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
															WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))  
                    
						--CREATE TABLE #BusinessCategory(BusinessCategoryID int, HcBusinessSubCategoryID int)

						DECLARE @BusinessCategory TABLE(BusinessCategoryID int, HcBusinessSubCategoryID int)
						IF @HcMenuItemID1 IS NOT NULL
						BEGIN
							INSERT INTO @BusinessCategory(BusinessCategoryID, HcBusinessSubCategoryID)
							SELECT DISTINCT F.BusinessCategoryID, FB.HcBusinessSubCategoryID -- FB.HcBusinessSubCategoryID--,FBC.HcBusinessSubCategoryID)                
							FROM  BusinessCategory F                               
							INNER JOIN HcMenuFindRetailerBusinessCategories FB ON F.BusinessCategoryID=FB.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemID1
							WHERE F.BusinessCategoryName = @CategoryName1 AND (FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID)
                        END
						
						ELSE IF @HcBottomButtonID1 IS NOT NULL OR @HcBottomButtonID1 = 0
						BEGIN                     					   
							INSERT INTO @BusinessCategory(BusinessCategoryID, HcBusinessSubCategoryID)
							SELECT DISTINCT F.BusinessCategoryID, FBC.HcBusinessSubCategoryID --HcBusinessSubCategoryID = ISNULL(,FBC.HcBusinessSubCategoryID)                
							FROM  BusinessCategory F                               
							INNER JOIN HcBottomButtonFindRetailerBusinessCategories FBC ON F.BusinessCategoryID=FBC.BusinessCategoryID AND FBC.HcBottomButonID=@HcBottomButtonID1                             
							WHERE F.BusinessCategoryName = @CategoryName1 AND (FBC.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID = FBC.BusinessCategoryID)     
						END    
						
				
						

			CREATE TABLE [dbo].#tempre(
			[RetailID] [int] NOT NULL,
			[RetailName] [varchar](1000) NULL,
			[RetailLocationID] [int] NOT NULL,
			[Address1] [varchar](150) NULL,
			[City] [varchar](50) NULL,
			[State] [char](2) NULL,
			[PostalCode] [varchar](10) NULL,
			[Headquarters] [bit] NOT NULL,
			[Active] [bit] NULL,
			[RetailLocationLatitude] [float] NULL,
			[RetailLocationLongitude] [float] NULL,
			[RetailLocationImagePath] [varchar](1000) NULL,
			[WebsiteSourceFlag] [bit] NOT NULL,
			[RetailerImagePath] [varchar](255) NULL,
			[SaleFlag] [int] NOT NULL,
			[HchubcitiID] [int] NULL
			)

			insert  into #tempre
			SELECT distinct   R.RetailID     
			, R.RetailName
			, RL.RetailLocationID 
			, RL.Address1     
			, RL.City
			, RL.State
			, RL.PostalCode,Headquarters,Active
			,RetailLocationLatitude,RetailLocationLongitude,RetailLocationImagePath,WebsiteSourceFlag,RetailerImagePath
			, SaleFlag = 0 ,HL.HchubcitiID --into #tempre
			FROM Retailer R 
			INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID AND RetailerActive = 1 
			AND RBC.BusinessCategoryID = @BusinessCategoryID
			INNER JOIN @BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
			INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
			INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID  AND RL.HcCityID = HL.HcCityID
			INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
			INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID ) OR (@RegionAppFlag =0 AND CL.CityID=0)

			--select * from #tempre where RetailLocationid  =2128695
						                                  
                        CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                            , RetailID INT 
                                            --, RetailName VARCHAR(1000)
                                            , RetailLocationID INT
                                            --, Address1 VARCHAR(1000)
                                            , City VARCHAR(1000)    
                                           -- , State CHAR(2)
                                           -- , PostalCode VARCHAR(20)
                                            , retLatitude FLOAT
                                            , retLongitude FLOAT
                                            , Distance FLOAT  
                                            , DistanceActual FLOAT                                                   
                                            , SaleFlag int
                                            , HcBusinessSubCategoryID INT
                                            , BusinessSubCategoryName VARCHAR(1000))

						
					IF (@CategoryName1 IS NOT NULL)
					BEGIN
                        IF EXISTS (SELECT 1 FROM @BusinessCategory WHERE HcBusinessSubCategoryID IS NULL)
                        BEGIN    
				                                                   
								INSERT INTO #Retail(RetailID   
													--, RetailName  
													, RetailLocationID  
													--, Address1  
													, City  
													--, State  
													--, PostalCode  
													, retLatitude  
													, retLongitude  
													, Distance   
													, DistanceActual                                               
													, SaleFlag  
													, HcBusinessSubCategoryID  
													, BusinessSubCategoryName)
                                    SELECT DISTINCT RL.RetailID     
															--, R.RetailName
															, RL.RetailLocationID 
															--, RL.Address1     
															, RL.City
															--, RL.State
															--, RL.PostalCode
															, retLatitude=0 --ISNULL(RL.RetailLocationLatitude, G.Latitude)
															, retLongitude=0.0 --ISNULL(RL.RetailLocationLongitude, G.Longitude)
															, Distance = 0 --ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															, DistanceActual = 0.0--ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
															, SaleFlag = 0   
															, HcBusinessSubCategoryID = null
															, BusinessSubCategoryName = null
											FROM #tempre RL
                                            INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RL.HchubcitiID AND Associated =1 
                                            LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                            LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                            --LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                            -----------to get retailer that have products on sale                                            
                                            LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
                                            LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID   
											WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1
                                            --AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                            AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))

												
											
                                                 
						END
						ELSE
						BEGIN
								INSERT INTO #Retail(RetailID   
													--, RetailName  
													, RetailLocationID  
													--, Address1  
													, City  
													--, State  
													--, PostalCode  
													, retLatitude  
													, retLongitude  
													, Distance   
													, DistanceActual                                               
													, SaleFlag  
													, HcBusinessSubCategoryID  
													, BusinessSubCategoryName)
                                     SELECT DISTINCT RL.RetailID     
															--, R.RetailName
															, RL.RetailLocationID 
															--, RL.Address1     
															, RL.City
															--, RL.State
															--, RL.PostalCode
															, retLatitude=0 --ISNULL(RL.RetailLocationLatitude, G.Latitude)  retLatitude
															, retLongitude=0 --ISNULL(RL.RetailLocationLongitude, G.Longitude) retLongitude
															, Distance = 0 --ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															, DistanceActual = 0--ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
															, SaleFlag = 0   
															, HcBusinessSubCategoryID = SB.HcBusinessSubCategoryID
															, BusinessSubCategoryName = BusinessSubCategoryName
											FROM #tempre RL
											INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RL.HchubcitiID  AND Associated =1 
											INNER JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
											INNER JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
											--LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
											-----------to get retailer that have products on sale                                            
											LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
											LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID             
											WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL AND RL.Active = 1
											--AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
											AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
									--	select getdatE()   
											
						END
					END
					ELSE IF (@CategoryName1 IS NULL)
					BEGIN

					insert  into #tempre
			SELECT distinct   R.RetailID     
			, R.RetailName
			, RL.RetailLocationID 
			, RL.Address1     
			, RL.City
			, RL.State
			, RL.PostalCode,Headquarters,Active
			,RetailLocationLatitude,RetailLocationLongitude,RetailLocationImagePath,WebsiteSourceFlag,RetailerImagePath
			, SaleFlag = 0 ,HL.HchubcitiID --into #tempre
					FROM Retailer R 
					INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID   AND RetailerActive = 1
					INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode  
					INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HcHubCitiID1  
					INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0)
  
						
						INSERT INTO #Retail(RetailID   
											--, RetailName  
											, RetailLocationID  
											--, Address1  
											, City  
											--, State  
											--, PostalCode  
											, retLatitude  
											, retLongitude  
											, Distance   
											, DistanceActual                                               
											, SaleFlag  
											, HcBusinessSubCategoryID  
											, BusinessSubCategoryName)
                                  
                                    SELECT DISTINCT RL.RetailID     
													--, R.RetailName
													, RL.RetailLocationID 
													--, RL.Address1     
													, RL.City
													--, RL.State
													--, RL.PostalCode
													, retLatitude=0 --ISNULL(RL.RetailLocationLatitude, G.Latitude)
													, retLongitude=0.0 --ISNULL(RL.RetailLocationLongitude, G.Longitude)
													, Distance = 0--ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
													, DistanceActual = 0.0--ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
													, SaleFlag = 0   
															, HcBusinessSubCategoryID = SB.HcBusinessSubCategoryID
															, BusinessSubCategoryName = BusinessSubCategoryName
									FROM #tempre RL 
                                    
                                    INNER JOIN HcHubCiti HC ON HC.HcHubCitiID=RL.HcHubCitiID AND HC.HcHubCitiID=RL.HchubcitiID  
                                    INNER JOIN HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = RL.HchubcitiID   AND Associated =1
									left JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
									left JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID   
                                    LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
                                    LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID   
									WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1
                                    --AND (RSC.HcBusinessSubCategoryID = @BusinessSubCategoryID OR (@BusinessSubCategoryID IS NULL OR @BusinessSubCategoryID = 0 OR @BusinessSubCategoryID =-1))
                                    AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
										OR (@SearchKey IS NULL))
									
					END

					--select 'aa' as a,* from #Retail where retaillocationid = 2128695
					
						-- To identify Retailer that have products on Sale or any type of discount
                        SELECT DISTINCT Retailid , RetailLocationid
                        INTO #RetailItemsonSale1
                        FROM 
							(
							SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
							FROM Coupon C 
							INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
							INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
							LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
							WHERE @GetDate BETWEEN CouponStartDate AND CouponExpireDate
							GROUP BY C.CouponID
									,NoOfCouponsToIssue
									,CR.RetailID
									,CR.RetailLocationID
							HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
										ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
							) Discount 

							CREATE  INDEX IX_C ON #Retail(Retailid)

						

						--	CREATE  INDEX IX_C1 ON RetailLocation(Retailid)

							select   distinct  RL.RetailLocationLatitude,RL.RetailLocationLongitude,PostalCode,R.* INTO #FIN 
							from #Retail R
							INNER JOIN #tempre RL ON R.RetailLocationID=RL.RetailLocationID-- and 



							--select 's' as s,* from #FIN where retaillocationid =2128695

							update #FIN 
							set retLatitude = ISNULL(RetailLocationLatitude, G.Latitude) 
							, retLongitude = ISNULL(RetailLocationLongitude, G.Longitude)
							, Distance = ROUND((ACOS((SIN(ISNULL(RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
							, DistanceActual = ROUND((ACOS((SIN(ISNULL(RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       						                                               
							
							from #FIN r
							left JOIN GeoPosition G ON G.PostalCode = R.PostalCode -- and g.city = r.City   
					 
							--select * from #FIN where retaillocationid =2128695


							--select @Radius
								
							select retailid into #RetailLocationn from retaillocation rl
							
					   SELECT RowNum
							, RetailID   
							--, RetailName  
							, RetailLocationID  
							--, Address1  
							, City  
							--, State  
							--, PostalCode  
							, retLatitude  
							, retLongitude  
							, Distance --= ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
							, DistanceActual-- = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       						                                               
							, SaleFlag --= CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
							, HcBusinessSubCategoryID  
							, BusinessSubCategoryName
					   INTO #Retail1
					   FROM
						(SELECT RowNum
							, R.RetailID   
							--, R.RetailName  
							, R.RetailLocationID  
							--, R.Address1  
							, R.City  
							--, R.State  
							--, R.PostalCode  
							, retLatitude --= ISNULL(R.RetailLocationLatitude, G.Latitude) 
							, retLongitude --= ISNULL(R.RetailLocationLongitude, G.Longitude)
							, Distance --= ROUND((ACOS((SIN(ISNULL(R.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(R.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(R.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
							, DistanceActual --= ROUND((ACOS((SIN(ISNULL(R.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(R.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(R.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       						                                               
							, SaleFlag = CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
							, HcBusinessSubCategoryID  
							, BusinessSubCategoryName
					
					 FROM  #FIN R 
					 LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID
			        
					 WHERE r.Distance <= @Radius --and 1<>1
					)R
				
					--select * from #retail1 where RetailLocationid =2128695

                        --To capture max row number.  
                        SELECT @MaxCnt = MAX(RowNum) FROM #Retail1

						--To display Filter items
						DECLARE @Distance bit = 1
						DECLARE @Alphabetically bit = 1
						DECLARE @LocalSpecials bit = 0
						DECLARE @Category bit = 0
						DECLARE @Options bit = 0
						DECLARE @City bit = 0
						DECLARE @Interests bit = 0
					
						SELECT @LocalSpecials = 1 --IIF(MAX(ISNULL(SaleFlag,0)) = 1,1,0)
						FROM #Retail1
						WHERE SaleFlag = 1


						IF (@CategoryName IS NOT NULL)
						BEGIN
						SELECT @Category = 1 FROM #Retail1 WHERE HcBusinessSubCategoryID IS NOT NULL AND @IsSubCategory = 1
						END
						--ELSE IF (@CategoryName IS NULL)
						--BEGIN
						--SELECT @Category = 1 FROM #Retail1 WHERE HcBusinessSubCategoryID IS NOT NULL
						--END

						SELECT @City = 1 FROM #Retail1 WHERE City IS NOT NULL AND (@RegionAppFlag = 1)

						SELECT @Interests = 1 
						FROM #Retail1 R
						INNER JOIN HcFilterRetailLocation RF ON R.RetailLocationID = RF.RetailLocationID
						INNER JOIN HcFilter F ON RF.HcFilterID = F.HcFilterID
						WHERE F.HcHubCitiID = @HcHubCitiID1

						SELECT @Options = 1
						FROM #Retail1 R
						INNER JOIN RetailerFilterAssociation F ON R.RetailLocationID = F.RetailLocationID AND F.BusinessCategoryID = @BusinessCategoryID
						AND F.AdminFilterID IS NOT NULL AND F.AdminFilterValueID IS NULL

						SELECT DISTINCT F.FilterName  AS FilterName
						INTO #Filterlist
						FROM #Retail1 R
						INNER JOIN RetailerFilterAssociation RF ON R.RetailLocationID = RF.RetailLocationID
						INNER JOIN AdminFilterValueAssociation A ON RF.AdminFilterValueID = A.AdminFilterValueID AND RF.BusinessCategoryID = @BusinessCategoryID
						INNER JOIN AdminFilterValue AV ON A.AdminFilterValueID = AV.AdminFilterValueID
						INNER JOIN AdminFilter F ON A.AdminFilterID = F.AdminFilterID AND RF.AdminFilterID = F.AdminFilterID
						ORDER BY FilterName
					
						DECLARE @Temp TABLE(Type Varchar(50), Items Varchar(50),Flag bit)
						INSERT INTO @Temp VALUES ('Sort Items by','Distance',@Distance)
						INSERT INTO @Temp VALUES ('Sort Items by','Alphabetically',@Alphabetically)

						INSERT INTO @Temp VALUES ('Filter Items by','Local Specials !',@LocalSpecials)
						INSERT INTO @Temp VALUES ('Filter Items by','Category',@Category)
						INSERT INTO @Temp VALUES ('Filter Items by','Options',@Options)
						
						INSERT INTO @Temp (Type
										  ,Items
										  ,Flag)
									 SELECT 'Filter Items by'
											, FilterName
											, 1
									 FROM #Filterlist

						INSERT INTO @Temp VALUES ('Filter Items by','City',@City)
						INSERT INTO @Temp VALUES ('Filter Items by','Interests',@Interests)

						SELECT Type AS fHeader
								,Items  AS filterName
						FROM @Temp
						WHERE Flag = 1
						
					--Confirmation of success
					SELECT @Status = 0 

			END TRY
            
	BEGIN CATCH
      
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN

		PRINT 'Error occured in Stored Procedure [usp_HcFindOptionsListDisplay].'                            
		--Execute retrieval of Error info.
		EXEC [HubCitiApp8].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
        SELECT @Status = 1                   
	END;
            
	END CATCH;
	END;
       






















GO
