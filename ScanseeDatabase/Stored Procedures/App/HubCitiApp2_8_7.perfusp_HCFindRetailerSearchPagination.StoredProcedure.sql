USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[perfusp_HCFindRetailerSearchPagination]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*    
Stored Procedure name : usp_HCFindRetailerSearchPagination    
Purpose     : To Search the Retailers in the given Radius.   
Example     :    
    
History    
Version		Date				Author				Change Description    
-------------------------------------------------------------------------     
1.0			11th Sept 2012		SPAN		Initial Version    
-------------------------------------------------------------------------  
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[perfusp_HCFindRetailerSearchPagination]
(

      --Input Parameter
      
      @UserID int 
    , @SearchKey varchar(100)   
    , @Latitude float
	, @Longitude float
	, @Radius int
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @HCHubCitiID Int
	, @HcMenuItemId Int			--NEWLY ADDED
    , @HcBottomButtonId Int		--NEWLY ADDED
	, @SortColumn varchar(50)
	, @SortOrder varchar(10)
	, @HcCityID Varchar(3000)
	, @FilterID Varchar(1000)
    , @FilterValuesID Varchar(1000) 
	, @LocalSpecials bit 
	, @Interests Varchar(1000) 

	 --Inputs for User Tracking
	, @MainMenuId int
	
      --Output Variable--  
	, @UserOutOfRange bit output 
	, @DefaultPostalCode varchar(50) output
    , @FindRetailerSearchID int output               
    , @MaxCnt int  output
	, @NxtPageFlag bit output  
	, @Status bit output  
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
               SET NOCOUNT ON           
                        DECLARE @RowCount int 
                        DECLARE @Count INT = 1
                        DECLARE @MaxCount INT 
                     --   DECLARE @SQL VARCHAR(1000) 
                      --  DECLARE @RetSearch VARCHAR(1000)
                        DECLARE @Config varchar(50)                     
                        DECLARE @RetailConfig varchar(50)
                        DECLARE @UpperLimit int   
                        DECLARE @Tomorrow DATETIME = GETDATE() + 1
                        DECLARE @Yesterday DATETIME = GETDATE() - 1
                        DECLARE @RowsPerPage int						   
						DECLARE @PostalCode varchar(10) 
						DECLARE @DistanceFromUser FLOAT
						DECLARE @UserLatitude float
						DECLARE @UserLongitude float 
					--	DECLARE @SpecialChars VARCHAR(1000)
						DECLARE @ModuleName varchar(100) = 'Find'	
						DECLARE @RegionAppFlag Bit
						DECLARE @GuestLoginFlag BIT=0
						DECLARE @CategoryID INT
						DECLARE @BusinessCategoryID INT --ADDED
						DECLARE @BusinessCatSearch bit --ADDED

						DECLARE @UserID1 INT = @UserID
							   ,@HCHubCitiID1 INT = @HCHubCitiID
							   ,@HcMenuItemId1 INT = @HcMenuItemId
							   ,@HcBottomButtonId1 INT = @HcBottomButtonId


                     
										
						--SearchKey implementation
						DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

						SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
											   WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
											   WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
											   ELSE @SearchKey END)


						SELECT Param IntIDs
						INTO #Interests
						FROM fn_SplitParam (@Interests,',')

						--Filter Implementation 
						IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
						BEGIN
								SELECT Param FilterIDs
								INTO #Filters
								FROM fn_SplitParam(@FilterID,',') P
								LEFT JOIN dbo.AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
								WHERE AF.AdminFilterID IS NULL

								SELECT Param FilterValues
								INTO #FilterValues
								FROM fn_SplitParam(@FilterValuesID,',')

								SELECT DISTINCT RetailID 
								INTO #FilterRetaillocations
								FROM(
								SELECT RF.RetailID                                    
								FROM dbo.RetailerFilterAssociation RF
								INNER JOIN #FilterValues F ON F.FilterValues =RF.AdminFilterValueID --AND BusinessCategoryID = @BusinessCategoryID
								
								UNION

								SELECT RF.RetailID                                    
								FROM dbo.RetailerFilterAssociation RF
								INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID --AND BusinessCategoryID = @BusinessCategoryID
								)A
						END

						CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
                                  
						CREATE TABLE #CityList(CityID Int,CityName Varchar(200))

						IF (SELECT 1 FROM dbo.HcUser WHERE UserName ='guestlogin' AND HcUserID =@UserID1) >0
						BEGIN
							SET @GuestLoginFlag =1
						END


						IF(SELECT 1 from dbo.HcHubCiti H
						INNER JOIN dbo.HcAppList AL ON H.HcAppListID =AL.HcAppListID 
						AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
					
						BEGIN
								SET @RegionAppFlag =1		
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM dbo.HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
									UNION ALL
									SELECT  @HCHubCitiID1 AS HcHubCitiID
									)
						END

						ELSE
						BEGIN
								SET @RegionAppFlag =0   

									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID1                                
						END

                      

						--To Fetch region app associated citylist			  
						INSERT INTO #CityList(CityID,CityName)
					
						SELECT DISTINCT C.HcCityID 
								,C.CityName					
						FROM #RHubcitiList H
						INNER JOIN dbo.HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID1 
						INNER JOIN dbo.HcCity C ON C.HcCityID =LA.HcCityID 						
						LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =C.HcCityID
					    LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1	AND UP.HcHubcitiID =H.HchubcitiID							        
						WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
						OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param )
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID =@UserID1)						  
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID =@UserID1)
				        
						UNION

						SELECT 0,'ABC' 

						DECLARE @Globalimage varchar(50)
						SELECT @Globalimage =ScreenContent 
						FROM dbo.AppConfiguration 
						WHERE ConfigurationType ='Image Not Found'
					   

						SELECT @UserLatitude = @Latitude
							, @UserLongitude = @Longitude

						IF (@UserLatitude IS NULL) 
						BEGIN
							SELECT @UserLatitude = Latitude
									, @UserLongitude = Longitude
							FROM dbo.HcUser A
							INNER JOIN dbo.GeoPosition B ON A.PostalCode = B.PostalCode
							WHERE HcUserID = @UserID1 
						END
						--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
						IF (@UserLatitude IS NULL) 
						BEGIN
							SELECT @UserLatitude = Latitude
									, @UserLongitude = Longitude
							FROM dbo.HcHubCiti A
							INNER JOIN dbo.GeoPosition B ON A.DefaultPostalCode = B.PostalCode
							WHERE A.HcHubCitiID = @HCHubCitiID1
						END


						EXEC [HubCitiApp2_3_1].[usp_HcUserHubCitiRangeCheck] @UserID1, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
					    SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
 
					
                        --Set the Next page flag to 0 initially.
                        SET @NxtPageFlag = 0

                        SELECT @Config=ScreenContent
                        FROM dbo.AppConfiguration 
                        WHERE ConfigurationType='App Media Server Configuration'

                        SELECT @RetailConfig=ScreenContent
                        FROM dbo.AppConfiguration 
                        WHERE ConfigurationType='Web Retailer Media Server Configuration'

						 --To fetch all the duplicate retailers.
						SELECT DISTINCT DuplicateRetailerID 
						INTO #DuplicateRet
						FROM dbo.Retailer 
						WHERE DuplicateRetailerID IS NOT NULL


                        --To get the row count for pagination.  
                        SELECT @UpperLimit = @LowerLimit + ScreenContent 
                                , @RowsPerPage = ScreenContent
                        FROM dbo.AppConfiguration   
                        WHERE ScreenName = @ScreenName 
                        AND ConfigurationType = 'Pagination'
                        AND Active = 1   
											
                        --Derive the Latitude and Longitude in the absence of the input.
                        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
                        BEGIN
                                --If the postal code is passed then derive the co ordinates.
                                IF @PostalCode IS NOT NULL
								BEGIN
                                        SELECT @Latitude = Latitude
                                            , @Longitude = Longitude
                                        FROM dbo.GeoPosition 
                                        WHERE PostalCode = @PostalCode
                                END
								ELSE
								--If the user has not passed the co ordinates & postal code then cosidered the user configured postal code.
                                BEGIN
                                        SELECT @Latitude = G.Latitude
                                            , @Longitude = G.Longitude
                                        FROM dbo.GeoPosition G
                                        INNER JOIN dbo.HcUser U ON G.PostalCode = U.PostalCode
                                        WHERE U.HcUserID = @UserID1
                                END
                        END    
                           
                        --Get the user preferred radius.
						IF @Radius IS NULL
						BEGIN

							SELECT @Radius = LocaleRadius
							FROM dbo.HcUserPreference 
							WHERE HcUserID = @UserID1
						END
                           
                        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM dbo.AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))
						
						--NEWLY ADDED CODE
						CREATE TABLE #BusinessCategory(BusinessCategoryID int ,BusinessCategoryName VARCHAR(100), HcBusinessSubCategoryID int)
						IF (@HcMenuItemId1 IS NOT NULL)
						BEGIN 

							INSERT INTO #BusinessCategory(BusinessCategoryID,BusinessCategoryName,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,BC.BusinessCategoryName,HcBusinessSubCategoryID               
							FROM BusinessCategory BC
							INNER JOIN HcMenuFindRetailerBusinessCategories F ON BC.BusinessCategoryID = F.BusinessCategoryID
							WHERE BC.BusinessCategoryName LIKE '%'+@searchkey+'%' AND HcMenuItemID = @HcMenuItemId1
						END
						ELSE IF (@HcBottomButtonId1 IS NOT NULL)
						BEGIN
			
							INSERT INTO #BusinessCategory(BusinessCategoryID,BusinessCategoryName,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,BC.BusinessCategoryName,HcBusinessSubCategoryID               
							FROM BusinessCategory BC
							INNER JOIN HcBottomButtonFindRetailerBusinessCategories F ON BC.BusinessCategoryID = F.BusinessCategoryID 
							WHERE BC.BusinessCategoryName LIKE '%'+@searchkey+'%' AND HcBottomButonID = @HcBottomButtonId1
						END
					
						IF EXISTS(SELECT 1 FROM #BusinessCategory WHERE BusinessCategoryID IS NOT NULL)
							SET @BusinessCatSearch = 1
						ELSE 
							SET @BusinessCatSearch = 0

						--To check matched category has sub categories or not

						DECLARE @HcBusinessSubCategoryID bit
						IF EXISTS(SELECT 1 FROM #BusinessCategory WHERE HcBusinessSubCategoryID IS NOT NULL)
							SET @HcBusinessSubCategoryID = 1
						ELSE 
							SET @HcBusinessSubCategoryID = 0

                        -- To identify Retailer that have products on Sale or any type of discount
                        SELECT DISTINCT Retailid , RetailLocationid
                        INTO #RetailItemsonSale
                        FROM 
                                (SELECT b.RetailID, a.RetailLocationID 
                                FROM dbo.RetailLocationDeal a 
                                INNER JOIN dbo.RetailLocation b ON a.RetailLocationID = b.RetailLocationID 
                                INNER JOIN dbo.HcLocationAssociation HL ON b.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1
								--INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					            --INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)
                                INNER JOIN dbo.HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = B.RetailLocationID AND Associated =1                                
                                INNER JOIN dbo.RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                                                                        and a.ProductID = c.ProductID
                                                                                        and GETDATE() between ISNULL(a.SaleStartDate, GETDATE()-1) and ISNULL(a.SaleEndDate, GETDATE()+1)
                                UNION ALL 
                                SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                                FROM dbo.Coupon C 
                                INNER JOIN dbo.CouponRetailer CR ON C.CouponID=CR.CouponID
                                INNER JOIN dbo.RetailLocation RL ON RL.RetailID = CR.RetailID
                                INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
							 --  INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					            --INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)
							    INNER JOIN dbo.HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND Associated =1                                                                                             
                                LEFT JOIN dbo.HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                                WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                                GROUP BY C.CouponID
                                            ,NoOfCouponsToIssue
                                            ,CR.RetailID
                                            ,CR.RetailLocationID
                                HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                                            ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                                  
        --                        UNION ALL  

        --                        select  RR.RetailID, 0 as RetaillocationID  
        --                        from dbo.Rebate R 
        --                        INNER JOIN dbo.RebateRetailer RR ON R.RebateID=RR.RebateID
        --                        INNER JOIN dbo.RetailLocation RL ON RL.RetailID = RR.RetailID
        --                        INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
								----INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					   ----         INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)
        --                        INNER JOIN dbo.HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND Associated =1                                                                                                                
        --                        WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

        --                        UNION ALL  

        --                        SELECT  c.retailid, a.RetailLocationID 
        --                        FROM  dbo.LoyaltyDeal a
        --                        INNER JOIN dbo.LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
        --                        INNER JOIN dbo.RetailLocation c on a.RetailLocationID = c.RetailLocationID
        --                        INNER JOIN dbo.HcLocationAssociation HL ON c.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1    
								----INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					   ----         INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)          
        --                        INNER JOIN dbo.HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = C.RetailLocationID AND Associated =1 
        --                        INNER JOIN dbo.RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
        --                                                                        and b.ProductID = LDP.ProductID 
        --                        Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                                UNION ALL 

                                SELECT DISTINCT rl.RetailID, rl.RetailLocationID
                                FROM dbo.ProductHotDeal p
                                INNER JOIN dbo.ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                                INNER JOIN dbo.RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID
                                INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
								--INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					   --         INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)                                                                                  
                                INNER JOIN dbo.HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND Associated =1 
                                LEFT JOIN dbo.HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                                LEFT JOIN dbo.HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                                WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE()-1) AND ISNULL(HotDealEndDate, GETDATE()+1)
                                GROUP BY P.ProductHotDealID
                                            ,NoOfHotDealsToIssue
                                            ,rl.RetailID
                                            ,rl.RetailLocationID
                                HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                                            ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                                UNION ALL 

                                select q.RetailID, qa.RetailLocationID
                                from dbo.QRRetailerCustomPage q
                                INNER JOIN dbo.QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                                INNER JOIN dbo.RetailLocation RL ON RL.RetailLocationID=qa.RetailLocationID
                                INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1     
								--INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
					   --         INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0)                    
                                INNER JOIN dbo.HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND Associated =1 
                                INNER JOIN dbo.QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                                WHERE GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
                                ) Discount  
						
							
                               CREATE TABLE #Retail (Row_Num INT IDENTITY(1,1)
                                                            , RetailID INT 
                                                            , RetailName VARCHAR(255)
                                                            , RetailLocationID INT
                                                            , Address1 VARCHAR(500)
                                                            , City VARCHAR(100)
                                                            , State VARCHAR(20)
                                                            , PostalCode CHAR(10)
                                                            , retLatitude FLOAT
                                                            , retLongitude FLOAT 
                                                            , RetailerImagePath VARCHAR(1000)
                                                            , Distance FLOAT
															, DistanceActual FLOAT
                                                            , BannerAdImagePath VARCHAR(1000)   
                                                            , RibbonAdImagePath VARCHAR(1000)
                                                            , RibbonAdURL VARCHAR(1000)
                                                            , RetailLocationAdvertisementID  INT
                                                            , SplashAdID INT
                                                            , SaleFlag  BIT)
															
			
		-------------If SearchKey matches with Business category---------------
			
	    	
				
		IF (@BusinessCatSearch=1)
			BEGIN			
				
					IF (@FilterID IS NULL AND @FilterValuesID IS NULL)
					BEGIN                                              
                                                                      
							INSERT INTO #Retail(RetailID   
												, RetailName  
												, RetailLocationID  
												, Address1  
												, City  
												, State  
												, PostalCode  
												, retLatitude  
												, retLongitude  
												, RetailerImagePath  
												, Distance   
												, DistanceActual                                               
												, SaleFlag) 
									SELECT DISTINCT RetailID    
                                            , RetailName
                                            , RetailLocationID
                                            , Address1
                                            , City    
                                            , State
                                            , PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , RetailerImagePath    
                                            , Distance  
											, DistanceActual                                               
                                            , SaleFlag
								FROM 
								(
								SELECT DISTINCT R.RetailID     
											, R.RetailName
											, RL.RetailLocationID 
											, RL.Address1     
											, RL.City
											, RL.State
											, RL.PostalCode
											, ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
											, ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
											, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
											, Distance = 0
											, DistanceActual = 0
											--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
											, SaleFlag = 0  
								FROM Retailer R 
								INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1
								INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID 
									AND ((@HcBusinessSubCategoryID = 1 AND BC.HcBusinessSubCategoryID = RBC.BusinessSubCategoryID) OR @HcBusinessSubCategoryID = 0)  
								INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                               
								INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
								INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
								INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0 )
								INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID AND Associated =1 
								LEFT JOIN HcRetailerSubCategory RSC ON  RL.RetailLocationID = RSC.RetailLocationID 
									AND BC.BusinessCategoryID = RSC.BusinessCategoryID 
								--LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID
								LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
								LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode  
								-----------to get retailer that have products on sale                                            
								LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
								LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID              
								WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL  AND RL.Active = 1 
								AND ((@HcBusinessSubCategoryID = 1 AND BC.HcBusinessSubCategoryID = RSC.HcBusinessSubCategoryID) OR @HcBusinessSubCategoryID = 0) 
								AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))

								UNION ALL
                                      
							    SELECT DISTINCT R.RetailID     
												, R.RetailName
												, RL.RetailLocationID 
												, RL.Address1     
												, RL.City
												, RL.State
												, RL.PostalCode
												, ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
												, ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
												, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
												, Distance = 0
												, DistanceActual = 0
												--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
												, SaleFlag = 0  
								FROM dbo.Retailer R    
								INNER JOIN dbo.RetailLocation RL ON RL.RetailID = R.RetailID 
								INNER JOIN dbo.GeoPosition G ON G.PostalCode = RL.PostalCode  
								INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HCHubCitiID1 
								INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
								INNER JOIN #CityList CL ON  (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.cityID = 0)
								INNER JOIN dbo.HcHubCiti HC ON HC.HcHubCitiID=HL.HcHubCitiID AND HC.HcHubCitiID=RH.HchubcitiID  
								INNER JOIN dbo.HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = RH.HchubcitiID   AND Associated =1  
								LEFT JOIN dbo.HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
								--INNER JOIN #Search S ON S.RetailID = R.RetailID                                           
								-----------to get retailer that have products on sale
								LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID 
								LEFT JOIN dbo.RetailerKeywords RK ON R.RetailID = RK.RetailID 
								WHERE RL.Headquarters = 0 AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')
								AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
								

                            )Retail
														  
 
				END
				
				ELSE IF (@FilterID IS NOT NULL AND @FilterValuesID IS NOT NULL)
				BEGIN 



							  INSERT INTO #Retail(RetailID   
                                                    , RetailName  
                                                    , RetailLocationID  
                                                    , Address1  
                                                    , City  
                                                    , State  
                                                    , PostalCode  
                                                    , retLatitude  
                                                    , retLongitude  
                                                    , RetailerImagePath  
                                                    , Distance   
                                                    , DistanceActual                                               
                                                    , SaleFlag)  
                                        SELECT DISTINCT RetailID   
                                                    , RetailName  
                                                    , RetailLocationID  
                                                    , Address1  
                                                    , City  
                                                    , State  
                                                    , PostalCode  
                                                    , retLatitude  
                                                    , retLongitude  
                                                    , RetailerImagePath  
                                                    , Distance   
                                                    , DistanceActual                                                
                                                    , SaleFlag  
									 FROM
									(SELECT DISTINCT R.RetailID     
                                                    , R.RetailName
                                                    , RL.RetailLocationID 
                                                    , RL.Address1     
                                                    , RL.City
                                                    , RL.State
                                                    , RL.PostalCode
                                                    , ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
                                                    , ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
                                                    , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                    , Distance =0
                                                    , DistanceActual = 0
                                                    --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                    , SaleFlag = 0  
                                    FROM #FilterRetaillocations RF                                                    
                                    INNER JOIN Retailer R ON R.RetailID =RF.RetailID  AND R.RetailerActive = 1
                                    INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID
                                    INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID 
									AND ((@HcBusinessSubCategoryID = 1 AND BC.HcBusinessSubCategoryID = RBC.BusinessSubCategoryID) OR @HcBusinessSubCategoryID = 0) 
                                    INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                               
                                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
                                    INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
                                    INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0 )
                                    INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
									LEFT JOIN HcRetailerSubCategory RSC ON  RL.RetailLocationID = RSC.RetailLocationID 
											AND BC.BusinessCategoryID = RSC.BusinessCategoryID 
                                    --LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                    -----------to get retailer that have products on sale                                            
                                    LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
									LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode 
									LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
                                    LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID              
                                    WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL  AND  RL.Active = 1  
                                    AND ((@HcBusinessSubCategoryID = 1 AND BC.HcBusinessSubCategoryID = RSC.HcBusinessSubCategoryID) OR @HcBusinessSubCategoryID = 0) 
									AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))	
				 
									UNION  ALL
									SELECT DISTINCT R.RetailID     
                                                    , R.RetailName
                                                    , RL.RetailLocationID 
                                                    , RL.Address1     
                                                    , RL.City
                                                    , RL.State
                                                    , RL.PostalCode
                                                    , ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
                                                    , ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
                                                    , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                    , Distance = 0
													, DistanceActual = 0
                                                    --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                    , SaleFlag = 0  
                                    FROM #FilterRetaillocations RF                                                           
									INNER JOIN dbo.Retailer R ON R.RetailID =RF.RetailID
									INNER JOIN dbo.RetailLocation RL ON RL.RetailID = R.RetailID 
									INNER JOIN dbo.GeoPosition G ON G.PostalCode = RL.PostalCode  
									INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HCHubCitiID1 
									INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
									INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0 )
									INNER JOIN dbo.HcHubCiti HC ON HC.HcHubCitiID=HL.HcHubCitiID AND HC.HcHubCitiID=RH.HchubcitiID  
									INNER JOIN dbo.HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = RH.HchubcitiID   AND Associated =1  
									LEFT JOIN dbo.HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
									LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID 
									LEFT JOIN dbo.RetailerKeywords RK ON R.RetailID = RK.RetailID 
									WHERE RL.Headquarters = 0 AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')
									AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))	
								)Retail

						END
							 
				END 							
				
		-------------If SearchKey does not matches with Business category---------------
		ELSE 
		BEGIN
	
			IF (@FilterID IS NULL AND @FilterValuesID IS NULL)
			BEGIN
                --If Coordinates exists fetching retailer list in the specified radius.    
                IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL AND @SearchKey IS NOT NULL AND @SearchKey <> '')    
                BEGIN
						
                        --Select all the Retailers in the vicinity of the User.   
                        INSERT INTO #Retail(RetailID    
                                            , RetailName
                                            , RetailLocationID
                                            , Address1
                                            , City    
                                            , State
                                            , PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , RetailerImagePath    
                                            , Distance  
											, DistanceActual                                                  
                                            , SaleFlag)
                                 SELECT DISTINCT RetailID    
                                                , RetailName
                                                , RetailLocationID
                                                , Address1
                                                , City    
                                                , State
                                                , PostalCode
                                                , retLatitude
                                                , retLongitude
                                                , RetailerImagePath    
                                                , Distance  
												, DistanceActual                                               
                                                , SaleFlag
									FROM 
                                    (SELECT DISTINCT R.RetailID     
                                                    , R.RetailName
                                                    , RL.RetailLocationID
                                                    , RL.Address1     
                                                    , RL.City
                                                    , RL.State
                                                    , RL.PostalCode
                                                    , RL.RetailLocationLatitude retLatitude
                                                    , RL.RetailLocationLongitude retLongitude
                                                    , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
                                                    , Distance = 0
													, DistanceActual =0
                                                    --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                    , SaleFlag = CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
                                    FROM dbo.Retailer R    
                                    INNER JOIN dbo.RetailLocation RL ON RL.RetailID = R.RetailID 
                                    INNER JOIN dbo.GeoPosition G ON G.PostalCode = RL.PostalCode  
                                    INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HCHubCitiID1 --AND RL.HcCityID = HL.HcCityID
									INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
									INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0 )
                                    INNER JOIN dbo.HcHubCiti HC ON HC.HcHubCitiID=HL.HcHubCitiID AND HC.HcHubCitiID=RH.HchubcitiID  
                                    INNER JOIN dbo.HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = RH.HchubcitiID   AND Associated =1  
									LEFT JOIN dbo.HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
									--INNER JOIN #Search S ON S.RetailID = R.RetailID                                           
                                    -----------to get retailer that have products on sale
                                    LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID 
									LEFT JOIN dbo.RetailerKeywords RK ON R.RetailID = RK.RetailID 
                                    WHERE RL.Headquarters = 0 AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')
									AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
												
									)Retail
                     END
				END
					
				ELSE IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
				BEGIN


		 

						IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL AND @SearchKey IS NOT NULL AND @SearchKey <> '')  
						BEGIN
						INSERT INTO #Retail(RetailID    
                                            , RetailName
                                            , RetailLocationID
                                            , Address1
                                            , City    
                                            , State
                                            , PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , RetailerImagePath    
                                            , Distance  
											, DistanceActual                                                  
                                            , SaleFlag)
                                                       
                                SELECT  DISTINCT RetailID    
                                            , RetailName
                                            , RetailLocationID
                                            , Address1
                                            , City    
                                            , State
                                            , PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , RetailerImagePath    
                                            , Distance  
											, DistanceActual                                               
                                            , SaleFlag
							FROM 
                            (SELECT DISTINCT R.RetailID     
                                            , R.RetailName
                                            , RL.RetailLocationID 
                                            , RL.Address1     
                                            , RL.City
                                            , RL.State
                                            , RL.PostalCode
                                            , RL.RetailLocationLatitude retLatitude
                                            , RL.RetailLocationLongitude retLongitude
                                            , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
											, Distance = 0
											, DistanceActual = 0
                                            , SaleFlag = CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
                            FROM #FilterRetaillocations RF                                                           
							INNER JOIN dbo.Retailer R ON R.RetailID =RF.RetailID
                            INNER JOIN dbo.RetailLocation RL ON RL.RetailID = R.RetailID 
                            INNER JOIN dbo.GeoPosition G ON G.PostalCode = RL.PostalCode  
                            INNER JOIN dbo.HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HCHubCitiID1 --AND RL.HcCityID = HL.HcCityID
							INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
							INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.CityID=0 )
                            INNER JOIN dbo.HcHubCiti HC ON HC.HcHubCitiID=HL.HcHubCitiID AND HC.HcHubCitiID=RH.HchubcitiID  
                            INNER JOIN dbo.HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = RH.HchubcitiID   AND Associated =1  
							LEFT JOIN dbo.HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
                            LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID 
							LEFT JOIN dbo.RetailerKeywords RK ON R.RetailID = RK.RetailID 
                            WHERE RL.Headquarters = 0 AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword LIKE '%'+@SearchKey+'%')
							AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM #Interests))
							)Retail
						END
					
					 END
				END
	
				SELECT DISTINCT Row_Num   
                        , RetailID     
                        , RetailName 
                        , R.RetailLocationID 
                        , Address1 
                        , City City
                        , State State
                        , PostalCode 
                        , retLatitude
                        , retLongitude    
                        , RetailerImagePath      
                        , Distance  
						, DistanceActual    
                        , S.BannerAdImagePath     
                        , B.RibbonAdImagePath     
                        , B.RibbonAdURL     
                        , B.RetailLocationAdvertisementID   
                        , S.SplashAdID 
                        , SaleFlag 
                INTO #Ret
                FROM #Retail R
                LEFT JOIN (SELECT DISTINCT BannerAdImagePath = 
				CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
                                                                                                    ELSE SplashAdImagePath
                                                                                            END
                                                        , SplashAdID  = ASP.AdvertisementSplashID
                                                        , R.RetailLocationID 
                                    FROM #Retail R
                                    INNER JOIN dbo.RetailLocation RL ON RL.RetailLocationID=R.RetailLocationID
                                    INNER JOIN dbo.RetailLocationSplashAd RS ON RL.RetailLocationID = RS.RetailLocationID
                                    INNER JOIN dbo.AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)) S ON R.RetailLocationID = S.RetailLocationID
                LEFT JOIN (SELECT DISTINCT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
                                                                                                                ELSE AB.BannerAdImagePath
                                                                                                        END 
                                                                , RibbonAdURL = AB.BannerAdURL
                                                                , RetailLocationAdvertisementID = AB.AdvertisementBannerID
                                                                , R.RetailLocationID 
                                    FROM #Retail R
                                    INNER JOIN dbo.RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
                                    INNER JOIN dbo.RetailLocation RL ON RL.RetailLocationID=RB.RetailLocationID AND RL.Headquarters = 0
                                    INNER JOIN dbo.AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
                                    WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, @Tomorrow)) B ON R.RetailLocationID = B.RetailLocationID                               
             
				SELECT DISTINCT R.RetailID    
                                , R.RetailName
                                , R.RetailLocationID
                                , R.Address1
                                , R.City    
                                , R.State
                                , R.PostalCode
                                , R.retLatitude
                                , R.retLongitude
                                , R.RetailerImagePath    
                                , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1) 
								, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                                                      
                                , R.SaleFlag
                                , R.BannerAdImagePath      
                                , R.RibbonAdImagePath      
                                , R.RibbonAdURL      
                                , R.RetailLocationAdvertisementID      
                                , R.SplashAdID
                INTO #Ret1  
                FROM #Ret R
                INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
				INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode
				WHERE RL.active = 1	 AND Distance <= @Radius				 
                   
				
                  SELECT DISTINCT RowNum = ROW_NUMBER() OVER --(ORDER BY Distance) RowNum 
						(ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
											WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(RetailName AS SQL_VARIANT)
											WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
									END ASC ,Distance ) 
                        , RetailID     
                        , RetailName 
                        , RetailLocationID
                        , Address1 
                        , City 
                        , State 
                        , PostalCode   
                        , retLatitude 
                        , retLongitude 
                        , RetailerImagePath      
                        , Distance 
						, DistanceActual    
                        , BannerAdImagePath     
                        , RibbonAdImagePath     
                        , RibbonAdURL ribbonAdURL    
                        , RetailLocationAdvertisementID     
                        , SplashAdID splashAdID
                        , SaleFlag
                INTO #Ret2     
                FROM #Ret1  
				WHERE (ISNULL(@LocalSpecials,0) = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))
					

				--To capture max row number.  
                SELECT @MaxCnt = Count(1) FROM #Ret2
                           
                --this flag is a indicator to enable "More" button in the UI.   
                --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
					         
                --Project the final result set with pagination.
                SELECT DISTINCT RowNum rowNumber  
                        , RetailID retailerId    
                        , RetailName retailerName
                        , RetailLocationID
                        , Address1 retaileraddress1
                        , City City
                        , State State
                        , PostalCode PostalCode  
                        , retLatitude 
                        , retLongitude 
                        , RetailerImagePath logoImagePath     
                        , Distance  
						, DistanceActual    
                        , BannerAdImagePath bannerAdImagePath    
                        , RibbonAdImagePath ribbonAdImagePath    
                        , RibbonAdURL ribbonAdURL    
                        , RetailLocationAdvertisementID advertisementID    
                        , SplashAdID splashAdID
                        , SaleFlag
                INTO #RetailerList     
                FROM #Ret2  
				WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit
                ORDER BY rowNumber
                      
										               
				SELECT DISTINCT rowNumber  
                            , retailerId    
                            , retailerName
                            , retailLocationID
                            , retaileraddress1
                            , City
                            , State
                            , PostalCode
                            , retLatitude
                            , retLongitude    
                            , logoImagePath     
                            , Distance
                            , DistanceActual                         
                            ,  bannerAdImagePath    
                            , ribbonAdImagePath    
                            , ribbonAdURL    
                            , advertisementID  
                            , splashAdID
                            , SaleFlag 
                INTO #RetailerList1 
				FROM #RetailerList
				WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
                ORDER BY rowNumber
                                  
                                    
                --User Tracking section.

                ---Region App

                INSERT INTO HubCitiReportingDatabase.dbo.CityList(MainMenuID
                                                            ,CityID
                                                            ,DateCreated)
				SELECT DISTINCT @MainMenuID 
                        ,CityID 
                        ,GETDATE()
                FROM #CityList 

                --Table to track the Retailer List.
                CREATE TABLE #Temp(Rowno int IDENTITY(1,1)
									,RetailerListID int
                                    ,LocationDetailID int
                                    ,MainMenuID int
                                    ,RetailID int
                                    ,RetailLocationID int)  

                --Capture the impressions of the Retailer list.
                INSERT INTO HubCitiReportingDatabase.dbo.RetailerList(MainMenuID
                                                                , RetailID
                                                                , RetailLocationID
                                                                , FindCategoryID
                                                                , DateCreated)
                     
                OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)                                             
                                                                                SELECT DISTINCT @MainMenuId
                                                                                            , retailerId
                                                                                            , RetailLocationID
                                                                                            , @CategoryID
                                                                                            , GETDATE()
                                                                                FROM #RetailerList                            
                           
                               
                    --Display the Retailer along with the keys generated in the Tracking table.
                    SELECT Distinct rowNumber
                                                , T.RetailerListID retListID
                                                , retailerId
                                                , retailerName
                                                , T.RetailLocationID
                                                , retaileraddress1
                                                , City
                                                , State
                                                , PostalCode                                   
                                                , retLatitude
                                                , retLongitude
                                                , Distance = ISNULL(DistanceActual, Distance)
                                                , logoImagePath
                                                , bannerAdImagePath
                                                , ribbonAdImagePath
                                                , ribbonAdURL
                                                , advertisementID
                                                , splashAdID
                                                , SaleFlag   
                    FROM #Temp T
                    INNER JOIN #RetailerList1 R ON  R.RetailLocationID = T.RetailLocationID     
					----INNER JOIN #RetailerList R ON  R.rowNumber = T.Rowno
                           
                    --To display bottom buttons                                                

                    EXEC [HubCitiApp2_3].[usp_HcFunctionalityBottomButtonDisplay] @HCHubCitiID1, @ModuleName, @UserID1, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT
                                
            
       END TRY
            
       BEGIN CATCH
      
       --Check whether the Transaction is uncommitable.
       IF @@ERROR <> 0
       BEGIN

               SELEct ERROR_LINE()
                     PRINT 'Error occured in Stored Procedure usp_HcFindRetailerListPagination.'                            
                     --Execute retrieval of Error info.
                     EXEC [HubCitiApp2_3_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                     PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
       END;
            
       END CATCH;
       

	   DROP TABLE #BusinessCategory,#CityList,#DuplicateRet,#Interests,#Ret,#Ret1,#Ret2,#Retail,#RetailerList,#RetailerList1,#RetailItemsonSale,#RHubcitiList,#Temp

	END
















GO
