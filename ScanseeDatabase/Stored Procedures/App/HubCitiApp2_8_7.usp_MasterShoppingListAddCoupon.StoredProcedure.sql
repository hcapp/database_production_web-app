USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_MasterShoppingListAddCoupon]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListAddCoupon
Purpose					: To add Coupon to User Gallery
Example					: usp_MasterShoppingListAddCoupon 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_MasterShoppingListAddCoupon]
(
	  @CouponID int
	, @UserID int
	, @UserCouponClaimTypeID tinyint
	, @CouponPayoutMethod varchar(20)
	, @RetailLocationID int
	, @CouponClaimDate datetime
	
	--User Trcking Inputs
	, @CouponListID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			IF NOT EXISTS (SELECT 1 FROM HcUserCouponGallery WHERE CouponID = @CouponID AND HcUserID=@UserID)
			BEGIN
				INSERT INTO [HcUserCouponGallery]
				   ([CouponID]
				   ,[HcUserID]
				   ,ClaimDate)
				VALUES
				   (@CouponID
				   ,@UserID
				   ,@CouponClaimDate)
				   
			END
			
			--User Tracking
			
			--Updating when user clip the coupon in the couponlist table
			Update HubCitiReportingDatabase..CouponList SET CouponClipped=1, CouponClick = 1
			WHERE CouponListID =@CouponListID 
				   
		    --Confirmation of Success.
			SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListAddCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
