USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcScanHistorytDelete]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcScanHistorytDelete]
Purpose					: To delete a Scan history item.
Example					: [usp_HcScanHistorytDelete] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			14thNov2013     Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcScanHistorytDelete]
(
	  @ScanHistoryID int
	, @UserID int
	
	--Output Variable
	, @Result bit output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
					 
		    --Swipe delete from Scan history table.

					DELETE FROM ScanHistory
					WHERE UserID = @UserID
					AND ScanHistoryID = @ScanHistoryID 
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_HcScanHistorytDelete].'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














































GO
