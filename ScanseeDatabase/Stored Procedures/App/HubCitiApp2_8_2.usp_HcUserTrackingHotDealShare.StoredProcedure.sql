USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserTrackingHotDealShare]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcUserTrackingHotDealShare 
Purpose               : To track hotdeal share details. 
Example               : usp_HcUserTrackingHotDealShare 
  
History  
Version    Date				Author        Change         Description  
---------------------------------------------------------------   
1.0        7th Nov 2013		Mohith H R    Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserTrackingHotDealShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @ProductHotDealID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         
         
		 INSERT INTO HubCitiReportingDatabase..ShareHotDeal(MainMenuID
														   ,ShareTypeID
														   ,TargetAddress
														   ,HotDealID 														 
														   ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@ProductHotDealID  
		        ,GETDATE())	
		        
		 --Confirmation of Success
		 SELECT @Status = 0       							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingHotDealShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
   --Confirmation of failure.
   SELECT @Status = 1   
  END;  
     
 END CATCH;  
END;











































GO
