USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetrieveState]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetrieveState
Purpose					: To Display the States in the system..
Example					: usp_WebRetrieveState

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			2th January 2014				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetrieveState]
(

	--Input Parameter(s)--   
	 @HubCitiID int
	--Output Variable--	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		SELECT DISTINCT S.Stateabbrevation, S.StateName
		FROM HcLocationAssociation HL
		INNER JOIN [State] S ON HL.[StateID] = S.StateID
		WHERE HL.HcHubCitiID = @HubCitiID
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveState.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;













































GO
