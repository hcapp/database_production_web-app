USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcProductSmartSearch]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcProductSmartSearch
Purpose					: To display the Fistst 50 categories in which the search text is present
Example					: usp_HcProductSmartSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29thOct2013 	Pavan Sharma K	        Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcProductSmartSearch]
(
	  @MainMenuID int 
	, @ProdSearch varchar(500)
	, @HcHubCitiID Int
    , @UserID Int
	 
	--Output Variable 
	, @TimeTaken float output--To measure the performance of the SP.
	, @ProductSmartSearchID int output --User tracking output(This should be passed to the next procedure.)
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	--To stop sending the counts to the client.
	SET NOCOUNT ON

	BEGIN TRY
		DECLARE @StartTime DATETIME
		DECLARE @STRREV VARCHAR(100)
		DECLARE @STRRES VARCHAR(100)
		DECLARE @ProdSearchInp VARCHAR(100)	
		DECLARE @SpecialChars VARCHAR(1000)	

		SELECT @StartTime = GETDATE()

		SELECT @ProdSearchInp = @ProdSearch
		
		--To avoid Parameter sniffing.
		SET @STRREV = REVERSE(LTRIM(RTRIM(@ProdSearchInp)))
		
		IF CHARINDEX('S', @STRREV)=1
		BEGIN
			SET @ProdSearchInp=''
			SET @STRRES = SUBSTRING(@STRREV,2,LEN(@STRREV))			
			SET @ProdSearchInp = REVERSE(@STRRES)		
		END				

		--Split the search string.
        SELECT Param
        INTO #ProdSearch
        FROM [HubCitiApp2_1].[fn_SplitParam](LTRIM(RTRIM(@ProdSearchInp)), ' ') 
			
		 --Remove the noise(stopwords) from the input search string.
		SET @ProdSearchInp = NULL
        SELECT @ProdSearchInp = COALESCE(@ProdSearchInp+' ','') + I.Param 
        FROM #ProdSearch I
        LEFT JOIN sys.fulltext_system_stopwords B ON I.Param = B.stopword AND B.language_id = 1033
        WHERE B.stopword IS NULL 
			
		SELECT @ProdSearchInp = REPLACE(REPLACE(@ProdSearchInp, '''', ''''''), ',', '')		

		--Replace the special characters.
		SELECT @SpecialChars = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ProdSearchInp,'!',''),'@',''),'#',''),'$',''),'%',''),'^',''),'&',''),'*',''), '-', ''), '_', '')
		SELECT @ProdSearchInp = IIF(LEN(@SpecialChars) = 0 OR @SpecialChars = '', @ProdSearchInp, @SpecialChars)
		--Logic to replace multiple white spaces with a single one.
		SELECT @ProdSearchInp = REPLACE(REPLACE(REPLACE(@ProdSearchInp, ' ', '!@'), '@!', ''), '!@', ' ')

		--For Full Text Search
		SELECT @ProdSearchInp = LTRIM(RTRIM(REPLACE(@ProdSearchInp, ' ', ' AND ')))		
		PRINT @ProdSearchInp
		
		/*Pattern match for ScanCode & ISBN.
		Full Text Search on Product Name & Product Keyword*/
		CREATE TABLE #Product(ProductID int, ExpDate datetime)

		INSERT INTO #Product(ProductID, ExpDate)
		SELECT ProductID
			 , ISNULL(ProductExpirationDate, GETDATE()+1)
		FROM Product
		WHERE ScanCode LIKE  @ProdSearchInp + '%'

		INSERT INTO #Product(ProductID, ExpDate)
		SELECT ProductID
			 , ISNULL(ProductExpirationDate, GETDATE()+1)
		FROM Product
		WHERE ISBN LIKE  @ProdSearchInp + '%'
		
		--Get the exact matches at the top.
		INSERT INTO #Product(ProductID, ExpDate)
		SELECT TOP 1000 ProductID
			 , ISNULL(ProductExpirationDate, GETDATE()+1)
		FROM Product P
		INNER JOIN CONTAINSTABLE(Product, ProductName, @ProdSearchInp)F ON F.[Key] = P.ProductID
		ORDER BY RANK DESC

		
		INSERT INTO #Product(ProductID, ExpDate)
		SELECT TOP 1000 P.ProductID	
			 , ISNULL(P.ProductExpirationDate, GETDATE()+1)
		FROM ProductKeywords PK
		INNER JOIN Product P ON P.ProductID = PK.ProductID
		INNER JOIN CONTAINSTABLE(ProductKeywords, ProductKeyword, @ProdSearchInp)F ON F.[Key] = P.ProductID
		ORDER BY RANK DESC 
		print @ProdSearchInp

		--If the Product does not have any category then display under "Others" category.
		;WITH ProdCTE(ParentCategoryID, ParentCategoryName)
		AS(
				SELECT DISTINCT IIF(ParentCategoryID IS NULL OR ParentCategoryName = 'Other', '-1', ParentCategoryID) parCatId
				, IIF(ParentCategoryID IS NULL OR ParentCategoryName = 'Other', 'Others', ParentCategoryName)ParentCategoryName
				FROM #Product P
				LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
				LEFT JOIN Category C ON PC.CategoryID = C.CategoryID
				WHERE GETDATE() < P.ExpDate
		)

		SELECT 0 parCatId, @ProdSearch categs, '0' ParentCategoryName
		UNION ALL
		SELECT ParentCategoryID parCatId
			,  @ProdSearch  +' |~| in '+ ParentCategoryName categs
			, ParentCategoryName
		FROM ProdCTE
		ORDER BY ParentCategoryName ASC

		--User tracking section.
		INSERT INTO HubCitiReportingDatabase..ProductSmartSearch(MainMenuID, SearchKeyword, CreatedDate)
														SELECT @MainMenuID
															 , @ProdSearch
															 , GETDATE()
															 
		--Capture the identity value inserted from the tracking table.		
		SELECT @ProductSmartSearchID = SCOPE_IDENTITY()

		--To check for the totoal time for execution
		SELECT @TimeTaken = DATEDIFF(MILLISECOND, @StartTime, GETDATE())

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcProductSmartSearch.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;















































GO
