USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcRetrieveUniversity]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: usp_WebRetrieveUniversity
Purpose					: To Retrieve University from the system.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			2nd Jan 2014				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcRetrieveUniversity]
(

	--Input Input Parameter(s)--
	  @State char(2)	
	, @HubCitiID int 
	--Output Variable--	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			-- Retrieve the University from the University Table.
			
			Select DISTINCT UniversityID
			     , UniversityName as college
			From University U
			INNER JOIN HcLocationAssociation H ON U.State = H.State
			Where U.[State] = @State
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveUniversity.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;














































GO
