USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcGetAPIListOnline]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_HcGetAPIListOnline
Purpose					: Retrieve List of APIs associated to a Submodule
Example					: EXEC usp_HcGetAPIListOnline 'FindOnlineStores'  

History
Version		Date			Author	   Change Description
--------------------------------------------------------------- 
1.0			08th Nov 2013	SPAN Initial Version  
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcGetAPIListOnline]
(
	  @prSubModule	Varchar(60)
	, @HcHubcitiID Int
	
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @SubModule Varchar(60)
	
		-- Assign parameter value to a local variable
		SET @SubModule = @prSubModule 

		-- Retrive basic information of desired API 
		SELECT DISTINCT APIURLPath
			, APIUsagePriority
			, APIKey
			, Apiusage.APIUsageID
			, APIPartner.APIPartnerID
			, APIPartner.APIPartnerName as VendorName
		FROM APISubModule 
			INNER JOIN APIParameterDetail ON APIParameterDetail.APISubModuleID=APISubModule.APISubModuleID
			INNER JOIN APIParameter ON APIParameter.APIParameterID=APIParameterDetail.APIParameterID
			INNER JOIN APIUsage ON APIParameter.APIUsageID = APIUsage.APIUsageID
			INNER JOIN APIPartner ON apipartner.APIPartnerID=APIUsage.APIPartnerID
		WHERE APISubModuleName = @prSubModule
			And APIUsageActive = 1
			And ActivePartner =1
		ORDER BY APIUsagePriority
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcGetAPIListOnline.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;














































GO
