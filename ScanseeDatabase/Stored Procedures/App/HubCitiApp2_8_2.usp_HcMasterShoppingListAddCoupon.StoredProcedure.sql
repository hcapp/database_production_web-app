USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcMasterShoppingListAddCoupon]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcMasterShoppingListAddCoupon
Purpose					: To add Coupon to User Gallery
Example					: usp_HcMasterShoppingListAddCoupon 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		  17thDec2013 	Dhananjaya TR	  Initial Version
2.0       12/27/2016     Shilpashree      Changed input variables
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcMasterShoppingListAddCoupon]
(
	  @CouponID int
	, @HcUserID int
	
	--User Trcking Inputs
	, @CouponListID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION

			IF NOT EXISTS (SELECT 1 FROM HcUserCouponGallery WHERE CouponID = @CouponID AND HcuserID=@HcUserID)
			BEGIN
				INSERT INTO [HcUserCouponGallery](CouponID
												   ,HcUserID
												   ,Claim
												   ,ClaimDate
												   ,UsedFlag
												   ,DateCreated)
											VALUES(@CouponID
												   ,@HcUserID
												   ,1
												   ,GETDATE()
												   ,0
												   ,GETDATE())
			END
			
			--User Tracking
			
			--Updating when user clip the coupon in the couponlist table
			Update HubCitiReportingDatabase..CouponsList SET CouponClipped=1, CouponClick = 1
			WHERE CouponsListID =@CouponListID 
				   
		    --Confirmation of Success.
			SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcMasterShoppingListAddCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
