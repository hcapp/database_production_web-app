USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcCityExperienceRetailerList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : [usp_HcCityExperienceRetailerList]
Purpose                    : To display CityExperience RetailerList.
Example                    : [usp_HcCityExperienceRetailerList]

History
Version      Date                Author       Change Description
--------------------------------------------------------------- 
1.0          15/10/2013      Pavan Sharma K       1.0
1.1			12/22/2016		 Bindu T A		 Day wise changes - Hours Filter Changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcCityExperienceRetailerList]
(
      --Input variable.
	     @UserID int
	   , @HcHubCitiID Int
	   , @CityExperienceID int
	   , @CategoryID Varchar(1000) --Comma Separated CategoryIDs
       , @SearchKey varchar(255)
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @Latitude decimal(18,6)    
       , @Longitude decimal(18,6)    
       --, @ZipCode varchar(10)
       , @SortColumn Varchar(200)
       , @SortOrder Varchar(100)
	   , @CityID Varchar(1000) --Comma Separated CityIDs
	   --, @GroupColumn Varchar(10)
	   , @Interests Varchar(1000) --Comma Sepearted FilterIDs
	   , @LocalSpecials Bit
       , @requestedTime DateTime

       --Inputs for User Tracking
       , @MainMenuId int
  
       --Output Variable
	   , @NoRecordsMsg nvarchar(max) output
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       , @MaxCnt int  output
       , @RetailAffiliateCount int output
       , @RetailAffiliateID int  output
       , @RetailAffiliateName varchar(255) output
       , @RetailGroupButtonImagePath varchar(1000) output
       , @NxtPageFlag bit output 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
                 --  select '1',getdate() 
					 
					 DECLARE @UserID1 int = @UserID
					 DECLARE @HcHubCitiID1 int = @HcHubCitiID
					 DECLARE @CityExperienceID1 int = @CityExperienceID
					 
					 DECLARE @RegionAppID int
					 DECLARE @HcAppListID int

					 SELECT @HcAppListID = HcAppListID
					 FROM HcApplist
					 WHERE HcAppListName = 'RegionApp'

					 SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
					 FROM HcHubCiti H
					 WHERE HcHubCitiID = @HcHubCitiID1
					 
                     DECLARE @Config VARCHAR(100)   
					 DECLARE @ZipCode varchar(10) 
                     DECLARE @DistanceFromUser FLOAT
                     DECLARE @ModuleName varchar(100) = 'Experience'
					 DECLARE @DateNameID Varchar(100)

					 SELECT @DateNameID = HcDaysOfWeekID FROM HcDaysOfWeek WHERE HcDaysOfWeekName = (DATENAME(WEEKDAY,@requestedTime))
                     
                     DECLARE @Globalimage varchar(50)
                     DECLARE @CityExpDefaultConfig varchar(50)

                     DECLARE @Tomorrow DATETIME = GETDATE() + 1
					 DECLARE @Yesterday DATETIME = GETDATE() - 1
                     
                     SELECT @Globalimage =ScreenContent 
                     FROM AppConfiguration 
                     WHERE ConfigurationType ='Image Not Found'
               
                     SELECT @Config=ScreenContent
                     FROM AppConfiguration 
                     WHERE ConfigurationType='App Media Server Configuration'
                     
                     SELECT @CityExpDefaultConfig = ScreenContent
                     FROM AppConfiguration 
                     WHERE ConfigurationType = 'City Experience Default Image Path'
                     AND Active = 1
                     
                     DECLARE @RetailConfig varchar(50)
                     SELECT @RetailConfig=ScreenContent
                     FROM AppConfiguration 
                     WHERE ConfigurationType='Web Retailer Media Server Configuration'
                     
                     --To get the row count for pagination.  
                     DECLARE @UpperLimit int   
                     SELECT @UpperLimit = @LowerLimit + ScreenContent   
                     FROM AppConfiguration   
                     WHERE ScreenName = @ScreenName 
                     AND ConfigurationType = 'Pagination'
                     AND Active = 1    
					    
                     DECLARE @UserLatitude float
                     DECLARE @UserLongitude float 
                     
                     SELECT  @UserLatitude = @Latitude
                             , @UserLongitude = @Longitude

                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcUser A
                           INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                           WHERE HcUserID = @UserID1 
                     END
                     --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                     IF (@UserLatitude IS NULL) 
                     BEGIN
                           SELECT @UserLatitude = Latitude
                                         , @UserLongitude = Longitude
                           FROM HcHubCiti A
                           INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                           WHERE A.HcHubCitiID = @HcHubCitiID1
                     END

                     
                     print @UserLatitude
                     print @UserLongitude
                     --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                     EXEC [HubCitiApp2_8_2].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                     SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
                     
				
                     
                     SELECT @RetailAffiliateID = 0

					
					
					SELECT a.RetailID, a.RetailLocationID,a. PostalCode,b.ProductID
					INTO #tempp
                    FROM RetailLocation a 
                    INNER JOIN RetailLocationdeal b ON a.RetailLocationID = b.RetailLocationID
					INNER JOIN HcLocationAssociation HL ON a.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
					WHERE GETDATE() between ISNULL(b.SaleStartDate, @Yesterday) and ISNULL(b.SaleEndDate, @Tomorrow)


                     -- select '2',getdate() 

					 CREATE TABLE #RetailItemsonSale(RetailID int , RetailLocationid int)

                     -- To identify Retailer that have products on Sale or any type of discount
					
                  
					-- insert into #RetailItemsonSale
					--SELECT a.RetailID, a.RetailLocationID 
     
					--from #tempp a  
     --               INNER JOIN HcLocationAssociation HL ON a.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
     --               INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = a.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                               
     --               INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
     --                                                                       and a.ProductID = c.ProductID
																			
                                                                        
                    insert into #RetailItemsonSale
                    SELECT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                    FROM Coupon C 
                    INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
                    INNER JOIN RetailLocation RL ON RL.RetailID = CR.RetailID
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                   
                    LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                    WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                    GROUP BY C.CouponID
                                ,NoOfCouponsToIssue
                                ,CR.RetailID
                                ,CR.RetailLocationID
                    HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                                ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                          
                   --insert into #RetailItemsonSale

                   -- select  RR.RetailID, 0 as RetaillocationID  
                   -- from Rebate R 
                   -- INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
                   -- INNER JOIN RetailLocation RL ON RL.RetailID = RR.RetailID
                   -- INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
                   -- INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                                                                                                                
                   -- WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

                    --insert into #RetailItemsonSale

                    --SELECT  c.retailid, a.RetailLocationID 
                    --FROM  LoyaltyDeal a
                    --INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
                    --INNER JOIN RetailLocation c on a.RetailLocationID = c.RetailLocationID
                    --INNER JOIN HcLocationAssociation HL ON c.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1              
                    --INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = C.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    --INNER JOIN RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
                    --                                                and b.ProductID = LDP.ProductID 
                    --Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                   insert into #RetailItemsonSale

                    SELECT DISTINCT rl.RetailID, rl.RetailLocationID
                    FROM ProductHotDeal p
                    INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                    INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                                                                                   
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                    LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                    WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
                    GROUP BY P.ProductHotDealID
                                ,NoOfHotDealsToIssue
                                ,rl.RetailID
                                ,rl.RetailLocationID
                    HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                                ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                   insert into #RetailItemsonSale

                    select q.RetailID, qa.RetailLocationID
                    from QRRetailerCustomPage q
                    INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                    INNER JOIN RetailLocation RL ON RL.RetailLocationID=qa.RetailLocationID
                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1                     
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1 
                    INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                    where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
                   -- ) Discount    
                     

					

					INSERT INTO #RetailItemsonSale

					SELECT b.RetailID, a.RetailLocationID 
                    FROM RetailLocationDeal a 
                    INNER JOIN RetailLocation b ON a.RetailLocationID = b.RetailLocationID 
                    INNER JOIN HcLocationAssociation HL ON b.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
                    INNER JOIN HcRetailerAssociation HR ON HR.HcHubCitiID = HL.HcHubCitiID AND HR.RetailLocationID = B.RetailLocationID AND HR.HcHubCitiID = @HcHubCitiID1 AND Associated =1                               
                    INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                                                            and a.ProductID = c.ProductID
                                                                            and GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) and ISNULL(a.SaleEndDate, @Tomorrow)
     

	   -- select '3',getdate() 
                     --Derive the Latitude and Longitude in the absence of the input.
                     IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
                     BEGIN
                           IF @ZipCode IS NULL
                           BEGIN
                                  SELECT @Latitude = G.Latitude
                                         , @Longitude = G.Longitude
                                  FROM GeoPosition G
                                  INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                  WHERE U.HcUserID = @UserID1
                           END
                           ELSE
                           BEGIN
                                  SELECT @Latitude = Latitude
                                         , @Longitude = Longitude
                                  FROM GeoPosition 
                                  WHERE PostalCode = @ZipCode
                           END
                     END
                     
                     --Display the count of the Affiliates.          
                     SELECT @RetailAffiliateCount = COUNT(DISTINCT HCF.HcFilterID)
                     FROM HcFilter HCF
                     --INNER JOIN HcFilterRetailLocation RA ON RA.HcFilterID = HCF.HcFilterID
                     WHERE HcCityExperienceID = @CityExperienceID1
                     
                     --To get the image path of the given group. 
                     SELECT @RetailGroupButtonImagePath = @Config + ISNULL(ButtonImagePath, @CityExpDefaultConfig)
                     FROM HcCityExperience
                     WHERE HcCityExperienceID = @CityExperienceID1
                     
                     --Identify the Affiliate ID.             
              
                     IF (SELECT COUNT(1) FROM HcFilterRetailLocation C INNER JOIN HcFilter R ON C.HcFilterID = R.HcFilterID WHERE HcCityExperienceID = @CityExperienceID1) > 0
                     BEGIN
                           SELECT @RetailAffiliateID = RA.HcFilterID
                                  , @RetailAffiliateName = FilterName
                           FROM HcFilterRetailLocation RA
                           INNER JOIN HcFilter AR ON AR.HcFilterID = RA.HcFilterID
                           WHERE HcCityExperienceID = @CityExperienceID1
                     END
                     print @userlatitude
                     print @userlongitude


					 DECLARE @UserPreferredCity bit 
					 SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID1 AND HcCityID IS NULL THEN 0
													    WHEN HcUserID = @UserID1 AND HcCityID IS NOT NULL THEN 1
													    ELSE 0 END
					 FROM HcUsersPreferredCityAssociation
					 WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1

					 SELECT @UserPreferredCity = ISNULL(@UserPreferredCity,0)

					 CREATE TABLE #Retail(Row_Num INT IDENTITY(1, 1)
							        , RetailID  int  
									, RetailName varchar(500)
									, RetailLocationID int
									, Address1 varchar(500)
									, Address2 varchar(500)
									, Address3 varchar(500)
									, Address4 varchar(500)
									, City varchar(500)   
									, State varchar(10)
									, PostalCode varchar(10)                                                      
									, RetailerImagePath varchar(2000)   
									, Distance decimal(18,6)
									, DistanceActual decimal(18,6)
									, SaleFlag int
									, RetailLocationLatitude decimal(18,6)
									, RetailLocationLongitude decimal(18,6)
									, HcCityID int
									, locationOpen VARCHAR(10) )

						SELECT Param BusCatIDs
						INTO #BusinessCategoryIDs
						FROM fn_SplitParam (@CategoryID,',')

						SELECT Param IntIDs
						INTO #Interests
						FROM fn_SplitParam (@Interests,',')
						--select '31',getdate()
						SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID
						INTO #HubCitiRetailerBusinessCategory
						FROM RetailerBusinessCategory RBC
						INNER JOIN HcRetailerAssociation RA ON RBC.RetailerID = RA.RetailID
						WHERE RA.HcHubCitiID = @HcHubCitiID AND Associated = 1
						--AND ((@CategoryID IS NULL AND 1=1) OR (RBC.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs))) 

				SELECT Param into #city1 FROM fn_SplitParam(@CityID,',')
				  --  select '32',getdate()

					 --select *  into  HubCitiRetailerBusinessCategory11 from #HubCitiRetailerBusinessCategory
						--									  select * into RetailItemsonSale11 from #RetailItemsonSale
						--									  select * into city11 from #city1
						--									  select * into BusinessCategoryIDs11 from #BusinessCategoryIDs

				--SELECT '12', GETDATE()		
SELECT distinct R.RetailID   , R.RetailName,RetailerImagePath,WebsiteSourceFlag INTO #Retailer  FROM Retailer R    
INNER JOIN #HubCitiRetailerBusinessCategory RB ON R.RetailID = RB.RetailerID 
 AND ((@CategoryID IS NULL AND 1=1) OR (RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs)))

select distinct R.RetailID     
, R.RetailName
, RL.RetailLocationID ,RetailerImagePath,WebsiteSourceFlag
INTO #A FROM #Retailer R
INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID


--SELECT '11', GETDATE()

		SELECT R.RetailID     
			, R.RetailName
			, RL.RetailLocationID
			, RL.Address1     
			, RL.Address2
			, RL.Address3
			, RL.Address4
			, RL.City
			, RL.State
			, RL.PostalCode,RetailLocationImagePath,RetailLocationLatitude,RetailLocationLongitude ,Headquarters,RL.HcCityID  ,
			RetailerImagePath,WebsiteSourceFlag
			,StartTimeUTC,EndTimeUTC,CE.HcCityExperienceID 
		INTO #B2
		FROM #A R 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = R.RetailLocationID
		INNER JOIN HcLocationAssociation HL ON HL.PostalCode =RL.PostalCode AND RL.HcCityID = HL.HcCityID
		INNER JOIN HcCity C ON HL.HcCityID = C.HcCityID                                   
		INNER JOIN HcCityExperienceRetailLocation CE ON CE.RetailLocationID = RL.RetailLocationID AND Headquarters = 0  
		WHERE  RL.Active =1

		SELECT Distinct *
		INTO #B1
		FROM #B2
		
		select RL.* 
		into #B
		 FROM #B1 RL  
         LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
		 where  ((@Interests IS NULL AND 1=1) OR (FRL.HcFilterID IN (SELECT IntIDs FROM #Interests)))
		
		-- select 	'NN1', GETDATE()

					--Filter the retailers who belong to the given Group.
				--	IF @CityID IS NULL
					BEGIN

					PRINT 'If Loop'

					 insert into #Retail( 
							         RetailID    
									, RetailName
									, RetailLocationID
									, Address1
									, Address2
									, Address3
									, Address4
									, City    
									, State
									, PostalCode                                                        
									, RetailerImagePath    
									, Distance 
									, DistanceActual
									, SaleFlag
									, RetailLocationLatitude 
									, RetailLocationLongitude
									, HcCityID
									, locationOpen)
							 SELECT  
							         RetailID    
									, RetailName
									, RetailLocationID
									, Address1
									, Address2
									, Address3
									, Address4
									, City    
									, State
									, PostalCode                                                        
									, RetailerImagePath    
									, Distance 
									, DistanceActual
									, SaleFlag
									, RetailLocationLatitude 
									, RetailLocationLongitude
									, HcCityID		
									, locationOpen					
							 FROM
							 (SELECT distinct  TOP 100 PERCENT RL.RetailID     
										, RL.RetailName
										, RL.RetailLocationID 
										, RL.Address1     
										, RL.Address2
										, RL.Address3
										, RL.Address4
										, RL.City
										, RL.State
										, RL.PostalCode										
										, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),Rl.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),Rl.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
										, Distance = 0.0
										, DistanceActual =0.0
										, SaleFlag = CASE WHEN T.RetailLocationID IS NOT NULL THEN 1 ELSE 0 END
										, RetailLocationLatitude = CASE WHEN RL.RetailLocationLatitude IS NOT NULL THEN RL.RetailLocationLatitude ELSE G.Latitude END 
										, RetailLocationLongitude = CASE WHEN RL.RetailLocationLongitude IS NOT NULL THEN RL.RetailLocationLongitude ELSE G.Longitude END 
										, HcCityID=0
										, locationOpen = 
											CASE WHEN H.StartTimeUTC < H.ENDTimeUTC THEN (CASE WHEN CAST(@requestedTime as time) >= H.StartTimeUTC AND CAST(@requestedTime as time) < H.ENDTimeUTC THEN 'Open'
												 WHEN H.StartTimeUTC IS NULL OR H.ENDTimeUTC IS NULL THEN 'N/A'
												 ELSE 'Close' END) 
												 WHEN H.StartTimeUTC >= H.ENDTimeUTC THEN (CASE WHEN CAST(@requestedTime AS TIME) >= H.StartTimeUTC AND CONVERT(int,SUBSTRING(CONVERT(varchar(20),@requestedTime),10,2)) < CONVERT(int,SUBSTRING(CONVERT(varchar(20),H.ENDTimeUTC),10,2))+24 THEN 'Open'
												 WHEN CAST(@requestedTime AS TIME) < H.ENDTimeUTC THEN 'Open'
												 ELSE 'Close' END) 
												 WHEN H.StartTimeUTC IS NULL OR H.ENDTimeUTC IS NULL THEN 'N/A'
											ELSE 'N/A' END 
						   FROM #B RL  
                           LEFT JOIN HcHoursFilterTime H ON H.RetailLocationID = RL.RetailLocationID AND HcDaysOfWeekID = @DateNameID 
						 -- LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
						  LEFT JOIN #RetailItemsonSale T ON T.RetailLocationID = RL.RetailLocationID
						  LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode AND RL.City = G.City					  
						  WHERE (@RegionAppID = 0 OR @RegionAppID = 1)  
						  AND (@UserPreferredCity = 0 AND RL.HcCityExperienceID = @CityExperienceID1) or @UserPreferredCity=1
						  AND ((@CityID IS NULL AND 1=1) OR (RL.HcCityID IN (SELECT Param FROM #city1)))						 
						--  AND ((@CategoryID IS NULL AND 1=1) OR (RB.BusinessCategoryID IN (SELECT BusCatIDs FROM #BusinessCategoryIDs)))
						--  AND ((@Interests IS NULL AND 1=1) OR (FRL.HcFilterID IN (SELECT IntIDs FROM #Interests)))						 
						  AND RL.RetailName LIKE CASE WHEN @SearchKey IS NULL THEN '%' ELSE '%'+@SearchKey+'%' END
									
						)Retailer
						
				END

				
				
				 SELECT DISTINCT  
                                         R.RetailID     
                                         , RetailName 
                                         , R.RetailLocationID retailLocationID
                                         , R.Address1 
                                         , R.Address2 
                                         , R.Address3 
                                         , R.Address4 
                                         , R.City City
                                         , R.State State
                                         , R.PostalCode PostalCode    
                                         , RetailerImagePath      
										 , Distance   = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1), 0)
                                         , DistanceActual = ISNULL(ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1),0)
                                         , SaleFlag
                                         , R.RetailLocationLatitude 
                                         , R.RetailLocationLongitude  
                                         --, BusinessCategoryID 
                                         --, BusinessCategoryDisplayValue   
										 , locationOpen  
                                  INTO #Retail1  
                                  FROM #Retail R
									 INNER JOIN RetailLocation RL ON RL.RetailLocationID= R.RetailLocationID 
									 LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode 
									 
									 SELECT DISTINCT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
                                                                                                                     ELSE SplashAdImagePath
                                                                                                                END
                                                                 , SplashAdID  = ASP.AdvertisementSplashID
                                                                 , R.RetailLocationID  into #a1
                                                       FROM #Retail R
                                                              INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
                                                              INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
                                                              AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)   
															  
															  SELECT DISTINCT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
                                                                                                              ELSE AB.BannerAdImagePath
                                                                                                       END 
                                                                , RibbonAdURL = AB.BannerAdURL
                                                                , RetailLocationAdvertisementID = AB.AdvertisementBannerID
                                                                , R.RetailLocationID into #b11
                                                        FROM #Retail R 
                                                       INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
                                                       INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
                                                       WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)                         						                       
              
               SELECT DISTINCT --Row_Num rowNumber  ,
                                          RetailID retailerId    
                                         , RetailName retailerName
                                         , R.RetailLocationID retailLocationID
                                         , Address1 retaileraddress1
                                         , Address2 retaileraddress2
                                         , Address3 retaileraddress3
                                         , Address4 retaileraddress4
                                         , City City
                                         , State State
                                         , PostalCode PostalCode    
                                         , RetailerImagePath logoImagePath     
                                         , Distance 
                                         , DistanceActual     
                                         , S.BannerAdImagePath bannerAdImagePath    
                                         , B.RibbonAdImagePath ribbonAdImagePath    
                                         , B.RibbonAdURL ribbonAdURL    
                                         , B.RetailLocationAdvertisementID advertisementID  
                                         , S.SplashAdID splashAdID
                                         , SaleFlag
                                         , RetailLocationLatitude retLatitude
                                         , RetailLocationLongitude retLongitude 
                                         --, BusinessCategoryID 
                                         --, BusinessCategoryDisplayValue     
										 , locationOpen  
                                  INTO #RetailerList  
                                  FROM #Retail1 R
                                  LEFT JOIN #a1 S ON R.RetailLocationID = S.RetailLocationID
                                  LEFT JOIN #b11 B ON R.RetailLocationID = B.RetailLocationID	
								  --WHERE (@LocalSpecials = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))								  
                                  --ORDER BY rowNumber
								
								   	 

				SELECT     --rowNumber = IDENTITY(INT,1,1)  
						rownumber = ROW_NUMBER() OVER 
								(ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
										WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
										WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
								END ASC ,Distance)
                        ,  retailerId    
                        ,  retailerName
                        ,  retailLocationID
                        ,  retaileraddress1
                        ,  retaileraddress2
                        ,  retaileraddress3
                        ,  retaileraddress4
                        , City City
                        , State State
                        , PostalCode PostalCode    
                        , logoImagePath     
                        , Distance 
                        , DistanceActual     
                        , bannerAdImagePath    
                        , ribbonAdImagePath    
                        , ribbonAdURL    
                        , advertisementID  
                        , splashAdID
                        , SaleFlag
                        , retLatitude
                        , retLongitude 
                        --, BusinessCategoryID 
                        --, BusinessCategoryDisplayValue    
						, locationOpen  
				INTO #FinalList	 
				FROM #RetailerList
				WHERE (@LocalSpecials = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))	  
				--ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
				--						WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
				--						WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
				--				END ASC ,Distance
								--CASE WHEN @SortOrder = 'DESC' AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
								--		WHEN @SortOrder = 'DESC' AND @SortColumn = 'RetailerName' THEN CAST(retailerName AS SQL_VARIANT) 
								--		WHEN @SortColumn IS NULL AND @SortOrder ='DESC' THEN CAST(DistanceActual AS SQL_VARIANT)
								--END DESC  
				
				--To capture max row number.  
				SELECT @MaxCnt = COUNT(rowNumber) FROM #FinalList
                                  
				--This flag is a indicator to enable "More" button in the UI.   
				--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
				
								  							   
               -- PRINT 'A'   
                      -- select '4',getdate()             
                --UserTracking.                                
                                   
                --Table to track the Retailer List.
                CREATE TABLE #Temp(rownumb int identity(1,1)
					,RetailerListID int
                    ,MainMenuID int
                    ,RetailID int
                    ,RetailLocationID int
                    ,HcCitiExperienceID int)  
                      
                      
				--Capture the impressions of the Retailer list.
				INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID                                                                                                                    
                                                            , RetailID
                                                            , RetailLocationID
                                                            , HcCitiExperienceID
                                                            , FindCategoryID 
                                                            , DateCreated)
                OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID,MainMenuID, RetailLocationID)                                           
                                                                        SELECT @MainMenuId
                                                                            , retailerId
                                                                            , RetailLocationID
                                                                            , @CityExperienceID1 
                                                                            , CASE WHEN @CategoryID IS NOT NULL THEN NULL ELSE 0 END
                                                                            , GETDATE()
                                                                        FROM #FinalList  
																		--INNER JOIN #BusinessCategoryIDs B ON R.BusinessCategoryID = B.BusCatIDs
																		ORDER BY rowNumber
																		
						
																						
				--Table to Track Cities
						
				SELECT DISTINCT HcCityID
				INTO #City
				FROM #Retail
						
				INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
																,CityID
																,DateCreated
																)
														SELECT @MainMenuId
																,HcCityID
																,Getdate()
														FROM #City

			                                                                                 
                --Display the Retailer along with the keys generated in the Tracking table.                                  

				--select * from #Temp

                        SELECT rowNumber
                            , T.RetailerListID retListID
                            , retailerId
                            , retailerName
                            , T.RetailLocationID
                            , retaileraddress1
                            , retaileraddress2
                            , retaileraddress3
                            , retaileraddress4
                            , City
                            , State
                            , PostalCode
                            , logoImagePath
                            , Distance =  ISNULL(DistanceActual, Distance)
                            , bannerAdImagePath
                            , ribbonAdImagePath
                            , ribbonAdURL
                            , advertisementID
                            , splashAdID
                            , SaleFlag   
                            , retLatitude
                            , retLongitude      
                            --, BusinessCategoryID catId
                            --, BusinessCategoryDisplayValue catName         
							, locationOpen                                                                         
                        FROM #Temp T
                        INNER JOIN #FinalList R ON  R.rowNumber  = T.rownumb --AND T.RetailLocationID =R.retailLocationID --AND T.RetailID =R.retailerId 
						WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit										 
						Order by rowNumber             
                     
                        --To display bottom buttons                                   

                        EXEC [HubCitiApp2_8_2].[usp_HcFunctionalityBottomButtonDisplay] @HcHubCitiID1, @ModuleName, @UserID1, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT


                     --Confirmation of Success
                     SELECT @Status = 0  
					 
				 --To display message when no Retailers display for user preferred cities.				

				 DECLARE @UserPrefCities NVarchar(MAX)

				 IF NOT EXISTS(SELECT 1 FROM HcCityExperienceRetailLocation WHERE HcCityExperienceID = @CityExperienceID1)
				 BEGIN 
					SELECT @NoRecordsMsg = 'No Records Found.'
				 END
				 ELSE IF (@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0)  AND (@SearchKey IS NULL) AND @CategoryID IS NULL AND @CityID IS NULL
						              AND @LocalSpecials = 0 AND @Interests IS NULL
				 BEGIN 
					
					SELECT @UserPrefCities = COALESCE(@UserPrefCities+', ' ,'') + UPPER(LEFT(CityName,1))+LOWER(SUBSTRING(CityName,2,LEN(CityName))) 
					FROM HcUsersPreferredCityAssociation P
					INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
					WHERE HcHubcitiID = @HcHubCitiID1 AND HcUserID = @UserID1
						
					SELECT @NoRecordsMsg = 'There currently is no information for your city preferences.\n\n'+  @UserPrefCities +
										'.\n\nUpdate your city preferences in the settings menu.'
						
				 END 
				 ELSE IF ((@RegionAppID =1) AND (ISNULL(@MaxCnt,0) = 0)) 
						 AND (@SearchKey IS NOT NULL OR @CategoryID IS NOT NULL OR @CityID IS NOT NULL
						              OR @LocalSpecials = 1 OR @Interests IS NOT NULL)
				 BEGIN
					
					SELECT @NoRecordsMsg = 'No Records Found.'

				 END  
				 ELSE IF (@RegionAppID = 0) AND (ISNULL(@MaxCnt,0) = 0)
				 BEGIN
						SELECT @NoRecordsMsg = 'No Records Found.'
				 END    
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
					
                     PRINT 'Error occured in Stored Procedure [usp_HcCityExperienceRetailerList].'            
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;










GO
