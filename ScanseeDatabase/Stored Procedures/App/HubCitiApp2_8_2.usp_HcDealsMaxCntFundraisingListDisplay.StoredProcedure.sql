USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcDealsMaxCntFundraisingListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : [usp_HcDealsMaxCntFundraisingListDisplay]
Purpose                    : To display List of FundraisingEvents.
Example                    : [usp_HcDealsMaxCntFundraisingListDisplay]

History
Version              Date               Author     Change Description
--------------------------------------------------------------------------------- 
1.0                  04/29/2015         SPAN        1.1
---------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcDealsMaxCntFundraisingListDisplay]
(   
      --Input variable.        
        @UserID Int  
	   , @HubCitiID int     
  
       --Output Variable 
       , @MaxCnt int  output

)
AS
BEGIN

       BEGIN TRY

					 DECLARE @RegionAppID int
					 DECLARE @HcAppListID int

					 SELECT @HcAppListID = HcAppListID
					 FROM HcApplist
					 WHERE HcAppListName = 'RegionApp'

					 SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
					 FROM HcHubCiti H
					 WHERE HcHubCitiID = @HubCitiID
				    
					--MenuItem
					SELECT DISTINCT H.HcHubCitiID,HE.HcFundraisingCategoryID,E.FundraisingCategoryName,HE.HcFundraisingID,HE.HcFundraisingDepartmentID
					INTO #RegionCat
					FROM HcHubCiti H                     
					INNER JOIN HcFundraisingCategoryAssociation HE ON H.HcHubCitiID = HE.HcHubCitiID
					INNER JOIN HcFundraisingCategory E ON HE.HcFundraisingCategoryID = E.HcFundraisingCategoryID					 
					WHERE H.HcHubCitiID = @HubCitiID					

					CREATE TABLE #RHubcitiList(HchubcitiID Int) 
					INSERT INTO #RHubcitiList( HchubcitiID)
					SELECT HcHubCitiID
					FROM
						(SELECT RA.HcHubCitiID
						FROM HcHubCiti H
						LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
						WHERE H.HcHubCitiID  = @HubcitiID
						UNION ALL
						SELECT @HubcitiID)A

					DECLARE @HubCitis varchar(100)
					SELECT @HubCitis =  COALESCE(@HubCitis,'')+ CAST(HchubcitiID as varchar(100)) + ',' 
					FROM #RHubcitiList


					 CREATE TABLE #Fund(RowNum INT IDENTITY(1, 1)
										,HcFundraisingID  int
										,FundraisingName Varchar(1000)  
										,ShortDescription Varchar(2000)
										,LongDescription  nVarchar(max)                                                
										,HcHubCitiID Int 
										,StartDate date
										,EndDate date
										,StartTime Time
										,EndTime Time                                     
										,HcFundraisingCategoryID  int
										,FundraisingCategoryName varchar(1000)
										,DepartmentFlag bit
										,CityName varchar(100))

					
							 INSERT INTO #Fund(HcFundraisingID  
												  ,FundraisingName  
												  ,ShortDescription 
												  ,LongDescription                                         
												  ,HcHubCitiID  
												  ,StartDate 
												  ,EndDate 
												  ,StartTime 
												  ,EndTime                                      
												  ,HcFundraisingCategoryID  
												  ,FundraisingCategoryName
												  ,DepartmentFlag
												  ,CityName )	   
                     
							 --To display List of Events for MenuItem
							 SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	  
													,ShortDescription
													,LongDescription                                                     
													,F.HcHubCitiID 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
												   ,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0) --@FundDepartmentFlag
												   ,C.CityName	
							FROM HcFundraising F 
							INNER JOIN #RHubcitiList HL ON HL.HchubcitiID=F.HcHubCitiID
							INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID=FCA.HcFundraisingID 
							INNER JOIN HcFundraisingCategory  FC ON FC.HcFundraisingCategoryID =FCA.HcFundraisingCategoryID AND F.RetailID IS NULL
							LEFT JOIN HcRetailerFundraisingAssociation 	RF ON RF.HcFundraisingID=F.HcFundraisingID								                         
							LEFT JOIN HcFundraisingAppsiteAssociation FA ON F.HcFundraisingID = FA.HcFundraisingID
							LEFT JOIN HcAppSite HA ON FA.HcAppSiteID = HA.HcAppsiteID
							LEFT JOIN HcRetailerAssociation RA ON RA.HcHubCitiID = HL.HchubcitiID AND Associated =1
							LEFT JOIN RetailLocation RL1 ON HA.RetailLocationID = RL1.RetailLocationID AND RL1.Active = 1
							LEFT JOIN HcCity C ON (RL1.City = C.CityName) 
							WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1	AND RF.HcFundraisingID IS NULL	
							UNION ALL								
					
							 --To display List of Events for Retailer
							 SELECT DISTINCT F.HcFundraisingID  
													,FundraisingName = ISNULL(FundraisingOrganizationName+' ','')+FundraisingName	  
													,ShortDescription
													,LongDescription                                                     
													,F.HcHubCitiID 
													,StartDate = CAST(StartDate AS DATE)
													,EndDate = CAST(EndDate AS DATE)								 								 
													,StartTime =CAST(StartDate AS Time)
													,EndTime =CAST(EndDate AS Time)                                     
													,FC.HcFundraisingCategoryID  
												   ,FC.FundraisingCategoryName 
												   ,DepartmentFlag = IIF(FCA.HcFundraisingDepartmentID IS NOT NULL,1,0)      
												   ,C.CityName	
							FROM HcFundraising F 
							 INNER JOIN HcFundraisingCategoryAssociation FCA ON F.HcFundraisingID =FCA.HcFundraisingID 
							 INNER JOIN HcFundraisingCategory FC ON FCA.HcFundraisingCategoryID =FC.HcFundraisingCategoryID 
							 INNER JOIN HcRetailerFundraisingAssociation RF ON F.HcFundraisingID = RF.HcFundraisingID --RA.RetailLocationID = RF.RetailLocationID
							 INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID=RF.RetailLocationID AND Associated =1 AND RA.HcHubCitiID =@HubCitiID
							 INNER JOIN RetailLocation RL1 ON RL1.RetailLocationID =RF.RetailLocationID AND RL1.Active = 1							 
							 INNER JOIN HcCity C ON RL1.City = C.CityName
							 WHERE GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active = 1
							
							--To capture max row number.  
							SELECT @MaxCnt = MAX(RowNum) FROM #Fund
						
				   
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN 
			          
                     PRINT 'Error occured in Stored Procedure [usp_HcDealsMaxCntFundraisingListDisplay].'             
              END;
              
       END CATCH;
END;

























GO
