USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_ClaimYourBusinessForm]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ClaimYourBusinessForm
Purpose					: To display Claim form
Example					: usp_ClaimYourBusinessForm

History
Version		Date				Author					Change Description
--------------------------------------------------------------- 
1.0		  10/04/2016			Sagar Byali				Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE[HubCitiApp2_8_7].[usp_ClaimYourBusinessForm]
(
	  @RetailLocationID INT
	 ,@HcHubcitiID INT
	 ,@HcuserID INT

	  --Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN 
	BEGIN TRAN

		BEGIN TRY


		DECLARE @BusinessCategoryDisplayValue VARCHAR(MAX)
			   ,@RetailID INT
			   ,@ClaimText varchar(max)
			   ,@ClaimImage VArchar(max)
			   ,@ImagePathURL VArchar(100)

		SELECT @ImagePathURL = ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType = 'Hubciti Media Server Configuration'

		SELECT @ClaimText = ClaimText
			  ,@ClaimImage = @ImagePathURL + cast(@HcHubcitiID as varchar(100)) + '/' + ClaimImage 
		FROM HcMenuCustomUI
		WHERE HubCitiId = @HcHubcitiID
		
		--To get Retailid for the input Location
		SELECT @RetailID = RetailID 
		FROM RetailLocation 
		WHERE RetailLocationID = @RetailLocationID

		--To collect Business categories for the Retailer
		SELECT DISTINCT BusinessCategoryDisplayValue 
		INTO #BusinessCategories
		FROM Retailer R
		INNER JOIN RetailerBusinessCategory RB ON RB.RetailerID = R.RetailID
		INNER JOIN BusinessCategory CCC ON CCC.BusinessCategoryID = RB.BusinessCategoryID AND Active = 1 
		WHERE R.RetailID = @RetailID

		--To Create comma seperated Business categories present for the Retailer
		SELECT @BusinessCategoryDisplayValue = COALESCE(@BusinessCategoryDisplayValue+', ' , '') + RTRIM(LTRIM(BusinessCategoryDisplayValue))
		FROM #BusinessCategories
		
		--To Display Business Claim form
		SELECT DISTINCT	   RetailName name
						  ,type = @BusinessCategoryDisplayValue 
						  ,RL.Address1 retAddress
						  ,Rl.Address2 retAddress2
						  ,ISNULL(RetailLocationURL,RetailURL) website
						  ,RL.City city
						  ,RL.State state
						  ,RL.PostalCode postalCode
						  ,phone = CASE WHEN REPLACE(CT.ContactPhone,' ','') IS NOT NULL AND REPLACE(CT.ContactPhone,' ','') <> ' ' THEN REPLACE(CT.ContactPhone,' ','') END 
						  ,CountryName country
						  ,@ClaimImage claimImg
						  ,@ClaimText claimTxt
						  ,RetailKeyword keywords
		FROM Retailer R
		INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID
		LEFT JOIN RetailContact CC ON CC.RetailLocationID = RL.RetailLocationID
		LEFT JOIN Contact CT ON CT.ContactID = CC.ContactID 
		LEFT JOIN CountryCode C ON C.CountryID = RL.CountryID
		LEFT JOIN RetailerKeywords RK ON RK.RetailID = RL.RetailID AND RK.RetailLocationID = RL.RetailLocationID
		WHERE RL.RetailLocationID = @RetailLocationID

		--Set Success status
		SET @Status = 0

		 COMMIT TRAN
		
		 END TRY    	

BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN

			SET @Status = 1
			PRINT 'Error occured in Stored Procedure usp_ClaimYourBusinessForm.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			ROLLBACK TRANSACTION
			
		END;
		 
END CATCH;
END;






GO
