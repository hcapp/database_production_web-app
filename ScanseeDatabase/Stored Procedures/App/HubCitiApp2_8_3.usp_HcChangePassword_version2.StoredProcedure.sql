USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcChangePassword_version2]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcChangePassword]
Purpose					: to update the password if it is changed.
Example					: usp_HcChangePassword 

History
Version		Date			    Author			Change Description
--------------------------------------------------------------- 
1.0			17th Oct 2013		SPAN			Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcChangePassword_version2]
(
	 @UserID int
	,@Password varchar(60)
	,@HubcitiKey varchar(100)
	
	--Output Variable 
	, @IsSuccess BIT output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			IF EXISTS (SELECT 1 FROM HcUser H INNER JOIN HcUserDeviceAppVersion UD ON UD.HcUserID = H.HcUserID
				INNER JOIN HcHubCiti HC ON HC.HcHubCitiID = UD.HcHubCitiID WHERE H.HcUserID = @UserID AND HC.HcHubCitiKey = @HubCitiKey )
			 BEGIN
				UPDATE HcUser SET [Password] = @Password,
								TempPassword = 0,
								TempTime = NULL,
								NoEmailCount = 0
							   --, [ResetPassword] = 1
				WHERE HcUserID = @UserID
				
				SELECT @IsSuccess = 0

			 END
			 else
			 BEGIN
				SELECT @IsSuccess = 1
			 END

		--Confirmation of Success.
		SELECT @Status = 0
			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcChangePassword.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_8_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
			
		END
	END CATCH;
END;










GO
