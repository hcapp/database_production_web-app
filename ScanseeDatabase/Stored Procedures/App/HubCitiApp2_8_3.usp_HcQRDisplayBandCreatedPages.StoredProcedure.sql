USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcQRDisplayBandCreatedPages]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*  
Stored Procedure name	: [usp_HcQRDisplayBandCreatedPages]
Purpose					: To Retrieve the list of Custom pages(Anything Page) associated with the retailer location.
Example					: [usp_HcQRDisplayBandCreatedPages]

History
Version			Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22th Apr 2016	Sagar Byali	        Initial version : usp_HcQRDisplayRetailerCreatedPages
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcQRDisplayBandCreatedPages]
(
	  
	  @UserID int
	, @BandID int
    --, @RetailLocationID int
	--, @AppsiteID int
	, @HcHubCitiID int
    
    --User Tracking Inputs
    --, @RetailerListID int
    , @MainMenuID int
    --, @ScanTypeID bit
    
	--Output Variable 
	, @EventExists bit output
	--, @FundRaisingExists bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Configuration VARCHAR(100)
		DECLARE @QRType INT
		DECLARE @RetailerConfig varchar(50)
		DECLARE @ScanType int
		DECLARE	@RowCount int		
		DECLARE @RetailEventsIconLogo Varchar(1000)
		DECLARE @RetailFundRaisingIconLogo Varchar(1000)

		SET @EventExists = 0
		DECLARE  @FundRaisingExists BIT = 0

		
		
		--SELECT --@RetailLocationID = ISNULL(@RetailLocationID , A.RetailLocationID)
		--	   ,@BandID = ISNULL(@BandID, RL.RetailID)
		--FROM HcAppSite A
		--INNER JOIN Band RL ON A.RetailLocationID = RL.RetailLocationID
		--WHERE HcAppSiteID = @AppsiteID

		
		SELECT @EventExists = 1 
		FROM HcBandEvents RE 
		WHERE BandID= @BandID
		--AND E.EndDate > GETDATE()
		AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND RE.Active=1

		--Verify If FundRaising Exist For A retailer
		--SELECT @FundRaisingExists = 1 
		--FROM HcRetailerFundraisingAssociation RF 
		--INNER  JOIN HcFundraising F ON F.HcFundraisingID =RF.HcFundraisingID 
		--WHERE RF.RetailID = @RetailID AND RetailLocationID = @RetailLocationID
		----AND E.EndDate > GETDATE()
		--AND GETDATE() < ISNULL(EndDate, GETDATE()+1) AND F.Active=1

		--SELECT @ScanType = ScanTypeID
		--FROM HubCitiReportingDatabase..ScanType
		--WHERE ScanType = 'Retailer Summary QR Code'
			   
		SELECT @RetailerConfig= ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Band Media Server Configuration' 
		
		--Get the Server Configuration.
		SELECT  @Configuration= ScreenContent FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'QR Code Configuration'
		AND Active = 1		
				 
		SELECT @QRType = QRTypeID 
		FROM QRTypes 
		WHERE QRTypeName LIKE 'Anything Page'
		
		 
		SELECT @RetailEventsIconLogo=@RetailerConfig+ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Retailer Events Icon Logo'

		--SELECT @RetailFundRaisingIconLogo=@RetailerConfig+ScreenContent  
		--FROM AppConfiguration   
		--WHERE ConfigurationType='Retailer Fundraiser Icon Logo'

		--Fetch all the Created Pages for the given Retailer Location		
		--Fetch all the Created Pages for the given Retailer Location		
		SELECT  DISTINCT QR.QRBandCustomPageIconID pageID
					   , QR.Pagetitle pageTitle
					   , pageImage = CASE WHEN [Image] IS NULL 
										  THEN (SELECT @RetailerConfig + QRbandCustomPageIconImagePath FROM QRBandCustomPageIcons WHERE QRbandCustomPageIconID = QR.QRbandCustomPageIconID) 
										  ELSE @RetailerConfig + CAST(ISNULL(@BandID, (SELECT BandID FROM Band RL WHERE RL.BandID = H.BandID AND rl.bandActive=1)) AS VARCHAR(10))+ '/' + [Image] END
					  , CASE WHEN MediaPath IS NOT NULL THEN  @RetailerConfig + CAST(QR.BandID AS VARCHAR(10)) + '/'+ MediaPath ELSE QR.url ENd   PageLink
					  , ISNULL(SortOrder, 100000) SortOrder	
					  , CASE WHEN ExternalFlag = 1 THEN MediaPath ELSE (SELECT @RetailerConfig + CAST(QR.BandID AS VARCHAR(10))+ '/' + MediaPath FROM QRbandCustomPageMedia WHERE QRbandCustomPageMediaID = M.QRbandCustomPageMediaID) END pageTempLink
					  
		INTO #AnythingPages	  
		FROM QRbandCustomPage QR
		--INNER JOIN QRBandCustomPageAssociation QRCPA ON QR.QRBandCustomPageID = QRCPA.QRBandCustomPageID
		LEFT JOIN QRBandCustomPageMedia M ON M.QRBandCustomPageID = QR.QRBandCustomPageID	
		LEFT JOIN HcbandAppSite H ON H.BandID = QR.BandID	
		WHERE (ISNULL(@BandID, 0) <> 0 AND QR.BandID = @BandID)
			--	OR
			-- (ISNULL(@AppsiteID, 0) <> 0 AND H.HcAppSiteID = @AppsiteID))
		AND QR.QRTypeID = @QRType
		AND ((StartDate IS NULL AND EndDate IS NULL) 
		      OR 
		     (CAST(GETDATE() AS DATE) BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1))
		    )
		ORDER BY ISNULL(SortOrder, 100000) ,Pagetitle  

		
		--Check for the existance of the Anything Pages. 
		SELECT @RowCount =@@ROWCOUNT 

		----Appsite Logistic implementation
		--DECLARE @AppsiteConfig Varchar(500) 
		----DECLARE @HubCitiID int

		--SELECT @AppsiteConfig = ScreenContent
		--FROM AppConfiguration 
		--WHERE ConfigurationType LIKE 'Hubciti Media Server Configuration'



		--SELECT HcAppsiteLogisticID pageID
		--		,LogisticName pageTitle
		--		,pageImage = @AppsiteConfig + CAST(RL.HcHubCitiID AS VARCHAR)+ '/'+ LogisticImage
		--		,PageLink = @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Appsite Logistic Page') AS VARCHAR(10)) 
		--			+ '.htm?logisticsId=' + CAST(HcAppsiteLogisticID AS VARCHAR(100)) + '&hubcitiId=' +CAST(RL.HcHubCitiID AS VARCHAR(100))
		--INTO #AppsiteLogisticMap
		--FROM HcAppsiteLogistic RL
		--INNER JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = @HcHubCitiID AND Associated = 1
		--WHERE RL.RetailLocationID = @RetailLocationID AND RL.HcHubCitiID = @HcHubCitiID
		--AND (CAST(GETDATE() AS DATE) BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1))
		
		--User Tracking	




		--UPDATE HubCitiReportingDatabase..RetailerList
		--SET RetailLocationClick = 1 
		--WHERE retailid = @RetailID and retaillocationid= @RetailLocationID AND RetailerListID = @RetailerListID
		

				
			--Capture the impressions of the Anything pages.
		
			--CREATE TABLE #Temp(AnythingPageListID INT, RetailerListID int, PageID int)
			
			--INSERT INTO HubCitiReportingDatabase..AnythingPageList(RetailerListID
			--													 , AnythingPageID
			--													 , AppsiteID
			--													 , MainMenuID
			--													 , DateCreated)
													
			--OUTPUT inserted.AnythingPageListID, inserted.RetailerListID, inserted.AnythingPageID INTO #Temp(AnythingPageListID, RetailerListID, PageID)
														
			--SELECT DISTINCT @RetailerListID
			--		, pageID
			--		, @AppsiteID
			--		, @MainMenuID
			--		, GETDATE()
			--FROM #AnythingPages
														
			--IF @AppsiteID IS NOT NULL
			--BEGIN
			--	CREATE TABLE #Temp1(AppsiteListID INT, AppsiteID int)
				
			--	INSERT INTO HubCitiReportingDatabase..AppsiteList(AppsiteID
			--														 , MainMenuID
			--														 , DateCreated)
														
			--	OUTPUT inserted.AppsiteListID, inserted.AppsiteID INTO #Temp1(AppsiteListID, AppsiteID)
															
			--												SELECT @AppsiteID															 
			--													 , @MainMenuID
			--													 , GETDATE()
			--												FROM #AnythingPages
			--END												
			
				declare @RetailerListID int
			--Display the Anything page list along with the primary key of the Tracking table. Also to list events first followed by anything pages.					
			
			IF @EventExists = 1 AND @FundRaisingExists =1
			BEGIN
					SELECT -- 99999 AnythingPageListID
						   --,@RetailerListID retListID
						   9999 pageID
						   ,'Events' pageTitle
						   ,@RetailEventsIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder	

					UNION ALL

					SELECT --99999 AnythingPageListID
						   --,@RetailerListID retListID
						   9999 pageID
						   ,'Fundraisers' pageTitle
						   ,@RetailFundRaisingIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder	
					UNION ALL
					
					--SELECT 99999 AnythingPageListID
					--		,@RetailerListID retListID
					--		,pageID
					--		,pageTitle
					--		,pageImage
					--		,PageLink
					--		,NULL pageTempLink	
					--	    ,0 SortOrder	
					--FROM #AppsiteLogisticMap

					--UNION ALL

					SELECT   --   T.AnythingPageListID
							-- , @RetailerListID retListID
							   A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink	
							 , A.SortOrder SortOrder		 
					FROM #AnythingPages A
					----INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle							
			END

			ELSE IF @EventExists = 1 AND @FundRaisingExists =0
			BEGIN


			


					SELECT ---99999 AnythingPageListID
						  -- ,@RetailerListID retListID
						    9999 pageID
						   ,'Events' pageTitle
						   ,@RetailEventsIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder
					
					UNION ALL
					
					--SELECT--- 99999 AnythingPageListID
					--		--,@RetailerListID retListID
					--		 pageID
					--		,pageTitle
					--		,pageImage
					--		,PageLink
					--		,NULL pageTempLink	
					--	    ,0 SortOrder	
					--FROM #AppsiteLogisticMap

					--UNION ALL

					SELECT --T.AnythingPageListID
							-- , @RetailerListID retListID
							  A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink	
							 , A.SortOrder SortOrder		 
					FROM #AnythingPages A
					--INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle						
			END


			ELSE IF @EventExists = 0 AND @FundRaisingExists =1
			BEGIN
					SELECT --99999 AnythingPageListID
						  --- ,@RetailerListID retListID
						    9999 pageID
						   ,'Fundraisers' pageTitle
						   ,@RetailFundRaisingIconLogo pageImage
						   ,NULL PageLink
						   ,NULL pageTempLink	
						   ,Null SortOrder	
					UNION ALL
					
					--SELECT 99999 AnythingPageListID
					--		,@RetailerListID retListID
					--		,pageID
					--		,pageTitle
					--		,pageImage
					--		,PageLink
					--		,NULL pageTempLink	
					--	    ,0 SortOrder	
					--FROM #AppsiteLogisticMap

					--UNION ALL

					SELECT --T.AnythingPageListID
							-- , @RetailerListID retListID
							   A.pageID
							 , A.pageTitle 
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink	
							 , A.SortOrder SortOrder		 
					FROM #AnythingPages A
					--INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle				
			END
			
			ELSE 
			BEGIN
					--SELECT 99999 AnythingPageListID
					--		,@RetailerListID retListID
					--		,pageID
					--		,pageTitle
					--		,pageImage
					--		,PageLink
					--		,NULL pageTempLink	
					--	    ,0 SortOrder	
					--FROM #AppsiteLogisticMap
					
					--UNION ALL

					SELECT --T.AnythingPageListID
							-- , @RetailerListID retListID
							   A.pageID
							 , A.pageTitle
							 , A.pageImage
							 , A.PageLink	
							 , A.pageTempLink
							 , A.SortOrder SortOrder			 
					FROM #AnythingPages A
					--INNER JOIN #Temp T ON A.pageID = T.PageID 
					ORDER BY SortOrder,pageTitle	
			END

		
			----Check if this SP is called when scanned and update associated tables.
			--IF ISNULL(@ScanTypeID, 0) <> 0
			--BEGIN
				
			--	INSERT INTO HubCitiReportingDatabase..Scan(MainMenuID
			--											  , ScanTypeID
			--											  , Success
			--											  , ID
			--											  , CreatedDate)
			--									SELECT  @MainMenuID 
			--										  , @ScanType
			--										  , (CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
			--										  , pageID
			--										  , GETDATE()
			--									FROM #AnythingPages
														
				


	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcQRDisplayRetailerCreatedPages.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_3_3].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;



















GO
