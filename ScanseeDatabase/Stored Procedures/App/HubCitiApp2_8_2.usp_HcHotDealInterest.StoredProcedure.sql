USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHotDealInterest]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcHotDealInterest
Purpose					: To store Users interest. (Like is a when a User click the URL from a Hot Deal detail screen. 
													At this point record interest in ProductHotDealInterest table
													and	Dislike is when a User swipe-deletes a Hot Deal from 
													the list. Record dislike in ProductHotDealInterest table)

Example					: usp_HcHotDealInterest 1, 1, 1, '6/23/2011'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15thNov2013 	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHotDealInterest]
(
	  @UserID int
	, @HotDealID int
	, @Interested bit
	, @InterestDate datetime
	, @HcHubcitiID Int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			IF EXISTS (SELECT 1 FROM HcProductHotDealInterest WHERE HcUserID  = @UserID AND ProductHotDealID =  @HotDealID)
			BEGIN
				UPDATE HcProductHotDealInterest 
				SET Interested = @Interested 
					, HotDealInterestDate = @InterestDate
				WHERE HcUserID  = @UserID 
					AND ProductHotDealID = @HotDealID 
			END
			ELSE
			BEGIN
				INSERT INTO HcProductHotDealInterest
				   (HcUserID
				   ,[ProductHotDealID]
				   ,[Interested]
				   ,[HotDealInterestDate])
				VALUES
				   (@UserID 
				   ,@HotDealID 
				   ,@Interested 
				   ,@InterestDate)
			END
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcHotDealInterest.'		
			--- Execute retrieval of Error info.
			EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












































GO
