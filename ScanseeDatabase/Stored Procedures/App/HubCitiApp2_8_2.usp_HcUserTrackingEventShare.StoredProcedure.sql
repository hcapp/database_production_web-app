USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserTrackingEventShare]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_HcUserTrackingEventShare 
Purpose					: To track event share details.  
Example					: usp_HcUserTrackingEventShare
  
History  
Version    Date				Author			Change         Description  
----------------------------------------------------------------------   
1.0        7th Nov 2013		Mohith H R		Initial Version  
---------------------------------------------------------------------- 
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserTrackingEventShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @EventID int 
 , @TargetAddress Varchar(1000)   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         
		 INSERT INTO HubCitiReportingDatabase..ShareEventPage(MainMenuID
														  ,ShareTypeID
														  ,TargetAddress
														  ,EventID
														  ,DateCreated)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@EventID 
		        ,GETDATE())	
		        
		 --Confirmation of failure.
		 SELECT @Status = 0           							     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingEventShare.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   --Confirmation of failure.
   SELECT @Status = 1 
  END;            
 END CATCH;  
END;











































GO
