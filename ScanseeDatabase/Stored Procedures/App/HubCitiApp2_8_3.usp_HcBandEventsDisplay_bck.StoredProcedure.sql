USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcBandEventsDisplay_bck]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name		 : [HubCitiApp2_8_3].[usp_HcBandEventsDisplay]
Purpose                      : To display List of BandEvents.
Example                      : [HubCitiApp2_8_3].[usp_HcBandEventsDisplay]

History
Version      Date                Author               Change Description
------------------------------------------------------------------------------------ 
1.0          04/21/2016			Bindu T A		 Initial Version - usp_HcBandEventsDisplay
2.0			 07/14/2016			Bindu T A		 Changes W.R.T NearBy, Ongoing and Upcoming Band Events
3.0          09/14/2016         Shilpashree      HubRegion Changes - Adding City
4.0			 09/15/2016			Bindu T A		 BandEvents with Band Association Changes
------------------------------------------------------------------------------------
*/

--exec [HubCitiApp2_8_3].[usp_HcBandEventsDisplay] 2162,10533,null,null,null,null,null,null,0,'clr screen',null,null,null,null,null,null,null,null,null,3 ,null,null,null,null,null,null,null,null

CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcBandEventsDisplay_bck]
(   
    --Input variable.        
         @HubCitiID int
       , @UserID Int
       , @CategoryID VARCHAR(2000)
       , @Latitude Float
       , @Longitude Float
	   , @Radius INT
       , @Postalcode Varchar(200)
       , @SearchParameter Varchar(1000)
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @SortColumn Varchar(200)
       , @SortOrder Varchar(100)
       , @GroupColumn varchar(50)
       , @HcMenuItemID int
       , @HcBottomButtonID int
       , @RetailID int
       , @RetailLocationID int
	   , @FundRaisingID Int
	   , @EventDate Date
	   , @BandEventTypeID INT 
	   , @CityIDs Varchar(2000)

       --User Tracking
       , @MainMenuID Int
  
       --Output Variable 
       , @UserOutOfRange bit output
       , @DefaultPostalCode VARCHAR(10) output
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN
       BEGIN TRY
            DECLARE @UpperLimit int
            DECLARE @DistanceFromUser FLOAT
            DECLARE @UserLatitude float
            DECLARE @UserLongitude float 
            DECLARE @RetailConfig Varchar(1000)
			DECLARE @Config VARCHAR(500)
			DECLARE @BandCategoryID VARCHAR(1000)
			DECLARE @BandID INT
			DECLARE @IsRegionApp bit = 0
			DECLARE @isPlayingToday Bit = 0

			SET @BandID = @RetailID
			SET @BandCategoryID = @CategoryID
			--Changes for Band 7/14/16
			SET @BandEventTypeID = NULL

			IF EXISTS (SELECT 1 FROM HcMenuItem WHERE HcMenuItemID = @HcMenuItemID AND HcLinkTypeID = (SELECT HcLinkTypeID FROM HcLinkType WHERE LinkTypeName = 'Playing Today')) 
			BEGIN
				SET @isPlayingToday=1
			END

														   
			IF (@Radius IS NULL)
			BEGIN
			SELECT @Radius = LocaleRadius
			FROM UserPreference WHERE UserID = @UserID
			END


            SELECT @RetailConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'Web Band Media Server Configuration'  
			
			SELECT @Config = ScreenContent
            FROM AppConfiguration
            WHERE ConfigurationType = 'Hubciti Media Server Configuration' 
			
			SELECT Param CatID
			INTO #CategoryList
			FROM fn_SplitParam(@BandCategoryID,',')

			SELECT Param CityID
			INTO #CitiIDs
			FROM fn_SplitParam(@CityIDs,',')

			IF (@Postalcode IS NULL)
			BEGIN
				SELECT @Postalcode  = Postalcode
				FROM HcUser
				WHERE HcUserID = @UserID
			END

			IF @Latitude IS NULL AND @Postalcode IS NOT NULL
			BEGIN
			SELECT @Latitude =Latitude 
					,@Longitude =Longitude
			FROM GeoPosition
			WHERE PostalCode =@Postalcode 
			END

            SET @UserLatitude = @Latitude
            SET @UserLongitude = @Longitude                  

            IF (@UserLatitude IS NULL) 
            BEGIN
                SELECT @UserLatitude = Latitude, @UserLongitude = Longitude
                FROM BandUsers A
                INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                WHERE BandUserID = @UserID 
            END
            --Pick the co ordinates of the default postal code if the user has not configured the Postal Code.
            IF (@UserLatitude IS NULL) 
            BEGIN
                SELECT @UserLatitude = Latitude, @UserLongitude = Longitude
                FROM HcHubCiti A
                INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                WHERE A.HcHubCitiID = @HubCitiID
            END

			SELECT ID, Day
			INTO #DayLookup
			FROM 			
			(
				SELECT 1 AS 'ID', 'Sunday' 'Day'
				UNION ALL
				SELECT 2, 'Monday'
				UNION ALL
				SELECT 3, 'Tuesday'
				UNION ALL
				SELECT 4, 'Wednesday'
				UNION ALL
				SELECT 5, 'Thursday'
				UNION ALL
				SELECT 6, 'Friday'
				UNION ALL
				SELECT 7, 'Saturday')A
					                           
            --To get the row count for pagination.   
            SELECT @UpperLimit = @LowerLimit + ScreenContent  
            FROM AppConfiguration   
            WHERE ScreenName = @ScreenName AND ConfigurationType = 'Pagination' AND Active = 1
			
			--SELECT @Postalcode

            --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
            EXEC [HubCitiApp2_8_3].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @Postalcode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
            SELECT @Postalcode = ISNULL(@DefaultPostalCode, @Postalcode)
			
			--SELECT @Postalcode
            
			--Derive the Latitude and Longitude in the absence of the input.
            IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
            BEGIN
                IF @Postalcode IS NULL
                BEGIN
                        SELECT @Latitude = G.Latitude, @Longitude = G.Longitude
                        FROM GeoPosition G
                        INNER JOIN Users U ON G.PostalCode = U.PostalCode
                        WHERE U.UserID = @UserID
                END
                ELSE
                BEGIN
                        SELECT @Latitude = Latitude, @Longitude = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @Postalcode
                END
            END
			
			--------------------Region App code------------------------------
			SELECT @IsRegionApp = IIF(HcApplistID = 2,1,0)
			FROM HcHubCiti
			WHERE HcHubCitiID = @HubCitiID

			SELECT HcHubCitiID
			INTO #RHubcitiList
			FROM
				(SELECT RA.HcHubCitiID
				FROM HcHubCiti H
				LEFT JOIN HcRegionAppHubcitiAssociation RA ON H.HcHubCitiID = RA.HcRegionAppID
				WHERE H.HcHubCitiID  = @HubcitiID
				UNION ALL
				SELECT @HubcitiID)A

			DELETE FROM #RHubcitiList WHERE HcHubCitiID IS NULL

			--SELECT DISTINCT P.HcHubcitiID
			--		, P.HcUserID
			--		, HcCityID = IIF(P.HcCityID IS NULL, LA.HcCityID, P.HcCityID)
			--INTO #PreferredCiti
			--FROM HcUsersPreferredCityAssociation P
			--INNER JOIN #RHubcitiList H ON P.HcHubcitiID = H.HcHubcitiID
			--INNER JOIN HcLocationAssociation LA ON H.HcHubcitiID = LA.HcHubCitiID
			--WHERE P.HcUserID = @UserID

			--SELECT DISTINCT HcHubcitiID, HcUserID, P.HcCityID, C.CityName 
			--INTO #PreferredCities
			--FROM #PreferredCiti P
			--INNER JOIN HcCity C ON P.HcCityID = C.HcCityID
			
			SELECT DISTINCT LA.HcHubCitiID,LA.HcCityID,LA.City AS CityName
			INTO #PreferredCities
			FROM #RHubcitiList H
			INNER JOIN HcLocationAssociation LA ON H.HcHubcitiID = LA.HcHubCitiID
			----------------------------------------------------------------------------
			
			SELECT DISTINCT  HE.HcBandEventID
							,HcBandEventName
							,ShortDescription
							,LongDescription
							,ImagePath
							,HE.HcHubCitiID
							,StartDate
							,EndDate
							,MoreInformationURL
							,HE.HcEventRecurrencePatternID
							,EventFrequency
							,ISNULL(OnGoingEvent,0)OnGoingEvent
							,B.BandID
							,B.BandName
							,Active
							,recurringDays = CASE WHEN R.RecurrencePattern = 'Daily'
												  THEN CASE WHEN EI.HcBandEventIntervalID IS NULL 
																		THEN 'Occurs Every ' +IIF(HE.RecurrenceInterval = 1, '',  CAST(HE.RecurrenceInterval AS VARCHAR(100))+' ') + 'Day(s)' 	
															ELSE 'Occurs Every Weekday' 
													  END
												 WHEN R.RecurrencePattern = 'Weekly'
												 THEN 'Occurs Every '+IIF(RecurrenceInterval = 1, '', CAST(RecurrenceInterval AS VARCHAR(10))+' ') + 'Week(s) on '
																				+ STUFF((SELECT ', ' + [DayName]
																				FROM HcBANDEventInterval F			
																				LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																				WHERE F.HcBandEventID = HE.HcBandEventID
																				ORDER BY L.ID
																				FOR XML PATH('')), 1, 2, '')
												 WHEN R.RecurrencePattern = 'Monthly'
												 THEN CASE WHEN [DayName] IS NULL 
																THEN 'Day '+ CAST(DayNumber AS VARCHAR(10)) + ' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
														    ELSE 'The ' + CASE WHEN DayNumber = 1 THEN 'First '
																				 WHEN DayNumber = 2 THEN 'Second '
																				 WHEN DayNumber = 3 THEN 'Third '
																				 WHEN DayNumber = 4 THEN 'Fourth '
																				 WHEN DayNumber = 5 THEN 'Last ' END													  
															   +  STUFF((SELECT ', ' + [DayName]
																		 FROM HcBANDEventInterval F			
																		 LEFT JOIN #DayLookup L ON L.Day = F.DayName																																							
																		 WHERE F.HcBandEventID = HE.HcBandEventID
																		 ORDER BY L.ID
																		 FOR XML PATH('')), 1, 2, '') +' of Every '+ IIF(MonthInterval = 1, '', CAST(MonthInterval AS VARCHAR(10))+' ') + 'month(s)'
														 END
										  END
							,HE.HcEventRecurrencePatternID recurrencePatternID
							,R.RecurrencePattern recurrencePatternName	
							,[isWeekDay] = CASE WHEN R.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
							-- ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
							,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
							,ByDayNumber = CASE WHEN R.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
							,DayNumber
							,EventListingImagePath 
							,HE.BandTicketURL
				INTO #BandEvents
				FROM HcBandEvents HE
				LEFT JOIN HcBandEventsAssociation HA ON HA.HcBandEventID= HE.HcBandEventID
				LEFT JOIN Band B ON HA.BandID = B.BandID AND B.BandActive = 1
				LEFT JOIN #RHubcitiList RH ON HE.HcHubCitiID = RH.HcHubcitiID
				LEFT JOIN HcBandEventInterval EI ON HE.HcBandEventID = EI.HcBandEventID
				LEFT JOIN HcEventRecurrencePattern R ON R.HcEventRecurrencePatternID = HE.HcEventRecurrencePatternID
				WHERE (HE.HcHubCitiID = @HubCitiID OR HE.HcHubCitiID IS NULL)
				AND CAST(GETDATE() AS DATE) <=  ISNULL(CAST(EndDate AS DATE), GETDATE()+1)  AND HE.Active = 1	

				SELECT * FROM #BandEvents

				  
				CREATE TABLE #Temp1(Bandid int,EventID Int
                                    ,EventName Varchar(max) 
									,BandName Varchar(max)
									,ShortDescription Varchar(max) 
									,LongDescription Varchar(max)                                                     
                                    ,HcHubCitiID Int
									,EventAddress Varchar(5000)
									,EventLatitude Float
									,EventLongitude Float
                                    ,ImagePath Varchar(5000) 
                                    ,MoreInformationURL Varchar(max)                                                    
                                    ,StartDate Date
                                    ,EndDate Date
                                    ,StartTime VARCHAR(15)
                                    ,EndTime VARCHAR(15)                                   
                                    ,MenuItemExist Int
                                    ,EventCategoryID INT
                                    ,EventCategoryName Varchar(5000)
									,Distance Int                                                                            
									,DistanceActual	Int									  
                                    ,OnGoingEvent Int
									,RetailLocationID Int
									,Latitude Float
									,Longitude Float
									,SignatureEvent bit
									,recurringDays VARCHAR(5000)
									,listingImgPath VARCHAR(max)
									,isBandFlag INT		-- Added 2 new parameters for Band Events Association
									,popupMsg VARCHAR(5000)
									,BandTicketURL VARCHAR(5000)
									)
				
				IF (@IsRegionApp = 0)
				BEGIN				
                INSERT INTO #Temp1(Bandid,EventID 
									,EventName
									,BandName
									,ShortDescription 
									,LongDescription                                                    
									,HcHubCitiID 
									,EventAddress
									,EventLatitude
									,EventLongitude
									,ImagePath 
									,MoreInformationURL                                               
									,StartDate
									,EndDate 
									,StartTime
									,EndTime                                
									,MenuItemExist
									,EventCategoryID
									,EventCategoryName 
									,Distance                                                                        
									,DistanceActual									  
									,OnGoingEvent 
									,RetailLocationID
									,Latitude 
									,Longitude
									,SignatureEvent
									,recurringDays
									,listingImgPath
									,isBandFlag
									,popupMsg
									,BandTicketURL)
						SELECT bandid,EventID 
									,EventName
									,BandName
									,ShortDescription 
									,LongDescription                                                    
									,HcHubCitiID 
									,EventAddress
									,EventLatitude
									,EventLongitude
									,ImagePath 
									,MoreInformationURL                                               
									,StartDate
									,EndDate 
									,StartTime 
									,EndTime                                
									,MenuItemExist
									,HcEventCategoryID
									,HcEventCategoryName 
									,Distance                                                                        
									,DistanceActual									  
									,OnGoingEvent 
									,RetailLocationID
									,Latitude 
									,Longitude
									,SignatureEvent
									,recurringDays
									,listingImgPath
									,isBandFlag
									,popupMsg
									,BandTicketURL
							FROM
							(SELECT DISTINCT  E.bandid, E.HcbandEventID AS EventID
										,E.hcBandEventName  AS EventName
										,E.BandName
										,ShortDescription ShortDescription
										,LongDescription   LongDescription                                                  
										,E.HcHubCitiID  HcHubCitiID
										,isnull(HL.Address,RL.Address1)+','+ isnull(HL.City,RL.City)+','+ isnull(HL.State,RL.State)+','+ isnull(HL.PostalCode,RL.PostalCode) EventAddress
										,isnull(HL.Latitude,RetailLocationLatitude) EventLatitude
										,isnull(HL.Longitude,RetailLocationLongitude) EventLongitude
										,ImagePath=IIF(E.HcHubcitiID IS NULL,(@RetailConfig + CAST(E.BandID AS VARCHAR(100))+'/'+ImagePath),@Config+CAST(E.HCHubcitiID AS VARCHAR)+'/'+ ImagePath) 
										,E.MoreInformationURL                                                  
										,StartDate =CAST(StartDate AS DATE)
										,EndDate =CAST(EndDate AS DATE)
										,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(StartDate AS Time),100),'AM',' AM') ,'PM',' PM')StartTime
										,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(EndDate AS Time),100),'AM',' AM') ,'PM',' PM') EndTime
										,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																			FROM HcMenuItem MI 
																			INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																			WHERE LinkTypeName = 'Events' 
																			AND MI.LinkID = E.HcBandEventID)>0 THEN 1 ELSE 0 END  
										,EC.HcBandEventCategoryID  AS HcEventCategoryID
										,EC.HcBandEventCategoryName AS HcEventCategoryName
										,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214 END, 0))
								        ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
										--  ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
								        --  ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
										,E.OnGoingEvent  OnGoingEvent
										,NULL RetailLocationID 
										,Latitude = ISNULL(HL.Latitude,NULL)
										,Longitude = ISNULL(HL.Longitude,NULL)
										,SignatureEvent = CASE WHEN (EC.HcBandEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END
										,recurringDays
										,listingImgPath=IIF(E.HcHubcitiID IS NULL,(@RetailConfig + CAST(E.BandID AS VARCHAR(100))+'/'+EventListingImagePath),@Config+CAST(E.HCHubcitiID AS VARCHAR)+'/'+ EventListingImagePath) 
							            ,isBandFlag = CASE WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID)  >= 2 THEN 2 
														   WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID) = 1 THEN 1
														   WHEN RE.HcBandEventsAssociationID IS NULL THEN 0
													  END
										,popupMsg = CASE WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID) = 0 THEN 'There is no band associated to this band event'
															   WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID) IS NULL THEN 'There is no band associated to this band event'
															   ELSE NULL
															   END
										,E.BandTicketURL
							FROM #BandEvents  E 
							INNER JOIN HcBandEventsCategoryAssociation EA ON EA.HcBandEventID =E.hcBandEventID AND (E.HcHubCitiID = @HubCitiID OR E.HcHubCitiID IS NULL)
							INNER JOIN HcBandEventsCategory EC ON EC.HcBandEventCategoryID =EA.HcBandEventCategoryID
							LEFT JOIN HcBandEventLocation HL ON HL.HcBandEventID =E.HcBandEventID 
							LEFT JOIN HcBandEventAppsite A ON E.hcBandEventID = A.HcBandEventID AND A.HcHubCitiID = @HubCitiID AND A.HcBandEventID IS NULL
							LEFT JOIN HcBandAppSite HA ON A.HcBandAppSiteID = HA.HcBandAppSiteID AND HA.HcHubCitiID = @HubCitiID 
							LEFT JOIN Geoposition G ON (G.Postalcode=HL.Postalcode)                     
							LEFT JOIN HcBandEventsAssociation RE ON E.HcBandEventID = RE.HcBandEventID 							 
							LEFT JOIN HcBandRetailerEventsAssociation L ON L.HcBandEventID= E.HcBandEventID  
							LEFT JOIN Retailer R On R.RetailID = L.retailid
							LEFT JOIN RetailLocation RL on RL.RetailID=R.RetailID AND RL.RetailID=L.RetailID AND RL.RetailLocationID=L.RetailLocationID
							WHERE (E.HcHubCitiID = @HubcitiID OR E.HcHubCitiID IS NULL)
							AND ((ISNULL(@CategoryID, '0') <> '0' 
								AND EA.HcBandEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))
							GROUP BY E.hcbandEventID 
									  , hcbandEventName
									  , E.BandID
									  , BandName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  --, isnull(HL.Address,'')+','+ isnull(HL.city,'')+','+ isnull(HL.State,'')+','+ isnull(HL.PostalCode,'')
									  , isnull(HL.Address,RL.Address1)+','+ isnull(HL.City,RL.City)+','+ isnull(HL.State,RL.State)+','+ isnull(HL.PostalCode,RL.PostalCode) 
									  , HL.Latitude
									  , HL.Longitude   
									  , ImagePath
									  , E.MoreInformationURL
									  , EventListingImagePath
									  , StartDate
									  , EndDate
									  , EC.HcBandEventCategoryID
									  , EC.HcBandEventCategoryName
									  , OnGoingEvent
									  , E.BandID
									  , RE.HcBandEventID
									  , RetailLocationLatitude
									  , RetailLocationLongitude
									  , recurringDays
									  , RE.HcBandEventsAssociationID
									  , E.BandTicketURL
									  ) E
				 END
				 ELSE IF (@IsRegionApp = 1)					  	
				 BEGIN
						SELECT DISTINCT  E.bandid
										, E.HcbandEventID AS EventID
										,E.hcBandEventName  AS EventName
										,E.BandName
										,ShortDescription ShortDescription
										,LongDescription   LongDescription                                                  
										,E.HcHubCitiID  HcHubCitiID
										--,isnull(HL.Address,RL.Address1)+','+ isnull(HL.City,RL.City)+','+ isnull(HL.State,RL.State)+','+ isnull(HL.PostalCode,RL.PostalCode) EventAddress
										,Address = isnull(HL.Address,RL.Address1)
										,City = isnull(HL.City,RL.City)
										,State = isnull(HL.State,RL.State) 
										,PostalCode = isnull(HL.PostalCode,RL.PostalCode) 
										,isnull(HL.Latitude,RetailLocationLatitude) EventLatitude
										,isnull(HL.Longitude,RetailLocationLongitude) EventLongitude
										,ImagePath=IIF(E.HcHubcitiID IS NULL,(@RetailConfig + CAST(E.BandID AS VARCHAR(100))+'/'+ImagePath),@Config+CAST(E.HCHubcitiID AS VARCHAR)+'/'+ ImagePath)
										,E.MoreInformationURL                                                  
										,StartDate =CAST(StartDate AS DATE)
										,EndDate =CAST(EndDate AS DATE)
										,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(StartDate AS Time),100),'AM',' AM') ,'PM',' PM')StartTime
										,REPLACE(REPLACE(CONVERT(VARCHAR(15),CAST(EndDate AS Time),100),'AM',' AM') ,'PM',' PM') EndTime
										,MenuItemExist =  CASE WHEN(SELECT COUNT(HcMenuItemID)
																			FROM HcMenuItem MI 
																			INNER JOIN HcLinkType LT ON MI.HcLinkTypeID = LT.HcLinkTypeID                                                                        
																			WHERE LinkTypeName = 'Events' 
																			AND MI.LinkID = E.HcBandEventID)>0 THEN 1 ELSE 0 END  
										,EC.HcBandEventCategoryID  AS HcEventCategoryID
										,EC.HcBandEventCategoryName AS HcEventCategoryName
										,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214 END, 0))
								        ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(isnull(HL.Latitude,RetailLocationLatitude)) IS NULL THEN MIN(G.Latitude) ELSE MIN(isnull(HL.Latitude,RetailLocationLatitude)) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(isnull(HL.Longitude,RetailLocationLongitude)) IS NULL THEN MIN(G.Longitude) ELSE MIN(isnull(HL.Longitude,RetailLocationLongitude)) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
										--  ,Distance =(ISNULL(CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214 END, 0))
								        --  ,DistanceActual = CASE WHEN @UserLatitude IS NOT NULL AND @UserLongitude IS NOT NULL THEN ROUND((ACOS((SIN(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(CASE WHEN MIN(HL.Latitude) IS NULL THEN MIN(G.Latitude) ELSE MIN(HL.Latitude) END / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (CASE WHEN MIN(HL.Longitude) IS NULL THEN MIN(G.Longitude) ELSE MIN(HL.Longitude) END/ 57.2958))))*6371) * 0.6214, 1, 1) END  
										,E.OnGoingEvent  OnGoingEvent
										,NULL RetailLocationID 
										,Latitude = ISNULL(HL.Latitude,NULL)
										,Longitude = ISNULL(HL.Longitude,NULL)
										,SignatureEvent = CASE WHEN (EC.HcBandEventCategoryName = 'Signature Events') THEN 1 ELSE 0 END
										,recurringDays
										,listingImgPath=IIF(E.HcHubcitiID IS NULL,(@RetailConfig + CAST(E.BandID AS VARCHAR(100))+'/'+EventListingImagePath),@Config+CAST(E.HCHubcitiID AS VARCHAR)+'/'+ EventListingImagePath) 
										,isBandFlag = CASE WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID)  >= 2 THEN 2 
														   WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID) = 1 THEN 1
														   WHEN RE.HcBandEventsAssociationID IS NULL THEN 0
													  END
										,popupMsg = CASE WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID) = 0 THEN 'There is no band associated to this band event'
															   WHEN (SELECT Count(HcBandEventsAssociationID) FROM HcBandEventsAssociation WHERE HcBandEventID= E.HcBandEventID) IS NULL THEN 'There is no band associated to this band event'
															   ELSE NULL
															   END
										,E.BandTicketURL
							INTO #Temp2
							FROM #BandEvents  E 
							INNER JOIN HcBandEventsCategoryAssociation EA ON EA.HcBandEventID =E.hcBandEventID --AND (E.HcHubCitiID = @HubCitiID OR E.HcHubCitiID IS NULL)
							INNER JOIN HcBandEventsCategory EC ON EC.HcBandEventCategoryID =EA.HcBandEventCategoryID
							INNER JOIN #RHubcitiList RH ON (E.HcHubCitiID = RH.HcHubcitiID OR E.HcHubCitiID IS NULL)
							LEFT JOIN HcBandEventLocation HL ON HL.HcBandEventID =E.HcBandEventID 
							LEFT JOIN HcBandEventAppsite A ON E.hcBandEventID = A.HcBandEventID AND A.HcHubCitiID = @HubCitiID AND A.HcBandEventID IS NULL
							LEFT JOIN HcBandAppSite HA ON A.HcBandAppSiteID = HA.HcBandAppSiteID AND HA.HcHubCitiID = @HubCitiID 
							LEFT JOIN Geoposition G ON (G.Postalcode=HL.Postalcode)                     
							LEFT JOIN HcBandEventsAssociation RE ON E.HcBandEventID = RE.HcBandEventID 							 
							LEFT JOIN HcBandRetailerEventsAssociation L ON L.HcBandEventID= E.HcBandEventID  
							LEFT JOIN Retailer R On R.RetailID = L.retailid
							LEFT JOIN RetailLocation RL on RL.RetailID=R.RetailID AND RL.RetailID=L.RetailID AND RL.RetailLocationID=L.RetailLocationID
							WHERE (E.HcHubCitiID = RH.HcHubcitiID OR E.HcHubCitiID IS NULL)
							AND ((ISNULL(@CategoryID, '0') <> '0' 
								AND EA.HcBandEventCategoryID IN (SELECT CatID FROM #CategoryList)) OR (ISNULL(@CategoryID, '0') = '0' AND 1=1))
							GROUP BY E.hcbandEventID 
									  , hcbandEventName
									  , E.BandID
									  , BandName
									  , ShortDescription
									  , LongDescription                                                     
									  , E.HcHubCitiID 
									  , isnull(HL.Address,RL.Address1)
									  , isnull(HL.City,RL.City)
									  , isnull(HL.State,RL.State) 
									  , isnull(HL.PostalCode,RL.PostalCode)
									  , HL.Latitude
									  , HL.Longitude   
									  , ImagePath
									  , E.MoreInformationURL
									  , EventListingImagePath
									  , StartDate
									  , EndDate
									  , EC.HcBandEventCategoryID
									  , EC.HcBandEventCategoryName
									  , OnGoingEvent
									  , E.BandID
									  , RE.HcBandEventID
									  , RetailLocationLatitude
									  , RetailLocationLongitude
									  , recurringDays
									  , RE.HcBandEventsAssociationID
									  , E.BandTicketURL


						
						INSERT INTO #Temp1(Bandid,EventID 
									,EventName
									,BandName
									,ShortDescription 
									,LongDescription                                                    
									,HcHubCitiID 
									,EventAddress
									,EventLatitude
									,EventLongitude
									,ImagePath 
									,MoreInformationURL                                               
									,StartDate
									,EndDate 
									,StartTime
									,EndTime                                
									,MenuItemExist
									,EventCategoryID
									,EventCategoryName 
									,Distance                                                                        
									,DistanceActual									  
									,OnGoingEvent 
									,RetailLocationID
									,Latitude 
									,Longitude
									,SignatureEvent
									,recurringDays
									,listingImgPath
									,isBandFlag
									,popupMsg 
									,BandTicketURL)
							SELECT Bandid
									,EventID 
									,EventName
									,BandName
									,ShortDescription 
									,LongDescription                                                    
									,HcHubCitiID 
									,EventAddress
									,EventLatitude
									,EventLongitude
									,ImagePath 
									,MoreInformationURL                                               
									,StartDate
									,EndDate 
									,StartTime 
									,EndTime                                
									,MenuItemExist
									,HcEventCategoryID
									,HcEventCategoryName 
									,Distance                                                                        
									,DistanceActual									  
									,OnGoingEvent 
									,RetailLocationID
									,Latitude 
									,Longitude
									,SignatureEvent
									,recurringDays
									,listingImgPath
									,isBandFlag
									,popupMsg
									,BandTicketURL
							FROM
							(SELECT bandid
									,EventID
									,EventName
									,BandName
									,ShortDescription
									,LongDescription
									,T.HcHubCitiID
									,EventAddress = Address + City + State + PostalCode
									,EventLatitude
									,EventLongitude
									,ImagePath
									,MoreInformationURL
									,StartDate
									,EndDate
									,StartTime
									,EndTime
									,MenuItemExist
									,HcEventCategoryID
									,HcEventCategoryName
									,Distance
									,DistanceActual
									,OnGoingEvent
									,RetailLocationID
									,Latitude
									,Longitude
									,SignatureEvent
									,recurringDays
									,listingImgPath
									,isBandFlag
									,popupMsg
									,BandTicketURL
							 FROM #Temp2 T
							 INNER JOIN #PreferredCities C ON T.City = C.CityName 
							 LEFT JOIN #CitiIDs CC ON CC.CityID =C.HcCityID  
							 WHERE (@CityIDs IS NULL AND 1=1) OR (@CityIDs IS NOT NULL AND C.HcCityID =CC.CityID)
							 ) E
				 END

				 SELECT * FROM #Temp1

                 CREATE TABLE #Events(RowNum INT IDENTITY(1, 1)
                                    ,HcEventID Int
                                    ,HcEventName  Varchar(max)
									,BandID INT
									,BandName VARCHAR(max)
                                    ,ShortDescription Varchar(max)
                                    ,LongDescription  Varchar(max)                                                
                                    ,HcHubCitiID Int
									,EventAddress Varchar(max)
									,EventLatitude Float
									,EventLongitude Float
                                    ,ImagePath Varchar(max) 
									,MoreInformationURL Varchar(max)                                             
                                    ,StartDate date
                                    ,EndDate date
                                    ,StartTime VARCHAR(15)
                                    ,EndTime VARCHAR(15)                                        
                                    ,MenuItemExist bit
                                    ,HcEventCategoryID INT
                                    ,HcEventCategoryName Varchar(2000)
                                    ,Distance Float
                                    ,DistanceActual float
                                    ,OnGoingEvent bit
									,SignatureEvent bit
									,recurringDays Varchar(500)
									,listingImgPath VARCHAR(max)
									,isBandFlag INT
									,popupMsg VARCHAR(1000)
									,BandTicketURL VARCHAR(1000))

					IF (@BandID IS NOT NULL AND  @BandEventTypeID IS NULL)			
					BEGIN
					
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName  
										,BandID
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath        
										,MoreInformationURL                                         
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent
										,recurringDays
										,listingImgPath
										,isBandFlag
										,popupMsg
										,BandTicketURL)
								SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,BandID
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath     
										   ,MoreInformationURL                                                
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent
										   ,recurringDays
										   ,listingImgPath
										   ,isBandFlag
										   ,popupMsg
										   ,BandTicketURL
								 FROM #Temp1        
								 WHERE  bandid = @BandID 
								 AND Distance <= @Radius 
					END 

					

					/*if (@BandID is  null and @BandEventTypeID is not null)         

					BEGIN

					if @BandEventTypeID=2---Near by EVENT
					
					  begin

                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath
										,isBandFlag)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
										   ,isBandFlag
								 FROM #Temp1  where isnull(Distance,DistanceActual) <=5
								 --isnull(Distance,DistanceActual) <= ISNULL(@Radius, 5) 
								 AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>=CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101))
								 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,isBandFlag
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									end

									if @BandEventTypeID=1---ON GOING  EVENT
					
					  begin

					 
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath
										,isBandFlag)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
										   ,isBandFlag
								 FROM #Temp1 
								 WHERE OnGoingEvent=1  AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>=CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101))
								 --CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)) between
								  -- CONVERT(DATETIME,CONVERT(VARCHAR,StartDate,101))
						-- AND  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,StartDate),101))
						-- AND isnull(Distance,DistanceActual) <= ISNULL(@Radius, 5) 
							 GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,listingImgPath
										 ,isBandFlag
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									END


									if @BandEventTypeID=3---Up coming EVENT
					
					  begin

			

                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName 
										,BandID 
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath 
										,MoreInformationURL                                                
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath
										,isBandFlag)
								 SELECT  EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                  
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
										   ,isBandFlag
								 FROM #Temp1
								 WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>=CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101))
								 -- WHERE ( CONVERT(DATETIME,CONVERT(VARCHAR,StartDate,101))>CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)) 
								-- and  CONVERT(DATETIME,CONVERT(VARCHAR,isnull(EndDate,GETDATE()+1),101))>CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),101)))
								 -- AND isnull(Distance,DistanceActual) <= ISNULL(@Radius, 5) 
							GROUP BY EventID 
										 ,EventName  
										 ,Bandid
										 ,BandName
										 ,ShortDescription
										 ,LongDescription                                                   
										 ,HcHubCitiID 
										 ,EventAddress
										 ,EventLatitude
										 ,EventLongitude
										 ,ImagePath
										 ,MoreInformationURL                                                   
										 ,StartDate 
										 ,EndDate 
										 ,StartTime
										 ,EndTime                        
										 ,MenuItemExist     
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
										 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
										 ,EventCategoryID
										 ,EventCategoryName
										 ,Distance
										 ,DistanceActual
										 ,OnGoingEvent
										 ,SignatureEvent,recurringDays
										 ,listingImgPath
								 ORDER BY SignatureEvent DESC, OnGoingEvent DESC, 
									CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'atoz' THEN EventName
										WHEN @SortOrder = 'ASC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
										WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant) END ASC,Distance
									--CASE WHEN @SortOrder = 'DESC' AND @SortColumn = 'HcEventName' THEN HcEventName
									--	WHEN @SortOrder = 'DESC' AND @SortColumn = 'Distance' THEN CAST(DistanceActual AS sql_variant)
									--	WHEN @SortOrder = 'DESC' AND @SortColumn= 'StartDate' THEN CAST(StartDate AS sql_variant) END DESC
									enD

									enD*/

					--IF (@BandID IS NULL AND  @BandEventTypeID IS NULL)	--AND ISNULL(@SearchParameter,'')= '')   
					--BEGIN
     --               INSERT INTO #Events(HcEventID  
     --                                   ,HcEventName 
					--					,BandID 
					--					,BandName
     --                                   ,ShortDescription 
     --                                   ,LongDescription                                                 
     --                                   ,HcHubCitiID 
					--					,EventAddress
					--					,EventLatitude
					--					,EventLongitude
     --                                   ,ImagePath 
					--					,MoreInformationURL                                                   
     --                                   ,StartDate 
     --                                   ,EndDate 
     --                                   ,StartTime 
     --                                   ,EndTime                                       
     --                                   ,MenuItemExist 
     --                                   ,HcEventCategoryID 
     --                                   ,HcEventCategoryName
     --                                   ,Distance
     --                                   ,DistanceActual
     --                                   ,OnGoingEvent
					--					,SignatureEvent,recurringDays
					--					,listingImgPath
										--,isBandFlag)
					--			 SELECT  EventID HcEventID 
     --                                      ,EventName HcEventName
					--					   ,Bandid
					--					   ,BandName
     --                                      ,ShortDescription
     --                                      ,LongDescription                                                   
     --                                      ,HcHubCitiID 
					--					   ,EventAddress
					--					   ,EventLatitude
					--					   ,EventLongitude
     --                                      ,ImagePath
					--					   ,MoreInformationURL                                                   
     --                                      ,StartDate 
					--					   ,EndDate 
					--					   ,StartTime
					--					   ,EndTime                   
     --                                      ,MenuItemExist
     --                                      --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
     --                                      --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
					--					   ,EventCategoryID HcEventCategoryID
					--					   ,EventCategoryName HcEventCategoryName
     --                                      ,Distance   
     --                                      ,DistanceActual
     --                                      ,OnGoingEvent
					--					   ,SignatureEvent,recurringDays
					--					   ,listingImgPath
					--                    ,isBandFlag
					--			 FROM #Temp1        
					--			 WHERE  Distance <= ISNULL(@Radius, 50) 
					--			 GROUP BY EventID 
					--					 ,EventName  
					--					 ,Bandid
					--					 ,BandName
					--					 ,ShortDescription
					--					 ,LongDescription                                                   
					--					 ,HcHubCitiID 
					--					 ,EventAddress
					--					 ,EventLatitude
					--					 ,EventLongitude
					--					 ,ImagePath
					--					 ,MoreInformationURL                                               
					--					 ,StartDate 
					--					 ,EndDate 
					--					 ,StartTime
					--					 ,EndTime                        
					--					 ,MenuItemExist     
					--					 --,CASE WHEN @GroupColumn <> 'Type'  THEN 0 ELSE HcEventCategoryID END
					--					 --,CASE WHEN @GroupColumn <> 'Type'  THEN '0' ELSE HcEventCategoryName END
					--					 ,EventCategoryID
					--					 ,EventCategoryName
					--					 ,Distance
					--					 ,DistanceActual
					--					 ,OnGoingEvent
					--					 ,SignatureEvent,recurringDays
					--					 ,listingImgPath
					--END

					IF (@BandID IS NULL AND  @BandEventTypeID IS NULL)	-- AND ISNULL(@SearchParameter,'') <> '')   -- Search Button
					BEGIN
				
                    INSERT INTO #Events(HcEventID  
                                        ,HcEventName  
										,BandID
										,BandName
                                        ,ShortDescription 
                                        ,LongDescription                                                 
                                        ,HcHubCitiID 
										,EventAddress
										,EventLatitude
										,EventLongitude
                                        ,ImagePath         
										,MoreInformationURL                                          
                                        ,StartDate 
                                        ,EndDate 
                                        ,StartTime 
                                        ,EndTime                                       
                                        ,MenuItemExist 
                                        ,HcEventCategoryID 
                                        ,HcEventCategoryName
                                        ,Distance
                                        ,DistanceActual
                                        ,OnGoingEvent
										,SignatureEvent,recurringDays
										,listingImgPath
										,isBandFlag
										,popupMsg
										,BandTicketURL)
								 SELECT   EventID HcEventID 
                                           ,EventName HcEventName
										   ,Bandid
										   ,BandName
                                           ,ShortDescription
                                           ,LongDescription                                                   
                                           ,HcHubCitiID 
										   ,EventAddress
										   ,EventLatitude
										   ,EventLongitude
                                           ,ImagePath
										   ,MoreInformationURL                                                    
                                           ,StartDate 
										   ,EndDate 
										   ,StartTime
										   ,EndTime                   
                                           ,MenuItemExist
                                           --,HcEventCategoryID = CASE WHEN @GroupColumn <> 'Type' THEN 0 ELSE HcEventCategoryID END
                                           --,HcEventCategoryName = CASE WHEN @GroupColumn <> 'Type'THEN '0' ELSE HcEventCategoryName END
										   ,EventCategoryID HcEventCategoryID
										   ,EventCategoryName HcEventCategoryName
                                           ,Distance   
                                           ,DistanceActual
                                           ,OnGoingEvent
										   ,SignatureEvent,recurringDays
										   ,listingImgPath
										   ,isBandFlag
										   ,popupMsg
										   ,BandTicketURL
								 FROM #Temp1  
								 WHERE  Distance <= @Radius
					END
					            

					SELECT DISTINCT RowNum =1
                              ,HcEventID  
                              ,HcEventName  
							  ,BandID
							  ,BandName
                              ,ShortDescription 
                              ,LongDescription                                                  
                              ,HcHubCitiID 
							 --,CASE WHEN L.HcBandRetailerEventsAssociationID IS  NULL THEN EventAddress ELSE  isnull(RL.Address1,'')+','+ isnull(RL.city,'')+','+ isnull(RL.State,'')+','+ isnull(RL.PostalCode,'') END AS EventAddress
							  ,EventAddress
							  ,ISNULL(EventLatitude,RetailLocationLatitude) AS  EventLatitude
							  ,ISNULL(EventLongitude,RetailLocationLongitude) AS EventLongitude
                              ,ImagePath 
							  ,MoreInformationURL                                                 
                              ,StartDate 
                              ,EndDate 
                              ,E.StartTime 
                              ,E.EndTime                                       
                              ,MenuItemExist 
                              ,HcEventCategoryID 
                              ,HcEventCategoryName
                              ,Distance
                              ,DistanceActual
                              ,OnGoingEvent 						 
							  ,SignatureEvent
							  ,AppSiteFlag = CASE WHEN L.HcBandRetailerEventsAssociationID IS NOT NULL THEN 1 ELSE 0 END
							  ,L.RetailID AS retailId
							  ,L.RetailLocationID retaillocationID
							  ,Retailname
							  ,E.recurringDays
							  ,E.listingImgPath
							  ,isBandFlag
							  ,popupMsg
							  ,BandTicketURL
                     INTO #Events1
                     FROM #Events E 
					 LEFT JOIN HcbandRetailerEventsAssociation L  ON L.hcBandEventID= E.HcEventID 
					 LEFT JOIN Retailer R On R.RetailID = L.retailid
					 LEFT JOIN RetailLocation RL on RL.RetailID=R.RetailID AND RL.RetailID=L.RetailID AND RL.RetailLocationID=L.RetailLocationID
					
                     SELECT   rowNum 
							,E.HcEventID eventId
							,E.HcEventName eventName
							,E.BandID 
							,E.BandName 
							,E.ShortDescription shortDes
							,E.LongDescription longDes                                                      
                            ,E.HcHubCitiID hubCitiId
							,E.EventAddress Address
							,E.EventLatitude Latitude
							,E.EventLongitude Longitude
                            ,E.ImagePath imgPath
							,E.MoreInformationURL moreInfoURL                                             
                            ,E.StartDate 
                            ,E.EndDate 
                            ,E.StartTime
                            ,E.EndTime                        
                            ,E.HcEventCategoryID eventCatId
                            ,E.HcEventCategoryName eventCatName      
                            ,ISNULL(DistanceActual,Distance)  Distance  
							,E.AppSiteFlag isAppSiteFlag 
							,E.RetailID retailId 
							,E.RetaillocationID retailLocationId 
							,E.Retailname  retailName  
							,evtLocTitle = CASE WHEN AppSiteFlag = 0 THEN EL.EventLocationKeyword ELSE E.retailName END 
							,recurringDays
							,listingImgPath
							,isBandFlag
							,popupMsg
							,BandTicketURL
					INTO #Final 
					FROM #Events1 E
					LEFT JOIN HcBandEventLocation EL ON EL.Hcbandeventid = E.HcEventID
					WHERE ((@SearchParameter IS NOT NULL AND (HcEventName LIKE '%'+@SearchParameter+'%' OR EventAddress LIKE '%'+@SearchParameter+'%'
							OR retailName LIKE '%'+@SearchParameter+'%' OR EL.EventLocationKeyword LIKE '%'+@SearchParameter+'%'))
							OR @SearchParameter IS NULL)
					AND (( @isPlayingToday = 1 AND (@EventDate IS NULL OR (@EventDate BETWEEN CAST(E.StartDate AS DATE) AND ISNULL(CAST(E.EndDate AS DATE),GETDATE()+1))) )
							 OR (@isPlayingToday = 0 AND ( @EventDate IS NULL OR CAST(E.StartDate AS DATE)=  @EventDate ) ))


					CREATE TABLE #Res(rowNum INT 
                                    ,eventId Int
                                    ,eventName  Varchar(8000)
									,BandIDs VARCHAR(8000)
									,BandNames VARCHAR(8000)
                                    ,shortDes Varchar(max)
                                    ,longDes  Varchar(max)                                                
                                    ,hubCitiId Int
									,Address Varchar(8000)
									,Latitude Float
									,Longitude Float
                                    ,imgPath Varchar(8000) 
									,moreInfoURL Varchar(8000)                                             
                                    ,StartDate date
                                    ,EndDate date
                                    ,StartTime VARCHAR(15)
                                    ,EndTime VARCHAR(15) 
                                    ,eventCatId INT
                                    ,eventCatName Varchar(8000)
                                    ,Distance Float
									,isAppSiteFlag bit
									,retailId int
									,retailLocationId int
									,retailName Varchar(8000)
									,evtLocTitle Varchar(8000)
									,recurringDays Varchar(8000)
									,listingImgPath VARCHAR(8000)
									,isBandFlag INT
									,popupMsg VARCHAR(1000)
									,BandTicketURL VARCHAR(8000)
									)

							INSERT INTO #Res
							SELECT DISTINCT
									 rowNum 
                                    ,eventId 
                                    ,eventName 
									,BandID = LTRIM(REPLACE(STUFF(
														(SELECT ', ' + CAST(BA.BandID AS VARCHAR(1000))
														FROM HcBandEventsAssociation BA 
														WHERE E.eventId =BA.HcBandEventID
														FOR XML PATH('')),1,1,''),'&amp;','&'))
									,BandNames =LTRIM(REPLACE(STUFF((SELECT ', ' + CAST(B.BandName AS VARCHAR(1000))
														FROM HcBandEventsAssociation BA 
														INNER JOIN Band B ON BA.BandID = B.BandID
														WHERE E.eventId =BA.HcBandEventID
														FOR XML PATH('')),1,1,''),'&amp;','&'))
									
                                    ,shortDes 
                                    ,longDes                                         
                                    ,hubCitiId 
									,Address 
									,Latitude 
									,Longitude 
                                    ,imgPath 
									,moreInfoURL                                
                                    ,StartDate 
                                    ,EndDate 
                                    ,StartTime 
                                    ,EndTime 
                                    ,eventCatId 
                                    ,eventCatName 
                                    ,Distance 
									,isAppSiteFlag 
									,retailId 
									,retailLocationId 
									,retailName
									,evtLocTitle 
									,recurringDays 
									,listingImgPath 
									,isBandFlag 
									,popupMsg 
									,BandTicketURL
								FROM #Final E
				
					 SELECT  rowNum = Row_Number() over(ORDER BY 
										CASE WHEN @SortOrder = 'ASC' AND @SortColumn = 'Name' THEN EventName
											 WHEN @SortOrder = 'ASC' AND @SortColumn = 'Mileage' THEN CAST(Distance AS INT)
											 WHEN @SortOrder = 'ASC' AND @SortColumn= 'Date' THEN CAST(StartDate AS sql_variant)
											 WHEN @SortOrder = 'ASC' AND @SortColumn = 'Band' THEN BandNames 
											 WHEN @SortOrder = 'ASC' AND @SortColumn = 'Venue' THEN (CASE WHEN evtLocTitle = ' ' THEN 'ZZ' 
																										  WHEN evtLocTitle IS NULL THEN 'ZZZZ'
																										  ELSE evtLocTitle END)
										ELSE CAST(StartDate AS sql_variant)	END ASC,Distance )
                            , eventId
							, eventName
							, BandIDs
							, BandNames BandName
							, shortDes
							, longDes                                                      
							, hubCitiId
							, Address
							, Latitude
							, Longitude
							, imgPath
							, moreInfoURL                                           
							, StartDate = CASE WHEN @isPlayingToday = 1 THEN CAST(GETDATE() AS DATE) ELSE E.StartDate END  
							, E.EndDate 
							, E.StartTime
							, E.EndTime                        
							, eventCatId
							, eventCatName      
							, Distance 
							, isAppSiteFlag
							, retailId 
							, retailLocationId 
							, retailName  
							, evtLocTitle
							, recurringDays
							, listingImgPath
							,isBandFlag bandCntFlag
							,popupMsg popUpMsg
							,BandTicketURL ticketURL
			 INTO #Result
			 FROM #Res E

				 SELECT DISTINCT * FROM #Result
				 WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit
			
						
			 --To capture max row number
			 SELECT @MaxCnt = COUNT(1) FROM #Result

            --This flag is a indicator to enable "More" button in the UI.   
            --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
            SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
				
			--To get list of BottomButtons for this Module
            EXEC [HubCitiApp2_8_3].[usp_HcFunctionalityBottomButtonDisplay] @HubCitiID,'Band Events',@UserID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                

			--Confirmation of Success
			SELECT @Status = 0         
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                     PRINT 'Error occured in Stored Procedure usp_HcBandEventsDisplay.'             
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;







GO
