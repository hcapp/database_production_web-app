USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[perfusp_HcFindOptionsInterestsListDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  [usp_HcFindOptionsInterestsListDisplay]
Purpose                 :  To get the list of filter options for Find Module.
Example                 :  [usp_HcFindOptionsInterestsListDisplay]

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/05/2015       SPAN            1.1
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_3].[perfusp_HcFindOptionsInterestsListDisplay]
(

      --Input variable
         @UserID int  
       , @CategoryName varchar(100)
       , @Latitude Decimal(18,6)
       , @Longitude  Decimal(18,6)
       , @HcHubCitiID Int
       , @HcMenuItemID Int
       , @HcBottomButtonID Int
	   , @SearchKey Varchar(250)

      --Output Variable 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY      
				
					DECLARE @Radius int                  
                    , @Tomorrow DATETIME = GETDATE() + 1
                    , @Yesterday DATETIME = GETDATE() - 1
                    , @PostalCode varchar(10)
                    , @DistanceFromUser FLOAT
                    , @UserLatitude float
                    , @UserLongitude float 
                    , @RegionAppFlag Bit
                    , @GuestLoginFlag BIT=0
					, @BusinessCategoryID Int
					, @UserOutOfRange bit   
				    , @DefaultPostalCode varchar(50)              
				    --DECLARE @MaxCnt int

					DECLARE @UserID1 INT= @UserID,
					@HcHubCitiID1 INT= @HcHubCitiID,
					@HcMenuItemID1 INT=@HcMenuItemID,
					@HcBottomButtonID1 INT = @HcBottomButtonID


					--SearchKey implementation
					DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

					SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
							WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
							WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
							ELSE @SearchKey END)

					SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName
					
					--SET @HcBottomButtonID1 = IIF(@HcBottomButtonID1 IS NOT NULL,0,Null)

                    CREATE TABLE #CityList(CityID Int,CityName Varchar(200))

                    IF (SELECT 1 FROM HcUser WHERE UserName ='guestlogin' AND HcUserID = @UserID1) > 0
                    BEGIN
                    SET @GuestLoginFlag = 1
                    END

					CREATE TABLE #RHubCitiList(HcHubCitiID Int) 

                    IF(SELECT 1 from HcHubCiti H
						INNER JOIN HcAppList AL ON H.HcAppListID = AL.HcAppListID 
						AND H.HcHubCitiID = @HcHubCitiID1 AND AL.HcAppListName = 'RegionApp')>0
                            BEGIN
									SET @RegionAppFlag = 1
										    
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HcHubCitiID1 
									UNION ALL
									SELECT  @HcHubCitiID1 AS HcHubCitiID
									)
                            END
                    ELSE
                            BEGIN
									SET @RegionAppFlag =0
									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HcHubCitiID1                                     
                            END

                     --To Fetch region app associated citylist
						DECLARE @HcCityID varchar(100) = null                   
						INSERT INTO #CityList(CityID,CityName)
                                  
						SELECT DISTINCT C.HcCityID 
										,C.CityName                              
						FROM #RHubcitiList H
						INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HcHubCitiID1 
						INNER JOIN HcCity C ON C.HcCityID =LA.HcCityID 
						LEFT JOIN fn_SplitParam(@HcCityID,',') S ON S.Param =C.HcCityID
						LEFT JOIN HcUsersPreferredCityAssociation UP ON UP.HcUserID=@UserID1  AND UP.HcHubcitiID =H.HchubcitiID                                                      
						WHERE (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NULL) 
						OR (@HcCityID IS NOT NULL AND C.HcCityID =S.Param )
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND C.HcCityID =UP.HcCityID AND UP.HcUserID = @UserID1)                                        
						OR (@HcCityID IS NULL AND UP.HcUsersPreferredCityAssociationID IS NOT NULL AND UP.HcCityID IS NULL AND UP.HcUserID = @UserID1)
						
						UNION

						SELECT 0,'ABC'


                        SELECT @UserLatitude = @Latitude
                            , @UserLongitude = @Longitude

                        IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcUser A
                                    INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
                                    WHERE HcUserID = @UserID1 
                        END
                        --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                        IF (@UserLatitude IS NULL) 
                        BEGIN
                                    SELECT @UserLatitude = Latitude
                                            , @UserLongitude = Longitude
                                    FROM HcHubCiti A
                                    INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                                    WHERE A.HcHubCitiID = @HcHubCitiID1
                        END

                                                      
                        --To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
						EXEC [HubCitiApp8].[usp_HcUserHubCitiRangeCheck] @UserID1, @HcHubCitiID1, @Latitude, @Longitude, @PostalCode, 1,  @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
						SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)
 
						--To fetch all the duplicate retailers.
						SELECT DISTINCT DuplicateRetailerID 
						INTO #DuplicateRet
						FROM Retailer 
						WHERE DuplicateRetailerID IS NOT NULL
                           
                        --Derive the Latitude and Longitude in the absence of the input.
                        IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
                        BEGIN
                                    --If the postal code is passed then derive the co ordinates.
                                    IF @PostalCode IS NOT NULL
                                    BEGIN
                                            SELECT @Latitude = Latitude
                                                    , @Longitude = Longitude
                                            FROM GeoPosition 
                                            WHERE PostalCode = @PostalCode
                                    END          
                                    ELSE
                                    BEGIN
                                            SELECT @Latitude = G.Latitude
                                                    , @Longitude = G.Longitude
                                            FROM GeoPosition G
                                            INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
                                            WHERE U.HcUserID = @UserID1 
                                    END                                                                               
                        END    
                     
                        --Get the user preferred radius.
						SELECT @Radius = LocaleRadius
                        FROM HcUserPreference 
                        WHERE HcUserID = @UserID1
                                         
                        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
															WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))  
                    
						IF (@CategoryName IS NOT NULL)
						BEGIN

						CREATE TABLE #BusinessCategory(BusinessCategoryID int, HcBusinessSubCategoryID int)

						INSERT INTO #BusinessCategory(BusinessCategoryID, HcBusinessSubCategoryID)

						SELECT DISTINCT F.BusinessCategoryID, HcBusinessSubCategoryID = ISNULL(FB.HcBusinessSubCategoryID,FBC.HcBusinessSubCategoryID)                
						FROM  BusinessCategory F                               
						LEFT JOIN HcMenuFindRetailerBusinessCategories FB ON F.BusinessCategoryID=FB.BusinessCategoryID AND FB.HcMenuItemID=@HcMenuItemID1
						LEFT JOIN HcBottomButtonFindRetailerBusinessCategories FBC ON F.BusinessCategoryID=FBC.BusinessCategoryID AND FBC.HcBottomButonID=@HcBottomButtonID1                             
						WHERE F.BusinessCategoryName = @CategoryName AND 
						(  (@HcMenuItemID1 IS NOT NULL AND FB.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID=FB.BusinessCategoryID)
                                                OR
                           (@HcBottomButtonID1 IS NOT NULL AND FBC.BusinessCategoryID IS NOT NULL AND F.BusinessCategoryID = FBC.BusinessCategoryID)
                           --                     OR
                           --(@HcBottomButtonID1 = 0)  
						    )
						END
                                                                    
                        CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                            , RetailID INT 
                                            , RetailName VARCHAR(1000)
                                            , RetailLocationID INT
                                            , Address1 VARCHAR(1000)
                                            , City VARCHAR(1000)    
                                            , State CHAR(2)
                                            , PostalCode VARCHAR(20)
                                            , retLatitude FLOAT
                                            , retLongitude FLOAT
                                            , Distance FLOAT  
                                            , DistanceActual FLOAT                                                   
                                            , SaleFlag int)
						
					IF (@CategoryName IS NOT NULL)
					BEGIN
                        IF EXISTS (SELECT 1 FROM #BusinessCategory WHERE HcBusinessSubCategoryID IS NULL)
                        BEGIN    
				                                                   
								INSERT INTO #Retail(RetailID   
													, RetailName  
													, RetailLocationID  
													, Address1  
													, City  
													, State  
													, PostalCode  
													, retLatitude  
													, retLongitude  
													, Distance   
													, DistanceActual                                               
													, SaleFlag )
                                            SELECT RetailID   
                                                    , RetailName  
                                                    , RetailLocationID  
                                                    , Address1  
                                                    , City  
                                                    , State  
                                                    , PostalCode  
                                                    , retLatitude  
                                                    , retLongitude  
                                                    , Distance 
                                                    , DistanceActual                                                  
                                                    , SaleFlag  
											FROM
                                            (SELECT DISTINCT R.RetailID     
															, R.RetailName
															, RL.RetailLocationID 
															, RL.Address1     
															, RL.City
															, RL.State
															, RL.PostalCode
															, ISNULL(RL.RetailLocationLatitude, G.Latitude)  retLatitude
															, ISNULL(RL.RetailLocationLongitude, G.Longitude) retLongitude
															, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
															, SaleFlag = 0   
											FROM Retailer R 
                                            INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1 AND RBC.BusinessCategoryID = @BusinessCategoryID
                                            --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
                                            INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                                   
                                            INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1
                                            INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
                                            INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID ) OR (@RegionAppFlag =0 AND CL.cityID = 0)
                                            INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
                                            LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                            LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                            LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                            -----------to get retailer that have products on sale                                            
                                            LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
											LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID    
                                            WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL
											AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
                                            ) Retailer
                                            WHERE Distance <= @Radius
											
                                                  
						END
						ELSE
						BEGIN
								INSERT INTO #Retail(RetailID   
													, RetailName  
													, RetailLocationID  
													, Address1  
													, City  
													, State  
													, PostalCode  
													, retLatitude  
													, retLongitude  
													, Distance   
													, DistanceActual                                               
													, SaleFlag  )
                                            SELECT RetailID   
                                                    , RetailName  
                                                    , RetailLocationID  
                                                    , Address1  
                                                    , City  
                                                    , State  
                                                    , PostalCode  
                                                    , retLatitude  
                                                    , retLongitude  
                                                    , Distance 
                                                    , DistanceActual                                                  
                                                    , SaleFlag  
											FROM
                                            (SELECT DISTINCT R.RetailID     
															, R.RetailName
															, RL.RetailLocationID 
															, RL.Address1     
															, RL.City
															, RL.State
															, RL.PostalCode
															, ISNULL(RL.RetailLocationLatitude, G.Latitude)  retLatitude
															, ISNULL(RL.RetailLocationLongitude, G.Longitude) retLongitude
															, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
															--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
															, SaleFlag = 0   
											FROM Retailer R 
											INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1 AND RBC.BusinessCategoryID = @BusinessCategoryID
											--INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
											INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID                               
											INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HcHubCitiID1 
											INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
											INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.cityID = 0)
											INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RH.HchubcitiID  AND Associated =1 
											INNER JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
											INNER JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
											LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
											-----------to get retailer that have products on sale                                            
											LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
											LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID   
											WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL 
											AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
											) Retailer
											WHERE Distance <= @Radius
						END
					END
					ELSE IF (@CategoryName IS NULL)
					BEGIN
						INSERT INTO #Retail(RetailID   
											, RetailName  
											, RetailLocationID  
											, Address1  
											, City  
											, State  
											, PostalCode  
											, retLatitude  
											, retLongitude  
											, Distance   
											, DistanceActual                                               
											, SaleFlag)  
                                    SELECT RetailID   
                                            , RetailName  
                                            , RetailLocationID  
                                            , Address1  
                                            , City  
                                            , State  
                                            , PostalCode  
                                            , retLatitude  
                                            , retLongitude  
                                            , Distance 
                                            , DistanceActual                                                  
                                            , SaleFlag  
									FROM
                                    (SELECT DISTINCT R.RetailID     
													, R.RetailName
													, RL.RetailLocationID 
													, RL.Address1     
													, RL.City
													, RL.State
													, RL.PostalCode
													, ISNULL(RL.RetailLocationLatitude, G.Latitude)  retLatitude
													, ISNULL(RL.RetailLocationLongitude, G.Longitude) retLongitude
													, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
													, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
													, SaleFlag = 0   
									FROM Retailer R 
                                    INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID   AND RetailerActive = 1
                                    INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode  
                                    INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HcHubCitiID =@HcHubCitiID1 
									INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID 
									INNER JOIN #CityList CL ON (@RegionAppFlag =1 AND CL.CityID =HL.HcCityID) OR (@RegionAppFlag =0 AND CL.cityID = 0)
                                    INNER JOIN HcHubCiti HC ON HC.HcHubCitiID=HL.HcHubCitiID AND HC.HcHubCitiID=RH.HchubcitiID  
                                    INNER JOIN HcRetailerAssociation HR ON HR.RetailLocationID = RL.RetailLocationID AND HR.HcHubCitiID = RH.HchubcitiID   AND Associated =1  
                                    LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID 
                                    LEFT JOIN RetailerKeywords RK ON R.RetailID = RK.RetailID   
									WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1
                                    AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (R.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
										OR (@SearchKey IS NULL))
									) Retailer
                                    WHERE Distance <= @Radius
					END	

						-- To identify Retailer that have products on Sale or any type of discount
                        SELECT DISTINCT Retailid , RetailLocationid
                        INTO #RetailItemsonSale1
                        FROM 
							(SELECT DISTINCT R.RetailID, a.RetailLocationID 
							FROM RetailLocationDeal a 
							INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
							INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
							and a.ProductID = c.ProductID
							and GETDATE() between ISNULL(a.SaleStartDate, GETDATE() - 1) and ISNULL(a.SaleEndDate, GETDATE() + 1)
							UNION ALL 

							SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
							FROM Coupon C 
							INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
							INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
							LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
							WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
							GROUP BY C.CouponID
									,NoOfCouponsToIssue
									,CR.RetailID
									,CR.RetailLocationID
							HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
										ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
							UNION ALL 
							
							SELECT DISTINCT R.RetailID, R.RetailLocationID
							FROM ProductHotDeal p
							INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
							INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
							LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
							LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
							WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE() - 1) AND ISNULL(HotDealEndDate, GETDATE() + 1)
							GROUP BY P.ProductHotDealID
									,NoOfHotDealsToIssue
									,R.RetailID
									,R.RetailLocationID
							HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
									ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  
							UNION ALL 

							SELECT q.RetailID, qa.RetailLocationID
							FROM QRRetailerCustomPage q
							INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
							INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
							INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
							where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE() + 1)
							) Discount 

							SELECT DISTINCT RowNum
                                            , r.RetailID    
                                            , RetailName
                                            , rl.RetailLocationID
                                            , r.Address1
                                            , r.City    
                                            , r.State
                                            , r.PostalCode
                                            , retLatitude
                                            , retLongitude
                                            , Distance           
                                            , DistanceActual                                       
                                            , SaleFlag= CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
							INTO #Retailer    
							FROM #Retail R
							INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
							LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
							WHERE RL.Active = 1
							ORDER BY RowNum ASC

						--To display List of Interests
						
						SELECT DISTINCT  F.HcFilterID AS filterId 
										, FilterName AS filterName                   
						FROM #Retailer R
						INNER JOIN HcFilterRetailLocation RF ON R.RetailLocationID = RF.RetailLocationID
						INNER JOIN HcFilter F ON RF.HcFilterID = F.HcFilterID
						WHERE F.HcHubCitiID = @HcHubCitiID1
						ORDER BY FilterName
												
					--Confirmation of success
					SELECT @Status = 0 

			END TRY
            
	BEGIN CATCH
      
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN

		PRINT 'Error occured in Stored Procedure [usp_HcFindOptionsInterestsListDisplay].'                            
		--Execute retrieval of Error info.
		EXEC [HubCitiApp8].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
        SELECT @Status = 1                   
	END;
            
	END CATCH;
	END;
       























GO
