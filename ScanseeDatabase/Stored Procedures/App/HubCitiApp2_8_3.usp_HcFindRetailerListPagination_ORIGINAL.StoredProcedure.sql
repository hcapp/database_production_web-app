USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_3].[usp_HcFindRetailerListPagination_ORIGINAL]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  usp_HcFindRetailerListPagination
Purpose                 :  To get the list of near by retailer for Find Module.
Example                 :  

History
Version       Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          5/17/2012       SPAN            Initial Version
1.0			03/03/2016       Sagar Byali	 Revised Version - Find Performance Changes
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [HubCitiApp2_8_3].[usp_HcFindRetailerListPagination_ORIGINAL]
(

      --Input Parameters 
		 @Postalcode varchar(100)
	   , @UserConfiguredPostalCode Varchar(10)
       , @CategoryName varchar(2000)
       , @Latitude Decimal(18,6)
       , @Longitude  Decimal(18,6)
       , @Radius int
       , @LowerLimit int  
       , @ScreenName varchar(50)
       , @HCHubCitiID Int
       , @HcMenuItemId Int
       , @HcBottomButtonId Int
       , @SearchKey Varchar(100)
       , @BusinessSubCategoryID Varchar(2000) 
       , @SortColumn varchar(50)
       , @SortOrder varchar(10)   
       , @HcCityID Varchar(3000)
       , @FilterID Varchar(1000)
       , @FilterValuesID Varchar(1000) 
	   , @LocalSpecials bit 
	   , @Interests Varchar(1000) 
	  

       --User tracking Inputs
       , @MainMenuID int
       
      --Output Variable
       , @UserOutOfRange bit output  
       , @DefaultPostalCode varchar(50) output             
       , @MaxCnt int  output
       , @NxtPageFlag bit output 
       , @Status bit output  
       , @ErrorNumber int output  
       , @ErrorMessage varchar(1000) output 
)
WITH RECOMPILE
AS
BEGIN

      BEGIN TRY      
	 -- SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

                    SET NOCOUNT ON

					DECLARE  @CategoryName1 VARCHAR(100) = @CategoryName
							,@HcMenuItemId1 INT = @HcMenuItemId 
							,@HCHubCitiID1 INT =  @HCHubCitiID
							,@BusinessSubCategoryID1 VARCHAR(1000) = @BusinessSubCategoryID
							,@HcBottomButtonId1 INT = @HcBottomButtonId
                                      
                    DECLARE	  @CategoryID INT  
							, @RowCount int 
							, @Count INT = 1
							, @MaxCount INT 
							, @SQL VARCHAR(1000)                     
							, @RetSearch VARCHAR(1000)
							, @Config varchar(50)                     
							, @RetailConfig varchar(50)
							, @UpperLimit int   
							, @Tomorrow DATETIME = GETDATE() + 1
							, @Yesterday DATETIME = GETDATE() - 1
							, @RowsPerPage int
							, @NearestPostalCode varchar(50)
							, @DistanceFromUser FLOAT
							, @UserLatitude float
							, @UserLongitude float 
							, @SpecialChars VARCHAR(1000)
							, @ModuleName varchar(100) = 'Find'
							, @RegionAppFlag Bit
							, @GuestLoginFlag BIT=0
							, @BusinessCategoryID Int


					--Save Copy of input PostalCode 
					DECLARE @PostalCodeCopy VARCHAR(10)
					SET @PostalCodeCopy = @Postalcode

					--SearchKey implementation
					DECLARE @Length INT = LEN(LTRIM(RTRIM(@SearchKey)))

					SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
							WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
							WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
							ELSE @SearchKey END)

					SELECT @BusinessCategoryID=BusinessCategoryID 
					FROM BusinessCategory 
					WHERE BusinessCategoryName LIKE @CategoryName1

					DECLARE @BusinessSubCategoryID1s Table (ID int identity(1,1), BusCatIDs int)
					INSERT INTO @BusinessSubCategoryID1s (BusCatIDs)
					SELECT Param
					FROM fn_SplitParam (@BusinessSubCategoryID1,',')

					DECLARE @Interestss Table (ID int identity(1,1), IntIDs int)
					INSERT INTO @Interestss (IntIDs)
					SELECT Param
					FROM fn_SplitParam (@Interests,',')


				--Filter Implementation 
                IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
                BEGIN
                        SELECT Param FilterIDs
                        INTO #Filters
                        FROM fn_SplitParam(@FilterID,',') P
                        LEFT JOIN AdminFilterValueAssociation AF ON AF.AdminFilterID =P.Param
                        WHERE AF.AdminFilterID IS NULL

                        SELECT Param FilterValues
                        INTO #FilterValues
                        FROM fn_SplitParam(@FilterValuesID,',')

                        SELECT DISTINCT RetailID 
                        INTO #FilterRetaillocations
                        FROM(
                        SELECT RF.RetailID                                    
                        FROM RetailerFilterAssociation RF
                        INNER JOIN #FilterValues F ON F.FilterValues =RF.AdminFilterValueID AND BusinessCategoryID =@BusinessCategoryID

                        UNION

                        SELECT RF.RetailID                                    
                        FROM RetailerFilterAssociation RF
                        INNER JOIN #Filters F ON F.FilterIDs  =RF.AdminFilterID AND BusinessCategoryID =@BusinessCategoryID
                        )A

				END

				--To collect Cities passed as input for RegionApp
				SELECT Param CityID
				INTO #CityIDs
				FROM fn_SplitParam(@HcCityID,',')

				--To Create CityList 
				CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
                CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100))                  

                IF(SELECT 1 from HcHubCiti H
                        INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
                        AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp')>0
                       BEGIN

									SET @RegionAppFlag = 1
										    
									INSERT INTO #RHubcitiList(HcHubCitiID)
									(SELECT DISTINCT HcHubCitiID 
									FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
									UNION ALL
									SELECT  @HCHubCitiID1 AS HcHubCitiID
									)

									INSERT INTO #CityList(CityID,CityName,PostalCode)		
									SELECT DISTINCT LA.HcCityID 
													,LA.City	
													,PostalCode				
									FROM #RHubcitiList H
									INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID =H.HchubcitiID AND LA.HcHubCitiID =@HCHubCitiID1   						
									INNER JOIN #CityIDs C ON LA.HcCityID=C.CityID 
                            END
						ELSE
                            BEGIN
									SET @RegionAppFlag =0

									INSERT INTO #RHubcitiList(HchubcitiID)
									SELECT @HCHubCitiID1  
	
									INSERT INTO #CityList(CityID,CityName,PostalCode)
									SELECT DISTINCT HcCityID 
												   ,City
												   ,PostalCode					
									FROM HcLocationAssociation WHERE HcHubCitiID =@HCHubCitiID1                                   
                            END


         

                DECLARE @Globalimage varchar(50)
                SELECT @Globalimage =ScreenContent 
                FROM AppConfiguration 
                WHERE ConfigurationType ='Image Not Found'

                SELECT @UserLatitude = @Latitude
                     , @UserLongitude = @Longitude


				IF (@UserLatitude IS NULL) 
                BEGIN
                        SELECT @UserLatitude = Latitude
                                , @UserLongitude = Longitude
                        FROM GeoPosition 
                        WHERE PostalCode = @PostalCode
                END
                --Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
                IF (@UserLatitude IS NULL) 
                BEGIN
                        SELECT @UserLatitude = Latitude
                                , @UserLongitude = Longitude
                        FROM HcHubCiti A
                        INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
                        WHERE A.HcHubCitiID = @HCHubCitiID1
                END

			--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
                EXEC [HubCitiApp2_8_3].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
                SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

				--Set the Next page flag to 0 initially.
                SET @NxtPageFlag = 0

				--Derive the Latitude and Longitude in the absence of the input.
               IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
               BEGIN
                    SELECT @Latitude = Latitude
                            , @Longitude = Longitude
                    FROM GeoPosition 
                    WHERE PostalCode = ISNULL(@PostalCode,@PostalCodeCopy)                                                                              
                END  
				

                SELECT @Config=ScreenContent
                FROM AppConfiguration 
                WHERE ConfigurationType='App Media Server Configuration'

                SELECT @RetailConfig=ScreenContent
                FROM AppConfiguration 
                WHERE ConfigurationType='Web Retailer Media Server Configuration'

                --To fetch all the duplicate retailers.
                SELECT DISTINCT DuplicateRetailerID 
                INTO #DuplicateRet
                FROM Retailer 
                WHERE DuplicateRetailerID IS NOT NULL

                --To get the row count for pagination.  
                SELECT @UpperLimit = @LowerLimit + ScreenContent 
                     , @RowsPerPage = ScreenContent
                FROM AppConfiguration   
                WHERE ScreenName = @ScreenName 
                AND ConfigurationType = 'Pagination'
                AND Active = 1   
                           
               
           
     					CREATE TABLE #BusinessCategory(BusinessCategoryID int , HcBusinessSubCategoryID int)
						IF (@HcMenuItemId IS NOT NULL)
						BEGIN 

							INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,HcBusinessSubCategoryID               
							FROM BusinessCategory BC
							INNER JOIN HcMenuFindRetailerBusinessCategories F ON BC.BusinessCategoryID = F.BusinessCategoryID
							WHERE BC.BusinessCategoryName = @CategoryName AND HcMenuItemID = @HcMenuItemId
						END
						ELSe IF (@HcBottomButtonID IS NOT NULL)
						BEGIN
			
							INSERT INTO #BusinessCategory(BusinessCategoryID,HcBusinessSubCategoryID) 
							SELECT DISTINCT F.BusinessCategoryID,HcBusinessSubCategoryID               
							FROM BusinessCategory BC
							INNER JOIN HcBottomButtonFindRetailerBusinessCategories F ON BC.BusinessCategoryID = F.BusinessCategoryID 
							WHERE BC.BusinessCategoryName = @CategoryName AND HcBottomButonID = @HcBottomButtonID
						END
        
						SELECT @CategoryID = BusinessCategoryID
						FROM #BusinessCategory                       
                                     
									   select distinct BusinessCategoryID  into #SubCategorytype  from HcBusinessSubCategorytype  WITH(NOLOCK) where BusinessCategoryID=@BusinessCategoryID
	
									 select distinct BusinessCategoryID into #NONSUB from BusinessCategory WITH(NOLOCK)   where BusinessCategoryID=@BusinessCategoryID  
									 except
									 select distinct BusinessCategoryID  from #SubCategorytype WITH(NOLOCK)
									 create  table #RetailerBusinessCategory1(RetailerID int,BusinessCategoryID  int)

									 select * into #HcMenuFindRetailerBusinessCategories 
									  from HcMenuFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
									  AND HcBusinessSubCategoryID IS NOT NULL
									 and HcMenuItemID=@HcMenuItemId

									  select * into #HcMenuFindRetailerBusinessCategories1 
									  from HcMenuFindRetailerBusinessCategories  where  BusinessCategoryID=@BusinessCategoryID
									    AND HcBusinessSubCategoryID IS  NULL
									 and HcMenuItemID=@HcMenuItemId

									 select * into #RetailerBusinessCategory2  from RetailerBusinessCategory where  BusinessCategoryID=@BusinessCategoryID
									 create  index  ix_nn  on #HcMenuFindRetailerBusinessCategories(BusinessCategoryID,
									 HcBusinessSubCategoryID)
									  create clustered index  ix_nn1  on #HcMenuFindRetailerBusinessCategories(HcMenuItemID)

									-- select  count(*)  from #HcMenuFindRetailerBusinessCategories

									 if exists(select  1 from #SubCategorytype)

									 begin
									  
									 if @HcMenuItemId  is not null
									 BEGIN
									 INSERT  INTO #RetailerBusinessCategory1
									 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								
									 INNER JOIN #HcMenuFindRetailerBusinessCategories HCB WITH(NOLOCK)
									  ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								     AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID

									  INSERT  INTO #RetailerBusinessCategory1
									 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									 FROM RetailerBusinessCategory RBC WITH(NOLOCK)
								
									 INNER JOIN #HcMenuFindRetailerBusinessCategories1 HCB WITH(NOLOCK)
									  ON HCB.BusinessCategoryID=RBC.BusinessCategoryID
								  --   AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID
									 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 where HCB.HcMenuItemID=@HcMenuItemId and RBC.BusinessCategoryID=@BusinessCategoryID
									 END

									 else

								BEGIN
								--select 'mm'

								SELECT distinct BusinessCategoryID into #BusinessCategory1 FROM #BusinessCategory
									 INSERT  INTO #RetailerBusinessCategory1
									 	 SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									FROM RetailerBusinessCategory RBC WITH(NOLOCK)
									 INNER JOIN #BusinessCategory1 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 WHERE RBC.BusinessCategoryID=@BusinessCategoryID
									--inner join #SubCategorytype st on   st.BusinessCategoryID= BC.BusinessCategoryID
									-- INNER JOIN HcMenuFindRetailerBusinessCategories HCB  ON HCB.BusinessCategoryID=st.BusinessCategoryID
									-- AND RBC.BusinessCategoryID= HCB.BusinessCategoryID
									-- AND RBC.BusinessSubCategoryID= HCB.HcBusinessSubCategoryID

									END

									end

									else 

									begin
									SELECT distinct BusinessCategoryID into #BusinessCategory2 FROM #BusinessCategory

									  INSERT  INTO #RetailerBusinessCategory1
									  SELECT  RBC.RetailerID,RBC.BusinessCategoryID  
									  FROM RetailerBusinessCategory RBC
									 INNER JOIN #BusinessCategory2 BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID
									 inner join #NONSUB st on   st.BusinessCategoryID= BC.BusinessCategoryID
									 and RBC.BusinessCategoryID=@BusinessCategoryID

									 end

									 			
						

									 SELECT DISTINCT #RetailerBusinessCategory1.* INTO #RetailerBusinessCategory  FROM #RetailerBusinessCategory1                  
                                     

									 
									 CREATE CLUSTERED INDEX IX_BUS1 ON  #BusinessCategory(BusinessCategoryID)
									 ----------------------------

									 --SELECT DISTINCT RBC.RetailerID,RBC.BusinessCategoryID  
									 --INTO #RetailerBusinessCategory 
									 --FROM RetailerBusinessCategory RBC
									 --INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID  
									 
					
									 
									 SELECT  DISTINCT R.RetailID     
														, R.RetailName
														, RL.RetailLocationID 
														, RL.Address1     
														, RL.City
														, RL.State
														, RL.PostalCode,Headquarters,Active
														,RetailLocationLatitude,RetailLocationLongitude,RetailLocationImagePath,WebsiteSourceFlag,RetailerImagePath
														, SaleFlag = 0 ,HL.HchubcitiID into #tempre
											FROM Retailer R 

											INNER JOIN #RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  AND RetailerActive = 1
											 AND RBC.BusinessCategoryID = @BusinessCategoryID
											 INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID 
											 -- INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
											INNER JOIN HcLocationAssociation HL ON RL.PostalCode=HL.PostalCode AND HL.HcHubCitiID=@HCHubCitiID1 
											INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
											INNER JOIN #CityList CL ON  CL.CityID =HL.HcCityID 

											--and RL.HcCityID=HL.HcCityID 
											---------------------------------------------      
											                                    
                CREATE TABLE #Retail(RowNum INT IDENTITY(1, 1)
                                    , RetailID INT 
                                    , RetailName VARCHAR(1000)
                                    , RetailLocationID INT
                                    , Address1 VARCHAR(1000)
                                    , City VARCHAR(1000)    
                                    , State CHAR(2)
                                    , PostalCode VARCHAR(20)
                                    , retLatitude FLOAT
                                    , retLongitude FLOAT
                                    , RetailerImagePath VARCHAR(1000)      
                                    , Distance FLOAT  
                                    , DistanceActual FLOAT                                                   
                                    , SaleFlag BIT)

					IF (@FilterID IS NULL AND @FilterValuesID IS NULL)
					BEGIN
							print 'filter is null'
								--IF (@SearchKey IS NOT NULL AND @SearchKey <> '')
								IF EXISTS (SELECT 1 FROM #BusinessCategory WHERE HcBusinessSubCategoryID IS NULL)
								BEGIN                                                         
                                                                               
									INSERT INTO #Retail(RetailID   
														, RetailName  
														, RetailLocationID  
														, Address1 
														, City  
														, State  
														, PostalCode  
														, retLatitude  
														, retLongitude  
														, RetailerImagePath  
														, Distance   
														, DistanceActual                                               
														, SaleFlag) 
											SELECT RetailID   
														, RetailName  
														, RetailLocationID  
														, Address1   
														, City  
														, State  
														, PostalCode  
														, retLatitude  
														, retLongitude  
														, RetailerImagePath  
														, Distance   
														, DistanceActual                                                
														, SaleFlag  
											FROM
											(SELECT DISTINCT Rl.RetailID     
														, Rl.RetailName
														, RL.RetailLocationID 
														, RL.Address1     
														, RL.City
														, RL.State
														, RL.PostalCode
														, ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
														, ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
														, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
														, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
														, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
														--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
														, SaleFlag = 0  
														  FROM #tempre RL
											INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RL.HchubcitiID  AND Associated =1 
												 INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = RL.RetailID  
									         AND RBC.BusinessCategoryID = @BusinessCategoryID
											LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
											LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
											LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
											LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
											-----------to get retailer that have products on sale                                            
											LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
											LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID                                                 
											WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1 
											AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
											AND (@BusinessSubCategoryID1 IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM @BusinessSubCategoryID1s)) 
											AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM @Interestss))
											) Retailer
											WHERE Distance <= @Radius


								END   
                                                              
								ELSE   
                                 --IF (@SearchKey IS NULL)
                                BEGIN
							
							
									INSERT INTO #Retail(RetailID   
														, RetailName  
														, RetailLocationID  
														, Address1  
														, City  
														, State  
														, PostalCode  
														, retLatitude  
														, retLongitude  
														, RetailerImagePath  
														, Distance   
														, DistanceActual                                               
														, SaleFlag) 
											SELECT RetailID   
														, RetailName  
														, RetailLocationID  
														, Address1   
														, City  
														, State  
														, PostalCode  
														, retLatitude  
														, retLongitude  
														, RetailerImagePath  
														, Distance 
														, DistanceActual                                                  
														, SaleFlag  

										FROM
										(SELECT DISTINCT Rl.RetailID     
														, Rl.RetailName
														, RL.RetailLocationID 
														, RL.Address1     
														, RL.City
														, RL.State
														, RL.PostalCode
														, ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
														, ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
														, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
														, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
														, DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
														--Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
														, SaleFlag = 0  
								FROM #tempre RL
								INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RL.HchubcitiID AND RLC.Associated =1 
								INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = RL.RetailID  
									AND RBC.BusinessCategoryID = @BusinessCategoryID
								LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
								LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
								LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
								LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
								-----------to get retailer that have products on sale                                            
								LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
								LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID              
								WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL AND RL.Active = 1
								AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' 
								OR RK.RetailKeyword = @SearchKey))
									OR (@SearchKey IS NULL))                             
								AND (@BusinessSubCategoryID1 IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM @BusinessSubCategoryID1s)) 
								AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM @Interestss))
								) Retailer
								WHERE Distance <= @Radius
								END 
                                        
					END 
                    ----If Filter Is not null then fetch filter related data
                    ELSE IF (@FilterID IS NOT NULL OR @FilterValuesID IS NOT NULL)
                    BEGIN
							--IF (@SearchKey IS NOT NULL AND @SearchKey <> '')
							IF EXISTS (SELECT 1 FROM #BusinessCategory WHERE HcBusinessSubCategoryID IS NULL)
							BEGIN                                                      
                                                                            
                                 INSERT INTO #Retail(RetailID   
                                                    , RetailName  
                                                    , RetailLocationID  
                                                    , Address1  
                                                    , City  
                                                    , State  
                                                    , PostalCode  
                                                    , retLatitude  
                                                    , retLongitude  
                                                    , RetailerImagePath  
                                                    , Distance   
                                                    , DistanceActual                                               
                                                    , SaleFlag)  
                                        SELECT RetailID   
                                                    , RetailName  
                                                    , RetailLocationID  
                                                    , Address1  
                                                    , City  
                                                    , State  
                                                    , PostalCode  
                                                    , retLatitude  
                                                    , retLongitude  
                                                    , RetailerImagePath  
                                                    , Distance   
                                                    , DistanceActual                                                
                                                    , SaleFlag  
												FROM
											(SELECT DISTINCT RL.RetailID     
                                                            , RL.RetailName
                                                            , RL.RetailLocationID 
                                                            , RL.Address1     
                                                            , RL.City
                                                            , RL.State
                                                            , RL.PostalCode
                                                            , ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
                                                            , ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
															, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                            , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                            , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                            --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                            , SaleFlag = 0  
											FROM #FilterRetaillocations RF                                                           
                                           INNER JOIN  #tempre RL  ON RF.RetailID=RL.RetailID
                                            INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID AND RLC.HcHubCitiID = RL.HchubcitiID  AND Associated =1                                               
											INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = RL.RetailID  
									         AND RBC.BusinessCategoryID = @BusinessCategoryID
                                            LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                            LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                            LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
											LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                            -----------to get retailer that have products on sale                                            
                                            LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
                                            LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID                                                 
                                            WHERE Headquarters = 0 AND D.DuplicateRetailerID IS NULL AND RL.Active = 1 
                                            AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey)) OR (@SearchKey IS NULL))
											AND (@BusinessSubCategoryID1 IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM @BusinessSubCategoryID1s)) 
											AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM @Interestss))
											) Retailer
                                            WHERE Distance <= @Radius

                                         END   
                                                              
                                         ELSE   
                                         --IF (@SearchKey IS NULL)
                                           BEGIN
                                                INSERT INTO #Retail(RetailID   
                                                                    , RetailName  
                                                                    , RetailLocationID  
                                                                    , Address1  
                                                                    , City  
                                                                    , State  
                                                                    , PostalCode  
                                                                    , retLatitude  
                                                                    , retLongitude  
                                                                    , RetailerImagePath  
                                                                    , Distance   
                                                                    , DistanceActual                                               
                                                                    , SaleFlag)  
															SELECT RetailID   
                                                                    , RetailName  
                                                                    , RetailLocationID  
                                                                    , Address1  
                                                                    , City  
                                                                    , State  
                                                                    , PostalCode  
                                                                    , retLatitude  
                                                                    , retLongitude  
                                                                    , RetailerImagePath  
                                                                    , Distance 
                                                                    , DistanceActual                                                  
                                                                    , SaleFlag  

															FROM
															(SELECT DISTINCT RL.RetailID     
                                                            , RL.RetailName
                                                            , RL.RetailLocationID 
                                                            , RL.Address1     
                                                            , RL.City
                                                            , RL.State
                                                            , RL.PostalCode
                                                            , ISNULL(RL.RetailLocationLatitude,G.Latitude)  retLatitude
                                                            , ISNULL(RL.RetailLocationLongitude,G.Longitude) retLongitude
															, RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(RL.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),RL.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
                                                            , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                            , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
                                                            --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
                                                            , SaleFlag = 0  
														FROM #FilterRetaillocations RF                                                           
														INNER JOIN  #tempre RL  ON RF.RetailID=RL.RetailID
														INNER JOIN HcRetailerAssociation RLC ON RLC.RetailLocationID =RL.RetailLocationID 
														AND RLC.HcHubCitiID = RL.HchubcitiID  AND Associated =1 
														INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = RL.RetailID  
									                      AND RBC.BusinessCategoryID = @BusinessCategoryID
                                                       LEFT JOIN HcRetailerSubCategory RSC ON RL.RetailLocationID = RSC.RetailLocationID
                                                       LEFT JOIN HcBusinessSubCategory SB ON SB.HcBusinessSubCategoryID=RSC.HcBusinessSubCategoryID 
                                                       LEFT JOIN HcFilterRetailLocation FRL ON RL.RetailLocationID = FRL.RetailLocationID
													   LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode                                  
                                                       -----------to get retailer that have products on sale                                            
                                                       LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID 
                                                       LEFT JOIN RetailerKeywords RK ON RL.RetailID = RK.RetailID              
                                                       WHERE Headquarters = 0  AND D.DuplicateRetailerID IS NULL  AND RL.Active = 1 
                                                       AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
                                                         OR (@SearchKey IS NULL))                             
                                                       AND (@BusinessSubCategoryID1 IS NULL OR RBC.BusinessSubCategoryID IN (SELECT BusCatIDs FROM @BusinessSubCategoryID1s)) 
													   AND (@Interests IS NULL OR FRL.HcFilterID IN (SELECT IntIDs FROM @Interestss))
														) Retailer
                                                       WHERE Distance <= @Radius
												END 
                                                  
								END 
                                    
                                                                          
                                   ----New Find Implementation

                                   -- To identify Retailer that have products on Sale or any type of discount
                                  SELECT DISTINCT Retailid , RetailLocationid
                                  INTO #RetailItemsonSale1
                                  FROM 
                                  (SELECT DISTINCT R.RetailID, a.RetailLocationID 
                                  FROM RetailLocationDeal a 
                                  INNER JOIN #Retail R ON R.RetailLocationID =A.RetailLocationID                                                            
                                  INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
                                                    and a.ProductID = c.ProductID
                                                    and GETDATE() between ISNULL(a.SaleStartDate, @Yesterday) and ISNULL(a.SaleEndDate, @Tomorrow)
                                  UNION ALL 
                                  SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
                                  FROM Coupon C 
                                  INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
                                  INNER JOIN #Retail R ON R.RetailLocationID =CR.RetailLocationID                                                                                               
                                  LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
                                  WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
                                  GROUP BY C.CouponID
                                                       ,NoOfCouponsToIssue
                                                       ,CR.RetailID
                                                       ,CR.RetailLocationID
                                  HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
                                                       ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)   
                                                                                          
                                  UNION ALL  
                                  SELECT DISTINCT  RR.RetailID, 0 as RetaillocationID  
                                  from Rebate R 
                                  INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
                                  INNER JOIN #Retail RE ON RE.RetailLocationID =RR.RetailLocationID
                                  WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 

                                  UNION ALL  

                                  SELECT DISTINCT R.retailid, a.RetailLocationID 
                                  FROM  LoyaltyDeal a
                                  INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
                                  INNER JOIN #Retail R ON R.RetailLocationID =a.RetailLocationID
                                  INNER JOIN RetailLocationProduct b ON a.RetailLocationID = b.RetailLocationID 
                                               AND b.ProductID = LDP.ProductID 
                                  WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, @Yesterday) AND ISNULL(LoyaltyDealExpireDate, @Tomorrow)

                                  UNION ALL 

                                  SELECT DISTINCT R.RetailID, R.RetailLocationID
                                  FROM ProductHotDeal p
                                  INNER JOIN ProductHotDealRetailLocation pr ON pr.ProductHotDealID = p.ProductHotDealID 
                                  INNER JOIN #Retail R ON R.RetailLocationID =PR.RetailLocationID
                                  LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
                                  LEFT JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
                                  WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, @Yesterday) AND ISNULL(HotDealEndDate, @Tomorrow)
                                  GROUP BY P.ProductHotDealID
                                                       ,NoOfHotDealsToIssue
                                                       ,R.RetailID
                                                       ,R.RetailLocationID
                                  HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
                                                       ELSE ISNULL(COUNT(HcUserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(HcUserHotDealGalleryID),0)  

                                  UNION ALL 

                                  select q.RetailID, qa.RetailLocationID
                                  from QRRetailerCustomPage q
                                  INNER JOIN QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
                                  INNER JOIN #Retail R ON R.RetailLocationID =qa.RetailLocationID
                                  INNER JOIN QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
                                  where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,@Tomorrow)
                                  ) Discount 
                    
                                   ---End Find Imp
                                                  
                        SELECT DISTINCT RowNum rowNumber  
                                    , R.RetailID retailerId    
                                    , RetailName retailerName
                                    , R.RetailLocationID retailLocationID
                                    , Address1 retaileraddress1
                                    , City City
                                    , State State
                                    , PostalCode PostalCode
                                    , retLatitude
                                    , retLongitude    
                                    , RetailerImagePath logoImagePath     
                                    , Distance
                                    , DistanceActual                         
                                    , S.BannerAdImagePath bannerAdImagePath    
                                    , B.RibbonAdImagePath ribbonAdImagePath    
                                    , B.RibbonAdURL ribbonAdURL    
                                    , B.RetailLocationAdvertisementID advertisementID  
                                    , S.SplashAdID splashAdID
                                    , SaleFlag = CASE WHEN SS.RetailLocationID IS NULL THEN 0 ELSE 1 END 
  
                        INTO #Retailer    
                        FROM #Retail R
                        LEFT JOIN #RetailItemsonSale1 SS ON SS.RetailID =R.RetailID AND R.RetailLocationID =SS.RetailLocationID 
                        LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
                                                                                                                ELSE SplashAdImagePath
                                                                                                                END
                                        , SplashAdID  = ASP.AdvertisementSplashID
                                        , R.RetailLocationID 
                                    FROM #Retail R
                                    INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
                                    INNER JOIN RetailLocation RL ON RL.RetailLocationID=RS.RetailLocationID
								    INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ISNULL(ASP.EndDate, GETDATE() + 1)
                                  ) S ON R.RetailLocationID = S.RetailLocationID
                        LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
                                                                                                                   ELSE AB.BannerAdImagePath
                                                                                                                   END 
                                        , RibbonAdURL = AB.BannerAdURL
                                        , RetailLocationAdvertisementID = AB.AdvertisementBannerID
                                        , R.RetailLocationID 
                                    FROM #Retail R
                                    INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
                                    INNER JOIN RetailLocation RL ON RL.RetailLocationID=RB.RetailLocationID
									INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
                                    WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, GETDATE() + 1)
									) B ON R.RetailLocationID = B.RetailLocationID
                        
						SELECT		  rowNumber =  ROW_NUMBER() OVER (ORDER BY CASE WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND (@SortColumn = 'Distance' OR @SortColumn = 'City') THEN CAST(DistanceActual AS SQL_VARIANT)
																		WHEN (@SortOrder = 'ASC' OR @SortOrder IS NULL) AND @SortColumn = 'atoz' THEN CAST(retailerName AS SQL_VARIANT)
																		WHEN @SortColumn IS NULL AND (@SortOrder ='ASC' OR @SortOrder IS NULL) THEN CAST(DistanceActual AS SQL_VARIANT)                                                       
																		END ASC ,Distance)  
                                    , retailerId    
                                    , retailerName
                                    , retailLocationID
                                    , retaileraddress1
                                    , City
                                    , State
                                    , PostalCode
                                    , retLatitude
                                    , retLongitude    
                                    , logoImagePath     
                                    , Distance
                                    , DistanceActual                         
                                    ,  bannerAdImagePath    
                                    , ribbonAdImagePath    
                                    , ribbonAdURL    
                                    , advertisementID  
                                    , splashAdID
                                    , SaleFlag 
                        INTO #Retailer1 
						FROM #Retailer
						WHERE (ISNULL(@LocalSpecials,0) = 0 OR (@LocalSpecials = 1 AND SaleFlag = 1))

					
					--To capture max row number.  
                    SELECT @MaxCnt = count(1) FROM #Retailer1
                                  
                    --this flag is a indicator to enable "More" button in the UI.   
                    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
                    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
										               
						SELECT DISTINCT rowNumber  
                                    , retailerId    
                                    , retailerName
                                    , retailLocationID
                                    , retaileraddress1
                                    , City
                                    , State
                                    , PostalCode
                                    , retLatitude
                                    , retLongitude    
                                    , logoImagePath     
                                    , Distance
                                    , DistanceActual                         
                                    ,  bannerAdImagePath    
                                    , ribbonAdImagePath    
                                    , ribbonAdURL    
                                    , advertisementID  
                                    , splashAdID
                                    , SaleFlag   
                        INTO #RetailerList 
						FROM #Retailer1
						WHERE rowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit
                        ORDER BY rowNumber   
                                                
                                         
                           --User Tracking section.

                           ---Region App

                           INSERT INTO HubCitiReportingDatabase..CityList(MainMenuID
                                                                        ,CityID
                                                                        ,DateCreated)
							SELECT DISTINCT @MainMenuID 
                                 ,CityID 
                                    ,GETDATE()
                           FROM #CityList 

                                  
                           --Table to track the Retailer List.
                           CREATE TABLE #Temp(Rowno int IDENTITY(1,1)
												,RetailerListID int
                                                ,LocationDetailID int
                                                ,MainMenuID int
                                                ,RetailID int
                                                ,RetailLocationID int)  

                           --Capture the impressions of the Retailer list.
                           INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
                                                                            , RetailID
                                                                            , RetailLocationID
                                                                            , FindCategoryID
                                                                            , DateCreated)
                     
                           OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)                                             
                                                                                         SELECT DISTINCT @MainMenuId
                                                                                                        , retailerId
                                                                                                        , RetailLocationID
                                                                                                        , @CategoryID
                                                                                                        , GETDATE()
                                                                                         FROM #RetailerList                            
                           
                                  
                           --Display the Retailer along with the keys generated in the Tracking table.
                           SELECT Distinct rowNumber
                                                       , T.RetailerListID retListID
                                                       , retailerId
                                                       , retailerName
                                                       , T.RetailLocationID
                                                       , retaileraddress1
                                                       , City
                                                       , State
                                                       , PostalCode                                   
                                                       , retLatitude
                                                       , retLongitude
                                                       , Distance = ISNULL(DistanceActual, Distance)
                                                       , logoImagePath
                                                       , bannerAdImagePath
                                                       , ribbonAdImagePath
                                                       , ribbonAdURL
                                                       , advertisementID
                                                       , splashAdID
                                                       , SaleFlag   
                         
                           FROM #Temp T
                           INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID     
                           
                           --To display bottom buttons                                                

                           EXEC [HubCitiApp2_8_3].[Find_usp_HcFunctionalityBottomButtonDisplay] @HCHubCitiID1, @ModuleName, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT
                                    
                                   
            
       END TRY
            
       BEGIN CATCH
      
       --Check whether the Transaction is uncommitable.
       IF @@ERROR <> 0
       BEGIN

               SELEct ERROR_LINE()
                     PRINT 'Error occured in Stored Procedure usp_HcFindRetailerListPagination.'                            
                     --Execute retrieval of Error info.
                     EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                     PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
       END;
            
       END CATCH;
       END;
       































GO
