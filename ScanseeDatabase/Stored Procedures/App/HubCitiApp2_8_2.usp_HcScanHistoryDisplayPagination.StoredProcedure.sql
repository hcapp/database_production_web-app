USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcScanHistoryDisplayPagination]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcScanHistoryDisplayPagination  
Purpose     : To display Scan History information of the User  
Example     : usp_HcScanHistoryDisplayPagination 2  
  
History  
Version  Date                Author          Change Description  
---------------------------------------------------------------   
1.0      14thNov2013         Dhananjaya TR   Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcScanHistoryDisplayPagination]  
(  
   @UserID int   
 , @LowerLimit int  
 , @ScreenName varchar(50)  
 , @HcHubcitiID Int
   
 --Output Variable   
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig = ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	   
	  --To get the row count for pagination.  
	  DECLARE @UpperLimit int   
	  SELECT @UpperLimit = @LowerLimit + ScreenContent   
	  FROM AppConfiguration   
	  WHERE ScreenName = @ScreenName   
	   AND ConfigurationType = 'Pagination'  
	   AND Active = 1  
	  DECLARE @MaxCnt int  
	     
	  --; WITH ScanHistoryDispaly  
	  --AS  
	  --(  
	   -- To fetch Scan History info.  
	   SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY SH.ScanDate desc)  
		, ScanHistoryID
		, SH.HcUserID userId  
		, p.ProductID  
		, P.ProductName   
		, SH.ScanDate  
		, SH.ScanCode  
		, ST.ScanType  
		, ProductLongDescription   
		, ProductImagePath  = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																								THEN @ManufConfig
																								+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																								+ProductImagePath ELSE ProductImagePath 
																						  END   
							  ELSE ProductImagePath END  
		, Status = 'Success'  
	   INTO #ScanHistoryDispaly  
	   FROM HCScanHistory SH  
		INNER JOIN Product P ON P.ProductID = SH.ProductID  
		LEFT JOIN ProductScanCodeType ST ON ST.ScanTypeID = SH.ScanTypeID  
	   WHERE SH.HcUserID = @UserID   
		AND SH.LookupMatch = 1  
	  
	  --)  
	  --To capture max row number.  
	  SELECT @MaxCnt = MAX(Row_Num) FROM #ScanHistoryDispaly  
	    
	  --this flag is a indicator to enable "More" button in the UI.   
	  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
	  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END    
	      
	      
	  SELECT Row_Num rowNumber   
		, ScanHistoryID 
		, ProductID ProductID  
		, ProductName productName  
		, ScanDate   scanDate  
		, ScanCode  barCode  
		, ScanType  scanType  
		, ProductLongDescription  ProductLongDescription  
		, ProductImagePath  ProductImagePath  
		, Status  scanStatus  
	  FROM #ScanHistoryDispaly  
	  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit  
	  --ORDER BY ScanDate  
	  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcScanHistoryDisplayPagination.'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_1].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;












































GO
