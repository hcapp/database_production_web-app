USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcDisplayRetailLocationInfo]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcDisplayRetailLocationInfo
Purpose					: To Display the basic infomration about the Retail Location.
Example					: usp_HcDisplayRetailLocationInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd May 2012	SPAN			Initial Version
---------------------------------------------------------------
*/

 CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcDisplayRetailLocationInfo]
(
	  
	  @UserID int
	, @RetailID int
    , @RetailLocationID int
    , @Latitude Decimal(18,6)
	, @Longitude  Decimal(18,6)
    
    --User Tracking Imputs
    , @ScanTypeID int
    , @MainMenuID int
    , @RetailerListID int
    
	--Output Variable 
	, @UserEmail varchar(1000) OUTPUT 
	, @claimFlag BIT OUTPUT
	, @claimBusiness varchar(1000) OUTPUT
	, @UserOutOfRange bit output
	, @DefaultPostalCode varchar(50) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRAN

				BEGIN TRY		
				
				DECLARE @RetailConfig varchar(50)
				DECLARE @RetailLocationAdvertisement VARCHAR(1000)--WelcomePage/SplashAd/BannerAd(previously)
				DECLARE @RibbonAdImagePath varchar(1000)--BannerAd
				DECLARE @RibbonAdURL VARCHAR(1000)--BannerAdURL
				DECLARE @RetailerPageQRURL VARCHAR(1000)
				DECLARE @Config VARCHAR(100)
				DECLARE @SplashAdID int
				DECLARE @BannerAdID int	 
				DECLARE @RetailerDetailsID INT
				--DECLARE @DefaultPostalCode varchar(50)
				DECLARE @HubCitiID INT
				--DECLARE @Latitude FLOAT
				--DECLARE @Longitude FLOAT
				DECLARE @ZipCode VARCHAR(10)	    
				--User Tracking
				DECLARE	@RowCount int 
				DECLARE @DistanceFromUser float
				DECLARE @UserLatitude float
				DECLARE @UserLongitude float 

				SELECT @UserLatitude = @Latitude
					 , @UserLongitude = @Longitude

				IF (@UserLatitude IS NULL) 
				BEGIN
					SELECT @UserLatitude = Latitude
						 , @UserLongitude = Longitude
					FROM HcUser A
					INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
					WHERE HcUserID = @UserID 
				END
			    
				SELECT @HubCitiID = HcHubCitiID
				FROM HubCitiReportingDatabase..MainMenu
				WHERE MainMenuID = @MainMenuID
			    
				--Retrieve the server configuration.
				SELECT @Config = ScreenContent 
				FROM AppConfiguration 
				WHERE ConfigurationType LIKE 'QR Code Configuration'
				AND Active = 1 
				
				--To get server Configuration.
				SELECT @RetailConfig= ScreenContent  
				FROM AppConfiguration   
				WHERE ConfigurationType='Web Retailer Media Server Configuration'	
				
				--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.		
				EXEC [HubCitiApp2_8_2].[usp_HcUserHubCitiRangeCheck] @UserID, @HubCitiID, @Latitude, @Longitude, @ZipCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFromUser OUTPUT
				SELECT @ZipCode = ISNULL(@DefaultPostalCode, @ZipCode)
				
				IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange = 1)
				BEGIN
					 --If the postal code is passed then derive the co ordinates.
                            IF @ZipCode IS NOT NULL
							BEGIN
                                    SELECT @Latitude = Latitude
                                        , @Longitude = Longitude
                                    FROM GeoPosition 
                                    WHERE PostalCode = @ZipCode
                            END		
							ELSE
							BEGIN
								SELECT @Latitude = G.Latitude
								     , @Longitude = G.Longitude
								FROM GeoPosition G
								INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
								WHERE U.HcUserID = @UserID	
							END	
				END  	
				
				--Get the associated Splash Ad/ Welcome Page Details.
				SELECT DISTINCT @RetailLocationAdvertisement =  @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.SplashAdImagePath
					 , @SplashAdID = RLA.AdvertisementSplashID
				FROM AdvertisementSplash RLA
				INNER JOIN RetailLocationSplashAd RL ON RL.RetailLocationID = RL.RetailLocationID
				WHERE RL.RetailLocationID = @RetailLocationID
				AND RLA.RetailID = @RetailID
				AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)
				AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementSplash ASP 
									 INNER JOIN  RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID = RLS.AdvertisementSplashID
									 WHERE RetailLocationID = @RetailLocationID)
									 
				--Get the associated Banner Ad Details.
				SELECT DISTINCT @RibbonAdImagePath = @RetailConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
					 , @RibbonAdURL = RLA.BannerAdURL
					 , @BannerAdID = RLA.AdvertisementBannerID
				FROM AdvertisementBanner RLA
				INNER JOIN RetailLocationBannerAd RL ON RL.RetailLocationID = RL.RetailLocationID
				WHERE RL.RetailLocationID = @RetailLocationID
				AND RLA.RetailID = @RetailID
				AND GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1) --if the end date is set null then that ad is always active.
				AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementBanner ASP 
									 INNER JOIN  RetailLocationBannerAd RLS ON ASP.AdvertisementBannerID = RLS.AdvertisementBannerID
									 WHERE RetailLocationID = @RetailLocationID)
				
				--Get the associated QRPage URL.
				SELECT DISTINCT @RetailerPageQRURL = CASE WHEN QR.QRRetailerCustomPageID IS NOT NULL THEN 
							@Config  + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(@RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10)) 
							ELSE NULL END			
				FROM QRRetailerCustomPage QR
				INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QR.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
				INNER JOIN QRTypes Q ON QR.QRTypeID = Q.QRTypeID
				WHERE QR.RetailID = @RetailID
				AND QRRCPA.RetailLocationID = @RetailLocationID
				AND Q.QRTypeName = 'Main Menu Page'
				
				--SELECT @Latitude = B.Latitude
				--	 , @Longitude = B.Longitude
				--	 , @PostalCode = A.PostalCode
				--FROM HcUser A
				--INNER JOIN GeoPosition B ON A.PostalCode = B.PostalCode
				--WHERE HcUserID = @UserID
				
				--Find if there is any sale or discounts associated with the Retail Location.
				SELECT DISTINCT Retailid , RetailLocationid
				INTO #RetailItemsonSale
				FROM 
				(
				SELECT DISTINCT  CR.RetailID, CR.RetailLocationID  as RetaillocationID 
				FROM Coupon C 
				INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				LEFT JOIN HcUserCouponGallery UCG ON C.CouponID = UCG.CouponID
				WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				AND CR.RetailLocationID = @RetailLocationID
				GROUP BY C.CouponID
						,NoOfCouponsToIssue
						,CR.RetailID
						,CR.RetailLocationID
				HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
						 ELSE ISNULL(COUNT(HcUserCouponGalleryID),0) + 1 END > ISNULL(COUNT(HcUserCouponGalleryID),0)
				) a			
						 
				--Display the Basic details of the Retail location.		

				SELECT DISTINCT R.RetailID retailerId
					 , R.RetailName retailerName
					 , RL.RetailLocationID retailLocationID
					 ,  retaileraddress1 = RL.Address1+','+RL.City+','+RL.State+','+cast(RL.PostalCode as varchar(1000))
					 , ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State )) retLatitude
					 , ISNULL(RL.RetailLocationLongitude, (SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) retLongitude
					 , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, (SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude,(SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958))))*6371) * 0.6214,1,1)  
					 , DistanceActual = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958) * SIN(@UserLatitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, (SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958) * COS(@UserLatitude / 57.2958) * COS((@UserLongitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude,(SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode AND City =RL.City AND State =RL.State)) / 57.2958))))*6371) * 0.6214,1,1)  
					 , CASE WHEN RL.CorporateAndStore=1 THEN R.CorporatePhoneNo ELSE C.ContactPhone END contactPhone
					 , CASE WHEN RL.RetailLocationURL IS NULL THEN R.RetailURL ELSE RL.RetailLocationURL END retailerURL
					 , @RetailLocationAdvertisement bannerAdImagePath
					 , @RibbonAdImagePath ribbonAdImagePath
					 , @RibbonAdURL ribbonAdURL
					 , image = IIF(RetailLocationImagePath IS NULL,(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),RetailerImagePath)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath) 
					 , @RetailerPageQRURL retailerPageQRURL 
					 , SaleFlag = CASE WHEN (SELECT COUNT(1) FROM #RetailItemsonSale)>0 THEN 1 ELSE 0 END
					 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')		
					 , @BannerAdID RibbonAdID
					 , @SplashAdID BannerAdID	
				INTO #RetailLocationDetails	
				FROM  RetailLocation RL 
				INNER JOIN Retailer R ON R.RetailID = RL.RetailID			
				LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
				LEFT JOIN Contact C ON C.ContactID = RC.ContactID		
				WHERE RL.RetailID = @RetailID AND RL.Active = 1 AND R.RetailerActive = 1
				AND RL.RetailLocationID = @RetailLocationID		
				
				
				--Capture the rows affected(No of Locations)
				SELECT @RowCount =@@ROWCOUNT 
				
				--User Tracking
						
				--Capture the RetailLocation click.

				 

					UPDATE HubCitiReportingDatabase..RetailerList 
					SET RetailLocationClick = RetailLocationClick +  1 
					WHERE RetailerListID in 
					
											(SELECT top 1 RetailerListID from HubCitiReportingDatabase..RetailerList
											 where Retailid = @retailid 
											 AND Retaillocationid  = @RetailLocationID order by datecreated desc )
											 

					
					--Capture the impression of the Banner & the Ribbon Ads.
					CREATE TABLE #Temp(RetailerDetailsID int
									 , RetailerListID int
									 , SplashAdID int
									 , BannerAdID int)
					
					INSERT INTO HubCitiReportingDatabase..RetailerDetail(RetailerListID
																		, SplashAdID
																		, BannerAdID
																		, DateCreated)
																		
						
														SELECT @RetailerListID 
															 , BannerAdID
															 , RibbonAdID
															 , GETDATE()
														FROM #RetailLocationDetails	
					
					--Capture the identity value inserted									
					SELECT @RetailerDetailsID = SCOPE_IDENTITY()			
					
				--Check is this is invoked from Scan Now module and update associated tables.
				IF ISNULL(@ScanTypeID, 0) <> 0
				BEGIN
					SELECT @ScanTypeID = ScanTypeID
					FROM HubCitiReportingDatabase..ScanType
					WHERE ScanType = 'Retailer Summary QR Code'
					
					INSERT INTO HubCitiReportingDatabase..Scan(MainMenuID
															  ,ScanTypeID
															  ,Success
															  ,ID
															  ,CreatedDate)
					SELECT DISTINCT @MainMenuID 
						  , @ScanTypeID 
						  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
						  , retailLocationID
						  ,GETDATE()
					FROM #RetailLocationDetails
					
					INSERT INTO HubCitiReportingDatabase..RetailerList(RetailID, RetailLocationID, RetailLocationClick, MainMenuID)
					SELECT retailerId
						 , retailLocationID
						 , 1
						 , @MainMenuID
					FROM #RetailLocationDetails
					
				END
					--Send the Retailer details ID in the result set.
					SELECT @RetailerDetailsID retDetID
						 , @RetailerListID retListID
						 , retailerId
						 , retailerName
						 , retailLocationID
						 , retaileraddress1
						 , retLatitude
						 , retLongitude
						 , Distance = ISNULL(DistanceActual, Distance)
						 , contactPhone
						 , retailerURL
						 , bannerAdImagePath
						 , ribbonAdImagePath
						 , ribbonAdURL
						 , image
						 , retailerPageQRURL 
						 , SaleFlag 
						-- , AppDownloadLink 
						 , RibbonAdID
						 , BannerAdID	
					FROM #RetailLocationDetails												
	     
		SELECT @UserEmail =  'support@hubciti.com'

			SELECT  @claimFlag = ISNULL(claimFlag,0) 
				  , @claimBusiness = CASE WHEN claimFlag = 0 OR ClaimFlag IS NULL THEN 'If this is your business, Claim It! - Free! <br/> Add or update your Website address, Phone <br/> Number, and ''''About Us''''. See more options…'
										  WHEN claimFlag = 1 THEN 'Already been Claimed' END
			FROM RetailLocation WHERE RetailLocationID = @RetailLocationID
			
	  
		   --Confirmation of Success.
		   SELECT @Status = 0	
		COMMIT TRANSACTION  
      
 END TRY    	
BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;







GO
