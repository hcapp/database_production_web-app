USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[1usp_HcSingleFindPreferredCityList]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_HcSingleFindPreferredCityList]
Purpose					: To display list of Cities which has Retailers for given HubCitiID(RegionApp).
Example					: [usp_HcSingleFindPreferredCityList]

History
Version		 Date		  Author		Change Description
--------------------------------------------------------------- 
1.0		  10/6/2014       SPAN              1.0
2.0      12/5/2016      Shilpashree     Re-written code for Optimization.
---------------------------------------------------------------
*/

CREATE PROCEDURE [HubCitiApp2_8_2].[1usp_HcSingleFindPreferredCityList]
(
    --Input variable	  
	  @UserID int	
	, @HcHubCitiID int
	, @HcMenuItemID int
    , @HcBottomButtonID int
	, @CategoryName varchar(100)
    , @Latitude Decimal(18,6)
    , @Longitude  Decimal(18,6)
    , @Radius int
    , @SearchKey varchar(255)
	, @BusinessSubCategoryID int

	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON

	DECLARE @PostalCode varchar(10)
			, @UserOutOfRange bit 
			, @DefaultPostalCode varchar(50)
			, @DistanceFromUser float 
			, @UserLatitude decimal(18,6)
			, @UserLongitude decimal(18,6)
            , @RegionAppFlag bit = 0
			, @BusinessCategoryID Int
			, @Length INT
			, @UserConfiguredPostalCode varchar(50)
			, @IsSubCategory bit = 0
			, @UserPreferredCity bit = 0
		
		DECLARE @UserID1 INT = @UserID,
				@HcHubCitiID1 INT = @HcHubCitiID,
				@HcMenuItemID1 INT = @HcMenuItemID,
				@HcBottomButtonID1 INT = @HcBottomButtonID,
				@CategoryName1 VARCHAR(100) = @CategoryName
		
		SELECT @RegionAppFlag = 1
		FROM HcHubCiti H
        INNER JOIN HcAppList AL ON H.HcAppListID =AL.HcAppListID 
        AND H.HcHubCitiID =@HCHubCitiID1 AND AL.HcAppListName ='RegionApp'
		
		IF(@RegionAppFlag = 1) 
		BEGIN

		SELECT @UserConfiguredPostalCode = PostalCode FROM HcUser WHERE HcUserID = @UserID1		
		SET @UserLatitude = @Latitude
		SET @UserLongitude = @Longitude
		SET @Length  = LEN(LTRIM(RTRIM(@SearchKey)))
		SET @SearchKey = (CASE WHEN (@Length = 4) THEN SUBSTRING(@SearchKey,1, @Length-1)
								WHEN (@Length = 5) THEN SUBSTRING(@SearchKey,1, @Length-2)
								WHEN (@Length >= 6) THEN SUBSTRING(@SearchKey,1, @Length-3) 
								ELSE @SearchKey END)
						
	    CREATE TABLE #SubCategoryList(BusinessCategoryID int,SubCategoryID int)
		CREATE TABLE #HcRetailerSubCategory(RetailID int,RetailLocationID int,BusinessCategoryID int,HcBusinessSubCategoryID int)
		CREATE TABLE #RHubcitiList(HchubcitiID Int)                                  
		CREATE TABLE #CityList (CityID Int,CityName Varchar(200),PostalCode varchar(100),HcHubCitiID int)
        CREATE TABLE #Retail(RetailID INT 
                            , RetailName VARCHAR(1000)
                            , RetailLocationID INT
                            , City VARCHAR(1000)  
							, HcCityID int  
                            , PostalCode VARCHAR(20)
                            , retLatitude FLOAT
                            , retLongitude FLOAT
                            , Distance FLOAT  
                            , DistanceActual FLOAT
							, HcHubCitiID int                                                   
							)
							   
		SELECT @BusinessCategoryID=BusinessCategoryID 
			FROM BusinessCategory 
				WHERE BusinessCategoryName LIKE @CategoryName1

		SELECT @IsSubCategory = 1
			FROM HcBusinessSubCategoryType T
			INNER JOIN HcBusinessSubCategory S ON T.HcBusinessSubCategoryTypeID = S.HcBusinessSubCategoryTypeID
				WHERE T.BusinessCategoryID = @BusinessCategoryID
				
		SELECT DISTINCT RBC.*,R.RetailName 
		INTO #RetailerBusinessCategory 
			FROM RetailerBusinessCategory RBC
			INNER JOIN Retailer R ON RBC.RetailerID = R.RetailID AND R.RetailerActive = 1 
				WHERE BusinessCategoryID=@BusinessCategoryID
	
		IF (@IsSubCategory = 1)
		BEGIN
			
			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcMenuFindRetailerBusinessCategories 
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcMenuItemID=@HcMenuItemID

			INSERT INTO #SubCategoryList(BusinessCategoryID,SubCategoryID)
			SELECT BusinessCategoryID,HcBusinessSubCategoryID
			FROM HcBottomButtonFindRetailerBusinessCategories  
				WHERE BusinessCategoryID=@BusinessCategoryID AND HcBottomButonID=@HcBottomButtonID

			INSERT INTO #HcRetailerSubCategory(RetailID,RetailLocationID,BusinessCategoryID,HcBusinessSubCategoryID)
			SELECT RS.RetailID,RS.RetailLocationID,RS.BusinessCategoryID,RS.HcBusinessSubCategoryID
			FROM HcRetailerSubCategory RS
			INNER JOIN #SubCategoryList SL ON RS.BusinessCategoryID = SL.BusinessCategoryID AND RS.HcBusinessSubCategoryID = SL.SubCategoryID
			
		END

		INSERT INTO #RHubcitiList(HcHubCitiID)
			(SELECT DISTINCT HcHubCitiID 
			FROM HcRegionAppHubcitiAssociation WHERE HcRegionAppID = @HCHubCitiID1 
			UNION ALL
			SELECT  @HCHubCitiID1 AS HcHubCitiID
			)

		--To check whether user has preferred cities are not.
		SELECT @UserPreferredCity = CASE WHEN HcUserID = @UserID AND HcCityID IS NULL THEN 0
											WHEN HcUserID = @UserID AND HcCityID IS NOT NULL THEN 1
											ELSE 0 END
		FROM HcUsersPreferredCityAssociation
		WHERE HcHubcitiID = @HcHubCitiID AND HcUserID = @UserID

		--To Fetch Region app associated citylist
		INSERT INTO #CityList(CityID
								,CityName
								,PostalCode
								,HcHubCitiID)
		SELECT DISTINCT LA.HcCityID 
						,LA.City
						,LA.PostalCode
						,LA.HcHubCitiID					
		FROM #RHubcitiList H
		INNER JOIN HcLocationAssociation LA ON LA.HcHubCitiID = H.HchubcitiID 
		WHERE H.HcHubcitiID = @HcHubCitiID

		--Pick the co ordicates of the default postal code if the user has not configured the Postal Code.
		IF (@UserLatitude IS NULL) 
		BEGIN
				SELECT @UserLatitude = Latitude
						, @UserLongitude = Longitude
				FROM HcHubCiti A
				INNER JOIN GeoPosition B ON A.DefaultPostalCode = B.PostalCode
				WHERE A.HcHubCitiID = @HCHubCitiID1
		END

		--To check if the User is there outside the range of the HubCiti and provide the data based on the nearest or the default Postal Code.              
		EXEC [HubCitiApp2_8_2].[Find_usp_HcUserHubCitiRangeCheck] @Radius, @HCHubCitiID1, @Latitude, @Longitude, @PostalCode, 1, @UserConfiguredPostalCode, @UserOutOfRange = @UserOutOfRange OUTPUT, @DefaultPostalCode = @DefaultPostalCode OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT, @Status = @Status OUTPUT, @Distance = @DistanceFROMUser OUTPUT
		SELECT @PostalCode = ISNULL(@DefaultPostalCode, @PostalCode)

		--Derive the Latitude and Longitude in the absence of the input.
		IF (@Latitude IS NULL AND @Longitude IS NULL) OR (@UserOutOfRange=1)
		BEGIN
				--If the postal code is passed then derive the co ordinates.
				IF @PostalCode IS NOT NULL
				BEGIN
						SELECT @Latitude = Latitude
							, @Longitude = Longitude
						FROM GeoPosition 
						WHERE PostalCode = @PostalCode
				END          
				ELSE
				BEGIN
						SELECT @Latitude = G.Latitude
							, @Longitude = G.Longitude
						FROM GeoPosition G
						INNER JOIN HcUser U ON G.PostalCode = U.PostalCode
						WHERE U.HcUserID = @UserID 
				END                                                                               
		END
					
		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL
                   
		--Get the user preferred radius.
		SELECT @Radius = LocaleRadius
        FROM HcUserPreference 
        WHERE HcUserID = @UserID1
                                         
        SELECT @Radius = ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration 
											WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius')) 

		SELECT DISTINCT RBC.RetailerID 
				, RBC.RetailName    
				, RL.RetailLocationID 
				, RL.City
				, RL.HcCityID
				, RL.PostalCode
				, RetailLocationLatitude
				, RetailLocationLongitude
				, SaleFlag = 0 
				, HL.HcHubCitiID 
		INTO #RetailInfo1
		FROM #RetailerBusinessCategory RBC
		INNER JOIN RetailLocation RL ON RBC.RetailerID = RL.RetailID
		INNER JOIN #CityList HL ON RL.PostalCode=HL.PostalCode AND RL.HcCityID = HL.CityID AND HL.HcHubCitiID=@HCHubCitiID1 
		INNER JOIN #RHubcitiList RH ON RH.HchubcitiID =HL.HcHubCitiID
		INNER JOIN HcRetailerAssociation RA ON RL.RetailID = RA.RetailID AND RL.RetailLocationID = RA.RetailLocationID AND RA.HcHubCitiID = RH.HchubcitiID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = RL.RetailID
		WHERE RL.Headquarters = 0 AND D.DuplicateRetailerID IS NULL 
		
		SELECT * INTO #RetailInfo FROM #RetailInfo1 WHERE 1=2

		IF(@IsSubCategory = 1)
		BEGIN
				INSERT INTO #RetailInfo
				SELECT R.* 
				FROM #RetailInfo1 R
				INNER JOIN #HcRetailerSubCategory RS ON R.RetailLocationID = RS.RetailLocationID
		END
		ELSE IF(@IsSubCategory = 0)
		BEGIN
				INSERT INTO #RetailInfo
				SELECT R.* 
				FROM #RetailInfo1 R
		END

		INSERT INTO #Retail 
        SELECT  RetailerID    
                , RetailName
                , RetailLocationID
                , City 
				, HcCityID  
                , PostalCode
                , retLatitude
                , retLongitude
                , Distance    
                , DistanceActual 
				, HcHubCitiID                                            
		FROM 
		(SELECT DISTINCT RL.RetailerID 
					, RL.RetailName    
					, RL.RetailLocationID 
					, RL.City
					, RL.HcCityID
					, RL.PostalCode
					, RL.RetailLocationLatitude  retLatitude
					, RL.RetailLocationLongitude retLongitude
					, Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)                       
					, DistanceActual = 0
					, RL.HcHubCitiID
		FROM #RetailInfo RL
		LEFT JOIN #HcRetailerSubCategory RS ON RL.RetailLocationID = RS.RetailLocationID
		LEFT JOIN GeoPosition G ON G.PostalCode = RL.PostalCode AND G.City = RL.City 
		LEFT JOIN RetailerKeywords RK ON RL.RetailerID = RK.RetailID                                 
        WHERE (@BusinessSubCategoryID IS NULL 
				OR (RS.HcBusinessSubCategoryID IN (SELECT Param FROM fn_SplitParam(@BusinessSubCategoryID,',')) ))
		AND (((@SearchKey IS NOT NULL AND @SearchKey <> '') AND (RL.RetailName LIKE '%'+@SearchKey+'%' OR RK.RetailKeyword = @SearchKey))
												OR (@SearchKey IS NULL))
		) Retailer
        WHERE Distance <= @Radius  
	
		SELECT DISTINCT  City 
						, HcCityID
						, HcHubCitiID                                      
		INTO #Retailer    
		FROM #Retail R
		
		SELECT DISTINCT R.HcCityID
				, R.City
				, isCityChecked  = CASE WHEN HcUsersPreferredCityAssociationID IS NOT NULL AND (R.HcCityID = U.HcCityID OR U.HcCityID IS NULL) THEN 1 
										WHEN HcUsersPreferredCityAssociationID IS NULL THEN 0
										ELSE 0 END
		INTO #Final
		FROM #Retailer R
		LEFT JOIN HcUsersPreferredCityAssociation U ON R.HcHubCitiID = U.HcHubCitiID
			AND U.HcUserID = @UserID1 AND (R.HcCityID = U.HcCityID OR U.HcCityID IS NULL)
			 
		SELECT DISTINCT HcCityID AS CityID
					,City AS CityName
					,isCityChecked
		FROM #Final
		ORDER BY CityName

		END

		--Confirmation of Success.
		SELECT @Status = 0						  
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcSingleFindPreferredCityList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
