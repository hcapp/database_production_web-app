USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcRetailLocationCouponsDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
Stored Procedure name : usp_HcRetailLocationCouponsDisplay         
Purpose				  : To list Coupon associated to the RetailLocation     
Example				  : usp_HcRetailLocationCouponsDisplay         
        
History        
Version		Date					Author					Change Description        
-------------------------------------------------------------------------------         
1.0			24thOct2013             Dhananjaya TR			Initial Version        
-------------------------------------------------------------------------------        
*/        
        
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcRetailLocationCouponsDisplay]        
(        
   @UserID int        
 , @RetailID int
 , @RetailLocationID int
 , @LowerLimit int        
 , @ScreenName varchar(50)
 , @HcHubCitiID Int   
 
 --User Tracking inputs.
 , @RetailerListID int
 , @MainMenuID int     
         
 --OutPut Variable        
 , @NxtPageFlag bit output 
 , @Status int output       
 , @ErrorNumber int output        
 , @ErrorMessage varchar(1000) output        
)        
AS        
BEGIN        
         
 BEGIN TRY    
 
	BEGIN TRANSACTION    
   
		 --To get Server Configuration
		 DECLARE @ManufConfig varchar(50) 
		 DECLARE @RetailerConfig varchar(50)
		 DECLARE @ProductName varchar(1000)
		  
		 SELECT @ManufConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		 
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration'
	         
		  --To get the row count for pagination.        
		  DECLARE @UpperLimit int         
		  SELECT @UpperLimit = @LowerLimit + ScreenContent         
		  FROM AppConfiguration         
		  WHERE ScreenName = @ScreenName         
		   AND ConfigurationType = 'Pagination'        
		   AND Active = 1        
		  DECLARE @MaxCnt int        
	          
		--To get Image of the product   
		SELECT DISTINCT c.CouponID 
			 , CouponName 
			 , CouponDiscountType
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponShortDescription
			 , CouponLongDescription
			 , CouponDateAdded
			 , CouponStartDate
			 , CouponExpireDate
			 , CouponURL  
			 , CouponImagePath = CASE WHEN CouponImagePath IS NULL --Display the associated product image path if the Coupon has no image.
										THEN (SELECT TOP 1 CASE WHEN P.WebsiteSourceFlag = 1 
																	THEN (CASE WHEN P.ProductImagePath IS NOT NULL 
																					THEN @ManufConfig + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + P.ProductImagePath
																				ELSE NULL 
																		  END)
																ELSE P.ProductImagePath 
														   END							
											  FROM CouponProduct CP
											  INNER JOIN Product P ON CP.ProductID = P.ProductID
											  WHERE CP.CouponID = C.CouponID)
								ELSE CASE WHEN C.WebsiteSourceFlag = 1 
											  THEN @RetailerConfig + CAST(CR.RetailID AS VARCHAR(100)) + '/' + C.CouponImagePath
										  ELSE C.CouponImagePath 
									 END		
								END		
	                           
	       
			 , usage = CASE WHEN UC.CouponID IS NULL THEN 'Red' ELSE 'Green' END        
			 , ViewableOnWeb
			 , NoOfCouponsToIssue
		INTO #Coupons1
		FROM Coupon C
		INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
		INNER JOIN RetailLocation RL ON RL.RetailLocationID=CR.RetailLocationID AND RL.Active=1
		LEFT JOIN HcLocationAssociation HL ON HL.Postalcode=RL.Postalcode AND HL.HcHubCitiID=@HcHubCitiID
		LEFT JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1
		LEFT JOIN HcUserCouponGallery UC ON UC.CouponID = C.CouponID AND UC.HcUserID = @UserID
		WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate     
		AND CR.RetailID = @RetailID
		AND CR.RetailLocationID = @RetailLocationID		
		
		
		SELECT RowNum = IDENTITY(INT, 1, 1)
			 , @RetailerListID RetailerListID
			 , C.CouponID
			 , C.CouponName
			 , CouponDiscountType
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponShortDescription
			 , CouponLongDescription
			 , CouponDateAdded
			 , CouponStartDate
			 , CouponExpireDate
			 , CouponURL  
			 , CouponImagePath  
			 , usage
			 , ViewableOnWeb	
		INTO #Coupons2	 
		FROM #Coupons1 C
		LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID
		GROUP BY  C.CouponID
			 , C.CouponName
			 , CouponDiscountType
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponShortDescription
			 , CouponLongDescription
			 , CouponDateAdded
			 , CouponStartDate
			 , CouponExpireDate
			 , CouponURL  
			 , CouponImagePath  
			 , usage
			 , ViewableOnWeb	
			 , NoOfCouponsToIssue
		HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
				 ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)
		
		--To capture max row number.        
	   SELECT @MaxCnt = MAX(RowNum) FROM #Coupons2
	   
	   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
	   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
		
		SELECT RowNum
			 , @RetailerListID RetailerListID
			 , CouponID
			 , CouponName
			 , CouponDiscountType
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponShortDescription
			 , CouponLongDescription
			 , CouponDateAdded
			 , CouponStartDate
			 , CouponExpireDate
			 , CouponURL  
			 , CouponImagePath  
			 , usage
			 , ViewableOnWeb	
		INTO #CouponsList	 
		FROM #Coupons2
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit 
		
		--User Tracking Section.
		
		--Capture the impression of the Coupons.
		CREATE TABLE #Temp(CouponListID INT
						 , RetailerListID INT
						 , CouponID INT)
		INSERT INTO HubCitiReportingDatabase..CouponsList(RetailerListID
													   , MainMenuID
													   , CouponID
													   , DateCreated)
		OUTPUT inserted.CouponsListID, inserted.RetailerListID, inserted.CouponID INTO #Temp(CouponListID, RetailerListID, CouponID)
											   
		SELECT @RetailerListID
			 , @MainMenuID
			 , CouponID
			 , GETDATE()
		FROM #CouponsList
		
		--Display the records along with the key of the tracking table.
		SELECT DISTINCT RowNum
			 , T.CouponListID
			 , T.RetailerListID retListID
			 , T.CouponID
			 , CouponName
			 , CouponDiscountType
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponShortDescription
			 , CouponLongDescription
			 , CouponDateAdded
			 , CouponStartDate
			 , CouponExpireDate
			 , CouponURL  
			 , CouponImagePath  
			 , usage
			 , ViewableOnWeb
		FROM #CouponsList C 
		INNER JOIN #Temp T ON T.CouponID = C.CouponID	
		
		--Confirmation of Success.
		SELECT @Status = 0								
	 COMMIT TRANSACTION         
 END TRY        
         
 BEGIN CATCH        
         
  --Check whether the Transaction is uncommitable.        
  IF @@ERROR <> 0        
  BEGIN        
   PRINT 'Error occured in Stored Procedure usp_HcRetailLocationCouponsDisplay.'          
   --- Execute retrieval of Error info.        
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
   ROLLBACK TRANSACTION; 
   --Confirmation of failure.
   SELECT @Status = 1								        
  END;        
           
 END CATCH;        
END;














































GO
