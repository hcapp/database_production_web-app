USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcHubCitiGalleryCouponLocationDisplay]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HcHubCitiGalleryCouponLocationDisplay    
Purpose               : To display RetailLocation details Associated to given Coupon   
Example               : usp_HcHubCitiGalleryCouponLocationDisplay    
    
History
Version		   Date			Author	  Change Description
-----------------------------------------------------------
1.0			6/11/2013	    SPAN	       1.0
----------------------------------------------------------- 
*/    
    
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcHubCitiGalleryCouponLocationDisplay]    
(  

	--Input Variables  
	  @UserID int    
	, @CouponID int
	, @HcHubCitiID int  

	--User Tracking input Variables
	, @CouponListID int
	, @MainMenuID int
	 
	--OutPut Variables
	, @Status int output
	, @ErrorNumber int output    
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY   
		
		DECLARE @Config varchar(1000)
		DECLARE @RetailConfig varchar(1000)

		SELECT @Config=ScreenContent
        FROM AppConfiguration 
        WHERE ConfigurationType='App Media Server Configuration'

        SELECT @RetailConfig=ScreenContent
        FROM AppConfiguration 
        WHERE ConfigurationType='Web Retailer Media Server Configuration'

		--To fetch all the duplicate retailers.
		SELECT DISTINCT DuplicateRetailerID 
		INTO #DuplicateRet
		FROM Retailer 
		WHERE DuplicateRetailerID IS NOT NULL AND RetailerActive=1
 
		SELECT rowNumber = ROW_NUMBER() OVER(ORDER BY CouponName)
				,CR.RetailID
				,R.RetailName
				,CR.RetailLocationID
				,completeAddress = RL.Address1 + ', ' + RL.City + ', ' + RL.[State] + ', ' + RL.PostalCode
				,logoImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)                                     
		INTO #RetailerList
		FROM Coupon C
		INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
		INNER JOIN Retailer R ON CR.RetailID = R.RetailID AND R.RetailerActive=1
		INNER JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active=1
		INNER JOIN HcRetailerAssociation HRA ON HRA.RetailID =RL.RetailID AND  HRA.RetailLocationID = RL.RetailLocationID AND HRA.HcHubCitiID= @HcHubCitiID
		LEFT JOIN #DuplicateRet D ON D.DuplicateRetailerID = R.RetailID
		WHERE  C.CouponID = @CouponID AND D.DuplicateRetailerID IS NULL    
		--ORDER BY rowNumber 
 
	  
	    --User Tracking section.
	    
	   CREATE TABLE #Temp1(RetailerListID INT	
						, RetailID INT
						, RetailLocationID INT)
			
	   INSERT INTO HubCitiReportingDatabase..RetailerList(MainMenuID
													        , RetailID
													        , RetailLocationID
													        , DateCreated
													        )
	   OUTPUT inserted.RetailerListID, inserted.RetailID, inserted.RetailLocationID INTO #Temp1(RetailerListID,RetailID,RetailLocationID)
													   SELECT @MainMenuID
															, RetailID
															, RetailLocationID
															, GETDATE()
													   FROM #RetailerList
	   
		--Display along with the primary key of the tracking table.
		SELECT rowNumber
			 , T.RetailerListID retListID
			 , R.RetailID retailerId
			 , R.RetailName retailerName
			 , R.RetailLocationID  
			 , completeAddress
			 , logoImagePath
		FROM #RetailerList R
		INNER JOIN #Temp1 T ON T.RetailLocationID = R.RetailLocationID
	    
	    --Confirmation of Success.
		SELECT @Status = 0	
		
 END TRY    
     
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_HcHubCitiGalleryCouponLocationDisplay].'    
   --- Execute retrieval of Error info.  
   EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;









GO
