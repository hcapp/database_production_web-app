USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcUserTrackingHotDealGetDealClick]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HcUserTrackingHotDealGetDealClick    
Purpose				  : To capture the getdeal click in the Hot Deals details page.    
Example				  : usp_HcUserTrackingHotDealGetDealClick 1,1    
    
History    
Version  Date			Author			Change Description    
---------------------------------------------------------------     
1.0		15thNov2013 	Dhananjaya TR	Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcUserTrackingHotDealGetDealClick]    
(    
   @UserID int    
 , @HotDealID int    

 --User Tracking inputs.
 , @HotDealListID int
     
 --OutPut Variable 
 , @Status int output   
 , @ErrorNumber int output    
 , @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY
 
	BEGIN TRANSACTION    
		
		UPDATE HubCitiReportingDatabase..HotDealList SET GetDealClick = 1 
		WHERE HotDealListID = @HotDealListID
	  
	  --Confirmation of Success.
	  SELECT @Status=0	
	COMMIT TRANSACTION
 END TRY    
     
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_HcUserTrackingHotDealGetDealClick.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
   ROLLBACK TRANSACTION;
   --Confirmation of failure.
   SELECT @Status = 1    
  END;    
       
 END CATCH;    
END;














































GO
