USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcMasterShoppingListCoupon]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
Stored Procedure name	: usp_HcMasterShoppingListCoupon         
Purpose					: To list Coupon        
Example					 : usp_HcMasterShoppingListCoupon         
        
History        
Version  Date			Author		 Change Description        
---------------------------------------------------------------         
1.0		4th Dec 2013	Mohith H R		Initial Version        
---------------------------------------------------------------        
*/        
        
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcMasterShoppingListCoupon]        
(        
   @ProductID int        
 , @RetailID int        
 , @UserID int        
 , @LowerLimit int        
 , @ScreenName varchar(50)  
 , @HcHubCitiID int
 
 --User Tracking inputs
 , @MainMenuID int      
         
 --OutPut Variable        
 , @NxtPageFlag bit output        
 , @ErrorNumber int output        
 , @ErrorMessage varchar(1000) output        
)        
AS        
BEGIN        
         
 BEGIN TRY        
   
	 --To get Server Configuration
	 DECLARE @ManufConfig varchar(50) 
	 DECLARE @RetailerConfig varchar(50)
	  
	 SELECT @ManufConfig= ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
	 
	 SELECT @RetailerConfig= ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Retailer Media Server Configuration'

         
	  --To get the row count for pagination.        
	  DECLARE @UpperLimit int         
	  SELECT @UpperLimit = @LowerLimit + ScreenContent         
	  FROM AppConfiguration         
	  WHERE ScreenName = @ScreenName         
	   AND ConfigurationType = 'Pagination'        
	   AND Active = 1        
	  DECLARE @MaxCnt int        
          
  --To get Image of the product        
   DECLARE @ProductImagePath varchar(1000)        
   SELECT @ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1     
                       THEN @ManufConfig    
                       +CONVERT(VARCHAR(30),ManufacturerID)+'/'    
                       +ProductImagePath ELSE ProductImagePath     
                       END       
                          ELSE ProductImagePath END           
   FROM Product         
   WHERE ProductID = @ProductID        
           
  IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)        
  BEGIN        
           
           
   SELECT Row_Num=ROW_NUMBER() over(order by C.couponid)        
    ,C.CouponID         
    ,CouponName        
    ,CouponDiscountType        
    ,CouponDiscountAmount        
    ,CouponDiscountPct        
    ,CouponShortDescription        
    ,CouponLongDescription        
    ,CouponDateAdded        
    ,CouponStartDate        
    ,CouponExpireDate      
    ,CouponURL couponURL      
    ,CouponImagePath =CASE WHEN CouponImagePath IS NULL THEN @ProductImagePath ELSE CASE WHEN CouponImagePath IS NOT NULL THEN   
																									  CASE WHEN WebsiteSourceFlag = 1   
																										   THEN @RetailerConfig  
																											  +CONVERT(VARCHAR(30),CR.RetailID)+'/'  
																											   +CouponImagePath   
																											 ELSE CouponImagePath   
																										 END  
                         END   
      END     
     ,ViewableOnWeb  
   INTO #Coupon        
   FROM Coupon C        
   INNER JOIN CouponProduct CP ON C.couponid=CP.CouponID      
   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID  
   LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active=1
   LEFT JOIN HcLocationAssociation HL ON RL.PostalCode = HL.PostalCode  AND HL.HcHubCitiID = @HcHubCitiID AND RL.City = HL.City AND RL.State = HL.State    
   WHERE CP.ProductID = @ProductID         
   AND CR.RetailID = @RetailID         
   AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate        
            
     --To capture max row number.        
   SELECT @MaxCnt = MAX(Row_Num) FROM #Coupon        
   --this flag is a indicator to enable "More" button in the UI.         
   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END        
          
    SELECT C.CouponID
    INTO #CouPon2
    FROM #Coupon C
    INNER JOIN CouponProduct CP ON c.CouponID=cp.CouponID
   DECLARE @ProductNames Varchar(max)
   SELECT @ProductNames = COALESCE(@Productnames+',','')+productname FROM  CouponProduct CP  
							INNER JOIN Product P ON P.ProductID=CP.ProductID WHERE CouponID in (SELECT CouponID FROM #CouPon2) 
   SELECT Row_Num         
    ,C.CouponID         
    ,C.CouponName 
    ,@ProductNames AS ProductName        
    ,C.CouponDiscountType         
    ,C.CouponDiscountAmount         
    ,C.CouponDiscountPct         
   ,CouponShortDescription         
    ,CouponLongDescription          
    ,C.CouponDateAdded         
    ,C.CouponStartDate         
    ,C.CouponExpireDate      
    ,C.CouponURL         
    ,C.CouponImagePath         
    ,usage = CASE WHEN CG.CouponID IS NULL THEN 'Red' ELSE 'Green' END
    ,ViewableOnWeb        
   INTO #CouponList1
   FROM #Coupon C        
   LEFT JOIN HcUserCouponGallery CG ON CG.CouponID = C.CouponID AND HcUserID = @UserID         
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit         
   ORDER BY Row_Num        
           
           
  END        
          
  IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS NULL OR @RetailID =0)        
  BEGIN        
	   SELECT Row_Num=ROW_NUMBER() over(order by C.CouponID)         
		,C.CouponID         
		,CouponName        
		,CouponDiscountType couponDiscountType        
		,CouponDiscountAmount couponDiscountAmount        
		,CouponDiscountPct couponDiscountPct        
		,CouponShortDescription        
		,CouponLongDescription        
		,CouponDateAdded couponDateAdded        
		,CouponStartDate couponStartDate        
		,CouponExpireDate couponExpireDate       
		,CouponURL       
		,CouponImagePath =CASE WHEN CouponImagePath IS NULL THEN @ProductImagePath ELSE CASE WHEN CouponImagePath IS NOT NULL THEN   
																										  CASE WHEN WebsiteSourceFlag = 1   
																											   THEN @RetailerConfig  
																												   +CONVERT(VARCHAR(30),CR.RetailID)+'/'  
																												   +CouponImagePath   
																												 ELSE CouponImagePath   
																											 END  
							 END  
		  END     
	   ,ViewableOnWeb       
	   INTO #Coupon1        
	   FROM Coupon C        
	   INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID     
	   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID     
	   LEFT JOIN RetailLocation RL ON CR.RetailLocationID = RL.RetailLocationID AND RL.Active=1
	   LEFT JOIN HcLocationAssociation HL ON RL.PostalCode = HL.PostalCode AND HL.HcHubCitiID = @HcHubCitiID AND RL.City = HL.City AND RL.State = HL.State  
	   WHERE CP.ProductID = @ProductID         
	   AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
	
		SELECT C.CouponID
		INTO #CouPon3
		FROM #Coupon1 C
		INNER JOIN CouponProduct CP ON c.CouponID=cp.CouponID
	   DECLARE @ProductName Varchar(max)
	   SELECT @ProductName = COALESCE(@Productname+',','')+productname FROM  CouponProduct CP  
								INNER JOIN Product P ON P.ProductID=CP.ProductID WHERE CouponID in (SELECT CouponID FROM #CouPon3)             
	           
		--To capture max row number.        
	   SELECT @MaxCnt = MAX(Row_Num) FROM #Coupon1        
	   --this flag is a indicator to enable "More" button in the UI.         
	   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
	   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
	   
	 
	   SELECT Row_Num         
		,C.CouponID         
		,C.CouponName  
		,@ProductName  AS  ProductName       
		,C.CouponDiscountType         
		,C.CouponDiscountAmount         
		,C.CouponDiscountPct         
		,CouponShortDescription        
		,CouponLongDescription        
		,C.CouponDateAdded         
		,C.CouponStartDate         
		,C.CouponExpireDate       
		,C.CouponURL       
		,C.CouponImagePath         
		,usage = CASE WHEN CG.CouponID IS NULL THEN 'Red' ELSE 'Green' END        
		,ViewableOnWeb
	   INTO #CouponList 
	   FROM #Coupon1 C        
		LEFT JOIN HcUserCouponGallery CG ON CG.CouponID = C.CouponID AND HcUserID = @UserID         
	   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit         
	   ORDER BY Row_Num   
	  
	 END 
	   --User Tracking
	   
	   CREATE TABLE #Temp(CouponListID int,CouponID int)
	   IF ISNULL(@RetailID, 0) = 0
	   BEGIN
		   
		   
		   INSERT INTO HubCitiReportingDatabase..CouponsList(MainMenuID   
															,CouponID													
															,DateCreated)
		   
		   OUTPUT inserted.CouponsListID,inserted.CouponID INTO #Temp(CouponListID,CouponID)
		   SELEct @MainMenuID 
				 ,couponId 
				 ,GETDATE()   
		   FROM #CouponList 
		   
		   SELECT Row_Num rowNum 
			,T.CouponListID      
			,C.CouponID couponId        
			,C.CouponName couponName 
			,ProductName       
			,C.CouponDiscountType couponDiscountType        
			,C.CouponDiscountAmount couponDiscountAmount        
			,C.CouponDiscountPct couponDiscountPct        
			,CouponShortDescription        
			,CouponLongDescription        
			,C.CouponDateAdded couponDateAdded        
			,C.CouponStartDate couponStartDate        
			,C.CouponExpireDate couponExpireDate      
			,C.CouponURL       
			,C.CouponImagePath imagePath        
			,usage     
			,ViewableOnWeb
		   FROM #CouponList C
		   INNER JOIN #Temp T ON T.CouponID=C.couponId       
	   
	   
	   END
	   
	   
	 ELSE         
	 BEGIN  
	 
	       INSERT INTO HubCitiReportingDatabase..CouponsList(MainMenuID   
															,CouponID													
															,DateCreated)
		   
		   OUTPUT inserted.CouponsListID,inserted.CouponID INTO #Temp(CouponListID,CouponID)
		   SELEct @MainMenuID 
				 ,couponId 
				 ,GETDATE()   
		   FROM #CouponList1 
	 
	  
	 SELECT Row_Num rowNum 
	    , T.CouponListID        
		,C.CouponID couponId        
		,C.CouponName couponName
		,ProductName        
		,C.CouponDiscountType         
		,C.CouponDiscountAmount         
		,C.CouponDiscountPct         
	   ,CouponShortDescription         
		,CouponLongDescription          
		,C.CouponDateAdded         
		,C.CouponStartDate         
		,C.CouponExpireDate      
		,C.CouponURL         
		,C.CouponImagePath imagePath        
		,usage
		,ViewableOnWeb        
   FROM #CouponList1 C
   INNER JOIN #Temp T ON T.CouponID =C.CouponID 
END	  
	       
 END TRY        
         
 BEGIN CATCH        
         
  --Check whether the Transaction is uncommitable.        
  IF @@ERROR <> 0        
  BEGIN        
   PRINT 'Error occured in Stored Procedure usp_MasterShoppingListCoupon.'          
   --- Execute retrieval of Error info.        
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output         
  END;        
           
 END CATCH;        
END;
















































GO
