USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_2].[usp_HcUserTrackingProductMediaClicks]    Script Date: 4/6/2017 1:08:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HcUserTrackingProductMediaClicks    
Purpose               : To Capture Clicks on ProductMedia  
Example               : usp_HcUserTrackingProductMediaClicks    
    
History
Version		   Date			Author	  Change Description
-----------------------------------------------------------
1.0			6/11/2013	    SPAN	       1.0
-----------------------------------------------------------  
*/    
    
CREATE PROCEDURE [HubCitiApp2_8_2].[usp_HcUserTrackingProductMediaClicks]    
(  

	--User Tracking input Variables
	  @ProductMediaListID int
	 
	--OutPut Variables
	, @Status int output
	, @ErrorNumber int output    
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY    
		
		--Update the ProductMediaList tracking table.
		
		UPDATE HubCitiReportingDatabase..ProductMediaList
		SET ProductMediaClick = 1
		WHERE ProductMediaListID = @ProductMediaListID
		
	    --Confirmation of Success.
		SELECT @Status = 0	
	 
 END TRY    
     
	BEGIN CATCH  

		--Check whether the Transaction is uncommitable.  
		IF @@ERROR <> 0  
		BEGIN  
		PRINT 'Error occured in Stored Procedure usp_HcUserTrackingProductMediaClicks.'    
		--- Execute retrieval of Error info.  
		EXEC [HubCitiApp2_8_2].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
		SELECT @Status = 1
		END;  
	 
	END CATCH;  
END;












































GO
