USE [scansee]
GO
/****** Object:  StoredProcedure [HubCitiApp2_8_7].[usp_HcScanProduct]    Script Date: 4/6/2017 1:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_HcScanProduct  
Purpose     : To get the details of the scanned product.  
Example     : EXEC usp_HcScanProduct 35, '38000391200', NULL, NULL, NULL, 1234, -73.99653, 40.750742, 0, '6/2/2011', NULL  
  
History  
Version      Date         Author        Change Description  
---------------------------------------------------------------   
1.0          30thOct2013  SPAN          Initial version 
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [HubCitiApp2_8_7].[usp_HcScanProduct]  
(  
      @UserId int  
    , @ScanCode varchar(20)  
   --Scan History and ProductUserHit Variables  
   --, @RetailLocationID int    
   --, @ProductID int    
   --, @RebateID int    
	 , @DeviceID varchar(60)  
	 , @ScanType varchar(10)    
	 , @ScanLongitude float    
	 , @ScanLatitude float    
	 , @FieldAgentRequest bit    
	 , @Date datetime  
	 , @HcHubCitiID Int 
	 --, @ScanTypeID tinyint   
 
    --User Tracking Imputs
    , @ScanTypesID bit
    , @MainMenuID int
   
 --OutPut Variable  
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	BEGIN TRANSACTION
 
		  --To get Media Server Configuration.  
		  DECLARE @ManufConfig varchar(50)    
		  SELECT @ManufConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
		  
		  DECLARE @ScanType1 int
		  SELECT @ScanType1 = ScanTypeID
		  FROM HubCitiReportingDatabase..ScanType
		  WHERE ScanType = 'Product Bar Code'
		  
		  		  
		  DECLARE @ScanTypeID tinyint  
		  SELECT @ScanTypeID = ScanTypeID  
		  FROM ProductScanCodeType   
		  WHERE ScanType = @ScanType  
		  DECLARE @ProductID int  
		  DECLARE @RebateID int   
		 
		  --To get scanned product details  
		  SELECT ProductID    
			 , ProductName    
			 , M.ManufName      
			 , ModelNumber    
			 , ProductLongDescription   
			 , ProductExpirationDate    
			 , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																									THEN @ManufConfig
																									+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																									+ProductImagePath ELSE ProductImagePath 
																							  END   
								  ELSE ProductImagePath END     
			 , SuggestedRetailPrice   
			 , Weight    
			 , WeightUnits  
		  INTO #Product          
		  FROM Product P  
		   LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID  
		  WHERE ScanCode = @ScanCode  
		   AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
		    
		  SELECT ProductID productId  
			 , ProductName productName  
			 , ManufName  manufacturersName  
			 , ModelNumber modelNumber  
			 , ProductLongDescription   
			 , ProductExpirationDate productExpDate  
			 , ProductImagePath imagePath  
			 , SuggestedRetailPrice   
			 , Weight productWeight  
			 , WeightUnits        
		  FROM #Product   
		    
		  -- To get the count of product fetched.  
		  DECLARE @ROWCOUNT int = @@ROWCOUNT  
		  --DECLARE @Status int  
		  SELECT @ProductID = ProductID FROM #Product     
		    
		 IF @ROWCOUNT > 0   
		  BEGIN  
		   DECLARE @RetailLocationID int     
		   -- If GPS is ON and the Scan Latitude and Longitude is passed, fetching Retailer location whose coordinates matches.    
		   IF @RetailLocationID IS NULL AND (@ScanLatitude IS NOT NULL AND @ScanLongitude IS NOT NULL)    
		   BEGIN    
			  SELECT RL.RetailLocationID    
			  INTO #RetailLocation    
			  FROM RetailLocationProduct RLP     
			  INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID AND RL.Active=1   
			  INNER JOIN HcLocationAssociation HL ON RL.Postalcode=HL.Postalcode AND HL.HcHubcitiid= @HcHubCitiID 
			  INNER JOIN HcRetailerAssociation RLC ON RLC.HcHubCitiID =@HcHubCitiID AND RLC.RetailLocationID =RL.RetailLocationID AND Associated = 1 
			  WHERE RLP.ProductID = @ProductID     
			  AND (RL.RetailLocationLatitude = @ScanLatitude AND RL.RetailLocationLongitude = @ScanLongitude)    
			-- If Single Retail locations matched then capture.    
				DECLARE @RetailLoc int    
				SELECT @RetailLoc = CASE WHEN COUNT(RetailLocationID) = 1 THEN MIN(RetailLocationID) END    
				FROM #RetailLocation  
			END
			
		   EXEC [HubCitiApp2_3_3].[usp_HcScanSuccessScanHistory]  @UserID, @ProductID, @RetailLocationID,  @RebateID, @DeviceID, @ScanLongitude, @ScanLatitude, 0, @Date, @ScanCode, @ScanTypeID, 1, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
		   EXEC [HubCitiApp2_3_3].[usp_HcUserProductHit] @UserID, @ProductID, @RetailLocationID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
		  END  
		  
		  --User Tracking		
		  IF @ScanTypesID = 1
		  BEGIN
				INSERT INTO HubCitiReportingDatabase..scan(MainMenuID
														  ,ScanTypeID
														  ,Success
														  ,ID
														  ,CreatedDate)
				SELECT @MainMenuID 
					  ,@ScanType1 
					  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
					  ,ProductID			
					  ,GETDATE()									 
				FROM #Product
														
	      END 
		  
		  
		  
		 --Confirmation of Success.
		SELECT @Status = 0 
	COMMIT TRANSACTION
 END TRY
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_HcScanProduct.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
   select ERROR_PROCEDURE() 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'			
   ROLLBACK TRANSACTION;
   --Confirmation of failure.
   SELECT @Status = 1
  END;  
     
 END CATCH;  
END;
















































GO
