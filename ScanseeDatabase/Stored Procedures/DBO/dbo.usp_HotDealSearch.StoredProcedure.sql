USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HotDealSearch
Purpose					: To get the list of Hotdeals available.
Example					: usp_HotDealSearch 1, 'o', 0,50

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23rd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HotDealSearch]
(
	@UserID int
	--, @SortBy char(1)
	, @Search varchar(255)
	, @LowerLimit int
	, @UpperLimit int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		;WITH HotDeals
		AS
		(
			SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY C.ParentCategoryName, AP.APIPartnerName, HD.Price)
				, HD.ProductHotDealID 
				, HDP.ProductID 
				, HotDealName
				, C.CategoryID 
				, C.ParentCategoryName 
				, AP.APIPartnerID 
				, AP.APIPartnerName 
				, Price 
				, HotDealShortDescription 
				, HotDealImagePath 
			FROM ProductHotDeal HD
				INNER JOIN HotDealProduct HDP ON HD.ProductHotDealID = HDP.ProductHotDealID
				INNER JOIN ProductCategory PC ON PC.ProductID = HDP.ProductID 				
				INNER JOIN UserCategory UC ON UC.CategoryID = PC.CategoryID 
				INNER JOIN Category C ON C.CategoryID = UC.CategoryID 
				LEFT JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID 
			WHERE UC.UserID = @UserID 
				AND HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
				AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
				AND CONVERT(DATE, GETDATE()) BETWEEN CONVERT(DATE, HD.HotDealStartDate) AND CONVERT(DATE, HD.HotDealEndDate) 
			--ORDER BY CASE WHEN @SortBy = 'P' THEN CONVERT(varchar(100), HD.Price)
			--			  WHEN @SortBy = 'C' THEN 
			--		 END 
		) 
		SELECT Row_Num  rowNumber
			, ProductHotDealID hotDealId
			, ProductID 
			, HotDealName hotDealName
			, CategoryID 
			, ParentCategoryName categoryName
			, APIPartnerID apiPartnerId
			, APIPartnerName apiPartnerName
			, Price hDPrice
			, HotDealShortDescription hDshortDescription
			, HotDealImagePath hotDealImagePath		
		FROM HotDeals 
		WHERE Row_Num BETWEEN (@LowerLimit+1) AND (@UpperLimit)	
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
