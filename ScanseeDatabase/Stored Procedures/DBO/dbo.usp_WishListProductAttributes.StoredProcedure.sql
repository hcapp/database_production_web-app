USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListProductAttributes]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WishListProductAttributes  
Purpose     : To dispaly Product Attributes in Wish List  
Example     : usp_WishListProductAttributes  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   6th Oct 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WishListProductAttributes]  
(  
 @ProductID int  
   
 --Output Variable   
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
   --To get Media Server Configuration. 
		 DECLARE @ManufConfig varchar(50) 
		  
		 SELECT @ManufConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
  -- To capture the existance of the media for the specified product.  
            DECLARE @VideoFlag bit  
            DECLARE @AudioFlag bit  
            DECLARE @FileFlag bit  
            SELECT @VideoFlag = SUM(CASE WHEN PT.ProductMediaType = 'Video Files' THEN 1 ELSE 0 END)  
                  , @AudioFlag = SUM(CASE WHEN PT.ProductMediaType = 'Audio Files' THEN 1 ELSE 0 END)  
                  , @FileFlag = SUM(CASE WHEN PT.ProductMediaType = 'Other Files' THEN 1 ELSE 0 END)  
            FROM ProductMedia PM  
            INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
            WHERE PM.ProductID = @ProductID 
             
   IF @ProductID in (SELECT ProductID FROM ProductMedia P 
						INNER JOIN ProductMediaType PM on P.ProductMediaTypeID =PM.ProductMediaTypeID WHERE ProductMediaType='Image Files')
	   BEGIN
			   --To get Product Info       
			   SELECT P.ProductID   
				, P.ProductName   
				, p.ProductLongDescription   
				, P.ProductShortDescription
				, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																										THEN @ManufConfig
																										+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																										+ProductImagePath ELSE ProductImagePath 
																								  END   
									  ELSE ProductImagePath END          
				, WarrantyServiceInformation warrantyServiceInfo
				, ModelNumber
				, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																										THEN @ManufConfig
																										+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																										+ProductMediaPath  END
									  ELSE ProductMediaPath END                    
				--, PA.AttributeName  
				--, PA.DisplayValue  
				, VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
							, AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
							, FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END  
			    
			   FROM Product P  
			   INNER JOIN ProductMedia PM ON P.ProductID = PM.ProductID
			   INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID 
			   --LEFT JOIN ProductAttributes PA ON PA.ProductID = P.ProductID
			   --LEFT JOIN Attribute A ON A.AttributeId = PA.AttributeID  
			   WHERE P.ProductID  = @ProductID AND  ProductMediaType='Image Files'  
	  END
	   
	  ELSE
		BEGIN
				SELECT P.ProductID   
				, P.ProductName   
				, p.ProductLongDescription   
				, P.ProductShortDescription
				, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																										THEN @ManufConfig
																										+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																										+ProductImagePath ELSE ProductImagePath 
																								  END   
									  ELSE ProductImagePath END          
				, WarrantyServiceInformation warrantyServiceInfo
				, ModelNumber
				, NULL ProductMediaPath
				--, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
							--																			THEN @ManufConfig
							--																			+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
							--																			+ProductMediaPath  END
				--                      ELSE ProductMediaPath END                    
				--, PA.AttributeName  
				--, PA.DisplayValue  
				, VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
				, AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
				, FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END  
			    
			   FROM Product P  
			   --LEFt JOIN ProductMedia PM ON P.ProductID = PM.ProductID
			   --LEFT JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
			   --LEFT JOIN ProductAttributes PA ON PA.ProductID = P.ProductID
			   --LEFT JOIN Attribute A ON A.AttributeId = PA.AttributeID  
			   WHERE P.ProductID  = @ProductID 
		END
		
		 --To get Product Info       
			 --  SELECT P.ProductID   
				--, P.ProductName   
				--, p.ProductLongDescription   
				--, P.ProductShortDescription
				--, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
				--																						THEN @ManufConfig
				--																						+CONVERT(VARCHAR(30),ManufacturerID)+'/'
				--																						+ProductImagePath ELSE ProductImagePath 
				--																				  END   
				--					  ELSE ProductImagePath END          
				--, WarrantyServiceInformation warrantyServiceInfo
				--, ModelNumber
				--, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
				--																						THEN @ManufConfig
				--																						+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
				--																						+ProductMediaPath  END
				--					  ELSE ProductMediaPath END                    
				----, PA.AttributeName  
				----, PA.DisplayValue  
				--, VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
				--			, AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
				--			, FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END  
			    
			 --  FROM Product P  
			 --  LEFT JOIN ProductMedia PM ON P.ProductID = PM.ProductID
			 --  LEFT JOIN ProductAttributes PA ON PA.ProductID = P.ProductID
			 --  LEFT JOIN Attribute A ON A.AttributeId = PA.AttributeID  
			 --  WHERE P.ProductID  = @ProductID 
     
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure <>.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
     
  END;  
     
 END CATCH;  
END;


GO
