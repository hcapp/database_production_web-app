USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAddPage]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerAddPage
Purpose					: To Create a Retailer Page (Main Menu Page).
Example					: usp_WebRetailerAddPage

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			21st May 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAddPage]
(

	--Input Parameter(s)--
	 @RetailID int	
   , @RetailLocationID varchar(1000)
   , @PageTitle varchar(255)
   , @StartDate datetime
   , @EndDate datetime
   , @Image varchar(1000)
   , @PageDescription varchar(1000)
   , @ShortDescription varchar(255)
   , @LongDescription varchar(1000)  
   	
	--Output Variable--	  	 
	, @PageID int output
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @QRRetailerCustomPageID INT			
			 
			--Create a record in QRRetailerCustomPage
			 INSERT INTO QRRetailerCustomPage(
											  QRTypeID
											, RetailID
											, Pagetitle
											, StartDate
											, EndDate
											, [Image]
											, PageDescription
											, ShortDescription
											, LongDescription
											, DateCreated)
								VALUES(
										 (SELECT QRTypeID FROM QRTypes WHERE QRTypeName = 'Main Menu Page')
										, @RetailID
										, @PageTitle
										, @StartDate
										, @EndDate
										, @Image
										, @PageDescription
										, @ShortDescription
										, @LongDescription
										, GETDATE()
										)
						  --Capture the PageID			
						  SET @QRRetailerCustomPageID = SCOPE_IDENTITY()
						  
						  SET @PageID = @QRRetailerCustomPageID
				--Make Retailer Location Associations.
				INSERT INTO QRRetailerCustomPageAssociation(
															  QRRetailerCustomPageID
															, RetailID
															, RetailLocationID
															, DateCreated)
												SELECT @QRRetailerCustomPageID
													 , @RetailID
													 , [Param]
													 , GETDATE()
												FROM dbo.fn_SplitParam(@RetailLocationID, ',')					 
		
		--Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAddPage.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
