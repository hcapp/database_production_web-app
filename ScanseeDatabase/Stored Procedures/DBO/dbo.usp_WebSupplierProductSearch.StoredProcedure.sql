USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebProductSearch
Purpose					: To Search for the Product placed by the Supplier to allow Rebates.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			15th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierProductSearch]
(

	--Input Input Parameter(s)--  
	   
	   @ManufacturerID int 
	 , @ProductName varchar(255)--search can be on productname or UPC.	
	 , @LowerLimit int	
	
	--Output Variable--
	  
	 , @NextPageFlag bit output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
	--To get Media Server Configuration.
	 DECLARE @Config varchar(50)  
	 SELECT @Config=ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	 
		DECLARE @LATITUDE FLOAT
		DECLARE @LONGITUDE FLOAT
		DECLARE @MaxCnt INT
		
	 --To get the row count for pagination.  
	 DECLARE @UpperLimit int   
	 DECLARE @ScreenContent Varchar(100)
	 SELECT @ScreenContent = ScreenContent   
	 FROM AppConfiguration   
	 WHERE ScreenName = 'All' 
		AND ConfigurationType = 'Website Pagination'
		AND Active = 1   
	
	SELECT @UpperLimit = @LowerLimit + @ScreenContent
 		
			
		
	SELECT RowNum = ROW_NUMBER() OVER (ORDER BY ProductID ASC)
	     , ProductID
	     , ProductName
	     , ScanCode
	     , ProductShortDescription
	     , ProductLongDescription
	     , ProductImagePath
	     , SuggestedRetailPrice price
	     , WarrantyServiceInformation
	   
	 INTO #TEMP
	 FROM 
	       	
	 (SELECT  DISTINCT P.ProductID
	         , P.ProductName
	         , P.ScanCode
	         , P.ProductShortDescription
	         , P.ProductLongDescription
	         , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config +  CONVERT(VARCHAR(30),P.ManufacturerID) +'/'+ ProductImagePath
										ELSE ProductImagePath END
	         , P.SuggestedRetailPrice 
	         , P.WarrantyServiceInformation	         
   FROM  Product P 
   INNER JOIN RetailLocationProduct RLP ON RLP.ProductID = P.ProductID
   INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID
   WHERE P.ProductID <> 0 AND Active = 1
   AND (P.ProductName LIKE (CASE WHEN @ProductName IS NOT NULL THEN '%'+@ProductName+'%' ELSE '%' END)
			OR  ScanCode LIKE @ProductName + '%')
   AND P.ManufacturerID = @ManufacturerID
   AND (P.ProductExpirationDate IS NULL OR P.ProductExpirationDate >= GETDATE())) Products								--CHECK IF THE PRODUCT IS EXPIRED
   
	SELECT @MaxCnt = MAX(RowNum) FROM #TEMP
	SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END   --CHECK IF THERE ARE SOME MORE RECORDS
	
	SELECT  RowNum = ROW_NUMBER() OVER (ORDER BY ProductID ASC)
	      , ProductID
	      , ProductName
	      , ScanCode
	      , ProductShortDescription
	      , ProductLongDescription
	      , ProductImagePath as productImagePath
	      , price
	      , WarrantyServiceInformation
	  	 
	FROM #TEMP	
	WHERE RowNum BETWEEN @LowerLimit AND @UpperLimit                                 --DISPLAY WITHIN THE LIMITS SPECIFIED
	 
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebProductSearch.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
