USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MainMenuDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MainMenuDisplay
Purpose					: To display Main menu based on the media viewed.
Example					: EXEC usp_MainMenuDisplay 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MainMenuDisplay]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		--Module list based on videos viewed by user
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='App Media Server Configuration'
		print @config
		
		SELECT M.ModuleID mediaId
			, M.ModuleName moduleName
			, M.ModuleDescription moduleDescription
			, ME.MediaID mediaId
			, @Config+ME.MediaVideoPath mediaPath
			, @config+ME.MediaImagePath menuImagePath  
			, ME.MediaName mediaName
			, VideoViewed = CASE WHEN UM.MediaID IS NOT NULL THEN 1 ELSE 0 END
		FROM Module M
			LEFT JOIN Media ME ON ME.ModuleID = M.ModuleID 
			LEFT JOIN UserMedia UM ON UM.MediaID = ME.MediaID AND UserID = @UserID 
		ORDER BY ModuleSort
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MainMenuDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
