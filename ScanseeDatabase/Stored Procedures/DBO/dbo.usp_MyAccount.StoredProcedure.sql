USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MyAccount]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MyAccount
Purpose					: To get user's account details
Example					: EXEC usp_MyAccount 1

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MyAccount]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
	
)
AS
BEGIN

	BEGIN TRY
		SELECT Curr.UserID 
			, Coupons_Curr  couponsCur
			, Rebates_Cur rebateCur
			, FieldAgent_Cur  fieldAgentCur
			, LoyaltyProgram_Cur loyaltyProgramCur
			, Coupons_Tot  couponsTot
			, Rebates_Tot  rebateTot
			, FieldAgent_Tot fieldAgentTot 
			, LoyaltyProgram_Tot	loyaltyProgramTot
			, RedeemSavingURL = (SELECT ScreenName FROM AppConfiguration WHERE ConfigurationType = 'Redeem Savings' AND Active = 1)		
		FROM
		 (SELECT UserID
			, Coupons_Curr = SUM(CASE WHEN CouponID IS NOT NULL THEN PaymentAmount END)
			, Rebates_Cur = SUM(CASE WHEN RebateID IS NOT NULL THEN PaymentAmount END)
			, FieldAgent_Cur = SUM(CASE WHEN FieldRequestID IS NOT NULL THEN PaymentAmount END)
			, LoyaltyProgram_Cur = SUM(CASE WHEN LoyaltyProgramID IS NOT NULL THEN PaymentAmount END)
		 FROM UserPayout
		 WHERE UserID = @UserID
		 GROUP BY UserID) Curr
		 FULL OUTER JOIN 
		 (SELECT UserID
			, Coupons_Tot = SUM(CASE WHEN CouponID IS NOT NULL THEN PaymentAmount END)
			, Rebates_Tot = SUM(CASE WHEN RebateID IS NOT NULL THEN PaymentAmount END)
			, FieldAgent_Tot = SUM(CASE WHEN FieldRequestID IS NOT NULL THEN PaymentAmount END)
			, LoyaltyProgram_Tot = SUM(CASE WHEN LoyaltyProgramID IS NOT NULL THEN PaymentAmount END)
		 FROM UserPayHistory  
		 WHERE UserID = @UserID 
		 GROUP BY UserID)Tot
		 ON CURR.UserID = TOT.UserID 
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MyAccount.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
