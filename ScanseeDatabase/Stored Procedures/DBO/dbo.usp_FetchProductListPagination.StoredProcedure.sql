USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FetchProductListPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_FetchProductListPagination  
Purpose     : To list Products of a category   
Example     : EXEC usp_FetchProductListPagination 35, 2, 0, 50  
History  
Version  Date  Author   Change Description  
---------------------------------------------------------------   
1.0   8/9/2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_FetchProductListPagination]  
(  
   @UserID int  
 , @RetailLocationID int  
 --, @CategoryID int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @MaxCnt int output
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
   --To get Media Server Configuration.  
  DECLARE @ManufConfig varchar(50)    
  SELECT @ManufConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
   
   --To get the row count for pagination.  
   DECLARE @UpperLimit int   
   SELECT @UpperLimit = @LowerLimit + ScreenContent   
   FROM AppConfiguration   
   WHERE ScreenName = @ScreenName   
    AND ConfigurationType = 'Pagination'  
    AND Active = 1  
   --DECLARE @MaxCnt int  
     
   --To get RetailId  
   DECLARE @RetailID INT  
   SELECT @RetailID = RetailID   
   FROM RetailLocation   
   WHERE RetailLocationID = @RetailLocationID   
     
   --To get list of Product available in the Retail Store.  
   SELECT  RetailLocationID   
      , ProductID   
   INTO #PROD  
   FROM RetailLocationProduct   
   WHERE RetailLocationID = @Retaillocationid  
     
   --Table Variable to store sale Product.   
   DECLARE @DiscountProduct TABLE (ProductId int, SalePrice money, DiscountAmount varchar(100),DiscountType varchar(10))  
     
   --To get Product on Deal.  
   INSERT INTO @DiscountProduct  
   SELECT RD.ProductID, rd.SalePrice,NULL ,'Deal'  
   FROM RetailLocationDeal RD   
   INNER JOIN #PROD P ON P.ProductID = RD.ProductID   
   WHERE RD.RetailLocationID = @RetailLocationID  
   AND GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
     
       
   --To get Products which has Coupons on it.  
   INSERT INTO @DiscountProduct  
   SELECT CP.ProductID, c.CouponDiscountAmount,C.CouponDiscountPct ,'Coupon'  
   FROM Coupon C  
   INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID  
   INNER JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
   inner join #PROD p on p.ProductID = CP.ProductID   
   WHERE CR.RetailID = @RetailID     
      AND GETDATE() BETWEEN C.CouponStartDate AND C.CouponExpireDate  
     
   --To get Products which has Rebate on it.  
   INSERT INTO @DiscountProduct  
   SELECT RP.ProductID, R.RebateAmount,NULL ,'Rebate'  
   FROM Rebate R  
   INNER JOIN RebateProduct RP on RP.RebateID = R.RebateID   
   INNER JOIN #PROD P ON P.ProductID = RP.ProductID   
   WHERE R.RetailID = @RetailID   
   AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate   
     
  
   --To get Products which has Loyalty on it.  
   INSERT INTO @DiscountProduct  
   select L.ProductID, L.LoyaltyDealDiscountAmount,L.LoyaltyDealDiscountPct ,'Loyalty'  
   from LoyaltyDeal L   
   INNER JOIN #PROD P ON P.ProductID = L.ProductID   
   WHERE L.RetailLocationID = @Retaillocationid    
   AND GETDATE() BETWEEN L.LoyaltyDealStartDate AND L.LoyaltyDealExpireDate  
     
  
   --To get Hot Deal Products.  
   INSERT INTO @DiscountProduct  
   SELECT H.ProductID, ph.SalePrice,PH.HotDealDiscountPct,'Hot Deals'  
   from HotDealProduct h   
   INNER JOIN ProductHotDealRetailLocation PR ON PR.ProductHotDealID=h.ProductHotDealID  
   INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = H.ProductHotDealID   
   INNER JOIN #PROD P ON P.ProductID = H.ProductID   
   WHERE PR.RetailLocationID = @RetailLocationID   
   AND GETDATE() BETWEEN PH.HotDealStartDate AND PH.HotDealEndDate   
     
     
   --To get the MIN sale price of a product.  
   SELECT ProductID, MIN(SalePrice) AS SalePrice  
   INTO #SaleProd  
   FROM @DiscountProduct P  
   GROUP BY ProductId  
     
   --And to check whether highest sale price product belongs to Deals. if yes enabling the flag, to get the prod image from RetailLocationDeal table  
   SELECT DP.ProductId, DP.SalePrice,DP.DiscountAmount ,Flag = 1  
   INTO #Deal  
   FROM @DiscountProduct DP  
   INNER JOIN #SaleProd P ON DP.ProductId = P.ProductId AND DP.SalePrice = P.SalePrice   
   WHERE DP.DiscountType = 'Deal'  
     
   SELECT ProductId, SalePrice,DiscountAmount ,Flag  
   INTO #Product  
   FROM ( --To get highest deal product  
     SELECT ProductId, SalePrice,DiscountAmount ,Flag   
     FROM #Deal   
     UNION ALL   
     --To get highest non deal (ie coupon, hotdeals etc) product    
     SELECT S.ProductId, S.SalePrice,D.DiscountAmount ,Flag = 0  
     FROM #SaleProd S  
     INNER JOIN @DiscountProduct D ON D.ProductId=S.ProductId AND D.SalePrice=S.SalePrice  
     WHERE S.ProductId NOT IN (SELECT ProductID FROM #Deal)   
    ) Details  
       
     
     
     
   --;WITH ProductList  
   --AS  
   --(  
    SELECT DISTINCT P.ProductID, Row_Num = IDENTITY(INT,1,1)  
     , P.ProductName   
     , P.ManufacturerID    
     , P.ModelNumber   
     , P.ProductLongDescription   
     , P.ProductExpirationDate   
     , ProductImagePath =CASE WHEN D.ThumbNailProductImagePath IS NULL THEN P.ProductImagePath ELSE D.ThumbNailProductImagePath END  
     , P.SuggestedRetailPrice   
     , P.Weight   
     , P.WeightUnits  
     , PR.SalePrice   
     , RL.Price  
     , DiscountAmount
     --, @RetailID RetailID   
       
    INTO #ProductList    
    FROM #Product PR  
     --INNER JOIN (  
     --   select MIN(productid) productid,  
     --   productname,ModelNumber, ProductLongDescription,  
     --   ManufacturerID    
     --   , ProductExpirationDate  
     --   , ProductImagePath  = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
					--																		THEN @ManufConfig
					--																		+CONVERT(VARCHAR(30),ManufacturerID)+'/'
					--																		+ProductImagePath ELSE ProductImagePath 
					--																  END   
     --                     ELSE ProductImagePath END  
     --   , SuggestedRetailPrice   
     --   , Weight   
     --   , WeightUnits   
     --   , ScanTypeID  
     --   from Product   
     --   group by ProductName,ScanCode,ModelNumber          
     --   , ProductLongDescription   
     --   , ManufacturerID    
     --   , ProductExpirationDate   
     --   , ProductImagePath   
     --   , SuggestedRetailPrice   
     --   , Weight   
     --   , WeightUnits   
     --   , ScanTypeID 
     --   , WebsiteSourceFlag           
     --   )   
     INNER JOIN Product P ON P.ProductID = PR.ProductID  
     LEFT JOIN RetailLocationDeal D ON D.ProductID = P.ProductID AND PR.Flag = 1 AND RetailLocationID=@RetailLocationID  
     LEFT JOIN RetailLocationProduct RL ON RL.ProductID=p.ProductID AND RL.RetailLocationID=@RetailLocationID      
    WHERE (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
       
       
   --)  
   --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #ProductList  
    
   print @MaxCnt  
   --this flag is a indicator to enable "More" button in the UI.   
   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
     
   SELECT Row_Num  rowNumber  
     , ProductID productId  
     , ProductName  productName  
     , ManufName  manufacturersName  
     , ModelNumber modelNumber  
     , ProductLongDescription productDescription  
     , ProductExpirationDate productExpDate  
     , ProductImagePath imagePath  
     , SuggestedRetailPrice suggestedRetailPrice  
     , Weight productWeight  
     , WeightUnits weightUnits  
     , SalePrice salePrice  
     , Price regularPrice  
     , DiscountAmount discount
     --, RetailID   
       
   FROM #ProductList PL  
    LEFT JOIN Manufacturer M ON M.ManufacturerID = PL.ManufacturerID   
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit    
   
   ----;WITH ProductList  
   ----AS  
   ----(  
   -- SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY P.ProductName)  
   --  , P.ProductID   
   --  , P.ProductName   
   --  , P.ManufacturerID    
   --  , P.ModelNumber   
   --  , P.ProductLongDescription   
   --  , P.ProductExpirationDate   
   --  , P.ProductImagePath   
   --  , P.SuggestedRetailPrice   
   --  , P.Weight   
   --  , P.WeightUnits  
   --  , RP.SalePrice  
   --  , RP.SaleStartDate   
   --  , RP.SaleEndDate    
   -- INTO #ProductList    
   -- FROM ProductCategory PC   
   --  INNER JOIN Product P ON P.ProductID = PC.ProductID  
   --  INNER JOIN RetailLocationProduct RP ON RP.ProductID = P.ProductID   
   --           AND RP.RetailLocationID = @RetailLocationID   
   -- WHERE PC.CategoryID  = @CategoryID  
   --  AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
   --  AND ISNULL(RP.SalePrice, 0) <> 0  
   --  AND GETDATE() BETWEEN RP.SaleStartDate AND RP.SaleEndDate   
   ----)  
   ----To capture max row number.  
   --SELECT @MaxCnt = MAX(Row_Num) FROM #ProductList  
     
   ----this flag is a indicator to enable "More" button in the UI.   
   ----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
   --SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
     
   --SELECT Row_Num  rowNumber  
   --  , ProductID productId  
   --  , ProductName  productName  
   --  , ManufName  manufacturersName  
   --  , ModelNumber modelNumber  
   --  , ProductLongDescription productDescription  
   --  , ProductExpirationDate productExpDate  
   --  , ProductImagePath imagePath  
   --  , SuggestedRetailPrice suggestedRetailPrice  
   --  , Weight productWeight  
   --  , WeightUnits weightUnits  
   --  , SalePrice salePrice  
   --  , SaleStartDate   
   --  , SaleEndDate   
   --FROM #ProductList PL  
   -- INNER JOIN Manufacturer M ON M.ManufacturerID = PL.ManufacturerID   
   --WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit     
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_FetchProductListPagination.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
