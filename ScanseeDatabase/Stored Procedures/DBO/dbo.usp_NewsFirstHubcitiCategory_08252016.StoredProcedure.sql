USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_NewsFirstHubcitiCategory_08252016]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_NewsFirstHubcitiCategory


create Proc [dbo].[usp_NewsFirstHubcitiCategory_08252016]

as

Begin


select distinct HcHubCitiID,HubCitiName,b.NewsCategoryID,b.NewsCategoryName,'https://www.austintexas.gov/department/news/341/rss.xml' as URL    from HcHubCiti a,NewsCategory b 
-- where a.HubCitiName='HubCiti'
where a.Active=1 and b.Active=1   --and RssFeedNews=1

end

GO
