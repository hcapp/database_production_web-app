USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCouponInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebCouponInsertion
Purpose					: To create a new coupon
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th November 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebCouponInsertion]
(

	--Input Input Parameter(s)--		
	  
	  --Input Variables for Coupon Table
	  @UserID int 
	, @CouponName varchar(100)	 
	, @CouponDiscountType varchar(20)
	, @CouponDiscountAmount money
	, @CouponDiscountPct float
	, @CouponShortDescription varchar(200)
	, @CouponLongDescription varchar(1000)	
	, @CouponStartDate datetime
	, @CouponExpireDate datetime
	, @CouponURL varchar(1000)
	, @ExternalCoupon bit
	, @ViewableOnWeb bit
	
	  --Input Variables for CouponProduct Table Table
	, @ProductIds varchar(max)
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @CouponID INT
			DECLARE @ProductID VARCHAR(100)=''
			
			--Insert into Coupon Table
			
			INSERT INTO Coupon (							
								
								CouponName,	
								CouponDiscountType ,
								CouponDiscountAmount, 
								CouponDiscountPct ,
								CouponShortDescription ,
								CouponLongDescription ,							
								CouponDateAdded,
								CouponStartDate ,
								CouponExpireDate ,
								CouponURL ,
								ExternalCoupon ,
								WebsiteSourceFlag,
								ViewableOnWeb,
								CreatedUserID)
						 
						 VALUES(								
							
								@CouponName,
								@CouponDiscountType,
								@CouponDiscountAmount,
								@CouponDiscountPct,
								@CouponShortDescription,
								@CouponLongDescription,							
								GETDATE(),
								@CouponStartDate,
								@CouponExpireDate,
								case when @CouponURL = '' then null else @CouponURL end,
								@ExternalCoupon,
								1,
								@ViewableOnWeb,
								@UserID)
								
				SET @CouponID = SCOPE_IDENTITY();	
				
				--Check if the ProductScanCoded are passed.
				IF(@ProductIds IS NOT NULL AND @ProductIds NOT LIKE '')
				BEGIN						
						--Insert into CouponProduct Table
					
						INSERT INTO CouponProduct(CouponID
												 ,ProductID
												 ,DateAdded)
					                    
										  SELECT @CouponID
											   , Param 
											   , GETDATE()
										  FROM dbo.fn_SplitParam(@ProductIds, ',') P						 
			   END		
				
				
				--Register the Push Notification for all the users who have added the product to their Wish Lists.
					SELECT DISTINCT UserID
						  ,ProductID
						  ,DeviceID
						  ,CouponID  	              
					INTO #Temp
					FROM (
					SELECT  U.UserID
						  ,UP.ProductID
						  ,UD.DeviceID 
						  ,CD.CouponID   
						  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
						  ,LocaleRadius 
					from fn_SplitParam(@ProductIDs,',') F
					INNER JOIN CouponProduct CD ON CD.ProductID =F.Param AND CD.CouponID =@CouponID         
					INNER JOIN CouponRetailer CR ON CR.CouponID =CD.CouponID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =CR.RetailLocationID AND RL.Active = 1  
					INNER JOIN UserProduct UP ON UP.ProductID =CD.ProductID AND WishListItem =1	
					INNER JOIN Users U ON U.UserID =UP.UserID 
					INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
					INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
					)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
					
					--User pushnotification already exist then NotificationSent flag updating to 0
					UPDATE UserPushNotification SET NotificationSent=0
												   ,DateModified =GETDATE()		
												   ,DiscountCreated = 1	                                      
					FROM UserPushNotification UP
					INNER JOIN #Temp T ON UP.CouponID=T.CouponID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=1
			       
		           --User pushnotification not exist then insert. 
				   INSERT INTO UserPushNotification(UserID
													,ProductID
													,DeviceID												
													,CouponID 												
													,NotificationSent
													, DiscountCreated
													,DateCreated)
					SELECT T.UserID 
						  ,T.ProductID 
						  ,T.DeviceID 
						  ,T.CouponID  
						  ,0
						  ,1
						  ,GETDATE()
					FROM #Temp T				
					LEFT JOIN UserPushNotification UP ON UP.UserID = T.UserID AND T.CouponID = UP.CouponID AND T.ProductID = UP.ProductID AND DiscountCreated = 1
					WHERE UP.UserPushNotificationID IS NULL  
			 
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebCouponInsertion.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
