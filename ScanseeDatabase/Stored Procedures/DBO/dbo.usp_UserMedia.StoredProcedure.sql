USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserMedia]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserMedia
Purpose					: To capture User viewed Media info.
Example					: usp_UserMedia

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserMedia]
(
	@UserID int
	, @MediaID int
	, @Date datetime
	
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			IF NOT EXISTS (SELECT 1 FROM UserMedia WHERE UserID = @UserID AND MediaID = @MediaID)
			BEGIN
				INSERT INTO [UserMedia]
					   ([UserID]
					   ,[MediaID]
					   ,[DateViewed])
				 VALUES
					   (@UserID
						, @MediaID 
						, @Date)
			END
			
			--Confirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserMedia.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
