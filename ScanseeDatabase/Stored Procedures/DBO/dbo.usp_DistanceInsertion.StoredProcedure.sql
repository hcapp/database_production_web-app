USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_DistanceInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_DistanceInsertion]
Purpose					: To insert the distances and compute miles.
Example					: [usp_DistanceInsertion] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			02 April 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_DistanceInsertion]
(
	  
	  @DistanceValue DECIMAL(13, 2)
	, @UnitID INT
	
	--Output Variable
		 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
				INSERT INTO Distance(DistanceValue 
								   , UnitTypeID
								   , DistanceInMiles)
							VALUES(@DistanceValue
							     , @UnitID
							     , CASE WHEN @UnitID = 1 THEN 0.000568181818 * @DistanceValue 
							            ELSE @DistanceValue END)		
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_DistanceInsertion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
