USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_TutorialMediaDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [dbo].[usp_TutorialMediaDisplay]
Purpose					: To display Media information related to tutorials.
Example					: [dbo].[usp_TutorialMediaDisplay]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10 Dec 2015		Sagar Byali		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_TutorialMediaDisplay]
(	
		 
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

	        DECLARE @Config VARCHAR(1000)
			SELECT @Config = ScreenContent from AppConfiguration
			WHERE ConfigurationType = 'Configuration of server'
	     

		  --To display the Tutorial media based on Mediatype
		  SELECT IDENTITY(int, 1, 1) AS RowNum 
				,MTI.MediaTypeID 
				,MT.MediaTypeName tutorialType
				,@Config + 'Images/' + VideoPath videoPath
				,@Config + 'Images/' + ImagePath poster
		  INTO #Temp
		  FROM MediaType MT
		  INNER JOIN MediaTypeInformation MTI ON MT.MediaTypeID = MTI.MediaTypeID


		  SELECT RowNum tutorialId
				,tutorialType
				,videoPath
				,poster 
		  FROM #Temp
						   
		  SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [dbo].[usp_TutorialMediaDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;




GO
