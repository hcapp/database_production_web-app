USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SalesAppRetrieveRetailerBusinessCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SalesAppRetrieveRetailerBusinessCategory
Purpose					: To retrieve list of Categories from the system for Registration..
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			18th December 2012				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SalesAppRetrieveRetailerBusinessCategory]

--Output variables
	@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			SELECT DISTINCT BusinessCategoryID cateId
						   ,BusinessCategoryName cateName
			FROM BusinessCategory
			WHERE Active = 1	
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SalesAppRetrieveRetailerBusinessCategory.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
