USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserLogin]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserLogin
Purpose					: To Validate User Login.
Example					: usp_UserLogin

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th Aug 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserLogin]
(
	  @UserName varchar(100)
	, @Password char(60)
	
	--Output Variable 
	, @UserID int output
	, @Login int output
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @CNT int
			
		BEGIN TRANSACTION			
			--To validate user login
			SELECT @UserID = UserID 
			FROM [Users] WITH(NOLOCK)
			WHERE BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName)  
					AND BINARY_CHECKSUM(Password) = BINARY_CHECKSUM( @Password)
					AND FaceBookAuthenticatedUser=0
					
					
			--If Login is success, then capture user login.		
			IF ISNULL(@UserID, 0) <> 0
			BEGIN
				SELECT @UserID
				-- To capture User Login.
				INSERT INTO [UserFirstUseLogin]
					   ([UserID]
					   ,[FirstUseLoginDate])
				 VALUES
					   (@UserID 
					   ,GETDATE())
			END
			ELSE
			BEGIN
				SELECT @Login = -1
			END
			
			IF EXISTS (SELECT TOP 1 UserID  FROM Users WHERE UserID = @UserID AND FirstUseComplete = 0)
			BEGIN
				SELECT @Result = CASE WHEN (COUNT(UserID)) % 5 = 0 THEN -1 ELSE 0 END
				FROM UserFirstUseLogin 
				WHERE UserID = @UserID
			END
			-- BELOW CODED TO HAVE DEFAULT VALUE OF FIRSTUSE STATUS
			ELSE
			BEGIN
			SELECT @Result =0
			END
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserLogin.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
