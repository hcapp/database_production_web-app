USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserInfoDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserInfoDisplay
Purpose					: To fetch User Information.
Example					: usp_UserInfoDisplay  2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserInfoDisplay]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		 SELECT U.UserID
			,FirstName firstName
			,Lastname lastName
			,Email Email
			,addressCaption = (SELECT LTRIM(RTRIM(ScreenContent)) 
								FROM AppConfiguration 
								WHERE ConfigurationType = 'Address Caption' AND ScreenName = 'Address Caption' AND Active = 1)
			,ScreenContent=(SELECT LTRIM(RTRIM(ScreenContent)) 
							FROM AppConfiguration
							WHERE ScreenName='User Information' AND Active=1)
			,Address1 address1
			,Address2 address2
			,Address3 address3
			,Address4 address4
			,City city
			,U.State state
			,PostalCode postalCode
			,MobilePhone mobileNumber
			,CountryID selectedCountryID
			,Gender
			,DOB dob
			,IncomeRangeID selectedIncomeRangeID
			,EducationLevelID educationLevelId
			,HomeOwner homeOwner
			,NumberOfChildren numberOfChildren
			,MaritalStatusID maritalStatus
			,Uni.UniversityName
			,Uni.State univStateName
		 FROM Users U
			LEFT OUTER JOIN UserDemographic UD ON UD.UserID = U.UserID 
			LEFT OUTER JOIN UserUniversity UU ON UU.UserID = U.UserID
			LEFT OUTER JOIN University Uni ON Uni.UniversityID = UU.UniversityID
		WHERE U.UserID = @UserID 
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserInfoDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
