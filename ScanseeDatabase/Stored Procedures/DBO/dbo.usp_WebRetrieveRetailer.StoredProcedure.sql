USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveRetailer]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: 
Purpose					: To retrieve list of Retailer from the system.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			23rd December 2011				Satish Teli		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveRetailer]

--Output variables
@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		Select Top 5000 RetailID, RetailName from Retailer 
		Where RetailerActive = 1
		Order by RetailName 
			
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveRetailer.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
