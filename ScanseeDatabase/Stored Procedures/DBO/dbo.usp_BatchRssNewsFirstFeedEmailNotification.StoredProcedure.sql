USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchRssNewsFirstFeedEmailNotification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchRssNewsFirstFeedEmailNotification
Purpose					: To send email notification if categories are without any news in it. 
Example					: usp_BatchRssNewsFirstFeedPushNotification 

Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15 July 2015	Mohith H R		Initial Version
---------------------------------------------------------------


*/

--exec [dbo].[usp_BatchRssNewsFirstFeedEmailNotification] 8,null,null,null

CREATE PROCEDURE [dbo].[usp_BatchRssNewsFirstFeedEmailNotification]
(
	  @HcHubCitiID varchar(10)

	--Output Variable 		
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			BEGIN

			SELECT  DISTINCT NewsCategoryName  INTO #TEMP  FROM NewsFirstCategory  WITH(NOLOCK)
				EXCEPT
				SELECT DISTINCT FSC.NewsCategoryName  FROM NewsFirstCategory FSC  WITH(NOLOCK)
				INNER JOIN RssNewsFirstFeedNewsStagingTable FN  WITH(NOLOCK) ON FSC.NewsCategoryName = FN.NewsType
				LEFT JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = FSC.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID
				--WHERE FN.HcHubCitiID = @HcHubCitiID

			; WITH AA AS(
				
				SELECT HC.HubCitiName as HubCitiName ,NewsType AS feedType,COUNT(*)  as newscount,Message 
				FROM NewsFirstCategory FSC
				INNER JOIN RssNewsFirstFeedNewsStagingTable FN ON FSC.NewsCategoryName = FN.NewsType  --WHERE HcHubCitiID = @HcHubCitiID
				INNER JOIN HcHubCiti HC ON HC.HcHubCitiID=FN.HcHubCitiID AND  TITLE IS NOT NULL
				LEFT JOIN NewsFirstCatSubCatAssociation A ON A.CategoryID = FSC.NewsCategoryID AND A.hchubcitiID = @HcHubCitiID
				GROUP BY  NewsType,HC.HubCitiName ,Message
				
				UNION

				SELECT  NULL,NewsCategoryName,0,'Currently no news for '+ NewsCategoryName +' category.' 
				FROM #TEMP)


				SELECT *  FROM AA ORDER  BY  newscount DESC

			--; with aa as(
			--	SELECT DISTINCT FN.HcHubCitiID as HubCitiID
			--				   ,FSC.NewsCategoryName feedType,
			--				    IIF(Message IS NULL, RssNewsFirstFeedNewsID,0) as newscount
			--				   ,IIF(Message IS NULL,'Currently no news for '+ FSC.NewsCategoryName +' category.',Message) Message
			--	--INTO #NoNews
			--	FROM NewsFirstCategory FSC
			--	LEFT JOIN RssNewsFirstFeedNewsStagingTable FN ON FSC.NewsCategoryName = FN.NewsType
			--	WHERE FN.HcHubCitiID = @HcHubCitiID AND (FN.RssNewsFirstFeedNewsID IS NULL
			--	OR Message IS NOT NULL))
			--	select  HubCitiID,feedType,count(distinct newscount) as newscount,Message  from aa
			--	group  by HubCitiID,feedType,Message
				
				--SELECT * 
				--FROM #NoNews
				
						
				--Confirmation of Success.
				SELECT @Status = 0
			END
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchRssNewsFirstFeedEmailNotification'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
