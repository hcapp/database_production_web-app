USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssNewsFirstFeedCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssNewsFirstFeedCategoryDisplay
Purpose					: 
Example					: usp_WebRssNewsFirstFeedCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebRssNewsFirstFeedCategoryDisplay]
(	
	
	  @HcHubCitiID int

	--Output Variable 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		  DECLARE @RockwallDefaultImage varchar(2000)

		  SELECT @RockwallDefaultImage = ScreenContent
		  FROM AppConfiguration
		  WHERE (ConfigurationType = 'RssNewsFirstFeedTylerDefaultImage' AND @HcHubCitiID = 10) 
		  OR (ConfigurationType = 'RssNewsFirstFeedRockwallDefaultImage')

		  SELECT CategoryName
			    ,IIF(ImagePath IS NULL OR ImagePath LIKE '',@RockwallDefaultImage,ImagePath) ImagePath
				,DateCreated
		  FROM RssNewsFirstFeedCategory
		  WHERE HcHubCitiID = @HcHubCitiID
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssNewsFirstFeedCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;





GO
