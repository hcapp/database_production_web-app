USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_TodayList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_TodayList
Purpose					: To retrive Today Shopping List products.
Example					:  usp_TodayList 2

History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			6th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_TodayList]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	--To ftech Today's shopping lists Product details which was added thro Shopping list Search Screen.
		SELECT UP.UserProductID 
			, UP.UserID
			, 0 AS retailerId
			, 'Others' AS retailerName 
			, C.CategoryID 
			, C.ParentCategoryID 
			, C.ParentCategoryName 
			, C.SubCategoryID 
			, C.SubCategoryName           
			, ISNULL(P.ProductID, 0) ProductID
			, ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END
			, Coupon.CouponID 
			, Loyalty.LoyaltyDealID
			, Rebate.RebateID
		INTO #TSL
		FROM UserProduct UP
			LEFT JOIN Product P ON P.ProductID = UP.ProductID
			LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			LEFT JOIN Category C ON C.CategoryID = PC.CategoryID   
			--LEFT JOIN Retailer R ON R.RetailID = UP.UserRetailPreferenceID
			OUTER APPLY (SELECT C.CouponID FROM Coupon C INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID WHERE CP.ProductID = UP.ProductID AND CouponExpireDate >= GETDATE()) Coupon
		    OUTER APPLY (SELECT LoyaltyDealID FROM LoyaltyDeal WHERE ProductID = UP.ProductID AND LoyaltyDealExpireDate >= GETDATE())Loyalty
		    OUTER APPLY (SELECT R.RebateID FROM RebateProduct RP INNER JOIN Rebate R ON R.RebateID = RP.RebateID  WHERE ProductID = UP.ProductID AND GETDATE() <=R.RebateEndDate)Rebate
		WHERE UserID = @UserID 
			AND TodayListtItem = 1
			--AND MasterListItem = 1
			AND UP.UserRetailPreferenceID IS NULL
			AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE()) 
			
		--To fetch shopping list product details which has Retailer binded with it.
		SELECT UP.UserProductID 
			, UP.UserID
			, ISNULL(R.RetailID, 0) retailerId
			, ISNULL(R.RetailName, 'Others') retailerName  
			, C.CategoryID 
			, C.ParentCategoryID 
			, C.ParentCategoryName 
			, C.SubCategoryID 
			, C.SubCategoryName          
			, ISNULL(P.ProductID, 0) ProductID
			, ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END
			, Coupon.Coupon 
			, Loyalty.Loyalty
			, Rebate.Rebate 
		INTO #RTSL
		FROM UserProduct UP
			LEFT JOIN Product P ON P.ProductID = UP.ProductID 
			LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			LEFT JOIN Category C ON C.CategoryID = PC.CategoryID  
			LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID 
			LEFT JOIN Retailer R ON R.RetailID = UR.RetailID  
			OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon
			OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty
			OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate
		WHERE UP.UserID = @UserID 
			AND TodayListtItem = 1
			--AND MasterListItem = 1
			AND UP.UserRetailPreferenceID IS NOT NULL
			AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE()) 
			
			
		SELECT UserProductID 
			, D.UserID
			, retailerId
			, retailerName 
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName          
			, D.ProductID
			, ProductName 
			, CLRFlag = CASE WHEN COUNT(D.CouponID)+COUNT(D.LoyaltyDealID)+COUNT(D.RebateID) > 0 THEN 1 ELSE 0 END 
			--, Coupon = CASE WHEN COUNT(D.CouponID) = 0 THEN 0 ELSE 1 END
			--, Loyalty = CASE WHEN COUNT(D.LoyaltyDealID) = 0 THEN 0 ELSE 1 END
			--, Rebate = CASE WHEN COUNT(D.RebateID) = 0 THEN 0 ELSE 1 END
			, Coupon_Status = CASE WHEN COUNT(D.CouponID) = 0 THEN 'Grey'
								   ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END
							  END
			, Loyalty_Status = CASE WHEN COUNT(D.LoyaltyDealID) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END
							   END 
			, Rebate_Status = CASE WHEN COUNT(D.RebateID) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UR.RebateID) = 0 THEN 'Red' ELSE 'Green' END
							   END 
		FROM #TSL D
			LEFT JOIN UserCouponGallery UC ON UC.UserID = D.UserID AND UC.CouponID = D.CouponID
			LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = D.UserID AND UL.LoyaltyDealID = D.LoyaltyDealID	
			LEFT JOIN UserRebateGallery UR ON UR.UserID = D.UserID AND UR.RebateID = D.RebateID
		GROUP BY UserProductID 
			, D.UserID
			, retailerId
			, retailerName  
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName         
			, D.ProductID
			, ProductName 
		UNION ALL 
		SELECT UserProductID 
			, R.UserID
			, retailerId
			, retailerName 
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName          
			, R.ProductID
			, ProductName 
			, CLRFlag = CASE WHEN COUNT(R.Coupon)+COUNT(R.Loyalty)+COUNT(R.Rebate) > 0 THEN 1 ELSE 0 END
			--, Coupon = CASE WHEN COUNT(R.Coupon) = 0 THEN 0 ELSE 1 END
			--, Loyalty = CASE WHEN COUNT(R.Loyalty) = 0 THEN 0 ELSE 1 END
			--, Rebate = CASE WHEN COUNT(R.Rebate) = 0 THEN 0 ELSE 1 END
			, Coupon_Status = CASE WHEN COUNT(R.Coupon) = 0 THEN 'Grey'
								   ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END
							  END
			, Loyalty_Status = CASE WHEN COUNT(R.Loyalty) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END
							   END 
			, Rebate_Status = CASE WHEN COUNT(R.Rebate) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UR.RebateID) = 0 THEN 'Red' ELSE 'Green' END
							   END 
		FROM #RTSL R
			LEFT JOIN UserCouponGallery UC ON UC.UserID = R.UserID AND UC.CouponID = R.Coupon
			LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = R.UserID AND UL.LoyaltyDealID = R.Loyalty	
			LEFT JOIN UserRebateGallery UR ON UR.UserID = R.UserID AND UR.RebateID = R.Rebate 
		GROUP BY UserProductID 
			, R.UserID
			, retailerId
			, retailerName 
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName          
			, R.ProductID
			, ProductName 
			
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_TodayList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
