USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandBannerAdDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandBannerAdDeletion
Purpose					: To delete a Banner Ad.
Example					: usp_WebBandBannerAdDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04/18/2016	Prakash C	    Initial Version(usp_WebRetailerBannerAdDeletion)
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandBannerAdDeletion]
(
	--Input Variables
	  @RetailID int
	, @BannerAdID int
	
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 DECLARE @ADStartDate date
			 DECLARE @ADEndDate date
			 
			 SELECT @ADStartDate = StartDate
				  , @ADEndDate = EndDate
			 FROM BandAdvertisementBanner
			 WHERE BandAdvertisementBannerID = @BannerAdID
			 
			 --Confirm if the Ad is inactive before deletion.
			 IF NOT EXISTS(SELECT 1 FROM BandAdvertisementBanner WHERE BandAdvertisementBannerID = @BannerAdID AND GETDATE() BETWEEN StartDate AND EndDate)
			 BEGIN
				--Move the Ad to archive.
				INSERT INTO BandAdvertisementBannerArchive(AdvertisementBannerID
													 , BannerAdName
													 , BannerAdDescription
													 , BannerAdImagePath
													 , BannerAdURL
													 , StartDate
													 , EndDate
													 , WebsiteSourceFlag
													 , DateCreated)
										  SELECT BandAdvertisementBannerID
													 , BannerAdName
													 , BannerAdDescription
													 , BannerAdImagePath
													 , BannerAdURL
													 , StartDate
													 , EndDate
													 , WebsiteSourceFlag
													 , GETDATE()
										  FROM BandAdvertisementBanner
										  WHERE BandAdvertisementBannerID = @BannerAdID
					
					--Move the Band Location association to the Archive.					  
					INSERT INTO BandLocationBannerAdArchive(BandLocationID
													 , AdvertisementBannerID
													 , DateCreated)
												SELECT BandLocationID
													 , AdvertisementBannerID
													 , GETDATE()
												FROM BandLocationBannerAd
												WHERE AdvertisementBannerID = @BannerAdID
												
				END	
					--Delete the child table.
					DELETE FROM BandLocationBannerAd WHERE AdvertisementBannerID = @BannerAdID
					--Delete from the master table.
					DELETE FROM BandAdvertisementBanner WHERE BandAdvertisementBannerID = @BannerAdID												
			 
			
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandBannerAdDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
               values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
