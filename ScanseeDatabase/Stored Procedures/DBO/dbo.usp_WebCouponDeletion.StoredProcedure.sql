USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCouponDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebCouponDeletion
Purpose					: To Delete an Existing Coupon Record.
Example					: usp_WebCouponDeletion

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th November 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebCouponDeletion]
(

	--Input Parameter(s)--
		
	  @CouponID int	
	
	--Output Variable--
	  
	, @ReferenceFlag int output           --Reference flag tells if the record is being referenced in any other table
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DELETE FROM CouponProduct WHERE CouponID = 	@CouponID		--DELETE ASSOCIATED RECORD(S) FROM CouponProduct TABLE
			
			DELETE FROM Coupon WHERE CouponID = @CouponID               --DELETE RECORD FROM Coupon TABLE
		
		--Confirmation of Success.
		    SELECT @ReferenceFlag = 0
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.		
	   IF @@ERROR <> 0
			BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebCouponDeletion.'		--Error No. 547 corresponds to the foreign key constraint. ie the recored is referenced in some othre table
			 --Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.			
			SELECT @ReferenceFlag = CASE WHEN ERROR_NUMBER() = 547 THEN 1 ELSE 0 END
			SELECT @Status = CASE WHEN ERROR_NUMBER() = 547 THEN 0 END			
		END;	
		 
	END CATCH;
END;


GO
