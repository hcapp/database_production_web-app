USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEventsDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchEventsDataPorting]
Purpose					: To move data from Events Stage table to HcEvents Table
Example					: [usp_BatchEventsDataPorting]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEventsDataPorting]
(
    --Input Variable
	  @FileName VARCHAR(255)
	  
	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
		DECLARE @Duplicates INT
		DECLARE @Mandatory INT
		DECLARE @Expired INT
		DECLARE @StateLength INT
		DECLARE @RowCount INT
					   	    
		--Insert into Batch Log table			
		INSERT INTO HcEventsBatchLog(ProcessedFileName
									 , ExecutionDate
									 , TotalRowCount
									 , ExistingRecordCount
									 )
							SELECT @FileName
									, GETDATE()
									, COUNT(1)
									, 0
							FROM HcEventsStagingData
				
			--Delete the duplicate records in the stage area before moving to production.		
			;WITH CTE 
			(
				HubCitiName
				,EventName
				,ShortDescription
				,LongDescription
				,ImagePath
				,StartDate
				,EndDate
				,DateCreated
				,DateModified
				,MoreInformationURL
				,CategoryName
				,Address
				,City
				,State
				,PostalCode
				,Latitude
				,Longitude
				,EventLocationTitle
				,EventListingImagePath
				,RWNO
			)
			AS
			(
				SELECT HubCitiName
						,EventName
						,ShortDescription
						,LongDescription
						,ImagePath
						,StartDate
						,EndDate
						,DateCreated
						,DateModified
						,MoreInformationURL
						,CategoryName
						,Address
						,City
						,State
						,PostalCode
						,Latitude
						,Longitude
						,EventLocationTitle
						,EventListingImagePath
				      ,ROW_NUMBER() OVER (PARTITION BY HubCitiName,EventName,ShortDescription,ImagePath,StartDate,EndDate ORDER BY HubCitiName) AS RWNO 
				 FROM HcEventsStagingData	
			) 
			DELETE FROM CTE WHERE RWNO > 1						
			SELECT @Duplicates = @@ROWCOUNT 
						
			--Deleting expired events from stage table.			
			SELECT *
			INTO #Expired 
			FROM HcEventsStagingData WHERE GETDATE()> ISNULL(EndDate,GETDATE()+1)
			SELECT @Expired  = @@ROWCOUNT 
			
			--Delete events if mandatory fields are not provided. 
			DELETE FROM HcEventsStagingData WHERE HubCitiName IS NULL OR EventName IS NULL OR ShortDescription IS NULL OR ImagePath IS NULL OR StartDate IS NULL 
							OR CategoryName IS NULL OR Address IS NULL OR City IS NULL OR State IS NULL OR PostalCode IS NULL OR EventListingImagePath IS NULL
			SELECT @Mandatory = @@ROWCOUNT 
			
			--Delete events if State Length > 2, from Stage Table.
			DELETE FROM HcEventsStagingData WHERE LEN(State)>2
			SELECT @StateLength = @@ROWCOUNT			

			--Remove extra space
			UPDATE HcEventsStagingData SET EventName = LTRIM(RTRIM(EventName))
										, HubCitiName = LTRIM(RTRIM(HubCitiName))
										, ImagePath = LTRIM(RTRIM(ImagePath))
										, ShortDescription = LTRIM(RTRIM(ShortDescription))
										, MoreInformationURL = LTRIM(RTRIM(MoreInformationURL))
										, Address = LTRIM(RTRIM(Address))
										, EventListingImagePath = LTRIM(RTRIM(EventListingImagePath))

			--Insert into Events table
			INSERT INTO HcEvents(HcEventName
								, ShortDescription
								, LongDescription
								, ImagePath
								, BussinessEvent
								, PackageEvent
								, HcHubCitiID
								, StartDate
								, EndDate
								, DateCreated
								, CreatedUserID
								, MoreInformationURL
								, OnGoingEvent
								, Active
								, EventListingImagePath
								)	
						SELECT S.EventName
							  , S.ShortDescription
							  , S.LongDescription
							  , S.ImagePath
							  , 0
							  , 0
							  , H.HcHubCitiID
							  , S.StartDate
							  , S.EndDate
							  , GETDATE()
							  ,	31899
							  , S.MoreInformationURL
							  , 0
							  , 1
							  , EventListingImagePath
						FROM HcEventsStagingData S
						INNER JOIN HcHubCiti H ON S.HubCitiName = H.HubCitiName
						INNER JOIN HcEventsCategory C ON S.CategoryName = C.HcEventCategoryName
						WHERE S.[FileName] = @FileName
			
			--Associate events to event categories
			INSERT INTO HcEventsCategoryAssociation(HcEventCategoryID 
													, HcEventID 
													, HcHubCitiID
													, DateCreated
													, CreatedUserID
													)
											SELECT C.HcEventCategoryID
													, E.HcEventID
													, E.HcHubCitiID
													, GETDATE()
													, 31899
											FROM HcEventsStagingData S
											INNER JOIN HcEvents E ON S.EventName = E.HcEventName AND S.ShortDescription = E.ShortDescription
											AND S.ImagePath = E.ImagePath AND S.StartDate = E.StartDate AND CONVERT(DATE,S.DateCreated) = CONVERT(DATE,E.DateCreated)
											INNER JOIN HcEventsCategory C ON S.CategoryName = C.HcEventCategoryName 	
											WHERE S.[FileName] = @FileName

		    --Insert Event Details to Event Location Table
			INSERT INTO HcEventLocation(HcEventID
										, HcHubCitiID
										, Address
										, City
										, State
										, PostalCode
										, Latitude
										, Longuitude
										, GeoErrorFlag
										, DateCreated
										, CreatedUserID
										, EventLocationTitle)
								SELECT E.HcEventID
										, E.HcHubCitiID
										, S.Address
										, S.City
										, S.State
										, S.PostalCode
										, S.Latitude
										, S.Longitude
										, IIF(S.Latitude IS NOT NULL AND S.Longitude IS NOT NULL, 1, 0)
										, GETDATE()
										, 31899
										, S.EventLocationTitle
								FROM HcEventsStagingData S
								INNER JOIN HcEvents E ON S.EventName = E.HcEventName AND S.ShortDescription = E.ShortDescription
								AND S.ImagePath = E.ImagePath AND S.StartDate = E.StartDate AND CONVERT(DATE,S.DateCreated) = CONVERT(DATE,E.DateCreated)
								WHERE S.[FileName] = @FileName
			  

			   	--Capture the number of records that got inserted into Events table from the current file.
				SELECT @RowCount = @@ROWCOUNT 						
				
				--Update the log with the counts.
				UPDATE HcEventsBatchLog 
				SET DuplicatesCount = @Duplicates
				  , ExpiredCount = @Expired
				  , MandatoryFieldsMissingCount = @Mandatory
				  , InvalidRecordsCount = @StateLength
				  ,	ProcessedRowCount = @RowCount
				WHERE ProcessedFileName = @FileName
				AND CONVERT(DATE, GETDATE()) = CONVERT(DATE, ExecutionDate) 
				
	       
		   --Confirmation of Success.
		   SELECT @Status = 0
		   
		   COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		SELECT ERROR_MESSAGE()
		 
		PRINT 'Error occured in Stored Procedure [usp_BatchEventsDataPorting].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
		PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
		ROLLBACK TRANSACTION;
		--Confirmation of failure.
		SELECT @Status = 1
	END;
		 
	END CATCH;
END;



GO
