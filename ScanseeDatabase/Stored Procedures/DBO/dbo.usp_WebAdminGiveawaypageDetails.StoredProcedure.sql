USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminGiveawaypageDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAdminGiveawaypageDetails
Purpose					: To Display GiveawayPage Details. 
Example					: usp_WebAdminGiveawaypageDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6 May 2013	    Span   	        Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminGiveawaypageDetails]
(
	  
	--Input Variable
	  @QRRetailerCustomPageID int
	    
	
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Config varchar(50)
		 
		SELECT @Config = ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		--To Display GiveAwayPage Details. 
		SELECT  Pagetitle
			   ,@Config + CAST(RetailID AS VARCHAR(10)) + '/' + [Image] AS [Image]
			   ,[Terms and Conditions]
			   ,Rules
			   ,ShortDescription
			   ,LongDescription
			   ,StartDate
			   ,EndDate
			   ,Quantity
		FROM QRRetailerCustomPage
		WHERE QRRetailerCustomPageID = @QRRetailerCustomPageID		 
					
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebAdminGiveawaypageDetails>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
