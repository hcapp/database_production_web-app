USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsInsertion_backup]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsInsertion]
Purpose					: To insert into RssFeedNews from Staging table.
Example					: [usp_WebRssFeedNewsInsertion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015		Mohith H R	        1.1
1.1         07 Dec 2015     Suganya C           Duplicate news removal
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedNewsInsertion_backup]
(
	 -- @HcHubCitiID int,

	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN
		 --Truncate RssFeedNews table before inserting new news.
		 --TRUNCATE TABLE RssFeedNews

		 --DELETE FROM RssFeedNews
		 --FROM RssFeedNews RF
		 --INNER JOIN RssFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		 --WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	 
			--OR (RS.Title IS NULL AND RS.NewsType = 'Classifields'))

		--Deleting 30 days older News for 'spanqa.regionsapp'.
		DECLARE @ExpiryPublishedDateRockwall datetime
		SET @ExpiryPublishedDateRockwall = GETDATE()-30

		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateRockwall AND HcHubCitiID = 28 

		--Deleting 5 days older News for 'Killeen Daily Herald'.
		
		DECLARE @ExpiryPublishedDateKilleen datetime
		SET @ExpiryPublishedDateKilleen = GETDATE()-5

		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate  < @ExpiryPublishedDateKilleen AND HcHubCitiID = 52  
		
		 --Fetching unique, latest data from RssFeedNewsStagingTable to insert into RssFeedNews
	
		 SELECT DISTINCT HcHubCitiID, NewsType, Title,Classification, MAX(RssFeedNewsID) AS MaxRssFeedNewsID
         INTO #Temp
         FROM RssFeedNewsStagingTable
		 --WHERE NewsType NOT IN (SELECT NewsType FROM RssFeedNews)
         GROUP BY HcHubCitiID,NewsType, Title, Classification
		
		 --Insert into RssFeedNews
		 INSERT INTO RssFeedNews(Title
								,ImagePath
								,ShortDescription
								,LongDescription
								,Link
								,PublishedDate
								,NewsType
								,Message
								,HcHubCitiID
								,Classification
								,Section
								,AdCopy)															
						SELECT  RS.Title
								,RS.ImagePath
								,RS.ShortDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy		
						FROM RssFeedNewsStagingTable RS 
						INNER JOIN #Temp T ON RS.HcHubCitiID = T.HcHubCitiID 
						AND RS.NewsType = T.NewsType 
						AND ((RS.NewsType <> 'Classifields' AND RS.Title = T.Title) OR (RS.NewsType = 'Classifields' AND 1=1))
						AND RS.RssFeedNewsID = T.MaxRssFeedNewsID 
						--AND CONVERT(VARCHAR(12), CONVERT(datetime2,RS.PublishedDate), 107) = T.MaxPublishedDate
						GROUP BY RS.Title
								,RS.ImagePath
								,RS.ShortDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy	
						ORDER BY MIN(RS.RssFeedNewsID) ASC

		DECLARE @LatestTime datetime
		SELECT @LatestTime = MAX(DateCreated)
		FROM RssFeedNews
						
		 DELETE FROM RssFeedNews
		 FROM RssFeedNews RF
		 INNER JOIN RssFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		 WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	 
			OR (RS.Title IS NULL AND RS.NewsType = 'Classifields'))
		 AND RF.DateCreated <> @LatestTime
				
				   
		  SET @Status=0	
		  	 COMMIT TRAN 				   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRssFeedNewsInsertion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;





GO
