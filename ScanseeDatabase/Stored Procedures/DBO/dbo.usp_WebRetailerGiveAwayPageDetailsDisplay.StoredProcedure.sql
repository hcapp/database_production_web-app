USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerGiveAwayPageDetailsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebGiveAwayPageCreation1
Purpose					: To Create a retailer Give Away Page
Example					: usp_WebGiveAwayPageCreation1

History
Version		Date						Author				Change Description
------------------------------------------------------------------------------- 
1.0			29th April 2013				Dhananjaya TR        		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerGiveAwayPageDetailsDisplay]
(

	--Input Parameter(s)--
	  @RetailID int	
	, @PageID int  
   	
	--Output Variable--	  	 
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			 
			 DECLARE @Config varchar(50)
			 
			 SELECT @Config=ScreenContent    
		     FROM AppConfiguration     
		     WHERE ConfigurationType='Web Retailer Media Server Configuration'  
			 
			 DECLARE @QRTypeID int
			 SELECT @QRTypeID=QRTypeID FROM QRTypes 
			 WHERE QRTypeName = 'Give Away Page'
		 
			--Create a record in QRRetailerCustomPage
			 SELECT Pagetitle retPageTitle
				, StartDate
				, EndDate
				, ImagePath = (CASE WHEN [Image] IS NOT NULL THEN @Config + CAST(@RetailId AS VARCHAR(10)) + '/' + [Image] ELSE NULL END)  											
				, imageName = [Image]
				, ShortDescription
				, LongDescription
				, DateCreated
				, Rules [rule]
				, [Terms and Conditions]
				, Quantity
			FROM QRRetailerCustomPage
			WHERE RetailID =@RetailID
			AND QRRetailerCustomPageID =@PageID 

		
		--Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAddPage.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
