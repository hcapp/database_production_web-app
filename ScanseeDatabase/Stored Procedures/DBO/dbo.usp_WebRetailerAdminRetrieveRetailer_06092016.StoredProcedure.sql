USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetrieveRetailer_06092016]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetrieveRetailer
Purpose					: To retrieve list of Retailers from the system.
Example					: usp_WebRetailerAdminRetrieveRetailer

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			16th July 2013			SPAN		Initial Version
-------------------------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebRetailerAdminRetrieveRetailer_06092016]

	--Input parameters	 
	  @RetailName VARCHAR(100)
	, @City VARCHAR(50)
	, @State CHAR(2)
	, @LowerLimit int  
    , @ScreenName varchar(50)

	--Output variables
	, @MaxCnt INT OUTPUT
	, @NxtPageFlag BIT OUTPUT 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
	  BEGIN TRANSACTION		
		
		--To get the row count for pagination.  
		DECLARE @UpperLimit int   
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
		FROM AppConfiguration   
		WHERE ScreenName = 'RetailAdmin' 
		AND ConfigurationType = 'Pagination'
		AND Active = 1
		
		IF @City =''
		BEGIN
		SET @City=NULL
		END
		
		IF @State  =''
		BEGIN
		SET @State =NULL
		END
		
		IF @RetailName  =''
		BEGIN
		SET @RetailName=NULL
		END
		
		CREATE TABLE #RetailerList1(RetailID INT
							      ,RetailName VARCHAR(500)
							      ,NumberOfLocations INT
							      ,Address1 VARCHAR(500)
							      ,[State] CHAR(2)							      
							      ,City VARCHAR(50)
							      ,PostalCode VARCHAR(10)
								  ,RetailURL VARCHAR(1000)
								  ,Latitude FLOAT
								  ,Longitude FLOAT
								  ,CorporatePhoneNo VARCHAR(10)
								  ,UserLoginFlag Bit
								  ,IsPaid Bit)
		
		IF (@RetailName IS NULL AND @City IS NULL AND @State IS NULL) 
			BEGIN 
				INSERT INTO #RetailerList1(RetailID
										,RetailName							      
										,NumberOfLocations
										,Address1
										,State							      
										,City
										,PostalCode
										,RetailURL
										,Latitude
										,Longitude
										,CorporatePhoneNo
										,UserLoginFlag
										,IsPaid)
				SELECT	DISTINCT	R.RetailID     
						   ,RetailName
						   ,NumberOfLocations = (SELECT COUNT(1) FROM RetailLocation RL1 WHERE R.RetailID = RL1.RetailID AND Headquarters = 0 AND Active = 1)
						   ,R.Address1
						   ,R.[State]
						   ,R.City	
						   ,R.PostalCode	
						   ,R.RetailURL
						   ,RetailLocationLatitude
						   ,RetailLocationLongitude
						   ,R.CorporatePhoneNo	
						   ,UserLoginFlag=IIF(UR.RetailID IS NOT NULL AND RL.RetailLocationID IS NOT NULL,1,0)	
						   ,IsPaid				   					
				FROM Retailer R
				LEFT JOIN UserRetailer UR ON UR.RetailID =R.RetailID 
				LEFT JOIN RetailLocation RL ON R.RetailID=RL.RetailID AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)	AND RL.Active = 1	
				LEFT JOIN RetailerBillingDetails RB ON R.RetailID = RB.RetailerID			
				WHERE R.RetailerActive = 1 --AND RL.Active = 1
				GROUP BY R.RetailID,RetailName,NumberOfLocations,R.Address1,R.[State],R.City,R.PostalCode,R.RetailURL,RL.RetailLocationLatitude,RL.RetailLocationLongitude,R.CorporatePhoneNo,RL.Headquarters, CorporateAndStore, RL.RetailID	,UR.RetailID			
				,RL.RetailLocationID,IsPaid
				ORDER BY RetailName	
			END		
		ELSE
			BEGIN		
				INSERT INTO #RetailerList1(RetailID
										,RetailName							      
										,NumberOfLocations
										,Address1
										,[State]
										,City
										,PostalCode
										,RetailURL
										,Latitude
										,Longitude
										,CorporatePhoneNo
										,UserLoginFlag
										,IsPaid)										
					SELECT DISTINCT  R.RetailID
						   ,RetailName
						   ,NumberOfLocations = (SELECT COUNT(1) FROM RetailLocation RL1 WHERE R.RetailID = RL1.RetailID AND Headquarters = 0 AND Active = 1)
						   ,R.Address1
						   ,R.[State]
						   ,R.City	
						   ,R.PostalCode
						   ,R.RetailURL
						   ,RetailLocationLatitude
						   ,RetailLocationLongitude
						   ,R.CorporatePhoneNo
						   ,UserLoginFlag=IIF(UR.RetailID IS NOT NULL AND RL.RetailLocationID IS NOT NULL,1,0)
						   ,IsPaid							   						
				FROM Retailer R
				LEFT JOIN RetailLocation RL ON R.RetailID=RL.RetailID AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)	AND RL.Active = 1
				LEFT JOIN UserRetailer UR ON UR.RetailID =R.RetailID 
				LEFT JOIN RetailerBillingDetails RB ON R.RetailID = RB.RetailerID
				WHERE R.RetailerActive = 1 AND ((@RetailName IS NOT NULL AND RetailName LIKE '%'+@RetailName+'%' AND @State IS NOT NULL AND R.[State] = @State AND @City IS NOT NULL AND R.City = @City)
				OR (@RetailName IS NOT NULL AND RetailName LIKE '%'+@RetailName+'%' AND @State IS NOT NULL AND R.[State] = @State AND @City IS NULL)
				OR (@RetailName IS NULL AND @State IS NOT NULL AND R.[State] = @State AND @City IS NULL)
				OR (@RetailName IS NOT NULL AND RetailName LIKE '%'+@RetailName+'%' AND @State IS NULL AND @City IS NULL)
				OR (@RetailName IS NULL AND @State IS NOT NULL AND R.[State] = @State AND @City IS NOT NULL AND R.City = @City)) 
				GROUP BY R.RetailID,RetailName,R.Address1,R.[State],R.City,R.PostalCode,R.RetailURL,RL.RetailLocationLatitude,RL.RetailLocationLongitude,R.CorporatePhoneNo,RL.Headquarters, CorporateAndStore, RL.RetailID	,UR.RetailID	
				,RL.RetailLocationID,IsPaid
				ORDER BY RetailName					
									
			END

			SELECT DISTINCT RetailID	
						   ,RetailName
						   ,NumberOfLocations
						   ,Address1
						   ,[State]
						   ,City
						   ,PostalCode
						   ,RetailURL retUrl
						   ,Latitude latitude
						   ,Longitude longitude
						   ,CorporatePhoneNo phoneNo
						   ,UserLoginFlag isLoginExist	
						   ,IsPaid isPaid
			 INTO #LoginExist	  		   
			 FROM #RetailerList1								    
			 WHERE UserLoginFlag =1


			
			 DELETE FROM #RetailerList1
			 FROM #RetailerList1 L
			 INNER JOIN #LoginExist LE ON LE.RetailName=L.RetailName AND L.Address1 =LE.Address1  AND LE.City =L.City AND L.City IS NOT NULL AND LE.State =L.State AND LE.PostalCode =L.PostalCode AND L.RetailID <>LE.RetailID AND L.UserLoginFlag =0 

			
			 SELECT DISTINCT Row_Num=identity(int, 1,1)
						   ,RetailID	
						   ,RetailName
						   ,NumberOfLocations
						   ,Address1
						   ,[State]
						   ,City
						   ,PostalCode
						   ,RetailURL retUrl
						   ,Latitude latitude
						   ,Longitude longitude
						   ,CorporatePhoneNo phoneNo
						   ,UserLoginFlag isLoginExist	
						   ,IsPaid isPaid
			 INTO #RetailerList	  		   
			 FROM #RetailerList1 L
			 ORDER BY RetailName
			
					
		 --To capture max row number. 		 
		 SELECT @MaxCnt = MAX(Row_Num) FROM #RetailerList
 
		 --This flag is a indicator to enable "More" button in the UI.   
		 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 		 		

		 SELECT Row_Num row_Num
			   ,R.RetailID	
			   ,RetailName
			   ,NumberOfLocations
			   ,R.Address1
			   ,R.[State]
			   ,R.City
			   ,R.PostalCode
			   ,retUrl
			   ,Latitude latitude
			   ,Longitude longitude
			   ,phoneNo
			   ,isLoginExist	
			   ,IIF(isPaid IS NULL,0,isPaid) isPaid		  		   
		 FROM #RetailerList	R
		 --INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID							    
		 WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit --AND RL.Active = 1
		 ORDER BY RetailName 
		

		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetrieveRetailer.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
