USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerShoppingListHistoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerShoppingListHistoryDisplay   
Purpose               : To display Shopping Lish History products of a user  
Example               : usp_WebConsumerShoppingListHistoryDisplay  
  
History  
Version  Date                Author        Change        Description  
---------------------------------------------------------------   
1.0      29thJuly2013       Dhananjaya TR  Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerShoppingListHistoryDisplay]  
(  
  @UserID int  
 ,@LowerLimit int   
 ,@RecordCount Int
 --Output Variable  
 , @MaxCnt int output  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
      --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
  --To get the row count for pagination.  
	  DECLARE @UpperLimit int  		           
	  SELECT @UpperLimit = @LowerLimit + @RecordCount 
    
     
     
  SELECT DISTINCT UP.UserID ,Row_Num= IDENTITY(INT,1,1)  
   , UP.UserProductID  
   , UP.ProductID productId  
   , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
   , ProductLongDescription
   , ProductShortDescription productShortDescription
   , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END 
   , P.SuggestedRetailPrice price   
   --if a item already exists in Favorite, flag is enabled.  
   , StarFlag = CASE WHEN UP.MasterListItem = 1 THEN 1 ELSE 0 END  
   , C.ParentCategoryID   
   , ParentCategoryName = CASE WHEN C.CategoryID IS NULL THEN 'Unassigned Products' ELSE C.ParentCategoryName END   
   , C.CategoryID 
   , FavProductFlag=CASE WHEN MasterListItem=1 AND P.ProductExpirationDate>=GETDATE() THEN 1 ELSE 0 END 
  INTO #History  
  FROM UserShoppingCartHistoryProduct SH  
  INNER JOIN UserProduct UP ON UP.UserProductID = SH.UserProductID  
  LEFT JOIN Product P ON P.ProductID = UP.ProductID    
  LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID   
  LEFT JOIN Category C ON C.CategoryID = PC.CategoryID   
  WHERE UserID = @USERID AND ISNULL(P.ProductExpirationDate,GETDATE()+1) >=GETDATE()
  ORDER BY CASE WHEN C.CategoryID IS NULL THEN 'Unassigned Products' ELSE C.ParentCategoryName END  
     ,CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END   
    
  --To capture max row number.  
  SELECT @MaxCnt = MAX(Row_Num) FROM #History  
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
  SELECT UserID   
   , Row_Num rowNum  
   , UserProductID  
   , ProductID productId  
   , ProductName   
   , ProductLongDescription
   , ProductShortDescription
   , ProductImagePath  
   , price   
   , StarFlag   
   , ParentCategoryID   
   , ParentCategoryName   
   , CategoryID  
   , FavProductFlag 
  FROM #History  
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
  Order by Row_Num  
    
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerShoppingListHistoryDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC dbo.[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
