USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandCustomPageDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebBandCustomPageDeletion]
Purpose					: To Update the Retailer Custom Page. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24th July 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandCustomPageDeletion]
(
	   
	  @RetailID int
    , @PageID int   
	
	--Output Variable 
	, @FindClearCacheURL VARCHAR(500) OUTPUT	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		   --Delete the Location Association.
		   DELETE FROM QRBandCustomPageAssociation 
		   WHERE QRBandCustomPageAssociationID = @PageID
		   
		   --Delete the Media Association.
		   DELETE FROM QRBandCustomPageMedia 
		   WHERE QRBandCustomPageID = @PageID
		   
		   --Delete the Custom Page.
		   DELETE FROM QRBandCustomPage 
		   WHERE QRBandCustomPageID = @PageID										
			 
			-------Find Clear Cache URL---03/03/2015--------

				-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		  ------------------------------------------------	    
				


			-----------------------------------------------

			 --Confirmation of Success.
			 SET @Status = 0		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCustomPageDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
