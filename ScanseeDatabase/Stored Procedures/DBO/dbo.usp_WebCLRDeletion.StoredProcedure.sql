USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCLRDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebCLRDeletion
Purpose					: To Delete an Existing Coupon Record.
Example					: usp_WebCLRDeletion

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th November 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebCLRDeletion]
(

	--Input Parameter(s)--
	
	 @CouponID int	
    ,@LoyaltyID int
    ,@RebateID int
	
	--Output Variable--
	
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			DECLARE @StartDate DateTime
			DECLARE @EndDate DateTime
			
			IF (@CouponID <> 0 AND @LoyaltyID = 0 AND @RebateID = 0)
			BEGIN
				SELECT @StartDate=CouponStartDate  FROM Coupon WHERE CouponID =@CouponID
				SELECT @EndDate=CouponExpireDate  FROM Coupon WHERE CouponID =@CouponID
					
				--Coupon will Expired manually then Inserting into CouponHistory Table.
				
				IF(@StartDate<>'01-01-1900' AND @EndDate<>'01-01-1900' )
				BEGIN
				INSERT INTO CouponHistory (						
											CouponID,	
											CouponStartDate,
											CouponExpireDate 
										 )
								 VALUES(								
									
											@CouponID,
											@StartDate,
											@EndDate
										)
								
				
					UPDATE Coupon set CouponStartDate ='01-01-1900',CouponExpireDate ='01-01-1900' WHERE CouponID=@CouponID 		
				END
			END
			

			IF (@RebateID <> 0 AND @CouponID = 0  AND @LoyaltyID = 0)
			BEGIN
				SELECT @StartDate=RebateStartDate FROM Rebate WHERE RebateID =@RebateID
				SELECT @EndDate=RebateEndDate FROM Rebate WHERE RebateID =@RebateID
					
				--Coupon will Expired manually then Inserting into CouponHistory Table.
				
				IF(@StartDate<>'01-01-1900' AND @EndDate<>'01-01-1900' )
				BEGIN
				INSERT INTO RebateHistory (						
											RebateID,	
											RebateStartDate,
											RebateEndDate  
										 )
								 VALUES(								
										
											@RebateID,
											@StartDate,
											@EndDate
										)
								
				
					UPDATE Rebate set RebateStartDate ='01-01-1900',RebateEndDate ='01-01-1900' WHERE RebateID=@RebateID 		
				END
			END
			
			--Confirmation of Success.
			SET @Status = 0

		COMMIT TRAN
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.		
	   IF @@ERROR <> 0
			BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebCLRDeletion.'		 
			 --Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.	
			SELECT @Status = 1			
		END;	
		 
	END CATCH;
END;


GO
