USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAttributeDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAttributeDisplay
Purpose					: 
Example					: usp_WebAttributeDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAttributeDisplay]
(

	--Output Variable 
	 @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		SELECT AttributeID 
				,Attributename
				,DateCreated
		INTO #Attribute
		FROM Attribute
		WHERE Active=1 AND AttributeName<>'Others'
		Order BY AttributeName
		
		SELECT AttributeID prodAttributesID
				,Attributename prodAttributeName
				,DateCreated
		FROM #Attribute
		UNION ALL
		SELECT AttributeID
				,Attributename
				,DateCreated
		FROM Attribute
		WHERE Active=1 AND AttributeName='Others'
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
