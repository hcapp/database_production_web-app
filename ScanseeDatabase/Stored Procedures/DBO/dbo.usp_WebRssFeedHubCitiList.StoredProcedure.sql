USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedHubCitiList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssFeedHubCitiList
Purpose					: To display RssFeedNews related HubCities.
Example					: usp_WebRssFeedHubCitiList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20 July 2015	Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedHubCitiList]
(	
		 
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		set transaction   isolation level read uncommitted

		  SELECT HcHubCitiID
				,HubCitiName
		  FROM HcHubCiti
		  WHERE RssFeedNews = 1
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedHubCitiList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;




GO
