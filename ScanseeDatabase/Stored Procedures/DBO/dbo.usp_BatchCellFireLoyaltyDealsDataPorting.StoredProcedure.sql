USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireLoyaltyDealsDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireLoyaltyDealsDataPorting
Purpose					: To move Loyalty Deals from stage to production.
Example					: usp_BatchCellFireLoyaltyDealsDataPorting

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			23rd October 2012   Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireLoyaltyDealsDataPorting]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
				
				--Update if the Deal exists in the system else insert.
				MERGE INTO LoyaltyDeal AS T
				USING (SELECT DISTINCT  R.RetailID
							, E.LoyaltyProgramID
							, A.CouponID
							, A.CouponName
							, A.DisplayBeforeDate
							, A.ExpireDate
							, A.CategoryName
							, A.Description
							, A.FaceValue
							, A.LongDescription
							, D.OfferType
						FROM APICellFireCouponDetails A
						INNER JOIN APICellFireCouponByUPC B ON A.CouponID = B.CouponID
						INNER JOIN APICellFireMerchants C ON C.MerchantID = B.MerchantID
						INNER JOIN Retailer R ON R.CellFireMerchantID = C.MerchantID
						INNER JOIN APICellFireOffers D ON D.OfferID = B.OfferID
						INNER JOIN LoyaltyProgram E ON E.RetailID = R.RetailID AND LTRIM(RTRIM(E.LoyaltyProgramName)) = LTRIM(RTRIM(C.CardName))) AS S					 
				 ON T.CellFireCouponID = S.CouponID
				 WHEN MATCHED THEN
					UPDATE SET T.LoyaltyDealName = S.CouponName
							 , T.LoyaltyDealDiscountAmount = S.FaceValue
							 , LoyaltyDealDescription = Description
							 , LoyaltyDealExpireDate  = DisplayBeforeDate
							 , RedemtionExpiryDate = ExpireDate
							 , Category = CategoryName
							 , LoyaltyDealLongDescription = LongDescription
							 , LoyaltyDealModifyDate = GETDATE()
				WHEN NOT MATCHED THEN
					INSERT(LoyaltyProgramID
						 , APIPartnerID
						 , LoyaltyDealName
						 , LoyaltyDealDiscountType
						 , LoyaltyDealDiscountAmount
						 , LoyaltyDealDescription
						 , LoyaltyDealLongDescription
						 , LoyaltyDealExpireDate
						 , RedemtionExpiryDate
						 , CellFireCouponID
						 , Category
						 , LoyaltyDealDateAdded)
					VALUES( LoyaltyProgramID
						  , (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName = 'CellFire')
						  , CouponName
						  , OfferType
						  , FaceValue
						  , Description
						  , LongDescription
						  , DisplayBeforeDate
						  , ExpireDate
						  , CouponID
						  , CategoryName
						  , GETDATE());
					
			
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireLoyaltyDealsDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
