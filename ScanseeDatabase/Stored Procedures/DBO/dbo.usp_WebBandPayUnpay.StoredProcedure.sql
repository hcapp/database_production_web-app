USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandPayUnpay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandPayUnpay
Purpose					: To insert/update pay option for respective Bands
Example					: usp_WebBandPayUnpay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26 Apr 2016		Prakash	 	Initial Version
---------------------------------------------------------------
*/

--exec  [dbo].[usp_WebBandPayUnpay] 0,0,0,0,0,0

CREATE PROCEDURE [dbo].[usp_WebBandPayUnpay]
(
		 @RetailerID int
		,@UserID int	
		,@IsPaid bit	
			
		--Output Variable 
		, @Status int output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION 

		IF EXISTS(SELECT 1 FROM BandBillingDetails WHERE BandID = @RetailerID )
		BEGIN


			UPDATE BandBillingDetails 
			SET IsPaid = @IsPaid 
			WHERE BandID = @RetailerID


		END
		ELSE IF EXISTS(SELECT 1 FROM Band WHERE Bandid = @RetailerID AND BandActive = 1)
		BEGIN				
		--INSERT INT0 THE BandBillingDetails TABLE.




				INSERT INTO BandBillingDetails(
													  UserID
													, BandID
													, BandBillingPlanID
													, Quantity												
													, BandProductFeeFlag		
													, IsPaid
													, DateCreated																							 
											  )
										
					                         SELECT @UserID
					                                , @RetailerID
					                                , 1
					                                , 1
					                                , 1
													, @IsPaid
													, GETDATE()
		
		END
		--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandPayUnpay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
