USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListRebateGallery]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_MasterShoppingListRebateGallery]
Purpose					: to display all the rebates in the user rebate gallery
Example					: [usp_MasterShoppingListRebateGallery]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListRebateGallery]
(
	@ProductID int
	, @RetailID int
	, @UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	    --To Fetch Rebate Information from user Rebate gallery for the product with retailer info.
		IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)
			BEGIN
			
				SELECT R.RebateID
					, R.ManufacturerID 
					, RebateAmount 
					, RebateStartDate 
					, RebateEndDate 
					, RR.RetailID
					, RebateName 
				FROM Rebate R
					INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID 
					INNER JOIN RebateRetailer RR ON RR.RebateID=R.RebateID
					INNER JOIN UserRebateGallery RG on R.RebateID=RG.RebateID
				WHERE RR.RetailID = @RetailID
					AND RP.ProductID = @ProductID 
					AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate
					AND UserID=@userid
					
			END
		
		IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS  NULL and @RetailID = 0)
			BEGIN
			
				SELECT R.RebateID
					, R.ManufacturerID 
					, RebateAmount 
					, RebateStartDate 
					, RebateEndDate 
					, NULL AS RetailID
					, RebateName 
				FROM Rebate R
					INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID 
					INNER JOIN UserRebateGallery RG on R.RebateID=RG.RebateID
				WHERE RP.ProductID = @ProductID
					 AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate
					 AND UserID=@userid
					
			END

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_MasterShoppingListRebateGallery].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
