USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCouponSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebCouponSearch
Purpose					: To search for a coupon on coupon name & description in the coupon admin page.
Example					: usp_WebCouponSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27thJune2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebCouponSearch]
(

	--Input Variables
	  @SearchParameter varchar(1000)
	, @ColumnName varchar(100) --Output is sorted based on this column
	, @SortOrder varchar(10) --Should be ASC or DESC only
	, @LowerLimit int 
	, @ShowExpired bit
	
	--Output Variable 
	, @RowCount INT OUTPUT    
	, @NextPageFlag bit output   
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Query VARCHAR(MAX)
		DECLARE @UpperLimit INT    
		DECLARE @MaxCnt INT    
		      
		  --To get the row count for pagination.      
		 DECLARE @ScreenContent Varchar(100)    
		 SELECT @ScreenContent = ScreenContent       
		 FROM AppConfiguration       
		 WHERE ScreenName = 'All'     
		 AND ConfigurationType = 'Website Pagination'    
		 AND Active = 1   
		 
		 SET @UpperLimit = @LowerLimit + @ScreenContent  
		 
		--To get the coupons based on the SowExpired check box value.(If checked, show expired)
		SELECT C.CouponID
			 , CouponName
			 , CouponShortDescription
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponDiscountType
			 , CouponURL
			 , CASE WHEN C.CouponStartDate = '01/01/1900' THEN (SELECT MAX(CouponStartDate) FROM CouponHistory WHERE CouponID = C.CouponID) ELSE C.CouponStartDate END CouponStartDate
			 , CASE WHEN C.CouponExpireDate = '01/01/1900' THEN (SELECT MAX(CouponExpireDate) FROM CouponHistory WHERE CouponID = C.CouponID) ELSE C.CouponExpireDate END  CouponExpireDate
			 , ExpiredFlag = CASE WHEN GETDATE() > C.CouponExpireDate THEN 1 ELSE 0 END
		INTO #Temp
		FROM Coupon C
		WHERE CouponName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END 
		AND CouponID NOT IN(SELECT CouponID FROM CouponRetailer) 
		AND C.CouponExpireDate >= CASE WHEN @ShowExpired = 1 THEN C.CouponExpireDate - 1 ELSE GETDATE() END 
		 
		--To acheive the Dynamic sorting.
		DECLARE @Coupons TABLE(RowNum INT IDENTITY(1, 1)
							 , CouponID INT
							 , CouponName VARCHAR(1000)
							 , CouponShortDescription VARCHAR(1000)
							 , CouponDiscountAmount FLOAT
							 , CouponDiscountPct FLOAT
							 , CouponDiscountType VARCHAR(100)
							 , CouponURL VARCHAR(1000)
							 , CouponStartDate DATETIME
							 , CouponExpireDate DATETIME
							 , ExpiredFlag BIT)
		
		INSERT INTO @Coupons(
							   CouponID
							 , CouponName
							 , CouponShortDescription
							 , CouponDiscountAmount
							 , CouponDiscountPct
							 , CouponDiscountType
							 , CouponURL
							 , CouponStartDate
							 , CouponExpireDate
							 , ExpiredFlag)
		SELECT CouponID
							 , CouponName
							 , CouponShortDescription
							 , CouponDiscountAmount
							 , CouponDiscountPct
							 , CouponDiscountType
							 , CouponURL
							 , CouponStartDate
							 , CouponExpireDate
							 , ExpiredFlag
		FROM #Temp			
		ORDER BY CASE WHEN @SortOrder = 'ASC' AND @ColumnName = 'CouponName' THEN CouponName
					  WHEN @SortOrder = 'ASC' AND @ColumnName= 'CouponDiscountAmount' THEN CAST(CouponDiscountAmount AS SQL_VARIANT)
					  WHEN @SortOrder = 'ASC' AND @ColumnName = 'CouponStartDate' THEN CAST(CouponStartDate AS SQL_VARIANT)
					  WHEN @SortOrder = 'ASC' AND @ColumnName = 'CouponExpireDate' THEN CAST(CouponExpireDate AS SQL_VARIANT)
            END ASC,
            CASE  WHEN @SortOrder = 'DESC' AND @ColumnName = 'CouponName' THEN CouponName  
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'CouponDiscountAmount' THEN CAST(CouponDiscountAmount AS SQL_VARIANT)
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'CouponStartDate' THEN CAST(CouponStartDate AS SQL_VARIANT)  
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'CouponExpireDate' THEN CAST(CouponExpireDate AS SQL_VARIANT)
            END DESC;

					    
			SELECT @MaxCnt = COUNT(RowNum) FROM @Coupons 
		--Output Total no of Records
		SET @RowCount = @MaxCnt 
		       
		SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS 			 
		 
		--Project the records.			
		SELECT CouponID
			 , CouponName
			 , CouponShortDescription
			 , CouponDiscountAmount
			 , CouponDiscountPct
			 , CouponDiscountType
			 , CouponURL
			 , CouponStartDate
			 , CouponExpireDate
			 , ExpiredFlag
		FROM @Coupons
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit	
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT ERROR_LINE()
		END;
		 
	END CATCH;
END;




GO
