USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerSaveUserRatings]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerSaveUserRatings  
Purpose               : Retrieve Average User Rating for product in context 
Example               : usp_WebConsumerSaveUserRatings  
  
History  
Version  Date            Author          Change Description  
---------------------------------------------------------------  
1.0      31st May 2013   Dhananjaya TR   Initial Version  
---------------------------------------------------------------   
*/ 

CREATE PROCEDURE [dbo].[usp_WebConsumerSaveUserRatings]
(
	@ProductID	int,
	@UserID		int,
	@Rating		tinyint,
		
	--Output Variable
	@AvgRating		int output, 
	@Status			int output,
	@ErrorNumber	int output,
	@ErrorMessage	varchar(1000) output 	
)	
As

Begin
	
	Begin Try
	
		BEGIN TRANSACTION
					
			-- Save User Rating
			IF EXISTS (SELECT 1 FROM [UserRating] WHERE UserID = @UserID and ProductID = @ProductID)
				BEGIN
					IF @Rating != 0 -- To check if User Rating is > or = zero
						BEGIN
							-- For an existing record the rating is updated if User Rating > zero
							UPDATE UserRating Set Rating = @Rating
							WHERE UserID = @UserID 
							AND ProductID = @ProductID
						END
					ELSE
						BEGIN
							-- If User Rating is zero, then the rating is deleted
							DELETE FROM UserRating 
							WHERE UserID = @UserID 
							AND ProductID = @ProductID 
							AND @Rating = 0
						END
				End			
			Else		
				BEGIN
					IF @Rating != 0 -- If User Rating is zero, then the rating is NOT recorded
						BEGIN
							INSERT INTO UserRating (ProductID, UserID, Rating, ReviewDate)
							VALUES (@ProductID, @UserID, @Rating, GETDATE())
						END
				 END	
				 
			SELECT @AvgRating = AVG(Isnull(Rating,0)) from UserRating  
			WHERE ProductID = @ProductID
				 
				
			--Confirmation of Success.
			SELECT @Status = 0	
		COMMIT TRANSACTION	
	End Try

Begin Catch

	--Check whether the Transaction is uncommitable.
	If @@ERROR <> 0
	Begin
		Print 'Error occured in Stored Procedure usp_WebConsumerSaveUserRatings.'		
		--- Execute retrieval of Error info.
		Exec [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
		ROLLBACK TRANSACTION;
		--Confirmation of failure.
		SELECT @Status = 1
	End;
	 
End Catch;
	
End


GO
