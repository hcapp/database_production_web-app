USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FindNearByRetailer]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_FindNearByRetailer  
Purpose     : To get near by retailers list.  
Example     : EXEC usp_FindNearByRetailer 14, '12.94715', '77.57888', 1, 2    
  
History  
Version  Date  Author   Change Description  
---------------------------------------------------------------   
  
---------------------------------------------------------------  
*/  
  
 CREATE PROCEDURE [dbo].[usp_FindNearByRetailer]  
(  
   @UserID INT  
 , @Latitude decimal(18,6)  
 , @Longitude decimal(18,6)  
 , @PostalCode varchar(10)  
 , @ProductId int  
 , @Radius int  
   
 --Output Variable   
 , @Result int OUTPUT  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
  --Latitude and longitude values are null, but Postal Code is passed.  
  IF (@Latitude IS NULL OR @Longitude IS NULL) AND @PostalCode IS NOT NULL  
  BEGIN  
   SELECT @Latitude = Latitude   
    , @Longitude = Longitude   
   FROM GeoPosition   
   WHERE PostalCode = @PostalCode   
  END  
    
  --Latitude, longitude and Postal Code values are not passed.  
  IF (@Latitude IS NULL OR @Longitude IS NULL) AND @PostalCode IS NULL  
  BEGIN  
   SELECT @PostalCode = PostalCode FROM Users WHERE UserID = @UserID  
   IF @PostalCode IS NOT NULL  
   BEGIN  
    SELECT @Latitude = Latitude, @Longitude = Longitude   
    FROM GeoPosition   
    WHERE PostalCode = @PostalCode  
   END  
   ELSE   
   BEGIN  
    SELECT @Result = -1  
   END  
     
  END  
    
    
    
  --To fetch User Preferred Radius.  
  IF @Radius IS NULL  
  BEGIN  
   SELECT @Radius = ISNULL(LocaleRadius, 5)   
   FROM UserPreference   
   WHERE UserID = @UserID   
  END  
    
  SELECT RetailID retailerId  
   , RetailName retailerName  
   , Address   
   , ContactMobilePhone phone  
   , RetailURL RetailerUrl  
   , Distance distance  
   , ProductId productId  
   , ProductName  
   , ProductImagePath imagePath  
   , Price productPrice  
   , UserID   
   , RetailLocationLatitude latitude  
   , RetailLocationLongitude longitude  
  FROM   
  (  
   SELECT R.RetailID   
    , R.RetailName   
    , RL.Address1 +','+isnull(RL.Address2,'')+ isnull(RL.Address3,'')+isnull(RL.Address4,'') +RL.City +','+ RL.State+ ',' +RL.PostalCode Address  
    , RC.ContactMobilePhone   
    , R.RetailURL  
    , Distance = (ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
    , P.ProductId   
    , P.ProductName  
    , ProductImagePath  = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END  
    , RP.Price  
    , @UserID  AS UserID  
    , RL.RetailLocationLatitude   
    , RL.RetailLocationLongitude    
   FROM Product P  
   INNER JOIN RetailLocationProduct RP ON RP.ProductID = P.ProductID   
   INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID  
   LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID    
   INNER JOIN Retailer R ON R.RetailID = RL.RetailID  
   WHERE P.ProductID = @ProductId   
  )Retailer   
  WHERE Distance <= ISNULL(@Radius, 5)
  ORDER BY Price, Distance  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_ShoppingNearByRetailer.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
