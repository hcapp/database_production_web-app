USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetailerBusinessCategoryUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetailerBusinessCategoryUpdation
Purpose					: To update Retailer's business categories.
Example					: usp_WebRetailerAdminRetailerBusinessCategoryUpdation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/03/2014				SPAN		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerAdminRetailerBusinessCategoryUpdation]

	--Input parameters	 
	  @RetailID INT	
	, @BusinessCategoryIDs VARCHAR(MAX)

	--Output variables 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
		DELETE FROM RetailerBusinessCategory WHERE RetailerID = @RetailID

		INSERT INTO RetailerBusinessCategory(RetailerID										
										 ,BusinessCategoryID
										 ,DateModified)
								SELECT	@RetailID									 
										,Param
										,GETDATE()									
								FROM dbo.fn_SplitParam(@BusinessCategoryIDs, ',')

		-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

		------------------------------------------------
							 
		--Confirmation of failure.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetailerBusinessCategoryUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
