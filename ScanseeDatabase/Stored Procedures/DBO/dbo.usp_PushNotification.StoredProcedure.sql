USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_PushNotification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_PushNotification
Purpose					: To capture Push Notification and FirstUseComplete info and also UserFirstUseLogin Table info.
Example					: 
DECLARE @Status INT
	, @ErrorNumber int  
	, @ErrorMessage varchar(1000)  
	
EXEC usp_PushNotification 2, null, null, '7/5/2011',@Status= @Status output ,@ErrorNumber = @ErrorNumber output,@ErrorMessage = @ErrorMessage output 
SELECT @Status, @ErrorNumber, @ErrorMessage

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_PushNotification]
(
	@UserID int
	, @PushNotify bit
	, @Date datetime
	
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
	
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			-- To capture Push Notification and First Use flag.
			UPDATE Users 
			SET PushNotify = @PushNotify 
			WHERE UserID = @UserID 
			
			
				   
			--Confirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_PushNotification.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
