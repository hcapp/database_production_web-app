USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateUserDeviceAppVersion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UpdateUserDeviceAppVersion
Purpose					: To Update the version of the App installed on the user's device.
Example					: usp_UpdateUserDeviceAppVersion

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			23rd July 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UpdateUserDeviceAppVersion]
(

	--Input Parameter(s)--
	  
	  @DeviceID varchar(60)
	, @AppVersion varchar(10)
	
   	
	--Output Variable--		
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			IF EXISTS(SELECT 1 FROM UserDeviceAppVersion WHERE DeviceID = @DeviceID AND AppVersion <> @AppVersion)
			BEGIN
				--Update the version of the app istalled on the device.
				UPDATE UserDeviceAppVersion SET AppVersion = @AppVersion
				WHERE AppVersion = @AppVersion	
			END				 
		
		--Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UpdateUserDeviceAppVersion.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
