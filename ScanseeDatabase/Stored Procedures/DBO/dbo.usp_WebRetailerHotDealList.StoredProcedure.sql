USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerHotDealList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebRetailerHotDealList] 
Purpose				  : To get HotDeals in DropDown.    
Example				  : [usp_WebRetailerHotDealList]  
    
History    
Version    Date          Author    Change Description    
---------------------------------------------------------------     
1.0     23rd July 2013   SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebRetailerHotDealList] 
(    
  
	--Input Variables   
	  @RetailID int
      
	--OutPut Variable
	, @Status int output 
	, @ErrorNumber int output    
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
	
	    --To get Media Server Configuration.  
	    DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
				 
		SELECT  ProductHotDealID hotDealID
			    , HotDealName
			    , HotDeaLonglDescription AS HotDealLongDesc
				, HotDealImagePath = CASE WHEN HotDealImagePath IS NOT NULL THEN 
											CASE WHEN WebsiteSourceFlag = 1 THEN 
													CASE WHEN ManufacturerID IS NOT NULL THEN @ManufacturerConfig + CONVERT(VARCHAR(30),ManufacturerID) +'/' + HotDealImagePath
														ELSE @RetailerConfig + CONVERT(VARCHAR(30),RetailID) +'/' + HotDealImagePath
													END
												ELSE HotDealImagePath
											END
									END  
				, HotDealStartDate
				, HotDealEndDate
				, HotDealExpirationDate
				, HotDealDiscountAmount
				, HotDealDiscountPct
				, price
				, salePrice 
		FROM ProductHotDeal 
		WHERE  RetailID = @RetailID
		ORDER BY HotDealName
		
		--Success
		SELECT @Status=0
		
	END TRY    
     
	BEGIN CATCH    
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerHotDealList].'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT @Status=1
		END;    
	END CATCH;
   
END;




GO
