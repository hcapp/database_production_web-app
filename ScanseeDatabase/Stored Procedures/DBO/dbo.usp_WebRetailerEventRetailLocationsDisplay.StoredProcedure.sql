USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerEventRetailLocationsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerEventRetailLocationsDisplay]
Purpose					: To display list of RetailLocations for given Retailer while creating Event.
Example					: [usp_WebRetailerEventRetailLocationsDisplay]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/25/2014	    SPAN	            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerEventRetailLocationsDisplay]
(
	  --Input Variables
	   @RetailID int	  
	
	 --Output Variables
	 , @Status int output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
	 

		SELECT DISTINCT
				   RL.RetailLocationID
				 , RL.Address1
				 , RL.City
				 , RL.State
				 , RL.PostalCode
				 , RetailLocationLatitude AS RetailerLocationLatitude
				 , RetailLocationLongitude AS RetailerLocationLongitude
		FROM RetailLocation RL
		INNER JOIN HcRetailerAssociation RA ON RA.RetailLocationID = RL.RetailLocationID
		WHERE RA.RetailID = @RetailID AND RL.Headquarters <> 1 AND RL.Active = 1	 

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerEventRetailLocationsDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;





GO
