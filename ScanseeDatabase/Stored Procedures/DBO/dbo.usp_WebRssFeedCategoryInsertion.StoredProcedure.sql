USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedCategoryInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: usp_WebRssFeedCategoryInsertion
Purpose					: 
Example					: usp_WebRssFeedCategoryInsertion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedCategoryInsertion]
(
	--  @CategoryName varchar(2000)
    --, @ImagePath varchar(max)
	--, @HcHubCitiID int
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		 DELETE FROM RssFeedCategory
		 FROM RssFeedCategory RF
		 INNER JOIN RssFeedCategoryStagingTable RS ON RF.CategoryName = RS.CategoryName
		 WHERE RS.CategoryName IS NOT NULL  

		 INSERT INTO RssFeedCategory(CategoryName
									,ImagePath
									,HcHubCitiID)
							SELECT	CategoryName
								   ,ImagePath
								   ,HcHubCitiID
							FROM RssFeedCategoryStagingTable RS WHERE RS.CategoryName NOT IN (SELECT CategoryName FROM RssFeedCategory)   
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedCategoryInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;





GO
