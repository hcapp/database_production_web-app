USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerPaymentInformation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerPaymentInformation
Purpose					: To insert the details of the plan choosed by the Retailer
Example					: usp_WebRetailerPaymentInformation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Jan 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerPaymentInformation]
(
		  @Retailer int
		, @UserID int
		, @ACHorBankInformationFlag bit
		, @BankName varchar(255)
		, @AccountTypeID int
		, @AccountNumber int
		, @AccountHolderName varchar(255)
		, @BillingAddress varchar(1000)
		, @City varchar(255) 
		, @State char(2)
		, @Zip char(10)
		, @PhoneNumber char(10)
			
	--Output Variable 
		, @Status int output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION 
		
		    INSERT INTO RetailerBankInformation(
														UserID
													  , RetailerID
													  , ACHorBankInformationFlag
													  , BankName 
													  , AccountTypeID
													  , AccountNumber 
													  , AccountHolderName 
													  , BillingAddress 
													  , City
													  , [State] 
													  , Zip 
													  , PhoneNumber 
												
													)
										VALUES(
												 @Retailer 
											   , @UserID 
											   , @ACHorBankInformationFlag 
											   , @BankName 
											   , @AccountTypeID 
											   , @AccountNumber 
											   , @AccountHolderName 
											   , @BillingAddress
											   , @City 
											   , @State 
											   , @Zip 
											   , @PhoneNumber
											   )			
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerPaymentInformation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
