USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FindCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FindCategoryDisplay
Purpose					: to display the categories to be displayed in find module when we search by location.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29 Nov 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_FindCategoryDisplay]
(
	
	--Output Variable 
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @ConfigirationType varchar(50)
			SELECT @ConfigirationType = ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType = 'App Media Server Configuration'
		
			SELECT  DISTINCT
			--CategoryName CatName
				   CategoryDisplayName CatDisName
				  , @ConfigirationType+CategoryImagePath CatImgPth
			FROM  FindSourceCategory F
			WHERE ActiveFlag=1 
			 
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FindCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
