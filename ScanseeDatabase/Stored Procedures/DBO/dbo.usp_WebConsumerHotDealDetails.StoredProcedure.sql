USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerHotDealDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebConsumerHotDealDetails    
Purpose               : To get details of a Hot Deal.    
Example               : usp_WebConsumerHotDealDetails    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   23rd June 2011 Padmapriya M Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebConsumerHotDealDetails]    
(    
   @UserID int    
 , @HotDealID int    
 
 --User Tracking inputs.
 , @HotDealListID int
     
 --OutPut Variable   
 , @Status int output 
 , @ErrorNumber int output    
 , @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY    
 
	BEGIN TRANSACTION
	 --To get Server Configuration
	 DECLARE @ManufConfig varchar(50) 
	 DECLARE @RetailerConfig varchar(50)
	 DECLARE @Config Varchar(50)  
	 DECLARE @Imagepath Varchar(100)
	 
	 SELECT @ManufConfig=(select ScreenContent  
						 FROM AppConfiguration   
						 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
		   ,@RetailerConfig=(select ScreenContent  
							 FROM AppConfiguration   
							 WHERE ConfigurationType='Web Retailer Media Server Configuration')
		   ,@Config=(select ScreenContent  
					 FROM AppConfiguration   
					 WHERE ConfigurationType='App Media Server Configuration')
	 FROM AppConfiguration
	  
	 SELECT @Imagepath=APIPartnerImagePath  from APIPartner 
	 WHERE APIPartnerName ='ScanSee'
	    
	  SELECT DISTINCT P.ProductHotDealID hotDealId     
		--,ISNULL(Category, 'Uncategorized') AS Category  	   
		,HotDealName 
		,P.RetailID 		   
		,CASE WHEN R.RetailID IS NOT NULL THEN R.RetailName END retName
		,CASE WHEN R.RetailID IS NOT NULL THEN R.Address1 END retAddr
		,CASE WHEN R.RetailID IS NOT NULL THEN R.City WHEN R.RetailID IS NULL THEN PHL.City END city
		,CASE WHEN R.RetailID IS NOT NULL THEN R.State WHEN R.RetailID IS NULL THEN PHL.State END state
		,CASE WHEN R.RetailID IS NOT NULL THEN R.PostalCode END zipcode	   
		,ISNULL(HotDealShortDescription,HotDeaLonglDescription) HotDealShortDescription 
		,hDLognDescription = CASE WHEN ISNULL(HotDeaLonglDescription,0) = '0' OR  HotDeaLonglDescription = '' THEN     
			   CASE WHEN ISNULL(HotDealShortDescription,0) = '0' OR  HotDealShortDescription = '' THEN HotDealName    
			   ELSE HotDealShortDescription     
			   END    
				ELSE HotDeaLonglDescription    
			  END    
	                    
		,hotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
													CASE WHEN P.WebsiteSourceFlag = 1 THEN 
																CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
																	+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
																   +HotDealImagePath  
																ELSE @RetailerConfig  
																+CONVERT(VARCHAR(30),P.RetailID)+'/'  
																+HotDealImagePath  
																END  					                         
													ELSE P.HotDealImagePath  
													END  
							END  
		,SUBSTRING(CONVERT(VARCHAR(20), HotDealStartDate, 100),1,7) hDStartDate    
		,SUBSTRING(CONVERT(VARCHAR(20), HotDealEndDate, 100),1,7) hDEndDate    
		,(CASE WHEN P.RetailID IS NOT NULL THEN  @Config+@Imagepath ELSE @Config+A.APIPartnerImagePath END) apiPartnerImagePath  	
		,P.HotDealURL hdURL 
		,A.APIPartnerName poweredBy
	  FROM ProductHotDeal P    
	  LEFT JOIN ProductHotDealLocation PHL ON PHL.ProductHotDealID = P.ProductHotDealID
	  LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID   	   
	  LEFT JOIN ProductHotDealRetailLocation RL ON RL.ProductHotDealID =P.ProductHotDealID 
	  LEFT JOIN RetailLocation RE ON RL.RetailLocationID =RE.RetailLocationID 
	  LEFT JOIN Retailer R ON (R.RetailID =P.RetailID OR RE.RetailID =R.RetailID)	  
	  WHERE P.ProductHotDealID = @HotDealID     
	  
	  --User Tracking section.
	  UPDATE ScanSeeReportingDatabase..HotDealList SET ProductHotDealClick = 1 
	  WHERE HotDealListID = @HotDealListID
	  
	  --Confirmation of Success.
	  SELECT @Status = 0
	COMMIT TRANSACTION  
 END TRY    
     
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebConsumerHotDealDetails.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'     
   ROLLBACK TRANSACTION;
   --Confirmation of failure.
   SELECT @Status = 1
  END;    
       
 END CATCH;    
END;


GO
