USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadRetailerLocations]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadRetailerLocations
Purpose					: To upload Retail locations by the Retailer.
Example					: usp_UploadRetailerLocations

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th Dec 2011	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadRetailerLocations]
(
	  @RetailerID int
	, @UserID int
	, @RetailLocationFileName Varchar(255)	
	
	--Output Variable 
	, @SuccessCount int output 
	, @FailureCount int output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @StartDate DATETIME
			DECLARE @EndDate DATETIME
			DECLARE @TotalCount int
			DECLARE @UploadRetailLocationsLogID INT
				
			
			DECLARE @RowCount int	
				
			
			--To Capture the Process Start Date
			SET @StartDate = GETDATE()
			
			-- Filter out the valid data i.e those having all the Store Identities unique fileds
			
			SELECT   RowNum
			        ,UploadRetaillocationsID
					,UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
					,RetailLocationImagePath
					
		INTO #UniqueStores
		FROM  
		(SELECT   RowNum = ROW_NUMBER() OVER (ORDER BY UploadRetaillocationsID ASC)
				, UploadRetaillocationsID 
				, UserID
				,UR.StoreIdentification
				,UR.RetailID
				,UR.Headquarters
				,UR.Address1
				,UR.Address2
				,UR.Address3
				,UR.Address4
				,UR.City
				,UR.[State]
				,UR.PostalCode
				,UR.PhoneNumber
				,UR.CountryID
				,UR.RetailLocationURL
				,UR.RetailLocationLatitude
				,UR.RetailLocationLongitude
				,UR.RetailLocationTimeZone
				,UR.DateCreated
				,UR.DateModified
				,UR.RetailLocationStoreHours
				,UR.RetailKeyword
				,UR.RetailLocationImagePath			
		FROM UploadRetaillocations UR
		WHERE UploadRetaillocationsID IN (SELECT MIN(UploadRetaillocationsID)UploadRetaillocationsID
										  FROM UploadRetaillocations
										  WHERE UserID = @UserID
										  AND RetailID = @RetailerID
										  AND LEN(State) <= 2
										  GROUP BY RetailID, StoreIdentification)) RetailerLocations
										  
		
		--Capture the duplicates in the Staging table.								  
		     SELECT  RowNum
			        ,UploadRetaillocationsID
					,UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
					,RetailLocationImagePath
		INTO #DuplicateStores
		FROM (								  
				SELECT  RowNum = ROW_NUMBER() OVER (ORDER BY UploadRetaillocationsID ASC)
						, UploadRetaillocationsID 
						, UserID
						,UR.StoreIdentification
						,UR.RetailID
						,UR.Headquarters
						,UR.Address1
						,UR.Address2
						,UR.Address3
						,UR.Address4
						,UR.City
						,UR.[State]
						,UR.PostalCode
						,UR.PhoneNumber
						,UR.CountryID
						,UR.RetailLocationURL
						,UR.RetailLocationLatitude
						,UR.RetailLocationLongitude
						,UR.RetailLocationTimeZone
						,UR.DateCreated
						,UR.DateModified
						,UR.RetailLocationStoreHours
						,UR.RetailKeyword
						,UR.RetailLocationImagePath
				FROM UploadRetaillocations UR
				WHERE UploadRetaillocationsID NOT IN (SELECT MIN(UploadRetaillocationsID)UploadRetaillocationsID
												  FROM UploadRetaillocations
												  WHERE UserID = @UserID
												  AND RetailID = @RetailerID
												  GROUP BY RetailID, StoreIdentification)
				AND UR.UserID = @UserID
				AND UR.RetailID = @RetailerID)RetailLocations
			
			--To get newly added unique Retailer Locations.									  
			SELECT   RowNum = ROW_NUMBER() OVER (ORDER BY UploadRetaillocationsID ASC)
			       , UserID
			       , UploadRetaillocationsID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours	
					,RetailKeyword	
					,RetailLocationImagePath	   
            INTO #NewStores				
			FROM #UniqueStores U						   --No Store hard coded because if the query returns Null then the query fails.
			WHERE StoreIdentification NOT IN(SELECT ISNULL(StoreIdentification,'No Store') FROM RetailLocation WHERE RetailID = @RetailerID AND Active = 1)
			AND UserID = @UserID
			AND RetailID = @RetailerID	
		
		--Select all the non unique stores.							
			SELECT   RowNum
					,UploadRetaillocationsID
					,UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
					,RetailLocationImagePath
		INTO #NonUniqueStores
		FROM  
		(SELECT   RowNum = ROW_NUMBER() OVER (ORDER BY UploadRetaillocationsID ASC)
				,UploadRetaillocationsID
				, UserID
				,UR.StoreIdentification
				,UR.RetailID
				,UR.Headquarters
				,UR.Address1
				,UR.Address2
				,UR.Address3
				,UR.Address4
				,UR.City
				,UR.[State]
				,UR.PostalCode
				,UR.PhoneNumber
				,UR.CountryID
				,UR.RetailLocationURL
				,UR.RetailLocationLatitude
				,UR.RetailLocationLongitude
				,UR.RetailLocationTimeZone
				,UR.DateCreated
				,UR.DateModified
				,UR.RetailLocationStoreHours
				,UR.RetailKeyword
				,UR.RetailLocationImagePath
		FROM #UniqueStores UR
		WHERE UploadRetaillocationsID IN (SELECT UploadRetaillocationsID
									FROM UploadRetaillocations UR
									INNER JOIN RetailLocation RL ON RL.StoreIdentification = UR.StoreIdentification
									WHERE UR.UserID = @UserID AND RL.Active = 1
									AND RL.RetailID = @RetailerID)) RetailerLocations	
									
										
			
			--Filter all the rows with Mandatory fileds present.
			
			SELECT   RowNum1 = ROW_NUMBER() OVER (ORDER BY RowNum ASC)
			       , UserID
			       , UploadRetaillocationsID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword	
					,RetailLocationImagePath		   
            INTO #Valid					
			FROM #NewStores U
			WHERE UserID = @UserID
			AND RetailID = @RetailerID 
			AND (RetailID IS NOT NULL
			  AND Address1 IS NOT NULL
			  AND PostalCode IS NOT NULL
			  AND Headquarters IS NOT NULL
			  AND CountryID IS NOT NULL
			  AND PhoneNumber IS NOT NULL)  --IN THE SCREEN THIS FIELD IS OPTIONAL BUT IN THE TABLE THIS IS MANDATORY.
						 
			  
			  --Contact Details
			  
			--  SELECT  RowNum2 = IDENTITY(int,1,1)			        
			--        , StoreIdentification
			--        , PhoneNumber
			--        , ContactEmail
			--	    , ContactTitle
			--	    , ContactFirstName
			--	    , ContactLastName
			--INTO #TEMP
			--FROM #Valid		
			
			--Filter out all the invalid  data that has any missing fields.	
			
			
			SELECT   UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
				    ,ReasonForDiscarding
					,RetailLocationImagePath
			INTO #Invalid
			FROM
			(SELECT  UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
				    ,'Retail Store ID Already Exists' ReasonForDiscarding
				    ,RetailLocationImagePath
				   FROM #DuplicateStores			
					  
				UNION ALL
				
				SELECT  UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
				    ,'Retail Store ID Already Exists' ReasonForDiscarding
				    ,RetailLocationImagePath
				  FROM #NonUniqueStores
				  
				  UNION ALL
				  
				 SELECT  UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
				    ,'Some Mandatory fields are Missing.' ReasonForDiscarding
				    ,RetailLocationImagePath
				  FROM #NewStores
				  WHERE UploadRetaillocationsID NOT IN(SELECT UploadRetaillocationsID FROM #Valid)
				  AND UserID = @UserID
				  AND RetailID = @RetailerID
				  
				  UNION ALL
				  
				  SELECT  UserID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,[State]
					,PostalCode
					,PhoneNumber
					,CountryID
					,RetailLocationURL
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,RetailKeyword
				    ,'Error in Data Provided' ReasonForDiscarding
				    ,RetailLocationImagePath
				   FROM UploadRetaillocations
				   WHERE RetailID = @RetailerID
				   AND UserID = @UserID
				   AND UploadRetaillocationsID NOT IN(SELECT UploadRetaillocationsID FROM #UniqueStores)
				   AND UploadRetaillocationsID NOT IN(SELECT UploadRetaillocationsID FROM #DuplicateStores))Invalid
				 	   
			  
			  --Insert the valid data into the RetailLocation(Production) Table.

			CREATE TABLE #HcAssociate(RowNum INT IDENTITY(1, 1), RetailID INT, RetailLocationID INT, City varchar(255), State char(2), PostalCode varchar(20))
			INSERT INTO RetailLocation(  RetailID
										,Headquarters
										,Address1
										,Address2
										,Address3
										,Address4
										,City
										,State
										,PostalCode
										,CountryID
										,RetailLocationURL
										,RetailLocationLatitude
										,RetailLocationLongitude
										,RetailLocationTimeZone
										,DateCreated									
										,RetailLocationStoreHours
										,StoreIdentification
										,RetailLocationImagePath) 

							OUTPUT inserted.RetailID, inserted.RetailLocationID, inserted.City, inserted.State, inserted.PostalCode INTO #HcAssociate(RetailID, RetailLocationID, City, State, PostalCode)		
          
								  SELECT RetailID
										,Headquarters
										,Address1
										,Address2
										,Address3
										,Address4
										,City
										,State
										,PostalCode
										,1
										,RetailLocationURL
										,CASE WHEN RetailLocationLatitude IS NULL OR RetailLocationLatitude = 0 THEN (SELECT Latitude FROM GeoPosition G WHERE G.PostalCode = #Valid.PostalCode AND G.City =#Valid.City AND G.State =#Valid.State) ELSE RetailLocationLatitude END
										,CASE WHEN RetailLocationLongitude IS NULL OR RetailLocationLongitude = 0 THEN(SELECT Longitude FROM GeoPosition G WHERE G.PostalCode = #Valid.PostalCode AND G.City =#Valid.City AND G.State =#Valid.State) ELSE RetailLocationLongitude END
										,RetailLocationTimeZone
										,GETDATE()									
										,RetailLocationStoreHours
										,StoreIdentification 
										,RetailLocationImagePath
								FROM #Valid
		
	    SELECT @RowCount = COUNT(1) FROM #Valid	    

		--Associate the Retail Locations to the Hub Cities that have the City, State & POstalCode same as that of the Retail locations.
		INSERT INTO HcRetailerAssociation(HcHubCitiID
										, RetailID
										, RetailLocationID
										, DateCreated
										, CreatedUserID
										, Associated)
						SELECT HcHubCitiID
							 , B.RetailID
							 , B.RetailLocationID
							 , GETDATE()
							 , @UserID
							 , 0
						FROM HcLocationAssociation A
						INNER JOIN #HcAssociate B ON A.City = B.City AND A.State = B.State AND A.PostalCode = B.PostalCode
		
		
		CREATE TABLE #Contact(RowNum int identity(1,1) ,ContactID int)
		--Contact Details Insertion.						
		INSERT INTO Contact(
							ContactPhone
						  , DateCreated
						  , CreateUserID)
		OUTPUT inserted.ContactID INTO #Contact                    
			        SELECT V.PhoneNumber
			             , GETDATE()
			             , @UserID
			        FROM #Valid V     
		
	   
	    --RetailContact table insertion.
	    INSERT INTO RetailContact(RetailLocationID
	                            , ContactID)
	                      SELECT RL.RetailLocationID
							   , C.ContactID
	                      FROM #Valid V
	                      INNER JOIN RetailLocation RL ON V.StoreIdentification = RL.StoreIdentification AND RL.Active = 1	                      
	                      INNER JOIN #Contact C ON C.RowNum = V.RowNum1 
	                      WHERE RL.RetailID = @RetailerID
	                      AND V.UserID = @UserID
	    --Capture the Process end date			  
		SET @EndDate = GETDATE()
		
		--Create the Retail Location Keywords.
		INSERT INTO RetailerKeywords(RetailID
								   , RetailKeyword
								   , RetailLocationID
								   , DateCreated)
					SELECT @RetailerID
						 , RetailKeyword
						 , RL.RetailLocationID
						 , GETDATE()
					FROM #Valid V
					INNER JOIN RetailLocation RL ON V.StoreIdentification = RL.StoreIdentification
					WHERE RL.RetailID = @RetailerID AND RL.Active = 1
		
		--Get all the counts including the total records processed, failure count and the successful counts.
		SELECT @TotalCount = COUNT(1) FROM UploadRetaillocations WHERE UserID = @UserID AND RetailID = @RetailerID
		SELECT @SuccessCount = COUNT(1) FROM #Valid
		SELECT @FailureCount =  (SELECT COUNT(1) FROM #InValid) 	
		
		--Insert the details into the Process Log Table.
		INSERT INTO UploadRetaillocationsLog (
												UserID 											  
											  , RetaillocationFileName
											  , RetaillocationTotalRowsProcessed
											  , RetaillocationSuccessfulRowCount
											  , RetaillocationFailureRowCount
											  , Remarks
											  , ProcessDate
											  , ProcessStartDate
											  , ProcessEndDate
											
											)	
								VALUES(
											@UserID
										  , @RetailLocationFileName
										  , @TotalCount
										  , @SuccessCount
										  , @FailureCount
										  , 'Successful'
										  , @StartDate
										  , @StartDate
										  , @EndDate										   
									   )	
			
			SET @UploadRetailLocationsLogID = SCOPE_IDENTITY() 
			
		 
			--FROM #Contact
			--Move the Inalid records into the table containing Discarded records.
			INSERT INTO UploadRetaillocationsDiscarded( 
			         
														 StoreIdentification
														,RetailID
														,Headquarters
														,Address1
														,Address2
														,Address3
														,Address4
														,City
														,[State]
														,PostalCode					 
														,CountryID
														,RetailLocationURL
														,RetailLocationLatitude
														,RetailLocationLongitude
														,RetailLocationTimeZone
														,PhoneNumber
														,DateCreated
														,DateModified
														,RetailLocationStoreHours	
														,UploadRetaillocationsLogID		
														,RetailKeyword
														,ReasonForDiscarding
														,RetailLocationImagePath		
											 )   
			   SELECT StoreIdentification
						,RetailID
						,Headquarters
						,Address1
						,Address2
						,Address3
						,Address4
						,City
						,[State]
						,PostalCode					 
						,CountryID
						,RetailLocationURL
						,RetailLocationLatitude
						,RetailLocationLongitude
						,RetailLocationTimeZone
						,PhoneNumber
						,GETDATE()
						,DateModified
						,RetailLocationStoreHours
						,@UploadRetailLocationsLogID
						,RetailKeyword
						,ReasonForDiscarding
						,RetailLocationImagePath
			   FROM #InValid
			   WHERE UserID = @UserID
			   AND RetailID = @RetailerID		
			   
			     
			   
			   
			   --Confirmation of success.
			SELECT @Status = 0
			--To generate xml for AX Dynamics
			SELECT RL.RetailLocationID 
					,V.StoreIdentification
					,V.RetailID
					,V.Headquarters
					,V.Address1
					,V.Address2
					,V.Address3
					,V.Address4
					,V.City
					,V.[State]
					,V.PostalCode
					,V.PhoneNumber
					,V.RetailLocationURL
					,V.RetailLocationLatitude
					,V.RetailLocationLongitude
					,V.RetailLocationTimeZone
					,V.DateCreated
					,V.DateModified
					,V.RetailLocationStoreHours	
					,V.RetailKeyword
					,V.RetailLocationImagePath		   
            FROM #Valid V
	        INNER JOIN RetailLocation RL ON V.RetailID = RL.RetailID AND V.StoreIdentification = RL.StoreIdentification AND RL.Active = 1  
									
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadRetailerLocations.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'			
			ROLLBACK TRANSACTION;
						
			--To capture Log info.
			
		INSERT INTO UploadRetaillocationsLog (
												UserID 
											  , RetaillocationFileName
											  , RetaillocationTotalRowsProcessed
											  , RetaillocationSuccessfulRowCount
											  , RetaillocationFailureRowCount
											  , Remarks
											  , ProcessDate
											  , ProcessStartDate
											  , ProcessEndDate
											
											)	
								VALUES(
											@UserID
										  , @RetailLocationFileName
										  , 0
										  , 0
										  , 0
										  , @ErrorMessage
										  , GETDATE()
										  , GETDATE()
										  , GETDATE()										   
									   )				
			
												      
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
