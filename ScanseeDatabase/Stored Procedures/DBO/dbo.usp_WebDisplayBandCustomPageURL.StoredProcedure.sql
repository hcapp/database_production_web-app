USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayBandCustomPageURL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayRetailerCustomPageURL
Purpose					: To Display the Retailer Custom Page URLs.
Example					: usp_WebDisplayRetailerCustomPageURL

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayBandCustomPageURL]
(
	  
	  @UserID int
	, @RetailID int  
	, @PageID int  
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	     --To Get App Media Server Configuration 
		 DECLARE @Config varchar(50)    
		 SELECT @Config=ScreenContent    
		 FROM AppConfiguration     
		 WHERE ConfigurationType='App Media Server Configuration'
		 
		 DECLARE @RetailerConfig varchar(50)
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='QR Code Configuration'
		 AND Active = 1
		 		 	 
		SELECT 
			   QRTypeCode
		     , QRRCP.BandID		       
		     , QRRCP.QRBandCustomPageID  
		    -- , COUNT(QRRCPA.RetailLocationID) Counts
		INTO #Temp
		FROM QRBandCustomPage QRRCP  
		INNER JOIN QRTypes QRT ON QRT.QRTypeID = QRRCP.QRTypeID
		LEFT JOIN QRBandCustomPageAssociation QRRCPA ON QRRCP.QRBandCustomPageID = QRRCPA.QRBandCustomPageID
		WHERE QRRCPA.QRBandCustomPageID = @PageID
		AND QRRCP.BandID = @RetailID
		GROUP BY QRTypeCode
		     , QRRCP.BandID		     
		     , QRRCP.QRBandCustomPageID  		
		  
		SELECT DISTINCT  QRRCP.Pagetitle AS retPageTitle
					   , QRRCP.PageDescription AS retPageDescription
					   , QRRCP.ShortDescription AS retShortDescription
					   , QRRCP.LongDescription AS retLongDescription  
					   --If the user has selected the upload pdf or images or the link to existing then follow the SSQR template to construct the URL.
					   , qrUrl = CASE WHEN QRRPM.MediaPath LIKE '%pdf' OR QRRPM.MediaPath LIKE '%png' OR QRRCP.URL IS NOT NULL
								 THEN --CASE WHEN Counts > 1 THEN 
								 @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.BandID AS VARCHAR(10))+ '&retlocId=0&pageId=' + CAST(T.QRBandCustomPageID AS VARCHAR(10)) +'&EL=true'
										  -- WHEN COUNTS = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.BandID AS VARCHAR(10))+ '&retlocId=' + CAST(RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRBandCustomPageID AS VARCHAR(10))+'&EL=true'
									--  END 
								 ELSE			
								 --If the user has selected "Build your own" while creating the page then follow the SSQR template to construct the URL.
									  --CASE WHEN Counts > 1 THEN 
									  @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.BandID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(T.QRBandCustomPageID AS VARCHAR(10))+ '&EL=false'
										--   WHEN Counts = 1 THEN @RetailerConfig + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(T.BandID AS VARCHAR(10)) + '&retlocId='  + CAST(RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(T.QRBandCustomPageID AS VARCHAR(10))+'&EL=false'
									  --END
								 END
		               ,@Config+'mailer_facebook.png' facebookimg
					   ,@Config+'mailer_scansee.png' scanseeimg
					   ,@Config+'mailer_twitter.png' twitterimg
					   ,@Config+'mailer_arrowscansee.png' arrowscanseeimg
		FROM #Temp T
		LEFT JOIN QRBandCustomPageAssociation QRRCPA ON T.QRBandCustomPageID = QRRCPA.QRBandCustomPageID
		INNER JOIN QRBandCustomPage QRRCP ON QRRCPA.QRBandCustomPageID = QRRCP.QRBandCustomPageID
		LEFT OUTER JOIN QRBandCustomPageMedia QRRPM ON QRRPM.QRBandCustomPageID = T.QRBandCustomPageID
				
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
