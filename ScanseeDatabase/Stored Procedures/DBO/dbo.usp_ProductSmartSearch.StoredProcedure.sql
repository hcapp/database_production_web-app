USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductSmartSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ProductSmartSearch
Purpose					: 
Example					: usp_ProductSmartSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ProductSmartSearch]
(
	 @ProdSearch varchar(500)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		--DECLARE @SearchKey VARCHAR(1000)		
		--SELECT @SearchKey = REPLACE(@ProdSearch,' ', ' AND ')
		
		--To get Matched Product name.
		SELECT TOP 50 ProductID productId
			   , ProductName productName			  
		FROM vw_Product
		WHERE ProductName LIKE '%' + @ProdSearch + '%'
		ORDER BY ProductName
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ProductSmartSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
