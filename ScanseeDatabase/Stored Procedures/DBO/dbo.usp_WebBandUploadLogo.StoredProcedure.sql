USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandUploadLogo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandUploadLogo
Purpose					: To Upload the Band Logo.
Example					: usp_WebBandUploadLogo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		  04/18/2016	Prakash Chinnasamy	 Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandUploadLogo]
(
	 	  
	  @RetailerID int
	, @RetailerLogo varchar(1000)
	  
	--Output Variable 
	, @FindClearCacheURL VARCHAR(1000) OUTPUT
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			UPDATE Band 
			SET BandImagePath = replace(@RetailerLogo,' ','')
			  , WebsiteSourceFlag = 1
			WHERE BandID = @RetailerID AND BandActive = 1
			
			-------Find Clear Cache URL---4/4/2016--------
			
			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL =  Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL =  Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
		  

			--Confirmation of Success.
			SELECT @Status = 0

		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandUploadLogo.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
			values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
