USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUserLoginDetails_Band]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebUserLoginDetails]
Purpose					: To Display user details.
Example					: [usp_WebUserLoginDetails]

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			26/04/2016	    Prakash C		  1.0
---------------------------------------------------------------
*/

--exec [usp_WebUserLoginDetails_Band] 1,null,null,null

CREATE PROCEDURE [dbo].[usp_WebUserLoginDetails_Band]
(
    --Input variable
	  @RetailID int	
		  
	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			BEGIN TRANSACTION	

			    --Display selected user information
				SELECT DISTINCT U.UserName 
					  ,U.email Email 
				FROM Users U
				INNER JOIN UserBand UR ON UR.UserID  =U.UserID  AND UR.BandID =@RetailID 
				LEFT JOIN BandLocation RL ON RL.BandID =UR.BandID AND RL.Active = 1
				LEFT JOIN BandContact RC ON RC.BandLocationID =RL.BandLocationID AND (CorporateAndStore ='1' OR Headquarters='1') 
				LEFT JOIN Contact C ON C.ContactID =RC.ContactID              
			  --  WHERE (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)				

			--Confirmation of Success
			SELECT @Status = 0
			
	        COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebUserLoginDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
