USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierAttributeDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierAttributeDisplay
Purpose					: To Display the attributes associated to the product.
Example					: usp_WebSupplierAttributeDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03rd Jan 2012	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierAttributeDisplay]
(
	@Productid int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--Get the Attributes of the Product from the attributes Table.
		SELECT  PA.AttributeID prodAttributesID
		       , A.AttributeName prodAttributeName
			   , DisplayValue prodDisplayValue
		FROM ProductAttributes PA
		INNER JOIN Attribute A ON PA.AttributeID = A.AttributeId
		WHERE ProductID=@Productid
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierAttributeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
