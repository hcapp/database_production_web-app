USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListAddTSL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListAddTSL
Purpose					: To add the product to TSL direct from Master Shopping List
Example					: usp_MasterShoppingListAddTSL

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListAddTSL]
(
	@UserID int
	, @UserProductID varchar(max)
	, @TodayListAddDate datetime
	
	--Output Variable 
	, @ProductExists int output
	, @Exists int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
				SELECT UserProductID
					   ,ProductID
					   ,TodayListtItem 
				INTO #Prod
				FROM UserProduct UP
					INNER JOIN fn_SplitParam (@UserProductID, ',') P ON P.Param = UP.UserProductID 
				WHERE UserRetailPreferenceID IS NULL
				
				SELECT UserProductID
					   ,ProductID
					   ,TodayListtItem
				INTO #activeProducts
				FROM #Prod
				WHERE TodayListtItem=0
				
				SELECT UserProductID
					   ,ProductID
					   ,TodayListtItem
				INTO #inactiveProducts
				FROM #Prod
				WHERE TodayListtItem=1
				
				UPDATE UserProduct 
				SET TodayListtItem  = 1
					,TodayListAddDate  = @TodayListAddDate 
				FROM UserProduct UP
				INNER JOIN #activeProducts A ON A.UserProductID=UP.UserProductID
				WHERE UserRetailPreferenceID IS NULL
				
				DECLARE @ActiveCount int
				DECLARE @InactiveCount int
				SELECT @ActiveCount=COUNT(*) FROM #activeProducts
				SELECT @InactiveCount=COUNT(*) FROM #inactiveProducts
				
				IF (@ActiveCount<>0) AND (@InactiveCount<>0)
				BEGIN
					SELECT @ProductExists = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
					FROM #inactiveProducts
					WHERE TodayListtItem=1
				END
				
				IF (SELECT COUNT(*) FROM #activeProducts)=0
				BEGIN
					SELECT @Exists = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
					FROM #inactiveProducts
					WHERE TodayListtItem=1
				END
				
				
			
				--UPDATE UserProduct 
				--SET TodayListtItem  = 1
				--	,TodayListAddDate  = @TodayListAddDate 
				--FROM UserProduct UP
				--INNER JOIN fn_SplitParam (@UserProductID, ',') P ON P.Param = UP.UserProductID 
				--WHERE UserRetailPreferenceID IS NULL
							
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListAddTSL.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
