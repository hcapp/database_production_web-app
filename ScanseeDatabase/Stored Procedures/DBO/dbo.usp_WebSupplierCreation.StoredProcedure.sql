USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierCreation
Purpose					: To Register a New Supplier/Manufacturer.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			9th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierCreation]
(

	--Input Input Parameter(s)--
	 
	  @AdminFlag bit
	, @SupplierName varchar(100)
	, @CorporateAddress varchar(50)
	, @Address2 varchar(50)
	, @CityID varchar(30)
	, @StateAbbreviation char(2)	
	, @PostalCode varchar(10)
	, @CorporatePhoneNumber char(10)
	, @LegalAuthorityFirstName varchar(20)
	, @LegalAuthorityLastName varchar(30)
	, @ContactFirstName varchar(30)
	, @ContactLastName varchar(30)
	, @ContactPhoneNumber char(10)
	, @ContactEmail varchar(100)
	, @UserName varchar(100)
	, @Password varchar(60)
	, @BusinessCategory varchar(1000)
	, @NonProfitStatus bit
	
	
	--Output Variable--
	  
	, @Status int output
	, @DuplicateSupplierFlag bit output
	, @DuplicateFlag bit output 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @ManufacturerID INT
		DECLARE @ContactID INT
		DECLARE @UserScopeID INT
		
		--DECLARE @City VARCHAR(100)
		--DECLARE @STATE VARCHAR(50)
		
		--SELECT @STATE = StateName
		--FROM [State]
		--WHERE Stateabbrevation = @StateAbbreviation
		
		--SELECT  @City = @CityID
	 	
		--Input parameter was changed from CityID to City Name, hence commented the code
	/*
		FROM GeoPosition
		WHERE GeoPositionID = @CityID
	*/	
		
		IF NOT EXISTS(SELECT 1 FROM Manufacturer WHERE ManufName LIKE @SupplierName)	
		BEGIN	
				--INSERT INTO USERS TABLE
				IF NOT EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName)
				BEGIN
					INSERT INTO Users (
									  UserName
									, [Password]
									, DateCreated
									
    							  )
    					   VALUES (
    								 @UserName
    							   , @Password
    							   , GETDATE()		    					  
    							  )
		    			     
					SET @UserScopeID = SCOPE_IDENTITY()  
		      
					--INSERT INTO MANUFACTURER TABLE  	
				
					INSERT INTO Manufacturer (
												 ManufName
											   , Address1
											   , Address2
											   , City
											   , [State]
											   , PostalCode
											   , CorporatePhoneNo
											   , LegalAuthorityFirstName
											   , LegalAuthorityLastName
											   , CreateUserID
											   , DateCreated			
											   , IsNonProfitOrganisation						   			                         
											 )
									 VALUES (  @SupplierName
											 , @CorporateAddress
											 , @Address2 
											 , @CityID
											 , @StateAbbreviation	
											 , @PostalCode 
											 , @CorporatePhoneNumber 	
											 , @LegalAuthorityFirstName
											 , @LegalAuthorityLastName
											 , @UserScopeID
											 , GETDATE()		
											 , @NonProfitStatus						
											)
										
					SET @ManufacturerID = SCOPE_IDENTITY()
				
					--INSERT INTO ManufacturerCategory TABLE
				    
					INSERT INTO ManufacturerCategory(ManufacturerID
												   , CategoryID
												   , DateAdded)
										  SELECT @ManufacturerID
											   , [Param]
											   , GETDATE()
										  FROM dbo.fn_SplitParam(@BusinessCategory, ',')
				
					--INSERT INTO CONTACT TABLE
				
					INSERT INTO Contact (
				                   
									   ContactFirstName
									 , ContactLastname
									 , ContactPhone
									 , ContactEmail		             
									 , DateCreated				                     
									 , ContactTitle									
									 , CreateUserID 
								   )
							VALUES (
										@ContactFirstName
									  , @ContactLastName 
									  , @ContactPhoneNumber 
									  , @ContactEmail 			
									  , GETDATE()									  
									  , 1                      --To Be Implemented												
									  , @UserScopeID							     
									)
					SET @ContactID = SCOPE_IDENTITY()
				
					--INSERT INTO ManufacturerContact CHILD TABLE
				
					INSERT INTO ManufacturerContact (
													ManufacturerID
												  , ContactID
												  , DateAdded											
												)
										  VALUES(
													@ManufacturerID
												  , @ContactID
												  , GETDATE()
												)
				
				 --INSERT INTO USERMANUFACTURER CHILD TABLE		   
		      
					INSERT INTO UserManufacturer (
												ManufacturerID
											  , UserID
											  , AdminFlag
											)			 
												
									VALUES (
												@ManufacturerID
											  , @UserScopeID
											  , @AdminFlag
											)
				
					--Confirmation of Success.
				
					SELECT @Status = 0
					SET @DuplicateFlag = 0	
					SET @DuplicateSupplierFlag = 0
			  END
			  ELSE
			  BEGIN
				SET @Status = 0
				SET @DuplicateFlag = 1
				SET @DuplicateSupplierFlag = 0
			  END			  
			END
			ELSE
			BEGIN
				PRINT 'Manufacturer Name Already Exists In The System.'
				SET @DuplicateSupplierFlag = 1
				SET @DuplicateFlag = 0	
				SET @Status = 0
			END
	COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierCreation.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
