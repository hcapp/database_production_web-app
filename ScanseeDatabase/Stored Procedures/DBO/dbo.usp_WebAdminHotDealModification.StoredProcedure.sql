USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminHotDealModification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebAdminHotDealModification]
Purpose					: To Update the details of the existing Hot Deal
Example					: [usp_WebAdminHotDealModification]

History    
Version     Date           Author    Change Description    
---------------------------------------------------------------     
1.0      30th Aug 2013     SPAN      Initial Version    
---------------------------------------------------------------    
*/ 
   
CREATE PROCEDURE [dbo].[usp_WebAdminHotDealModification]
(

	--Input Variables
	  @ProductHotDealID int
	, @HotDealName  varchar(255)
	, @RegularPrice money
    , @SalePrice money    
    , @HotDealShortDescription varchar(100)
    , @HotDealLongDescription varchar(1000)
    , @DealStartDate date
    , @DealStartTime time
    , @DealEndDate date
    , @DealEndTime time
    , @HotDealURL varchar(1000)  

	--Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		-- To calculate Hot Deal percentage
		DECLARE @Percentage float  
		DECLARE @Difference float
		DECLARE @HDOldSalePice Money
		SET @Difference = (@RegularPrice - @SalePrice)
		SET @Percentage = (@Difference*100)/@RegularPrice
	
		BEGIN
		   
		    SELECT @HDOldSalePice=SalePrice 
		    FROM ProductHotDeal 
		    WHERE ProductHotDealID =@ProductHotDealID  
		    --IF (@SalePrice<>@HDOldSalePice)
		    --BEGIN	    
		    
		    
		    
		    
		        SELECT DISTINCT UserID
		              ,ProductID
		              ,DeviceID
		              ,ProductHotDealID		              
		        INTO #Temp
		        FROM (
		        SELECT U.UserID
		              ,UP.ProductID
		              ,UD.DeviceID 
		              ,HD.ProductHotDealID  
		              ,Distance= (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(Latitude  / 57.2958) * COS((Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 
		              ,LocaleRadius 
		        from ProductHotDeal PH
		        INNER JOIN HotDealProduct  HD ON PH.ProductHotDealID =HD.ProductHotDealID AND HD.ProductHotDealID =@ProductHotDealID 	        
		        INNER JOIN ProductHotDealLocation  PL ON PL.ProductHotDealID =HD.ProductHotDealID 
		        --INNER JOIN RetailLocation RL ON RL.RetailLocationID =PRL.RetailLocationID  
		        INNER JOIN UserProduct UP ON UP.ProductID =HD.ProductID AND WishListItem =1	
		        INNER JOIN Users U ON U.UserID =UP.UserID 
		        INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
		        INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
		        INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
		    )Note
		     --WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		    
		    
		        --UPDATE UserPushNotification SET CouponID=NULL,RetailLocationDealID=Null 
		        --FROM UserPushNotification UP
		        --INNER JOIN #Temp T ON T.UserID =UP.UserID AND T.ProductID =UP.ProductID 
		        --WHERE NotificationSent =1 AND (T.ProductHotDealID =UP.ProductHotdealID OR UP.ProductHotdealID IS NULL)
		
		
		    --Update ProductHotDeal table
			UPDATE ProductHotDeal SET HotDealName = @HotDealName
			                        , Price = @RegularPrice
			                        , HotDealDiscountAmount = @Difference
									, HotDealDiscountPct = @Percentage
			                        , SalePrice = @SalePrice
			                        , HotDealShortDescription = @HotDealShortDescription
			                        , HotDeaLonglDescription = @HotDealLongDescription
			                        , HotDealStartDate = CASE WHEN (CAST(@DealStartDate AS DATETIME)+CAST(@DealStartTime AS DATETIME))='1900-01-01 00:00:00.000' 
															  THEN NULL 
															  ELSE (CAST(@DealStartDate AS DATETIME)+CAST(@DealStartTime AS DATETIME)) 
														 END
			                        , HotDealEndDate = CASE WHEN (CAST(@DealEndDate AS DATETIME)+CAST(@DealEndTime AS DATETIME))='1900-01-01 00:00:00.000'
															THEN NULL 
															ELSE (CAST(@DealEndDate AS DATETIME)+CAST(@DealEndTime AS DATETIME))
													   END	
			                        , DateModified = GETDATE() 
			                        , HotDealURL = @HotDealURL                   
			             WHERE ProductHotDealID = @ProductHotDealID
			       
				IF (@SalePrice<>@HDOldSalePice)
		        BEGIN	         
						 --User pushnotification already exist then NotificationSent flag updating to 0
						UPDATE UserPushNotification SET NotificationSent=0
													   ,DateModified =GETDATE()			                                      
						FROM UserPushNotification UP
						INNER JOIN #Temp T ON UP.ProductHotdealID=T.ProductHotDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0
		       
					   --User pushnotification not exist then insert. 
					   INSERT INTO UserPushNotification(UserID
														,ProductID
														,DeviceID												
														,ProductHotdealID												
														,NotificationSent
														,DateCreated)
						SELECT T.UserID 
							  ,T.ProductID 
							  ,T.DeviceID 
							  ,T.ProductHotDealID 
							  ,0
							  ,GETDATE()
						FROM #Temp T				
						LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.ProductHotDealID =UP.ProductHotdealID AND T.ProductID =UP.ProductID AND DiscountCreated=0 
						WHERE UP.UserPushNotificationID IS NULL   		          
		    
		    END	
            	
		END 				            
	
		--Confirmation of Success
		SELECT @Status = 0
		  
		COMMIT TRANSACTION
		  
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebAdminHotDealModification].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
