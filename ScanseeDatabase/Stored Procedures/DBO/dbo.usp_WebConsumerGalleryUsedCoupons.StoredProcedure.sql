USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGalleryUsedCoupons]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_WebConsumerGalleryUsedCoupons  
Purpose					: To display the used coupons.  
Example					: usp_WebConsumerGalleryUsedCoupons  
  
History  
Version  Date				Author		 Change Description  
---------------------------------------------------------------   
1.0		 24th April 2013	Mohith H R	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerGalleryUsedCoupons]  
(  
   
   @UserID int  
 , @RetailerID int
 , @SearchKey varchar(255)
 , @CategoryIDs varchar(1000)
 , @LowerLimit int  
 , @ScreenName varchar(50)  
 , @RecordCount int   
 
  
 --Output Variable   
 , @MaxCnt int output
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 --To get Media Server Configuration.  
  DECLARE @RetailConfig varchar(50)    
  SELECT @RetailConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Retailer Media Server Configuration'   
  
  --To get the row count for pagination.  
  DECLARE @UpperLimit int   
  SELECT @UpperLimit = @LowerLimit + @RecordCount
  
  CREATE TABLE #UsedCoupons(Row_Num INT IDENTITY(1, 1)
					  , UserCouponGalleryID int
					  , CouponID INT 
					  , CouponName VARCHAR(255)
					  , CouponDiscountType VARCHAR(255)
					  , CouponDiscountAmount MONEY
					  , CouponDiscountPct FLOAT
					  , CouponDescription VARCHAR(1000) 
					  , CouponDateAdded DATETIME
					  , CouponStartDate DATETIME
					  , CouponExpireDate DATETIME
					  , CouponURL VARCHAR(1000)
					  , CouponImagePath VARCHAR(1000)
					  , CategoryID int
					  , CategoryName varchar(100)
					  , UsedFlag BIT
					  , ViewableOnWeb BIT)
   --If there is no category selected then display all the coupons.						  
  IF @CategoryIDs = '0'  
  BEGIN	
	print 'No Category'
	--If no Retailer selected 
	IF @RetailerID = 0
	BEGIN	      
		   print 'No Retailer'
		   INSERT INTO #UsedCoupons(UserCouponGalleryID
								    ,CouponID   
									,CouponName  
									,CouponDiscountType  
									,CouponDiscountAmount  
									,CouponDiscountPct  
									,CouponDescription 							
									,CouponDateAdded  
									,CouponStartDate  
									,CouponExpireDate  
									,CouponURL  
									,CouponImagePath  
									,UsedFlag  
									,ViewableOnWeb
									,CategoryID
									,CategoryName)
		   SELECT DISTINCT UserCouponGalleryID
				, uc.CouponID   
				, CouponName  
				, CouponDiscountType  
				, CouponDiscountAmount  
				, CouponDiscountPct  
				, CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
				, CouponDateAdded  
				, CouponStartDate  
				, CouponExpireDate  
				, CouponURL  
				, CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																													CASE WHEN C.WebsiteSourceFlag = 1 
																														THEN @RetailConfig
																														+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																														+CouponImagePath 
																													ELSE CouponImagePath 
																													END
																											 END 
				   				 END  
				, UsedFlag  
				, ViewableOnWeb  
				, CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
			    , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName  END
		   FROM Coupon c  
		   INNER JOIN UserCouponGallery UC ON C.couponid=UC.CouponID AND UserID=@Userid 
		   LEFT JOIN CouponProduct CP ON CP.CouponID = C.CouponID
		   LEFT JOIN ProductCategory PC ON PC.ProductID = CP.ProductID
		   LEFT JOIN Category Cat ON Cat. CategoryID = PC.CategoryID 
		   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
		  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate AND UsedFlag=1  
		  AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END	
		  ORDER BY CategoryName, C.CouponName ASC
	END
	
	ELSE
	BEGIN
		print 'Retailer'
		INSERT INTO #UsedCoupons(UserCouponGalleryID
								    ,CouponID   
									,CouponName  
									,CouponDiscountType  
									,CouponDiscountAmount  
									,CouponDiscountPct  
									,CouponDescription 							
									,CouponDateAdded  
									,CouponStartDate  
									,CouponExpireDate  
									,CouponURL  
									,CouponImagePath  
									,UsedFlag  
									,ViewableOnWeb
									,CategoryID
									,CategoryName)
		   SELECT DISTINCT UserCouponGalleryID
				, uc.CouponID   
				, CouponName  
				, CouponDiscountType  
				, CouponDiscountAmount  
				, CouponDiscountPct  
				, CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
				, CouponDateAdded  
				, CouponStartDate  
				, CouponExpireDate  
				, CouponURL  
				, CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																													CASE WHEN C.WebsiteSourceFlag = 1 
																														THEN @RetailConfig
																														+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																														+CouponImagePath 
																													ELSE CouponImagePath 
																													END
																											 END 
				   				 END  
				, UsedFlag  
				, ViewableOnWeb  
				, CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
			    , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+ Cat.SubCategoryName  END
		   FROM Coupon c  
		   INNER JOIN UserCouponGallery UC ON C.couponid=UC.CouponID AND UserID=@Userid 
		   LEFT JOIN CouponProduct CP ON CP.CouponID = C.CouponID
		   LEFT JOIN ProductCategory PC ON PC.ProductID = CP.ProductID
		   LEFT JOIN Category Cat ON Cat. CategoryID = PC.CategoryID 
		   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
		  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate AND UsedFlag=1  
		  AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END	
		  AND CR.RetailID = @RetailerID
		  ORDER BY CategoryName, C.CouponName ASC
	END

  END
	
	--If a category id is input then filter based on the categories associated to the products that belong to the coupon.
	IF @CategoryIDs <> '0'
	BEGIN
		 print 'Category'
		 --If Retailer Not Selected		 
		 IF @RetailerID = 0
		 BEGIN		 
		     print 'No Retailer'
		 	 INSERT INTO #UsedCoupons(UserCouponGalleryID
		 								,CouponID   
										,CouponName  
										,CouponDiscountType  
		 								,CouponDiscountAmount  
		 								,CouponDiscountPct  
		 								,CouponDescription 							
										,CouponDateAdded  
										,CouponStartDate  
										,CouponExpireDate  
										,CouponURL  
										,CouponImagePath  
										,UsedFlag  
										,ViewableOnWeb
										,CategoryID
										,CategoryName)
			   SELECT DISTINCT UserCouponGalleryID
					, UC.CouponID
					, CouponName  
					, CouponDiscountType  
					, CouponDiscountAmount  
					, CouponDiscountPct  
					, CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
					, CouponDateAdded  
					, CouponStartDate  
					, CouponExpireDate  
					, CouponURL  
					, CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																														CASE WHEN C.WebsiteSourceFlag = 1 
																															THEN @RetailConfig
																															+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																															+CouponImagePath 
																														ELSE CouponImagePath 
																														END
																												 END 
				   					 END  
					, UsedFlag  
					, ViewableOnWeb 
					, CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
					, CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END
			   FROM Coupon c  
			   INNER JOIN UserCouponGallery UC ON C.couponid=UC.CouponID AND UserID=@Userid  
			   INNER JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			   INNER JOIN Product P ON P.ProductID = CP.ProductID
			   INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			   INNER JOIN Category Cat ON CAT.CategoryID = PC.CategoryID
			   INNER JOIN dbo.fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
			   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
			   WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate AND UsedFlag=1  
			   AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
			   ORDER BY CategoryName, C.CouponName ASC
		END
		
		ELSE
		BEGIN		 
			--If the Retailer is selected
		     print 'Retailer'
		 	 INSERT INTO #UsedCoupons(UserCouponGalleryID
		 								,CouponID   
										,CouponName  
										,CouponDiscountType  
		 								,CouponDiscountAmount  
		 								,CouponDiscountPct  
		 								,CouponDescription 							
										,CouponDateAdded  
										,CouponStartDate  
										,CouponExpireDate  
										,CouponURL  
										,CouponImagePath  
										,UsedFlag  
										,ViewableOnWeb
										,CategoryID
										,CategoryName)
			   SELECT DISTINCT UserCouponGalleryID
					, UC.CouponID
					, CouponName  
					, CouponDiscountType  
					, CouponDiscountAmount  
					, CouponDiscountPct  
					, CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
					, CouponDateAdded  
					, CouponStartDate  
					, CouponExpireDate  
					, CouponURL  
					, CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																														CASE WHEN C.WebsiteSourceFlag = 1 
																															THEN @RetailConfig
																															+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																															+CouponImagePath 
																														ELSE CouponImagePath 
																														END
																												 END 
				   					 END  
					, UsedFlag  
					, ViewableOnWeb 
					, CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
					, CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END
			   FROM Coupon c  
			   INNER JOIN UserCouponGallery UC ON C.couponid=UC.CouponID AND UserID=@Userid  
			   INNER JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			   INNER JOIN Product P ON P.ProductID = CP.ProductID
			   INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			   INNER JOIN Category Cat ON CAT.CategoryID = PC.CategoryID
			   INNER JOIN dbo.fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
			   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
			   WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate AND UsedFlag=1  
			   AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
			   AND CR.RetailID = @RetailerID
			   ORDER BY CategoryName, C.CouponName ASC
		END
		
	END
    
  --To capture max row number.  
  SELECT @MaxCnt = ISNULL(MAX(Row_Num),0) FROM #UsedCoupons  
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
  SELECT  Row_Num rowNum  
    ,UserCouponGalleryID userCouponGalleryID   
    ,CouponID   
    ,CouponName  
    ,CouponDiscountType  
    ,CouponDiscountAmount  
    ,CouponDiscountPct  
    ,CouponDescription
    ,CouponDateAdded coupDateAdded 
    ,CouponStartDate coupStartDate 
    ,CouponExpireDate coupExpireDate
    ,CouponURL  
    ,CouponImagePath
    ,UsedFlag favFlag
    ,ViewableOnWeb 
    ,CategoryID cateId
	,CategoryName cateName
  FROM #UsedCoupons  
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
  Order by Row_Num  
 
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerGalleryUsedCoupons.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
