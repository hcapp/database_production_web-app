USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayRetailLocationProductInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayRetailLocationProductInfo
Purpose					: usp_WebDisplayRetailLocationProductInfo
Example					: usp_WebDisplayRetailLocationProductInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Feb 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayRetailLocationProductInfo]
(

	--Input Input Parameter(s)--
	   
	  
	   @ProductID int 
	 , @RetailerID int
	 , @RetailLocationID int 
	
	--Output Variable--	  

	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		DECLARE @CategoryID int
	
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration' 
		
		SELECT DISTINCT P.ProductName		   
		     , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND P.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+  ProductImagePath
										ELSE ProductImagePath END
		     , ProductMediaPath = CASE WHEN PM.ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																						   THEN @Config
																						   +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																						   +PM.ProductMediaPath 
																						 ELSE @AppConfig+PM.ProductMediaPath 
																						 END   
										  ELSE PM.ProductMediaPath END 
		     , P.ProductShortDescription as shortDescription
		     , P.ProductLongDescription  as longDescription
		     , PA.AttributeName
		     , PA.DisplayValue
		     , P.WarrantyServiceInformation
		     , P.ScanCode
		     , CASE WHEN RL.RetailLocationID = RLP.RetailLocationID THEN RLP.Price ELSE P.SuggestedRetailPrice END Price
		     , P.ModelNumber
		     , P.ProductExpirationDate as prodExpirationDate
		     , PC.CategoryID 
		     , C.ParentCategoryName + ' - ' + C.SubCategoryName as categoryName 
		FROM Product P 	
		LEFT JOIN RetailLocationProduct RLP ON P.ProductID=RLP.ProductID AND RLP.RetailLocationID = @RetailLocationID 
		LEFT JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID AND RL.RetailID = @RetailerID AND RL.Active = 1 			
		LEFT JOIN ProductAttributes PA ON P.ProductID = PA.ProductID
		LEFT JOIN ProductMedia PM ON P.ProductID = PM.ProductID
		LEFT JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = PM.ProductMediaTypeID
		LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
		LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
		WHERE P.ProductID = @ProductID  
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
