USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_RateReviewDispaly]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RateReviewDispaly
Purpose					: To dispaly Product Reviews
Example					: usp_RateReviewDispaly

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RateReviewDispaly]
(
	@ProductID int
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--To get Product's Review
		SELECT A.APIPartnerName 
			, PR.ReviewComments 
			, PR.ReviewURL  
		FROM ProductReviews PR
		INNER JOIN APIPartner A ON A.APIPartnerID = PR.APIPartnerID
		WHERE ProductID = @ProductID
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_RateReviewDispaly.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
