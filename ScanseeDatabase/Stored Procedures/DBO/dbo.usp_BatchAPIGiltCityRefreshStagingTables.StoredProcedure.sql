USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchAPIGiltCityRefreshStagingTables]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchAPIGiltCityRefreshStagingTables]
Purpose					: To refersh the Stage Table
Example					: [usp_BatchAPIGiltCityRefreshStagingTables]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0		 31th July 2013	     SPAN	    Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchAPIGiltCityRefreshStagingTables]
(
	
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		TRUNCATE TABLE APIGiltCityData
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_BatchAPIGiltCityRefreshStagingTables].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 		
		END;
		 
	END CATCH;
END;

GO
