USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierProductQRCodeCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierProductQRCodeCreation
Purpose					: To Create a Product QR code. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierProductQRCodeCreation]
(

	  @UserID int
	, @ProductID int
	, @AudioID int
	, @VideoID int
	, @FileLink varchar(1000)
	
	--Output Variable 	
	, @ProductPageID int output
	, @AudioPageID int output
	, @VideoPageID int output
	, @FileLinkPageID int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			DECLARE @QRProductID INT
			
		   		INSERT INTO QRProduct(
									  ProductID
									, QRProductTypeID
									, DateCreated)
							VALUES( @ProductID
								  , (SELECT QRProductTypeID FROM QRProductType WHERE QRProductTypeDescription LIKE 'Product Page')
								  , GETDATE())
								  
						SET @QRProductID = SCOPE_IDENTITY()
						SET @ProductPageID = @QRProductID
						
						--Check if there exists audio.
						IF(@AudioID IS NOT NULL)
						BEGIN
							INSERT INTO QRProduct(
									  ProductID
									, QRProductTypeID
									, DateCreated)
							VALUES( @ProductID
								  , (SELECT QRProductTypeID FROM QRProductType WHERE QRProductTypeDescription LIKE 'Product Audio')
								  , GETDATE())
								  
							SET @QRProductID = SCOPE_IDENTITY()
							SET @AudioPageID = @QRProductID
							
							INSERT INTO QRProductMedia(	  QRProductID
														, ProductMediaID													 
														, DateCreated)
											VALUES(
													  @QRProductID
													, @AudioID
													, GETDATE()
												   )
						END
						
						--Check if there exists a video.
						IF(@VideoID IS NOT NULL)
						BEGIN
							INSERT INTO QRProduct(
									  ProductID
									, QRProductTypeID
									, DateCreated)
							VALUES( @ProductID
								  , (SELECT QRProductTypeID FROM QRProductType WHERE QRProductTypeDescription LIKE 'Product Video')
								  , GETDATE())
								  
							SET @QRProductID = SCOPE_IDENTITY()
							SET @VideoPageID = @QRProductID
							
							INSERT INTO QRProductMedia(	  QRProductID
														, ProductMediaID													 
														, DateCreated)
											VALUES(
													  @QRProductID
													, @VideoID
													, GETDATE()
												   )
						END
						
						--Check if there exists a file link.
						IF(@FileLink IS NOT NULL)
						BEGIN
							INSERT INTO QRProduct(
									  ProductID
									, QRProductTypeID
									, DateCreated)
							VALUES( @ProductID
								  , (SELECT QRProductTypeID FROM QRProductType WHERE QRProductTypeDescription LIKE 'File Link')
								  , GETDATE())
								  
							SET @QRProductID = SCOPE_IDENTITY()
							SET @FileLinkPageID = @QRProductID
							
							INSERT INTO QRProductMedia(	  QRProductID
														, QRMediaURL													 
														, DateCreated)
											VALUES(
													  @QRProductID
													, @FileLink
													, GETDATE()
												   )
						END
  								
			 
			 --Confirmation of Success.
			 SET @Status = 0		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierProductQRCodeCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

--SELECT * FROM UserRetailer U INNER JOIN Users US ON U.UserID = US.UserID


GO
