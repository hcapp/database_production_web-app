USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_CommissionJunctionBatchLogFiles]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_CommissionJunctionBatchLogFiles 
Purpose                 : To get the status of the batch process for the day along with the file name that was processed for commission junction.
Example                 : usp_CommissionJunctionBatchLogFiles  
  
History  
Version           Date				  Author           Change Description  
-----------------------------------------------------------------------   
1.0               14th March 2013     SPAN   Initial Version  
-----------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_CommissionJunctionBatchLogFiles]     
(  
        @APIPartnerName varchar(100)
      , @Date DATETIME   
      , @BatchNumber int
      --OutPut Variable  
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY 
		
		--To get the status of the batch process for the day along with the file name that was processed.
		SELECT APILogID
		     , REPLACE(CONVERT(VARCHAR(10), ExecutionDate, 102),'.', '-') +' '+ CONVERT(VARCHAR(10), ExecutionDate, 108) ExecutionDate
			 , Status
			 , Reason
			 , APIPartnerName BatchProcessName
			 , ISNULL(APIRowCount, 0) RowsRecieved
			 , ISNULL(ProcessedRowCount, 0) RowsProcessed
			 , APIPartnerID
			 , ISNULL(DuplicatesCount, 0) DuplicatesCount
			 , ISNULL(ExpiredCount, 0) ExpiredCount
			 , ISNULL(MandatoryFieldsMissingCount, 0) MandatoryFieldsMissingCount
			 , ProcessedFileName 
			 , ISNULL(ExistingRecordCount, 0) ExistingRecordCount
			 , ISNULL(InvalidRecordsCount, 0) InvalidRecordsCount			 
		FROM APIBatchLog 
		WHERE CONVERT(DATE,ExecutionDate) = @Date 
		AND ((@APIPartnerName <> 'All' AND APIPartnerName = @APIPartnerName))
		AND BatchNumber=@BatchNumber
			 -- OR
			 --(@APIPartnerName = 'All' AND APIPartnerName  IN ('Living Social'
				--										   , 'Plum District'
				--										   , 'CommissionJunction'
				--										   , 'Mamasource'
				--										   , 'Restaurant.com'
				--										   , 'Half Off Depot')))  
		
      
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_BatchLogFiles.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;
GO
