USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterValueCreationAndUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterValueCreationAndUpdation]
Purpose                  : To Create Filter Values.
Example                  : [usp_WebAdminFilterValueCreationAndUpdation]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterValueCreationAndUpdation]
(

      --Input Input Parameter(s)--       
        @UserID INT
	  , @FilterValueID Int
	  , @FilterValue VARCHAR(500)
      
      
      --Output Variable--	
	  , @AdminFilterID INT OUTPUT
	  , @DuplicateValue BIT OUTPUT 
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY

             SET @AdminFilterID=0
			 SET @DuplicateValue=0

	         -- To check Filter Values already exists.
		     IF EXISTS(SELECT 1 FROM AdminFilterValue WHERE (@FilterValueID IS NULL AND @FilterValue =FilterValueName) OR (@FilterValueID IS NOT NULL AND @FilterValueID<>AdminFilterValueID AND @FilterValue =FilterValueName))
			 BEGIN
				SET @DuplicateValue=1
			 END

			 ELSE
			 BEGIN
				IF @FilterValueID IS NULL
					BEGIN
						INSERT INTO AdminFilterValue(FilterValueName
													,DateCreated
													,CreatedUserID)
						VALUES(@FilterValue,GETDATE(),@UserID)

						SET @AdminFilterID=SCOPE_IDENTITY()
					END
				ELSE
				BEGIN
					UPDATE AdminFilterValue SET FilterValueName =@FilterValue 
					                           ,DateModified =GETDATE()
											   ,ModifiedUserID =@UserID 
					WHERE AdminFilterValueID =@FilterValueID 
				END

			 END

        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterValueCreationAndUpdation.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;



GO
