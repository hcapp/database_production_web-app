USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsInsertion_Rewritten]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
StORed Procedure name	: [usp_WebRssFeedNewsInsertion]
Purpose					: To insert into RssFeedNews from Staging table.
Example					: [usp_WebRssFeedNewsInsertion]

HistORy
Version		Date			AuthOR			Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015		Mohith H R	        1.1
1.1         07 Dec 2015     Suganya C           Duplicate news removal
1.2							Sagar Byali			Video deletion changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedNewsInsertion_Rewritten]
(

	--Output Variable 
	  @Status bit Output
	, @ErrORNumber int output
	, @ErrORMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		


		--Deleting 30 days older News fOR 'spanqa.regionsapp'.
		DECLARE  @ExpiryDate datetime = GETDATE()-30,
				 @ExpiryDateAllCategORyNews datetime,
				 @ExpiryDateVideos datetime,
				 @ExpiryPublishedDateKilleen datetime = GETDATE() - 5
				

		SELECT @ExpiryDateAllCategORyNews = DATEADD(d,-14,max(convert(date,PublishedDate)))
		from RssFeedNewsStagingTable 
		where NewsType = 'all' AND HcHubCitiID = 10
		
		SELECT @ExpiryDateVideos = max(convert(date,DateCreated)) 
		from RssFeedNewsStagingTable 
		where NewsType='videos' AND HcHubCitiID = 10
		
		BEGIN TRAN

		DELETE FROM RssFeedNewsStagingTable
		WHERE (convert(date,PublishedDate) < @ExpiryDate AND HcHubCitiID = 28) OR (convert(date,PublishedDate) < @ExpiryDate AND HcHubCitiID = 10 AND NewsType NOT IN ('Videos','all')) OR (convert(date,PublishedDate) < @ExpiryDateAllCategORyNews AND HcHubCitiID = 10 AND NewsType = 'all') OR (convert(date,DateCreated) <> @ExpiryDateVideos AND HcHubCitiID = 10 AND NewsType = 'Videos') OR (convert(date,PublishedDate) < @ExpiryPublishedDateKilleen AND HcHubCitiID = 52)
		
		 --Fetching unique, latest data from RssFeedNewsStagingTable to insert into RssFeedNews
		 SELECT DISTINCT HcHubCitiID, NewsType, Title,Classification, MAX(RssFeedNewsID) AS MaxRssFeedNewsID
         INTO #Temp
         FROM RssFeedNewsStagingTable
         GROUP BY HcHubCitiID,NewsType, Title, Classification
		
		 --Insert into RssFeedNews
		 INSERT INTO RssFeedNews(Title
								,ImagePath
								,ShORtDescription
								,LongDescription
								,Link
								,PublishedDate
								,NewsType
								,Message
								,HcHubCitiID
								,Classification
								,Section
								,AdCopy
								,VideoLink)															
						SELECT  DISTINCT RS.Title
								,RS.ImagePath
								,RS.ShORtDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy
								,RS.VideoLink		
						FROM RssFeedNewsStagingTable RS 
						INNER JOIN #Temp T ON RS.HcHubCitiID = T.HcHubCitiID 
						AND RS.NewsType = T.NewsType 
						AND ((RS.NewsType <> 'Classifields' AND RS.Title = T.Title) OR (RS.NewsType = 'Classifields' AND 1=1))
						AND RS.RssFeedNewsID = T.MaxRssFeedNewsID 
		
						
		 DELETE FROM RssFeedNews
		 FROM RssFeedNews RF
		 INNER JOIN RssFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		 WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields') OR (RS.Title IS NULL AND RS.NewsType = 'Classifields')) AND RF.DateCreated <> (SELECT MAX(DateCreated) FROM RssFeedNews)
				
				   
		 SET @Status=0	
		 
		 COMMIT TRAN 				   		
	
	END TRY
		
	BEGIN CATCH
		SET @ErrorNumber = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @Status=1 
	END CATCH;
END;





GO
