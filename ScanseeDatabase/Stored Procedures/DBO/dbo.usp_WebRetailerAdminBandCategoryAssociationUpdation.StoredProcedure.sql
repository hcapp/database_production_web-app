USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminBandCategoryAssociationUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminBandCategoryAssociationUpdation
Purpose					: To update Retailer's business categories.
Example					: usp_WebRetailerAdminBandCategoryAssociationUpdation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/04/2016				SPAN		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminBandCategoryAssociationUpdation]

	--Input parameters	 
	  @RetailID INT	
	, @BusinessCategoryIDs VARCHAR(MAX)
	, @BusinessSubCategoryIDs VARCHAR(1000)
	, @UserID int

	--Output variables 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
		DELETE FROM BandCategoryAssociation WHERE BandID = @RetailID

		--select  *  from dbo.fn_SplitParam(@BusinessCategoryIDs, ',')

		INSERT INTO BandCategoryAssociation(BandID										
										 ,BandCategoryID
										 ,DateModified)
								SELECT	@RetailID									 
										,Param
										,GETDATE()									
								FROM dbo.fn_SplitParam(@BusinessCategoryIDs, ',')

									

								If @BusinessSubCategoryIDs is not null
								

					begin
					DELETE FROM BandCategoryAssociation WHERE BandID = @RetailID
					--select  *  from  dbo.fn_SplitParam(@BusinessSubCategoryIDs, ',')

								INSERT INTO BandCategoryAssociation( BandID
									, BandSubCategoryID
									, DateCreated 
									, BandCategoryID)
								SELECT @RetailID
								, Param
								, GETDATE()
								, BC.BandCategoryID
								FROM dbo.fn_SplitParam(@BusinessSubCategoryIDs, ',') F
								INNER JOIN BandSubCategory SC ON F.Param = SC.BandSubCategoryID
								INNER JOIN BandSubCategoryType T ON SC.BandSubCategoryTypeID = T.BandSubCategoryTypeID
								left JOIN BandCategory BC ON T.BandCategoryID = BC.BandCategoryID
					end

					INSERT INTO BandCategoryAssociation(BandID										
										 ,BandCategoryID
										 ,DateModified)
								SELECT	@RetailID									 
										,Param
										,GETDATE()									
								FROM dbo.fn_SplitParam(@BusinessCategoryIDs, ',')
								WHERE Param  NOT  IN(SELECT BandCategoryID  FROM BandCategoryAssociation WHERE BandID=@RetailID)
								



		-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
							 
		--Confirmation of failure.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminBandCategoryAssociationUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
