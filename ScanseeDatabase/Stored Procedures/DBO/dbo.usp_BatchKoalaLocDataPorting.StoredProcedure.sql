USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchKoalaLocDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchKoalaLocDataPorting
Purpose					: To move Location data from Stage Koala Map table to Production ProductHotDealLocation Table
Example					: usp_BatchKoalaLocDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th Sep 2011	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchKoalaLocDataPorting]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			MERGE ProductHotDealLocation AS T
			USING (SELECT AD.ID
						, HD.ProductHotDealID 
						, AD.City
				   FROM APIKoalaData AD
						INNER JOIN ProductHotDeal HD ON HD.SourceID = AD.ID ) AS S
			ON (T.ProductHotDealID = S.ProductHotDealID) 
			WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
				THEN INSERT([ProductHotDealID]
						   ,[City])			   
					 VALUES(S.[ProductHotDealID]
						   ,S.[City])
			WHEN MATCHED  
				THEN UPDATE SET T.[City] = S.[City];
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchKoalaLocDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;
-----------------------------------------------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[usp_BatchKoalaDataPorting]    Script Date: 07/17/2012 02:00:11 ******/
SET ANSI_NULLS ON

GO
