USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListUnassignedProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListUnassignedProducts
Purpose					: To add Unassigned Product to User's Wish List.
Example					: usp_WishListUnassignedProducts 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListUnassignedProducts]
(
	  @UserID int
	, @UnassignedProductName varchar(50)
	, @UnassignedProductDescription varchar(255)
	, @WishListAddDate DATETIME
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To check the existance of the product.
			IF EXISTS (SELECT TOP 1 UserProductID FROM UserProduct WHERE UserID = @UserID AND UnassignedProductName = @UnassignedProductName)
			BEGIN
				UPDATE UserProduct 
				SET WishListItem = 1
					, WishListAddDate  = @WishListAddDate  
				WHERE UserID = @UserID 
					AND UnassignedProductName = @UnassignedProductName  
			END
			ELSE
			BEGIN
			--To Insert Unassigned Products.
			INSERT INTO [UserProduct]
				   ([UserID]
				   ,[ProductID] 
				   ,[MasterListItem]
				   ,[WishListItem]
				   ,[TodayListtItem]
				   ,[ShopCartItem]
				   ,[UnassignedProductName]
				   ,[UnassignedProductDescription]
				   ,[WishListAddDate])
			 VALUES
				   (@UserID 
				   ,0
				   ,0
				   ,1 
				   ,0 
				   ,0 
				   ,@UnassignedProductName 
				   ,@UnassignedProductDescription
				   ,@WishListAddDate)
			END
			--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION
		
		--Confirmation of Success
		SELECT 0
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListUnassignedProducts.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
