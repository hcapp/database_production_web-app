USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEmailSubscription]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchEmailSubscription
Purpose					: To get all the users who have subcribed while registering. 
Example					: usp_BatchEmailSubscription 

Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th July 2013	SPAN		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEmailSubscription]
(
	
	--Output Variable 		
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			BEGIN
			
			SELECT   DISTINCT U.UserID
					,FirstName firstName
					,Lastname lastName
					,UserName
					,Email
					,Address1
					,City
					,State
					,PostalCode
					,MobilePhone
			FROM Users U
			INNER JOIN UserPreference UP ON U.UserID=UP.UserID
			WHERE CAST(DateCreated  AS DATE)>= DATEADD(day,-7, CAST(GETDATE() AS DATE)) AND EmailAlert=1
			ORDER BY UserName
			
			--Confirmation of Success.
			SELECT @Status = 0
			END
			
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchEmailSubscription'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
