USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireRefreshStagingTables]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireRefreshStagingTables
Purpose					: To truncate the CellFire Related staging tables and set the Status of Batch process as "Inprogress" in the AppConfiguration table.
Example					: usp_BatchCellFireRefreshStagingTables

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Oct 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireRefreshStagingTables]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
		
			DECLARE @APIPartnerID INT				
		
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'CellFire'				
			
				
			--Update the status of the Batch as started i.e 1
			UPDATE AppConfiguration
			SET ScreenContent = '1'
			WHERE ConfigurationType = 'CellFire Batch Process Running'			
			
			--Clear Stage tables
			TRUNCATE TABLE APICellFireCouponDetails
			TRUNCATE TABLE APICellFireCouponByUPC		
			TRUNCATE TABLE APICellFireOfferUPCs	
			TRUNCATE TABLE APICellFireOfferMerchants		
			DELETE FROM APICellFireOffers		
			DELETE FROM APICellFireMerchants		
			
			--Create an Entry in the Batch Log and populate the APIRowCount in the first sp where data is moved from stage to production and processed row count in the last procedure of the batch. 
			INSERT INTO APIBatchLog (ExecutionDate
									, APIPartnerID
									, APIPartnerName
									, APIRowCount)
			SELECT GETDATE()
				, @APIPartnerID
				, 'CellFire'
				, COUNT(1)
			FROM APICellFireCouponDetails	
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireRefreshStagingTables.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
