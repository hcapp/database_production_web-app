USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEversaveCategoryXRefUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchEversaveCategoryXRefUpdation
Purpose					: Update the cross reference table.
Example					: usp_BatchEversaveCategoryXRefUpdation

History
Version		Date			Author	  Change Description
--------------------------------------------------------------- 
1.0		30th July 2013	    SPAN	   Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEversaveCategoryXRefUpdation]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		--To update Scansee categoryID in ProductHotDeal Table		
				
			UPDATE ProductHotDeal 
			SET CategoryID = C.CategoryID 
			FROM ProductHotDeal PHD
			INNER JOIN APIPartner A ON A.APIPartnerID = PHD.APIPartnerID AND A.APIPartnerName ='Eversave'						
			INNER JOIN YipitCategoryXRef YC ON YC.YipitCategory = PHD.Category 
			INNER JOIN Category C ON C.ParentCategoryID = YC.ScanSeeParentCategoryID  
			AND C.SubCategoryID = YC.ScanSeeSubCategoryID 
									
		--Confirmation of Success.
			SELECT @Status = 0									
			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchEversaveCategoryXRefUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
