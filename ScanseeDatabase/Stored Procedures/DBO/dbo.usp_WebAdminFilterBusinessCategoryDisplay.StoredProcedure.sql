USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterBusinessCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterBusinessCategoryDisplay]
Purpose                  : To display Filter Business Categories.
Example                  : [usp_WebAdminFilterBusinessCategoryDisplay]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               28th Nov 2014        Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterBusinessCategoryDisplay]
(

      --Input Input Parameter(s)--       
        @UserID INT
      
      
      --Output Variable--	 
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
	         -- To display Filter Business Categories.
		     SELECT DISTINCT B.BusinessCategoryID categoryId
			                ,B.BusinessCategoryName  categoryName
			 FROM AdminfilterBusinessCategory A 
			 INNER JOIN BusinessCategory B ON A.BusinessCategoryID =B.BusinessCategoryID         
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterBusinessCategoryDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;



GO
