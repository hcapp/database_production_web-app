USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandBannerAdsBandLocationDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name    : [usp_WebBandBannerAdsBandLocationDisplay]
Purpose                  : To display the Band Locations associated to the Banner Ads.
Example                  : [usp_WebBandBannerAdsBandLocationDisplay]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               04/18/2016     Prakash C   Initial Version (usp_WebRetailerBannerAdsRetailLocationDisplay)                                     
-------------------------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebBandBannerAdsBandLocationDisplay]
(

      --Input Input Parameter(s)--  
      
        @AdvertisementBannerID INT
	  --Output Variable--
	  , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY 
      		      		
       --Display The Locations Associated to the Banner Ad
       
              SELECT RL.BandLocationID as RetailerLocationIds
				  ,RL.Address1 As Address1
				  ,RL.StoreIdentification as StoreIdentification
			  FROM BandLocationBannerAd RLB
			  INNER JOIN BandLocation RL ON RLB .BandLocationID =RL.BandLocationID 
			  WHERE RLB.AdvertisementBannerID = @AdvertisementBannerID AND RL.Active = 1  
			  
					        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebBandBannerAdsBandLocationDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
				   insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
               values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())    
                               
                  
            END;
            
      END CATCH;
END;



GO
