USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadRetailerProductsLogCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadRetailerProductsLogCreation
Purpose					: To create an entry in the Log table that holds the summary about the batch process.
Example					: usp_WebUploadRetailerProductsLogCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		25th July 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadRetailerProductsLogCreation]
(
	
	  
	   @UserID int
     , @RetailerID int
     , @FileName varchar(1000)     
 	
	--Output Variable 
	, @UploadRetailLocationProductLogID int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			
			--Create the Log entry.
			INSERT INTO UploadRetailLocationProductLog( UserID
													   ,RetailID
													   ,RetailLocationProductFileName
													   ,RetailLocationProductTotalRowsProcessed
													   ,RetailLocationProductSuccessfulRowCount
													   ,RetailLocationProductFailureRowCount
													   ,ProcessDate
													   ,ProcessStartDate
													   ,ProcessEndDate)
												VALUES(@UserID
													 , @RetailerID
													 , @FileName
													 , 0
													 , 0
													 , 0
													 , GETDATE()
													 , GETDATE()
													 , GETDATE())
													 
			SET @UploadRetailLocationProductLogID = SCOPE_IDENTITY()
			
	--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadRetailerProductsLogCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'			
			ROLLBACK TRANSACTION;				
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
