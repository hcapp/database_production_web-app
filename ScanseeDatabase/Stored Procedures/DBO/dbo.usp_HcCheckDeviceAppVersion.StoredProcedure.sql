USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HcCheckDeviceAppVersion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HcCheckDeviceAppVersion
Purpose					: usp_HcCheckDeviceAppVersion
Example					: usp_HcCheckDeviceAppVersion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12 May 2014		Mohith H R	Initial Version
1.1			26 Oct 2016		Bindu T A	New versions added for Android
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HcCheckDeviceAppVersion]
(	  
	  @AppVersion VARCHAR(10)
	, @RequestPlatformType VARCHAR(60)
	, @HubCitiKey VARCHAR(100)
	
	--Output Variable 	
	, @ForcefulUpdateVersionPrompt bit output
	, @DownLoadLinkIOS Varchar(2000) output
	, @DownLoadLinkAndroid Varchar(2000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY						    		   		   
			
			--To check whether given HubCitiID is RegionApp or not.
			DECLARE @RegionAppID int
			DECLARE @HcAppListID int

			SELECT @HcAppListID = HcAppListID
			FROM HcApplist
			WHERE HcAppListName = 'RegionApp'

			SELECT @RegionAppID = IIF(H.HcAppListID = @HcAppListID,1,0)
			FROM HcHubCiti H
			WHERE HcHubCitiKey = @HubCitiKey

			--Initialization					
			SET @ForcefulUpdateVersionPrompt = 0			
			
			--select @RequestPlatformType,@RegionAppID

			--SELECT @AppVersion

			--select @RegionAppID
			IF (@RegionAppID = 0)
			BEGIN
				--Check if the version running on the device is outdated/expired & prompt the IOS user accordingly for HubCiti.
				IF @RequestPlatformType = 'IOS'
				BEGIN
					--SELECT 'A'
					IF (@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										  
												  WHERE ConfigurationType = 'YetToReleaseHubCitiAppVersionIOS' AND ScreenName = 'YetToReleaseHubCitiAppVersionIOS' AND Active = 1))
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'CurrentHubCitiAppVersionIOS' AND ScreenName = 'CurrentHubCitiAppVersionIOS' AND Active = 1))	
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportHubCitiAppVersionIOS' AND ScreenName = 'SupportHubCitiAppVersionIOS' AND Active = 1))		
												  
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportHubCitiAppVersionIOS_Perf' AND ScreenName = 'SupportHubCitiAppVersionIOS_Perf' AND Active = 1))	
												  
												  AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportHubCitiAppVersionIOS_Perf1' AND ScreenName = 'SupportHubCitiAppVersionIOS_Perf1' AND Active = 1))				
					BEGIN

					--select 'hh'
						SET @ForcefulUpdateVersionPrompt = 1
						SELECT @DownLoadLinkIOS = DownloadLinkIOS
						FROM HcHubCiti HC
						WHERE HC.HcHubCitiKey=@HubCitiKey  
					END
					ELSE 
					BEGIN
						SET @ForcefulUpdateVersionPrompt = 0
						SET @DownLoadLinkIOS = NULL
					END
				END
				ELSE IF @RequestPlatformType = 'Android'
				BEGIN
				--SELECT 'andriod'

					--Check if the version running on the device is outdated/expired & prompt the Android user accordingly for HubCiti.
					IF (@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										  
												  WHERE ConfigurationType = 'YetToReleaseHubCitiAppVersionAndroid' AND ScreenName = 'YetToReleaseHubCitiAppVersionAndroid' AND Active = 1))
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'CurrentHubCitiAppVersionAndroid' AND ScreenName = 'CurrentHubCitiAppVersionAndroid' AND Active = 1))	
						--AND 
						--(@AppVersion <> (SELECT ScreenContent
						--						  FROM AppConfiguration										 
						--						  WHERE ConfigurationType = 'SupportHubCitiAppVersionAndroid' AND ScreenName = 'SupportHubCitiAppVersionAndroid' AND Active = 1))									  		
					
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportHubCitiAppVersionIOS_Perf' AND ScreenName = 'SupportHubCitiAppVersionIOS_Perf' AND Active = 1))					
					AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportHubCitiAppVersionIOS_Perf1' AND ScreenName = 'SupportHubCitiAppVersionIOS_Perf1' AND Active = 1))	
					--AND 
					--	(@AppVersion <> (SELECT ScreenContent
					--							  FROM AppConfiguration	  									 
					--							  WHERE ConfigurationType = 'SupportHubCitiAppVersionAndroid_Perf2' AND ScreenName = 'SupportHubCitiAppVersionAndroid_Perf2' AND Active = 1))				
					BEGIN
						SET @ForcefulUpdateVersionPrompt = 1
						SELECT @DownLoadLinkAndroid = DownloadLinkAndroid
						FROM HcHubCiti HC
						WHERE HC.HcHubCitiKey=@HubCitiKey  
					END
					ELSE 
					BEGIN
						SET @ForcefulUpdateVersionPrompt = 0
						SET @DownLoadLinkAndroid = NULL
					END
				END
			END

			ELSE IF(@RegionAppID = 1)
			BEGIN
				--Check if the version running on the device is outdated/expired & prompt the IOS user accordingly for RegionApp.
				IF @RequestPlatformType = 'IOS'
				BEGIN
					--PRINT 'A'
					IF (@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										  
												  WHERE ConfigurationType = 'YetToReleaseRegionAppVersionIOS' AND ScreenName = 'YetToReleaseRegionAppVersionIOS' AND Active = 1))
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'CurrentRegionAppVersionIOS' AND ScreenName = 'CurrentRegionAppVersionIOS' AND Active = 1))	
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportRegionAppVersionIOS' AND ScreenName = 'SupportRegionAppVersionIOS' AND Active = 1))			
					
											AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportRegionAppVersionIOS_Perf' AND ScreenName = 'SupportRegionAppVersionIOS_Perf' AND Active = 1))	
					 AND
					 (@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportRegionAppVersionIOS_Perf1' AND ScreenName = 'SupportRegionAppVersionIOS_Perf1' AND Active = 1))	
					
					
					BEGIN

					  --select @HubCitiKey
						SET @ForcefulUpdateVersionPrompt = 1
						SELECT @DownLoadLinkIOS = DownloadLinkIOS
						FROM HcHubCiti HC
						WHERE HC.HcHubCitiKey=@HubCitiKey  
					END
					ELSE 
					BEGIN
						SET @ForcefulUpdateVersionPrompt = 0
						SET @DownLoadLinkIOS = NULL
					END
				END
				ELSE IF @RequestPlatformType = 'Android'
				BEGIN
				--SELECT 1
					--Check if the version running on the device is outdated/expired & prompt the Android user accordingly for RegionApp.
					IF (@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										  
												  WHERE ConfigurationType = 'YetToReleaseRegionAppVersionAndroid' AND ScreenName = 'YetToReleaseRegionAppVersionAndroid' AND Active = 1))
						AND 
						(@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'CurrentRegionAppVersionAndroid' AND ScreenName = 'CurrentRegionAppVersionAndroid' AND Active = 1))	
						--AND 
						--(@AppVersion <> (SELECT ScreenContent
						--						  FROM AppConfiguration										 
						--						  WHERE ConfigurationType = 'SupportRegionAppVersionAndroid' AND ScreenName = 'SupportRegionAppVersionAndroid' AND Active = 1))							  		
					
						AND
					    (@AppVersion <> (SELECT ScreenContent
					 							  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportRegionAppVersionAndroid_Perf' AND ScreenName = 'SupportRegionAppVersionAndroid_Perf' AND Active = 1))
						AND
					    (@AppVersion <> (SELECT ScreenContent
												  FROM AppConfiguration										 
												  WHERE ConfigurationType = 'SupportRegionAppVersionAndroid_Perf1' AND ScreenName = 'SupportRegionAppVersionAndroid_Perf1' AND Active = 1))	
					    --AND
					    --(@AppVersion <> (SELECT ScreenContent
									--			  FROM AppConfiguration										 
									--			  WHERE ConfigurationType = 'SupportRegionAppVersionAndroid_Perf2' AND ScreenName = 'SupportRegionAppVersionAndroid_Perf2' AND Active = 1))	
					
					BEGIN
						SET @ForcefulUpdateVersionPrompt = 1
						SELECT @DownLoadLinkAndroid = DownloadLinkAndroid
						FROM HcHubCiti HC
						WHERE HC.HcHubCitiKey=@HubCitiKey  
					END
					ELSE 
					BEGIN
						SET @ForcefulUpdateVersionPrompt = 0
						SET @DownLoadLinkAndroid = NULL
					END
				END

			END

			--Confirmation of Success
			SELECT @Status = 0
					
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HcCheckDeviceAppVersion.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_HcCheckDeviceAppVersion] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
