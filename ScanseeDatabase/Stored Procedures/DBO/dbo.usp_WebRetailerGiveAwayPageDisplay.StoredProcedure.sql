USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerGiveAwayPageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebGiveAwayPageDisplayRetailers1
Purpose					: To Diapley a Give Away Page
Example					: usp_WebGiveAwayPageDisplayRetailers1

History
Version		Date						Author				Change Description
------------------------------------------------------------------------------- 
1.0			29th April 2013				SPAN        		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerGiveAwayPageDisplay]
(

	--Input Parameter(s)--
	  @SearchParameter Varchar(255)
	, @RetailID int	
	, @LowerLimit int
	
	--Output Variable--	  
     , @RowCount int output	
	 , @NxtPageFlag bit output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @Config varchar(50)
			DECLARE @MaxCnt INT 
			DECLARE @UpperLimit INT 
			 
			SELECT @Config=ScreenContent    
		    FROM AppConfiguration     
		    WHERE ConfigurationType='Web Retailer Media Server Configuration' 
			
			DECLARE @QRTypeID int
			SELECT @QRTypeID=QRTypeID FROM QRTypes 
			WHERE QRTypeName = 'Give Away Page'
			
			SELECT @UpperLimit = @LowerLimit + ScreenContent 
			FROM AppConfiguration 
			WHERE ConfigurationType = 'Website Pagination'
			AND Active = 1
				
			SELECT Row_Num = ROW_NUMBER() OVER (ORDER BY Pagetitle) 
			      ,QRRetailerCustomPageID 
			      ,Pagetitle
			      ,StartDate
			      ,EndDate
			      ,ImagePath = (CASE WHEN [Image] IS NOT NULL THEN @Config + CAST(@RetailId AS VARCHAR(10)) + '/' + [Image] ELSE NULL END) 
			      ,Quantity 
			INTO #Temp      
			FROM QRRetailerCustomPage
			WHERE QRTypeID = @QRTypeID
			AND RetailID = @RetailID
			AND ((@SearchParameter IS NULL) OR (@SearchParameter IS NOT NULL AND Pagetitle  like '%'+@SearchParameter+'%'))
			AND StartDate <> '1900-01-01' 
			
			SELECT @MaxCnt = MAX(Row_Num) FROM #Temp
		    SET @RowCount = @MaxCnt	
			
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 	
			
			SELECT Row_Num
			      ,QRRetailerCustomPageID
			      ,Pagetitle
			      ,StartDate
			      ,EndDate
			      ,ImagePath
			      ,Quantity 
			FROM #Temp 
			WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit			
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
