USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveContactTitle]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: 
Purpose					: To retrieve list of Contact titles from the system.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			10 April 2012				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveContactTitle]

--Input parameters 

--Output variables
	@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		SELECT ContactTitleID, ContactTitle
		FROM ContactTitle 	 
		
		SET @Status = 0	
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveRetailerLocation.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
