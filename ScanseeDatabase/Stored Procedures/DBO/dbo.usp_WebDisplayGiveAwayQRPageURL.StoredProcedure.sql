USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayGiveAwayQRPageURL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebDisplayGiveAwayQRPageURL]
Purpose					: To Display the Retailer GiveAway Page URLs.
Example					: [usp_WebDisplayGiveAwayQRPageURL]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			2nd May 2013	Span   	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayGiveAwayQRPageURL]
(
	  
	--Input Variable
	  @RetailID int
	, @QRRetailerCustomPageID int
	  
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 
		 DECLARE @Config varchar(50)
		 DECLARE @QRTypeCode int
		 
		 SELECT @QRTypeCode = QRTypeCode
		 FROM  QRTypes 
		 WHERE QRTypeName='Give Away Page'
		 		 		
		 SELECT @Config= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='QR Code Configuration'
		 
		--Creating URL for Retailer GiveAwayPage QRCode. 
		SELECT URL = @Config + CAST(@QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRRetailerCustomPageID AS VARCHAR(10))
		      ,Pagetitle  retPageTitle
		FROM QRRetailerCustomPage 
		WHERE RetailID = @RetailID
		AND QRRetailerCustomPageID = @QRRetailerCustomPageID
							
	    
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebDisplayGiveAwayQRPageURL>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;



 

GO
