USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProductSetupRetailLocationSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerProductSetupRetailLocationSearch
Purpose					: To display the Products available.
Example					: usp_WebRetailerProductSetupRetailLocationSearch

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			1st January 2012				SPAN			Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerProductSetupRetailLocationSearch]
(

	--Input Parameter(s)--
	 
	  @RetailerID int
	--Output Variable-- 
	  
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--Get the RetailLocations associated with the input Retailer.
		SELECT RL.RetailLocationID as retailerLocationID
		     , RL.StoreIdentification as StoreIdentification
		FROM RetailLocation RL
		WHERE RetailID = @RetailerID
		AND StoreIdentification IS NOT NULL
		AND Headquarters<>1 AND RL.Active = 1
					
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerProductSetupRetailLocationSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
