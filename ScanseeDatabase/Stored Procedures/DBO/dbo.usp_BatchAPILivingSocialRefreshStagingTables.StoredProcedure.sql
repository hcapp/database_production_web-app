USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchAPILivingSocialRefreshStagingTables]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchAPILivingSocialRefreshStagingTables
Purpose					: To refersh the Stage Table
Example					: usp_BatchAPILivingSocialRefreshStagingTables

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Dec 2012	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchAPILivingSocialRefreshStagingTables]
(
	
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		TRUNCATE TABLE APILivingSocialData
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchAPILivingSocialRefreshStagingTables.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 		
		END;
		 
	END CATCH;
END;

GO
