USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerDiscardedProductAttributesDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadManufacturerLogDisplayDetails
Purpose					: 
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			28th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerDiscardedProductAttributesDisplayDetails]
(

	--Input Input Parameter(s)--	 
	
	  @ManufacturerID int
	, @UserID int
	, @UploadManufacturerLogID INT
	  
	--Output Variable--
	
	, @ContactEmail VARCHAR(1000) OUTPUT  
	, @FirstName VARCHAR(1000) OUTPUT  
	, @Status INT OUTPUT	
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
				
				--Display all the discarded Attributes that have not yet been noified to the user.
				SELECT  UploadSupplierDiscardedProductAttributesID
					  ,	UML.UploadManufacturerLogID
					  ,	ScanCode as scanCode
					  ,	AttributeName as attributeName
					  ,	DisplayValue as prodDisplayValue
					  , UMDPA.ReasonForDiscarding as discardReason    
				FROM UploadManufacturerDiscardedProductAttributes UMDPA
				INNER JOIN UploadManufacturerLog UML ON UML.UploadManufacturerLogID = UMDPA.UploadManufacturerLogID
				WHERE UML.UploadManufacturerLogID = @UploadManufacturerLogID
				AND UML.ManufacturerID = @ManufacturerID
				AND UML.UserID = @UserID
				AND UML.EmailNotificationFlag = 0
				
				--Update saying notified.
				UPDATE UploadManufacturerLog
				SET EmailNotificationFlag = 1
				WHERE UploadManufacturerLogID = @UploadManufacturerLogID
				
				 --Fetch the ContactEmailID
				SELECT @ContactEmail = C.ContactEmail
			         , @FirstName = C.ContactFirstName
				FROM ManufacturerContact MC
				INNER JOIN Contact C ON C.ContactID = MC.ContactID
				WHERE ManufacturerID = @ManufacturerID	
				
				--Confirmation of Success.
				SET @Status = 0
				
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerLogDisplayDetails.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			--Confirmation of Failure.
			SET @Status = 1
			ROLLBACK TRANSACTION;
			
		END;
		 
	END CATCH;
END;


GO
