USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerUserTrackingShareAppsite]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebConsumerUserTrackingShareAppsite] 
Purpose               : To dispaly Main menu Details.  
Example               : [usp_WebConsumerUserTrackingShareAppsite] 
  
History  
Version    Date           Author        Change         Description  
---------------------------------------------------------------   
1.0        26th June 2013 Dhananjaya TR Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerUserTrackingShareAppsite]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @RetailLocationID int 
 , @TargetAddress Varchar(255)   
 --OutPut Variable  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         --Display Module Details
		 INSERT INTO ScanSeeReportingDatabase..ShareAppsite(MainMenuID
														   ,ShareTypeID
														   ,TargetAddress
														   ,RetailLocationID														 
														   ,CreatedDate)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@RetailLocationID 
		        ,GETDATE())								     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebConsumerUserTrackingShareAppsite].'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
