USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListDeleteLoyalty]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListDeleteLoyalty
Purpose					: To delete Loyalty from User Gallery.
Example					: usp_MasterShoppingListDeleteLoyalty 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListDeleteLoyalty]
(
	@UserID int
   ,@LoyaltyDealID int
   
   --Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DELETE FROM UserLoyaltyGallery
			WHERE UserID = @UserID
			AND LoyaltyDealID = @LoyaltyDealID 
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListDeleteLoyalty.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
