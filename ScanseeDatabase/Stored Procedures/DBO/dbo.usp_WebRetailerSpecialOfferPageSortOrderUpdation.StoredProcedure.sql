USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerSpecialOfferPageSortOrderUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name   :  [usp_WebRetailerSpecialOfferPageSortOrderUpdation]
Purpose                  : To update sort order for Special Offer Pages for given Retailer.
Example                  : [usp_WebRetailerSpecialOfferPageSortOrderUpdation]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0            25th Jun 2014           SPAN             1.0                                      
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerSpecialOfferPageSortOrderUpdation]
(

      --Input Parameters
        @RetailID INT
      , @SpecialOfferPageID Varchar(MAX) --Comma separated SpecialOfferPageids
      , @SortOrder VARCHAR (255)
      
      --Output Parameters
	  , @Status INT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN
  
      BEGIN TRY
      
      BEGIN TRANSACTION
			
			 --Store Sort order to temp table
             CREATE TABLE #Temp(Rownum INT IDENTITY(1,1),SortID varchar(100))
             INSERT INTO #Temp (SortID)
			 SELECT R.Param 
			 FROM dbo.fn_SplitParam(@SortOrder, ',') R

			 --Store SpecialOfferPageIDs to temp1 table
			 CREATE TABLE #Temp1(Rownum INT IDENTITY(1,1),PageID varchar(100))
			 INSERT INTO #Temp1(PageID)
			 SELECT R.Param 
			 FROM dbo.fn_SplitParam(@SpecialOfferPageID,',') R
             
             --Store Sort order and SpecialOfferPageIDs together in temp2 table
			 CREATE TABLE #Temp2(Rownum INT IDENTITY(1,1),SortID VARCHAR(255),PageID VARCHAR(100))
             INSERT INTO #Temp2 (SortID,PageID )
			 SELECT	T1.SortID 
				   ,T2.PageID  
			 FROM #Temp T1 
			 INNER JOIN #Temp1 T2 ON T1.Rownum =T2.Rownum 

		   
		    --Update SpecialOfferPage based on sorting.
		    UPDATE QRRetailerCustomPage SET SortOrder = T.SortID 
		    FROM QRRetailerCustomPage R
	        INNER JOIN #temp2 T ON T.PageID =R.QRRetailerCustomPageID  

	        
	   --Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION      
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerSpecialOfferPageSortOrderUpdation.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  --Confirmation of Failure.
                  SELECT @Status =1
                  ROLLBACK TRANSACTION;              
                  
            END;
            
      END CATCH;
END;







GO
