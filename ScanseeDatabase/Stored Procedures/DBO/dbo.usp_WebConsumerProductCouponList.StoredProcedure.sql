USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerProductCouponList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*        
Stored Procedure name : usp_WebConsumerProductCouponList         
Purpose               : To list Coupon        
Example               : usp_WebConsumerProductCouponList         
        
History        
Version     Date            Author          Change Description        
---------------------------------------------------------------         
1.0                
---------------------------------------------------------------        
*/        
        
CREATE PROCEDURE [dbo].[usp_WebConsumerProductCouponList]        
(        
   @ProductID int        
 , @RetailID int        
 , @UserID int        
 , @LowerLimit int        
 , @ScreenName varchar(50)  
 
 --User Tracking inputs
 , @MainMenuID int      
         
 --OutPut Variable   
 , @MaxCnt int output      
 , @NxtPageFlag bit output        
 , @ErrorNumber int output        
 , @ErrorMessage varchar(1000) output        
)        
AS        
BEGIN        
         
 BEGIN TRY        
   
	 --To get Server Configuration
	 DECLARE @ManufConfig varchar(50) 
	 DECLARE @RetailerConfig varchar(50)
	  
	 SELECT @ManufConfig= ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
	 
	 SELECT @RetailerConfig= ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Retailer Media Server Configuration'

         
	  --To get the row count for pagination.        
	  DECLARE @UpperLimit int         
	  SELECT @UpperLimit = @LowerLimit + ScreenContent         
	  FROM AppConfiguration         
	  WHERE ScreenName = @ScreenName         
	   AND ConfigurationType = 'Pagination'        
	   AND Active = 1        
	        
          
  --To get Image of the product        
   DECLARE @ProductImagePath varchar(1000)        
   SELECT @ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1     
                       THEN @ManufConfig    
                       +CONVERT(VARCHAR(30),ManufacturerID)+'/'    
                       +ProductImagePath ELSE ProductImagePath     
                       END       
                          ELSE ProductImagePath END           
   FROM Product         
   WHERE ProductID = @ProductID   
   
   
    SELECT Row_Num=ROW_NUMBER() over(order by C.couponid)        
    ,C.CouponID         
    ,CouponName        
    ,CouponDiscountType        
    ,CouponDiscountAmount        
    ,CouponDiscountPct        
    ,ISNULL(CouponShortDescription,CouponLongDescription) CouponDescription        
    ,CouponDateAdded        
    ,CouponStartDate        
    ,CouponExpireDate      
    ,CouponURL couponURL      
    ,CouponImagePath =CASE WHEN CouponImagePath IS NULL THEN @ProductImagePath ELSE CASE WHEN CouponImagePath IS NOT NULL THEN   
																									  CASE WHEN WebsiteSourceFlag = 1   
																										   THEN @RetailerConfig  
																											  +CONVERT(VARCHAR(30),CR.RetailID)+'/'  
																											   +CouponImagePath   
																											 ELSE CouponImagePath   
																										 END  
                         END   
      END     
     ,ViewableOnWeb  
   INTO #Coupons        
   FROM Coupon C        
   INNER JOIN CouponProduct CP ON C.couponid=CP.CouponID  
   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID          
   WHERE CP.ProductID = @ProductID         
   AND GETDATE() BETWEEN CouponStartDate AND CouponExpireDate  
          
  
             
	           
		--To capture max row number.        
	   SELECT @MaxCnt = MAX(Row_Num) FROM #Coupons         
	   --this flag is a indicator to enable "More" button in the UI.         
	   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
	   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
	   
	           

	   
	   CREATE TABLE #Temp(CouponListID int,CouponID int)
	   --IF ISNULL(@RetailID, 0) = 0
	   --BEGIN
		   
		   
		   INSERT INTO ScanSeeReportingDatabase..CouponList(MainMenuID   
															,CouponID													
															,CreatedDate)
		   
		   OUTPUT inserted.CouponListID,inserted.CouponID INTO #Temp(CouponListID,CouponID)
		   SELEct @MainMenuID 
				 ,couponId 
				 ,GETDATE()   
		   FROM #Coupons  
		   
		   SELECT Row_Num rowNum 
			,T.CouponListID      
			,C.CouponID couponId        
			,C.CouponName couponName 			      
			,C.CouponDiscountType couponDiscountType        
			,C.CouponDiscountAmount couponDiscountAmount        
			,C.CouponDiscountPct couponDiscountPct        
			,CouponDescription       
			,C.CouponDateAdded couponDateAdded        
			,C.CouponStartDate couponStartDate        
			,C.CouponExpireDate couponExpireDate      
			,C.CouponURL       
			,C.CouponImagePath imagePath     
			,ViewableOnWeb
		   FROM #Coupons  C
		   INNER JOIN #Temp T ON T.CouponID=C.couponId    	  
	       WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit         
	      ORDER BY Row_Num   
 END TRY        
         
 BEGIN CATCH        
         
  --Check whether the Transaction is uncommitable.        
  IF @@ERROR <> 0        
  BEGIN        
   PRINT 'Error occured in Stored Procedure usp_WebConsumerProductCouponList.'          
   --- Execute retrieval of Error info.        
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output         
  END;        
           
 END CATCH;        
END;


GO
