USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerProductAttributesLogCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadManufacturerProductAttributesLogCreation
Purpose					: To create a log entry associated to a attribute batch upload.
Example					: usp_WebUploadManufacturerProductAttributesLogCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			31st July 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerProductAttributesLogCreation]
(
	 @ManufacturerID int
	,@UserID int	
	,@ProductAttributeFileName Varchar(255)
	
	--Output Variable 
	, @ManufacturerLogID int output	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
			--To get Manufacturer name
			DECLARE @manufname varchar(255)
			SELECT @manufname=ManufName
			FROM Manufacturer
			WHERE ManufacturerID=@ManufacturerID				 
			
			--To Populate Log table.
			INSERT INTO [UploadManufacturerLog]
					   ([UserID]
					   ,[ManufacturerID]
					   ,[ManufacturerName]					   
					   ,[ProductAttributeFileName]
					   ,[ProductTotalRowsProcessed]
					   ,[ProductSuccessfulRowCount]
					   ,[ProductFailureRowCount]
					   ,[ProductAttributeTotalRowsProcessed]
					   ,[ProductAttributeSuccessfulRowCount]
					   ,[ProductAttributeFailureRowCount]
					   ,[Remarks]
					   ,[ProcessDate]
					   ,[ProcessStartDate]
					   ,[ProcessEndDate])
			 VALUES(@UserID
					,@ManufacturerID
					,@manufname				
					,@ProductAttributeFileName
					,0
					,0
					,0
					,0
					,0
					,0
					,'Successfull'
					,GETDATE()
					,GETDATE()
					,GETDATE())
										
			--To capture UploadManufacturerLogID				
				SET @ManufacturerLogID=SCOPE_IDENTITY()
							
	--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerProductAttributesLogCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			--DECLARE @ERRORMsg Varchar(2000)
			--SET @ERRORMsg=ERROR_MESSAGE()
			ROLLBACK TRANSACTION;
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
