USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponProductSearch
Purpose					: 
Example					: usp_WebRetailerCouponProductSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCouponProductSearch]
(
	@RetailID int
	,@RetailLocationID int
	,@ProductSearch Varchar(1000)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		
		IF(@RetailLocationID IS NOT NULL)
		BEGIN
			
			SELECT DISTINCT
				    P.ProductID
				   ,ProductName
				   ,ScanCode
				   ,ProductShortDescription shortDescription
				   ,productImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @Config
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END  
				   
			FROM Product P
			INNER JOIN RetailLocationProduct RP ON P.ProductID=RP.ProductID
			INNER JOIN RetailLocation R ON R.RetailLocationID=RP.RetailLocationID
			WHERE RetailID=@RetailID AND R.RetailLocationID=@RetaillocationID
				AND (ProductName LIKE(CASE WHEN @ProductSearch IS NULL THEN '%' ELSE '%' + @ProductSearch + '%' END)
					OR ScanCode LIKE @ProductSearch + '%')
		    AND (P.ProductExpirationDate IS NULL OR P.ProductExpirationDate >= GETDATE())
		    AND P.ProductID <> 0 AND R.Active = 1
		END
		
		ELSE
		BEGIN
		
			SELECT DISTINCT P.ProductID
				   ,ProductName
				   ,ScanCode
				   ,ProductShortDescription shortDescription	
				   ,productImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @Config
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END  			   
			FROM Product P
			INNER JOIN RetailLocationProduct RP ON P.ProductID=RP.ProductID
			INNER JOIN RetailLocation R ON R.RetailLocationID=RP.RetailLocationID
			WHERE RetailID=@RetailID 
				AND (ProductName LIKE(CASE WHEN ISNULL(@ProductSearch,'') = '' THEN '%' ELSE '%' + @ProductSearch + '%' END)
					OR ScanCode LIKE @ProductSearch + '%')
             AND (P.ProductExpirationDate IS NULL OR P.ProductExpirationDate >= GETDATE())
             AND P.ProductID <> 0 AND R.Active = 1
					
		END
				    
		
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponProductSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
