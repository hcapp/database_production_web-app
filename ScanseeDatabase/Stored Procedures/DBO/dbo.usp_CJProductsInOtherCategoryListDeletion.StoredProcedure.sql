USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_CJProductsInOtherCategoryListDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CJProductsInOtherCategoryListDeletion
Purpose					: Delte Product list 
Example					: usp_CJProductsInOtherCategoryListDeletion

History
Version		Date			Author					Change Description
--------------------------------------------------------------- 
1.0			7th Oct 2013	Dhananjaya TR   		Initial Version
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_CJProductsInOtherCategoryListDeletion]
(
	 	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION	
			
			--Delete Product list 
			DELETE FROM  [CJListProductsInOterCategory] 
			WHERE ProductSent=0 
		            	
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CJProductsInOtherCategoryListDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
