USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryDeleteUsedRebate]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GalleryDeleteUsedRebate
Purpose					: 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th Dec 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryDeleteUsedRebate]
(
	
	@UserRebateGalleryID int
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To Swipe delete Used Rebate.
			--UPDATE UserRebateGallery   
			--SET UsedFlag = 0
			--WHERE UserRebateGalleryID = @UserRebateGalleryID
			
			DELETE FROM UserRebateGallery
			WHERE UserRebateGalleryID=@UserRebateGalleryID
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_GalleryDeleteUsedRebate>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
