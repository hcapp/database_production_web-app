USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEventsImageList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchEventsImageList]
Purpose					: To display Events imagepath list.
Example					: [usp_BatchEventsImageList]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEventsImageList]
(
    --Input Variable
	  @FileName VARCHAR(255)
	, @Date Datetime
	  
	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
			SELECT E.HcHubCitiID AS HubCitiID
					,E.HcEventID AS EventID
					,E.ImagePath
					,E.EventListingImagePath AS listImagePath
			FROM HcEventsStagingData S
		    INNER JOIN HcEvents E ON E.HcEventName = S.EventName AND E.ImagePath = S.ImagePath AND E.StartDate = S.StartDate 
						AND E.ShortDescription = S.ShortDescription 
			WHERE S.[FileName] = @FileName
			AND CONVERT(Date,S.DateCreated) = @Date 
			AND CONVERT(Date,E.DateCreated) = @Date 
			

		   --Confirmation of Success.
		   SELECT @Status = 0
	
	END TRY
		
	BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		PRINT 'Error occured in Stored Procedure [usp_BatchEventsImageList].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
		--Confirmation of failure.
		SELECT @Status = 1
	END;
		 
	END CATCH;
END;


GO
