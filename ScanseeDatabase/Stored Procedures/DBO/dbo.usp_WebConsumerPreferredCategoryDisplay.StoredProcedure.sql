USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerPreferredCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerPreferredCategoryDisplay
Purpose					: To dispaly User'd preferred Categories.
Example					: usp_WebConsumerPreferredCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23rd may 2013   Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerPreferredCategoryDisplay]
(
	@UserID int
	--, @LowerLimit int
	--, @ScreenName varchar(50)
	--OutPut Variable
	--, @NxtPageFlag bit output
	, @Email bit output
	, @CellPhone bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	
		SELECT @Email = EmailAlert
			 , @CellPhone = CellPhoneAlert
		FROM UserPreference 
		WHERE UserID = @UserID 
	
		SELECT  Row_Num=ROW_NUMBER() over(order by C.categoryid)
			,C.CategoryID categoryID
		    ,C.ParentCategoryID  parentCategoryID
			, C.ParentCategoryName  parentCategoryName
			, C.SubCategoryID  subCategoryID
			, C.SubCategoryName  subCategoryName
			, displayed = CASE WHEN UC.CategoryID IS NOT NULL THEN 1 ELSE 0 END
		FROM Category C
		LEFT JOIN UserCategory UC ON UC.CategoryID = C.CategoryID AND UC.UserID =@UserID 
		--LEFT JOIN UserPreference UP ON UP.UserID =UC.UserID 		
		order by ParentCategoryName, subCategoryName 
	
		----To get the row count for pagination.
		--DECLARE @UpperLimit int 
		--SELECT @UpperLimit = @LowerLimit + ScreenContent 
		--FROM AppConfiguration 
		--WHERE ScreenName = @ScreenName 
		--	AND ConfigurationType = 'Pagination'
		--	AND Active = 1
		--DECLARE @MaxCnt int
		
		
		--SELECT  Row_Num=ROW_NUMBER() over(order by C.categoryid)
		--	,C.CategoryID categoryID
		--    ,C.ParentCategoryID  parentCategoryID
		--	, C.ParentCategoryName  parentCategoryName
		--	, C.SubCategoryID  subCategoryID
		--	, C.SubCategoryName  subCategoryName
		--	, displayed = CASE WHEN UC.CategoryID IS NOT NULL THEN 1 ELSE 0 END
		--INTO #Category
		--FROM Category C
		--INNER JOIN UserCategory UC ON UC.CategoryID = C.CategoryID AND UC.UserID = @UserID 
		
		----To capture max row number.
		--SELECT @MaxCnt = MAX(Row_Num) FROM #Category	
		
		----this flag is a indicator to enable "More" button in the UI. 
		----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		--SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
		
		--SELECT  Row_Num
		--		,categoryID
		--		,parentCategoryID
		--		,parentCategoryName
		--		,subCategoryID
		--		,subCategoryName
		--		,displayed 
		--FROM #Category
		--WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerPreferredCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;


GO
