USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCategoriesSubCategories_Script]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [dbo].[UpdateCategoriesSubCategories_Script]
Purpose					: To associate Retailers with Categories/Sub-categories
Example					: [dbo].[UpdateCategoriesSubCategories_Script]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			8 Aug 2016	    Sagar Byali	    1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[UpdateCategoriesSubCategories_Script]
(
	 --OutPut Variable	
	   @Status int output
	 , @ErrorNumber int output  
	 , @ErrorMessage varchar(1000) output  
  
)
AS
BEGIN


	BEGIN TRY

	
	
														




------EXEC sp_RENAME '[DataUpload_AddCAT].[category name]', 'category', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[Sub-Category Name]', 'subcategory', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail id]', 'retailid', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail locationid]', 'retaillocationid', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail location id]', 'retaillocationid', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[address]', 'address1', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[postal code]', 'postalcode', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[contact phone]', 'contactphone', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail name]', 'retailname', 'COLUMN'


--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'retail name')
--EXEC sp_RENAME '[DataUpload_AddCAT].[retail name]', 'retailname', 'COLUMN'

--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'category name')
--EXEC sp_RENAME '[DataUpload_AddCAT].[category name]', 'category', 'COLUMN'

--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'Sub-Category Name')
--EXEC sp_RENAME '[DataUpload_AddCAT].[Sub-Category Name]', 'subcategory', 'COLUMN'

--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'retail id')
--EXEC sp_RENAME '[DataUpload_AddCAT].[retail id]', 'retailid', 'COLUMN'

--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'retail location id')
--EXEC sp_RENAME '[DataUpload_AddCAT].[retail location id]', 'retaillocationid', 'COLUMN'

--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'address')
--EXEC sp_RENAME '[DataUpload_AddCAT].[address]', 'address1', 'COLUMN'

--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'postal code')
--EXEC sp_RENAME '[DataUpload_AddCAT].[postal code]', 'postalcode', 'COLUMN'


--IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'contact phone')			
--EXEC sp_RENAME '[DataUpload_AddCAT].[contact phone]', 'contactphone', 'COLUMN'


--IF NOT EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'DataUpload_AddCAT' AND COLUMN_NAME like '%phone%')
--alter table DataUpload_AddCAT add contactphone varchar(50)



													
--To insert data into Retailer upload table----														
insert into DataUpload_AddRetailerBusinessCategoriesSubCategories(Category,
																 SubCategory,
																 RetailName,
																 RetailID,
																 RetailLocationID,
																 Address1,
																 City,
																 State,
																 PostalCode
																 ,contactphone
																, CatID,
SubCatID
																 )

											select distinct Category,
													SubCategory,
													RetailName,
													RetailID,
													RetailLocationID,
													Address1,
													City,
													State,
													PostalCode,
													contactphone
													,CatID,
SubCatID
													from temp


								SELECT * FROM DataUpload_AddRetailerBusinessCategoriesSubCategories					
	
--to insert data from excel sheet into Archieve---	
insert into DataUpload_AddRetailerBusinessCategoriesSubCategories_Archieve(Category,
																 SubCategory,
																 RetailName,
																 RetailID,
																 RetailLocationID,
																 Address1,
																 City,
																 State,
																 PostalCode,
																 contactphone,DateCreated
																 )

													select distinct s.Category,
																	s.SubCategory,
																	s.RetailName,
																	s.RetailID,
																	s.RetailLocationID,
																	s.Address1,
																	s.City,
																	s.State,
																	s.PostalCode,
																	s.contactphone,getdate()
													from DataUpload_AddRetailerBusinessCategoriesSubCategories s
													left join DataUpload_AddRetailerBusinessCategoriesSubCategories_Archieve a on a.CatID=s.CatID and a.RetailID=s.RetailID and a.City=s.City and a.PostalCode=s.PostalCode and a.Address1=s.Address1
													where a.RetailID is null and a.CatID is null and a.City is null and a.Address1 is null and a.State is null and a.PostalCode is null



					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = null
					where SubCategory='N/A'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Appraisers/Inspectors' where Category = 'Appraisers'
					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Banks/Credit Union' where Category = 'Banks/Credit Unions'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Funeral Services' where Category = 'Funeral Homes'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Museums' where Category = 'Museum'

					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Pest Control' where Category = 'Pest Control Services'

					
					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Tattoo' where Category = 'Tattoos'

										
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subCategory = 'Sporting / Outdoor Events' where subCategory = 'Sporting/Outdoor Events'

					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Home Decor/Interior Design' where Category = 'Home Décor/Interior Design'

					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Cosmetics/Beauty Supply' where Category = 'Cosmetics/Beauty Supplies'

					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Florist' where Category = 'Florists'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subCategory = 'Steakhouse' where subCategory = 'Steakhouses'


					
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subCategory = 'Barbeque' where subCategory = 'Barbecue'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = 'Hotel / Motel' 
					where SubCategory='Hotel/Motel'

			
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = 'Campground' 
					where SubCategory='Campgrounds'

			
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Communications & Media' 
					where category='Communication/Media'

		
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Communications & Media' 
					where category='Communications/Media'



					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Construction/Manufacturing' 
					where category='Construction/Manufactoring'


		
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Financial' 
					where category='Financial Services'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'NonProfit' 
					where category='Non-Profit'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'NonProfit' 
					where category='Non Profit'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Specialty/Gift Stores' 
					where category='Speicalty/Gift Shops'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Salon/Barbers' 
					where category='Salon/Barber'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Hospitals/Medical' 
					where category='Hospital/Medical'

							
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subcategory = 'Hot Dogs' 
					where subcategory='Hotdogs'



					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subcategory = 'Wine Bar' 
					where subcategory='Wine Bars'

				 


			update DataUpload_AddRetailerBusinessCategoriesSubCategories
			set catid = BusinessCategoryid
			from BusinessCategory B
			INNER JOIN DataUpload_AddRetailerBusinessCategoriesSubCategories Z ON Z.Category = B.BusinessCategoryDisplayValue
			where Subcategory is null

			update DataUpload_AddRetailerBusinessCategoriesSubCategories
			set catid = B.BusinessCategoryid , SubCatID = T.HcBusinessSubCategoryID
			from BusinessCategory B
			INNER JOIN DataUpload_AddRetailerBusinessCategoriesSubCategories Z ON Z.Category = B.BusinessCategoryDisplayValue
			INNER JOIN HcBusinessSubCategoryType TT ON TT.BusinessCategoryID = B.BusinessCategoryID
			INNER JOIN HcBusinessSubCategory T ON T.HcBusinessSubCategoryTypeID = TT.HcBusinessSubCategoryTypeID AND T.BusinessSubCategoryName = Z.Subcategory
			where Subcategory is not null


			SELECT DISTINCT * FROM DataUpload_AddRetailerBusinessCategoriesSubCategories WHERE SubCatID IS not NULL
		

			------------Categories with no subcategories------------------------
			insert into RetailerBusinessCategory 
			select distinct z.retailid,z.catid,getdate(),null,null 
			from DataUpload_AddRetailerBusinessCategoriesSubCategories z
			left join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID
			where cc.RetailerID is null and z.subcatid is null and Z.catid is not null


			------------Categories with subcategories only----------------------------
			insert into RetailerBusinessCategory
			select distinct  z.retailid,z.catid,getdate(),null,z.subcatid
			from DataUpload_AddRetailerBusinessCategoriesSubCategories z
			left join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID 
			and cc.BusinessSubCategoryID = z.subcatid
			where cc.RetailerID is null and z.subcatid is not null

			--------------------------------------------------------------------------------------------------------------------------------------------------

			------------Categories with subcategories only along with retaillocations

			insert into HcRetailerSubCategory(RetailID,RetailLocationID,HcBusinessSubCategoryID,DateCreated,DateModified,CreatedUserID,BusinessCategoryID)
			select distinct z.retailid,z.retaillocationid,z.subcatid,getdate(),null,1,z.catid
			from DataUpload_AddRetailerBusinessCategoriesSubCategories z
			left join HcRetailerSubCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailID = z.RetailID and cc.HcBusinessSubCategoryID = z.subcatid 
			and cc.RetailLocationID = z.retaillocationid
			where cc.RetailID is null and cc.RetailLocationID is null and z.subcatid is not null and cc.BusinessCategoryID is null and cc.HcBusinessSubCategoryID is null

			select * into #DataUpload_AddRetailerBusinessCategoriesSubCategories  from DataUpload_AddRetailerBusinessCategoriesSubCategories

			--deleting inserted category data only
			delete from #DataUpload_AddRetailerBusinessCategoriesSubCategories
			from #DataUpload_AddRetailerBusinessCategoriesSubCategories z
			inner join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID


			--deleting inserted business category & subcategory data 
			delete from #DataUpload_AddRetailerBusinessCategoriesSubCategories
			from #DataUpload_AddRetailerBusinessCategoriesSubCategories z
			inner join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID and cc.BusinessSubCategoryID = z.subcatid
			inner join HcRetailerSubCategory C ON C.BusinessCategoryID = Z.catid and c.RetailID = z.RetailID and c.HcBusinessSubCategoryID = z.subcatid and c.RetailLocationID = z.retaillocationid


			select * from #DataUpload_AddRetailerBusinessCategoriesSubCategories


			--truncate table DataUpload_AddCAT

			--To truncate old data---
			if exists(select * from #DataUpload_AddRetailerBusinessCategoriesSubCategories where RetailName is null)
			begin
					truncate  table DataUpload_AddRetailerBusinessCategoriesSubCategories 
			end



			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcFilterOptionList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





















GO
