USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebRetailerCouponDisplay    
Purpose     : To Display coupon details of a retailer    
Example     : usp_WebRetailerCouponDisplay    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   27th Dec 2011 Naga Sandhya S Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebRetailerCouponDisplay]    
(    
   @RetailID int    
 , @CouponSearch Varchar(100)    
 , @LowerLimit int
 , @RecordCount int
     
 --Output Variable--    
  , @RowCount INT output    
  , @NextPageFlag bit output     
  , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY    
 --To get Media Server Configuration.  
  DECLARE @Config varchar(50)    
  SELECT @Config=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Retailer Media Server Configuration'   
     
  DECLARE @UpperLimit INT    
  DECLARE @MaxCnt INT    
      
   --To get the row count for pagination.       
  SELECT @UpperLimit = @LowerLimit + @RecordCount  
      
  SELECT RowNum = ROW_NUMBER() OVER (ORDER BY CouponID ASC)    
         ,CouponID    
      ,CouponName    
      ,RetailID    
      ,couponDiscountAmt    
      ,CouponDiscountPct    
      ,CouponDiscountType    
      ,CouponImagePath    
      ,CouponLongDescription    
      ,CouponShortDescription    
      ,CouponTermsAndConditions    
      ,CouponStartDate    
      ,CouponEndDate    
      ,CouponDisplayExpirationDate    
      ,timeZoneId    
      ,CouponDateAdded    
      ,POSIntegrate
	  ,ActualCouponExpirationDate 
	  ,KeyWords 
	  ,BannerTitle
	  ,CouponDetailImage
     INTO #Temp    
     FROM         
  ( SELECT DISTINCT TOP 100 PERCENT    
         C. CouponID    
      ,CouponName    
      ,CR.RetailID    
      ,CouponDiscountAmount couponDiscountAmt    
      ,ISNULL(CouponDiscountPct,0)  CouponDiscountPct    
      ,CouponDiscountType    
      ,CouponImagePath  = CASE WHEN C.CouponImagePath IS NOT NULL THEN CASE WHEN C.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),CR.RetailID) +'/' + CouponImagePath ELSE C.CouponImagePath END    
         ELSE C.CouponImagePath  
       END   
      ,CouponLongDescription    
      ,CouponShortDescription    
      ,CouponTermsAndConditions    
      ,CONVERT(VARCHAR(10),CouponStartDate,105) + ' ' + CONVERT(VARCHAR(10),CouponStartDate,108) CouponStartDate    
      ,CONVERT(VARCHAR(10),CouponExpireDate,105) + ' ' + CONVERT(VARCHAR(10),CouponExpireDate,108) CouponEndDate    
      ,CouponDisplayExpirationDate    
      ,CouponTimeZoneID timeZoneId    
      ,CouponDateAdded     
      ,POSIntegrate
	  ,ActualCouponExpirationDate  
	  ,KeyWords 
	  ,BannerTitle
	 
	  ,CouponDetailImage = CASE WHEN CouponDetailImage IS NOT NULL THEN CASE WHEN C.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),CR.RetailID) +'/'+ CouponDetailImage ELSE CouponDetailImage END 
			                    ELSE CouponDetailImage END
  FROM Coupon C  
  LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID   
  WHERE C.RetailID=@RetailID 
  AND (CouponName LIKE (CASE WHEN ISNULL(@CouponSearch,'')='' THEN '%' ELSE '%'+@CouponSearch+'%' END) 
  OR (KeyWords LIKE CASE WHEN @CouponSearch IS NOT NULL THEN '%'+@CouponSearch+'%' ELSE '%' END))
     
  

  --AND CouponExpireDate>=GETDATE()    
  ORDER BY CouponDateAdded DESC, CouponName ASC) RetailerCoupons 
     
  --select * from #Temp     
  --Find out the total number of records in the result set.    
 SELECT @MaxCnt = MAX(RowNum) FROM #TEMP    
 SET @RowCount = @MaxCnt    
 SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END    --CHECK IF THERE ARE STILL MORE RECORDS    
     
 SELECT  RowNum    
        ,CouponID    
     ,CouponName    
     ,RetailID    
     ,couponDiscountAmt    
     ,CouponDiscountPct    
     ,CouponDiscountType    
     ,CouponImagePath    
     ,CouponLongDescription    
     ,CouponShortDescription    
     ,CouponTermsAndConditions    
     ,CouponStartDate    
     ,CouponEndDate    
     ,CouponDisplayExpirationDate    
     ,timeZoneId     
     ,CouponDateAdded   
     ,POSIntegrate
	 ,ActualCouponExpirationDate   
	 ,KeyWords keyword
	 ,BannerTitle bannerTitle
	 ,CouponDetailImage
    FROM #Temp    
 WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit             --display the records within the specified limits    
     
     
 IF @RowCount IS NULL         --TO CHECK IF THE RESULT SET IS EMPTY    
    SET @RowCount = 0    
     
        
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponDisplay.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       
  END;    
       
 END CATCH;    
END;






GO
