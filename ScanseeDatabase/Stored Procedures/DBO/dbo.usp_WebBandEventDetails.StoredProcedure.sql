USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandEventDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [dbo].[usp_WebBandEventDetails]
(   
Purpose					: To display Retailer Event Details.
Example					: [dbo].[usp_WebBandEventDetails]
(   

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014		SPAN           1.0
---------------------------------------------------------------
*/

--exec usp_WebBandEventDetails 87,null,null,null

CREATE PROCEDURE [dbo].[usp_WebBandEventDetails]
(   
    --Input variable.	  
	  @EventID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @EventCategoryID Varchar(2000)
			DECLARE @RetailLocationID Varchar(2000)
			DECLARE @Config VARCHAR(500)
			DECLARE @WeeklyDays VARCHAR(1000)

	        SELECT @Config=ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
			
			SELECT @EventCategoryID = COALESCE(@EventCategoryID+',', '') + CAST(HcbandEventCategoryID AS VARCHAR)
			FROM HcBandEventsCategoryAssociation 
			WHERE HcBandEventID = @EventID 
			
			SELECT @WeeklyDays = COALESCE(@WeeklyDays+',', '') + CAST([DayName] AS VARCHAR)
			FROM HcBandEventInterval			
			WHERE HcbandEventID = @EventID
			
			SET @WeeklyDays=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@WeeklyDays, 'Sunday', '1'), 'Monday', '2'), 'Tuesday', '3'), 'Wednesday', '4'), 'Thursday', '5'), 'Friday', '6'), 'Saturday', '7')

			--To display Event Details.
			SELECT DISTINCT E.HcbandEventID EventID
						   ,HcbandEventName eventName 
			               ,ShortDescription shortDescription
                           ,LongDescription longDescription 								
						   ,eventImagePath = @Config + CAST(E.BandId AS VARCHAR(100))+'/'+ ImagePath 
						   ,ImagePath eventImageName
						   ,EvtImagePath = @Config + CAST(E.BandId AS VARCHAR(100))+'/'+ EventListingImagePath 
						   ,EventListingImagePath=@Config + CAST(E.BandId AS VARCHAR(100))+'/'+ EventListingImagePath 
						    ,EventListingImagePath EvtImageName
						   ,BussinessEvent bsnsLoc
						   ,eventStartDate =CAST(StartDate AS DATE)
						   ,eventEndDate =ISNULL(CAST(EndDate AS DATE),NULL)
						   ,eventStartTime =CONVERT(VARCHAR(5),CAST(StartDate AS Time))
						   ,eventEndTime =ISNULL(CONVERT(VARCHAR(5),CAST(EndDate AS Time)),NULL)
						    ,Address = EL.Address 
										
							  ,City =  EL.City 
									 
							  ,State =  EL.State 
									  
							  ,PostalCode = EL.PostalCode 
									    	
						
						     ,Latitude =
																		   EL.Latitude  	
							 ,Longitude = EL.Longitude 
									    		
						   ,@EventCategoryID  EventCategory		
						   ,RetailLocationIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(RetailLocationID AS VARCHAR(10)))
													FROM HcbandRetailerEventsAssociation REA
													WHERE REA.HcbandEventID = E.HcbandEventID
												    FOR XML PATH( '' )
												  ), 3, 1000 ), ' ', '')
						  ,geoError = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(EL.GeoErrorFlag,0) END 		
						  ,MoreInformationURL moreInfoURL
						  ,OnGoingEvent isOnGoing	 	
						  ,E.HcEventRecurrencePatternID recurrencePatternID
						  ,HR.RecurrencePattern recurrencePatternName
						  ,[isWeekDay] = CASE WHEN HR.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
						  ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
						  ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
						  ,ByDayNumber = CASE WHEN HR.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
						  ,DayNumber
						  --,MonthInterval	
						  ,EventFrequency AS EndAfter	,EventLocationKeyword	
						  ,RE.RetailID retailerId
						  ,R.RetailName retailerName
						  ,RL.Address1+','+RL.City+','+RL.State+','+RL.PostalCode LocationAddress
						  ,RE.RetailLocationID LocationID
			FROM HcbandEvents E
			LEFT JOIN HcBandRetailerEventsAssociation RE ON RE.HcBandEventID = E.HcBandEventID
			LEFT JOIN RetailLocation RL ON RE.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
			LEFT JOIN HcBandEventLocation EL ON E.HcbandEventID =EL.HcbandEventID 
			LEFT JOIN HcEventRecurrencePattern HR ON HR.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
			LEFT JOIN HcBandEventInterval EI ON EL.HcbandEventID = EI.HcbandEventID  --OR RE.HcbandEventID = EI.HcEventID
			LEFT JOIN Retailer R ON R.RetailID = RL.RetailID
			WHERE E.HcbandEventID =  @EventID
			AND E.bandid IS NOT NULL
			AND E.Active = 1
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerEventDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
