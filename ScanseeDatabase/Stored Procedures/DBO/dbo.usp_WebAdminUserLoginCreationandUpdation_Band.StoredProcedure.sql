USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminUserLoginCreationandUpdation_Band]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : [usp_WebAdminUserLoginCreationandUpdation]
Purpose                    : To Create User Login.
Example                    : [usp_WebAdminUserLoginCreationandUpdation]

History
Version              Date                 Author   Change Description
--------------------------------------------------------------- 
1.0                  26/04/2016        Prakash C            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminUserLoginCreationandUpdation_Band]
(
    --Input variable
         @RetailID int
       , @Username Varchar(1000) 
       , @Password varchar(1000)
       , @EmailID Varchar(1000)
       , @LoginExist Bit
      -- , @AdminUserID int
                
       --Output Variable
       , @DuplicateEmail bit output   
       , @DuplicateUserName bit output
       , @RetailName Varchar(500) output
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
                     
                     BEGIN TRANSACTION
                     
                     DECLARE @UserID INT
                     DECLARE @UserType INT
                     DECLARE @ContactID INT
                     DECLARE @UserExist INT
                     DECLARE @OldUsername Varchar(500)
                     DECLARE @OldEmail Varchar(500)


					-- Check for login exist, if so capture UserID.
                     SELECT @UserExist=UserID 
                     FROM UserBand 
                     WHERE BandID =@RetailID AND @LoginExist =1 


					 --Capture username who having login.
                     SELECT @OldUsername =UserName                   
                     FROM Users 
                     WHERE UserID =@UserExist

					 --Capture Band name.
                     SELECT @RetailName=BandName
                     FROM Band 
                     WHERE BandID=@RetailID AND BandActive = 1 

					 --DECLARE @Address1 varchar(1000)
					 --DECLARE @City varchar(500)
					 --DECLARE @State varchar(50)
					 --DECLARE @PostalCode varchar(100)

					 --SELECT @Address1 = Address1
						--   ,@City = City
						--   ,@State = @State
						--   ,@PostalCode = @PostalCode
					 --FROM Band
					 --WHERE BandID = @RetailID

                     IF EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName AND (@UserExist IS NULL OR (@UserExist IS NOT NULL AND @UserExist<> UserID))) 
                     BEGIN
                           
                                  SET @DuplicateUserName = 1
                     END
                     ----Check if the EmailID duplication.
                     --ELSE IF EXISTS(SELECT 1 FROM Users WHERE Email = @EmailID AND (@UserExist IS NULL OR (@UserExist IS NOT NULL AND @UserExist<> UserID)))
                     --BEGIN                       
                     --            SET @DuplicateEmail = 1
                     --END  
                     
                     ELSE
                     BEGIN    
                                         
            --                                    IF NOT EXISTS(SELECT 1 FROM BandLocation WHERE BandID =@RetailID AND ((TRY_CONVERT(BIT, Headquarters)>0) OR (TRY_CONVERT(BIT, CorporateAndStore)>0) ))
												----IF NOT EXISTS(SELECT 1 FROM BandLocation WHERE BandID =@RetailID and Address1 =@Address1 and city=@City and state=@State and PostalCode=@PostalCode)
            --                                    BEGIN
            --                                                         INSERT INTO BandLocation(BandID
            --                                                                                                         ,Headquarters
            --                                                                                                         ,Address1
            --                                                                                                         ,City
            --                                                                                                         ,State
            --                                                                                                         ,PostalCode
            --                                                                                                         ,CountryID
            --                                                                                                         ,DateCreated
            --                                                                                                         ,StoreIdentification
            --                                                                                                         ,CorporateAndStore
            --                                                                                                         --,CreatedUserID
            --                                                                                                         )
            --                                                         SELECT BandID 
            --                                                                  ,0
            --                                                                  ,Address1 
            --                                                                  ,City 
            --                                                                  ,State 
            --                                                                  ,PostalCode
            --                                                                  ,1
            --                                                                  ,GETDATE()
            --                                                                  ,'9999'
            --                                                                  ,1
            --                                                                 -- ,@AdminUserID
            --                                                         FROM Band 
            --                                                         WHERE BandID =@RetailID 
            --                                    END 
			
			
			--To check if login exists for a respective Band.
			DECLARE @ExistLogin Bit

			SELECT @ExistLogin = 1
			FROM UserBand 
			WHERE BandID = @RetailID
												
												
			--For Bands imported from excel sheet, Set CorporateAndStore as '1'
			--IF ((SELECT COUNT(DISTINCT BandLocationID) FROM BandLocation WHERE Active = 1 AND BandID = @RetailID) = 1 )
			--BEGIN   
			                                     
			--		UPDATE TOP (1) BandLocation SET CorporateAndStore = 1
			--		WHERE BandID = @RetailID AND Headquarters = 0 
			--END
												
			--For Bands having single location:- Set CorporateAndStore as '1' and reset Headquarters to '0' 
			--IF ((SELECT COUNT(DISTINCT BandLocationID) FROM BandLocation WHERE BandID =@RetailID AND Active = 1 AND @ExistLogin = 1) =1)
			--BEGIN 
												       
			--		IF EXISTS (SELECT Headquarters = 1 FROM BandLocation WHERE BandID = @RetailID)
			--		BEGIN
			--		UPDATE BandLocation 
			--		SET Headquarters = 0 , CorporateAndStore = 1 
			--		WHERE BandID = @RetailID AND Headquarters = 1 AND Active = 1
			--		END
													  
			--END	 
			

			--IF EXISTS(SELECT 1 FROM BandLocation WHERE BandID =@RetailID AND (Headquarters <> 1 AND CorporateAndStore <> 1)) 
			--BEGIN                                  
			--	 UPDATE TOP (1) BandLocation SET CorporateAndStore = 1
			--	 WHERE BandID = @RetailID 
			--END
			--ELSE IF (SELECT COUNT(1) FROM BandLocation WHERE BandID =@RetailID AND (Headquarters=1 AND CorporateAndStore=0))=1
			--BEGIN
			--	 UPDATE TOP (1) BandLocation SET CorporateAndStore = 1,Headquarters = 0
			--	 WHERE BandID = @RetailID 												
			--END
											
            IF NOT EXISTS(SELECT 1 FROM UserBand WHERE BandID =@RetailID)
            BEGIN                          
                            --Creating New User
                            INSERT INTO users(CountryID
                                                        ,Password
                                                        ,DateCreated
                                                        ,UserName
														,Email
														,UserTypeID
                                                    -- ,CreatedUserID
														)


                                    SELECT 1
                                            ,@Password 
                                            ,GETDATE()
                                            ,@Username 
											,@EmailID
											,4
                                        --  ,@AdminUserID
                                  
                            SET @UserID=SCOPE_IDENTITY()

                            INSERT INTO UserBand(BandID
                                                    ,UserID) 
                            SELECT @RetailID 
                                  ,@UserID 


					IF ((SELECT COUNT(DISTINCT BandLocationID) FROM BandLocation WHERE Active = 1 AND BandID = @RetailID) = 1 )
					BEGIN   
			                                     
							UPDATE TOP (1) BandLocation SET CorporateAndStore = 1
							WHERE BandID = @RetailID AND Headquarters = 0 
					END


            END    
                                         
            ELSE IF EXISTS(SELECT 1 FROM UserBand WHERE BandID =@RetailID)
            BEGIN
			--IF @OldUsername <>@Username
				--BEGIN
					UPDATE Users SET ResetPassword = 0
											,DateModified = GETDATE()
										-- ,ModifiedUserID = @AdminUserID
					WHERE UserID =@UserExist
				--END

					UPDATE Users SET UserName =@Username 
												,Password =@Password 
												,DateModified = GETDATE()
												,Email = @EmailID
										-- ,ModifiedUserID = @AdminUserID
					WHERE UserID =@UserExist 

					UPDATE C SET ContactEmail = @EmailID
					FROM Contact C
					INNER JOIN BandContact RC ON C.ContactID = RC.ContactID
					INNER JOIN BandLocation RL ON RC.BandLocationID = RL.BandLocationID AND RL.Active = 1
					WHERE BandID = @RetailID
					AND (CorporateAndStore='1' OR Headquarters ='1')
            END           

            IF EXISTS(SELECT 1 FROM BandContact RC
                                    INNER JOIN BandLocation RL ON RC.BandLocationID =RL.BandLocationID AND BandID =@RetailID AND RL.Active = 1 
                                                                                    AND (CorporateAndStore='1' OR Headquarters ='1'))
            BEGIN

            SELECT @OldEmail  =C.ContactEmail  
            FROM CONTACT C 
            INNER JOIN BandContact RC ON RC.ContactID =C.ContactID 
            INNER JOIN BandLocation RL ON RL.BandLocationID =RC.BandLocationID AND BandID =@RetailID AND RL.Active = 1
            AND (CorporateAndStore='1' OR Headquarters ='1')

            --IF @OldEmail <>@EmailID
            --BEGIN
                        UPDATE Users SET  ResetPassword =0
                                                    ,DateModified = GETDATE()
                                                    --,ModifiedUserID = @AdminUserID
                        WHERE UserID =@UserExist
            --END

            UPDATE Contact Set ContactEmail =@EmailID
                                            ,DateModified = GETDATE()
                                        --  ,ModifyUserID = @AdminUserID 
            FROM CONTACT C 
            INNER JOIN BandContact RC ON RC.ContactID =C.ContactID 
            INNER JOIN BandLocation RL ON RL.BandLocationID =RC.BandLocationID AND BandID =@RetailID AND RL.Active = 1
            AND (CorporateAndStore='1' OR Headquarters ='1')
            END


            IF NOT EXISTS(SELECT 1 FROM BandContact RC
                                    INNER JOIN BandLocation RL ON RC.BandLocationID =RL.BandLocationID AND BandID =@RetailID 
									AND RL.Active = 1 
                                       AND (CorporateAndStore='1' OR Headquarters ='1')
									   )
            BEGIN
			INSERT INTO Contact(ContactPhone
										,ContactEmail
										,DateCreated
										,CreateUserID
										)
			SELECT CorporatePhoneNo 
					,@EmailID 
					,GETDATE()
					,ISNULL(@UserID,@UserExist) --@AdminUserID
			FROM Band 
			WHERE BandID =@RetailID AND BandActive = 1

			SET @ContactID=SCOPE_IDENTITY()

			INSERT INTO BandContact(ContactID
													,BandLocationID
													,ContactEmail)
			SELECT @ContactID 
					,BandLocationID
					,@EmailID                                
			FROM BandLocation 
			WHERE BandID =@RetailID AND (CorporateAndStore='1' OR Headquarters ='1') AND Active = 1
            END    
                                                
            UPDATE Users SET ResetPassword = 0
                                                    ,DateModified = GETDATE()
                                                -- ,ModifiedUserID = @AdminUserID
            WHERE UserID =@UserExist         

            END
                     
			--Confirmation of Success
			SELECT @Status = 0
                     
            COMMIT TRANSACTION
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                  SELECT ERROR_LINE() 
                     PRINT 'Error occured in Stored Procedure [usp_WebAdminUserLoginCreationandUpdation].'              
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
                     ROLLBACK TRANSACTION;
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;















GO
