USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerGiveAwayPageDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebGiveAwayPageDeletion1
Purpose					: To Delete a Give Away Page 
Example					: usp_WebGiveAwayPageDeletion1

History
Version		Date						Author				Change Description
------------------------------------------------------------------------------- 
1.0			29th April 2013				SPAN        		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerGiveAwayPageDeletion]
(

	--Input Parameter(s)--
	 @RetailID int	
   , @PageID int
   	
	--Output Variable--	  	 
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
		 
			--Create a record in QRRetailerCustomPage
			 UPDATE QRRetailerCustomPage SET StartDate ='1900-01-01'
			                                ,EndDate = '1900-01-01' 
			 WHERE QRRetailerCustomPageID = @PageID 
			 AND RetailID = @RetailID 				 
		
		    --Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAddPage.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
