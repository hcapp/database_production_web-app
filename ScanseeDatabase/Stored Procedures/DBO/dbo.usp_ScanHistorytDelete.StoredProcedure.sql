USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ScanHistorytDelete]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_ScanHistorytDelete]
Purpose					: To delete a Scan history item.
Example					: [usp_ScanHistorytDelete] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th June 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ScanHistorytDelete]
(
	  @ScanHistoryID int
	, @UserID int
	
	--Output Variable
	, @Result bit output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
					 
		    --Swipe delete from Scan history table.

					DELETE FROM ScanHistory
					WHERE UserID = @UserID
					AND ScanHistoryID = @ScanHistoryID 
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_ScanHistorytDelete].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
