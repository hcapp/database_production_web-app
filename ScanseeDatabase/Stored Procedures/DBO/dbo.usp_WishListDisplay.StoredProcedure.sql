USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WishListDisplay  
Purpose     : To dispaly User's Wish List.  
Example     : usp_WishListDisplay 2   
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   22nd June 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WishListDisplay]  
(  
   @UserID int  
 , @Latitude decimal(18,6)      
 , @Longitude decimal(18,6)      
 , @ZipCode varchar(10)      
 , @Radius int  
 --OutPut Variable  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
--To get Media Server Configuration.  
  DECLARE @Config varchar(50)    
  SELECT @Config=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'

 --While User search by Zipcode, coordinates are fetched from GeoPosition table.      
 IF (@Latitude IS NULL AND @Longitude IS NULL )      
 BEGIN      
  SELECT @Latitude = Latitude       
    , @Longitude = Longitude       
  FROM GeoPosition       
  WHERE PostalCode = @ZipCode       
 END      
 --If radius is not passed, getting user preferred radius from UserPreference table.      
 IF @Radius IS NULL       
 BEGIN       
  SELECT @Radius = LocaleRadius    
  FROM dbo.UserPreference WHERE UserID = @UserID      
 END  
   
  IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL )     
    
  BEGIN  
  
 --To get user's locationwise retail store infomation  
 SELECT ProductID  
     , RetailID   
     , RetailLocationID  
     , Distance  
     --, SalePrice  
     --, Price  
 INTO #Details  
 FROM (    
   SELECT UP.ProductID  
    , RL.RetailID   
    , RL.RetailLocationID  
    , Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214       
    --, MIN(RD.SalePrice) SalePrice   
    --, MIN(RD.Price) Price  
   FROM UserProduct UP  
    INNER JOIN RetailLocationProduct RP ON RP.ProductID = UP.ProductID   
    INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID   
    --LEFT JOIN RetailLocationDeal RD ON RD.RetailLocationID = RP.RetailLocationID AND RP.ProductID=RD.ProductID  
   WHERE UP.UserID = @UserID   
    AND UP.WishListItem = 1 AND RL.Active = 1
   GROUP BY UP.ProductID  
    , RL.RetailID   
    , RL.RetailLocationID  
    , RetailLocationLatitude  
    , RetailLocationLongitude  
   ) D  
 WHERE Distance <= ISNULL(@Radius, 5) 
 
 -- To get Deals
 SELECT RD.RetailLocationID 
 , RD.ProductID 
 , RD.SalePrice 
 , RD.Price 
 INTO #Deal
 FROM RetailLocationDeal RD
 INNER JOIN #Details D ON D.RetailLocationID = RD.RetailLocationID AND D.ProductID = RD.ProductID 
 WHERE GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
   
 --To get Coupons available for User's Wish List Products     
 SELECT C.CouponID   
  , CP.ProductID   
 INTO #Coupon    
 FROM Coupon C  
 INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID 
 INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID  
 INNER JOIN #Details D ON D.RetailID = CR.RetailID AND D.RetailLocationID = CR.RetailLocationID AND D.ProductID = CP.ProductID 
 WHERE CouponExpireDate >= GETDATE()    
    
    
 --To get Locationwise Hotdeals   
 SELECT DISTINCT ProductID  
 INTO #HotDeal  
 FROM (SELECT UP.ProductID   
   , Distance = (ACOS((SIN(PL.HotDealLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(PL.HotDealLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (PL.HotDealLongitude / 57.2958))))*6371) * 0.6214       
    FROM UserProduct UP  
   INNER JOIN HotDealProduct HP ON HP.ProductID = UP.ProductID   
   INNER JOIN ProductHotDeal P ON P.ProductHotDealID = HP.ProductHotDealID   
   INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = HP.ProductHotDealID  
    WHERE UP.UserID =  @UserID  
   AND UP.WishListItem = 1  
   AND GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate)HotDeal  
 WHERE Distance <= ISNULL(@Radius, 5) 
   
 SELECT UserProductID   
  , UserID   
  , ProductID  
  , WishListAddDate   
  , ProductName  
  , productimagepath   
  , ProductPrice   
  , SalePrice  
  , RetailFlag = CASE WHEN ISNULL(SalePrice, 0) = 0 THEN 0 ELSE 1 END  
  , CouponSaleFlag =(SELECT COUNT(CouponID) FROM #Coupon C WHERE C.ProductID = SaleProd.ProductID)  --(SELECT CASE WHEN COUNT(CouponID) = 0 THEN 0 ELSE 1 END FROM #Coupon C WHERE C.ProductID = SaleProd.ProductID)  
  , HotDealFlag =(SELECT COUNT(DISTINCT(ProductID)) FROM #HotDeal H WHERE H.ProductID = SaleProd.ProductID) --(SELECT CASE WHEN COUNT(ProductID) = 0 THEN 0 ELSE 1 END FROM #HotDeal H WHERE H.ProductID = SaleProd.ProductID)  
  , PushNotifyFlag  
 FROM        
  (    
   SELECT UP.UserProductID   
    , UP.UserID   
    , ISNULL(UP.ProductID, 0) ProductID  
    , UP.WishListAddDate   
    , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
    , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @Config																							
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END   
    , MIN(D.Price) ProductPrice   
    , MIN(D.saleprice) SalePrice  
    , UP.PushNotifyFlag   
       
   FROM UserProduct UP  
    LEFT JOIN #Deal D ON D.ProductID = UP.ProductID   
    LEFT JOIN Product P ON P.ProductID = UP.ProductID   
   WHERE UP.UserID = @UserID   
    AND UP.WishListItem = 1    
   GROUP BY UP.UserID   
    , UP.UserProductID      
    , UP.ProductID  
    , UP.WishListAddDate  
    , ProductImagePath  
    , UP.PushNotifyFlag  
    , CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
    , p.ManufacturerID
    , P.WebsiteSourceFlag     
  ) SaleProd  
  ORDER BY WishListAddDate, ProductName  
    
 END  
   
 IF(@Latitude IS NULL AND @Longitude IS NULL AND @ZipCode IS NULL)  
   
    SELECT UserProductID	
     , UserID   
     , ProductID  
     , WishListAddDate   
     , ProductName   
     , ProductImagePath  
     , ProductPrice  
     , SalePrice = (select MIN(SalePrice) from RetailLocationDeal rld where rld.ProductID = Prod.ProductID)
     , RetailFlag  = (select (case when COUNT(1)>0 then 1 else 0 end) from RetailLocationDeal rld where rld.ProductID = Prod.ProductID)
     , CouponSaleFlag = (SELECT COUNT(C.CouponID) FROM Coupon C INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID AND cp.ProductID=Prod.ProductID  
          WHERE  CouponExpireDate >= GETDATE() )  
          --(SELECT CASE WHEN COUNT(C.CouponID) = 0 THEN 0 ELSE 1 END FROM Coupon C INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID AND cp.ProductID=Prod.ProductID  
          --WHERE  CouponExpireDate >= GETDATE() )  
     , HotDealFlag = (SELECT COUNT(DISTINCT(HP.ProductHotDealID)) FROM HotDealProduct HP INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = HP.ProductHotDealID AND Prod.ProductID=hp.ProductID  
          WHERE  GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate)  
         --(SELECT CASE WHEN COUNT(HP.ProductID) = 0 THEN 0 ELSE 1 END FROM HotDealProduct HP INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = HP.ProductHotDealID AND Prod.ProductID=hp.ProductID  
         -- WHERE  GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate)  
     , PushNotifyFlag   
    FROM (SELECT U.UserProductID   
         , U.UserID   
         , ISNULL(U.ProductID, 0) ProductID  
         , U.WishListAddDate   
         , ProductName = CASE WHEN U.ProductID = 0 THEN U.UnassignedProductName ELSE P.ProductName END  
         , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @Config
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END  
         , NULL ProductPrice  
         , NULL SalePrice  
         , NULL RetailFlag  
         , U.PushNotifyFlag   
        FROM UserProduct U  
        INNER JOIN Product P ON U.ProductID=p.ProductID  
        WHERE UserID=@UserID AND WishListItem=1) Prod  
      
      
  
    
    
   --SELECT UserProductID   
   --  , UserID   
   --  , ProductID  
   --  , WishListAddDate   
   --  , ProductName  
   --  , productimagepath   
   --  , ProductPrice   
   --  , SalePrice  
   --  , RetailFlag = CASE WHEN ISNULL(SalePrice, 0) = 0 THEN 0 ELSE 1 END  
   --  , CouponSaleFlag = dbo.fn_WishListCoupon(ProductID)--(SELECT CASE WHEN COUNT(CouponID) = 0 THEN 0 ELSE 1 END FROM Coupon c WHERE c.ProductID = ProductID)  
   --  , HotDealFlag = dbo.fn_WishListHotDeal(ProductID)--(SELECT CASE WHEN COUNT(HotDealProductID) = 0 THEN 0 ELSE 1 END FROM HotDealProduct HP WHERE HP.ProductID = SaleProd.ProductID)  
   --  , PushNotifyFlag  
   --FROM        
   -- (  
   -- SELECT UP.UserProductID   
   --  , UP.UserID   
   --  , ISNULL(UP.ProductID, 0) ProductID  
   --  , UP.WishListAddDate   
   --  , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
   --  , ProductImagePath  
   --  , MIN(Price) ProductPrice   
   --  , MIN(saleprice) SalePrice   
   --  , UP.PushNotifyFlag       
   -- FROM UserProduct UP  
   --  LEFT JOIN Product P ON P.ProductID = UP.ProductID   
   --  LEFT JOIN RetailLocationProduct RL ON RL.ProductID=UP.ProductID AND GETDATE() BETWEEN RL.SaleStartDate AND RL.SaleEndDate  
   -- WHERE UP.UserID = @UserID   
   --  AND UP.WishListItem = 1  
       
   -- GROUP BY  UP.UserProductID   
   --     , UP.UserID   
   --     , UP.WishListAddDate  
   --     , UP.ProductID  
   --     , ProductImagePath  
   --     , UP.PushNotifyFlag  
   --     , CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
   -- ) SaleProd  
          
   --SELECT UP.UserProductID   
  --  , UP.UserID   
  --  , ISNULL(UP.ProductID, 0) ProductID  
  --  , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
  -- FROM UserProduct UP  
  --  LEFT JOIN Product P ON P.ProductID = UP.ProductID   
  -- WHERE UserID = @UserID   
  --  AND WishListItem = 1  
    
    
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WishListDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;




GO
