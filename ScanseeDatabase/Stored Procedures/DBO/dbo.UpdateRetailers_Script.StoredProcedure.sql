USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRetailers_Script]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [UpdateRetailers_Script]
Purpose					: To Update the retailers from the given excel.
Example					: [UpdateRetailers_Script]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Bindu T A           1.1
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UpdateRetailers_Script]
(
	  --@HubcitiID INT,

	--Output Variable
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN 

BEGIN TRY
	
	BEGIN TRANSACTION

		-- To Move things into Archive table

		INSERT INTO UpdateRetailerLoad_archive  ([Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Website,Latitude,Longitude,DateProcessed)
		SELECT 
		[Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Website,Latitude,Longitude,DateProcessed
		FROM UpdateRetailerLoad

		TRUNCATE TABLE UpdateRetailerLoad

		-- TO Move data from Sample table to main table

		INSERT INTO UpdateRetailerLoad ([Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Website,Latitude,Longitude,DateProcessed,RetailLocationImagePath)
		SELECT DISTINCT
		[Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Website,Latitude,Longitude,GETDATE(),RetailLocationImagePath
		FROM UpdateRetailerLoad_sample
	
		SELECT DISTINCT Z.* 
		INTO #Processed
		FROM UpdateRetailerLoad  Z
		INNER JOIN Retailer R ON Z.[Retail ID] = R.RetailID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = Z.[Retail LocationID] AND R.retailID = RL.retailID AND R.RetailID = Z.[Retail ID] 

		SELECT DISTINCT [Retail ID],[Retail LocationID], [Retail Name], Address,City,State,[Postal Code],Website,Latitude,Longitude
		INTO #UpdateRetailerLoad_Unprocessed
		FROM(
		SELECT [Retail ID],[Retail LocationID], [Retail Name], Address,City,State,[Postal Code],Website,Latitude,Longitude
		FROM UpdateRetailerLoad
		EXCEPT
		SELECT [Retail ID],[Retail LocationID], [Retail Name], Address,City,State,[Postal Code],Website,Latitude,Longitude
		FROM #Processed
		)P
	
	BEGIN TRAN
		UPDATE Retaillocation
		SET Address1 = Z.Address,
		City = Z.City,
		State = Z.state,
		PostalCode = Z.[Postal Code],
		RetailLocationURL = Z.website,
		RetailLocationLatitude = Z.Latitude,
		RetailLocationLongitude = Z.Longitude, 
		RetailLocationImagePath = Z.RetailLocationImagePath
		FROM #Processed Z
		INNER JOIN Retailer R ON Z.[Retail ID] = R.RetailID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = Z.[Retail LocationID] AND R.retailID = RL.retailID
		WHERE RL.Active=1

		UPDATE Retailer
		SET RetailName = Z.[retail Name]
		FROM #Processed Z 
		INNER JOIN Retailer R ON R.retailID = Z.[Retail ID]

		COMMIT


		--To update City & state changes

		UPDATE RetailLocation
		SET HccityID = C.HccityID
		FROM RetailLocation RL
		INNER JOIN HcCity C on RL.City = C.CityName

        UPDATE RetailLocation 
		SET StateID = C.StateID
		FROM RetailLocation RL
		INNER JOIN state C on RL.State = C.Stateabbrevation

		--To Update Longitude and Latitude for the Retailers 
				
		UPDATE RetailLocation 
		SET SINRetailLocationLatitude = SIN (RetailLocationLatitude/57.2958), 
		COSRetailLocationLatitude = COS (RetailLocationLatitude/57.2958), 
		COSRetailLocationLongitude = Cos (RetailLocationLongitude / 57.2958) 

		-- To Update the retailers having HeadQuarters/Corp&Store

		IF EXISTS (SELECT 1 FROM Retailer R INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID
		INNER JOIN #Processed Z ON Z.[Retail ID]= R.RetailID AND Z.[Retail LocationID] = RL.RetailLocationID
		AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1) AND RL.Active = 1)
		BEGIN
			UPDATE Retailer 
			SET RetailName= Z.[Retail Name],
			CorporatePhoneNo = C.ContactPhone ,
			Address1 = RL.Address1,
			City = RL.City,
			State = RL.State,
			PostalCode = RL.PostalCode
			FROM RetailLocation RL 
			INNER JOIN Retailer R ON R.retailID = RL.RetailID
			INNER JOIN #Processed Z ON Z.[Retail ID]= R.RetailID AND Z.[Retail LocationID] = RL.RetailLocationID
			INNER JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
			INNER JOIN Contact C ON C.ContactID = RC.ContactID
			WHERE  (RL.Headquarters = 1 OR RL.CorporateAndStore = 1) AND RL.Active = 1
		END

		-- To display Processed Retailer/RetailLocation

		SELECT * FROM #Processed

		--To display the unprocessed Retailer/RetailLocation

		SELECT * FROM #UpdateRetailerLoad_Unprocessed

		TRUNCATE TABLE UpdateRetailerLoad_sample

	COMMIT TRANSACTION

END TRY

BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		
		ROLLBACK TRANSACTION
		PRINT 'Error occured in Stored Procedure [UpdateRetailers_Script].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
		--Confirmation of failure.
		SELECT @Status = 1
	END
		 
END CATCH

END

GO
