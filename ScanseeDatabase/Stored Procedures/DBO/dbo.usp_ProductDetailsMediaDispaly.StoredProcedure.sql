USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductDetailsMediaDispaly]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ProductDetailsMediaDispaly
Purpose					: To diplay Product related Audio, Video and other Files.
Example					: usp_ProductDetailsMediaDispaly 1

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ProductDetailsMediaDispaly]
(
	 @ProductID int
	 ,@MediaType varchar(20)
	 
	 --OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
				
			--To get Server Configuration
			 DECLARE @ManufConfig varchar(50) 
			 DECLARE @Config varchar(50)
			  
			 SELECT @ManufConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
			 
			  SELECT @Config = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='App Media Server Configuration'  
			 
			-- To fetch Product Media info based on the media type sent.
			SELECT PM.ProductMediaID 
				, PM.ProductMediaName 
				, ProductMediaPath = CASE WHEN PM.ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																						   THEN @ManufConfig
																						   +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																						   +PM.ProductMediaPath 
																						 ELSE @Config+PM.ProductMediaPath 
																						 END   
										  ELSE PM.ProductMediaPath END  
			FROM ProductMedia PM
			INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID 
			LEFT JOIN Product P ON P.ProductID = PM.ProductID
			WHERE PM.ProductID = @ProductID 
				AND PT.ProductMediaType = @MediaType
	END TRY
		 
	BEGIN CATCH	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ProductDetailsMediaDispaly.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
