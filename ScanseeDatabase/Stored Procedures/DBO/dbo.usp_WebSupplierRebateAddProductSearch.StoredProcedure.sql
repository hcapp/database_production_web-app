USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierRebateAddProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierRebateAddProductSearch
Purpose					: To Search for the Product and Add Rebate on it.
Example					: usp_WebSupplierRebateAddProductSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th DEC 2011	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierRebateAddProductSearch]
(
	
	  @ManufacturerID int
	, @RetailerID int
	, @RetailerLocationID int
	, @Search Varchar(1000)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
				DECLARE @Config varchar(50)
				SELECT @Config=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
				
				SELECT  P.ProductID as productID
					   , P.ProductName as productName					 
					   , P.ProductShortDescription as shortDescription
					   , P.ScanCode as scanCode
					   , P.ProductLongDescription as longDescription
					   , RL.RetailID
					   , RL.RetailLocationID as retailerLocID
					   , productImagePath =CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @Config
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                                ELSE ProductImagePath END  
					   , P.ProductExpirationDate as prodExpirationDate
				FROM 				
				Product P 
				INNER JOIN RetailLocationProduct RLP ON RLP.ProductID = P.ProductID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID			
				WHERE P.ProductID <> 0 AND Active = 1 
				AND P.ManufacturerID = @ManufacturerID
				AND RL.RetailLocationID = @RetailerLocationID
		        AND RL.RetailID = @RetailerID
				AND (P.ProductName LIKE (CASE WHEN @Search IS NOT NULL THEN '%'+@Search+'%' ELSE '%' END)
					OR ScanCode LIKE @Search + '%')
		        
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierRebateAddProductSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;


GO
