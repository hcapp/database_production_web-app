USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveBusinessCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: 
Purpose					: To retrieve list of Categories from the system for Registration..
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			27th Jan 2012				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveBusinessCategory]

--Output variables
	@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			SELECT CategoryID, ParentCategoryName +' - '+ SubCategoryName parentSubCategory
			FROM Category
			ORDER BY ParentCategoryName, SubCategoryName ASC		
			
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveBusinessCategory.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
