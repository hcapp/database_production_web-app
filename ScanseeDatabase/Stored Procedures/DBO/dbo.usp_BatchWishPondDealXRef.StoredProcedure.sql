USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondDealXRef]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondDealXRef
Purpose					: To move data from staging table APIWishpondProductData to production APIWishpondDealXref table.
Example					: usp_BatchWishPondDealXRef

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			11th Oct 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondDealXRef]
(
	
	--Output Variable 
	 @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
				MERGE dbo.APIWishPondDealXRef AS T
					USING APIWishPondProductData AS S
					ON (T.WishPondDealID = S.DealID and T.WishPondItemID = S.ItemID AND T.WishPondRetailerID = S.APIWishpondLocId) 
					WHEN NOT MATCHED BY TARGET
						THEN INSERT ([WishPondRetailerID]
								   ,[WishPondDealID]
								   ,[WishPondItemID]
								   ,[WishPondDealName]
								   ,[WishPondProductName]
								   ,[WishPondSalePrice]
								   ,[WishPondDescription]
								   ,[WishPondThumbNailImageURL]
								   ,[WishPondLargeImageURL]
								   ,[DateCreated])								   
							 VALUES( S.APIWishpondLocId
									,S.DealId
									,S.ItemId
									,S.DealName
									,S.ProductName
									,S.SalePrice
									,S.Description
									,S.ThumbNailImageUrl
									,S.LargeImageURL
									,GETDATE())
					WHEN MATCHED 
						THEN UPDATE SET  T.[WishPondRetailerID] = S.APIWishpondLocId
									   ,T.[WishPondDealID] = S.DealId
									   ,T.[WishPondItemID] = S.ItemId
									   ,T.[WishPondDealName] = S.DealName
									   ,T.[WishPondProductName] = S.ProductName 
									   ,T.[WishPondSalePrice] = S.SalePrice
									   ,T.[WishPondDescription] = S.Description
									   ,T.[WishPondThumbNailImageURL] = S.ThumbNailImageUrl
									   ,T.[WishPondLargeImageURL]=S.LargeImageURL
									   ,T.[DateModified] = GETDATE();

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondDealXRef.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
