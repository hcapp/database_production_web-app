USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCLRProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_GetCLRProductDetails  
Purpose                             : To fetch Product Details.  
Example                             : usp_ProductDetails 2,1   
  
History  
Version           Date              Author                  Change Description  
---------------------------------------------------------------   
1.0               9th June 2012    SPAN Infotech India      Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_GetCLRProductDetails]  
(  
        @UserID int
		, @CouponID int
		, @LoyaltyID int
		, @RebateID int  
      --OutPut Variable  
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY  
      
		   --To get Media Server Configuration.  
		  DECLARE @ManufConfig varchar(50)    
		  SELECT @ManufConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 

		IF @CouponID IS NOT NULL
		BEGIN
			SELECT P.ProductID productId
			, P.ProductName productName
			, P.ProductShortDescription productShortDescription
			, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																									THEN @ManufConfig
																									+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																									+ProductImagePath ELSE ProductImagePath 
																							  END   
								  ELSE ProductImagePath END 
			FROM CouponProduct CP
			INNER JOIN Product P ON P.ProductID = CP.ProductID 
			WHERE CouponID = @CouponID 
		END

		IF @LoyaltyID IS NOT NULL
		BEGIN
			SELECT P.ProductID productId
			, P.ProductName productName
			, P.ProductShortDescription productShortDescription
			, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																									THEN @ManufConfig
																									+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																									+ProductImagePath ELSE ProductImagePath 
																							  END   
								  ELSE ProductImagePath END 
			FROM LoyaltyDeal L
			INNER JOIN Product P ON P.ProductID = L.ProductID 
			WHERE L.LoyaltyDealID = @LoyaltyID 
		END

		IF @RebateID IS NOT NULL
		BEGIN
			SELECT P.ProductID productId
			, P.ProductName productName
			, P.ProductShortDescription productShortDescription
			, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																									THEN @ManufConfig
																									+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																									+ProductImagePath ELSE ProductImagePath 
																							  END   
								  ELSE ProductImagePath END 
			FROM RebateProduct RP
			INNER JOIN Product P ON P.ProductID = RP.ProductID 
			WHERE RP.RebateID  = @RebateID  
		END
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_GetCLRProductDetails.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;

GO
