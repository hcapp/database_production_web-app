USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MaintenanceMergeRetailers]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MaintenanceMergeRetailers
Purpose					: To Merge multiple Retailer into single Retailer.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th Sept 2012  Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MaintenanceMergeRetailers]
(

	 @MergeRetailIDs VARCHAR(255)--Comma separated Retail Ids.
	,@RetainRetailID INT
	
	--Output Variable
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
			
			--Update Ads info
			UPDATE AdvertisementBanner 
			SET RetailID = @RetainRetailID
			FROM AdvertisementBanner AB
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = AB.RetailID
				
			UPDATE AdvertisementSplash 
			SET RetailID = @RetainRetailID
			FROM AdvertisementBanner ASP
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = ASP.RetailID
			
			--Update Loyalty Deals.
			UPDATE LoyaltyProgram 
			SET RetailID = @RetainRetailID
			FROM LoyaltyProgram LP
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = LP.RetailID
			
			--Update ProductHotDeal
			UPDATE ProductHotDeal 
			SET RetailID = @RetainRetailID
			FROM ProductHotDeal P
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = P.RetailID
			
			--Update Retailer Keywords			
			UPDATE ProductKeywordsRetailer
			SET RetailID = @RetainRetailID
			FROM ProductKeywordsRetailer P
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = P.RetailID
			
			--Update ProductReviews			
			UPDATE ProductReviews
			SET Retailid = @RetainRetailID
			FROM ProductReviews P
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = P.Retailid
			
			--Update QR Custom Pages.
			UPDATE QRRetailerCustomPage
			SET RetailID = @RetainRetailID
			FROM QRRetailerCustomPage QR
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = QR.RetailID
			
			--Update Rebate Table.
			UPDATE Rebate
			SET RetailID = @RetainRetailID
			FROM Rebate R
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = R.RetailID
			
			--Update RetailCategory
			UPDATE RetailCategory
			SET RetailID = @RetainRetailID
			FROM RetailCategory R
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = R.RetailID
			
			--Update Payent info
			UPDATE RetailerBankInformation
			SET RetailerID = @RetainRetailID
			FROM RetailerBankInformation R
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = R.RetailerID
			
			--Update Billing Details
			UPDATE RetailerBillingDetails
			SET RetailerID = @RetainRetailID
			FROM RetailerBillingDetails R
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = R.RetailerID
			
			--Retailer Keywords			
			UPDATE RetailerKeywords
			SET RetailID = @RetainRetailID 
			FROM RetailerKeywords R
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = R.RetailID
			
			--RetailLocation
			UPDATE RetailLocation
			SET RetailID = @RetainRetailID
			FROM RetailLocation RL
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = RL.RetailID
			
			--UserRetailerPreference
			UPDATE UserRetailPreference
			SET RetailID = @RetainRetailID
			FROM UserRetailPreference U
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = U.RetailID
			
			--UserRetailerPreference
			UPDATE QRUserRetailerSaleNotification
			SET RetailID = @RetainRetailID
			FROM QRUserRetailerSaleNotification U
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = U.RetailID
			
			--Business Category
			UPDATE RetailerBusinessCategory
			SET RetailerID = @RetainRetailID
			FROM RetailerBusinessCategory U
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = U.RetailerID
			
			--Capture the UserIDs to delete.
			SELECT UserID
			INTO #Temp
			FROM UserRetailer UR
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = UR.RetailID
			WHERE UR.RetailID <> @RetainRetailID
			
			--UserRetailer
			DELETE FROM UserRetailer
			FROM UserRetailer UR
			INNER JOIN dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = UR.RetailID
			WHERE UR.RetailID <> @RetainRetailID
			
		    --Users
			DELETE FROM Users
			FROM Users UR
			INNER JOIN #Temp T ON T.UserID = UR.UserID			
			
			--Delete Retailers.			
			DELETE FROM Retailer 
			FROM Retailer R 
			INNER JOIN  dbo.fn_SplitParam(@MergeRetailIDs, ',') M ON M.Param = R.RetailID
			WHERE R.RetailID <> @RetainRetailID
			
			
			--Delete the duplicate business categories.
			DELETE FROM RetailerBusinessCategory
			WHERE RetailerBusinessCategoryID NOT IN(SELECT MIN(RetailerBusinessCategoryID)RetailerBusinessCategoryID
													FROM RetailerBusinessCategory
													GROUP BY RetailerID, BusinessCategoryID)
													
			
			
			--Confirmation of Success
			SET @Status = 0
			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MaintenanceMergeRetailers.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
