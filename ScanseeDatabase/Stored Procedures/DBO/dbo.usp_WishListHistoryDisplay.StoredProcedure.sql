USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListHistoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WishListHistoryDisplay  
Purpose     :   
Example     : usp_WishListHistoryDisplay  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   04 Sep 2011  SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WishListHistoryDisplay]  
(  
   @userid int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
 --Output Variable   
 , @MaxCnt INT OUTPUT
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
     --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
   
  --To get the row count for pagination.  
   DECLARE @UpperLimit int   
   SELECT @UpperLimit = @LowerLimit + ScreenContent   
   FROM AppConfiguration   
   WHERE ScreenName = @ScreenName   
    AND ConfigurationType = 'Pagination'  
    AND Active = 1  
   --DECLARE @MaxCnt int  
   
    
  --To get the information of the products present in the wish list history.  
      SELECT   Row_Num=ROW_NUMBER() OVER (ORDER BY U.WishListAddDate desc ,U.Userproductid)  
     , U.userproductid   
     , U.ProductID productId  
     , ProductName  
              --, MIN(Price) productPrice  
              --, MIN(saleprice) salePrice  
     --,ProductLongDescription
     ,ProductShortDescription productShortDescription
     , ProductLongDescription productLongDescription
     , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END   
     , U.WishListAddDate  
   INTO #WishListHistory  
   FROM userproductHistory U  
   --INNER JOIN UserProduct up on up.UserProductID=u.userproductid  
   INNER JOIN Product P ON P.ProductID=U.Productid  
   LEFT JOIN RetailLocationProduct RP on RP.ProductID=P.ProductID  
   WHERE U.Userid=@userid and U.wishlistitem=1  
      GROUP BY  U.userproductid     
     , U.ProductID  
     , ProductName  
     , ProductLongDescription
     , ProductShortDescription
     , ProductImagePath  
     , U.WishListAddDate
     , P.ManufacturerID
     , P.WebsiteSourceFlag  
   ORDER BY U.WishListAddDate 
     
  --To capture max row number.  
  SELECT @MaxCnt = MAX(Row_Num) FROM #WishListHistory  
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
  SELECT Row_Num row_Num  
   , userproductid   
   , productId  
   , ProductName
   , ProductLongDescription
   , ProductShortDescription  
   , ProductImagePath  
   , WishListAddDate  
  FROM #WishListHistory  
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
  Order by Row_Num  
    
    
     
    
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WishListHistoryDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
     
  END;  
     
 END CATCH;  
END;


GO
