USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchYipitDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchYipitDataPorting
Purpose					: To move data from Stage Yipit table to Production ProductHotDeal Table
Example					: usp_BatchYipitDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th Aug 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchYipitDataPorting]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'Yipit'
			
			DECLARE @Yipit TABLE([ProductHotDealID] int
								,[RetailID] int
								,[APIPartnerID] int
								,[HotDealProvider] varchar(100)
								,[Price] money
								,[HotDealDiscountType] varchar(20)
								,[HotDealDiscountAmount] money
								,[HotDealDiscountPct] float
								,[SalePrice] money
								,[HotDealName] varchar(255)
								,[HotDealShortDescription] varchar(1000)
								,[HotDeaLonglDescription] varchar(3000)
								,[HotDealImagePath] varchar(1000)
								,[HotDealTermsConditions] varchar(1000)
								,[HotDealURL] varchar(1000)
								,[Expired] bit
								,[HotDealStartDate] datetime
								,[HotDealEndDate] datetime
								,[APIDeleteFlag] bit
								,[SourceID] varchar(100)
								,[Category] varchar(100)
								,[CreatedDate] datetime
								,[DateModified] datetime
								,[CategoryID] int)
								
			--INSERT INTO [ProductHotDealHistory]
			--	([ProductHotDealID]
			--	,[ProductID]
			--	,[RetailID]
			--	,[APIPartnerID]
			--	,[HotDealProvider]
			--	,[Price]
			--	,[HotDealDiscountType]
			--	,[HotDealDiscountAmount]
			--	,[HotDealDiscountPct]
			--	,[SalePrice]
			--	,[HotDealName]
			--	,[HotDealShortDescription]
			--	,[HotDeaLonglDescription]
			--	,[HotDealImagePath]
			--	,[HotDealTermsConditions]
			--	,[HotDealURL]
			--	,[Expired]
			--	,[HotDealStartDate]
			--	,[HotDealEndDate]
			--	,[APIDeleteFlag]
			--	,[SourceID]
			--	,[Category]
			--	,[CreatedDate]
			--	,[DateModified]
			--	,[CategoryID])
			--SELECT [ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,1
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--		,[DateModified]
			--		,[CategoryID]
			--FROM
			--	(
			--		MERGE ProductHotDeal AS T
			--		USING APIYipitData AS S
			--		ON (T.SourceID = S.ID) 
			--		WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
			--			THEN INSERT([SourceID]
			--							 ,[APIPartnerID]
			--							 ,[Price]
			--							 ,[SalePrice]
			--							 ,[HotDealName]
			--							 ,[HotDealShortDescription]
			--							 ,[HotDealImagePath]
			--							 ,[HotDealURL]
			--							 ,[HotDealStartDate]
			--							 ,[HotDealEndDate]
			--							 ,[Category]
			--							 ,[CreatedDate]) 
			--					 VALUES(S.[ID]
			--							,@APIPartnerID
			--							,S.[Price]
			--							,S.[SalePrice]
			--							,S.[Title]
			--							,S.[Yipit_Title]
			--							,S.[Image_small]
			--							,S.[Url]
			--							,S.[Date_added]
			--							,S.[End_date]
			--							,S.[Category]
			--							,S.[CreatedDate])
			--		WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID
			--			THEN UPDATE SET   T.[Price] = S.[Price]
			--							 ,T.[SalePrice] = S.[SalePrice]
			--							 ,T.[HotDealName] = S.[Title]
			--							 ,T.[HotDealShortDescription] = S.[Yipit_Title]
			--							 ,T.[HotDealImagePath] = S.[Image_small]
			--							 ,T.[HotDealURL] = S.[Url]
			--							 ,T.[HotDealStartDate] = S.[Date_added]
			--							 ,T.[HotDealEndDate] = S.[End_date]
			--							 ,T.[Category] = S.[Category]
			--							 ,T.[DateModified] = S.[CreatedDate]
			--		WHEN NOT MATCHED BY SOURCE AND T.[APIPartnerID] = @APIPartnerID
			--			THEN DELETE 
			--		OUTPUT $action, Deleted.*
			--	) AS HotDeal
			--	(
			--		 [Action]
			--		,[ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--		,[DateModified]
			--		,[CategoryID]
			--	)
			--WHERE [Action] = 'DELETE'

			INSERT INTO @Yipit
					([ProductHotDealID]
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID])
			SELECT [ProductHotDealID]
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,1
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM
				(
					MERGE ProductHotDeal AS T
					USING APIYipitData AS S
					ON (T.SourceID = S.ID) 
					WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
						THEN INSERT([SourceID]
										 ,[APIPartnerID]
										 ,[Price]
										 ,[SalePrice]
										 ,[HotDealName]
										 ,[HotDealShortDescription]
										 ,[HotDealImagePath]
										 ,[HotDealDiscountPct]
										 ,[HotDealURL]
										 ,[HotDealStartDate]
										 ,[HotDealEndDate]
										 ,[Category]
										 ,[CreatedDate]) 
								 VALUES(S.[ID]
										,@APIPartnerID
										,S.[Value]
										,S.[SalePrice]
										,S.[Title]
										,S.[Yipit_Title]
										,S.[Image_small]
										,S.[Discount]
										,S.[Url]
										,S.[Date_added]
										,S.[End_date]
										,S.[Category]
										,S.[CreatedDate])
					WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID
						THEN UPDATE SET   T.[Price] = S.[Value]
										 ,T.[SalePrice] = S.[SalePrice]
										 ,T.[HotDealName] = S.[Title]
										 ,T.[HotDealShortDescription] = S.[Yipit_Title]
										 ,T.[HotDealImagePath] = S.[Image_small]
										 ,T.[HotDealDiscountPct] = S.[Discount]
										 ,T.[HotDealURL] = S.[Url]
										 ,T.[HotDealStartDate] = S.[Date_added]
										 ,T.[HotDealEndDate] = S.[End_date]
										 ,T.[Category] = S.[Category]
										 ,T.[DateModified] = S.[CreatedDate]
					WHEN NOT MATCHED BY SOURCE AND T.[APIPartnerID] = @APIPartnerID
						THEN DELETE 
					OUTPUT $action, Deleted.*
				) AS HotDeal
				(
					 [Action]
					,[ProductHotDealID]
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
					,ManufacturerID
					, HotDealTimeZoneID
					, WebsiteSourceFlag
				)
			WHERE [Action] = 'DELETE'
			
			
			INSERT INTO [ProductHotDealHistory]
					([ProductHotDealID]
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID])
			SELECT [ProductHotDealID]
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,1
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM @Yipit
			
			
			--To delete hot deals from hot deal product table when the deals are moved from history
			
			DELETE from HotDealProduct
			from HotDealProduct H
			INNER JOIN @Yipit Y on H.ProductHotDealID=Y.ProductHotDealID		

			

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchYipitDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
