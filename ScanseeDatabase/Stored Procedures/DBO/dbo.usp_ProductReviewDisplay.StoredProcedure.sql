USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductReviewDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ProductReviews
Purpose					: To display the Product reviews. 
Example					: usp_ProductReviews

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04th Nov 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ProductReviewDisplay]
(
	  @ProductID int
	--Output Variable 
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			SELECT  ProductReviewsID productReviewsID
				   ,ProductID productID
				   ,ReviewURL reviewURL
				   ,ReviewComments reviewComments 
			FROM ProductReviews 
			WHERE  ProductID=@ProductID
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ProductReviews.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
