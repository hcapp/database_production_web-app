USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerWelcomePageDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerSplashAdDeletion
Purpose					: To delete a Splash Ad.
Example					: usp_WebRetailerSplashAdDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerWelcomePageDeletion]
(
	--Input Variables
	  @RetailID int
	, @WelcomePageID int
	
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 DECLARE @ADStartDate date
			 DECLARE @ADEndDate date
			 
			 SELECT @ADStartDate = StartDate
				  , @ADEndDate = EndDate
			 FROM AdvertisementSplash
			 WHERE AdvertisementSplashID = @WelcomePageID
			 
			 --Confirm if the Ad is inactive before deletion.
			 IF NOT EXISTS(SELECT 1 FROM AdvertisementSplash WHERE AdvertisementSplashID = @WelcomePageID AND GETDATE() BETWEEN StartDate AND EndDate)
			 BEGIN
				--Move the Ad to archive.
				INSERT INTO AdvertisementSplashArchive(AdvertisementSplashID
													 , SplashAdName
													 , SplashAdDescription
													 , SplashAdImagePath
													 , SplashAdURL
													 , StartDate
													 , EndDate
													 , WebsiteSourceFlag
													 , DateCreated)
										  SELECT AdvertisementSplashID
													 , SplashAdName
													 , SplashAdDescription
													 , SplashAdImagePath
													 , SplashAdURL
													 , StartDate
													 , EndDate
													 , WebsiteSourceFlag
													 , GETDATE()
										  FROM AdvertisementSplash
										  WHERE AdvertisementSplashID = @WelcomePageID
					
					--Move the Retail Location association to the Archive.					  
					INSERT INTO RetailLocationSplashAdArchive(RetailLocationID
													 , AdvertisementSplashID
													 , DateCreated)
												SELECT RetailLocationID
													 , AdvertisementSplashID
													 , GETDATE()
												FROM RetailLocationSplashAd
												WHERE AdvertisementSplashID = @WelcomePageID
												
					
					--Delete the child table.
					DELETE FROM RetailLocationSplashAd WHERE AdvertisementSplashID = @WelcomePageID
					--Delete from the master table.
					DELETE FROM AdvertisementSplash WHERE AdvertisementSplashID = @WelcomePageID												
			 END
			
			
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerSplashAdDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
