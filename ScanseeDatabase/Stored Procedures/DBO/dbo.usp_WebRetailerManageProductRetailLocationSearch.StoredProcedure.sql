USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerManageProductRetailLocationSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerManageProductRetailLocationSearch
Purpose					: To display the Products Associated in RetailLocation.
Example					: usp_WebRetailerManageProductRetailLocationSearch

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			19th November 2012				MOHITH H R	    Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerManageProductRetailLocationSearch]
(

	--Input Parameter(s)--
	 
	   @RetailerID int
	 , @ProductID int
	 , @SearchKey varchar(1000)
	 --, @LowerLimit int
	 --, @ScreenName varchar(50)
	 
	--Output Variable-- 
	
	--, @NxtPageFlag bit output 
	, @Product int output
	, @ProductName varchar(255) output
	, @ProductImagePath varchar(1000) output	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
	
)
AS
BEGIN

	BEGIN TRY
	
	    --To get the row count for pagination.
		--DECLARE @UpperLimit int 
		--SELECT @UpperLimit = @LowerLimit + ScreenContent 
		--FROM AppConfiguration 
		--WHERE ScreenName = @ScreenName 
		--	AND ConfigurationType = 'Pagination'
		--	AND Active = 1
		--Declare @MaxCnt int 
		
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'	
		
		SELECT @ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),ManufacturerID)+'/'+  ProductImagePath
										ELSE ProductImagePath END
			 , @ProductName = ProductName
			 , @Product = ProductID								
		FROM Product
		WHERE ProductID = @ProductID							
	
		--Get the RetailLocations associated with the input Retailer.
		SELECT --Row_Num = IDENTITY(INT, 1, 1) 
			   RL.RetailLocationID
			 , RL.StoreIdentification
			 , Price
			 , SalePrice
			 , SaleStartDate
			 , SaleEndDate	
			 , Added = CASE WHEN RLP.RetailLocationProductID IS NULL THEN 0 ELSE 1 END
		INTO #Product	 
		FROM RetailLocation RL
		LEFT JOIN RetailLocationProduct RLP ON RLP.RetailLocationID=RL.RetailLocationID AND RLP.ProductID= @ProductID AND RL.Active = 1
		WHERE RL.RetailID = @RetailerID
		AND RL.StoreIdentification LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%' + @SearchKey + '%' ELSE '%' END
		
		 
		--To capture max row number.
		--SELECT @MaxCnt = MAX(Row_Num) FROM #Product
					
		--this flag is a indicator to enable "More" button in the UI. 
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		--SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

		SELECT --Row_Num  
		          RetailLocationID
		        , StoreIdentification
				, Price
				, SalePrice
				, SaleStartDate
				, SaleEndDate	
				, Added AS productAdded
		FROM #Product
		--WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
		--ORDER BY Row_Num		
					
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerManageProductRetailLocationSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
