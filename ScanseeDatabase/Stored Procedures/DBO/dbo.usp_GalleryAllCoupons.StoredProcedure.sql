USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryAllCoupons]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_GalleryAllCoupons  
Purpose     : To display all the coupons related to the user.  
Example     : [usp_GalleryAllCoupons]  
  
History  
Version  Date        Author   Change Description  
---------------------------------------------------------------   
1.0   15th Oct 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_GalleryAllCoupons]  
(  
   @Userid int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
	--To get Media Server Configuration.  
  DECLARE @RetailConfig varchar(50)    
  SELECT @RetailConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Retailer Media Server Configuration' 
  
 --To get the row count for pagination.  
  DECLARE @UpperLimit int   
  SELECT @UpperLimit = @LowerLimit + ScreenContent   
  FROM AppConfiguration   
  WHERE ScreenName = @ScreenName   
   AND ConfigurationType = 'Pagination'  
   AND Active = 1  
  DECLARE @MaxCnt int  
     
    
       
  SELECT  Row_Num=ROW_NUMBER() over(order by C.couponid)  
    ,C.CouponID   
    ,CouponName  
    ,CouponDiscountType  
    ,CouponDiscountAmount  
    ,CouponDiscountPct  
    ,CouponShortDescription  
    ,CouponLongDescription  
    ,CouponDateAdded  
    ,CouponStartDate  
    ,CouponExpireDate  
    ,CouponURL  
    ,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN DBO.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																										CASE WHEN WebsiteSourceFlag = 1 
																											THEN @RetailConfig
																											+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																											+CouponImagePath 
																									    ELSE CouponImagePath 
																									    END
																								 END 
					 END  
    ,Fav_Flag=CASE WHEN C.CouponID=UC.CouponID THEN 1 ELSE 0 END  
    ,ViewableOnWeb
  INTO #AllCoupons  
  FROM Coupon c  
   LEFT JOIN UserCouponGallery UC ON c.couponid=uc.CouponID AND UserID=@Userid    
   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate  
    
  --To capture max row number.  
  SELECT @MaxCnt = MAX(Row_Num) FROM #AllCoupons  
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
  SELECT  Row_Num rowNum  
    ,CouponID   
    ,CouponName  
    ,CouponDiscountType  
    ,CouponDiscountAmount  
    ,CouponDiscountPct  
    ,CouponShortDescription  
    ,CouponLongDescription  
    ,CouponDateAdded  
    ,CouponStartDate  
    ,CouponExpireDate  
    ,CouponURL  
    ,CouponImagePath  
    ,Fav_Flag added  
    ,ViewableOnWeb
  FROM #AllCoupons  
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
  Order by Row_Num  
    
        
  ----To get Image of the product  
  -- DECLARE @ProductImagePath TABLE(couponid int,productimagepath varchar(1000))  
  -- INSERT INTO @ProductImagePath(CouponID,ProductImagePath)  
  -- SELECT C.CouponID, ProductImagePath   
  -- FROM Product P  
  -- INNER JOIN Coupon C ON C.ProductID=P.ProductID  
  -- INNER JOIN UserCouponGallery UC ON UC.CouponID=c.CouponID  
  -- WHERE UserID=@Userid  
     
  --SELECT  uc.CouponID   
  --  ,CouponName  
  --  ,CouponDiscountType  
  --  ,CouponDiscountAmount  
  --  ,CouponDiscountPct  
  --  ,CouponShortDescription  
  --  ,CouponLongDescription  
  --  ,CouponDateAdded  
  --  ,CouponStartDate  
  --  ,CouponExpireDate  
  --  ,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN productimagepath ELSE CouponImagePath END  
  --FROM Coupon c  
  -- INNER JOIN UserCouponGallery UC ON c.couponid=uc.CouponID  
  -- INNER JOIN @ProductImagePath P ON P.couponid=UC.CouponID  
  --WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate  
  --    AND UserID=@userid  
    
    
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_GalleryAllCoupons.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
