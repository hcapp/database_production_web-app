USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerMasterShoppingListDeleteProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerMasterShoppingListDeleteProducts
Purpose					: To delete product from Favorite list 
Example					: usp_WebConsumerMasterShoppingListDeleteProducts 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th July 2013	Dhananjaya TR	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerMasterShoppingListDeleteProducts]
(
	  @UserID int
	, @UserProductID varchar(max)	
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE UserProduct
			SET MasterListItem = 0				
				, MasterListRemoveDate = GETDATE()				
			FROM UserProduct UP
			INNER JOIN fn_SplitParam (@UserProductID, ',') P ON P.Param  = UP.UserProductID  
			WHERE UserID = @UserID 		
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerMasterShoppingListDeleteProducts.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
