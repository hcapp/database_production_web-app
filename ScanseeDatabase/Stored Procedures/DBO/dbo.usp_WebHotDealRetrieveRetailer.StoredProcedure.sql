USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHotDealRetrieveRetailer]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebHotDealRetrieveRetailer
Purpose					: To retrieve list of Retailer that is having the input product.
Example					: usp_WebHotDealRetrieveRetailer

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			5th January 2012				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHotDealRetrieveRetailer]

--Input parameters
	
    @ProductID varchar(100)

--Output variables
	,@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		
			--Display only those Retailers which has the input products.
			SELECT DISTINCT R.RetailID
			     , R.RetailName
			FROM Retailer R
			INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND RL.Active = 1 AND R.RetailerActive = 1
			INNER JOIN RetailLocationProduct RLP ON RL.RetailLocationID = RLP.RetailLocationID	
			INNER JOIN dbo.fn_SplitParam(@ProductID, ',') P ON P.Param = RLP.ProductID			
		
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealRetrieveRetailer.'		
			END;
		 
	END CATCH;
END;




GO
