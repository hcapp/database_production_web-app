USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealSearchPopulationCenter]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HotDealPopulationCenterSearch
Purpose					: To fetch the DMA.
Example					: usp_HotDealSearchPopulationCenter

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			15th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HotDealSearchPopulationCenter]
(

	--Input Parameter(s)--
	  
	  @PopulationCenter varchar(100)	  
	  
	
	--Output Variable--
	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		SELECT PopulationCenterID, DMAName
		FROM PopulationCenters
		WHERE DMAName LIKE '%' + @PopulationCenter +'%'
		
		
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HotDealSearchPopulationCenter.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;

GO
