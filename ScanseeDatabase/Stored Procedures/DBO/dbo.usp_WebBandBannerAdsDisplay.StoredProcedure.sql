USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandBannerAdsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template

/*
Stored Procedure name   :  [usp_WebBandBannerAdsDisplay]
Purpose                  : To display the Banner ads created by the given Band.
Example                  : [usp_WebBandBannerAdsDisplay]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               04/18/2016      Prakash C   Initial Version (usp_WebBandBannerAdsDisplay)                                       
-------------------------------------------------------------------------------
*/
--exec usp_WebBandBannerAdsDisplay 1,null,0,1,null,null,null,null

CREATE PROCEDURE [dbo].[usp_WebBandBannerAdsDisplay]
(
      --Input Input Parameter(s)--  
	   @RetailID INT
	 , @SearchParameter VARCHAR(1000)
	 , @LowerLimit INT
	 , @ShowAll BIT --To include expired Ads

      --Output Variable--
	  , @RowCount INT OUTPUT
	  , @NextPageFlag BIT OUTPUT
	  , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 

)
AS
BEGIN

      BEGIN TRY
		DECLARE @UpperLimit INT    
		DECLARE @MaxCnt INT  

    		  --To get the row count for pagination.      

			 DECLARE @ScreenContent Varchar(100)    

			 SELECT @ScreenContent = 
			 ScreenContent       
			 FROM AppConfiguration       
			 WHERE ScreenName = 'All'     
			 AND ConfigurationType = 'Website Pagination'    
			 AND Active = 1   

		    SET @UpperLimit = @LowerLimit + @ScreenContent   

		    --To Fetch the Band Media server Configuaration      
			DECLARE @Config varchar(255)    
			SELECT @Config=
			ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='App Band Media Server Configuration'  AND Active =1  		

			DECLARE @Temp TABLE(RowNum INT IDENTITY(1, 1)
								   , BandLocationAdvertisementID INT
								   , advertisementName VARCHAR(255)
								   , strBannerAdImagePath VARCHAR(1000)
								   , advertisementDate DATE
								   , advertisementEndDate DATE
								   , DeleteFlag BIT
								   , ExpireFlag BIT)

			  --Select the ads that are currently running & those that start in future. This will be common in both the events where @ShowAll is 0 or 1.

			   INSERT INTO @Temp(BandLocationAdvertisementID  
				, advertisementName  
				, strBannerAdImagePath 
				, advertisementDate  
				, advertisementEndDate  
				, DeleteFlag  
				, ExpireFlag)

			 SELECT DISTINCT AB.bandAdvertisementBannerID as BandLocationAdvertisementID
			 ,AB.BannerAdName as advertisementName 
			 ,@Config +'retailer'+'/'+CONVERT(VARCHAR(30),AB.BandID)+'/' + BannerAdImagePath AS strBannerAdImagePath 
			 ,advertisementDate = AB.StartDate   
			,advertisementEndDate = AB.EndDate 						 
			 ,DeleteFlag = CASE WHEN AB.StartDate > GETDATE() THEN 1 ELSE 0 END
			 ,ExpireFlag = CASE WHEN (CONVERT(DATE, GETDATE())>=CONVERT(DATE, AB.StartDate) AND CONVERT(DATE, GETDATE())<=CONVERT(DATE, ISNULL(AB.EndDate, GETDATE() + 1))) THEN 1 ELSE 0 END																									      																								      
				FROM bandAdvertisementBanner  AB 
				INNER JOIN BandLocationBannerAd  RLB ON AB.bandAdvertisementBannerID = RLB.AdvertisementBannerID  
			--	INNER JOIN BandLocation RL ON RLB.BandLocationID = RL.BandLocationID 
				WHERE AB.BandID =@RetailID --AND AB.Active = 1
			AND GETDATE() <= ISNULL(AB.EndDate, GETDATE() + 1)
			AND AB.BannerAdName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END

				--Select the expired Ads when @ShowAll is 1
				IF (@ShowAll = 1)
					BEGIN
				 INSERT INTO @Temp(BandLocationAdvertisementID  
				   , advertisementName  
				  , strBannerAdImagePath 
				  , advertisementDate  
				 , advertisementEndDate  
				  , DeleteFlag  
				  , ExpireFlag)

				SELECT DISTINCT AB.bandAdvertisementBannerID as BandLocationAdvertisementID
					 ,AB.BannerAdName as advertisementName 
					 ,@Config +'retailer'+'/'+CONVERT(VARCHAR(30),AB.BandID)+'/'+ BannerAdImagePath AS strBannerAdImagePath 
					,advertisementDate = CASE WHEN AB.StartDate = '01/01/1900' THEN (SELECT StartDate 
					   FROM bandAdvertisementBannerHistory 
					   WHERE bandAdvertisementBannerID = AB.bandAdvertisementBannerID
					   AND DateCreated = (SELECT MAX(DateCreated) 
					 FROM bandAdvertisementBannerHistory
					   WHERE bandAdvertisementBannerID = AB.bandAdvertisementBannerID)) ELSE AB.StartDate END  
					, advertisementEndDate = CASE WHEN AB.EndDate = '01/01/1900' THEN (SELECT EndDate 
					  FROM bandAdvertisementBannerHistory 
					 WHERE bandAdvertisementBannerID = AB.bandAdvertisementBannerID
						AND DateCreated = (SELECT MAX(DateCreated) 
							  FROM bandAdvertisementBannerHistory
		  							  WHERE bandAdvertisementBannerID = AB.bandAdvertisementBannerID)) ELSE AB.EndDate END  
					, DeleteFlag = CASE WHEN AB.StartDate > GETDATE() THEN 1 ELSE 0 END
					 , ExpireFlag = CASE WHEN (CONVERT(DATE, GETDATE())>=CONVERT(DATE, AB.StartDate) AND CONVERT(DATE, GETDATE())<=CONVERT(DATE, ISNULL(AB.EndDate, GETDATE() + 1))) THEN 1 ELSE 0 END																									      																								      
					FROM bandAdvertisementBanner  AB 
					INNER JOIN BandLocationBannerAd  RLB ON AB.bandAdvertisementBannerID = RLB.AdvertisementBannerID  
					--INNER JOIN BandLocation RL ON RLB.BandLocationID = RL.BandLocationID 
					WHERE AB.BandID =@RetailID --AND RL.Active = 1
					AND AB.BannerAdName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END
					AND GETDATE() > isnull(AB.EndDate,getdate()-1)

			END

		SELECT @MaxCnt = COUNT(RowNum) FROM @Temp 

		--Output Total no of Records
		SET @RowCount = @MaxCnt 

				--CHECK IF THERE ARE SOME MORE ROWS        
		SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  

		
--Display the records in the given limit


			DECLARE @Tempp TABLE(RowNum INT IDENTITY(1, 1)
								   , BandLocationAdvertisementID INT
								   , advertisementName VARCHAR(255)
								   , strBannerAdImagePath VARCHAR(1000)
								   , advertisementDate DATE
								   , advertisementEndDate DATE
								   , DeleteFlag BIT
								   , ExpireFlag BIT)



		    SELECT distinct 
		    BandLocationAdvertisementID  as RetailLocationAdvertisementID
		  , advertisementName  
		  , strBannerAdImagePath  
		  , advertisementDate  
		  , advertisementEndDate
		  , DeleteFlag
		  , ExpireFlag
		  into #tt
	 	  FROM @Temp	
		
		insert into @Tempp
		select *  from  #tt

		select RowNum, BandLocationAdvertisementID  as RetailLocationAdvertisementID	  , advertisementName  
		  , strBannerAdImagePath  
		  , advertisementDate  
		  , advertisementEndDate
		  , DeleteFlag
		  , ExpireFlag from @Tempp
		  WHERE RowNum BETWEEN (@LowerLimit + 1) AND (@UpperLimit)              

      END TRY

      BEGIN CATCH 

            --Check whether the Transaction is uncommitable.

            IF @@ERROR <> 0
		   BEGIN
			  PRINT 'Error occured in Stored Procedure usp_WebBandBannerAdsDisplay.'           
			   -- Execute retrieval of Error info.
			  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
				  insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
			values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())       

           END;

      END CATCH;

END;





GO
