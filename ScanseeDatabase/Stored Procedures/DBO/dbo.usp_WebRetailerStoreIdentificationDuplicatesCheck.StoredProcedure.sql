USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerStoreIdentificationDuplicatesCheck]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerStoreIdentificationDuplicatesCheck]
Purpose					: 
Example					: [usp_WebRetailerStoreIdentificationDuplicatesCheck]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03 April 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerStoreIdentificationDuplicatesCheck]
(
	   @RetailID int 	 
	,  @StoreIdentification varchar(max)  --CSV StoreIdentification	 
	
	--Output Variable 
	, @DuplicateStores varchar(max) output
	, @Status bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 
		   SELECT @DuplicateStores = COALESCE(@DuplicateStores+',','') + StoreIdentification
		   FROM RetailLocation RL
		   INNER JOIN dbo.fn_SplitParam(@StoreIdentification, ',') S ON RL.StoreIdentification = S.Param	
		   AND RL.Active = 1
		   	
		--Confirmation of Success.
			SELECT @Status = 0
		
		 
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerUpdateRetailerLocations].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
