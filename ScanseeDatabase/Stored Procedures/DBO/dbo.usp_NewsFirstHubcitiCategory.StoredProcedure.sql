USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_NewsFirstHubcitiCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_NewsFirstHubcitiCategory]
Purpose					: To enable news for only for given hubcitis.
Example					: [usp_NewsFirstHubcitiCategory]

HIStory
VersiON		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			01/14/2016	    SPAN           1.0
---------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[usp_NewsFirstHubcitiCategory]
AS
BEGIN

	SELECT DISTINCT H.HcHubCitiID
					,H.HubCitiName
					,C.NewsCategoryID
					,C.NewsCategoryName
					,NS.NewsFeedURL URL
					,0 IsSubCategory
					,null NewsSubCategoryName
					,null NewsSubCategoryURL
	FROM NewsFirstSettings NS  
	INNER JOIN HcHubCiti H ON NS.HcHubCitiID = H.HcHubCitiID
	INNER JOIN NewsFirstCategory C ON C.NewsCategoryID = NS.NewsCategoryID
	INNER JOIN NewsFirstCatSubCatAssociation AA ON AA.CategoryID = C.NewsCategoryID AND AA.HcHubcitiID = H.HcHubCitiID
	WHERE H.Active=1 AND C.Active=1 AND NewsFirstFlag = 1 AND NS.IsNewsFeed = 1 --and h.HcHubCitiID= 2264

	UNION ALL
		
	SELECT DISTINCT H.HcHubCitiID
				,H.HubCitiName
				,C.NewsCategoryID
				,C.NewsCategoryName
				,NULL URL
				,1 IsSubCategory
				,SC.NewsSubCategoryName
				,SUB.NewsFirstSubCategoryURL NewsSubCategoryURL
	FROM NewsFirstSettings NS  
	INNER JOIN HcHubCiti H ON NS.HcHubCitiID = H.HcHubCitiID
	INNER JOIN NewsFirstCategory C ON C.NewsCategoryID = NS.NewsCategoryID
	INNER JOIN NewsFirstSubCategoryType T ON T.NewsCategoryID = NS.NewsCategoryID
	INNER JOIN NewsFirstSubCategory SC ON SC.NewsSubCategoryTypeID = T.NewsSubCategoryTypeID
	INNER JOIN NewsFirstSubCategorySettings SUB ON SUB.NewsCategoryID = NS.NewsCategoryID AND SC.NewsSubCategoryID = SUB.NewsSubCategoryID 
	INNER JOIN NewsFirstCatSubCatAssociation AA ON AA.CategoryID = C.NewsCategoryID AND AA.HcHubcitiID = H.HcHubCitiID AND AA.SubCategoryID = SC.NewsSubCategoryID
	AND NS.NewsCategoryID = SUB.NewsCategoryID AND NS.HcHubCitiID = SUB.HcHubCitiID   
	WHERE H.Active=1 AND C.Active=1 AND SUB.NewsFirstSubCategoryURL IS NOT NULL AND NewsFirstFlag = 1 AND NS.IsNewsFeed = 1 --and h.HcHubCitiID= 2264
	ORDER BY HcHubCitiID,C.NewsCategoryID

END



GO
