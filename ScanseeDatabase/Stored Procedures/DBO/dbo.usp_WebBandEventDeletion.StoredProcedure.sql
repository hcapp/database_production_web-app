USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandEventDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebBandEventDeletion]
Purpose					: Deleting Selected  Event.
Example					: [usp_WebBandEventDeletion]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			04/20/2016      Bindu T A        1.0 - usp_WebBandEventDeletion
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[usp_WebBandEventDeletion]
(   
    --Input variable.	  
      @EventID Int
	
  	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			DECLARE @bandID INT 
			SET @BandID = @EventID

			--Logical Delete
			UPDATE HcBandEvents	SET Active = 0
			WHERE HcBandEventID = @BandID


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebBandEventDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			INSERT  INTO SCANSEEVALIDATIONERRORS(ERRORCODE,ERRORLINE,ERRORDESCRIPTION,ERRORPROCEDURE)
            VALUES(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
