USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAnythingPageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebBandAnythingPageDisplay]
Purpose                  : To display the Welcome pages created by the given Band.
Example                  : [usp_WebBandAnythingPageDisplay]

History
Version           Date                Author				Change Description
------------------------------------------------------------------------------- 
1.0               18th Apr 2016       Sagar Byali			usp_WebRetailerAnythingPageDisplay                                
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandAnythingPageDisplay]
(

      --Input Input Parameter(s)--  
      
        @RetailID INT
      , @SearchParameter VARCHAR(1000)
      , @LowerLimit int
      
      --Output Variable--
	  , @RowCount INT OUTPUT
	  , @NextPageFlag BIT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
		     DECLARE @UpperLimit INT    
			 DECLARE @MaxCnt INT        
      
			  --To get the row count for pagination.      
			 DECLARE @ScreenContent Varchar(100)    
			 SELECT @ScreenContent = ScreenContent       
			 FROM AppConfiguration       
			 WHERE ScreenName = 'All'     
			 AND ConfigurationType = 'Website Pagination'    
			 AND Active = 1   
		 
		    SET @UpperLimit = @LowerLimit + @ScreenContent   
		    --To Fetch the Band Media server Configuaration      
		
			--A large number is taken in the isnull function to handle Null sort orders.
		    SELECT RowNum = ROW_NUMBER() OVER (ORDER BY ISNULL(SortOrder, 1000000),Pagetitle ASC)
				 , QRBandCustomPageID
		         , Pagetitle 
				 , [Start Date]
				 , [Start Time]  
				 , [End Date] 
				 , [End Time]
				 , SortOrder
			INTO #Temp
			FROM(    
            
				SELECT DISTINCT QRC.QRBandCustomPageID  as QRBandCustomPageID
				    ,QRC.Pagetitle as Pagetitle
				    ,CONVERT(DATE,QRC.StartDate) as [Start Date]
				    ,CONVERT(Time,QRC.StartDate) as [Start Time]
				    ,CONVERT(DATE,QRC.EndDate) as [End Date] 
					,CONVERT(Time,QRC.EndDate) as [End Time]
					,SortOrder 
					FROM QRBandCustomPage  QRC
					INNER JOIN QRTypes QRT ON QRC.QRTypeID=QRT.QRTypeID 
					WHERE QRT.QRTypeName LIKE 'Anything Page' 
					AND QRC.BandID =@RetailID 
					AND Pagetitle  LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END) AnythingPage     
								   
				   
			
		    
			SELECT @MaxCnt = COUNT(RowNum) FROM #Temp
		    --Output Total no of Records
		    SET @RowCount = @MaxCnt 
		
		    --CHECK IF THERE ARE SOME MORE ROWS        
		    SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  
		
	    	--Display the records in the given limit
		    SELECT RowNum iRowId
		         , QRBandCustomPageID as QRRetailerCustomPageID
		         , Pagetitle 
				 , [Start Date] 
				 , [Start Time]
				 , [End Date]
				 , [End Time] 	
				 , SortOrder iSortOrderID		
		    FROM #Temp
		    WHERE RowNum BETWEEN (@LowerLimit + 1) AND (@UpperLimit)                  
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebBandAnythingPageDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;



GO
