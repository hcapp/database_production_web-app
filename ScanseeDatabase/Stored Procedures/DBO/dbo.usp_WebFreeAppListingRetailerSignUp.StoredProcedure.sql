USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFreeAppListingRetailerSignUp]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebFreeAppListingRetailerSignUp]
Purpose					: To register New Retailer. 
Example					: [usp_WebFreeAppListingRetailerSignUp]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			01st April 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFreeAppListingRetailerSignUp]
(

	 @RetailerName Varchar(100)
	,@Address1 Varchar(100)
	,@Address2 Varchar(50)
	,@City Varchar(50)
	,@State Char(2)	
	,@PostalCode Varchar(5)
	,@CorporatePhoneNo Char(10)
	,@RetailLocationLatitude float
	,@RetailLocationLongitude float
	
	--Input Variables of Users--
	--,@UserName varchar(100)
	--,@Password varchar(60)
				
	--Output Variable 
	--, @ResponseUserID int output
	, @ResponseRetailID int output
	, @ResponseRetailLocationID int output	
	, @ResponseCount bit output
	--, @DuplicateFlag bit output
	, @DuplicateRetailerFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
			--IF EXISTS (SELECT 1 FROM Users WHERE UserName = @UserName)
			--BEGIN
			--	SET @DuplicateFlag=1
			--	SET @DuplicateRetailerFlag=0
			--	SET @Status=0			
			--END
		    IF EXISTS(SELECT 1 FROM Retailer R
						  LEFT JOIN UserRetailer UR ON R.RetailID = UR.RetailID
						  --INNER JOIN RetailerBillingDetails RB ON R.RetailID = RB.RetailerID
						  WHERE RetailName = @RetailerName AND R.Address1 = @Address1  
						  AND CorporatePhoneNo = @CorporatePhoneNo AND R.City = @City AND R.[State] = @State AND RetailerActive = 1)			
			BEGIN						
					SET @DuplicateRetailerFlag=1
					SET @ResponseCount=0
					SET @Status = 0	
			END					
			ELSE
			BEGIN	
			
			----INSERT INTO USERS TABLE
			--INSERT INTO Users
			--			(UserName
			--			,Password
			--			,DateCreated)
			--	VALUES (@UserName
			--		   ,@Password
			--		   ,GETDATE())				
					
			
			--SET @ResponseUserID=SCOPE_IDENTITY();	
			
		    -- INSERT INTO RETAILER TABLE
			INSERT INTO [Retailer]
					   ([RetailName]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[CorporatePhoneNo]
					   ,[CountryID]					  					   
					   ,[DateCreated]									
					   )
			VALUES( @RetailerName
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   ,@CorporatePhoneNo
				   ,1							  
				   ,GETDATE())				 	   				   
				   			
			SET @ResponseRetailID=SCOPE_IDENTITY(); 
			
			----INSERT INTO USERRETAILER CHILD TABLE			
				   
			--INSERT INTO UserRetailer
			--		   ([RetailID]
			--		   ,[UserID])
			--		   --,[AdminFlag])
			--VALUES(@ResponseRetailID
			--	   ,@ResponseUserID)
			--	   ,@AdminFlag)
			
			--To update City state changes
			UPDATE Retailer 
			SET HccityID = C.HccityID
			FROM Retailer R
			INNER JOIN HcCity C on R.City = C.CityName
			WHERE Retailid = @ResponseRetailID

            UPDATE Retailer 
			SET StateID = C.StateID
			FROM Retailer R
			INNER JOIN state C on R.State = C.Stateabbrevation
			WHERE Retailid = @ResponseRetailID
			
			--INSERT INTO RETAILLOCATION TABLE
			
			INSERT INTO [RetailLocation]
					   ([RetailID]
					   ,[Headquarters]					 
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[CorporateAndStore]					   
					   ,[CountryID]					   
					   ,[DateCreated] 
					   ,RetailLocationLatitude 
					   ,RetailLocationLongitude)
			VALUES(@ResponseRetailID
				   ,1			  				   
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode	
				   ,0				   			  				  
                   ,1						   				  
				   ,GETDATE()
				   ,CASE WHEN @RetailLocationLatitude IS NULL OR @RetailLocationLatitude = 0 THEN (SELECT Latitude FROM GeoPosition WHERE PostalCode = @PostalCode) ELSE @RetailLocationLatitude END
				   ,CASE WHEN @RetailLocationLongitude IS NULL OR @RetailLocationLongitude = 0 THEN (SELECT Longitude FROM GeoPosition WHERE PostalCode = @PostalCode) ELSE @RetailLocationLongitude END
				   )
				   
			SET @ResponseRetailLocationID=SCOPE_IDENTITY();

			--To update City state changes
			UPDATE RetailLocation 
			SET HccityID = C.HccityID
			FROM RetailLocation R
			INNER JOIN HcCity C on R.City = C.CityName
			WHERE Retaillocationid = @ResponseRetailLocationID

            UPDATE RetailLocation 
			SET StateID = C.StateID
			FROM RetailLocation R
			INNER JOIN state C on R.State = C.Stateabbrevation
			WHERE Retaillocationid = @ResponseRetailLocationID
		
			SELECT DISTINCT R.RetailID
				   ,RL.RetailLocationID 
				   ,R.RetailName
				   ,RL.Address1
				   ,RL.Address2
				   ,RL.City
				   ,RL.[STATE]
				   ,RL.PostalCode
				   ,CASE WHEN RL.CorporateAndStore = 0 THEN C.ContactPhone ELSE R.CorporatePhoneNo END	CorporatePhoneNo		   
			FROM Retailer R					
			INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID	
			LEFT JOIN RetailContact RC ON RL.RetailLocationID= RC.RetailLocationID
			LEFT JOIN Contact C ON RC.ContactID=C.ContactID						
			LEFT JOIN UserRetailer UR ON R.RetailID=UR.RetailID		
			WHERE RetailName LIKE '%'+@RetailerName+'%' 
			AND RL.Address1 LIKE '%'+@Address1+'%'  
			AND CASE WHEN RL.CorporateAndStore = 0 THEN C.ContactPhone ELSE R.CorporatePhoneNo END = @CorporatePhoneNo  
			AND RL.City = @City 
			AND RL.[State] = @State
			AND R.RetailID <> @ResponseRetailID
			AND UR.UserRetailerID IS NULL 
			AND RL.Headquarters = 0 AND RL.Active = 1	
			
			SET @ResponseCount=CASE WHEN @@ROWCOUNT>0 THEN 1 ELSE 0 END
			
			
			--SET @DuplicateFlag=0
			SET @DuplicateRetailerFlag=0	
			SET @Status = 0	
												
		 END 	
	   COMMIT TRANSACTION
	END TRY
		
		
	BEGIN CATCH	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebFreeAppListingRetailerSignUp].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
