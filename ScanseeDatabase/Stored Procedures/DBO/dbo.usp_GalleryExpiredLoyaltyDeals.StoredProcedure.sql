USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryExpiredLoyaltyDeals]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_MyGalleryExpiredLoyaltyDeals  
Purpose     : To dispaly Loyalty.  
Example     : usp_MyGalleryExpiredLoyaltyDeals   
  
History  
Version  Date      Author   Change Description  
---------------------------------------------------------------   
1.0   15th Oct 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_GalleryExpiredLoyaltyDeals]  
(  
  @UserID int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
   
 BEGIN TRY  
 
  --To get Media Server Configuration.  
  DECLARE @ManufConfig varchar(50)    
  SELECT @ManufConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
   
   --To get the row count for pagination.  
    DECLARE @UpperLimit int   
    SELECT @UpperLimit = @LowerLimit + ScreenContent   
    FROM AppConfiguration   
    WHERE ScreenName = @ScreenName   
     AND ConfigurationType = 'Pagination'  
     AND Active = 1  
    DECLARE @MaxCnt int  
      
   SELECT Row_Num=ROW_NUMBER() over(order by L.LoyaltyDealID)  
      ,L.LoyaltyDealID    
      ,LoyaltyDealName  
      ,LoyaltyDealDiscountType  
      ,LoyaltyDealDiscountAmount  
      ,LoyaltyDealDiscountPct  
      ,LoyaltyDealDescription  
      ,LoyaltyDealDateAdded  
      ,LoyaltyDealStartDate  
      ,LoyaltyDealExpireDate  
      ,ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END   
   INTO #Loyalty  
   FROM LoyaltyDeal L  
   LEFT JOIN Product P ON  P.ProductID=L.ProductID  
   INNER JOIN UserLoyaltyGallery CG ON CG.LoyaltyDealID = L.LoyaltyDealID AND UserID=@UserID  
   WHERE LoyaltyDealExpireDate < GETDATE() AND DATEDIFF(day,GETDATE(),LoyaltyDealExpireDate)<=90  
     
   --To capture max row number.  
    SELECT @MaxCnt = MAX(Row_Num) FROM #Loyalty  
    --this flag is a indicator to enable "More" button in the UI.   
    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
  
   SELECT Row_Num rowNum  
      ,LoyaltyDealID    
      ,LoyaltyDealName  
      ,LoyaltyDealDiscountType  
      ,LoyaltyDealDiscountAmount  
      ,LoyaltyDealDiscountPct  
      ,LoyaltyDealDescription  
      ,LoyaltyDealDateAdded  
      ,LoyaltyDealStartDate  
      ,LoyaltyDealExpireDate  
      ,ProductImagePath imagePath  
   FROM #Loyalty   
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   Order by Row_Num  
    
    
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_MyGalleryExpiredLoyaltyDeals.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
