USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerDataCleanUpIndividual]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerDataCleanUpIndividual
Purpose					: 
Example					: usp_WebRetailerDataCleanUpIndividual

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			2nd Feb 2012	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerDataCleanUpIndividual]
(
	@UserName varchar(100)
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @UserID int 
			SELECT @UserID = UserID
			FROM Users 
			WHERE BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName)

			SELECT RetailID
			INTO #Retailer
			FROM UserRetailer
			WHERE UserID = @UserID

			SELECT C.CouponID
			INTO #Coupon
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID= CR.CouponID
			INNER JOIN #Retailer R ON R.RetailID=CR.RetailID

			SELECT RE.RebateID 
			INTO #Rebate
			FROM Rebate RE
			INNER JOIN RebateRetailer RR ON RE.RebateID=RR.RebateID 
			INNER JOIN #Retailer R ON R.RetailID=RR.RetailID

			SELECT ProductHotDealID
			INTO #HotDeal
			FROM ProductHotDeal P
			INNER JOIN #Retailer R ON R.RetailID=P.RetailID


			--Add's

			DELETE FROM RetailLocationAdvertisement
			FROM RetailLocationAdvertisement RA
			INNER JOIN RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID AND RL.Active = 1
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID

			--Retailer data deletion

			DELETE FROM RetailCategory
			FROM RetailCategory R
			INNER JOIN #Retailer RR ON R.RetailID=RR.RetailID

			DELETE FROM ProductReviews
			FROM ProductReviews P
			INNER JOIN #Retailer RR ON P.RetailID=RR.RetailID

			DELETE FROM UserRetailPreference
			FROM UserRetailPreference U
			INNER JOIN #Retailer R ON U.RetailID=R.RetailID

			DELETE FROM RetailerBankInformation
			from RetailerBankInformation RB
			INNER JOIN #Retailer R ON R.RetailID=RB.RetailerID

			DELETE FROM RetailerBillingDetails
			FROM RetailerBillingDetails RB
			INNER JOIN #Retailer R ON R.RetailID=RB.RetailerID

			DELETE FROM RetailLocationProduct
			FROM RetailLocationProduct PA
			INNER JOIN RetailLocation RL ON PA.RetailLocationID=RL.RetailLocationID AND RL.Active = 1
			INNER JOIN #Retailer R ON R.RetailID=rl.RetailID

			DELETE FROM RetailContact
			FROM RetailContact R
			INNER JOIN RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID AND RL.Active = 1 
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID

			----Product related tables

			DELETE FROM RebateProduct
			FROM RebateProduct PA
			INNER JOIN RebateRetailer RR ON PA.RebateID=RR.RebateID
			inner join #Retailer R ON R.RetailID=RR.RetailID

			DELETE FROM HotDealProduct
			FROM HotDealProduct PA
			INNER JOIN ProductHotDeal P on p.ProductHotDealID=PA.ProductHotDealID
			INNER JOIN #Retailer R ON  R.RetailID=P.RetailID

			DELETE FROM ProductUserHit
			FROM ProductUserHit PA
			INNER JOIN RetailLocation RL ON rl.RetailLocationID=PA.RetailLocationID AND RL.Active = 1
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID

			----Coupon related tables 

			DELETE FROM CouponRetailer
			FROM CouponRetailer CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  
			INNER JOIN #Retailer R ON R.RetailID=CP.RetailID

			DELETE FROM UserCouponGallery
			FROM UserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 			
			
			DELETE FROM CouponProduct
			FROM CouponProduct CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID
			
			DELETE FROM Coupon
			FROM Coupon CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID
			
			DELETE FROM Coupon
			FROM Coupon CP
			INNER JOIN #Retailer C ON C.RetailID = CP.RetailID

			----Rebate related tables

			DELETE FROM RebateRetailer
			FROM RebateRetailer  RP
			INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 
			INNER JOIN #Retailer RR ON RP.RetailID=RR.RetailID

			DELETE FROM ScanHistory
			FROM ScanHistory  RP
			INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 
			INNER JOIN RetailLocation RL ON RP.RetailLocationID=RL.RetailLocationID AND RL.Active = 1
			INNER JOIN #Retailer RT ON RT.RetailID=RL.RetailID

			DELETE FROM UserRebateGallery
			FROM UserRebateGallery  RP
			INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 
			INNER JOIN RetailLocation RL ON RL.RetailLocationID=RP.RetailLocationID AND RL.Active = 1
			INNER JOIN #Retailer RR ON RR.RetailID=RL.RetailID

			----ProductHotDeal related tables.

			DELETE FROM ProductHotDealRetailLocation
			FROM ProductHotDealRetailLocation P
			INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 

			DELETE FROM RetailLocation
			FROM RetailLocation RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID AND RL.Active = 1
			
			DELETE FROM ProductHotDeal
			FROM ProductHotDeal RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID

			DELETE FROM UserRetailer 
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)

			DELETE FROM Retailer 
			WHERE RetailID IN (SELECT RetailID FROM #Retailer) AND RetailerActive = 1

			DELETE FROM Users 
			WHERE UserID=@UserID

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerDataCleanUpIndividual.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
