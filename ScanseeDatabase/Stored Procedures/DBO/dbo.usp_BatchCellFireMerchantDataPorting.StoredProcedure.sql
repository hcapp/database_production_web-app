USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireMerchantDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireMerchantDataPorting
Purpose					: To move Retailers from Stage to Retailer Table and maintain the association in the CellfireMerchant table.
Example					: usp_BatchCellFireMerchantDataPorting

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			23rd October 2012   Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireMerchantDataPorting]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE	@APIPartnerID INT
			
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'CellFire'
		
			--To get the row of APIRowCount which was inserted at the time of deleting duplicates.
			DECLARE @Date DATETIME
			SELECT @Date = MAX(ExecutionDate)
			FROM APIBatchLog 
			WHERE APIPartnerID = @APIPartnerID 
			AND CONVERT(DATE,ExecutionDate) = CONVERT(DATE,GETDATE())
		
			UPDATE APIBatchLog 
			SET APIRowCount = (SELECT COUNT(1) FROM APICellFireCouponDetails)
			WHERE APIPartnerID = @APIPartnerID 
			AND ExecutionDate = @Date		    	
			
			CREATE TABLE #Retailer(RetailID INT)
			
			--Capture the newly inserted Retailers
			INSERT INTO #Retailer(RetailID)
			SELECT RetailID
			FROM(		
			
					MERGE INTO Retailer AS T
					USING APICellFireMerchants AS S
					ON LTRIM(RTRIM(T.RetailName)) = LTRIM(RTRIM(S.MerchantName))
					WHEN MATCHED THEN 
						UPDATE SET CellFireMerchantID = S.MerchantID
								 , CellFireMerchantSmallImage = S.MerchantSmallImage 
								 , CellFireMerchantLargeImage = S.MerchantLargeImage
					WHEN NOT MATCHED THEN
						INSERT(RetailName
							 , CountryID
							 , CellFireMerchantID
							 , DateCreated)
						VALUES(S.MerchantName
							 , 1
							 , MerchantID
							 , S.CreatedDate)
						OUTPUT $ACTION, Inserted.RetailID) AS Ret([Action], RetailID)
			WHERE [ACTION] = 'INSERT'
			
			--Create dummy RetailLocations for the newly created Retailers.
			INSERT INTO RetailLocation(RetailID
									  ,Headquarters
									  ,CountryID
									  ,DateCreated)	
							SELECT RetailID
								 , 1
								 , 1
								 , GETDATE()
							FROM #Retailer
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireMerchantDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
