USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsDisplay_Original]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssFeedNewsDisplay
Purpose					: 
Example					: usp_WebRssFeedNewsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebRssFeedNewsDisplay_Original]
(	
	  @NewsType varchar(500)
	, @HcHubCitiID int  		
	--Output Variable 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		  

		  IF @NewsType='top'
		  BEGIN
			
			--to display first 12 records
			SELECT TOP 12 Id
					,NewsType
					,Title
					,ImagePath
					,shortDesc
					,description
					,Link
					,PublishedDate
					,PubStrtDate
			INTO #Top12
			FROM 
			(SELECT RssFeedNewsID Id
					,NewsType
					,Title
					,ImagePath
					,ShortDescription shortDesc
					,LongDescription description
					,Link
					,PublishedDate
					,convert(datetime,PublishedDate,105) PubStrtDate
			  FROM RssFeedNews RFN
			  WHERE NewsType = 'top' AND HcHubCitiID = @HcHubCitiID
			  AND RFN.Title IS NOT NULL

			  UNION ALL
			  SELECT RssFeedNewsID Id
						,NewsType
						,Title
						,ImagePath
						,ShortDescription shortDesc
						,LongDescription description
						,Link
						,PublishedDate
						,convert(datetime,PublishedDate,105) PubStrtDate
			  FROM RssFeedNews RFN
			  WHERE NewsType = 'breaking' AND HcHubCitiID = @HcHubCitiID AND RFN.Title IS NOT NULL) A
			  ORDER BY Id

			

			  --to display records except top 12 records
			  SELECT Id
					,NewsType
					,Title
					,ImagePath
					,shortDesc
					,description
					,Link
					,PublishedDate
					,PubStrtDate
			INTO #AllRecords
			FROM 
			(
				SELECT RssFeedNewsID Id
					,NewsType
					,Title
					,ImagePath
					,ShortDescription shortDesc
					,LongDescription description
					,Link
					,PublishedDate
					,convert(datetime,PublishedDate,105) PubStrtDate
			  FROM RssFeedNews RFN
			  WHERE NewsType = 'top' AND HcHubCitiID = @HcHubCitiID AND RFN.Title IS NOT NULL
			  UNION ALL
			  SELECT RssFeedNewsID Id
						,NewsType
						,Title
						,ImagePath
						,ShortDescription shortDesc
						,LongDescription description
						,Link
						,PublishedDate
						,convert(datetime,PublishedDate,105) PubStrtDate
			  FROM RssFeedNews RFN
			  WHERE NewsType = 'breaking' AND HcHubCitiID = @HcHubCitiID AND RFN.Title IS NOT NULL) A
			  ORDER BY Id
			 

			  SELECT * FROM #AllRecords
			  EXCEPT
			  SELECT * FROM #Top12

			  SELECT * FROM #Top12
			 

			END
			ELSE
			BEGIN

				SELECT RssFeedNewsID Id
					,NewsType
					,Title
					,ImagePath
					,ShortDescription shortDesc
					,LongDescription description
					,Link
					,CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END PublishedDate
					,convert(datetime,PublishedDate,105) PubStrtDate
					,Classification
					,Section
					,AdCopy
			  FROM RssFeedNews RFN
			  WHERE HcHubCitiID = @HcHubCitiID 
			  AND ((RFN.Title IS NOT NULL AND @NewsType <> 'Classifields')
					 OR (RFN.Title IS NULL AND @NewsType = 'Classifields'))
			  AND (NewsType = @NewsType OR @NewsType IS NULL)

			  ORDER BY Id

			-- insert  into hist SELECT client_net_address,getdate() ,object_name(@@procid)  FROM sys.dm_exec_connections 

			END		
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;


GO
