USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayProductQRPageURL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayRetailerCustomPageURL
Purpose					: To Display the Retailer Custom Page URLs.
Example					: usp_WebDisplayRetailerCustomPageURL

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23rd May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayProductQRPageURL]
(
	  
	  @UserID int
	, @ProductID int
	, @PageID int  
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 
		 DECLARE @Config varchar(50)
		 		 		
		 SELECT @Config= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='QR Code Configuration'
		 
		 
		SELECT URL =  CASE WHEN QRP.QRProductTypeID = (SELECT QRProductTypeID FROM QRProductType WHERE QRProductTypeDescription = 'Product Page') 
							THEN @Config + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName = 'Product Page') AS VARCHAR(10)) + '.htm?key=' + CAST(@ProductID AS VARCHAR(10))
						   END						
	    FROM QRProduct QRP
	    INNER JOIN QRProductType QRPT ON QRPT.QRProductTypeID = QRP.QRProductTypeID
	    LEFT OUTER JOIN QRProductMedia QRPM ON QRPM.QRProductID = QRP.QRProductID
	    WHERE QRP.QRProductID = @PageID
	    AND QRP.ProductID = @ProductID
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
