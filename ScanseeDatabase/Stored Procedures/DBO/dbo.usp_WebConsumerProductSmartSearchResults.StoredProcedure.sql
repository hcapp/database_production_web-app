USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerProductSmartSearchResults]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ProductSmartSearchResults
Purpose					: To display the products grouped by category names.
Example					: usp_ProductSmartSearchResults

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd Sept 2012	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerProductSmartSearchResults]
(
	
   --Input Variables 
     @UserID int
   , @ProdSearch varchar(500)
   , @RecordCount INT
  -- , @ParentCategoryID varchar(10)
   , @LowerLimit int  
   
   -- User Tracking
   ,@MainmenuID int
   
	--Output Variable 
	, @NxtPageFlag bit output
	, @MaxCnt int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	    DECLARE @Config varchar(50)  
	    DECLARE @Tomorrow DATETIME = GETDATE()+1  
	    DECLARE @MediaTypeID INT
	    DECLARE @Today DATETIME =GETDATE()
		
		SELECT @Config = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		--To get the row count for pagination.
		DECLARE @UpperLimit int 
		SELECT @UpperLimit = @LowerLimit + @RecordCount
			
		
		SELECT @MediaTypeID = ProductMediaTypeID 
		FROM ProductMediaType
		WHERE ProductMediaType = 'Image Files'
		
		--First ProductID in a category is displayed for all the categories. 
		--If the count = 1, this productid is used to get the product details 
		--else if the count>1 then another procedure is called to get the list of products.
		
		CREATE TABLE #Temp(RowNum INT IDENTITY(1, 1)
						   , ProductID INT
						   , ProductName VARCHAR(255)
						   , ProductImagePath VARCHAR(1000)
						   , SuggestedRetailPrice MONEY
						   , ProductShortDescription Varchar(2000)) 
		
		
		
		CREATE TABLE #Prod(ProductID INT)
				
		declare @sql Varchar(2000)			
		
		SELECT @sql='SELECT top 500 P.ProductID
				
		FROM Product P
		LEFT JOIN ProductKeywords PK ON PK.ProductID = P.ProductID	AND PK.ProductKeyword LIKE ''%'+ @ProdSearch +'%''
		WHERE (P.ProductName LIKE ''%'+ @ProdSearch +'%''
		OR P.ScanCode LIKE ''%'+ @ProdSearch +'%'' OR P.ISBN LIKE ''%'+ @ProdSearch +'%'' )
		ORDER BY P.ProductName ASC'
		--OR PK.ProductKeyword LIKE ''%'+ @ProdSearch +'%'' 
		
		
		
		insert into #Prod (ProductID)
		EXEC (@sql)
		
		
		
		INSERT INTO #Temp(ProductID
						, ProductName
						, ProductImagePath
						, SuggestedRetailPrice
						,ProductShortDescription)
		SELECT  P.ProductID
			 , P.ProductName
			 , ProdImgPath = ISNULL(CASE WHEN WebsiteSourceFlag = 1 
								THEN @Config + CAST(ManufacturerID AS VARCHAR(10)) + '/' + ProductImagePath
							ELSE ProductImagePath END, '') --+ ', '
							-- + ISNULL(STUFF((SELECT + ', ' +CASE WHEN WebsiteSourceFlag = 1 
							--				                    THEN  @Config + CAST(ManufacturerID AS VARCHAR(10)) + '/' + B.ProductMediaPath
							--							    ELSE B.ProductMediaPath END  
							--FROM ProductMedia b 
							--WHERE b.ProductID = P.ProductID
							--AND B.ProductMediaTypeID = @MediaTypeID
							--FOR XML PATH('')), 1, 2, ''), '')
			 , SuggestedRetailPrice		
			 ,P.ProductShortDescription 				
		FROM #Prod T
		INNER JOIN Product P ON P.ProductID =T.ProductID 	
		WHERE  ISNULL(P.ProductExpirationDate,@Tomorrow )>=@Today		
		ORDER BY ProductName 	
		 
		
		--Calculate the total number of records.
		SELECT @MaxCnt = MAX(RowNum) FROM #Temp	
		
		--Check if the next page exixts.
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
	
		SELECT RowNum rowNumber
			 , ProductID productId
			 , ProductName productName			
			 , CASE WHEN CHARINDEX(', ', productImagePath) = LEN(productImagePath) THEN REPLACE(productImagePath, ', ', '') ELSE productImagePath END productImagePath			 		  
			 , SuggestedRetailPrice
			 , ProductShortDescription productShortDescription
		INTO #Product
		FROM #Temp
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit	
		 	
		
		
		
		--User Tracking Section.
			
			--Capture the category that the user has selected.
																				 
			
			CREATE TABLE #Temp1(ProductListID INT								
							  , ProductSmartSearchID INT
							  , ProductID INT)
							  
			
			
			INSERT INTO ScanSeeReportingDatabase..ProductSmartSearch (MainMenuID
													        , SearchKeyword 													      
													        , CreatedDate)
			
		    SELECT @MainmenuID 
			    	, @ProdSearch 			    	
			    	, GETDATE()
		    FROM #Temp
		    
		    DECLARE @ProductSmartSearchID int 
		    set @ProductSmartSearchID =SCOPE_IDENTITY()			  
							  
							  
			
			INSERT INTO ScanSeeReportingDatabase..ProductList(ProductSmartSearchID
													        , MainMenuID
													        , ProductID
													        , CreatedDate
													        )
							OUTPUT inserted.ProductListID, inserted.ProductSmartSearchID, inserted.ProductID INTO #Temp1(ProductListID, ProductSmartSearchID, ProductID)
													   SELECT @ProductSmartSearchID
															, @MainMenuID
															, productId
															, GETDATE()
													   FROM #Temp
			--Display along with the primary key of the tracking table.
					   
			SELECT  rowNumber
			     , T.ProductListID 
				 ,  P.productId
				 ,  productName			
				 ,  productImagePath			 		  
				 , SuggestedRetailPrice
				 , productShortDescription productShortDescription
			FROM #Product P
			INNER JOIN #Temp1 T ON T.ProductID = P.productId
			ORDER BY ProductName   
		
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ProductSearchResults.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
