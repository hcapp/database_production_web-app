USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebsiteProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_ProductDetails  
Purpose                             : To fetch Product Details.  
Example                             : usp_ProductDetails 2,1   
  
History  
Version           Date              Author                  Change Description  
---------------------------------------------------------------   
1.0               26th July 2011    Padmapriya M      Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebsiteProductDetails]  
(  
        @UserID int  
      , @ProductID int  
      , @RetailID int  
      , @ScanLongitude float    
   , @ScanLatitude float   
      --OutPut Variable  
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY  
      
      --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'        

     
            -- To capture the existance of the Media for the specified product.  
            DECLARE @VideoFlag bit  
            DECLARE @AudioFlag bit  
            DECLARE @FileFlag bit  
            DECLARE @Coupon bit  
            DECLARE @Loyalty bit  
            DECLARE @Rebate bit  
            SELECT @VideoFlag = SUM(CASE WHEN PT.ProductMediaType = 'Video Files' THEN 1 ELSE 0 END)  
                  , @AudioFlag = SUM(CASE WHEN PT.ProductMediaType = 'Audio Files' THEN 1 ELSE 0 END)  
                  , @FileFlag = SUM(CASE WHEN PT.ProductMediaType = 'Other Files' THEN 1 ELSE 0 END)  
            FROM ProductMedia PM  
            INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
            WHERE PM.ProductID = @ProductID   
              
         
              
            --To fetch product info.  
              
            SELECT  P.ProductID   
                    , ProductName   
                    , M.ManufName manufacturersName     
                    , ModelNumber modelNumber  
                    , ProductLongDescription   
                    , ProductExpirationDate productExpDate  
                    , imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath 																						
																						ELSE ThumbNailProductImagePath 
																					  END   
                          ELSE ThumbNailProductImagePath END 
                                                       
                    , SuggestedRetailPrice suggestedRetailPrice  
                    , Weight productWeight  
                    , WeightUnits   
                    , ScanTypeID   
                    , VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
                    , AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
                    , FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END                       
                    , UR.Rating  
                    , Coupon.Coupon   
     , Loyalty.Loyalty  
     , Rebate.Rebate   
   INTO #Product   
            FROM Product P  
                  LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID  
                  LEFT JOIN UserRating UR ON UR.ProductID = P.ProductID AND UR.UserID = @UserID
                  LEFT JOIN RetailLocationDeal D ON D.ProductID = P.ProductID   
                  OUTER APPLY fn_CouponDetails(@ProductID, @RetailID) Coupon  
      OUTER APPLY fn_LoyaltyDetails(@ProductID, @RetailID) Loyalty  
      OUTER APPLY fn_RebateDetails(@ProductID, @RetailID) Rebate
       
            WHERE P.ProductID = @ProductID   
                  AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
                    
                    
   SELECT   p.ProductID   
       , ProductName   
       , manufacturersName     
       , modelNumber  
       , ProductLongDescription   
       , productExpDate  
       , imagePath  
       --, ProductMediaPath
       , suggestedRetailPrice  
       , productWeight  
       , WeightUnits   
       , ScanTypeID   
       , VideoFlag   
       , AudioFlag   
       , FileFlag   
       , Rating  
       , CLRFlag = CASE WHEN (COUNT(p.Coupon)+COUNT(p.Loyalty)+COUNT(p.Rebate)) > 0 THEN 1 ELSE 0 END  
       , coupon_Status = CASE WHEN COUNT(p.Coupon) = 0 THEN 'Grey'   
           ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
             , loyalty_Status = CASE WHEN COUNT(p.Loyalty) = 0 THEN 'Grey'  
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
          END  
             , rebate_Status = CASE WHEN COUNT(p.Rebate) = 0 THEN 'Grey'  
             ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
        
   FROM #Product p  
        LEFT JOIN UserCouponGallery UC ON UC.UserID = @UserID AND UC.CouponID = p.Coupon  
        LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = @UserID AND UL.LoyaltyDealID = p.Loyalty  
        LEFT JOIN UserRebateGallery UR ON UR.UserID = @UserID AND UR.RebateID = p.Rebate   
      GROUP BY   p.ProductID   
       ,ProductName   
       , manufacturersName     
       , modelNumber  
       , ProductLongDescription   
       , productExpDate  
       , imagePath 
       --, ProductMediaPath 
       , suggestedRetailPrice  
       , productWeight  
       , WeightUnits   
       , ScanTypeID   
       , VideoFlag   
       , AudioFlag   
       , FileFlag   
       , Rating  
        
        
        
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure <>.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;


GO
