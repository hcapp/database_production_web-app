USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerBannerAdCreation
Purpose					: To create a Banner Ad.
Example					: usp_WebRetailerBannerAdCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdCreation]
(
	--Input Variables
	  @RetailID int
	, @RetailLocationIds varchar(max) --CSV
	, @BannerAdName varchar(100)
	, @BannerAdURL varchar(1000)
	, @BannerAdPageImagePath varchar(255)
	, @StartDate varchar(20)
	, @EndDate varchar(20)
	
	--Output Variable 
	, @InvalidRetailLocations varchar(5000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 --Initialise to empty string
			 SET @InvalidRetailLocations = NULL
			 
			 DECLARE @BannerAdID INT
			 
			 --Filter all the locations that already has a Banner Ad in the given date range.
			 IF @EndDate IS NOT NULL
			 BEGIN
			 
				 SELECT @InvalidRetailLocations = COALESCE(@InvalidRetailLocations + ',','') + RL.Param
				 FROM AdvertisementBanner AB
				 INNER JOIN RetailLocationBannerAd RLBA ON AB.AdvertisementBannerID = RLBA.AdvertisementBannerID
				 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLBA.RetailLocationID
				 WHERE (@StartDate BETWEEN AB.StartDate AND ISNULL(AB.EndDate, DATEADD(DD, 1, AB.StartDate))
			 			OR
			 			ISNULL(@EndDate, DATEADD(DD, 1, AB.StartDate)) BETWEEN AB.StartDate AND ISNULL(AB.EndDate, DATEADD(DD, 1, @EndDate)))
			 END
			 
			 IF @EndDate IS NULL
			 BEGIN
			     SELECT @InvalidRetailLocations = COALESCE(@InvalidRetailLocations + ',','') + RL.Param
				 FROM AdvertisementBanner AB
				 INNER JOIN RetailLocationBannerAd RLBA ON AB.AdvertisementBannerID = RLBA.AdvertisementBannerID
				 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLBA.RetailLocationID
			     WHERE (EndDate IS NULL AND ((@StartDate >= StartDate OR @StartDate < StartDate)OR (@StartDate = StartDate AND EndDate IS not Null)))
			 	 OR (@StartDate BETWEEN AB.StartDate AND AB.EndDate AND EndDate IS not Null)	
			 	 
			 END
			 	 
			 --Allow the user to create the Banner Ad only if no location has a Banner Ad in the given date range.
			 IF (@InvalidRetailLocations IS NULL OR @InvalidRetailLocations LIKE '')
			 BEGIN
				INSERT INTO AdvertisementBanner ( BannerAdName
												, BannerAdImagePath
												, BannerAdURL
												, StartDate
												, EndDate
												, WebsiteSourceFlag
												, DateCreated
												, RetailID )
												
										VALUES( @BannerAdName
										      , @BannerAdPageImagePath
										      , @BannerAdURL
										      , @StartDate
										      , @EndDate
										      , 1
										      , GETDATE()
										      ,@RetailID )
										      
						--Capture the inserted identity value
						SET @BannerAdID = SCOPE_IDENTITY()
				
				--Associate the Retail Locations to the Banner Ad.		
				INSERT INTO RetailLocationBannerAd(RetailLocationID
												 , AdvertisementBannerID
												 , DateCreated)	
										 SELECT [Param]
										      , @BannerAdID
										      , GETDATE()											   
										 FROM dbo.fn_SplitParam(@RetailLocationIds, ',')	      
							
				END		
										
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
