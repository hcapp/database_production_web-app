USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierRebateProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierRebateProductSearch
Purpose					: To Search for the rebate based on the rebate name or based on the product name it has been associated to.
Example					: usp_WebSupplierRebateProductSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th DEC 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierRebateProductSearch]
(
	
	@ManufacturerID int
	,@Search Varchar(1000)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			 DECLARE @Config varchar(50)
			 SELECT @Config=ScreenContent
			 FROM AppConfiguration 
			 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
				
			 DECLARE @AppConfig varchar(50)
			 SELECT @AppConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='App Media Server Configuration' 
			 
				SELECT RP.ProductID
					   ,P.ProductName
					   ,R.RebateName
					   ,R.RebateAmount
					   ,RebateLongDescription
					   ,RebateShortDescription
					   ,productImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+  ProductImagePath
										ELSE ProductImagePath END
					   ,RebateTermsConditions
					   ,RebateStartDate
					   ,RebateEndDate
					   , P.ProductShortDescription
					   , P.ScanCode
					   , P.ProductLongDescription
				FROM Rebate R
				INNER JOIN RebateProduct RP ON R.RebateID=RP.RebateID
				INNER JOIN Product P ON P.ProductID=RP.ProductID
				WHERE P.ProductID <> 0 
				AND R.ManufacturerID=@ManufacturerID
					AND (RebateName LIKE '%'+@Search+'%'
					OR ProductName LIKE '%'+@Search+'%'
					OR ScanCode LIKE '%'+@Search+'%')
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierRebateProductSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;


GO
