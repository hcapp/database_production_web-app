USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierReRunHotDealDisplayProductInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebSupplierReRunHotDealDisplayProductInfo]
Purpose					: To display the given product details.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			11th December 2012			Pavan Sharma K	    Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierReRunHotDealDisplayProductInfo]  
(  
  
 --Input Input Parameter(s)--  
      
     
    @ProductID VARCHAR(MAX)     
   
 --Output Variable--     
  
  , @ErrorNumber int output  
  , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY    
  DECLARE @Config varchar(50)  
  SELECT @Config=ScreenContent  
  FROM AppConfiguration   
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
  
  SELECT DISTINCT P.ProductName as productName  
       , P.ProductID    
       , P.ScanCode 
       , imagePath = CASE WHEN WebsiteSourceFlag = 1 AND ProductImagePath IS NOT NULL THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID) +'/'+  ProductImagePath 
									ELSE ProductImagePath END
       --, ProductMediaPath = CASE WHEN WebsiteSourceFlag = 1 AND ProductMediaPath IS NOT NULL THEN @Config +  CONVERT(VARCHAR(30),P.ManufacturerID) +'/'+ ProductMediaPath
							--		ELSE ProductMediaPath END
       , P.ProductShortDescription as shortDescription  
       , P.ProductLongDescription as longDescription  
       --, PA.AttributeName  
       --, PA.DisplayValue  
       , P.WarrantyServiceInformation 
       , Category = CASE WHEN PC.CategoryID IS NOT NULL THEN C.ParentCategoryName + ' - ' + C.SubCategoryName ELSE NULL END
  FROM Product P  
  LEFT JOIN ProductCategory PC ON P.ProductID = PC.ProductID
  LEFT JOIN Category C ON C.CategoryID = PC.CategoryID    
  INNER JOIN dbo.fn_SplitParam(@ProductID, ',') Prod ON P.ProductID = Prod.Param  
  --LEFT JOIN ProductAttributes PA ON P.ProductID = PA.ProductID  
  --LEFT JOIN ProductMedia PM ON P.ProductID = PM.ProductID  
      
    
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebSupplierReRunHotDealDisplayProductInfo.'    
  -- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output      
  END;  
     
 END CATCH;  
END;


GO
