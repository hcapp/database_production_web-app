USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerWishListProductHotDealInformation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerWishListProductHotDealInformation
Purpose					: to get the hot deal product details.
Example					: usp_WebConsumerWishListProductHotDealInformation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16thAug2013 	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerWishListProductHotDealInformation]
(
	  @UserId int
	, @ProductId int
	--, @Latitude decimal(18,6)    
	--, @Longitude decimal(18,6)    
	--, @ZipCode varchar(10)
	--, @Radius int    
	
	--User Tracking Inputs
    , @MainMenuID int
    , @AlertedProductID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
			--To get Server Configuration
			 DECLARE @Config varchar(50)  
			 SELECT @Config=ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='App Media Server Configuration' 
			 
			 DECLARE @ManufacturerConfig varchar(50)  
			 SELECT @ManufacturerConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
			 
			 
			 DECLARE @RetailerConfig varchar(50)  
			 SELECT @RetailerConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'
			 
			 DECLARE @Latitude decimal(18,6)    
			 DECLARE @Longitude decimal(18,6)  
			 DECLARE @ZipCode VARCHAR(10)
			 DECLARE @Radius int
			 
			 --Get the user configured postal code.
			 SELECT @ZipCode = PostalCode
			 FROM Users 
			 WHERE UserID = @UserId 
						    
			 SELECT @Latitude = Latitude     
			      , @Longitude = Longitude     
			 FROM GeoPosition     
			 WHERE PostalCode = @ZipCode  	 
		  
			 --If radius is not passed, getting user preferred radius from UserPreference table.    
			 IF @Radius IS NULL     
			 BEGIN     
				SELECT @Radius = LocaleRadius  
				FROM dbo.UserPreference WHERE UserID = @UserID    
			 END     
						
					--To get hot deal info for the wish list product.
					IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL) 					
						BEGIN	
						print 'a'
					
								SELECT ProductHotDealID
									   , HotDealName
									   , ProductName
									   , HotDealDescription 
									   , HotDealImagePath
									   , Price 
									   , SalePrice 
									   , Distance
									   , HotDealTermsConditions 
									   , HotDealStartDate 
									   , HotDealEndDate 
									   , HotDealURL 
									   , APIPartnerName 
									   , retLocIDs
									   , retLocs  
									   , retName
									   , extFlag 
									   , apiPartnerImagePath 
									   , ClaimFlag  
									   , RedeemFlag  
									   , UserName  
							    INTO #WishListProductHotDeal		   
								FROM ( SELECT DISTINCT  P.ProductHotDealID  
											  , p.HotDealName
											  , PR.ProductName 
											  , HotDealDescription=ISNULL(HotDealShortDescription,HotDeaLonglDescription)
                                              , HotDealImagePath = CASE WHEN P.WebsiteSourceFlag = 1 THEN
															CASE WHEN P.ManufacturerID IS NOT NULL 
															THEN @ManufacturerConfig  +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +HotDealImagePath
															ELSE 
															   CASE WHEN RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
															   THEN [dbo].fn_HotDealImagePath(p.ProductHotDealID)
															   ELSE @RetailerConfig + CONVERT(VARCHAR(30),P.RetailID)+'/' +HotDealImagePath
															   END	 
															END
														ELSE HotDealImagePath
												   END
											  , Price
											  , SalePrice
											--  ,Distance =1
											  , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
											  , p.HotDealTermsConditions
											  , p.HotDealStartDate
											  , p.HotDealEndDate
											  , p.HotDealURL
											  , A.APIPartnerName
											  , retLocIDs = [dbo].[fn_CreateCommaSeparatedRetailLocation](P.ProductHotDealID)
											  , retLocs = [dbo].[fn_CreateCommaSeparatedRetailLocationAddress](P.ProductHotDealID)
											  , retName = [dbo].[fn_CreateCommaSeparatedRetailerName](P.ProductHotDealID)
											  , apiPartnerImagePath = @Config + A.APIPartnerImagePath
											  , extFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
											  , ClaimFlag = CASE WHEN UH.UsedFlag IS NOT NULL THEN 1 ELSE 0 END
											  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
											  , UserName = CASE WHEN UH.UsedFlag IS NOT NULL THEN (SELECT UserName FROM Users WHERE UserID = @UserID) ELSE NULL END
									          , UH.UserHotDealGalleryID 
									          , P.NoOfHotDealsToIssue 
									  FROM UserProduct up
										INNER JOIN HotDealProduct HP ON up.ProductID=HP.ProductID
										INNER JOIN Product PR ON PR.ProductID =HP.ProductID 
										INNER JOIN ProductHotDeal P ON p.ProductHotDealID=hp.ProductHotDealID
										INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=P.ProductHotDealID
										--LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State AND (G.PostalCode =@ZipCode OR (G.Latitude=@Latitude AND G.Longitude=@Longitude)) 
										LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID
										LEFT JOIN UserHotDealGallery UH ON UH.HotDealID = P.ProductHotDealID
									  WHERE HP.productid=@productid 
									   AND UP.UserID =  @userid
									   AND UP.WishListItem = 1
									   AND (GETDATE() >= ISNULL(HotDealStartDate, GETDATE() - 1) AND GETDATE()<= ISNULL(HotDealEndDate, GETDATE()+1))
									  
									   ) HotDeal
									WHERE Distance <= ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenContent = 'WebConsumerDefaultRadius')) 
                                     GROUP BY ProductHotDealID
								     , HotDealName
								   , ProductName
								   , HotDealDescription 
								   , HotDealImagePath
								   , Price 
								   , SalePrice 
								   , Distance
								   , HotDealTermsConditions 
								   , HotDealStartDate 
								   , HotDealEndDate 
								   , HotDealURL 
								   , APIPartnerName
								   , retLocIDs
								   , retLocs 
								   , retName
								   , apiPartnerImagePath 
								   , extFlag  
								   , ClaimFlag  
								   , RedeemFlag 
								   , UserName  
								   , UserHotDealGalleryID 
								   , NoOfHotDealsToIssue 
									   HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				                       ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)

						  END							  
						  
						  
						  IF (@Latitude IS NULL AND @Longitude IS NULL ) 
						  BEGIN
							  --print 'b'
							  SELECT ProductHotDealID
								   , HotDealName
								   , ProductName
								   , HotDealDescription 
								   , HotDealImagePath
								   , Price 
								   , SalePrice 
								   , Distance
								   , HotDealTermsConditions 
								   , HotDealStartDate 
								   , HotDealEndDate 
								   , HotDealURL 
								   , APIPartnerName
								   , retLocIDs
								   , retLocs 
								   , retName
								   , apiPartnerImagePath 
								   , extFlag  
								   , ClaimFlag  
								   , RedeemFlag 
								   , UserName  
							INTO #WishListProductHotDeal1	   
							FROM (SELECT DISTINCT p.ProductHotDealID 
									, p.HotDealName
									, PR.ProductName 									
									, HotDealDescription=ISNULL(HotDealShortDescription,HotDeaLonglDescription)
									, HotDealImagePath = CASE WHEN p.WebsiteSourceFlag = 1 THEN
																		CASE WHEN p.ManufacturerID IS NOT NULL 
																		THEN @ManufacturerConfig+CONVERT(VARCHAR(30),P.ManufacturerID)+'/' +HotDealImagePath																		 
																		 ELSE CASE WHEN RetailID IS NOT NULL AND HotDealImagePath IS NULL                      
																		   THEN [dbo].fn_HotDealImagePath(p.ProductHotDealID)
																		   ELSE @RetailerConfig + CONVERT(VARCHAR(30),P.RetailID)+'/' +HotDealImagePath
																		   END	 
																		END
																	ELSE HotDealImagePath
															   END    
									, Price
									, SalePrice
									, 0 Distance
									, p.HotDealTermsConditions
									, p.HotDealStartDate
									, p.HotDealEndDate
									, p.HotDealURL
									, A.APIPartnerName 
									, retLocIDs = [dbo].[fn_CreateCommaSeparatedRetailLocation](P.ProductHotDealID)
									, retLocs = [dbo].[fn_CreateCommaSeparatedRetailLocationAddress](P.ProductHotDealID)
									, retName = [dbo].[fn_CreateCommaSeparatedRetailerName](P.ProductHotDealID)
									, apiPartnerImagePath = @Config + A.APIPartnerImagePath
									, extFlag = CASE WHEN APIPartnerName= 'ScanSee' THEN 0 ELSE 1 END
											  , ClaimFlag = CASE WHEN UH.UsedFlag IS NOT NULL THEN 1 ELSE 0 END
											  , RedeemFlag = CASE WHEN UsedFlag = 1 THEN 1 ELSE 0 END
											  , UserName = CASE WHEN UH.UsedFlag IS NOT NULL THEN (SELECT UserName FROM Users WHERE UserID = @UserID) ELSE NULL END
									
									,P.NoOfHotDealsToIssue 
									,UH.UserHotDealGalleryID 									
									FROM UserProduct up
									INNER JOIN HotDealProduct HP ON up.ProductID=HP.ProductID
									INNER JOIN Product PR ON PR.ProductID =HP.ProductID 
									INNER JOIN ProductHotDeal P ON p.ProductHotDealID=hp.ProductHotDealID
									--INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HP.ProductHotDealID
									LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID
									LEFT JOIN UserHotDealGallery UH ON UH.HotDealID = P.ProductHotDealID
									WHERE HP.productid=@productid 
									AND  UP.UserID =  @userid
									AND UP.WishListItem = 1
									AND (GETDATE() >= ISNULL(HotDealStartDate, GETDATE() - 1) AND GETDATE()<= ISNULL(HotDealEndDate, GETDATE()+1))
									--HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				     --               ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)
									) HotDeal
									GROUP BY ProductHotDealID
								     , HotDealName
								   , ProductName
								   , HotDealDescription 
								   , HotDealImagePath
								   , Price 
								   , SalePrice 
								   , Distance
								   , HotDealTermsConditions 
								   , HotDealStartDate 
								   , HotDealEndDate 
								   , HotDealURL 
								   , APIPartnerName
								   , retLocIDs
								   , retLocs 
								   , retName
								   , apiPartnerImagePath 
								   , extFlag  
								   , ClaimFlag  
								   , RedeemFlag 
								   , UserName  
								   , UserHotDealGalleryID 
								   , NoOfHotDealsToIssue 
									   HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				                       ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)
									
							END
							
							
							--User Tracking
      
							  UPDATE ScanSeeReportingDatabase..AlertedProducts SET AlertedProductClick = 1
							  WHERE AlertedProductID = @AlertedProductID
							  
							  CREATE TABLE #Temp(HotDealListID int,ProductHotDealID int)
				     
				              --Capture the alerted Hotdeals items when the user has configured Postal code or GPS is on.
									  
									  
									  
									   	
									IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)
									BEGIN
									   Insert into ScanSeeReportingDatabase..HotDealList(MainMenuID
																						  ,ProductHotDealID
																						  ,CreatedDate)															   
									   OUTPUT inserted.HotDealListID ,inserted.ProductHotDealID INTO #Temp(HotDealListID,ProductHotDealID)													  
									   SELECT @MainMenuID  
											 ,ProductHotDealID  
											 ,GETDATE()
									   FROM #WishListProductHotDeal	
									   
										   SELECT DISTINCT T.HotDealListID hotdealLstId
											   , W.ProductHotDealID hotDealId
											   , HotDealName
											   , ProductName
											   , HotDealDescription hDLognDescription
											   , HotDealImagePath
											   , Price hDPrice
											   , SalePrice hDSalePrice
											   , Distance
											   , HotDealTermsConditions hDTermsConditions
											   , HotDealStartDate hDStartDate
											   , HotDealEndDate hDEndDate
											   , HotDealURL hdURL
											   , APIPartnerName
											   , retLocIDs
											   , retLocs 
											   , retName 
											   , extFlag  
											   , apiPartnerImagePath
											   , ClaimFlag  
											   , RedeemFlag 
											   , UserName
										  FROM #WishListProductHotDeal W
										  INNER JOIN #Temp T ON T.ProductHotDealID = W.ProductHotDealID		
									END
									
									IF (@Latitude IS NULL AND @Longitude IS NULL)
									BEGIN
									  INSERT INTO ScanSeeReportingDatabase..HotDealList(MainMenuID
																						  ,ProductHotDealID
																						  ,CreatedDate)															   
									   OUTPUT inserted.HotDealListID ,inserted.ProductHotDealID INTO #Temp(HotDealListID,ProductHotDealID)													  
									   SELECT @MainMenuID  
											 ,ProductHotDealID  
											 ,GETDATE()
									   FROM #WishListProductHotDeal1								 
									 
									   SELECT T.HotDealListID hotdealLstId
										     , W.ProductHotDealID hotDealId
										     , HotDealName
										     , ProductName
										     , HotDealDescription hDLognDescription
										     , HotDealImagePath
										     , Price hDPrice
										     , SalePrice hDSalePrice
										     , Distance
										     , HotDealTermsConditions hDTermsConditions
										     , HotDealStartDate hDStartDate
										     , HotDealEndDate hDEndDate
										     , HotDealURL hdURL
										     , APIPartnerName
										     , retLocIDs
											 , retLocs  
											 , retName
											 , extFlag
										     , apiPartnerImagePath
										     , extFlag  
										     , ClaimFlag  
										     , RedeemFlag 
										     , UserName
									    FROM #WishListProductHotDeal1 W
									    INNER JOIN #Temp T ON T.ProductHotDealID = W.ProductHotDealID	
									END	
									
				--Confirmation of Success.
				SELECT @Status = 0
		COMMIT TRANSACTION										
												
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerWishListProductHotDealInformation'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1					
		END;
		 
	END CATCH;
END;


GO
