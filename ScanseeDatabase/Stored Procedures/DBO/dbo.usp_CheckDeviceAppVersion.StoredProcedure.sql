USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_CheckDeviceAppVersion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CheckDeviceAppVersion
Purpose					: To check the app version installed on the device.
Example					: usp_CheckDeviceAppVersion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			2nd Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_CheckDeviceAppVersion]
(
	  @DeviceID VARCHAR(60)
	, @AppVersion VARCHAR(10)
	
	--Output Variable 
	, @OptionalUpdateVersionPrompt bit output
	, @ForcefulUpdateVersionPrompt bit output
	, @LatestAppVersionURL varchar(1000) output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY				
		    
		    DECLARE @UserDeviceAppVersion VARCHAR(20)
		    DECLARE @Status int 
		    DECLARE @YetToReleaseVersion VARCHAR(10) 
			--Initialisation
			SET @OptionalUpdateVersionPrompt = 0
			SET @ForcefulUpdateVersionPrompt = 0
			
			--Find the version number that is still not approved by Apple.
			SELECT @YetToReleaseVersion = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'Yet to Release Version'
			AND Active = 1
			
			
			--Execute the App version Updation procedure.
			EXEC [dbo].usp_UpdateUserDeviceAppVersion @DeviceID, @AppVersion, @Status = @Status OUTPUT, @ErrorNumber = @ErrorNumber OUTPUT, @ErrorMessage = @ErrorMessage OUTPUT
			
			--Capture the Latest version URL and provide for the user to download
			SELECT @LatestAppVersionURL = ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'LatestAppVersionURL' AND ScreenName = 'LatestAppVersionURL' AND Active = 1

			--Check if the version running on the device is current one & prompt the user accordingly.
			IF (@AppVersion = (SELECT ScreenContent
										  FROM AppConfiguration
										  WHERE ConfigurationType = 'CurrentAppVersion' AND ScreenName = 'CurrentAppVersion' AND Active = 1)
				AND
				(@AppVersion <> (SELECT ScreenContent
										  FROM AppConfiguration
										  WHERE ConfigurationType = 'LatestAppVersion' AND ScreenName = 'LatestAppVersion' AND Active = 1 )))
				
		    BEGIN
				SET @OptionalUpdateVersionPrompt = 1
		    END
			
			--Check if the version running on the device is outdated/expired & prompt the user accordingly.
			IF (@AppVersion <> (SELECT ScreenContent
										  FROM AppConfiguration
										  WHERE ConfigurationType = 'LatestAppVersion' AND ScreenName = 'LatestAppVersion' AND Active = 1))
				AND 
				(@AppVersion <> (SELECT ScreenContent
										  FROM AppConfiguration
										  WHERE ConfigurationType = 'CurrentAppVersion' AND ScreenName = 'CurrentAppVersion' AND Active = 1))
				AND (@AppVersion <> ISNULL(@YetToReleaseVersion, 0))
			BEGIN
				SET @ForcefulUpdateVersionPrompt = 1
			END
			
			----Temporary Fix
			--if @AppVersion = '1.3.0'
			--begin
			--	set @OptionalUpdateVersionPrompt = 0
			--	set @ForcefulUpdateVersionPrompt = 0
			--end			
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CheckDeviceAppVersion.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
