USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_CouponShare]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CouponShare
Purpose					: 
Example					: usp_CouponShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th DEC 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_CouponShare]
(
	@CouponID int
	
	--Output Variable 
	, @CouponExpired bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 --To get Server Configuration 
			 DECLARE @RetailConfig varchar(50)
		  
			 SELECT @RetailConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'
			 
			SELECT  CouponName
				   ,CouponURL
				   ,CouponStartDate
				   ,CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN DBO.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																										CASE WHEN WebsiteSourceFlag = 1 
																											THEN @RetailConfig
																											+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																											+CouponImagePath 
																									    ELSE CouponImagePath 
																									    END
																								 END 
					 END  
				   ,CouponExpireDate
			FROM Coupon c
			LEFT JOIN CouponRetailer cr on c.CouponID = cr.CouponID
			WHERE GETDATE() Between CouponStartDate AND CouponExpireDate AND c.CouponID=@CouponID
			
			SELECT @CouponExpired=CASE WHEN GETDATE()>CouponExpireDate Then 0 else 1 END 
			FROM Coupon
			WHERE CouponID=@CouponID		
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CouponShare.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
