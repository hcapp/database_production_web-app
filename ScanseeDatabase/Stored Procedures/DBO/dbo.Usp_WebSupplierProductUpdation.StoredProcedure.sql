USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[Usp_WebSupplierProductUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: Usp_WebProductUpdation
Purpose					: To update the product related information
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[Usp_WebSupplierProductUpdation]
(
	--Input variables for the product table
	
	 @ProductID int
	,@ProductUPC varchar(20)
	,@ProductName Varchar(500)	
	,@ModelNumber varchar(50)
	,@SuggestedRetailPrice Money
	,@ProductimagePath varchar(100)
	,@LongDescription Varchar(max)
	,@ShortDescription Varchar(255)
	,@Warranty varchar(255)
	
	--input variables for productmedia table 
	
	,@AudioMediapath varchar(255)
	,@VideoMediaPath varchar(255)
	
	--input variables for productCategory table 
	
	,@CategoryName varchar(100)
	
	--input variables for productattributes table
	,@AttributeName Varchar(110)
	,@DisplayValue Varchar(max)
	
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			--TO Update the product table
			UPDATE Product
			SET [ScanCode]=@ProductUPC
			   ,[Service]=0
			   ,[ProductName]=@ProductName
			   ,[ProductShortDescription]=@ShortDescription
			   ,[ProductLongDescription]=@LongDescription
			   ,[ModelNumber]=@ModelNumber
			   ,[ProductImagePath]=@ProductimagePath
			   ,[SuggestedRetailPrice]=@SuggestedRetailPrice
			   ,[ProductModifyDate]=GETDATE()
			   ,[WarrantyServiceInformation]=@Warranty
			 WHERE ProductID=@ProductID
			 
			 --To update the ProductMedia Table
			 UPDATE ProductMedia
			 SET  [ProductMediaPath]=@AudioMediapath
				  ,[ProductMediaName]='Audio'
			 WHERE ProductID=@ProductID AND ProductMediaTypeID=2
				  
			 UPDATE ProductMedia
			 SET [ProductMediaPath]=@AudioMediapath
				  ,[ProductMediaName]='Audio'
			WHERE ProductID=@ProductID AND ProductMediaTypeID=1
			
			--To Update the ProductAttribute table
			
			SELECT Row_num=IDENTITY(int,1,1)
					  ,Param AttributeName
				INTO #AttributeName
				FROM fn_SplitParam(@AttributeName,',')
				
			SELECT Row_num=IDENTITY(int,1,1)
				  ,Param DisplayValue
			INTO #DisplayValue
			FROM fn_SplitParam(@DisplayValue,',')
			
			UPDATE ProductAttributes
			SET AttributeName=A.AttributeName,DisplayValue=D.DisplayValue
			FROM #AttributeName A
			Inner JOIN #DisplayValue D ON A.Row_num=D.Row_num
			WHERE ProductID=@ProductID

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
