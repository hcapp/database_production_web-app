USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponDetails
Purpose					: To Display coupon details.
Example					: usp_WebRetailerCouponDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCouponDetails]
(
	@CouponID int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--To get Media Server Configuration.
		 DECLARE @Config varchar(50)  
		 SELECT @Config=ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration' 

	
		 
		 DECLARE @ProductIDs VARCHAR(100)
		 DECLARE @ProductNames VARCHAR(1000)
		 DECLARE @ScanCodes VARCHAR(1000)
		 DECLARE @RetailerLocations VARCHAR(1000)
		 
		 SELECT CouponID
			  , ProductID
			  , ProductName
			  , ScanCode
		 INTO #Temp
		 FROM (SELECT C.CouponID
					, P.ProductID
					, P.ProductName
					, P.ScanCode					
			   FROM Coupon C
			   INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			   INNER JOIN Product P ON P.ProductID = CP.ProductID
			   WHERE C.CouponID = @CouponID) Prod
			   
		 SELECT @ProductIDs = COALESCE(@ProductIDs + ',','') + CAST(ProductID AS VARCHAR(100)) FROM #Temp
		 SELECT @ProductNames = COALESCE(@ProductNames + ',','') + CAST(ProductName AS VARCHAR(1000)) FROM #Temp
		 SELECT @ScanCodes = COALESCE(@ScanCodes + ',','') + CAST(ScanCode AS VARCHAR(100)) FROM #Temp
		 SELECT @RetailerLocations = COALESCE(@RetailerLocations + ',','') + CAST(RetailLocationID AS VARCHAR(100)) FROM CouponRetailer WHERE CouponID = @CouponID
		 
		 DECLARE @RetailerID INT 
		 SELECT @RetailerID = RetailID
		 FROM CouponRetailer 
		 WHERE CouponID = @CouponID
 

		 
		SELECT DISTINCT C.CouponID
			   ,CouponName couponName
			   --,CR.RetailID			   
			   --,C.APIPartnerID 			   
			   ,BannerTitle as bannerTitle
			   ,CouponDiscountPct as couponDiscountPercentage
			   --,CouponDiscountType
			   ,CouponImagePath = CASE WHEN CouponImagePath IS NOT NULL THEN CASE WHEN C.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),CR.RetailID) +'/'+ CouponImagePath ELSE CouponImagePath END 
			                    ELSE CouponImagePath END
			   ,CouponImagePath as couponImageName
			   ,CouponLongDescription as couponLongDesc
			   ,CouponShortDescription as couponShortDesc
			   ,CouponTermsAndConditions as couponTermsCondt
			   ,NoOfCouponsToIssue as numOfCouponIssue
			   ,CAST(CouponStartDate AS DATE) CouponStartDate
			   ,CAST(CouponExpireDate AS DATE) CouponEndDate
			   ,CAST(ActualCouponExpirationDate AS DATE) couponExpireDate
			   ,LEFT(CAST(CouponStartDate AS TIME),5) CouponStartTime
			   ,LEFT(CAST(CouponExpireDate AS TIME),5) CouponEndTime
			    ,LEFT(CAST(ActualCouponExpirationDate AS TIME),5) couponExpTime
			   --,CouponDisplayExpirationDate as couponDisplayExpirationDate
			   --,ExternalCoupon   
			   ,CouponTimeZoneID timeZoneId
			   --,POSIntegrate as strPos
			   ,@ProductIDs as productID
			   ,@ProductNames as productName
			   ,@ScanCodes as scanCode
			   ,@RetailerLocations as LocationID
			   ,KeyWords keyword
			   ,CouponDetailImage detailImgPath
			   ,coupDetImgPath = CASE WHEN CouponDetailImage  = 'uploadIconSqr.png' THEN @Config  + C.CouponDetailImage
									  ELSE @Config + CONVERT(VARCHAR(30),CR.RetailID) +'/'+ CouponDetailImage END

			                    
		FROM Coupon C
		LEFT OUTER JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
		LEFT JOIN CouponProduct CP ON CP.CouponID=C.CouponID
		LEFT JOIN Product P ON CP.ProductID=P.ProductID 
		WHERE C.CouponID=@CouponID
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;







GO
