USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerWelcomePageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebRetailerWelcomePageDisplay]
Purpose                  : To display the Welcome pages created by the given Retailer.
Example                  : [usp_WebRetailerWelcomePageDisplay]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               31st July 2012      Dhananjaya TR   Initial Version                                 
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerWelcomePageDisplay]
(

      --Input Input Parameter(s)--  
      
        @RetailID INT
      , @SearchParameter VARCHAR(1000)
      , @LowerLimit int
      , @ShowAll BIT --To include expired Ads
      
      --Output Variable--
	  , @RowCount INT OUTPUT
	  , @NextPageFlag BIT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
		     DECLARE @UpperLimit INT    
			 DECLARE @MaxCnt INT        
      
			  --To get the row count for pagination.      
			 DECLARE @ScreenContent Varchar(100)    
			 SELECT @ScreenContent = ScreenContent       
			 FROM AppConfiguration       
			 WHERE ScreenName = 'All'     
			 AND ConfigurationType = 'Website Pagination'    
			 AND Active = 1   
		 
		    SET @UpperLimit = @LowerLimit + @ScreenContent   
		    --To Fetch the Retailer Media server Configuaration      
			DECLARE @Config varchar(255)    
			SELECT @Config=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration' AND Active =1  
		
		
	        DECLARE @Temp TABLE(RowNum INT IDENTITY(1, 1)
								   , retailLocationAdvertisementID INT
								   , advertisementName VARCHAR(255)
								   , strSplashAdImagePath VARCHAR(1000) 
								   , advertisementDate DATE
								   , advertisementEndDate DATE
								   , DeleteFlag BIT
								   , ExpireFlag BIT)			
			
			  --Select the ads that are currently running & those that start in future. This will be common in both the events where @ShowAll is 0 or 1.
			   INSERT INTO @Temp(retailLocationAdvertisementID  
							   , advertisementName  
							   , strSplashAdImagePath 
							   , advertisementDate  
							   , advertisementEndDate  
							   , DeleteFlag  
							   , ExpireFlag)
	       
				SELECT DISTINCT ASP.AdvertisementSplashID as retailLocationAdvertisementID
					  ,ASP.SplashAdName as advertisementName
					  ,@Config + CONVERT(VARCHAR(30),RL.RetailID) +'/' + SplashAdImagePath as strBannerAdImagePath
				      ,advertisementDate = ASP.StartDate   
					  ,advertisementEndDate = ASP.EndDate 
					  ,DeleteFlag = CASE WHEN ASP.StartDate > GETDATE() THEN 1 ELSE 0 END
				      ,ExpireFlag = CASE WHEN (CONVERT(DATE, GETDATE())>=CONVERT(DATE, ASP.StartDate) AND CONVERT(DATE, GETDATE())<=CONVERT(DATE, ISNULL(ASP.EndDate, GETDATE() + 1))) THEN 1 ELSE 0 END																									      													
				FROM AdvertisementSplash ASP 
				INNER JOIN RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID=RLS.AdvertisementSplashID 
				INNER JOIN RetailLocation RL ON RLS.RetailLocationID =RL.RetailLocationID 
				WHERE RL.RetailID = @RetailID AND RL.Active = 1
				AND GETDATE() <= ISNULL(ASP.EndDate, GETDATE() + 1)
				AND SplashAdName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END     
				
				
				--Select the expired Ads when @ShowAll is 1
				IF (@ShowAll = 1)
				BEGIN
					
					 INSERT INTO @Temp(retailLocationAdvertisementID  
									   , advertisementName  
									   , strSplashAdImagePath  
									   , advertisementDate  
									   , advertisementEndDate  
									   , DeleteFlag  
								   , ExpireFlag)
				     SELECT DISTINCT ASP.AdvertisementSplashID as retailLocationAdvertisementID
				 	 ,ASP.SplashAdName as advertisementName
					 ,@Config + CONVERT(VARCHAR(30),RL.RetailID) +'/' + SplashAdImagePath as strBannerAdImagePath
					 ,advertisementDate = CASE WHEN ASP.StartDate = '01/01/1900' THEN (SELECT StartDate 
																						   FROM AdvertisementSplashHistory 
																						   WHERE AdvertisementSplashID = ASP.AdvertisementSplashID
																	                       AND DateCreated = (SELECT MAX(DateCreated)
																											  FROM AdvertisementSplashHistory
																										      WHERE AdvertisementSplashID = ASP.AdvertisementSplashID)) ELSE ASP.StartDate END  
				     ,advertisementEndDate = CASE WHEN ASP.EndDate = '01/01/1900' THEN (SELECT EndDate 
																						   FROM AdvertisementSplashHistory 
																						   WHERE AdvertisementSplashID = ASP.AdvertisementSplashID
																	                       AND DateCreated = (SELECT MAX(DateCreated) 
																											  FROM AdvertisementSplashHistory
																										      WHERE AdvertisementSplashID = ASP.AdvertisementSplashID)) ELSE ASP.EndDate END  --Check if the campaign was stopped or ad is expired or ad is still not started(In all the cases the ad is treated as inactive)
					 , DeleteFlag = CASE WHEN ASP.StartDate > GETDATE() THEN 1 ELSE 0 END
				     , ExpireFlag = CASE WHEN (CONVERT(DATE, GETDATE())>=CONVERT(DATE, ASP.StartDate) AND CONVERT(DATE, GETDATE())<=CONVERT(DATE, ISNULL(ASP.EndDate, GETDATE() + 1))) THEN 1 ELSE 0 END																									      													
				   FROM AdvertisementSplash ASP 
				   INNER JOIN RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID=RLS.AdvertisementSplashID 
				   INNER JOIN RetailLocation RL ON RLS.RetailLocationID =RL.RetailLocationID 
				   WHERE RL.RetailID = @RetailID AND RL.Active = 1				
				   AND SplashAdName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END      
				   AND GETDATE() > ASP.EndDate
				
				END	
				
			SELECT @MaxCnt = COUNT(RowNum) FROM @Temp 
		    --Output Total no of Records
		    SET @RowCount = @MaxCnt 
		
		    --CHECK IF THERE ARE SOME MORE ROWS        
		    SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  
		
	    	--Display the records in the given limit
		    SELECT RowNum 
		         , retailLocationAdvertisementID
		         , advertisementName
				 , strSplashAdImagePath strBannerAdImagePath
				 , advertisementDate
				 , advertisementEndDate
				 , DeleteFlag 
				 , ExpireFlag
		    FROM @Temp	
		    WHERE RowNum BETWEEN (@LowerLimit + 1) AND (@UpperLimit)                  
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerWelcomePageDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;




GO
