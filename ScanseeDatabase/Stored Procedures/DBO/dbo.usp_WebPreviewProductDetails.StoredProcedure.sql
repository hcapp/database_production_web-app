USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebPreviewProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayProductDetails
Purpose					: To display the details of the selected product.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			15th March 2012				    Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebPreviewProductDetails]
(

	--Input Input Parameter(s)--	   
	  
	   @Scancodes varchar(2000)
	
	--Output Variable--
	  
	 , @STATUS INT OUTPUT
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
	
	  DECLARE @Config varchar(50)    
	  SELECT @Config=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	  
			DECLARE @ProductName VARCHAR(1000)
			DECLARE @ProductImagePath VARCHAR(MAX)
			DECLARE @ShortDescription VARCHAR(MAX) 
			
			SELECT @ProductName = COALESCE(@ProductName+',','')+ProductName FROM Product P
			INNER JOIN dbo.fn_SplitParam(@Scancodes, ',') F ON P.ScanCode = F.Param
			
			SELECT @ProductImagePath = COALESCE(@ProductImagePath+',','')+ CASE WHEN WebsiteSourceFlag = 1 THEN @Config+CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductImagePath ELSE ProductImagePath END  FROM Product P
			INNER JOIN dbo.fn_SplitParam(@Scancodes, ',') F ON P.ScanCode = F.Param
			
			SELECT @ShortDescription = COALESCE(@ShortDescription+',','')+ProductShortDescription FROM Product P
			INNER JOIN dbo.fn_SplitParam(@Scancodes, ',') F ON P.ScanCode = F.Param
			
				
			SELECT @Scancodes scanCode
				 , @ProductName	productName	
				 , @ProductImagePath productImagePath 	
				 , @ShortDescription productshortDescription
		 
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
