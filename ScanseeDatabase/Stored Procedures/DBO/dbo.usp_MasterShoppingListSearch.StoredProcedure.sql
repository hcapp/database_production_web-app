USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListSearch
Purpose					: To search for product
Example					: EXEC usp_MasterShoppingListSearch 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	Padampriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListSearch]
(
	 @UserID int
	,@ProdSearch varchar(max)
)
AS
BEGIN

	BEGIN TRY
		--To fetch product info 
		SELECT ProductID 
			, ProductName
		FROM Product
		WHERE (ProductLongDescription LIKE '%' + @ProdSearch + '%'
				OR
			   ScanCode =  @ProdSearch)
			AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE()) 
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			
		END;
		 
	END CATCH;
END;

GO
