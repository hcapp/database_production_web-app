USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserTokenInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserTokenInsertion
Purpose					: To insert User Token.
Example					:  
DECLARE	@return_value int
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th Jan 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserTokenInsertion]
(	 
	 @DeviceID varchar(60)
	 ,@UserToken varchar(100)
	 ,@FaceBookAuthenticatedUser bit 
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			--checking for the existance of the TokenId for the same device.
			IF EXISTS (SELECT 1
						   FROM dbo.UserToken
						   WHERE DeviceID = @DeviceID 
								 AND UserToken = @UserToken AND FaceBookAuthenticatedUser=@FaceBookAuthenticatedUser )
			BEGIN
				  SET @Result=-1
			END
			
			IF NOT EXISTS (SELECT 1
						   FROM dbo.UserToken
						   WHERE DeviceID = @DeviceID 
								 AND UserToken = @UserToken AND FaceBookAuthenticatedUser=@FaceBookAuthenticatedUser )
			BEGIN
			INSERT INTO [UserToken]
					   ([DeviceID]
					   ,[UserToken]
					   ,[FaceBookAuthenticatedUser])
			VALUES
				   (@DeviceID
				   ,@UserToken
				   ,@FaceBookAuthenticatedUser)
			END
			SET @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserTokenInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
