USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetrieveRetailerLocation
Purpose					: To retrieve list of Retailer locations for a Retailer from the system.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			23rd December 2011				SPAN		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveRetailerLocation]

--Input parameters
	@RetailID int

--Output variables
	,@RetailLocationID as varchar(500) output 
	,@Status int output

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
		
			Select RetailLocationID as retailerLocID
				 , StoreIdentification
				 , Address1			
			 from Retaillocation where RetailID = @RetailID AND Active = 1	
			 AND (CorporateAndStore = 1
			 OR Headquarters = 0)
			 order by retailerLocID asc
			 
			 
			 Select @RetailLocationID =  COALESCE(@RetailLocationID+',', '') + CAST(RetaillocationID AS VARCHAR(100)) 
			 from Retaillocation
			 where RetailID = @RetailID AND Active = 1
			 AND (CorporateAndStore = 1
			 OR Headquarters = 0)
			 
			SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveRetailerLocation.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
