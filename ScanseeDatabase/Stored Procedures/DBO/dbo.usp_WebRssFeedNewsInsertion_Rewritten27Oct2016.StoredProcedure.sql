USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsInsertion_Rewritten27Oct2016]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsInsertion]
Purpose					: To insert into RssFeedNews from Staging table.
Example					: [usp_WebRssFeedNewsInsertion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015		Mohith H R	        1.1
1.1         07 Dec 2015     Suganya C           Duplicate news removal
1.2							Sagar Byali			Video deletion changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedNewsInsertion_Rewritten27Oct2016]
(
	

	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @ExpiryPublishedDateRockwall datetime = GETDATE()-15
		-- ,@ExpiryPublishedDateMarbel datetime = GETDATE()-15
		 ,@ExpiryPublishedDateMarbelVideos datetime
		 ,@ExpiryPublishedDateMarbelAllCategoryNews datetime
		 ,@ExpiryPublishedDateKilleen datetime = GETDATE()-5


		 --Added on 26/10/2016
		CREATE TABLE #Temp (MaxRssFeedNewsID int
						    ,NewsType varchar(500)
							,HcHubCitiID int
							,classification varchar(200)
							,Title varchar(5000))

		--added on 26th Sept 2016 to retain 14 day news for 'All' category
		SELECT @ExpiryPublishedDateMarbelAllCategoryNews = DATEADD(d,-14,max(convert(date,PublishedDate)))
		FROM RssFeedNewsStagingTable 
		WHERE NewsType = 'all' AND HcHubCitiID = 10


		 --Added on 26/10/2016
		 INSERT INTO #Temp
		 SELECT DISTINCT MAX(RssFeedNewsID) as MaxRssFeedNewsID,NewsType,HcHubCitiID, classification, Title
         FROM RssFeedNewsStagingTable
		 WHERE newstype = 'classifields' and Classification is not null  
         GROUP BY NewsType,HcHubCitiID, classification, Title



		--added on 22nd Aug 2016 to retain one day videos 
		SELECT @ExpiryPublishedDateMarbelVideos = max(convert(date,DateCreated)) 
		FROM RssFeedNewsStagingTable 
		WHERE NewsType='videos' AND HcHubCitiID = 10

		 --Fetching unique, latest data from RssFeedNewsStagingTable to insert into RssFeedNews
		 --SELECT DISTINCT HcHubCitiID, NewsType, Title, classification, MAX(RssFeedNewsID) AS MaxRssFeedNewsID
   --      INTO #Temp
   --      FROM RssFeedNewsStagingTable
   --      GROUP BY HcHubCitiID,NewsType, Title, classification

   
		 --Added on 26/10/2016
		 INSERT INTO #Temp
		 SELECT DISTINCT MAX(RssFeedNewsID) as MaxRssFeedNewsID,NewsType,HcHubCitiID, classification, Title
         FROM RssFeedNewsStagingTable
		 WHERE Classification is null 
         GROUP BY NewsType,HcHubCitiID, classification, Title

		 SELECT  RS.Title
				,RS.ImagePath
				,RS.ShortDescription
				,RS.LongDescription
				,RS.Link
				,RS.PublishedDate
				,RS.NewsType
				,RS.Message
				,RS.HcHubCitiID	
				,RS.Classification
				,RS.Section
				,RS.AdCopy
				,RS.VideoLink
		INTO #InsertValues		
		FROM RssFeedNewsStagingTable RS 
		INNER JOIN #Temp T ON RS.HcHubCitiID = T.HcHubCitiID 
		AND RS.NewsType = T.NewsType 
		AND ((RS.NewsType <> 'Classifields' AND RS.Title = T.Title) OR (RS.NewsType = 'Classifields' AND 1=1))
		AND RS.RssFeedNewsID = T.MaxRssFeedNewsID 
		GROUP BY RS.Title
				,RS.ImagePath
				,RS.ShortDescription
				,RS.LongDescription
				,RS.Link
				,RS.PublishedDate
				,RS.NewsType
				,RS.Message
				,RS.HcHubCitiID	
				,RS.Classification
				,RS.Section
				,RS.AdCopy
				,RS.VideoLink	
		ORDER BY MIN(RS.RssFeedNewsID) ASC

		--DELETE FROM RssFeedNewsStagingTable
		--WHERE (convert(date,PublishedDate) < @ExpiryDate AND HcHubCitiID = 28) OR (convert(date,PublishedDate) < @ExpiryDate AND HcHubCitiID = 10 AND NewsType NOT IN ('Videos','all')) OR (convert(date,PublishedDate) < @ExpiryDateAllCategORyNews AND HcHubCitiID = 10 AND NewsType = 'all') OR (convert(date,DateCreated) <> @ExpiryDateVideos AND HcHubCitiID = 10 AND NewsType = 'Videos') OR (convert(date,PublishedDate) < @ExpiryPublishedDateKilleen AND HcHubCitiID = 52)
		

	BEGIN TRAN

		--Deleting 30 days older News for 'spanqa.regionsapp'.
		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,PublishedDate) < @ExpiryPublishedDateRockwall AND HcHubCitiID = 28 

		---added on 05/05/2016
		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,PublishedDate) < @ExpiryPublishedDateRockwall AND HcHubCitiID = 10 AND NewsType NOT IN ('Videos','all')

		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,PublishedDate) < @ExpiryPublishedDateMarbelAllCategoryNews AND HcHubCitiID = 10 AND NewsType = 'all'

		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,DateCreated) <> @ExpiryPublishedDateMarbelVideos AND HcHubCitiID = 10 AND NewsType = 'Videos'

		--Deleting 5 days older News for 'Killeen Daily Herald'.
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateKilleen AND HcHubCitiID = 52  
		
		 --Insert into RssFeedNews
		 INSERT INTO RssFeedNews(Title
								,ImagePath
								,ShortDescription
								,LongDescription
								,Link
								,PublishedDate
								,NewsType
								,Message
								,HcHubCitiID
								,Classification
								,Section
								,AdCopy
								,VideoLink)															
			SELECT *
			FROM #InsertValues

						
		 DELETE FROM RssFeedNews
		 FROM RssFeedNews RF
		 INNER JOIN RssFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		 WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	 
			OR (RS.Title IS NULL AND RS.NewsType = 'Classifields'))
		 AND RF.DateCreated <> (SELECT MAX(DateCreated) FROM RssFeedNews)

		 COMMIT TRAN 		
				   
		  SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
		SET @ErrORNumber = ERROR_NUMBER()
		SET @ErrORMessage = ERROR_MESSAGE()
		SET @Status=1 
	END CATCH;
END;






GO
