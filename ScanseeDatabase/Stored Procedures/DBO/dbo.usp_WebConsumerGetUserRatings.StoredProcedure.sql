USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGetUserRatings]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebConsumerGetUserRatings]  
Purpose               : Retrieve Average User Rating for product in context 
Example               : EXEC [usp_WebConsumerGetUserRatings] ''  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------  
1.0      30th May 2013   Span   Initial Version  
---------------------------------------------------------------   
*/ 

CREATE PROCEDURE [dbo].[usp_WebConsumerGetUserRatings]
(
	@ProductID int,
	@UserID	int,
	
	--Output Variable 
	@ErrorNumber	int output,
	@ErrorMessage	varchar(1000) output 	
)	
As

Begin
	
	Begin Try		
				
		-- If User Rating already exists in the database
		
		If Exists (SELECT 1 FROM [UserRating] WHERE ProductID = @ProductID And UserID = @UserID)
			Begin			
			-- Retrive average user rating for a product
				SELECT ProductId, AVG(Isnull(Rating,0)) As AverageRating, COUNT(UserID) AS noOfUsersRated
				INTO #Avgrating 
				FROM [UserRating] 
				WHERE ProductID = @ProductID	
				GROUP BY ProductID
			

				SELECT ProductId, Isnull(Rating,0) As UserRating
				INTO #userrating
				FROM [UserRating] 
				WHERE ProductID = @ProductID
				And UserID = @UserID
				
				SELECT A.ProductID, AverageRating avgRating, UserRating currentRating , noOfUsersRated
				FROM #Avgrating A
				INNER JOIN #userrating U ON A.ProductID=U.ProductID
			End	
		
		ELSE
			BEGIN
						
				If Exists (Select 1 from [UserRating] where ProductID = @ProductID AND UserID <> @UserID )
					Begin			
					-- Retrive average user rating for a product
						SELECT ProductId, AVG(Isnull(Rating,0)) As avgRating, 0 currentRating ,COUNT(UserID) AS noOfUsersRated
						FROM [UserRating] 
						WHERE ProductID = @ProductID	
						GROUP BY ProductID
						
					End		
					
				Else
				
					-- If User Rating Does NOT exists in the database
					Begin
						Select ProductId, AVG(Isnull(Rating,0)) As  avgRating, 0 As UserRating ,0 AS noOfUsersRated
						From [UserRating] 
						Where ProductID = @ProductID	
						Group By ProductID		
					End
			END
		
	End Try

Begin Catch

	--Check whether the Transaction is uncommitable.
	If @@ERROR <> 0
	Begin
		Print 'Error occured in Stored Procedure usp_WebConsumerGetUserRatings.'		
		--- Execute retrieval of Error info.
		Exec [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
	End;
	 
End Catch;
	
End


GO
