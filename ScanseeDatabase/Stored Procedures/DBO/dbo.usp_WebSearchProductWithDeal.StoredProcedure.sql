USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSearchProductWithDeal]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebSearchProductWithDeal    
Purpose     : To Search for the Product in the vicinity with deal.    
Example     :     
    
History    
Version  Date       Author   Change Description    
-------------------------------------------------------------------------------     
1.0   9th December 2011    Pavan Sharma K Initial Version    
-------------------------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebSearchProductWithDeal]    
(    
    
 --Input Input Parameter(s)--    
        
       
    @ZIPCode varchar(10)    
  , @ProductName varchar(255)   
  , @Radius int   
  , @LowerLimit int    
     
      
     
 --Output Variable--    
       
  , @NextPageFlag bit output    
  , @RowCount INT OUTPUT    
  , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY      
  DECLARE @LATITUDE FLOAT    
  DECLARE @LONGITUDE FLOAT    
  DECLARE @UpperLimit INT    
  DECLARE @MaxCnt INT    
      
      
      
  --To get Media Server Configuration.    
   DECLARE @Config varchar(50)      
   SELECT @Config=ScreenContent      
   FROM AppConfiguration       
   WHERE ConfigurationType='Web Manufacturer Media Server Configuration'     
       
   --To get the row count for pagination.      
   DECLARE @ScreenContent Varchar(100)    
   SELECT @ScreenContent = ScreenContent       
   FROM AppConfiguration       
   WHERE ScreenName = 'All'     
   AND ConfigurationType = 'Website Pagination'    
   AND Active = 1       
      
  SET @UpperLimit = @LowerLimit + @ScreenContent    
      
      
   SELECT  @LATITUDE = G.Latitude    
      , @LONGITUDE = G.Longitude    
   FROM RetailLocation RL     
   INNER JOIN GeoPosition G ON G.PostalCode = RL.PostalCode    
   WHERE  RL.PostalCode = @ZIPCode     --CONVERT THE ZIP CODE INTO RESPECTIVE LATITUDE AND LONGITUDES    
      
 SELECT   RowNum = ROW_NUMBER() OVER (ORDER BY ProductID ASC)    
          , ProductID    
          , ProductName    
          , ProductShortDescription    
          , ProductLongDescription    
          , ProductImagePath    
          , ScanCode            
    , Distance     
    , RetailLocationID    
      
  INTO #TEMP    
  FROM     
             
  (SELECT DISTINCT TOP 100 PERCENT P.ProductID    
          , P.ProductName    
          , P.ProductShortDescription    
          , P.ProductLongDescription    
          , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND P.WebsiteSourceFlag = 1 THEN @Config +  CONVERT(VARCHAR(30),P.ManufacturerID) +'/'+ ProductImagePath    
          ELSE ProductImagePath END    
          , P.ScanCode            
    , Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214
 ,1,1)         
    , RL.RetailLocationID      
      
   FROM Product P     
   INNER JOIN RetailLocationProduct RLP ON P.ProductID = RLP.ProductID    
   INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID    
   INNER JOIN HotDealProduct HDP ON HDP.ProductID = P.ProductID    
   INNER JOIN ProductHotDeal PHD ON PHD.ProductHotDealID = HDP.ProductHotDealID    
   WHERE P.ProductID <> 0 AND Active = 1
   AND P.ProductName LIKE (CASE WHEN @ProductName IS NOT NULL THEN '%'+@ProductName+'%' ELSE '%' END)    
   OR P.ScanCode LIKE @ProductName + '%'    
   AND (P.ProductExpirationDate > GETDATE() OR P.ProductExpirationDate IS NULL)
   ORDER BY P.ProductName ASC) Product    
 WHERE Distance <= ISNULL(@Radius, 5)    
       
    --Capture the Number of rows in the result set.    
 SELECT @MaxCnt = MAX(RowNum) FROM #TEMP    
 SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS    
     
 SET @RowCount = @MaxCnt    
     
 SELECT DISTINCT    
           --RowNum     
            ProductID    
          , ProductName    
          , ProductShortDescription    
          , ProductLongDescription    
          , ProductImagePath    
          , ScanCode            
   -- , Distance     
   -- , RetailLocationID    
       
 FROM #TEMP     
 WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit    
      
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebSearchProductWithDeal.'      
  -- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output        
  END;    
       
 END CATCH;    
END;


GO
