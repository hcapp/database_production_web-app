USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebEventsAssociationRetailLocationsList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebEventsAssociationRetailLocationsList]
Purpose					: To display List of Associated RetailLocations.
Example					: [usp_WebEventsAssociationRetailLocationsList]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014      SPAN             1.0
---------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[usp_WebEventsAssociationRetailLocationsList]
(   
    --Input variable.	  
	  @EventID int
	, @RetailID int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			IF EXISTS (SELECT 1 FROM HcEvents WHERE HcEventID = @EventID AND RetailID = @RetailID AND BussinessEvent = 1)
			BEGIN
					SELECT retailLocationIds = RL.RetailLocationID
						  ,Address1 = RL.Address1
						  ,City = RL.City
						  ,State = RL.State
						  ,PostalCode = RL.PostalCode
						  ,StoreIdentification = RL.StoreIdentification
					FROM HcEvents E
					INNER JOIN HcRetailerEventsAssociation EA ON E.HcEventID = EA.HcEventID
					INNER JOIN RetailLocation RL ON EA.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
					WHERE E.HcEventID = @EventID AND E.Active = 1 AND BussinessEvent = 1
					AND E.RetailID = @RetailID AND RL.Active = 1	 
			END

			ELSE 
			BEGIN
					SELECT retailLocationIds = NULL
						  ,Address1 = EL.Address
						  ,City = EL.City
						  ,State = EL.State
						  ,PostalCode = EL.PostalCode
						  ,StoreIdentification = Null
					FROM HcEvents E
					INNER JOIN HcEventLocation EL ON E.HcEventID = EL.HcEventID
					WHERE E.HcEventID = @EventID AND E.Active = 1 AND BussinessEvent = 0
					AND E.RetailID = @RetailID
			END
			
			 
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebEventsAssociationRetailLocationsList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
