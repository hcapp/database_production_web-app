USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[HcxBatchProcess]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[HcxBatchProcess]
AS
BEGIN

SELECT DISTINCT top 5000 R.RetailID accountId,Retailname accountName
FROM Retailer R
INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND R.RetailerActive = 1 ANd Rl.Active = 1
LEFT JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
LEFT JOIN Contact C ON RC.ContactID = C.ContactID
INNER JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.Associated = 1
INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = RL.RetailID
WHERE RA.HcHubCitiID IN (49,19)
ORDER BY R.RetailID

END



GO
