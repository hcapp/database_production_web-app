USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBatchLogFiles]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_UpdateBatchLogFiles
Purpose                 : To get the status of the batch process for the day along with the file name that was processed.
Example                 : usp_UpdateBatchLogFiles  
  
History  
Version           Date				  Author           Change Description  
-----------------------------------------------------------------------   
1.0               7th January 2012    SPAN    Initial Version  
-----------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_UpdateBatchLogFiles]  
(  
      @APIPartnerID INT
    , @ProcessedFileName VARCHAR(255)
	, @APIStatus BIT
	, @Reason VARCHAR(1000)
	
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @APIPartnerName varchar(100)
			SELECT @APIPartnerName = APIPartnerName 
			FROM APIPartner 
			WHERE APIPartnerID = @APIPartnerID 

			--To get the count of processed rows.
			DECLARE @Count INT
			
			
			--IF @APIPartnerName = 'Plum District' OR @APIPartnerName='Living Social' OR @APIPartnerName='Mamasource' OR @APIPartnerName ='Restaurant.com' OR @APIPartnerName ='Half Off Depot' 
			--BEGIN
			--	SELECT @Count = COUNT(1)
			--	FROM ProductHotDeal HD 
			--	WHERE APIPartnerID = @APIPartnerID
			--	AND (CONVERT(DATE,CreatedDate) = CONVERT(DATE,GETDATE()) 
			--		OR
			--		CONVERT(DATE,DateModified) = CONVERT(DATE,GETDATE())) 
			--END	
			
				
			--To get the row of APIRowCount which was inserted at the time of deleting duplicates.
			DECLARE @Date DATETIME
			SELECT @Date = MAX(ExecutionDate)
			FROM APIBatchLog 
			WHERE APIPartnerID = @APIPartnerID 
			AND CONVERT(DATE,ExecutionDate) = CONVERT(DATE,GETDATE())
			
			--This logic should not be applied for CJ as the rowcounts are updated in the individual procedures.
			
			UPDATE APIBatchLog 
			SET Status = @APIStatus
			  , Reason = @Reason
			WHERE APIPartnerID = @APIPartnerID 
			AND ExecutionDate = @Date
			AND ProcessedFileName = @ProcessedFileName

			

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_BatchLogFiles.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;

GO
