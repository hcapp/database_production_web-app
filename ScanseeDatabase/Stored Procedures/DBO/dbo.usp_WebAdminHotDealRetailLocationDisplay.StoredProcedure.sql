USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminHotDealRetailLocationDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebAdminHotDealRetailLocationDisplay]
Purpose				  : To get Retailers RetailLocation.
Example				  : [usp_WebAdminHotDealRetailLocationDisplay] 
    
History    
Version     Date           Author    Change Description    
---------------------------------------------------------------     
1.0      2nd Sept 2013     SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebAdminHotDealRetailLocationDisplay] 
(    
     --Input Variables
      @RetailID int
    , @City varchar(1000)
    , @State char(1000)
	
	 --OutPut Variables
	, @Status int output 
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
	
		SELECT DISTINCT RetailLocationID
				,RetailName
				,RL.Address1
				,RL.Address2
				,RL.City
				,RL.[State]
				,RL.PostalCode
				,RL.StoreIdentification
				,RL.RetailLocationLatitude
				,RL.RetailLocationLongitude
				,RL.RetailLocationURL
		FROM RetailLocation RL
		INNER JOIN Retailer R ON RL.RetailID = R.RetailID AND RL.Active = 1 AND R.RetailerActive = 1
		INNER JOIN dbo.fn_SplitParam(@City, ',')C ON C.Param = RL.City
		INNER JOIN dbo.fn_SplitParam(@State, ',')S ON S.Param = RL.State
		WHERE RL.RetailID = @RetailID
		
		--Confirmation of Success
		SELECT @Status=0
		
	END TRY    
     
	BEGIN CATCH    
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure [usp_WebAdminHotDealRetailLocationDisplay].'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT @Status=1
		END;    
	END CATCH;
   
END;




GO
