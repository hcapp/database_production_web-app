USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerPreferredCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerPreferredCategory
Purpose					: 
Example					: usp_WebConsumerPreferredCategory 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			23rd may 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerPreferredCategory]
(
	 @UserID int
	,@Category varchar(max)
	,@Date datetime
	,@EmailAlert Bit
	,@CellPhoneAlert Bit
	
		
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
	
			DECLARE @LocaleID INT
			
			SELECT @LocaleID = LocaleID
			FROM UserLocation 
			WHERE UserID = @UserID 
		
			IF ISNULL(@Category, '') <> ''
			BEGIN
				SELECT Param Category
				INTO #Category
				FROM fn_SplitParam(@Category, ',')
			
				--To capture not existing categories.
				INSERT INTO [UserCategory]
					   ([UserID]
					   ,[CategoryID]
					   ,[DateAdded])
				SELECT @UserID 
					, Category 
					, @Date 
				FROM #Category C
				LEFT JOIN UserCategory UC ON UC.CategoryID = C.Category AND UC.UserID = @UserID 
				WHERE UC.CategoryID IS NULL
				
				--Update UserPreference SET EmailAlert=(CASE WHEN @EmailAlert=1 THEN 1 ELSE 0 END)
				--                         ,CellPhoneAlert=(CASE WHEN @CellPhoneAlert=1 THEN 1 ELSE 0 END)
				--WHERE UserID =@UserID 
				
				--To revome disabled categories.
				DELETE FROM UserCategory WHERE UserID = @UserID AND CategoryID NOT IN (SELECT Category FROM #Category)
			END
			ELSE IF ISNULL(@Category, '') = ''
			BEGIN
				--To revome disabled categories.
				DELETE FROM UserCategory WHERE UserID = @UserID --AND CategoryID NOT IN (ISNULL(@Category, ''))
			END
			
			IF EXISTS (SELECT 1 FROM UserPreference WHERE UserID = @UserID AND LocaleID = @LocaleID)
			BEGIN
				UPDATE UserPreference SET EmailAlert = @EmailAlert
										, CellPhoneAlert = @CellPhoneAlert
				WHERE UserID = @UserID AND LocaleID = @LocaleID 
				
			END
			ELSE 
			BEGIN
				INSERT INTO UserPreference(UserID, LocaleID, EmailAlert, CellPhoneAlert)
				VALUES(@UserID, @LocaleID, @EmailAlert, @CellPhoneAlert)
			END
			
				
			--Confirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerPreferredCategory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
