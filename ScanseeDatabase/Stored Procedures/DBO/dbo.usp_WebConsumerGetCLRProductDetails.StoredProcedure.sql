USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGetCLRProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name               : usp_WebConsumerGetCLRProductDetails  
Purpose                             : To fetch Product Details.  
Example                             : usp_WebConsumerGetCLRProductDetails 
  
History  
Version           Date              Author                  Change Description  
---------------------------------------------------------------   
1.0               3rd Sep 2013      Dhananjaya TR      Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerGetCLRProductDetails]  
(  
          @UserID int
		, @CouponID int
		, @LoyaltyID int
		, @RebateID int  
	
	  --Inputs for User Tracking	
	   , @MainMenuID int
	   , @CouponListID int
	   , @LoyaltyListID int
	   , @RebateListID int
	 
      --OutPut Variable  
      , @Status int output
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY  
      
		BEGIN TRANSACTION
      
				 --To get Media Server Configuration.  
				 DECLARE @ManufConfig varchar(50)    
				 SELECT @ManufConfig=ScreenContent    
				 FROM AppConfiguration     
				 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
				  
				CREATE TABLE #ProductList(ProductID int
										, productName varchar(1000)
										, productShortDescription varchar(1000)
										, imagePath varchar(1000))

				IF @CouponID IS NOT NULL
				BEGIN
				
					INSERT INTO #ProductList(ProductID
										   , productName
										   , productShortDescription
										   , imagePath)
										   
					SELECT P.ProductID ProductID
					, P.ProductName productName
					, P.ProductShortDescription productShortDescription
					, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																											THEN @ManufConfig
																											+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																											+ProductImagePath ELSE ProductImagePath 
																									  END   
										  ELSE ProductImagePath END 
					FROM CouponProduct CP
					INNER JOIN Product P ON P.ProductID = CP.ProductID 
					WHERE CouponID = @CouponID 
				END

				IF @LoyaltyID IS NOT NULL
				BEGIN
				
					INSERT INTO #ProductList(ProductID
										   , productName
										   , productShortDescription
										   , imagePath)
										   
					SELECT DISTINCT P.ProductID ProductID
					, P.ProductName productName
					, P.ProductShortDescription productShortDescription
					, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																											THEN @ManufConfig
																											+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																											+ProductImagePath ELSE ProductImagePath 
																									  END   
										  ELSE ProductImagePath END 
					FROM LoyaltyDeal L
					INNER JOIN LoyaltyDealProduct LDP ON L.LoyaltyDealID = LDP.LoyaltyDealID
					INNER JOIN Product P ON P.ProductID = LDP.ProductID 
					WHERE L.LoyaltyDealID = @LoyaltyID 
				END

				IF @RebateID IS NOT NULL
				BEGIN
				
					INSERT INTO #ProductList(ProductID
										   , productName
										   , productShortDescription
										   , imagePath)
					SELECT P.ProductID ProductID
					, P.ProductName productName
					, P.ProductShortDescription productShortDescription
					, imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																											THEN @ManufConfig
																											+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																											+ProductImagePath ELSE ProductImagePath 
																									  END   
										  ELSE ProductImagePath END 
					FROM RebateProduct RP
					INNER JOIN Product P ON P.ProductID = RP.ProductID 
					WHERE RP.RebateID  = @RebateID  
					
					
					END
					
					--Capture the impression of the products that are associated to the input CLR.
					CREATE TABLE #Temp(ProductListID int
									 , ProductID int)
									 
					INSERT INTO ScanSeeReportingDatabase..ProductList(MainMenuID
																	, ProductID
																	, CouponListID
																	, LoyaltyListID
																	, RebateListID
																	, CreatedDate)
					OUTPUT inserted.ProductListID, inserted.ProductID INTO #Temp(ProductListID, ProductID)
					SELECT @MainMenuID
						 , ProductID
						 , @CouponListID
						 , @LoyaltyListID
						 , @RebateListID
						 , GETDATE()
					FROM #ProductList
					
					--Display the result with the Primary key of the tracking table.
					SELECT T.ProductListID prodListID
						 , P.ProductID
						 , P.productName
						 , P.productShortDescription
						 , P.imagePath
					FROM #ProductList P
					INNER JOIN #Temp T ON T.ProductID = P.ProductID
					
				
			 --Confirmation of Success.
			 SELECT @Status = 0	
			COMMIT TRANSACTION
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_WebConsumerGetCLRProductDetails.'           
                  --- Execute retrieval of Error info.  
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output  
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
                  ROLLBACK TRANSACTION; 
                  --Confirmation of failure.
				  SELECT @Status = 1
            END;  
              
      END CATCH;  
END;


GO
