USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGetScreenContent]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerGetScreenContent]
Purpose					: To get Image of Product Summary page.
Example					: [usp_WebConsumerGetScreenContent]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			31st May 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerGetScreenContent]
(
	  @ConfigurationType varchar(50)
	  
	----User Tracking Imputs
 --   , @ScanTypeID int
 --   , @MainMenuID int
	  
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @Config varchar(50)
			DECLARE @WebConfig Varchar(200)
			
			SELECT @WebConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'Configuration of server'
			AND ScreenName = 'Web Email'
			
			--User Tracking
	        DECLARE	@RowCount int
	        
			--To get Email Template URL.
			IF @ConfigurationType = 'ShareURL'
			BEGIN
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Configuration of server'
			END
			--For all the other URL framing.
			ELSE
			BEGIN
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
			END

			SELECT  ConfigurationType
					,ScreenContent	= CASE WHEN ScreenName Like '%image%' THEN  @Config+ScreenContent 
										  WHEN ConfigurationType='ShareURL' THEN @Config+ScreenContent 
										  WHEN ConfigurationType = 'WebShareURL' THEN @WebConfig + ScreenContent 
									  Else ScreenContent END														
					,ScreenName 
					,Active 
					,DateCreated
					,DateModified
			FROM  AppConfiguration
			WHERE ConfigurationType=@ConfigurationType
	
	        SELECT @RowCount =@@ROWCOUNT 
		
		 --   --User Tracking
	
			--IF @ScanTypeID IS NOT NULL
			--BEGIN
			--	INSERT INTO ScanSeeReportingDatabase..scan(MainMenuID
			--											  ,ScanTypeID
			--											  ,Success
			--											  ,CreatedDate)
			--	Values(@MainMenuID 
			--		  ,@ScanTypeID 
			--		  ,(CASE WHEN @RowCount >0 THEN 1 ELSE 0 END)
			--		  ,GETDATE())										  
														
			--END	
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerGetScreenContent].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
