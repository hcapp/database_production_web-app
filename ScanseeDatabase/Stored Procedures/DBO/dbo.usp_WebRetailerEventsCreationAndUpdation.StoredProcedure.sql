USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerEventsCreationAndUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerEventsCreationAndUpdation]
Purpose					: To Create and Update Retailer Event.
Example					: [usp_WebRetailerEventsCreationAndUpdation]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/25/2014      SPAN            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerEventsCreationAndUpdation]
(
     
	 --Input variable	  
	  @RetailID Int
	, @EventID Int
	, @EventName Varchar(1000)
	, @ShortDescription Varchar(2000)
    , @LongDescription Varchar(2000) 
	, @ImagePath Varchar(2000)
	, @MoreInformationURL Varchar(2000)
	, @EventCategoryID Varchar(1000)
	, @OngoingEvent bit 
	, @StartDate DATE
	, @StartTime TIME
	, @EndDate date
	, @EndTime time
	, @RecurrencePatternID int
	, @RecurrenceInterval int
	, @EveryWeekday bit
	, @Days varchar(1000)
	, @EndAfter int
	, @DayNumber int
	, @BussinessEvent Bit  
	, @RetailLocationID Varchar(max)  -- Comma separated ID's
	, @Address Varchar(2000)
    , @City varchar(200)
    , @State Varchar(100)
    , @PostalCode VARCHAR(10)
    , @Latitude Float
    , @Longitude Float
    , @GeoErrorFlag BIT
	, @EventListingImagePath Varchar(2000)
	
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           
								
			DECLARE @RecurrencePattern varchar(10)

			SELECT @EventID = ISNULL(@EventID,0)

			SELECT @EveryWeekday = ISNULL(@EveryWeekday, 0)

			SELECT @RetailLocationID = REPLACE(@RetailLocationID, 'NULL', '0')
			
			SELECT @Days = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Days, '1', 'Sunday'), '2', 'Monday'), '3', 'Tuesday'), '4', 'Wednesday'), '5', 'Thursday'), '6', 'Friday'), '7', 'Saturday')

			SELECT @RecurrencePattern = RecurrencePattern FROM HcEventRecurrencePattern WHERE HcEventRecurrencePatternID = @RecurrencePatternID

			IF @EveryWeekday = 1
			BEGIN
				SELECT @Days = 'Monday,Tuesday,Wednesday,Thursday,Friday'				
			END


			IF EXISTS (SELECT 1 FROM HcEvents WHERE HcEventID = @EventID)
				BEGIN
						--Updating Event		
						UPDATE HcEvents SET HcEventName = LTRIM(RTRIM(@EventName))  
											, ShortDescription = @ShortDescription
											, LongDescription = @LongDescription								
											, ImagePath = @ImagePath
											, EventListingImagePath = @EventListingImagePath
											, BussinessEvent = @BussinessEvent 
											, StartDate = CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
											--, EndDate = IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @StartDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, '23:59:00.000') AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
											, EndDate = IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
											, DateModified = GETDATE() 
											, MoreInformationURL= @MoreInformationURL
											, OnGoingEvent = @OngoingEvent
											, HcEventRecurrencePatternID = IIF(@OngoingEvent = 1, @RecurrencePatternID, NULL) 
											, RecurrenceInterval = IIF(@OngoingEvent = 1, CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
																			THEN @RecurrenceInterval 
																		ELSE NULL 
																   END, NULL)
											, EventFrequency =  IIF(@OngoingEvent = 1, @EndAfter, NULL)
											, RetailID = @RetailID
											, Active = 1
						WHERE HcEventID = @EventID	
				END
			ELSE 
				BEGIN
						--Creating New Event		
						INSERT INTO HcEvents(HcEventName  
											, ShortDescription
											, LongDescription 								
											, ImagePath
											, EventListingImagePath
											, BussinessEvent 
											, StartDate 
											, EndDate 
											, DateCreated  
											, MoreInformationURL
											, OnGoingEvent
											, HcEventRecurrencePatternID
											, RecurrenceInterval
											, EventFrequency
											, RetailID
											, Active
											)	
						SELECT LTRIM(RTRIM(@EventName))
							 , @ShortDescription 
							 , @LongDescription 			      
							 , @ImagePath 
							 , @EventListingImagePath
							 , @BussinessEvent
							 , CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
							 --, @EndDate
							 , IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
							 , GETDATE()
							 , @MoreInformationURL	
							 , @OngoingEvent
							 , @RecurrencePatternID
							 , CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
										THEN @RecurrenceInterval 
									ELSE NULL 
							   END
							 , @EndAfter
							 , @RetailID
							 , 1
					
						SET @EventID = SCOPE_IDENTITY()

				END
			
			--Deleting EventCategory association
			DELETE FROM HcEventsCategoryAssociation
			WHERE HcEventID = @EventID

			--Associating Categories to Event
			INSERT INTO HcEventsCategoryAssociation(HcEventCategoryID 
													, HcEventID 
													, DateCreated
													, CreatedUserID)	
			SELECT [Param]
			      , @EventID 
			      , GETDATE()
			      , NULL
			FROM dbo.fn_SplitParam(@EventCategoryID, ',') 


			--Delete event Locations if already Existed
			DELETE FROM HcEventLocation 
			WHERE HcEventID = @EventID 

			--Insert Event Details to Event Location Table
			IF @BussinessEvent = 0
			BEGIN
					INSERT INTO HcEventLocation(HcEventID
												, Address
												, City
												, State
												, PostalCode
												, Latitude
												, Longuitude
												, GeoErrorFlag
												, DateCreated)
					SELECT  @EventID 
							, @Address 
							, @City
							, @State 
							, @PostalCode 
							, @Latitude
							, @Longitude 
							, @GeoErrorFlag
							, GETDATE()
			END
	
			--Deleting RetailLocations for given Retailer
			DELETE FROM HcRetailerEventsAssociation
			WHERE HcEventID = @EventID
					 
			IF @BussinessEvent = 1
			BEGIN
					
					--Associating Events to RetailLocations
					INSERT INTO HcRetailerEventsAssociation(HcEventID
															, RetailID
															, RetailLocationID
															, DateCreated)
					SELECT  @EventID
						  , @RetailID
						  , [Param]
						  , GETDATE()
					FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END
			
			DELETE FROM HcEventInterval
			WHERE HcEventID = @EventID	   	
			--For On Going Events insert the ongoing pattern into the association tables.
			IF @OngoingEvent = 1
			BEGIN
				--If the event is created as Daily with a inteval value then do not insert record into HcEventInterval table.
				--IF ((@RecurrencePattern <> 'Daily' AND @EveryWeekday <> 0) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				IF ((@RecurrencePattern = 'Weekly' OR @RecurrencePattern = 'Monthly' ) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				BEGIN
					DELETE FROM HcEventInterval WHERE HcEventID = @EventID
					INSERT INTO HcEventInterval(HcEventID
											  , DayNumber
											  , MonthInterval
											  , DayName
											  , DateCreated)
											  --, CreatedUserID)
									SELECT @EventID
										 , @DayNumber
										 , IIF(@RecurrencePattern ='Monthly', @RecurrenceInterval, NULL)
										 , LTRIM(RTRIM(REPLACE(REPLACE(Param, '[', ''), ']', '')))
										 , GETDATE()
										 --, @UserID
									FROM [HubCitiWeb].fn_SplitParam(@Days, ',') A
				END
			END
				
	       --Confirmation of Success.
		   SELECT @Status = 0

		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerEventsCreationAndUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;

			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
