USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHotDealRetrieveRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebHotDealRetrieveRetailerLocation
Purpose					: To retrieve list of Retailer locations for a Retailer that is having the input product.
Example					: usp_WebHotDealRetrieveRetailerLocation

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			5th January 2012				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHotDealRetrieveRetailerLocation]

--Input parameters
	@RetailID varchar(100)   --Comma separated reailIDs and productIDs
  , @ProductID varchar(100)

--Output variables
	,@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		--Filter RetailLocations.
		SELECT RetailID, RetailLocationID, Address1
		INTO #RetailLocations
		FROM RetailLocation 
		WHERE RetailID IN (SELECT PARAM FROM dbo.fn_SplitParam(@RetailID,',')) AND Active = 1
		ORDER BY RetailID
		
		--Select RetailLocations Associated to the retailer and also based on the product.
		
		--If product is selected, display only those retaillocations where the product is available.
		IF @ProductID IS NOT NULL
		BEGIN
			SELECT DISTINCT RL.RetailID
			 , RL.RetailLocationID	
			 , RL.Address1	     
		FROM #RetailLocations RL
		INNER JOIN RetailLocationProduct RLP ON RL.RetailLocationID = RLP.RetailLocationID
		INNER JOIN dbo.fn_SplitParam(@ProductID, ',') F ON F.Param = RLP.ProductID
		END
		
		ELSE
		--If product is not selected diplay all the locations for the retailer.
		BEGIN
			SELECT DISTINCT RL.RetailID
			 , RL.RetailLocationID	
			 , RL.Address1	     
		FROM #RetailLocations RL
		END
		
		--Confirm Success
		SELECT @Status = 0
			
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealRetrieveRetailerLocation.'		
			SELECT @Status = 1
			ROLLBACK TRANSACTION
		END;
		 
	END CATCH;
END;




GO
