USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAddHotDealProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponProductSearch
Purpose					: 
Example					: usp_WebRetailerCouponProductSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		12th January 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAddHotDealProductSearch]
(
	@RetailID int	
	,@ProductSearch Varchar(1000)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration' 
		
		
		
			SELECT DISTINCT P.ProductID
				   ,ProductName
				   ,ScanCode
				   ,ProductShortDescription shortDescription
				   ,productImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+  ProductImagePath
				   						ELSE ProductImagePath END
				   ,ISNULL(RP.Price,0) price
				   ,ISNULL(RP.SalePrice,0) suggestedRetailPrice
			FROM Product P
			INNER JOIN RetailLocationProduct RP ON P.ProductID=RP.ProductID
			INNER JOIN RetailLocation R ON R.RetailLocationID=RP.RetailLocationID
			WHERE P.ProductID <> 0 AND R.Active = 1
			AND RetailID=@RetailID
				AND (ProductName LIKE(CASE WHEN ISNULL(@ProductSearch,'') = '' THEN '%' ELSE '%' + @ProductSearch + '%' END)
					OR ScanCode LIKE @ProductSearch + '%')
				    
		
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponProductSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
