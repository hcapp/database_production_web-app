USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchHalfOffDepotLocDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchHalfOffDepotLocDataPorting
Purpose					: To move Location data from Stage Half Off Depot Map table to Production ProductHotDealLocation Table
Example					: usp_BatchHalfOffDepotLocDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15th Feb 2013	SPAN		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchHalfOffDepotLocDataPorting]
(
	
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'Half Off Depot'
		
			MERGE ProductHotDealLocation AS T
			USING (SELECT PD.ID
						, HD.ProductHotDealID 
						, PD.City
						, PD.[State]
				   FROM APIHalfOffDepotData PD
						INNER JOIN ProductHotDeal HD ON HD.HotDealName = PD.Name 
						AND ISNULL(HD.HotDealShortDescription,0)=ISNULL(PD.[Short Description],0)
						AND HD.HotDealImagePath=PD.[IMAGEURL]
						AND ISNULL(HD.HotDealURL, 0) = ISNULL(PD.URL, 0)
						AND HD.price =PD.Price
						AND HD.Saleprice=PD.[Sale price]
						AND PD.CITY IS NOT NULL AND PD.STATE IS NOT NULL
						AND HD.APIPartnerID = @APIPartnerID) AS S
			ON (T.ProductHotDealID = S.ProductHotDealID)
			
			WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
				THEN INSERT([ProductHotDealID]
						   ,[City]
						   ,[State])			   
					 VALUES(S.[ProductHotDealID]
						   ,S.[City]
						   ,S.[State])
			WHEN MATCHED  
				THEN UPDATE SET T.[City] = S.[City]
				               ,T.[State]=S.[State];
				
		--To assocaiate Hot Deals to the Retail Locations.
			INSERT INTO ProductHotDealRetailLocation(ProductHotDealID
									   , RetailLocationID)
			SELECT DISTINCT P.ProductHotDealID
							, RL.RetailLocationID
			FROM APIHalfOffDepotData T
			INNER JOIN Retailer R ON R.RetailName = T.[Short Description]
			INNER JOIN ProductHotDeal P ON P.HotDealName = T.NAME AND ISNULL(P.HotDealShortDescription,0)=ISNULL(T.[Short Description],0)
						AND P.HotDealImagePath=T.[IMAGEURL]
						AND ISNULL(P.HotDealURL, 0) = ISNULL(T.URL, 0)
						AND P.price =T.Price
						AND P.Saleprice=T.[Sale price]
			INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND RL.City = T.CITY AND RL.State = T.STATE
			
		--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchHalfOffDepotLocDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
