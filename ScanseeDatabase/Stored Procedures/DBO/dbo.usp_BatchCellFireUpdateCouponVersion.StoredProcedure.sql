USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireUpdateCouponVersion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireUpdateCouponVersion
Purpose					: To update the Version of the coupon that the current batch processes.
Example					: usp_BatchCellFireUpdateCouponVersion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Oct 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireUpdateCouponVersion]
(
	
	   @CellFireCouponVersion VARCHAR(100)
	   
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
				
				UPDATE AppConfiguration 
				SET ScreenContent = LTRIM(RTRIM(@CellFireCouponVersion))		
				WHERE ConfigurationType = 'CellFire Coupon Version Number'
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireUpdateCouponVersion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
