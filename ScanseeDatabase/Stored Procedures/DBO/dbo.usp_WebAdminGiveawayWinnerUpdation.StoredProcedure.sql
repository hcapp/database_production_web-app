USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminGiveawayWinnerUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAdminGiveawayWinnerUpdation
Purpose					: To Update Giveaway winner. 
Example					: usp_WebAdminGiveawayWinnerUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6 May 2013	    Span        	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminGiveawayWinnerUpdation]
(
	  
	--Input Variable
	  @QRRetailerCustomPageID Varchar(255)
	    
	
	--Output Variable
	, @Status int output 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 BEGIN TRANSACTION
		 
		 --Updating Winner of GiveAwayPage.
		 UPDATE UserGiveawayAssociation
		 SET  Winner = 1
		 FROM UserGiveawayAssociation U
		 INNER JOIN dbo.fn_SplitParam(@QRRetailerCustomPageID,',') P ON P.Param =U.QRRetailerCustomPageID 	 
		 
		 
		--Confirmation of Success.
		 SELECT @Status = 0
		COMMIT TRANSACTION
						
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebAdminGiveawayWinnerUpdation>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
