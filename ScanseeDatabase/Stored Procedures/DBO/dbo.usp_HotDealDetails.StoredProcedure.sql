USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_HotDealDetails    
Purpose      : To get details of a Hot Deal.    
Example      : usp_HotDealDetails 1,1    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   23rd June 2011 SPAN Infotech India Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_HotDealDetails]    
(    
   @UserID int    
 , @HotDealID int    
     
 --OutPut Variable    
 , @ErrorNumber int output    
 , @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    
    
 BEGIN TRY    
  --To get Server Configuration
 DECLARE @ManufConfig varchar(50) 
 DECLARE @RetailerConfig varchar(50)
 DECLARE @Config Varchar(50)  
 
 SELECT @ManufConfig=(select ScreenContent  
					 FROM AppConfiguration   
					 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
	   ,@RetailerConfig=(select ScreenContent  
						 FROM AppConfiguration   
						 WHERE ConfigurationType='Web Retailer Media Server Configuration')
	   ,@Config=(select ScreenContent  
				 FROM AppConfiguration   
				 WHERE ConfigurationType='App Media Server Configuration')
 FROM AppConfiguration
    
    
  SELECT DISTINCT P.ProductHotDealID    
    ,ISNULL(Category, 'Uncategorized') AS Category    
    ,HotDealName    
    ,HotDealProvider    
    ,Price hDPrice    
    ,HotDealDiscountType hDDiscountType    
    ,HotDealDiscountAmount hDDiscountAmount    
    ,HotDealDiscountPct hDDiscountPct    
    ,SalePrice hDSalePrice    
    ,hDLognDescription = CASE WHEN ISNULL(HotDeaLonglDescription,0) = '0' OR  HotDeaLonglDescription = '' THEN     
           CASE WHEN ISNULL(HotDealShortDescription,0) = '0' OR  HotDealShortDescription = '' THEN HotDealName    
           ELSE HotDealShortDescription     
           END    
            ELSE HotDeaLonglDescription    
          END    
                    
    ,hotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
												CASE WHEN WebsiteSourceFlag = 1 THEN 
														    CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
															    +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
															   +HotDealImagePath  
															ELSE @RetailerConfig  
															+CONVERT(VARCHAR(30),P.RetailID)+'/'  
															+HotDealImagePath  
															END  					                         
									            ELSE P.HotDealImagePath  
												END  
                        END  
    ,HotDealTermsConditions hDTermsConditions    
    ,HotDealURL hdURL    
    ,SUBSTRING(CONVERT(VARCHAR(20), HotDealStartDate, 100),1,7) hDStartDate    
    ,SUBSTRING(CONVERT(VARCHAR(20), HotDealEndDate, 100),1,7) hDEndDate    
    ,@Config+A.APIPartnerImagePath apiPartnerImagePath    
    ,A.APIPartnerName   
    , PHL.City  
  FROM ProductHotDeal P    
  LEFT JOIN ProductHotDealLocation PHL ON PHL.ProductHotDealID = P.ProductHotDealID
  LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID    
  WHERE P.ProductHotDealID = @HotDealID     
 END TRY    
     
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_HotDealDetails.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
  END;    
       
 END CATCH;    
END;

GO
