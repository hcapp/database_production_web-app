USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerUpdateRetailerLocations_bck]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerUpdateRetailerLocations]
Purpose					: 
Example					: [usp_WebRetailerUpdateRetailerLocations] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03 April 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerUpdateRetailerLocations_bck]
(
	  @RetailID int 
	, @RetailLocationID int
	, @StoreIdentification varchar(20)
	, @Address1 varchar(255)
	, @City varchar(100)
	, @State char(2)
	, @PostalCode varchar(10)
	, @PhoneNumber varchar(20)
	, @StartTime TIME
	, @EndTime TIME
	, @StartTimeUTC TIME
	, @EndTimeUTC TIME
	, @TimeZoneID INT
	, @RetailLocationURL Varchar(1000)
	, @RetailLocationKeyword Varchar(1000)
	, @RetailLocationLatitude decimal(18,6)
	, @RetailLocationLongitude decimal(18,6)
	, @RetailLocationImagePath varchar(1000)
	
	--Output Variable 
	, @DuplicateStore bit output
	, @Status bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 

)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		    DECLARE @RetailAssociatedFlag Int
			DECLARE @RetailLocAssociatedFlag Int	

		    SET @RetailAssociatedFlag=0
			SET @RetailLocAssociatedFlag =0

			--Check if the Store ID is Null
			IF @StoreIdentification IS NULL
			BEGIN
				RAISERROR('Mandatory field cant be Null !!!!', 18, 1)
			END
			
			ELSE			
			BEGIN
			SELECT 'A'
				
				--Check if the CorporateandStore is updated.				
				IF(@RetailLocationID = (SELECT RetailLocationID FROM RetailLocation WHERE CorporateAndStore = 1 AND RetailID = @RetailID AND Active = 1))
				BEGIN
					
				    IF (@PostalCode<>(Select PostalCode From retailer Where RetailID =@RetailID))
					BEGIN
					SET @RetailAssociatedFlag=1
					END				
			        
					UPDATE Retailer
					SET  
					    [Address1]=@Address1 
					   ,[City]=@City
					   ,[State]=@State
					   ,[PostalCode]=@PostalCode
					   ,[CountryID]=1
					   ,[RetailURL] = @RetailLocationURL
					   --,[NumberOfLocations] = @NumberOfLocations
					   ,[DateModified]=GETDATE()   
					WHERE RetailID=@RetailID 
					 
				END
				
				--Check if the storeID already exists for some other retail location for the retailer.
				IF EXISTS(SELECT 1 
						  FROM RetailLocation 
						  WHERE StoreIdentification = @StoreIdentification 
						  --StoreIdentification LIKE '%@StoreIdentification%' 
						  AND RetailLocationID <> @RetailLocationID
						  AND RetailID = @RetailID AND Active = 1) 
							
					BEGIN
				
					SET @DuplicateStore=1
					
				
					END
				--IF the StoreID is unique
				ELSE
				BEGIN				
					 SELECT 'B'
					  IF (@PostalCode<>(Select PostalCode From RetailLocation  Where RetailLocationID =@RetailLocationID AND Active = 1))
					  BEGIN
					      SET @RetailLocAssociatedFlag =1
					  END		
					   SELECT 'C'

					  UPDATE RetailLocation SET Address1 = @Address1
											  , StoreIdentification=@StoreIdentification 
											  , RetailLocationURL =@RetailLocationURL 
											  , State = UPPER(@State)
											  , City = @City
											  , PostalCode = @PostalCode
											  , StartTime = @StartTime
											  , EndTime = @EndTime
											  , StartTimeUTC = @StartTimeUTC
											  , EndTimeUTC = @EndTimeUTC
											  , TimeZoneID = @TimeZoneID
											  , RetailLocationLatitude = CASE WHEN @RetailLocationLatitude = '0.0' OR @RetailLocationLatitude IS NULL THEN (SELECT Latitude FROM GeoPosition WHERE PostalCode = @PostalCode) ELSE @RetailLocationLatitude END
											  , RetailLocationLongitude = CASE WHEN @RetailLocationLongitude = '0.0' OR @RetailLocationLongitude IS NULL THEN (SELECT Longitude FROM GeoPosition WHERE PostalCode = @PostalCode)ELSE @RetailLocationLongitude END
											  , RetailLocationImagePath = CASE WHEN @RetailLocationImagePath IS NULL OR @RetailLocationImagePath LIKE '' THEN Null ELSE @RetailLocationImagePath END
									WHERE RetailLocationID = @RetailLocationID AND Active = 1

									SELECT 'D'
									
					 --Update the corporate phone number if the Retail location is found to be the Corporate and Store.
					 UPDATE Retailer SET CorporatePhoneNo = CASE WHEN RL.CorporateAndStore = 1 THEN @PhoneNumber ELSE R.CorporatePhoneNo END 
					 FROM RetailLocation RL
		             INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND RL.Active = 1
		             WHERE RL.RetailLocationID = @RetailLocationID 
						 
					 UPDATE Contact SET ContactPhone = @PhoneNumber
					 FROM Contact C 
					 WHERE ContactID = (SELECT ContactID FROM RetailContact RC 
										WHERE C.ContactID = RC.ContactID
										AND RC.RetailLocationID = @RetailLocationID)	 
					
					--Check if there are keyywords associated to the Retail Location.
					 IF EXISTS(SELECT 1 FROM RetailerKeywords WHERE RetailLocationID = @RetailLocationID)
					 BEGIN
							 UPDATE RetailerKeywords SET RetailKeyword = @RetailLocationKeyword 
														,DateModified = GETDATE()					                            
							 WHERE RetailLocationID = @RetailLocationID 
					 END
					 --If the given Retail Location does not have any keywords asoociated then insert.
					 ELSE IF(@RetailLocationKeyword IS NOT NULL)
					 BEGIN
						INSERT INTO RetailerKeywords(RetailID
												   , RetailLocationID
												   , RetailKeyword
												   , DateCreated)
											VALUES(@RetailID
												 , @RetailLocationID
												 , @RetailLocationKeyword
												 , GETDATE())
					 END
					 
					--Update the ProductHotDealLocation Table because the association is maintained there.					
					UPDATE ProductHotDealLocation
					SET City = @City
					  , State = @State					
					FROM ProductHotDealRetailLocation PRL
					INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = PRL.ProductHotDealID 
					WHERE PRL.RetailLocationID = @RetailLocationID

					----Refresh the association w.r.t the Retail location
					--DELETE FROM HcRetailerAssociation 
					--FROM HcRetailerAssociation A
					--INNER JOIN RetailLocation RL ON RL.RetailLocationID = A.RetailLocationID
					--WHERE RL.RetailLocationID = @RetailLocationID
					--AND RL.Headquarters = 0 AND A.Associated = 1

					----Associate the retailer to the Hub Citi.
					--INSERT INTO HcRetailerAssociation(HcHubCitiID
					--							,RetailID
					--							,RetailLocationID
					--							,DateCreated
					--							,Associated) 
					--			SELECT DISTINCT LA.HcHubCitiID 
					--								,@RetailID
					--								,@RetailLocationID
					--								,GETDATE()
					--								,0
					--			FROM HcLocationAssociation LA
					--			INNER JOIN RetailLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode 
					--			LEFT JOIN HcRetailerAssociation RA ON RA.RetailLocationID = @RetailLocationID AND LA.HcHubCitiID = RA.HcHubCitiID 
					--			WHERE RA.HcRetailerAssociationID IS NULL
					--			AND  RL.RetailLocationID = @RetailLocationID
			  --   				AND RL.Headquarters = 0 

			   --      IF (@RetailAssociatedFlag =1)
					 --BEGIN
						--UPDATE HcRetailerAssociation set Associated =0 Where RetailID =@RetailID 					  
					 --END
					  IF (@RetailLocAssociatedFlag  =1)
					 BEGIN
						 UPDATE HcRetailerAssociation set Associated =0 Where RetailLocationID =@RetailLocationID  					  
					 END

					
					
					SET @DuplicateStore=0
			    END
		
		--Confirmation of Success.
			SELECT @Status = 0
			
		END
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerUpdateRetailerLocations].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
