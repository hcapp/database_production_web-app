USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerPreferredUniversity]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_UserTrackingWebConsumerMainMenuCreation]
Purpose					: To Display the Shopper preferred university.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			27th May 2013				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerPreferredUniversity]

 --Input Variables
    @UserID int
  , @UniversityID int

 --Output variables
  , @Status int OUTPUT

AS
BEGIN

	BEGIN TRY
	
		--Delete the university if the user has deselected the university.
		IF EXISTS (SELECT 1 FROM UserUniversity WHERE UserID = @UserID)
		BEGIN
			IF @UniversityID IS NULL
			BEGIN
				DELETE FROM UserUniversity WHERE UserID = @UserID
			END
		END
		
		--Update the user preferred university.
		IF @UniversityID IS NOT NULL
		BEGIN
			DELETE FROM UserUniversity WHERE UserID = @UserID
			
			INSERT INTO UserUniversity(UserID, UniversityID, DateCreated)
			SELECT @UserID, @UniversityID, GETDATE()
		END
		
		--Confirm Success
		SELECT @Status = 0
		
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebShopperDisplayPreferences.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
