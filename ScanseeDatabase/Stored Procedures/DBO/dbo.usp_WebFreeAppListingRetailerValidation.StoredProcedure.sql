USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFreeAppListingRetailerValidation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebFreeAppListingRetailerValidation]
Purpose					: To register New Retailer. 
Example					: [usp_WebFreeAppListingRetailerValidation]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			01st April 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFreeAppListingRetailerValidation]
(
	  @RetailerID int
	, @DuplicateRetailerID int
	, @DuplicateRetailerLocationID int

				
	--Output Variable 	
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
		  UPDATE Retailer SET DuplicateRetailerID=@DuplicateRetailerID,DuplicateRetailLocationID=@DuplicateRetailerLocationID
		  WHERE RetailID=@RetailerID AND RetailerActive = 1

		  UPDATE RetailLocation SET Active = 0 WHERE RetailLocationID = @DuplicateRetailerLocationID AND Active = 1
		  
				
		--Confirmation of Success.
		SELECT @Status = 0
		  
	   COMMIT TRANSACTION
	END TRY
		
		
	BEGIN CATCH	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebFreeAppListingRetailerValidation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
