USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierHotDealUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierHotDealUpdation
Purpose					: To Add a new Hot Deal by the Supplier.
Example					: usp_WebHotDealAdd

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierHotDealUpdation]               
(

	--Input Parameter(s)--
	 
	 --Inputs for ProductHotDeal Table
	  @ManufacturerID int 
	, @HotDealID int 
	, @HotDealName  varchar(255)
    , @RegularPrice money
    , @SalePrice money
    , @HotDealDescription  varchar(1000)
    , @HotDealLongDescription  varchar(3000)
    , @HotDealTerms  varchar(1000)
    , @URL  varchar(1000)
    , @DealStartDate date
    , @DealStartTime time
    , @DealEndDate date
    , @DealEndTime time
    , @HotDealTimeZoneID int  
    , @CategoryID int
    
    --, @City varchar(MAX)			Commented because the Prototype is changed to take the Population Centre ID as input.
    --, @State varchar(MAX)
    
    , @PopulationCentreID VARCHAR(MAX) 
    
    , @RetailID varchar(max)
    , @RetailLocationID varchar(max)
     
     --Inputs for HotDeaalProduct Table   
  
    , @ProductID varchar(max)
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
	   
	   
	   
	   DECLARE @HDOldSalePice Money
	   
	   SELECT @HDOldSalePice=SalePrice 
		    FROM ProductHotDeal 
		    WHERE ProductHotDealID =@HotDealID  
		    IF (@SalePrice<>@HDOldSalePice)
		    BEGIN	    
		    
		        SELECT UserID
		              ,ProductID
		              ,DeviceID
		              ,ProductHotDealID		              
		        INTO #Temp
		        FROM (
		        SELECT DISTINCT U.UserID
		              ,UP.ProductID
		              ,UD.DeviceID 
		              ,HD.ProductHotDealID  
		              , Distance= (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(Latitude  / 57.2958) * COS((Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 
		              ,LocaleRadius 
		        from fn_SplitParam(@ProductID,',') F
		        INNER JOIN HotDealProduct  HD ON HD.ProductID =F.Param AND HD.ProductHotDealID =@HotDealID 	        
		        INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID =HD.ProductHotDealID 
		       -- INNER JOIN RetailLocation RL ON RL.RetailLocationID =PRL.RetailLocationID  
		        INNER JOIN UserProduct UP ON UP.ProductID =HD.ProductID AND WishListItem =1	
		        INNER JOIN Users U ON U.UserID =UP.UserID
		        INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1   
		        INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
		        INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
		    )Note
		     --WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		    
		    
		        --UPDATE UserPushNotification SET CouponID=NULL,RetailLocationDealID=Null 
		        --FROM UserPushNotification UP
		        --INNER JOIN #Temp T ON T.UserID =UP.UserID AND T.ProductID =UP.ProductID 
		        --WHERE NotificationSent =1 AND (T.ProductHotDealID =UP.ProductHotdealID OR UP.ProductHotdealID IS NULL)
		        
		        --User pushnotification already exist then NotificationSent flag updating to 0
		        UPDATE UserPushNotification SET NotificationSent=0
		                                       ,DateModified =GETDATE()			                                      
		        FROM UserPushNotification UP
		        INNER JOIN #Temp T ON UP.ProductHotdealID=T.ProductHotDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0
		       
		       --User pushnotification not exist then insert. 
		       INSERT INTO UserPushNotification(UserID
												,ProductID
												,DeviceID												
												,ProductHotdealID												
												,NotificationSent
												,DateCreated)
				SELECT T.UserID 
				      ,T.ProductID 
				      ,T.DeviceID 
				      ,T.ProductHotDealID 
				      ,0
				      ,GETDATE()
				FROM #Temp T				
				LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.ProductHotDealID =UP.ProductHotdealID AND T.ProductID =UP.ProductID AND DiscountCreated=0  
				WHERE UP.UserPushNotificationID IS NULL   		          
		    
		    END	
		
		--UPDATE ProductHotDeal TABLE 
		
		
		     		UPDATE ProductHotDeal
					SET HotDealName = @HotDealName
					  , Price =  @RegularPrice
					  , SalePrice = @SalePrice
					  , HotDealShortDescription = @HotDealDescription
					  , HotDeaLonglDescription = @HotDealLongDescription
					  , HotDealTermsConditions = @HotDealTerms
					  , HotDealURL = @URL
					  , HotDealStartDate = CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'') AS DATETIME)
					  , HotDealEndDate = CAST(@DealEndDate AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME)
					  , DateModified = GETDATE()	
					  , HotDealTimeZoneID = @HotDealTimeZoneID
					  , CategoryID = @CategoryID
				WHERE ProductHotDealID = @HotDealID
							
		--UPDATE HotDealProduct TABLE
		
		IF(@RetailID IS NOT NULL AND @RetailLocationID IS NOT NULL)
		BEGIN
			DELETE FROM ProductHotDealLocation WHERE ProductHotDealID = @HotDealID     --IF IN CASE THE DEAL WAS W.R.T THE CITY THEN DELETE THAT ENTRY. 
			
			DELETE FROM ProductHotDealRetailLocation WHERE ProductHotDealID = @HotDealID         --DELETE THE EXISTING ENTRY
			
			INSERT INTO ProductHotDealRetailLocation (ProductHotDealID
			                                        , RetailLocationID)
			                                SELECT @HotDealID
			                                     , PARAM
			                                FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			INSERT INTO ProductHotDealLocation(ProductHotDealID												 
												  , City
												  , [State])
										SELECT @HotDealID
										     , RL.City
											 , RL.State
										FROM RetailLocation RL 
										WHERE RetailLocationID = @RetailLocationID
		
		END
		
		--Update the Product Association Details.
		IF(@ProductID IS NOT NULL)			
		BEGIN
		DELETE FROM HotDealProduct WHERE ProductHotDealID = @HotDealID					--DELETE THE EXISTING RECORD AND INSERT NEW SET OF VALUES
		
		INSERT INTO HotDealProduct (
										ProductHotDealID
									  , ProductID									  									 									  
								    )
								    
					SELECT @HotDealID
						 , PARAM
					FROM dbo.fn_SplitParam(@ProductID, ',')
      								
		
		END
		
		--UPDATE ProductHotDealRetailLocation TABLE
		
		 IF (@PopulationCentreID IS NOT NULL)
			BEGIN
				DELETE FROM ProductHotDealRetailLocation WHERE ProductHotDealID = @HotDealID     --If the hot deal was placed w.r.t the retail location before then delete it.
				
				DELETE FROM ProductHotDealLocation WHERE ProductHotDealID = @HotDealID
				
				INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]											 
											  )
										SELECT @HotDealID
										     , City
										     , [State]
										FROM dbo.fn_SplitParam(@PopulationCentreID, ',') P
										INNER JOIN PopulationCenterCities PC ON P.Param = PC.PopulationCenterID										
			
		  END           
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealAdd.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
