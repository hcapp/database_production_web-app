USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerEventDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerEventDeletion]
Purpose					: Deleting Selected  Event.
Example					: [usp_WebRetailerEventDeletion]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014      SPAN             1.0
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[usp_WebRetailerEventDeletion]
(   
    --Input variable.	  
      @EventID Int
	
  	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--Logical Delete
			UPDATE HcEvents	SET Active = 0
			WHERE HcEventID = @EventID 


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerEventDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
