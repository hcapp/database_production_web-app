USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerHotDealSearchPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name               : usp_WebConsumerHotDealSearchPagination
Purpose                             : To get the list of Hotdeals available.
Example                             : usp_WebConsumerHotDealSearchPagination 1, 'o', 0,50

History
Version           Date              Author                  Change Description
--------------------------------------------------------------- 
1.0               13th June 2013    Dhananjaya TR           Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerHotDealSearchPagination]
(
        @UserID int
      --, @SortBy char(1)
      , @Category varchar(2000)
     -- , @Search varchar(255)
      , @LowerLimit int
      , @RecordCount Int
     -- , @Latitude decimal(18,6)    
    ---  , @Longitude decimal(18,6) 
      , @PopulationCentreID int  
      , @Radius int
      , @PostalCode varchar(10) 
      
      --User Tracking Inputs  
      , @MainMenuID Int
      
      --OutPut Variable
      , @MaxCnt int output       
      , @FavCatFlag bit output
      , @NxtPageFlag bit output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output
)
AS
BEGIN

      
      BEGIN TRY
            DECLARE @ByCategoryFlag bit
           -- DECLARE @Today datetime = GETDATE()           
            DECLARE @CategorySet bit
            DECLARE @SearchID Int                                 
            DECLARE @HotDealBycityID Int 
            DECLARE @Latitude decimal(18,6)    
            DECLARE @Longitude decimal(18,6)  
            
                      
            --Check if the User has selected the any categories
            SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
            FROM UserCategory 
            WHERE UserID = @UserID       
         
            IF @PostalCode IS NULL
            BEGIN
            SELECT @PostalCode= PostalCode 
            FROM USERS 
            WHERE UserID =@UserID 
            END  
            
            SELECT ProductHotDealID 
            INTO #ProductHotdeal
            FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0
            
            SELECT @FavCatFlag = CASE WHEN @CategorySet = 1 THEN 0 ELSE 1 END 
      
            --To get Server Configuration
            DECLARE @ManufConfig varchar(50) 
             DECLARE @RetailerConfig varchar(50) 
               
             SELECT @ManufConfig=(SELECT ScreenContent  
                                           FROM AppConfiguration   
                                           WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
                   ,@RetailerConfig=(SELECT ScreenContent  
                                           FROM AppConfiguration   
                                           WHERE ConfigurationType='Web Retailer Media Server Configuration')
            FROM AppConfiguration  
      
            --To get the row count for pagination.
            DECLARE @UpperLimit int 
			SELECT @UpperLimit = @LowerLimit + @RecordCount
			                                                                                                                
            --DECLARE @MaxCnt int
            
            --To capture Koala APIPartnerID
            --DECLARE @KoalaPartnerID int
            --SELECT @KoalaPartnerID =  APIPartnerID  FROM APIPartner WHERE APIPartnerName = 'SimpleRelevance'
            
            SELECT @Radius = ScreenContent 
            FROM AppConfiguration 
            WHERE ConfigurationType = 'HotDeals Default Radius'
            
            --To get city and state of the POstalcode
			DECLARE @City varchar(30)
				  , @State char(2)
			IF @PostalCode IS NOT NULL AND @PopulationCentreID IS NULL
			BEGIN
					  SELECT @City = City, @State = State ,@Latitude =Latitude ,@Longitude =Longitude 
					  FROM GeoPosition 
					  WHERE PostalCode = @PostalCode 					  
	     
			--To get the population center of the postalcode.
			IF @City IS NOT NULL AND @State IS NOT NULL 		      
			BEGIN
			          DECLARE @PopulationCenter int  
					  SELECT @PopulationCenter = PopulationCenterID 
					  FROM PopulationCenterCities 
					  WHERE City = @City 
					  AND State = @State 
			END 
        --if population center is available then get all the daels of the DMA else get the vicinity of the lat and long
        IF @PopulationCenter IS NOT NULL
        BEGIN               
                  SET @PopulationCentreID = @PopulationCenter 
                  SET @Latitude = NULL
                  SET @Longitude = NULL 
            END         
            
            END
            --To get DMA's associated Cities.
        SELECT City,State
        INTO #TEMP
        FROM PopulationCenterCities 
        WHERE PopulationCenterID = @PopulationCentreID        
        
          
        --To capture hotdeals
        CREATE TABLE #HotDeal(Row_Num INT
                                                ,ProductHotDealID INT
                                                , HotDealName VARCHAR(300)
                                                , City VARCHAR(50)
                                                , Category VARCHAR(100)
                                                , APIPartnerID INT
                                                , APIPartnerName VARCHAR(50)
                                                , Price MONEY
                                                , SalePrice MONEY
                                                , HotDealShortDescription VARCHAR(2000)
                                                , HotDealImagePath VARCHAR(1000)
                                                , HotDealURL VARCHAR(1000)    
                                                --, Distance FLOAT
                                                , CategoryID int)  
            
            
         
        --To get the HotDeals for which Categories are not mapped, ie. CategoryID is null. 
        --These HotDeal's are grouped under a section called "Others".
            SELECT DISTINCT ProductHotDealID
                  , HotDealName
                  , MIN(City) City
                  , Category
                  , APIPartnerID
                  , APIPartnerName
                  , Price
                  , SalePrice
                  , HotDealShortDescription
                  , HotDealImagePath
                  , HotDealURL
                  --, Distance
                  , CategoryID categoryId             
            INTO #Others
            FROM (SELECT p.ProductHotDealID
                              , HotDealName
                              , 'Uncategorized' Category
                              , ISNULL(p.APIPartnerID,0) APIPartnerID
                              , APIPartnerName
                              , isnull(Price,0) Price
                              , SalePrice
                              , HotDealShortDescription
                              , HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
                                                                        CASE WHEN WebsiteSourceFlag = 1 THEN 
                                                                                        CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
                                                                                             +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
                                                                                             +HotDealImagePath  
                                                                                          ELSE @RetailerConfig  
                                                                                          +CONVERT(VARCHAR(30),P.RetailID)+'/'  
                                                                                          +HotDealImagePath  
                                                                                          END                                                         
                                                                  ELSE P.HotDealImagePath  
                                                                        END  
                                                            END 
                              , HotDealURL
                              , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                              , CategoryID categoryId
                              , PL.City
                              , PL.state
                              , PCC.PopulationCenterID
                  FROM ProductHotDeal P
                        INNER JOIN ProductHotDealLocation PL on PL.ProductHotDealID=P.ProductHotDealID AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM #ProductHotdeal)
                        LEFT JOIN PopulationCenterCities PCC ON PCC.City = PL.City AND PCC.State = PL.State
                        LEFT JOIN APIPartner A on A.APIPartnerID=P.APIPartnerID
                        LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State
                  WHERE CategoryID is null 
                        --  AND P.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
                        --AND ((LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP)))                        
                        --AND GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(P.HotDealEndDate, GETDATE() + 1)
                        AND ISNULL(P.HotDealStartDate,GETDATE()-1)<= GETDATE() AND ISNULL(P.HotDealEndDate,GETDATE()+1) >= GETDATE()
                        AND (@Category = '0' OR @Category = '-1')
                       -- AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM #ProductHotdeal)             
                        ) Others
            WHERE (((@Latitude IS NOT NULL AND @Longitude IS NOT NULL) AND Distance <= @Radius)
                    OR 
                    ((@Latitude IS NULL AND @Longitude IS NULL) AND 1=1))
                  AND ((@PopulationCentreID IS NOT NULL AND (PopulationCenterID = @PopulationCentreID))
                           OR
                      (@PopulationCentreID IS NULL AND 1=1))           
                  --To Filter Koala Hot Deals.
                  --AND APIPartnerID != @KoalaPartnerID
            GROUP BY ProductHotDealID
                  , HotDealName 
                  , Category
                  , APIPartnerID
                  , APIPartnerName
                  , Price
                  , SalePrice
                  , HotDealShortDescription
                  , HotDealImagePath
                  , HotDealURL
                  --, Distance
                  , CategoryID      
            ORDER BY APIPartnerName, Price              
            
            --To get the deals that are not associated to city.
            SELECT DISTINCT ProductHotDealID
                  , HotDealName
                  , Category
                  , City
                  , APIPartnerID
                  , APIPartnerName
                  , Price
                  , SalePrice
                  , HotDealShortDescription
                  , HotDealImagePath
                  , HotDealURL
                  --, Distance
                  , CategoryID                  
            INTO #NoCity
            FROM (
            SELECT DISTINCT p.ProductHotDealID
                              , HotDealName
                              , Category = ISNULL(C.ParentCategoryName,'Uncategorized')
                              , 'No City' City
                              , ISNULL(p.APIPartnerID,0) APIPartnerID
                              , APIPartnerName
                              , isnull(Price,0) Price
                              , SalePrice
                              , HotDealShortDescription
                              , HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
                                                                        CASE WHEN WebsiteSourceFlag = 1 THEN 
                                                                                        CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
                                                                                             +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
                                                                                             +HotDealImagePath  
                                                                                          ELSE @RetailerConfig  
                                                                                          +CONVERT(VARCHAR(30),P.RetailID)+'/'  
                                                                                          +HotDealImagePath  
                                                                                          END                                                         
                                                                  ELSE P.HotDealImagePath  
                                                                        END  
                                                            END 
                              , HotDealURL
                              --, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                              , P.CategoryID 
            FROM ProductHotDeal P
            LEFT JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
            --INNER JOIN UserCategory UC ON UC.CategoryID = P.CategoryID
            LEFT JOIN Category C ON P.CategoryID = C.CategoryID
            LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
            WHERE 
            --GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(P.HotDealEndDate, GETDATE() + 1)
            (ISNULL(P.HotDealStartDate,GETDATE()-1)<= GETDATE() AND ISNULL(P.HotDealEndDate,GETDATE()+1) >= GETDATE())
          --  AND P.HotDealName LIKE CASE WHEN @Search IS NULL THEN '%' ELSE '%'+@Search+'%' END
            AND (((@CategorySet = 1) AND P.CategoryID IN (SELECT CategoryID FROM UserCategory WHERE UserID = @UserID))
                     OR
                  (@CategorySet = 0 AND 1 = 1))
            AND PL.City IS NULL
            AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM #ProductHotdeal)) Deals    
            
            
            
            --To get the deals that are not associated to city & categories.
            SELECT DISTINCT ProductHotDealID
                  , HotDealName
                  , Category
                  , City
                  , APIPartnerID
                  , APIPartnerName
                  , Price
                  , SalePrice
                  , HotDealShortDescription
                  , HotDealImagePath
                  , HotDealURL
                  --, Distance
                  , CategoryID                  
            INTO #NoCityUnCategorized
            FROM (
            SELECT DISTINCT p.ProductHotDealID
                              , HotDealName
                              , 'Uncategorized' Category
                              , 'No City' City
                              , ISNULL(p.APIPartnerID,0) APIPartnerID
                              , APIPartnerName
                              , isnull(Price,0) Price
                              , SalePrice
                              , HotDealShortDescription
                              , HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
                                                                        CASE WHEN WebsiteSourceFlag = 1 THEN 
                                                                                        CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
                                                                                             +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
                                                                                             +HotDealImagePath  
                                                                                          ELSE @RetailerConfig  
                                                                                          +CONVERT(VARCHAR(30),P.RetailID)+'/'  
                                                                                          +HotDealImagePath  
                                                                                          END                                                         
                                                                  ELSE P.HotDealImagePath  
                                                                        END  
                                                            END 
                              , HotDealURL
                              --, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                              , P.CategoryID 
            FROM ProductHotDeal P
            --INNER JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
            --INNER JOIN UserCategory UC ON UC.CategoryID = P.CategoryID            
            LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
            LEFT JOIN APIPartner A ON A.APIPartnerID = P.APIPartnerID
            WHERE P.CategoryID IS NULL
            --GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(P.HotDealEndDate, GETDATE() + 1)
            AND (ISNULL(P.HotDealStartDate,GETDATE()-1)<= GETDATE() AND ISNULL(P.HotDealEndDate,GETDATE()+1) >= GETDATE())
          --  AND P.HotDealName LIKE CASE WHEN @Search IS NULL THEN '%' ELSE '%'+@Search+'%' END
            AND (((@CategorySet = 1) AND P.CategoryID IN (SELECT CategoryID FROM UserCategory WHERE UserID = @UserID))
                     OR
                  (@CategorySet = 0 AND 1 = 1)
                     OR
                    (@CategorySet = 1 AND P.CategoryID IS NULL))
            AND PL.City IS NULL
            AND (@Category = '0' OR @Category = '-1')
            AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM #ProductHotdeal)) Deals  
            
                  
        --To enable/disable "By Category" button.       
         IF EXISTS( SELECT 1 FROM Category C
                              INNER JOIN ProductHotDeal P on p.CategoryID=C.CategoryID
                              INNER JOIN UserCategory UC ON UC.CategoryID=C.CategoryID
                        WHERE 
                         --(GETDATE() BETWEEN ISNULL(P.HotDealStartDate, GETDATE() - 1) AND ISNULL(P.HotDealEndDate, GETDATE() + 1)) 
                         P.HotDealStartDate<= GETDATE() AND P.HotDealEndDate >= GETDATE()
                        AND UserID=@UserID)
            BEGIN
                  SET @ByCategoryFlag = 0 
            END
            ELSE 
                  SET @ByCategoryFlag = 1
                  
                        
        -- --If Coordinates exists fetching retailer list in the specified radius.    
        -- IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)    
                  --BEGIN
            
            --To get hot deals based on lat and long
            IF ISNULL(@PopulationCentreID,0) = 0 
            BEGIN          
                                                                          
                        --To get Hot Deal info
                        SELECT DISTINCT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price)
                                    , ProductHotDealID
                                    , HotDealName
                                    , MIN(City) City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL
                                    --, Distance
                                    , CategoryID categoryId
                        INTO #HD
                        FROM (SELECT HD.ProductHotDealID 
                                          , HD.HotDealName
                                          , ISNULL(C.ParentCategoryName, 'Uncategorized') AS Category
                                          , PL.City 
                                          , ISNULL(AP.APIPartnerID,0) APIPartnerID
                                          , AP.APIPartnerName 
                                          , isnull(Price,0) Price
                                          , SalePrice
                                          , HotDealShortDescription 
                                          , HotDealImagePath = CASE WHEN HD.HotDealImagePath IS NOT NULL THEN   
                                                                                                            CASE WHEN WebsiteSourceFlag = 1 THEN 
                                                                                                                              CASE WHEN HD.ManufacturerID IS NOT NULL THEN @ManufConfig  
                                                                                                                                 +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/'  
                                                                                                                                 +HotDealImagePath  
                                                                                                                              ELSE @RetailerConfig  
                                                                                                                              +CONVERT(VARCHAR(30),HD.RetailID)+'/'  
                                                                                                                              +HotDealImagePath  
                                                                                                                              END                                                    
                                                                                                            ELSE HD.HotDealImagePath  
                                                                                                            END  
                                                                        END 
                                          , HotDealURL
                                          , C.CategoryID categoryId
                                          , Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
                                FROM ProductHotDeal HD
                                INNER JOIN Category C ON C.CategoryID = HD.CategoryID 
                                INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID  AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM #ProductHotdeal)
                                INNER JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID 
                                LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State
                                WHERE -- HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
                                         -- AND
                                           (
                                                      ((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
                                                      OR 
                                                      ((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
                                                )
                                          AND (((@CategorySet = 1) AND HD.CategoryID IN(SELECT CategoryID FROM UserCategory WHERE UserID = @UserID))
                                                      OR
                                                ((@CategorySet = 0) AND 1 = 1))
                                      -- AND (
                                                --(LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP))
                                                
                                          --  )
                                      
                                       --AND GETDATE() BETWEEN ISNULL(HD.HotDealStartDate, GETDATE() - 1) AND ISNULL(HD.HotDealEndDate, GETDATE() + 1)
                                       AND ISNULL(HD.HotDealStartDate,GETDATE()-1)<= GETDATE() AND ISNULL(HD.HotDealEndDate,GETDATE()+1) >= GETDATE()
                                ) HotDeal
                        WHERE (((@Latitude IS NOT NULL AND @Longitude IS NOT NULL) AND Distance <= @Radius)
                                    OR 
                                ((@Latitude IS NULL AND @Longitude IS NULL) AND 1=1))
                                --To Filter Koala Hot Deals.
                                    --AND APIPartnerID != @KoalaPartnerID
                        GROUP BY ProductHotDealID
                                    , HotDealName 
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL
                                    --, Distance
                                    , CategoryID                  
                        ORDER BY Category, APIPartnerName, Price
                        
                        IF ISNULL(@Category,0) <> '0' AND ISNULL(@Category,0) <> '-1'
                        BEGIN
                              print 'Lat Long, Category'
                              INSERT INTO #HotDeal (Row_Num  
                                                            ,ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            , HotDealShortDescription  
                                                            , HotDealImagePath  
                                                            , HotDealURL      
                                                            --, Distance  
                                                            , CategoryID)
                              SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price )
                                          ,ProductHotDealID
                                          , HotDealName
                                          , City
                                          , Category
                                          , APIPartnerID
                                          , APIPartnerName
                                          , Price
                                          , SalePrice
                                          , HotDealShortDescription
                                          , HotDealImagePath
                                          , HotDealURL                                    
                                          --, Distance 
                                          , CategoryID categoryId
                              FROM (
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                          FROM #HD                                              
                                          UNION                                                 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                          FROM #NoCity N
                                          INNER JOIN [dbo].fn_SplitParam(@Category, ',')C ON C.Param = N.CategoryID) HD
                              
                        END                          
                                                     
                        IF ISNULL(@Category,0) = '0'
                        BEGIN
                              print 'Lat Long, No Category'
                              --To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
                              INSERT INTO #HotDeal (Row_Num  
                                                            ,ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            , HotDealShortDescription  
                                                            , HotDealImagePath  
                                                            , HotDealURL      
                                                            --, Distance  
                                                            , CategoryID)
                              SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
                                                                                    ,Category
                                                                                    , APIPartnerName
                                                                                    , Price )
                                    ,ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL                                    
                                    --, Distance 
                                    , CategoryID categoryId
                              FROM 
                                    (SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                , CategoryID categoryId
                                                --, Distance
                                          FROM #HD
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance
                                                , CategoryID categoryId
                                          FROM #Others
                                          UNION
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                          FROM #NoCity
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                          FROM #NoCityUnCategorized
                                    ) HotDeals 
                              ORDER BY Row_Num
                        END                           
                        IF ISNULL(@Category,0) = '-1'
                        BEGIN
                              print 'Uncategorised only'
                              --To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
                              INSERT INTO #HotDeal (Row_Num  
                                                            ,ProductHotDealID  
                                                            , HotDealName  
                                                            , City  
                                                            , Category  
                                                            , APIPartnerID  
                                                            , APIPartnerName  
                                                            , Price  
                                                            , SalePrice  
                                                            , HotDealShortDescription  
                                                            , HotDealImagePath  
                                                            , HotDealURL      
                                                            --, Distance  
                                                            , CategoryID)
                              SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
                                                                                    ,Category
                                                                                    , APIPartnerName
                                                                                    , Price )
                                    ,ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL                                    
                                    --, Distance 
                                    , CategoryID categoryId
                              FROM 
                                    (SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance
                                                , CategoryID categoryId
                                          FROM #Others                                         
                                          UNION 
                                          SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                          FROM #NoCityUnCategorized
                                    ) HotDeals 
                              ORDER BY Row_Num
                        END                      
                        --To capture max row number.
                        SELECT @MaxCnt = MAX(Row_Num) FROM #HotDeal
                        --this flag is a indicator to enable "More" button in the UI. 
                        --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
                        SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

                        SELECT  Row_Num  rowNumber
                                    , ProductHotDealID hotDealId
                                    , HotDealName hotDealName
                                    , City
                                    , Category categoryName
                                    , APIPartnerID apiPartnerId
                                    , APIPartnerName apiPartnerName
                                    , Price hDPrice
                                    , SalePrice hDSalePrice
                                    , HotDealShortDescription hDshortDescription
                                    , HotDealImagePath hotDealImagePath 
                                    , HotDealURL  hdURL
                                    --, Distance      distance
                                    , CategoryID categoryId
                        INTO #Hdeals
                        FROM #HotDeal 
                        WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit 
                        ORDER BY Row_Num
                                          
                        --User Tracking Section.                        
                        --Check if the search is happening on the Hotdeal name.
                        --IF @Search IS NOT NULL
                        --BEGIN
                        
                        --      INSERT INTO ScanSeeReportingDatabase..HotDealSearch(MainMenuID
                        --                                                                                 ,SearchKeyword
                        --                                                                                 ,CreatedDate)
                                                      
                        --      VALUES(@MainMenuID 
                        --              ,@Search 
                        --              ,GETDATE())     
                        --      SELECT @SearchID = SCOPE_IDENTITY()                                                                            
                                                                                                   
                        --END
            
                  --Capture the impression of the hot deals.
                        CREATE Table #Temp3(HotDealListID Int
                                           ,ProductHotDealID Int)
                        
                        INSERT INTO ScanSeeReportingDatabase..HotDealList(MainMenuID
                                                                                                ,HotDealSearchID
                                                                                                ,HotDealByCityID
                                                                                                ,ProductHotDealID                                                                        
                                                                                                 ,CreatedDate)
                        
                        OUTPUT inserted.HotDealListID,inserted.ProductHotDealID INTO #Temp3(HotDealListID,ProductHotDealID)
                        
                        SELECT @MainMenuID 
                              ,@SearchID 
                              ,@HotDealBycityID 
                              ,hotDealId 
                              ,GETDATE()                          
                        FROM #Hdeals
                        
                        --Display the list along with the primary key of the tracking table.
                        SELECT rowNumber
                             , HotDealListID hotdealLstId
                              , hotDealId
                              , hotDealName
                              , City
                              , categoryName
                              , apiPartnerId
                              , apiPartnerName
                              , hDPrice
                              , hDSalePrice
                              , hDshortDescription
                              , hotDealImagePath     
                              , hdURL
                                    --, Distance      distance
                              , categoryId
                        FROM #Hdeals H
                        INNER JOIN #Temp3 T ON H.hotDealId = T.ProductHotDealID 
                        
                        --Display the hot deal categories that fall in the current search criteria.
                        SELECT DISTINCT Category,                 
                                                      ISNULL(STUFF(REPLACE(REPLACE((
                                                      SELECT DISTINCT  ',' + CAST(CategoryID AS VARCHAR(10)) CategoryID
                                                      FROM #HotDeal H
                                                      WHERE H.Category = H1.Category
                                                      FOR XML PATH('')), '<CategoryID>', ''), '</CategoryID>', ''), 1, 1, ''), '-1')catID                       
                        FROM #HotDeal H1
            
            END
            --To get hot deals based on Population Centre.
            
            IF ISNULL(@PopulationCentreID,0) <> 0
            BEGIN                
                   
                        
                  SELECT DISTINCT Row_Num = ROW_NUMBER() OVER(ORDER BY Category
                                                                                                      , APIPartnerName
                                                                                                      , Price )
                              , ProductHotDealID
                              , HotDealName
                              , City
                              , Category
                              , APIPartnerID
                              , APIPartnerName
                              , Price
                              , SalePrice
                              , HotDealShortDescription
                              , HotDealImagePath
                              , HotDealURL
                              --, Distance  
                              , CategoryID categoryId
                  INTO #PopulationCenterHD
                  FROM (
                              SELECT HD.ProductHotDealID
                                    , HotDealName
                                    , MIN(PL.City)City
                                    , C.ParentCategoryName Category
                                    , ISNULL(HD.APIPartnerID,0) APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath = CASE WHEN HD.HotDealImagePath IS NOT NULL THEN   
                                                                                                      CASE WHEN WebsiteSourceFlag = 1 THEN 
                                                                                                                        CASE WHEN HD.ManufacturerID IS NOT NULL THEN @ManufConfig  
                                                                                                                           +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/'  
                                                                                                                           +HotDealImagePath  
                                                                                                                        ELSE @RetailerConfig  
                                                                                                                        +CONVERT(VARCHAR(30),HD.RetailID)+'/'  
                                                                                                                        +HotDealImagePath  
                                                                                                                        END                                                    
                                                                                                      ELSE HD.HotDealImagePath  
                                                                                                      END  
                                                                  END 
                                    , HotDealURL
                                    --, NULL Distance
                                    , C.CategoryID categoryId
                              FROM ProductHotDeal HD                          
                              INNER JOIN Category C ON C.CategoryID = HD.CategoryID 
                              INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = HD.ProductHotDealID 
                              INNER JOIN PopulationCenterCities PCC ON PCC.City = PL.City AND PCC.State = PL.State
                              INNER JOIN PopulationCenters PC ON PC.PopulationCenterID = PCC.PopulationCenterID
                              INNER JOIN APIPartner A ON A.APIPartnerID = HD.APIPartnerID 
                              WHERE HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM #ProductHotdeal)
                                   -- AND HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
                                    --AND GETDATE() BETWEEN ISNULL(HD.HotDealStartDate, GETDATE() - 1) AND ISNULL(HD.HotDealEndDate, GETDATE() + 1)
                                    AND ISNULL(HD.HotDealStartDate,GETDATE())<= GETDATE() AND ISNULL(HD.HotDealEndDate,GETDATE()) >= GETDATE()
                                    AND (
                                                ((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
                                                OR 
                                                ((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
                                          )
                                    AND (((@CategorySet = 1) AND HD.CategoryID IN (SELECT CategoryID FROM UserCategory WHERE UserID = @UserID)
                                                OR
                                          (@CategorySet = 0) AND 1 = 1))
                                    AND PCC.PopulationCenterID = @PopulationCentreID
                                    --To Filter Koala Hot Deals.
                                    --AND HD.APIPartnerID != @KoalaPartnerID
                              GROUP BY HD.ProductHotDealID
                                  , WebsiteSourceFlag
                                  , ManufacturerID
                                  , RetailID
                                    , HotDealName
                                    , C.ParentCategoryName 
                                    , HD.APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealURL
                                    , C.CategoryID 
                                    , HotDealImagePath            
                        ) HotDeals              
                        
                        
                        
                        --If the user has selected then display only those deals with categories
                        IF ISNULL(@Category,0) <> '0' AND ISNULL(@Category,0) <> '-1'
                        BEGIN
                              print 'Population Center, Category'
                              INSERT INTO #HotDeal (Row_Num  
                                                                        ,ProductHotDealID  
                                                                        , HotDealName  
                                                                        , City  
                                                                        , Category  
                                                                        , APIPartnerID  
                                                                        , APIPartnerName  
                                                                        , Price  
                                                                        , SalePrice  
                                                                        , HotDealShortDescription  
                                                                        , HotDealImagePath  
                                                                        , HotDealURL      
                                                                        --, Distance  
                                                                        , CategoryID)
                              SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category
                                                                                                      , APIPartnerName
                                                                                                      , Price )
                                          , ProductHotDealID
                                          , HotDealName
                                          , City
                                          , Category
                                          , APIPartnerID
                                          , APIPartnerName
                                          , Price
                                          , SalePrice
                                          , HotDealShortDescription
                                          , HotDealImagePath
                                          , HotDealURL
                                          --, Distance  
                                          , categoryId
                              FROM (
                              SELECT   ProductHotDealID
                                          , HotDealName
                                          , City
                                          , Category
                                          , APIPartnerID
                                          , APIPartnerName
                                          , Price
                                          , SalePrice
                                          , HotDealShortDescription
                                          , HotDealImagePath
                                          , HotDealURL
                                          --, Distance  
                                          , CategoryID categoryId
                              FROM #PopulationCenterHD
                              
                              UNION
                              
                              SELECT ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL                                   
                                    --, Distance 
                                     , CategoryID categoryId
                              FROM #NoCity N
                              INNER JOIN [dbo].fn_SplitParam(@Category, ',')C ON C.Param = N.CategoryID)Deals                        
                              
                        END
                         
                                                               
                        --If no category is selected then display all the deals those under "others" section also.
                        IF ISNULL(@Category,0) = '0' 
                        BEGIN
                              print 'Population Center, No Category'
                              INSERT INTO #HotDeal (Row_Num  
                                                                  ,ProductHotDealID  
                                                                  , HotDealName  
                                                                  , City  
                                                                  , Category  
                                                                  , APIPartnerID  
                                                                  , APIPartnerName  
                                                                  , Price  
                                                                  , SalePrice  
                                                                  , HotDealShortDescription  
                                                                  , HotDealImagePath  
                                                                  , HotDealURL      
                                                                  --, Distance  
                                                                  , CategoryID)
                  
                              SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
                                                                                                            , Category
                                                                                                            , APIPartnerName
                                                                                                            , Price )
                                    ,ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL
                                    --, Distance  
                                    , CategoryID categoryId
                              FROM (SELECT ProductHotDealID  
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance  
                                                , categoryId
                                    FROM #PopulationCenterHD
                                    UNION 
                                     SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID 
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance
                                                , CategoryID categoryId
                                    FROM #Others
                                    UNION
                                    SELECT ProductHotDealID
                                          , HotDealName
                                          , City
                                          , Category
                                          , APIPartnerID
                                          , APIPartnerName
                                          , Price
                                          , SalePrice
                                          , HotDealShortDescription
                                          , HotDealImagePath
                                          , HotDealURL                                   
                                          --, Distance 
                                           , CategoryID categoryId
                                    FROM #NoCity
                                    UNION
                                    SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                    FROM #NoCityUnCategorized) HotDeal
                        END 
                        IF ISNULL(@Category,0) = '-1' 
                        BEGIN
                              print 'UnCategorized only'
                              INSERT INTO #HotDeal (Row_Num  
                                                                  ,ProductHotDealID  
                                                                  , HotDealName  
                                                                  , City  
                                                                  , Category  
                                                                  , APIPartnerID  
                                                                  , APIPartnerName  
                                                                  , Price  
                                                                  , SalePrice  
                                                                  , HotDealShortDescription  
                                                                  , HotDealImagePath  
                                                                  , HotDealURL      
                                                                  --, Distance  
                                                                  , CategoryID)
                  
                              SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
                                                                                                            , Category
                                                                                                            , APIPartnerName
                                                                                                            , Price )
                                    ,ProductHotDealID
                                    , HotDealName
                                    , City
                                    , Category
                                    , APIPartnerID
                                    , APIPartnerName
                                    , Price
                                    , SalePrice
                                    , HotDealShortDescription
                                    , HotDealImagePath
                                    , HotDealURL
                                    --, Distance  
                                    , CategoryID categoryId
                              FROM ( SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID 
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL
                                                --, Distance
                                                , CategoryID categoryId
                                    FROM #Others
                                    UNION                                    
                                    SELECT ProductHotDealID
                                                , HotDealName
                                                , City
                                                , Category
                                                , APIPartnerID
                                                , APIPartnerName
                                                , Price
                                                , SalePrice
                                                , HotDealShortDescription
                                                , HotDealImagePath
                                                , HotDealURL                                   
                                                --, Distance 
                                                 , CategoryID categoryId
                                    FROM #NoCityUnCategorized) HotDeal
                        END 
                        --To capture max row number.
                        SELECT @MaxCnt = MAX(Row_Num) FROM #HotDeal
                        
                        --this flag is a indicator to enable "More" button in the UI. 
                        --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
                        SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

                        SELECT Row_Num  rowNumber
                                    , ProductHotDealID
                                    , HotDealName hotDealName
                                    , City city
                                    , Isnull(Category, 'Others') categoryName
                                    , APIPartnerID apiPartnerId
                                    , APIPartnerName apiPartnerName
                                    , Price hDPrice
                                    , SalePrice hDSalePrice
                                    , HotDealShortDescription hDshortDescription
                                    , HotDealImagePath hotDealImagePath 
                                    , HotDealURL  hdURL
                                    --, Distance      distance
                                    , CategoryID categoryId
                        INTO #Hotdealslist
                        FROM #HotDeal
                        WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit 
                        ORDER BY Row_Num
                        
                        --User Tracking                                       
                        
                        --Check if the search is happening on the Hotdeal name.
                        --IF @Search IS NOT NULL
                        --BEGIN
                        
                        --      INSERT INTO ScanSeeReportingDatabase..HotDealSearch(MainMenuID
                        --                                                                                 ,SearchKeyword
                        --                                                                                 ,CreatedDate)
                                                      
                        --      VALUES(@MainMenuID 
                        --              ,@Search 
                        --              ,GETDATE())     
                        --      Select @SearchID = SCOPE_IDENTITY()                                                                            
                                                                                                   
                        --END
                        
                        --Capture the PopulationCenter ID whinh the user has selected.
                        IF @PopulationCentreID is Not Null
                        BEGIN
                              INSERT INTO ScanSeeReportingDatabase..HotDealByCity(MainMenuID
                                                                                                         ,DMAID
                                                                                                         ,CreatedDate)
                                                                                                         
                              Values(@MainMenuID 
                                      ,@PopulationCentreID 
                                      ,GETDATE())
                              Select @HotDealBycityID = SCOPE_IDENTITY()  
                        END
                        
                        --Capture the impression of the hot deals.
                        CREATE Table #Temp1(HotDealListID Int
                                           ,ProductHotDealID Int)
                        
                        INSERT INTO ScanSeeReportingDatabase..HotDealList(MainMenuID
                                                                                                ,HotDealSearchID
                                                                                                ,HotDealByCityID
                                                                                                ,ProductHotDealID                                                                        
                                                                                                 ,CreatedDate)
                        
                        OUTPUT inserted.HotDealListID,inserted.ProductHotDealID INTO #Temp1(HotDealListID,ProductHotDealID)
                        
                        SELECT @MainMenuID 
                              ,@SearchID 
                              ,@HotDealBycityID 
                              ,ProductHotDealID 
                              ,GETDATE()                          
                        FROM #Hotdealslist
                        
                        SELECT Row_Num  rowNumber
                                    , T.HotDealListID hotdealLstId
                                    , H.ProductHotDealID hotDealId
                                    , HotDealName hotDealName
                                    , City city
                                    , Isnull(Category, 'Others') categoryName
                                    , APIPartnerID apiPartnerId
                                    , APIPartnerName apiPartnerName
                                    , Price hDPrice
                                    , SalePrice hDSalePrice
                                    , HotDealShortDescription hDshortDescription
                                    , HotDealImagePath hotDealImagePath 
                                    , HotDealURL  hdURL
                                    --, Distance      distance
                                    , CategoryID categoryId
                        FROM #HotDeal H
                        INNER JOIN #Temp1 T ON H.ProductHotDealID = T.ProductHotDealID 
                        
                        
                        --Display the hot deal categories that fall in the current search criteria.
                        SELECT DISTINCT Category,                 
                                                      ISNULL(STUFF(REPLACE(REPLACE((
                                                      SELECT DISTINCT  ',' + CAST(CategoryID AS VARCHAR(10)) CategoryID
                                                      FROM #HotDeal H
                                                      WHERE H.Category = H1.Category
                                                      FOR XML PATH('')), '<CategoryID>', ''), '</CategoryID>', ''), 1, 1, ''), '-1')catID                       
                        FROM #HotDeal H1                    
                        
                
            END   
      END TRY
      
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebConsumerHotDealSearchPagination.'        
                  --- Execute retrieval of Error info.
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
            END;
            
      END CATCH;
END;


GO
