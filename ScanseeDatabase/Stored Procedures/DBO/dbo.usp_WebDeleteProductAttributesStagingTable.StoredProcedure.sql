USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDeleteProductAttributesStagingTable]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDeleteProductAttributesStagingTable
Purpose					: To refresh the Product Attributes staging table
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			25th May 2012			        Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDeleteProductAttributesStagingTable]
(

	--Input Input Parameter(s)--		
	  
	  @ManufacturerID int
	, @UserID int 
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DELETE FROM UploadManufacturerProductAttributes
			WHERE UserID = @UserID
			AND ManufacturerId = @ManufacturerID
		
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDeleteProductAttributesStagingTable.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
