USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MaintenanceRetailLocationCleanUp]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name				: usp_MaintenanceRetailLocationCleanUp
Purpose                             : To delete the RetailLocation & the associated data. 
Example                             : usp_MaintenanceRetailLocationCleanUp

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------- 
1.0               1st Oct 2012      Pavan Sharma K			Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MaintenanceRetailLocationCleanUp]
(
        
        @RetailLocationIDs varchar(max) --Comma Separated RetailLocationIDs
      --Output Variable 
      
      , @Status int output
      , @ErrorNumber int output
      , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
            BEGIN TRANSACTION
            
            SELECT Param RetailLocationID
            INTO #Retailer
            FROM dbo.fn_SplitParam (@RetailLocationIDs, ',')

            DELETE FROM CouponRetailer
            FROM CouponRetailer CR
            INNER JOIN #Retailer R ON CR.RetailLocationID = R.RetailLocationID
            
            DELETE FROM LoyaltyDeal
            FROM LoyaltyDeal LD
            INNER JOIN #Retailer R ON R.RetailLocationID = LD.RetailLocationID
            
            DELETE FROM ProductHotDealRetailLocation
            FROM ProductHotDealRetailLocation P
            INNER JOIN #Retailer R ON R.RetailLocationID = P.RetailLocationID
            
            DELETE FROM ProductUserHit
            FROM ProductUserHit P
            INNER JOIN #Retailer R ON R.RetailLocationID = P.RetailLocationID
            
            DELETE FROM QRRetailerCustomPageAssociation
            FROM QRRetailerCustomPageAssociation Q
            INNER JOIN #Retailer R ON Q.RetailLocationID = R.RetailLocationID
            
            DELETE FROM RetailLocationAdvertisement
            FROM RetailLocationAdvertisement RLA
            INNER JOIN #Retailer R ON R.RetailLocationID = RLA.RetailLocationID
            
            DELETE FROM RetailLocationBannerAd
            FROM RetailLocationBannerAd RL
            INNER JOIN #Retailer R ON RL.RetailLocationID = R.RetailLocationID
            
            DELETE FROM RetailLocationSplashAd
            FROM RetailLocationSplashAd RL
            INNER JOIN #Retailer R ON RL.RetailLocationID = R.RetailLocationID            
            
            DELETE FROM QRUserRetailerSaleNotification 
            FROM QRRetailerCustomPageAssociation Q
            INNER JOIN #Retailer R ON Q.RetailLocationID = R.RetailLocationID
            
            DELETE FROM RebateRetailer 
            FROM RebateRetailer RR      
            INNER JOIN #Retailer R ON RR.RetailLocationID = R.RetailLocationID
            
            DELETE FROM RetailContact 
            FROM RetailContact RC
            INNER JOIN #Retailer R ON RC.RetailLocationID = R.RetailLocationID
            
            DELETE FROM RetailLocationProduct 
            FROM RetailLocationProduct RLP
            INNER JOIN #Retailer R ON RLP.RetailLocationID = R.RetailLocationID
            
            DELETE FROM ScanHistory
            FROM ScanHistory S
            INNER JOIN #Retailer R ON R.RetailLocationID = S.RetailLocationID
            
            DELETE FROM UserCouponGallery
            FROM UserCouponGallery U
            INNER JOIN #Retailer R ON U.RetailLocationID = R.RetailLocationID
            
            DELETE FROM UserRebateGallery
            FROM UserRebateGallery UR
            INNER JOIN #Retailer R ON UR.RetailLocationID = R.RetailLocationID
            
            --This table is included to accomdate the RetailLocation if we include the user mapping with the RetailLocation level in future.
            DELETE FROM UserRetailer 
            FROM UserRetailer U
            INNER JOIN #Retailer R ON U.RetailLocationID = R.RetailLocationID
            
            --After deleting from all the child tables delete from the RetailLocation table.
            DELETE FROM RetailLocation
            FROM RetailLocation RL
            INNER JOIN #Retailer R ON R.RetailLocationID = RL.RetailLocationID 
            
            --Confirmation of Success.
             SELECT @Status = 0
             
          COMMIT TRANSACTION
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_MaintenanceRetailLocationCleanUp.'          
                  --- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
                  ROLLBACK TRANSACTION;
                  --Confirmation of failure.
                  SELECT @Status = 1
            END;
            
      END CATCH;
END;

GO
