USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_ProductDetails  
Purpose                             : To fetch Product Details.  
Example                             : usp_ProductDetails 2,1   
  
History  
Version           Date              Author                  Change Description  
---------------------------------------------------------------   
1.0               26th July 2011    SPAN Infotech India      Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_ProductDetails]  
(  
        @UserID int  
      , @ProductID int  
      , @RetailID int  
      , @ScanLongitude float    
   , @ScanLatitude float   
      --OutPut Variable  
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY  
      
       --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
        
   DECLARE @RetailLocationID int   
            DECLARE @Status int    
   -- If GPS is ON and the Scan Latitude and Longitude is passed, fetching Retailer location whose coordinates matches.    
   IF @RetailLocationID IS NULL AND (@ScanLatitude IS NOT NULL AND @ScanLongitude IS NOT NULL)    
   BEGIN    
      SELECT RL.RetailLocationID    
      INTO #RetailLocation    
      FROM RetailLocationProduct RLP     
    INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID     
      WHERE RLP.ProductID = @ProductID     
    AND (RL.RetailLocationLatitude = @ScanLatitude AND RL.RetailLocationLongitude = @ScanLongitude)    
    -- If Single Retail locations matched then capture.    
    DECLARE @RetailLoc int    
    SELECT @RetailLoc = CASE WHEN COUNT(RetailLocationID) = 1 THEN MIN(RetailLocationID) END    
    FROM #RetailLocation  
    END        
           EXEC usp_UserProductHit @UserID, @ProductID, @RetailLocationID, @Status = @Status output , @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
     
     
            -- To capture the existance of the Media for the specified product.  
            DECLARE @VideoFlag bit  
            DECLARE @AudioFlag bit  
            DECLARE @FileFlag bit  
            DECLARE @Coupon bit  
            DECLARE @Loyalty bit  
            DECLARE @Rebate bit  
            --SELECT @VideoFlag = SUM(CASE WHEN PT.ProductMediaType = 'Video Files' THEN 1 ELSE 0 END)  
            --      , @AudioFlag = SUM(CASE WHEN PT.ProductMediaType = 'Audio Files' THEN 1 ELSE 0 END)  
            --      , @FileFlag = SUM(CASE WHEN PT.ProductMediaType = 'Other Files' THEN 1 ELSE 0 END)  
            --FROM ProductMedia PM  
            --INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
            --WHERE PM.ProductID = @ProductID   
              
            --TOo get the count of CLR  
              
    --        IF ISNULL(@RetailID, 0) = 0  
    --        BEGIN  
    --SELECT @Coupon = COUNT(CouponID)  
    --FROM Coupon   
    --WHERE ProductID = @ProductID  
    -- AND CouponExpireDate >= GETDATE()  
       
    --SELECT @Loyalty = COUNT(LoyaltyDealID)  
    --FROM LoyaltyDeal L  
    --WHERE ProductID = @ProductID   
    -- AND LoyaltyDealExpireDate >= GETDATE()  
               
    --SELECT @Rebate = COUNT(R.RebateID)  
    --FROM Rebate R  
    -- INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID     
    --WHERE ProductID = @ProductID  
    -- AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate  
    --        END  
    --        ELSE  
    --        BEGIN  
    --SELECT @Coupon = COUNT(CouponID)  
    --FROM Coupon   
    --WHERE ProductID = @ProductID  
    -- AND RetailID = @RetailID   
    -- AND CouponExpireDate >= GETDATE()  
    --SELECT @Loyalty = COUNT(LoyaltyDealID)  
    --FROM LoyaltyDeal L  
    -- INNER JOIN RetailLocation RL ON RL.RetailLocationID = L.RetailLocationID  AND RL.RetailID = @RetailID   
    --WHERE ProductID = @ProductID   
    -- AND LoyaltyDealExpireDate >= GETDATE()  
               
    --SELECT @Rebate = COUNT(R.RebateID)  
    --FROM Rebate R  
    -- INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID     
    --WHERE ProductID = @ProductID  
    -- AND RetailID = @RetailID   
    -- AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate  
    --        END  
              
            --To fetch product info.  
              
            SELECT  P.ProductID   
                    , ProductName   
                    --, M.ManufName manufacturersName     
                   -- , ModelNumber modelNumber  
                    , ProductLongDescription 
                    , ProductShortDescription  
                    --, ProductExpirationDate productExpDate  
                    , imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END 
                    --, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																				--			THEN @Config
																				--			+'Images/manufacturer/'
																				--			+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																				--			+ProductMediaPath  ELSE @Config+ProductMediaPath 
																				--	  END   
                    --      ELSE ProductMediaPath END                    
                    --, SuggestedRetailPrice suggestedRetailPrice  
                    --, Weight productWeight  
                    --, WeightUnits   
                    --, ScanTypeID   
                   -- , VideoFlag = CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  
                    --, AudioFlag = CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  
                    --, FileFlag = CASE WHEN @FileFlag > 0 THEN 'Available' ELSE 'N/A' END  
                    --, couponFlag = CASE WHEN @Coupon > 0 THEN 'Available' ELSE 'N/A' END  
                    --, loyaltyFlag = CASE WHEN @Loyalty > 0 THEN 'Available' ELSE 'N/A' END  
                    --, rebateFlag = CASE WHEN @Rebate > 0 THEN 'Available' ELSE 'N/A' END  
                    , UR.Rating  
                    , Coupon.Coupon                     
                    --, P.WarrantyServiceInformation warrantyServiceInfo
     , Loyalty.Loyalty  
     , Rebate.Rebate   
   INTO #Product   
            FROM Product P  
                  --LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID  
                  LEFT JOIN UserRating UR ON UR.ProductID = P.ProductID AND UR.UserID = @UserID                                 
                  OUTER APPLY fn_CouponDetails(@ProductID, @RetailID) Coupon  
      OUTER APPLY fn_LoyaltyDetails(@ProductID, @RetailID) Loyalty  
      OUTER APPLY fn_RebateDetails(@ProductID, @RetailID) Rebate
      --LEFT JOIN ProductMedia PM ON P.ProductID=PM.ProductID
      --LEFT JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID AND ProductMediaType='Image Files'  
            WHERE P.ProductID = @ProductID   
                  AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
                    
                    
   SELECT   p.ProductID   
       , ProductName   
       --, manufacturersName     
       --, modelNumber  
       , ProductShortDescription
       , ProductLongDescription   
       --, productExpDate  
       , imagePath  
       --, ProductMediaPath
       --, suggestedRetailPrice  
       --, productWeight  
       --, WeightUnits   
       --, ScanTypeID   
       --, VideoFlag   
       --, AudioFlag   
       --, FileFlag   
       , Rating      
      --, warrantyServiceInfo 
       , CLRFlag = CASE WHEN (COUNT(p.Coupon)+COUNT(p.Loyalty)+COUNT(p.Rebate)) > 0 THEN 1 ELSE 0 END  
       , coupon_Status = CASE WHEN COUNT(p.Coupon) = 0 THEN 'Grey'   
           ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
             , loyalty_Status = CASE WHEN COUNT(p.Loyalty) = 0 THEN 'Grey'  
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
          END  
             , rebate_Status = CASE WHEN COUNT(p.Rebate) = 0 THEN 'Grey'  
             ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
       
        
   FROM #Product p  
        LEFT JOIN UserCouponGallery UC ON UC.UserID = @UserID AND UC.CouponID = p.Coupon  
        LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = @UserID AND UL.LoyaltyDealID = p.Loyalty  
        LEFT JOIN UserRebateGallery UR ON UR.UserID = @UserID AND UR.RebateID = p.Rebate   
      GROUP BY   p.ProductID   
       ,ProductName   
       --, manufacturersName     
       --, modelNumber  
       , ProductLongDescription 
       , ProductShortDescription  
       --, productExpDate  
       , imagePath 
       --, ProductMediaPath 
       --, suggestedRetailPrice  
       --, productWeight  
       --, WeightUnits   
       --, ScanTypeID   
       --, VideoFlag   
       --, AudioFlag   
       --, FileFlag   
       , Rating         
       --, warrantyServiceInfo  
        
        
        
        
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure <>.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;

GO
