USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchKoalaCategoryXRef]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchKoalaCategoryXRef
Purpose					: To update categoryid
Example					: usp_BatchKoalaCategoryXRef

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st Oct 2011	PadmaPriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchKoalaCategoryXRef]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
							
				UPDATE ProductHotDeal
				SET CategoryID=C.CategoryID
				FROM ProductHotDeal HD
				INNER JOIN APIKoalaData K ON K.ID=HD.SourceID 
				INNER JOIN KoalaCategoryXRef KX ON KX.KoalaCategory=K.Category
				INNER JOIN Category C ON C.ParentCategoryID=KX.ScanSeeParentCategoryID AND C.SubCategoryID=KX.ScanSeeSubCategoryID
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchKoalaCategoryXRef.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
