USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_PreferredRetailers]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_PreferredRetailers
Purpose					: To capture user's preferred retailers.
Example					: usp_PreferredRetailers 2, '1,2,3,4,5,6,7,8,9', '7/4/2011'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_PreferredRetailers]
(
	@UserID int
	, @RetailID varchar(max)
	, @DateAdded datetime
	
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			SELECT Param RetailID
			INTO #Retailer
			FROM fn_SplitParam(@RetailID, ',')
			
			--To Capture Preferred retailers.
			INSERT INTO [UserRetailPreference]
				   ([UserID]
				   ,[RetailID]
				   ,[Active]
				   ,[LoyaltyProgram]
				   ,[RetailFavorite]
				   ,[DateAdded])
			 SELECT @UserID 
					, R.RetailID 
					, 1
					, 0
					, 0
					, @DateAdded
			FROM #Retailer R
			LEFT JOIN UserRetailPreference UR ON UR.RetailID = R.RetailID AND UR.UserID = @UserID 
			WHERE UR.RetailID IS NULL
			
			--To disable unchecked Retailers.
			UPDATE [UserRetailPreference]
			SET Active = 0
			WHERE RetailID NOT IN (SELECT RetailID FROM #Retailer)
			
			--To enable rechecked Retailers
			UPDATE [UserRetailPreference]
			SET Active = 1
				, DateAdded = @DateAdded 
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)
			
			--Confirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_PreferredRetailers.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
