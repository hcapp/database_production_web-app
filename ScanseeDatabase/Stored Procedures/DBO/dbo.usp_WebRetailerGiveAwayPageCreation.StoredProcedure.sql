USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerGiveAwayPageCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebGiveAwayPageCreation1
Purpose					: To Create a retailer Give Away Page
Example					: usp_WebGiveAwayPageCreation1

History
Version		Date						Author				Change Description
------------------------------------------------------------------------------- 
1.0			29th April 2013				SPAN        		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerGiveAwayPageCreation]
(

	--Input Parameter(s)--
	 @RetailID int	
   , @PageTitle varchar(255)
   , @StartDate varchar(10)
   , @EndDate varchar(10)
   , @Image varchar(1000)  
   , @ShortDescription varchar(255)
   , @LongDescription varchar(1000)
   , @Rules varchar(1000)
   , @TermsandConditions varchar(2000)
   , @Quantity int
   	
	--Output Variable--	
	, @PageID int output   	 
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			 
			 DECLARE @QRTypeID int
			 SELECT @QRTypeID=QRTypeID FROM QRTypes 
			 WHERE QRTypeName = 'Give Away Page'
		 
			--Create a record in QRRetailerCustomPage
			 INSERT INTO QRRetailerCustomPage(QRTypeID
											, RetailID
											, Pagetitle
											, StartDate
											, EndDate
											, [Image]											
											, ShortDescription
											, LongDescription
											, DateCreated
											, Rules
											, [Terms and Conditions]
											, Quantity)
			 VALUES(  @QRTypeID 
					, @RetailID
					, @PageTitle
					, @StartDate
					, @EndDate
					, @Image										
					, @ShortDescription
					, @LongDescription
					, GETDATE()
					, @Rules 
					, @TermsandConditions
					, @Quantity)

		SELECT @PageID =SCOPE_IDENTITY()
			
		--Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAddPage.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
