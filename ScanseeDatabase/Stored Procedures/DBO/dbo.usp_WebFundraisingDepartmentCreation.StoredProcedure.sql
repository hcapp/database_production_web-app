USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFundraisingDepartmentCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebFundraisingDepartmentCreation
Purpose					: To Create fundraising Departments.
Example					: usp_WebFundraisingDepartmentCreation

History
Version		 Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			11/11/2014	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFundraisingDepartmentCreation]
(
	
	 --Input variable
	   @UserID Int	  
	 , @RetailID Int
	 , @HcDepartmentName Varchar(100)
	  
	--Output Variable
	, @HcFundraisingDepartmentID Int output	
	, @DuplicateFlag Bit output 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	BEGIN TRANSACTION           
											
			IF EXISTS(SELECT 1 FROM HcFundraisingDepartments WHERE FundraisingDepartmentName = @HcDepartmentName AND RetailID = @RetailID)
			BEGIN
				SET @DuplicateFlag = 1
			END
			ELSE
			BEGIN			
				INSERT INTO HcFundraisingDepartments(FundraisingDepartmentName
													 ,RetailID
													 ,CreatedUserID												
													 ,DateCreated)
												
											SELECT @HcDepartmentName
												 , @RetailID
												 , @UserID 			      
												 , GETDATE() 

				SELECT @HcFundraisingDepartmentID = SCOPE_IDENTITY()
				SET @DuplicateFlag = 0
		     END
										
			
			   --Confirmation of Success.
			   SELECT @Status = 0
			   COMMIT TRANSACTION		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebFundraisingDepartmentCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
