USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayRebate]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebDisplayRebate  
Purpose     : To the detailes of the input Rebate ID.  
Example     : usp_WebDisplayRebate  
  
History  
Version  Date       Author   Change Description  
-------------------------------------------------------------------------------   
1.0   20th December 2011    Pavan Sharma K Initial Version  
-------------------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebDisplayRebate]      
(  
  
 --Input Input Parameter(s)--  
   
   @RebateID int  
   
 --Output Variable--     
  
 , @ErrorNumber INT OUTPUT  
 , @ErrorMessage VARCHAR(1000) OUTPUT   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
 DECLARE @ProductIDs VARCHAR(1000)
 DECLARE @ProductNames VARCHAR(1000)
 DECLARE @RebateImagePath VARCHAR(100)
 
 
 SELECT @ProductIDs = COALESCE(@ProductIDs+',','')+ CAST(ProductID AS VARCHAR(100)) FROM RebateProduct WHERE RebateID = @RebateID 
 SELECT @ProductNames = COALESCE(@ProductNames+',','')+ CAST(P.ProductName AS VARCHAR(1000))
 FROM dbo.fn_SplitParam(@ProductIDs,',')F 
 INNER JOIN Product P ON F.Param = P.ProductID
 
 SELECT @RebateImagePath =  dbo.fn_RebateImage(@RebateID) 
  
  SELECT DISTINCT --R.RetailID as retailID  
         R.RebateAmount as rebAmount  
       , R.RebateName  as rebName  
       , R.RebateShortDescription as rebShortDescription  
       , R.RebateTermsConditions as rebTermCondtn        
       , CAST(R.RebateStartDate AS DATE) as rebStartDate  
       , CAST(R.RebateEndDate AS DATE) as rebEndDate  
       , CAST(R.RebateStartDate AS TIME) as rebStartTime  
       , CAST(R.RebateEndDate AS TIME) as rebEndTime  
       , R.RebateLongDescription  as rebLongDescription     
       , RR.RetailID as retailerID  
       , RR.RetailLocationID  as retailerLocID  
       , @ProductIDs productID  
       , @ProductNames productNames
       , @RebateImagePath imagePath
       , R.NoOfRebatesIssued as noOfrebatesIsued  
       , NoOfRebatesUsed = (SELECT COUNT(UserID) FROM UserRebateGallery WHERE RebateID = @RebateID)  
       , RebateTimeZoneID rebateTimeZoneId  
  FROM Rebate R     
  LEFT OUTER JOIN RebateRetailer RR ON R.RebateID = RR.RebateID  
  LEFT OUTER JOIN RebateProduct RP ON RR.RebateID = RP.RebateID  
  --INNER JOIN TimeZone T ON T.TimeZoneID = R.RebateTimeZoneID  
  WHERE R.RebateID = @RebateID  
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebDisplayRebate.'    
   --Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
    
  END;  
 END CATCH    
   
END;




GO
