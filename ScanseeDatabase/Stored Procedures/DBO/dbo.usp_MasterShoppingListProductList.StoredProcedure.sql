USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListProductList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
Stored Procedure name : usp_MasterShoppingListProductList      
Purpose     : To get Product and CLR info for the specified Retailer and Category.      
Example     : EXEC usp_MasterShoppingListProductList 2      
      
History      
Version  Date   Author   Change Description      
---------------------------------------------------------------       
1.0   7th June 2011 SPAN Infotech India Initial Version      
---------------------------------------------------------------      
*/      
      
CREATE PROCEDURE [dbo].[usp_MasterShoppingListProductList]      
(      
 @UserID int      
 , @UserRetailPreferenceID int      
 , @ParentCategoryID int      
 , @RetailID INT      
 , @LowerLimit int      
 , @ScreenName varchar(50)      
 --OutPut Variable      
 , @NxtPageFlag bit output      
 , @ErrorNumber int output      
 , @ErrorMessage varchar(1000) output      
)      
AS      
BEGIN      
      
 BEGIN TRY 
 
  --To get Media Server Configuration.  
  DECLARE @ManufConfig varchar(50)    
  SELECT @ManufConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'    
      
  --To get the row count for pagination.      
  DECLARE @UpperLimit int       
  SELECT @UpperLimit = @LowerLimit + ScreenContent       
  FROM AppConfiguration       
  WHERE ScreenName = @ScreenName AND Active = 1      
        
  --DECLARE @MaxCnt int      
  --DECLARE @CLR int      
        
  ----To get Product and CLR info      
  --SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY P.ProductName)      
  -- , UP.UserID      
  -- , UP.UserProductID        
  -- , P.ProductID       
  -- , ProductName  = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END      
  -- , CLR = dbo.fn_MSLCLRButtonDisplay(@UserID, P.ProductID)      
  --INTO #Product      
  --FROM UserProduct UP      
  -- INNER JOIN Product P ON P.ProductID = UP.ProductID       
  -- LEFT JOIN ProductCategory PC ON PC.ProductID = UP.ProductID       
  --WHERE UP.UserID = @UserID       
  -- AND ISNULL(UP.UserRetailPreferenceID, 0) = @UserRetailPreferenceID       
  -- AND ISNULL(PC.CategoryID, 0) = @CategoryID       
        
  ----To capture max row number.      
  --SELECT @MaxCnt = MAX(Row_Num) FROM #Product      
        
  ----this flag is a indicator to enable "More" button in the UI.       
  ----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button       
  --SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END      
        
  --SELECT Row_Num        
  --  , UserID      
  --  , UserProductID        
  --  , ProductID       
  --  , ProductName       
  --  , CLR      
  --FROM #Product       
  --WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit      
        
  DECLARE @MaxCnt int      
  --To get Product and CLR info      
  SELECT UP.UserID      
   , UP.UserProductID        
   , P.ProductID       
   , ProductName  = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END     
   , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END      
   , Coupon.Coupon       
   , Loyalty.Loyalty      
   , Rebate.Rebate  
   --, ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0  
   --          WHEN (up.TodayListtItem=1) THEN 1  
   --    END       
  INTO #Product      
  FROM UserProduct UP      
   INNER JOIN Product P ON P.ProductID = UP.ProductID       
   LEFT JOIN ProductCategory PC ON PC.ProductID = UP.ProductID      
   LEFT JOIN Category C ON C.CategoryID = PC.CategoryID        
   OUTER APPLY fn_CouponDetails(UP.ProductID, @RetailID) Coupon      
   OUTER APPLY fn_LoyaltyDetails(UP.ProductID, @RetailID) Loyalty      
   OUTER APPLY fn_RebateDetails(UP.ProductID, @RetailID) Rebate      
  WHERE UP.UserID = @UserID       
   AND ISNULL(UP.UserRetailPreferenceID, 0) = @UserRetailPreferenceID       
   AND ISNULL(C.ParentCategoryID, 0) = @ParentCategoryID      
   AND UP.MasterListItem = 1      
         
  SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY P.ProductName)      
   --, P.UserID      
   , UserProductID        
   , P.ProductID       
   , ProductName    
   , P.ProductImagePath  
   --, ButtonFlag       
   , coupon = CASE WHEN COUNT(P.Coupon) = 0 THEN 0 ELSE 1 END      
   , loyalty = CASE WHEN COUNT(P.Loyalty) = 0 THEN 0 ELSE 1 END      
   , rebate = CASE WHEN COUNT(P.Rebate) = 0 THEN 0 ELSE 1 END      
   , coupon_Status = CASE WHEN COUNT(P.Coupon) = 0 THEN 'Grey'       
           ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END      
         END      
   , loyalty_Status = CASE WHEN COUNT(P.Loyalty) = 0 THEN 'Grey'      
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END      
          END      
   , rebate_Status = CASE WHEN COUNT(P.Rebate) = 0 THEN 'Grey'      
           ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END      
         END      
   , CLRFlag = CASE WHEN (COUNT(P.Coupon)+COUNT(P.Loyalty)+COUNT(P.Rebate)) > 0 THEN 1 ELSE 0 END      
  INTO #Prod      
  FROM #Product P      
   LEFT JOIN UserCouponGallery UC ON UC.UserID = P.UserID AND UC.CouponID = P.Coupon      
   LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = P.UserID AND UL.LoyaltyDealID = P.Loyalty      
   LEFT JOIN UserRebateGallery UR ON UR.UserID = P.UserID AND UR.RebateID = P.Rebate       
  GROUP BY P.UserID      
   , UserProductID        
   , P.ProductID       
   , ProductName   
   , ProductImagePath  
   --, ButtonFlag       
          
  --To capture max row number.      
  SELECT @MaxCnt = MAX(Row_Num) FROM #Prod      
        
  ----this flag is a indicator to enable "More" button in the UI.       
  ----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button       
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END      
        
  SELECT Row_Num        
    --this flag is a indicator to enable "More" button in the UI.       
    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button       
    , Max_Row_Num = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END      
    --, UserID      
    , UserProductID        
    , ProductID       
    , ProductName     
    , ProductImagePath imagePath   
    --, ButtonFlag      
    --, coupon coupons      
    --, loyalty loyalties      
    --, rebate rebates      
    , coupon_Status       
    , loyalty_Status       
    , rebate_Status       
    , CLRFlag      
  FROM #Prod       
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit      
        
 END TRY      
       
 BEGIN CATCH      
       
  --Check whether the Transaction is uncommitable.      
  IF @@ERROR <> 0      
  BEGIN      
   PRINT 'Error occured in Stored Procedure usp_MasterShoppingListProductList.'        
   --- Execute retrieval of Error info.      
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output       
  END;      
         
 END CATCH;      
END;

GO
