USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerModuleDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  usp_WebConsumerModuleDisplay
Purpose                 :  To get the list of modules.
Example                 :  usp_WebConsumerModuleDisplay

History
Version      Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          27/5/2013		Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[usp_WebConsumerModuleDisplay]
(
     
      --Output Variable--                 
	  @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
               SELECT ModuleID
				    , ModuleName 
               FROM Module                 	 
            
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebConsumerModuleDisplay.'         
                  --Execute retrieval of Error info.
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
            END;
            
      END CATCH;
END;


GO
