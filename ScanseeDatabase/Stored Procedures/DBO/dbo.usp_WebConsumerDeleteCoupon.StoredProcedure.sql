USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerDeleteCoupon]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerDeleteCoupon
Purpose					: To delete Coupon from User Gallery
Example					: usp_WebConsumerDeleteCoupon

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd Aug 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerDeleteCoupon]
(
	  @CouponID int
	, @UserID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DELETE FROM UserCouponGallery
			WHERE UserID = @UserID AND CouponID = @CouponID 
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerDeleteCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [Version9].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
