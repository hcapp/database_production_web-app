USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerWishListRetailerInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerWishListRetailerInfo
Purpose					: 
Example					: usp_WebConsumerWishListRetailerInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16thAug2013     Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerWishListRetailerInfo]
(
	  @UserID int
	, @ProductID int
	--, @Latitude decimal(18,6)    
	--, @Longitude decimal(18,6)    
	--, @ZipCode varchar(10)    
	--, @Radius int
	
	--User Tracking Inputs
    , @MainMenuID int 
    , @AlertedProductID INT
	
	--Output Variable
	, @Status int output 	 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		   
		    DECLARE @Latitude decimal(18,6)    
			DECLARE @Longitude decimal(18,6)  
			DECLARE @ZipCode VARCHAR(10)
			DECLARE @Radius int
			
			 --Get the user configured postal code.
			 SELECT @ZipCode = PostalCode
			 FROM Users 
			 WHERE UserID = @UserId 
						    
			 SELECT @Latitude = Latitude     
			      , @Longitude = Longitude     
			 FROM GeoPosition     
			 WHERE PostalCode = @ZipCode  	 
		  
			 --If radius is not passed, getting user preferred radius from UserPreference table.    
			 IF @Radius IS NULL     
			 BEGIN     
				SELECT @Radius = LocaleRadius  
				FROM dbo.UserPreference WHERE UserID = @UserID    
			 END  		
		
		    CREATE TABLE #RetailerList(ProductID int
		                             , ProductName Varchar(2000)
									 , RetailLocationDealID int
									 , RetailID int 
									 , RetailLocationID int
									 , RetailName Varchar(255)
									 , Address1 Varchar(2000)
									 , City Varchar(255)
									 , State Varchar(10)
									 , PostalCode Varchar(10)
									 , Distance float
									 , SalePrice float 
									 , Price float)
					
	
			--If not location services 
			IF(@Latitude IS NULL AND @Longitude IS NULL AND @ZipCode IS NULL)
			BEGIN
			        INSERT INTO #RetailerList(ProductID 
			                                 , ProductName
											 , RetailLocationDealID 
											 , RetailID  
											 , RetailLocationID 
											 , RetailName 
											 , Address1 
											 , City 
											 , State 
											 , PostalCode 
											 , Distance 
											 , SalePrice 
											 , Price)
									 
					SELECT P.ProductID
					    , P.ProductName 
						, RD.RetailLocationDealID 
						, R.RetailID 
						, RL.RetailLocationID
						, R.RetailName 
						, RL.Address1 
						, RL.City 
						, RL.State 
						, RL.PostalCode 
						, NULL Distance 
						, RD.SalePrice 
						, RD.Price				
					FROM RetailLocationDeal RD
					INNER JOIN Product P ON P.ProductID =RD.ProductID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID = RD.RetailLocationID 
					INNER JOIN Retailer R ON R.RetailID = RL.RetailID 
					WHERE RD.ProductID = @ProductID 
					AND GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
			END
			
			ELSE
			BEGIN
				--While User search by Zipcode, coordinates are fetched from GeoPosition table.    
				IF (@Latitude IS NULL AND @Longitude IS NULL )    
				BEGIN    
					SELECT @Latitude = Latitude     
					  , @Longitude = Longitude     
					FROM GeoPosition     
					WHERE PostalCode = @ZipCode     
				END    
				--If radius is not passed, getting user preferred radius from UserPreference table.    
				IF @Radius IS NULL     
				BEGIN     
					SELECT @Radius = LocaleRadius  
					FROM dbo.UserPreference WHERE UserID = @UserID    
				END

			--To get user's locationwise retail store infomation
			
			INSERT INTO #RetailerList(ProductID
			                         ,ProductName 
									 ,RetailLocationDealID 
									 ,RetailID  
									 ,RetailLocationID 
									 ,RetailName 
									 ,Address1 
									 ,City 
									 ,State 
									 ,PostalCode 
									 ,Distance 
									 ,SalePrice 
									 ,Price)
									 			
			SELECT ProductID
			       ,ProductName
			       ,RetailLocationDealID
				   , RetailID  
				   , RetailLocationID
				   , RetailName retailerName
				   , Address1 retaileraddress1
				   , City 
				   , State 
				   , PostalCode 
				   , Distance
				   , SalePrice
				   , Price		       
		    FROM (		
					SELECT UP.ProductID
					    ,P.ProductName 
					    , RP.RetailLocationDealID
						, R.RetailID 
						, RL.RetailLocationID
						, R.RetailName 
						, RL.Address1 
						, RL.City 
						, RL.State 
						, RL.PostalCode 
						, Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214     
						, RP.SalePrice 
						, RP.Price 
					FROM UserProduct UP
					INNER JOIN RetailLocationDeal RP ON RP.ProductID = UP.ProductID 
					INNER JOIN Product P ON P.ProductID =RP.ProductID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID 
					INNER JOIN Retailer R ON R.RetailID = RL.RetailID 
					WHERE UP.UserID = @UserID 
						AND UP.WishListItem = 1
						AND UP.ProductID = @ProductID 
				 ) D
			WHERE Distance <= ISNULL(@Radius, 5)
			
			END
			----To get Retailer Info.
			-- SELECT RP.Price price
			--		, RP.SalePrice 
			--		, R.RetailName retailerName
			--		, RL.Address1 retaileraddress1
			--		, RL.City 
			--		, RL.State 
			--		, RL.PostalCode 
			-- FROM RetailLocationProduct RP
			--	INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID
			--	INNER JOIN Retailer r on r.RetailID = rl.RetailID  
			-- WHERE ProductID = @ProductID 
			--	AND GETDATE() BETWEEN SaleStartDate AND SaleEndDate 
			--	AND ISNULL(RP.SalePrice, 0) <> 0
			
			--To diabale PushNotification flag (If user view the sale info then the pushnotification should be disbale for the product of the user)	
			
			UPDATE UserProduct 
			SET PushNotifyFlag = 0 
			WHERE UserID = @UserID 
			AND ProductID = @ProductID
			
			
			--User Tracking		
			
		    UPDATE ScanSeeReportingDatabase..AlertedProducts SET AlertedProductClick = 1
		    WHERE AlertedProductID = @AlertedProductID
		    
		  
		  
				CREATE TABLE #Temp(SalesListID int,RetailLoactionDealID int, MainMenuID int)
				
				INSERT INTO ScanSeeReportingDatabase..SalesList(RetailLoactionDealID
															   ,MainMenuID
															   ,RetailLocationDealClick
															   ,CreatedDate
															   ,SalePrice )
				OUTPUT inserted.SaleListID,inserted.RetailLoactionDealID INTO #Temp(SalesListID,RetailLoactionDealID)
				
				SELECT RetailLocationDealID 
					  ,@MainMenuID
					  , 1
					  ,GETDATE()
					  ,SalePrice 
				FROM #RetailerList
				
				--Capture the impression & click on the Retailer location in the wish list since the flow ends there.
				INSERT INTO ScanSeeReportingDatabase..RetailerList(MainMenuID
																 , RetailID
																 , RetailLocationID
																 , RetailLocationClick
																 , CreatedDate)
														SELECT @MainMenuID
															 , RetailID
															 , RetailLocationID
															 , 1
															 , GETDATE()
														FROM #RetailerList RL
														
					
				SELECT T.SalesListID 
				    , ProductID		
				    , ProductName 		     
					, RetailID 
					, RetailLocationID
					, RetailName retailerName
					, Address1 retaileraddress1
					, City 
					, State 
					, PostalCode 
					, NULL Distance 
					, SalePrice 
					, Price
				FROM #RetailerList R
				INNER JOIN #Temp T ON R.RetailLocationDealID = T.RetailLoactionDealID 	
				
					 
		
		  --Confirmation of Success.
		  SELECT @Status = 0	
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerWishListRetailerInfo.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1			
		END;
		 
	END CATCH;
END;


GO
