USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ThisLocationFindNearByProductPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_ThisLocationFindNearByProductPagination  
Purpose     : To get Searched product information  
Example     : EXEC usp_ThisLocationFindNearByProductPagination 35, '38000391200',  '12347890', -73.99653, 40.750742, 0, '7/22/2011'  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   01 Nov 2011  SPAN Infotech India  Initial Version   
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_ThisLocationFindNearByProductPagination]  
(  
     
    @UserID int      
  , @Latitude decimal(18,6)      
  , @Longitude decimal(18,6)      
  , @ZipCode varchar(10)      
  , @Radius int      
  , @LowerLimit int    
  , @ScreenName varchar(50)    
  , @ProdSearch varchar(max)  
  --, @RetaiLocationID Int  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY 
 
     --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
   --To get the row count for pagination.  
    DECLARE @UpperLimit int   
    SELECT @UpperLimit = @LowerLimit + ScreenContent   
    FROM AppConfiguration   
    WHERE ScreenName = @ScreenName   
     AND ConfigurationType = 'Pagination'  
     AND Active = 1  
    DECLARE @MaxCnt int  
  
   --While User search by Zipcode, coordinates are fetched from GeoPosition table.      
      IF (@Latitude IS NULL AND @Longitude IS NULL )      
      BEGIN      
    SELECT @Latitude = Latitude       
      , @Longitude = Longitude       
    FROM GeoPosition       
    WHERE PostalCode = @ZipCode       
      END      
    --  --If radius is not passed, getting user preferred radius from UserPreference table.      
    --  IF @Radius IS NULL       
    --  BEGIN       
    --SELECT @Radius = LocaleRadius    
    --FROM dbo.UserPreference WHERE UserID = @UserID      
    --  END       
      
      --If Coordinates exists fetching retailer list in the specified radius.      
      IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)      
      BEGIN      
      
       SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Distance, RetailName)    
      , RetailID      
      , RetailName   
      , RetailLocationID   
      , Distance          
      INTO #Retail  
      FROM       
       (SELECT R.RetailID       
       , R.RetailName  
       , RetailLocationID  
       , Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214,1,1)        
     FROM Retailer R      
      INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID        
       )Retailer      
      WHERE Distance <= ISNULL(@Radius,0.0568181818)  
      
     
   --To get product details  
    SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY ProductName)  
       , ProductID   
       , ProductName  
       , ScanCode    
       , ManufName     
       , ModelNumber   
       , ProductLongDescription   
       , ProductExpirationDate   
       , ProductImagePath   
       , SuggestedRetailPrice   
       , Weight   
       , WeightUnits   
       , ScanTypeID  
    INTO #Product   
    FROM (SELECT  P.ProductID   
        , ProductName  
        , ScanCode    
        , M.ManufName     
        , ModelNumber   
        , ProductLongDescription   
        , ProductExpirationDate   
        , ProductImagePath   
        , SuggestedRetailPrice   
        , Weight   
        , WeightUnits   
        , ScanTypeID   
    FROM #Retail R  
        --INNER JOIN RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID  
     INNER JOIN RetailLocationProduct RP ON R.RetailLocationID=RP.RetailLocationID  
     --INNER JOIN Product P On P.ProductID=RP.ProductID  
     INNER JOIN   
        (  
        select MIN(productid) productid,productname,ScanCode,ModelNumber, ProductLongDescription,  
        ManufacturerID    
        , ProductExpirationDate  
        , ProductImagePath  =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END   
        , SuggestedRetailPrice   
        , Weight   
        , WeightUnits   
        , ScanTypeID  
        from Product   
        group by ProductName,ScanCode,ModelNumber          
        , ProductLongDescription   
        , ManufacturerID    
        , ProductExpirationDate   
        , ProductImagePath   
        , SuggestedRetailPrice   
        , Weight   
        , WeightUnits   
        , ScanTypeID  
        , WebsiteSourceFlag 
        ) as P   
        On P.ProductID=RP.ProductID    
     LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID  
    WHERE (ProductLongDescription LIKE '%' + @ProdSearch + '%'  
        OR  
       ScanCode LIKE @ProdSearch + '%'  
      OR   
       ProductName LIKE '%' + @ProdSearch + '%')  
     AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
     AND P.ProductID <> 0 ) prod  
    --GROUP BY   P.ProductID  
    --   , ProductName  
    --   , ScanCode    
    --   , M.ManufName     
    --   , ModelNumber   
    --   , ProductLongDescription   
    --   , ProductExpirationDate   
    --   , ProductImagePath   
    --   , SuggestedRetailPrice   
    --   , Weight   
    --   , WeightUnits   
    --   , ScanTypeID   
      
    --To capture max row number.  
    SELECT @MaxCnt = MAX(Row_Num) FROM #Product   
      
    --this flag is a indicator to enable "More" button in the UI.   
    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
         
    SELECT Row_Num  rowNumber  
       , ProductID   
       , ProductName  
       , ManufName     
       , ModelNumber   
       , ProductLongDescription   
       , ProductExpirationDate   
       , ProductImagePath imagePath   
       , SuggestedRetailPrice   
       , Weight   
       , WeightUnits      
    FROM #Product   
    WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit    
    ORDER BY ProductName  
  END  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_ThisLocationFindNearByProductPagination.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
