USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAnythingPageDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerAnythingPageCreation
Purpose					: Fetch the details of the input Anything Page.
Example					: usp_WebRetailerAnythingPageCreation

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			8th August 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAnythingPageDisplayDetails]
(

	--Input Parameter(s)--
	
	  @RetailId int
	, @PageID int
	
	--Output Variable--	  	
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
		   DECLARE @Config varchar(50)
		   DECLARE @RetailLocations varchar(max)
		   
		  --To get Media Server Configuration. 		     
		  SELECT @Config=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Retailer Media Server Configuration'  
		   
		  --To fetch the Associated Retail Locations.
		  SELECT @RetailLocations = COALESCE(@RetailLocations + ',', '') + CAST(QRA.RetailLocationID AS VARCHAR(10))
		  FROM QRRetailerCustomPage QR
		  INNER JOIN QRRetailerCustomPageAssociation QRA ON QR.QRRetailerCustomPageID = QRA.QRRetailerCustomPageID
		  WHERE QR.RetailID = @RetailId 
		  AND QR.QRRetailerCustomPageID = @PageID
		  
		  --Fetch the details of the input Anything Page.
			SELECT QR.Pagetitle
			     , imageName = [Image]
				 , ImagePath = (CASE WHEN QR.[Image] IS NOT NULL THEN @Config + CAST(@RetailId AS VARCHAR(10)) + '/' + QR.[Image] ELSE NULL END)  
				 , URL
				 , PageDescription
				 , ShortDescription
				 , LongDescription
				 , StartDate = CONVERT(DATE, StartDate) 
				 , StartTime = CONVERT(TIME, StartDate)
				 , EndDate = CONVERT(DATE, EndDate)
				 , EndTime = CONVERT(TIME, EndDate)
				 , (CASE WHEN QRM.MediaPath IS NOT NULL THEN @Config + CAST(@RetailId AS VARCHAR(10)) + '/' + QRM.MediaPath ELSE NULL END) fileName
				 , pdfFileName=(CASE WHEN QRM.MediaPath IS NOT NULL THEN QRM.MediaPath ELSE NULL END)
				 , imageIconID=CASE WHEN QRI.QRRetailerCustomPageIconID IS NOT NULL THEN QRI.QRRetailerCustomPageIconID ELSE 0 END 
				 , DefaultImageIcon = (CASE WHEN QR.QRRetailerCustomPageIconID IS NOT NULL THEN @Config + QRI.QRRetailerCustomPageIconImagePath ELSE NULL END)
				 , RetailLocations = @RetailLocations 
			FROM QRRetailerCustomPage QR
			INNER JOIN QRTypes QRT ON QRT.QRTypeID = QR.QRTypeID			
			LEFT OUTER JOIN QRRetailerCustomPageMedia QRM ON QR.QRRetailerCustomPageID = QRM.QRRetailerCustomPageID
			LEFT OUTER JOIN QRRetailerCustomPageIcons QRI ON QRI.QRRetailerCustomPageIconID = QR.QRRetailerCustomPageIconID
			WHERE QRT.QRTypeName = 'Anything Page'
			AND QR.RetailID = @RetailId
			AND QR.QRRetailerCustomPageID = @PageID
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAnythingPageCreation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
