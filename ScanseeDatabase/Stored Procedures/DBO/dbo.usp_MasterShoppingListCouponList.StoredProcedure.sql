USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListCouponList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListCouponList 
Purpose					: To list Coupon
Example					: usp_MasterShoppingListCouponList 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListCouponList]
(
	@ProductID int
	, @RetailID int
	, @UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)
		BEGIN
			SELECT C.CouponID 
				,CouponName
				,CouponDiscountType
				,CouponDiscountAmount
				,CouponDiscountPct
				,CouponShortDescription
				,CouponLongDescription
				,CouponDateAdded
				,CouponStartDate
				,CouponExpireDate
				,ViewableOnWeb
			FROM Coupon C 
			INNER JOIN CouponProduct CP ON c.CouponID=CP.CouponID 
			INNER JOIN CouponRetailer CR ON CR.CouponID=C.CouponID
			WHERE CP.ProductID = @ProductID 
			AND CR.RetailID = @RetailID 
			AND CouponExpireDate >= GETDATE()
			AND C.CouponID NOT IN (SELECT CouponID FROM UserCouponGallery WHERE UserID = @UserID)
		END
		IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS NULL OR @RetailID =0)
		BEGIN
			SELECT C.CouponID 
				,CouponName
				,CouponDiscountType
				,CouponDiscountAmount
				,CouponDiscountPct
				,CouponShortDescription
				,CouponLongDescription
				,CouponDateAdded
				,CouponStartDate
				,CouponExpireDate
				,ViewableOnWeb
			FROM Coupon C 
			INNER JOIN CouponProduct CP ON c.CouponID=CP.CouponID 
			WHERE CP.ProductID = @ProductID 
			AND CouponExpireDate >= GETDATE()
			AND C.CouponID NOT IN (SELECT CouponID FROM UserCouponGallery WHERE UserID = @UserID)
		END
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListCouponList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
