USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerProductSearchAddtoWishList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerProductSearchAddtoWishList  
Purpose               : To search for the product.  
Example               : usp_WebConsumerProductSearchAddtoWishList   
 
Version    Date              Author         Change Description  
---------------------------------------------------------------   
1.0        2th Aug 2013      Dhananjaya TR  Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerProductSearchAddtoWishList]  
(  
   @UserID int  
 , @ProductID varchar(max) 
 
 --User Tracking Inputs
 , @MainMenuID int
   
 --Output Variable   
 , @ProductExists Bit output
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
  
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
    
   SET @ProductExists=0
   
   -- To fetch Product which already exists for the user (Active and Inactive Products). 
      
	       IF EXISTS (Select 1 from UserProduct UP
				   INNER JOIN fn_SplitParam(@ProductID,',') F ON F.Param =UP.ProductID  
				   where UserID =@UserID AND WishListItem =1)
        	BEGIN
			  SET @ProductExists =1
			END    
		   
		   IF EXISTS (Select 1 from UserProduct UP
					  INNER JOIN fn_SplitParam(@ProductID,',') F ON F.Param =UP.ProductID   
					  where UserID =@UserID AND WishListItem =0)
		   BEGIN
				Update UserProduct SET WishListItem =1
				       ,WishListAddDate=GETDATE()
				FROM UserProduct UP
				INNER JOIN fn_SplitParam(@ProductID,',') F ON F.Param =UP.ProductID  
				WHERE UserID =@UserID
				AND WishListItem =0 
			
				
				 --User Tracking Section
				Update ScanSeeReportingDatabase..UserProduct SET WishListItem =1
				FROM ScanSeeReportingDatabase..UserProduct UP
				INNER JOIN fn_SplitParam(@ProductID,',') F ON F.Param =UP.ProductID  
				WHERE UserID =@UserID
				AND WishListItem =0 		
		   END
		   
		   IF NOT EXISTS (Select 1 from UserProduct UP
						  INNER JOIN fn_SplitParam(@ProductID,',') F ON F.Param =UP.ProductID  
						  where UserID =@UserID)
			   BEGIN
					INSERT INTO [UserProduct] ([UserID]  
											  ,[ProductID]  
											  ,[MasterListItem]  
											  ,WishListAddDate    
											  ,[WishListItem]  
											  ,[TodayListtItem]  
											  ,[ShopCartItem]
											  ,PushNotifyFlag )  
					SELECT @UserID  
						 , F.Param      
						 , 0  
						 , GETDATE()  
						 , 1 
						 , 0  
						 , 0
						 , 0
					 FROM fn_SplitParam(@ProductID,',') F
					 Where F.Param NOT IN (SELECT ProductID from UserProduct WHERE UserID =@UserID) 	  
						 
					 --User Tracking Section	 
					 INSERT INTO ScanSeeReportingDatabase..UserProduct(UserID
																	  ,ProductID	
																	  ,WishListMainMenuID 
																	  ,MasterListItem 
																	  ,WishListItem 
																	  ,TodayListtItem 														 																
																	  ,WishListAddDate 
																	  ,ShopCartItem 
																	  ,PushNotifyFlag)													 
															
					  SELECT   @userid 
		    				  ,F.Param    		 
							  ,@MainMenuID
							  ,0
							  ,1
							  ,0
							  ,GETDATE()
							  ,0
							  ,0
					  FROM fn_SplitParam(@ProductID,',') F
					  Where F.Param NOT IN (SELECT ProductID from ScanSeeReportingDatabase..UserProduct WHERE UserID =@UserID)	
					  	 
			  END  
        
    
   --Confirmation of Success.  
   SELECT @Status = 0  
  COMMIT TRANSACTION    
   
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerProductSearchAddtoWishList.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;


GO
