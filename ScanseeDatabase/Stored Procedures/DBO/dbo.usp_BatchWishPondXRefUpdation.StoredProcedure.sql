USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondXRefUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondXRefUpdation
Purpose					: To update X Ref table with ScanSee RetailID, RetailLocationID and ProductID
Example					: usp_BatchWishPondXRefUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Oct 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondXRefUpdation]
(
	
	--Output Variable 
	 @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
		BEGIN TRANSACTION
		
		DECLARE	@Status1 int,
					@ErrorNumber1 int,
					@ErrorMessage1 varchar(1000)

			EXEC [dbo].[usp_BatchWishPondProductCreation] @Status = @Status1 OUTPUT,@ErrorNumber = @ErrorNumber1 OUTPUT,@ErrorMessage = @ErrorMessage1 OUTPUT


			--To Update RetailID in WishPond Retailer XRef table.(Checked for exact match for the Retailer name)
			UPDATE APIWishPondRetailerXRef
			SET ScanSeeRetailID = R.RetailID
			FROM APIWishPondRetailerXRef WR
			INNER JOIN Retailer R ON LTRIM(RTRIM(R.RetailName)) = LTRIM(RTRIM(WR.WishPondRetailerName)) 
			WHERE ScanSeeRetailID IS NULL  
			AND WR.WishPondRetailerXRefID = WishPondRetailerXRefID
			--To Update RetailID in WishPond Retailer XRef table.(Checked for exact match for the RetailLocation, ie, Address, City, State)
			UPDATE APIWishPondRetailerXRef 
			SET ScanSeeRetailLocID = RL.RetailLocationID
			FROM APIWishPondRetailerXRef WR
			INNER JOIN Retailer R ON R.RetailName = WR.WishPondRetailerName
			INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID 
											AND LTRIM(RTRIM(RL.Address1)) = LTRIM(RTRIM(WR.WishPondAddress)) 
											AND LTRIM(RTRIM(RL.City)) = LTRIM(RTRIM(WR.WishPondCity))  
											AND LTRIM(RTRIM(RL.State)) = LTRIM(RTRIM(WR.WishPondState)) 
			WHERE ScanSeeRetailLocID IS NULL 
			AND WR.WishPondRetailerXRefID = WishPondRetailerXRefID
			--To Update ProductID in WishPond Product XRef table.(Checked for exact match for the Product name)
			UPDATE APIWishPondDealXRef  
			SET ScanSeeProductID = PROD.ProductID
			FROM 
			(
			SELECT WD.WishPondProductName
					, COUNT(WD.WishPondProductName) CNT
					, WD.WishPondDealXRefID
					, MIN(P.ProductID) ProductID
			FROM APIWishPondDealXRef WD
			INNER JOIN Product P ON LTRIM(RTRIM(P.ProductName)) = LTRIM(RTRIM(WD.WishPondProductName)) AND P.WishPondDealID IS NULL  
			GROUP BY WD.WishPondDealXRefID ,WD.WishPondProductName
			HAVING COUNT(WD.WishPondProductName) = 1
			)PROD
			WHERE PROD.WishPondDealXRefID = APIWishPondDealXRef.WishPondDealXRefID
			AND ScanSeeProductID IS NULL
			
			--UnMatch WishPond Products are created as a new product in Product Table, Those ProductIDs should be updated in Deal XREF Table.
			UPDATE APIWishPondDealXRef  
			SET ScanSeeProductID = PROD.ProductID
			FROM 
			(
			SELECT WD.WishPondProductName
					, COUNT(WD.WishPondProductName) CNT
					, WD.WishPondDealXRefID
					, MIN(P.ProductID) ProductID
			FROM APIWishPondDealXRef WD
			INNER JOIN Product P ON LTRIM(RTRIM(P.ProductName)) = LTRIM(RTRIM(WD.WishPondProductName))  AND P.WishPondDealID = WD.WishPondDealID 
			GROUP BY WD.WishPondDealXRefID ,WD.WishPondProductName
			HAVING COUNT(WD.WishPondProductName) = 1
			)PROD
			WHERE PROD.WishPondDealXRefID = APIWishPondDealXRef.WishPondDealXRefID
			AND ScanSeeProductID IS NULL
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondXRefUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
