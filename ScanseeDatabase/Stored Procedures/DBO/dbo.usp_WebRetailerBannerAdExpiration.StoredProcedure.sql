USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdExpiration]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerBannerAdExpiration
Purpose					: To Expire a Banner Ad.
Example					: usp_WebRetailerBannerAdExpiration

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdExpiration]
(
	--Input Variables
	  @RetailID int
	, @BannerAdID int
	
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 DECLARE @ADStartDate date
			 DECLARE @ADEndDate date
			 
			 SELECT @ADStartDate = StartDate
				  , @ADEndDate = EndDate
			 FROM AdvertisementBanner
			 WHERE AdvertisementBannerID = @BannerAdID
			 
			 --Check if the Ad is active and move the record to the history and then update the date to '01/01/1900'
			 IF (CONVERT(DATE,GETDATE()) >= @ADStartDate AND CONVERT(DATE,GETDATE()) <= ISNULL(@ADEndDate, DATEADD(DD,1,GETDATE())))
			 BEGIN
				INSERT INTO AdvertisementBannerHistory(AdvertisementBannerID
													 , BannerAdName
													 , StartDate
													 , EndDate
													 , DateCreated)
											SELECT AdvertisementBannerID
												 , BannerAdName
												 , StartDate
												 , EndDate
												 , GETDATE()
											FROM AdvertisementBanner
											WHERE AdvertisementBannerID = @BannerAdID
				
				--Update the date to the starting date in SQL Server.
				UPDATE AdvertisementBanner
				SET StartDate  = '01/01/1900'
				  , EndDate = '01/01/1900'
				WHERE AdvertisementBannerID = @BannerAdID
			 END
			
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdExpiration.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
