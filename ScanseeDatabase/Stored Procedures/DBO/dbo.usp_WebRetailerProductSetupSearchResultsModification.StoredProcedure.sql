USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProductSetupSearchResultsModification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerProductSetupSearchResultsModification
Purpose					: To Update the Attributes of a product in the search results of the retail location.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		1st January 2012  Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerProductSetupSearchResultsModification]
(
	
	--Input Variables
	  @RetailID int
	, @RetailLocationID varchar(1000)	
	, @ProductID int
	, @Price money
	, @SalePrice money
    , @Description varchar(1000)
    , @SaleStartDate datetime
    , @SaleEndDate datetime   	
	, @ApplyAll bit
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			--Check if the ApplyAll is selected in the UI.
			IF @ApplyAll = 1
			BEGIN
				SELECT @RetailLocationID = COALESCE(@RetailLocationID+',','') + CAST(RetailLocationID AS VARCHAR(10))
				FROM RetailLocation 
				WHERE RetailID = @RetailID AND StoreIdentification IS NOT NULL --and (Headquarters =1 OR Headquarters =0)
				AND Active = 1			
			END
			
			DECLARE @CurrentSalePrice Money
			DECLARE @RLPFlag BIT = 0
			SELECT @CurrentSalePrice=SalePrice  
			      ,@RLPFlag=CASE WHEN RetailLocationDealID  IS NOT NULL THEN 1 ELSE 0 END 
		    FROM RetailLocationDeal RL   
		    INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = RL.RetailLocationID
		    WHERE ProductID =@ProductID    
		    
			--To get all retaillocations even though no product is associated to it.
			SELECT ProductID
			     , Price
			     , SalePrice
			     , SaleStartDate
			     , SaleEndDate
			     , RetailLocationID
			     , RetailLocationProductDescription
			     , RetailLocationProductID
			     , [PARAM]
			INTO #TEMP1
			FROM 
				(SELECT RLP.ProductID
					 , RLP.Price
					 , RLP.SalePrice
					 , RLP.SaleStartDate
					 , RLP.SaleEndDate
					 , RLP.RetailLocationID
					 , RLP.RetailLocationProductDescription
					 , RLP.RetailLocationProductID
					 , R.[Param]					 
				FROM RetailLocationProduct RLP
				RIGHT OUTER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') R ON R.[Param] = RLP.RetailLocationID
				AND RLP.ProductID = @ProductID)Products
						
			--Insert into RetailLocation table only those RetailLocations that not there.
			INSERT INTO RetailLocationProduct 
			(
				ProductID
			  , Price
			  , SalePrice
		      , SaleStartDate
		      , SaleEndDate
			  , RetailLocationID
			  , RetailLocationProductDescription
			  , DateCreated
			)
			SELECT @ProductID
				 , @Price
				 , @SalePrice
				 , @SaleStartDate
				 , @SaleEndDate
				 , [Param]
				 , @Description	
				 , GETDATE()
			FROM #TEMP1 
			WHERE RetailLocationID IS NULL 
			
			--Cerate a record about the existing Details in the RetailLocationProductHistory table.
			INSERT INTO RetailLocationProductHistory (RetailLocationProductID
			                                        , OldPrice
			                                        , OldSalePrice
			                                        , [Description]
			                                        , DateCreated) 
			                                        
            SELECT RetailLocationProductID
                 , Price
                 , SalePrice
                 , RetailLocationProductDescription
                 , GETDATE()
            FROM #TEMP1
            WHERE RetailLocationID IS NOT NULL AND ProductID IS NOT NULL 		
            
              
			  
            --Update the Record if already exists in the RetailLocationProduct table.
            UPDATE RetailLocationProduct SET RetailLocationProductDescription = @Description
										   , Price = @Price
										   , SalePrice = @SalePrice
										   , SaleStartDate = @SaleStartDate
										   , SaleEndDate = @SaleEndDate
										   , DateUpdated = GETDATE()
		    WHERE RetailLocationID IN (SELECT RetailLocationID FROM #TEMP1 WHERE RetailLocationID IS NOT NULL)
		    AND ProductID = @ProductID
					 
			IF(@SaleEndDate IS NOT NULL AND @SaleEndDate IS NOT NULL) --Insert into RetailLocationDeal table if there exits a Sale Start & End Dates.
			BEGIN
				SELECT ProductID
			     , Price
			     , SalePrice
			     , SaleStartDate
			     , SaleEndDate
			     , RetailLocationID
			     , RetailLocationProductDescription			     
			     , [PARAM]
				INTO #TEMP2
				FROM 
					(SELECT RLD.ProductID
						 , RLD.Price
						 , RLD.SalePrice
						 , RLD.SaleStartDate
						 , RLD.SaleEndDate
						 , RLD.RetailLocationID
						 , RLD.RetailLocationProductDescription					  
						 , R.[Param]					 
					FROM RetailLocationDeal RLD
					RIGHT OUTER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') R ON R.[Param] = RLD.RetailLocationID
					AND RLD.ProductID = @ProductID)Products
					
				--Create a new Record if it is unique.
				INSERT INTO RetailLocationDeal(	 
												 RetailLocationID
												,ProductID
												,RetailLocationProductDescription											
												,Price
												,SalePrice
												,SaleStartDate
												,SaleEndDate
												,DateCreated												
											    ,APIPartnerID)
								 SELECT [Param]
									  , @ProductID
									  , @Description
									  , @Price
									  , @SalePrice
									  , @SaleStartDate
									  , @SaleEndDate
									  , GETDATE()
									  , (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName LIKE 'ScanSee')
								 FROM #TEMP2
								 WHERE RetailLocationID IS NULL
				   
				   --Update the Record if the association already exists.		 
                   UPDATE RetailLocationDeal SET RetailLocationProductDescription = @Description
										   , Price = @Price
										   , SalePrice = @SalePrice
										   , SaleStartDate = @SaleStartDate
										   , SaleEndDate = @SaleEndDate
										   , DateUpdated = GETDATE()
				   WHERE RetailLocationID IN (SELECT RetailLocationID FROM #TEMP2 WHERE RetailLocationID IS NOT NULL)
				   AND ProductID = @ProductID	
								 
			END		
			
			  --Check If coupon value is changed or not. 			    
		       IF (@SalePrice <>@CurrentSalePrice AND @RLPFlag=1 )
		       BEGIN	    
		    
					SELECT DISTINCT UserID
						  ,ProductID
						  ,DeviceID
						  ,RetailLocationDealID  	              
					INTO #Temp
					FROM (
					SELECT U.UserID
						  ,UP.ProductID
						  ,UD.DeviceID 
						  ,RLP.RetailLocationDealID     
						  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
						  ,LocaleRadius 
					from RetailLocationDeal RLP
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID  AND RLP.ProductID = @ProductID 
					INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = RL.RetailLocationID 
					INNER JOIN UserProduct UP ON UP.ProductID =RLP.ProductID AND WishListItem =1	
					INNER JOIN Users U ON U.UserID =UP.UserID 
					INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
					INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  
					WHERE RL.Active = 1	       
					)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
			        
						        
					--User pushnotification already exist then NotificationSent flag updating to 0
					UPDATE UserPushNotification SET NotificationSent=0
												   ,DateModified =GETDATE()			                                      
					FROM UserPushNotification UP
					INNER JOIN #Temp T ON UP.RetailLocationDealID=T.RetailLocationDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0
			      
		           --User pushnotification not exist then insert. 
				   INSERT INTO UserPushNotification(UserID
													,ProductID
													,DeviceID												
													,RetailLocationDealID  												
													,NotificationSent
													,DateCreated)
					SELECT T.UserID 
						  ,T.ProductID 
						  ,T.DeviceID 
						  ,T.RetailLocationDealID   
						  ,0
						  ,GETDATE()
					FROM #Temp T				
					LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.RetailLocationDealID =UP.RetailLocationDealID AND T.ProductID =UP.ProductID AND DiscountCreated=0
					WHERE UP.UserPushNotificationID IS NULL  
			  END 			 
			
			 IF (@RLPFlag=0 )
		       BEGIN	    
		    
					SELECT DISTINCT UserID
						  ,ProductID
						  ,DeviceID
						  ,RetailLocationDealID  	              
					INTO #Temp22
					FROM (
					SELECT U.UserID
						  ,UP.ProductID
						  ,UD.DeviceID 
						  ,RLP.RetailLocationDealID     
						  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
						  ,LocaleRadius 
					from RetailLocationDeal RLP
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID  AND RLP.ProductID =@ProductID 
					INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F on F.Param = RL.RetailLocationID
					INNER JOIN UserProduct UP ON UP.ProductID =RLP.ProductID AND WishListItem =1	
					INNER JOIN Users U ON U.UserID =UP.UserID 
					INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
					INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode
					WHERE RL.Active = 1  	       
					)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
			       
						        
					--User pushnotification already exist then NotificationSent flag updating to 0
					UPDATE UserPushNotification SET NotificationSent=0
												   ,DateModified =GETDATE()		
												   ,DiscountCreated=1	                                      
					FROM UserPushNotification UP
					INNER JOIN #Temp22 T ON UP.RetailLocationDealID=T.RetailLocationDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=1
			       
		           --User pushnotification not exist then insert. 
				   INSERT INTO UserPushNotification(UserID
													,ProductID
													,DeviceID												
													,RetailLocationDealID  												
													,NotificationSent
													,DateCreated
													,DiscountCreated)
					SELECT T.UserID 
						  ,T.ProductID 
						  ,T.DeviceID 
						  ,T.RetailLocationDealID   
						  ,0
						  ,GETDATE()
						  ,1
					FROM #Temp22 T				
					LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.RetailLocationDealID =UP.RetailLocationDealID AND T.ProductID =UP.ProductID AND DiscountCreated=1
					WHERE UP.UserPushNotificationID IS NULL  
			  END   
			
			   
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerProductSetupSearchResultsModification.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
