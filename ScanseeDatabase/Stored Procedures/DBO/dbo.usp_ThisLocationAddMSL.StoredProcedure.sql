USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ThisLocationAddMSL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ThisLocationAddMSL
Purpose					: To add a product to MSL from This Location
Example					: EXEC usp_ThisLocationAddMSL 
EXEC usp_MasterShoppingListSearchAddProduct 1, '5,6', '6/10/2011'
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4th August 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ThisLocationAddMSL]
(
	  @UserID int
	, @RetailID int
	, @ProductID int
	, @MasterListAddDate datetime
	
	--Output Variable
	, @Result int output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output

)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--If retailer is a Preferred Retailer for the user then get the UserRetailPreferenceID.
			DECLARE @UserRetailPreferenceID INT
			SELECT @UserRetailPreferenceID = UserRetailPreferenceID 
			FROM UserRetailPreference 
			WHERE UserID = @UserID 
				AND RetailID = @RetailID 
			--If product is already available in MSL, banner an alert msg(-1)
			IF EXISTS (SELECT 1 FROM UserProduct WHERE UserID = @UserID AND ProductID = @ProductID AND MasterListItem = 1)
			BEGIN
				SELECT @Result = -1
			END
			ELSE
			BEGIN
				INSERT INTO [UserProduct]
				   ([UserRetailPreferenceID]
				   ,[UserID]
				   ,[ProductID]
				   ,[MasterListItem]
				   ,[WishListItem]
				   ,[TodayListtItem]
				   ,[ShopCartItem]
				   ,[MasterListAddDate])
				SELECT @UserRetailPreferenceID
					, @UserID 
					, @ProductID 
					, 1
					, 0
					, 0
					, 0
					, @MasterListAddDate  
				
			END
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ThisLocationAddMSL.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
