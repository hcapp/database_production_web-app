USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerLocationSpecialOffersDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerLocationSpecialOffersDisplay
Purpose					: To List the Special Offers (Special Offer Page) for the given Retailer Location.
Example					: usp_RetailerLocationSpecialOffersDisplay

History
Version		Date			 Author			Change Description
--------------------------------------------------------------- 
1.0			18th May 2012	 SPAN  				1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerLocationSpecialOffersDisplay]
(
	  
	  @UserID int
	, @RetailID int    
	, @RetailLocationID int
    , @LowerLimit int
    , @ScreenName varchar(100)
    
   --User Tracking Inputs.
    , @RetailerListID int
    
	--Output Variable 
	, @Status int output     
	, @NxtPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		
				DECLARE @QRType INT
				DECLARE @Config VARCHAR(100)
				DECLARE @QRTypeCode INT			      
				DECLARE @UpperLimit int   
				DECLARE @MaxCnt int
				
			   --To get the row count for pagination.  	           
			   SELECT @UpperLimit = @LowerLimit + ScreenContent         
			   FROM AppConfiguration         
			   WHERE ScreenName = @ScreenName         
			   AND ConfigurationType = 'Pagination'        
			   AND Active = 1       
				
				--Retrieve the server configuration.
				SELECT @Config = ScreenContent 
				FROM AppConfiguration 
				WHERE ConfigurationType LIKE 'QR Code Configuration'
				AND Active = 1
				 
				--Capture the ID for Special Offers.
				SELECT @QRType = QRTypeID 
					 , @QRTypeCode = QRTypeCode
				FROM QRTypes WHERE QRTypeName LIKE 'Special Offer Page'	
				 
				
				SELECT DISTINCT RowNum = IDENTITY(int, 1, 1)
					 , QR.QRRetailerCustomPageID pageID
					 , QR.Pagetitle pageTitle
					 , QR.ShortDescription shortDescription
					 --, [pageLink] = CASE WHEN QR.URL IS NULL THEN @Config + CAST(@QRTypeCode AS VARCHAR(10)) + '.htm?key1=' + CAST(@RetailID AS VARCHAR(10)) +'&key2=' + CAST(@RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))
						--				 ELSE QR.URL END
					 , [pageLink] = @Config + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') as varchar(10)) + '.htm?retId=' + CAST(QRRCPA.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10)) + CASE WHEN QR.URL IS NOT NULL THEN '&EL=true' ELSE '&EL=false'	END
					 , ISNULL(SortOrder, 100000) SortOrder	
				INTO #SpecialOffers
				FROM QRRetailerCustomPage QR
				INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QR.QRRetailerCustomPageID
				WHERE QRTypeID = @QRType
				AND ((StartDate IS NULL AND EndDate IS NULL) OR (GETDATE() BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1)))
				AND QRRCPA.RetailID = @RetailID
				AND QRRCPA.RetailLocationID = @RetailLocationID
				ORDER BY SortOrder, QR.Pagetitle ASC
				
				--To capture max row number.        
				SELECT @MaxCnt = MAX(RowNum) FROM #SpecialOffers
				
				 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button         
				SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
				
				SELECT RowNum
					 , @RetailerListID RetailerListID
					 , pageID
					 , pageTitle
					 , shortDescription
					 , [pageLink]
					 , SortOrder
				INTO #SplOffers
				FROM #SpecialOffers
				WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit  
				
				--User Tracking.
				
				--Capture the impression of the Special offers.
				CREATE TABLE #Temp(SpecialsListID INT
								 , RetailerListID INT
								 , SpecialOfferID INT )
				
				INSERT INTO ScanSeeReportingDatabase..SpecialsList(RetailerListID
																 , SpecialOfferID
																 , CreatedDate)
								OUTPUT inserted.SpecialsListID, inserted.RetailerListID, inserted.SpecialOfferID INTO #Temp(SpecialsListID, RetailerListID, SpecialOfferID)
											SELECT @RetailerListID
												 , pageID
												 , GETDATE()
											FROM #SpecialOffers
				
				SELECT S.RowNum
					 , T.SpecialsListID
					 , S.RetailerListID retListID
					 , S.pageID
					 , S.pageTitle
					 , S.shortDescription
					 , S.pageLink
					 , S.SortOrder
				FROM #SplOffers S
				INNER JOIN #Temp T ON T.SpecialOfferID = S.pageID
				
			--Confirmation of Success.
			SELECT @Status = 0	
		COMMIT TRANSACTION		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_RetailerLocationSpecialOffersDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
