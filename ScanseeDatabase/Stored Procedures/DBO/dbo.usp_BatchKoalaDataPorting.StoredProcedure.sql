USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchKoalaDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchKoalaDataPorting
Purpose					: To move data from Stage Deal Map table to Production ProductHotDeal Table
Example					: usp_BatchKoalaDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th Aug 2011	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchKoalaDataPorting]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'SimpleRelevance'
			DECLARE @Koala TABLE([ProductHotDealID] int								
								,[RetailID] int
								,[APIPartnerID] int
								,[HotDealProvider] varchar(100)
								,[Price] money
								,[HotDealDiscountType] varchar(20)
								,[HotDealDiscountAmount] money
								,[HotDealDiscountPct] float
								,[SalePrice] money
								,[HotDealName] varchar(1000)
								,[HotDealShortDescription] varchar(2000)
								,[HotDeaLonglDescription] varchar(MAX)
								,[HotDealImagePath] varchar(1000)
								,[HotDealTermsConditions] varchar(1000)
								,[HotDealURL] varchar(2000)
								,[Expired] bit
								,[HotDealStartDate] datetime
								,[HotDealEndDate] datetime
								,[APIDeleteFlag] bit
								,[SourceID] varchar(100)
								,[Category] varchar(100)
								,[CreatedDate] datetime
								,[DateModified] datetime
								,[CategoryID] int)
								
			--INSERT INTO [ProductHotDealHistory]
			--		([ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,[APIDeleteFlag]
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--	    ,[DateModified]
			--	    ,[CategoryID])
			--SELECT [ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,1
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--		,[DateModified]
			--		,[CategoryID]
			--FROM
			--	(
			--		MERGE ProductHotDeal AS T
			--		USING APIKoalaData AS S
			--		ON (T.SourceID = S.ID) 
			--		WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
			--			THEN INSERT([SourceID]
			--							 ,[APIPartnerID]
			--							 ,[Price]
			--							 ,[SalePrice]
			--							 ,[HotDealName]
			--							 ,[HotDealShortDescription]
			--							 ,[HotDeaLonglDescription]
			--							 ,[HotDealImagePath]
			--							 ,[HotDealTermsConditions]
			--							 ,[HotDealURL]
			--							 ,[HotDealStartDate]
			--							 ,[HotDealEndDate]
			--							 ,[Category]
			--							 ,[CreatedDate]) 
			--					 VALUES(S.[ID]
			--							,@APIPartnerID
			--							,S.[Price]
			--							,S.[Value]
			--							,S.[Title]
			--							,S.[Highlights]
			--							,S.[Description]
			--							,S.[Image_url]
			--							,S.[Restrictions]
			--							,S.[Deal_url]
			--							,S.[Start_date]
			--							,S.[End_date]
			--							,S.[Category]
			--							,S.[CreatedDate])
			--		WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID
			--			THEN UPDATE SET T.[Price] = S.[Price]
			--							 ,T.[SalePrice] = S.[Value]
			--							 ,T.[HotDealName] = S.[Title]
			--							 ,T.[HotDealShortDescription] = S.[Highlights]
			--							 ,T.[HotDeaLonglDescription] = S.[Description]
			--							 ,T.[HotDealImagePath] = S.[Image_url]
			--							 ,T.[HotDealTermsConditions] = S.[Restrictions]
			--							 ,T.[HotDealURL] = S.[Deal_url]
			--							 ,T.[HotDealStartDate] = S.[Start_date]
			--							 ,T.[HotDealEndDate] = S.[End_date]
			--							 ,T.[Category] = S.[Category]
			--							 ,T.[DateModified] = S.[CreatedDate]
			--		WHEN NOT MATCHED BY SOURCE AND T.[APIPartnerID] = @APIPartnerID
			--			THEN DELETE 
			--		OUTPUT $action, Deleted.*
			--	) AS HotDeal
			--	(
			--		 [Action]
			--		,[ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--		,[DateModified]
			--		,[CategoryID]
			--	)
			--WHERE [Action] = 'DELETE'
			

			INSERT INTO @Koala
					([ProductHotDealID]
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
				    ,[DateModified]
				    ,[CategoryID])
			SELECT [ProductHotDealID]			
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,1
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM
				(
					MERGE ProductHotDeal AS T
					USING APIKoalaData AS S
					ON (T.SourceID = S.ID AND T.[APIPartnerID] = @APIPartnerID) 
					WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
						THEN INSERT([SourceID]
										 ,[APIPartnerID]
										 ,[Price]
										 ,[SalePrice]
										 ,[HotDealName]
										 ,[HotDealShortDescription]
										 ,[HotDeaLonglDescription]
										 ,[HotDealImagePath]
										 ,[HotDealTermsConditions]
										 ,[HotDealURL]
										 ,[HotDealStartDate]
										 ,[HotDealEndDate]
										 ,[Category]
										 ,[CreatedDate]) 
								 VALUES(S.[ID]
										,@APIPartnerID
										,S.[Value]
										,S.[Price]
										,S.[Title]
										,S.[Highlights]
										,S.[Description]
										,S.[Image_url]
										,S.[Restrictions]
										,S.[Deal_url]
										,S.[Start_date]
										,S.[End_date]
										,S.[Category]
										,S.[CreatedDate])
					WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID
						THEN UPDATE SET T.[Price] = S.[Value]
										 ,T.[SalePrice] = S.[Price]
										 ,T.[HotDealName] = S.[Title]
										 ,T.[HotDealShortDescription] = S.[Highlights]
										 ,T.[HotDeaLonglDescription] = S.[Description]
										 ,T.[HotDealImagePath] = S.[Image_url]
										 ,T.[HotDealTermsConditions] = S.[Restrictions]
										 ,T.[HotDealURL] = S.[Deal_url]
										 ,T.[HotDealStartDate] = S.[Start_date]
										 ,T.[HotDealEndDate] = S.[End_date]
										 ,T.[Category] = S.[Category]
										 ,T.[DateModified] = S.[CreatedDate]
					WHEN NOT MATCHED BY SOURCE AND T.[APIPartnerID] = @APIPartnerID
						THEN DELETE 
					OUTPUT $action, Deleted.*
				) AS HotDeal
				(
					 [Action]
					,[ProductHotDealID]					
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
					, ManufacturerID
					, HotDealTimeZoneID
					, WebsiteSourceFlag
				)
			WHERE [Action] = 'DELETE'
			
			
			INSERT INTO [ProductHotDealHistory]

					([ProductHotDealID]					
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
				    ,[DateModified]
				    ,[CategoryID])
			SELECT [ProductHotDealID]					
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM @Koala
			
			--To update Scansee categoryID in ProductHotDeal Table
			UPDATE ProductHotDeal 
			SET CategoryID = C.CategoryID 
			FROM APIKoalaData DM
			INNER JOIN ProductHotDeal HD ON HD.SourceID = DM.ID 
			INNER JOIN KoalaCategoryXRef DC ON DC.KoalaCategory = HD.Category 
			INNER JOIN Category C ON C.ParentCategoryID = DC.ScanSeeParentCategoryID  
									AND C.SubCategoryID = DC.ScanSeeSubCategoryID 
									
			--To delete hot deals from hot deal product table when the deals are moved from history
			
			DELETE from HotDealProduct
			from HotDealProduct H
			INNER JOIN @Koala K on H.ProductHotDealID=K.ProductHotDealID						
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchKoalaDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
