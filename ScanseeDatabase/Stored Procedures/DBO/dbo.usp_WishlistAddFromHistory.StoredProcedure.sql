USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishlistAddFromHistory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishlistAddFromHistory
Purpose					: To add products from wish list history to wish list
Example					: usp_WishlistAddFromHistory

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04 Sep 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishlistAddFromHistory]
(
	  @userproductid int
	, @userid int
	, @WishListAddDate datetime
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
	, @ProductExists bit output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		--To check the existance of the product in Wish List
		SELECT @ProductExists = WishListItem
		FROM UserProduct 
		WHERE UserProductID = @userproductid 
		
		--If product does not exist in User's Wish List
		IF @ProductExists = 0 
		BEGIN		
			--To add products from wish list history to wish list		
			UPDATE UserProduct
			SET WishListItem=1 , WishListAddDate= @WishListAddDate
			WHERE userproductid=@userproductid AND UserID=@userid  
		END
		--UPDATE UserProducthistory
		--SET WishListItem=0
		--FROM UserProduct u inner join userproducthistory up on u.UserProductID=up.userproductid 
		--WHERE up.WishListItem=1 and u.UserID=@userid
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishlistAddFromHistory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
