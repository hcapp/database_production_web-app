USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[AddRetailers_Script]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [AddRetailers_Script]
Purpose					: To add the retailers from the given excel.
Example					: [AddRetailers_Script]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Bindu T A           1.1
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AddRetailers_Script]
(
	  @HcHubcitiID INT

	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

BEGIN TRY 
	
	BEGIN TRANSACTION

	--To Update Phone Numbers if included special characters
	UPDATE AddRetailerLoad_Sample
	SET [Contact Phone] = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([Contact Phone],'-',''),'.',''),',',''),')',''),'(',''),' ','')

	UPDATE AddRetailerLoad_Sample
	SET [Retail Name] = LTRIM(RTRIM([Retail Name]))

	UPDATE AddRetailerLoad_Sample
	SET [Retail Name] = REPLACE(REPLACE(REPLACE([Retail Name], CHAR(13), ''), CHAR(10), ''),'’',''''),
	Address = REPLACE(REPLACE([Address], CHAR(13), ''), CHAR(10), '')

	-- To Move things into Archive table

	INSERT INTO AddRetailerLoad_archive  ([Retail Name],Address,City,State,[Postal Code],RetailLocationLatitude,RetailLocationLongitude,Website,[Contact Phone],DateProcessed)
	SELECT 
	[Retail Name],Address,City,State,[Postal Code],RetailLocationLatitude,RetailLocationLongitude,Website,[Contact Phone],DateProcessed
	FROM AddRetailerLoad

	TRUNCATE TABLE AddRetailerLoad

	-- TO Move data from Sample table to main table

	INSERT INTO AddRetailerLoad ([Retail Name],Address,City,State,[Postal Code],RetailLocationLatitude,RetailLocationLongitude,Website,[Contact Phone],DateProcessed)
	SELECT DISTINCT
	[Retail Name],Address,City,State,[Postal Code],RetailLocationLatitude,RetailLocationLongitude,Website,[Contact Phone],GETDATE()
	FROM AddRetailerLoad_sample
	
	-- Validation For Similar Sounding Retail Names and Special characters

	--SELECT *
	--INTO UnProcessedRetailers
	--FROM
	--(	
	--	SELECT R.RetailName	,A.[Retail Name],A.Address,A.City,A.State,A.[Postal Code],A.RetailLocationLatitude,A.RetailLocationLongitude,A.Website,A.[Contact Phone]
	--	FROM Retailer R
	--	INNER JOIN AddRetailerLoad A ON  SOUNDEX (R.RetailName)= SOUNDEX (A.[Retail Name])
	--	WHERE SOUNDEX (R.RetailName)= SOUNDEX (A.[Retail Name])

	--)A

	--PRINT 'A'
	
	--To Capture New Retailers from EXCEL
	SELECT * 
	INTO #NewRetailers1 FROM
	(
		SELECT A.*
		FROM AddRetailerLoad A
		LEFT JOIN Retailer R ON dbo.fn_StripCharacters_V1 (A.[Retail Name])  = dbo.fn_StripCharacters_V1 (R.RetailName ) 
		AND R.RetailerActive=1
		WHERE R.retailID IS NULL   

		--EXCEPT 

		--SELECT DISTINCT * FROM UnProcessedRetailers
	)P

	--PRINT 'B'

	SELECT DISTINCT * ,ROW_NUMBER() OVER (PARTITION BY A.[Retail Name] ORDER BY A.[Retail Name]) AS RN
	INTO #NewRetailers
	FROM #NewRetailers1 A

	--DROP TABLE #ExistingRetailers1
	--To Capture Existing Retailers from EXCEL
	SELECT  A.*
	INTO #ExistingRetailers1
	FROM AddRetailerLoad A
	INNER JOIN Retailer R ON dbo.fn_StripCharacters_V1 (A.[Retail Name])  = dbo.fn_StripCharacters_V1 (R.RetailName) 
	--INNER JOIN Retailer R ON A.[Retail Name]  = R.RetailName
	
	--DROP TABLE #ExistingRetailers
	SELECT DISTINCT * ,ROW_NUMBER() OVER (PARTITION BY A.[Retail Name],A.Address,A.city,A.State,A.[Postal Code],A.retailLocationLatitude,A.retailLocationLongitude,
	Website  ORDER BY A.[Retail Name]) AS RN
	INTO #ExistingRetailers2
	FROM #ExistingRetailers1 A

	SELECT * INTO #ExistingRetailers  FROM #ExistingRetailers2 WHERE RN=1
		
	ALTER TABLE #NewRetailers  ADD  RetailId INT
	ALTER TABLE #ExistingRetailers  ADD  RetailId INT

	--PRINT 'C'

	--Insert new Retailers


	CREATE TABLE #InsertNewRetailers(RetailID int, [Retail Name] varchar(1000))
	INSERT INTO Retailer(
	 RetailName
	,Address1
	,City
	,State
	,PostalCode
	,CountryID
	,DateCreated
	,RetailURL
	,CorporatePhoneNo
	,RetailerActive
	)
	OUTPUT inserted.RetailID, inserted.RetailName INTO #InsertNewRetailers(RetailID, [Retail Name])
		SELECT DISTINCT  Z.[Retail Name]  
				   , Z.[Address]
				   , Z.city
				   , Z.state
				   , Z.[Postal Code]
				   , 1
				   , GETDATE() 
				   , Z.Website
				   , CAST(Z.[Contact Phone] AS VARCHAR(10))
				   , 1
		FROM #NewRetailers Z  where RN=1
		--LEFT JOIN Retailer R ON Z.Retailname = R.RetailName  AND R.Address1 = Z.Address AND R.City = Z.city AND R.State = Z.state AND R.PostalCode = convert(varchar(15),Z.Postalcode )
		--WHERE  Z.RN = 1
		
	--PRINT 'C'
	

	UPDATE #NewRetailers
	SET RetailId= I.RetailId
	FROM #InsertNewRetailers I
	INNER JOIN #NewRetailers N ON dbo.fn_StripCharacters_V1 (I.[Retail Name]) = dbo.fn_StripCharacters_V1 (N.[Retail Name])

	UPDATE #ExistingRetailers
	SET RetailId= n.RetailID
	FROM #ExistingRetailers I
	INNER JOIN Retailer N ON dbo.fn_StripCharacters_V1 (I.[Retail Name]) = dbo.fn_StripCharacters_V1 (N.RetailName)


	
	--Insert new RetailLocations for New Retailers

	CREATE TABLE #InsertNewRetailerLocations (RetLocID INT, RetID INT,CONTACTPHONE VARCHAR(10))

	INSERT INTO RetailLocation(
	 RetailID
	,Headquarters
	,Address1
	,City
	,State
	,PostalCode
	,CountryID
	,RetailLocationLatitude
	,RetailLocationLongitude
	,DateCreated
	,RetailLocationURL
	,StoreIdentification
	)
	OUTPUT inserted.RetailLocationID, inserted.RetailID INTO #InsertNewRetailerLocations(RetLocID, RetID)
		SELECT  DISTINCT R.RetailID
				, 0
				, R.[Address] 
				, R.city
				, R.state
				, R.[Postal Code] 
				, 1
				, R.RetailLocationLatitude
				, R.RetailLocationLongitude
				, GETDATE()
				, R.Website  
				, 9999   
				FROM #NewRetailers R

	--Insert new RetailLocations for Existing Retailers

	       ;WITH AA AS(
			SELECT DISTINCT Z.RetailID, RL.RetailLocationID ,ROW_NUMBER() OVER (PARTITION BY Z.[Retail Name],Z.Address,Z.city,Z.State,Z.[Postal Code],Z.retailLocationLatitude,Z.retailLocationLongitude,
	        Website  ORDER BY Z.[Retail Name])  AS RN,[Contact Phone]
			FROM #ExistingRetailers Z
			left JOIN RetailLocation RL ON RL.Address1 = Z.Address AND RL.City = Z.city AND RL.State = Z.state AND RL.PostalCode = convert(varchar(15),Z.[Postal Code] )
			and RL.RetailID=z.retailid
			WHERE RL.RetailLocationID IS not  NULL and z.RN=1 AND ACTIVE=1)
              SELECT RetailID,RetailLocationID,[Contact Phone]  INTO #FINALUPDATE FROM AA WHERE RN=1

			

			   ;WITH AA AS(
			SELECT DISTINCT Z.RetailID
				, [Address] 
				, Z.city
				, Z.state
				, Z.[Postal code]
				, Z.RetailLocationLatitude
				, Z.RetailLocationLongitude
				, Z.Website  
				,ROW_NUMBER() OVER (PARTITION BY Z.[Retail Name],Z.Address,Z.city,Z.State,Z.[Postal Code],Z.retailLocationLatitude,Z.retailLocationLongitude,
	        Website  ORDER BY Z.[Retail Name])  AS RN,[Contact Phone]
			FROM #ExistingRetailers Z
			LEFT JOIN RetailLocation RL ON RL.Address1 = Z.Address AND RL.City = Z.city AND RL.State = Z.state AND RL.PostalCode = convert(varchar(15),Z.[Postal Code] )
			AND ACTIVE=1 and RL.RetailID=z.retailid
			WHERE RL.RetailLocationID IS   NULL   )
              SELECT RetailID,[Contact Phone]  INTO #FINALUPDATE1 
			  FROM AA WHERE RN=1

		   ;WITH AA AS(
			SELECT DISTINCT Z.RetailID, RL.RetailLocationID ,ROW_NUMBER() OVER (PARTITION BY Z.[Retail Name],Z.Address,Z.city,Z.State,Z.[Postal Code],Z.retailLocationLatitude,Z.retailLocationLongitude,
	        Website  ORDER BY Z.[Retail Name])  AS RN
			FROM #ExistingRetailers Z
			LEFT JOIN RetailLocation RL ON RL.Address1 = Z.Address AND RL.City = Z.city AND RL.State = Z.state AND RL.PostalCode = convert(varchar(15),Z.[Postal Code] )
			and RL.RetailID=z.retailid
			WHERE RL.RetailLocationID IS not  NULL and z.RN=1 AND ACTIVE=1)

			insert  into #InsertNewRetailerLocations( RetID,RetLocID)
			 SELECT RetailID,RetailLocationID FROM AA WHERE RN=1

	INSERT INTO RetailLocation(
	 RetailID
	,Headquarters
	,Address1
	,City
	,State
	,PostalCode
	,CountryID
	,RetailLocationLatitude
	,RetailLocationLongitude
	,DateCreated
	,RetailLocationURL
	,StoreIdentification
	,Active
	,Address4
	)
	OUTPUT inserted.RetailLocationID, inserted.RetailID,inserted.[Address4] INTO #InsertNewRetailerLocations(RetLocID, RetID,CONTACTPHONE)
		SELECT DISTINCT Z.RetailID
				, 0
				, [Address] 
				, Z.city
				, Z.state
				, Z.[Postal code]
				, 1
				, Z.RetailLocationLatitude
				, Z.RetailLocationLongitude
				, GETDATE()
				, Z.Website  
				, 9999   
				,1
				,CAST ([Contact Phone] AS VARCHAR(10))
				FROM #ExistingRetailers Z
				LEFT JOIN RetailLocation RL ON RL.Address1 = Z.Address AND RL.City = Z.city AND RL.State = Z.state AND RL.PostalCode = convert(varchar(15),Z.[Postal Code] )
				AND RL.Active=1 and RL.RetailID=z.retailid
				WHERE RL.RetailLocationID IS NULL  

			
	--To insert contacts for the new Retailers
		
	--		SELECT Row_Num = IDENTITY(INT,1,1),N.[Contact Phone],I.RetLocID
	--		INTO #InsertNewContacts 
	--		FROM #NewRetailers N 
	--		LEFT JOIN Contact C ON C.ContactPhone=N.[Contact Phone]
	--		INNER JOIN #InsertNewRetailerLocations I ON I.RetID = N.retailID
	--		WHERE C.ContactPhone IS NULL AND ISNULL(N.[Contact Phone],'')<>''

						
			
	--		CREATE TABLE #NewContactID (Row_Num INT IDENTITY(1,1),ContactID INT, ContactPhone VARCHAR(10))
	--		INSERT INTO Contact(ContactPhone
	--							,DateCreated
	--							,CreateUserID)
	--		OUTPUT inserted.ContactID, inserted.ContactPhone INTO #NewContactID (ContactID,ContactPhone)
	--		SELECT [Contact Phone]
	--			  ,GETDATE()
	--			  ,3		
	--		FROM #InsertNewContacts
			
	--INSERT INTO RetailContact(ContactID
	--						,RetailLocationID
	--						,ContactPhone)
	--		SELECT C.ContactID 
	--		      ,I.RetLocID
	--			  ,C.[ContactPhone]
	--		FROM #InsertNewContacts I
	--		INNER JOIN #NewContactID C ON C.Row_Num = I.Row_Num

	--Revised ONE
	
	

SELECT ROW_NUM=identity(INT,1,1),
			 RL.RetailLocationID,Z.[Contact Phone] --,Z.[Contact Name] 
			INTO #NewContacts1
			FROM AddRetailerLoad_Sample Z
			INNER JOIN Retailer R ON dbo.fn_StripCharacters_V1 (R.RetailName)= dbo.fn_StripCharacters_V1 (Z.[Retail Name]	)		
			INNER JOIN RetailLocation RL ON R.RetailID =RL.RetailID AND  LTRIM(RTRIM(RL.Address1)) =LTRIM(RTRIM(Z.[Address])) AND RL.City =Z.[City ] AND RL.State =Z.[State ] 
			INNER JOIN #InsertNewRetailerLocations T ON T.RetLocID =RL.RetailLocationID 
			WHERE Z.[Contact Phone] IS NOT NULL
			ORDER BY RetailLocationID  
			
			--SELECT * FROM #NewContacts1
			
			CREATE TABLE #TEMP2(ROW_NUM INT IDENTITY(1,1),ContactID int,Contactphone varchar(20))
			INSERT INTO Contact(ContactPhone
			                    --,ContactFirstName
								--,ContactLastname
								,DateCreated
								,CreateUserID)
			OUTPUT inserted.ContactID,inserted.ContactPhone INTO #TEMP2(ContactID,Contactphone)
			SELECT [contact Phone]
			   --   ,SUBSTRING([Contact Name],1,CHARINDEX(' ',[Contact Name])) 
				  --,SUBSTRING([Contact Name],CHARINDEX(' ',[Contact Name])+1,LEN([Contact Name]))
			      ,GETDATE()
				  ,3		
			FROM #NewContacts1 
			ORDER BY ROW_NUM  

			
			INSERT INTO RetailContact(ContactID
									,RetailLocationID
									,ContactPhone)
			SELECT T.ContactID 
			      ,N.RetailLocationID 
				  ,N.[Contact Phone]
			FROM #NewContacts1 N
			INNER JOIN #TEMP2 T ON T.ROW_NUM =N.ROW_NUM 
			ORDER BY T.ROW_NUM 


	--select  #InsertNewRetailerLocations.RetLocID,#NewRetailers.[Contact Phone] ,#NewRetailers.retailID  into #insertcontact
	-- from  #InsertNewRetailerLocations  inner join #NewRetailers  on #NewRetailers.retailID=#InsertNewRetailerLocations.RetID
	--union
	--select  DISTINCT #FINALUPDATE.RetailLocationID,#ExistingRetailers.[Contact Phone], #ExistingRetailers.retailID  
	--from  #FINALUPDATE  inner join #ExistingRetailers  on #ExistingRetailers.retailID=#FINALUPDATE.retailID
	--AND #FINALUPDATE.[Contact Phone]=#ExistingRetailers.[Contact Phone]
	--union
	--select  DISTINCT #InsertNewRetailerLocations.RetLocID,#ExistingRetailers.[Contact Phone], #ExistingRetailers.retailID  
	--from  #FINALUPDATE1  inner join #ExistingRetailers  on #ExistingRetailers.retailID=#FINALUPDATE1.retailID
	--INNER JOIN #InsertNewRetailerLocations  ON #InsertNewRetailerLocations.CONTACTPHONE=#ExistingRetailers.[Contact Phone]
	--AND #FINALUPDATE1.retailID=#InsertNewRetailerLocations.RetID
	--AND #ExistingRetailers.retailID=#InsertNewRetailerLocations.RetID
	


	--INSERT INTO Contact(ContactPhone
	--,DateCreated
	--,CreateUserID)
	--select DISTINCT [Contact Phone],getdate(),1 from #insertcontact
	--left join Contact  on Contact.ContactPhone=#insertcontact.[Contact Phone]
	--where  Contact.ContactID is null AND #insertcontact.[Contact Phone] IS NOT NULL

	--delete RetailContact  from RetailContact inner join #insertcontact on  RetailContact.RetailLocationID=#insertcontact.RetLocID
	--inner join Contact  on Contact.ContactPhone=#insertcontact.[Contact Phone]

	
	--; with aa as(
	--select Contact.ContactID,#insertcontact.RetLocID,#insertcontact.[Contact Phone],
	--row_number()  over( partition by #insertcontact.RetLocID,#insertcontact.[Contact Phone] order  by #insertcontact.RetLocID,#insertcontact.[Contact Phone]) rowno
	--from #insertcontact inner join Contact  on Contact.ContactPhone=#insertcontact.[Contact Phone])

	--select *  into #ins  from  aa  where  rowno=1

	--INSERT INTO RetailContact(ContactID
	--,RetailLocationID
	--,ContactPhone)
	--select  ContactID,RetLocID,[Contact Phone]  from #ins


	-- To insert into Retailer Association
	
	INSERT INTO HcRetailerAssociation (HcHubCitiID
									,RetailID
									,RetailLocationID
									,DateCreated
									,Associated)

			SELECT  @HcHubcitiID
				   ,RL.RetId
				   ,RL.RetLocID
				   ,GETDATE()
				   ,0
			FROM #InsertNewRetailerLocations RL  left  join HcRetailerAssociation  on  HcRetailerAssociation.RetailID=RL.RetId
			and RetailLocationID=RL.RetLocID 
			and HcHubCitiID=@HcHubcitiID
			where HcRetailerAssociation.HcRetailerAssociationID is  null

	--To update City state changes

			UPDATE Retailer 
			SET HccityID = C.HccityID
			FROM Retailer R
			INNER JOIN HcCity C on R.City = C.CityName
			WHERE R.HccityID IS NULL

            UPDATE Retailer 
			SET StateID = C.StateID
			FROM Retailer R
			INNER JOIN state C on R.State = C.Stateabbrevation
			WHERE R.StateID IS NULL

			UPDATE RetailLocation
			SET HccityID = C.HccityID
			FROM RetailLocation RL
			INNER JOIN HcCity C on RL.City = C.CityName
			WHERE RL.HcCityID IS NULL

            UPDATE RetailLocation 
			SET StateID = C.StateID
			FROM RetailLocation RL
			INNER JOIN state C on RL.State = C.Stateabbrevation
			WHERE RL.StateID IS NULL
	
	--To Update Longitude and Latitude for the Retailers 
				
			UPDATE RetailLocation 
			SET SINRetailLocationLatitude = SIN (RetailLocationLatitude/57.2958), 
			COSRetailLocationLatitude = COS (RetailLocationLatitude/57.2958), 
			COSRetailLocationLongitude = Cos (RetailLocationLongitude / 57.2958) 
			WHERE SINRetailLocationLatitude IS NULL

	--To Update The Corporate And HeadQuarters for RetailLocations

		SELECT DISTINCT retailid  INTO #temp  FROM retaillocation RL  
		WHERE  (RL.Headquarters = 1 OR RL.CorporateAndStore = 1) AND Active=1

		SELECT retailid  into #update  from retailer
		EXCEPT
		SELECT * FROM #temp
	
		;WITH aa AS(
		SELECT ROW_NUMBER() OVER(PARTITION BY  a.RetailID ORDER BY a.RetailID) AS rowno,a.*  
		FROM retaillocation  a  INNER JOIN #update  b ON a.RetailID=b.RetailID)
		SELECT * INTO #sc FROM aa  WHERE rowno=1

		UPDATE retaillocation SET CorporateAndStore=1  FROM retaillocation INNER JOIN #sc ON #sc.RetailID=retaillocation.RetailID
		AND #sc.retaillocationid=retaillocation.retaillocationid
		
		-- To Update the retailers having HeadQuarters/Corp&Store

		IF EXISTS (SELECT 1 FROM Retailer R INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID
		INNER JOIN #InsertNewRetailerLocations Z ON Z.RetID= R.RetailID AND Z.RetLocID = RL.RetailLocationID
		AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1) AND RL.Active = 1)
		BEGIN
			UPDATE Retailer 
			SET CorporatePhoneNo = C.ContactPhone ,
			Address1 = RL.Address1,
			City = RL.City,
			State = RL.State,
			PostalCode = RL.PostalCode
			FROM RetailLocation RL 
			INNER JOIN Retailer R ON R.retailID = RL.RetailID
			INNER JOIN #InsertNewRetailerLocations Z ON Z.RetID= R.RetailID AND Z.RetLocID = RL.RetailLocationID
			INNER JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
			INNER JOIN Contact C ON C.ContactID = RC.ContactID
			WHERE  (RL.Headquarters = 1 OR RL.CorporateAndStore = 1) AND RL.Active = 1
		END

		-- Processed Retailers

		SELECT distinct R.RetailID , RL.RetailLocationID, R.Retailname,RL.Address1,RL.City  ,RL.State ,Rl.PostalCode ,RL.RetailLocationLatitude,
		RL.RetailLocationLongitude,RL.RetailLocationURL,Contact.ContactPhone
		FROM Retailer R 
		INNER JOIN #InsertNewRetailerLocations I ON R.RetailID = I.RetID 
		INNER JOIN RetailLocation RL On R.RetailID = RL.RetailID AND I.RetID = RL.retailID AND RL.RetailLocationID = I.RetLocID
		inner JOIN AddRetailerLoad A ON dbo.fn_StripCharacters_V1 (REPLACE(A.[Retail Name],'’','''')) = dbo.fn_StripCharacters_V1 (REPLACE(R.RetailName,'’',''''))
		LEFT join RetailContact  on RetailContact.RetailLocationID=RL.RetailLocationID 
		and RetailContact.RetailLocationID=I.RetLocID
		LEFT join Contact  on Contact.ContactID=RetailContact.ContactID
		ORDER BY RetailLocationID,Contact.ContactPhone
	
		--SELECT *  FROM #InsertNewRetailerLocations


		SELECT [Retail Name] 
		INTO #PROCESSED  FROM #NewRetailers
		UNION
		SELECT [Retail Name]  FROM #ExistingRetailers
		
		SELECT *  FROM AddRetailerLoad  WHERE [Retail Name]  NOT IN(SELECT [Retail Name]  FROM #PROCESSED)

		TRUNCATE TABLE  AddRetailerLoad_Sample
				
	COMMIT TRANSACTION

END TRY

BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		
		ROLLBACK TRANSACTION
		PRINT 'Error occured in Stored Procedure [AddRetailers_Script].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
		--Confirmation of failure.
		SELECT @Status = 1
	END
		 
END CATCH

END
GO
