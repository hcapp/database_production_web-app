USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchHalfOffDepotDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchHalfOffDepotDataPorting
Purpose					: To move data from Stage Deal Map table to Production ProductHotDeal Table
Example					: usp_BatchHalfOffDepotDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15th Feb 2013	SPAN		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchHalfOffDepotDataPorting]
(
   ----Input variable.
	  @FileName VARCHAR(255)
	  
	--Output Variable ;
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
		DECLARE @Duplicates INT
		DECLARE @Mandatory INT
		DECLARE @Expired INT
		DECLARE @RowCount INT
		   	    
	     IF NOT EXISTS(SELECT 1 FROM APIHalfOffDepotData WHERE [PARTNER]  = 'Half Off Depot' AND PROVIDER = 'Half Off Depot')
	     BEGIN  
	       RAISERROR ('Present file Data is not related to Half Off Depot', 16,1);
         END
	     ELSE
	     BEGIN
			--Port the deals from the stage area to the hot deal table.
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'Half Off Depot'		
			
			UPDATE APIHalfOffDepotData SET [DISCOUNT AMOUNT]=[DISCOUNT AMOUNT]*100
			
			--Insert into Batch Log table			
			INSERT INTO APIBatchLog(APIPartnerID
								  , APIPartnerName
								  , APIRowCount
								  , ExecutionDate
								  , ProcessedFileName)
						SELECT @APIPartnerID
							 , 'Half Off Depot'
							 , COUNT(1)
							 , GETDATE()
							 , @FileName
						FROM APIHalfOffDepotData 
				
			--Delete the duplicate records in the stage area before moving to production.		
			;WITH CTE 
			(
				 PRICE
				,[DISCOUNT AMOUNT] 
				,[SALE PRICE]
				,NAME
				,[SHORT DESCRIPTION]
				,[LONG DESCRIPTION]
				,IMAGEURL
				,[TERMS AND CONDITIONS]
				,URL
				,STARTDATE
				,ENDDATE
				,CATEGORY
				,CITY
				,STATE	
				,RWNO
			)
			AS
			(
				SELECT PRICE
				      ,[DISCOUNT AMOUNT]
				      ,[SALE PRICE]
				      , NAME
				      ,[SHORT DESCRIPTION]
				      ,[LONG DESCRIPTION]
				      ,IMAGEURL
				      ,[TERMS AND CONDITIONS]
				      ,URL
				      ,STARTDATE
				      ,ENDDATE
				      ,CATEGORY
				      ,CITY
				      ,STATE
				      ,ROW_NUMBER() OVER (PARTITION BY NAME, [SHORT DESCRIPTION], [LONG DESCRIPTION], IMAGEURL ORDER BY NAME) AS RWNO 
				 FROM APIHalfOffDepotData 	
			) 
			DELETE FROM CTE WHERE RWNO > 1						
			SELECT @Duplicates = @@ROWCOUNT 
						
			--Deleting expired hot deals from stage table.			
			DELETE FROM APIHalfOffDepotData WHERE GETDATE()> ISNULL(ENDDATE,GETDATE()+1)
			SELECT @Expired  = @@ROWCOUNT 
			
			--Delete Hot Deals if Name and URL not provided.
			DELETE FROM APIHalfOffDepotData WHERE NAME IS NULL OR [SHORT DESCRIPTION] IS NULL OR [LONG DESCRIPTION] IS NULL OR IMAGEURL IS NULL
			SELECT @Mandatory = @@ROWCOUNT 
			
			--To delete the associated RetailLocation before deleting from the hot deals table.
				SELECT * 
				INTO #Temp
				FROM ProductHotDeal T
				WHERE CAST(T.HotDealEndDate AS DATE) <CAST(GETDATE()AS DATE)
				AND T.[APIPartnerID] = @APIPartnerID
				
				
				INSERT INTO [ProductHotDealHistory]
							([ProductHotDealID]					
							,[RetailID]
							,[APIPartnerID]
							,[HotDealProvider]
							,[Price]
							,[SalePrice]
							,[HotDealName]
							,[HotDealShortDescription]
							,[HotDeaLonglDescription]
							,[HotDealImagePath]
							,[HotDealTermsConditions]
							,[HotDealURL]
							,[HotDealStartDate]
							,[HotDealEndDate]
							,[Category]
							,[CreatedDate]
							,[DateModified])
					SELECT [ProductHotDealID]					
							,[RetailID]
							,[APIPartnerID]
							,[HotDealProvider]
							,[Price]
							,[SalePrice]
							,[HotDealName]
							,[HotDealShortDescription]
							,[HotDeaLonglDescription]
							,[HotDealImagePath]
							,[HotDealTermsConditions]
							,[HotDealURL]
							,[HotDealStartDate]
							,[HotDealEndDate]
							,[Category]
							,[CreatedDate]
							,[DateModified]
					FROM #Temp 	
					
				DELETE FROM ProductHotDeal 
				WHERE CAST(HotDealEndDate AS DATE) <CAST(GETDATE()AS DATE)
				AND [APIPartnerID] = @APIPartnerID
				
				
				
					MERGE ProductHotDeal AS T
					USING APIHalfOffDepotData AS S
					ON T.[APIPartnerID] = @APIPartnerID 
					AND T.HotDealName = S.Name
					AND ISNULL(T.[HotDealShortDescription], 0) = ISNULL(S.[Short Description], 0)
					AND ISNULL(T.HotDealURL, 0) = ISNULL(S.URL, 0)
					AND S.NAME IS NOT NULL
					
					WHEN NOT MATCHED BY TARGET  --AND T.[APIPartnerID] = @APIPartnerID  
						THEN INSERT([APIPartnerID]
										 ,[HotDealProvider]
										 ,[Price]	
										 ,[HotDealDiscountPct]					
										 ,[SalePrice]
										 ,[HotDealName]
										 ,[HotDealShortDescription]
										 ,[HotDeaLonglDescription]
										 ,[HotDealImagePath]
										 ,[HotDealTermsConditions]
										 ,[HotDealURL]
										 ,[HotDealStartDate]
										 ,[HotDealEndDate]
										 ,[Category]
										 ,[CreatedDate]) 
								VALUES(@APIPartnerID
										,S.[Provider]
										,S.[Price]
										,S.[Discount Amount]
										,S.[Sale Price]
										,S.[Name]
										,S.[Short Description]
										,S.[Long Description]
										,S.[ImageURL]
										,S.[Terms And Conditions]
										,S.[URL]
										,S.[StartDate]
										,S.[Enddate]
										,S.[Category]
										,Getdate())										
					WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID 
						THEN UPDATE SET T.[Price] = S.[Price]
										 ,T.[HotDealDiscountPct] = S.[Discount Amount]
										 ,T.[SalePrice] = S.[Sale Price]
										 ,T.[HotDealName] = S.[Name]
										 ,T.[HotDealShortDescription] = S.[Short Description]
										 ,T.[HotDeaLonglDescription] = S.[Long Description]
										 ,T.[HotDealImagePath] = S.[ImageURL]
										 ,T.[HotDealTermsConditions] = S.[Terms And Conditions]
										 ,T.[HotDealURL] = S.[URL]
										 ,T.[HotDealStartDate] = S.[StartDate]
										 ,T.[HotDealEndDate] = S.[EndDate]
										 ,T.[Category] = S.[Category]
										 ,T.[CreatedDate] = S.[Created Date]
										 ,T.[DateModified] = GETDATE();	
										 
					--Capture the number of records that got inserted or updated from the current file.
					SELECT @RowCount = @@ROWCOUNT 						
				
			
				--Update the log with the counts.
				UPDATE APIBatchLog 
				SET DuplicatesCount = @Duplicates
				  , ExpiredCount = @Expired
				  , MandatoryFieldsMissingCount = @Mandatory
				  , ProcessedRowCount = @RowCount
				WHERE ProcessedFileName = @FileName
				AND CONVERT(DATE, GETDATE()) = CONVERT(DATE, ExecutionDate) 
		  
	       END
	       
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_BatchHalfOffDepotDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
