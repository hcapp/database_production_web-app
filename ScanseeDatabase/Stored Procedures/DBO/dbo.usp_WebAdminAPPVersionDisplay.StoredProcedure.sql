USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminAPPVersionDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: [usp_WebAdminAPPVersionDisplay]  
Purpose					: Display all Version Details.  
Example					: [usp_WebAdminAPPVersionDisplay]  
  
History  
Version  Date				Author			 Change	Description  
---------------------------------------------------------------   
1.0		 5th Sep 2013   	Dhananjaya TR	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebAdminAPPVersionDisplay]  
(  
   @VersionNumber Varchar(100)  
 , @LowerLimit int   
 , @RecordCount int 
  
 --OutPut Variable
 , @MaxCnt int  output 
 , @NxtPageFlag bit output 
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN    
  BEGIN TRY  
	  DECLARE @Config Varchar(1000)
	  SELECT @Config=ScreenContent  
	  FROM AppConfiguration 
	  WHERE ScreenName like 'FTP URL' AND ConfigurationType like 'FTP Server Configuaration'  
	  
	  --To get the row count for pagination.  
	  DECLARE @UpperLimit int  		           
	  SELECT @UpperLimit = @LowerLimit + @RecordCount
	  
	  --Fetch all the version details.
	  SELECT Rownumber=ROW_NUMBER() OVER (ORDER BY QALastReleaseDate,ProductionLastReleaseDate DESC) 
			,APPVersionManagementID  
			,VersionNumber 
			,DatabaseSchemaName 
			,ServerBuild 
			,QAFirstReleaseDate 
			,QALastReleaseDate  
			,relQANotePath=CASE WHEN QAReleaseNotePath IS NOT NULL THEN @Config+'/'+QAReleaseNotePath ELSE QAReleaseNotePath END 
			,ProductionFirstReleaseDate 
			,ProductionLastReleaseDate 
			,relProdtnNotePath=CASE WHEN ProductionReleaseNotePath IS NOT NULL THEN @Config+'/'+ProductionReleaseNotePath ELSE ProductionReleaseNotePath END 
			,ITunesUploadDate
			,BuildApprovalDate
			,Type 
			,QAReleaseNoteFileName QAFileName
			,ProductionReleaseNoteFileName ProdFileName
			,reltypeID=CASE WHEN ((ITunesUploadDate IS NOT NULL AND ITunesUploadDate>ProductionLastReleaseDate AND ITunesUploadDate>QALastReleaseDate) OR
			      (BuildApprovalDate IS NOT NULL AND BuildApprovalDate>ProductionLastReleaseDate AND BuildApprovalDate>QALastReleaseDate)) THEN 3
			      WHEN ProductionLastReleaseDate IS NOT NULL AND ProductionLastReleaseDate>QALastReleaseDate THEN 2
			      WHEN QALastReleaseDate IS NOT NULL AND ISNULL(ProductionLastReleaseDate,QALastReleaseDate-1)<QALastReleaseDate THEN 1 END
	  INTO #Temp
	  FROM APPVersionManagement AV
	  INNER JOIN ApplicationType AT ON AT.ApplicationTypeID =AV.ApplicationTypeID 
	  WHERE ((@VersionNumber IS NOT NULL AND VersionNumber like @VersionNumber+'%') OR @VersionNumber IS NULL) 
	    
	  --To capture max row number.  
	  SELECT @MaxCnt = MAX(Rownumber) FROM #Temp 
	  
	  --this flag is a indicator to enable "More" button in the UI.   
	  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
	  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
	  
	  SELECT Rownumber
			,APPVersionManagementID  VersionID
			,VersionNumber appVersionNo
			,DatabaseSchemaName SchemaName
			,ServerBuild serverBuildNo
			,QAFirstReleaseDate firstQARelDate
			,QALastReleaseDate lastQARelDate
			,relQANotePath
			,ProductionFirstReleaseDate firstProdtnRelDate
			,ProductionLastReleaseDate lastProdtnRelDate
			,relProdtnNotePath
			,ITunesUploadDate
			,BuildApprovalDate
			,Type AppType
			,QAFileName
			,ProdFileName
			,reltypeID
	  FROM #Temp 
	  WHERE Rownumber Between (@LowerLimit+1) AND @UpperLimit 
       
  END TRY      
 BEGIN CATCH    
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebAdminAPPVersionDisplay].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
  END;  
     
 END CATCH;  
END;


GO
