USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerMasterShoppingListDeleteCoupon]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerMasterShoppingListDeleteCoupon
Purpose					: To delete Coupon from User Gallery
Example					: usp_WebConsumerMasterShoppingListDeleteCoupon

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd June 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerMasterShoppingListDeleteCoupon]
(
	  @CouponID varchar(max)
	, @UserID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			--DELETE FROM UserCouponGallery
			--WHERE UserID = @UserID AND CouponID = @CouponID 
			
			DELETE FROM UserCouponGallery
			FROM UserCouponGallery UC 
			INNER JOIN fn_SplitParam(@CouponID, ',') F ON F.Param = UC.CouponID
			AND UC.UserID = @UserID
			
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerMasterShoppingListDeleteCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
