USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealSearchPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_HotDealSearchPagination
Purpose					: To get the list of Hotdeals available.
Example					: usp_HotDealSearchPagination 1, 'o', 0,50

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			9th Aug 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HotDealSearchPagination]
(
	@UserID int
	--, @SortBy char(1)
	, @Category varchar(max)
	, @Search varchar(255)
	, @LowerLimit int
	, @ScreenName varchar(50)
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)    
	--, @ZipCode varchar(10)
	, @PopulationCentreID int  
	, @Radius int    
	
	--OutPut Variable
	, @MaxCnt int output 
	, @ByCategoryFlag bit output
	, @FavCatFlag bit output
	, @NxtPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	
	BEGIN TRY
	
		--To get Server Configuration
		 DECLARE @ManufConfig varchar(50) 
		 DECLARE @RetailerConfig varchar(50) 
		   
		 SELECT @ManufConfig=(SELECT ScreenContent  
							 FROM AppConfiguration   
							 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
			   ,@RetailerConfig=(SELECT ScreenContent  
								 FROM AppConfiguration   
								 WHERE ConfigurationType='Web Retailer Media Server Configuration')
		 FROM AppConfiguration  
	
		--To get the row count for pagination.
		DECLARE @UpperLimit int 
		SELECT @UpperLimit = @LowerLimit + ScreenContent 
		FROM AppConfiguration 
		WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1																			
		--DECLARE @MaxCnt int
		
		--To capture Koala APIPartnerID
		DECLARE @KoalaPartnerID int
		SELECT @KoalaPartnerID =  APIPartnerID  FROM APIPartner WHERE APIPartnerName = 'SimpleRelevance'
			
		
		--To get DMA's associated Cities.
        SELECT City,State
        INTO #TEMP
        FROM PopulationCenterCities 
        WHERE PopulationCenterID = @PopulationCentreID
        
        --To capture hotdeals
        DECLARE @HotDeal TABLE (Row_Num INT
								,ProductHotDealID INT
								, HotDealName VARCHAR(300)
								, City VARCHAR(50)
								, Category VARCHAR(100)
								, APIPartnerID INT
								, APIPartnerName VARCHAR(50)
								, Price MONEY
								, SalePrice MONEY
								, HotDealShortDescription VARCHAR(1000)
								, HotDealImagePath VARCHAR(1000)
								, HotDealURL VARCHAR(1000)	
								--, Distance FLOAT
								, CategoryID int)
        
	   
		
		----While User search by Zipcode, coordinates are fetched from GeoPosition table.    
	 --  IF (@Latitude IS NULL AND @Longitude IS NULL )    
	 --  BEGIN    
		--	SELECT @Latitude = Latitude     
		--	  , @Longitude = Longitude     
		--	FROM GeoPosition     
		--	WHERE PostalCode = @ZipCode     
	 --  END    
	   --If radius is not passed, getting user preferred radius from UserPreference table.    
	   IF @Radius IS NULL     
	   BEGIN     
			SELECT @Radius = CASE WHEN LocaleRadius = 0 THEN 5 ELSE LocaleRadius END
			FROM dbo.UserPreference WHERE UserID = @UserID    
	   END    
	  -- IF @Radius = 0
	  -- BEGIN
			--SET @Radius = 5
	  -- END
	   
	  --To get the HotDeals for which Categories are not mapped, ie. CategoryID is null. 
	  --These HotDeal's are grouped under a section called "Others".
		SELECT DISTINCT ProductHotDealID
			, HotDealName
			, MIN(City) City
			, Category
			, APIPartnerID
			, APIPartnerName
			, Price
			, SalePrice
			, HotDealShortDescription
			, HotDealImagePath
			, HotDealURL
			--, Distance
			, CategoryID categoryId			
		INTO #Others
		FROM (SELECT p.ProductHotDealID
					, HotDealName
					, 'Uncategorized' Category
					, ISNULL(p.APIPartnerID,0) APIPartnerID
					, APIPartnerName
					, isnull(Price,0) Price
					, SalePrice
					, HotDealShortDescription
					, HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
												CASE WHEN WebsiteSourceFlag = 1 THEN 
														    CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig  
															   +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
															   +HotDealImagePath  
															ELSE @RetailerConfig  
															+CONVERT(VARCHAR(30),P.RetailID)+'/'  
															+HotDealImagePath  
															END  					                         
									            ELSE P.HotDealImagePath  
												END  
										END 
					, HotDealURL
					, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
					, CategoryID categoryId
					, PL.City
					, PL.state
					
			FROM ProductHotDeal P
				INNER JOIN ProductHotDealLocation PL on PL.ProductHotDealID=P.ProductHotDealID
				LEFT JOIN APIPartner A on A.APIPartnerID=P.APIPartnerID
				LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State
			WHERE CategoryID is null 
				  AND P.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
			      --AND ((LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP)))
				AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
				AND GETDATE() BETWEEN P.HotDealStartDate AND P.HotDealEndDate
				) Others
		WHERE (((@Latitude IS NOT NULL AND @Longitude IS NOT NULL) AND Distance <= ISNULL(@Radius,5))
			  OR 
			  ((@Latitude IS NULL AND @Longitude IS NULL) AND 1=1))
			 AND ((@PopulationCentreID IS NOT NULL AND (
															(LTRIM(RTRIM(City)) IN (SELECT City FROM #TEMP)) 
															AND (LTRIM(RTRIM(state)) IN (SELECT State from #TEMP))
													     )
				   )
				   OR
				   (@PopulationCentreID IS NULL AND 1=1)
			      )
			--To Filter Koala Hot Deals.
			--AND APIPartnerID != @KoalaPartnerID
		GROUP BY ProductHotDealID
			, HotDealName 
			, Category
			, APIPartnerID
			, APIPartnerName
			, Price
			, SalePrice
			, HotDealShortDescription
			, HotDealImagePath
			, HotDealURL
			--, Distance
			, CategoryID  	
		ORDER BY APIPartnerName, Price
	  
	  --To enable/disable "By Category" button.	  
	   IF EXISTS( SELECT 1 FROM Category C
				      INNER JOIN ProductHotDeal P on p.CategoryID=C.CategoryID
				      INNER JOIN UserCategory UC ON UC.CategoryID=C.CategoryID
				 WHERE (GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate) AND UserID=@UserID)
		BEGIN
			SET @ByCategoryFlag = 0 
		END
		ELSE 
			SET @ByCategoryFlag = 1
			
				
	  -- --If Coordinates exists fetching retailer list in the specified radius.    
	  -- IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)    
			--BEGIN
		
		--To get hot deals based on lat and long
		IF ISNULL(@PopulationCentreID,0) = 0 
		BEGIN
				IF EXISTS(SELECT TOP 1 CategoryID FROM UserCategory  WHERE UserID=@UserID)
				BEGIN
					PRINT 'CAT'
				
					    --if user's favorite category exists then a flag is set to O, which indicates to disable prompt button in the screen.
						 SET @FavCatFlag = 0
						--To get Hot Deal info
						 SELECT DISTINCT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price)
								, ProductHotDealID
								, HotDealName
								, MIN(City) City
								, Category
								, APIPartnerID
								, APIPartnerName
								, Price
								, SalePrice
								, HotDealShortDescription
								, HotDealImagePath
								, HotDealURL
								--, Distance
								, CategoryID categoryId
						INTO #HD
						FROM (SELECT HD.ProductHotDealID 
									, HD.HotDealName
									, ISNULL(C.ParentCategoryName, 'Uncategorized') AS Category
									, PL.City 
									, ISNULL(AP.APIPartnerID,0) APIPartnerID
									, AP.APIPartnerName 
									, isnull(Price,0) Price
									, SalePrice
									, HotDealShortDescription 
									, HotDealImagePath = CASE WHEN HD.HotDealImagePath IS NOT NULL THEN   
																				CASE WHEN WebsiteSourceFlag = 1 THEN 
																							CASE WHEN HD.ManufacturerID IS NOT NULL THEN @ManufConfig  
																							   +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/'  
																							   +HotDealImagePath  
																							ELSE @RetailerConfig  
																							+CONVERT(VARCHAR(30),HD.RetailID)+'/'  
																							+HotDealImagePath  
																							END  					                         
																				ELSE HD.HotDealImagePath  
																				END  
														END 
									, HotDealURL
									, C.CategoryID categoryId
									, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
							  FROM ProductHotDeal HD
								  INNER JOIN UserCategory U on HD.CategoryID=U.CategoryID
								  INNER JOIN Category C ON C.CategoryID = U.CategoryID 
								  INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID
								  LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State
								  LEFT JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID 
							  WHERE UserID=@UserID AND HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
									AND (
											((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
											OR 
											((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
										)
								  -- AND (
										--(LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP))
										
									 --  )
								   AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
								   AND GETDATE() BETWEEN HD.HotDealStartDate AND HD.HotDealEndDate
							  ) HotDeal
						WHERE (((@Latitude IS NOT NULL AND @Longitude IS NOT NULL) AND Distance <= ISNULL(@Radius,5))
								OR 
							  ((@Latitude IS NULL AND @Longitude IS NULL) AND 1=1))
							  --To Filter Koala Hot Deals.
								--AND APIPartnerID != @KoalaPartnerID
						GROUP BY ProductHotDealID
								, HotDealName 
								, Category
								, APIPartnerID
								, APIPartnerName
								, Price
								, SalePrice
								, HotDealShortDescription
								, HotDealImagePath
								, HotDealURL
								--, Distance
								, CategoryID  			
						ORDER BY Category, APIPartnerName, Price
						
						IF ISNULL(@Category,0) <> '0'
						BEGIN
							INSERT INTO @HotDeal (Row_Num  
												,ProductHotDealID  
												, HotDealName  
												, City  
												, Category  
												, APIPartnerID  
												, APIPartnerName  
												, Price  
												, SalePrice  
												, HotDealShortDescription  
												, HotDealImagePath  
												, HotDealURL  	
												--, Distance  
												, CategoryID)
							SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price )
									,ProductHotDealID
									, HotDealName
									, City
									, Category
									, APIPartnerID
									, APIPartnerName
									, Price
									, SalePrice
									, HotDealShortDescription
									, HotDealImagePath
									, HotDealURL						
									--, Distance 
									, CategoryID categoryId
							FROM #HD
							
						END						
						ELSE
						BEGIN
							--To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
							INSERT INTO @HotDeal (Row_Num  
												,ProductHotDealID  
												, HotDealName  
												, City  
												, Category  
												, APIPartnerID  
												, APIPartnerName  
												, Price  
												, SalePrice  
												, HotDealShortDescription  
												, HotDealImagePath  
												, HotDealURL  	
												--, Distance  
												, CategoryID)
							SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
																 ,Category
																 , APIPartnerName
																 , Price )
								,ProductHotDealID
								, HotDealName
								, City
								, Category
								, APIPartnerID
								, APIPartnerName
								, Price
								, SalePrice
								, HotDealShortDescription
								, HotDealImagePath
								, HotDealURL						
								--, Distance 
								, CategoryID categoryId
							FROM 
								(SELECT ProductHotDealID
										, HotDealName
										, City
										, Category
										, APIPartnerID
										, APIPartnerName
										, Price
										, SalePrice
										, HotDealShortDescription
										, HotDealImagePath
										, HotDealURL
										, CategoryID categoryId
										--, Distance
									FROM #HD
									UNION ALL
									SELECT ProductHotDealID
										, HotDealName
										, City
										, Category
										, APIPartnerID
										, APIPartnerName
										, Price
										, SalePrice
										, HotDealShortDescription
										, HotDealImagePath
										, HotDealURL
										--, Distance
										, CategoryID categoryId
									FROM #Others
								) HotDeals 
							ORDER BY Row_Num
						END
							
							--To capture max row number.
							SELECT @MaxCnt = MAX(Row_Num) FROM @HotDeal
							--this flag is a indicator to enable "More" button in the UI. 
							--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
							SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

							SELECT  Row_Num  rowNumber
									, ProductHotDealID hotDealId
									, HotDealName hotDealName
									, City
									, Category categoryName
									, APIPartnerID apiPartnerId
									, APIPartnerName apiPartnerName
									, Price hDPrice
									, SalePrice hDSalePrice
									, HotDealShortDescription hDshortDescription
									, HotDealImagePath hotDealImagePath	
									, HotDealURL  hdURL
									, CategoryID
									--, Distance 	distance
									, CategoryID categoryId
							FROM @HotDeal 
							WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
							ORDER BY Row_Num
						END	
				ELSE
					BEGIN
					--if user's favorite category does not exists then a flag is set to 1, which indicates to enable prompt button in the screen.
						SET @FavCatFlag = 1
						--commented below code for not considering only Dealmap deals.(if Fav category is not set then the next priority is given to dealmap deals. but now this functionality is discarded. if fav category is not set all the deals shoul dbe dispalyed)
						
						--DECLARE @ApiPartnerID int
						--SELECT @ApiPartnerID= APIPartnerID FROM APIPartner WHERE APIPartnerName='DealMap'
				
						--	  	 SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price)
						--				, ProductHotDealID
						--				, HotDealName
						--				, City
						--				, Category
						--				, APIPartnerID
						--				, APIPartnerName
						--				, Price
						--				, SalePrice
						--				, HotDealShortDescription
						--				, HotDealImagePath
						--				, HotDealURL
						--				, Distance 
						--				, CategoryID categoryId
						--		INTO #DealMap
						--		FROM (SELECT HD.ProductHotDealID 
						--					, HD.HotDealName
						--					, PL.City 
						--					, ISNULL(C.ParentCategoryName, 'Uncategorized') AS Category
						--					, ISNULL(AP.APIPartnerID,0) APIPartnerID
						--					, AP.APIPartnerName 
						--					, isnull(Price,0) Price
						--					, SalePrice
						--					, HotDealShortDescription 
						--					, HotDealImagePath = CASE WHEN HD.HotDealImagePath IS NOT NULL THEN   
						--																CASE WHEN WebsiteSourceFlag = 1 THEN 
						--																			CASE WHEN HD.ManufacturerID IS NOT NULL THEN @Config  
						--																			   +'Images/manufacturer/'  
						--																			   +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/'  
						--																			   +HotDealImagePath  
						--																			ELSE @Config  
						--																			+'Images/retailer/'  
						--																			+CONVERT(VARCHAR(30),HD.RetailID)+'/'  
						--																			+HotDealImagePath  
						--																			END  					                         
						--																ELSE HD.HotDealImagePath  
						--																END  
						--										END 
						--					, HotDealURL
						--					, Distance = CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(PL.HotDealLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(PL.HotDealLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (PL.HotDealLongitude / 57.2958))))*6371) * 0.6214 END
						--					, C.CategoryID categoryId
						--			   FROM ProductHotDeal HD
						--					INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID
						--					INNER JOIN APIPartner AP on AP.APIPartnerID=HD.APIPartnerID AND HD.APIPartnerID=@ApiPartnerID 
						--					LEFT JOIN Category C ON C.CategoryID = HD.CategoryID
						--			   WHERE HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
						--					AND (
						--							((isnull(@Category,0) = 0) AND (ISNULL(HD.CategoryID, 0) <> 0))
						--							OR 
						--							((isnull(@Category,0) <> 0) AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
						--						)
						--					--AND (
						--					--		(LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP))
													
						--					--     )
						--		   			AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
						--					AND GETDATE() BETWEEN HD.HotDealStartDate AND HD.HotDealEndDate
						--			   ) DealMap
						--		WHERE (((@Latitude IS NOT NULL AND @Longitude IS NOT NULL) AND Distance <= ISNULL(@Radius,5))
						--				OR 
						--			  ((@Latitude IS NULL AND @Longitude IS NULL) AND 1=1))
						--			  --To Filter Koala Hot Deals.
						--				--AND APIPartnerID != @KoalaPartnerID
						--		ORDER BY Category, APIPartnerName, Price
						
						
						--IF EXISTS (SELECT TOP 1 ProductHotDealID FROM #DealMap)
						--	BEGIN
						--		PRINT 'DEAL'
								
						--		IF @Category <> 0
						--		BEGIN
									
						--			INSERT INTO @HotDeal (Row_Num  
						--						,ProductHotDealID  
						--						, HotDealName  
						--						, City  
						--						, Category  
						--						, APIPartnerID  
						--						, APIPartnerName  
						--						, Price  
						--						, SalePrice  
						--						, HotDealShortDescription  
						--						, HotDealImagePath  
						--						, HotDealURL  	
						--						, Distance  
						--						, CategoryID)
						--			SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price )
						--					,ProductHotDealID
						--					, HotDealName
						--					, City
						--					, Category
						--					, APIPartnerID
						--					, APIPartnerName
						--					, Price
						--					, SalePrice
						--					, HotDealShortDescription
						--					, HotDealImagePath
						--					, HotDealURL
						--					, Distance 
						--					, CategoryID categoryId
						--			FROM #DealMap
						--		END
						--		ELSE
						--		BEGIN
						--		--To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
						--		INSERT INTO @HotDeal (Row_Num  
						--						,ProductHotDealID  
						--						, HotDealName  
						--						, City  
						--						, Category  
						--						, APIPartnerID  
						--						, APIPartnerName  
						--						, Price  
						--						, SalePrice  
						--						, HotDealShortDescription  
						--						, HotDealImagePath  
						--						, HotDealURL  	
						--						, Distance  
						--						, CategoryID)
						--		SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
						--											 ,Category
						--											 , APIPartnerName
						--											 , Price )
						--			,ProductHotDealID
						--			, HotDealName
						--			, City
						--			, Category
						--			, APIPartnerID
						--			, APIPartnerName
						--			, Price
						--			, SalePrice
						--			, HotDealShortDescription
						--			, HotDealImagePath
						--			, HotDealURL
						--			, Distance 
						--			, CategoryID categoryId
						--		FROM 
						--			(SELECT ProductHotDealID
						--					, HotDealName
						--					, City
						--					, Category
						--					, APIPartnerID
						--					, APIPartnerName
						--					, Price
						--					, SalePrice
						--					, HotDealShortDescription
						--					, HotDealImagePath
						--					, HotDealURL
						--					, Distance
						--					, CategoryID categoryId
						--				FROM #DealMap
						--				UNION ALL
						--				SELECT ProductHotDealID
						--					, HotDealName
						--					, City
						--					, Category
						--					, APIPartnerID
						--					, APIPartnerName
						--					, Price
						--					, SalePrice
						--					, HotDealShortDescription
						--					, HotDealImagePath
						--					, HotDealURL
						--					, Distance
						--					, CategoryID categoryId
						--				FROM #Others
						--			) HotDeals 
						--		ORDER BY Row_Num	
						--		END
								
						--		--To capture max row number.
						--			SELECT @MaxCnt = MAX(Row_Num) FROM @HotDeal
									
						--			--this flag is a indicator to enable "More" button in the UI. 
						--			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
						--			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

						--			SELECT Row_Num  rowNumber
						--					, ProductHotDealID hotDealId
						--					, HotDealName hotDealName
						--					, City
						--					, Category categoryName
						--					, APIPartnerID apiPartnerId
						--					, APIPartnerName apiPartnerName
						--					, Price hDPrice
						--					, SalePrice hDSalePrice
						--					, HotDealShortDescription hDshortDescription
						--					, HotDealImagePath hotDealImagePath	
						--					, HotDealURL  hdURL
						--					, Distance 	distance
						--					, CategoryID categoryId
						--			FROM @HotDeal
						--			WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
						--			ORDER BY Row_Num
									
						--	END
							
					
						--ELSE
						--	BEGIN
								PRINT 'OTHERS'
								 SELECT DISTINCT Row_Num = ROW_NUMBER() OVER(ORDER BY Category, APIPartnerName, Price)
										, ProductHotDealID
										, HotDealName
										, MIN(City) City
										, Category
										, APIPartnerID
										, APIPartnerName
										, Price
										, SalePrice
										, HotDealShortDescription
										, HotDealImagePath
										, HotDealURL
										--, Distance
										, CategoryID categoryId
								INTO #Deals
								FROM (SELECT HD.ProductHotDealID 
											, HD.HotDealName
											, PL.City
											, ISNULL(C.ParentCategoryName, 'Uncategorized') AS Category
											, ISNULL(AP.APIPartnerID,0) APIPartnerID
											, AP.APIPartnerName 
											, isnull(Price,0) Price 
											, SalePrice
											, HotDealShortDescription 
											, HotDealImagePath= CASE WHEN HD.HotDealImagePath IS NOT NULL THEN   
																						CASE WHEN WebsiteSourceFlag = 1 THEN 
																									CASE WHEN HD.ManufacturerID IS NOT NULL THEN @ManufConfig  
																									   +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/'  
																									   +HotDealImagePath  
																									ELSE @RetailerConfig  
																									+CONVERT(VARCHAR(30),HD.RetailID)+'/'  
																									+HotDealImagePath  
																									END  					                         
																						ELSE HD.HotDealImagePath  
																						END  
																END 
											, HotDealURL
											, Distance=CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
											, C.CategoryID categoryId
									  FROM ProductHotDeal HD
										   INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID
										   LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State
										   LEFT JOIN Category C ON C.CategoryID = HD.CategoryID 
										   LEFT JOIN APIPartner AP on AP.APIPartnerID=HD.APIPartnerID
									  WHERE HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
									
											AND (
													((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
													OR 
													((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
												)
											--AND (
											--		(LTRIM(RTRIM(PL.City)) IN (SELECT City FROM #TEMP)) AND (LTRIM(RTRIM(PL.state)) IN (SELECT State from #TEMP))
													
											--     )
												
								   			AND 
								   	
								   			HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
								   			
											AND GETDATE()-1 BETWEEN HD.HotDealStartDate AND HD.HotDealEndDate
 											
											
									) Deals
									
								WHERE (((@Latitude IS NOT NULL AND @Longitude IS NOT NULL) AND Distance <= ISNULL(@Radius,5))
										OR 
									  ((@Latitude IS NULL AND @Longitude IS NULL) AND 1=1))
									  --To Filter Koala Hot Deals.
										--AND APIPartnerID != @KoalaPartnerID
								GROUP BY ProductHotDealID
										, HotDealName 
										, Category
										, APIPartnerID
										, APIPartnerName
										, Price
										, SalePrice
										, HotDealShortDescription
										, HotDealImagePath
										, HotDealURL
										--, Distance
										, CategoryID  
								ORDER BY Category, APIPartnerName, Price
								
							
								IF ISNULL(@Category,0) <> '0'
								BEGIN
									INSERT INTO @HotDeal (Row_Num  
												,ProductHotDealID  
												, HotDealName  
												, City  
												, Category  
												, APIPartnerID  
												, APIPartnerName  
												, Price  
												, SalePrice  
												, HotDealShortDescription  
												, HotDealImagePath  
												, HotDealURL  	
												--, Distance  
												, CategoryID)
									SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category
																	 , APIPartnerName
																	 , Price )
											,ProductHotDealID
											, HotDealName
											, City
											, Category
											, APIPartnerID
											, APIPartnerName
											, Price
											, SalePrice
											, HotDealShortDescription
											, HotDealImagePath
											, HotDealURL
											--, Distance 
											, CategoryID categoryId
									FROM #Deals
								END
								ELSE 
								BEGIN
								
								--To combine "Others" Category Hotdeals at last of the actual Category Hotdeals.
								INSERT INTO @HotDeal (Row_Num  
												,ProductHotDealID  
												, HotDealName  
												, City  
												, Category  
												, APIPartnerID  
												, APIPartnerName  
												, Price  
												, SalePrice  
												, HotDealShortDescription  
												, HotDealImagePath  
												, HotDealURL  	
												--, Distance  
												, CategoryID)
								SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
																	 ,Category
																	 , APIPartnerName
																	 , Price )
									,ProductHotDealID
									, HotDealName
									, City
									, Category
									, APIPartnerID
									, APIPartnerName
									, Price
									, SalePrice
									, HotDealShortDescription
									, HotDealImagePath
									, HotDealURL
									--, Distance 
									, CategoryID categoryId
								FROM 
									(SELECT ProductHotDealID
											, HotDealName
											, City
											, Category
											, APIPartnerID
											, APIPartnerName
											, Price
											, SalePrice
											, HotDealShortDescription
											, HotDealImagePath
											, HotDealURL
											--, Distance
											, CategoryID categoryId
										FROM #Deals
										UNION ALL
										SELECT ProductHotDealID
											, HotDealName
											, City
											, Category
											, APIPartnerID 
											, APIPartnerName
											, Price
											, SalePrice
											, HotDealShortDescription
											, HotDealImagePath
											, HotDealURL
											--, Distance
											, CategoryID categoryId
										FROM #Others
									) HotDeals 
								ORDER BY Row_Num
								END
								
								--To capture max row number.
									SELECT @MaxCnt = MAX(Row_Num) FROM @HotDeal
									
									--this flag is a indicator to enable "More" button in the UI. 
									--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
									SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

									SELECT Row_Num  rowNumber
											, ProductHotDealID hotDealId
											, HotDealName hotDealName
											, City city
											, Category categoryName
											, APIPartnerID apiPartnerId
											, APIPartnerName apiPartnerName
											, Price hDPrice
											, SalePrice hDSalePrice
											, HotDealShortDescription hDshortDescription
											, HotDealImagePath hotDealImagePath	
											, HotDealURL  hdURL
											--, Distance 	distance
											, CategoryID categoryId
									FROM @HotDeal
									WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
									ORDER BY Row_Num
							--END		
			
					END
		
		END
		--To get hot deals based on Population Centre.
		IF ISNULL(@PopulationCentreID,0) <> 0
		BEGIN
			-- setting the flag to zero as the prompt in the screen should not appear for this flow (population center)
			
			SET @FavCatFlag = 0
		
			SELECT DISTINCT Row_Num = ROW_NUMBER() OVER(ORDER BY Category
																	 , APIPartnerName
																	 , Price )
					,ProductHotDealID
					, HotDealName
					, MIN(City) City
					, Category
					, APIPartnerID
					, APIPartnerName
					, Price
					, SalePrice
					, HotDealShortDescription
					, HotDealImagePath
					, HotDealURL
					--, Distance  
					, CategoryID categoryId
			INTO #PopulationCenterHD
			FROM (
					SELECT HD.ProductHotDealID
						, HotDealName
						, City
						, C.ParentCategoryName Category
						, ISNULL(HD.APIPartnerID,0) APIPartnerID
						, APIPartnerName
						, Price
						, SalePrice
						, HotDealShortDescription
						, HotDealImagePath = CASE WHEN HD.HotDealImagePath IS NOT NULL THEN   
																	CASE WHEN WebsiteSourceFlag = 1 THEN 
																				CASE WHEN HD.ManufacturerID IS NOT NULL THEN @ManufConfig  
																				   +CONVERT(VARCHAR(30),HD.ManufacturerID)+'/'  
																				   +HotDealImagePath  
																				ELSE @RetailerConfig  
																				+CONVERT(VARCHAR(30),HD.RetailID)+'/'  
																				+HotDealImagePath  
																				END  					                         
																	ELSE HD.HotDealImagePath  
																	END  
											END 
						, HotDealURL
						--, NULL Distance
						, C.CategoryID categoryId
					FROM ProductHotDeal HD
					LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = HD.ProductHotDealID 
					LEFT JOIN APIPartner A ON A.APIPartnerID = HD.APIPartnerID 
					INNER JOIN Category C ON C.CategoryID = HD.CategoryID 
					WHERE (ISNULL(LTRIM(RTRIM(PL.City)),(SELECT TOP 1 City FROM #TEMP)) IN (SELECT City FROM #TEMP)) 
						AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
						AND GETDATE() BETWEEN HD.HotDealStartDate AND HD.HotDealEndDate
						AND (ISNULL(LTRIM(RTRIM(PL.state)), (SELECT TOP 1 State FROM #TEMP)) IN (SELECT State from #TEMP))
						AND (
								((isnull(@Category,0) = '0') AND (ISNULL(HD.CategoryID, 0) <> 0))
								OR 
								((isnull(@Category,0) <> '0') AND (HD.CategoryID IN (SELECT Param FROM fn_SplitParam(@Category, ','))))
							)
						--To Filter Koala Hot Deals.
						--AND HD.APIPartnerID != @KoalaPartnerID
					
				) HotDeals 
			GROUP BY ProductHotDealID
					, HotDealName 
					, Category
					, APIPartnerID
					, APIPartnerName
					, Price
					, SalePrice
					, HotDealShortDescription
					, HotDealImagePath
					, HotDealURL
					--, Distance  
					, CategoryID  
				
				IF ISNULL(@Category,0) <> '0'
				BEGIN
					INSERT INTO @HotDeal (Row_Num  
												,ProductHotDealID  
												, HotDealName  
												, City  
												, Category  
												, APIPartnerID  
												, APIPartnerName  
												, Price  
												, SalePrice  
												, HotDealShortDescription  
												, HotDealImagePath  
												, HotDealURL  	
												--, Distance  
												, CategoryID)
					SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY Category
																	 , APIPartnerName
																	 , Price )
							,ProductHotDealID
							, HotDealName
							, City
							, Category
							, APIPartnerID
							, APIPartnerName
							, Price
							, SalePrice
							, HotDealShortDescription
							, HotDealImagePath
							, HotDealURL
							--, Distance  
							, CategoryID categoryId
					FROM #PopulationCenterHD
					
					
				END
				
				IF ISNULL(@Category,0) = '0'
				BEGIN
				print 'a'
						INSERT INTO @HotDeal (Row_Num  
												,ProductHotDealID  
												, HotDealName  
												, City  
												, Category  
												, APIPartnerID  
												, APIPartnerName  
												, Price  
												, SalePrice  
												, HotDealShortDescription  
												, HotDealImagePath  
												, HotDealURL  	
												--, Distance  
												, CategoryID)
				
						SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY CASE WHEN Category = 'Uncategorized' THEN 1 ELSE 0 END
																			 , Category
																			 , APIPartnerName
																			 , Price )
							,ProductHotDealID
							, HotDealName
							, City
							, Category
							, APIPartnerID
							, APIPartnerName
							, Price
							, SalePrice
							, HotDealShortDescription
							, HotDealImagePath
							, HotDealURL
							--, Distance  
							, CategoryID categoryId
						FROM (SELECT ProductHotDealID
									, HotDealName
									, City
									, Category
									, APIPartnerID
									, APIPartnerName
									, Price
									, SalePrice
									, HotDealShortDescription
									, HotDealImagePath
									, HotDealURL
									--, Distance  
									, categoryId
							 FROM #PopulationCenterHD
							 UNION ALL
							 SELECT ProductHotDealID
									, HotDealName
									, City
									, Category
									, APIPartnerID 
									, APIPartnerName
									, Price
									, SalePrice
									, HotDealShortDescription
									, HotDealImagePath
									, HotDealURL
									--, Distance
									, CategoryID categoryId
							FROM #Others
							) HotDeal
					END
					--To capture max row number.
					SELECT @MaxCnt = MAX(Row_Num) FROM @HotDeal
					
					--this flag is a indicator to enable "More" button in the UI. 
					--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
					SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

					SELECT Row_Num  rowNumber
							, ProductHotDealID hotDealId
							, HotDealName hotDealName
							, City city
							, Isnull(Category, 'Others') categoryName
							, APIPartnerID apiPartnerId
							, APIPartnerName apiPartnerName
							, Price hDPrice
							, SalePrice hDSalePrice
							, HotDealShortDescription hDshortDescription
							, HotDealImagePath hotDealImagePath	
							, HotDealURL  hdURL
							--, Distance 	distance
							, CategoryID categoryId
					FROM @HotDeal
					WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
					ORDER BY Row_Num
				
				    
		END
			--END

		--To get Hot Deal info
		--SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY HD.Category, AP.APIPartnerName, HD.Price)
		--	, HD.ProductHotDealID 
		--	, HD.HotDealName
		--	, ISNULL(HD.Category, 'Uncategorized') AS Category
		--	, AP.APIPartnerID 
		--	, AP.APIPartnerName 
		--	, Price 
		--	, SalePrice
		--	, HotDealShortDescription 
		--	, HotDealImagePath 
		--INTO #HotDeals
		--FROM ProductHotDeal HD
		--	LEFT JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID 
		--WHERE HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
		--	AND ((@Category = 'All') AND (ISNULL(HD.Category,'') LIKE '%')
		--		OR 
		--		(@Category <> 'All') AND (HD.Category IN (@Category)))
		--	AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
		--	AND CONVERT(DATE, GETDATE()) BETWEEN CONVERT(DATE, HD.HotDealStartDate) AND CONVERT(DATE, HD.HotDealEndDate) 
		--ORDER BY HD.Category, AP.APIPartnerName, HD.Price	
		----To capture max row number.
		--SELECT @MaxCnt = MAX(Row_Num) FROM #HotDeals
		----this flag is a indicator to enable "More" button in the UI. 
		----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		--SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

		--SELECT Row_Num  rowNumber
		--, ProductHotDealID hotDealId
		--, HotDealName hotDealName
		--, Category categoryName
		--, APIPartnerID apiPartnerId
		--, APIPartnerName apiPartnerName
		--, Price hDPrice
		--, SalePrice hDSalePrice
		--, HotDealShortDescription hDshortDescription
		--, HotDealImagePath hotDealImagePath		
		--FROM #HotDeals 
		--WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
		--ORDER BY Row_Num
		--;WITH HotDeals
		--AS
		----(
		--	SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY C.ParentCategoryName, AP.APIPartnerName, HD.Price)
		--		, HD.ProductHotDealID 
		--		, HD.ProductID 
		--		, HotDealName
		--		, C.CategoryID 
		--		, C.ParentCategoryName 
		--		, AP.APIPartnerID 
		--		, AP.APIPartnerName 
		--		, Price 
		--		, HotDealShortDescription 
		--		, HotDealImagePath 
		--	INTO #HotDeals
		--	FROM ProductHotDeal HD
		--		INNER JOIN ProductCategory PC ON PC.ProductID = HD.ProductID 
		--		INNER JOIN UserCategory UC ON UC.CategoryID = PC.CategoryID 
		--		INNER JOIN Category C ON C.CategoryID = UC.CategoryID 
		--		LEFT JOIN APIPartner AP ON AP.APIPartnerID = HD.APIPartnerID 
		--	WHERE UC.UserID = @UserID 
		--		AND HD.HotDealName LIKE (CASE WHEN ISNULL(@Search,'') = '' THEN '%' ELSE '%' + @Search + '%' END) 
		--		AND HD.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
		--		AND CONVERT(DATE, GETDATE()) BETWEEN CONVERT(DATE, HD.HotDealStartDate) AND CONVERT(DATE, HD.HotDealEndDate) 
		--	--ORDER BY CASE WHEN @SortBy = 'P' THEN CONVERT(varchar(100), HD.Price)
		--	--			  WHEN @SortBy = 'C' THEN 
		--	--		 END 
		----) 
		----To capture max row number.
		--SELECT @MaxCnt = MAX(Row_Num) FROM #HotDeals
		----this flag is a indicator to enable "More" button in the UI. 
		----If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		--SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
	
		--SELECT Row_Num  rowNumber
		--	, ProductHotDealID hotDealId
		--	, ProductID 
		--	, HotDealName hotDealName
		--	, CategoryID 
		--	, ParentCategoryName categoryName
		--	, APIPartnerID apiPartnerId
		--	, APIPartnerName apiPartnerName
		--	, Price hDPrice
		--	, HotDealShortDescription hDshortDescription
		--	, HotDealImagePath hotDealImagePath		
		--FROM #HotDeals 
		--WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
		
	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HotDealSearchPagination.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
