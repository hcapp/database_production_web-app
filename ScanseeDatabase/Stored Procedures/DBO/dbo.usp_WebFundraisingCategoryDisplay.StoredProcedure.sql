USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFundraisingCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebFundraisingCategoryDisplay
Purpose					: To display fundraising Categories.
Example					: usp_WebFundraisingCategoryDisplay

History
Version		 Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			11/11/2014	    SPAN		  1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFundraisingCategoryDisplay]
(
    
	--Input variable	  
	  @RetailID Int
	  
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
					
			     SELECT HcFundraisingCategoryID As categoryID
						,FundraisingCategoryName As categoryName
						,CategoryImagePath cateImg
				 FROM HcFundraisingCategory	
																		
			
			   --Confirmation of Success.
			   SELECT @Status = 0
	
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebFundraisingCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
