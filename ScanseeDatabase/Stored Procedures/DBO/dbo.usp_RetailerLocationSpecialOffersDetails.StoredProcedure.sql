USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerLocationSpecialOffersDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerLocationSpecialOffersDisplay
Purpose					: To List the Special Offers for the given Retailer Location.
Example					: usp_RetailerLocationSpecialOffersDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerLocationSpecialOffersDetails]
(
	  
	  @RetailID int    
	, @RetailLocationID int
	, @PageID int
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	 
		
		DECLARE @RetailerConfig varchar(50)
		 DECLARE @MediaTypes VARCHAR(1000) = ''
		 DECLARE @MediaPath VARCHAR(MAX) = ''
		 DECLARE @ExternalFlag varchar(100) = ''
		 DECLARE @RetailLocations VARCHAR(1000) = ''
		
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration'
		 
		 --Get Associated Media Details
		 SELECT  @MediaTypes = COALESCE(@MediaTypes+',','') + CAST(PMT.ProductMediaType AS VARCHAR(10))
			   , @MediaPath = COALESCE(@MediaPath+',','') +  @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + CAST(QR.MediaPath AS VARCHAR(100))
			   , @ExternalFlag = COALESCE(@ExternalFlag+',','') + CAST(ExternalFlag AS VARCHAR(10))
		 FROM QRRetailerCustomPageMedia QR
		 INNER JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = QR.MediaTypeID
		 WHERE QRRetailerCustomPageID = @PageID	 
		 
		--Get Page Details 
		SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
			 , QRRCP.Pagetitle
			 , QRRCP.PageDescription
			 , QRRCP.ShortDescription
			 , QRRCP.LongDescription
			 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID)
				    ELSE QRRCP.Image END  ImageName
			 , CASE WHEN QRRCP.Image IS NULL THEN (SELECT @RetailerConfig + CAST(QRRetailerCustomPageIconImagePath AS VARCHAR(100)) FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QRRCP.QRRetailerCustomPageIconID) 
					ELSE @RetailerConfig + CAST(QRRCP.RetailID AS VARCHAR(10)) + '/'+ CAST(Image AS VARCHAR(100)) END	ImagePath
			 , SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
			 , SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
			 , SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
			 , QRRCP.RetailID			 
			 , QRRCP.StartDate
			 , QRRCP.EndDate
			 , Expired = CASE WHEN GETDATE() > EndDate THEN 1
						 ELSE 0 END
		FROM QRRetailerCustomPage QRRCP
		INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID	
		INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCPA.QRRetailerCustomPageID = QRRCP.QRRetailerCustomPageID	
		WHERE QRRCP.RetailID = @RetailID
		AND QRRCP.QRRetailerCustomPageID = @PageID
		AND QRRCPA.RetailLocationID = @RetailLocationID
		 
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
