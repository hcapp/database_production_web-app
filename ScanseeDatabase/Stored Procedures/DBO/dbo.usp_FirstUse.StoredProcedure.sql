USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FirstUse]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FirstUse
Purpose					: For User Registration.
Example					:  
DECLARE	@return_value int
EXEC @return_value =  [usp_FirstUse] 'John@em.com', 'Mitter','6/28/2011', '123234234', null, null, @Result = @return_value OUTPUT
SELECT @return_value as return_value
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_FirstUse]
(
	  @UserName varchar(15)
	, @Password varchar(60)
	, @Email varchar(100)
	, @DateCreated datetime
	
	
	--UserLocation Variables.
	, @DeviceID varchar(60)
	, @UserLongitude float
	, @UserLatitude float
	
	--Output Variable 
	, @FirstUserID int output
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			-- Check for existance of the Email Address. 
			--If already found, return -1
			IF EXISTS (SELECT 1 FROM Users WHERE UserName = @UserName and FaceBookAuthenticatedUser=0)
			BEGIN
				SELECT @Result = -1
			END
			--If not found, insert into Users table. ie,allow user to register.
			ELSE
			BEGIN
				INSERT INTO Users 
					(
					  UserName
					 ,Password
					 ,Email 
					 ,PushNotify
					 ,FirstUseComplete
					 ,FieldAgent
					 ,DeviceID
					 ,DateCreated
					)
				VALUES
					(
					  @UserName 
					 ,@Password 
					 ,@Email 
					 ,0
					 ,0
					 ,0
					 ,@DeviceID
					 ,@DateCreated 
					)
				
				DECLARE @UserID INT
				SET @UserID = SCOPE_IDENTITY()
				
				INSERT INTO UserLocation 
					(
						UserID 
						,DeviceID
						,UserLongitude
						,UserLatitude
						,UserLocationDate
					)
				VALUES
					(
						@UserID 
						,@DeviceID 
						,@UserLongitude 
						,@UserLatitude 
						,@DateCreated 
					)
			-- To capture User First Use Login.
			INSERT INTO [UserFirstUseLogin]
				   ([UserID]
				   ,[FirstUseLoginDate])
			 VALUES
				   (@UserID 
				   ,GETDATE())
				   		
			--To Pass generated UserID.
			SELECT @FirstUserID = @UserID
			
			--Confirmation of Success.
			SELECT @Status = 0
			END
			
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FirstUse.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
