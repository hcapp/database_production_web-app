USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminGiveawaypageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAdminGiveawaypageDisplay
Purpose					: To display all give page details.
Example					: usp_WebAdminGiveawaypageDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7thmay2013  	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminGiveawaypageDisplay]
(

	--Input Variables
	  @SearchParameter varchar(1000)
	, @ColumnName varchar(100) --Output is sorted based on this column
	, @SortOrder varchar(10) --Should be ASC or DESC only
	, @LowerLimit int 
	, @ShowExpired bit
	
	--Output Variable 
	, @RowCount INT OUTPUT    
	, @NextPageFlag bit output   
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		 DECLARE @Query VARCHAR(MAX)
		 DECLARE @UpperLimit INT    
		 DECLARE @MaxCnt INT    
		 
		 DECLARE @QRTypeid Int
		
		 SELECT @QRTypeid =QRTypeID  from QRTypes where QRTypeName like 'Give Away Page'		
		
		      
		  --To get the row count for pagination.      
		 DECLARE @ScreenContent Varchar(100)    
		 SELECT @ScreenContent = ScreenContent       
		 FROM AppConfiguration       
		 WHERE ScreenName = 'All'     
		 AND ConfigurationType = 'Website Pagination'    
		 AND Active = 1   
		 
		 SET @UpperLimit = @LowerLimit + @ScreenContent  
		 
		--To get the coupons based on the ShowExpired check box value.(If checked, show expired)
		SELECT DISTINCT Q.QRRetailerCustomPageID 
			 , Q.Pagetitle 
			 , Q.Quantity 
			 , Q.ShortDescription 
			 , R.RetailName 
			 , Q.StartDate 
			 , Q.EndDate			 
		INTO #Temp
		FROM QRRetailerCustomPage Q
		INNER JOIN Retailer R ON R.RetailID =Q.RetailID AND R.RetailerActive = 1
		LEFT JOIN UserGiveawayAssociation U ON U.QRRetailerCustomPageID =Q.QRRetailerCustomPageID 
		WHERE Pagetitle LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END 		
		AND ((@ShowExpired =1 AND CAST(ISNULL(Q.EndDate,GETDATE()) AS DATE) <>'1900-01-01')  
		OR (@ShowExpired =0 AND ISNULL(Q.EndDate,GETDATE()+1)>=GETDATE()))
		AND Q.QRTypeID =@QRTypeid AND ISNULL(Winner,0)=0
		
				 
		--To acheive the Dynamic sorting.
		DECLARE @Promotion TABLE(RowNum INT IDENTITY(1, 1)
							 , QRRetailerCustomPageID INT
							 , Pagetitle VARCHAR(1000)
							 , Quantity int							 
							 , ShortDescription VARCHAR(1000)
							 , RetailName VARCHAR(1000)
							 , StartDate DATETIME
							 , EndDate DATETIME)
		
		INSERT INTO @Promotion(  QRRetailerCustomPageID
							 , Pagetitle
							 , Quantity
							 , ShortDescription
							 , RetailName
							 , StartDate
							 , EndDate)
							 
		SELECT QRRetailerCustomPageID 
			 , Pagetitle 
			 , Quantity 
			 , ShortDescription 
			 , RetailName 
			 , StartDate 
			 , EndDate			 
		FROM #Temp
		ORDER BY CASE WHEN @SortOrder = 'ASC' AND @ColumnName = 'Pagetitle' THEN CAST(Pagetitle AS SQL_VARIANT)
					  WHEN @SortOrder = 'ASC' AND @ColumnName= 'Quantity' THEN CAST(Quantity AS SQL_VARIANT)
					  WHEN @SortOrder = 'ASC' AND @ColumnName = 'RetailName' THEN CAST(RetailName AS SQL_VARIANT) 
					  WHEN @SortOrder = 'ASC' AND @ColumnName = 'StartDate' THEN CAST(StartDate AS SQL_VARIANT)
					  WHEN @SortOrder = 'ASC' AND @ColumnName = 'EndDate' THEN CAST(EndDate AS SQL_VARIANT)
            END ASC,
            CASE  WHEN @SortOrder = 'DESC' AND @ColumnName = 'Pagetitle' THEN CAST(Pagetitle AS SQL_VARIANT)  
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'Quantity' THEN CAST(Quantity AS SQL_VARIANT)
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'RetailName' THEN CAST(RetailName AS SQL_VARIANT)  
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'StartDate' THEN CAST(StartDate AS SQL_VARIANT)
				  WHEN @SortOrder = 'DESC' AND @ColumnName = 'EndDate' THEN CAST(EndDate AS SQL_VARIANT)
            END DESC;

					    
			SELECT @MaxCnt = COUNT(RowNum) FROM @Promotion  
		--Output Total no of Records
		SET @RowCount = @MaxCnt 
		       
		SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS 			 
		 
		--Project the records.			
		SELECT QRRetailerCustomPageID 
			 , Pagetitle 
			 , Quantity 
			 , ShortDescription 
			 , RetailName 
			 , StartDate 
			 , EndDate			 
		FROM @Promotion 
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit	
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebAdminGiveawaypageDisplay>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT ERROR_LINE()
		END;
		 
	END CATCH;
END;




GO
