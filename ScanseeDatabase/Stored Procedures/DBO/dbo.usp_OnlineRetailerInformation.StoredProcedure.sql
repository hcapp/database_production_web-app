USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_OnlineRetailerInformation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_OnlineRetailerInformation
Purpose					: To display online store retailer information.
Example					: usp_OnlineRetailerInformation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20th March 2012 	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_OnlineRetailerInformation]
(
	
	 @UserID int
	,@ProductID int
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		SELECT DISTINCT RL.RetailID retailerId
			   ,RetailName retailerName
			   ,RetailOnlineURL retailOnlineURL
			   ,Price
			   ,Price SalePrice
			   ,BuyURL buyURL
			   ,ShipmentDetails ShipmentCost 
		FROM Retailer R
		INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID
		INNER JOIN RetailLocationProduct RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=@ProductID
		WHERE OnlineStoreFlag=1 
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_OnlineRetailerInformation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
