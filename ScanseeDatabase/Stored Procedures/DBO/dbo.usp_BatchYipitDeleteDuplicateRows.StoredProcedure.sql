USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchYipitDeleteDuplicateRows]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchYipitDeleteDuplicateRows
Purpose					: To delete duplicate rows.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd Sep 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchYipitDeleteDuplicateRows]
(
	
	--Output Variable 
	  @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID  
			FROM APIPartner
			WHERE APIPartnerName = 'Yipit'
			
			--To get the count of rows received from API.
			INSERT INTO APIBatchLog (ExecutionDate
									, APIPartnerID
									, APIPartnerName
									, APIRowCount)
			SELECT GETDATE()
			, @APIPartnerID 
			, 'Yipit'
			, COUNT(1)
			FROM APIYipitData
		
			; WITH Yipit
			AS
			(
			SELECT ID
				  , Ranking  = ROW_NUMBER() OVER(PARTITION BY ID ORDER BY NEWID())
			FROM APIYipitData
			)
			DELETE FROM Yipit WHERE Ranking > 1
			
			; WITH YipitTitle
			AS
			(
			SELECT Title
				  , Ranking  = ROW_NUMBER() OVER(PARTITION BY Title ORDER BY NEWID())
			FROM APIYipitData
			)
			DELETE FROM YipitTitle WHERE Ranking > 1
			
			--Update the US state if it is not in abbreviated form.
			UPDATE APIYipitData SET [State] = S.Stateabbrevation
			FROM 
			(SELECT ID
				 , City
				 , [State]
			FROM APIYipitData
			WHERE LEN([State])>2) Deals
			INNER JOIN [State] S ON S.StateName = Deals.[State]
			
			--Delete the Non US or mis-spelt state which is not in abbreviated form.
			
			DELETE FROM APIYipitData WHERE LEN([State])>2
			
			--To get # Rows affected
			SELECT @Result = @@ROWCOUNT 

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchYipitDeleteDuplicateRows.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
