USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListAddRebate]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ShoppingListAddRebate
Purpose					: To add Rebate to User gallery
Example					: usp_MasterShoppingListAddRebate

History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListAddRebate]
(
	@UserID int
   ,@RebateID int
   ,@RetailLocationID int
   ,@RedemptionStatusID tinyint
   ,@PurchaseAmount money
   ,@PurchaseDate datetime
   ,@ProductSerialNumber varchar(20)
   ,@ScanImage Varchar(255)
   
   --Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		IF NOT EXISTS (SELECT 1 FROM UserRebateGallery WHERE RebateID = @RebateID AND UserID=@UserID)
				INSERT INTO [UserRebateGallery]
				   ([UserID]
				   ,[RebateID]
				   ,[RetailLocationID]
				   ,[RedemptionStatusID]
				   ,[PurchaseAmount]
				   ,[PurchaseDate]
				   ,[ProductSerialNumber]
				   ,[ScanImagePath])
				VALUES
				   (@UserID  
				   ,@RebateID
				   ,@RetailLocationID  
				   ,0 
				   ,@PurchaseAmount  
				   ,@PurchaseDate  
				   ,@ProductSerialNumber  
				   ,@ScanImage)
				--Confirmation of Success.
				SELECT @Status = 0
	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListAddRebate.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
