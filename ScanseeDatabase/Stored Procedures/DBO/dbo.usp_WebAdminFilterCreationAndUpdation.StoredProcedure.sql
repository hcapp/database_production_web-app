USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterCreationAndUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAdminFilterCreationAndUpdation
Purpose					: 
Example					: usp_WebAdminFilterCreationAndUpdation

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			1st Dec 2014		Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterCreationAndUpdation]
(
	--Input parameters
      @UserID int
	, @FilterID int    
    , @FilterName varchar(100)	
	, @BusinessCategoryIDs varchar(1000)
	, @FilterValuesIDS varchar(1000)
	, @FilterValueFlag Bit		
	
	--Output Variable 
	, @DuplicateFilterName Bit Output
	--, @DuplicateFilterValue Bit Output	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			SET @DuplicateFilterName=0

			DECLARE @AdminFilterID Int

			--Check whether the filter already exists
			IF EXISTS(SELECT 1 FROM AdminFilter WHERE (FilterName=@FilterName AND (@FilterID IS NULL OR (@FilterID IS NOT NULL AND @FilterID <> AdminFilterID))))
			BEGIN
			    
				SET @DuplicateFilterName=1
			END

			ELSE
			BEGIN
					IF @FilterID IS NULL
					BEGIN
						INSERT INTO AdminFilter(FilterName
												,DateCreated
												,CreatedUserID)
						VALUES(@FilterName,GETDATE(),@UserID)

						SET @AdminFilterID =SCOPE_IDENTITY()


						IF(@FilterValuesIDS IS NOT NULL AND @FilterValueFlag =1)
						BEGIN
									
								INSERT INTO AdminFilterValueAssociation(AdminFilterValueID 
																		,AdminFilterID
																		,DateCreated
																		,CreatedUserID)
								SELECT Param 
										,@AdminFilterID
										,GETDATE()
										,@UserID 
								FROM fn_SplitParam(@FilterValuesIDS,',')								 

								UPDATE AdminFilterValue SET Associated =1 
								WHERE Associated =0
						END

						IF @BusinessCategoryIDs IS NOT NULL
						BEGIN
							INSERT INTO AdminFilterCategoryAssociation(AdminFilterID
																	  ,BusinessCategoryID
																	  ,DateCreated
																	  ,CreatedUserID)
							SELECT DISTINCT @AdminFilterID
									,Param 
									,GETDATE()
									,@UserID
							FROM fn_SplitParam(@BusinessCategoryIDs,',')

						END

			          END

					  IF @FilterID IS NOT NULL
					  BEGIN
								
							UPDATE AdminFilter SET FilterName =@FilterName 
								                    ,DateModified =GETDATE()
													,ModifiedUserID =@UserID 
							WHERE AdminFilterID =@FilterID




							SELECT AdminFilterValueID
							INTO #ADFValue
							FROM AdminFilterValueAssociation WHERE AdminFilterID =@FilterID

							DELETE FROM AdminFilterValueAssociation
							WHERE AdminFilterID =@FilterID 



							IF @FilterValueFlag=0
							BEGIN
								DELETE FROM AdminFilterValue 
								FROM AdminFilterValue AF
								INNER JOIN #ADFValue P ON P.AdminFilterValueID=AF.AdminFilterValueID
							END

							IF(@FilterValuesIDS IS NOT NULL AND @FilterValueFlag=1)
							BEGIN
									INSERT INTO AdminFilterValueAssociation(AdminFilterValueID 
																			,AdminFilterID
																			,DateCreated
																			,CreatedUserID)
									SELECT Param 
											,@FilterID
											,GETDATE()
											,@UserID 
									FROM fn_SplitParam(@FilterValuesIDS,',')
									UPDATE AdminFilterValue SET Associated =1 
								    WHERE Associated =0										
							END

								DELETE FROM AdminFilterCategoryAssociation
								WHERE AdminFilterID =@FilterID

								IF @BusinessCategoryIDs IS NOT NULL
								BEGIN
									INSERT INTO AdminFilterCategoryAssociation(AdminFilterID
																				,BusinessCategoryID
																				,DateCreated
																				,CreatedUserID)
									SELECT DISTINCT @FilterID
										    ,Param 
											,GETDATE()
											,@UserID
									FROM fn_SplitParam(@BusinessCategoryIDs,',')
								END
								
								UPDATE AdminFilterValue SET Associated =1 
								WHERE Associated =0

						    END
				END


		    
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebAdminFilterCreationAndUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
