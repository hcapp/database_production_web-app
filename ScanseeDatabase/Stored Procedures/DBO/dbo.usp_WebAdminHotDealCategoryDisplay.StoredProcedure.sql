USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminHotDealCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebAdminHotDealCategoryDisplay] 
Purpose				  : To get all Categories for HotDeals 
Example				  : [usp_WebAdminHotDealCategoryDisplay] 
    
History    
Version     Date           Author    Change Description    
---------------------------------------------------------------     
1.0      2nd Sept 2013     SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebAdminHotDealCategoryDisplay] 
(    
     --Input Variables
     --@SearchKey varchar(500)

	 --OutPut Variables
	  @Status int output 
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
		
		SELECT CategoryID 
			  ,ParentCategoryName + ' - ' + SubCategoryName CategoryName
		FROM Category
		--WHERE ParentCategoryName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
		ORDER BY CategoryName
			
	
		--Confirmation of Success
		SELECT @Status=0
		
	END TRY    
     
	BEGIN CATCH    
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure [usp_WebAdminHotDealCategoryDisplay].'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT @Status=1
		END;    
	END CATCH;
   
END;


GO
