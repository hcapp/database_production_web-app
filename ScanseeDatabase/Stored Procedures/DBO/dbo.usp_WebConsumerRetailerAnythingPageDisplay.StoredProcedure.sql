USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerRetailerAnythingPageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*    
Stored Procedure name : usp_WebConsumerRetailerAnythingPageDisplay    
Purpose				  : To diaplay the anything page list when the Retailer is selectes in the Consumer web.   
Example				  : usp_WebConsumerRetailerAnythingPageDisplay   
    
History    
Version		Date				Author				Change Description    
-------------------------------------------------------------------------     
1.0			22nd May 2013		Pavan Sharma K		Initial Version    
-------------------------------------------------------------------------  
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerRetailerAnythingPageDisplay]
(

      --Input Parameter(s)--
      
      @UserID int 
    , @RetailId int
    , @RetailLocationID int
    	
	 --Inputs for User Tracking
	, @MainMenuId int        
    , @RetailerListID int                
    
    --Output Variable-- 
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
	, @ribbonAdImagePath Varchar(2000) output
	, @ribbonAdURL Varchar(1000) Output
)
AS
BEGIN

      BEGIN TRY
      
			DECLARE @QRTypeID INT
			DECLARE @RetailerConfig varchar(50)
			DECLARE @Configuration VARCHAR(100)
			
			SELECT @QRTypeID = QRTypeID
			FROM QRTypes
			WHERE QRTypeName = 'Anything Page' 
			
			--Get the Server Configuration.
			SELECT @Configuration = ScreenContent FROM AppConfiguration 
			WHERE ConfigurationType LIKE 'QR Code Configuration'
			AND Active = 1
			
					
			SELECT @RetailerConfig= ScreenContent  
			FROM AppConfiguration   
			WHERE ConfigurationType='Web Retailer Media Server Configuration'
			
			SELECT @ribbonAdImagePath=@RetailerConfig+CAST(@RetailId AS VARCHAR)+'/'+BannerAdImagePath ,@ribbonAdURL=BannerAdURL  
			From AdvertisementBanner AB
			INNER JOIN RetailLocationBannerAd RB ON AB.AdvertisementBannerID=RB.AdvertisementBannerID 
			WHERE RB.RetailLocationID =@RetailLocationID 	
			
			
			
			CREATE TABLE #AnythingPages(RowNum INT IDENTITY(1, 1)
									  , PageID int
									  , pageTitle VARCHAR(1000)
									  , pageImage VARCHAR(1000)
									  , PageLink VARCHAR(2000)
									  , SortOrder INT)
			
            --Display the associated Anythng pages list.   
            
            INSERT INTO #AnythingPages(PageID
									 , pageTitle
									 , pageImage
									 , PageLink
									 , SortOrder)           
			SELECT  DISTINCT QR.QRRetailerCustomPageID pageID
							, QR.Pagetitle pageTitle
							, CASE WHEN [Image] IS NULL THEN (SELECT @RetailerConfig + QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QR.QRRetailerCustomPageIconID) 
									ELSE @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + [Image] END pageImage		 
							--, PageLink = CASE WHEN M.QRRetailerCustomPageID IS NOT NULL AND M.MediaPath LIKE '%pdf%' OR M.MediaPath LIKE '%png%'
							--					THEN @RetailerConfig + CAST(QR.RetailID AS VARCHAR(10)) + '/' + M.MediaPath
							--				  ELSE 
							--					CASE WHEN QR.URL IS NULL 
							--							THEN @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') AS VARCHAR(10)) + '.htm?key1=' + CAST(QRA.RetailID AS VARCHAR(10)) + '&key2=' + CAST(QRA.RetailLocationID AS VARCHAR(10)) + '&key3=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10)) 
							--				         ELSE QR.URL
							--				    END
							--			 END
							, PageLink = @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') as varchar(10)) + '.htm?retId=' + CAST(QRA.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10))+ CASE WHEN QR.URL IS NOT NULL OR M.MediaPath LIKE '%pdf' OR M.MediaPath LIKE '%png' THEN '&EL=true' ELSE '&EL=false'	END
		    			 
							, ISNULL(SortOrder, 100000) SortOrder	
			FROM QRRetailerCustomPage QR
			INNER JOIN QRRetailerCustomPageAssociation QRA ON QR.QRRetailerCustomPageID = QRA.QRRetailerCustomPageID
			LEFT JOIN QRRetailerCustomPageMedia M ON M.QRRetailerCustomPageID = QR.QRRetailerCustomPageID	
			WHERE QRA.RetailLocationID = @RetailLocationID
			AND QR.QRTypeID = @QRTypeID
			AND ((StartDate IS NULL AND EndDate IS NULL) 
		      OR 
				(CAST(GETDATE() AS DATE) BETWEEN StartDate AND ISNULL(EndDate, GETDATE() + 1))
		    ) 		    
		    ORDER BY ISNULL(SortOrder, 100000) ,Pagetitle  
		    
		    --select @Configuration
		  		  
		    --User tracking.
		    UPDATE ScanSeeReportingDatabase..RetailerList 
		    SET RetailLocationClick=1 
		    WHERE RetailerListID =@RetailerListID
		    
		    CREATE TABLE #Temp(AnythingPageListID INT, pageID INT)
		    INSERT INTO ScanSeeReportingDatabase..AnythingPageList(
																   RetailerListID
																 , AnythingPageID
																 , CreatedDate
																 , MainMenuID)
							OUTPUT inserted.AnythingPageListlID, inserted.AnythingPageID INTO #Temp(AnythingPageListID, pageID)
														SELECT @RetailerListID
															 , pageID
															 , GETDATE()
															 , @MainMenuId
														FROM #AnythingPages
		
			--Display the Anything page list along with the primary key of the tracking table.											
			SELECT A.RowNum
				 , AnythingPageListID
				 , A.pageID
				 , A.pageTitle
				 , A.pageImage
				 , A.PageLink
				 , ExternalQR = CASE WHEN A.PageLink LIKE '%'+'=true' THEN 1 ELSE 0 END
			FROM #Temp T
			INNER JOIN #AnythingPages A ON T.PageID = A.pageID
            
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebConsumerFindRetailerSearchPagination.'         
                  --Execute retrieval of Error info.
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
            END;
            
      END CATCH;
END;


GO
