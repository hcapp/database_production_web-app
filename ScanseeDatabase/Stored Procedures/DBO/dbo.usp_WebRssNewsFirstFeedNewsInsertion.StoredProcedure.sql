USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssNewsFirstFeedNewsInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssNewsFirstFeedNewsInsertion]
Purpose					: To insert into RssNewsFirstFeedNews from Staging table.
Example					: [usp_WebRssNewsFirstFeedNewsInsertion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015		Mohith H R	        1.1
1.1         07 Dec 2015     Suganya C           Duplicate news removal
1.2         17 Oct 2016     Shilpashree       Adding Cache Url
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[usp_WebRssNewsFirstFeedNewsInsertion]
(
	 --@HcHubCitiID int,

	--Output Variable
	  @ClearCacheURL Varchar(max) output 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		 --Truncate RssNewsFirstFeedNews table before inserting new news.

		

		 --DELETE FROM RssNewsFirstFeedNews
		 --FROM RssNewsFirstFeedNews RF
		 --INNER JOIN RssNewsFirstFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		 --WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	 
			--OR (RS.Title IS NULL AND RS.NewsType = 'Classifields'))

		--Deleting 30 days older News for 'spanqa.regionsapp'.
		------DECLARE @ExpiryPublishedDateRockwall datetime
		------SET @ExpiryPublishedDateRockwall = GETDATE()-30

		------DELETE FROM RssNewsFirstFeedNewsStagingTable
		------WHERE PublishedDate < @ExpiryPublishedDateRockwall AND HcHubCitiID = 2070 

		---------added on 05/05/2016
		------DECLARE @ExpiryPublishedDateMarbel datetime
		------SET @ExpiryPublishedDateMarbel = GETDATE()-30

		------DELETE FROM RssNewsFirstFeedNewsStagingTable
		------WHERE PublishedDate < @ExpiryPublishedDateMarbel AND HcHubCitiID = 10 

		--------Deleting 5 days older News for 'Killeen Daily Herald'.
		
		------DECLARE @ExpiryPublishedDateKilleen datetime
		------SET @ExpiryPublishedDateKilleen = GETDATE()-5

		------DELETE FROM RssNewsFirstFeedNewsStagingTable
		------WHERE PublishedDate  < @ExpiryPublishedDateKilleen AND HcHubCitiID = 2146  
		
		 --Fetching unique, latest data from RssNewsFirstFeedNewsStagingTable to insert into RssNewsFirstFeedNews
	
		 SELECT DISTINCT T.HcHubCitiID, T.NewsType, T.Title,T.Classification, MAX(T.RssNewsFirstFeedNewsID) AS MaxRssNewsFirstFeedNewsID
         INTO #Temp
         FROM RssNewsFirstFeedNewsStagingTable T 
		 LEFT JOIN RssNewsFirstFeedNews N 
		 ON ISNULL(N.Title,'') =ISNULL(T.Title,'') AND ISNULL(T.Link,'') =ISNULL(N.Link,'')
		  AND ISNULL(T.PublishedDate,'') =ISNULL(N.PublishedDate,'')  AND ISNULL(T.NewsType,'') =ISNULL(N.NewsType,'')
		  AND T.HcHubCitiID =N.HcHubCitiID AND  ISNULL(T.subcategory,'') =ISNULL(N.subcategory,'')
		
		 	WHERE N.RssNewsFirstFeedNewsID IS NULL
         GROUP BY T.HcHubCitiID, T.NewsType, T.Title,T.Classification

		
		--select * from #Temp

		 --Insert into RssNewsFirstFeedNews
		 INSERT INTO RssNewsFirstFeedNews(Title
								,ImagePath
								,ShortDescription
								,LongDescription
								,Link
								,PublishedDate
								,NewsType
								,Message
								,HcHubCitiID
								,Classification
								,Section
								,AdCopy
								,thumbnail
								,author
								,subcategory
								,PublishedTime
								,VideoLink
								,SubCategoryURL)															
						SELECT  RS.Title
								,RS.ImagePath
								,RS.ShortDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy	
								,thumbnail
								,author
								,subcategory	
								,PublishedTime
								,VideoLink
								,SubCategoryURL
						FROM RssNewsFirstFeedNewsStagingTable RS 
						INNER JOIN #Temp T ON RS.HcHubCitiID = T.HcHubCitiID 
						AND RS.NewsType = T.NewsType 
						--AND ((RS.NewsType <> 'Classifields' AND RS.Title = T.Title) OR (RS.NewsType = 'Classifields' AND 1=1))
						AND RS.RssNewsFirstFeedNewsID = T.MaxRssNewsFirstFeedNewsID 
						--AND CONVERT(VARCHAR(12), CONVERT(datetime2,RS.PublishedDate), 107) = T.MaxPublishedDate
						GROUP BY RS.Title
								,RS.ImagePath
								,RS.ShortDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy	
								,RS.thumbnail
								,RS.VideoLink
								,RS.author
								,RS.subcategory	
								,PublishedTime
								,SubCategoryURL
						ORDER BY MIN(RS.RssNewsFirstFeedNewsID) ASC

		DECLARE @LatestTime datetime
		SELECT @LatestTime = MAX(DateCreated)
		FROM RssNewsFirstFeedNews

		DECLARE @HubCitiID int
		SELECT @HubCitiID = HcHubCitiID FROM RssNewsFirstFeedNews WHERE DateCreated = @LatestTime

		UPDATE HcMenuItem SET DateCreated = GETDATE()
		WHERE HcMenuID IN (SELECT HcMenuID FROM HcMenu WHERE HcHubCitiID = @HubCitiID)
						
		-- DELETE FROM RssNewsFirstFeedNews
		-- FROM RssNewsFirstFeedNews RF
		-- INNER JOIN RssNewsFirstFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		-- WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	 
		--	OR (RS.Title IS NULL AND RS.NewsType = 'Classifields'))
		---- AND RF.DateCreated <> @LatestTime

--		 set  identity_insert RssNewsFirstFeedNewsStagingTable_archive  on

--insert  into RssNewsFirstFeedNewsStagingTable_archive(
--RssNewsFirstFeedNewsID,
--Title,
--ImagePath,
--ShortDescription,
--LongDescription,
--Link,
--PublishedDate,
--NewsType,
--DateCreated,
--DateModified,
--CreatedUserID,
--ModifiedUserID,
--HcHubCitiID,
--Message,
--Classification,
--Section,
--AdCopy,
--thumbnail,
--author,
--subcategory,
--PublishedTime,
--VideoLink,
--SubCategoryURL)

--select RssNewsFirstFeedNewsID,
--Title,
--ImagePath,
--ShortDescription,
--LongDescription,
--Link,
--PublishedDate,
--NewsType,
--DateCreated,
--DateModified,
--CreatedUserID,
--ModifiedUserID,
--HcHubCitiID,
--Message,
--Classification,
--Section,
--AdCopy,
--thumbnail,
--author,
--subcategory,
--PublishedTime,
--VideoLink,
--SubCategoryURL from  RssNewsFirstFeedNewsStagingTable

--set  identity_insert RssNewsFirstFeedNewsStagingTable_archive  off

-- TRUNCATE TABLE RssNewsFirstFeedNewsStagingTable
			
	-----------------------------------------CACHING CHANGES------MAIN MENU CLEAR CACHE URL-------------------------------------------------

		
				DECLARE @ClearCacheURL1 varchar(1000),@ClearCacheURL2 varchar(1000),@ClearCacheURL3 varchar(1000)
				SELECT @ClearCacheURL1= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema3'
				SELECT @ClearCacheURL2= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema2'
				SELECT @ClearCacheURL3= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubCitiMenuCacheURL' and ScreenName = 'Schema1'
				
				DECLARE @ClearCacheURL11 varchar(1000),@ClearCacheURL22 varchar(1000),@ClearCacheURL33 varchar(1000)
				SELECT @ClearCacheURL11= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema3'
				SELECT @ClearCacheURL22= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema2'
				SELECT @ClearCacheURL33= ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'HubRegionMenuCacheURL' and ScreenName = 'Schema1'

				SET @ClearCacheURL =@ClearCacheURL1 +','+ @ClearCacheURL2 +','+ @ClearCacheURL3 +','+ @ClearCacheURL11 +','+ @ClearCacheURL22 +','+ @ClearCacheURL33

	------------------------------------------------MAIN MENU CLEAR CACHE URL-------------------------------------------------	
				   
		  SET @Status=0	
					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRssNewsFirstFeedNewsInsertion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;




--commit
GO
