USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondRetailerXRef]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondRetailerXRef
Purpose					: To move data from staging table APIWishpondLocationData to production APIWishpondLocationData table.
Example					: usp_BatchWishPondRetailerXRef

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			11th Oct 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondRetailerXRef]
(
	
	--Output Variable 
	 @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			    DECLARE @APIPartnerID int
				SELECT @APIPartnerID = APIPartnerID  
				FROM APIPartner
				WHERE APIPartnerName = 'wishpond'
			
			--To get the count of rows received from API.
			INSERT INTO APIBatchLog (ExecutionDate
									, APIPartnerID
									, APIPartnerName
									, APIRowCount)
			SELECT GETDATE()
			, @APIPartnerID 
			, 'wishpond'
			, COUNT(1)
			FROM APIWishPondProductData
				MERGE dbo.APIWishPondRetailerXRef AS T
					USING APIWishpondLocationData AS S
					ON (T.WishPondRetailerID = S.ID) 
					WHEN NOT MATCHED BY TARGET
						THEN INSERT ([WishPondRetailerID]
								   ,[WishPondRetailerName]
								   ,[WishPondAddress]
								   ,[WishPondCity]
								   ,[WishPondState]
								   ,[WishPondLatitude]
								   ,[WishPondLongitude]
								   ,[WishPondPostalCode]
								   ,[DateCreated])
								   
							 VALUES( S.[ID]
									,S.[Name]
									,S.[Address1]
									,S.[City]
									,S.[State]
									,S.[Latitude]
								    ,S.[Longitude]
								    ,S.[Postalcode]
								    ,GETDATE())
					WHEN MATCHED 
						THEN UPDATE SET   T.[WishPondRetailerName] = S.[Name]
										 ,T.[WishPondAddress] = S.[Address1]
										 ,T.[WishPondCity] = S.[City]
										 ,T.[WishPondState] = S.[State]
										 ,T.[WishPondLatitude] = S.[Latitude]
										 ,T.[WishPondLongitude] = S.[Longitude]
										 ,T.[WishPondPostalCode] = S.[Postalcode]
										 ,T.[ModifiedDate] = GETDATE();

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondRetailerXRef.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
