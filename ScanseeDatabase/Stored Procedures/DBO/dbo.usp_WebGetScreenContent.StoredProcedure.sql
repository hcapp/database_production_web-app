USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebGetScreenContent]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebGetScreenContent]
Purpose					: To get Image of Product Summary page.
Example					:[usp_WebGetScreenContent]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			09th Feb 2012	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebGetScreenContent]
(
	@ConfigurationType varchar(50)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @Config varchar(50)
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'

			select ConfigurationType
					,ScreenContent													
					,ScreenName 
					,Active 
					,DateCreated
					,DateModified
					,sort
			INTO #APPconfig
			from (SELECT  ConfigurationType
					,ScreenContent	=CASE WHEN ScreenName Like '%image%' THEN  @Config+ScreenContent 
										  WHEN ConfigurationType='ShareURL' THEN @Config+ScreenContent  Else ScreenContent END														
					,ScreenName 
					,Active 
					,DateCreated
					,DateModified
					,sort=case when ScreenName like '%header' then 1 else 2 end
			FROM  AppConfiguration
			where ConfigurationType=@ConfigurationType) a
			order by sort

			SELECT rowid=IDENTITY(int,1,2),ConfigurationType
								,ScreenContent													
								,ScreenName 
								,Active 
								,DateCreated
								,DateModified
								,sort
			INTO #Image
			FROM #APPconfig
			WHERE sort=2
			ORDER BY ScreenName
			--select * from #B

			SELECT rowid=IDENTITY(int,2,2),ConfigurationType
								,ScreenContent													
								,ScreenName 
								,Active 
								,DateCreated
								,DateModified
								,sort
			INTO #Header
			FROM #APPconfig
			WHERE sort=1
			ORDER BY ScreenName
			--select * from #C 

			SELECT ConfigurationType
								,ScreenContent													
								,ScreenName 
								,Active 
								,DateCreated
								,DateModified		
			FROM (SELECT ConfigurationType
								,ScreenContent													
								,ScreenName 
								,Active 
								,DateCreated
								,DateModified
								,rowid
			FROM #Image
			UNION ALL
			SELECT ConfigurationType
								,ScreenContent													
								,ScreenName 
								,Active 
								,DateCreated
								,DateModified
								,rowid
			FROM #Header) Appconfig
			ORDER BY rowid
	
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebGetScreenContent].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
