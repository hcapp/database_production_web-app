USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_PreferredRetailersDisplayPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_PreferredRetailersDisplayPagination
Purpose					: To display User'd Preferred retailers.
Example					: usp_PreferredRetailersDisplayPagination 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			9th Aug 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_PreferredRetailersDisplayPagination] 
(
	  @UserID int
	, @LowerLimit int
	, @ScreenName varchar(50)
	--OutPut Variable
	, @NxtPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		--To get the row count for pagination.
		DECLARE @UpperLimit int 
		SELECT @UpperLimit = @LowerLimit + ScreenContent 
		FROM AppConfiguration 
		WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Pagination'
			AND Active = 1
		DECLARE @MaxCnt int
		--;WITH PrefRetail
		--AS
		--(
			--To fetch Retailer's Info 
			SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY R.RetailName)
				, R.RetailID  
				, R.RetailName  
				, Displayed = CASE WHEN UR.RetailID IS NOT NULL THEN 1 ELSE 0 END
			INTO #PrefRetail
			FROM Retailer R
				LEFT JOIN UserRetailPreference UR ON UR.RetailID = R.RetailID AND UR.UserID = @UserID AND UR.Active = 1
		--)
		--To capture max row number.
		SELECT @MaxCnt = MAX(Row_Num) FROM #PrefRetail	
		
		--this flag is a indicator to enable "More" button in the UI. 
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
			
				
		SELECT Row_Num  rowNumber
			, RetailID retailerId
			, RetailName retailerName
			, Displayed
		FROM #PrefRetail
		WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_PreferredRetailersDisplayPagination.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	 
		END;
		 
	END CATCH;
END;

GO
