USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetScreenStaticContent]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GetScreenStaticContent
Purpose					: To get Static text for the Screen.
Example					: usp_GetScreenStaticContent

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th Aug 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GetScreenStaticContent]
(
	@ScreenName varchar(50)
	
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='App Media Server Configuration'
		
		
		SELECT  sectionContent = CASE WHEN ConfigurationType = 'Static Screen Content' THEN ScreenContent 
									  ELSE (@Config+ScreenContent)
							     END
				, Section sectionNumber
				, ConfigurationType 
				, ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		 FROM AppConfiguration 
		 WHERE ScreenName = @ScreenName 
			AND ConfigurationType IN ('Static Screen Content', 'Welcome Video', 'Welcome Image')
			AND Active = 1
		UNION ALL	
		SELECT ScreenContent sectionContent
				, Section sectionNumber
				, ConfigurationType 
				, ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		FROM AppConfiguration 
		WHERE ScreenName = @ScreenName 
			AND ConfigurationType = 'Ticker'
		AND Active = 1
		ORDER BY CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
				, Section
		
		----To get static screen contect
		
		--SELECT  (@Config+ScreenContent) sectionContent
		--		,Section sectionNumber
		--		,ConfigurationType
		--		, ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		--FROM AppConfiguration 
		--WHERE ScreenName = @ScreenName AND 
		--	ConfigurationType IN ('Welcome Video', 'Welcome Image')
		--	AND Active = 1
		--UNION ALL
		--SELECT  ScreenContent sectionContent
		--		,Section sectionNumber 
		--		,ConfigurationType
		--		,ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		--FROM AppConfiguration 
		--WHERE ScreenName = @ScreenName AND 
		--	ConfigurationType IN ('Static Screen Content')
		--	AND Active = 1
		--UNION ALL	
		--SELECT    ScreenContent sectionContent
		--		, Section sectionNumber
		--		, ConfigurationType 
		--		, ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		--FROM AppConfiguration 
		--WHERE ScreenName = @ScreenName AND
		--	 ConfigurationType = 'Ticker'
		--	 AND Active = 1
		--ORDER BY CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		--		, Section
				
		-- SELECT  ScreenContent sectionContent
		--		, Section sectionNumber
		--		, ConfigurationType 
		--		, ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		-- FROM AppConfiguration 
		-- WHERE ScreenName = @ScreenName 
		--	AND ConfigurationType IN ('Static Screen Content', 'Welcome Video', 'Welcome Image')
		--	AND Active = 1
		--UNION ALL	
		--SELECT ScreenContent sectionContent
		--		, Section sectionNumber
		--		, ConfigurationType 
		--		, ORD = CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		--FROM AppConfiguration 
		--WHERE ScreenName = @ScreenName 
		--	AND ConfigurationType = 'Ticker'
		--AND Active = 1
		--ORDER BY CASE WHEN ConfigurationType =  'Ticker' THEN 1 ELSE 0 END
		--		, Section
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
