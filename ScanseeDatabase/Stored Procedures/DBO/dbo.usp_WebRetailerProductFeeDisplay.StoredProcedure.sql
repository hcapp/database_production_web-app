USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProductFeeDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerProductFeeDisplay
Purpose					: To Display the Billing Plans available.
Example					: usp_WebRetailerProductFeeDisplay

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			25th Jan 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerProductFeeDisplay]
(

	--Input Parameter(s)--	  
	
	--Output Variable-- 
	  
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--Display the all the Active Product fee plans available.			
		SELECT RetailerProductFeeID
		     , ProductFee
		     , LowerBoundViews
		     , HigherBoundViews
		     , ProductFeeDescription		     
		FROM RetailerProductFee 	
		WHERE ActiveFlag = 1
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerProductFeeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
