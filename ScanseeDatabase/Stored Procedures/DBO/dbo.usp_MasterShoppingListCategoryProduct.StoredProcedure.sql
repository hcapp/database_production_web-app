USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListCategoryProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_MasterShoppingListCategoryProduct  
Purpose     : To fetch User's Favorite Items.  
Example     : EXEC usp_MasterShoppingListCategoryProduct 2  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   27th Sep 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_MasterShoppingListCategoryProduct]  
(  
 @UserID int  
   
 --OutPut Variable  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY   
 
      --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'   
	  
  --To get categories, Products and CLR info of a User's Favorite list.  
  SELECT UP.UserProductID  
   , UP.UserID  
   , R.RetailID   
   , UP.ProductID  
   , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
   , ProductImagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END    
   , ISNULL(C.CategoryID ,0) CategoryID  
   , ISNULL(C.ParentCategoryID, 0) ParentCategoryID  
   , ISNULL(C.ParentCategoryName, 'Unassigned Products') ParentCategoryName   
   , C.SubCategoryID   
   , C.SubCategoryName   
   , Coupon.Coupon   
   , Loyalty.Loyalty  
   , Rebate.Rebate   
   , P.ProductShortDescription
   , P.ProductLongDescription
   --, ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0  
   --       WHEN (up.TodayListtItem=1) THEN 1  
   --   END   
  INTO #CLR      
  FROM UserProduct UP  
   LEFT JOIN Product P ON P.ProductID = UP.ProductID  
   LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID  
   LEFT JOIN Category C ON C.CategoryID = PC.CategoryID    
   LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID   
   LEFT JOIN Retailer R ON R.RetailID = UR.RetailID    
   OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon  
   OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty  
   OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate  
  WHERE UP.UserID = @UserID  
   AND UP.MasterListItem = 1   
   AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())  
    
    
  SELECT UserProductID userProductID  
   , CLR.UserID  
   , RetailID  
   , CLR.ProductID  
   , CLR.ProductName  
   , CLR.ProductImagePath  
   , CategoryID categoryID  
   , ParentCategoryID parentCategoryID  
   , ParentCategoryName parentCategoryName  
   , SubCategoryID subCategoryID  
   , SubCategoryName subCategoryName  
   , ProductShortDescription
   , ProductLongDescription
   , CLRFlag = CASE WHEN (COUNT(CLR.Coupon)+COUNT(CLR.Loyalty)+COUNT(CLR.Rebate)) > 0 THEN 1 ELSE 0 END  
   , coupon_Status = CASE WHEN COUNT(CLR.Coupon) = 0 THEN 'Grey'   
           ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
   , loyalty_Status = CASE WHEN COUNT(CLR.Loyalty) = 0 THEN 'Grey'  
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
          END  
   , rebate_Status = CASE WHEN COUNT(CLR.Rebate) = 0 THEN 'Grey'  
           ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
  INTO #DET  
  FROM #CLR CLR  
   LEFT JOIN UserCouponGallery UC ON UC.UserID = CLR.UserID AND UC.CouponID = CLR.Coupon  
   LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = CLR.UserID AND UL.LoyaltyDealID = CLR.Loyalty  
   LEFT JOIN UserRebateGallery UR ON UR.UserID = CLR.UserID AND UR.RebateID = CLR.Rebate   
  WHERE CLR.UserID = @UserID   
  GROUP BY UserProductID  
   , CLR.UserID  
   , RetailID  
   , CategoryID   
   , ParentCategoryID   
   , ParentCategoryName   
   , SubCategoryID   
   , SubCategoryName    
   , CLR.ProductID  
   , CLR.ProductName  
   , CLR.ProductImagePath     
   , ProductShortDescription
   , ProductLongDescription 
  --To get minimum price of the product, where the product is associated with Retailers.   
  SELECT UserProductID userProductID  
   , UserID  
   , D.RetailID  
   , D.ProductID  
   , ProductName  
   , ProductImagePath  
   , CategoryID categoryID  
   , ParentCategoryID parentCategoryID  
   , ParentCategoryName parentCategoryName  
   , SubCategoryID subCategoryID  
   , SubCategoryName subCategoryName  
   , MIN(RP.Price) AS Price  
   , CLRFlag    
   , ProductShortDescription
   , ProductLongDescription
   , coupon_Status    
   , loyalty_Status    
   , rebate_Status   
  FROM #DET D  
   LEFT JOIN RetailLocation RL ON D.RetailID = RL.RetailID   
   LEFT JOIN RetailLocationProduct RP ON RP.RetailLocationID = RL.RetailLocationID AND RP.ProductID = D.ProductID   
  WHERE D.RetailID IS NOT NULL  
  GROUP BY UserProductID    
   , UserID  
   , D.RetailID  
   , CategoryID    
   , ParentCategoryID    
   , ParentCategoryName    
   , SubCategoryID    
   , SubCategoryName   
   , D.ProductID  
   , ProductName  
   , ProductImagePath   
   , CLRFlag    
   , ProductShortDescription
   , ProductLongDescription
   , coupon_Status    
   , loyalty_Status    
   , rebate_Status   
  UNION ALL  
  --To get minimun price of a product, where the product is not associated with the Retailer.  
  SELECT UserProductID userProductID  
   , UserID  
   , D.RetailID  
   , D.ProductID  
   , ProductName  
   , ProductImagePath  
   , CategoryID categoryID  
   , ParentCategoryID parentCategoryID  
   , ParentCategoryName parentCategoryName  
   , SubCategoryID subCategoryID  
   , SubCategoryName subCategoryName  
   , MIN(RP.Price) AS Price  
   , CLRFlag    
   , ProductShortDescription
   , ProductLongDescription
   , coupon_Status    
   , loyalty_Status    
   , rebate_Status   
  FROM #DET D  
   LEFT JOIN RetailLocation RL ON D.RetailID = RL.RetailID   
   LEFT JOIN RetailLocationProduct RP ON RP.ProductID = D.ProductID   
  WHERE D.RetailID IS NULL  
  GROUP BY UserProductID    
   , UserID  
   , D.RetailID  
   , CategoryID    
   , ParentCategoryID    
   , ParentCategoryName    
   , SubCategoryID    
   , SubCategoryName  
   , D.ProductID  
   , ProductName  
   , ProductImagePath    
   , CLRFlag    
   , ProductShortDescription
   , ProductLongDescription
   , coupon_Status    
   , loyalty_Status    
   , rebate_Status   
  ORDER BY  ParentCategoryName, ProductName  
    
 --       --To get list of product in MasterShopingList for the user which has association with the retailer  
  
 --  SELECT UP.UserProductID  
 --   , UP.UserID  
 --   , UP.ProductID  
 --   , P.ProductName  
 --   , p.ProductImagePath   
 --   , MIN(Price) Price  
 --   , C.CategoryID  
 --   , ISNULL(C.ParentCategoryID, 0) ParentCategoryID  
 --   , ParentCategoryName   
 --   , C.SubCategoryID   
 --   , C.SubCategoryName   
 --   , Coupon.Coupon   
 --   , Loyalty.Loyalty  
 --   , Rebate.Rebate   
 --   , ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0  
 --          WHEN (up.TodayListtItem=1) THEN 1  
 --      END   
 --  INTO #Retailer    
 --  FROM UserProduct UP  
 --   INNER JOIN Product P ON P.ProductID = UP.ProductID  
 --   LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID  
 --   LEFT JOIN Category C ON C.CategoryID = PC.CategoryID    
 --   LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID   
 --   LEFT JOIN Retailer R ON R.RetailID = UR.RetailID    
 --   LEFT JOIN RetailLocation RL on RL.RetailID=R.RetailID  
 --   LEFT JOIN RetailLocationProduct RP on RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=p.ProductID  
 --   OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon  
 --   OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty  
 --   OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate  
 --  WHERE UP.UserID = @UserID AND UP.UserRetailPreferenceID IS NOT NULL  
 --   AND UP.MasterListItem = 1   
 --   AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())  
 --  GROUP BY UserProductID  
 --     , UP.UserID  
 --     , C.CategoryID   
 --     , ParentCategoryID   
 --     , ParentCategoryName   
 --     , SubCategoryID   
 --     , SubCategoryName    
 --     , UP.ProductID  
 --     , P.ProductName  
 --     , P.ProductImagePath  
 --     , RP.Price  
 --     , Coupon.Coupon  
 --     , Loyalty.Loyalty  
 --     , Rebate.Rebate  
 --     , UP.TodayListtItem  
        
 --  --product which is not Associated with the retailer  
  
 --  SELECT UP.UserProductID  
 --     , UP.UserID  
 --     , UP.ProductID  
 --     , P.ProductName   
 --     , p.ProductImagePath  
 --     , MIN(Price) Price  
 --     , ISNULL(C.CategoryID ,0) CategoryID  
 --     , ISNULL(C.ParentCategoryID, 0) ParentCategoryID  
 --     , ISNULL(C.ParentCategoryName, 'Unassigned Products') ParentCategoryName   
 --     , C.SubCategoryID   
 --     , C.SubCategoryName   
 --     , Coupon.Coupon   
 --     , Loyalty.Loyalty  
 --     , Rebate.Rebate   
 --     , ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0  
 --            WHEN (up.TodayListtItem=1) THEN 1  
 --        END   
 --    INTO #NoRetailer  
 --    FROM UserProduct UP  
 --     INNER JOIN Product P ON P.ProductID = UP.ProductID  
 --     LEFT JOIN RetailLocationProduct RP ON rp.ProductID=p.ProductID  
 --     LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID  
 --     LEFT JOIN Category C ON C.CategoryID = PC.CategoryID          
 --     OUTER APPLY fn_CouponDetails(UP.ProductID, NULL) Coupon  
 --     OUTER APPLY fn_LoyaltyDetails(UP.ProductID,NULL) Loyalty  
 --     OUTER APPLY fn_RebateDetails(UP.ProductID,NULL) Rebate  
 --    WHERE UP.UserID = @UserID AND UP.UserRetailPreferenceID IS NULL  
 --     AND UP.MasterListItem = 1   
 --     AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())  
 --    GROUP BY UserProductID  
 --     , UP.UserID  
 --     , C.CategoryID   
 --     , ParentCategoryID   
 --     , ParentCategoryName   
 --     , SubCategoryID   
 --     , SubCategoryName    
 --     , UP.ProductID  
 --     , P.ProductName  
 --     , P.ProductImagePath  
 --     , RP.Price  
 --     , Coupon.Coupon  
 --     , Loyalty.Loyalty  
 --     , Rebate.Rebate  
 --     , UP.TodayListtItem  
        
  
     
 -- SELECT UserProductID userProductID  
 --  , R.UserID  
 --  , R.ProductID  
 --  , R.ProductName   
 --  , R.Price  
 --  , R.ProductImagePath  
 --  , CategoryID categoryID  
 --  , ParentCategoryID parentCategoryID  
 --  , ParentCategoryName parentCategoryName  
 --  , SubCategoryID subCategoryID  
 --  , SubCategoryName subCategoryName  
 --  , ButtonFlag  
 --  , CLRFlag = CASE WHEN (COUNT(R.Coupon)+COUNT(R.Loyalty)+COUNT(R.Rebate)) > 0 THEN 1 ELSE 0 END  
 --  , coupon_Status = CASE WHEN COUNT(R.Coupon) = 0 THEN 'Grey'   
 --          ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
 --        END  
 --  , loyalty_Status = CASE WHEN COUNT(R.Loyalty) = 0 THEN 'Grey'  
 --        ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
 --         END  
 --  , rebate_Status = CASE WHEN COUNT(R.Rebate) = 0 THEN 'Grey'  
 --          ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
 --        END  
 -- FROM #Retailer R  
 --  LEFT JOIN UserCouponGallery UC ON UC.UserID = R.UserID AND UC.CouponID = R.Coupon  
 --  LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = R.UserID AND UL.LoyaltyDealID = R.Loyalty  
 --  LEFT JOIN UserRebateGallery UR ON UR.UserID = R.UserID AND UR.ProductID = R.ProductID   
 -- WHERE R.UserID = @UserID   
 -- GROUP BY UserProductID  
 --  , R.UserID  
 --  , CategoryID   
 --  , ParentCategoryID   
 --  , ParentCategoryName   
 --  , SubCategoryID   
 --  , SubCategoryName    
 --  , R.ProductID  
 --  , R.ProductName  
 --  , R.ProductImagePath  
 --  , R.Price  
 --  , ButtonFlag  
 -- --ORDER BY  ParentCategoryName, ProductName  
    
 --UNION ALL  
   
           
 -- SELECT UserProductID userProductID  
 --  , NR.UserID  
 --  , NR.ProductID  
 --  , NR.ProductName   
 --  , NR.Price  
 --  , NR.ProductImagePath  
 --  , CategoryID categoryID  
 --  , ParentCategoryID parentCategoryID  
 --  , ParentCategoryName parentCategoryName  
 --  , SubCategoryID subCategoryID  
 --  , SubCategoryName subCategoryName  
 --  , ButtonFlag  
 --  , CLRFlag = CASE WHEN (COUNT(NR.Coupon)+COUNT(NR.Loyalty)+COUNT(NR.Rebate)) > 0 THEN 1 ELSE 0 END  
 --  , coupon_Status = CASE WHEN COUNT(NR.Coupon) = 0 THEN 'Grey'   
 --          ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
 --        END  
 --  , loyalty_Status = CASE WHEN COUNT(NR.Loyalty) = 0 THEN 'Grey'  
 --        ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
 --         END  
 --  , rebate_Status = CASE WHEN COUNT(NR.Rebate) = 0 THEN 'Grey'  
 --          ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
 --        END  
 -- FROM #NoRetailer NR  
 --  LEFT JOIN UserCouponGallery UC ON UC.UserID = NR.UserID AND UC.CouponID = NR.Coupon  
 --  LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = NR.UserID AND UL.LoyaltyDealID = NR.Loyalty  
 --  LEFT JOIN UserRebateGallery UR ON UR.UserID = NR.UserID AND UR.ProductID = NR.ProductID   
 -- WHERE NR.UserID = @UserID   
 -- GROUP BY UserProductID  
 --  , NR.UserID  
 --  , CategoryID   
 --  , ParentCategoryID   
 --  , ParentCategoryName   
 --  , SubCategoryID   
 --  , SubCategoryName    
 --  , NR.ProductID  
 --  , NR.ProductName  
 --  , NR.ProductImagePath  
 --  , NR.Price  
 --  , ButtonFlag  
 -- ORDER BY  ParentCategoryName, ProductName  
    
    
    
   
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_MasterShoppingListCategoryProduct.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
