USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterValueDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterValueDeletion]
Purpose                  : To delete Filter Values.
Example                  : [usp_WebAdminFilterValueDeletion]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterValueDeletion]
(

      --Input Input Parameter(s)--     
	    @FilterValueID Int	 
      
      
      --Output Variable--	  
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY             
			
	         -- To delete Filter Values with association table.
			 DELETE FROM AdminFilterValueAssociation 
			 WHERE AdminFilterValueID =@FilterValueID 

			 DELETE FROM RetailerFilterAssociation 
			 WHERE AdminFilterValueID =@FilterValueID

		     DELETE FROM AdminFilterValue 
			 WHERE AdminFilterValueID =@FilterValueID 

        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterValueDeletion.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;





GO
