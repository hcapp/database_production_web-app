USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListCouponDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListCouponDisplay
Purpose					: To display Coupon associated with product.
Example					: usp_WishListCouponDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20th Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListCouponDisplay]
(
	@UserID int
	, @ProductID int
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)    
	, @ZipCode varchar(10)    
	, @Radius int
	
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--To get Media Server Configuration.  
		  DECLARE @RetailConfig varchar(50)    
		  SELECT @RetailConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Retailer Media Server Configuration' 
  
		--While User search by Zipcode, coordinates are fetched from GeoPosition table.    
		IF (@Latitude IS NULL AND @Longitude IS NULL )    
		BEGIN    
			SELECT @Latitude = Latitude     
			  , @Longitude = Longitude     
			FROM GeoPosition     
			WHERE PostalCode = @ZipCode     
		END    
		--If radius is not passed, getting user preferred radius from UserPreference table.    
		IF @Radius IS NULL     
		BEGIN     
			SELECT @Radius = LocaleRadius  
			FROM dbo.UserPreference WHERE UserID = @UserID    
		END
		IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL )    
		BEGIN
		--To get user's locationwise retail store infomation
		SELECT DISTINCT ProductID
			   , RetailID 
			   , RetailLocationID
			   , Distance
			   --, SalePrice
			   --, Price
			  
		INTO #Details
		FROM (		
				SELECT UP.ProductID
					, RL.RetailID 
					, RL.RetailLocationID
					, Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214     
					--, MIN(RP.SalePrice) SalePrice
					--, MIN(RP.Price) Price
					
				FROM UserProduct UP
					INNER JOIN RetailLocationProduct RP ON RP.ProductID = UP.ProductID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID 
				WHERE UP.UserID = @UserID 
					AND UP.WishListItem = 1
					AND RP.ProductID = @ProductID AND RL.Active = 1
				--GROUP BY UP.ProductID
				--	, RL.RetailID 
				--	, RL.RetailLocationID
				--	, RetailLocationLatitude
				--	, RetailLocationLongitude
			 ) D
		WHERE Distance <= ISNULL(@Radius, 5)
		
		--To get Coupons available for User's Wish List Products   
		SELECT DISTINCT C.CouponID   
			,CouponName  
			,CouponDiscountType couponDiscountType  
			,CouponDiscountAmount couponDiscountAmount  
			,CouponDiscountPct couponDiscountPct  
			,CouponShortDescription  
			,CouponLongDescription  
			,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN DBO.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																										CASE WHEN WebsiteSourceFlag = 1 
																											THEN @RetailConfig
																											+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																											+CouponImagePath 
																									    ELSE CouponImagePath 
																									    END
																								 END 
					 END  
			,CouponDateAdded couponDateAdded  
			,CouponStartDate couponStartDate  
			,CouponExpireDate couponExpireDate
			,usage = CASE WHEN CG.CouponID IS NULL THEN 'Red' ELSE 'Green' END			
		FROM Coupon C
		INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID		
		INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
		INNER JOIN #Details D ON D.ProductID = CP.ProductID AND D.RetailID = CR.RetailID AND D.RetailLocationID = CR.RetailLocationID 
		LEFT JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID  
		WHERE CouponExpireDate >= GETDATE()  
		END
		
		IF ( @Latitude IS NULL AND @Longitude IS NULL) AND @ZipCode IS NULL
		BEGIN
			--SELECT ProductID
			--	   , RetailID 
			--	   , RetailLocationID
			--	   , Distance
			--	   , SalePrice
			--	   , Price
				  
			--INTO #Details1
			--FROM (		
			--		SELECT UP.ProductID
			--			, RL.RetailID 
			--			, RL.RetailLocationID
			--			, 0 Distance
			--			, MIN(RP.SalePrice) SalePrice
			--			, MIN(RP.Price) Price
						
			--		FROM UserProduct UP
			--			INNER JOIN RetailLocationProduct RP ON RP.ProductID = UP.ProductID 
			--			INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID 
			--		WHERE UP.UserID = @UserID 
			--			AND UP.WishListItem = 1
			--			AND UP.ProductID = @ProductID 
			--		GROUP BY UP.ProductID
			--			, RL.RetailID 
			--			, RL.RetailLocationID
			--	 ) D
				 
				 --To get Coupons available for User's Wish List Products   
					SELECT DISTINCT C.CouponID   
						,CouponName  
						,CouponDiscountType couponDiscountType  
						,CouponDiscountAmount couponDiscountAmount  
						,CouponDiscountPct couponDiscountPct  
						,CouponShortDescription  
						,CouponLongDescription  
						,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN DBO.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																										CASE WHEN WebsiteSourceFlag = 1 
																											THEN @RetailConfig
																											+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																											+CouponImagePath 
																									    ELSE CouponImagePath 
																									    END
																								 END 
																	END  
						,CouponDateAdded couponDateAdded  
						,CouponStartDate couponStartDate  
						,CouponExpireDate couponExpireDate
						,usage = CASE WHEN CG.CouponID IS NULL THEN 'Red' ELSE 'Green' END			
					FROM Coupon C
					INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID
					LEFT JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID AND UserID = @UserID 
					LEFT JOIN CouponRetailer CR ON CR.CouponID=C.CouponID
					--INNER JOIN #Details1 D ON D.ProductID = CP.ProductID AND D.RetailID = CR.RetailID   
					WHERE CouponExpireDate >= GETDATE()  
						AND CP.ProductID = @ProductID 
					
		END
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListCouponDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;




GO
