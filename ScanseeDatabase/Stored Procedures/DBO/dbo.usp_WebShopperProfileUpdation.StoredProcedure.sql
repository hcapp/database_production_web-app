USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebShopperProfileUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebShopperProfileUpdation
Purpose					: To update user information.
Example					: usp_WebShopperProfileUpdation

History
Version		Date			 Author			Change Description
--------------------------------------------------------------- 
1.0			21st March 2012	 Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebShopperProfileUpdation]
(
	 @UserID int
	,@FirstName Varchar(20)
	,@LastName Varchar(50)
	,@Address1 Varchar(50)
	,@Address2 Varchar(50)
	,@State char(2)
	,@City varchar(50)
	,@PostalCode Varchar(10)
	,@MobilePhoneNumber  Varchar(20)
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE Users 
			SET FirstName=@FirstName
				,Lastname=@LastName
				,Address1=@Address1
				,Address2=@Address2
				,State=@State
				,City=@City
				,PostalCode=@PostalCode
				,MobilePhone=@MobilePhoneNumber
			WHERE UserID=@UserID
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebShopperProfileUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
