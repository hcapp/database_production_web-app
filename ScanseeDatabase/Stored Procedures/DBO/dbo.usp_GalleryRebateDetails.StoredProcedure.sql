USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryRebateDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GalleryRebateDetails
Purpose					: to display rebates details in the user rebate gallery
Example					: usp_GalleryRebateDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15th Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryRebateDetails]
(
	@UserID int
	,@RebateID int
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
   			SELECT	ISNULL(ProductName,'') ProductName
   					, ISNULL(RP.ProductID,0) ProductID
				    , R.RebateID
					, RebateName
					, RebateLongDescription
					, RebateStartDate
					, RebateEndDate 
					--, UsedFlag=(SELECT CASE WHEN R.RebateID=UR.RebateID THEN 1 ELSE 0 END FROM UserRebateGallery UR WHERE UserID=@UserID AND RebateID=@RebateID)
			FROM Rebate R
				LEFT JOIN RebateProduct RP ON RP.RebateID=R.RebateID
				LEFT JOIN Product P ON P.ProductID=RP.ProductID
			WHERE R.RebateID=@RebateID
			

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GalleryRebateDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
