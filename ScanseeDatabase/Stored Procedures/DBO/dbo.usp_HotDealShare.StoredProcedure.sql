USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealShare]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HotDealShare
Purpose					: 
Example					: usp_HotDealShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th DEC 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HotDealShare]
(
	@HotDealID int
	--Output Variable 
	, @HotDealExpired bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			SELECT  HotDealName hotDealName
				   ,HotDealURL hdURL
				   ,HotDealStartDate hDStartDate
				   ,HotDealEndDate hDEndDate
			FROM ProductHotDeal
			WHERE ProductHotDealID=@HotDealID
				AND GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate
			
			SELECT @HotDealExpired=CASE WHEN GETDATE()>HotDealEndDate Then 0 
										WHEN GETDATE()<=HotDealEndDate THEN 1 END FROM ProductHotDeal
			WHERE ProductHotDealID=@HotDealID
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HotDealShare.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
