USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchLog]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_BatchLog 
Purpose                 : To get log details.  
Example                 : usp_ProductDetails 2,1   
  
History  
Version           Date              Author           Change Description  
-----------------------------------------------------------------------   
1.0               14th June 2012    SPAN      Initial Version  
-----------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_BatchLog]  
(  
        @APIPartnerName varchar(100)
      , @Date DATETIME   
      --OutPut Variable  
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY 
       --To update the content version changes for all API's.   
       IF @APIPartnerName = 'All'
       BEGIN  
		SELECT APILogID
			,REPLACE(CONVERT(VARCHAR(10), ExecutionDate, 102),'.', '-') +' '+ CONVERT(VARCHAR(10), ExecutionDate, 108) ExecutionDate
			,Status
			,Reason
			,APIPartnerName BatchProcessName
			,APIRowCount rowsRecieved
			,ProcessedRowCount rowsProcessed
			,APIPartnerID 
		FROM APIBatchLog 
		WHERE CONVERT(DATE,ExecutionDate) = @Date 
		AND APIPartnerName NOT IN('Living Social','Plum District','CommissionJunction', 'Mamasource','Restaurant.com','Eversave','Voice Daily Deals','CBS Local','Gilt City','CrowdSavings')
	  END
	  
	  --To update the content version changes only for CellFire API.  
	  IF @APIPartnerName = 'CellFire'
	  BEGIN
		SELECT APILogID
			,REPLACE(CONVERT(VARCHAR(10), ExecutionDate, 102),'.', '-') +' '+ CONVERT(VARCHAR(10), ExecutionDate, 108) ExecutionDate
			,Status
			,Reason
			,APIPartnerName BatchProcessName
			,APIRowCount rowsRecieved
			,ProcessedRowCount rowsProcessed
			,APIPartnerID 
		FROM APIBatchLog 
		WHERE CONVERT(DATE,ExecutionDate) = @Date 
		AND APIPartnerName = @APIPartnerName
	  END
	  
      
      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_BatchLog.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;

GO
