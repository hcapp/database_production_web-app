USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCreatedPageCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCreatedPageCreation
Purpose					: To Create a Retailer Custom Page(Landing Page).
Example					: usp_WebRetailerCreatedPageCreation

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			17th May 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCreatedPageCreation]
(

	--Input Parameter(s)--
	 @RetailID int	
   , @RetailLocationID varchar(MAX)
   , @PageTitle varchar(255)
   , @StartDate datetime
   , @EndDate datetime
   , @Image varchar(1000)
   , @PageDescription varchar(1000)
   , @ShortDescription varchar(255)
   , @LongDescription varchar(1000)
   , @MediaPath varchar(max)
   , @ExternalFlag varchar(max)
   	
	--Output Variable--	  	 
	, @PageID int output
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @QRRetailerCustomPageID INT
			DECLARE @ProductMediaTypeID int
		 
			SELECT @ProductMediaTypeID = ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType LIKE 'Other Files'
			
			--Create a record in QRRetailerCustomPage
			 INSERT INTO QRRetailerCustomPage(
											  QRTypeID
											, RetailID
											, Pagetitle
											, StartDate
											, EndDate
											, [Image]
											, PageDescription
											, ShortDescription
											, LongDescription
											, DateCreated)
								VALUES(
										 (SELECT QRTypeID FROM QRTypes WHERE QRTypeName = 'Landing Page')
										, @RetailID
										, @PageTitle
										, @StartDate
										, @EndDate
										, @Image
										, @PageDescription
										, @ShortDescription
										, @LongDescription
										, GETDATE()
										)
						  --Capture the PageID			
						  SET @QRRetailerCustomPageID = SCOPE_IDENTITY()
						  
						  SET @PageID = @QRRetailerCustomPageID
				--Make Retailer Location Associations.
				INSERT INTO QRRetailerCustomPageAssociation(
															  QRRetailerCustomPageID
															, RetailID
															, RetailLocationID
															, DateCreated)
												SELECT @QRRetailerCustomPageID
													 , @RetailID
													 , [Param]
													 , GETDATE()
												FROM dbo.fn_SplitParam(@RetailLocationID, ',')
				
				IF @MediaPath IS NOT NULL
				BEGIN
								
					--Split the Comma Separated Media												
					SELECT Row = IDENTITY(INT, 1, 1)
						 , [Param] Media
					INTO #Media
					FROM dbo.fn_SplitParam(@MediaPath, ',')	
	                
					--Split the Comma Separated External Flags
					SELECT RowNum = IDENTITY(INT, 1, 1)
						 , [Param] ExternalFlag
					INTO #ExternalFlag
					FROM dbo.fn_SplitParam(@ExternalFlag, ',')	
	                
	                												
					--Make Media Association.
					INSERT INTO QRRetailerCustomPageMedia(
															  QRRetailerCustomPageID
															, MediaTypeID
															, MediaPath
															, ExternalFlag
															, DateCreated
														  )
													SELECT
															 @QRRetailerCustomPageID
														   , @ProductMediaTypeID
														   , M.Media
														   , E.ExternalFlag
														   , GETDATE()
													FROM #Media M
													INNER JOIN #ExternalFlag E ON M.Row = E.RowNum													 
			    END
		--Confirmation of Success.
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCreatedPageCreation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
