USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPushNotificationmessages]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GetPushNotificationmessages]
Purpose					: To get the Push Notification messages.
Example					: usp_GetPushNotificationmessages

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Sept 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GetPushNotificationmessages]
(
	--Output Variable 
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			SELECT PushNotificationMessageNumber screenValue
				 , PushNotificationMessage screenContent
			FROM PushNotificationMessages
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GetPushNotificationmessages.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
