USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondRetLocDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondRetLocDataPorting
Purpose					: To port data into RetailLocationProduct
Example					: usp_BatchWishPondRetLocDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Oct 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondRetLocDataPorting]
(
	
	--Output Variable 
	 @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
				SELECT RowNum = IDENTITY(INT, 1, 1)
					 , ScanSeeRetailLocID
					 , ProductID
					 , Description
					 , SalePrice
					 , DateCreated
				INTO #Temp
				FROM (SELECT WR.ScanSeeRetailLocID ScanSeeRetailLocID
											  , P.ProductID ProductID
											  , WP.Description Description
											  , WP.SalePrice SalePrice
											  ,GETDATE() DateCreated
										FROM APIWishPondProductData WP
											INNER JOIN APIWishpondLocationData WL ON WL.Id = WP.APIWishpondLocId 
											INNER JOIN dbo.APIWishPondDealXRef WD ON WD.WishPondDealID = WP.DealId and WD.WishPondItemID = WP.ItemID and WL.Id = WD.WishPondRetailerID
											INNER JOIN Product P ON P.ProductID = WD.ScanSeeProductID
											INNER JOIN dbo.APIWishPondRetailerXRef WR ON WR.WishPondRetailerID = WD.WishPondRetailerID
										WHERE WR.ScanSeeRetailLocID IS NOT NULL
												AND WD.ScanSeeProductID IS NOT NULL)Src


					 MERGE RetailLocationProduct AS T
					 USING (SELECT ScanSeeRetailLocID
							,	ProductID
							, Description
							, SalePrice
							, DateCreated
							FROM #Temp WHERE RowNum IN(SELECT MAX(RowNum) FROM #Temp GROUP BY ScanSeeRetailLocID, ProductID)
						  ) AS S
										
					  ON (T.RetailLocationID=s.ScanSeeRetailLocID) and (T.ProductID=S.ProductID)
					  WHEN NOT MATCHED BY TARGET 
						THEN INSERT([RetailLocationID]
							   ,[ProductID]
							   ,[RetailLocationProductDescription]
							   ,[SalePrice]
							   ,[HotDeal]
							   ,[DateCreated])
							 VALUES( S.ScanSeeRetailLocID
							   , S.ProductID
							   , S.Description
							   , S.Saleprice
							   , 0
							   , S.DateCreated)
					   WHEN MATCHED  
						 THEN UPDATE SET [RetailLocationID]= S.ScanSeeRetailLocID
							   ,[ProductID] = S.ProductID
							   ,[RetailLocationProductDescription] = S.Description
							   ,[SalePrice] = S.Saleprice
							   ,[DateUpdated]=S.DateCreated;	
							
							
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondRetLocDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
