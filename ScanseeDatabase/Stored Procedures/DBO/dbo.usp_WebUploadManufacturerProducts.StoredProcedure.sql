USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UploadManufacturerProducts
Purpose					: To upload product data from stage table UploadManufacturerProducts to production table Products
Example					: usp_UploadManufacturerProducts

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerProducts]
(
	 @ManufacturerID int
	,@UserID int
	,@ProductFileName Varchar(255)
	,@ManufacturerLogID int
	 
	--Output Variable 
	, @SuccessCount int output
	, @TotalRows int output
	, @FailureCount int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			
			--To get Manufacturer name
			DECLARE @manufname varchar(255)
			SELECT @manufname=ManufName
			FROM Manufacturer
			WHERE ManufacturerID=@ManufacturerID
			
			--To capture execution start date & time
			DECLARE @startDate datetime
			SET @startDate=GETDATE()
			
			------- Processing Product Data --------------------------------------
			
				---To get the products that are not already existing...
				SELECT U.UploadManufacturerProductID
						,U.ProductName
						,U.ScanCode
						,U.ScanType
						,U.ProductExpirationDate
						,U.ProductImagePath
						,U.Category
						,U.ProductShortDescription
						,U.ProductLongDescription
						,U.ModelNumber
						,U.SuggestedRetailPrice
						,U.Weight
						,U.WeightUnits
						,U.ManufacturerProdcutUrl
						,U.Audio
						,U.Video
						,U.Image
						,U.ManufacturerId
						,U.UserID
						,U.DateCreated
						,U.OtherFiles
				INTO #NewProducts
				FROM UploadManufacturerProducts U
				LEFT JOIN Product P ON U.ScanCode=P.ScanCode
				WHERE U.UserID=@UserID 
					AND U.ManufacturerId=@ManufacturerID
					AND P.ScanCode IS NULL 					
				
				--select * from #NewProducts
				---To get the products that are already existing...
				SELECT U.UploadManufacturerProductID
						,U.ProductName
						,U.ScanCode
						,U.ScanType
						,U.ProductExpirationDate
						,U.ProductImagePath
						,U.Category
						,U.ProductShortDescription
						,U.ProductLongDescription
						,U.ModelNumber
						,U.SuggestedRetailPrice
						,U.Weight
						,U.WeightUnits
						,U.ManufacturerProdcutUrl
						,U.Audio
						,U.Video
						,U.Image
						,U.ManufacturerId
						,U.UserID
						,U.DateCreated
						,U.OtherFiles
				INTO #ExistingProducts
				FROM UploadManufacturerProducts U
				INNER JOIN Product P ON U.ScanCode=P.ScanCode
				WHERE U.UserID=@UserID 
					AND U.ManufacturerId=@ManufacturerID
				 
					 
				
				--To get the mandatory value missing products information.			
				SELECT UploadManufacturerProductID
						,ProductName
						,ScanCode
						,ScanType
						,ProductExpirationDate
						,ProductImagePath
						,Category
						,ProductShortDescription
						,ProductLongDescription
						,ModelNumber
						,SuggestedRetailPrice
						,Weight
						,WeightUnits
						,ManufacturerProdcutUrl
						,Audio
						,Video
						,Image
						,ManufacturerId
						,UserID
						,DateCreated
						,OtherFiles
				INTO #CheckMandatory
				FROM #NewProducts
				WHERE UserID=@UserID
				 AND ManufacturerId=@ManufacturerID
				 AND ProductName IS NOT NULL 
				 AND ScanCode IS NOT NULL
				 AND ScanType IS NOT NULL
				 AND ProductExpirationDate IS NOT NULL
				 --AND ProductImagePath IS NOT NULL
				 AND Category IS NOT NULL
				 AND Audio IS  NOT NULL
				 AND Video IS  NOT NULL	
					
					--To filter the duplicates.
			 		SELECT UploadManufacturerProductID
						,ProductName
						,ScanCode
						,ScanType
						,ProductExpirationDate
						,ProductImagePath
						,Category
						,ProductShortDescription
						,ProductLongDescription
						,ModelNumber
						,SuggestedRetailPrice
						,Weight
						,WeightUnits
						,ManufacturerProdcutUrl
						,Audio
						,Video
						,Image
						,ManufacturerId
						,UserID
						,DateCreated
						,OtherFiles
					INTO #UniqueProducts
			 		FROM #CheckMandatory
			 		WHERE UploadManufacturerProductID IN(
															 SELECT	MIN(UploadManufacturerProductID)UploadManufacturerProductID
															 FROM #CheckMandatory
															 GROUP BY ScanCode)
				
				
				--Formula: 1.) Total Rows = (No. of rows in Discarded table while inserting into stage) + (No. of rows in the stage)
				SELECT @TotalRows = COUNT(1)
				FROM UploadManufacturerProducts
				WHERE ManufacturerId = @ManufacturerID
				AND UserID =@UserID 
					
				--Total Rows
				SELECT @TotalRows = @TotalRows + COUNT(1)
				FROM UploadManufacturerDiscardedProducts
				WHERE UploadManufacturerLogID = @ManufacturerLogID
													 
				--Insert into Product Table from Upload stage table
				INSERT INTO [Product]
					   ([ManufacturerID]
					   ,[ScanCode]
					   ,[ScanTypeID]
					   ,[Service]
					   ,[ProductName]
					   ,[ProductShortDescription]
					   ,[ProductLongDescription]
					   ,[ModelNumber]
					   ,[ProductImagePath]
					   ,[SuggestedRetailPrice]
					   ,[Weight]
					   ,[WeightUnits]
					   ,[ProductExpirationDate]
					   ,[ProductAddDate]
					   ,[ProductModifyDate]
					   ,[ManufacturerProuductURL]
					   ,WebsiteSourceFlag)
				SELECT [ManufacturerID]
					   ,[ScanCode]
					   ,scantype
					   ,1 
					   ,[ProductName]
					   ,[ProductShortDescription]
					   ,[ProductLongDescription]
					   ,[ModelNumber]
					   ,ISNULL([ProductImagePath], (SELECT TOP 1 [Param] FROM fn_SplitParam([Image],'|')))
					   ,[SuggestedRetailPrice]
					   ,[Weight]
					   ,[WeightUnits]
					   ,[ProductExpirationDate]
					   ,GETDATE() 
					   ,GETDATE() 
					   ,ManufacturerProdcutUrl
					   ,1
				FROM #UniqueProducts	
				
				UPDATE #UniqueProducts SET [Image] = CASE WHEN U.ProductImagePath IS NULL THEN SUBSTRING(U.Image,CHARINDEX( '|', U.Image) + 1, LEN(U.Image)) ELSE U.Image END
				FROM #UniqueProducts U
				INNER JOIN Product P ON U.ScanCode = P.ScanCode
				WHERE U.ProductImagePath IS NULL

				--To insert data into ProductCategory table
				--To get the categoryid for the products
				SELECT ScanCode,CategoryID
				INTO #Category
				FROM Category C
				INNER JOIN #UniqueProducts N ON C.ParentCategoryName=N.Category 
				WHERE ManufacturerId=@ManufacturerID
				AND UserID = @UserID
				
				INSERT INTO [ProductCategory]
						   ([ProductID]
						   ,[CategoryID]
						   ,[DateAdded])
				SELECT ProductID
					  ,CategoryID
					  ,GETDATE()
				FROM #Category C
				INNER JOIN Product P ON C.ScanCode=p.ScanCode
				WHERE CategoryID IS NOT NULL		
		
			------Processing Product Media data------------------------------			
				--To get Product Media type IDs
				DECLARE @VideoFiles INT, @AudioFiles INT, @ImageFiles INT, @OtherFiles INT
				
				SELECT @VideoFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Video Files' 
				SELECT @AudioFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Audio Files' 
				SELECT @ImageFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Image Files'
				SELECT @OtherFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Other Files' 
		
				--To Process multiple Video, Audio, Image and Other files.
				DECLARE @productid int,@Audio Varchar(max),@Video Varchar(max),@Image Varchar(max),@Other Varchar(max)
				--Declare Cursor to process product and Multiple files.
				DECLARE Cur_ProductMedia Cursor FOR
					SELECT ProductID
							,Audio
							,Video
							,Image
							,OtherFiles 
					FROM  #UniqueProducts N
					INNER JOIN Product P ON P.Scancode = N.Scancode
				OPEN Cur_ProductMedia
				--to process product by product info.	
				FETCH NEXT FROM Cur_ProductMedia INTO @productid, @Audio,@Video,@Image,@Other
					WHILE @@Fetch_Status = 0 
					  BEGIN
						--To split multiple Audio files and to insert the same into ProductMedia table.
						IF @Audio IS NOT NULL
						BEGIN
							--SELECT @Audio
							INSERT INTO [ProductMedia]
							   ([ProductID]
							   ,[ProductMediaTypeID]
							   ,[DateAdded]
							   ,[ProductMediaPath])
							SELECT @productid
								   ,@AudioFiles
								   ,GETDATE()
								   ,Param
							FROM fn_SplitParam(@audio,'|')
						END
						--To split multiple Video files and to insert the same into ProductMedia table.
						IF @Video IS NOT NULL
						BEGIN
						--SELECT @Video
							INSERT INTO [ProductMedia]
							   ([ProductID]
							   ,[ProductMediaTypeID]
							   ,[DateAdded]
							   ,[ProductMediaPath])
							SELECT @ProductID
								   ,@VideoFiles
								   ,GETDATE()
								   ,Param
							FROM fn_SplitParam(@Video,'|')
						END
						--To split multiple Image files and to insert the same into ProductMedia table.
						
						IF @Image IS NOT NULL
						BEGIN 
						--SELECT @Image
							INSERT INTO [ProductMedia]
							   ([ProductID]
							   ,[ProductMediaTypeID]
							   ,[DateAdded]
							   ,[ProductMediaPath])
						   SELECT @ProductID
								   ,@ImageFiles
								   ,GETDATE()
								   ,param
						   FROM fn_SplitParam(@Image,'|')
						END
					   --To split multiple Other files and to insert the same into ProductMedia table.
					   
					   IF @Other IS NOT NULL
					   BEGIN
					   --SELECT @Other
						   INSERT INTO [ProductMedia]
							   ([ProductID]
							   ,[ProductMediaTypeID]
							   ,[DateAdded]
							   ,[ProductMediaPath])
						   SELECT @ProductID
								   ,@OtherFiles
								   ,GETDATE()
								   ,param
						   FROM fn_SplitParam(@Other,'|')
					   END
				--Increment (to get next product info)		
				FETCH NEXT FROM Cur_ProductMedia INTO @productid, @Audio,@Video,@Image,@OtherFiles
					  END
				CLOSE Cur_ProductMedia
				DEALLOCATE Cur_ProductMedia				
			
			---To capture execution end date & time
			DECLARE @EndDate datetime
			SET @EndDate=GETDATE()	
							
			--To insert the failure records of the product.
						
			INSERT INTO [UploadManufacturerDiscardedProducts]
					   ([UploadManufacturerLogID]
					   ,[ProductName]
					   ,[ScanCode]
					   ,[ScanType]
					   ,[ProductExpirationDate]
					   ,[ProductImagePath]
					   ,[Category]
					   ,[ProductShortDescription]
					   ,[ProductLongDescription]
					   ,[ModelNumber]
					   ,[SuggestedRetailPrice]
					   ,[Weight]
					   ,[WeightUnits]
					   ,[ManufacturerProdcutUrl]
					   ,[Audio]
					   ,[Video]
					   ,[Image]
					   ,[ManufacturerId]
					   ,[UserID]
					   ,[DateCreated]
					   ,[OtherFiles]
					   ,ProductFileName
					   ,ReasonForDiscarding)
					   
			SELECT DISTINCT ManufacturerLogID 
				   ,[ProductName]
				   ,[ScanCode]
				   ,[ScanType]
				   ,[ProductExpirationDate]
				   ,[ProductImagePath]
				   ,[Category]
				   ,[ProductShortDescription]
				   ,[ProductLongDescription]
				   ,[ModelNumber]
				   ,[SuggestedRetailPrice]
				   ,[Weight]
				   ,[WeightUnits]
				   ,[ManufacturerProdcutUrl]
				   ,[Audio]
				   ,[Video]
				   ,[Image]
				   ,[ManufacturerId]
				   ,[UserID]
				   ,[DateCreated]
				   ,[OtherFiles]
				   ,ProductFileName 
				   ,Reason
		     FROM
			(SELECT @ManufacturerLogID ManufacturerLogID 
				   ,[ProductName]
				   ,[ScanCode]
				   ,[ScanType]
				   ,[ProductExpirationDate]
				   ,[ProductImagePath]
				   ,[Category]
				   ,[ProductShortDescription]
				   ,[ProductLongDescription]
				   ,[ModelNumber]
				   ,[SuggestedRetailPrice]
				   ,[Weight]
				   ,[WeightUnits]
				   ,[ManufacturerProdcutUrl]
				   ,[Audio]
				   ,[Video]
				   ,[Image]
				   ,[ManufacturerId]
				   ,[UserID]
				   ,[DateCreated]
				   ,[OtherFiles]
				   ,@ProductFileName ProductFileName 
				   ,'ScanCode Already Exists.' Reason
			FROM #ExistingProducts 			
			
			UNION 
			
			SELECT @ManufacturerLogID ManufacturerLogID 
				   ,[ProductName]
				   ,[ScanCode]
				   ,[ScanType]
				   ,[ProductExpirationDate]
				   ,[ProductImagePath]
				   ,[Category]
				   ,[ProductShortDescription]
				   ,[ProductLongDescription]
				   ,[ModelNumber]
				   ,[SuggestedRetailPrice]
				   ,[Weight]
				   ,[WeightUnits]
				   ,[ManufacturerProdcutUrl]
				   ,[Audio]
				   ,[Video]
				   ,[Image]
				   ,[ManufacturerId]
				   ,[UserID]
				   ,[DateCreated]
				   ,[OtherFiles]
				   ,@ProductFileName ProductFileName 
				   ,'Some Mandatory Fields are Missing.' Reason
			FROM #NewProducts
			WHERE UploadManufacturerProductID NOT IN(SELECT UploadManufacturerProductID FROM #CheckMandatory)
			
			UNION
			
			SELECT @ManufacturerLogID ManufacturerLogID 
				   ,[ProductName]
				   ,[ScanCode]
				   ,[ScanType]
				   ,[ProductExpirationDate]
				   ,[ProductImagePath]
				   ,[Category]
				   ,[ProductShortDescription]
				   ,[ProductLongDescription]
				   ,[ModelNumber]
				   ,[SuggestedRetailPrice]
				   ,[Weight]
				   ,[WeightUnits]
				   ,[ManufacturerProdcutUrl]
				   ,[Audio]
				   ,[Video]
				   ,[Image]
				   ,[ManufacturerId]
				   ,[UserID]
				   ,[DateCreated]
				   ,[OtherFiles]
				   ,@ProductFileName ProductFileName 
				   ,'Duplicate Scan Code.' Reason
			FROM #CheckMandatory
			WHERE UploadManufacturerProductID NOT IN(SELECT UploadManufacturerProductID FROM #UniqueProducts))DiscardedProducts		
			
			
			--To Populate Log table.			
			UPDATE [UploadManufacturerLog]
			SET [ProductFileName] = @ProductFileName
			   ,[ProductTotalRowsProcessed] = @TotalRows
			   ,[ProductSuccessfulRowCount] = (SELECT COUNT(1) FROM #UniqueProducts)
			   ,[ProductFailureRowCount] = (SELECT COUNT(1) FROM UploadManufacturerDiscardedProducts WHERE UploadManufacturerLogID = @ManufacturerLogID)
			   ,[Remarks] =	'Successful'
			   ,[ProcessDate] = GETDATE()
			   ,[ProcessEndDate] = GETDATE()
			WHERE UploadManufacturerLogID = @ManufacturerLogID		   
			
			--To capture the Counts.
		    SELECT @SuccessCount = ProductSuccessfulRowCount
				 , @FailureCount = ProductFailureRowCount
		    FROM UploadManufacturerLog
		    WHERE UploadManufacturerLogID = @ManufacturerLogID
		    
	--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerProducts.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			--DECLARE @ERRORMsg Varchar(2000)
			--SET @ERRORMsg=ERROR_MESSAGE()
			ROLLBACK TRANSACTION;
						
			--To capture Log info.	
			
			UPDATE [UploadManufacturerLog]
			SET [ProductFileName] = @ProductFileName
			   ,[ProductTotalRowsProcessed] = 0
			   ,[ProductSuccessfulRowCount] = 0
			   ,[ProductFailureRowCount] = 0
			   ,[Remarks] =	@ErrorMessage
			   ,[ProcessDate] = GETDATE()
			   ,[ProcessEndDate] = GETDATE()
			WHERE UploadManufacturerLogID = @ManufacturerLogID			
		
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
