USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerRetailLocationHotDealsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebConsumerRetailLocationHotDealsDisplay
Purpose					: To get the list of Hotdeals available.
Example					: usp_WebConsumerRetailLocationHotDealsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.4			30th May 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerRetailLocationHotDealsDisplay]
(
	  @UserID INT
	, @RetailID INT
	, @RetailLocationID INT
	, @LowerLimit int
	, @ScreenName varchar(50)
	, @RecordCount INT
	
    --User Tracking inputs.
    , @MainMenuID int
    , @RetailerListID int	
	
	--OutPut Variable	
	, @MaxCnt int output 
	, @NxtPageFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN
	
	BEGIN TRY
	
		BEGIN TRANSACTION
	
			 --To get Server Configuration
			 DECLARE @ManufConfig varchar(50) 
			 DECLARE @RetailerConfig varchar(50) 
			   
			 SELECT @ManufConfig=(SELECT ScreenContent  
								 FROM AppConfiguration   
								 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
				   ,@RetailerConfig=(SELECT ScreenContent  
									 FROM AppConfiguration   
									 WHERE ConfigurationType='Web Retailer Media Server Configuration')
			 FROM AppConfiguration  
		
			--To get the row count for pagination.
			 DECLARE @UpperLimit int 
			 SELECT @UpperLimit = @LowerLimit + @RecordCount			 
			 
			 --SELECT @UpperLimit = @LowerLimit + ScreenContent 
			 --FROM AppConfiguration 
			 --WHERE ScreenName = @ScreenName 
			 --AND ConfigurationType = 'Pagination'
			 --AND Active = 1			      
		      
		      
			  Select Row_Num=IDENTITY(int,1,1)
					 ,PH.ProductHotDealID 
					 ,PH.HotDealName 
					 ,RL.City
					 ,Isnull(PH.Category, 'Others') categoryName
					 ,A.APIPartnerID 
					 ,A.APIPartnerName 
					 ,PH.Price 
					 ,PH.SalePrice 
					 ,PH.HotDealShortDescription 
					 ,HotDealImagePath=CASE WHEN WebsiteSourceFlag=1 THEN 
					                            CASE WHEN PH.RetailID IS NOT NULL THEN @RetailerConfig+CAST(PH.RetailID AS Varchar(100))+'/'+ PH.HotDealImagePath 
					                                 ELSE @ManufConfig+CAST(PH.ManufacturerID AS Varchar(100)) +'/'+PH.HotDealImagePath END
					                         ELSE PH.HotDealImagePath END 
					                            
					                            
					 ,PH.HotDealURL 
					 ,PH.CategoryID	
					 ,PH.HotDealStartDate 
					 ,PH.HotDealEndDate              	      
			  INTO #Product
			  FROM ProductHotDealRetailLocation PRL
			  INNER JOIN ProductHotDeal PH ON PRL.ProductHotDealID = PH.ProductHotDealID
			  INNER JOIN RetailLocation RL ON RL.RetailLocationID = PRL.RetailLocationID
			  INNER JOIN APIPartner A ON A.APIPartnerID = PH.APIPartnerID
			  Where RL.RetailID = @RetailID 
			  AND RL.RetailLocationID = @RetailLocationID 
			  AND GETDATE() Between ISNULL(PH.HotDealStartDate,GETDATE()-1) AND ISNULL(PH.HotDealEndDate,GETDATE()+1) 
			
			  --To capture max row number.
			  SELECT @MaxCnt = MAX(Row_Num) FROM #Product
			
			  --this flag is a indicator to enable "More" button in the UI. 
			  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
			  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			  SELECT  Row_Num  rowNumber
					, ProductHotDealID hotDealId
					, HotDealName hotDealName
					, City city
					, Isnull(categoryName, 'Others') categoryName
					, APIPartnerID apiPartnerId
					, APIPartnerName apiPartnerName
					, Price hDPrice
					, SalePrice hDSalePrice
					, HotDealShortDescription hDshortDescription
					, HotDealImagePath hotDealImagePath	
					, HotDealURL  hdURL
					,HotDealStartDate 
					,HotDealEndDate 
					--, Distance 	distance
					, CategoryID categoryId
			  INTO #HotDeals
			  FROM #Product
			  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
			  ORDER BY Row_Num
			  
			  --User Tracking section.
			  
			  --Capture the impression of the Hot deals.
			  CREATE TABLE #Temp(HotDealListID int
							   , ProductHotDealID int
							   , RetailerListID int)
			  
			  INSERT INTO ScanSeeReportingDatabase..HotDealList(MainMenuID
															  , ProductHotDealID
															  , RetailerListID
															  , CreatedDate)
											
				OUTPUT inserted.HotDealListID, inserted.ProductHotDealID, inserted.RetailerListID INTO #Temp(HotDealListID, ProductHotDealID, RetailerListID)
										
											SELECT @MainMenuID
												 , hotDealId
												 , @RetailerListID
												 , GETDATE()
											FROM #HotDeals H
											
			 --Display the hot deal list along with the primary key of the tracking table.
			 SELECT rowNumber
				  , T.HotDealListID
				  , T.RetailerListID retListID
				  , hotDealId
				  , hotDealName
				  , city
				  , categoryName
				  , apiPartnerId
				  , apiPartnerName
				  , hDPrice
				  , hDSalePrice
				  , hDshortDescription
				  , hotDealImagePath
				  , hdURL
				  , categoryId
				  , HotDealStartDate 
				  , HotDealEndDate 
			 FROM #Temp T
			 INNER JOIN #HotDeals H ON T.ProductHotDealID = H.hotDealId
			 
			--Confirmation of Success.
			SELECT @Status = 0										  
		COMMIT TRANSACTION		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerRetailLocationHotDealsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION; 
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
