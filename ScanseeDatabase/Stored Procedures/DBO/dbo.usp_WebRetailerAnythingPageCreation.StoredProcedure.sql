USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAnythingPageCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerAnythingPageCreation
Purpose					: To Create Anything Page by the Retailer.
Example					: usp_WebRetailerAnythingPageCreation

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			8th August 2012				SPAN	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAnythingPageCreation]
(

	--Input Parameter(s)--
	
	--Common
	 @RetailID int	
   , @RetailLocationID varchar(MAX)
   , @AnythingPageTitle varchar(255)
   , @Image varchar(100) 
   
   --Link to Existing.
   , @WebLink varchar(1000)
   , @ImageIconID int
   
   --Make your Own
   , @PageDescription varchar(Max)
   , @PageShortDescription varchar(max)
   , @PageLongDescription varchar(max) 
   , @StartDate varchar(10)
   , @EndDate varchar(10)
   , @StartTime varchar(5)
   , @EndTime varchar(5)
   , @UploadFileName varchar(500)   
   	
	--Output Variable--	  	 
	, @PageID int output
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @QRRetailerCustomPageID INT
			DECLARE @ProductMediaTypeID int
			
			--Create the Anything Page & retrieve the page id for the QR code generation.
			INSERT INTO QRRetailerCustomPage(QRTypeID
											,RetailID
											,Pagetitle
											,StartDate
											,EndDate
											,Image
											,PageDescription
											,ShortDescription
											,LongDescription
											,QRRetailerCustomPageIconID
											,URL
											,DateCreated)
											
				                 SELECT (SELECT QRTypeID FROM QRTypes WHERE QRTypeName LIKE 'Anything Page')
									  , @RetailID
									  , @AnythingPageTitle
									  , @StartDate + ' '+ @StartTime 
									  , @EndDate+' '+@EndTime 
									  , @Image
									  , @PageDescription
									  , @PageShortDescription
									  , @PageLongDescription
									  , @ImageIconID
									  , @WebLink
									  , GETDATE()
									  
			SELECT @QRRetailerCustomPageID = SCOPE_IDENTITY()
			SELECT @PageID = @QRRetailerCustomPageID
			
			--If the Retailer Locations are not null.						  
			IF (@RetailLocationID IS NOT NULL)
			BEGIN
				INSERT INTO QRRetailerCustomPageAssociation(QRRetailerCustomPageID
														  , RetailID
														  , RetailLocationID
														  , DateCreated)
													SELECT @QRRetailerCustomPageID
														 , @RetailID
														 , Param
														 , GETDATE()
													FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END
			
			--This will have a value only if the Create own page is selected.
			IF (@UploadFileName IS NOT NULL)
			BEGIN
				INSERT INTO QRRetailerCustomPageMedia(QRRetailerCustomPageID
													, MediaTypeID
													, MediaPath
													, DateCreated)
										  SELECT @QRRetailerCustomPageID
										       ,(SELECT ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType LIKE 'Other Files')
										       , @UploadFileName
										       , GETDATE()														 
										
			END				  
									  
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAnythingPageCreation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
