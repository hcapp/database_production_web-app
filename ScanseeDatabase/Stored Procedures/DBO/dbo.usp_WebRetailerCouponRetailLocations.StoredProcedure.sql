USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponRetailLocations]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponRetailLocations
Purpose					: 
Example					: usp_WebRetailerCouponRetailLocations

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th DEC 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCouponRetailLocations]
(
	@RetailID int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
				SELECT RetailLocationID retailerLocationID
					  ,State
					  ,City
					  ,Address1
				FROM RetailLocation
				WHERE RetailID=@RetailID
				AND Headquarters <> 1 AND Active = 1
				order by RetailLocationID
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponRetailLocations.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;





GO
