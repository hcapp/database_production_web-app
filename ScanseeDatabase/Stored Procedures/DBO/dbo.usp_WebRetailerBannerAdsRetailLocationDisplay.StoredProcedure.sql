USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdsRetailLocationDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name    : [usp_WebRetailerBannerAdsRetailLocationDisplay]
Purpose                  : To display the Retailer Locations associated to the Banner Ads.
Example                  : [usp_WebRetailerBannerAdsRetailLocationDisplay]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               31st July 2012      Dhananjaya TR   Initial Version                                        Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdsRetailLocationDisplay]
(

      --Input Input Parameter(s)--  
      
        @AdvertisementBannerID INT
	  --Output Variable--
	  , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY 
      		      		
       --Display The Locations Associated to the Banner Ad
       
              SELECT RL.RetailLocationID as retailLocationIds
				  ,RL.Address1 As Address1
				  ,RL.StoreIdentification as StoreIdentification
			  FROM RetailLocationBannerAd RLB
			  INNER JOIN RetailLocation RL ON RLB .RetailLocationID =RL.RetailLocationID 
			  WHERE RLB.AdvertisementBannerID = @AdvertisementBannerID AND RL.Active = 1  
			  
					        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdsRetailLocationDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;




GO
