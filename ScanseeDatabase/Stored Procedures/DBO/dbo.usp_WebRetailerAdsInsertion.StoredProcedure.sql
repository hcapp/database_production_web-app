USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdsInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: WebRetailerAdsInsertion
Purpose					: 
Example					: WebRetailerAdsInsertion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdsInsertion]
(
	@RetailLocationID int
	,@ADName varchar(100)
	,@Description varchar(255)
	,@BannerAdImage varchar(255)
	,@RibbonAdImage varchar(255)
	,@RibbonAdURL varchar(255)
	,@Comments Varchar(1000)
	,@AdStartDate date
	,@AdEndDate date
	
	--Output Variable 
	, @Success int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
		
		IF (SELECT COUNT(1) from RetailLocationAdvertisement where RetailLocationID=@RetailLocationID)<>0
		BEGIN	
			DECLARE @RetailLocationAdvertisementID int 
			SELECT @RetailLocationAdvertisementID= MAX(RetailLocationAdvertisementID) 
			FROM RetailLocationAdvertisement 
			WHERE RetailLocationID=@RetailLocationID 
		
		IF EXISTS(SELECT 1 from RetailLocationAdvertisement where RetailLocationAdvertisementID=@RetailLocationAdvertisementID AND @AdStartDate >(SELECT AdEndDate FROM RetailLocationAdvertisement WHERE RetailLocationAdvertisementID=@RetailLocationAdvertisementID) )
		BEGIN
			
			INSERT INTO [RetailLocationAdvertisement]
					   ([RetailLocationID]
					   ,[AdName]
					   ,[AdDescription]
					   ,[BannerAdImagePath]
					   ,[RibbonAdImagePath]
					   ,[RibbonAdURL]
					   ,[AdStartDate]
					   ,[AdEndDate]
					   ,[DateAdded]
					   ,[DateModified]
					   ,[Comments]
					   ,WebsiteSourceFlag)
			VALUES(@RetailLocationID
					,@ADName
					,@Description
					,@BannerAdImage
					,@RibbonAdImage
					,@RibbonAdURL
					,@AdStartDate
					,@AdEndDate
					,GETDATE()
					,GETDATE()
					,@Comments
					,1)
			SET @Success=0
		END		
		ELSE 
			BEGIN
				SET @Success=1
			END		
		END	
		IF (SELECT COUNT(1) from RetailLocationAdvertisement where RetailLocationID=@RetailLocationID)=0
		BEGIN
			INSERT INTO [RetailLocationAdvertisement]
					   ([RetailLocationID]
					   ,[AdName]
					   ,[AdDescription]
					   ,[BannerAdImagePath]
					   ,[RibbonAdImagePath]
					   ,[RibbonAdURL]
					   ,[AdStartDate]
					   ,[AdEndDate]
					   ,[DateAdded]
					   ,[DateModified]
					   ,[Comments]
					   ,WebsiteSourceFlag)
			VALUES(@RetailLocationID
					,@ADName
					,@Description
					,@BannerAdImage
					,@RibbonAdImage
					,@RibbonAdURL
					,@AdStartDate
					,@AdEndDate
					,GETDATE()
					,GETDATE()
					,@Comments
					,1)
					
			SET @Success = 0
		END
		--Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure WebRetailerAdsInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
