USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUserRatings]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_GetUserRatings]  
Purpose     : Retrieve Average User Rating for product in context 
Example     : EXEC [usp_GetUserRatings] ''  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------  
1.0   30th July 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------   
*/ 

CREATE Procedure [dbo].[usp_GetUserRatings]
(
	@prProductID	int,
	@prUserID		int,
	
	--Output Variable 
	@ErrorNumber	int output,
	@ErrorMessage	varchar(1000) output 	
)	
As

Begin
	
	Begin Try
		
		Declare @ProductID int, @UserID	Int, @Counter bit 
		
		-- Assign parameter value to a local variable
		Select  @ProductID = @prProductID,								
				@UserID    = @prUserID
				
		-- If User Rating already exists in the database
		
		If Exists (Select 1 from [UserRating] where ProductID = @ProductID And UserID = @UserID)
			Begin			
			-- Retrive average user rating for a product
				Select ProductId, AVG(Isnull(Rating,0)) As AverageRating, COUNT(UserID) AS noOfUsersRated
				INTO #Avgrating 
				From [UserRating] 
				Where ProductID = @ProductID	
				Group By ProductID
			

				Select ProductId, Isnull(Rating,0) As UserRating
				INTO #userrating
				From [UserRating] 
				Where ProductID = @ProductID
				And UserID = @UserID
				
				Select A.ProductID, AverageRating avgRating, UserRating currentRating , noOfUsersRated
				From #Avgrating A
				INNER JOIN #userrating U ON A.ProductID=U.ProductID
			End			
		If Exists (Select 1 from [UserRating] where ProductID = @ProductID )
			Begin			
			-- Retrive average user rating for a product
				Select ProductId, AVG(Isnull(Rating,0)) As avgRating, 0 currentRating ,COUNT(UserID) AS noOfUsersRated
				From [UserRating] 
				Where ProductID = @ProductID	
				Group By ProductID
				
			End		
			
		Else
		
			-- If User Rating Does NOT exists in the database
			Begin
				Select ProductId, AVG(Isnull(Rating,0)) As  avgRating, 0 As UserRating ,0 AS noOfUsersRated
				From [UserRating] 
				Where ProductID = @ProductID	
				Group By ProductID		
			End
				
		--If Exists (Select 1 from [UserRating] where ProductID = @ProductID --And UserID = @UserID
		--)
		--	Begin			
		--	-- Retrive average user rating for a product
		--		;With UserAvgProductRating
		--		As (
		--				Select ProductId, AVG(Isnull(Rating,0)) As AverageRating, COUNT(UserID) AS noOfUsersRated 
		--				From [UserRating] 
		--				Where ProductID = @ProductID	
		--				Group By ProductID
		--			)
					
		--			,UserProductRating
		--			As
		--			(
		--				Select ProductId, Isnull(Rating,0) As UserRating
		--				From [UserRating] 
		--				Where ProductID = @ProductID
		--				--And UserID = @UserID
		--			)

		--		Select UserAvgProductRating.ProductID, AverageRating avgRating, UserRating currentRating , noOfUsersRated
		--		From UserAvgProductRating
		--		inner join UserProductRating on UserProductRating.ProductID = UserAvgProductRating.ProductID
		--	End			
		
	End Try

Begin Catch

	--Check whether the Transaction is uncommitable.
	If @@ERROR <> 0
	Begin
		Print 'Error occured in Stored Procedure usp_GetAPIInputParameters.'		
		--- Execute retrieval of Error info.
		Exec [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
	End;
	 
End Catch;
	
End

GO
