USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerPrintProductInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebConsumerPrintProductInfo]
(

	--Input Input Parameter(s)--
	   
	  
	   @ProductIDs varchar(max) 	 
	
	--Output Variable--	  

	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		DECLARE @CategoryID int
	
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration' 
		
		SELECT DISTINCT P.ProductName		   
		     , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+  ProductImagePath
										ELSE ProductImagePath END
		     , ISNULL(P.ProductShortDescription, P.ProductLongDescription) productDescription 
		     , P.WarrantyServiceInformation warrantyServiceInfo
		     , P.ScanCode
		     , P.SuggestedRetailPrice productPrice
		     , P.ModelNumber
		     , P.ProductExpirationDate as productExpDate
		FROM Product P 	
		INNER JOIN dbo.fn_SplitParam(@ProductIDs, ',') F ON P.ProductID = F.Param
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
