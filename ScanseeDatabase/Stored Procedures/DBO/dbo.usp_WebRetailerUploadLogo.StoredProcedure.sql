USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerUploadLogo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerUploadLogo
Purpose					: To Upload the Retailer Logo.
Example					: usp_WebRetailerUploadLogo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		3rd January 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerUploadLogo]
(
	 	  
	  @RetailerID int
	, @RetailerLogo varchar(1000)
	  
	--Output Variable 
	, @FindClearCacheURL VARCHAR(1000) OUTPUT
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			UPDATE Retailer 
			SET RetailerImagePath = @RetailerLogo
			  , WebsiteSourceFlag = 1
			WHERE RetailID = @RetailerID AND RetailerActive = 1
			
			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------

			--Confirmation of Success.
			SELECT @Status = 0

		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerUploadLogo.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
