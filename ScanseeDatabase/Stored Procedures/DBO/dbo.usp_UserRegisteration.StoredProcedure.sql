USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserRegisteration]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_usp_UserRegisteration
Purpose					: To store User Registration info in User, UserDemographic, UserRetailPreference tables
Example					: 
DECLARE	@return_value int
EXEC @return_value =  [usp_UserRegisteration] 'John', 'Mitter', '#48', '', '', '', 'New York', 'SC', '5000050', 01, '9898989898', '978675656', 'John@span.com', 'John', 1, 0, 0, 0, '12/6/2000',1, '2/3/2001', 0, 1,'1,2,3',1, 1, 1, @Result = @return_value OUTPUT
SELECT @return_value as return_value
History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0		12th May 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserRegisteration]
(	--User Info Parameter

	@FirstName varchar(20),
	@Lastname varchar(30),
	@Address1 varchar(50),
	@Address2 varchar(50),
	@Address3 varchar(50),
	@Address4 varchar(50),
	@City varchar(30),
	@State char(2),
	@PostalCode varchar(10),
	@CountryID smallint,
	@MobilePhone char(10),
	@DeviceID varchar(60),
	@Email varchar(100),
	@Password varchar(100),
	@PushNotify bit,
	@FirstUseComplete bit,
	@FieldAgent bit,
	@AverageFieldAgentRating tinyint,
	@DateCreated datetime,
	
	--User Demograpny Info Parameter
	@Gender bit,
	@DOB date,
	@MaritalStatus varchar(30),
	@IncomeRangeID tinyint,
	
	--User Prefered Retailer Info Parameter
	@RetailID varchar(max),
	@Active bit,
	@LoyaltyProgram bit,
	@RetailFavorite bit,
	
	--Output Variable 
	@Result int OUTPUT
)
AS
BEGIN
	--To get current Date.
	DECLARE @Today DATETIME = GETDATE();

	BEGIN TRY
		BEGIN TRANSACTION
			--To insert User info
			INSERT INTO [Users] 
				(FirstName,
				Lastname,
				Address1,
				Address2,
				Address3,
				Address4,
				City,
				State,
				PostalCode,
				CountryID,
				MobilePhone,
				DeviceID,
				Email,
				Password,
				PushNotify,
				FirstUseComplete,
				FieldAgent,
				AverageFieldAgentRating,
				DateCreated,
				DateModified)
			VALUES
				(@FirstName, 
				@Lastname, 
				@Address1, 
				@Address2, 
				@Address3, 
				@Address4, 
				@City, 
				@State, 
				@PostalCode, 
				@CountryID, 
				@MobilePhone,
				@DeviceID, 
				@Email, 
				@Password, 
				@PushNotify,
				@FirstUseComplete,
				@FieldAgent, 
				@AverageFieldAgentRating, 
				@DateCreated, 
				@DateCreated)
			
			--To get recently generated UserID	
			DECLARE @UserID INT
			SET @UserID = SCOPE_IDENTITY()
			
			--To Insert user Demography info
			INSERT INTO [UserDemographic] 
				(UserID,
				Gender,
				DOB,
				MaritalStatusID,
				IncomeRangeID,
				DateCreated,
				DateModified)
			VALUES
				(@UserID, 
				@Gender, 
				@DOB, 
				@MaritalStatus, 
				@IncomeRangeID, 
				@DateCreated, 
				@DateCreated) 
				
			--To insert user perfered retailers info
			INSERT INTO UserRetailPreference
				(UserID,
				RetailID,
				Active,
				LoyaltyProgram,
				RetailFavorite,
				DateAdded)
			SELECT
				@UserID, 
				PARAM, 
				@Active, 
				@LoyaltyProgram, 
				@RetailFavorite, 
				@DateCreated 
			FROM dbo.fn_SplitParam(@RetailID, ',')
			
			SELECT @Result = 0 --To indicate successfull execution.
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
			
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserRegisteration.'
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]		
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			SELECT @Result = 1 --To indicate unsuccessfull execution.
			ROLLBACK TRANSACTION;
		END;
		
	END CATCH;
END;

GO
