USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierProductFeeDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebSupplierProductFeeDisplay]
(

	--Input Parameter(s)--	  
	
	--Output Variable-- 
	  
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--Display the all the Active Product fee plans available.			
		SELECT ManufacturerProductFeeID
		     , ProductFee
		     , LowerBoundViews
		     , HigherBoundViews
		     , ProductFeeDescription feedescription		     
		FROM ManufacturerProductFee 	
		WHERE ActiveFlag = 1
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierProductFeeDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
