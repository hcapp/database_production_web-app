USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHotDealPopulationCentres]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HotDealPopulationCentres
Purpose					: To Display DMA Names for placing a HotDeal.
Example					: usp_HotDealPopulationCentres

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			12thMarch2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHotDealPopulationCentres]
(

	--Input Input Parameter(s)-- 	

	
	--Output Variable--	  
	  
	   @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
			
			--To get the Dma names that have deals.
			SELECT DISTINCT P.PopulationCenterID
							,DMAName+' - '+PC.State dMAName
							,PC.State
			FROM PopulationCenters P
				INNER JOIN PopulationCenterCities PC ON P.PopulationCenterID=PC.PopulationCenterID				 
			WHERE Active=1  AND DMAName IS NOT NULL
			ORDER BY DMAName+' - '+PC.State	
		  	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HotDealPopulationCentres.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
