USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ScanHistoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ScanHistoryDisplay
Purpose					: To display Scan History information of the User
Example					: usp_ScanHistoryDisplay 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ScanHistoryDisplay]
(
	  @UserID int	
	, @LowerLimit int
	, @UpperLimit int
	
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		; WITH ScanHistoryDispaly
		AS
		(
			-- To fetch Scan History info.
			
		--	SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY SH.ScanDate)
		--		, SH.UserID
		--		, P.ProductName 
		--		, SH.ScanDate
		--		, SH.ScanCode
		--		, ST.ScanType
		--		, Status =  'Success' 
		--	FROM ScanHistory SH
		--		LEFT JOIN Product P ON P.ProductID = SH.ProductID
		--		LEFT JOIN ProductScanCodeType ST ON ST.ScanTypeID = SH.ScanTypeID
		--	WHERE SH.UserID = @UserID 
		--		AND SH.LookupMatch = 1
			
		--)
		
		--SELECT Row_Num  
		--		, UserID
		--		, ProductName 
		--		, ScanDate
		--		, ScanCode
		--		, ScanType
		--		, Status 
		--FROM ScanHistoryDispaly
		--WHERE Row_Num BETWEEN (@LowerLimit+1) AND (@UpperLimit)
		
		
			SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY SH.ScanDate)
				, SH.UserID
				, P.ProductName 
				, SH.ScanDate
				, SH.ScanCode
				, ST.ScanType
				, ProductLongDescription 
				, ProductImagePath
				, Status =  'Success'
			FROM ScanHistory SH
				LEFT JOIN Product P ON P.ProductID = SH.ProductID
				LEFT JOIN ProductScanCodeType ST ON ST.ScanTypeID = SH.ScanTypeID
			WHERE SH.UserID = @UserID 
				AND SH.LookupMatch = 1
			
			)
		
		SELECT Row_Num  
				, UserID
				, ProductName 
				, ScanDate
				, ScanCode
				, ScanType
				, ProductLongDescription
				, ProductImagePath
				, Status 
		FROM ScanHistoryDispaly
		WHERE Row_Num BETWEEN (@LowerLimit+1) AND (@UpperLimit)
	    ORDER BY ScanDate
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
