USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingCartCategoryProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_ShoppingCartCategoryProduct  
Purpose     : To fetch User's Favorite Items.  
Example     : EXEC usp_ShoppingCartCategoryProduct 2  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   27th Sep 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_ShoppingCartCategoryProduct]  
(  
 @UserID int  
   
 --OutPut Variable  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY   
 
	--To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	   
  --To get categories, Products and CLR info of a User's Favorite list.  
  SELECT UP.UserProductID  
   , UP.UserID  
   , UP.ProductID  
   , ProductName = CASE WHEN UP.ProductID = 0  THEN UP.UnassignedProductName ELSE P.ProductName END  
   , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END 
   , ProductShortDescription  productShortDescription
   , ProductLongDescription   productLongDescription
   , ISNULL(C.CategoryID ,0) CategoryID  
   , ISNULL(C.ParentCategoryID, 0) ParentCategoryID  
   , ISNULL(C.ParentCategoryName, 'Unassigned Products') ParentCategoryName   
   , C.SubCategoryID   
   , C.SubCategoryName   
   , Coupon.Coupon   
   , Loyalty.Loyalty  
   , Rebate.Rebate      
  INTO #CLR      
  FROM UserProduct UP  
   LEFT JOIN Product P ON P.ProductID = UP.ProductID  
   LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID  
   LEFT JOIN Category C ON C.CategoryID = PC.CategoryID    
   LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID   
   LEFT JOIN Retailer R ON R.RetailID = UR.RetailID    
   OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon  
   OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty  
   OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate  
  WHERE UP.UserID = @UserID  
   AND UP.ShopCartItem = 1   
   AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())  
     
  SELECT UserProductID userProductID  
   , CLR.UserID  
   , CLR.ProductID  
   , CLR.ProductName   
   , CLR.ProductShortDescription
   , CLR.ProductImagePath  
   , CategoryID categoryID  
   , ParentCategoryID parentCategoryID  
   , ParentCategoryName parentCategoryName  
   , SubCategoryID subCategoryID  
   , SubCategoryName subCategoryName  
   , CLRFlag = CASE WHEN (COUNT(CLR.Coupon)+COUNT(CLR.Loyalty)+COUNT(CLR.Rebate)) > 0 THEN 1 ELSE 0 END  
   , coupon_Status = CASE WHEN COUNT(CLR.Coupon) = 0 THEN 'Grey'   
           ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
   , loyalty_Status = CASE WHEN COUNT(CLR.Loyalty) = 0 THEN 'Grey'  
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
          END  
   , rebate_Status = CASE WHEN COUNT(CLR.Rebate) = 0 THEN 'Grey'  
           ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
  FROM #CLR CLR  
   LEFT JOIN UserCouponGallery UC ON UC.UserID = CLR.UserID AND UC.CouponID = CLR.Coupon  
   LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = CLR.UserID AND UL.LoyaltyDealID = CLR.Loyalty  
   LEFT JOIN UserRebateGallery UR ON UR.UserID = CLR.UserID AND UR.RebateID = CLR.Rebate   
  WHERE CLR.UserID = @UserID   
  GROUP BY UserProductID  
   , CLR.UserID  
   , CategoryID   
   , ParentCategoryID   
   , ParentCategoryName   
   , SubCategoryID   
   , SubCategoryName    
   , CLR.ProductID  
   , CLR.ProductName
   , CLR.ProductShortDescription 
   , CLR.productLongDescription 
   , CLR.ProductImagePath  
  ORDER BY  ParentCategoryName, ProductName  
  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_ShoppingCartCategoryProduct.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
