USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayProductMedia]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayProductMedia
Purpose					: To Display the media related to a product.
Example					: usp_WebDisplayProductMedia

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			4th January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayProductMedia]
(

	--Input Parameter(s)--   
	
	   @ProductID int
	--Output Variable--	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	DECLARE @AudioPath VARCHAR(1000)
	DECLARE @VideoPath VARCHAR(1000)	
	DECLARE @ImagePath VARCHAR(1000)
	
	DECLARE @Config varchar(50)	
	 
	SELECT @Config=ScreenContent
	FROM AppConfiguration 
	WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
	
	
	SELECT	  ProductMediaID     
		    , ProductMediaPath mediaPath
	        , PMT.ProductMediaType
	        , @Config + CAST(P.ManufacturerID AS VARCHAR(10))+'/'+ ProductMediaPath FullMediaPath
	FROM Product P
	INNER JOIN ProductMedia PM ON P.ProductID = PM.ProductID
	INNER JOIN ProductMediaType PMT ON PM.ProductMediaTypeID = PMT.ProductMediaTypeID	
	AND P.ProductID = @ProductID
	AND (ProductMediaPath IS NOT NULL
	OR ProductMediaPath <> ' ')
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayProductMedia.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
