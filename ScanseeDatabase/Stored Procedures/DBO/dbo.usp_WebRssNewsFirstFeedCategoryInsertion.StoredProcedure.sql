USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssNewsFirstFeedCategoryInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: usp_WebRssNewsFirstFeedCategoryInsertion
Purpose					: 
Example					: usp_WebRssNewsFirstFeedCategoryInsertion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebRssNewsFirstFeedCategoryInsertion]
(
	--  @CategoryName varchar(2000)
 --   , @ImagePath varchar(max)
	--, @HcHubCitiID int
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		 DELETE FROM RssNewsFirstFeedCategory
		 FROM RssNewsFirstFeedCategory RF
		 INNER JOIN RssNewsFirstFeedCategoryStagingTable RS ON RF.CategoryName = RS.CategoryName
		 WHERE RS.CategoryName IS NOT NULL  

		 INSERT INTO RssNewsFirstFeedCategory(CategoryName
									,ImagePath
									,HcHubCitiID)
							SELECT	CategoryName
								   ,ImagePath
								   ,HcHubCitiID
							FROM RssNewsFirstFeedCategoryStagingTable RS WHERE RS.CategoryName NOT IN (SELECT CategoryName FROM RssNewsFirstFeedCategory)   
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssNewsFirstFeedCategoryInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;






GO
