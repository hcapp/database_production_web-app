USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerWelcomePageDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebRetailerWelcomePageDisplayDetails]
Purpose                  : To display the Welcome pages created by the given Retailer.
Example                  : [usp_WebRetailerWelcomePageDisplayDetails]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Aug 2012        Dhananjaya TR   Initial Version                                    
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerWelcomePageDisplayDetails]
(

      --Input Input Parameter(s)--  
      
        @AdvertisementSplashID INT
      --Output Variable--
	  
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
      --To Fetch the Retailer Media server Configuaration
      
			DECLARE @Config varchar(255)    
			SELECT @Config=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration' AND Active =1  
		
		
       --Display The Welcome Pages 
       
				SELECT DISTINCT A.AdvertisementSplashID as retailLocationAdvertisementID
					  ,A.SplashAdName as advertisementName
					  ,@Config + CONVERT(VARCHAR(30),A.RetailID) +'/' + SplashAdImagePath as strBannerAdImagePath 
				      ,advertisementDate = CASE WHEN A.StartDate = '01/01/1900' THEN (SELECT StartDate
																					  FROM AdvertisementSplashHistory 
																					  WHERE AdvertisementSplashID = @AdvertisementSplashID
																					  AND DateCreated = (SELECT MAX(DateCreated)
																										 FROM AdvertisementSplashHistory
																										 WHERE AdvertisementSplashID = @AdvertisementSplashID)) ELSE A.StartDate END
				      ,advertisementEndDate = CASE WHEN A.EndDate = '01/01/1900' THEN (SELECT EndDate
																					  FROM AdvertisementSplashHistory 
																					  WHERE AdvertisementSplashID = @AdvertisementSplashID
																					  AND DateCreated = (SELECT MAX(DateCreated)
																										 FROM AdvertisementSplashHistory
																										 WHERE AdvertisementSplashID = @AdvertisementSplashID)) ELSE A.EndDate END
				FROM AdvertisementSplash A
				LEFT OUTER JOIN AdvertisementSplashHistory ASH ON A.AdvertisementSplashID = ASH.AdvertisementSplashID
				WHERE A.AdvertisementSplashID = @AdvertisementSplashID                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerWelcomePageDisplayDetails.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;




GO
