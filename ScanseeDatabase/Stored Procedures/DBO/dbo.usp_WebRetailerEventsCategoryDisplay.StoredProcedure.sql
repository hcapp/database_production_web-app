USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerEventsCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerEventsCategoryDisplay]
Purpose					: To display list of Event categories.
Example					: [usp_WebRetailerEventsCategoryDisplay]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014		SPAN           1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerEventsCategoryDisplay]
(   
    --Input variable.	  
	  --@RetailID Int
  
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To get the Configuration Details.
			DECLARE @Config Varchar(500)	 
			SELECT @Config = ScreenContent   
			FROM AppConfiguration   
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			AND Active = 1

			--To display List fo Events Categories
			SELECT DISTINCT HcEventCategoryID As categoryID
						  ,HcEventCategoryName As categoryName
						  ,IIF(CategoryImagePath IS NOT NULL ,@Config+CategoryImagePath,CategoryImagePath) cateImgName
						  ,CategoryImagePath cateImg
			FROM HcEventsCategory  
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcRetailerEventDetails].'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
