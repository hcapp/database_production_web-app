USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerOnlineRetailerInformation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerOnlineRetailerInformation
Purpose					: To display online store retailer information.
Example					: usp_WebConsumerOnlineRetailerInformation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20th March 2012 	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerOnlineRetailerInformation]
(
	
	 @UserID int
	,@ProductID int
	,@LowerLimit int
	
	--User Tracking Imputs
    , @ProductListID int
    , @MainMenuID int
    
	--Output Variable 
	, @Status int output
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			--To get the row count for pagination.  
				DECLARE @UpperLimit int   
				SELECT @UpperLimit = @LowerLimit + ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = 'Online Retailer' 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
			
				SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
					   ,RL.RetailID retailerId
					   ,RL.RetailLocationID
					   ,RetailName retailerName
					   ,RetailOnlineURL retailerURL
					   ,Price
					   ,SalePrice
					   ,BuyURL buyURL
					   ,ShipmentCost = CASE WHEN ShipmentDetails = '0' OR ShipmentDetails = '0.00' OR ShipmentDetails = '$0.00' OR ShipmentDetails = '$0.0' THEN NULL ELSE ShipmentDetails END 
				INTO #Prod
				FROM Retailer R
				INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID
				INNER JOIN RetailLocationProduct RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=@ProductID
				WHERE OnlineStoreFlag=1 
			
				SELECT RowNum
					   ,retailerId
					   ,RetailLocationID
					   ,retailerName
					   ,retailerURL 
					   ,Price
					   ,SalePrice
					   ,buyURL
					   ,ShipmentCost 
				INTO #OnlineProd
				FROM #Prod
				WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
					
				
				--User Tracking		
				
				--Capture the online stores availble for the given product.
				CREATE TABLE #Temp(OnlineProductDetailID INT
								 , RetailLocationID INT
								 , ProductID INT)
						
				INSERT INTO ScanSeeReportingDatabase..OnlineProductDetails(ProductListID
																		  ,MainMenuID
																		  ,RetailLocationID
																		  ,ProductID
																		  ,CreatedDate)
				OUTPUT inserted.OnlineProductDetailID, inserted.RetailLocationID, inserted.ProductID INTO #Temp(OnlineProductDetailID, RetailLocationID, ProductID)
																SELECT @ProductListID
																	 , @MainMenuID
																	 , RetailLocationID
																	 , @ProductID
																	 , GETDATE()
																FROM #OnlineProd
				--Display along with the primary key of the tracking table.								    
				 SELECT T.OnlineProductDetailID onProdDetId
					   ,retailerId
					   ,retailerName
					   ,retailerURL
					   ,Price
					   ,SalePrice
					   ,buyURL
					   ,ShipmentCost 
				FROM #OnlineProd P
				INNER JOIN #Temp T ON T.RetailLocationID = P.RetailLocationID
				AND T.ProductID = @ProductID			
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerOnlineRetailerInformation.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			SELECT @Status = 1
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;


GO
