USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerImageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerImageDisplay
Purpose					: To Display the Basic Info about the Retailer.
Example					: usp_WebRetailerImageDisplay

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			29th May 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerImageDisplay]
(

	--Input Parameter(s)--
	  
	  @RetailID  int	 
	
	--Output Variable--	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	--To get Media Server Configuration.
		 DECLARE @Config varchar(50)  
		 SELECT @Config=ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration' 		
 
	
		SELECT Distinct R.RetailName as retailerName 
		     , R.Address1 address1
		     , R.Address2 address2
		     , R.State 'state'
		     , retailerImagePath = CASE WHEN RetailerImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),R.RetailID) +'/' + RetailerImagePath 
																				ELSE @Config + CONVERT(VARCHAR(30),R.RetailID) +'/' + RetailerImagePath  END  
							             
									END
		     , R.City city
		     , R.PostalCode  postalCode
		     , R.CorporatePhoneNo contactPhone
		     , R.LegalAuthorityFirstName as legalAuthorityFName
		     , R.LegalAuthorityLastName as legalAuthorityLName		 	    
		FROM Retailer R 		
	    WHERE RetailID = @RetailID AND RetailerActive = 1
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerImageDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
