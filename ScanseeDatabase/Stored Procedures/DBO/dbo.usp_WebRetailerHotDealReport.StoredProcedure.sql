USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerHotDealReport]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebRetailerHotDealReport 
Purpose				  : To get HotDeal Report.    
Example				  : usp_WebRetailerHotDealReport 
    
History    
Version    Date          Author    Change Description    
---------------------------------------------------------------     
1.0     23rd July 2013   SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebRetailerHotDealReport] 
(    
  
	--Input Variables   
	   @RetailID int
	 , @ProductHotDealID int
	 , @LowerLimit int
	 , @RecordCount int
      
	--OutPut Variable
	, @Status int output 
	, @NxtPageFlag bit output
	, @MaxCnt int output
	, @ErrorNumber int output 
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
			
		  DECLARE @UpperLimit INT
		  
		  SELECT @UpperLimit = @LowerLimit + @RecordCount
		
		  SELECT P.ProductHotDealID hotDealID
			   , P.HotDealName
			   , NoOfHotDealsToIssue AS numOfHotDeals 
			   , ClaimCount = COUNT(CASE WHEN Usedflag IS NOT NULL THEN 1 END) + COUNT(CASE WHEN HG.Used IS NOT NULL THEN 1 END)
			   , RedeemCount = COUNT(CASE WHEN Usedflag = 1 THEN 1 END) + COUNT(CASE WHEN HG.Used = 1 THEN 1 END)
		FROM ProductHotDeal P
		LEFT JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
		LEFT JOIN HcUserHotDealGallery HG ON HG.HotDealID = P.ProductHotDealID 
		WHERE P.ProductHotDealID = @ProductHotDealID
		AND P.RetailID = @RetailID
		GROUP BY P.ProductHotDealID
				, P.HotDealName
				, NoOfHotDealsToIssue
		ORDER BY P.HotDealName

		SELECT RowNum = ROW_NUMBER() OVER (ORDER BY HotDealName ASC)
			 , hotDealID
			 , HotDealName
			 , contactFirstName
			 , contactEmail
			 , Claimed
			 , Redeemed
		INTO #Users
		FROM(				 
		SELECT	P.ProductHotDealID hotDealID
			   , P.HotDealName
			   , U.FirstName AS contactFirstName
			   , U.Email AS contactEmail
			   , CASE WHEN UsedFlag IS NOT NULL THEN 'YES' END AS Claimed
			   , CASE WHEN UsedFlag = 1 THEN 'YES' ELSE 'NO' END AS Redeemed		
		FROM ProductHotDeal P
		INNER JOIN UserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
		INNER JOIN Users U ON UHG.UserID = U.UserID
		WHERE P.ProductHotDealID = @ProductHotDealID
		AND P.RetailID = @RetailID
		--ORDER BY P.HotDealName

		UNION ALL

		SELECT	P.ProductHotDealID hotDealID
			   , P.HotDealName
			   , U.FirstName AS contactFirstName
			   , U.Email AS contactEmail
			   , CASE WHEN Used IS NOT NULL THEN 'YES' END AS Claimed
			   , CASE WHEN Used = 1 THEN 'YES' ELSE 'NO' END AS Redeemed		
		FROM ProductHotDeal P
		INNER JOIN HcUserHotDealGallery UHG ON P.ProductHotDealID = UHG.HotDealID
		INNER JOIN HCUser U ON UHG.HCuserID = U.HcUserID
		WHERE P.ProductHotDealID = @ProductHotDealID
		AND P.RetailID = @RetailID)HD	
		
		SELECT @MaxCnt = COUNT(1) FROM #Users
		
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END
		
		--The same SP is used for print functionality. Send all the records without pagination when Lower Limit is 0 and Record Count = 0 or NULL.
		IF @LowerLimit = 0 AND ISNULL(@RecordCount, 0) = 0
		BEGIN
			SELECT @UpperLimit = MAX(RowNum)
			FROM #Users
			
			--Intimate that there are no more pages.
			SELECT @NxtPageFlag = 0
		END
		
		SELECT RowNum  
			   , hotDealID
			   , HotDealName
			   , contactFirstName
			   , contactEmail
			   , Claimed
			   , Redeemed
		FROM #Users
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
		
	END TRY    
     
	BEGIN CATCH    
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerHotDealReport].'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
		END;    
	END CATCH;
   
END;




GO
