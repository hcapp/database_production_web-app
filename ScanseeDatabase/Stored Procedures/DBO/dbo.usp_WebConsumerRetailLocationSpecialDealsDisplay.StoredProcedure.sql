USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerRetailLocationSpecialDealsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerRetailLocationSpecialDealsDisplay]
Purpose					: To Identify Special Deals
Example					: [usp_WebConsumerRetailLocationSpecialDealsDisplay]

History
Version		Date			  Author		Change Description
--------------------------------------------------------------- 
1.0			29th May 2013	  SPAN	        Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerRetailLocationSpecialDealsDisplay]
(
	--Input Variable
	
	  @UserID int
	, @RetailID varchar(50)
	, @RetailLocationID varchar(50)
	 
   --User Tracking
    , @RetailerListID Int
   	 
	  	
	--Output Variable 
	, @SaleFlag bit output
	, @SpecialFlag bit output
	, @HotDealFlag bit output
	, @CouponFlag bit output	 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		--To Check Retailer that have products on sale
		SELECT DISTINCT Retailid , RetailLocationid
		INTO #Sales
		FROM 
	    (SELECT B.RetailID, A.RetailLocationID 
		FROM RetailLocationDeal A 
		INNER JOIN RetailLocation B ON A.RetailLocationID = B.RetailLocationID 
		INNER JOIN RetailLocationProduct C ON A.RetailLocationID = C.RetailLocationID
		INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',')D on D.Param = A.RetailLocationID
		AND A.ProductID = C.ProductID
		AND GETDATE() BETWEEN ISNULL(A.SaleStartDate, GETDATE()-1) AND ISNULL(A.SaleEndDate, GETDATE()+1)
		AND B.RetailID = @RetailID
		AND A.RetailLocationID = @RetailLocationID) offers
		
		--To Check Retailer that have special offers 
		SELECT DISTINCT Retailid , RetailLocationid
		INTO #Specials
		FROM 
		(SELECT Q.RetailID, QA.RetailLocationID
		FROM QRRetailerCustomPage Q
		INNER JOIN QRRetailerCustomPageAssociation QA ON QA.QRRetailerCustomPageID = Q.QRRetailerCustomPageID
		INNER JOIN QRTypes qt ON QT.QRTypeID = Q.QRTypeID and QT.QRTypeName = 'Special Offer Page'
		INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',')D on D.Param = QA.RetailLocationID
		WHERE GETDATE() BETWEEN ISNULL(Q.startdate,'1/1/1900') AND ISNULL(Q.enddate,GETDATE()+1)
		AND Q.RetailID = @RetailID
		AND QA.RetailLocationID = @RetailLocationID) offers
		
		--To Check retailer that have hotdealProducts
		SELECT DISTINCT Retailid , RetailLocationid
		INTO #HotDeals
		FROM
		(SELECT DISTINCT RL.RetailID, RL.RetailLocationID
		FROM ProductHotDeal P
		INNER JOIN ProductHotDealRetailLocation PR ON PR.ProductHotDealID = P.ProductHotDealID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = PR.RetailLocationID
		LEFT JOIN HotDealProduct HP ON HP.ProductHotDealID = P.ProductHotDealID
		INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',')D on D.Param = RL.RetailLocationID
		WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE()-1) AND ISNULL(HotDealEndDate, GETDATE()+1)
		AND RL.RetailID = @RetailID
		AND RL.RetailLocationID = @RetailLocationID) offers 
		
		--To check Retailer that have Coupons on products
		SELECT DISTINCT Retailid , RetailLocationid
		INTO #Coupons
		FROM
		(SELECT  CR.RetailID, RetailLocationID  AS RetaillocationID 
		FROM Coupon C 
		INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
		INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',')D on D.Param = RetailLocationID
		WHERE GETDATE() BETWEEN ISNULL(CouponStartDate, GETDATE()-1) AND ISNULL(CouponExpireDate, GETDATE()+1)
		AND CR.RetailID = @RetailID
		AND RetailLocationID = @RetailLocationID) offers
		
		
		SELECT   @SaleFlag = CASE WHEN (SELECT COUNT(1) FROM #Sales) > 0 THEN 1 ELSE 0 END
				,@SpecialFlag = CASE WHEN (SELECT COUNT(1) FROM #Specials) > 0 THEN 1 ELSE 0 END	  
				,@HotDealFlag = CASE WHEN (SELECT COUNT(1) FROM #HotDeals) > 0 THEN 1 ELSE 0 END	
				,@CouponFlag = CASE WHEN (SELECT COUNT(1) FROM #Coupons) > 0 THEN 1 ELSE 0 END 
		
	   --User Tracking
	   UPDATE ScanSeeReportingDatabase..RetailerList 
	   SET RetailLocationClick =1
	   WHERE RetailerListID =@RetailerListID 			
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerRetailLocationSpecialDealsDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
		 
	END CATCH;
END;


GO
