USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierHotDealSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
    
/*    
Stored Procedure name : usp_WebSupplierHotDealSearch    
Purpose     : To display the details of the selected HotDeal.    
Example     :     
    
History    
Version  Date       Author   Change Description    
-------------------------------------------------------------------------------     
1.0   15th December 2011    Pavan Sharma K Initial Version    
-------------------------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebSupplierHotDealSearch]    
(    
    
 --Input Input Parameter(s)--    
     
    @ManufacturerID int    
  , @SearchParameter varchar(100)     
  , @LowerLimit int        
       
 --Output Variable--    
       
  , @RowCount int output  
  , @NextPageFlag bit output   
  , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY      
   
   DECLARE @RowCnt INT  
   DECLARE @MaxCnt INT  
   DECLARE @UpperLimit INT  
     
   --To get Media Server Configuration.    
   DECLARE @ManufacturerConfig varchar(50)      
 DECLARE @RetailerConfig varchar(50)     
   
 SELECT @ManufacturerConfig = ScreenContent      
 FROM AppConfiguration       
 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'    
   
 SELECT @RetailerConfig = ScreenContent      
 FROM AppConfiguration       
 WHERE ConfigurationType='Web Retailer Media Server Configuration'   
     
    --To get the row count for pagination.    
    DECLARE @ScreenContent Varchar(100)  
 SELECT @ScreenContent = ScreenContent     
 FROM AppConfiguration     
 WHERE ScreenName = 'All'   
  AND ConfigurationType = 'Website Pagination'  
  AND Active = 1     
      
 SELECT @UpperLimit = @LowerLimit + @ScreenContent  
       
   
 SELECT   RowNum = ROW_NUMBER() OVER (ORDER BY hotDealID ASC)  
       ,  hotDealID  
    ,  hotDealName  
       ,  hotDealLongDescription  
       ,  dealImgPath  
       ,  dealStartDate  
       ,  dealEndDate        
       --, HotDealImagePath       
       , hotDealDiscountAmount  
       , hotDealDiscountPct  
       , price  
       , salePrice        
       ,  hotDealTermsConditions  
       ,  url  
     
  INTO #TEMP  
  FROM   
  (    
    SELECT DISTINCT TOP 100 PERCENT  PHD.ProductHotDealID as hotDealID    
						   ,PHD.HotDealName as hotDealName    
						   , PHD.HotDeaLonglDescription as hotDealLongDescription    
						   , dealImgPath  = CASE WHEN PHD.HotDealImagePath IS NOT NULL THEN   
												CASE WHEN PHD.WebsiteSourceFlag = 1 THEN   
													CASE WHEN PHD.ManufacturerID IS NOT NULL THEN @ManufacturerConfig +  CONVERT(VARCHAR(30),PHD.ManufacturerID) +'/'+ HotDealImagePath  
													ELSE @RetailerConfig +  CONVERT(VARCHAR(30),phd.RetailID) +'/'+ HotDealImagePath  
												 END    
												ELSE PHD.HotDealImagePath  
											END  
											END  
						  , CONVERT(VARCHAR(10),PHD.HotDealStartDate,105) + ' ' + CONVERT(VARCHAR(10),PHD.HotDealStartDate,108) as dealStartDate    
						  , CONVERT(VARCHAR(10),PHD.HotDealEndDate,105) + ' ' +  CONVERT(VARCHAR(10),PHD.HotDealEndDate,108) as dealEndDate    
						   --, PHD.HotDealImagePath      
						  , PHD.HotDealDiscountAmount as hotDealDiscountAmount  
						  , PHD.HotDealDiscountPct  as hotDealDiscountPct  
						  , PHD.Price as price  
					      , PHD.SalePrice   as salePrice        
					      , PHD.HotDealTermsConditions as hotDealTermsConditions    
						  , PHD.HotDealURL as url  
						  , PHD.CreatedDate           
	FROM ProductHotDeal PHD  
    LEFT JOIN HotDealProduct HDP ON PHD.ProductHotDealID = HDP.ProductHotDealID    
    LEFT JOIN Product P ON P.ProductID = HDP.ProductID       
    WHERE  PHD.HotDealName LIKE (CASE WHEN @SearchParameter IS NOT NULL THEN '%'+ @SearchParameter +'%' ELSE '%' END)    
    --OR P.ProductName LIKE '%'+ @SearchParameter +'%'   
    AND PHD.ManufacturerID = @ManufacturerID
    ORDER BY PHD.CreatedDate ASC)HotDeals        
    
    --Capture Total Number of records.
    SELECT @MaxCnt = MAX(RowNum) FROM #TEMP  
       
     SET @RowCount = @MaxCnt  
     
     --Check if the there are still more records.
     SELECT @NextPageFlag = (CASE WHEN (@MaxCnt - @UpperLimit)>0 THEN 1 ELSE 0 END)  
      
     SELECT  hotDealID  
		   , hotDealName  
           , hotDealLongDescription  
		   , dealImgPath  
           , dealStartDate  
           , dealEndDate        
           --, HotDealImagePath       
           , hotDealDiscountAmount  
           , hotDealDiscountPct  
           , price  
           , salePrice        
           , hotDealTermsConditions  
           , url           
       FROM #TEMP  
       WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit           
       
       --Check if the Result is Empty.  
       IF @RowCount IS NULL  
       BEGIN  
			 SET @RowCount = 0  
       END        
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebSupplierHotDealSearch.'      
  -- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output        
  END;    
       
 END CATCH;    
END;


GO
