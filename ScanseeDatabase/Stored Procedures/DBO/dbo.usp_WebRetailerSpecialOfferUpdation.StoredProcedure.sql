USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerSpecialOfferUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerSpecialOfferUpdation
Purpose					: To Update the anything page.
Example					: usp_WebRetailerSpecialOfferUpdation

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			9th August 2012				Dhananjaya TR	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerSpecialOfferUpdation]
(

	--Input Parameter(s)--
	
     @PageID int	
 	--Common
   , @RetailID int	
   , @RetailLocationID varchar(MAX)
   , @SpecialOfferPageTitle varchar(255)
   , @Image varchar(100) 
   
   --Link to Existing.
   , @WebLink varchar(1000)
   , @ImageIconID int
   
   --Make your Own
   , @PageDescription varchar(Max)
   , @PageShortDescription varchar(Max)
   , @PageLongDescription varchar(max) 
   , @StartDate varchar(20)
   , @EndDate varchar(20)
   , @StartTime varchar(20)
   , @EndTime varchar(20)
   , @UploadFileName varchar(500)   
   	
	--Output Variable--	
	, @FindClearCacheURL varchar(1000) output	 
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @ProductMediaTypeID int
			
			--Create the Special Offer Page & retrieve the page id for the QR code generation.
			
			UPDATE QRRetailerCustomPage SET Pagetitle = @SpecialOfferPageTitle 
										  , StartDate = CAST(@StartDate AS VARCHAR(10))+' '+CAST(@StartTime AS VARCHAR(12))
										  , EndDate = CAST(@EndDate AS VARCHAR(10))+' '+CAST(@EndTime AS VARCHAR(12))
										  , [Image] = @Image
										  , PageDescription = @PageDescription
										  , ShortDescription = @PageShortDescription
										  , LongDescription = @PageLongDescription
										  , QRRetailerCustomPageIconID = @ImageIconID
										  , URL = @WebLink
										  , DateUpdated = GETDATE()
						WHERE QRRetailerCustomPageID = @PageID
						
			--If the Retailer Locations are not null.						  
			IF (@RetailLocationID IS NOT NULL)
			BEGIN
				--Delete the Existing Association with Location.
				DELETE FROM QRRetailerCustomPageAssociation WHERE QRRetailerCustomPageID = @PageID
				
				--Create a fresh association with the Retail Locations.
				INSERT INTO QRRetailerCustomPageAssociation(QRRetailerCustomPageID
														  , RetailID
														  , RetailLocationID
														  , DateCreated)
													SELECT @PageID
														 , @RetailID
														 , Param
														 , GETDATE()
													FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END
			
			--This will have a value only if the Create own page is selected.
			IF (@UploadFileName IS NOT NULL)
			BEGIN
			
			   --Delete the Existing Media Content.
			   DELETE FROM QRRetailerCustomPageMedia where QRRetailerCustomPageID = @PageID
			
			   --Create the fresh set of Media.
			   INSERT INTO QRRetailerCustomPageMedia(QRRetailerCustomPageID
													, MediaTypeID
													, MediaPath
													, DateCreated)
										  SELECT @PageID
										       ,(SELECT ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType LIKE 'Other Files')
										       , @UploadFileName
										       , GETDATE()														 
										
			END				  
				
			
			-------Find Clear Cache URL---29/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------						  
		   	
									  
		    --Comfirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerSpecialOfferUpdation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
