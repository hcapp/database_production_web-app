USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsDeletion_Today]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsDeletion]
Purpose					: To delete from RssFeedNewsStagingTable.
Example					: [usp_WebRssFeedNewsDeletion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17 Nov 2015		SPAN	        1.1
1.1							Sagar Byali		Changes in Video news
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebRssFeedNewsDeletion_Today]
(
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		DECLARE @ExpiryPublishedDateRockwall DATETIME = GETDATE()-15
		, @ExpiryPublishedDateMarbelVideos DATETIME
		, @ExpiryPublishedDateMarbelAllCategoryNews DATETIME
		, @ExpiryPublishedDateKilleen datetime  = GETDATE()-5
		

		
		SELECT @ExpiryPublishedDateMarbelVideos = max(convert(date,DateCreated)) 
		FROM RssFeedNewsStagingTable 
		WHERE NewsType='videos' AND HcHubCitiID = 10
		
		SELECT @ExpiryPublishedDateMarbelAllCategoryNews = DATEADD(d,-14,max(convert(date,PublishedDate)))
		FROM RssFeedNewsStagingTable 
		WHERE NewsType = 'all' AND HcHubCitiID = 10


		BEGIN TRAN

		--Deleting 15 days older News for Rockwall('spanqa.regionsapp'.)
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateRockwall AND HcHubCitiID = 28

		--Retaining 15 days news for Tyler
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateRockwall AND HcHubCitiID = 10 AND NewsType NOT IN ('Videos','all')

		--Added on 22nd Aug 2016 to retain one day videos 
		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,DateCreated) <> @ExpiryPublishedDateMarbelVideos AND HcHubCitiID = 10 AND NewsType = 'Videos'
	
		--Added on 26th Sept 2016 to retain 14 day news for 'All' category
		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,PublishedDate) < @ExpiryPublishedDateMarbelAllCategoryNews AND HcHubCitiID = 10 AND NewsType = 'all'

		--Deleting 5 days older News for 'Killeen Daily Herald'.
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateKilleen AND HcHubCitiID = 52 

		--Deleting all news for other HubCities.
		DELETE FROM RssFeedNewsStagingTable 
		WHERE HcHubCitiID NOT IN (28 ,52, 10)

		COMMIT TRAN						   
		
		SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRssFeedNewsDeletion].'		
			--- Execute retrieval of Error info.
			ROLLBACK TRAN
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;





GO
