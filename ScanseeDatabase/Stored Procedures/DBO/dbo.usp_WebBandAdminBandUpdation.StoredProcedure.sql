USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAdminBandUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandAdminBandUpdation
Purpose					: 
Example					: usp_WebBandAdminBandUpdation

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			26th April 2016		Prakash C		Initial Version
---------------------------------------------------------------
*/

--exec [dbo].[usp_WebBandAdminBandUpdation] 1,17,'One Direction1',null,null,null,null,null,null,null,null,null,null,null,null
CREATE PROCEDURE [dbo].[usp_WebBandAdminBandUpdation]
(
	--Input parameters
      @UserID int
	, @RetailID int    
    , @RetailName varchar(100)
	, @RetailURL varchar(1000)
	, @Address varchar(1000)
	, @City varchar(50)
	, @State varchar(50)
	, @PosatalCode varchar(5)
	, @Latitude float
	, @Longitude float
	, @CorporatePhoneNo varchar(10)
	
	--Output Variable 	
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			DECLARE @RetailLocationID INT

			SELECT @RetailLocationID = BandLocationID
			FROM BandLocation
			WHERE BandID = @RetailID
			AND CorporateAndStore = 1 AND Active = 1

			DECLARE @RetailAssociatedFlag Int
			DECLARE @RetailLocAssociatedFlag Int
				

		    SET @RetailAssociatedFlag=0
			SET @RetailLocAssociatedFlag =0
			DECLARE @RetailLocID Int

			--To update Band information.
			UPDATE Band SET BandName = @RetailName
							  , BandURL = CASE WHEN @RetailURL = ' ' THEN NULL ELSE  @RetailURL END
							  , Address1 = @Address
							  , City = @City
							  , State = @State
							  , PostalCode = @PosatalCode
							  , CorporatePhoneNo = CASE WHEN @CorporatePhoneNo = ' ' THEN NULL ELSE @CorporatePhoneNo END
							  , ModifyUserID = @UserID
							  , DateModified = GETDATE()
			WHERE BandID = @RetailID AND BandActive = 1

			--To update Band location information.
			IF (@PosatalCode<>(Select PostalCode From BandLocation  WHERE BandID=@RetailID AND Active = 1
					                                                          AND (Headquarters = 1 OR CorporateAndStore = 1)))
					  BEGIN
					      SET @RetailLocAssociatedFlag =1
						  SET @RetailLocID=(Select BandLocationID From BandLocation  WHERE BandID=@RetailID AND Active = 1 
					                                                          AND (Headquarters = 1 OR CorporateAndStore = 1))
					  END	

			UPDATE BandLocation SET Address1 = @Address
									, City = @City
									, State = @State
									, PostalCode = @PosatalCode
									, BandLocationLatitude = @Latitude
									, BandLocationLongitude = @Longitude
									, DateModified = GETDATE()
			WHERE BandID = @RetailID AND (CorporateAndStore = 1 OR Headquarters = 1) AND Active = 1

			--Refresh the association w.r.t the Band location
				--DELETE FROM HcBandAssociation 
				--FROM HcBandAssociation A
				--INNER JOIN BandLocation RL ON RL.BandLocationID = A.BandLocationID
				--WHERE RL.BandLocationID = @RetailLocationID
				--AND RL.Headquarters = 0 AND A.Associated = 1

				--Associate the Band to the Hub Citi.
				--INSERT INTO HcBandAssociation(HcHubCitiID
				--							,BandID
				--							,BandLocationID
				--							,DateCreated
				--							,CreatedUserID
				--							,Associated) 
				--			SELECT DISTINCT LA.HcHubCitiID 
				--								,@RetailID
				--								,@RetailLocationID
				--								,GETDATE()
				--								,@UserID 
				--								,0
				--			FROM HcLocationAssociation LA
				--			INNER JOIN BandLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode 
				--			LEFT JOIN HcBandAssociation RA ON RA.BandLocationID = @RetailLocationID AND LA.HcHubCitiID = RA.HcHubCitiID 
				--			WHERE RA.HcBandAssociationID IS NULL
				--			AND RL.BandLocationID = @RetailLocationID
			 --    			AND RL.Headquarters = 0 
		
		      IF (@RetailLocAssociatedFlag  =1)
			  BEGIN
					UPDATE HcBandAssociation set Associated =0 Where BandLocationID =@RetailLocID  					  
			  END

			-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
		    
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandAdminBandUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












GO
