USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryRebate]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_GalleryRebate  
Purpose     : to display all the rebates in the user rebate gallery  
Example     : usp_GalleryRebate  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   9th Sep 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_GalleryRebate]  
(  
   @UserID int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
   
  --To get the row count for pagination.  
    
   DECLARE @UpperLimit int   
   SELECT @UpperLimit = @LowerLimit + ScreenContent   
   FROM AppConfiguration   
   WHERE ScreenName = @ScreenName   
    AND ConfigurationType = 'Pagination'  
    AND Active = 1  
   DECLARE @MaxCnt int  
     
     --To Fetch Rebate Information from user Rebate gallery   
       
   SELECT Row_Num=ROW_NUMBER() over(order by  R.RebateID)  
    ,R.RebateID  
    ,R.RebateName  
    ,R.RebateShortDescription  
    ,R.RebateLongDescription  
    ,R.RebateAmount  
    ,R.RebateStartDate  
    ,R.RebateEndDate  
    ,dbo.fn_RebateImage(R.RebateID) ImagePath  
    ,UsedFlag  
   INTO #Rebate  
   FROM Rebate R   
    INNER JOIN UserRebateGallery UR on UR.RebateID = R.RebateID  
   WHERE  UserID = @UserID  
    AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate AND UsedFlag=0  
     
   --To capture max row number.  
    SELECT @MaxCnt = MAX(Row_Num) FROM #Rebate  
    --this flag is a indicator to enable "More" button in the UI.   
    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
      
     
   SELECT Row_Num rowNum  
    ,RebateID  
    ,RebateName  
    ,RebateShortDescription  
    ,RebateLongDescription  
    ,RebateAmount  
    ,RebateStartDate  
    ,RebateEndDate  
    ,ImagePath productImagePath  
    ,UsedFlag  
   FROM #Rebate  
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   Order by Row_Num  
  
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_GalleryRebate.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
     
  END;  
     
 END CATCH;  
END;

GO
