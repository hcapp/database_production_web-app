USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAddRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerAddRetailerLocation
Purpose					: 
Example					: usp_WebRetailerAddRetailerLocation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			2nd April 2012	Naga Sandhya S	Initial Version
2.0			25/11/2016		Bindu T A		Hours - Filters Changes- Day Wise
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAddRetailerLocation]
(
	 @RetailID int
	,@StoreIdentification varchar(100)
	,@StoreAddress varchar(150)
	,@State Char(2)
	,@City varchar(50)
	,@PostalCode varchar(10)
	,@Phone Char(10)
	,@StartTime VARCHAR(1000)
	,@EndTime VARCHAR(1000)
	,@StartTimeUTC VARCHAR(1000)
	,@EndTimeUTC VARCHAR(1000)
	,@TimeZoneID INT
	,@RetailLocationURL VARCHAR(1000)
	,@RetailLocationLatitude float
	,@RetailLocationLongitude float
	,@RetailLocationKeyword Varchar(1000)
	,@RetailLocationImagePath Varchar(1000)
	--Output Variable 
	,@FindClearCacheURL VARCHAR(1000) output
	,@ResponseRetailLocationID int output
	,@StoreExists int output
	,@Status int output
	,@ErrorNumber int output
	,@ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			CREATE TABLE #StartTime ( StartTimes TIME, Num INT IDENTITY(1,1) )
			CREATE TABLE #EndTime ( EndTimes TIME, Num INT IDENTITY(1,1) )
			CREATE TABLE #StartTimeUTC ( StartTimeUTCs TIME, Num INT IDENTITY(1,1) )
			CREATE TABLE #EndTimeUTC ( EndTimeUTCs TIME, Num INT IDENTITY(1,1) )

			INSERT INTO #StartTime (StartTimes )
						SELECT Param
						FROM fn_SplitParam (@StartTime,',')

			INSERT INTO #EndTime (EndTimes)
						SELECT Param
						FROM fn_SplitParam (@EndTime,',')

			INSERT INTO #StartTimeUTC (StartTimeUTCs)
						SELECT Param
						FROM fn_SplitParam (@StartTimeUTC,',')

			INSERT INTO #EndTimeUTC (EndTimeUTCs)
						SELECT Param
						FROM fn_SplitParam (@EndTimeUTC,',')
			
			IF Not Exists(SELECT 1 FROM RetailLocation Where StoreIdentification=@StoreIdentification AND RetailID = @RetailID AND Active = 1)
			BEGIN
				SET @StoreExists=0
				INSERT INTO [RetailLocation]
						   ([RetailID]
						   ,[Headquarters]
						   ,[Address1]
						   ,[City]
						   ,[State]
						   ,[PostalCode]
						   --,[StartTime]
						   --,[EndTime]
						   --,[StartTimeUTC]
						   --,[EndTimeUTC]
						   ,[TimeZoneID]
						   , RetailLocationLatitude
						   , RetailLocationLongitude
						   ,[CountryID]
						   , RetailLocationURL
						   ,[DateCreated]
						   ,[DateModified]
						   ,[StoreIdentification]
						   ,RetailLocationImagePath)
				 VALUES(@RetailID 
						,0
						,@StoreAddress 
						,@City
						,@State
						,@PostalCode
						--,@StartTime
						--,@EndTime
						--,@StartTimeUTC
						--,@EndTimeUTC
						,@TimeZoneID
						,CASE WHEN @RetailLocationLatitude IS NULL OR @RetailLocationLatitude = 0 THEN (SELECT Latitude FROM GeoPosition WHERE City = @City AND State = @State AND PostalCode = @PostalCode) ELSE @RetailLocationLatitude END 
						,CASE WHEN @RetailLocationLongitude IS NULL OR @RetailLocationLongitude = 0 THEN (SELECT Longitude FROM GeoPosition WHERE City = @City AND State = @State AND PostalCode = @PostalCode) ELSE @RetailLocationLongitude END
						,1
						,@RetailLocationURL
						,GETDATE()
						,GETDATE()
						,@StoreIdentification
						,@RetailLocationImagePath)

				 DECLARE @RetailLocationID int
				 SET @RetailLocationID=SCOPE_IDENTITY()

				 
				INSERT INTO HcHoursFilterTime 
				 (
				 RetailLocationID,
				 HcDaysOfWeekID,
				 StartTime,
				 EndTime,
				 StartTimeUTC,
				 EndTimeUTC,
				 [DateCreated],
				 [DateModified]
				 )
				 SELECT 
				 @RetailLocationID,
				 ROW_NUMBER ()OVER (ORDER BY S.num),
				 CASE WHEN S.StartTimes = '00:00' AND E.EndTimes = '00:00' THEN CONVERT(datetime,NULL,121)
						   ELSE S.StartTimes END, 
				 CASE WHEN E.EndTimes = '00:00' AND S.StartTimes = '00:00' THEN CONVERT(datetime,NULL,121)
							ELSE E.EndTimes END ,
				 CASE WHEN SU.StartTimeUTCs = '00:00' AND EU.EndTimeUTCs = '00:00' THEN CONVERT(datetime,NULL,121)
							ELSE SU.StartTimeUTCs END,
				 CASE WHEN EU.EndTimeUTCs = '00:00' AND SU.StartTimeUTCs = '00:00' THEN CONVERT(datetime,NULL,121)
							ELSE EU.EndTimeUTCs END,
				 GETDATE(),
				 GETDATE()			 			
				 FROM #StartTime S 
				 INNER JOIN #EndTime E ON S.Num= E.Num
				 INNER JOIN #StartTimeUTC SU ON SU.Num = S.Num AND SU.Num = S.Num
				 INNER JOIN #EndTimeUTC EU ON EU.Num = SU.Num AND SU.Num = EU.Num

				 -- To update city state in Retaillocation 
				 UPDATE RetailLocation
				 SET HcCityID = C.HcCityID
				 FROM RetailLocation RL
				 INNER JOIN HcCity C ON C.CityName = RL.City
				 WHERE RL.RetailLocationID = @RetailLocationID

				 UPDATE RetailLocation
				 SET StateID = C.StateID
				 FROM RetailLocation RL
				 INNER JOIN State C ON C.Stateabbrevation = RL.State
				 WHERE RL.RetailLocationID = @RetailLocationID




				 --Contact
				 DECLARE @CreateUserID int
				 SELECT @CreateUserID=UserID  
				 FROM UserRetailer
				 WHERE RetailID=@RetailID
				 
				 INSERT INTO [Contact]
						   ([ContactPhone]						   
						   ,[DateCreated]
						   ,[CreateUserID]
						   ,[DateModified])
					 VALUES (@Phone
							,GETDATE()
							,@CreateUserID
							,GETDATE())
							
				DECLARE @ContactID int
				SET @ContactID=SCOPE_IDENTITY()	
				 
				 --RetailContact
				 
				 INSERT INTO [RetailContact]
						   ([ContactID]
						   ,[RetailLocationID]
						   )
				   VALUES(@ContactID
						  ,@RetailLocationID
						)
			  --INSERT INTO RetailerKeywords Table
			  IF(@RetailLocationKeyword IS NOT NULL)
			  BEGIN
				  INSERT INTO RetailerKeywords
							  (RetailID
							  ,RetailLocationID 
							  ,RetailKeyword 
							  ,DateCreated)
				  VALUES(@RetailID 
						 ,@RetailLocationID 
						 ,@RetailLocationKeyword
						 ,GETDATE())
			  END

			--Insert into HcRetailerAssociation table.			
			INSERT INTO HcRetailerAssociation(HcHubCitiID
												,RetailID
												,RetailLocationID
												,DateCreated
												,Associated) 
								SELECT DISTINCT HcHubcitiID 
												  ,@RetailID
												  ,@RetailLocationID
												  ,GETDATE()
												  ,0
								FROM HcLocationAssociation LA
								INNER JOIN RetailLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode 
								WHERE RL.RetailLocationID = @RetailLocationID
			     				AND RL.Headquarters = 0 AND RL.Active = 1             
			  
			  -------Find Clear Cache URL---29/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'

			SELECT @FindClearCacheURL= @CurrentURL+Screencontent--+','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------
			
		END
			
		ELSE
		BEGIN
			SET @StoreExists=1
		END
			
	--Confirmation of Success.
		SELECT @Status = 0
		set @ResponseRetailLocationID = @RetailLocationID 
	COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAddRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
