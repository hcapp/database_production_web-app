USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebLogin]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebLogin
Purpose					: To Login to the Web Application.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			11th December 2011				SPAN Infotech	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebLogin]
(

	--Input Input Parameter(s)--
	
	  @UserName varchar(100)
	, @Password varchar(60) 	
	
	--Output Variable--
	  
	, @Status INT OUTPUT
	, @UserType INT OUTPUT 
	, @LandingPage varchar(255) OUTPUT
    , @ResetPassword BIT OUTPUT
	 
	--, @UserID INT OUTPUT                   -- 1 = Consumer, 2 = Manufacturer/ Supplier, 3 = Retailer, 4 = Band
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT
	, @EventActiveFlag BIT OUTPUT 
)
AS
BEGIN

	BEGIN TRY	
			 DECLARE @UserID INT

			--To get Media Server Configuration.
			 DECLARE @ManufacturerConfig varchar(50)  
			 SELECT @ManufacturerConfig=ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 

	 
			 --To get Media Server Configuration.
			 DECLARE @RetailerConfig varchar(50)  
			 SELECT @RetailerConfig=
			 ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration' 

			 DECLARE @BandConfig varchar(50)  
			 SELECT @BandConfig=
			 ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Band Media Server Configuration' 

			 --To Check Deactivated Retailer
			 DECLARE @DeactivatedRetailer bit = 0
			 SELECT @DeactivatedRetailer = 1 
			 FROM Users U
			 INNER JOIN UserRetailer UR ON UR.UserID = U.UserID
			 INNER JOIN Retailer R ON UR.RetailID = R.RetailID 
			 WHERE U.UserName = @UserName AND R.RetailerActive = 0

			 if @DeactivatedRetailer=0

			 SELECT @DeactivatedRetailer = 1 
			 FROM Users U
			 INNER JOIN Userband UR ON UR.UserID = U.UserID
			 INNER JOIN band R ON UR.BandID = R.BandID 
			 WHERE U.UserName = @UserName AND R.BandActive = 0

			 --select @DeactivatedRetailer

		 
	   IF EXISTS(SELECT 1 FROM Users WHERE BINARY_CHECKSUM(UserName)= BINARY_CHECKSUM(@UserName) AND BINARY_CHECKSUM([Password]) = BINARY_CHECKSUM(@Password))
	   BEGIN
	   IF (@DeactivatedRetailer = 0)
	   BEGIN 

			SELECT @UserID = UserID
				 , @ResetPassword = ResetPassword
			FROM Users WHERE UserName = @UserName
			
			--IF EXISTS(SELECT 1 FROM UserManufacturer WHERE UserID = @UserID)
			--BEGIN

			--	SELECT M.ManufName 'Supplier Name'
			--		 , M.ManufDescription 'Description'
			--		 , C.ContactEmail
			--		 , C.ContactFirstName firstName
			--		 , C.ContactLastname  lastName
			--		 , C.ContactMobilePhone				
			--		 , U.UserID userID
			--		 , U.ShowPanel
			--		 , MU.ManufacturerID supplierId
			--		 , M.Address1
			--		 , M.Address2					 
			--		 , M.[State]
			--		 , M.City
			--		 , M.PostalCode			
			--		 FROM Users U INNER JOIN UserManufacturer MU ON U.UserID = MU.UserID
			--		 INNER JOIN Manufacturer M ON M.ManufacturerID = MU.ManufacturerID
			--		 LEFT OUTER JOIN ManufacturerContact MC ON MC.ManufacturerID = M.ManufacturerID
			--		 LEFT OUTER JOIN Contact C ON C.ContactID = MC.ContactID
			--		 WHERE U.UserName = @UserName AND U.[Password] = @Password			 
			--		SET @UserType = 2 
			--		SET @Status = 0
					
			--		DECLARE @ManufacturerID INT 
			--		SELECT @ManufacturerID = ManufacturerID FROM UserManufacturer WHERE UserID = @UserID
			--		--Check if ProductSetup is done.
			--		IF EXISTS(SELECT 1 FROM Product WHERE ManufacturerID = @ManufacturerID)
			--		BEGIN
			--			--Check if Choose Plan is done.
			--			IF EXISTS(SELECT 1 FROM ManufacturerBillingDetails WHERE ManufacturerID = @ManufacturerID)
			--			BEGIN
			--				SET @LandingPage = 'SUPPLIERDASHBOARD'
			--			END
			--			ELSE
			--			BEGIN
			--				SET @LandingPage = 'NOCHOOSEPLAN'
			--			END						
			--		END
			--		ELSE
			--		BEGIN
			--			SET @LandingPage = 'NOPRODUCT'
			--		END					
			--END
			--ELSE 


			

			IF EXISTS(SELECT 1 FROM UserRetailer U INNER JOIN Retailer R ON U.RetailID = R.RetailID WHERE U.UserID = @UserID AND R.RetailerActive = 1)
			BEGIN

			


				SELECT DISTINCT C.ContactFirstName firstName
				     , C.ContactLastname  lastName
				     , C.ContactPhone
				     , R.RetailName 
				     , U.UserID userID
				     , U.ShowPanel
				     , RU.RetailID retailerId
					 , R.Address1
					 , R.Address2					
					 , R.[State]
					 , R.City
					 , R.PostalCode
					 , retailerLogoImage = CASE WHEN RetailerImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @RetailerConfig +  CONVERT(VARCHAR(30),R.RetailID) +'/'+ RetailerImagePath
										        ELSE RetailerImagePath END 					
				FROM Users U
				INNER JOIN UserRetailer RU ON U.UserID = RU.UserID
				INNER JOIN Retailer R ON R.RetailID = RU.RetailID
				LEFT OUTER JOIN RetailLocation RL ON RL.RetailID = R.RetailID
				LEFT OUTER JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
				LEFT OUTER JOIN Contact C ON C.ContactID = RC.ContactID
				WHERE U.UserName = @UserName AND U.[Password] = @Password AND RL.Active = 1 AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)
				
				DECLARE @RetailerID INT 
				SELECT @RetailerID = RetailID FROM UserRetailer WHERE UserID = @UserID
				
				--Check if the RetailerLocations are setup.		
				IF EXISTS(SELECT 1 FROM RetailLocation WHERE RetailID = @RetailerID)-- AND Headquarters <> 1)
				BEGIN
					--Check if the Retailer has payed.
					IF EXISTS(SELECT 1 FROM RetailerBillingDetails WHERE RetailerID = @RetailerID AND IsPaid = 1)
					BEGIN
						SET @LandingPage = 'RETAILERDASHBOARD'
					END
					ELSE
					BEGIN
						SET @LandingPage = 'NORETAILERCHOOSEPLAN'
					END
				END
				--Check if the Retailer has uploaded the Logo
				ELSE IF EXISTS(SELECT 1 FROM Retailer WHERE RetailID = @RetailerID AND RetailerImagePath IS NOT NULL AND RetailerActive = 1)
				BEGIN
					SET @LandingPage = 'NORETAILERLOCATION'
				END
				ELSE
				--Logo is not mandatory. So this is checked at the end.
				BEGIN
					SET @LandingPage = 'NORETAILERLOGO'
				END
				
				IF EXISTS (SELECT 1 FROM HcRetailerAssociation WHERE RetailID = @RetailerID AND Associated = 1)
					BEGIN
							SET @EventActiveFlag = 1
					END
				ELSE 
					BEGIN
							SET @EventActiveFlag = 0
					END	
				
				SET @UserType = 3
				SET @Status = 0
				
			END

			ELSE IF EXISTS(SELECT 1 FROM UserBand U INNER JOIN Band R ON U.BandID = R.BandID WHERE U.UserID = @UserID AND R.BandActive = 1)
			BEGIN


			--select 'a'
				SELECT DISTINCT C.ContactFirstName firstName
				     , C.ContactLastname  lastName
				     , C.ContactPhone
				     , R.BandName RetailerName
				     , U.UserID userID
				     , U.ShowPanel
				     , RU.BandID RetailerId
					 , R.Address1
					 , R.Address2					
					 , R.[State]
					 , R.City
					 , R.PostalCode
					 ,'http://66.228.143.28:8080/Images/band/web.png'  as BandLogoImage
					-- , BandLogoImage = CASE WHEN BandImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @RetailerConfig +  CONVERT(VARCHAR(30),R.BandID) +'/'+ BandImagePath
							--			        ELSE @RetailerConfig +  CONVERT(VARCHAR(30),R.BandID) +'/'+ BandImagePath END 					
				FROM Users U
				INNER JOIN UserBand RU ON U.UserID = RU.UserID
				INNER JOIN Band R ON R.BandID = RU.BandID
				LEFT OUTER JOIN BandLocation RL ON RL.BandID = R.BandID
				LEFT OUTER JOIN BandContact RC ON RC.BandLocationID = RL.BandLocationID
				LEFT OUTER JOIN Contact C ON C.ContactID = RC.ContactID
				WHERE U.UserName = @username AND U.[Password] = @Password AND r.bandActive = 1-- AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)
				
				DECLARE @BandID INT 
				SELECT @BandID = BandID FROM UserBand WHERE UserID = @UserID
				
				--Check if the BandLocations are setup.		
				IF EXISTS(SELECT 1 FROM BandLocation WHERE BandID = @BandID)-- AND Headquarters <> 1)
				BEGIN
					--Check if the Band has payed.
					IF EXISTS(SELECT 1 FROM BandBillingDetails WHERE BandID = @BandID AND IsPaid = 1)
					BEGIN
						SET @LandingPage = 'BandDASHBOARD'
					END
					ELSE
					BEGIN
						SET @LandingPage = 'NOBandCHOOSEPLAN'
					END
				END
				--Check if the Band has uploaded the Logo
				ELSE IF EXISTS(SELECT 1 FROM Band WHERE BandID = @BandID AND BandActive = 1 )--AND BandImagePath IS NOT NULL)
				BEGIN
					SET @LandingPage = 'NOBandLOCATION'
				END
				ELSE
				--Logo is not mandatory. So this is checked at the end.
				BEGIN
					SET @LandingPage = 'NOBandLOGO'
				END
				
				--IF EXISTS (SELECT 1 FROM HcBandAssociation WHERE BandID = @BandID AND Associated = 1)
					BEGIN
							SET @EventActiveFlag = 1
					END
				--ELSE 
				--	BEGIN
				--			SET @EventActiveFlag = 0
				--	END	
				
				SET @UserType = 4
				SET @Status = 0
				
			END

			ELSE IF EXISTS(SELECT 1 FROM Users WHERE UserID = @UserID)
			BEGIN

	
		
				SELECT U.FirstName firstName
				     , U.Lastname lastName
				     , U.Email 
				     , U.MobilePhone				  
				     , U.UserID userID
				     , U.Address1 
				     , U.Address2
				     , U.[State]
				     , U.City
				     , U.PostalCode
				     , U.ShowPanel
				     
				FROM Users U
				WHERE U.UserName = @UserName AND U.[Password] = @Password
				
				--Check if the user has preferences set.
				IF EXISTS(SELECT 1 FROM UserCategory WHERE UserID = @UserID)
				BEGIN
					IF EXISTS(SELECT 1 FROM UserUniversity WHERE UserID = @UserID)
					BEGIN
						SET @LandingPage = 'SHOPPERDASHBOARD'
					END
					ELSE
					BEGIN
						SET @LandingPage = 'NOSHOPPERSELECTSCHOOL'
					END				
				END
				ELSE
				BEGIN
					SET @LandingPage = 'NOSHOPPERPREFERENCES'
				END
				
				SET @UserType = 1
				SET @Status = 0
			END
		END	
		END
	   
		--Confirmation of Success.		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebLogin.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 		
			
		END;
		 
	END CATCH;
END;





GO
