USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchAPIAlatestDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchAPIAlatestDataPorting
Purpose					: To move data from Stage APIAlatestData table to Production ProductReviews Table
Example					: usp_BatchAPIAlatestDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd Nov 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchAPIAlatestDataPorting]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'AlaTest'
			
			MERGE ProductReviews AS T
			USING (SELECT ProductID
						   ,AlaId
						   ,ReviewURL
						   ,p.ProductName
						   ,TestSummary
						   ,@APIPartnerID APIPartnerID
						   --,APIPartnerID=(SELECT APIPartnerID FROM APIPartner where APIPartnerName='AlaTest')   
					FROM APIAlaTestData A
						INNER JOIN Product P on P.ProductName=A.ProductName
					WHERE TestSummary IS NOT NULL AND ReviewURL IS NOT NULL) AS S
			ON (T.SourceID = S.AlaID) 
			WHEN NOT MATCHED BY TARGET 
				THEN INSERT([ProductID]
						   ,[ReviewURL]
						   ,[ReviewComments]
						   ,[DateCreated]
						   ,[DateModified]
						   ,[APIPartnerID]
						   ,[SourceID]) 
						   
					VALUES(S.ProductID
						  , S.ReviewURL
						  , S.TestSummary
						  , GETDATE()
						  , NULL
						  , S.APIPartnerID
						  , S.AlaId)
						  
			WHEN MATCHED
				THEN UPDATE SET T.[ProductID]=S.[ProductID]
							   , T.[ReviewURL]= S.ReviewURL
							   , T.[ReviewComments]= S.TestSummary
							   , T.[DateCreated]=T.DateCreated
							   , T.[Datemodified]=GETDATE()
							   , T.[SourceID]=S.AlaId ;
			
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchAPIAlatestDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
