USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingList
Purpose					: To fetch User shopping list 
Example					: EXEC usp_MasterShoppingList 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingList]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY 
	--To ftech shopping lists Product details which was added thro Shopping list Search Screen.
		SELECT UP.UserProductID  
			, UP.UserID
			, UP.ProductID
			, P.ProductName
			, 0 AS RetailID
			, 'Others' AS RetailName
			, C.CategoryID 
			, C.ParentCategoryID 
			, C.ParentCategoryName 
			, C.SubCategoryID 
			, C.SubCategoryName 
			, Coupon.CouponID 
			, Loyalty.LoyaltyDealID
			, Rebate.RebateID
			--, ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0
			--			       WHEN (up.TodayListtItem=1) THEN 1
			--				END 
		INTO #Det
		FROM UserProduct UP
		INNER JOIN Product P ON P.ProductID = UP.ProductID 
		LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
		LEFT JOIN Category C ON C.CategoryID = PC.CategoryID  
		OUTER APPLY (SELECT Distinct C.CouponID FROM Coupon C  INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID WHERE CP.ProductID = UP.ProductID AND CouponExpireDate >= GETDATE()) Coupon
		OUTER APPLY (SELECT LoyaltyDealID FROM LoyaltyDeal WHERE ProductID = UP.ProductID AND LoyaltyDealExpireDate >= GETDATE())Loyalty
		OUTER APPLY (SELECT Distinct R.RebateID FROM RebateProduct RP INNER JOIN Rebate R ON R.RebateID = RP.RebateID  WHERE ProductID = UP.ProductID AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate )Rebate
		WHERE UP.UserID = @UserID
		AND UP.UserRetailPreferenceID IS NULL AND UP.ProductID <> 0
		AND UP.MasterListItem = 1
		AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE()) 
		
		--To fetch Unassigned Product details in Shopping list
		SELECT UP.UserProductID
				, UP.UserID
				, ISNULL(UP.ProductID,0) AS ProductID
				, UP.UnassignedProductName 
				, 0 AS RetailID
				, 'Others' AS RetailName
				, CouponID = 0
				, LoyaltyDealID = 0
				, Rebate = 0
				--, ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0
				--		       WHEN (up.TodayListtItem=1) THEN 1
				--		END 
			INTO #Unass
			FROM UserProduct UP
			WHERE UP.UserID = @UserID 
			--AND UP.ProductID IS NULL 
			AND UP.ProductID = 0
			AND UP.MasterListItem = 1
			
		--To fetch shopping list product details which has Retailer binded with it.
		SELECT UP.UserProductID
			, UP.UserID
			, UP.ProductID
			, P.ProductName 
			, R.RetailID 
			, R.RetailName 
			, C.CategoryID 
			, C.ParentCategoryID 
			, C.ParentCategoryName 
			, C.SubCategoryID 
			, C.SubCategoryName 
			, Coupon.Coupon 
			, Loyalty.Loyalty
			, Rebate.Rebate 
			--, ButtonFlag = CASE WHEN (up.TodayListtItem=0) THEN 0
			--			       WHEN (up.TodayListtItem=1) THEN 1
			--			END 
		INTO #CLR				
		FROM UserProduct UP
			INNER JOIN Product P ON P.ProductID = UP.ProductID
			LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			LEFT JOIN Category C ON C.CategoryID = PC.CategoryID  
			LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID 
			LEFT JOIN Retailer R ON R.RetailID = UR.RetailID  
			OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon
			OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty
			OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate
		WHERE UP.UserID = @UserID
			AND UP.MasterListItem = 1 
			AND UP.UserRetailPreferenceID IS NOT NULL
			AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())
			
		SELECT UserProductID userProductID
			, CLR.UserID
			, CLR.ProductID
			, CLR.ProductName 
			, RetailID  retailerId
			, RetailName retailerName
			, CategoryID categoryID
			, ParentCategoryID parentCategoryID
			, ParentCategoryName parentCategoryNam
			, SubCategoryID subCategoryID
			, SubCategoryName subCategoryName
			--, ButtonFlag
			, CLRFlag = CASE WHEN (COUNT(CLR.Coupon)+COUNT(CLR.Loyalty)+COUNT(CLR.Rebate)) > 0 THEN 1 ELSE 0 END
			--, coupon = CASE WHEN COUNT(CLR.Coupon) = 0 THEN 0 ELSE 1 END
			--, loyalty = CASE WHEN COUNT(CLR.Loyalty) = 0 THEN 0 ELSE 1 END
			--, rebate = CASE WHEN COUNT(CLR.Rebate) = 0 THEN 0 ELSE 1 END
			, coupon_Status = CASE WHEN COUNT(CLR.Coupon) = 0 THEN 'Grey' 
								   ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END
							  END
			, loyalty_Status = CASE WHEN COUNT(CLR.Loyalty) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END
							   END
			, rebate_Status = CASE WHEN COUNT(CLR.Rebate) = 0 THEN 'Grey'
								   ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END
							  END
		FROM #CLR CLR
			LEFT JOIN UserCouponGallery UC ON UC.UserID = CLR.UserID AND UC.CouponID = CLR.Coupon
			LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = CLR.UserID AND UL.LoyaltyDealID = CLR.Loyalty
			LEFT JOIN UserRebateGallery UR ON UR.UserID = CLR.UserID AND UR.RebateID = CLR.Rebate
		WHERE CLR.UserID = @UserID 
		GROUP BY UserProductID
			, CLR.UserID
			, RetailID 
			, RetailName
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName 	
			, CLR.ProductID
			, CLR.ProductName
			--, ButtonFlag 
			
		UNION ALL
		SELECT UserProductID
			, D.UserID
			, D.ProductID
			, D.ProductName
			, RetailID 
			, RetailName  
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName
			--, ButtonFlag
			, CLRFlag = CASE WHEN (COUNT(D.CouponID)+COUNT(D.LoyaltyDealID)+COUNT(D.RebateID)) > 0  THEN 1 ELSE 0 END
			--, Coupon = CASE WHEN COUNT(D.CouponID) = 0 THEN 0 ELSE 1 END
			--, Loyalty = CASE WHEN COUNT(D.LoyaltyDealID) = 0 THEN 0 ELSE 1 END
			--, Rebate = CASE WHEN COUNT(D.RebateID) = 0 THEN 0 ELSE 1 END
			, Coupon_Status = CASE WHEN COUNT(D.CouponID) = 0 THEN 'Grey'
								   ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END
							  END
			, Loyalty_Status = CASE WHEN COUNT(D.LoyaltyDealID) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END
							   END 
			, Rebate_Status = CASE WHEN COUNT(D.RebateID) = 0 THEN 'Grey'
									ELSE CASE WHEN COUNT(UR.RebateID) = 0 THEN 'Red' ELSE 'Green' END
							   END 
		FROM #Det D
		LEFT JOIN UserCouponGallery UC ON UC.UserID = D.UserID AND UC.CouponID = D.CouponID
		LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = D.UserID AND UL.LoyaltyDealID = D.LoyaltyDealID	
		LEFT JOIN UserRebateGallery UR ON UR.UserID = D.UserID AND UR.RebateID = D.RebateID 
		GROUP BY UserProductID
			, D.UserID
			, RetailID 
			, CategoryID 
			, ParentCategoryID 
			, ParentCategoryName 
			, SubCategoryID 
			, SubCategoryName 
			, RetailName	
			, D.ProductID
			, D.ProductName
			--, ButtonFLAG
			
		UNION ALL
		SELECT UserProductID
			, UserID
			, ProductID
			, UnassignedProductName 
			, RetailID
			, RetailName
			, NULL CategoryID 
			, NULL ParentCategoryID 
			, NULL ParentCategoryName 
			, NULL SubCategoryID 
			, NULL SubCategoryName
			--, ButtonFlag
			, CLRFlag = 0
			--, CouponID 
			--, LoyaltyDealID 
			--, Rebate 
			, Coupon_Status = 'Grey'
			, Loyalty_Status = 'Grey'
			, Rebate_Status = 'Grey'
		FROM #Unass
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
