USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAdminDeleteBand]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebBandAdminDeleteBand
Purpose					: To delete single/list of Bands from the system.
Example					: usp_WebBandAdminDeleteBand

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			26th April 2016		Prakash C		Initial Version
---------------------------------------------------------------
*/
--exec usp_WebBandAdminDeleteBand 17,1,null,null,null,null
CREATE PROCEDURE [dbo].[usp_WebBandAdminDeleteBand]

		--Input parameters	
	  @RetailID VARCHAR(MAX)	
	, @UserID INT

	--Output variables	
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			

			--Update statement is added for the Logical delete implementaion 
			--and rest of the delete statements are commented.
			UPDATE Band SET BandActive=0
			FROM Band R 
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') RD ON R.BandID=RD.Param;
			
			UPDATE BandLocation SET Active=0
			FROM BandLocation RL 
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') RLD ON RL.BandID=RLD.Param;
			
			
			--The below delete statements are commented with /* */ symbols.
			/*SELECT Param BandID
			INTO #Band
			FROM dbo.fn_SplitParam(@RetailID,',')			
			
			INSERT INTO BandDeletionHistory(BandID
											,BandName
											,Address1
											,Address2
											,Address3
											,Address4
											,City
											,State
											,PostalCode
											,CountryID
											,BandImagePath
											,UserID
											,DateCreated
											,ModifyUserID
											,DateModified
											,ProgramParticipant
											,WishPondBandID
											,POSIntegrated
											,BandURL
											,LegalAuthorityFirstName
											,LegalAuthorityLastName
											,CorporatePhoneNo
											,IsNonProfitOrganisation
											,WebsiteSourceFlag
											,BandOnlineURL
											,NumberOfLocations											
											,CellFireMerchantID
											,CellFireMerchantSmallImage
											,CellFireMerchantLargeImage
											,FreeApplistingReferralName
											,FreeApplistingBand
											,AssociateOrganizations
											,DuplicateBandID
											,DuplicateBandLocationID
											)
											
			SELECT 							BandID
											,BandName
											,Address1
											,Address2
											,Address3
											,Address4
											,City
											,State
											,PostalCode
											,CountryID
											,BandImagePath
											,@UserID 
											,GETDATE()
											,ModifyUserID
											,DateModified
											,ProgramParticipant
											,WishPondBandID
											,POSIntegrated
											,BandURL
											,LegalAuthorityFirstName
											,LegalAuthorityLastName
											,CorporatePhoneNo
											,IsNonProfitOrganisation
											,WebsiteSourceFlag
											,BandOnlineURL
											,NumberOfLocations											
											,CellFireMerchantID
											,CellFireMerchantSmallImage
											,CellFireMerchantLargeImage
											,FreeApplistingReferralName
											,FreeApplistingBand
											,AssociateOrganizations
											,DuplicateBandID
											,DuplicateBandLocationID
												
			FROM Band R	
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') F ON R.Bandid = F.Param	
			
			INSERT INTO BandLocationDeletionHistory(BandLocationID
												,BandID
												,UserID
												,Headquarters
												,Address1
												,Address2
												,Address3
												,Address4
												,City
												,State
												,PostalCode
												,CountryID
												,BandLocationLatitude
												,BandLocationLongitude
												,BandLocationTimeZone
												,DateCreated
												,DateModified
												,BandLocationStoreHours
												,StoreIdentification
												,WishPondBandID
												,OnlineStoreFlag
												,CorporateAndStore
												,BandLocationURL
												,DuplicateFlag
												,FreeAppBandID)		
			SELECT								BandLocationID
												,BandID
												,@UserID
												,Headquarters
												,Address1
												,Address2
												,Address3
												,Address4
												,City
												,State
												,PostalCode
												,CountryID
												,BandLocationLatitude
												,BandLocationLongitude
												,BandLocationTimeZone
												,GETDATE()
												,DateModified
												,BandLocationStoreHours
												,StoreIdentification
												,WishPondBandID
												,OnlineStoreFlag
												,CorporateAndStore
												,BandLocationURL
												,DuplicateFlag
												,FreeAppBandID																			
			FROM BandLocation RL	
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') F ON RL.BandID = F.Param											

			SELECT C.CouponID
			INTO #Coupon
			FROM Coupon C		
			INNER JOIN #Band R ON  R.BandID= C.BandID						

			SELECT RE.RebateID 
			INTO #Rebate
			FROM Rebate RE
			INNER JOIN RebateBand RR ON RE.RebateID=RR.RebateID 
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=RR.BandID

			SELECT ProductHotDealID
			INTO #HotDeal
			FROM ProductHotDeal P
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=P.BandID
			
			SELECT AdvertisementBannerID
			INTO #AdvertisementBanner
			FROM AdvertisementBanner A
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=A.BandID

			SELECT LoyaltyProgramID
			INTO #LoyaltyProgram
			FROM LoyaltyProgram A
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=A.BandID

			--Add's			
			
			DELETE FROM UserAdvertisementHit
			FROM UserAdvertisementHit RA
			INNER JOIN BandLocationAdvertisement AD ON AD.BandLocationAdvertisementID =RA.BandLocationAdvertisementID 
			INNER JOIN BandLocation RL ON AD.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=RL.BandID
			
            print'BandLocationAdvertisement'
			DELETE FROM BandLocationAdvertisement
			FROM BandLocationAdvertisement RA
			INNER JOIN BandLocation RL ON RA.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=RL.BandID
			
			print'BandLocationBannerAd'
			DELETE FROM BandLocationBannerAd
			FROM BandLocationBannerAd RA
			INNER JOIN BandLocation RL ON RA.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=RL.BandID
			
			print'AdvertisementBanner'
			DELETE FROM AdvertisementBanner
			FROM AdvertisementBanner RA
			INNER JOIN #Band R ON R.BandID=RA.BandID
			
			print'BandLocationSplashAd'
			DELETE FROM BandLocationSplashAd
			FROM BandLocationSplashAd RA
			INNER JOIN BandLocation RL ON RA.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=RL.BandID
			
			print'AdvertisementSplash'
			DELETE FROM AdvertisementSplash
			FROM AdvertisementSplash RA
			INNER JOIN #Band R ON R.BandID=RA.BandID
			
			print'CellFireUserCardsHistory'
			DELETE FROM CellFireUserCardsHistory
			FROM CellFireUserCardsHistory RA
			INNER JOIN #Band R ON R.BandID=RA.BandID
			
			print'CellFireUserCards'
			DELETE FROM CellFireUserCards
			FROM CellFireUserCards RA
			INNER JOIN #Band R ON R.BandID=RA.BandID
			

			--Band data deletion
		
			print'BandKeywords'
			DELETE FROM BandKeywords
			FROM BandKeywords RK
			INNER JOIN #Band RR ON RK.BandID=RR.BandID

			print'BandCategory'
			DELETE FROM BandCategory
			FROM BandCategory R
			INNER JOIN #Band RR ON R.BandID=RR.BandID

			print'ProductReviews'
			DELETE FROM ProductReviews
			FROM ProductReviews P
			INNER JOIN #Band RR ON P.BandID=RR.BandID

			print'UserBandPreference'
			DELETE FROM UserBandPreference
			FROM UserBandPreference U
			INNER JOIN #Band R ON U.BandID=R.BandID
			
			print'BandBillingDetails'
			DELETE FROM BandBillingDetails
			FROM BandBillingDetails RB
			INNER JOIN #Band R ON R.BandID=RB.BandID			
			
			print'BandBankInformation'
			DELETE FROM BandBankInformation
			from BandBankInformation RB
			INNER JOIN #Band R ON R.BandID=RB.BandID

			print'UserPushNotification'
			DELETE FROM UserPushNotification
			FROM UserPushNotification PA
			INNER JOIN BandLocationDeal RD ON PA.BandLocationDealID = RD.BandLocationDealID
			INNER JOIN BandLocation RL ON RD.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=rl.BandID
			
			print'BandLocationDeal'
			DELETE FROM BandLocationDeal
			FROM BandLocationDeal PA
			INNER JOIN BandLocation RL ON PA.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=rl.BandID
								
			print'BandLocationProduct'
			DELETE FROM BandLocationProduct
			FROM BandLocationProduct PA
			INNER JOIN BandLocation RL ON PA.BandLocationID=RL.BandLocationID
			INNER JOIN #Band R ON R.BandID=rl.BandID
			
			
			SELECT ContactID 
			INTO #BandContact
			FROM BandContact R
			INNER JOIN BandLocation RL ON R.BandLocationID=RL.BandLocationID
			INNER JOIN #Band RR ON RL.BandID=RR.BandID
				
			print'BandContact'
			DELETE FROM BandContact
			FROM BandContact R
			INNER JOIN BandLocation RL ON R.BandLocationID=RL.BandLocationID
			INNER JOIN #Band RR ON RL.BandID=RR.BandID			
				
			print'Contact'
			DELETE FROM Contact 
			FROM Contact C
			INNER JOIN #BandContact RC ON C.ContactID=RC.ContactID	    	
				
						
		
			----Product related tables

			print'RebateProduct'
			DELETE FROM RebateProduct
			FROM RebateProduct PA
			INNER JOIN Rebate RR ON PA.RebateID =RR.RebateID 
			--INNER JOIN RebateBand RR ON PA.RebateID=RR.RebateID
			inner join #Band R ON R.BandID=RR.BandID
			
			print'HotDealProduct'
			DELETE FROM HotDealProduct
			FROM HotDealProduct PA
			INNER JOIN ProductHotDeal P on p.ProductHotDealID=PA.ProductHotDealID
			INNER JOIN #Band R ON  R.BandID=P.BandID

			DELETE FROM UserPushNotification
			FROM UserPushNotification PA			
			INNER JOIN ProductHotDeal P on p.ProductHotDealID=PA.ProductHotDealID
			INNER JOIN #Band R ON  R.BandID=P.BandID	
			
			
			print'ProductUserHit'
			DELETE FROM ProductUserHit
			FROM ProductUserHit PA
			INNER JOIN BandLocation RL ON rl.BandLocationID=PA.BandLocationID
			INNER JOIN #Band R ON R.BandID=RL.BandID

			----Coupon related tables 

			print'CouponHistory'
			DELETE FROM CouponHistory
			FROM CouponHistory CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID	
					
			print'CouponProduct'
			DELETE FROM CouponProduct
			FROM CouponProduct CP
			INNER JOIN #Coupon CR ON CP.CouponID = CR.CouponID			
			
			print'CouponBand'
			DELETE FROM CouponBand  where CouponID IN (SELECT CouponID from #Coupon)
			--FROM CouponBand CP where couponid in (select couponid from #coupon)
			--INNER JOIN #Coupon C ON C.CouponID = CP.CouponID AND C.CouponID IS NOT NULL 			
			
			print'UserCouponGallery'
			DELETE FROM UserCouponGallery
			FROM UserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 

			DELETE FROM HcUserCouponGallery
			FROM HcUserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 
			
			print'UserPushNotification'
			DELETE FROM UserPushNotification
			FROM UserPushNotification PA			
			INNER JOIN #Coupon C ON C.CouponID = PA.CouponID	
			
			print'Coupon'
			DELETE FROM Coupon
			FROM Coupon CP
			INNER JOIN #Band C ON C.BandID = CP.BandID
			
			print'UserLoyaltyProgram'
			DELETE FROM UserLoyaltyProgram
			FROM UserLoyaltyProgram RA
			INNER JOIN LoyaltyProgram RL ON RA.LoyaltyProgramID=RL.LoyaltyProgramID
			INNER JOIN #Band R ON R.BandID=RL.BandID
			
			
			print'UserPayHistory'
			DELETE FROM UserPayHistory
			FROM UserPayHistory RA
			INNER JOIN LoyaltyProgram RL ON RA.LoyaltyProgramID=RL.LoyaltyProgramID
			INNER JOIN #Band R ON R.BandID=RL.BandID
			
			
			print'LoyaltyDeal'
			DELETE FROM LoyaltyDeal
			FROM LoyaltyDeal RA
			INNER JOIN LoyaltyProgram RL ON RA.LoyaltyProgramID=RL.LoyaltyProgramID
			INNER JOIN #Band R ON R.BandID=RL.BandID			
			
			print'LoyaltyProgram'
			DELETE FROM LoyaltyProgram
			FROM LoyaltyProgram RB
			INNER JOIN #Band R ON R.BandID=RB.BandID

			----Rebate related tables
			print'RebateBand'
			DELETE FROM RebateBand
			FROM RebateBand  RP			
			INNER JOIN #Band RR ON RP.BandID=RR.BandID
			 
			
			print'ScanHistory'
			DELETE FROM ScanHistory
			FROM ScanHistory  RP			
			INNER JOIN BandLocation RL ON RP.BandLocationID=RL.BandLocationID
			INNER JOIN #Band RT ON RT.BandID=RL.BandID
			
			print'UserRebateGallery'
			DELETE FROM UserRebateGallery
			FROM UserRebateGallery  RP		
			INNER JOIN Rebate RL ON RL.RebateID =RP.RebateID
			INNER JOIN #Band RR ON RR.BandID=RL.BandID
			
			
			print'RebateHistory'
			DELETE FROM RebateHistory
			FROM RebateHistory RH
			INNER JOIN Rebate R ON R.RebateID = RH.RebateID
			INNER JOIN #Band RR ON RR.BandID=R.BandID
			
			
			print'RebateUserHit'
			DELETE FROM RebateUserHit
			FROM RebateUserHit RH
			INNER JOIN Rebate R ON R.RebateID = RH.RebateID
			INNER JOIN #Band RR ON RR.BandID=R.BandID
			
			print'Rebate'
			DELETE FROM Rebate
			FROM Rebate R
			INNER JOIN #Band RR ON RR.BandID=R.BandID

			----ProductHotDeal related tables.
			print'ProductHotDealBandLocation'
			DELETE FROM ProductHotDealBandLocation
			FROM ProductHotDealBandLocation P
			INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 
			
			print'QRBandCustomPageAssociation'
			DELETE FROM QRBandCustomPageAssociation 
			From QRBandCustomPageAssociation QR
			INNER JOin #Band R ON R.BandID =QR.BandID 
			
			print'QRBandCustomPageMedia'
			DELETE FROM QRBandCustomPageMedia
			From QRBandCustomPageMedia QR
			INNER JOIN QRBandCustomPage QC ON QR.QRBandCustomPageID=QC.QRBandCustomPageID
			INNER JOIN #Band R ON R.BandID =QC.BandID 
			
			print'UserGiveawayAssociation'
			DELETE FROM UserGiveawayAssociation
			From UserGiveawayAssociation QR
			INNER JOIN QRBandCustomPage QC ON QR.QRBandCustomPageID=QC.QRBandCustomPageID
			INNER JOIN #Band R ON R.BandID =QC.BandID
			
			print'QRBandCustomPage'
			DELETE FROM QRBandCustomPage
			From QRBandCustomPage QC
			INNER JOin #Band R ON R.BandID =QC.BandID
			
			print'ProductKeywordsBand'
			DELETE FROM ProductKeywordsBand
			From ProductKeywordsBand PR
			INNER JOin #Band R ON R.BandID =PR.BandID
			
			--print'UserHotDealGallery'
			--DELETE FROM UserHotDealGallery
			--FROM UserHotDealGallery UH
			--INNER JOIN BandLocation RL ON UH.BandLocationID=RL.BandLocationID
			--INNER JOIN #Band RR ON RL.BandID=RR.BandID

			--DELETE FROM HcUserHotDealGallery
			--FROM HcUserHotDealGallery UH
			--INNER JOIN BandLocation RL ON UH.BandLocationID=RL.BandLocationID
			--INNER JOIN #Band RR ON RL.BandID=RR.BandID
			
			print'AffiliateBand'
			DELETE FROM AffiliateBand
			FROM AffiliateBand RL
			INNER JOIN #Band RR ON RL.BandID=RR.BandID
			
			print'GroupBand'
			DELETE FROM GroupBand
			FROM GroupBand RL
			INNER JOIN #Band RR ON RL.BandID=RR.BandID
			
			print'ProductUserHit'
			DELETE FROM ProductUserHit
			FROM ProductUserHit RL
			INNER JOIN BandLocation R ON R.BandLocationID=RL.BandLocationID
			INNER JOIN #Band RR ON R.BandID=RR.BandID
			
			print'QRUserBandSaleNotification'
			DELETE FROM QRUserBandSaleNotification
			FROM QRUserBandSaleNotification RL
			INNER JOIN #Band RR ON RL.BandID=RR.BandID			
			
			print'UserBandPreference'
			DELETE FROM UserBandPreference
			WHERE BandID IN (SELECT BandID FROM #Band)
				
			print'UserHotDealGallery'
			DELETE FROM UserHotDealGallery
			WHERE HotDealID IN (SELECT ProducthotdealID FROM #HotDeal)	

			DELETE FROM HcUserHotDealGallery
			WHERE HotDealID IN (SELECT ProducthotdealID FROM #HotDeal)
			
			----Delete Data from Hubciti related Tables

			-- Delte from Appsite related data from Hubciti
			SELECT DISTINCT RL.BandLocationID 
			INTO #BandLocation
			FROM #Band R
			INNER JOIN BandLocation RL ON RL.BandID =R.BandID AND R.BandID IS NOT NULL


			SELECT DISTINCT HcAppSiteID 
			INTO #Appsites
			FROM HcAppSite A
			INNER JOIN #BandLocation R ON R.BandLocationID =A.BandLocationID      						
			
			SELECT E.HcEventID 
			INTO #Events 
			FROM HcEventAppsite E
			INNER JOIN #Appsites A ON A.HcAppSiteID =E.HcAppsiteID

			SELECT DISTINCT R1.HcEventID
			INTO #Events1
			FROM HcEvents R1
			INNER JOIN #Band R ON R1.BandID = R.BandID
			

			DELETE FROM HcBandEventsAssociation
			FROM HcBandEventsAssociation HR
			INNER JOIN #BandLocation RL ON HR.BandLocationID = RL.BandLocationID

			DELETE FROM HcEventAppsite 
			FROM HcEventAppsite E
			INNER JOIN #Appsites A ON A.HcAppSiteID =E.HcAppsiteID			

			DELETE FROM HcEventInterval 
			FROM HcEventInterval I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			

			DELETE FROM HcEventLocation
			FROM HcEventLocation I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			

			DELETE FROM HcEventPackage 
			FROM HcEventPackage I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			

			DELETE FROM HcEventsCategoryAssociation  
			FROM HcEventsCategoryAssociation I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			
			DELETE FROM HcFundraisingEventsAssociation  
			FROM HcFundraisingEventsAssociation I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID

			--DELETE FROM HcAppSite  
			--FROM HcAppSite C
			--INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			DELETE FROM HcEvents
			FROM HcEvents E
			INNER JOIN #Band R on E.BandID = R.BandID


			--Delete from HcCityExperienceBandLocation		

			DELETE FROM HcEventPackage 
			FROM HcEventPackage C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			DELETE FROM HcCityExperienceBandLocation
			FROM HcCityExperienceBandLocation C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

            
			DELETE FROM HcFilterBandLocation 
			FROM HcFilterBandLocation C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			DELETE FROM HCProductUserHit  
			FROM HCProductUserHit C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			DELETE FROM HcBandAssociation   
			FROM HcBandAssociation C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			DELETE FROM HcBandSubCategory    
			FROM HcBandSubCategory C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			--DELETE FROM HcUserCouponGallery    
			--FROM HcUserCouponGallery C
			--INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID
			

			--print 'HcFundraisingAppsiteAssociation'
			--DELETE FROM HcFundraisingAppsiteAssociation
			--FROM HcFundraisingAppsiteAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.BandID = @RetailID

			--print 'HcFundraisingCategoryAssociation'
			--DELETE FROM HcFundraisingCategoryAssociation
			--FROM HcFundraisingCategoryAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.BandID = @RetailID

			--print 'HcFundraisingBottomButtonAssociation'
			--DELETE FROM HcFundraisingBottomButtonAssociation
			--FROM HcFundraisingBottomButtonAssociation A
			--INNER JOIN HcFundraisingDepartments D ON A.HcFundraisingDepartmentID = D.HcFundraisingDepartmentID
			--WHERE D.BandID = @RetailID

			--print 'HcFundraisingDepartments'
			--DELETE FROM HcFundraisingDepartments WHERE BandID = @RetailID

			--print 'HcFundraisingEventsAssociation'
			--DELETE FROM HcFundraisingEventsAssociation
			--FROM HcFundraisingEventsAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.BandID = @RetailID

			--print 'HcBandFundraisingAssociation'
			--DELETE FROM HcBandFundraisingAssociation
			--FROM HcBandFundraisingAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.BandID = @RetailID

			--print 'HcFundraising'
			--DELETE FROM HcFundraising WHERE BandID = @RetailID
			
			print 'HcFundraisingAppsiteAssociation'
			DELETE FROM HcFundraisingAppsiteAssociation
			FROM HcFundraisingAppsiteAssociation FA 
			INNER JOIN HcAppSite A ON FA.HcAppsiteID = A.HcAppsiteID
			INNER JOIN BandLocation RL ON A.BandLocationID = RL.BandLocationID
			INNER JOIN #Band R ON RL.BandID = R.BandID

			print 'HcFundraisingAppsiteAssociation'
			DELETE FROM HcFundraisingAppsiteAssociation
			FROM HcFundraisingAppsiteAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Band R ON F.BandID = R.BandID
			
			print 'HcAppSite'
			DELETE FROM HcAppSite  
			FROM HcAppSite C
			INNER JOIN #BandLocation R ON R.BandLocationID=C.BandLocationID

			print 'HcFundraisingCategoryAssociation'
			DELETE FROM HcFundraisingCategoryAssociation
			FROM HcFundraisingCategoryAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Band R ON F.BandID = R.BandID

			print 'HcFundraisingBottomButtonAssociation'
			DELETE FROM HcFundraisingBottomButtonAssociation
			FROM HcFundraisingBottomButtonAssociation A
			INNER JOIN HcFundraisingDepartments D ON A.HcFundraisingDepartmentID = D.HcFundraisingDepartmentID
			INNER JOIN #Band R ON D.BandID = R.BandID

			print 'HcFundraisingDepartments'
			DELETE FROM HcFundraisingDepartments 
			FROM HcFundraisingDepartments F 
			INNER JOIN #Band R ON F.BandID = R.BandID

			print 'HcFundraisingEventsAssociation'
			DELETE FROM HcFundraisingEventsAssociation
			FROM HcFundraisingEventsAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Band R ON F.BandID = R.BandID
			
			print 'HcBandFundraisingAssociation'
			DELETE FROM HcBandFundraisingAssociation
			FROM HcBandFundraisingAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Band R ON F.BandID = R.BandID

			print 'HcFundraising'
			DELETE FROM HcFundraising 
			FROM HcFundraising F
			INNER JOIN #Band R ON F.BandID = R.BandID			
			
			print'BandLocation'
			DELETE FROM BandLocation
			FROM BandLocation RL
			INNER JOIN #Band RR ON RL.BandID=RR.BandID
			
			print'ProductHotDeal'
			DELETE FROM ProductHotDeal
			FROM ProductHotDeal RL
			INNER JOIN #Band RR ON RL.BandID=RR.BandID			

            SELECT UserID 
            INTO #UserID
            FROM UserBand 
			WHERE BandID IN (SELECT BandID FROM #Band)
			
			print'UserBand'
			DELETE FROM UserBand 
			WHERE BandID IN (SELECT BandID FROM #Band)

			print'BandFilterAssociation'
			DELETE FROM BandFilterAssociation
			WHERE BandID IN (SELECT BandID FROM #Band)

			--print'BandCategory_BAK'
			--DELETE FROM BandCategory_BAK   
			--WHERE BandID IN (SELECT BandID FROM #Band)
			
			print'BandBusinessCategory'
            DELETE FROM BandBusinessCategory 
			WHERE BandID IN (SELECT BandID FROM #Band)
			
			
			print'Band'
			DELETE FROM Band 
			WHERE BandID IN (SELECT BandID FROM #Band)
			
			print'Users'
			DELETE FROM Users 
			FROM Users U
			INNER JOIN #UserID US ON US.UserID =U.UserID */


			-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
			
		--Confirmation of Success.  
		SELECT @Status = 0
		COMMIT TRANSACTION
		
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandAdminDeleteBand.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
