USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerProductDetails
Purpose					: 
Example					: usp_WebRetailerProductDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		12th January 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerProductDetails]
(
	@RetailID int	
	,@ProductID Varchar(1000)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration' 
		
		
		
			SELECT DISTINCT P.ProductID
				   ,ProductName
				   ,ScanCode
				   ,ProductShortDescription shortDescription
				   ,productImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+  ProductImagePath
				   						ELSE ProductImagePath END				  
				   ,ISNULL(RP.Price ,P.SuggestedRetailPrice ) price
			FROM Product P
			INNER JOIN RetailLocationProduct RP ON P.ProductID=RP.ProductID			
			INNER JOIN RetailLocation R ON R.RetailLocationID=RP.RetailLocationID AND R.Active = 1
			INNER JOIN fn_SplitParam(@ProductID,',') PM ON RP.ProductID =Pm.Param 
			WHERE P.ProductID <> 0
			AND RetailID=@RetailID  	
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerProductDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
