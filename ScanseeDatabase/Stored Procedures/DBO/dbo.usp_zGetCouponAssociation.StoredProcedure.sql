USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_zGetCouponAssociation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_APIWishPondInputCity
Purpose					: To Pass city as the input for APIWishPondURL.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th June 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_zGetCouponAssociation]
(
	
	  @CouponName varchar(1000)
	--Output Variable 

	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			SELECT C.CouponID
				 , C.CouponName
				 , C.CouponStartDate
				 , C.CouponExpireDate
				 , C.CouponDateAdded
				 , P.ProductID
				 , P.ScanCode
				 , P.ProductName
			FROM Coupon C
			INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID
			INNER JOIN Product P ON P.ProductID = CP.ProductID
			WHERE C.CouponName LIKE '%'+@CouponName+'%'			
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_APIWishPondInputCity.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
		 
	END CATCH;
END;


GO
