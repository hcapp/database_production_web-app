USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetScreenContent]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GetScreenContent]
Purpose					: To get Image of Product Summary page.
Example					: [usp_GetScreenContent]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Oct 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GetScreenContent]
(
	@ConfigurationType varchar(50)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			DECLARE @Config varchar(50)
			DECLARE @WebConfig Varchar(200)
			
			SELECT @WebConfig = ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType = 'FaceBook'
			AND ScreenName = 'Base_URL'
			
			
			--To get Email Template URL.
			IF @ConfigurationType = 'ShareURL'
			BEGIN
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='Configuration of server'
			END
			--For all the other URL framing.
			ELSE
			BEGIN
			SELECT @Config=ScreenContent
			FROM AppConfiguration 
			WHERE ConfigurationType='App Media Server Configuration'
			END

      

			SELECT  ConfigurationType
					,ScreenContent	= CASE WHEN ScreenName Like '%image%' THEN  @Config+ScreenContent 
										  WHEN ConfigurationType='ShareURL' THEN @Config+ScreenContent 
										  WHEN ConfigurationType = 'WebShareURL' THEN @WebConfig + ScreenContent 
									  Else ScreenContent END														
					,ScreenName 
					,Active 
					,DateCreated
					,DateModified
					,ScreenValue=CASE WHEN ScreenName LIKE 'Push Notification Sale' THEN 1
					                 WHEN ScreenName LIKE 'Push Notification Hotdeal' THEN 2
					                 WHEN ScreenName LIKE 'Push Notification Coupons' THEN 4
					                 WHEN ScreenName LIKE 'Push Notification Hotdeal And Sale' THEN 3
					                 WHEN ScreenName LIKE 'Push Notification Coupon And Sale' THEN 5
					                 WHEN ScreenName LIKE 'Push Notification Hotdeal And Coupons' THEN 6
					                 WHEN ScreenName LIKE 'Push Notification Hotdeals,Coupons And Sale' THEN 7 END
					
					 
			FROM  AppConfiguration
			WHERE ConfigurationType=@ConfigurationType
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GetScreenContent].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
