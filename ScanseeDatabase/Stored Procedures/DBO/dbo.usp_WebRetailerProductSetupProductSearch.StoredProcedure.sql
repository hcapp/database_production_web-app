USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProductSetupProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
Stored Procedure name : usp_WebRetailerProductSetupProductSearch      
Purpose     : To display the Products available.      
Example     : usp_WebRetailerProductSetupProductSearch      
      
History      
Version  Date       Author   Change Description      
-------------------------------------------------------------------------------       
1.0   1st January 2012    Pavan Sharma K Initial Version      
-------------------------------------------------------------------------------      
*/      
      
CREATE PROCEDURE [dbo].[usp_WebRetailerProductSetupProductSearch]      
(      
      
 --Input Parameter(s)--      
        
   @SearchParameter varchar(100)      
 , @LowerLimit int  
 , @RecordCount int    
       
 --Output Variable--       
       
 , @NextPageFlag bit output        
 , @RowCount int output      
 , @ErrorNumber int output      
 , @ErrorMessage varchar(1000) output       
)      
AS      
BEGIN      
      
 BEGIN TRY           
     
 --To get Media Server Configuration.    
  DECLARE @Config varchar(50)      
  SELECT @Config=ScreenContent      
  FROM AppConfiguration       
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'     
         
    DECLARE @UpperLimit INT      
    DECLARE @RowCnt INT      
         
   --To get the row count for pagination.    
  --DECLARE @ScreenContent Varchar(100)  
  --SELECT @ScreenContent = ScreenContent     
  --FROM AppConfiguration     
  --WHERE ScreenName = 'All'   
  --AND ConfigurationType = 'Website Pagination'  
  --AND Active = 1   
    
   SET @UpperLimit = @LowerLimit + @RecordCount    
   
   --Set the search string to null if empty string is passed.
   IF @SearchParameter LIKE ''
   BEGIN
	SET @SearchParameter = NULL
   END     
   
   CREATE TABLE #Temp1(RowNumber INT IDENTITY(1, 1)
				     , ProductID INT    
				     , ProductName VARCHAR(500)    
				     , ProductImagePath VARCHAR(1000)      
				     , ProductExpirationDate DATETIME     
				     , ManufacturerID INT
				     , ModelNumber VARCHAR(50)    
				     , ProductLongDescription VARCHAR(MAX)
				     , productShortDescription VARCHAR(500)
				     , ScanCode VARCHAR(20)
				     , price MONEY
				     , WarrantyServiceInformation VARCHAR(500)) 
   
   --Search based on the search string if the input is passed.      
   IF @SearchParameter IS NOT NULL
   BEGIN     
		  INSERT INTO #Temp1(ProductID
						   , ProductName
						   , ProductImagePath
						   , ProductExpirationDate
						   , ManufacturerID
						   , ModelNumber
						   , ProductLongDescription
						   , productShortDescription
						   , ScanCode
						   , price
						   , WarrantyServiceInformation)     
		             
		  SELECT P.ProductID      
			   , P.ProductName      
			   , ProductImagePath = CASE WHEN WebsiteSourceFlag = 1 AND ProductImagePath IS NOT NULL THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+ ProductImagePath ELSE ProductImagePath END     
			   , P.ProductExpirationDate      
			   , P.ManufacturerID      
			   , P.ModelNumber      
			   , P.ProductLongDescription      
			   , P.ProductShortDescription      
			   , P.ScanCode      
			   , P.SuggestedRetailPrice      
			   , P.WarrantyServiceInformation  	             
		  FROM Product P       
		  WHERE P.ProductID > 0 
		  AND (P.ProductName LIKE '%'+@SearchParameter+'%'       
		  OR P.ScanCode LIKE @SearchParameter+'%')
		  AND ISNULL(P.ProductExpirationDate,GETDATE()+1)>=GETDATE()
		  ORDER BY P.ProductName ASC
		    
	 END
	 
	 --If the search string is empty then select top 100 rows.
	 ELSE
	 BEGIN
		  
		  INSERT INTO #Temp1(ProductID
						   , ProductName
						   , ProductImagePath
						   , ProductExpirationDate
						   , ManufacturerID
						   , ModelNumber
						   , ProductLongDescription
						   , productShortDescription
						   , ScanCode
						   , price
						   , WarrantyServiceInformation) 
						   
		  SELECT TOP 1000 P.ProductID      
			   , P.ProductName      
			   , ProductImagePath = CASE WHEN WebsiteSourceFlag = 1 AND ProductImagePath IS NOT NULL THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+ ProductImagePath ELSE ProductImagePath END     
			   , P.ProductExpirationDate      
			   , P.ManufacturerID      
			   , P.ModelNumber      
			   , P.ProductLongDescription      
			   , P.ProductShortDescription      
			   , P.ScanCode      
			   , P.SuggestedRetailPrice      
			   , P.WarrantyServiceInformation     
		             
		  FROM Product P       
		  WHERE P.ProductID <> 0 
		  AND (P.ProductName LIKE  '%'    
		  OR P.ScanCode LIKE '%')
		  AND ISNULL(P.ProductExpirationDate,GETDATE()+1)>=GETDATE()
		  --ORDER BY P.ProductName ASC
		  
	 END
      
        
  --Get the Total Records in the Result Set.      
  SELECT @RowCnt = MAX(RowNumber) FROM #Temp1      
        
  SET @RowCount = @RowCnt      
        
  --Chek if there are still more records.      
  SELECT @NextPageFlag = (CASE WHEN (@RowCount - @UpperLimit )>0 THEN 1 ELSE 0 END)      
        
  --Display the records within the specified lower & upper limits.      
  SELECT RowNumber       
    , ProductID      
       , ProductName      
       , ProductImagePath      
       , ProductExpirationDate      
       , ManufacturerID      
       , ModelNumber      
       , ProductLongDescription      
       , productShortDescription      
       , ScanCode      
       , price       
       , WarrantyServiceInformation      
        FROM #Temp1 WHERE RowNumber BETWEEN (@LowerLimit+1) AND @UpperLimit          
                     
          
 END TRY      
        
 BEGIN CATCH      
       
  --Check whether the Transaction is uncommitable.      
  IF @@ERROR <> 0      
  BEGIN      
   PRINT 'Error occured in Stored Procedure usp_WebRetailerProductSetupProductSearch.'        
   --- Execute retrieval of Error info.      
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output          
  END;      
         
 END CATCH;      
END;




GO
