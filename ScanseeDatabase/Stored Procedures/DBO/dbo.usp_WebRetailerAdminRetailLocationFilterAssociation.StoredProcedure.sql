USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetailLocationFilterAssociation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: [usp_WebRetailerAdminRetailLocationFilterAssociation]
Purpose					: To associate Filters to RetailLocations.
Example					: [usp_WebRetailerAdminRetailLocationFilterAssociation]

History
Version		 Date				Author			Change Description
------------------------------------------------------------------------------- 
1.0		  5th AUG 2016		Shilpashree B H		  1.0
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetailLocationFilterAssociation]

	--Input parameters	 
	  @RetailID INT
	, @RetailLocationID INT	
	, @UserID int
	, @BusinessCategoryIDs VARCHAR(1000)
	, @AdminFilterID VARCHAR(MAX)
	, @AdminFilterValueID VARCHAR(MAX)

	--Output variables 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY  
		BEGIN TRANSACTION	
			
		--To remove esisting filter association
		DELETE FROM RetailerFilterAssociation WHERE RetailID = @RetailID AND RetailLocationID = @RetailLocationID

		IF (@BusinessCategoryIDs IS NOT NULL)
		BEGIN
			--To fetch Retailer filter Categories
			DECLARE	@FilterBusinessCategory varchar(1000) 
					
			SELECT Param
			INTO #FilterCategory
			FROM fn_SplitParam(@BusinessCategoryIDs,',')
			--FROM BusinessCategory BC
			--INNER JOIN fn_SplitParam(@BusinessCategoryIDs,',') P ON BC.BusinessCategoryID = P.Param
			--WHERE BusinessCategoryName in ('Bars','Dining') AND @AdminFilterID IS NOT NULL

			--Retailer filter updation
			IF (SELECT DISTINCT 1 FROM #FilterCategory) > 0
			BEGIN
			SELECT @FilterBusinessCategory = COALESCE(@FilterBusinessCategory + '!~~!' , '') + CAST(Param AS varchar(100))
			FROM  #FilterCategory

			SELECT @AdminFilterID = REPLACE(@AdminFilterID, 'NULL', '0')  
			SELECT @AdminFilterValueID = REPLACE(@AdminFilterValueID, 'NULL', '0')
		
				--Capture Category and filter ID's
				SELECT RowNum = IDENTITY(INT, 1, 1)
						,CategoryID = Param
				INTO #CategoryID
				FROM dbo.fn_SplitParamMultiDelimiter(@FilterBusinessCategory, '!~~!')                     

				SELECT RowNum = IDENTITY(INT, 1, 1)
						, FilterId = Param
				INTO #FilterID
				FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterID, '!~~!')  
					
				 SELECT RowNum = IDENTITY(INT, 1, 1)
						, FilterValueId = Param
				INTO #FilterValueID
				FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterValueID, '!~~!')  
	
				SELECT RowNum = IDENTITY(INT, 1, 1)
						,C.CategoryID
						,FilterID =  F.Param 
				INTO #cat_filterValues 
				FROM #CategoryID C
				INNER JOIN #FilterID S ON C.RowNum = S.RowNum 
				CROSS APPLY (select Param from fn_SplitParam(S.FilterId,'|') ) F
				
				SELECT CategoryID
					  ,	FilterID
					  , FilterValueId
				INTO #Final_filtervalues
				FROM #cat_filterValues V
				INNER JOIN #FilterValueID I on V.RowNum = I.RowNum
		
				--Insert into Retailer Filter Association table.

				IF CURSOR_STATUS('global','CategoryList')>=-1
				BEGIN
				DEALLOCATE CategoryList
				END
                                                
				DECLARE @CategoryID1 int
				DECLARE @FilterID1 int
				DECLARE @FilterValueID1 varchar(MAX)
				DECLARE CategoryList CURSOR STATIC FOR
				SELECT CategoryID
					  ,FilterId
					  ,FilterValueId          
				FROM #Final_filtervalues 
                      
				OPEN CategoryList
                           
				FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                           
				CREATE TABLE #TempBiz(BusinnessCategoryID INT)

				WHILE @@FETCH_STATUS = 0
				BEGIN   
                            
						INSERT INTO #TempBiz(BusinnessCategoryID) 			                                
						OUTPUT inserted.BusinnessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT F.[Param]						     
						FROM dbo.fn_SplitParam(@CategoryID1, ',') F


						INSERT INTO RetailerFilterAssociation(RetailID
												  ,AdminFilterID
												  ,BusinessCategoryID
												  ,AdminFilterValueID
												  ,DateCreated
												  ,CreatedUserID
												  ,RetailLocationID)
									OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT @RetailID
											,IIF(@FilterID1 = 0,NULL,@FilterID1)
											,@CategoryID1
											,IIF([Param] = 0,NULL,[Param])
											,GETDATE()
											,@UserID
											,@RetailLocationID
									FROM dbo.fn_SplitParam(@FilterValueID1, '|')
					
									DECLARE @Cnt INT 
									SELECT @Cnt = 0
									SELECT @Cnt = COUNT(Param)
									FROM dbo.fn_SplitParam(@FilterValueID1, '|')
                                   
                               
						TRUNCATE TABLE #TempBiz
		
						FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                                  
				END                        

				CLOSE CategoryList
				DEALLOCATE CategoryList
		   END

		END
		ELSE
		BEGIN
			Print 'All filters are unassociated'
		END
		-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'

			SELECT @FindClearCacheURL= @CurrentURL+Screencontent--+','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

		------------------------------------------------					 
							 
		 
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerAdminRetailLocationFilterAssociation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
