USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerPayUnpay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerPayUnpay
Purpose					: To insert/update pay option for respective retailers
Example					: usp_WebRetailerPayUnpay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30 Apr 2015		Mohith H R	 	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerPayUnpay]
(
		 @RetailerID int
		,@UserID int	
		,@IsPaid bit	
			
		--Output Variable 
		, @Status int output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION 

		IF EXISTS(SELECT 1 FROM RetailerBillingDetails WHERE RetailerID = @RetailerID )
		BEGIN


			UPDATE RetailerBillingDetails 
			SET IsPaid = @IsPaid 
			WHERE RetailerID = @RetailerID


		END
		ELSE IF EXISTS(SELECT 1 FROM Retailer WHERE Retailid = @RetailerID AND RetailerActive = 1)
		BEGIN				
		--INSERT INT0 THE RetailerBillingDetails TABLE.




				INSERT INTO RetailerBillingDetails(
													  UserID
													, RetailerID
													, RetailerBillingPlanID
													, Quantity												
													, RetailerProductFeeFlag		
													, IsPaid
													, DateCreated																							 
											  )
										
					                         SELECT @UserID
					                                , @RetailerID
					                                , 1
					                                , 1
					                                , 1
													, @IsPaid
													, GETDATE()
																	
		
		END

		UPDATE RetailLocation
		SET claimFlag=@IsPaid
		WHERE RetailId = @RetailerID

		--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerPayUnpay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
