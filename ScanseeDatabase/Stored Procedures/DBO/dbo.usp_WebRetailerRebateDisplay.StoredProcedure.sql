USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerRebateDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebRetailerRebateDisplay  
Purpose     : To Display coupon details of a retailer  
Example     : usp_WebRetailerRebateDisplay  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   28th Dec 2011 Pavan Sharma K Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebRetailerRebateDisplay]  
(  
  @RetailID int  
 ,@RebateSearch Varchar(100)  
 --Output Variable   
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
  SELECT RebateID  
      ,RebateName  
      ,RetailID  
      ,RebateAmount rebateAmt  
      ,RebateShortDescription rebateDescription  
      ,RebateLongDescription rebateDescription  
      ,CONVERT(VARCHAR(100), RebateStartDate, 110) + ' ' + CONVERT(VARCHAR(100), RebateStartDate, 108)RebateStartDate  
      ,CONVERT(VARCHAR(100), RebateEndDate, 110) + ' ' + CONVERT(VARCHAR(100), RebateEndDate, 108) RebateEndDate   
      ,RebateTermsConditions rebateTermsAndCondt  
      ,RebateTimeZoneID  
       
  FROM Rebate R  
  --INNER JOIN TimeZone T ON R.RebateTimeZoneID = T.TimeZoneID  
  WHERE RetailID=@RetailID AND RebateName LIKE (CASE WHEN ISNULL(@RebateSearch,'')='' THEN '%' ELSE '%'+@RebateSearch+'%' END)  
    AND RebateEndDate>GETDATE()  
      
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebRetailerRebateDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
     
  END;  
     
 END CATCH;  
END;




GO
