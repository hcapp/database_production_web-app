USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerRetrieveCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerRetrieveCategory
Purpose					: To retrieve list of Categories from the system.
Example					: usp_WebConsumerRetrieveCategory

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			14thJune				Dhananjaya TR	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerRetrieveCategory]

--Output variables
	@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		SELECT categoryID
			 , CategoryName
		FROM (
		
		
		SELECT 0 categoryID, 'All Categories' CategoryName
		
		UNION ALL
		
		SELECT CategoryID categoryID
			 , CategoryName = ParentCategoryName + ' - ' + SubCategoryName 
		FROM Category) Categories
		ORDER BY CASE WHEN CategoryName = 'All Categories' THEN '1' ELSE CategoryName END ASC
			
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerRetrieveCategory.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
