USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetrieveRetailerBusinessCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetrieveRetailerBusinessCategory
Purpose					: To retrieve list of Retailer's business categories.
Example					: usp_WebRetailerAdminRetrieveRetailerBusinessCategory

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/03/2014				SPAN		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetrieveRetailerBusinessCategory]

	--Input parameters	 
	  @RetailID INT	

	--Output variables 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
	
		
		SELECT DISTINCT A.BusinessCategoryID categoryId
			 , BusinessCategoryName categoryName
			 , Associated = CASE WHEN A.BusinessCategoryID = B.BusinessCategoryID THEN 1 ELSE 0 END
			 , filterAssociated = CASE WHEN F.AdminFilterID IS NOT NULL THEN 1 ELSE 0 END
			 , subCatAssociated = CASE WHEN B.BusinessSubCategoryID IS NOT NULL THEN 1 ELSE 0 END
		FROM BusinessCategory A		 
		LEFT JOIN RetailerBusinessCategory B ON A.BusinessCategoryID = B.BusinessCategoryID AND B.RetailerID = @RetailID
		LEFT JOIN RetailerFilterAssociation F ON A.BusinessCategoryID = F.BusinessCategoryID AND F.RetailID = @RetailID
		ORDER BY BusinessCategoryName
		
		--Confirmation of failure.
		SELECT @Status = 0

	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetrieveRetailerBusinessCategory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
