USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCheckRetailerPage]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCheckRetailerPage
Purpose					: To check for the existance of the Retailer Page(Main Menu Page) for the given RetailLocation. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			5th June 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCheckRetailerPage]
(
	
	  @RetailID int
    , @RetailLocationID varchar(max)	
	, @InputPageID int
	, @QRTypeID INT
	--Output Variable 	
	
	, @PageExists int output
	, @PageID int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
 
	BEGIN TRY
	
	   SELECT @PageID = QR.QRRetailerCustomPageID
		          FROM QRRetailerCustomPage QR 
				  INNER JOIN QRRetailerCustomPageAssociation QRRPA ON QR.QRRetailerCustomPageID = QRRPA.QRRetailerCustomPageID
				  INNER JOIN QRTypes Q ON Q.QRTypeID = QR.QRTypeID
				  INNER JOIN dbo.fn_SplitParam(@RetailLocationID,',') RL On RL.Param =QRRPA.RetailLocationID
				  WHERE Q.QRTypeID = @QRTypeID
				  AND QR.RetailID = @RetailID
				  
				  
		--Check for the existance of the Main Menu Page		  
		IF @InputPageID IS NOT NULL AND @PageID IS NOT NULL
		BEGIN
			IF @InputPageID = @PageID
			BEGIN
				SET @PageExists = 0
			END
			ELSE
			BEGIN
				SET @PageExists = 1
			END
		END	
		ELSE IF(@PageID IS NOT NULL)
		BEGIN
			SET @PageExists = 1			
		END
		ELSE
		BEGIN
			SET @PageExists = 0
		END
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCheckRetailerPage.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'		
		END;
		 
	END CATCH;
END;

--SELECT * FROM UserRetailer U INNER JOIN Users US ON U.UserID = US.UserID




GO
