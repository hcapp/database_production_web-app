USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandBannerAdExpiration]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Stored Procedure name	: usp_WebBandBannerAdExpiration

Purpose					: To Expire a Banner Ad.

Example					: usp_WebBandBannerAdExpiration



History

Version		Date			Author			Change Description

--------------------------------------------------------------- 

1.0			13th Aug 2012	Prakash C	Initial Version(usp_WebRetailerBannerAdExpiration)

---------------------------------------------------------------

*/



CREATE PROCEDURE [dbo].[usp_WebBandBannerAdExpiration]

(

	--Input Variables

	  @RetailID int

	, @BannerAdID int

	

	--Output Variable 

	

	, @Status int output

	, @ErrorNumber int output

	, @ErrorMessage varchar(1000) output 

)

AS

BEGIN



	BEGIN TRY

		BEGIN TRANSACTION			

			 

			 DECLARE @ADStartDate date

			 DECLARE @ADEndDate date

			 

			 SELECT @ADStartDate = StartDate

				  , @ADEndDate = EndDate

			 FROM bandAdvertisementBanner

			 WHERE bandAdvertisementBannerID = @BannerAdID

			 

			 --Check if the Ad is active and move the record to the history and then update the date to '01/01/1900'

			 IF (CONVERT(DATE,GETDATE()) >= @ADStartDate AND CONVERT(DATE,GETDATE()) <= ISNULL(@ADEndDate, DATEADD(DD,1,GETDATE())))

			 BEGIN

				INSERT INTO bandAdvertisementBannerHistory(AdvertisementBannerID

													 , BannerAdName

													 , StartDate

													 , EndDate

													 , DateCreated)

											SELECT bandAdvertisementBannerID

												 , BannerAdName

												 , StartDate

												 , EndDate

												 , GETDATE()

											FROM bandAdvertisementBanner

											WHERE bandAdvertisementBannerID = @BannerAdID

				

				--Update the date to the starting date in SQL Server.

				UPDATE bandAdvertisementBanner

				SET StartDate  = '01/01/1900'

				  , EndDate = '01/01/1900'

				WHERE bandAdvertisementBannerID = @BannerAdID

			 END

			

		   --Confirmation of Success.

			SELECT @Status = 0

		

		COMMIT TRANSACTION

	END TRY

		

	BEGIN CATCH

	

		--Check whether the Transaction is uncommitable.

		IF @@ERROR <> 0

		BEGIN

			PRINT 'Error occured in Stored Procedure usp_WebBandBannerAdExpiration.'		

			--- Execute retrieval of Error info.

			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 

			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'

			ROLLBACK TRANSACTION;

			insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)

               values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())

			--Confirmation of failure.

			SELECT @Status = 1

		END;

		 

	END CATCH;

END;





GO
