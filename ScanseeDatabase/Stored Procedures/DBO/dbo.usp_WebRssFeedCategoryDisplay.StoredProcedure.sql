USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedCategoryDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRssFeedCategoryDisplay
Purpose					: 
Example					: usp_WebRssFeedCategoryDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13 Feb 2015		Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedCategoryDisplay]
(	
	
	  @HcHubCitiID int

	--Output Variable 
	, @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

	set transaction   isolation level read uncommitted

		  DECLARE @RockwallDefaultImage varchar(2000)

		  SELECT @RockwallDefaultImage = ScreenContent
		  FROM AppConfiguration
		  WHERE ConfigurationType = 'RssFeedRockwallDefaultImage'

		  SELECT CategoryName
			    ,IIF(ImagePath IS NULL OR ImagePath LIKE '',@RockwallDefaultImage,ImagePath) ImagePath
				,DateCreated
		  FROM RssFeedCategory
		  WHERE HcHubCitiID = @HcHubCitiID
								   
		  SET @Status=0						   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRssFeedCategoryDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;




GO
