USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_TodayListAddDelete]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_TodayListAddDelete
Purpose					: To add/remove Product from today shopping list
Example					: usp_TodayListAddDelete 14, '125'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_TodayListAddDelete]
(
	 @UserID int
	, @UserProductID varchar(max)
	
	--Output Variable 
	--, @ProductExists bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Today datetime
		SET @Today = GETDATE()
		SELECT up.UserProductID 
			, UP.UserID 
			, UP.UserRetailPreferenceID RetailID
			, UP.ProductID 
			, TodayListtItem = CASE WHEN TodayListtItem = 1 THEN 0
								  WHEN TodayListtItem = 0 THEN 1
							 END
			, AddDate = CASE WHEN TodayListtItem = 1 THEN TodayListAddDate ELSE @Today END
			, DeleteDate = CASE WHEN TodayListtItem = 1 THEN @Today ELSE TodayListRemoveDate END
		INTO #Update
		FROM UserProduct UP
			INNER JOIN fn_SplitParam(@UserProductID, ',') P ON P.Param = UP.UserProductID 
		WHERE UserID = @UserID 
		
		--SELECT UserProductID
		--	   ,TodayListtItem
		--INTO #prod
		--FROM UserProduct
		--INNER JOIN fn_SplitParam(@UserProductID, ',') P ON P.Param = UP.UserProductID 
		--WHERE UserID = @UserID 
		
		--SELECT @ProductExists=CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
		--FROM #prod
		--WHERE TodayListtItem=1
		
		BEGIN TRANSACTION
			--To Add / Delete Shopping Cart
			UPDATE UserProduct
			SET TodayListtItem = U.TodayListtItem 
				, TodayListAddDate = U.AddDate 
				, TodayListRemoveDate = U.DeleteDate
				
			FROM UserProduct UP
				INNER JOIN #Update U ON U.UserProductID = UP.UserProductID 
			WHERE UP.UserID = @UserID
			
		--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_TodayListAddDelete.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
