USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminAPPVersionCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: [usp_WebAdminAPPVersionCreation]  
Purpose					: Create Appversion details.  
Example					: [usp_WebAdminAPPVersionCreation]  
  
History  
Version  Date				Author			 Change	Description  
---------------------------------------------------------------   
1.0		 5th Sep 2013   	Dhananjaya TR	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebAdminAPPVersionCreation]  
(  
   @VersionNumber Varchar(100) 
 , @AppType Varchar(255)  
 , @DatabaseSchemaName Varchar(200)
 , @ServerBuild Varchar(500)
 , @QAFirstReleaseDate DateTime 
 , @QALastReleaseDate DateTime
 , @QAReleaseNotePath Varchar(500) 
 , @ProductionFirstReleaseDate DateTime   
 , @ProductionLastReleaseDate DateTime 
 , @ProductionReleaseNotePath Varchar(500) 
 , @ITunesUploadDate Datetime   
 , @BuildApprovalDate Datetime
 , @QAReleaseNote Varchar(2000)
 , @ProductionReleaseNote Varchar(2000)
   
 --OutPut Variable   
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
  BEGIN TRY  
 
	BEGIN TRANSACTION
 
    -- Insert new app version details
    INSERT INTO APPVersionManagement(VersionNumber
									,DatabaseSchemaName 
									,ServerBuild 
									,QAFirstReleaseDate
									,QALastReleaseDate
									,QAReleaseNotePath
									,ProductionFirstReleaseDate
									,ProductionLastReleaseDate
									,ProductionReleaseNotePath
									,ITunesUploadDate
									,BuildApprovalDate
									,ApplicationTypeID
									,QAReleaseNoteFileName  
									,ProductionReleaseNoteFileName )
     SELECT  @VersionNumber  
			 , @DatabaseSchemaName 
			 , @ServerBuild  
			 , @QAFirstReleaseDate 
			 , ISNULL(@QALastReleaseDate,@QAFirstReleaseDate) 
			 , @QAReleaseNotePath  
			 , @ProductionFirstReleaseDate    
			 , ISNULL(@ProductionLastReleaseDate,@ProductionFirstReleaseDate) 
			 , @ProductionReleaseNotePath  
			 , @ITunesUploadDate    
			 , @BuildApprovalDate 
			 , ApplicationTypeID 
			 , @QAReleaseNote 
			 , @ProductionReleaseNote 
	 FROM ApplicationType 
	 WHERE [Type]=@AppType 
	 	
	--Confirmation of Success.
	SELECT @Status = 0
   COMMIT TRANSACTION
 END TRY  
    
 BEGIN CATCH 
 
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebAdminAPPVersionCreation].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
    --Confirmation of failure.
   SELECT @Status = 1 
    
   ROLLBACK TRANSACTION;  
  END;  
     
 END CATCH;  
END;


GO
