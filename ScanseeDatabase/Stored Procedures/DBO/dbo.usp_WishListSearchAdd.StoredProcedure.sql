USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListSearchAdd]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListSearchAdd
Purpose					: To add Product to User's Wish List.
Example					: usp_WishListSearchAdd 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListSearchAdd]
(
	 @userid int
	,@ProductID varchar(max)
	,@WishListAddDate datetime
	
	--Output Variable 
	, @ProductExists int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			-- To fetch Product which already exists for the user.
			SELECT UP.UserID 
					, UP.ProductID 
					, WishListItem
			INTO #PROD
			FROM UserProduct UP
				INNER JOIN fn_SplitParam (@ProductID, ',') P ON P.Param = UP.ProductID
			WHERE UserID = @userid
				AND UserRetailPreferenceID IS NULL --AND WishListItem=1
			
			--to set the flag if the product already exists for the user	
			--SELECT @ProductExists=1
			--FROM #PROD P
			--INNER JOIN UserProduct UP on up.ProductID=p.ProductID
			--WHERE WishListItem=1 AND up.UserID=@userid
			
			 --To get inactive product to make it active	
				SELECT UserID 
						, ProductID
						, WishListItem
				INTO #InActivePROD 
				FROM #PROD 
				WHERE WishListItem = 0
				
				--To set @ProductExists = 1. If the product is  already active for the User.	
				SELECT @ProductExists = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
				FROM #PROD 
				WHERE WishListItem = 1  
				  
			--To fetch Product which doesnot exist for the user.	
			SELECT UP.UserID 
					, P.Param  
			INTO #NOPROD
			FROM UserProduct UP
				RIGHT JOIN fn_SplitParam (@ProductID, ',') P ON P.Param = UP.ProductID 
																AND UP.UserID = @UserID 
																AND UserRetailPreferenceID IS NULL 
			WHERE UP.UserID IS NULL
				
			--If Product exists
			IF EXISTS (SELECT TOP 1 ProductID FROM #InActivePROD)
			BEGIN
				
				UPDATE UserProduct 
				SET WishListItem  = 1
					,WishListAddDate = @WishListAddDate 
				FROM UserProduct UP
					INNER JOIN #InActivePROD P ON P.ProductID = UP.ProductID AND P.UserID = UP.UserID
				WHERE UserRetailPreferenceID IS NULL
			END
			--If product doesnot exists
			IF EXISTS (SELECT TOP 1 Param FROM #NOPROD)
			BEGIN
				--SELECT @ProductExists=0
				INSERT INTO [UserProduct]
						   ([UserID]
						   ,[ProductID]
						   ,[MasterListItem]
						   ,[WishListItem]
						   ,[WishListAddDate] 
						   ,[TodayListtItem]
						   ,[ShopCartItem])
				SELECT  @UserID
						, Param  
						, 0
						, 1
						, @WishListAddDate 
						, 0
						, 0
				FROM #NOPROD b where not exists (select 1 From UserProduct a where a.ProductID = b.Param AND b.UserID=a.UserID)
			END
	
			       
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListSearchAdd.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
