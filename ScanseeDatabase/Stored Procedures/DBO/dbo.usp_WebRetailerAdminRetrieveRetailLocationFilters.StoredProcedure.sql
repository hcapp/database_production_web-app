USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetrieveRetailLocationFilters]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: [usp_WebRetailerAdminRetrieveRetailLocationFilters]
Purpose					: To display list of Filters for given Retailer associated Category.
Example					: [usp_WebRetailerAdminRetrieveRetailLocationFilters]

History        Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			5th AUG 2016		Shilpashree B H		  1.0
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetrieveRetailLocationFilters]

	--Input parameters	 
	  @RetailID INT
	, @RetailLocationID INT	

	--Output variables 
	, @FilterCategory Varchar(1000) OUTPUT
	, @Filters Varchar(1000) OUTPUT
	, @FilterValues Varchar(1000) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		
			
			SELECT DISTINCT BC.BusinessCategoryID businessCategoryID
					, BusinessCategoryDisplayValue businessCategoryName
					, A.AdminFilterID filterID
					, FilterName filterName
					, V.AdminFilterValueID filterValueID
					, FilterValueName FilterValueName
					, FilterAssociated = CASE WHEN RF.AdminFilterID IS NOT NULL AND RF.AdminFilterValueID IS NULL THEN 1
											 WHEN RF.AdminFilterID IS NOT NULL AND RF.AdminFilterValueID IS NOT NULL THEN 1
											ELSE 0 END 
			FROM RetailerBusinessCategory RBC
			INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID AND RetailerID = @RetailID
			INNER JOIN AdminFilterCategoryAssociation FC ON BC.BusinessCategoryID = FC.BusinessCategoryID
			INNER JOIN AdminFilter A ON FC.AdminFilterID = A.AdminFilterID
			LEFT JOIN AdminFilterValueAssociation FV ON A.AdminFilterID = FV.AdminFilterID
			LEFT JOIN AdminFilterValue V ON FV.AdminFilterValueID = V.AdminFilterValueID
			LEFT JOIN RetailerFilterAssociation RF ON RBC.RetailerID = RF.RetailID
				AND RF.RetailLocationID = @RetailLocationID AND BC.BusinessCategoryID = RF.BusinessCategoryID AND A.AdminFilterID = RF.AdminFilterID
				AND (RF.AdminFilterValueID IS NULL OR RF.AdminFilterValueID = V.AdminFilterValueID)
			ORDER BY BusinessCategoryDisplayValue,A.FilterName,V.FilterValueName 

			--To get comma separated Business Category IDs, FilterIds and Filter Value IDs

			SELECT DISTINCT BusinessCategoryID
			INTO #CatIDs
			FROM RetailerFilterAssociation
			WHERE RetailID = @RetailID AND RetailLocationID = @RetailLocationID

			SELECT @FilterCategory = COALESCE(@FilterCategory + ',','') + CAST(BusinessCategoryID AS varchar(100))
			FROM #CatIDs

			SELECT DISTINCT BusinessCategoryID ,AdminFilterID
			INTO #Filters
			FROM RetailerFilterAssociation WHERE RetailID = @RetailID AND RetailLocationID = @RetailLocationID

			SELECT DISTINCT BusinessCategoryID,AdminFilterID,AdminFilterValueID 
			INTO #FilterValues
			FROM RetailerFilterAssociation WHERE RetailID = @RetailID AND RetailLocationID = @RetailLocationID


			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
					,RetailID
					,RF.BusinessCategoryID
					,AdminFilterID = STUFF(( SELECT ',' + CAST(F.AdminFilterID AS VARCHAR(1000))
								FROM #Filters F
								WHERE  F.BusinessCategoryID = RF.BusinessCategoryID 
								FOR XML PATH ('')
								), 1,1,'')
			INTO #Temp1
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			WHERE RF.RetailID = @RetailID AND RetailLocationID = @RetailLocationID

		
			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
					,RF.BusinessCategoryID
					,RF.AdminFilterID
					,FilterValueID = STUFF(( SELECT ',' + CAST(V.AdminFilterValueID AS VARCHAR(1000))
										FROM #FilterValues V
										WHERE  V.AdminFilterID = RF.AdminFilterID AND V.BusinessCategoryID = RF.BusinessCategoryID
										FOR XML PATH ('')
										), 1,1,'')
			INTO #Temp2
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			WHERE RF.RetailID = @RetailID AND RetailLocationID = @RetailLocationID
			ORDER BY RF.BusinessCategoryID


			SELECT DISTINCT RF.BusinessCategoryID 
						,Filters = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.AdminFilterID IS NULL,'asd',F.AdminFilterID) AS VARCHAR(MAX))
															FROM #Temp1 F
															WHERE F.RetailID = RF.RetailID
															ORDER BY F.Rownum 
															FOR XML PATH(''))
															, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
						,FilterValues = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.FilterValueID IS NULL,'asd',F.FilterValueID) AS VARCHAR(MAX))
															FROM #Temp2 F
															WHERE F.BusinessCategoryID = RF.BusinessCategoryID
															ORDER BY F.Rownum 
															FOR XML PATH(''))
															, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
			INTO #FinalResult
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			WHERE RF.RetailID = @RetailID  AND RF.RetailLocationID = @RetailLocationID
			ORDER BY RF.BusinessCategoryID 

			--select * from #FinalResult

			SELECT @Filters = Filters
				   ,@FilterValues = REPLACE(COALESCE(@FilterValues+',','') + CAST(FilterValues AS varchar(max))	, ',' ,'!~~!' )
			FROM #FinalResult



		SELECT @Status = 0
	
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerAdminRetrieveRetailLocationFilters].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
