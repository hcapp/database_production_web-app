USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadRetailLocationsUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadRetailLocationsUpdation
Purpose					: 
Example					: usp_WebUploadRetailLocationsUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			08 Feb 2012	    Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadRetailLocationsUpdation]
(
	  @RetailLocationID int
	, @StoreIdentification varchar(100)
	, @Address1 varchar(1000)
	, @City varchar(255)
	, @State char(2)
	, @ZipCode char(20)
	, @PhoneNumber char(10)
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @ContactID INT
		
		--Update RetailLoaction table.	
		UPDATE RetailLocation
		SET StoreIdentification = @StoreIdentification
		  , Address1 = @Address1
		  , City = @City
		  , [State] = @State
		WHERE RetailLocationID = @RetailLocationID
		
		--Update the child table.	
		SELECT @ContactID = ContactID
		FROM RetailContact
		WHERE RetailLocationID = @RetailLocationID
		
		--Update the Contact table.
		UPDATE Contact 
		SET ContactPhone = @PhoneNumber
		WHERE ContactID = @ContactID		
		
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadRetailLocationsUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
