USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHotDealSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHotDealSearch
Purpose					: To Search for the Hot Deals in the vicinity Default/ Consumer.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			9th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHotDealSearch]
(

	--Input Input Parameter(s)--  
	   
	   @ZIPCode varchar(10)
	 , @HotDealName varchar(255)	
	 , @Radius int
	 , @LowerLimit int

	
	--Output Variable--
	 , @RowCount INT output
	 , @NextPageFlag bit output
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		DECLARE @LATITUDE FLOAT
		DECLARE @LONGITUDE FLOAT
		DECLARE @UpperLimit INT
		DECLARE @MaxCnt INT
		
		--To get Media Server Configuration.  
		DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'		
		
		--To get the Number of records per page.
		DECLARE @ScreenContent INT
		SELECT @ScreenContent = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Website Pagination'
		AND ScreenName = 'All'
		AND Active = 1
		
		
		SELECT @UpperLimit = @LowerLimit + @ScreenContent
		IF @ZIPCode IS NOT NULL
		BEGIN
		
		--Derive the Lat & Long for the input zip code.
					SELECT  @LATITUDE = G.Latitude
						  , @LONGITUDE = G.Longitude
					FROM GeoPosition G  
					WHERE G.PostalCode = @ZIPCode		
			
					SELECT [State]
						 , [City]
						 , PostalCode
						 , Distance
					INTO #ZipCodes
					FROM(		
					SELECT [State]
						 , [City]
						 , PostalCode
						 , Distance = (ACOS((SIN(G.Latitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(G.Latitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (G.Longitude / 57.2958))))*6371) * 0.6214   	    
					from GeoPosition G)D
					WHERE Distance <= ISNULL(@Radius, 5)
			
			
			
			--Get all the Hot deals into a Temporary table.	
			SELECT RowNum = ROW_NUMBER()OVER(ORDER BY ProductHotDealID)
				 , ProductHotDealID
				 , HotDealName
				 , HotDeaLonglDescription
				 , HotDealShortDescription
				 , HotDealImagePath
				 , HotDealDiscountAmount
				 , HotDealTermsConditions
				 , HotDealURL
				 , Price
				 , HotDealStartDate
				 , HotDealEndDate
			  --   , ProductID
				 --, ProductImagePath
				 --, ProductLongDescription
				 --, ProductShortDescription
				 --, ScanCode  
				 , RetailLocationID
			     
			 INTO #Temp
			 FROM 
			  --  Hot Deal details
			 (SELECT DISTINCT TOP 100 PERCENT  PH.ProductHotDealID
					 , PH.HotDealName
					 , PH.HotDeaLonglDescription
					 , PH.HotDealShortDescription
					 , HotDealImagePath = CASE WHEN PH.HotDealImagePath IS NOT NULL THEN 
										CASE WHEN PH.WebsiteSourceFlag = 1 THEN 
											CASE WHEN PH.ManufacturerID IS NOT NULL THEN @ManufacturerConfig +CONVERT(VARCHAR(30),PH.ManufacturerID)+'/'+ HotDealImagePath
																					 ELSE @RetailerConfig + CONVERT(VARCHAR(30),PH.RetailID)+'/'+HotDealImagePath
											   END

 											ELSE PH.HotDealImagePath
										END
									END
					 , PH.HotDealDiscountAmount
					 , PH.HotDealTermsConditions
					 , PH.HotDealURL
					 , PH.Price
					 , PH.HotDealStartDate
					 , PH.HotDealEndDate
					 
					 --Product Details
					-- , P.ProductID
					 --, P.ProductImagePath
					 --, P.ProductLongDescription
					 --, P.ProductShortDescription
					 --, P.ScanCode
					 , PHDRL.RetailLocationID
					 
					 FROM #ZipCodes Z 
					 INNER JOIN ProductHotDealLocation PHL ON Z.City = PHL.City AND PHL.State = Z.State
					 INNER JOIN ProductHotDeal PH ON PHL.ProductHotDealID = PH.ProductHotDealID AND PH.HotDealName LIKE (CASE WHEN @HotDealName IS NOT NULL THEN '%'+@HotDealName+'%'  ELSE '%' END)
					 LEFT OUTER JOIN ProductHotDealRetailLocation PHDRL ON PHDRL.ProductHotDealID = PH.ProductHotDealID
					 WHERE GETDATE()>=HotDealStartDate AND GETDATE()<=HotDealEndDate
					 ORDER BY PH.HotDealName)Deals
		   
		   
		   
			--Find out the total number of records in the result set.
			SELECT @MaxCnt = COUNT(DISTINCT(ProductHotDealID)) FROM #TEMP
			SET @RowCount = @MaxCnt
			SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END    --CHECK IF THERE ARE STILL MORE RECORDS
			
			SELECT DISTINCT
				   ProductHotDealID hotDealID
				 , HotDealName hotDealName
				 , HotDeaLonglDescription
				 , HotDealShortDescription hotDealShortDescription
				 , HotDealImagePath dealImgPath
				 , HotDealDiscountAmount
				 , HotDealTermsConditions
				 , HotDealURL
				 , Price
				 , HotDealStartDate
				 , HotDealEndDate	     
			  --   , ProductID
				 --, ProductImagePath
				 --, ProductLongDescription
				 --, ProductShortDescription
				 --, ScanCode
				 , RetailLocationID  
			FROM #TEMP	
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit             --display the records within the specified limits
			
			IF @RowCount IS NULL									--TO CHECK IF THE RESULT SET IS EMPTY
						SET @RowCount = 0
	END
	
	--If Zip Code is not input
	ELSE
	BEGIN
		   SELECT  RowNum = ROW_NUMBER()OVER(ORDER BY ProductHotDealID ASC)
				 , ProductHotDealID 
				 , HotDealName 
				 , HotDeaLonglDescription
				 , HotDealShortDescription 
				 , HotDealImagePath 
				 , HotDealDiscountAmount
				 , HotDealTermsConditions
				 , HotDealURL
				 , Price
				 , HotDealStartDate
				 , HotDealEndDate	
	     	     , RetailLocationID 
	     	 INTO #Temp1
			 FROM 
			(SELECT DISTINCT
				   PHD.ProductHotDealID 
				 , HotDealName 
				 , HotDeaLonglDescription
				 , HotDealShortDescription 
				 , HotDealImagePath 
				 , HotDealDiscountAmount
				 , HotDealTermsConditions
				 , HotDealURL
				 , Price
				 , HotDealStartDate
				 , HotDealEndDate	
				 , PHDRL.RetailLocationID	 
			FROM ProductHotDeal PHD
			INNER JOIN ProductHotDealLocation PHDL ON PHD.ProductHotDealID = PHDL.ProductHotDealID
			LEFT OUTER JOIN ProductHotDealRetailLocation PHDRL ON PHDRL.ProductHotDealID = PHD.ProductHotDealID
			WHERE PHD.HotDealName LIKE (CASE WHEN @HotDealName IS NOT NULL THEN '%'+@HotDealName+'%' ELSE '%' END)
			AND GETDATE()>=HotDealStartDate AND GETDATE()<=HotDealEndDate)HotDeals
			
		    SELECT @MaxCnt = COUNT(DISTINCT(ProductHotDealID)) FROM #Temp1
			SET @RowCount = @MaxCnt
			SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END 
			
			SELECT ProductHotDealID hotDealID
				 , HotDealName 
				 , HotDeaLonglDescription
				 , HotDealShortDescription 
				 , HotDealImagePath 
				 , HotDealDiscountAmount
				 , HotDealTermsConditions
				 , HotDealURL
				 , Price
				 , HotDealStartDate
				 , HotDealEndDate	
	     	     , RetailLocationID
			FROM #Temp1
			WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
			
			IF @RowCount IS NULL									--TO CHECK IF THE RESULT SET IS EMPTY
					SET @RowCount = 0
		
	END
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealSearch.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
