USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierAddProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAddProduct
Purpose					: To insert product information by the suppliers.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierAddProduct]
(
	--Input variables for the product table
	 @UserID int
	,@ProductUPC varchar(20)
	,@ProductName Varchar(500)
	,@ModelNumber varchar(50)
	,@SuggestedRetailPrice Money
	,@ProductimagePath varchar(100)
	,@LongDescription Varchar(max)
	,@ShortDescription Varchar(255)
	,@Warranty varchar(255)
	,@ImagePath Varchar(max)
	
	--input variables for productmedia table 
	,@AudioMediapath varchar(255)
	,@VideoMediaPath varchar(255)
	
	--Input variables for ProductCategory
	,@CategoryID int
	
	--input variables for productattributes table
	,@AttributeID Varchar(110)			         
	,@DisplayValue Varchar(max)
	
	--Output Variable 
	, @ProductID int output
	, @DuplicateProduct bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @ManufacturerID INT
			DECLARE @OtherAttributeID int
			
			Select @OtherAttributeID = AttributeId from Attribute Where AttributeName = 'Others'
			
			SELECT @ManufacturerID = ManufacturerID 
			FROM UserManufacturer 
			WHERE UserID = @UserID
			
			DECLARE @VideoFiles INT, @AudioFiles INT, @ImageFiles INT
			
			SELECT @VideoFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Video Files' 
			SELECT @AudioFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Audio Files' 
			SELECT @ImageFiles=ProductMediaTypeID from ProductMediaType where ProductMediaType='Image Files' 
			
			
				--Inserting into product table
			IF NOT EXISTS(SELECT 1 FROM Product WHERE ScanCode = @ProductUPC)
			BEGIN			
				 INSERT INTO [Product]
						   ([ScanCode]
						   ,[Service]
						   ,[ProductName]
						   ,[ProductShortDescription]
						   ,[ProductLongDescription]
						   ,[ModelNumber]
						   ,[ProductImagePath]
						   ,[SuggestedRetailPrice]
						   ,[ProductAddDate]
						   ,[ProductModifyDate]
						   ,[WarrantyServiceInformation]
						   ,[ManufacturerID] 
						   , WebsiteSourceFlag)
				  VALUES(@ProductUPC
						,0
						,@ProductName
						,@ShortDescription
						,@LongDescription
						,@ModelNumber
						,@ProductimagePath
						,@SuggestedRetailPrice
						,GETDATE()
						,GETDATE()
						,@Warranty
						,@ManufacturerID
						, 1)
					
				 	
					DECLARE @Product_ID int
					SET @Product_ID=SCOPE_IDENTITY();
					SET @ProductID=@Product_ID
					
				--Inserting Category of the product into productcategory table	
				IF(@CategoryID IS NOT NULL)
				BEGIN
				INSERT INTO ProductCategory (ProductID
										   , CategoryID
										   , DateAdded)
									VALUES(@ProductID
									     , @CategoryID
									     , GETDATE())
				END
									     
				--Inserting media info of the product into product media table
				IF(@AudioMediapath IS NOT NULL)
				BEGIN	
					INSERT INTO [ProductMedia]
							   ([ProductID]
							   ,[ProductMediaTypeID]
							   ,[DateAdded]
							   ,[ProductMediaPath]
							   ,[ProductMediaName])
					SELECT @ProductID
						 , @AudioFiles
						 , GETDATE()
						 , Param
						 , 'Audio'
						FROM fn_SplitParam(@AudioMediapath,',')
				END
				
				IF(@VideoMediaPath IS NOT NULL)
				BEGIN	  
						INSERT INTO [ProductMedia]
								   ([ProductID]
								   ,[ProductMediaTypeID]
								   ,[DateAdded]
								   ,[ProductMediaPath]
								   ,[ProductMediaName])
						SELECT @ProductID
							 , @VideoFiles
							 , GETDATE()
							 , Param
							 , 'Video'
						FROM fn_SplitParam(@VideoMediaPath,',')
						
				END
				
				IF (@ImagePath IS NOT NULL)
				BEGIN	  
						INSERT INTO [ProductMedia]
								   ([ProductID]
								   ,[ProductMediaTypeID]
								   ,[DateAdded]
								   ,[ProductMediaPath]
								   ,[ProductMediaName])
						SELECT @Product_ID
							   ,@ImageFiles				
							   ,GETDATE()
							   ,param
							   ,'Image'
					   FROM fn_SplitParam(@ImagePath,',')
				END
  
				--inserting product attributes into product attribut table
				
				SELECT Row_num=IDENTITY(int,1,1)
					  ,Param AttributeID
				INTO #AttributeName
				FROM fn_SplitParam(@AttributeID,',')
				
				SELECT Row_num=IDENTITY(int,1,1)
					  ,Param DisplayValue
				INTO #DisplayValue
				FROM fn_SplitParam(@DisplayValue,',')
				
				SELECT Row_num=IDENTITY(int,1,1)
				     , AttributeID
				     , DisplayValue
				INTO #Temp
				FROM (Select A.AttributeID
						   , D.DisplayValue   
				       From #AttributeName A 
				       INNER JOIN #DisplayValue D ON A.Row_num = D.Row_num) Attributes
				
					  
				INSERT INTO ProductAttributes(
												ProductID
											  , AttributeID
											  , DisplayValue											  
											 )
											Select @ProductID
												 , AttributeID
												 , DisplayValue
											From #Temp 
											Where AttributeID <> @OtherAttributeID
											
			    INSERT INTO ProductAttributes(
												ProductID
											  , AttributeID											 
											  , DisplayValue
											  )
								   SELECT @ProductID
								        , AttributeID								       
									    , DisplayValue
									From #Temp 
									Where AttributeID = @OtherAttributeID    
			
		
		
		 --Confirmation of Success.
				SELECT @Status = 0
				SET @DuplicateProduct = 0
			END
			ELSE
			BEGIN
				PRINT 'Product Exsts in the System'
				SET @DuplicateProduct = 1
				SET @Status = 0 
			END
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebAddProduct.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
