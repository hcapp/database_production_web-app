USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerDisplayProductReviews]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerDisplayProductReviews]
Purpose					: To display the products Reviews.
Example					: [usp_WebConsumerDisplayProductReviews]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			24th may 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerDisplayProductReviews]
(
	
   --Input Variables 
    
     @ProductID varchar(500)
  -- , @ParentCategoryID varchar(10)
   , @LowerLimit int
   , @ScreenName varchar(100)
   
	--Output Variable 
	, @NxtPageFlag bit output
	, @MaxCnt int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	    DECLARE @Config varchar(50)	    
	    DECLARE @UpperLimit INT 
	    DECLARE @ManufConfig varchar(50)   	
		
		SELECT @UpperLimit = @LowerLimit + ScreenContent 
        FROM AppConfiguration 
        WHERE ScreenName = @ScreenName 
        AND ConfigurationType = 'Pagination'
        AND Active = 1 	 
	  
		SELECT @ManufConfig= ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		 
	    --Select product reviews
	    
		SELECT Rownum=ROW_NUMBER() Over (order by ProductReviewsID)
			,ProductReviewsID productReviewsID
			,PR.ProductID productID
			,ReviewURL reviewURL
			,ReviewComments reviewComments 
			,ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1     
                       THEN @ManufConfig    
                       +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'    
                       +ProductImagePath ELSE ProductImagePath     
                       END       
                          ELSE ProductImagePath END
		INTO #Temp
		FROM ProductReviews PR
		INNER JOIN Product P ON P.ProductID =PR.ProductID 
		WHERE PR.ProductID=@ProductID 	
		
		
		--Calculate the total number of records.
		SELECT @MaxCnt = MAX(RowNum) FROM #Temp	
		
		--Check if the next page exixts.
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
	
		SELECT RowNum rowNumber
			  ,productReviewsID 
			  ,productID 
			  ,reviewURL 
			  ,reviewComments 
			  ,ProductImagePath	  
		FROM #Temp
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerDisplayProductReviews].'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
