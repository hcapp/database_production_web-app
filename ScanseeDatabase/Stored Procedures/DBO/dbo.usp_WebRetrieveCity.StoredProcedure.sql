USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveCity]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetrieveCity
Purpose					: To Display the City ASSOCIATED WITH STATE.
Example					: usp_WebRetrieveCity

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			4th January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveCity]
(

	--Input Parameter(s)--   
	  
	  @State char(2)
	--Output Variable--	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		SELECT CAST(@State AS VARCHAR(2)) +'-'+ City
		FROM RetailLocation 
		WHERE [State] = @State
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveCity.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
