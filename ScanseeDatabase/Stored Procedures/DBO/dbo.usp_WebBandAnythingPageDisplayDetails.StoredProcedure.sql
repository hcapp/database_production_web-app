USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAnythingPageDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandAnythingPageCreation
Purpose					: Fetch the details of the input Anything Page.
Example					: usp_WebBandAnythingPageCreation

History
Version				Date				Author			Change Description
------------------------------------------------------------------------------- 
1.0               18th Apr 2016       Sagar Byali		[usp_WebRetailerAnythingPageDisplayDetails]   
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandAnythingPageDisplayDetails]
(

	--Input Parameter(s)--
	
	  @RetailID int
	, @PageID int
	
	--Output Variable--	  	
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
		   DECLARE @Config varchar(50)
		   DECLARE @RetailLocations varchar(max)
		   
		  --To get Media Server Configuration. 		     
		  SELECT @Config= 
		  ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Retailer Media Server Configuration'  
		   
		  --To fetch the Associated Retail Locations.
		  SELECT @RetailLocations = COALESCE(@RetailLocations + ',', '') + CAST(QRA.RetailLocationID AS VARCHAR(10))
		  FROM QRBandCustomPage QR
		  LEFT JOIN QRBandCustomPageAssociation QRA ON QR.QRBandCustomPageID = QRA.QRBandCustomPageID
		  WHERE QR.BandID = @RetailID 
		  AND QR.QRBandCustomPageID = @PageID
		  
		  --Fetch the details of the input Anything Page.
			SELECT QR.Pagetitle
			     , imageName = [Image]
				 , ImagePath = (CASE WHEN QR.[Image] IS NOT NULL THEN @Config + CAST(@RetailID AS VARCHAR(10)) + '/' + QR.[Image] ELSE NULL END)  
				 , URL
				 , PageDescription
				 , ShortDescription
				 , LongDescription
				 , StartDate = CONVERT(DATE, StartDate) 
				 , StartTime = CONVERT(TIME, StartDate)
				 , EndDate = CONVERT(DATE, EndDate)
				 , EndTime = CONVERT(TIME, EndDate)
				 , (CASE WHEN QRM.MediaPath IS NOT NULL THEN @Config + CAST(@RetailID AS VARCHAR(10)) + '/' + QRM.MediaPath ELSE NULL END) fileName
				 , pdfFileName=(CASE WHEN QRM.MediaPath IS NOT NULL THEN QRM.MediaPath ELSE NULL END)
				 , imageIconID=CASE WHEN QRI.QRBandCustomPageIconID IS NOT NULL THEN QRI.QRBandCustomPageIconID ELSE 0 END 
				 , DefaultImageIcon = (CASE WHEN QR.QRBandCustomPageIconID IS NOT NULL THEN @Config + QRI.QRBandCustomPageIconImagePath ELSE NULL END)
				 , RetailLocations = @RetailLocations 
			FROM QRBandCustomPage QR
			INNER JOIN QRTypes QRT ON QRT.QRTypeID = QR.QRTypeID			
			LEFT OUTER JOIN QRBandCustomPageMedia QRM ON QR.QRBandCustomPageID = QRM.QRBandCustomPageID
			LEFT OUTER JOIN QRBandCustomPageIcons QRI ON QRI.QRBandCustomPageIconID = QR.QRBandCustomPageIconID
			WHERE QRT.QRTypeName = 'Anything Page'
			AND QR.BandID = @RetailID
			AND QR.QRBandCustomPageID = @PageID
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandAnythingPageCreation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
