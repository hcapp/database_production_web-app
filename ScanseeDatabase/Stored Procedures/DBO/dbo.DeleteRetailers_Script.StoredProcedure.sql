USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[DeleteRetailers_Script]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [DeleteRetailers_Script]
Purpose					: To Delete the retailers from the given excel.
Example					: [DeleteRetailers_Script]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Bindu T A           1.1
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DeleteRetailers_Script]
(
	  --@HubcitiID INT,

	--Output Variable
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN 

BEGIN TRY
	
	BEGIN TRANSACTION

		-- To Move things into Archive table

		INSERT INTO DeleteRetailerLoad_archive  ([Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Dateprocessed)
		SELECT 
		[Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Dateprocessed
		FROM DeleteRetailerLoad

		TRUNCATE TABLE DeleteRetailerLoad

		-- TO Move data from Sample table to main table

		INSERT INTO DeleteRetailerLoad ([Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],Dateprocessed)
		SELECT DISTINCT
		[Retail ID],[Retail LocationID],[Retail Name],Address,City,State,[Postal Code],GETDATE()
		FROM DeleteRetailerLoad_sample

		SELECT DISTINCT Z.* 
		INTO #Processed
		FROM DeleteRetailerLoad  Z
		INNER JOIN Retailer R ON Z.[Retail ID] = R.RetailID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = Z.[Retail LocationID] AND R.retailID = RL.retailID AND R.RetailID = Z.[Retail ID] 

		SELECT DISTINCT [Retail ID],[Retail LocationID], [Retail Name], Address,City,State,[Postal Code]
		INTO [DeleteRetailerLoad_Unprocessed]
		FROM(
		SELECT [Retail ID],[Retail LocationID], [Retail Name], Address,City,State,[Postal Code]
		FROM DeleteRetailerLoad
		EXCEPT
		SELECT [Retail ID],[Retail LocationID], [Retail Name], Address,City,State,[Postal Code] 
		FROM #Processed
		)P
	

		UPDATE Retaillocation
		SET Active = 0
		FROM #Processed Z
		INNER JOIN Retailer R ON Z.[Retail ID] = R.RetailID 
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = Z.[Retail LocationID] AND R.retailID = RL.retailID

		--To Update The Corporate And HeadQuarters for RetailLocations

		SELECT DISTINCT retailid  INTO #temp  FROM retaillocation RL  
		WHERE  (RL.Headquarters = 1 OR RL.CorporateAndStore = 1) AND Active=1

		SELECT retailid  into #update  from retailer
		EXCEPT
		SELECT * FROM #temp
	
		;WITH aa AS(
		SELECT ROW_NUMBER() OVER(PARTITION BY  a.RetailID ORDER BY a.RetailID) AS rowno,a.*  
		FROM retaillocation  a  INNER JOIN #update  b ON a.RetailID=b.RetailID)
		SELECT * INTO #sc FROM aa  WHERE rowno=1

		UPDATE retaillocation SET CorporateAndStore=1  FROM retaillocation INNER JOIN #sc ON #sc.RetailID=retaillocation.RetailID
		AND #sc.retaillocationid=retaillocation.retaillocationid

		-- To display the unprocessed data

		SELECT * FROM [DeleteRetailerLoad_Unprocessed]

		DROP TABLE [DeleteRetailerLoad_Unprocessed]

		TRUNCATE TABLE DeleteRetailerLoad_sample
	
	COMMIT TRANSACTION

END TRY

BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		
		ROLLBACK TRANSACTION
		PRINT 'Error occured in Stored Procedure [DeleteRetailers_Script].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
		--Confirmation of failure.
		SELECT @Status = 1
	END
		 
END CATCH

END
GO
