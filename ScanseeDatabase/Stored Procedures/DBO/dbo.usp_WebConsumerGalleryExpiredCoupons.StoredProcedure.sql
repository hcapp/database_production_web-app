USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGalleryExpiredCoupons]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_WebConsumerGalleryExpiredCoupons   
Purpose					: To list Coupon  
Example					: usp_WebConsumerGalleryExpiredCoupons   
  
History  
Version  Date				Author		 Change Description  
---------------------------------------------------------------   
1.0		 24th April 2013	Mohith H R	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerGalleryExpiredCoupons]  
(  
   @UserID int  
 , @RetailerID int
 , @LowerLimit int  
 , @SearchKey varchar(255)
 , @CategoryIDs varchar(1000)
 , @ScreenName varchar(50)   
 , @RecordCount int  
   
 --OutPut Variable  
 , @MaxCnt int output
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
   
 BEGIN TRY  
 
  --To get Media Server Configuration.  
  DECLARE @RetailConfig varchar(50)    
  SELECT @RetailConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Retailer Media Server Configuration'  
   
   --To get the row count for pagination.  
    DECLARE @UpperLimit int   
    SELECT @UpperLimit = @LowerLimit + @RecordCount
    
    CREATE TABLE #Coupon(Row_Num INT IDENTITY(1, 1)
					  , CouponID INT 
					  , CouponName VARCHAR(255)
					  , CouponDiscountType VARCHAR(255)
					  , CouponDiscountAmount MONEY
					  , CouponDiscountPct FLOAT
					  , CouponDescription VARCHAR(1000) 
					  , CouponDateAdded DATETIME
					  , CouponStartDate DATETIME
					  , CouponExpireDate DATETIME
					  , CouponURL VARCHAR(1000)
					  , CouponImagePath VARCHAR(1000)
					  , UsedFlag BIT
					  , ViewableOnWeb BIT
					  , CategoryID int
					  , CategoryName varchar(100))
   --If there is no category selected then display all the coupons.
   IF @CategoryIDs = '0'  
   BEGIN
		print 'No Category'
	   --If Retailer not selected
	   IF @RetailerID = 0
		BEGIN		
			 print 'No Retailer'  
			  INSERT INTO #Coupon(CouponID   
								,CouponName  
								,CouponDiscountType  
								,CouponDiscountAmount  
								,CouponDiscountPct  
								,CouponDescription 							
								,CouponDateAdded  
								,CouponStartDate  
								,CouponExpireDate  
								,CouponURL  
								,CouponImagePath  
								,UsedFlag  
								,ViewableOnWeb
								,CategoryID
								,CategoryName)
		
				SELECT DISTINCT CG.CouponID   
				,CouponName  
				,CouponDiscountType  
				,CouponDiscountAmount  
				,CouponDiscountPct  
				,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
				,CouponDateAdded  
				,CouponStartDate  
				,CouponExpireDate  
				,CouponURL  
				,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																													CASE WHEN C.WebsiteSourceFlag = 1 
																														THEN @RetailConfig
																														+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																														+CouponImagePath 
																													ELSE CouponImagePath 
																													END
																											 END 
								 END  
				,UsedFlag  
				,ViewableOnWeb
				, CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
				, CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END 
			   FROM Coupon C  
			   INNER JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID AND UserID = @UserID   
			   LEFT JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			   LEFT JOIN ProductCategory PC ON PC.ProductID = CP.ProductID
			   LEFT JOIN Category Cat ON Cat. CategoryID = PC.CategoryID  	
			   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID   
			   WHERE CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
			   AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
			   ORDER BY CategoryName,C.CouponName ASC
			END 
			
		ELSE
		--If Retailer Selected
		BEGIN
			print 'Retailer'
			  INSERT INTO #Coupon(CouponID   
								,CouponName  
								,CouponDiscountType  
								,CouponDiscountAmount  
								,CouponDiscountPct  
								,CouponDescription 							
								,CouponDateAdded  
								,CouponStartDate  
								,CouponExpireDate  
								,CouponURL  
								,CouponImagePath  
								,UsedFlag  
								,ViewableOnWeb
								,CategoryID
								,CategoryName)
		
				SELECT DISTINCT CG.CouponID   
				,CouponName  
				,CouponDiscountType  
				,CouponDiscountAmount  
				,CouponDiscountPct  
				,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
				,CouponDateAdded  
				,CouponStartDate  
				,CouponExpireDate  
				,CouponURL  
				,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																													CASE WHEN C.WebsiteSourceFlag = 1 
																														THEN @RetailConfig
																														+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																														+CouponImagePath 
																													ELSE CouponImagePath 
																													END
																											 END 
								 END  
				,UsedFlag  
				,ViewableOnWeb
				, CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
				, CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END 
			   FROM Coupon C  
			   INNER JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID AND UserID = @UserID   
			   LEFT JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			   LEFT JOIN ProductCategory PC ON PC.ProductID = CP.ProductID
			   LEFT JOIN Category Cat ON Cat. CategoryID = PC.CategoryID  	
			   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID   
			   WHERE CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
			   AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
			   AND CR.RetailID = @RetailerID
			   ORDER BY CategoryName, C.CouponName ASC
			END 
	   END
				
		
		--If a category id is input then filter based on the categories associated to the products that belong to the coupon.
		IF @CategoryIDs <> '0'  
		BEGIN
			PRINT 'Category'
			--Check if Retailer selected
			IF @RetailerID <> 0
			BEGIN
			   print 'Retailer'
			   INSERT INTO #Coupon(CouponID   
								,CouponName  
								,CouponDiscountType  
								,CouponDiscountAmount  
								,CouponDiscountPct  
								,CouponDescription 							
								,CouponDateAdded  
								,CouponStartDate  
								,CouponExpireDate  
								,CouponURL  
								,CouponImagePath  
								,UsedFlag  
								,ViewableOnWeb
								,CategoryID
								,CategoryName)
		
				SELECT DISTINCT CG.CouponID   
					  ,CouponName  
					  ,CouponDiscountType  
					  ,CouponDiscountAmount  
					  ,CouponDiscountPct  
					  ,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
					  ,CouponDateAdded  
					  ,CouponStartDate  
					  ,CouponExpireDate  
					  ,CouponURL  
					  ,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																														CASE WHEN C.WebsiteSourceFlag = 1 
																															THEN @RetailConfig
																															+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																															+CouponImagePath 
																														ELSE CouponImagePath 
																														END
																												 END 
									   END  
					  ,UsedFlag  
					  ,ViewableOnWeb 
					  , CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
					  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END 
			   FROM Coupon C  
			   INNER JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID AND UserID = @UserID
			   INNER JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			   INNER JOIN Product P ON P.ProductID = CP.ProductID
			   INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			   INNER JOIN Category CAT ON CAT.CategoryID = PC.CategoryID
			   INNER JOIN dbo.fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID   
			   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID   
			   WHERE CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
			   AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 	
			   AND CR.RetailID = @RetailerID
			   ORDER BY CategoryName, C.CouponName ASC	
		 END
		
		ELSE
		BEGIN
			--If No Retailer Selected
			print 'No Retailer'
			   INSERT INTO #Coupon(CouponID   
								,CouponName  
								,CouponDiscountType  
								,CouponDiscountAmount  
								,CouponDiscountPct  
								,CouponDescription 							
								,CouponDateAdded  
								,CouponStartDate  
								,CouponExpireDate  
								,CouponURL  
								,CouponImagePath  
								,UsedFlag  
								,ViewableOnWeb
								,CategoryID
								,CategoryName)
		
				SELECT DISTINCT CG.CouponID   
					  ,CouponName  
					  ,CouponDiscountType  
					  ,CouponDiscountAmount  
					  ,CouponDiscountPct  
					  ,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
					  ,CouponDateAdded  
					  ,CouponStartDate  
					  ,CouponExpireDate  
					  ,CouponURL  
					  ,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																														CASE WHEN C.WebsiteSourceFlag = 1 
																															THEN @RetailConfig
																															+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																															+CouponImagePath 
																														ELSE CouponImagePath 
																														END
																												 END 
									   END  
					  ,UsedFlag  
					  ,ViewableOnWeb 
					  , CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
					  , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END 
			   FROM Coupon C  
			   INNER JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID AND UserID = @UserID
			   INNER JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			   INNER JOIN Product P ON P.ProductID = CP.ProductID
			   INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			   INNER JOIN Category CAT ON CAT.CategoryID = PC.CategoryID
			   INNER JOIN dbo.fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID   
			   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID   
			   WHERE CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90
			   AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
			   ORDER BY CategoryName, C.CouponName ASC	
		END
     END
   --To capture max row number.  
    SELECT @MaxCnt = ISNULL(MAX(Row_Num),0) FROM #Coupon  
    --this flag is a indicator to enable "More" button in the UI.   
    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
  
   SELECT Row_Num rowNum  
    ,C.CouponID couponId  
    ,C.CouponName couponName  
    ,C.CouponDiscountType   
    ,C.CouponDiscountAmount   
    ,C.CouponDiscountPct 
    , CouponDescription coupDesptn   
    ,C.CouponDateAdded coupDateAdded  
    ,C.CouponStartDate coupStartDate  
    ,C.CouponExpireDate coupExpireDate  
    ,CouponURL  
    ,C.CouponImagePath 
    ,C.UsedFlag favFlag 
    ,C.ViewableOnWeb
    ,C.CategoryID cateId
    , C.CategoryName cateName
   FROM #Coupon C  
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   Order by Row_Num  
    
    
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerGalleryExpiredCoupons.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
