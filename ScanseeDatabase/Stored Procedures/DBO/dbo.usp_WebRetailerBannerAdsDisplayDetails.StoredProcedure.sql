USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdsDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebRetailerBannerAdsDisplayDetails]
Purpose                  : To display the Banner ads created by the given Retailer.
Example                  : [usp_WebRetailerBannerAdsDisplayDetails]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Aug 2012        Dhananjaya TR   Initial Version                                        Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdsDisplayDetails]
(

      --Input Input Parameter(s)--  
      
        @AdvertisementBannerID INT
      --Output Variable--
	  
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
      --To Fetch the Retailer Media server Configuaration
      
			DECLARE @Config varchar(255)    
			SELECT @Config=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration' AND Active =1  
		
		
       --Display The Banner page Details
       
				SELECT AB.AdvertisementBannerID as retailLocationAdvertisementID
				      ,AB.BannerAdName as advertisementName 
					  ,@Config + CONVERT(VARCHAR(30),AB.RetailID) +'/' + BannerAdImagePath as strBannerAdImagePath
					  ,AB.BannerAdURL as ribbonXAdURL
					  ,advertisementDate = CASE WHEN AB.StartDate = '01/01/1900' THEN (SELECT StartDate
																					  FROM AdvertisementBannerHistory 
																					  WHERE AdvertisementBannerID = @AdvertisementBannerID
																					  AND DateCreated = (SELECT MAX(DateCreated)
																										 FROM AdvertisementBannerHistory
																										 WHERE AdvertisementBannerID = @AdvertisementBannerID)) ELSE AB.StartDate END
				      ,advertisementEndDate = CASE WHEN AB.EndDate = '01/01/1900' THEN (SELECT EndDate
																					  FROM AdvertisementBannerHistory 
																					  WHERE AdvertisementBannerID = @AdvertisementBannerID
																					  AND DateCreated = (SELECT MAX(DateCreated)
																										 FROM AdvertisementBannerHistory
																										 WHERE AdvertisementBannerID = @AdvertisementBannerID)) ELSE AB.EndDate END
				FROM AdvertisementBanner AB 
				WHERE AB.AdvertisementBannerID = @AdvertisementBannerID                   
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdsDisplayDetails.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;




GO
