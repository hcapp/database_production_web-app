USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponInsertion
Purpose					: To create a new coupon
Example					: 

History
Version		Date					Author				Change Description
------------------------------------------------------------------------------- 
1.0			26th Dec 2011			Naga Sandhya S		Initial Version
2.0														Keyword changes
3.0														
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCouponInsertion]
(
  
	--Input Variables for Coupon Table
	  
	  @RetailID int
	, @RetailLocationID VARCHAR(4000)
	, @CouponName varchar(100)
	, @NoOfCouponsToIssue int
	, @CouponLongDescription nvarchar(max)
	, @CouponTermsAndConditions varchar(1000)
	, @CouponImagePath varchar(1000)
	, @CouponStartDate date
	, @CouponEndDate date
	, @CouponStartTime time
	, @CouponEndTime time
	, @CouponTimeZoneID int
	, @BannerTitle varchar(5000)
	, @CouponExpirationDate date
	, @CouponExpirationTime time
	, @KeyWords varchar(max)
	, @CouponDetailImage varchar(max)
	
	--Input Variables for CouponProduct Table 
	, @ProductIDs Varchar(1000)
	
	--Output Variable--
	--, @PercentageFlag bit output 
	, @FindClearCacheURL varchar(1000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @CouponID INT
			DECLARE @APIPartnerID INT
			SELECT @APIPartnerID =APIPartnerID
		    FROM APIPartner
		    WHERE APIPartnerName = 'ScanSee'
			
			DECLARE @CouponDiscountPct float
			DECLARE @RegularPrice Money
			SELECT @RegularPrice = Price
		    FROM Product p 
			INNER JOIN RetailLocationProduct RLP ON P.ProductID = RLP.ProductID 
			INNER JOIN dbo.fn_SplitParam(@ProductIDs, ',') Pr ON Pr.Param = P.ProductID
			
			--SET @CouponDiscountPct = (@CouponFaceValue * 100)/@RegularPrice			
					
		--IF (@CouponDiscountPct <= 50)
		--BEGIN
			--Insert into Coupon Table			
			INSERT INTO Coupon (
								 RetailID
								, APIPartnerID
								,CouponName	
								,BannerTitle
								,CouponDiscountPct 
								,CouponShortDescription 
								,CouponLongDescription 
								,CouponTermsAndConditions
								,NoOfCouponsToIssue
								,CouponImagePath
								,CouponDateAdded
								,CouponStartDate
								,CouponExpireDate 
								,ActualCouponExpirationDate 
								,CouponTimeZoneID
								,WebsiteSourceFlag
								,CreatedUserID
								,KeyWords
								,CouponDetailImage
								)
						 
						 VALUES(
								@RetailID
								,@APIPartnerID
								,@CouponName
								,@BannerTitle
								,@CouponDiscountPct
								,@CouponLongDescription
								,@CouponLongDescription
								,@CouponTermsAndConditions
								,@NoOfCouponsToIssue
								,@CouponImagePath
								,GETDATE()
								,CAST(@CouponStartDate AS DATETIME) + CAST(ISNULL(@CouponStartTime,'') AS datetime)
								,CAST(@CouponEndDate AS DATETIME) + CAST(ISNULL(@CouponEndTime,'') AS datetime)
								,CAST(ISNULL(@CouponExpirationDate,NULL) AS DATETIME) + CAST(ISNULL(@CouponExpirationTime,'') AS datetime)
								,@CouponTimeZoneID
								,1
								,(SELECT UserID FROM UserRetailer WHERE RetailID = @RetailID)
								,@KeyWords
								,@CouponDetailImage
								)
								
								
				SET @CouponID = SCOPE_IDENTITY();	
				
				IF(@ProductIDs IS NOT NULL)
				BEGIN						
				--Insert into CouponProduct Table
				CREATE TABLE #Prod(CouponID INT, ProductID INT)
				INSERT INTO CouponProduct(CouponID
				                         ,ProductID
				                         ,DateAdded)
				  OUTPUT inserted.CouponID, inserted.ProductID INTO #Prod                       
				                  SELECT @CouponID
				                       , P.Param
				                       , GETDATE()
				                  FROM dbo.fn_SplitParam(@ProductIDs, ',') P
				END
				--Insert into CouponRetailer Table
				IF(@RetailLocationID IS NOT NULL)
				BEGIN
				INSERT INTO CouponRetailer (					--Associate the Coupon to the given Retail Locations
												CouponID
											  , RetailID 
											  , RetailLocationID
											  , DateCreated	
											)
                                   SELECT @CouponID
                                        , @RetailID
                                        , RL.Param
                                        , GETDATE()
                                   FROM dbo.fn_SplitParam(@RetailLocationID, ',') RL									
			    END
			    IF(@RetailLocationID IS  NULL AND @ProductIDs IS NOT NULL)
			    BEGIN
					SELECT @CouponID CouponID				--Associate the Coupon Retail Locations where the product is available if Retaillocations is not passed
						 , RetailLocationID
					INTO #RetailLocations					
					FROM 
					(SELECT distinct RL.RetailLocationID 
					FROM RetailLocationProduct RLP
					INNER JOIN #Prod P ON RLP.ProductID = P.ProductID
					INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID AND RL.Active = 1
					WHERE RL.RetailID = @RetailID) Retailer
					
					INSERT INTO CouponRetailer (
												CouponID
											  , RetailID 
											  , RetailLocationID
											  , DateCreated	
											)
									SELECT @CouponID
										 , @RetailID
										 , RetailLocationID
										 , GETDATE()
					                FROM #RetailLocations
			    END
			    
			    --Register the Push Notification for all the users who have added the product to their Wish Lists.
					SELECT DISTINCT UserID
						  ,ProductID
						  ,DeviceID
						  ,CouponID  	              
					INTO #Temp
					FROM (
					SELECT  U.UserID
						  ,UP.ProductID
						  ,UD.DeviceID 
						  ,CD.CouponID   
						  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
						  ,LocaleRadius 
					from fn_SplitParam(@ProductIDs,',') F
					INNER JOIN CouponProduct CD ON CD.ProductID =F.Param AND CD.CouponID =@CouponID         
					INNER JOIN CouponRetailer CR ON CR.CouponID =CD.CouponID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =CR.RetailLocationID AND RL.Active = 1 
					INNER JOIN UserProduct UP ON UP.ProductID =CD.ProductID AND WishListItem =1	
					INNER JOIN Users U ON U.UserID =UP.UserID 
					INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
					INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
					)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
					
					--User pushnotification already exist then NotificationSent flag updating to 0
					UPDATE UserPushNotification SET NotificationSent=0
												   ,DateModified =GETDATE()		
												   ,DiscountCreated = 1	                                      
					FROM UserPushNotification UP
					INNER JOIN #Temp T ON UP.CouponID=T.CouponID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=1
			       
		           --User pushnotification not exist then insert. 
				   INSERT INTO UserPushNotification(UserID
													,ProductID
													,DeviceID												
													,CouponID 												
													,NotificationSent
													,DiscountCreated
													,DateCreated)
					SELECT T.UserID 
						  ,T.ProductID 
						  ,T.DeviceID 
						  ,T.CouponID  
						  ,0
						  ,1
						  ,GETDATE()
					FROM #Temp T				
					LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.CouponID =UP.CouponID AND T.ProductID =UP.ProductID AND DiscountCreated=1
					WHERE UP.UserPushNotificationID IS NULL 
			--    SET @PercentageFlag = 1
		 --END	
		
		 --ELSE 
		 --BEGIN
			--SET @PercentageFlag = 0
		 --END
		 		 
			-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
		
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponInsertion.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
