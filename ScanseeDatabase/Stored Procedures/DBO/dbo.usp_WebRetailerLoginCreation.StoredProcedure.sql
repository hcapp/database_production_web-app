USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerLoginCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerLoginCreation
Purpose					: To create a Login to the Existing Retailer(UserName will be Retailer Name & Password will be Scansee01). 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th Sept 2012 Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerLoginCreation]
(

	 @RetailID int
	,@RetailName varchar(100)
	,@UserName varchar(100)
	,@ContactFirstName Varchar(20)
	,@ContactLastName Varchar(30)
	,@ContactPhone Char(10)
	,@ContactEmail Varchar(100)
	
	--Output Variable 
	, @ExistingUserName varchar(100) output
	, @ExistingPassword varchar(100) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @ExistingUserID INT
			DECLARE @UserID INT		
			DECLARE @RetailLocationID INT	
			DECLARE @ContactID INT
		
			SELECT @ExistingUserID = UserID
			FROM UserRetailer 
			WHERE RetailID = @RetailID
			
			--Check if the Retailer already has a Login. If not create a new.
			IF (@ExistingUserID IS NULL)
			BEGIN
				INSERT INTO Users(
								  UserName
								, Password
								, DateCreated)
						VALUES(   
								 @UserName
							   , 'R4NLRVmE5dJvZxJeVtnMVw==' --Scansee01
							   , GETDATE() 
							  )
							  
				SELECT @UserID = SCOPE_IDENTITY()
				
				INSERT INTO UserRetailer(UserID
									   , RetailID)
								SELECT @UserID
							    	  , @RetailID
							    	  
				
				SELECT @RetailLocationID = RetailLocationID
				FROM RetailLocation
				WHERE RetailID = @RetailID
				AND (Headquarters = 1 OR CorporateAndStore = 1)	AND Active = 1				    	  
				
				IF(@RetailLocationID IS NULL)
				BEGIN
					INSERT INTO RetailLocation(RetailID
											 , Headquarters
											 , CountryID
											 , DateCreated)
										SELECT @RetailID
										     , 1
										     , 1
										     , GETDATE()
				END
				
				SET @RetailLocationID = SCOPE_IDENTITY()
				
				INSERT INTO Contact([ContactFirstName]
								   ,[ContactLastname]
								   ,[ContactTitle]					  
								   ,[ContactPhone]
								   ,[ContactEmail]
								   ,[DateCreated]
								   ,[CreateUserID])
							VALUEs(@ContactFirstName
								 , @ContactLastName
								 , 2
								 , @ContactPhone
								 , @ContactEmail
								 , GETDATE()
								 , @UserID)
							
				SELECT @ContactID = SCOPE_IDENTITY()
				
				INSERT INTO RetailContact(RetailLocationID
										, ContactID)
								  VALUES(@RetailLocationID
									   , @ContactID)
								
				
				
			END
			ELSE
			--If the Login for the Retailer already exists return the credentials.
			BEGIN
				SELECT @ExistingUserName = UserName
				     , @ExistingPassword = Password
				FROM Users 
				WHERE UserID = @ExistingUserID
			END
			
			--Confirmation of Success
			SET @Status = 0
			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerLoginCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
