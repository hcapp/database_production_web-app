USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUserFaceBookLogIn]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUserFacebookLogin
Purpose					: For User log in through facebook.
Example					: usp_WebUserFacebookLogin
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0	   16th December 2011	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUserFaceBookLogIn]
(
	  @UserName varchar(100)
	, @DateCreated datetime
	, @FaceBookAuthenticatedUser Bit
	
	--UserLocation Variables.

	--Output Variable 
	, @FirstUserID int output
	, @UserType int output
	, @LandingPage varchar(255) OUTPUT	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			-- Check for existance of the Email Address. 
			--If already found, return -1
			
			    DECLARE @UserID INT
				
				SELECT @UserID = UserID 
				FROM Users 
				WHERE UserName = @UserName
			
			--Check if the User Exists in the System.
			IF @UserID IS NOT NULL
			BEGIN
				SELECT U.FirstName firstName
				     , U.Lastname lastName
				     , U.MobilePhone				  
				     , U.UserID userID
				     , U.Address1 'Address'
				     , U.Address2
				     , U.[State]
				     , U.City
				     , U.PostalCode				     
				FROM Users U
				WHERE BINARY_CHECKSUM(U.UserName) = BINARY_CHECKSUM(@UserName) AND FaceBookAuthenticatedUser = @FaceBookAuthenticatedUser				
				
				--Check if the user has preferences set.
				IF EXISTS(SELECT 1 FROM UserCategory WHERE UserID = @UserID)
				BEGIN
					IF EXISTS(SELECT 1 FROM UserUniversity WHERE UserID = @UserID)
					BEGIN
						SET @LandingPage = 'SHOPPERDASHBOARD'
					END
					ELSE
					BEGIN
						SET @LandingPage = 'NOSHOPPERSELECTSCHOOL'
					END				
				END
				ELSE
				BEGIN
					SET @LandingPage = 'NOSHOPPERPREFERENCES'
				END
				
				SET @UserType = 1
				SET @Status = 0
			END
			
			--If not found, insert into Users table. ie,allow user to register.
			IF ISNULL(@UserID, '') = ''			
			BEGIN
				INSERT INTO Users 
					(
					  UserName					
					 ,FirstUseComplete
					 ,FieldAgent				
					 ,DateCreated
					 ,FaceBookAuthenticatedUser
					)
				VALUES
					(
					  @UserName 				
					 ,0
					 ,0			
					 ,@DateCreated
					 ,@FaceBookAuthenticatedUser 
					)
				
				SET @UserID = SCOPE_IDENTITY()				
				
			-- To capture User First Use Login.
			INSERT INTO [UserFirstUseLogin]
				   ([UserID]
				   ,[FirstUseLoginDate])
			 VALUES
				   (@UserID 
				   ,GETDATE())
				   
            SELECT U.FirstName firstName
				     , U.Lastname lastName
				     , U.MobilePhone				  
				     , U.UserID userID
				     , U.Address1 'Address'
				     , U.Address2
				     , U.[State]
				     , U.City
				     , U.PostalCode				     
				FROM Users U
				WHERE UserID = @UserID
								   				   
			--To Pass generated UserID.
			SELECT @FirstUserID = @UserID
				 , @UserType = 1
				 , @LandingPage = 'NOSHOPPERPREFERENCES'
				 
			END	 
			
			--Confirmation of Success.
			SELECT @Status = 0		
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUserFacebookLogin.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
			
		END;
		 
	END CATCH;
END;


GO
