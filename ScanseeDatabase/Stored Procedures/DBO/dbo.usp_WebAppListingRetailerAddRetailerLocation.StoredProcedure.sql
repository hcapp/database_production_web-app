USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAppListingRetailerAddRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAppListingRetailerAddRetailerLocation
Purpose					: 
Example					: usp_WebAppListingRetailerAddRetailerLocation

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			06th January 2013	SPAN		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAppListingRetailerAddRetailerLocation]
(
	 @RetailID int
	,@StoreIdentification varchar(100)
	,@StoreAddress varchar(150)
	,@State Char(2)
	,@City varchar(50)
	,@PostalCode varchar(10)
	,@Phone Char(10)
	,@RetailLocationURL VARCHAR(1000)
	,@RetailLocationLatitude float
	,@RetailLocationLongitude float
	,@RetailLocationKeyword Varchar(1000)
	--Output Variable 
	,@ResponseRetailLocationID int output
	,@StoreExists int output
	,@Status int output
	,@ErrorNumber int output
	,@ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			IF Not Exists(SELECT 1 FROM RetailLocation Where StoreIdentification=@StoreIdentification AND RetailID = @RetailID AND Active = 1)
			BEGIN
				SET @StoreExists=0
				INSERT INTO [RetailLocation]
						   ([RetailID]
						   ,[Headquarters]
						   ,[Address1]
						   ,[City]
						   ,[State]
						   ,[PostalCode]
						   , RetailLocationLatitude
						   , RetailLocationLongitude
						   ,[CountryID]
						   , RetailLocationURL
						   ,[DateCreated]
						   ,[DateModified]
						   ,[StoreIdentification])
				 VALUES(@RetailID 
						,0
						,@StoreAddress 
						,@City
						,@State
						,@PostalCode
						,@RetailLocationLatitude 
						,@RetailLocationLongitude
						,1
						,@RetailLocationURL
						,GETDATE()
						,GETDATE()
						,@StoreIdentification)
				 
				 DECLARE @RetailLocationID int
				 SET @RetailLocationID=SCOPE_IDENTITY()
				 
				 --Contact
				 DECLARE @CreateUserID int
				 SELECT @CreateUserID=UserID  
				 FROM UserRetailer
				 WHERE RetailID=@RetailID
				 
				 INSERT INTO [Contact]
						   ([ContactPhone]						   
						   ,[DateCreated]
						   ,[CreateUserID]
						   ,[DateModified])
					 VALUES (@Phone
							,GETDATE()
							,@CreateUserID
							,GETDATE())
							
				DECLARE @ContactID int
				SET @ContactID=SCOPE_IDENTITY()	
				 
				 --RetailContact
				 INSERT INTO [RetailContact]
						   ([ContactID]
						   ,[RetailLocationID]
						   )
				   VALUES(@ContactID
						  ,@RetailLocationID
						)
						
			  --INSERT INTO RetailerKeywords Table
			  IF(@RetailLocationKeyword IS NOT NULL)
			  BEGIN
			  INSERT INTO RetailerKeywords
			              (RetailID
			              ,RetailLocationID 
			              ,RetailKeyword 
			              ,DateCreated)
			  VALUES(@RetailID 
			         ,@RetailLocationID 
			         ,@RetailLocationKeyword
			         ,GETDATE())		                 			  
			  END
			END
			
			ELSE
			BEGIN
				SET @StoreExists=1
			END
			
		--Confirmation of Success.
			SELECT @Status = 0
			SET @ResponseRetailLocationID = @RetailLocationID 
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebAppListingRetailerAddRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
