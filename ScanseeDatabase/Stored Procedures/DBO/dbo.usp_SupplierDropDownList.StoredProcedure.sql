USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierDropDownList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_SupplierDropDownList]
Purpose					: [usp_SupplierDropDownList]
Example					: [usp_SupplierDropDownList]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			08th April 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierDropDownList]
AS
BEGIN

	BEGIN TRY
			
		SELECT 1 ID, 'Top 15 Products' Name
		UNION ALL
		--SELECT 2, 'Ad Spend Overview'
		--UNION ALL
		SELECT 2, 'Coupons & Deals'
		UNION ALL
		SELECT 3, 'This Location'
		UNION ALL
		SELECT 4, 'Scanned/Searched Products'
		UNION ALL
		SELECT 5, 'Shopping List'
		UNION ALL
		SELECT 6, 'Wish List'

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_SupplierDropDownList].'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;

GO
