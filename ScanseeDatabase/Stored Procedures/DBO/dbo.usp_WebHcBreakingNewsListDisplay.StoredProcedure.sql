USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHcBreakingNewsListDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebHcBreakingNewsListDisplay]
Purpose					: To Display List of news.
Example					: [usp_WebHcBreakingNewsListDisplay]


History
Version		Date			Author	              Change Description
--------------------------------------------------------------- 
1.0			09/04/2015	    SAGAR BYALI                1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHcBreakingNewsListDisplay]
(
    --Input variable	
	  @HcNewsType VARCHAR(100)	
	
	--Output Variable	
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
	
		--Default image 
		DECLARE @DefaultImage varchar(500)    
		
		SELECT @DefaultImage = ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType = 'WebRssFeedDefaultImage'   		
			
		-- To display Trending News
		IF @HcNewsType = 'Trending'
			
			SELECT DISTINCT TOP 3 HcNewsID Id
								 ,T.NewsType
								 ,Title
								 ,ImagePath
								 ,Description description
								 ,URL Link
								 ,PublishedDate
								 ,PublishedDate PubStrtDate							 		  
			FROM HcNewsStaging N
			INNER JOIN HcNewsType T ON T.HcNewsTypeID=N.HcNewsTypeID AND T.NewsType IN ('Trending')
			WHERE T.Newstype = @HcNewsType

		-- To display Breaking News for 'Taylor' and 'Tyler'
		IF @HcNewsType = 'tyler test Breaking News' OR @HcNewsType = 'Breaking News'
			 
			 WITH CTE AS(
						SELECT DISTINCT  HcNewsID Id
										,T.NewsType
										,Title
										,ImagePath = CASE WHEN ImagePath = 'NULL' THEN NULL ELSE ImagePath END --CASE WHEN T.NewsType = 'Taylor Breaking News' THEN ImagePath ELSE ISNULL(ImagePath,@DefaultImage) END 
										,Description description
										,URL Link
										,PublishedDate
										,PublishedDate PubStrtDate					 		  
						FROM HcNews N 
						INNER JOIN HcNewsType T ON T.HcNewsTypeID=N.HcNewsTypeID AND T.NewsType IN ('tyler test Breaking News','Breaking News')
						WHERE T.Newstype = @HcNewsType
						AND DATEDIFF(hour, N.DateCreated, GETDATE()) <= 24
						) SELECT * 
						  FROM CTE
						  ORDER BY    CAST(SUBSTRING(publisheddate, 6, 11) AS DATE) DESc 


		--Confirmation of Success
		SELECT @Status = 0
		
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebHcBreakingNewsListDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
