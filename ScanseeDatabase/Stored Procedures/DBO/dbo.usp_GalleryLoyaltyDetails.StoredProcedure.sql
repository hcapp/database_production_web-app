USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryLoyaltyDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GalleryLoyaltyDetails]
Purpose					: To display the details of loyalty deals related to particular user in user loyalty gallery
Example					: usp_GalleryLoyalty

History
Version		Date			     Author			 Change Description
--------------------------------------------------------------- 
1.0			15th Oct 2011	 SPAN Infotech India	 Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryLoyaltyDetails]
(
	  @Userid int
	, @LoyaltyDealID int
	--Output Variable 
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			SELECT	 ISNULL(ProductName, '')  ProductName
					, ISNULL(L.ProductID,0) ProductID
			        , LoyaltyDealID
					, LoyaltyDealName
					, LoyaltyDealDescription loyaltyDealDescription
					, LoyaltyDealStartDate
					, LoyaltyDealExpireDate
					--, UsedFlag=(SELECT CASE WHEN L.LoyaltyDealID=UL.LoyaltyDealID then 1 ELSE 0 END FROM UserLoyaltyGallery UL WHERE UserID=@Userid AND LoyaltyDealID=@LoyaltyDealID)
			FROM LoyaltyDeal L
				LEFT JOIN Product P ON P.ProductID=L.ProductID
			WHERE  L.LoyaltyDealID=@LoyaltyDealID
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GalleryLoyaltyDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
