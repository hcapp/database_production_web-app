USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[HcxRetailLocationBatchProcess]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[HcxRetailLocationBatchProcess]
AS
BEGIN

SELECT DISTINCT  RL.RetailID ,RL.RetailLocationID
			,Category = STUFF((SELECT ',' + CAST(RBC.BusinessCategoryID AS Varchar(Max))
								FROM RetailerBusinessCategory RBC
								--INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID
								WHERE RBC.BusinessSubCategoryID IS NULL
								AND RBC.Retailerid = RL.RetailID
								FOR XML PATH ('')
								),1,1,'')
			,SubCategory = STUFF((SELECT ','+ (CAST(RSC.BusinessCategoryID AS varchar(Max)) +'/'+ CAST(RSC.HcBusinessSubCategoryID AS varchar(Max)))
								FROM HcRetailerSubCategory RSC
								--INNER JOIN BusinessCategory BC ON RSC.BusinessCategoryID = BC.BusinessCategoryID
								--INNER JOIN HcBusinessSubCategory BSC ON RSC.HcBusinessSubCategoryID = BSC.HcBusinessSubCategoryID
								WHERE RSC.RetailLocationID = RL.RetailLocationID
								FOR XML PATH ('')),1,1,'')
INTO #Categories
FROM Retailer R
INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND R.RetailerActive = 1 ANd Rl.Active = 1
LEFT JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
LEFT JOIN Contact C ON RC.ContactID = C.ContactID
INNER JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.Associated = 1
WHERE RA.HcHubCitiID IN (49,19)

SELECT DISTINCT top 10 RL.RetailID accountId
			,RL.RetailLocationID locationId
			,RL.Address1 address1
			,RL.Address2 address2
			,RL.Address3
			,RL.Address4
			,RL.City
			,RL.State
			,RL.PostalCode postalCode
			,RL.CountryID
			,'Unspecified' openInfo
			,phone = REPLACE(c.ContactPhone+','+ISNULL(c.ContactMobilePhone,'00'),',00','')
			,locationType = CASE WHEN Headquarters = 1 THEN 'HQ' WHEN CorporateAndStore = 1 THEN 'HQ_AND_SERVICE' 
							WHEN Headquarters = 0 and CorporateAndStore = 0 THEN 'SERVICE' END
			,RetailLocationURL websiteUrl
			,RetailLocationImagePath retImage
			,RetailLocationLatitude latitude
			,RetailLocationLongitude longitude
			,RL.StartTimeUTC openTime
			,RL.EndTimeUTC closeTime
			,StoreIdentification storeCode
			,Categories = REVERSE(STUFF(REVERSE(CA.Category+','+REPLACE(ISNULL(CA.SubCategory,'00'),'00','')),1,1,''))
FROM Retailer R 
INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND R.RetailerActive = 1 ANd Rl.Active = 1  
LEFT JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
LEFT JOIN Contact C ON RC.ContactID = C.ContactID
INNER JOIN HcRetailerAssociation RA ON RL.RetailLocationID = RA.RetailLocationID AND RA.Associated = 1
INNER JOIN #Categories CA ON CA.RetailID = RL.RetailID AND CA.RetailLocationID = RL.RetailLocationID
INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = RL.RetailID
LEFT JOIN HcRetailerSubCategory RSC ON RSC.RetailLocationID = RL.RetailLocationID
INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID
LEFT JOIN HcBusinessSubCategoryType ST ON BC.BusinessCategoryID = ST.BusinessCategoryID
LEFT JOIN HcBusinessSubCategory SC ON ST.HcBusinessSubCategoryTypeID = SC.HcBusinessSubCategoryTypeID
WHERE RA.HcHubCitiID IN (49,19)
ORDER BY RL.RetailID


END

GO
