USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserPostalcodeCheck]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserPostalcodeCheck
Purpose					: To Check for the existance of the user postal code.
Example					: usp_UserPostalcodeCheck

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10th May 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserPostalcodeCheck]
(
	  @UserID int 
	
	--Output Variables
	, @PostalCodeExists bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			 
			 DECLARE @PostalCode VARCHAR(20)
			 SELECT @PostalCode = PostalCode FROM Users WHERE UserID = @UserID
			 
			 IF @PostalCode IS NULL OR @PostalCode = ''
			 BEGIN
				SET @PostalCodeExists = 0
			 END
			 
			 ELSE
			 BEGIN
				SET @PostalCodeExists = 1
			 END				
		
		--Confirmation of Success.
		SET @Status = 0		
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure WebRetailerAdsInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
