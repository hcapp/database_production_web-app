USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerFindRetailerListPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   :  usp_WebConsumerFindRetailerListPagination
Purpose                 :  To get the list of near by retailer for Find Module.
Example                 :  usp_WebConsumerFindRetailerListPagination

History
Version      Date           Author          Change Description
------------------------------------------------------------------------------- 
1.0          20/5/2013		Dhananjaya TR	Initial Version
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[usp_WebConsumerFindRetailerListPagination]
(

      --Input Parameter(s)-- 
      
      @UserID int  
    , @ApiPartnerName Varchar(50)
	, @CategoryName varchar(100)
	, @Latitude float
	, @Longitude float
	, @Radius int
	, @LowerLimit int  
	, @ScreenName varchar(50)
	
	--User tracking Inputs
	, @MainMenuID int
	
      --Output Variable--                 
    , @MaxCnt int  output
	, @NxtPageFlag bit output  
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
                
                DECLARE @CategoryID INT      
                        
				--To get Find Category associated Business Category
				SELECT DISTINCT BusinessCategoryID
				INTO #BusinessCategory
				FROM  FindSourceCategory F
				INNER JOIN APIPartner A ON A.APIPartnerID = F.APIPartnerID
				WHERE ActiveFlag = 1 And APIPartnerName = @ApiPartnerName
				AND CategoryDisplayName = @CategoryName
				
				SELECT @CategoryID = BusinessCategoryID
				FROM #BusinessCategory

				DECLARE @Config varchar(50)
				SELECT @Config=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='App Media Server Configuration'

				DECLARE @RetailConfig varchar(50)
				SELECT @RetailConfig=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='Web Retailer Media Server Configuration'
						
				--To get the row count for pagination.  
				DECLARE @UpperLimit int   
				SELECT @UpperLimit = @LowerLimit + ScreenContent   
				FROM AppConfiguration   
				WHERE ScreenName = @ScreenName 
				AND ConfigurationType = 'Pagination'
				AND Active = 1   
				
				--Select the radius that the User has configured.
				SELECT @Radius = LocaleRadius
				FROM UserPreference 
				WHERE UserID = @UserID
				
				--If the radius is still null then retrieve the global radius.	
				IF @Radius IS NULL
				BEGIN			
					SELECT @Radius = ScreenContent
					FROM AppConfiguration 
					WHERE ConfigurationType = 'WishList Default Radius'
				END

				-- To identify Retailer that have products on Sale or any type of discount
				select distinct Retailid , RetailLocationid
				into #RetailItemsonSale
				from 
				(select b.RetailID, a.RetailLocationID 
				from RetailLocationDeal a 
				inner join RetailLocation b on a.RetailLocationID = b.RetailLocationID 
				inner join RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
												   and a.ProductID = c.ProductID 
												    and GETDATE() between isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)
				union 
				select  CR.RetailID, RetailLocationID  as RetaillocationID 
				from Coupon C 
				INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
				where GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
				union 
				select  RR.RetailID,  RetaillocationID  
				from Rebate R 
				inner join RebateRetailer RR ON R.RebateID=RR.RebateID
				where GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
				union 
				select  c.retailid, a.RetailLocationID 
				from  LoyaltyDeal a
				INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
				inner join RetailLocation c on a.RetailLocationID = c.RetailLocationID
				inner join RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
											and a.ProductID = LDP.ProductID 
				 
				Where GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
				union
				select distinct rl.RetailID, rl.RetailLocationID
				from ProductHotDeal p
				inner join ProductHotDealRetailLocation pr on pr.ProductHotDealID = p.ProductHotDealID 
				inner join RetailLocation rl on rl.RetailLocationID = pr.RetailLocationID
				left join HotDealProduct hp on hp.ProductHotDealID = p.ProductHotDealID 
				WHERE GETDATE() between HotDealStartDate and HotDealEndDate 
				
				union
				
				select q.RetailID, qa.RetailLocationID
				from QRRetailerCustomPage q
				inner join QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
				inner join QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
				where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE()+1)) a
	 
	   
	     
			 --  --If Coordinates exists fetching retailer list in the specified radius.    
			   IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)    
			   BEGIN    			   
				
				--CREATE TABLE #Retail(Row_Num INT IDENTITY(1,1)
				--					, RetailID INT 
				--					, RetailName VARCHAR(255)
				--					, RetailLocationID INT
				--					, Address1 VARCHAR(500)
				--					, Address2 VARCHAR(255)
				--					, Address3 VARCHAR(255)
				--					, Address4 VARCHAR(255)
				--					, City VARCHAR(100)
				--					, State CHAR(20)
				--					, PostalCode CHAR(10)
				--					, retLatitude FLOAT
				--					, retLongitude FLOAT 
				--					, RetailerImagePath VARCHAR(1000)
				--					, Distance FLOAT
				--					, BannerAdImagePath VARCHAR(1000)   
				--					, RibbonAdImagePath VARCHAR(1000)
				--					, RibbonAdURL VARCHAR(1000)
				--					, RetailLocationAdvertisementID  INT
				--					, SplashAdID INT
				--					, SaleFlag  BIT)
					
			 --     --Select all the Retailers in the vicinity of the User.	
				--  INSERT INTO #Retail(RetailID    
				--					, RetailName
				--					, RetailLocationID
				--					, Address1
				--					, Address2
				--					, Address3
				--					, Address4
				--					, City    
				--					, State
				--					, PostalCode
				--					, retLatitude
				--					, retLongitude
				--					, RetailerImagePath    
				--					, Distance 								
				--					, SaleFlag)
									
				--	           SELECT RetailID    
				--					, RetailName
				--					, RetailLocationID
				--					, Address1
				--					, Address2
				--					, Address3
				--					, Address4
				--					, City    
				--					, State
				--					, PostalCode
				--					, retLatitude
				--					, retLongitude
				--					, RetailerImagePath    
				--					, Distance 								
				--					, SaleFlag
				--	    FROM 
				--	   (SELECT DISTINCT R.RetailID     
				--					 , R.RetailName
				--					 , RL.RetailLocationID 
				--					 , RL.Address1     
				--					 , RL.Address2
				--					 , RL.Address3
				--					 , RL.Address4
				--					 , RL.City
				--					 , RL.State
				--					 , RL.PostalCode
				--					 , RL.RetailLocationLatitude retLatitude
				--					 , RL.RetailLocationLongitude retLongitude
				--					 , RetailerImagePath = CASE WHEN RetailerImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath ELSE @Config+RetailerImagePath END  
				--											ELSE RetailerImagePath 
				--										   END  
				--					 , Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)   			
				--					 --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
				--					 , SaleFlag = CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
				--		    FROM Retailer R    
				--			INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID 
				--			INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
				--			INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID    
				--			-----------to get retailer that have products on sale
				--			LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID) Retailer
				--			WHERE Distance <= ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'DefaultRadius' AND ScreenName = 'DefaultRadius'))
				--			ORDER BY Distance, RetailName ASC
				
				--	 --Update the Banner Ads(Before/ Welcome page/ Splash Ad Detail 
				--	 UPDATE #Retail SET  BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
				--													ELSE SplashAdImagePath
				--												  END
				--					   , SplashAdID  = ASP.AdvertisementSplashID
				--	 FROM #Retail R
				--	 INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
				--	 INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
				--	 WHERE GETDATE() BETWEEN ASP.StartDate AND ASP.EndDate
					 
				--	 --Update the Banner Ads/ Ribbon Ads(Before) detail
				--	 UPDATE #Retail SET RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
				--												ELSE AB.BannerAdImagePath
				--											END 
				--					  , RibbonAdURL = AB.BannerAdURL
				--					  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
				--	 FROM #Retail R
				--	 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
				--	 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
				--	 WHERE GETDATE() BETWEEN AB.StartDate AND AB.EndDate	
				
				
				-- --To capture max row number.  
				-- SELECT @MaxCnt = MAX(Row_Num) FROM #Retail
				 
				-- --this flag is a indicator to enable "More" button in the UI.   
				-- --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				-- SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
				  
				-- SELECT DISTINCT Row_Num rowNumber  
				--	, RetailID retailerId    
				--	, RetailName retailerName
				--	, RetailLocationID
				--	, Address1 retaileraddress1
				--	, Address2 retaileraddress2
				--	, Address3 retaileraddress3
				--	, Address4 retaileraddress4
				--	, City City
				--	, State State
				--	, PostalCode PostalCode  
				--	, retLatitude 
				--	, retLongitude 
				--	, RetailerImagePath logoImagePath     
				--	, Distance      
				--	, BannerAdImagePath bannerAdImagePath    
				--	, RibbonAdImagePath ribbonAdImagePath    
				--	, RibbonAdURL ribbonAdURL    
				--	, RetailLocationAdvertisementID advertisementID    
				--	, SplashAdID splashAdID
				--	, SaleFlag   
				-- FROM #Retail  
				-- WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit 
				
				SELECT Row_Num = IDENTITY(INT,1,1)
									, RetailID    
									, RetailName
									, RetailLocationID
									, Address1
									, Address2
									, Address3
									, Address4
									, City    
									, State
									, PostalCode
									, retLatitude
									, retLongitude
									, RetailerImagePath    
									, Distance 								
									, SaleFlag
							INTO #Retail
					    FROM 
					   (SELECT DISTINCT R.RetailID     
									 , R.RetailName
									 , RL.RetailLocationID 
									 , RL.Address1     
									 , RL.Address2
									 , RL.Address3
									 , RL.Address4
									 , RL.City
									 , RL.State
									 , RL.PostalCode
									 , RL.RetailLocationLatitude  retLatitude
									 , RL.RetailLocationLongitude retLongitude
									 , RetailerImagePath = CASE WHEN RetailerImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath ELSE @Config+RetailerImagePath END  
															ELSE RetailerImagePath 
														   END  
									 , Distance = ROUND((ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214 ,1,1)   			
									 --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
									 , SaleFlag = CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
					 FROM Retailer R 
					 INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID  
					 INNER JOIN #BusinessCategory BC ON BC.BusinessCategoryID = RBC.BusinessCategoryID    
					 INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID 
					 -----------to get retailer that have products on sale
					 LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
					 WHERE Headquarters = 0) Retailer
					 WHERE Distance <= @Radius
					 ORDER BY Distance, RetailName ASC
					 
					 --To capture max row number.  
					 SELECT @MaxCnt = MAX(Row_Num) FROM #Retail
					 
					 --this flag is a indicator to enable "More" button in the UI.   
					 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
					 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
					  
					 SELECT Row_Num rowNumber  
						, RetailID retailerId    
						, RetailName retailerName
						, R.RetailLocationID retailLocationID
						, Address1 retaileraddress1
						, Address2 retaileraddress2
						, Address3 retaileraddress3
						, Address4 retaileraddress4
						, City City
						, State State
						, PostalCode PostalCode
						, retLatitude
						, retLongitude    
						, RetailerImagePath logoImagePath     
						, Distance      
						, S.BannerAdImagePath bannerAdImagePath    
						, B.RibbonAdImagePath ribbonAdImagePath    
						, B.RibbonAdURL ribbonAdURL    
						, B.RetailLocationAdvertisementID advertisementID  
						, S.SplashAdID splashAdID
						, SaleFlag   
					 INTO #RetailerList    
					 FROM #Retail R
					 LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
																	ELSE SplashAdImagePath
																  END
									   , SplashAdID  = ASP.AdvertisementSplashID
									   , R.RetailLocationID 
								FROM #Retail R
									INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
									INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
											AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ASP.EndDate) S
							ON R.RetailLocationID = S.RetailLocationID
					 LEFT JOIN (SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
																ELSE AB.BannerAdImagePath
															END 
									  , RibbonAdURL = AB.BannerAdURL
									  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
									  , R.RetailLocationID 
								 FROM #Retail R
										 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
										 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
								 WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND AB.EndDate) B
							ON R.RetailLocationID = B.RetailLocationID
					 WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit
					 
					 --User Tracking section.
					 
					  --Table to track the Retailer List.
					 CREATE TABLE #Temp(RetailerListID int
                                        ,LocationDetailID int
                                        ,MainMenuID int
                                        ,RetailID int
                                        ,RetailLocationID int)  
                      --Capture the impressions of the Retailer list.
                      INSERT INTO ScanSeeReportingDatabase..RetailerList(MainMenuID
																	, RetailID
																	, RetailLocationID
																	, FindCategoryID
																	, CreatedDate)
                     
                      OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)							
													SELECT @MainMenuId
														 , retailerId
														 , RetailLocationID
														 , @CategoryID
														 , GETDATE()
													FROM #RetailerList
					--Display the Retailer along with the keys generated in the Tracking table.
					SELECT rowNumber
						 , T.RetailerListID retListID
						 , retailerId
						 , retailerName
						 , T.RetailLocationID
						 , retaileraddress1
						 , retaileraddress2
						 , retaileraddress3
						 , retaileraddress4
						 , City
						 , State
						 , PostalCode
						 , Distance 
						 , logoImagePath
						 , bannerAdImagePath
						 , ribbonAdImagePath
						 , ribbonAdURL
						 , advertisementID
						 , splashAdID
						 , SaleFlag						 
					FROM #Temp T
					INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID
					 
					 
					 
            END
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebConsumerFindRetailerListPagination.'         
                  --Execute retrieval of Error info.
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
            END;
            
      END CATCH;
END;


GO
