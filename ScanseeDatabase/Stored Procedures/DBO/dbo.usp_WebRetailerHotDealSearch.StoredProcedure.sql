USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerHotDealSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  
/*  
Stored Procedure name : usp_WebRetailerHotDealSearch  
Purpose     : To display the details of the selected HotDeal.  
Example     :   
  
History  
Version  Date       Author   Change Description  
-------------------------------------------------------------------------------   
1.0   30th December 2011    Pavan Sharma K Initial Version  
-------------------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebRetailerHotDealSearch]  
(  
  
 --Input Input Parameter(s)--  
   
    @RetailerID int  
  , @ProductHotDealID int   
  , @LowerLimit int
  , @RecordCount int
	
  --Output Variable--	  
	
  , @RowCount int output
  , @NextPageFlag bit output 
  , @ErrorNumber int output  
  , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
   
        DECLARE @RowCnt INT
	    DECLARE @MaxCnt INT
	    DECLARE @UpperLimit INT
	   
	   
	    --To get Media Server Configuration.  
	    DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		--To get the row count for pagination.  
		 --DECLARE @ScreenContent Varchar(100)
		 --SELECT @ScreenContent = ScreenContent   
		 --FROM AppConfiguration   
		 --WHERE ScreenName = 'All' 
			--AND ConfigurationType = 'Website Pagination'
			--AND Active = 1  
	   
	    SELECT @UpperLimit = @LowerLimit + @RecordCount
	    
					     SELECT RowNum = ROW_NUMBER() OVER (ORDER BY hotDealName ASC) 
								, hotDealID
								, hotDealName
								, hotDealLongDesc
								, HotDealImagePath 
								, dealStartDate
								, dealEndDate
								, hotDealDiscountAmt 
								, price
								, salePrice      
								, hotDealDiscountPct
								, hotDealTermsCondt 
								, url
								, CreatedDate
								, CategoryID 
								, ParentCategoryName 
								, SubCategoryName
						INTO #Temp
						FROM 	
						(SELECT TOP 100 PERCENT PHD.ProductHotDealID as hotDealID
							   , PHD.HotDealName  as hotDealName
							   , PHD.HotDeaLonglDescription as hotDealLongDesc
							   , HotDealImagePath = CASE WHEN PHD.HotDealImagePath IS NOT NULL THEN 
												CASE WHEN WebsiteSourceFlag = 1 THEN 
													CASE WHEN PHD.ManufacturerID IS NOT NULL THEN @ManufacturerConfig + CONVERT(VARCHAR(30),PHD.ManufacturerID) +'/' + HotDealImagePath
																							 ELSE @RetailerConfig + CONVERT(VARCHAR(30),PHD.RetailID) +'/' + HotDealImagePath
													   END

 													ELSE PHD.HotDealImagePath
												END
											END 
							   , CONVERT(VARCHAR(10),PHD.HotDealStartDate,105) + ' ' + CONVERT(VARCHAR(10),PHD.HotDealStartDate,108) as dealStartDate
							   , CONVERT(VARCHAR(10),PHD.HotDealEndDate,105) + ' ' + CONVERT(VARCHAR(10),PHD.HotDealEndDate,108) as dealEndDate
							   , PHD.HotDealDiscountAmount as hotDealDiscountAmt 
							   , PHD.Price as price
							   , PHD.SalePrice   as salePrice      
							   , PHD.HotDealDiscountPct  as  hotDealDiscountPct
							   , PHD.HotDealTermsConditions as hotDealTermsCondt 
							   , PHD.HotDealURL as url 
							   , PHD.CreatedDate  
							   , PHD.CategoryID 
							   , C.ParentCategoryName 
							   , C.SubCategoryName 
						  FROM ProductHotDeal PHD
						  LEFT JOIN Category C ON C.CategoryID = PHD.CategoryID      	   
						  --WHERE PHD.HotDealName LIKE (CASE WHEN @SearchParameter IS NULL THEN '%' ELSE '%'+ @SearchParameter +'%' END) --OR P.ProductName LIKE '%'+ @SearchParameter +'%'
						  WHERE PHD.RetailID = @RetailerID
						  AND ((ISNULL(@ProductHotDealID, 0) = 0)
								OR
								(@ProductHotDealID > 0 AND PHD.ProductHotDealID = @ProductHotDealID)
							   )						  
						  ORDER BY PHD.CreatedDate DESC, PHD.HotDealName ASC)HotDeals	
		
		  
		SELECT @MaxCnt = MAX(RowNum) FROM #Temp

		SET @RowCount = @MaxCnt

		SELECT @NextPageFlag = (CASE WHEN (@MaxCnt - @UpperLimit)>0 THEN 1 ELSE 0 END)

		SELECT   RowNum 
			, hotDealID
			, hotDealName
			, hotDealLongDesc
			, HotDealImagePath 
			, dealStartDate
			, dealEndDate
			, hotDealDiscountAmt 
			, price
			, salePrice      
			, hotDealDiscountPct
			, hotDealTermsCondt 
			, url
			, CreatedDate
			, CategoryID 
			, ParentCategoryName 
			, SubCategoryName
		FROM #Temp
		WHERE RowNum BETWEEN (@LowerLimit +1) AND @UpperLimit   
 	       
    
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebRetailerHotDealSearch.'    
  -- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output      
  END;  
     
 END CATCH;  
END;




GO
