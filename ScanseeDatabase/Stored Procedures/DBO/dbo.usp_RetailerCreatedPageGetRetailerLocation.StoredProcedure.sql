USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerCreatedPageGetRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_RetailerCreatedPageGetRetailerLocation
Purpose					: To get the Nearest Retailer Location to which the Page is associated from the current location when the Retailer CreatedPage is directly Scanned.
Example					: usp_RetailerCreatedPageGetRetailerLocation 1, 'o', 0,50

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21 May 2012	    SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerCreatedPageGetRetailerLocation]
(
	  @UserID int
	, @RetailerID int
	, @PageID int
	, @Latitude float
	, @Longitude float
	
	--OutPut Variable	 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	
	BEGIN TRY
	
		SELECT QRRetailerCustomPageID
			 , RetailID
			 , RetailLocationID
			 , Distance
		INTO #RetailLocations
		FROM(
				SELECT QRRCPA.QRRetailerCustomPageID
					 , QRRCPA.RetailID
					 , QRRCPA.RetailLocationID
					 , Distance = ((ACOS((SIN(ISNULL(RL.RetailLocationLatitude, (SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, (SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, (SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) / 57.2958))))*6371) * 0.6214)
				FROM QRRetailerCustomPage QRRCP
				INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QRRCP.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = QRRCPA.RetailLocationID
				WHERE QRRCPA.QRRetailerCustomPageID = @PageID
				AND QRRCPA.RetailID = @RetailerID)RetailLocations
		 WHERE Distance<=5
				
		 SELECT TOP 1 QRRetailerCustomPageID
			 , RetailID
			 , RetailLocationID
			 , Distance
		 FROM #RetailLocations
		 WHERE Distance = (SELECT MIN(Distance) FROM #RetailLocations)
		
	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_RetailerCreatedPageGetRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
