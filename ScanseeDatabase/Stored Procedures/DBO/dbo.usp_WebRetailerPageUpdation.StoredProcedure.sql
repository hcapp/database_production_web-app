USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerPageUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerPageUpdation
Purpose					: To Update the Retailer Custom Page. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerPageUpdation]
(

	  @UserID int
	, @RetailID int
    , @RetailLocationID varchar(1000)
	, @PageID int
	, @Title varchar(255)
	, @Image varchar(100)
	, @PageDescription varchar(1000)
	, @ShortDescription varchar(255)
	, @LongDescription varchar(1000) 
	
	--Output Variable 	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
			
		    --Modify the Custom Page Details.
			UPDATE QRRetailerCustomPage
			SET Pagetitle = @Title
			  , [Image] = @Image
			  , PageDescription = @PageDescription
			  , ShortDescription = @ShortDescription
			  , LongDescription = @LongDescription
			 WHERE RetailID = @RetailID
			 AND QRRetailerCustomPageID = @PageID
			 
			 --Update the RetailLocation Association
			 DELETE FROM QRRetailerCustomPageAssociation 
			 WHERE QRRetailerCustomPageID = @PageID
			 			 
			 INSERT INTO QRRetailerCustomPageAssociation(
															  QRRetailerCustomPageID
															, RetailID
															, RetailLocationID
															, DateCreated)
												SELECT @PageID
													 , @RetailID
													 , [Param]
													 , GETDATE()
												FROM dbo.fn_SplitParam(@RetailLocationID, ',')
												
															
			 
			 --Confirmation of Success.
			 SET @Status = 0		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerPageUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

--SELECT * FROM UserRetailer U INNER JOIN Users US ON U.UserID = US.UserID




GO
