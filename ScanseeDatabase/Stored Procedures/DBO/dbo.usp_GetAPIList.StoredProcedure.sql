USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAPIList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_GetAPIList
Purpose					: Retrieve List of APIs associated to a Submodule
Example					: EXEC usp_GetAPIList 'findnearby'  

History
Version		Date			Author	   Change Description
--------------------------------------------------------------- 
1.0			28th July 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GetAPIList]
(
	@prSubModule	Varchar(60)
	
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @SubModule Varchar(60)
	
		-- Assign parameter value to a local variable
		SET @SubModule = @prSubModule 

		---- Retrive basic information of desired API 
		--DECLARE @API TABLE (APIPartnerID INT)
		--INSERT INTO @API 
		----To get API Partner assigned for this module.
		--SELECT DISTINCT APIPartner.APIPartnerID
		--FROM APISubModule 
		--	INNER JOIN APISubModuleDetail On  APISubModule.APISubModuleID = APISubModuleDetail.APISubModuleID
		--	INNER JOIN APIMap On APIMap.APIMapID = APISubModuleDetail.APIMapID
		--	INNER JOIN APIUsage On APIMap.APIUsageID = APIUsage.APIUsageID
		--	INNER JOIN APIPartner On APIPartner.APIPartnerID = APIUsage.APIPartnerID
		--WHERE APISubModuleName = @prSubModule
		--	And APIUsageActive = 1
		--	And ActivePartner =1
		----To get all levels of URL for the API Partner.				
		--SELECT APIURLPath
		--	, APIUsagePriority
		--	, APIKey
		--	, APIUsageID
		--	, AP.APIPartnerID
		--	, APIPartnerName as VendorName
		--	, AU.ExecutionOrder as excecutionOrder
		--FROM APIPartner AP
		--INNER JOIN APIUsage AU ON AU.APIPartnerID = AP.APIPartnerID 
		--INNER JOIN @API A ON A.APIPartnerID = AP.APIPartnerID 
		--ORDER BY APIUsagePriority, ExecutionOrder


		---- Retrive basic information of desired API
	   SELECT DISTINCT APIURLPath
			, APIUsagePriority
			, APIKey
			, Apiusage.APIUsageID
			, APIPartner.APIPartnerID
			, APIPartner.APIPartnerName as VendorName
			,ExecutionOrder as excecutionOrder
		FROM APISubModule 
			INNER JOIN APISubModuleDetail On  APISubModule.APISubModuleID = APISubModuleDetail.APISubModuleID
			INNER JOIN APIMap On APIMap.APIMapID = APISubModuleDetail.APIMapID
			INNER JOIN APIUsage On APIMap.APIUsageID = APIUsage.APIUsageID
			INNER JOIN APIPartner On APIPartner.APIPartnerID = APIUsage.APIPartnerID
		WHERE APISubModuleName = @SubModule
		And APIUsageActive = 1
		And ActivePartner =1
		ORDER BY APIUsagePriority, ExecutionOrder
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GetAPIList.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
