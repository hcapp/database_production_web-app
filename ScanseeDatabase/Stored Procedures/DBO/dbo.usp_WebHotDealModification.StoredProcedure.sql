USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHotDealModification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHotDealModification
Purpose					: To Update the details of the existing Hot Deal.
Example					: usp_WebHotDealModification

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			7th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHotDealModification]
(

	--Input Parameter(s)--
	
	--Inputs to ProductHotdeal Table
	  @HotDealID int
	, @HotDealName  varchar(255)
    , @SalePrice money    
    , @DealStartDate Date
    , @DealStartTime time
    , @DealEndDate Date
    , @DealEndTime time
    , @CategoryID int 
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			UPDATE ProductHotDeal SET HotDealName = @HotDealName
			                        , SalePrice = @SalePrice
			                        --, HotDealStartDate = TRY_CONVERT( DATE,@DealStartDate)-- + TRY_CONVERT(TIME,@DealStartTime  ))
									,HotDealStartDate=CONVERT(DATETIME, CONVERT(CHAR(8), @DealStartDate, 112) 
											+ ' ' + CONVERT(CHAR(8), @DealStartTime , 108))
			                        --, HotDealEndDate = CAST(@DealEndDate AS DATE) + CAST(@DealEndTime AS TIME)
									,HotDealEndDate=CONVERT(DATETIME, CONVERT(CHAR(8), @DealEndDate, 112) 
											+ ' ' + CONVERT(CHAR(8), @DealEndTime , 108))

			                        , CategoryID= @CategoryID
			             WHERE ProductHotDealID = @HotDealID


						 SELECT CONVERT(DATETIME, CONVERT(CHAR(8), @DealStartDate, 112) 
  + ' ' + CONVERT(CHAR(8), @DealStartTime , 108))
  FROM dbo.whatever;
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealModification.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
