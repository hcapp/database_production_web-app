USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerFindRetailerSearchPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*    
Stored Procedure name : usp_FindRetailerSearchPagination    
Purpose     : To Search the Retailers in the given Radius.   
Example     :    
    
History    
Version		Date				Author				Change Description    
-------------------------------------------------------------------------     
1.0			20th May 2013		Dhananjaya TR		Initial Version    
-------------------------------------------------------------------------  
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerFindRetailerSearchPagination]
(

      --Input Parameter(s)--
      
      @UserID int 
    , @SearchKey varchar(100)   
    , @Postalcode int	
	, @LowerLimit int  
	, @ScreenName varchar(50)
	, @CategoryName varchar(100)
	, @RecordCount INT
	
	 --Inputs for User Tracking
	 , @MainMenuId int 
	
      --Output Variable--  
    , @FindRetailerSearchID int output               
    , @MaxCnt int  output
	, @NxtPageFlag bit output  
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

      BEGIN TRY
                          
				DECLARE @RowCount int  
				DECLARE @Combination VARCHAR(1000)  
				DECLARE @Fact INT = 1
				DECLARE @Word VARCHAR(100) = ''
				DECLARE @Count INT = 1
				DECLARE @MaxCount INT 
				DECLARE @SQL VARCHAR(1000) 
				DECLARE @Radius int
				DECLARE @Latitude float
	            DECLARE @Longitude float
	            DECLARE @USerPostalcode int
                DECLARE @QRTypeID int
                DECLARE @AnyThingpageQRType int
                
				DECLARE @Config varchar(50)
				SELECT @Config=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='App Media Server Configuration'
				
				SELECT @QRTypeID=QRTypeID 
				FROM QRTypes 
				WHERE QRTypeName ='Special Offer Page'
				
				SELECT @AnyThingpageQRType=QRTypeID 
				FROM QRTypes 
				WHERE QRTypeName ='Anything Page'
				

				DECLARE @RetailConfig varchar(50)
				SELECT @RetailConfig=ScreenContent
				FROM AppConfiguration 
				WHERE ConfigurationType='Web Retailer Media Server Configuration'
						
				--To get the row count for pagination.  
				DECLARE @UpperLimit int   
				SELECT @UpperLimit = @LowerLimit + @RecordCount
				
				--Select the radius that the User has configured.
				SELECT @Radius = LocaleRadius
				FROM UserPreference 
				WHERE UserID = @UserID
				
				DECLARE @CategoryID INT      
                        
				--To get Find Category associated Business Category
				SELECT DISTINCT BusinessCategoryID
				INTO #BusinessCategory
				FROM  FindSourceCategory F
				INNER JOIN APIPartner A ON A.APIPartnerID = F.APIPartnerID
				WHERE ActiveFlag = 1 And APIPartnerName = 'ScanSee'
				AND CategoryDisplayName = @CategoryName
				
				SELECT @CategoryID = BusinessCategoryID
				FROM #BusinessCategory
				
				--Select the postalcode from user has configured
				IF @Postalcode IS NULL 
				BEGIN
					SELECT @Postalcode = PostalCode 
					FROM Users 
					WHERE UserID = @UserID  
				END
				
				--Select Lat and long based on user Postalcode
				SELECT @Latitude = Latitude 
				      ,@Longitude = Longitude 
				FROM GeoPosition 
				WHERE PostalCode = @Postalcode  
				
				
				--If the radius is still null then retrieve the global radius.	
				IF @Radius IS NULL
				BEGIN			
					SELECT @Radius = ScreenContent
					FROM AppConfiguration 
					WHERE ConfigurationType = 'WishList Default Radius'
				END
				
				

				-- To identify Retailer that have products on Sale or any type of discount
			 
				
					SELECT DISTINCT Retailid , RetailLocationid
					INTO #RetailItemsonSale
					FROM 
					(SELECT b.RetailID, a.RetailLocationID 
					FROM RetailLocationDeal a 
					INNER JOIN RetailLocation b on a.RetailLocationID = b.RetailLocationID 
					INNER JOIN RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
													   AND a.ProductID = c.ProductID 
														AND GETDATE() BETWEEN isnull(a.SaleStartDate, GETDATE()-1) AND isnull(a.SaleEndDate, GETDATE()+1)
					--UNION 
					
					--SELECT  CR.RetailID, RetailLocationID  as RetaillocationID 
					--FROM Coupon C 
					--INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
					--WHERE GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
					
					UNION 
					
					SELECT  RR.RetailID,  RetaillocationID  
					FROM Rebate R 
					INNER JOIN RebateRetailer RR ON R.RebateID=RR.RebateID
					WHERE GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
					
					UNION 
					
					SELECT  c.retailid, a.RetailLocationID 
					FROM  LoyaltyDeal a
					INNER JOIN LoyaltyDealProduct LDP ON LDP.LoyaltyDealID = A.LoyaltyDealID
					INNER JOIN RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
												and a.ProductID = LDP.ProductID 
					INNER JOIN RetailLocation c on a.RetailLocationID = b.RetailLocationID 
					WHERE GETDATE() BETWEEN ISNULL(LoyaltyDealStartDate, GETDATE()-1) AND ISNULL(LoyaltyDealExpireDate, GETDATE() + 1)
					
					UNION
					
					SELECT DISTINCT rl.RetailID, rl.RetailLocationID
					FROM ProductHotDeal p
					INNER JOIN ProductHotDealRetailLocation pr on pr.ProductHotDealID = p.ProductHotDealID 
					INNER JOIN RetailLocation rl ON rl.RetailLocationID = pr.RetailLocationID
					LEFT JOIN HotDealProduct hp ON hp.ProductHotDealID = p.ProductHotDealID 
					WHERE GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate 
					
					union
					
					select q.RetailID, qa.RetailLocationID
					from QRRetailerCustomPage q
					inner join QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID										
					where GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE()+1)
					AND q.QRTypeID =@QRTypeID) a
	 
	            
	     
			   --If Coordinates exists fetching retailer list in the specified radius.    
			   IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)    
			   BEGIN    			   
				
				CREATE TABLE #Retail (Row_Num INT IDENTITY(1,1)
									, RetailID INT 
									, RetailName VARCHAR(255)
									, RetailLocationID INT
									, Address1 VARCHAR(500)
									, Address2 VARCHAR(255)
									, Address3 VARCHAR(255)
									, Address4 VARCHAR(255)
									, City VARCHAR(100)
									, State CHAR(20)
									, PostalCode CHAR(10)
									, retLatitude FLOAT
									, retLongitude FLOAT 
									, RetailerImagePath VARCHAR(1000)
									, Distance FLOAT
									, BannerAdImagePath VARCHAR(1000)   
									, RibbonAdImagePath VARCHAR(1000)
									, RibbonAdURL VARCHAR(1000)
									, RetailLocationAdvertisementID  INT
									, SplashAdID INT
									, SaleFlag  BIT)
					
			      --Select all the Retailers in the vicinity of the User.	
				  INSERT INTO #Retail(RetailID    
									, RetailName
									, RetailLocationID
									, Address1
									, Address2
									, Address3
									, Address4
									, City    
									, State
									, PostalCode
									, retLatitude
									, retLongitude
									, RetailerImagePath    
									, Distance 								
									, SaleFlag)
									
					           SELECT RetailID    
									, RetailName
									, RetailLocationID
									, Address1
									, Address2
									, Address3
									, Address4
									, City    
									, State
									, PostalCode
									, retLatitude
									, retLongitude
									, RetailerImagePath    
									, Distance 								
									, SaleFlag
					    FROM 
					   (SELECT DISTINCT R.RetailID     
									 , R.RetailName
									 , RL.RetailLocationID 
									 , RL.Address1     
									 , RL.Address2
									 , RL.Address3
									 , RL.Address4
									 , RL.City
									 , RL.State
									 , RL.PostalCode
									 , RL.RetailLocationLatitude retLatitude
									 , RL.RetailLocationLongitude retLongitude
									 , RetailerImagePath = CASE WHEN RetailerImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath ELSE @Config+RetailerImagePath END  
															ELSE RetailerImagePath 
														   END  
									 , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude,G.Latitude) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, G.Latitude) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude, G.Longitude) / 57.2958))))*6371) * 0.6214 ,1,1)   			
									 --Flag represents Sale Item on Retailer Locatio. 0 = no Sale available and 1 = Sale available
									 , SaleFlag = CASE WHEN RS.RetailLocationID IS NULL THEN 0 ELSE 1 END   
						    FROM Retailer R    
							--INNER JOIN RetailerBusinessCategory RBC ON RBC.RetailerID = R.RetailID 
							INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID   
							LEFT JOIN GeoPosition G ON RL.PostalCode = G.PostalCode 
							-----------to get retailer that have products on sale
							LEFT JOIN #RetailItemsonSale RS on rl.retailid = RS.retailid and rl.RetailLocationID = RS.RetailLocationID
							WHERE Headquarters = 0) Retailer
							WHERE Distance <= @Radius
							ORDER BY Distance, RetailName ASC										 
					 					
					SELECT DISTINCT Row_Num 
						, R.RetailID     
						, RetailName 
						, R.RetailLocationID 
						, Address1 
						, Address2 
						, Address3 
						, Address4 
						, City City
						, State State
						, PostalCode 
						, retLatitude
						, retLongitude    
						, RetailerImagePath      
						, Distance      
						, S.BannerAdImagePath     
						, B.RibbonAdImagePath     
						, B.RibbonAdURL     
						, B.RetailLocationAdvertisementID   
						, S.SplashAdID 
						, SaleFlag 
						, AnythingPageFlag = CASE WHEN COUNT(DISTINCT QRA.QRRetailerCustomPageID) > 0 THEN 1 ELSE 0 END  
					INTO #Ret
					FROM #Retail R
					LEFT JOIN QRRetailerCustomPage QR ON QR.RetailID = R.RetailID
					LEFT JOIN QRRetailerCustomPageAssociation QRA ON QRA.QRRetailerCustomPageID = QR.QRRetailerCustomPageID AND QRA.RetailLocationID = R.RetailLocationID AND GETDATE() BETWEEN ISNULL(QR.StartDate, GETDATE()-1) AND ISNULL(QR.EndDate, GETDATE()+1) AND QR.QRTypeID =@AnyThingpageQRType						
					LEFT JOIN (SELECT BannerAdImagePath = CASE WHEN SplashAdImagePath IS NOT NULL THEN CASE WHEN ASP.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+SplashAdImagePath ELSE @Config+SplashAdImagePath END  
																	ELSE SplashAdImagePath
																  END
									   , SplashAdID  = ASP.AdvertisementSplashID
									   , R.RetailLocationID 
								FROM #Retail R
								INNER JOIN RetailLocationSplashAd RS ON R.RetailLocationID = RS.RetailLocationID
								INNER JOIN AdvertisementSplash ASP ON ASP.AdvertisementSplashID = RS.AdvertisementSplashID
								AND CAST(GETDATE() AS DATE) BETWEEN ASP.StartDate AND ASP.EndDate) S
					ON R.RetailLocationID = S.RetailLocationID
					LEFT JOIN (  SELECT RibbonAdImagePath = CASE WHEN AB.BannerAdImagePath IS NOT NULL THEN CASE WHEN AB.WebsiteSourceFlag = 1 THEN @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+AB.BannerAdImagePath ELSE @Config+AB.BannerAdImagePath END  
																ELSE AB.BannerAdImagePath
															END 
									  , RibbonAdURL = AB.BannerAdURL
									  , RetailLocationAdvertisementID = AB.AdvertisementBannerID
									  , R.RetailLocationID 
								 FROM #Retail R
								 INNER JOIN RetailLocationBannerAd RB ON R.RetailLocationID = RB.RetailLocationID
								 INNER JOIN AdvertisementBanner AB ON AB.AdvertisementBannerID = RB.AdvertisementBannerID
								 WHERE CAST(GETDATE() AS DATE) BETWEEN AB.StartDate AND AB.EndDate) B
					ON R.RetailLocationID = B.RetailLocationID		
					WHERE (@CategoryName IS NOT NULL AND R.RetailID IN (SELECT RetailerID FROM RetailerBusinessCategory WHERE RetailerID = R.RetailID AND BusinessCategoryID = @CategoryID))
							OR
						   (@CategoryName IS NULL AND 1 = 1)
					GROUP BY Row_Num   
						, R.RetailID     
						, RetailName 
						, R.RetailLocationID 
						, Address1 
						, Address2 
						, Address3 
						, Address4 
						, City
						, State
						, PostalCode 
						, retLatitude
						, retLongitude    
						, RetailerImagePath      
						, Distance      
						, S.BannerAdImagePath     
						, B.RibbonAdImagePath     
						, B.RibbonAdURL     
						, B.RetailLocationAdvertisementID   
						, S.SplashAdID 
						, SaleFlag 
						
					
				   --Check if the Retailers in the given radius has the input keyword or name.
				  CREATE TABLE #Numbers(Number INT)
 
                  
				 --Split the input search string into words.
				 SELECT  RowNum = IDENTITY(INT, 1, 1)
					   , Words = LTRIM(RTRIM(Param))+'%'
				 INTO #Temp1
				 FROM dbo.fn_SplitParam(@SearchKey, ' ') T
				 LEFT JOIN ExceptionKeywords  EK ON EK.ExceptionKeyword=T.PARAM
			   	 WHERE EK.ExceptionKeywordID is NULL 
				 AND LTRIM(RTRIM(Param))+'%' <> '%'

				--Build the input to the string combination code from the Row Numbers
				SELECT @Word = @Word + CAST(RowNum AS VARCHAR(10))
				FROM #Temp1

				SELECT @MaxCount = MAX(RowNum)FROM #Temp1

				--Insert into the Numbers table based on the number of words
				WHILE(@Count <= @MaxCount)
				BEGIN
					INSERT INTO #Numbers
					SELECT @Count
					
					SELECT @Count = @Count + 1
				END

				SET @Count = 1
 
				--This part of the code generates all the possible combinations of the Number string.
				;WITH cteYak(Word, Letters)
				AS (
					SELECT  CAST(SUBSTRING(@Word, Number, 1) AS VARCHAR(100)) AS Word,
							STUFF(@Word, Number, 1, '') AS Letters
					FROM    #Numbers
					WHERE   Number BETWEEN 1 AND LEN(@Word)

					UNION ALL

					SELECT      CAST(Word + SUBSTRING(y.Letters, n.Number, 1) AS VARCHAR(100)) AS Word,
								STUFF(y.Letters, n.Number, 1, '') AS Letters
					FROM        cteYak AS y
					INNER JOIN  #Numbers AS n ON n.Number BETWEEN 1 AND LEN(y.Letters)
				)

				--The combinations of the number string generated are placed in a temp table.
				SELECT  DISTINCT
						  RN = IDENTITY(INT , 1, 1)
						, Word        
				INTO #Combinations        
				FROM    cteYak
				WHERE   LEN(Word) = LEN(@Word)

				--Updating back the numbers to its corresponding words.
				WHILE(@Count <= @MaxCount)
				BEGIN
					SET @SQL = 'UPDATE #Combinations SET Word = REPLACE(Word, '+CAST(@Count AS VARCHAR(10))+', (SELECT Words FROM #Temp1 WHERE RowNum = '+CAST(@Count AS VARCHAR(10))+'))'
					EXEC(@SQL)
					--PRINT @SQL
					
					SET @Count += 1
				END

				--Append the % symbol to perform the "like" search
				UPDATE #Combinations SET Word = STUFF(Word, 1, 0, '%')
				
				SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
						 , R.RetailID    
						 , R.RetailName
						 , R.RetailLocationID
						 , R.Address1
						 , R.Address2
						 , R.Address3
						 , R.Address4
						 , R.City    
						 , R.State
						 , R.PostalCode
						 , R.retLatitude
						 , R.retLongitude
						 , R.RetailerImagePath    
						 , R.Distance 								
						 , R.SaleFlag
						 , R.AnythingPageFlag
						 , R.BannerAdImagePath      
					     , R.RibbonAdImagePath      
						 , R.RibbonAdURL      
						 , R.RetailLocationAdvertisementID      
						 , R.SplashAdID
						 , CASE WHEN RL.CorporateAndStore =1 THEN RE.CorporatePhoneNo ELSE RC.ContactPhone END ContactPhone
						 , CASE WHEN RL.RetailLocationURL IS NULL THEN RE.RetailURL ELSE RL.RetailLocationURL END retailerURL
				INTO #Ret1  
				FROM #Ret R
				INNER JOIN RetailLocation RL ON R.retailLocationID = RL.RetailLocationID
				INNER JOIN Retailer RE ON RE.RetailID =RL.RetailID 
				LEFT JOIN #Combinations C ON 1 = 1
				LEFT JOIN RetailerKeywords RK ON RK.RetailID = RL.RetailID
				LEFT JOIN RetailContact RC ON RC.RetailLocationID =RL.RetailLocationID 
				WHERE R.RetailName LIKE CASE WHEN @SearchKey IS NOT NULL THEN (SELECT C1.Word FROM #Combinations C1 WHERE C1.RN = C.RN) ELSE '%' END
				OR RK.RetailKeyword LIKE CASE WHEN @SearchKey IS NOT NULL THEN (SELECT C1.Word FROM #Combinations C1 WHERE C1.RN = C.RN) ELSE '%' END 
				ORDER BY R.Distance, R.RetailName ASC
				
					
											
				 --To capture max row number.  
				 SELECT @MaxCnt = MAX(RowNum) FROM #Ret1
				 
				 --this flag is a indicator to enable "More" button in the UI.   
				 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
				 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
				  
				 --Project the final result set with pagination.
				 SELECT DISTINCT RowNum rowNumber  
					, RetailID retailerId    
					, RetailName retailerName
					, RetailLocationID
					, Address1 retaileraddress1
					, Address2 retaileraddress2
					, Address3 retaileraddress3
					, Address4 retaileraddress4
					, City City
					, State State
					, PostalCode PostalCode  
					, retLatitude 
					, retLongitude 
					, RetailerImagePath logoImagePath     
					, Distance      
					, BannerAdImagePath bannerAdImagePath    
					, RibbonAdImagePath ribbonAdImagePath    
					, RibbonAdURL ribbonAdURL    
					, RetailLocationAdvertisementID advertisementID    
					, SplashAdID splashAdID
					, SaleFlag
					, AnythingPageFlag
					, ContactPhone
					, retailerURL
				 INTO #RetailerList     
				 FROM #Ret1  
				 WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 
			
							 
				  --User Tracking section.
					 
				  --Capture the search key.
				  INSERT INTO ScanSeeReportingDatabase..FindRetailerSearch(MainMenuID
																		 , SearchKeyword
																		 , CreatedDate)
																SELECT @MainMenuId
																	 , @SearchKey
																	 , GETDATE() 
					SELECT @FindRetailerSearchID = SCOPE_IDENTITY()
					 
					  --Table to track the Retailer List.
					 CREATE TABLE #Temp(RetailerListID int
                                        ,LocationDetailID int
                                        ,MainMenuID int
                                        ,RetailID int
                                        ,RetailLocationID int)  
                      --Capture the impressions of the Retailer list.
                     -- select * from ScanSeeReportingDatabase..retailerlist                       
                      
                      
                      INSERT INTO ScanSeeReportingDatabase..RetailerList(MainMenuID
																	, RetailID
																	, RetailLocationID
																	, FindRetailerSearchID
																	, CreatedDate)
                     
                      OUTPUT inserted.RetailerListID, inserted.MainMenuID, inserted.RetailLocationID INTO #Temp(RetailerListID, MainMenuID, RetailLocationID)							
													SELECT @MainMenuId
														 , retailerId
														 , RetailLocationID
														 , @FindRetailerSearchID
														 , GETDATE()
													FROM #RetailerList
					--Display the Retailer along with the keys generated in the Tracking table.				
				
					
					SELECT rowNumber
						 , T.RetailerListID retListID
						 , retailerId
						 , retailerName
						 , T.RetailLocationID
						 , retaileraddress1
						 , retaileraddress2
						 , retaileraddress3
						 , retaileraddress4
						 , City
						 , State
						 , PostalCode
						 , Distance 
						 , logoImagePath
						 , bannerAdImagePath
						 , ribbonAdImagePath
						 , ribbonAdURL
						 , advertisementID
						 , splashAdID
						 , SaleFlag		
						 , AnythingPageFlag	
						 , ContactPhone
					     , retailerURL			 
				   	FROM #Temp T
					INNER JOIN #RetailerList R ON  R.RetailLocationID = T.RetailLocationID
				 
            END
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebConsumerFindRetailerSearchPagination.'         
                  --Execute retrieval of Error info.
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  PRINT 'The Transaction is uncommittable. Rolling Back Transaction'                 
            END;
            
      END CATCH;
END;


GO
