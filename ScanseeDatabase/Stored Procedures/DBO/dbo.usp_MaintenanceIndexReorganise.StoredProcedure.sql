USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MaintenanceIndexReorganise]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MaintenanceIndexReorganise
Purpose					: Used to Reorganise all the Indexes  
Example					: usp_MaintenanceIndexReorganise

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			13th Feb 2014	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MaintenanceIndexReorganise]
(
	  
		
	--Output Variable 
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
	
		SELECT DISTINCT ps.database_id
					  , ps.OBJECT_ID 
					  , O.name TableName
					  , ps.index_id 
					  , b.name IndexName
					  , ps.avg_fragmentation_in_percent
		INTO #Temp	 
		FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS ps
		INNER JOIN sys.indexes AS b ON ps.OBJECT_ID = b.OBJECT_ID
		INNER JOIN SYS.index_columns AS IC ON IC.index_id = B.index_id
		INNER JOIN SYS.objects O ON O.object_id = B.object_id
		AND ps.index_id = b.index_id
		WHERE ps.database_id = DB_ID()
		AND IC.is_included_column = 0 

		DECLARE @TableName varchar(255)
		DECLARE @IndexName VARCHAR(1000)
		DECLARE @Query VARCHAR(1000)
		
		--filter all the indexes whose fragmentaion > 10%
		DECLARE TableCursor CURSOR FOR
		SELECT TableName 
			 , IndexName	 
		FROM #Temp
		WHERE avg_fragmentation_in_percent > 10

		OPEN TableCursor

		FETCH NEXT FROM TableCursor INTO @TableName, @IndexName
		--Loop till the data is there in the table.
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			PRINT 'ReBuild: ' + @TableName
			SET @Query = 'ALTER INDEX ' +'['+ @IndexName +']'+ ' ON ' + @TableName + ' REORGANIZE'
			--select @Query
			EXEC(@Query)
			FETCH NEXT FROM TableCursor INTO @TableName, @IndexName
			END

		CLOSE TableCursor

		DEALLOCATE TableCursor 
		
		--Update statistics one the Rebuild is done.
		EXEC SP_UPDATESTATS
		
	END TRY
		
	BEGIN CATCH		
		IF @@ERROR <> 0
		BEGIN
		--select ERROR_MESSAGE()
			PRINT 'Error occured in Stored Procedure usp_MaintenanceIndexReorganise.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;		 
	END CATCH;
END;

GO
