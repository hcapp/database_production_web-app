USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsDeletion_27oCT2016]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsDeletion]
Purpose					: To delete from RssFeedNewsStagingTable.
Example					: [usp_WebRssFeedNewsDeletion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17 Nov 2015		SPAN	        1.1
1.1							Sagar Byali		Changes in Video news
1.2			28 Oct 2016						Performance changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedNewsDeletion_27oCT2016]
(
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

		DECLARE @ExpiryPublishedDateRockwall DATETIME = GETDATE()-15
		, @ExpiryPublishedDateMarbelVideos DATETIME
		, @ExpiryPublishedDateMarbelAllCategoryNews DATETIME
		, @ExpiryPublishedDateKilleen datetime  = GETDATE()-5
		, @DoDelete TINYINT = CASE WHEN DATEPART(HH,GETDATE()) = 0 THEN 1 ELSE 0 END
		
		SELECT @ExpiryPublishedDateMarbelVideos = max(convert(date,DateCreated)) 
		FROM RssFeedNewsStagingTable 
		WHERE NewsType='videos' AND HcHubCitiID = 10
		
		SELECT @ExpiryPublishedDateMarbelAllCategoryNews = DATEADD(d,-14,max(convert(date,PublishedDate)))
		FROM RssFeedNewsStagingTable 
		WHERE NewsType = 'all' AND HcHubCitiID = 10

		IF @DoDelete = 1
		BEGIN
			BEGIN TRAN

			DELETE FROM RssFeedNewsStagingTable
				WHERE (HcHubCitiID = 28 AND convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateRockwall)
				OR (HcHubCitiID = 10 AND ((convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateRockwall AND NewsType NOT IN ('Videos','all')) OR (convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateMarbelAllCategoryNews AND NewsType = 'all') OR (convert(date,ISNULL(PublishedDate,DateCreated)) <> @ExpiryPublishedDateMarbelVideos AND NewsType = 'Videos')))
				OR (HcHubCitiID = 52 AND convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateKilleen)
			COMMIT TRAN						   
		END
		SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
		SET @ErrORNumber = ERROR_NUMBER()
		SET @ErrORMessage = ERROR_MESSAGE()
		SET @Status=1 
	END CATCH;
END;





GO
