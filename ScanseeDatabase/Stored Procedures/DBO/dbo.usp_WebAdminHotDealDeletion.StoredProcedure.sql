USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminHotDealDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebAdminHotDealDeletion]
Purpose				  : To Delete the existing Hot Deal
Example				  : [usp_WebAdminHotDealDeletion] 
    
History    
Version     Date           Author    Change Description    
---------------------------------------------------------------     
1.0      30th Aug 2013     SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebAdminHotDealDeletion] 
(    
      --Input Variables
      @ProductHotDealID VArchar(max)
       
	  --OutPut Variables
	, @Status int output 
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
		
		BEGIN TRANSACTION
		INSERT INTO ProductHotDealHistory(ProductHotDealID
										, RetailID
										, APIPartnerID
										, HotDealProvider
										, Price
										, HotDealDiscountType
										, HotDealDiscountAmount
										, HotDealDiscountPct
										, SalePrice
										, HotDealName
										, HotDealShortDescription
										, HotDeaLonglDescription
										, HotDealImagePath
										, HotDealTermsConditions
										, HotDealURL
										, Expired
										, HotDealStartDate
										, HotDealEndDate
										, SourceID
										, Category
										, CreatedDate
										, DateModified
										, CategoryID
										)
						   SELECT ProductHotDealID
										, RetailID
										, APIPartnerID
										, HotDealProvider
										, Price
										, HotDealDiscountType
										, HotDealDiscountAmount
										, HotDealDiscountPct
										, SalePrice
										, HotDealName
										, HotDealShortDescription
										, HotDeaLonglDescription
										, HotDealImagePath
										, HotDealTermsConditions
										, HotDealURL
										, Expired
										, HotDealStartDate
										, HotDealEndDate
										, SourceID
										, Category
										, CreatedDate
										, DateModified
										, CategoryID
						 FROM ProductHotDeal PH
						 INNER JOIN fn_SplitParam(@ProductHotDealID,',') F ON F.Param =PH.ProductHotDealID 
						

		DELETE FROM UserHotDealGallery 
		FROM UserHotDealGallery UH
		INNER JOIN fn_SplitParam(@ProductHotDealID,',') F ON F.Param =UH.HotDealID 		
		
		DELETE FROM ProductHotDealRetailLocation 
		FROM ProductHotDealRetailLocation PRL
		INNER JOIN fn_SplitParam(@ProductHotDealID,',') F ON F.Param =PRL.ProductHotDealID 	
		
		DELETE FROM ProductHotDeal
		FROM ProductHotDeal PH 
		INNER JOIN fn_SplitParam(@ProductHotDealID,',') F ON F.Param =PH.ProductHotDealID  	 
					
		--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
		
	END TRY
		
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebAdminHotDealDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
