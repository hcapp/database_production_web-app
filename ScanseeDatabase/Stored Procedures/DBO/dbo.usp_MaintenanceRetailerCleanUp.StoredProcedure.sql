USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MaintenanceRetailerCleanUp]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_MaintenanceRetailerCleanUp]
Purpose					: 
Example					: [usp_MaintenanceRetailerCleanUp]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th Sep 2012	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MaintenanceRetailerCleanUp]
(
	@RetailID int
	
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		

			SELECT C.CouponID
			INTO #Coupon
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID= CR.CouponID
			WHERE CR.RetailID = @RetailID 
			
			SELECT RE.RebateID 
			INTO #Rebate
			FROM Rebate RE
			INNER JOIN RebateRetailer RR ON RE.RebateID=RR.RebateID 
			WHERE RR.RetailID = @RetailID 

			SELECT ProductHotDealID
			INTO #HotDeal
			FROM ProductHotDeal P
			WHERE P.RetailID = @RetailID 

			--Add's

			DELETE FROM RetailLocationAdvertisement
			FROM RetailLocationAdvertisement RA
			INNER JOIN RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
			WHERE RL.RetailID = @RetailID 

			--Retailer data deletion

			DELETE FROM RetailCategory
			FROM RetailCategory R
			WHERE R.RetailID = @RetailID 

			DELETE FROM ProductReviews
			FROM ProductReviews P
			WHERE P.RetailID = @RetailID 

			DELETE FROM UserRetailPreference
			FROM UserRetailPreference U
			WHERE U.RetailID = @RetailID 

			DELETE FROM RetailerBankInformation
			from RetailerBankInformation RB
			WHERE RB.RetailerID = @RetailID 

			DELETE FROM RetailerBillingDetails
			FROM RetailerBillingDetails RB
			WHERE RB.RetailerID = @RetailID 

			DELETE FROM RetailLocationProduct
			FROM RetailLocationProduct PA
			INNER JOIN RetailLocation RL ON PA.RetailLocationID=RL.RetailLocationID
			WHERE RL.RetailID = @RetailID 

			DELETE FROM RetailContact
			FROM RetailContact R
			INNER JOIN RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID
			WHERE RL.RetailID = @RetailID 

			----Product related tables

			DELETE FROM RebateProduct
			FROM RebateProduct PA
			INNER JOIN RebateRetailer RR ON PA.RebateID=RR.RebateID
			WHERE RR.RetailID = @RetailID 

			DELETE FROM HotDealProduct
			FROM HotDealProduct PA
			INNER JOIN ProductHotDeal P on p.ProductHotDealID=PA.ProductHotDealID
			WHERE P.RetailID = @RetailID 

			DELETE FROM ProductUserHit
			FROM ProductUserHit PA
			INNER JOIN RetailLocation RL ON rl.RetailLocationID=PA.RetailLocationID
			WHERE RL.RetailID = @RetailID 

			----Coupon related tables 

			DELETE FROM CouponRetailer
			FROM CouponRetailer CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  
			WHERE CP.RetailID = @RetailID 

			DELETE FROM UserCouponGallery
			FROM UserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 			
			
			DELETE FROM CouponProduct
			FROM CouponProduct CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID
			
			DELETE FROM Coupon
			FROM Coupon CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID
			
			DELETE FROM CouponProduct
			FROM CouponProduct C
			INNER JOIN Coupon CP ON CP.CouponID = C.CouponID
			WHERE CP.RetailID = @RetailID 
			
			DELETE FROM Coupon
			FROM Coupon CP
			WHERE CP.RetailID = @RetailID 

			----Rebate related tables

			DELETE FROM RebateRetailer
			FROM RebateRetailer  RP
			INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 
			WHERE RP.RetailID = @RetailID 

			DELETE FROM ScanHistory
			FROM ScanHistory  RP
			INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 
			INNER JOIN RetailLocation RL ON RP.RetailLocationID=RL.RetailLocationID
			WHERE RL.RetailID = @RetailID 

			DELETE FROM UserRebateGallery
			FROM UserRebateGallery  RP
			INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 
			INNER JOIN RetailLocation RL ON RL.RetailLocationID=RP.RetailLocationID
			WHERE RL.RetailID = @RetailID 

			----ProductHotDeal related tables.

			DELETE FROM ProductHotDealRetailLocation
			FROM ProductHotDealRetailLocation P
			INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 

			DELETE FROM RetailLocation
			FROM RetailLocation RL
			WHERE RL.RetailID = @RetailID 
			
			DELETE FROM ProductHotDeal
			FROM ProductHotDeal RL
			WHERE RL.RetailID = @RetailID 

			DELETE FROM UserRetailer 
			WHERE RetailID = @RetailID

			DELETE FROM Retailer 
			WHERE RetailID = @RetailID

			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MaintenanceRetailerCleanUp.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
