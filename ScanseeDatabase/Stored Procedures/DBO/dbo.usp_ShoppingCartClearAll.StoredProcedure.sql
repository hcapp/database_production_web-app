USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingCartClearAll]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ShoppingCartClearAll
Purpose					: To clear all the product from shopping Cart and move it to History.
Example					: usp_ShoppingCartClearAll 1, NULL, NULL

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ShoppingCartClearAll]
(
	  @UserID int
	, @CartLatitude float
	, @CartLongitude float
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		DECLARE @Today datetime
		DECLARE @UserCartID int
		SET @Today = GETDATE()
		BEGIN TRANSACTION
			--Moving Shopping Cart data to History
			INSERT INTO [UserShoppingCartHistory]
				   ([UserID]
				   ,[CartLatitude]
				   ,[CartLongitude]
				   ,[CartCreateDate])
			 VALUES
				   (@UserID 
				   ,@CartLatitude
				   ,@CartLongitude
				   ,@Today)
			
			SET @UserCartID = @@IDENTITY 
			
			INSERT INTO [UserShoppingCartHistoryProduct]
				   ([UserCartID]
				   ,[UserProductID]
				   ,[MasterListAddDate]
				   ,[TodayListAddDate]
				   ,[ShopCartAddDate])
			 SELECT @UserCartID
					, UserProductID
					, MasterListAddDate
					, TodayListAddDate
					, ShopCartAddDate
			 FROM UserProduct 
		     WHERE UserID = @UserID 
					AND ShopCartItem = 1
					
			--To delete product from Shopping Cart
			UPDATE UserProduct 
			SET ShopCartItem = 0
				, ShopCartRemoveDate = @Today 
			WHERE UserID = @UserID 
				AND ShopCartItem = 1
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ShoppingCartClearAll.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
