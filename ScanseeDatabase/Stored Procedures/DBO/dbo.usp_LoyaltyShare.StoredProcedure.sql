USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoyaltyShare]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_LoyaltyShare
Purpose					: 
Example					: usp_LoyaltyShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Jan 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_LoyaltyShare]
(
	   
	   @LoyaltyID int
	   	
	--Output Variable 
	, @LoyaltyExpired bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		
		BEGIN
			SELECT  LoyaltyDealName loyaltyDealName
				   ,NULL loyaltyURL 
				   ,LoyaltyDealStartDate loyaltyDealStartDate
				   ,LoyaltyDealExpireDate loyaltyDealExpireDate
			FROM LoyaltyDeal 
			WHERE GETDATE() Between LoyaltyDealStartDate AND LoyaltyDealExpireDate AND LoyaltyDealID = @LoyaltyID
			
			SELECT @LoyaltyExpired=CASE WHEN GETDATE()>LoyaltyDealExpireDate Then 0 else 1 END 
			FROM LoyaltyDeal
			WHERE LoyaltyDealID = @LoyaltyID
		END	
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_LoyaltyShare.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
