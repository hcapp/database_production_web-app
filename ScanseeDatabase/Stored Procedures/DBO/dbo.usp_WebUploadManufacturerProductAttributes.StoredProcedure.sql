USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerProductAttributes]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UploadManufacturerProducts
Purpose					: To upload product data from stage table UploadManufacturerProducts to production table Products
Example					: usp_UploadManufacturerProducts

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15th Mar 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerProductAttributes]
(
	 @ManufacturerID int
	,@UserID int	
	,@ProductAttributeFileName Varchar(255)
	,@ManufacturerLogID int 
	
	--Output Variable 
	, @SuccessCount int output
	, @TotalRows int output
	, @FailureCount int output	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			 
			--To get Manufacturer name
			DECLARE @manufname varchar(255)
			SELECT @manufname=ManufName
			FROM Manufacturer
			WHERE ManufacturerID=@ManufacturerID
			
			--To capture execution start date & time
			DECLARE @startDate datetime
			SET @startDate=GETDATE()		
			
			--To get get the records with all the mandatory fields.
			
			SELECT UploadManufacturerProductAttributesID
				 , ScanCode
				 , AttributeName
				 , DisplayValue
				 , Userid
				 , ManufacturerID
				 , DateCreated
				 , AttributeID = (SELECT AttributeId FROM Attribute WHERE AttributeName = U.AttributeName)
			INTO #MandatoryFields
			FROM UploadManufacturerProductAttributes U
			WHERE ScanCode IS NOT NULL
			AND AttributeName IS NOT NULL 
			AND AttributeName NOT LIKE ''
			AND DisplayValue IS NOT NULL 
			AND DisplayValue NOT LIKE ''
			AND ManufacturerID = @ManufacturerID
			AND Userid = @UserID
			
			
			--To get get the records which does not have all the mandatory fields.
			
			SELECT UploadManufacturerProductAttributesID
				 , ScanCode
				 , AttributeName
				 , DisplayValue
				 , Userid
				 , ManufacturerID
				 , DateCreated
				 , AttributeID = (SELECT AttributeId FROM Attribute WHERE AttributeName = U.AttributeName)
			INTO #NonMandatoryFields
			FROM UploadManufacturerProductAttributes U
			WHERE (ScanCode IS NULL
			OR AttributeName IS  NULL
			OR AttributeName LIKE ''
			OR DisplayValue LIKE ''
			OR DisplayValue IS  NULL)
			AND ManufacturerID = @ManufacturerID
			AND Userid = @UserID
			
			--To get the product attributes of the products which are newly inserted
			
			SELECT  DISTINCT UA.UploadManufacturerProductAttributesID
					,UA.ScanCode
					,(CASE WHEN A.AttributeName IS NULL THEN  UA.AttributeName
						  WHEN UA.AttributeName=A.AttributeName THEN NULL END) AttributeName
					,UA.DisplayValue
					,UA.Userid
					,UA.ManufacturerID
					,UA.DateCreated
					,p.ProductID
					,(CASE WHEN A.AttributeName IS NULL THEN  (SELECT AttributeId FROM Attribute WHERE AttributeName='Others') 
						  WHEN UA.AttributeName=A.AttributeName THEN A.AttributeId END) AttributeId
			INTO #Attributes
			FROM #MandatoryFields UA
			INNER JOIN Product P ON P.ScanCode=UA.ScanCode
			LEFT OUTER JOIN Attribute A ON A.AttributeName=UA.AttributeName
			LEFT OUTER JOIN ProductAttributes PA ON P.ProductID = PA.ProductID AND PA.AttributeID = A.AttributeId 
			WHERE  PA.ProductAttributesID IS NULL
							
			--To get the invalid products.
					
			SELECT UA.UploadManufacturerProductAttributesID
			     , UA.ScanCode
				 , UA.AttributeName
				 , UA.DisplayValue
				 , AttributeID = (SELECT AttributeId FROM Attribute WHERE AttributeName = UA.AttributeName)
			INTO #InvalidProducts
			FROM #MandatoryFields UA
			LEFT OUTER JOIN Product P ON P.ScanCode=UA.ScanCode
			WHERE P.ProductID IS NULL
			AND UA.ManufacturerID = @ManufacturerID
			
			--SELECT COUNT(1) FROM #InvalidProducts
			
			SELECT UploadManufacturerProductAttributesID
				 , ScanCode
				 , AttributeName
				 , DisplayValue
				 , Userid
				 , ManufacturerID
				 , DateCreated
				 , ProductID
				 , AttributeId
			INTO #ValidAttributes
			FROM #Attributes
			WHERE UploadManufacturerProductAttributesID IN(
			SELECT MIN(UploadManufacturerProductAttributesID)UploadManufacturerProductAttributesID
			FROM #Attributes
			GROUP BY ScanCode
			       , AttributeName)
			 
			--To get the mandatory value missing product attribute information.
			SELECT UploadManufacturerProductAttributesID
					,ScanCode
					,AttributeName
					,DisplayValue
					,Userid
					,ManufacturerID
					,DateCreated
			INTO #DiscardedAttributes
			FROM UploadManufacturerProductAttributes 
			WHERE UserID = @UserID AND ManufacturerID = @ManufacturerID 
				AND UploadManufacturerProductAttributesID NOT IN (SELECT UploadManufacturerProductAttributesID FROM #ValidAttributes)
								
			
			
			---Processing Product Attributes ----------------------
				--Insert into Product Attributes
				INSERT INTO [ProductAttributes]
						   ([ProductID]
						   ,[AttributeName]
						   ,[DisplayValue]
						   ,[AttributeID])		
				SELECT ProductID
					   ,AttributeName
					   ,DisplayValue
					   ,AttributeId
				FROM #ValidAttributes
			-------------------End of Processing Product Attributes--------------------	
						
			--To insert the failure records of the product Attribute.
			
			INSERT INTO [UploadManufacturerDiscardedProductAttributes]
					   ([UploadManufacturerLogID]
					   ,[ScanCode]
					   ,[AttributeName]
					   ,[DisplayValue]
					   , [ProductAttributeFileName] 
					   , ReasonForDiscarding)
		 	SELECT  ManufacturerLogID
				   ,[ScanCode]
				   ,[AttributeName]
				   ,[DisplayValue]
				   , ProductAttributeFileName
				   , ReasonForDiscarding
		 	FROM 				   
		    (
				SELECT @ManufacturerLogID ManufacturerLogID
				   ,[ScanCode]
				   ,ISNULL(AttributeName, (SELECT AttributeName FROM Attribute WHERE AttributeId = N.AttributeID)) AttributeName
				   ,[DisplayValue]
				   ,@ProductAttributeFileName ProductAttributeFileName
				   , 'Some Mandatory fields are missing.' ReasonForDiscarding
				FROM #NonMandatoryFields N
				
				UNION ALL
				
				SELECT @ManufacturerLogID ManufacturerLogID
				   ,[ScanCode]
				   ,ISNULL(AttributeName, (SELECT AttributeName FROM Attribute WHERE AttributeId = N.AttributeID)) AttributeName
				   ,[DisplayValue]
				   ,@ProductAttributeFileName ProductAttributeFileName
				   , 'Invalid Product.' ReasonForDiscarding
				FROM #InvalidProducts N
				
				UNION ALL
				
				SELECT @ManufacturerLogID ManufacturerLogID
				   ,[ScanCode]
				   ,ISNULL(AttributeName, (SELECT AttributeName FROM Attribute WHERE AttributeId = N.AttributeID)) AttributeName
				   ,[DisplayValue]
				   ,@ProductAttributeFileName ProductAttributeFileName
				   , 'Duplicate Attribute.' ReasonForDiscarding
				FROM #Attributes N
				WHERE UploadManufacturerProductAttributesID NOT IN(SELECT UploadManufacturerProductAttributesID FROM #ValidAttributes)
								
				UNION ALL
				
				SELECT @ManufacturerLogID ManufacturerLogID
				   ,[ScanCode]
				   ,ISNULL(AttributeName, (SELECT AttributeName FROM Attribute WHERE AttributeId = N.AttributeID)) AttributeName
				   ,[DisplayValue]
				   ,@ProductAttributeFileName ProductAttributeFileName
				   , 'Given Attribute already exist for the Product.' ReasonForDiscarding
				FROM #MandatoryFields N
				WHERE UploadManufacturerProductAttributesID NOT IN(SELECT UploadManufacturerProductAttributesID FROM #Attributes)
				AND UploadManufacturerProductAttributesID NOT IN (SELECT UploadManufacturerProductAttributesID FROM #InvalidProducts)
				
		    ) DiscardedAttributes
		    
		    
		    --To update the Log for the batch process.
		    UPDATE [UploadManufacturerLog] 
			SET [ProductAttributeFileName] = @ProductAttributeFileName			   
			   ,[ProductAttributeTotalRowsProcessed] = ((SELECT Count(1) FROM UploadManufacturerDiscardedProductAttributes WHERE Userid =@UserID AND ManufacturerId=@ManufacturerID) +
														(SELECT COUNT(1) FROM #ValidAttributes))
			   ,[ProductAttributeSuccessfulRowCount] = (SELECT COUNT(1) FROM #ValidAttributes)
			   ,[ProductAttributeFailureRowCount] = (SELECT Count(1) FROM UploadManufacturerDiscardedProductAttributes WHERE Userid =@UserID AND ManufacturerId=@ManufacturerID)
			   ,[Remarks] = 'Success'
			   ,[ProcessDate] = GETDATE()
			   ,[ProcessEndDate] = GETDATE()
		    WHERE UploadManufacturerLogID = @ManufacturerLogID
		    
		    --To capture the Counts.
		    SELECT @TotalRows = ProductAttributeTotalRowsProcessed
				 , @SuccessCount = ProductAttributeSuccessfulRowCount
				 , @FailureCount = ProductAttributeFailureRowCount
		    FROM UploadManufacturerLog
		    WHERE UploadManufacturerLogID = @ManufacturerLogID
							
	--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerProducts.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			--DECLARE @ERRORMsg Varchar(2000)
			--SET @ERRORMsg=ERROR_MESSAGE()
			ROLLBACK TRANSACTION;
						
			--To capture Log info.			
			UPDATE [UploadManufacturerLog] 
			SET [ProductAttributeFileName] = @ProductAttributeFileName			   
			   ,[ProductAttributeTotalRowsProcessed] = 0
			   ,[ProductAttributeSuccessfulRowCount] = 0
			   ,[ProductAttributeFailureRowCount] = 0
			   ,[Remarks] = @ErrorMessage
			   ,[ProcessDate] = GETDATE()
			   ,[ProcessEndDate] = GETDATE()
		    WHERE UploadManufacturerLogID = @ManufacturerLogID 
		    
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
