USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCustomPageRetailLcoationDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCustomPageRetailLcoationDisplay
Purpose					: To Get AnythingPage RetailLocation details 
Example					: usp_WebRetailerCustomPageRetailLcoationDisplay

History
Version		Date		Author	   Change Description
------------------------------------------------------
1.0		20th Aug 2013	Span	   Initail Version
------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCustomPageRetailLcoationDisplay]
(

	--Input Variable
	  @RetailID int
	, @PageID int  
   	
	--Output Variable	  	 
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--To Get AnythingPage RetailLocation details 
		SELECT DISTINCT QRC.RetailID
					 , RL.RetailLocationID
					 , RL.StoreIdentification 
					 , RL.Address1
					 , RL.City
					 , RL.[State]
					 , RL.PostalCode
		FROM  QRRetailerCustomPage QRC 
		INNER JOIN QRRetailerCustomPageAssociation QRCA ON QRC.QRRetailerCustomPageID = QRCA.QRRetailerCustomPageID
		INNER JOIN RetailLocation RL ON QRCA.RetailLocationID = RL.RetailLocationID
		WHERE QRC.QRRetailerCustomPageID  = @PageID 		
		AND QRC.RetailID = @RetailID AND RL.Active = 1
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCustomPageRetailLcoationDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;



GO
