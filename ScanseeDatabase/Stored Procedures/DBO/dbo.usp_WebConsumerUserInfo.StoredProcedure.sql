USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerUserInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserInfo
Purpose					: To capture User information
Example					: usp_UserInfo 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th May 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerUserInfo]
(
	@UserID int
	,@FirstName varchar(20)
	,@Lastname varchar(30)
	--,@Email Varchar(100)
	,@Address1 varchar(50)
	,@Address2 varchar(50)
	--,@Address3 varchar(50)
	--,@Address4 varchar(50)
	,@City varchar(30)
	,@State char(2)
	,@PostalCode varchar(10)
	,@CountryID int
	,@Gender bit
	,@DOB date
	,@IncomeRangeID int
	,@EducationLevelID int
	,@HomeOwner bit
	,@Children int
	,@MaritalStatus  varchar(30)	
	,@DateModified datetime
	,@MobilePhone char(10)
    --,@DeviceID varchar(60)
    --,@UniversityIDs VARCHAR(1000)
    
    --OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--If the countryId is sent as null then default countryid is set to 1.
			--SET @CountryID = ISNULL(@CountryID, 1) 
			
			--To replace the default date when empty DOB is sent.
			IF @DOB LIKE '1900-01-01'
			BEGIN
				SET @DOB = NULL
			END
			
			--IF EXISTS (SELECT 1 FROM Users WHERE UserID = @UserID)
			--BEGIN
			 UPDATE Users 
			 SET FirstName = @FirstName 
				,Lastname = @Lastname 
				--,Email= @Email
				,Address1 = @Address1 
				,Address2 = @Address2 
				--,Address3 = @Address3 
				--,Address4 = @Address4 
				,City = @City 
				,State = @State 
				,PostalCode = @PostalCode 
				,CountryID = @CountryID
				,MobilePhone =  @MobilePhone 
				--,DeviceID = @DeviceID 
				,FirstUseComplete = 1 
				,DateModified = @DateModified  
			WHERE UserID = @UserID 
			
			IF EXISTS (SELECT 1 FROM UserDemographic WHERE UserID = @UserID )
			BEGIN
				UPDATE UserDemographic 
				SET Gender = @Gender 
					,DOB = @DOB 
					,IncomeRangeID = @IncomeRangeID
					,EducationLevelID = @EducationLevelID 
					,HomeOwner = @HomeOwner 
					,NumberOfChildren = @Children 
					,MaritalStatusID = @MaritalStatus 					 
					,DateModified = @DateModified
				WHERE UserID = @UserID  
			END
			ELSE
			BEGIN
				INSERT INTO [UserDemographic]
					   ([UserID]
					   ,[Gender]
					   ,[DOB]
					   ,[IncomeRangeID]
					   ,[EducationLevelID] 
					   ,[HomeOwner] 
					   ,[NumberOfChildren] 
					   ,[MaritalStatusID]
					   ,[DateCreated]
					   ,[DateModified])
				 VALUES
					   (@UserID 
					   ,@Gender 
					   ,@DOB 
					   ,@IncomeRangeID 
					   ,@EducationLevelID 
					   ,@HomeOwner 
					   ,@Children 
					   ,@MaritalStatus 					   
					   ,@DateModified
					   ,@DateModified)
			END		
				
			--Confirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
		
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerUserInfo.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
