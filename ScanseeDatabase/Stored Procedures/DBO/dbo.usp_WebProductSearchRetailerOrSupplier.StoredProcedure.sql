USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebProductSearchRetailerOrSupplier]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebProductSearchRetailerOrSupplier    
Purpose     : To Search for the Hot Deals of Retailers.    
Example     :     
    
History    
Version  Date       Author   Change Description    
-------------------------------------------------------------------------------     
1.0   15th December 2011    Pavan Sharma K Initial Version    
-------------------------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebProductSearchRetailerOrSupplier]    
(    
    
 --Input Input Parameter(s)--      
      
    @UserID int         
  , @SearchParameter varchar(100)    
  , @LowerLimit int    
     
 --Output Variable--    
      
  , @RowCount INT output    
  , @NextPageFlag bit output    
  , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY      
      
  DECLARE @UpperLimit INT    
  DECLARE @MaxCnt INT      
      
  DECLARE @ManufacturerID INT    
  DECLARE @RetailID INT    
      
      
  --To get the Number of records per page.    
  DECLARE @ScreenContent INT    
  SELECT @ScreenContent = ScreenContent    
  FROM AppConfiguration    
  WHERE ConfigurationType = 'Website Pagination'    
  AND ScreenName = 'All'    
  AND Active = 1    
      
  SELECT @UpperLimit = @LowerLimit + @ScreenContent    
            
        DECLARE @Config varchar(50)    
  SELECT @Config=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'    
      
    
  --TO CHECK IF THE LOGGED IN USER IS A MANUFACTURER    
    
  IF EXISTS(SELECT 1 FROM UserManufacturer where UserID = @UserID)    
  BEGIN           
    
   SELECT RowNum = ROW_NUMBER() OVER (ORDER BY ProductName ASC)   
     , ProductID   
     , ProductName    
     , SuggestedRetailPrice    
     , ProductImagePath    
     --, ProductMediaPath    
     , ProductShortDescription    
     , ProductLongDescription    
     --, AttributeName    
     --, DisplayValue    
     , WarrantyServiceInformation        
        
    INTO #TEMP    
    FROM     
     (    
       
     SELECT DISTINCT P.ProductID    
          , P.ProductName    
       , P.SuggestedRetailPrice    
       , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config+CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductImagePath    
               ELSE ProductImagePath END    
       --, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config+'Images/manufacturer/'+CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductMediaPath    
       --     ELSE ProductMediaPath END     
       , P.ProductShortDescription    
       , P.ProductLongDescription    
       --, PA.AttributeName    
       --, PA.DisplayValue    
       , P.WarrantyServiceInformation    
     FROM UserManufacturer UM     
     INNER JOIN Product P ON P.ManufacturerID = UM.ManufacturerID    
     AND (( P.ProductName LIKE (CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END)) OR (P.ScanCode LIKE @SearchParameter+'%'))    
     --LEFT OUTER JOIN ProductMedia PM ON PM.ProductID = P.ProductID    
     --LEFT OUTER JOIN ProductAttributes PA ON PA.ProductID = P.ProductID         
     AND UM.UserID = @UserID   
     WHERE P.ProductID <> 0)Products      
   SELECT @MaxCnt = MAX(RowNum) FROM #TEMP    
   SET @RowCount = @MaxCnt    
   SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END      
       
   SELECT RowNum   
     , ProductID      
     ,  ProductName    
     , SuggestedRetailPrice    
     , ProductImagePath    
     --, ProductMediaPath    
     , ProductShortDescription    
     , ProductLongDescription    
     --, AttributeName    
     --, DisplayValue    
     , WarrantyServiceInformation    
       
   FROM #TEMP     
   WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit    
       
   IF @RowCount IS NULL         --TO CHECK IF THE RESULT SET IS EMPTY    
    SET @RowCount = 0    
 END    
     
 --TO CHECK IF THE LOGGED IN USER IS A RETAILER    
     
 ELSE IF EXISTS(SELECT 1 FROM UserRetailer where UserID = @UserID)    
 BEGIN    
  SELECT @RetailID = RetailID    
  FROM UserRetailer WHERE UserID = @UserID    
  
  SELECT RowNum = ROW_NUMBER() OVER (ORDER BY ProductID ASC)    
  , ProductID  
     , ProductName    
     , SuggestedRetailPrice    
     , ProductImagePath    
     --, ProductMediaPath    
     , ProductShortDescription    
     , ProductLongDescription    
     --, AttributeName    
     --, DisplayValue    
     , WarrantyServiceInformation    
     --, RetailLocationID    
         
        
    INTO #TEMP1    
    FROM(      
     SELECT DISTINCT P.ProductID    
       , P.ProductName    
       , P.SuggestedRetailPrice    
       , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),ManufacturerID)+'/' + ProductImagePath    
             ELSE ProductImagePath END    
       --, ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config+'Images/manufacturer/'+CONVERT(VARCHAR(30),ManufacturerID)+'/' + ProductMediaPath    
       --      ELSE ProductMediaPath END    
       , P.ProductShortDescription    
       , P.ProductLongDescription    
       --, PA.AttributeName    
       --, PA.DisplayValue    
       , P.WarrantyServiceInformation     
       --, RL.RetailLocationID    
  FROM RetailLocation RL     
  INNER JOIN RetailLocationProduct RLP ON RL.RetailLocationID = RLP.RetailLocationID AND RL.Active = 1   
  INNER JOIN Product P ON P.ProductID = RLP.ProductID AND RL.RetailID = @RetailID   
  AND (( P.ProductName LIKE (CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END)) OR (P.ScanCode LIKE @SearchParameter+'%'))    
  --LEFT OUTER JOIN ProductAttributes PA ON PA.ProductID = P.ProductID    
  --LEFT OUTER JOIN ProductMedia PM ON PM.ProductID = P.ProductID  
  )Products           
  SELECT @MaxCnt = MAX(RowNum) FROM #TEMP1    
  SET @RowCount = @MaxCnt    
   SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END    
       
     
     
   SELECT RowNum  
  ,ProductID      
      , ProductName    
     , SuggestedRetailPrice    
     , ProductImagePath    
     --, ProductMediaPath
     , ProductShortDescription    
     , ProductLongDescription    
     --, AttributeName    
     --, DisplayValue    
     , WarrantyServiceInformation    
     --, RetailLocationID  
       
   FROM #TEMP1     
   WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit    
       
   IF @RowCount IS NULL     --TO CHECK IF THE RESULT SET IS EMPTY    
    SET @RowCount = 0    
 END    
     
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebProductSearchRetailerOrSupplier.'      
  -- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output        
  END;    
       
 END CATCH;    
END;




GO
