USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponUpdation
Purpose					: 
Example					: usp_WebRetailerCouponUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCouponUpdation]
(
	  @CouponID int
	, @CouponName varchar(100)
	, @NoOfCouponsToIssue int
	, @CouponShortDescription nvarchar(max)
	, @CouponLongDescription nvarchar(MAX)
	, @CouponTermsAndConditions varchar(1000)
	, @CouponImagePath varchar(200)
	, @CouponStartDate date
	, @CouponEndDate date
	, @CouponStartTime time
	, @CouponEndTime time
	, @CouponTimeZoneID int
	, @RetailID INT
	, @RetailLocationID varchar(1000)
	, @ProductIDs Varchar(1000)
	, @CouponExpirationDate date
	, @CouponExpirationTime time 
	, @KeyWords varchar(max)
    , @CouponDetailImage VARCHAR(1000)
	, @BannerTitle varchar(1000)
	
	--Output Variable 
	, @FindClearCacheURL varchar(1000) output
	, @PercentageFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		    DECLARE @CouponOldFaceValue Money
			DECLARE @CouponDiscountPct float
			DECLARE @RegularPrice Money
			SELECT @RegularPrice = Price
			FROM Product p 
			INNER JOIN RetailLocationProduct RLP ON P.ProductID = RLP.ProductID 
			INNER JOIN dbo.fn_SplitParam(@ProductIDs, ',') Pr ON Pr.Param = P.ProductID
				
		--	SET @CouponDiscountPct = (@CouponFaceValue * 100)/@RegularPrice
			
			--Select Coupon Current Discount VAlue
			SELECT @CouponOldFaceValue=CouponDiscountAmount  
		    FROM Coupon  
		    WHERE CouponID =@CouponID   
					
		--IF (@CouponDiscountPct <= 50)
		--BEGIN
		
		  --Check If coupon value is changed or not. 			    
		   --    IF (@CouponFaceValue<>@CouponOldFaceValue)
		   --    BEGIN	    
		    
					--SELECT DISTINCT UserID
					--	  ,ProductID
					--	  ,DeviceID
					--	  ,CouponID  	              
					--INTO #Temp
					--FROM (
					--SELECT U.UserID
					--	  ,UP.ProductID
					--	  ,UD.DeviceID 
					--	  ,CD.CouponID   
					--	  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
					--	  ,LocaleRadius 
					--from fn_SplitParam(@ProductIDs,',') F
					--INNER JOIN CouponProduct CD ON CD.ProductID =F.Param AND CD.CouponID =@CouponID         
					--INNER JOIN CouponRetailer CR ON CR.CouponID =CD.CouponID 
					--INNER JOIN RetailLocation RL ON RL.RetailLocationID =CR.RetailLocationID AND RL.Active = 1
					--INNER JOIN UserProduct UP ON UP.ProductID =CD.ProductID AND WishListItem =1	
					--INNER JOIN Users U ON U.UserID =UP.UserID
					--INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1 
					--INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					--INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
					--)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
			        
						        
					----User pushnotification already exist then NotificationSent flag updating to 0
					--UPDATE UserPushNotification SET NotificationSent=0
					--							   ,DateModified =GETDATE()			                                      
					--FROM UserPushNotification UP
					--INNER JOIN #Temp T ON UP.CouponID=T.CouponID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0
			       
		   --        --User pushnotification not exist then insert. 
				 --  INSERT INTO UserPushNotification(UserID
					--								,ProductID
					--								,DeviceID												
					--								,CouponID 												
					--								,NotificationSent
					--								,DateCreated)
					--SELECT T.UserID 
					--	  ,T.ProductID 
					--	  ,T.DeviceID 
					--	  ,T.CouponID  
					--	  ,0
					--	  ,GETDATE()
					--FROM #Temp T				
					--LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.CouponID =UP.CouponID AND T.ProductID =UP.ProductID AND DiscountCreated=0 
					--WHERE UP.UserPushNotificationID IS NULL 
			  --END
				
			UPDATE Coupon
			SET CouponName=@CouponName
               	,NoOfCouponsToIssue=@NoOfCouponsToIssue		
				,CouponDiscountPct=@CouponDiscountPct
			--	,CouponDiscountAmount=@CouponFaceValue
				,CouponShortDescription=isnull(@CouponshortDescription,@CouponLongDescription)
				,CouponLongDescription=isnull(@CouponLongDescription,@CouponshortDescription)
				,CouponTermsAndConditions=@CouponTermsAndConditions
				,CouponImagePath=@CouponImagePath
				,CouponStartDate=CAST(@CouponStartDate AS DATETIME) + CAST(ISNULL(@CouponStartTime,'') AS DATETIME)
				,CouponExpireDate=CAST(@CouponEndDate AS DATETIME) + CAST(ISNULL(@CouponEndTime,'') AS DATETIME)
				,ActualCouponExpirationDate=CAST(ISNULL(@CouponExpirationDate,NULL) AS DATETIME) + CAST(ISNULL(@CouponExpirationTime,'') AS DATETIME)
				,CouponTimeZoneID = @CouponTimeZoneID
				,WebsiteSourceFlag = 1
				,KeyWords = @KeyWords 
				,CouponDetailImage= @CouponDetailImage
				,BAnnerTitle = @BannerTitle
			WHERE CouponID=@CouponID
			
			IF(@ProductIDs IS NOT NULL)
			BEGIN
			
			 
				DELETE FROM CouponProduct WHERE CouponID = @CouponID
				
				CREATE TABLE #Prod(CouponID INT, ProductID INT)
				
				INSERT INTO CouponProduct(CouponID
										, ProductID
										, DateAdded)
				OUTPUT inserted.CouponID, inserted.ProductID INTO #Prod          										
							SELECT @CouponID
								, P.ProductID
								, GETDATE()
							FROM dbo.fn_SplitParam(@ProductIDs,',') Pr
							INNER JOIN Product P ON Pr.Param = P.ProductID
			END
			
			IF(@ProductIDs IS NULL)
			BEGIN
				DELETE FROM CouponProduct WHERE CouponID = @CouponID
			END
			
			IF(@RetailLocationID IS NOT NULL)
			BEGIN
				DELETE FROM CouponRetailer WHERE CouponID = @CouponID
				
				INSERT INTO CouponRetailer(CouponID
										 , RetailID
										 , RetailLocationID										 
										 , DateModified)
								SELECT @CouponID
									 , @RetailID
									 , R.Param
									 , GETDATE()
								FROM dbo.fn_SplitParam(@RetailLocationID,',') R
			END
			
			IF(@RetailLocationID IS NULL AND @ProductIDs IS NOT NULL)
			BEGIN
				SELECT @CouponID CouponID
						 , RetailLocationID
					INTO #RetailLocations					
					FROM 
					(SELECT distinct RL.RetailLocationID 
					FROM RetailLocationProduct RLP
					INNER JOIN #Prod P ON RLP.ProductID = P.ProductID
					INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID AND RL.Active = 1
					WHERE RL.RetailID = @RetailID) Retailer
					
					INSERT INTO CouponRetailer (
												CouponID
											  , RetailID 
											  , RetailLocationID
											  , DateCreated	
											)
									SELECT @CouponID
										 , @RetailID
										 , RetailLocationID
										 , GETDATE()
					                FROM #RetailLocations
			END
		--    SET @PercentageFlag = 1
		-- END	
		
		--ELSE 
		--BEGIN
		--	SET @PercentageFlag = 0
		--END

		-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
		------------------------------------------------
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
