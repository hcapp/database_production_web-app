USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBillingPlanDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebRetailerBillingPlanDisplay]
(

	--Input Parameter(s)--	  
	
	--Output Variable-- 
	  
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--Retrieve all the active plans.			
		SELECT RetailerBillingPlanID productId
		     , RetailerBillingPlanName productName
		     , RetailerBillingPlanDescription description
		     , RetailerBillingPlanPrice price
		     , RetailerBillingYearly 
		     , RetailerBillingMonthly 
		FROM RetailerBillingPlan 	
	    WHERE ActiveFlag = 1
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerBillingPlanDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
