USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFreeAppListingRetailerSignUpFinalization]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebFreeAppListingRetailerSignUpFinalization]
Purpose					: To register New Retailer. 
Example					: [usp_WebFreeAppListingRetailerSignUpFinalization]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			01st April 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFreeAppListingRetailerSignUpFinalization]
(	 
	 @RetailID int
	,@RetailLocationID int	
	,@RetailerURL varchar(1000)
	,@BusinessCategory varchar(1000)	
	,@RetailerKeyword Varchar(1000)	
	,@ReferralName varchar(500)
    ,@AssociateOrganizations varchar(255)
    
	--Input Variables of RetailContact
	,@ContactFirstName Varchar(20)
	,@ContactLastName Varchar(30)
	,@ContactPhone Char(10)
	
	--Input Variables of Users--
	,@UserName varchar(100)
	,@Password varchar(60)
	,@ContactEmail Varchar(100)

	--Input Variables of UserRetailer--
	--,@AdminFlag Bit	
	,@CorporateAndSore Bit
	,@StoreIdentification Varchar(20)
	
	--For Retailer Filter
	,@AdminFilterID varchar(max)
	,@AdminFilterValueID varchar(max)

	--For Sub-Category
	,@SubCategoryID varchar(max)
						
	--Output Variable
	, @ResponseUserID int output
	, @DuplicateFlag bit output 	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS 
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
		--To fetch Retailer filter Categories
		DECLARE	@FilterBusinessCategory varchar(1000) 

		SELECT P.Param
		INTO #FilterCategory
		FROM BusinessCategory BC
		INNER JOIN fn_SplitParam(@BusinessCategory,',') P ON BC.BusinessCategoryID = P.Param
		WHERE BusinessCategoryName in ('Bars','Dining') AND @AdminFilterID IS NOT NULL

		   IF EXISTS (SELECT 1 FROM Users WHERE UserName = @UserName)
			BEGIN
				SET @DuplicateFlag=1
				--SET @DuplicateRetailerFlag=0
				SET @Status=0			
			END
			ELSE
			BEGIN				
				--INSERT INTO USERS TABLE
				INSERT INTO Users
							(UserName
							,Password
							,DateCreated)
					VALUES (@UserName
						   ,@Password
						   ,GETDATE())				
						
				
				SET @ResponseUserID=SCOPE_IDENTITY();	
						   
			
				--UPDATE INTO RETAILER TABLE
				
				UPDATE Retailer SET RetailURL = @RetailerURL									
									,FreeApplistingReferralName = @ReferralName
									,AssociateOrganizations = @AssociateOrganizations
									,FreeApplistingRetailer = 1
				WHERE RetailID=@RetailID
			 
			--Insert into RetailerBusinessCategory table			
			   
				--INSERT INTO RetailerBusinessCategory(RetailerID
				--						, BusinessCategoryID
				--						, DateCreated)
				--				   SELECT @RetailID
				--						, [Param]
				--						, GETDATE()		                            
				--				   FROM dbo.fn_SplitParam(@BusinessCategory, ',')

			SELECT @SubCategoryID = REPLACE(@SubCategoryID, 'NULL', '0')  

			--Capture sub-CategoryIDs
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 ,BusCategoryID = Param
			INTO #BusCatIds
			FROM dbo.fn_SplitParamMultiDelimiter(@BusinessCategory, ',')                     
	
			SELECT RowNum = IDENTITY(INT, 1, 1)
				, SubCategoryID = Param
			INTO #SubCatIds
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCategoryID, '!~~!')  

			SELECT RowNum = IDENTITY(INT, 1, 1)
					,B.BusCategoryID
					,SubcatID =  F.Param 
			INTO #Buscat_SubCat 
			FROM #BusCatIds B
			INNER JOIN #SubCatIds S ON B.RowNum = S.RowNum 
			CROSS APPLY (select Param from fn_SplitParam(S.SubCategoryID,'|') ) F
				

			--Insert into RetailerBusinessCategory table.

			IF CURSOR_STATUS('global','CategoryList')>=-1
			BEGIN
			DEALLOCATE CategoryList
			END
                                                
			DECLARE @BusCategoryID1 int
			DECLARE @SubcatID1 int
			DECLARE CategoryList CURSOR STATIC FOR
			SELECT BusCategoryID
				  ,SubcatID
			FROM #Buscat_SubCat 
                           
			OPEN CategoryList
                           
			FETCH NEXT FROM CategoryList INTO @BusCategoryID1, @SubcatID1 
                           
			CREATE TABLE #TempBusCat(BusinnessCategoryID INT)

			WHILE @@FETCH_STATUS = 0
			BEGIN   
                            
				INSERT INTO #TempBusCat(BusinnessCategoryID) 			                                
				OUTPUT inserted.BusinnessCategoryID INTO #TempBusCat(BusinnessCategoryID)
							SELECT F.[Param]						     
				FROM dbo.fn_SplitParam(@BusCategoryID1, ',') F

				INSERT INTO RetailerBusinessCategory(RetailerID
												, BusinessCategoryID
												, DateCreated
												, BusinessSubCategoryID)
					OUTPUT inserted.BusinessCategoryID INTO #TempBusCat(BusinnessCategoryID)
											SELECT @RetailID
												, IIF(@BusCategoryID1= 0,NULL, @BusCategoryID1)
												, GETDATE()
												, IIF([Param] = 0,NULL,[Param])
											FROM dbo.fn_SplitParam(@SubcatID1, '|')

					
							DECLARE @Cnts INT 
							SELECT @Cnts = 0
							SELECT @Cnts = COUNT(Param)
							FROM dbo.fn_SplitParam(@SubcatID1, '|')
                                   
                               
				TRUNCATE TABLE #TempBusCat
		
				FETCH NEXT FROM CategoryList INTO @BusCategoryID1, @SubcatID1
                                  
			END                        

			CLOSE CategoryList
			DEALLOCATE CategoryList
			                      
				--UPDATE INTO RETAILLOCATION TABLE
				UPDATE RetailLocation SET Headquarters = CASE WHEN @CorporateAndSore = 1 THEN 0 ELSE 1 END
										 ,CorporateAndStore = @CorporateAndSore
										 ,StoreIdentification = @StoreIdentification
										  ,RetailLocationURL = @RetailerURL
				WHERE RetailID=@RetailID
											  			
				
				--INSERT INTO CONTACT TABLE
						   
				INSERT INTO [Contact]
						   ([ContactFirstName]
						   ,[ContactLastname]
						   ,[ContactTitle]					  
						   ,[ContactPhone]
						   ,[ContactEmail]
						   ,[DateCreated]
						   ,[CreateUserID])
				VALUES(@ContactFirstName
					   ,@ContactLastName
					   ,1				 
					   ,@ContactPhone
					   ,@ContactEmail
					   ,GETDATE()
					   ,@ResponseUserID)
					   
				DECLARE @ContactID int
				SET @ContactID=SCOPE_IDENTITY();
					   
				--INSERT INTO RETAILCONTACT TABLE	
										   
				INSERT INTO [RetailContact]
						   ([ContactID]
						   ,[RetailLocationID])
				VALUES(@ContactID
					   ,@RetailLocationID)
					   
				--INSERT INTO USERRETAILER CHILD TABLE			
					   
				INSERT INTO UserRetailer
						   ([RetailID]
						   ,[UserID])
						   --,[AdminFlag])
				VALUES(@RetailID
					   ,@ResponseUserID)
					   --,@AdminFlag)
											   
	         
			   --INSERT INTO RETAILERKEYWORDS TABLE
	       
			   INSERT INTO RetailerKeywords 
					   (RetailID
						,RetailKeyword
						,DateCreated
						,RetailLocationID)
			   VALUES(@RetailID
					  ,@RetailerKeyword 
					  ,GETDATE()
					  ,@RetailLocationID)
					  
			
				--Insert into HcRetailerAssociation table.			
				INSERT INTO HcRetailerAssociation(HcHubCitiID
												,RetailID
												,RetailLocationID
												,DateCreated
												,CreatedUserID
												,Associated) 
								SELECT DISTINCT HcHubcitiID 
												  ,@RetailID
												  ,@RetailLocationID
												  ,GETDATE()
												  ,@ResponseUserID 
												  ,0
								FROM HcLocationAssociation LA
								INNER JOIN RetailLocation RL ON LA.City = RL.City AND LA.State = RL.State AND LA.PostalCode = RL.PostalCode 
								WHERE RL.RetailLocationID = @RetailLocationID
			     				AND RL.Headquarters = 0 		         
			
			--Retailer filter changes
			IF (SELECT DISTINCT 1 FROM #FilterCategory) > 0
			BEGIN

			SELECT @FilterBusinessCategory = COALESCE(@FilterBusinessCategory + '!~~!' , '') + CAST(Param AS varchar(100))
			FROM  #FilterCategory

			SELECT @AdminFilterID = REPLACE(@AdminFilterID, 'NULL', '0')  
			SELECT @AdminFilterValueID = REPLACE(@AdminFilterValueID, 'NULL', '0')

			--Capture Category and filter ID's
			SELECT RowNum = IDENTITY(INT, 1, 1)
					,CategoryID = Param
			INTO #CategoryID
			FROM dbo.fn_SplitParamMultiDelimiter(@FilterBusinessCategory, '!~~!')                     
	
			SELECT RowNum = IDENTITY(INT, 1, 1)
					, FilterId = Param
			INTO #FilterID
			FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterID, '!~~!')  
				
			 SELECT RowNum = IDENTITY(INT, 1, 1)
					, FilterValueId = Param
			INTO #FilterValueID
			FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterValueID, '!~~!')  
				  	
			SELECT RowNum = IDENTITY(INT, 1, 1)
					,C.CategoryID
					,FilterID =  F.Param 
			INTO #cat_filterValues 
			FROM #CategoryID C
			INNER JOIN #FilterID S ON C.RowNum = S.RowNum 
			CROSS APPLY (select Param from fn_SplitParam(S.FilterId,'|') ) F
						
			SELECT CategoryID
				  ,	FilterID
				  , FilterValueId
			INTO #Final_filtervalues
			FROM #cat_filterValues V
			INNER JOIN #FilterValueID I on V.RowNum = I.RowNum

			--Insert into Retailer Filter Association table.

				IF CURSOR_STATUS('global','CategoryList')>=-1
				BEGIN
				DEALLOCATE CategoryList
				END
                                                
				DECLARE @CategoryID1 int
				DECLARE @FilterID1 int
				DECLARE @FilterValueID1 varchar(1000)
				DECLARE CategoryList CURSOR STATIC FOR
				SELECT CategoryID
					  ,FilterId
					  ,FilterValueId          
				FROM #Final_filtervalues 
                           
				OPEN CategoryList
                           
				FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                           
				CREATE TABLE #TempBiz(BusinnessCategoryID INT)

				WHILE @@FETCH_STATUS = 0
				BEGIN   
                            
						INSERT INTO #TempBiz(BusinnessCategoryID) 			                                
						OUTPUT inserted.BusinnessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT F.[Param]						     
						FROM dbo.fn_SplitParam(@CategoryID1, ',') F


						INSERT INTO RetailerFilterAssociation(RetailID
															  ,AdminFilterID
															  ,BusinessCategoryID
															  ,AdminFilterValueID
															  ,DateCreated
															  ,CreatedUserID)
									OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinnessCategoryID)
									SELECT @RetailID
											,IIF(@FilterID1 = 0,NULL, @FilterID1)
											,@CategoryID1
											,IIF([Param] = 0,NULL,[Param])
											,GETDATE()
											,@ResponseUserID
									FROM dbo.fn_SplitParam(@FilterValueID1, '|')
					
									DECLARE @Cnt INT 
									SELECT @Cnt = 0
									SELECT @Cnt = COUNT(Param)
									FROM dbo.fn_SplitParam(@FilterValueID1, '|')
                                   
                               
						TRUNCATE TABLE #TempBiz
		
						FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                                  
				END                        

				CLOSE CategoryList
				DEALLOCATE CategoryList
			END
			  
			--Confirmation of Success
	        SET @DuplicateFlag=0	
			SET @Status = 0		
					
		 END
		 
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebFreeAppListingRetailerSignUpFinalization].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
