USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListLoyaltyGallery]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_MasterShoppingListLoyaltyGallery]
Purpose					: to get loyalty deals from the gallery
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListLoyaltyGallery]
(
	
	 @ProductID int
	, @RetailID int
	, @UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	    --To Fetch LoyaltyDeal Information from user loyalty gallery for the product with retailer info.
		IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)
		BEGIN
		
			SELECT RetailLocationID 
			INTO #Ret
			FROM RetailLocation 
			WHERE RetailID = @RetailID
			 
			SELECT UL.LoyaltyDealID  
				,LoyaltyDealName
				,LoyaltyDealDiscountType
				,LoyaltyDealDiscountAmount
				,LoyaltyDealDiscountPct
				,LoyaltyDealDescription
				,LoyaltyDealDateAdded
				,LoyaltyDealStartDate
				,LoyaltyDealExpireDate
			FROM LoyaltyDeal L
			INNER JOIN #Ret R ON R.RetailLocationID = L.RetailLocationID 
			INNER JOIN UserLoyaltyGallery UL ON UL.LoyaltyDealID=l.LoyaltyDealID
			WHERE ProductID = @ProductID 
			AND LoyaltyDealExpireDate >= GETDATE()
			AND UserID=@Userid
		END
		
		IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS  NULL and @RetailID = 0)
		BEGIN
			 
			SELECT UL.LoyaltyDealID  
				,LoyaltyDealName
				,LoyaltyDealDiscountType
				,LoyaltyDealDiscountAmount
				,LoyaltyDealDiscountPct
				,LoyaltyDealDescription
				,LoyaltyDealDateAdded
				,LoyaltyDealStartDate
				,LoyaltyDealExpireDate
			FROM LoyaltyDeal L
			INNER JOIN UserLoyaltyGallery UL ON UL.LoyaltyDealID=l.LoyaltyDealID
			WHERE ProductID = @ProductID 
			AND LoyaltyDealExpireDate >= GETDATE()
			AND UserID=@Userid
		END
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListLoyaltyGallery.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
