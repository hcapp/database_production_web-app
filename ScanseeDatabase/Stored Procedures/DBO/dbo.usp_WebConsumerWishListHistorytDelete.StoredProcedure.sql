USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerWishListHistorytDelete]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerWishListHistorytDelete]
Purpose					: To delete a Wish List history item.
Example					: [usp_WebConsumerWishListHistorytDelete] 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th Aug 2013	Dhananjaya TR   Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerWishListHistorytDelete]
(
	  @UserProductID Varchar(max)
	, @UserID int
	
	--Output Variable
	, @Result bit output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
					 
		    --Swipe delete from history table.

					DELETE FROM UserProductHistory
					FROM UserProductHistory UH
					INNER JOIN fn_SplitParam(@UserProductID,',') F ON F.Param =UH.UserProductID 
					WHERE UserID = @UserID												
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerWishListHistorytDelete].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
