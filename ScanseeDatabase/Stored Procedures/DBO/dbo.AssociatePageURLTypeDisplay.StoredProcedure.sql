USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[AssociatePageURLTypeDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_APIWishPondInputCity
Purpose					: To Pass city as the input for APIWishPondURL.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0						Sagar Byali
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[AssociatePageURLTypeDisplay]
(
	
	--Output Variable 

	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			SELECT
				  AssociatePageURLTypeID associatePageURLtypeID
				, AssociatePageURLTypeName associatePageURLName
			FROM AssociatePageURLType 

			SET @Status = 0
			
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN

			SET @Status = 1

			PRINT 'Error occured in Stored Procedure usp_APIWishPondInputCity.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
		 
	END CATCH;
END;
GO
