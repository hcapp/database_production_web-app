USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListDisplayUserZipCodeCheck]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListDisplayGpsOff
Purpose					: 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16th Dec 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListDisplayUserZipCodeCheck]
(
	@UserID int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			SELECT PostalCode zipcode
				  , ZipCodeExists=Case when Postalcode IS NULL THEN 0 Else 1 END
			FROM Users
			WHERE UserID=@UserID
		

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListDisplayGpsOff.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
