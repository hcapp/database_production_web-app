USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierDiscountPlanDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebSupplierDiscountPlanDisplay]
(

	--Input Input Parameter(s)--  	   
	  
	  @DiscountCode varchar(10)
	  
	--Output Variable--
	  
	
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		SELECT D.ManufacturerDiscountPlanID
		     , D.ManufacturerDiscountPlanCode
			 , D.DiscountPrice discount
			 , D.ManufacturerDiscountPlanDescription 
		FROM ManufacturerDiscountPlan D
		WHERE ManufacturerDiscountPlanCode = @DiscountCode	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierDiscountPlanDisplay.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
