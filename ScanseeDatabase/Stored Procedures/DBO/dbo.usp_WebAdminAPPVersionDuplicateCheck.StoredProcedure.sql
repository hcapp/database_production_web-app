USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminAPPVersionDuplicateCheck]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: [usp_WebAdminAPPVersionDuplicateCheck]  
Purpose					: Veify Vesrin already exist.  
Example					: [usp_WebAdminAPPVersionDuplicateCheck]  
  
History  
Version  Date				Author			 Change	Description  
---------------------------------------------------------------   
1.0		 5th Sep 2013   	Dhananjaya TR	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebAdminAPPVersionDuplicateCheck]  
(  
   @VersionNumber Varchar(100) 
  ,@AppType Varchar(255) 
   
 --OutPut Variable   
 , @VersionExist Bit Output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
  BEGIN TRY  
  
  
  --Verify if the Version already exist in database
  SET @VersionExist =0
  IF EXISTS (SELECT 1 FROM APPVersionManagement AV
                      INNER JOIN ApplicationType AT ON AV.ApplicationTypeID=AT.ApplicationTypeID 
                      WHERE VersionNumber =@VersionNumber AND AT.Type =@AppType)
  BEGIN
     SET @VersionExist =1
  END   
     
  END TRY      
 BEGIN CATCH    
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebAdminAPPVersionDuplicateCheck].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output       
  END;  
     
 END CATCH;  
END;


GO
