USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetailLocationUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetailLocationUpdation
Purpose					: To update the Retail location details.
Example					: usp_WebRetailerAdminRetailLocationUpdation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/03/2014			Pavan Sharma K		Initial Version
1.1			20/07/2016			Bindu T A			Changes w.r.t Updating Retail details when Headquaters/ Corp&Store
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetailLocationUpdation]  

	--Input parameters	 
	  @RetailID INT
	, @RetailLocationID INT	
	, @StoreIdentification varchar(100)
	, @Address varchar(1000)
	, @City varchar(255)
	, @State char(2)
	, @PostalCode varchar(20)
	, @WebsiteURL varchar(1000)
	, @Latitude float
	, @Longitude float
	, @Phone varchar(100) 
	, @UserID int
	, @Retaillocationimagepath varchar(2000)

	--Output variables 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		 
		 DECLARE @RetailLocAssociatedFlag Int	

		 SET @RetailLocAssociatedFlag =0	
		
		IF (@PostalCode<>(Select PostalCode From RetailLocation  Where RetailLocationID =@RetailLocationID AND Active = 1))
		BEGIN
			SET @RetailLocAssociatedFlag =1
		END

		DECLARE @CityID INT
		DECLARE @StateID INT

		SELECT @CityID = HcCityID
		FROM HcCity WHERE cityname = @City

		SELECT @StateID = Stateid 
		FROM State WHERE Stateabbrevation = @State

		--Update the Retail Location information.
		UPDATE RetailLocation SET Address1 = @Address
								,City = @City
								,State = @State
								,PostalCode = @PostalCode
								,CountryID = 1
								,RetailLocationLatitude = @Latitude
								,RetailLocationLongitude = @Longitude
								,DateModified = GETDATE()
								,StoreIdentification = @StoreIdentification
								,RetailLocationURL = @WebsiteURL
								,RetailLocationImagePath =@Retaillocationimagepath
								,HcCityID = @CityID
								,StateID = @StateID
		WHERE RetailLocationID = @RetailLocationID AND Active = 1
				
		--Update the contact phone information of the retail location.
		IF EXISTS(SELECT 1 FROM Contact C
				  INNER JOIN RetailContact RC ON RC.ContactID = C.ContactID AND RC.RetailLocationID = @RetailLocationID)
		BEGIN
			UPDATE Contact SET ContactPhone = @Phone
			FROM Contact C
			INNER JOIN RetailContact RC ON RC.ContactID = C.ContactID AND RC.RetailLocationID = @RetailLocationID

			UPDATE RetailContact
			SET ContactPhone = @Phone
			WHERE RetailLocationID = @RetailLocationID

		END
		ELSE
		BEGIN
			DECLARE @ContactID int

			INSERT INTO Contact(ContactPhone
							   ,DateCreated
							   ,CreateUserID)
				SELECT @Phone
					  ,GETDATE()	
					  ,@UserID	

			SET @ContactID = SCOPE_IDENTITY()

			INSERT INTO RetailContact(ContactID
									 ,RetailLocationID)
							SELECT   @ContactID
								    ,@RetailLocationID
		END

		IF EXISTS (SELECT 1 FROM Retailer R INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID
		WHERE RL.RetailLocationID = @RetailLocationID AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)AND RL.Active=1 )
		BEGIN 
			UPDATE Retailer 
			SET CorporatePhoneNo = C.ContactPhone ,
			Address1 = RL.Address1,
			City = RL.City,
			State = RL.State,
			PostalCode = RL.PostalCode
			FROM RetailLocation RL 
			INNER JOIN Retailer R ON R.retailID = RL.RetailID
			INNER JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
			INNER JOIN Contact C ON C.ContactID = RC.ContactID
			WHERE RL.RetailLocationID = @RetailLocationID AND R.RetailID = @RetailID
		END


		--Refresh the association w.r.t the Retail location

		IF NOT EXISTS (Select 1 From RetailLocation  Where RetailLocationID =@RetailLocationID AND Active = 1)
		BEGIN
				DELETE FROM HcRetailerAssociation 
				FROM HcRetailerAssociation A
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = A.RetailLocationID AND RL.Active = 1
				WHERE RL.RetailLocationID = @RetailLocationID
				AND RL.Headquarters = 0  AND A.Associated = 1
		END


		----Associate the retailer to the Hub Citi.
		INSERT INTO HcRetailerAssociation(HcHubCitiID
									,RetailID
									,RetailLocationID
									,DateCreated
									,CreatedUserID
									,Associated) 
					SELECT DISTINCT  LA.HcHubCitiID
										,@RetailID
										,@RetailLocationID
										,GETDATE()
										,@UserID 
										,0
					FROM HcLocationAssociation LA
					INNER JOIN RetailLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode AND RL.Active = 1
					LEFT JOIN HcRetailerAssociation RA ON RA.RetailLocationID = @RetailLocationID AND LA.HcHubCitiID = RA.HcHubCitiID 
					WHERE  RA.HcRetailerAssociationID IS NULL
					AND RL.RetailLocationID = @RetailLocationID
			     	AND RL.Headquarters = 0 

		 IF (@RetailLocAssociatedFlag  =1)
		 BEGIN
			UPDATE HcRetailerAssociation set Associated =0 Where RetailLocationID =@RetailLocationID  					  
		 END
 
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetailLocationUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
