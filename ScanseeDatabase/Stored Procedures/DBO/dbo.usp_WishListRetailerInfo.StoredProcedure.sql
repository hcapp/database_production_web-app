USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListRetailerInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListRetailerInfo
Purpose					: 
Example					: usp_WishListRetailerInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListRetailerInfo]
(
	@UserID int
	,@ProductID int
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)    
	, @ZipCode varchar(10)    
	, @Radius int
	
	--Output Variable 	 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--If not location services 
		IF(@Latitude IS NULL AND @Longitude IS NULL AND @ZipCode IS NULL)
		BEGIN
			SELECT ProductID
				, R.RetailID 
				, RL.RetailLocationID
				, R.RetailName retailerName
				, RL.Address1 retaileraddress1
				, RL.City 
				, RL.State 
				, RL.PostalCode 
				, NULL Distance 
				, RD.SalePrice 
				, RD.Price
			FROM RetailLocationDeal RD
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = RD.RetailLocationID 
				INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND R.RetailerActive = 1
			WHERE RD.ProductID = @ProductID AND RL.Active = 1
			AND GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
		END
		
		ELSE
		BEGIN
		--While User search by Zipcode, coordinates are fetched from GeoPosition table.    
		IF (@Latitude IS NULL AND @Longitude IS NULL )    
		BEGIN    
			SELECT @Latitude = Latitude     
			  , @Longitude = Longitude     
			FROM GeoPosition     
			WHERE PostalCode = @ZipCode     
		END    
		--If radius is not passed, getting user preferred radius from UserPreference table.    
		IF @Radius IS NULL     
		BEGIN     
			SELECT @Radius = LocaleRadius  
			FROM dbo.UserPreference WHERE UserID = @UserID    
		END

		--To get user's locationwise retail store infomation
		SELECT ProductID
			   , RetailID 
			   , RetailLocationID
			   , RetailName retailerName
			   , Address1 retaileraddress1
			   , City 
			   , State 
			   , PostalCode 
			   , Distance
			   , SalePrice
			   , Price
		FROM (		
				SELECT UP.ProductID
					, R.RetailID 
					, RL.RetailLocationID
					, R.RetailName 
					, RL.Address1 
					, RL.City 
					, RL.State 
					, RL.PostalCode 
					, Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214     
					, RP.SalePrice 
					, RP.Price 
				FROM UserProduct UP
					INNER JOIN RetailLocationProduct RP ON RP.ProductID = UP.ProductID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID 
					INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND R.RetailerActive = 1
				WHERE UP.UserID = @UserID 
					AND UP.WishListItem = 1
					AND UP.ProductID = @ProductID AND RL.Active = 1
			 ) D
		WHERE Distance <= ISNULL(@Radius, 5)
		
		END
		----To get Retailer Info.
		-- SELECT RP.Price price
		--		, RP.SalePrice 
		--		, R.RetailName retailerName
		--		, RL.Address1 retaileraddress1
		--		, RL.City 
		--		, RL.State 
		--		, RL.PostalCode 
		-- FROM RetailLocationProduct RP
		--	INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID
		--	INNER JOIN Retailer r on r.RetailID = rl.RetailID  
		-- WHERE ProductID = @ProductID 
		--	AND GETDATE() BETWEEN SaleStartDate AND SaleEndDate 
		--	AND ISNULL(RP.SalePrice, 0) <> 0
		
		--To diabale PushNotification flag (If user view the sale info then the pushnotification should be disbale for the product of the user)	
		UPDATE UserProduct 
		SET PushNotifyFlag = 0 
		WHERE UserID = @UserID 
			AND ProductID = @ProductID 
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListRetailerInfo.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
