USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierProfileUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierProfileUpdation
Purpose					: To Update the profile of the Existing Suppliers/ Manufacturers.
Example					: usp_WebSupplierProfileUpdation

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			6th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierProfileUpdation]
(

	--Input Parameter(s)--
	  
	  @ManufacturerID int	
	, @UserID int 
    , @CompanyName varchar(100)
    , @CorporateAddress varchar(50)
    , @Address2 varchar(50)
    , @City varchar(30)
    , @PostalCode varchar(10)
    , @State varchar(20)
    , @PhoneNumber varchar(10)
    , @ContactFirstName varchar(30)
    , @ContactLastName varchar(30)
    , @ContactPhoneNumber varchar(10)
    , @ContactEmail	varchar(100)
    , @IsNonProfitOrganisation bit
    , @BusinessCategoryIDs varchar(1000)
	  
	
	--Output Variable--
	, @ResponseManufacturer int output  
	, @Status int output
	, @DuplicateSupplier bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @ContactID INT	
			DECLARE @SupplierName  VARCHAR(100) 			
			
			SELECT @SupplierName = ManufName
			FROM Manufacturer
			WHERE ManufacturerID = @ManufacturerID
			
			--Check if the user is changing the Manufacturer Name.
		    IF @SupplierName <> @CompanyName
		    BEGIN
				--Check if the manufacturer name already exists.
				IF NOT EXISTS(SELECT 1 FROM Manufacturer WHERE ManufName = @CompanyName)
				BEGIN
					--Update the Manufacturer Table.				
					UPDATE Manufacturer SET ManufName = @CompanyName
										  , Address1 = @CorporateAddress
										  , Address2 = @Address2
										  , City = @City
										  , PostalCode = @PostalCode
										  , [State] = @State
										  , CorporatePhoneNo = @PhoneNumber
										  , IsNonProfitOrganisation = @IsNonProfitOrganisation
										  , DateModified = GETDATE()
										  , ModifyUserID = @UserID
					WHERE ManufacturerID = @ManufacturerID
				
					SELECT @ContactID = ContactID FROM ManufacturerContact WHERE ManufacturerID = @ManufacturerID
					
					--Update ManufacturerCategory Table.
					
					DELETE FROM ManufacturerCategory WHERE ManufacturerID = @ManufacturerID                  --Delete existing categories and insert fresh set.
					
					INSERT INTO ManufacturerCategory(ManufacturerID
												   , CategoryID
												   , DateAdded)
										SELECT @ManufacturerID
											 , [Param]
											 , GETDATE()
										FROM dbo.fn_SplitParam(@BusinessCategoryIDs, ',')
					
					--Update Contact Table.
				
					UPDATE Contact SET ContactFirstName = @ContactFirstName
									  , ContactLastname = @ContactLastName
									  , ContactPhone = @ContactPhoneNumber
									  , ContactEmail = @ContactEmail
					WHERE ContactID = @ContactID	
				 	
				 	--Confirm no Duplication of Supplier Name.
				    SET @DuplicateSupplier = 0			
			END
			ELSE
			BEGIN
				--Confirm Duplication of Supplier Name.
				SET @DuplicateSupplier = 1
			END
		  END
		  
		  --If the user is not changing the Manufacturer Name.
		  ELSE IF @SupplierName = @CompanyName
		  BEGIN
			--Update the Manufacturer Table.				
					UPDATE Manufacturer SET ManufName = @CompanyName
										  , Address1 = @CorporateAddress
										  , Address2 = @Address2
										  , City = @City
										  , PostalCode = @PostalCode
										  , [State] = @State
										  , CorporatePhoneNo = @PhoneNumber
										  , IsNonProfitOrganisation = @IsNonProfitOrganisation
										  , DateModified = GETDATE()
					WHERE ManufacturerID = @ManufacturerID
				
					SELECT @ContactID = ContactID FROM ManufacturerContact WHERE ManufacturerID = @ManufacturerID
					
					--Update ManufacturerCategory Table.
					
					DELETE FROM ManufacturerCategory WHERE ManufacturerID = @ManufacturerID                  --Delete existing categories and insert fresh set.
					
					INSERT INTO ManufacturerCategory(ManufacturerID
												   , CategoryID
												   , DateAdded)
										SELECT @ManufacturerID
											 , [Param]
											 , GETDATE()
										FROM dbo.fn_SplitParam(@BusinessCategoryIDs, ',')
					
					--Update Contact Table.
				
					UPDATE Contact SET ContactFirstName = @ContactFirstName
									  , ContactLastname = @ContactLastName
									  , ContactPhone = @ContactPhoneNumber
									  , ContactEmail = @ContactEmail
					WHERE ContactID = @ContactID	
				 	
				 	--Confirm No Duplication of Manufacturer Name.		 
				    SET @DuplicateSupplier = 0
		  END
		  
		  --Confirmation of Success.		
		  SELECT @Status = 0
		  SET @ResponseManufacturer = @ManufacturerID 
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierProfileUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
