USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFiltersDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRetailerFiltersDisplay]
Purpose					: To Display list of Filters for given Category
Example					: [usp_WebRetailerFiltersDisplay]

History
Version		Date		    Author		Change Description
-----------------------------------------------------------------
1.0			11/28/2014		Span			1.0
-----------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerFiltersDisplay]
(

	--Input Variable 
	  @CategoryID Varchar(1000)
	  	
	--Output Variable 	
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		SELECT  BC.BusinessCategoryID 
			  , BC.BusinessCategoryDisplayValue  BusinessCategoryName
			  , A.AdminFilterID filterId
			  , A.FilterName
			  , FV.AdminFilterValueID filterValueId
			  , V.FilterValueName
		FROM AdminFilter A
		INNER JOIN AdminFilterCategoryAssociation FC ON A.AdminFilterID = FC.AdminFilterID
		INNER JOIN BusinessCategory BC ON FC.BusinessCategoryID = BC.BusinessCategoryID
		LEFT JOIN AdminFilterValueAssociation FV ON A.AdminFilterID = FV.AdminFilterID
		LEFT JOIN AdminFilterValue V ON FV.AdminFilterValueID = V.AdminFilterValueID
		WHERE FC.BusinessCategoryID IN (SELECT Param FROM fn_SplitParam(@CategoryID,','))
		ORDER BY BC.BusinessCategoryID,A.FilterName,V.FilterValueName 
	
		--Confirmation of Success
		SET @Status = 0

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerFiltersDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status = 1			
		END;
		 
	END CATCH;
END;





GO
