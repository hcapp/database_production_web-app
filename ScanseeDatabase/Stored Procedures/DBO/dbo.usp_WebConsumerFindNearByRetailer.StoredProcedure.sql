USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerFindNearByRetailer]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerFindNearByRetailer  
Purpose               : To get near by retailers list.  
Example               : usp_WebConsumerFindNearByRetailer   
  
History  
Version		Date		Author			Change Description  
---------------------------------------------------------------   
 1.0	23rd May 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------  
*/  
  
 CREATE PROCEDURE [dbo].[usp_WebConsumerFindNearByRetailer]  
(  
   @UserID INT  
 , @Latitude decimal(18,6)  
 , @Longitude decimal(18,6)  
 , @PostalCode varchar(10)  
 , @ProductId int  
 , @Radius int 
 , @LowerLimit int
 , @ScreenName varchar(50)
 
 --User Tracking
 ,@MainMenuID int
    
 --Output Variable   
 , @MaxCnt int  output
 , @NxtPageFlag bit output  
 , @Result int OUTPUT  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	  
	  DECLARE @Config varchar(50)
	  SELECT @Config=ScreenContent
	  FROM AppConfiguration 
	  WHERE ConfigurationType='App Media Server Configuration'

	  DECLARE @RetailConfig varchar(50)
	  SELECT @RetailConfig=ScreenContent
	  FROM AppConfiguration 
	  WHERE ConfigurationType='Web Retailer Media Server Configuration'
			
	  --To get the row count for pagination.  
	  DECLARE @UpperLimit int   
	  SELECT @UpperLimit = @LowerLimit + ScreenContent   
	  FROM AppConfiguration   
	  WHERE ScreenName = @ScreenName 
	  AND ConfigurationType = 'Pagination'
	  AND Active = 1   
   
	--Latitude and longitude values are null, but Postal Code is passed.  
	  IF (@Latitude IS NULL OR @Longitude IS NULL) AND @PostalCode IS NOT NULL  
	  BEGIN  
			SELECT @Latitude = Latitude   
				, @Longitude = Longitude   
			FROM GeoPosition   
			WHERE PostalCode = @PostalCode   
	  END  
    
	  --Latitude, longitude and Postal Code values are not passed.  
	  IF (@Latitude IS NULL OR @Longitude IS NULL) AND @PostalCode IS NULL  
	  BEGIN  
		   SELECT @PostalCode = PostalCode FROM Users WHERE UserID = @UserID  
		   IF @PostalCode IS NOT NULL  
		   BEGIN  
				SELECT @Latitude = Latitude, @Longitude = Longitude   
				FROM GeoPosition   
				WHERE PostalCode = @PostalCode  
		   END  
		   ELSE   
		   BEGIN  
			SELECT @Result = -1  
		   END  	     
	  END    

	  --To fetch User Preferred Radius.  
	  IF @Radius IS NULL  
	  BEGIN  
	   SELECT @Radius=LocaleRadius 
	   FROM UserPreference  
	   WHERE UserID =@UserID 	    
	  END  
    
	  SELECT RowNum = IDENTITY(INT, 1, 1)
		, RetailID   
		, RetailName   
		, RetailLocationID 
		, Address   
		, ContactMobilePhone   
		, RetailURL   
		, Distance   
		, ProductId   
		, ProductName  
		, ProductImagePath   
		, Price  
		, SalePrice 
		, UserID   
		, RetailLocationLatitude   
		, RetailLocationLongitude   
	  INTO #RetailerLists 
	  FROM   
	  (  
	   SELECT R.RetailID   
		, R.RetailName
		, RL.RetailLocationID   
		, RL.Address1 +','+isnull(RL.Address2,'')+ isnull(RL.Address3,'')+isnull(RL.Address4,'') +RL.City +','+ RL.State+ ',' +RL.PostalCode Address  
		, RC.ContactMobilePhone   
		, R.RetailURL  
		, Distance = (ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
		, P.ProductId   
		, P.ProductName  
		, ProductImagePath  = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																								THEN @ManufConfig
																								+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																								+ProductImagePath ELSE ProductImagePath 
																						  END   
							  ELSE ProductImagePath END  
		, RP.Price  
		, RP.SalePrice 
		, @UserID  AS UserID  
		, RL.RetailLocationLatitude   
		, RL.RetailLocationLongitude    
	   FROM Product P  
	   INNER JOIN RetailLocationProduct RP ON RP.ProductID = P.ProductID   
	   INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID  
	   LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID    
	   INNER JOIN Retailer R ON R.RetailID = RL.RetailID  
	   WHERE P.ProductID = @ProductId   
	  )Retailer   
	  WHERE Distance <= ISNULL(@Radius, 5)
	  ORDER BY Price, Distance  

	--To capture max row number.  
	 SELECT @MaxCnt = MAX(RowNum) FROM #RetailerLists
	 
	 --this flag is a indicator to enable "More" button in the UI.   
	 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
	 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

	 SELECT RowNum  
		, RetailID   
		, RetailName   
		, RetailLocationID 
		, Address   
		, ContactMobilePhone   
		, RetailURL   
		, Distance   
		, ProductId   
		, ProductName  
		, ProductImagePath   
		, Price  
		, SalePrice 
		, UserID   
		, RetailLocationLatitude   
		, RetailLocationLongitude
	 INTO #RetList
	 FROM #RetailerLists
	 WHERE RowNum BETWEEN @LowerLimit + 1 AND @UpperLimit
 
 
	 --User Tracking
	 
	 CREATE TABLE #Temp(RetailerListID int,RetailLocationID int)
	 
	 INSERT INTO ScanSeeReportingDatabase..RetailerList(MainMenuID
													   ,RetailID
													   ,RetailLocationID
													   ,CreatedDate)
	  OUTPUT inserted.RetailerListID,inserted.RetailLocationID INTO #Temp(RetailerListID,RetailLocationID)	
	  											   
	  SELECT @MainMenuID 
			 ,RetailID  
			 ,RetailLocationID
			 ,GETDATE()
	  FROM #RetailerLists       
	 
	 
	  SELECT T.RetailerListID retListID
	   , RetailID retailerId      
	   , RetailName retailerName  
	   , R.RetailLocationID retLocId
	   , Address   
	   , ContactMobilePhone phone  
	   , RetailURL retailerUrl  
	   , Distance distance  
	   , ProductId productId  
	   , ProductName  
	   , ProductImagePath imagePath  
	   , Price productPrice 
	   , SalePrice 
	   , UserID   
	   , RetailLocationLatitude latitude  
	   , RetailLocationLongitude longitude  
	  FROM #RetList R
	  INNER JOIN #Temp T ON R.RetailLocationID = T.RetailLocationID  
 
 END TRY 
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerFindNearByRetailer.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
