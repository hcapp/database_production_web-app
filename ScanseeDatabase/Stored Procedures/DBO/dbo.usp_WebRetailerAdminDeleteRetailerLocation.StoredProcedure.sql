USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminDeleteRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminDeleteRetailerLocation
Purpose					: To delete single/list of Retailer Locations from the system.
Example					: usp_WebRetailerAdminDeleteRetailerLocation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			16th July 2013			SPAN			Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerAdminDeleteRetailerLocation]

	--Input parameters	
	  @RetailLocationID VARCHAR(MAX)	
	, @UserID INT

	--Output variables	
	, @FindClearCacheURL VARCHAR(1000) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
	
			--Update statement is added for the Logical delete implementaion 
			--and rest of the delete statements are commented.
			UPDATE RetailLocation SET Active=0
			FROM RetailLocation RL 
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID,',') RLD ON RL.RetailLocationID=RLD.Param;

			SELECT Param RetailLocationID, RL.CorporateAndStore
			INTO #RetailLocation
			FROM dbo.fn_SplitParam(@RetailLocationID,',') F
			INNER JOIN RetailLocation RL ON RL.RetailLocationID = F.Param
			
			--The below delete statements are commented with /* */ symbols.
			/*INSERT INTO RetailLocationDeletionHistory(RetailLocationID
												,RetailID
												,UserID
												,Headquarters
												,Address1
												,Address2
												,Address3
												,Address4
												,City
												,State
												,PostalCode
												,CountryID
												,RetailLocationLatitude
												,RetailLocationLongitude
												,RetailLocationTimeZone
												,DateCreated
												,DateModified
												,RetailLocationStoreHours
												,StoreIdentification
												,WishPondRetailID
												,OnlineStoreFlag
												,CorporateAndStore
												,RetailLocationURL
												,DuplicateFlag
												,FreeAppRetailerID)		
			SELECT								RetailLocationID
												,RetailID
												,@UserID
												,Headquarters
												,Address1
												,Address2
												,Address3
												,Address4
												,City
												,State
												,PostalCode
												,CountryID
												,RetailLocationLatitude
												,RetailLocationLongitude
												,RetailLocationTimeZone
												,GETDATE()
												,DateModified
												,RetailLocationStoreHours
												,StoreIdentification
												,WishPondRetailID
												,OnlineStoreFlag
												,CorporateAndStore
												,RetailLocationURL
												,DuplicateFlag
												,FreeAppRetailerID																			
			FROM RetailLocation RL	
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID,',') F ON RL.RetailLocationID = F.Param
			
			DECLARE @Count INT
			DECLARE @Count1 INT			
						
			--Ad's
			SELECT QR.QRRetailerCustomPageID
			INTO #QRRetailerCustomPage
			FROM QRRetailerCustomPage QR
			INNER JOIN QRRetailerCustomPageAssociation QC ON QR.QRRetailerCustomPageID=QC.QRRetailerCustomPageID			
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = QC.RetailLocationID
			
			SELECT DISTINCT QR.QRRetailerCustomPageID
			INTO #DelQRRetCustPage
			FROM QRRetailerCustomPage QR
			INNER JOIN QRRetailerCustomPageAssociation QC ON QR.QRRetailerCustomPageID=QC.QRRetailerCustomPageID			
			INNER JOIN #QRRetailerCustomPage F ON F.QRRetailerCustomPageID = QC.QRRetailerCustomPageID
			WHERE QC.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))	
			
			--Banner
			SELECT RA.AdvertisementBannerID
			INTO #AdvertisementBanner
			FROM AdvertisementBanner RA
			INNER JOIN RetailLocationBannerAd RL ON RA.AdvertisementBannerID=RL.AdvertisementBannerID
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = RL.RetailLocationID	
			
			SELECT DISTINCT RA.AdvertisementBannerID
			INTO #DelAdvertisementBanner
			FROM AdvertisementBanner RA
			INNER JOIN RetailLocationBannerAd RL ON RA.AdvertisementBannerID=RL.AdvertisementBannerID
			INNER JOIN #AdvertisementBanner F ON F.AdvertisementBannerID = RL.AdvertisementBannerID	
			WHERE RL.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))	
			
			--Splash
			SELECT RA.AdvertisementSplashID
			INTO #AdvertisementSplash
			FROM AdvertisementSplash RA
			INNER JOIN RetailLocationSplashAd RL ON RA.AdvertisementSplashID=RL.AdvertisementSplashID
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = RL.RetailLocationID	
			
			SELECT RA.AdvertisementSplashID
			INTO #DelAdvertisementSplash
			FROM AdvertisementSplash RA
			INNER JOIN RetailLocationSplashAd RL ON RA.AdvertisementSplashID=RL.AdvertisementSplashID
			INNER JOIN #AdvertisementSplash F ON F.AdvertisementSplashID = RL.AdvertisementSplashID	
			WHERE RL.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))	
			
			
			--Coupon
			SELECT C.CouponID
			INTO #Coupon
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID= CR.CouponID
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = CR.RetailLocationID
			
			SELECT DISTINCT C.CouponID
			INTO #DelCoupon
			FROM Coupon C
			INNER JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			INNER JOIN #Coupon F ON Cr.CouponID = F.CouponID
			WHERE CR.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))
						
			--Rebate			
			SELECT C.RebateID
			INTO #Rebate
			FROM Rebate C
			INNER JOIN RebateRetailer CR ON C.RebateID= CR.RebateID
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = CR.RetailLocationID
			
			SELECT DISTINCT C.RebateID
			INTO #DelRebate
			FROM Rebate C
			INNER JOIN RebateRetailer CR ON C.RebateID = CR.RebateID
			INNER JOIN #Rebate F ON Cr.RebateID = F.RebateID
			WHERE CR.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))
			
			--Loyalty			
			SELECT C.LoyaltyProgramID
			INTO #LoyaltyProgram
			FROM LoyaltyProgram C
			INNER JOIN LoyaltyDeal CR ON C.LoyaltyProgramID= CR.LoyaltyProgramID
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = CR.RetailLocationID
			
			SELECT DISTINCT C.LoyaltyProgramID
			INTO #DelLoyalty
			FROM LoyaltyProgram C
			INNER JOIN LoyaltyDeal CR ON C.LoyaltyProgramID = CR.LoyaltyProgramID
			INNER JOIN #LoyaltyProgram F ON Cr.LoyaltyProgramID = F.LoyaltyProgramID
			WHERE CR.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))
												
		    --HotDeal
			SELECT P.ProductHotDealID
			INTO #HotDeal
			FROM ProductHotDeal P
			INNER JOIN ProductHotDealRetailLocation PL ON P.ProductHotDealID=PL.ProductHotDealID
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = PL.RetailLocationID
			
			SELECT DISTINCT  C.ProductHotDealID
			INTO #DelHotDeal
			FROM ProductHotDeal C
			INNER JOIN ProductHotDealRetailLocation CR ON C.ProductHotDealID=CR.ProductHotDealID
			INNER JOIN #HotDeal F ON F.ProductHotDealID=CR.ProductHotDealID
			WHERE CR.RetailLocationID NOT IN (SELECT Param FROM dbo.fn_SplitParam(@RetailLocationID, ','))
						

			--Add's
			
			SELECT RetailLocationAdvertisementID
			INTO #RetailLocationAdvertisement
			FROM RetailLocationAdvertisement RA
			INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') F ON F.Param = RA.RetailLocationID
			
			
			DELETE FROM UserAdvertisementHit
			FROM UserAdvertisementHit U
			INNER JOIN #RetailLocationAdvertisement R ON R.RetailLocationAdvertisementID=U.RetailLocationAdvertisementID	
			
			--SELECT RetailLocationAdvertisementID
			--INTO #RetailLocationAdvertisement1
			--FROM RetailLocationAdvertisement RA
			--INNER JOIN #RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
			
			--SET @Count = @@ROWCOUNT	
						
			--IF @Count=1
			--BEGIN
			--	DELETE FROM RetailLocationAdvertisement
			--	FROM RetailLocationAdvertisement RA
			--	INNER JOIN #RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
			--END	
			
			
			--DELETE FROM UserAdvertisementHit
			--FROM UserAdvertisementHit RA
			--INNER JOIN RetailLocationAdvertisement AD ON AD.RetailLocationAdvertisementID =RA.RetailLocationAdvertisementID 
			--INNER JOIN RetailLocation RL ON AD.RetailLocationID=RL.RetailLocationID
			--INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
   --         print'RetailLocationAdvertisement'
			--DELETE FROM RetailLocationAdvertisement
			--FROM RetailLocationAdvertisement RA
			--INNER JOIN #RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID			
			
			print'RetailLocationBannerAd'
			DELETE FROM RetailLocationBannerAd
			FROM RetailLocationBannerAd RA
			INNER JOIN #RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
						
			DELETE FROM AdvertisementBanner
			FROM AdvertisementBanner C
			INNER JOIN #AdvertisementBanner Q ON C.AdvertisementBannerID=Q.AdvertisementBannerID
			WHERE C.AdvertisementBannerID NOT IN(SELECT AdvertisementBannerID FROM #DelAdvertisementBanner)
			
			--print'AdvertisementBanner'
			--DELETE FROM AdvertisementBanner
			--FROM AdvertisementBanner RA
			----INNER JOIN #Retailer R ON R.RetailID=RA.RetailID
			
			print'RetailLocationSplashAd'
			DELETE FROM RetailLocationSplashAd
			FROM RetailLocationSplashAd RA
			INNER JOIN #RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
						
			DELETE FROM AdvertisementSplash
			FROM AdvertisementSplash C
			INNER JOIN #AdvertisementSplash Q ON C.AdvertisementSplashID=Q.AdvertisementSplashID
			WHERE C.AdvertisementSplashID NOT IN(SELECT AdvertisementSplashID FROM #DelAdvertisementSplash)
			
			
			--print'AdvertisementSplash'
			--DELETE FROM AdvertisementSplash
			--FROM AdvertisementSplash RA
			----INNER JOIN #Retailer R ON R.RetailID=RA.RetailID
			
	
			--Retailer data deletion

			--DELETE FROM RetailCategory
			--FROM RetailCategory R
			--INNER JOIN #Retailer RR ON R.RetailID=RR.RetailID

			--DELETE FROM ProductReviews
			--FROM ProductReviews P
			--INNER JOIN #Retailer RR ON P.RetailID=RR.RetailID

			--DELETE FROM UserRetailPreference
			--FROM UserRetailPreference U
			--INNER JOIN #Retailer R ON U.RetailID=R.RetailID

			--DELETE FROM RetailerBankInformation
			--from RetailerBankInformation RB
			--INNER JOIN #Retailer R ON R.RetailID=RB.RetailerID

			--DELETE FROM RetailerBillingDetails
			--FROM RetailerBillingDetails RB
			--INNER JOIN #Retailer R ON R.RetailID=RB.RetailerID

			DELETE FROM RetailLocationProduct
			FROM RetailLocationProduct PA
			INNER JOIN #RetailLocation RL ON PA.RetailLocationID=RL.RetailLocationID

			DELETE FROM UserPushNotification
			FROM UserPushNotification PA
			INNER JOIN RetailLocationDeal RD ON PA.RetailLocationDealID = RD.RetailLocationDealID
			INNER JOIN #RetailLocation RL ON RD.RetailLocationID=RL.RetailLocationID

					
			DELETE FROM RetailLocationDeal
			FROM RetailLocationDeal PA
			INNER JOIN #RetailLocation RL ON PA.RetailLocationID=RL.RetailLocationID
								
			--Considered for Retailer Login.
			DELETE FROM RetailContact
			FROM RetailContact R
			INNER JOIN #RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID
			WHERE RL.CorporateAndStore = 0
			
			----Product related tables

			--DELETE FROM RebateProduct
			--FROM RebateProduct PA
			--INNER JOIN RebateRetailer RR ON PA.RebateID=RR.RebateID
			--inner join #Retailer R ON R.RetailID=RR.RetailID
		

			DELETE FROM ProductUserHit
			FROM ProductUserHit PA
			INNER JOIN #RetailLocation RL ON rl.RetailLocationID=PA.RetailLocationID			
		
			DELETE FROM CouponRetailer
			FROM CouponRetailer CP
			INNER JOIN #RetailLocation RL ON CP.RetailLocationID=RL.RetailLocationID		
			
			DELETE FROM UserCouponGallery
			FROM UserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 	
			
			DELETE FROM HcUserCouponGallery    
			FROM HcUserCouponGallery CG
			INNER JOIN #Coupon C ON C.CouponID = CG.CouponID 		
			
			DELETE FROM CouponProduct
			FROM CouponProduct CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID
									
			DELETE FROM Coupon
			FROM Coupon C
			INNER JOIN #Coupon Q ON C.CouponID=Q.CouponID
			WHERE C.CouponID NOT IN(SELECT CouponID FROM #DelCoupon)
												

			----Rebate related tables

			DELETE FROM RebateRetailer
			FROM RebateRetailer  RP
			INNER JOIN #RetailLocation RL ON RP.RetailLocationID=RL.RetailLocationID
			
			DELETE FROM UserRebateGallery
			WHERE RetailLocationID IN(SELECT RetailLocationID FROM #RetailLocation)
			
			DELETE FROM RebateProduct
			FROM RebateProduct CP
			INNER JOIN #Rebate C ON C.RebateID = CP.RebateID
			
			DELETE FROM Rebate
			FROM Rebate C
			INNER JOIN #Rebate Q ON C.RebateID=Q.RebateID
			WHERE C.RebateID NOT IN(SELECT RebateID FROM #DelRebate)

			DELETE FROM ScanHistory
			WHERE RetailLocationID IN(SELECT RetailLocationID FROM #RetailLocation)
			
			--Loyalty			
			
			DELETE FROM LoyaltyDeal
			FROM LoyaltyDeal  RP
			INNER JOIN #RetailLocation RL ON RP.RetailLocationID=RL.RetailLocationID
			
			DELETE FROM LoyaltyProgram
			FROM LoyaltyProgram C
			INNER JOIN #LoyaltyProgram Q ON C.LoyaltyProgramID=Q.LoyaltyProgramID
			WHERE C.LoyaltyProgramID NOT IN(SELECT LoyaltyProgramID FROM #DelLoyalty)
		
			
			--Pages
			DELETE FROM QRRetailerCustomPageAssociation 
			From QRRetailerCustomPageAssociation QR
			INNER JOin #RetailLocation R ON R.RetailLocationID =QR.RetailLocationID 
			
			DELETE FROM QRUserRetailerSaleNotification 
			From QRUserRetailerSaleNotification QR
			INNER JOin #RetailLocation R ON R.RetailLocationID = QR.RetailLocationID 
			
			--print'QRRetailerCustomPageMedia'
			--DELETE FROM QRRetailerCustomPageMedia
			--From QRRetailerCustomPageMedia QR
			--INNER JOIN #QRRetailerCustomPage QC ON QR.QRRetailerCustomPageID=QC.QRRetailerCustomPageID
			
			DELETE FROM QRRetailerCustomPageMedia
			FROM QRRetailerCustomPageMedia QC
			INNER JOIN #QRRetailerCustomPage QR ON QC.QRRetailerCustomPageID=QR.QRRetailerCustomPageID
			WHERE QC.QRRetailerCustomPageID NOT IN(SELECT QRRetailerCustomPageID FROM #DelQRRetCustPage)
			
			print'UserGiveawayAssociation'
			DELETE FROM UserGiveawayAssociation
			From UserGiveawayAssociation QR
			INNER JOIN #QRRetailerCustomPage QC ON QR.QRRetailerCustomPageID=QC.QRRetailerCustomPageID
						
			DELETE FROM QRRetailerCustomPage
			FROM QRRetailerCustomPage QC
			INNER JOIN #QRRetailerCustomPage QR ON QC.QRRetailerCustomPageID=QR.QRRetailerCustomPageID
			WHERE QC.QRRetailerCustomPageID NOT IN(SELECT QRRetailerCustomPageID FROM #DelQRRetCustPage)
									
			DELETE FROM RebateRetailer
			From RebateRetailer QR
			INNER JOin #RetailLocation R ON R.RetailLocationID = QR.RetailLocationID 		
			
			--Considered for Retailer Login.
			DELETE FROM RetailerKeywords
			From RetailerKeywords QR
			INNER JOin #RetailLocation R ON R.RetailLocationID =QR.RetailLocationID 
			WHERE R.CorporateAndStore = 0
			
			DELETE FROM UserHotDealGallery
			From UserHotDealGallery QR
			INNER JOin #HotDeal R ON R.ProductHotDealID =QR.HotDealID 

			DELETE FROM HcUserHotDealGallery
			From HcUserHotDealGallery QR
			INNER JOin #HotDeal R ON R.ProductHotDealID =QR.HotDealID 
			
			
			--DELETE FROM UserCouponGallery
			--From UserCouponGallery QR
			--INNER JOin #RetailLocation R ON R.RetailLocationID =QR.RetailLocationID 
			
			
			DELETE FROM UserRebateGallery
			From UserRebateGallery QR
			INNER JOin #RetailLocation R ON R.RetailLocationID =QR.RetailLocationID 

			
			--DELETE FROM UserRetailer
			--From UserRetailer QR
			--INNER JOin #RetailLocation R ON R.RetailLocationID =QR.RetailLocationID 
			
			----ProductHotDeal related tables.

			DELETE FROM ProductHotDealRetailLocation
			WHERE RetailLocationID IN(SELECT RetailLocationID FROM #RetailLocation)
			--FROM ProductHotDealRetailLocation P
			--INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 
						
			
			DELETE FROM AffiliateRetailer
			FROM AffiliateRetailer RL
			INNER JOIN #RetailLocation RR ON RL.RetailLocationID=RR.RetailLocationID
			
			
			DELETE FROM GroupRetailer
			FROM GroupRetailer RL
			INNER JOIN #RetailLocation RR ON RL.RetailLocationID=RR.RetailLocationID				
			

					
			

			----Delete Data from Hubciti related Tables

			-- Delte from Appsite related data from Hubciti

			SELECT DISTINCT HcAppSiteID 
			INTO #Appsites
			FROM HcAppSite A
			INNER JOIN #RetailLocation R ON R.RetailLocationID =A.RetailLocationID
            
			SELECT E.HcEventID 
			INTO #Events 
			FROM HcEventAppsite E
			INNER JOIN #Appsites A ON A.HcAppSiteID =E.HcAppsiteID

			DELETE FROM HcRetailerEventsAssociation
			FROM HcRetailerEventsAssociation HR
			INNER JOIN #RetailLocation RL ON HR.RetailLocationID = RL.RetailLocationID

			DELETE FROM HcEventAppsite 
			FROM HcEventAppsite E
			INNER JOIN #Appsites A ON A.HcAppSiteID =E.HcAppsiteID			

			DELETE FROM HcEventInterval 
			FROM HcEventInterval I
			INNER JOIN #Events E ON E.HcEventID =I.HcEventID 
			LEFT JOIN HcEventAppsite A ON A.HcEventID =E.HcEventID 
			WHERE A.HcEventID IS NULL

			DELETE FROM HcEventLocation
			FROM HcEventLocation I
			INNER JOIN #Events E ON E.HcEventID =I.HcEventID 
			LEFT JOIN HcEventAppsite A ON A.HcEventID =E.HcEventID 
			WHERE A.HcEventID IS NULL

			DELETE FROM HcEventPackage 
			FROM HcEventPackage I
			INNER JOIN #Events E ON E.HcEventID =I.HcEventID 
			LEFT JOIN HcEventAppsite A ON A.HcEventID =E.HcEventID 
			WHERE A.HcEventID IS NULL

			DELETE FROM HcEventsCategoryAssociation  
			FROM HcEventsCategoryAssociation I
			INNER JOIN #Events E ON E.HcEventID =I.HcEventID 
			LEFT JOIN HcEventAppsite A ON A.HcEventID =E.HcEventID 
			WHERE A.HcEventID IS NULL

			--F1	
			
			SELECT DISTINCT FA.HcAppSiteID 
			INTO #HcFundAppsites
			FROM HcFundraisingAppsiteAssociation FA
			INNER JOIN HcAppSite A ON FA.HcAppsiteID = A.HcAppSiteID
			INNER JOIN #RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID	

			DELETE FROM HcFundraisingAppsiteAssociation
			FROM HcFundraisingAppsiteAssociation I
			INNER JOIN #HcFundAppsites E ON E.HcAppsiteID =I.HcAppsiteID

			
			DELETE FROM HcRetailerFundraisingAssociation
			FROM HcRetailerFundraisingAssociation HR
			INNER JOIN #RetailLocation R ON HR.RetailLocationID = R.RetailLocationID


			DELETE FROM HcAppSite
			FROM HcAppSite A 
			INNER JOIN #RetailLocation R ON R.RetailLocationID =A.RetailLocationID


			--Delete from HcCityExperienceRetailLocation


			DELETE FROM HcCityExperienceRetailLocation
			FROM HcCityExperienceRetailLocation C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

            
			DELETE FROM HcFilterRetailLocation 
			FROM HcFilterRetailLocation C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HCProductUserHit  
			FROM HCProductUserHit C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HcRetailerAssociation   
			FROM HcRetailerAssociation C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HcRetailerSubCategory    
			FROM HcRetailerSubCategory C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			--DELETE FROM HcUserCouponGallery    
			--FROM HcUserCouponGallery C
			--INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID


			DELETE FROM RetailLocation
			WHERE RetailLocationID IN(SELECT RetailLocationID FROM #RetailLocation WHERE CorporateAndStore = 0)
			
			DELETE FROM HotDealProduct
			FROM HotDealProduct PA
			INNER JOIN #HotDeal P on P.ProductHotDealID=PA.ProductHotDealID					
			
			
			DELETE FROM ProductHotDeal
			FROM ProductHotDeal QC
			INNER JOIN #HotDeal QR ON QC.ProductHotDealID=QR.ProductHotDealID
			WHERE QC.ProductHotDealID NOT IN(SELECT ProductHotDealID FROM #DelHotDeal)					
			
		
			
			--SELECT RL.ProductHotDealID
			--INTO #ProductHotDeal
			--FROM ProductHotDeal RL
			--INNER JOIN #HotDeal RR ON RL.ProductHotDealID=RR.ProductHotDealID
			
			--SET @Count3 = @@ROWCOUNT
			
			--select @count3
			
			--IF @Count3=0
			--BEGIN
			--	DELETE FROM ProductHotDeal
			--	FROM ProductHotDeal RL
			--	INNER JOIN #HotDeal RR ON RL.ProductHotDealID=RR.ProductHotDealID
			--END	

            --SELECT UserID 
            --INTO #UserID
            --FROM UserRetailer 
			--WHERE RetailID IN (SELECT RetailID FROM #Retailer)

			--DELETE FROM UserRetailer 
			--WHERE RetailID IN (SELECT RetailID FROM #Retailer)

            --DELETE FROM RetailCategory_BAK   
			--WHERE RetailID IN (SELECT RetailID FROM #Retailer)

            --DELETE FROM RetailerBusinessCategory 
			--WHERE RetailerID IN (SELECT RetailID FROM #Retailer)

			--DELETE FROM Retailer 
			--WHERE RetailID IN (SELECT RetailID FROM #Retailer)

			--DELETE FROM Users 
			--FROM Users U
			--INNER JOIN #UserID US ON US.UserID =U.UserID 
			--WHERE UserID=@UserID*/
			
			--Set the CorporateAndStore flag to 0 when the user is deleting the isLocation flagged Location.
			UPDATE RetailLocation SET CorporateAndStore = 0, Headquarters = 1, Active = 1
			FROM RetailLocation RL
			INNER JOIN #RetailLocation R ON R.RetailLocationID = RL.RetailLocationID
			WHERE R.CorporateAndStore = 1 	

			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------

		--Confirmation of Success.  
		SELECT @Status = 0
		COMMIT TRANSACTION
		
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminDeleteRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
