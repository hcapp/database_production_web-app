USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_fetchTutorialMediaInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_TodayList
Purpose					: To fetch Tutorial Media Information.
Example					:  usp_fetchTutorialMediaInfo

History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			6th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_fetchTutorialMediaInfo]

AS
BEGIN

	BEGIN TRY
	
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='App Media Server Configuration'
		
		SELECT TutorialID tutorialID 
		     , TutorialName tutorialName
			 , @Config+TutorialMediaPath tutorialMediaPath
			 , @Config+TutorialImagePath TutorialImagePath
			 , TutorialVideoDuration tutVidDuration
		FROM TutorialMedia
		ORDER BY TutorialSort	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_TodayList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			
		END;
		 
	END CATCH;
END;

GO
