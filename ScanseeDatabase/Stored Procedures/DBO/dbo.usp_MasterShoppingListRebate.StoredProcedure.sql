USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListRebate]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_MasterShoppingListRebate  
Purpose     : To display Rebate.  
Example     : usp_MasterShoppingListRebate 1,0,14  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   7th June 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_MasterShoppingListRebate]  
(  
   @ProductID int  
 , @RetailID int  
 , @UserID int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	--To get Server Configuration
	 DECLARE @ManufConfig varchar(50) 
	  
	 SELECT @ManufConfig = ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
	  
  --To get Image of the product  
   DECLARE @ProductImagePath varchar(1000)  
   SELECT @ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END    
   FROM Product   
   WHERE ProductID = @ProductID  
    
   --To get the row count for pagination.  
    
   DECLARE @UpperLimit int   
   SELECT @UpperLimit = @LowerLimit + ScreenContent   
   FROM AppConfiguration   
   WHERE ScreenName = @ScreenName   
    AND ConfigurationType = 'Pagination'  
    AND Active = 1  
   DECLARE @MaxCnt int   
     
  DECLARE @Rebate table (Row_Num int,rebateId int  
    , manufacturerId int  
    , manufacturerName varchar(75)   
    , rebateAmount money  
    , rebateStartDate date  
    , rebateEndDate date  
    , retailId int  
    , rebateName varchar(100)  
    , usage varchar(10))  
 --To fetch Rebate info of product with Retailer info.  
  IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)  
  BEGIN  
   SELECT Row_Num= ROW_NUMBER() over(ORDER BY R.RebateID)  
    , R.RebateID  
    , RP.ProductID      
    , R.ManufacturerID   
    , M.ManufName   
    , RebateAmount   
    , RebateStartDate   
    , RebateEndDate   
    , RR.RetailID  
    , RebateName   
   INTO #Rebate  
   FROM Rebate R  
    INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID   
    INNER JOIN RebateRetailer RR ON RR.RebateID=R.RebateID  
    LEFT JOIN Manufacturer M ON M.ManufacturerID = R.ManufacturerID   
   WHERE RR.RetailID = @RetailID   
   AND RP.ProductID = @ProductID   
   AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate  
     
    --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #Rebate  
   --this flag is a indicator to enable "More" button in the UI.   
   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
     
   INSERT INTO @Rebate  
   SELECT Row_Num rowNum  
    , R.RebateID rebateId  
    , R.ManufacturerID manufacturerId  
    , R.ManufName manufacturerName  
    , R.RebateAmount rebateAmount  
    , R.RebateStartDate rebateStartDate  
    , R.RebateEndDate rebateEndDate  
    , R.RetailID retailId  
    , R.RebateName rebateName  
    , usage = CASE WHEN UR.UserRebateGalleryID IS NULL THEN 'Red' ELSE 'Green' END  
   FROM #Rebate R  
    LEFT JOIN UserRebateGallery UR ON UR.RebateID = R.RebateID AND UR.UserID = @UserID  
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   ORDER BY Row_Num  
     
   --SELECT RetailLocationID, RetailID    
   --INTO #Ret  
   --FROM RetailLocation      
   --WHERE RetailID = @RetailID   
     
   --SELECT DISTINCT RetailID   
   --INTO #Retail  
   --FROM #Ret R  
   --INNER JOIN UserRebateGallery UG ON UG.RetailLocationID = R.RetailLocationID   
   --WHERE UserID = @UserID   
     
   --SELECT RebateID rebateId  
   -- , R.ManufacturerID manufacturerId  
   -- , R.ManufName  
   -- , R.RebateAmount rebateAmount  
   -- , R.RebateStartDate rebateStartDate  
   -- , R.RebateEndDate rebateEndDate  
   -- , R.RetailID retailId  
   -- , RebateName  
   -- , usage = CASE WHEN RT.RetailID IS NULL THEN 'Red' ELSE 'Green' END  
   --FROM #Rebate R  
   --LEFT JOIN #Retail RT ON RT.RetailID = R.RetailID      
  END  
  --To fetch Rebate info of Product from "Others" Category.  
  IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS NULL OR @RetailID =0)   
   AND @ProductID IS NOT NULL  
  BEGIN  
    
   SELECT Row_Num= ROW_NUMBER() over(ORDER BY R.RebateID)  
    , R.RebateID  
    , RP.ProductID  
    , R.ManufacturerID   
    , M.ManufName   
    , RebateAmount   
    , RebateStartDate   
    , RebateEndDate   
    , NULL AS RetailID  
    , RebateName   
   INTO #ProdRebate  
   FROM Rebate R  
    INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID   
    LEFT JOIN Manufacturer M ON M.ManufacturerID = R.ManufacturerID   
   WHERE RP.ProductID = @ProductID   
   AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate  
     
    --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #ProdRebate  
   --this flag is a indicator to enable "More" button in the UI.   
   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
     
   INSERT INTO @Rebate  
   SELECT Row_Num rowNum  
    , R.RebateID rebateId  
    , R.ManufacturerID manufacturerId  
    , R.ManufName manufacturerName  
    , R.RebateAmount rebateAmount  
    , R.RebateStartDate rebateStartDate  
    , R.RebateEndDate rebateEndDate  
    , R.RetailID retailId  
    , R.RebateName rebateName  
    , usage = CASE WHEN UR.UserRebateGalleryID IS NULL THEN 'Red' ELSE 'Green' END  
   FROM #ProdRebate R  
   LEFT JOIN UserRebateGallery UR ON UR.RebateID = R.RebateID AND  UR.UserID = @UserID   
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   ORDER BY Row_Num  
     
  END  
  SELECT Row_Num
    , rebateId   
    , manufacturerId   
    , manufacturerName   
    , rebateAmount   
    , rebateStartDate   
    , rebateEndDate   
    , retailId   
    , rebateName  
    , usage 
    , @ProductImagePath imagePath 
  FROM @Rebate  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_MasterShoppingListRebate.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
