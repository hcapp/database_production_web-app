USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerProductImagesDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	 : usp_WebConsumerProductImagesDisplay  
Purpose					 : To dispaly Product images 
Example					 : usp_WebConsumerProductImagesDisplay 
  
History  
Version  Date				Author		Change Description  
---------------------------------------------------------------   
1.0		 29th April 2013	Pavan Sharma K	Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerProductImagesDisplay]  
(  

  @ProductID int  
   
 --Output Variable   
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	BEGIN TRANSACTION
 
   --To get Media Server Configuration. 
	DECLARE @ManufConfig varchar(50) 
	DECLARE @MediaType  INT
			  
	SELECT @ManufConfig = ScreenContent  
	FROM AppConfiguration   
	WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	
	SELECT @MediaType = ProductMediaTypeID
	FROM ProductMediaType
	WHERE ProductMediaType = 'Image Files'		
	
	SELECT  ProductImagePath = CASE WHEN WebsiteSourceFlag = 1 
								THEN @ManufConfig + CAST(ManufacturerID AS VARCHAR(10)) + '/' + ProductImagePath
							ELSE ProductImagePath END
	FROM Product 
	WHERE ProductID = @ProductID
	
	UNION ALL
	
	SELECT CASE WHEN P.WebsiteSourceFlag = 1 
								THEN @ManufConfig + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + PM.ProductMediaPath
							ELSE PM.ProductMediaPath END
	FROM ProductMedia PM
	INNER JOIN Product P ON P.ProductID = PM.ProductID
	WHERE P.ProductID = @ProductID
	AND ProductMediaTypeID = @MediaType

	
	
	
	--Confirmation of Success.
	SELECT @Status = 0
   COMMIT TRANSACTION
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerProductImagesDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION; 
   --Confirmation of failure.
   SELECT @Status = 1 
  END;  
     
 END CATCH;  
END;


GO
