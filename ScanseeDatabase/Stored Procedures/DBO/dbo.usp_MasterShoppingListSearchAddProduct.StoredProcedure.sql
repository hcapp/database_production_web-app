USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListSearchAddProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_MasterShoppingListSearchAddProduct  
Purpose     : To search for the product.  
Example     : EXEC usp_MasterShoppingListSearchAddProduct   
EXEC usp_MasterShoppingListSearchAddProduct 1, '5,6', '6/10/2011'  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   7th June 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_MasterShoppingListSearchAddProduct]  
(  
   @UserID int  
 , @ProductID varchar(max)  
 , @MasterListAddDate datetime  
   
 --Output Variable   
 , @ProductExists int output
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
  
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
   -- To fetch Product which already exists for the user (Active and Inactive Products).  
   SELECT UP.UserID   
     , UP.ProductID
     , MasterListItem   
   INTO #PROD  
   FROM UserProduct UP  
    INNER JOIN fn_SplitParam (@ProductID, ',') P ON P.Param = UP.ProductID  
   WHERE UserID = @UserID   
    AND UserRetailPreferenceID IS NULL  
    
   --To get inactive product to make it active	
	SELECT UserID 
			, ProductID
			, MasterListItem
	INTO #InActivePROD 
	FROM #PROD 
	WHERE MasterListItem = 0
	
	--To set @ProductExists = 1. If the product is  already active for the User.	
	SELECT @ProductExists = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
	FROM #PROD 
	WHERE MasterListItem = 1    
    
    
   --To fetch Product which doesnot exist for the user.   
   SELECT UP.UserID   
     , P.Param    
   INTO #NOPROD  
   FROM UserProduct UP  
    RIGHT JOIN fn_SplitParam (@ProductID, ',') P ON P.Param = UP.ProductID   
                AND UP.UserID = @UserID   
                AND UserRetailPreferenceID IS NULL  
   WHERE UP.UserID IS NULL  
      
   --If Product exists  
   IF EXISTS (SELECT TOP 1 ProductID FROM #InActivePROD)  
   BEGIN  
    UPDATE UserProduct   
    SET MasterListItem = 1  
     ,MasterListAddDate = @MasterListAddDate  
    FROM UserProduct UP  
     INNER JOIN #InActivePROD P ON P.ProductID = UP.ProductID AND P.UserID = UP.UserID  
    WHERE UserRetailPreferenceID IS NULL  
   END  
   --If product doesnot exists  
   IF EXISTS (SELECT TOP 1 Param FROM #NOPROD)  
   BEGIN  
    INSERT INTO [UserProduct]  
         ([UserID]  
         ,[ProductID]  
         ,[MasterListItem]  
         ,[MasterListAddDate]  
         ,[WishListItem]  
         ,[TodayListtItem]  
         ,[ShopCartItem])  
    SELECT  @UserID  
      , Param    
      , 1  
      , @MasterListAddDate  
      , 0  
      , 0  
      , 0  
    FROM #NOPROD  
   END  
   --Confirmation of Success.  
   SELECT @Status = 0  
  COMMIT TRANSACTION  
    
  --To return affected UserProductID.  
  SELECT UserProductID userProductId  
  FROM UserProduct UP  
   INNER JOIN fn_SplitParam (@ProductID, ',') P ON P.Param = UP.ProductID  
  WHERE UserID = @UserID   
   --AND MasterListAddDate = @MasterListAddDate  --Commented this because Server team want to, as they need UserProductID in all the Cases.
   AND UserRetailPreferenceID IS NULL  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_MasterShoppingListSearchAddProduct.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;

GO
