USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerHotDealSearchPopulationCenter]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerHotDealPopulationCentres
Purpose					: To Display DMA Names
Example					: usp_WebConsumerHotDealPopulationCentres

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			13th June 2013			Dhananjaya TR	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerHotDealSearchPopulationCenter]
(

	--Input Input Parameter(s)-- 	
	   @UserID int	
	 , @PopulationCenter varchar(100)
	--Output Variable--	  
	  
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
			
		DECLARE @CategorySet bit
		DECLARE @Today DATE = GETDATE()
		
		--Check if the User has selected the any categories
		SELECT @CategorySet = CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END
		FROM UserCategory 
		WHERE UserID = @UserID 		
		
		--If the User has configured any categories then consider it.
		IF @CategorySet = 1
		BEGIN
			--To get all the Dma names.
			SELECT DISTINCT P.PopulationCenterID
						  , P.DMAName
						  , hotdealsCount = CAST(ISNULL(C.Cnt, 0) + (SELECT COUNT(DISTINCT P.ProductHotDealID) 
														  FROM ProductHotDeal P
														  LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
													      WHERE PL.ProductHotDealID IS NULL
													      AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
													      AND ((P.CategoryID IS NULL AND 1 = 1)
															OR (P.CategoryID IS NOT NULL AND P.CategoryID IN (SELECT CategoryID FROM UserCategory WHERE UserID = @UserID)))													      
														  AND GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE()-1) AND ISNULL(HotDealEndDate, GETDATE()+1))
												AS VARCHAR(10)) + ' Deals'
			FROM PopulationCenters P
			LEFT JOIN ( 
				 SELECT PCC.PopulationCenterID, COUNT(DISTINCT PH.ProductHotDealID)Cnt
				 FROM ProductHotDeal PH
				 INNER JOIN ProductHotDealLocation PL ON PH.ProductHotDealID = PL.ProductHotDealID
				 INNER JOIN PopulationCenterCities PCC ON PCC.City = PL.City AND PCC.State = PL.State
				 INNER JOIN PopulationCenters PC ON PC.PopulationCenterID = PCC.PopulationCenterID
				 WHERE GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE()-1) AND ISNULL(HotDealEndDate, GETDATE()+1)				  
				 AND PH.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
				 AND ((PH.CategoryID IS NULL )
					 OR PH.CategoryID IS NOT NULL AND PH.CategoryID IN (SELECT CategoryID FROM UserCategory WHERE UserID = @UserID))
				 GROUP BY PCC.PopulationCenterID) C ON P.PopulationCenterID = C.PopulationCenterID
			WHERE DMAName LIKE @PopulationCenter+'%'
			ORDER BY DMAName ASC
		END
		
		--If the user has not configured any category then consider all categories.
		ELSE
		BEGIN
			--To get all the Dma names.
			SELECT DISTINCT P.PopulationCenterID
						  , P.DMAName
						  , hotdealsCount = ISNULL(C.Cnt, 0) + (SELECT COUNT(DISTINCT P.ProductHotDealID) 
														  FROM ProductHotDeal P
														  LEFT JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = P.ProductHotDealID
													      WHERE PL.ProductHotDealID IS NULL
													      AND P.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
														  AND GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE()-1) AND ISNULL(HotDealEndDate, GETDATE()+1))
			FROM PopulationCenters P
			LEFT JOIN ( 
				 SELECT PCC.PopulationCenterID, COUNT(DISTINCT PH.ProductHotDealID)Cnt
				 FROM ProductHotDeal PH
				 INNER JOIN ProductHotDealLocation PL ON PH.ProductHotDealID = PL.ProductHotDealID
				 INNER JOIN PopulationCenterCities PCC ON PCC.City = PL.City AND PCC.State = PL.State
				 INNER JOIN PopulationCenters PC ON PC.PopulationCenterID = PCC.PopulationCenterID
				 AND GETDATE() BETWEEN ISNULL(HotDealStartDate, GETDATE()-1) AND ISNULL(HotDealEndDate, GETDATE()+1)
				 WHERE  PH.ProductHotDealID NOT IN (SELECT ProductHotDealID FROM ProductHotDealInterest WHERE UserID = @UserID AND Interested = 0)
				 GROUP BY PCC.PopulationCenterID) C ON P.PopulationCenterID = C.PopulationCenterID
			WHERE DMAName LIKE @PopulationCenter+'%'
			ORDER BY DMAName ASC
		END		
		
		--Capture the search key if the dma is not present.
		IF @@ROWCOUNT = 0
		BEGIN
			INSERT INTO HotDealPopulationCenterSearchLog(UserID
													   , DMAName
													   , DateCreated)
											    VALUES (@UserID
												       , @PopulationCenter
												       , GETDATE()) 
		END 
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerHotDealPopulationCentres.'		
		-- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
