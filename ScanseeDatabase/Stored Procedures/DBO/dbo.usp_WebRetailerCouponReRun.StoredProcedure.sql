USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCouponReRun]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCouponReRun
Purpose					: to update the same coupon and insert as a new record.
Example					: usp_WebRetailerCouponReRun

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCouponReRun]
(
	  @CouponID int
	, @RetailID int
	, @ManufacturerID int
	, @APIPartnerID int
	, @CouponName varchar(100)
	, @CouponDiscountType varchar(20)
	, @CouponDiscountAmount money
	, @CouponDiscountPct float
	, @NoOfCouponsToIssue int
	, @CouponShortDescription varchar(200)
	, @CouponLongDescription varchar(1000)
	, @CouponTermsAndConditions varchar(1000)
	, @CouponDisplayExpirationDate datetime
	, @CouponImagePath varchar(1000)
	, @CouponStartDate date
	, @CouponEndDate date
	, @CouponStartTime time
	, @CouponEndTime time
	, @CouponURL varchar(1000)
	, @ExternalCoupon bit
	, @CouponTimeZoneID int
	, @POSIntegrate bit
	, @KeyWords varchar(max)

	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @NewCouponID INT
			
			--Insert into Coupon Table
		
			INSERT INTO Coupon (
								RetailID
								--,RetailLocationID
								,ManufacturerID
								,APIPartnerID
								,CouponName	
								,CouponDiscountType 
								,CouponDiscountAmount
								,CouponDiscountPct 
								,CouponShortDescription 
								,CouponLongDescription 
								,CouponTermsAndConditions
								,NoOfCouponsToIssue
								,CouponImagePath
								,CouponDateAdded
								,CouponStartDate 
								,CouponExpireDate 
								--,CouponDisplayExpirationDate
								,CouponURL 
								,ExternalCoupon
								,CouponTimeZoneID
								,WebsiteSourceFlag
								,POSIntegrate
								,keywords)
						 
						 VALUES(
								@RetailID
								--,@RetailLocationID
								,@ManufacturerID
								,@APIPartnerID
								,@CouponName
								,@CouponDiscountType
								,@CouponDiscountAmount
								,@CouponDiscountPct
								,@CouponShortDescription
								,@CouponLongDescription
								,@CouponTermsAndConditions
								,@NoOfCouponsToIssue
								,@CouponImagePath
								,GETDATE()
								,CAST(@CouponStartDate AS DATETIME) + CAST(ISNULL(@CouponStartTime,'') AS DATETIME)
								,CAST(@CouponEndDate AS DATETIME) + CAST(ISNULL(@CouponEndTime,'') AS DATETIME)
								--,@CouponDisplayExpirationdate
								,@CouponURL
								,@ExternalCoupon
								,@CouponTimeZoneID
								,1
								,@POSIntegrate
								,@KeyWords
								)
								
								
				SET @NewCouponID = SCOPE_IDENTITY();	
				
										
				--Insert into CouponProduct Table
				
				INSERT INTO CouponProduct(
										  CouponID,
										  P.ProductID,
										  DateAdded)
				SELECT @NewCouponID
				     , ProductID
				     , GETDATE()
			    FROM CouponProduct 
			    WHERE CouponID = @CouponID
				
				--Insert into CouponRetailer Table
				
				INSERT INTO CouponRetailer (
												CouponID
											  , RetailID 
											  , RetailLocationID
											  , DateCreated	
											)
                                   SELECT @NewCouponID
                                        , RetailID
                                        , RetailLocationID
                                        , GETDATE()
                                   FROM CouponRetailer	
                                   WHERE CouponID = @CouponID					
					
					
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCouponReRun.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
