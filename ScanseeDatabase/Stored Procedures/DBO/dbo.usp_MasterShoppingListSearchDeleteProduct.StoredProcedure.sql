USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListSearchDeleteProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListSearchDeleteProduct
Purpose					: To delete product from shopping list 
Example					: usp_MasterShoppingListSearchDeleteProduct 14, '115, 114', '6/20/2011' 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListSearchDeleteProduct]
(
	  @UserID int
	, @UserProductID varchar(max)
	, @MasterListRemoveDate datetime
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE UserProduct
			SET MasterListItem = 0
				--, TodayListtItem = 0
				--, ShopCartItem = 0
				, MasterListRemoveDate = @MasterListRemoveDate
				--, TodayListRemoveDate = @MasterListRemoveDate
				--, ShopCartRemoveDate = @MasterListRemoveDate
			FROM UserProduct UP
			INNER JOIN fn_SplitParam (@UserProductID, ',') P ON P.Param  = UP.UserProductID  
			WHERE UserID = @UserID 
			--AND UserRetailPreferenceID = @UserRetailPreferenceID
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListSearchDeleteProduct.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
