USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerUserDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerUserDetails
Purpose					: To get User details
Example					: usp_WebConsumerUserDetails

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			2nd July 2013   				Dhananjaya TR	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerUserDetails]
(

	--Input Input Parameter	
	  @UserID Int
	
	
	--Output Variable--
	  
	, @Status INT OUTPUT	                  
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY	
		 		
				SELECT FirstName firstName
				     , Lastname lastName
				     , Email 
				     , MobilePhone				  
				     , UserID userID
				     , Address1 
				     , Address2
				     , [State]
				     , City
				     , PostalCode				     
				FROM Users 
				WHERE UserID=@UserID 
				
	   
		--Confirmation of Success.		
		SET @Status = 0
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerUserDetails.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 		
			
		END;
		 
	END CATCH;
END;


GO
