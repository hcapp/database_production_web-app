USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealsCategoryList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HotDealsCategoryList
Purpose					: To fetch the categories that have Hotdeals.
Example					: usp_HotDealsCategoryList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd Nov 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HotDealsCategoryList]
(
	@UserID int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			--SELECT CategoryID,ParentCategoryName,SubCategoryName 
			--FROM(SELECT 0 CategoryID 
			--			, 'All' ParentCategoryName
			--			, 'All' SubCategoryName
			--	 UNION ALL 
				 SELECT Distinct C.CategoryID CategoryID
								,ParentCategoryName+' - '+SubCategoryName categoryName
				 FROM Category C
				      INNER JOIN ProductHotDeal P on p.CategoryID=C.CategoryID
				      INNER JOIN UserCategory UC ON UC.CategoryID=C.CategoryID
				 WHERE (GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate) AND UserID=@UserID
				 ORDER BY ParentCategoryName + ' - ' + SubCategoryName
				 --) Category 
			--ORDER BY CASE WHEN ParentCategoryName = 'All' THEN 0 ELSE 1 END,ParentCategoryName,SubCategoryName 

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HotDealsCategoryList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
