USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_NewsFirstHubcitiCategory_Original]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_NewsFirstHubcitiCategory]
Purpose					: To enable news for only for given hubcitis.
Example					: [usp_NewsFirstHubcitiCategory]

HIStory
VersiON		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			01/14/2016	    SPAN           1.0
---------------------------------------------------------------
*/
create PROCEDURE [dbo].[usp_NewsFirstHubcitiCategory_Original]

AS

BEGIN

	SELECT DISTINCT H.HcHubCitiID
					,H.HubCitiName
					,C.NewsCategoryID
					,C.NewsCategoryName
					,ISNULL(NewsFeedURL,'') AS URL
					,CASE WHEN SC.NewsSubCategoryTypeID IS NULL THEN 0 ELSE 1 END AS IsSubCategory
					,SC.NewsSubCategoryName
					,CASE WHEN SC.NewsSubCategoryTypeID IS NULL THEN '' ELSE ISNULL(SUB.NewsFirstSubCategoryURL,  '') END AS NewsSubCategoryURL
	FROM NewsFirstSettings NS  
	INNER JOIN HcHubCiti H ON NS.HcHubCitiID = H.HcHubCitiID
	INNER JOIN NewsFirstCategory C ON C.NewsCategoryID = NS.NewsCategoryID
	LEFT JOIN NewsFirstSubCategoryType T ON T.NewsCategoryID = NS.NewsCategoryID
	LEFT JOIN NewsFirstSubCategory SC ON SC.NewsSubCategoryTypeID = T.NewsSubCategoryTypeID
	LEFT JOIN NewsFirstSubCategorySettings SUB ON SUB.NewsCategoryID = NS.NewsCategoryID AND SC.NewsSubCategoryID = SUB.NewsSubCategoryID AND NS.NewsCategoryID = SUB.NewsCategoryID AND NS.HcHubCitiID = SUB.HcHubCitiID
	WHERE H.Active=1 AND C.Active=1 AND NewsFirstFlag = 1 
	ORDER BY HcHubCitiID,C.NewsCategoryID

END


GO
