USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierManageProductAddAttributes]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierManageProductAddAttributes
Purpose					: To Add a new Hot Deal by the Supplier.
Example					: usp_WebSupplierManageProductAddAttributes

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			3rd January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierManageProductAddAttributes]             
(

	--Input Parameter(s)--
	 
	  @UserID int
	, @ManufacturerID int
	, @ProductID int
	, @AttributeID varchar(1000)
	, @AttributeContent varchar(1000)
	 
	--Output Variable--
	--,  @AttributeIDop  varchar(1000) output
	--, @AttributeContentop  varchar(1000)  output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
		
		Declare @OthersAttributeID int
		Select @OthersAttributeID = AttributeId from Attribute where AttributeName = 'Others' 
		
		--set @AttributeContentop=@AttributeContent
		--set @AttributeIDop = @AttributeID
		
		--Insert into ProductAttributes those that has Attribute Association.
			SELECT Row_num=IDENTITY(int,1,1)
					, LTRIM(RTRIM(Param)) AttributeID
				INTO #Attributes
				FROM fn_SplitParam(@AttributeID,',')			
				
				--SELECT * FROM  #Attributes	
				
				SELECT Row_num=IDENTITY(int,1,1)
					  ,LTRIM(RTRIM(Param)) DisplayValue
				INTO #DisplayValue
				FROM fn_SplitParam(@AttributeContent,',')				
				
				
				--SELECT * FROM #DisplayValue
				
				
				--Load the new set of the Attributes and Display values into a temp table.
				SELECT Row_num=IDENTITY(int,1,1)
				     , @ProductID 'ProductID'
				     , AttributeID
				     , DisplayValue
				INTO #Temp
				FROM (Select A.AttributeID				          
						   , D.DisplayValue   
				       From #Attributes A 
				       INNER JOIN #DisplayValue D ON A.Row_num = D.Row_num) Attributes
				 
				 --Check if the Product already has the attribute given and Update the display content only of those not        
				 UPDATE ProductAttributes
				 SET DisplayValue = DisplayContent.DisplayValue
				 FROM 
				 (SELECT T.DisplayValue
				       , T.AttributeID
				 FROM #Temp T
				 LEFT OUTER JOIN ProductAttributes PA ON T.ProductID = PA.ProductID
				 AND T.AttributeID = PA.AttributeID
				 WHERE PA.AttributeID IS NOT NULL
				 AND T.AttributeID <> @OthersAttributeID) DisplayContent
				 WHERE DisplayContent.AttributeID = ProductAttributes.AttributeID
				 
				 
				 --Insert only new attributes.
				 INSERT INTO ProductAttributes(
													ProductID
												  , AttributeID
												  , DisplayValue												 
											   )
							 SELECT  T.ProductID
							       , T.AttributeID
							       , T.DisplayValue
							 FROM #Temp T
							 LEFT OUTER JOIN ProductAttributes PA ON T.ProductID = PA.ProductID
							 AND T.AttributeID = PA.AttributeID
							 WHERE PA.AttributeID IS NULL 	
							 OR T.AttributeID = @OthersAttributeID		
								 
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierManageProductAddAttributes.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
