USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveRetailerBusinessCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: 
Purpose					: To retrieve list of Categories from the system for Registration..
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			03 April 2012				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveRetailerBusinessCategory]

	--Output variables
	@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		
			--To display list of all Business Categories along with flags indicating filters and subcategory association.
			SELECT DISTINCT BC.BusinessCategoryID businessCategoryID
						,BC.BusinessCategoryName businessCategoryName
						, SortOrder
						,isAssociated = IIF(AdminFilterBusinessCategoryID IS NULL ,0 ,1)
						,isSubCat = IIF(HcBusinessSubCategoryID IS NULL, 0, 1)
			FROM BusinessCategory BC
			LEFT JOIN AdminFilterBusinessCategory FBC ON BC.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN HcBusinessSubCategoryType SBT ON BC.BusinessCategoryID = SBT.BusinessCategoryID
			LEFT JOIN HcBusinessSubCategory SC ON SBT.HcBusinessSubCategoryTypeID = SC.HcBusinessSubCategoryTypeID
			WHERE Active = 1	
			ORDER BY SortOrder 

	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveBusinessCategory.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
