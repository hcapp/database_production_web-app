USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadProducts
Purpose					: To insert into the product related stage tables.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			15th December 2011			Naga Sandhya S	    Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadProducts]
(

	--Input Input Parameter(s)--
	
	--For Product Stage Table
	   @ProductName varchar(500) 
	 , @ScanCode varchar(20)
	 , @ScanType varchar(10) 
	 , @ProductExpirationDate datetime
	 , @ProductImagePath varchar(1000) 
	 , @Category varchar(1000) 
	 , @ProductShortDescription varchar(255)
	 , @ProductLongDescription varchar(max)
	 , @ModelNumber varchar(50)
	 , @SuggestedRetailPrice money 
	 , @Weight float 
     , @WeightUnits varchar(20)
     , @ManufacturerProdcutUrl varchar(1000)
     , @Audio varchar(255)
     , @Video varchar(255)
     , @Image varchar(1000)
	  
	--For ProductAttributes Stage Table	
	
	 , @AttributeName varchar(110)
	 , @DisplayValue varchar(max)
	 
   --For Product SKU Stage Table   
	
	 , @SKUNumber varchar(30)
	
	--Output Variable--
	  
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		--Truncating the stage tables...
		
		TRUNCATE TABLE UploadSupplierProducts
		TRUNCATE TABLE UploadSupplierProductsSKU
		TRUNCATE TABLE UploadSupplierProductAttributes
				
			--To insert product data into the stage table 'UploadSupplierProducts'
			
			INSERT INTO [UploadSupplierProducts]
					   ([ProductName]
					   ,[ScanCode]
					   ,[ScanType]
					   ,[ProductExpirationDate]
					   ,[ProductImagePath]
					   ,[Category]
					   ,[ProductShortDescription]
					   ,[ProductLongDescription]
					   ,[ModelNumber]
					   ,[SuggestedRetailPrice]
					   ,[Weight]
					   ,[WeightUnits]
					   ,[ManufacturerProdcutUrl]
					   ,[Audio]
					   ,[Video]
					   ,[Image])
			VALUES ( @ProductName
					,@ScanCode
					,@ScanType
					,@ProductExpirationDate
					,@ProductImagePath
					,@Category
					,@ProductShortDescription
					,@ProductLongDescription
					,@ModelNumber
					,@SuggestedRetailPrice
					,@Weight
					,@WeightUnits
					,@ManufacturerProdcutUrl
					,@Audio
					,@Video
					,@Image)
					
			--To insert productSKU data into the stage table 'UploadSupplierProductsSKU'
					
			INSERT INTO [UploadSupplierProductsSKU]
					   ([ScanCode]
					   ,[SKUNumber])
				VALUES(@ScanCode
					   ,@SKUNumber)
			
			--To insert productAttributes data into the stage table 'UploadSupplierProductAttributes'
				   
		    INSERT INTO [UploadSupplierProductAttributes]
					   ([ScanCode]
					   ,[AttributeName]
					   ,[DisplayValue])
				VALUES( @ScanCode
					   ,@AttributeName
					   ,@DisplayValue)
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadProducts.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
