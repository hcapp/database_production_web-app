USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayProductAssociatedHotDeals]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebDisplayAssociatedHotDeals  
Purpose     : To Display the Hot Deals associated to the product.  
Example     :   
  
History  
Version  Date       Author   Change Description  
-------------------------------------------------------------------------------   
1.0   10th December 2011    Pavan Sharma K Initial Version  
-------------------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebDisplayProductAssociatedHotDeals]  
(  
  
 --Input Input Parameter(s)--  
      
     
   @ProductID int   
    
   
 --Output Variable--     
    
  , @ErrorNumber int output  
  , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN   
    
 BEGIN TRY  
 
      --To get Media Server Configuration.  
        DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration' 
  
  --Display the Product associated with Hot Deal.  
    
  SELECT DISTINCT PHD.ProductHotDealID  
       , PHD.HotDealName  
       , PHD.HotDealShortDescription  
       , HotDealImagePath = CASE WHEN PHD.HotDealImagePath IS NOT NULL THEN 
												CASE WHEN WebsiteSourceFlag = 1 THEN CASE WHEN PHD.ManufacturerID IS NOT NULL THEN @ManufacturerConfig + CONVERT(VARCHAR(30),PHD.ManufacturerID) +'/' + HotDealImagePath
																															  ELSE @RetailerConfig + CONVERT(VARCHAR(30),PHD.RetailID) +'/' + HotDealImagePath
														 END																					 
       
														ELSE PHD.HotDealImagePath
												END
					        END
       , PHD.HotDealStartDate  
       , PHD.HotDealEndDate  
  FROM HotDealProduct HDP  
  INNER JOIN ProductHotDeal PHD ON PHD.ProductHotDealID = HDP.ProductHotDealID
  WHERE HDP.ProductID = @ProductID  
 END TRY   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebDisplayAssociatedHotDeals.'    
  -- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output      
  END;  
     
 END CATCH;  
END;




GO
