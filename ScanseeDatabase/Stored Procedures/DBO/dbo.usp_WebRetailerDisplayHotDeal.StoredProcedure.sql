USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerDisplayHotDeal]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerDisplayHotDeal
Purpose					: To the detailes of the input Hot Deal ID.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			4th January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerDisplayHotDeal]				
(

	--Input Input Parameter(s)--
	
	  @HotDealID int
	
	--Output Variable--	  

	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		
		DECLARE @ProductIDs VARCHAR(MAX)
		DECLARE @ScanCodes VARCHAR(MAX)
		DECLARE @RetailLocationIDs INT
		DECLARE @DMAID VARCHAR(MAX)
		
		 --To get Media Server Configuration.  
		DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'	  
		
		--Get Comma Separated ProductIDs associated with the input hot deal.
		SELECT @ProductIDs = COALESCE(@ProductIDs+',','')+ CAST(ProductID AS VARCHAR(100)) FROM HotDealProduct WHERE ProductHotDealID = @HotDealID
		
		--Get Scan Codes of the Associated Products.
		SELECT ScanCode
		INTO #TEMP
		FROM
			(SELECT P.ScanCode
			FROM  HotDealProduct HDP 
			INNER JOIN Product P ON HDP.ProductID = P.ProductID
			WHERE HDP.ProductHotDealID = @HotDealID)ScanCodes
		
		SELECT @ScanCodes = COALESCE(@ScanCodes+',','')+ CAST(ScanCode AS VARCHAR(100)) FROM #TEMP 
		
		SELECT @RetailLocationIDs = CAST(RetailLocationID AS VARCHAR(50))
		FROM ProductHotDealRetailLocation
		WHERE ProductHotDealID = @HotDealID
		
		SELECT @DMAID = COALESCE(@DMAID+',', '') + CAST(PopulationCenterID AS VARCHAR(50))
		FROM ProductHotDealLocation PL
		INNER JOIN PopulationCenterCities PCC ON PCC.City = PL.City AND PCC.State = PL.State
		WHERE ProductHotDealID = @HotDealID
		GROUP BY PopulationCenterID
		
		--Display the Details of the input Hot Deal.
		SELECT DISTINCT PHD.ProductHotDealID as hotDealID
		     , PHD.HotDealName as hotDealName
		     , PHD.HotDealShortDescription as hotDealShortDescription
		     , PHD.HotDeaLonglDescription AS hotDealLongDescription 
		     , hotDealImagePath = CASE WHEN PHD.HotDealImagePath IS NOT NULL THEN 
								CASE WHEN PHD.WebsiteSourceFlag = 1 THEN 
									CASE WHEN PHD.ManufacturerID IS NOT NULL THEN @ManufacturerConfig +  CONVERT(VARCHAR(30),PHD.ManufacturerID) +'/'+ HotDealImagePath
																			 ELSE @RetailerConfig +  CONVERT(VARCHAR(30),PHD.RetailID) +'/'+ HotDealImagePath
									   END

 									ELSE PHD.HotDealImagePath
								END
					        END
		     , CAST(PHD.HotDealStartDate AS DATE) dealStartDate
		     , CAST(PHD.HotDealEndDate AS DATE) dealEndDate
		     , ISNULL(CAST(PHD.HotDealStartDate AS TIME), '00:00:00.000')  as dealStartTime
		     , ISNULL(CAST(PHD.HotDealEndDate AS TIME), '00:00:00.000') as dealEndTime
		     , CAST(PHD.HotDealExpirationDate AS DATE) as expireDate
		     , ISNULL(CAST(PHD.HotDealExpirationDate AS TIME), '00:00:00.000')as expireTime
		     , PHD.HotDealTermsConditions as hotDealTermsConditions
		     , PHD.Price as price
		     , PHD.SalePrice salePrice
		  --   , PHD.RetailID as retailID
		     , @ProductIDs as productId
		     , @ScanCodes as scanCode     
		     , @RetailLocationIDs as retailerLocID
		     , CASE WHEN @RetailLocationIDs IS NOT NULL THEN NULL ELSE @DMAID END as city
		     , PHD.HotDealTimeZoneID as dealTimeZoneId
		     , PHD.CategoryID bCategory
		     , C.ParentCategoryName 
		     , C.SubCategoryName
		     , PHD.HotDealCode as couponCode
		     , PHD.NoOfHotDealsToIssue as numOfHotDeals
		FROM ProductHotDeal PHD 
		LEFT OUTER JOIN HotDealProduct HDP ON PHD.ProductHotDealID = HDP.ProductHotDealID
		LEFT OUTER JOIN Product P ON HDP.ProductID = P.ProductID
		LEFT OUTER JOIN Category C ON C.CategoryID = PHD.CategoryID 
		WHERE PHD.ProductHotDealID = @HotDealID --AND (PHD.HotDealExpirationDate>=GETDATE() OR PHD.HotDealExpirationDate IS NULL)
		
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerDisplayHotDeal.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
	END CATCH	 
	
END;







GO
