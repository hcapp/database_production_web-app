USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAddHotDeal]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerADDHotDeal
Purpose					: To Add a new Hot Deal By the Retailer.
Example					: usp_WebRetailerADDHotDeal

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			28th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerAddHotDeal]               
(

	--Input Parameter(s)--
	 
	--Inputs for ProductHotDeal Table
	  @HotDealName  varchar(255)
    , @RegularPrice money
    , @SalePrice money
    , @HotDealDescription  varchar(1000)
    , @HotDealLongDescription  varchar(3000)
    , @HotDealTerms  varchar(1000)
    , @DealStartDate DATE
    , @DealStartTime Time
    , @DealEndDate DATE
    , @DealEndTime Time
    , @HotDealTimeZoneID int
    , @CategoryID int
    , @CouponCode varchar(100)
    , @Quantity int   -- No. of HotDeals
    , @ExpirationDate DATE
    , @ExpirationTime Time    
    , @HotDealImagePath varchar(1000)   
    
    --Product HotDealLocation Inputs.
    --, @City varchar(max) 
    --, @State varchar(max)			Commented because the Prototype is changed to take the Population Centre ID as input.
    
    , @PopulationCentreID varchar(max)
  
    
    --Product HotDealRetailLocation Inputs.
    , @RetailerID int
    , @RetailLocationID varchar(max)
     
     --Inputs for HotDealProduct Table   
  
    , @ProductID varchar(max)
    
	--Output Variable--
	, @FindClearCacheURL varchar(1000) output	 
	, @PercentageFlag bit output  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
		
		DECLARE @ProductHotDealID INT
		DECLARE @APIPartnerID INT
		SELECT @APIPartnerID = APIPartnerID FROM APIPartner WHERE APIPartnerName = 'ScanSee'
		
		DECLARE @Percentage float  -- To calculate Hot Deal percentage
		DECLARE @Difference float
		SET @Difference = (@RegularPrice - @SalePrice)
		SET @Percentage = (@Difference*100)/@RegularPrice
		
		IF (@Percentage >= 50)
		BEGIN
				
		   -- Create a record in Product Hot Deal Table.   
		   INSERT INTO ProductHotDeal( APIPartnerID
									 , HotDealName
									 , Price
									 , HotDealDiscountAmount
									 , HotDealDiscountPct
									 , SalePrice
									 , HotDealShortDescription
									 , HotDeaLonglDescription
									 , HotDealTermsConditions
									 , HotDealStartDate
									 , HotDealEndDate  
									 , RetailID
									 , CreatedDate
									 , HotDealTimeZoneID					
									 , WebsiteSourceFlag
									 , CategoryID
									 , HotDealCode 
									 , NoOfHotDealsToIssue
									 , HotDealExpirationDate
									 , HotDealImagePath
								  )	
							VALUES(     @APIPartnerID
									  , @HotDealName
									  , @RegularPrice
									  , @Difference
									  , @Percentage
									  , @SalePrice
									  , @HotDealDescription
									  , @HotDealLongDescription
									  , @HotDealTerms
									  , CASE WHEN (CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'') AS DATETIME))='1900-01-01 00:00:00.000' 
											   THEN NULL 
											 ELSE (CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'') AS DATETIME))
										END
									  , CASE WHEN (CAST(ISNULL(@DealEndDate,NULL) AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME))='1900-01-01 00:00:00.000'
											    THEN NULL 
											 ELSE (CAST(ISNULL(@DealEndDate,NULL) AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME))
										END
									  , @RetailerID
									  , GETDATE()	
									  , @HotDealTimeZoneID
									  , 1
									  , @CategoryID
									  , @CouponCode    
									  , @Quantity
									  ,CASE WHEN ((CAST(ISNULL(@ExpirationDate,NULL) AS DATETIME)+CAST(ISNULL(@ExpirationTime,'') AS DATETIME))='1900-01-01 00:00:00.000' OR ((CAST(ISNULL(@ExpirationDate,NULL) AS DATETIME)+CAST(ISNULL(@ExpirationTime,'') AS DATETIME)) IS NULL))	
												THEN NULL
											 ELSE (CAST(ISNULL(@ExpirationDate,NULL) AS DATETIME)+CAST(ISNULL(@ExpirationTime,'') AS DATETIME))
										END	
									  , @HotDealImagePath							  
								  )
								  
								  
			 					  
				
          --Capture the inserted hot deal ID.	
          			
          SET @ProductHotDealID = SCOPE_IDENTITY()  
          
            
                          			     
            
          --If the Hot Deal is associated with the City.  								  
          IF (@PopulationCentreID IS NOT NULL)
          BEGIN
          INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]											 
											  )
										SELECT @ProductHotDealID
										     , City
										     , [State]
										FROM dbo.fn_SplitParam(@PopulationCentreID, ',') P
										INNER JOIN PopulationCenterCities PC ON P.Param = PC.PopulationCenterID										
			
		  END							     
		  IF(@ProductID IS NOT NULL)	
		  BEGIN								--Product ID not mandatory if the deal is specific to the city and state.
		  INSERT INTO HotDealProduct(
										ProductHotDealID
									  , ProductID
									)
						SELECT @ProductHotDealID
						     , [Param]
						FROM dbo.fn_SplitParam(@ProductID, ',')          
          
          
          							     
          END	
          
          --If Hot Deal is associated with the Retail Location.
          IF (@RetailLocationID IS NOT NULL)
          BEGIN
			
				INSERT INTO ProductHotDealRetailLocation(
															ProductHotDealID
														  , RetailLocationID
														)
											SELECT @ProductHotDealID
											     , [Param]
											FROM dbo.fn_SplitParam(@RetailLocationID, ',')
				INSERT INTO ProductHotDealLocation(ProductHotDealID												 
												  , City
												  , [State])
										SELECT @ProductHotDealID
										     , RL.City
											 , RL.State
										FROM RetailLocation RL 
										INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',') P ON P.Param = RL.RetailLocationID AND RL.Active =1
		  END
		  SET @PercentageFlag = 0
		END	
		
		ELSE 
		BEGIN
			SET @PercentageFlag = 1
		END  
		
		SELECT DISTINCT UserID
		              ,ProductID
		              ,DeviceID
		              ,ProductHotDealID		              
		        INTO #Temp
		        FROM (
		        SELECT U.UserID
		              ,UP.ProductID
		              ,UD.DeviceID 
		              ,HD.ProductHotDealID  
		              , Distance= (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(Latitude  / 57.2958) * COS((Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 
		              ,LocaleRadius 
		        from ProductHotDeal PH
		        INNER JOIN HotDealProduct  HD ON HD.ProductHotDealID =PH.ProductHotDealID AND PH.ProductHotDealID=@ProductHotDealID        
		        INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID =HD.ProductHotDealID 
		       -- INNER JOIN RetailLocation RL ON RL.RetailLocationID =PRL.RetailLocationID  
		        INNER JOIN UserProduct UP ON UP.ProductID =HD.ProductID AND WishListItem =1	
		        INNER JOIN Users U ON U.UserID =UP.UserID 
		        INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
		        INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
		        INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
		    )Note
		    -- WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		    
		            
		        --User pushnotification already exist then NotificationSent flag updating to 0
		        UPDATE UserPushNotification SET NotificationSent=0
		                                       ,DateModified =GETDATE()	
		                                       ,DiscountCreated=1		                                      
		        FROM UserPushNotification UP
		        INNER JOIN #Temp T ON UP.ProductHotdealID=T.ProductHotDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=1
		       
		       --User pushnotification not exist then insert. 
		       INSERT INTO UserPushNotification(UserID
												,ProductID
												,DeviceID												
												,ProductHotdealID												
												,NotificationSent
												,DateCreated
												,DiscountCreated)
				SELECT T.UserID 
				      ,T.ProductID 
				      ,T.DeviceID 
				      ,T.ProductHotDealID 
				      ,0
				      ,GETDATE()
				      ,1
				FROM #Temp T				
				LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.ProductHotDealID =UP.ProductHotdealID AND T.ProductID =UP.ProductID AND DiscountCreated=1
				WHERE UP.UserPushNotificationID IS NULL  
		

			-------Find Clear Cache URL---29/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------	
		
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealAdd.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
