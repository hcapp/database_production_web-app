USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchAPIMamaSourceRefreshStagingTables]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchAPIMamaSourceRefreshStagingTables
Purpose					: To refersh the Stage Table
Example					: usp_BatchAPIMamaSourceRefreshStagingTables

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd Jan 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchAPIMamaSourceRefreshStagingTables]
(
	
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		TRUNCATE TABLE APIMamaSourceData
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchAPIMamaSourceRefreshStagingTables.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 		
		END;
		 
	END CATCH;
END;

GO
