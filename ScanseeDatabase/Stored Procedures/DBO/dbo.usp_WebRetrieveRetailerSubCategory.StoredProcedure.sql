USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveRetailerSubCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [dbo].[usp_WebRetrieveRetailerSubCategory]
Purpose					: To display list of subcategories for given Business categories.
Example					: [dbo].[usp_WebRetrieveRetailerSubCategory]

History
Version				Date				    Author			
--------------------------------------------------------------------------
1.0					16 june 2015		    Span	    
--------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveRetailerSubCategory]

	--Input Variables
	  @BusinessCategoryID Varchar(500)

	--Output variables
	,  @Status int OUTPUT

AS
BEGIN

	BEGIN TRY
			
			--To display list of Sub-Categories for given Business Category IDs
			SELECT DISTINCT BC.BusinessCategoryID
							,BC.BusinessCategoryName
							,SC.HcBusinessSubCategoryID AS SubCategoryID
							,SC.BusinessSubCategoryName AS SubCategoryName
			FROM BusinessCategory BC
			LEFT JOIN HcBusinessSubCategoryType SBT ON BC.BusinessCategoryID = SBT.BusinessCategoryID
			LEFT JOIN HcBusinessSubCategory SC ON SBT.HcBusinessSubCategoryTypeID = SC.HcBusinessSubCategoryTypeID
			WHERE BC.BusinessCategoryID IN (SELECT Param FROM fn_SplitParam(@BusinessCategoryID, ','))  AND Active = 1 	
			ORDER BY BusinessCategoryName, BusinessSubCategoryName

			SELECT @Status = 0

	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveRetailerSubCategory.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;








GO
