USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserPaymentPreferecesDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserPaymentPreferecesDisplay
Purpose					: To fetch User prefernces info.
Example					: usp_UserPaymentPreferecesDisplay 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserPaymentPreferecesDisplay]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		SELECT PaymentIntervalID as selPaymentIntervalID
			,PaymentTypeID as selPaymentTypeID
			,PayoutTypeID as selPayoutTypeID
			,DefaultPayout
			--,UseDefaultAddress
			--,PayAddress1
			--,PayAddress2
			--,PayCity
			--,PayState
			--,PayPostalCode
			--,U.Address1 AS UserAddress1
			--,U.Address2 AS UserAddress2
			--,U.City AS UserCity
			--,U.State AS UserState
			--,U.PostalCode AS UserPostalCode
			,ProvisionalPayoutActive
			,ProvisionalPayoutStartDate
			,ProvisionalPayoutEndDate
		FROM  UserPayInfo UP
			FULL OUTER JOIN Users U ON U.UserID = UP.UserID  
		WHERE UP.UserID = @UserID 
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserPaymentPreferecesDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
