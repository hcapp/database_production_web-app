USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[1usp_WebRssFeedNewsInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsInsertion]
Purpose					: To insert into RssFeedNews from Staging table.
Example					: [usp_WebRssFeedNewsInsertion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015		Mohith H R	        1.1
1.1         07 Dec 2015     Suganya C           Duplicate news removal
1.2							Sagar Byali			Video deletion changes
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[1usp_WebRssFeedNewsInsertion]
(
	 -- @HcHubCitiID int,

	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRAN


		--Deleting 30 days older News for 'spanqa.regionsapp'.
		DECLARE @ExpiryPublishedDateRockwall datetime = GETDATE()-30
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateRockwall AND HcHubCitiID = 28 

		---added on 05/05/2016
		DECLARE @ExpiryPublishedDateMarbel datetime = GETDATE()-30
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate < @ExpiryPublishedDateMarbel AND HcHubCitiID = 10 AND NewsType NOT IN ('Videos','all')

		
		--added on 26th Sept 2016 to retain 14 day news for 'All' category
		DECLARE @ExpiryPublishedDateMarbelAllCategoryNews datetime

		SELECT @ExpiryPublishedDateMarbelAllCategoryNews = DATEADD(d,-14,max(convert(date,PublishedDate)))
		from RssFeedNewsStagingTable 
		where NewsType = 'all' AND HcHubCitiID = 10
				
		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,PublishedDate) < @ExpiryPublishedDateMarbelAllCategoryNews AND HcHubCitiID = 10 AND NewsType = 'all'


		--added on 22nd Aug 2016 to retain one day videos 

		DECLARE @ExpiryPublishedDateMarbelVideos datetime
		SELECT @ExpiryPublishedDateMarbelVideos = max(convert(date,DateCreated)) 
		from RssFeedNewsStagingTable 
		where NewsType='videos' AND HcHubCitiID = 10

		DELETE FROM RssFeedNewsStagingTable
		WHERE convert(date,DateCreated) <> @ExpiryPublishedDateMarbelVideos AND HcHubCitiID = 10 AND NewsType = 'Videos'



		--Deleting 5 days older News for 'Killeen Daily Herald'.
		DECLARE @ExpiryPublishedDateKilleen datetime = GETDATE()-5
		DELETE FROM RssFeedNewsStagingTable
		WHERE PublishedDate  < @ExpiryPublishedDateKilleen AND HcHubCitiID = 52  
		
		 --Fetching unique, latest data from RssFeedNewsStagingTable to insert into RssFeedNews
	
		 SELECT DISTINCT HcHubCitiID, NewsType, Title,Classification, MAX(RssFeedNewsID) AS MaxRssFeedNewsID
         INTO #Temp
         FROM RssFeedNewsStagingTable
         GROUP BY HcHubCitiID,NewsType, Title, Classification
		
		 --Insert into RssFeedNews
		 INSERT INTO RssFeedNews(Title
								,ImagePath
								,ShortDescription
								,LongDescription
								,Link
								,PublishedDate
								,NewsType
								,Message
								,HcHubCitiID
								,Classification
								,Section
								,AdCopy
								,VideoLink)															
						SELECT  RS.Title
								,RS.ImagePath
								,RS.ShortDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy
								,RS.VideoLink		
						FROM RssFeedNewsStagingTable RS 
						INNER JOIN #Temp T ON RS.HcHubCitiID = T.HcHubCitiID 
						AND RS.NewsType = T.NewsType 
						AND ((RS.NewsType <> 'Classifields' AND RS.Title = T.Title) OR (RS.NewsType = 'Classifields' AND 1=1))
						AND RS.RssFeedNewsID = T.MaxRssFeedNewsID 
						GROUP BY RS.Title
								,RS.ImagePath
								,RS.ShortDescription
								,RS.LongDescription
								,RS.Link
								,RS.PublishedDate
								,RS.NewsType
								,RS.Message
								,RS.HcHubCitiID	
								,RS.Classification
								,RS.Section
								,RS.AdCopy
								,RS.VideoLink	
						ORDER BY MIN(RS.RssFeedNewsID) ASC

		DECLARE @LatestTime datetime
		SELECT @LatestTime = MAX(DateCreated)
		FROM RssFeedNews
						
		 DELETE FROM RssFeedNews
		 FROM RssFeedNews RF
		 INNER JOIN RssFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType	
		 WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	 
			OR (RS.Title IS NULL AND RS.NewsType = 'Classifields'))
		 AND RF.DateCreated <> @LatestTime
				
				   
		  SET @Status=0	
		  	 COMMIT TRAN 				   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRssFeedNewsInsertion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;





GO
