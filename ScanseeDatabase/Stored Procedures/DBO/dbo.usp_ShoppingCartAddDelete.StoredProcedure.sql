USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingCartAddDelete]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ShoppingCartAddDelete
Purpose					: To Swap Product between Today Shopping list and Shopping Cart
Example					: usp_ShoppingCartAddDelete 14, NULL, '125,113'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ShoppingCartAddDelete]
(
	@UserID int
	, @TodayListUserProductID varchar(max)
	, @CartUserProductID varchar(max)
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output

)
AS
BEGIN

	BEGIN TRY
		--Current Date.
		DECLARE @Today datetime
		SET @Today = GETDATE()
		BEGIN TRANSACTION
			--To Swap product from Cart to Today List.
			UPDATE UserProduct 
			SET ShopCartItem = 0
				, ShopCartRemoveDate = @Today 
				, TodayListtItem = 1
				, TodayListAddDate = @Today 
			FROM UserProduct UP
				INNER JOIN fn_SplitParam(@TodayListUserProductID, ',') P ON P.Param = UP.UserProductID 
			WHERE UserID = @UserID 

			--To Swap product from Today List to Cart.
			UPDATE UserProduct 
			SET TodayListtItem = 0
				, TodayListRemoveDate = @Today 
				, ShopCartItem = 1
				, ShopCartAddDate = @Today 
			FROM UserProduct UP
				INNER JOIN fn_SplitParam(@CartUserProductID, ',') P ON P.Param = UP.UserProductID 
			WHERE UserID = @UserID 
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ShoppingCartAddDelete.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
