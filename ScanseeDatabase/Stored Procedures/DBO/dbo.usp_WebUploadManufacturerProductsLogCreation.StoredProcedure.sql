USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerProductsLogCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadManufacturerProductsLogCreation
Purpose					: To create a Log entry for the batch that will be processed.
Example					: usp_WebUploadManufacturerProductsLogCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th July 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerProductsLogCreation]
(
	 @ManufacturerID int
	,@UserID int
	,@FileName Varchar(255)
	 
	--Output Variable 	
	,@ManufacturerLogID int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
			--To get Manufacturer name
			DECLARE @manufname varchar(255)
			SELECT @manufname=ManufName
			FROM Manufacturer
			WHERE ManufacturerID=@ManufacturerID		
			
			INSERT INTO [UploadManufacturerLog]
					   ([UserID]
					   ,[ManufacturerID]
					   ,[ManufacturerName]
					   ,[ProductFileName]
					   ,[ProductTotalRowsProcessed]
					   ,[ProductSuccessfulRowCount]
					   ,[ProductFailureRowCount]
					   , ProductAttributeTotalRowsProcessed
					   , ProductAttributeFailureRowCount
					   , ProductAttributeSuccessfulRowCount
					   ,[ProcessDate]
					   ,[ProcessStartDate]
					   , [ProcessEndDate])
			 VALUES(@UserID
					,@ManufacturerID
					,@manufname
					,@FileName
					, 0
					, 0
					, 0
					, 0
					, 0
					, 0
					,GETDATE()
					,GETDATE()
					, GETDATE())
					
		SELECT @ManufacturerLogID = SCOPE_IDENTITY()
	--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerProductsLogCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			--DECLARE @ERRORMsg Varchar(2000)
			--SET @ERRORMsg=ERROR_MESSAGE()
			ROLLBACK TRANSACTION;				
		
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
