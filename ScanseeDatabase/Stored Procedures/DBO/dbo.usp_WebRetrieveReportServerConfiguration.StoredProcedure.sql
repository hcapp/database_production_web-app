USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveReportServerConfiguration]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetrieveReportServerConfiguration
Purpose					: To retrieve the URL of Report server from the system.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			07-02-2012					Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveReportServerConfiguration]

--Output variables
@Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			SELECT ScreenName
				 , ScreenContent 
			FROM AppConfiguration
			WHERE ConfigurationType = 'Report Server Configuration'
			
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveReportServerConfiguration.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
