USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerDisplayRetailLocations]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerDisplayRetailLocations]
Purpose					: 
Example					: [usp_WebRetailerDisplayRetailLocations]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03 April 2012	Pavan Sharma K	Initial Version
2.0			25/11/2016		Bindu T A		Hours - Filters Changes- Day Wise
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerDisplayRetailLocations]
(
	  @RetailID int	  
	 ,@SearchKey varchar(100)
     ,@LowerLimit int

	
	--Output Variable--
     , @FindClearCacheURL VARCHAR(500) OUTPUT
	 , @RowCount INT output
	 , @NextPageFlag bit output	
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
	 
		DECLARE @UpperLimit INT
		DECLARE @MaxCnt INT
		DECLARE @Config VARCHAR(500)
		
		--To get the row count for pagination.  
		 DECLARE @ScreenContent Varchar(100)
		 SELECT @ScreenContent = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ScreenName = 'All' 
			AND ConfigurationType = 'Website Pagination'
			AND Active = 1  

		----To get the Configuration Details.	 
		SELECT @Config = ScreenContent   
		FROM AppConfiguration   
		WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
		AND Active = 1
   
    SELECT @UpperLimit = @LowerLimit + @ScreenContent
		 
	SELECT RowNum = ROW_NUMBER() OVER (ORDER BY RetailLocationID)
	     , RetailLocationID 
	     , StoreIdentification
	     , Address1
	     , Address2
	     , City
	     , State
	     , PostalCode
		 --, StartTime
		 --, EndTime
		 --, StartTimeUTC
		 --, EndTimeUTC
		 --, TimeZoneID
	     , RetailLocationLatitude
	     , RetailLocationLongitude
	     , RetailLocationURL
	     , ContactPhone
	     --, ContactFirstName
	     --, ContactLastname
	     --, ContactMobilePhone
	     --, ContactTitleID
	     --, ContactTitle
	     --, ContactEmail
	     , RetailKeyword
		 , GridImgLocationPath
	INTO #Temp
	FROM(
		SELECT DISTINCT
			  RL.RetailLocationID
			 , StoreIdentification
			 , RL.Address1
			 , RL.Address2
			 , RL.City
			 , RL.State
			 , RL.PostalCode
			 --, RL.StartTime
			 --, RL.EndTime
			 --, RL.StartTimeUTC
			 --, RL.EndTimeUTC
			 --, RL.TimeZoneID
			 , RetailLocationLatitude 
			 , RetailLocationLongitude
			 , RL.RetailLocationURL
			 , CASE WHEN RL.CorporateAndStore = 1 THEN R.CorporatePhoneNo ELSE C.ContactPhone END ContactPhone
		     --, C.ContactFirstName
		     --, C.ContactLastname
		     --, C.ContactMobilePhone
		     --, C.ContactTitle ContactTitleID 
		     --, CT.ContactTitle
		     --, C.ContactEmail
		     , RK.RetailKeyword 
			 --, RL.RetailLocationImagePath LocationImgPath
			 , IIF(RL.RetailLocationImagePath IS NOT NULL ,@Config+CAST(@RetailID AS VARCHAR(10))+'/locationlogo/'+RL.RetailLocationImagePath,IIF(RetailerImagePath IS NOT NULL, @Config + CONVERT(VARCHAR(30),R.RetailID) +'/' + RetailerImagePath,Null)) GridImgLocationPath
		FROM RetailLocation RL
		INNER JOIN Retailer R ON R.RetailID = RL.RetailID AND RL.Active = 1 AND RetailerActive = 1
		LEFT OUTER JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
		LEFT OUTER JOIN Contact C ON C.ContactID = RC.ContactID
		LEFT OUTER JOIN ContactTitle CT ON CT.ContactTitleID = C.ContactTitle
		LEFT OUTER JOIN RetailerKeywords RK ON RK.RetailLocationID = RL.RetailLocationID 
		WHERE RL.RetailID = @RetailID  
		--AND (RL.StoreIdentification LIKE (CASE WHEN @SearchKey IS NOT NULL THEN @SearchKey+'%' ELSE '%' END)
	    AND RL.Headquarters <> 1
		--OR RL.StoreIdentification LIKE '%'+@SearchKey+'%') RetailLocations
		AND ((@SearchKey IS NOT NULL  AND RL.StoreIdentification LIKE '%'+@SearchKey+'%') OR (@SearchKey IS NULL AND 1=1))) RetailLocations
		--WHERE ((RL.RetailID = @RetailID AND RL.StoreIdentification IS NOT NULL AND StoreIdentification LIKE '%'+@SearchKey+'%')	 
		--OR (RL.RetailID = @RetailID AND RL.StoreIdentification IS NULL))		
		--AND RL.Headquarters <> 1) RetailLocations	 
	 
	
	SELECT @MaxCnt = MAX(RowNum) FROM #TEMP
	SET @RowCount = @MaxCnt
	SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END    --CHECK IF THERE ARE STILL MORE RECORDS

   
	SELECT RetailLocationID 
	     , StoreIdentification
	     , Address1
	     , Address2
	     , City
	     , State
	     , PostalCode
		 --, StartTime
		 --, EndTime
		 --, StartTimeUTC
		 --, EndTimeUTC
		 --, TimeZoneID
		 , RetailLocationLatitude as RetailerLocationLatitude
		 , RetailLocationLongitude as RetailerLocationLongitude
	     , RetailLocationURL
	     , ContactPhone phonenumber
	     --, ContactFirstName
	     --, ContactLastname
	     --, ContactMobilePhone
	     --, ContactTitleID
	     --, ContactTitle
	     --, ContactEmail
	     , RetailKeyword as keyword
		 , GridImgLocationPath
	FROM #Temp
	WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
	
	IF @RowCount IS NULL									--TO CHECK IF THE RESULT SET IS EMPTY
				SET @RowCount = 0	 
	
	-------Find Clear Cache URL---03/02/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'

			SELECT @FindClearCacheURL= @CurrentURL+Screencontent-- +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
	------------------------------------------------
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerDisplayRetailLocations].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
