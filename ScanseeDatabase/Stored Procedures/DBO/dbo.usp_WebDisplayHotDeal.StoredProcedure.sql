USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayHotDeal]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/*
Stored Procedure name	: usp_WebDisplayHotDeal
Purpose					: To the detailes of the input Hot Deal ID.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			4th January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayHotDeal]				
(

	--Input Input Parameter(s)--
	
	  @HotDealID int
	
	--Output Variable--	  

	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY

		DECLARE @ProductIDs VARCHAR(MAX)
		DECLARE @ScanCodes VARCHAR(MAX)
		DECLARE @PopulationCentreID varchar(max) 
		
		--To get Media Server Configuration.  
		DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'		
		
		SELECT @ProductIDs = COALESCE(@ProductIDs+',','')+ CAST(ProductID AS VARCHAR(100)) FROM HotDealProduct WHERE ProductHotDealID = @HotDealID
		
		--SELECT @PopulationCentreID = PC.PopulationCenterID
		--FROM PopulationCenterCities PC
		--INNER JOIN ProductHotDealLocation PHDL ON PC.City = PHDL.City AND PC.[State] = PHDL.[State]
		--AND PHDL.ProductHotDealID = @HotDealID
		
		SELECT ScanCode
		INTO #TEMP
		FROM
			(SELECT P.ScanCode
			FROM  HotDealProduct HDP 
			INNER JOIN Product P ON HDP.ProductID = P.ProductID
			WHERE HDP.ProductHotDealID = @HotDealID)ScanCodes
		
		SELECT @ScanCodes = COALESCE(@ScanCodes+',','')+ CAST(ScanCode AS VARCHAR(100)) FROM #TEMP 
		
		SELECT DISTINCT PopulationCenterID
		INTO #PopulationCentres
		FROM PopulationCenterCities PC 
		INNER JOIN ProductHotDealLocation PHDL ON PC.City = PHDL.City AND PHDL.State = PC.State
		AND PHDL.ProductHotDealID = @HotDealID
	
		SELECT @PopulationCentreID = COALESCE(@PopulationCentreID + ',','')+CAST(PopulationCenterID AS VARCHAR(10)) FROM #PopulationCentres
		
		--Display the Details of the input Hot Deal.
		SELECT DISTINCT PHD.ProductHotDealID as hotDealID
		     , PHD.HotDealName as hotDealName
		     , PHD.HotDealShortDescription as hotDealShortDescription
		     , HotDealImagePath = CASE WHEN PHD.HotDealImagePath IS NOT NULL THEN 
								CASE WHEN PHD.WebsiteSourceFlag = 1 THEN 
									CASE WHEN PHD.ManufacturerID IS NOT NULL THEN @ManufacturerConfig +CONVERT(VARCHAR(30),PHD.ManufacturerID)+'/' + HotDealImagePath
																			 ELSE @RetailerConfig +CONVERT(VARCHAR(30),PHD.RetailID)+'/' + HotDealImagePath
									   END

 									ELSE PHD.HotDealImagePath
								END
					        END
		     , PHD.HotDeaLonglDescription as hotDealLongDescription
		     , PHD.HotDealURL as url
		     , CAST(PHD.HotDealStartDate AS DATE) dealStartDate
		     , CAST(PHD.HotDealEndDate AS DATE) dealEndDate
		     , CAST(PHD.HotDealStartDate AS TIME)  as dealStartTime
		     , CAST(PHD.HotDealEndDate AS TIME) as dealEndTime
		     , PHD.HotDealTermsConditions as hotDealTermsConditions
		     , PHD.HotDealDiscountAmount as hotDealDiscountAmount
			 , PHD.HotDealDiscountPct  as hotDealDiscountPct
			 , PHD.Price as price
			 , PHD.SalePrice   as salePrice      
		  -- , PHD.RetailID as retailID
		     , ProductID = CASE WHEN @ProductIDs = '0' THEN NULL ELSE @ProductIDs END
		     , @ScanCodes as scanCode     
		     --, PHDL.City as city
		     --, PHDL.[State] as state
		     , ISNULL(PHDRL.RetailLocationID , 0)retailerLocID
		     , ISNULL(RL.RetailID, 0) retailID
		     , PHD.HotDealTimeZoneID dealTimeZoneId
		     , City = CASE WHEN PHDRL.RetailLocationID IS NOT NULL AND PHDL.ProductHotDealLocationID IS NOT NULL THEN NULL ELSE @PopulationCentreID END
		     , PHD.CategoryID as bCategory
		     , C.ParentCategoryName 
		     , C.SubCategoryName 
		FROM ProductHotDeal PHD 
		LEFT OUTER JOIN HotDealProduct HDP ON PHD.ProductHotDealID = HDP.ProductHotDealID
		LEFT OUTER JOIN ProductHotDealLocation PHDL ON PHDL.ProductHotDealID = PHD.ProductHotDealID
		LEFT OUTER JOIN ProductHotDealRetailLocation PHDRL ON PHD.ProductHotDealID = PHDRL.ProductHotDealID
		LEFT OUTER JOIN RetailLocation RL ON RL.RetailLocationID = PHDRL.RetailLocationID AND Active = 1
		LEFT OUTER JOIN Product P ON HDP.ProductID = P.ProductID
		LEFT JOIN Category C ON C.CategoryID = PHD.CategoryID 
		WHERE PHD.ProductHotDealID = @HotDealID
		
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayHotDeal.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
	END CATCH	 
	
END;


GO
