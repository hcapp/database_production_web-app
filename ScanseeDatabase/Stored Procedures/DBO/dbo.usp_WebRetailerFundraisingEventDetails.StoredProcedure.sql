USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFundraisingEventDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerFundraisingEventDetails
Purpose					: To display list of Retailer Fundraising Events.
Example					: usp_WebRetailerFundraisingEventDetails

History
Version		   Date			Author		Change Description
--------------------------------------------------------------- 
1.0			11/11/2014	    SPAN		     1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerFundraisingEventDetails]
(   
    --Input variable.	  
	  @HcFundraisingID int
    , @RetailID int
		
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @HcFundraisingCategoryID Int
			--DECLARE @HcFundraisingDepartmentID Int
            DECLARE @RetailLocationIDs VArchar(1000)
			DECLARE @AppSiteFlag Bit
            DECLARE @EventID Varchar(2000)
            DECLARE @Config Varchar(500)
                     

			SELECT @Config = ScreenContent
            FROM AppConfiguration
            WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
                     
            SELECT @HcFundraisingCategoryID = HcFundraisingCategoryID
					--, @HcFundraisingDepartmentID = HcFundraisingDepartmentID
            FROM HcFundraisingCategoryAssociation 
            WHERE HcFundraisingID  = @HcFundraisingID 

            SELECT @RetailLocationIDs = COALESCE(@RetailLocationIDs+',','')+ CAST(RetailLocationID AS VARCHAR)
            FROM HcRetailerFundraisingAssociation 
            WHERE HcFundraisingID  = @HcFundraisingID 

			SELECT @AppSiteFlag = FundraisingAppsiteFlag
            FROM HcFundraising
            WHERE HcFundraisingID = @HcFundraisingID 

            --Select Event if Exist
            SELECT @EventID = COALESCE(@EventID+',', '')+CAST(HcEventID AS VARCHAR)
            FROM HcFundraisingEventsAssociation 
            WHERE HcFundraisingID = @HcFundraisingID 
              
            --To display Event Details
            SELECT DISTINCT FundraisingName eventName 
                                ,FundraisingOrganizationName organization
                                ,eventImagePath = @Config + CAST(E.RetailID AS VARCHAR(100))+'/'+FundraisingOrganizationImagePath
								,FundraisingOrganizationImagePath eventImageName    
								,ShortDescription shortDescription
								,LongDescription longDescription                                                   
                                ,E.RetailID                                                  
                                ,FundraisingGoal fundraisingGoal
                                ,CurrentLevel currentLevel                                                      
                                ,FundraisingAppsiteFlag isRetLoc
                                ,FundraisingEventFlag isEventTied
                                ,MoreInformationURL moreInfo
                                ,PurchaseProductURL purchaseProduct                                                                                                                                                         
                                ,eventDate =CAST(StartDate AS DATE)
                                ,eventEDate =ISNULL(CAST(EndDate AS DATE),NULL)
                                --,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
                                --,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)                                                                                                                   
                                ,@HcFundraisingCategoryID  eventCategory                                                        
                                ,@RetailLocationIDs retLocId
                                ,eventAssociatedIds = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(HcEventID AS VARCHAR(10)))
                                                                                FROM  HcFundraisingEventsAssociation HP
                                                                                WHERE HP.HcFundraisingID = E.HcFundraisingID
                                                                            FOR XML PATH( '' )
                                                                        ), 3, 1000 ), ' ', '')
                                ,Active
								--,@HcFundraisingDepartmentID department
								,FL.Address
								,FL.City
								,FL.State
								,FL.PostalCode
								,FL.Latitude
								,FL.Longitude
								,ISNULL(FL.GeoErrorFlag,0) geoError
            FROM HcFundraising E 
			LEFT JOIN HcFundraisingLocation FL ON E.HcFundraisingID = FL.HcFundraisingID
            --LEFT JOIN HcFundraisingEventsAssociation FE ON E.HcFundraisingID = FE.HcFundraisingID         
            WHERE E.HcFundraisingID  = @HcFundraisingID  AND RetailID = @RetailID
            AND E.Active = 1 
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebRetailerFundraisingEventDetails.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;












GO
