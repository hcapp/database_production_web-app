USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserTrackingWebConsumerMainMenuCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_UserTrackingWebConsumerMainMenuCreation  
Purpose				  : To capture the user id, module id & the location from where the user is accessing the module.  
Example				  : usp_UserTrackingWebConsumerMainMenuCreation  
  
History  
Version    Date				Author        Change Description  
---------------------------------------------------------------   
1.0        27th May 2013 Pavan SHarma K	  Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_UserTrackingWebConsumerMainMenuCreation]  
(  
   @UserID INT
 , @ModuleName VARCHAR(100)
 , @Latitude Float
 , @Longitude Float
 , @Postalcode Varchar(10)  
 --OutPut Variable  
 , @MainMenuID int output
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	BEGIN TRANSACTION
         --Display Main Menu Details
	Insert into ScanSeeReportingDatabase.dbo.MainMenu (ModuleID
													 , UserID
													 , PostalCode
													 , Latitude
													 , Longitude
													 , WebConsumer
													 , CreatedDate)
												SELECT ModuleID
													 , @UserID
													 , @Postalcode
													 , @Latitude
													 , @Longitude
													 , 1
													 , GETDATE()
												FROM ScanSeeReportingDatabase..Module
												WHERE ModuleName = LTRIM(RTRIM(@ModuleName))
							
							SELECT @MainMenuID = SCOPE_IDENTITY()
		
		 
     
       --Confirmation of Success.
       SELECT @Status=0
	COMMIT TRANSACTION
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_UserTrackingWebConsumerMainMenuCreation.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
   ROLLBACK TRANSACTION;
   --Confirmation of Failure.
       SELECT @Status=1 
  END;  
     
 END CATCH;  
END;

GO
