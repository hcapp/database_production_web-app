USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_TodayListDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_TodayListDisplay
Purpose					: To dispaly product to Add/Delete from TSL.
Example					: usp_TodayListDisplay 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_TodayListDisplay]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		SELECT UP.UserProductID 
			, UP.UserID 
			, ISNULL(R.RetailID, 0) retailerId 
			, retailerName = ISNULL(R.RetailName, 'Others')
			, C.CategoryID 
			, C.ParentCategoryID 
			, C.ParentCategoryName 
			, C.SubCategoryID 
			, C.SubCategoryName 
			, ISNULL(P.ProductID, 0) productId
			, productName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END
			, usage = CASE WHEN TodayListtItem = 1 THEN 'Green' ELSE 'Red' END 
		FROM UserProduct UP
			LEFT JOIN Product P ON P.ProductID = UP.ProductID 
			LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID
			LEFT JOIN Category C ON C.CategoryID = PC.CategoryID  
			LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID 
			LEFT JOIN Retailer R ON R.RetailID = UR.RetailID  			  
		WHERE UP.UserID = @UserID 
			--AND MasterListItem = 1
			AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE()) 
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_TodayListDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
