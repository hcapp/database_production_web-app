USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminHotDealList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebAdminHotDealList] 
Purpose				  : To get all HotDeals  
Example				  : [usp_WebAdminHotDealList] 
    
History    
Version     Date           Author    Change Description    
---------------------------------------------------------------     
1.0      30th Aug 2013     SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebAdminHotDealList] 
(    
     --Input Variables
      @SearchKey varchar(500)
	, @ColumnName varchar(100) --Output is sorted based on this column
	, @SortOrder varchar(10) --Should be ASC or DESC only
    , @LowerLimit int 
	, @ShowExpired bit 
	
	 --OutPut Variables
	, @RowCount int output    
	, @NextPageFlag bit output  
	, @Status int output 
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
		
		DECLARE @UpperLimit int    
		DECLARE @ApiPartnerID int
		DECLARE @MaxCnt int    
		  
		--To get the row count for pagination.      
		DECLARE @ScreenContent Varchar(100)    
		SELECT @ScreenContent = ScreenContent       
		FROM AppConfiguration       
		WHERE ScreenName = 'All'     
		AND ConfigurationType = 'Website Pagination'    
		AND Active = 1   

		SET @UpperLimit = @LowerLimit + @ScreenContent 
		
		--To get Media Server Configuration.  
		DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   

		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		

		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		SELECT @ApiPartnerID = APIPartnerID
		FROM APIPartner
		WHERE APIPartnerName = 'ScanSee'
				
		--To get the HotDeals based on the ShowExpired check box value.(If checked, show expired HotDeals along with active HotDeals)
		SELECT DISTINCT  ProductHotDealID
				, HotDealName
				, RetailID
				, P.APIPartnerID
				, APIPartnerName
				, HotDealShortDescription
				, HotDeaLonglDescription
				, HotDealTermsConditions
				, Price
				, SalePrice
				, HotDealDiscountAmount
				, HotDealDiscountPct
				, HotDealImagePath = CASE WHEN HotDealImagePath IS NOT NULL THEN 
										CASE WHEN WebsiteSourceFlag = 1 THEN 
												CASE WHEN ManufacturerID IS NOT NULL THEN @ManufacturerConfig + CONVERT(VARCHAR(30),ManufacturerID) +'/' + HotDealImagePath
													ELSE @RetailerConfig + CONVERT(VARCHAR(30),RetailID) +'/' + HotDealImagePath
												END
											ELSE HotDealImagePath
										END
								END  
				, HotDealStartDate
				, HotDealEndDate
				, HotDealExpirationDate
				, HotDealURL
				, CategoryID
				, Category
				, NoOfHotDealsToIssue
				, HotDealCode
		INTO #HotDealslist
		FROM ProductHotDeal P
		INNER JOIN APIPartner A ON P.APIPartnerID = A.APIPartnerID
		WHERE P.APIPartnerID <> @ApiPartnerID
		AND ((@ShowExpired = 1 AND GETDATE()>HotDealEndDate) OR ((@ShowExpired<>1 OR @ShowExpired IS NULL) AND  CAST(GETDATE() AS DATE) >=CAST(HotDealStartDate AS DATE) AND CAST(GETDATE() AS DATE) <=CAST(HotDealEndDate AS DATE)))
		--AND CASE WHEN @ShowExpired = 1 THEN GETDATE() ELSE ISNULL(HotDealEndDate, GETDATE()+1) END >= GETDATE()
		AND HotDealName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END
		
		
				
		--To acheive the Dynamic sorting.
		DECLARE @HotDeals TABLE(RowNum INT IDENTITY(1, 1)
								, ProductHotDealID INT
								, HotDealName VARCHAR(500)
								, RetailID INT
								, APIPartnerID INT
								, APIPartnerName VARCHAR(1000)
								, HotDealShortDescription VARCHAR(2000)
								, HotDeaLonglDescription VARCHAR(Max)
								, HotDealTermsConditions VARCHAR(1000)
								, Price Money
								, SalePrice money
								, HotDealDiscountAmount Money
								, HotDealDiscountPct FLOAT
								, HotDealImagePath VARCHAR(1000)
								, HotDealStartDate DATETIME
								, HotDealEndDate DATETIME
								, HotDealExpirationDate DATETIME
								, HotDealURL VARCHAR(2000)
								, CategoryID INT
								, Category VARCHAR(1000)
								, NoOfHotDealsToIssue INT
								, HotDealCode VARCHAR(100))
		INSERT INTO @HotDeals(ProductHotDealID
							    , HotDealName
							    , RetailID 
								, APIPartnerID 
								, APIPartnerName 
								, HotDealShortDescription 
								, HotDeaLonglDescription 
								, HotDealTermsConditions 
								, Price 
								, SalePrice 
								, HotDealDiscountAmount 
								, HotDealDiscountPct 
								, HotDealImagePath 
								, HotDealStartDate 
								, HotDealEndDate 
								, HotDealExpirationDate 
								, HotDealURL 
								, CategoryID 
								, Category 
								, NoOfHotDealsToIssue 
								, HotDealCode)
					SELECT  ProductHotDealID
								, HotDealName
							    , RetailID 
								, APIPartnerID 
								, APIPartnerName 
								, HotDealShortDescription 
								, HotDeaLonglDescription 
								, HotDealTermsConditions 
								, Price 
								, SalePrice 
								, HotDealDiscountAmount 
								, HotDealDiscountPct 
								, HotDealImagePath 
								, HotDealStartDate 
								, HotDealEndDate 
								, HotDealExpirationDate 
								, HotDealURL 
								, CategoryID 
								, Category 
								, NoOfHotDealsToIssue 
								, HotDealCode
					FROM #HotDealslist
					ORDER BY CASE WHEN @SortOrder = 'ASC' AND @ColumnName = 'Name' THEN HotDealName
								  WHEN @SortOrder = 'ASC' AND @ColumnName= 'Partner' THEN CAST(APIPartnerName AS SQL_VARIANT)
								  WHEN @SortOrder = 'ASC' AND @ColumnName = 'Price' THEN CAST(Price AS SQL_VARIANT)
								  WHEN @SortOrder = 'ASC' AND @ColumnName = 'SalePrice' THEN CAST(SalePrice AS SQL_VARIANT)
								  WHEN @SortOrder = 'ASC' AND @ColumnName = 'StartDate' THEN CAST(HotDealStartDate AS SQL_VARIANT)
								  WHEN @SortOrder = 'ASC' AND @ColumnName = 'EndDate' THEN CAST(HotDealEndDate AS SQL_VARIANT)
							 END ASC,
							 CASE WHEN @SortOrder = 'DESC' AND @ColumnName = 'Name' THEN HotDealName
								  WHEN @SortOrder = 'DESC' AND @ColumnName= 'Partner' THEN CAST(APIPartnerName AS SQL_VARIANT)
								  WHEN @SortOrder = 'DESC' AND @ColumnName = 'Price' THEN CAST(Price AS SQL_VARIANT)
								  WHEN @SortOrder = 'DESC' AND @ColumnName = 'SalePrice' THEN CAST(SalePrice AS SQL_VARIANT)
								  WHEN @SortOrder = 'DESC' AND @ColumnName = 'StartDate' THEN CAST(HotDealStartDate AS SQL_VARIANT)
								  WHEN @SortOrder = 'DESC' AND @ColumnName = 'EndDate' THEN CAST(HotDealEndDate AS SQL_VARIANT)
							 END DESC;

		SELECT @MaxCnt = COUNT(RowNum) FROM @HotDeals --Output Total no of Records
		SET @RowCount = @MaxCnt
		SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS 
		 
		
		--Project the records
		SELECT RowNum
				, ProductHotDealID
				, HotDealName
				, RetailID
				, APIPartnerID
				, APIPartnerName
				, HotDealShortDescription
				, HotDeaLonglDescription
				, HotDealTermsConditions
				, Price
				, SalePrice
				, HotDealDiscountAmount
				, HotDealDiscountPct
				, HotDealImagePath 
				, CAST(HotDealStartDate AS DATE) HotDealStartDate
				, CAST(HotDealStartDate AS TIME) DealStartTime
				, CAST(HotDealEndDate AS DATE) HotDealEndDate
				, CAST(HotDealEndDate AS TIME) DealEndTime
				, CAST(HotDealExpirationDate AS DATE) HotDealExpirationDate
				, CAST(HotDealExpirationDate AS TIME) ExpirationTime
				, HotDealURL
				, CategoryID
				, Category
				, NoOfHotDealsToIssue
				, HotDealCode
		FROM @HotDeals
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
	
		
		--Confirmation of Success
		SELECT @Status = 0
		
	END TRY    
     
	BEGIN CATCH    
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure [usp_WebAdminHotDealList].'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT @Status=1
		END;    
	END CATCH;
   
END;




GO
