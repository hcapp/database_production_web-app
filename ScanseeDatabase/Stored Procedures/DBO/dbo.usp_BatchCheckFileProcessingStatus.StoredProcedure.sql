USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCheckFileProcessingStatus]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchCheckFileProcessingStatus]
Purpose					: To check if the file is previously processed. 
Example					: [usp_BatchCheckFileProcessingStatus]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th Feb 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCheckFileProcessingStatus]
(
	  @APIPartnerID int
	, @FileName varchar(100)
					
	--Output Variable 	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		DECLARE @FileProcessed BIT
		DECLARE @ProcessedDate DATETIME
		
		SELECT @FileProcessed = 1
			 , @ProcessedDate = ExecutionDate 
		FROM APIBatchLog
		WHERE ProcessedFileName = @FileName
		AND APIPartnerID = @APIPartnerID
		
		 
		SELECT @FileProcessed FileProcessed
			 , @ProcessedDate ProcessedDate
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_BatchCheckFileProcessingStatus].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
