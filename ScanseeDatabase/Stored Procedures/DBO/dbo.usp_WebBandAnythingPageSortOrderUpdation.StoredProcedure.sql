USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAnythingPageSortOrderUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebBandAnythingPageSortOrderUpdation]
Purpose                  : To display the Welcome pages created by the given Band.
Example                  : [usp_WebBandAnythingPageSortOrderUpdation]

History
Version           Date                Author			 Change Description
------------------------------------------------------------------------------- 
1.0               18th Apr 2016       Sagar Byali		[usp_WebRetailerAnythingPageSortOrderUpdation]                                       
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandAnythingPageSortOrderUpdation]
(

      --Input Input Parameter(s)--  
      
        @RetailID INT
      , @AnyThingPageId Varchar(MAX) --Comma separated Anythingpageids
      , @SortOrder VARCHAR (255)
      
      --Output Variable--
	  , @Status INT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN
  
      BEGIN TRY
      
      BEGIN TRANSACTION
      
             CREATE TABLE #Temp(Rownum INT IDENTITY(1,1),SortID varchar(100))
             INSERT INTO #Temp (SortID)
			 SELECT R.Param 
			 FROM dbo.fn_SplitParam(@SortOrder, ',') R

			 --Store Sort order to temp table
			 CREATE TABLE #Temp1(Rownum INT IDENTITY(1,1),PageID varchar(100))
			 INSERT INTO #Temp1(PageID)
			 SELECT R.Param 
			 FROM dbo.fn_SplitParam(@AnyThingPageId,',') R
             
             --Store  AnyThingPageIds to temp table
			 CREATE TABLE #Temp2(Rownum INT IDENTITY(1,1),SortID VARCHAR(255),PageID VARCHAR(100))
             INSERT INTO #Temp2 (SortID,PageID )
			 SELECT	T1.SortID 
				   ,T2.PageID  
			 FROM #Temp T1 
			 INNER JOIN #Temp1 T2 ON T1.Rownum =T2.Rownum 

		   
		    --Update Anythingpage based based on sorting.
		    UPDATE QRBandCustomPage SET SortOrder = T.SortID 
		    FROM QRBandCustomPage R
	        INNER JOIN #temp2 T ON T.PageID =R.QRBandCustomPageID  
	        
	   --Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION      
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebBandAnythingPageSortOrderUpdation.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                  --Confirmation of Failure.
                  SELECT @Status =1
                  ROLLBACK TRANSACTION;   
				  
				  INSERT INTO ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
                  VALUES(ERROR_NUMBER(),ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())
           
                  
            END;
            
      END CATCH;
END;



GO
