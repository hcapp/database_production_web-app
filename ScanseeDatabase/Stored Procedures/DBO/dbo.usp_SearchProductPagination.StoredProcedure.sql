USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchProductPagination]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_SearchProductPagination  
Purpose     : To get Searched product information  
Example     : EXEC usp_SearchProductPagination 35, '38000391200',  '12347890', -73.99653, 40.750742, 0, '7/22/2011'  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   9th Aug 2011 SPAN Infotech India Initial Version   
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_SearchProductPagination]  
(  
   @UserId int  
 , @ProdSearch varchar(max)  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @MaxCnt int output
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
  --To get Media Server Configuration.  
	  DECLARE @Config varchar(50)    
	  SELECT @Config=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'   
  
  --To get the row count for pagination.  
  DECLARE @UpperLimit int   
  SELECT @UpperLimit = @LowerLimit + ScreenContent   
  FROM AppConfiguration   
  WHERE ScreenName = @ScreenName   
   AND ConfigurationType = 'Pagination'  
   AND Active = 1  
  
  --Replace Spaces with AND in the search key.
  DECLARE @SearchKey VARCHAR(1000)  
  SELECT @SearchKey = REPLACE(@ProdSearch, ' ', ' AND ')
  
 --To get product details  
  SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY P.ProductName)  
     , ProductID   
     , ProductName  
     , ScanCode    
     , M.ManufName     
     , ModelNumber   
     , ProductShortDescription productShortDescription 
     , ProductLongDescription productLongDescription
     , ProductExpirationDate   
     , ProductImagePath   = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @Config
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END 
     , SuggestedRetailPrice   
     , Weight   
     , WeightUnits   
     , ScanTypeID   
  INTO #Product          
  FROM Product P  
   INNER JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID  
  WHERE ScanCode LIKE @ProdSearch + '%'        
   OR ProductName like '%' + @ProdSearch + '%'  
   AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
   AND P.ProductID <> 0   
  --To capture max row number.  
  SELECT @MaxCnt = MAX(Row_Num) FROM #Product   
    
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
       
  SELECT Row_Num  rowNumber  
     , ProductID   
     , ProductName   
     , ManufName     
     , ModelNumber   
     , ProductShortDescription  
     , ProductLongDescription
     , ProductExpirationDate   
     , ProductImagePath imagePath   
     , SuggestedRetailPrice   
     , Weight   
     , WeightUnits      
  FROM #Product   
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit    
  ORDER BY ProductName  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_SearchProductPagination.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
