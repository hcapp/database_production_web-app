USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[BatchRealEstateProcessedDataDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: RealEstateBatchProcessingDisplay
Purpose					: To display RealEstate Processed Data Details.
Example					: RealEstateBatchProcessingDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4 Dec 2014      Mohith H R		1.0
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[BatchRealEstateProcessedDataDisplay]
(   

	--Output Variable 
	
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			--To display RealEstate processed data.
			SELECT  Title
				   ,StartDate
				   ,EndDate
				   ,CreatedUserID
				   ,DateCreated
			FROM RealEstateBatchProcessing
				
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure RealEstateBatchProcessingDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;














GO
