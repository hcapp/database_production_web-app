USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserPushNotificationDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: [usp_UsersPushNotificationDetails]  
Purpose					: Create Appversion details.  
Example					: [usp_UsersPushNotificationDetails]  
  
History  
Version  Date				Author			 Change	Description  
---------------------------------------------------------------   
1.0		 12th Sep 2013   	Dhananjaya TR	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_UserPushNotificationDetails]  
(  
       
 --OutPut Variable   
   @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
  BEGIN TRY  
 
	BEGIN TRANSACTION
 
    
   
                SELECT DISTINCT UserID
		              ,ProductID
		              ,DeviceID
		              ,ProductHotDealID	
		              ,UserPushNotificationID	
		              ,DiscountCreated              
		        INTO #Hotdeals
		        FROM (
		        SELECT U.UserID
		              ,UP.ProductID
		              ,UD.DeviceID 
		              ,HD.ProductHotDealID  
		              ,Distance= (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(Latitude  / 57.2958) * COS((Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 
		              ,LocaleRadius 
		              ,UPN.UserPushNotificationID
		              ,DiscountCreated
		        from UserPushNotification UPN
		        INNER JOIN HotDealProduct  HD ON HD.ProductID =UPN.ProductID AND HD.ProductHotDealID =UPN.ProductHotDealID        
		        INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID =HD.ProductHotDealID 		      
		        INNER JOIN UserProduct UP ON UP.ProductID =HD.ProductID AND WishListItem =1	
		        INNER JOIN Users U ON U.UserID =UP.UserID 		        
		        INNER JOIN UserDeviceAppVersion UD ON U.UserID =UD.UserID AND UD.PrimaryDevice =1
		        INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
		        INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  
		        WHERE UPN.NotificationSent =0
		              
		    )Note
		     WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
      
      
                SELECT UserID
					  ,ProductID
					  ,DeviceID
					  ,CouponID  
					  ,UserPushNotificationID	
					  ,DiscountCreated              
				INTO #Coupons
				FROM (
				SELECT U.UserID
					  ,UP.ProductID
					  ,UD.DeviceID 
					  ,CD.CouponID   
					  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
					  ,LocaleRadius 
					  ,UPN.UserPushNotificationID
					  ,DiscountCreated
				from UserPushNotification UPN
				INNER JOIN CouponProduct CD ON CD.ProductID =UPN.ProductID AND CD.CouponID =UPN.CouponID         
				INNER JOIN CouponRetailer CR ON CR.CouponID =CD.CouponID 
				INNER JOIN RetailLocation RL ON RL.RetailLocationID =CR.RetailLocationID  
				INNER JOIN UserProduct UP ON UP.ProductID =CD.ProductID AND WishListItem =1	
				INNER JOIN Users U ON U.UserID =UP.UserID 		        
		        INNER JOIN UserDeviceAppVersion UD ON U.UserID =UD.UserID AND UD.PrimaryDevice =1 
				INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
				INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode 
				WHERE UPN.NotificationSent =0 	 
				      
				)Note
				WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		        
		           
                SELECT UserID
					  ,ProductID
					  ,DeviceID
					  ,RetailLocationDealID 
					  ,UserPushNotificationID 	 
					  ,DiscountCreated             
				INTO #Sales
				FROM (
				SELECT U.UserID
					  ,UP.ProductID
					  ,UD.DeviceID 
					  ,RLP.RetailLocationDealID     
					  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
					  ,LocaleRadius
					  ,UPN.UserPushNotificationID 
					  ,DiscountCreated
				from UserPushNotification UPN
				INNER JOIN RetailLocationDeal RLP ON UPN.RetailLocationDealID=RLP.RetailLocationDealID 
				INNER JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID  AND RLP.ProductID =UPN.ProductID 
				INNER JOIN UserProduct UP ON UP.ProductID =RLP.ProductID AND WishListItem =1	
				INNER JOIN Users U ON U.UserID =UP.UserID 		        
		        INNER JOIN UserDeviceAppVersion UD ON U.UserID =UD.UserID AND UD.PrimaryDevice =1 
				INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
				INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  
				WHERE UPN.NotificationSent =0	 
					     
				)Note
				WHERE Distance <= ISNULL(LocaleRadius, (SELECT Screencontent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		       	             
		               
               SELECT USP.UserID 
                     ,USP.ProductID  
                     ,UT.UserToken UserTokenID                      
                     ,CFlag=CASE WHEN C.ProductID IS NOT NULL AND C.UserID IS NOT NULL AND C.DiscountCreated = 1 
								   THEN 2 
								   WHEN C.ProductID IS NOT NULL AND C.UserID IS NOT NULL
									THEN 1
								  ELSE 0 
							END
                     ,HDFlag=CASE WHEN H.ProductID IS NOT NULL AND H.UserID IS NOT NULL AND H.DiscountCreated = 1 
								   THEN 2 
								   WHEN H.ProductID IS NOT NULL AND H.UserID IS NOT NULL
									THEN 1
								  ELSE 0 
							END
                     ,Saleflag=CASE WHEN S.ProductID IS NOT NULL AND S.UserID IS NOT NULL AND S.DiscountCreated = 1 
								   THEN 2 
								   WHEN S.ProductID IS NOT NULL AND S.UserID IS NOT NULL
									THEN 1
								  ELSE 0 
							END                 
               INTO #Temp
               FROM UserPushNotification USP
               LEFT JOIN UserDeviceAppVersion UD ON UD.UserID =USP.UserID AND PrimaryDevice =1             
               LEFT JOIN UserToken UT ON UT.DeviceID=UD.DeviceID 
               LEFT JOIN #Hotdeals H ON H.ProductID  =USP.ProductID AND H.UserID =USP.UserID 
               LEFT JOIN #Coupons C ON C.ProductID  =USP.ProductID AND C.UserID =USP.UserID 
               LEFT JOIN #Sales S ON S.ProductID  =USP.ProductID AND S.UserID =USP.UserID
               WHERE NotificationSent=0 
               
               SELECT UserID 
                     ,ProductID productId  
                     ,UserTokenID userTokenID 
                     ,MAX(hdFlag)hdFlag
                     ,MAX(CFlag) coupFlag
                     ,MAX(Saleflag) spFlag                    
               FROM  #Temp
               GROUP BY UserID 
                     ,ProductID  
                     ,UserTokenID  
                     --,HDFlag
                     --,CFlag
                     --,Saleflag 
               HAVING  MAX(hdFlag)<>0
                     OR MAX(CFlag) <> 0
                     OR MAX(Saleflag)<>0
               
           --Once notification is sent Update push notification flag to 1          
           Update UserPushNotification  SET NotificationSent=1
           WHERE NotificationSent=0        
	--Confirmation of Success.
	SELECT @Status = 0
   COMMIT TRANSACTION
 END TRY  
    
 BEGIN CATCH 
 
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_UsersPushNotificationDetails].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
    --Confirmation of failure.
   SELECT @Status = 1 
    
   ROLLBACK TRANSACTION;  
  END;  
     
 END CATCH;  
END;

GO
