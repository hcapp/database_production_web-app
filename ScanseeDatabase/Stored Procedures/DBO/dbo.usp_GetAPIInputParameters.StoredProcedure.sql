USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAPIInputParameters]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_GetAPIInputParameters 
Purpose					: Retrieve List of input parameters that needs to be passed to an API and Submodule in context.
Example					: Declare @ErrorNumber int , @ErrorMessage varchar(1000)
						Execute usp_GetAPIInputParameters 4, 'Findnearby', @ErrorNumber output, @ErrorMessage output

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			28th July 2011	Satish Teli Initial Version  
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GetAPIInputParameters]
(
	
	@prAPIUsageID	int	
	, @PrAPISubModuleName varchar(50)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @APIUsageID int, @APISubModuleName varchar(50)
	
		-- Assign parameter value to a local variable
		Select @APIUsageID = @prAPIUsageID,
			   @APISubModuleName = @PrAPISubModuleName

		-- Retrive input parameters for API and SubModule in context
		Select 
			APIParameter apiParameter, APIparametervalue apiParamtervalue, apiDynamic  apiDynamic 
		From APIParameter
			Inner Join APIParameterDetail On APIParameter.APIParameterID = APIParameterDetail.APIParameterID
			Inner Join APISubModule On APISubModule.APISubModuleID = APIParameterDetail.APISubModuleID
		Where 
			APIUsageID = @APIUsageID
			and APISubModuleName = @APISubModuleName 
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GetAPIInputParameters.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
