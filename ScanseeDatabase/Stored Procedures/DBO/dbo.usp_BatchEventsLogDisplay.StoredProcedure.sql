USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEventsLogDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchEventsLogDisplay]
Purpose					: To get the status of the batch process that was processed.
Example					: [usp_BatchEventsLogDisplay]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEventsLogDisplay]
(
    --Input Variable
	  @FileName VARCHAR(255)
	, @Date Datetime
	  
	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
			--To get the status of the batch process that was processed.
			IF(@FileName IS NOT NULL)
			BEGIN
				SELECT REPLACE(CONVERT(VARCHAR(10), ExecutionDate, 102),'.', '-') +' '+ CONVERT(VARCHAR(10), ExecutionDate, 108) ExecutionDate
						, Status
						, Reason
						, ISNULL(TotalRowCount, 0) AS RowsRecieved
						, ISNULL(ProcessedRowCount, 0) AS RowsProcessed
						, ISNULL(DuplicatesCount, 0) AS DuplicatesCount
						, ISNULL(ExpiredCount, 0) AS ExpiredCount
						, ISNULL(MandatoryFieldsMissingCount, 0) AS MandatoryFieldsMissingCount
						, ISNULL(InvalidRecordsCount, 0) AS ExistingRecordCount
						, ISNULL(ExistingRecordCount, 0) AS InvalidRecordsCount
						, ISNULL(ImageMissingCount, 0) AS imageMissingCount
						, ISNULL(EventListingImageMissingCount, 0) AS listingImageMissingCount
						, ProcessedFileName
				FROM HcEventsBatchLog
				WHERE ProcessedFileName = @FileName
				AND CONVERT(DATE, ExecutionDate) = @Date
			END
			ELSE
			BEGIN
				SELECT REPLACE(CONVERT(VARCHAR(10), ExecutionDate, 102),'.', '-') +' '+ CONVERT(VARCHAR(10), ExecutionDate, 108) ExecutionDate
						, Status
						, Reason
						, ISNULL(TotalRowCount, 0) AS RowsRecieved
						, ISNULL(ProcessedRowCount, 0) AS RowsProcessed
						, ISNULL(DuplicatesCount, 0) AS DuplicatesCount
						, ISNULL(ExpiredCount, 0) AS ExpiredCount
						, ISNULL(MandatoryFieldsMissingCount, 0) AS MandatoryFieldsMissingCount
						, ISNULL(InvalidRecordsCount, 0) AS ExistingRecordCount
						, ISNULL(ExistingRecordCount, 0) AS InvalidRecordsCount
						, ISNULL(ImageMissingCount, 0) AS imageMissingCount
						, ISNULL(EventListingImageMissingCount, 0) AS listingImageMissingCount
						, ProcessedFileName
				FROM HcEventsBatchLog
				WHERE CONVERT(DATE, ExecutionDate) = @Date
				AND ProcessedFileName IS NULL
			END
			
				 
		   --Confirmation of Success.
		   SELECT @Status = 0
	
	END TRY
		
	BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		PRINT 'Error occured in Stored Procedure [usp_BatchEventsLogDisplay].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			
		--Confirmation of failure.
		SELECT @Status = 1
	END;
		 
	END CATCH;
END;


GO
