USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_zWebRetailerAdminRetrieveRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetrieveRetailerLocation
Purpose					: To retrieve list of Retailer's RetailLocations from the system.
Example					: usp_WebRetailerAdminRetrieveRetailerLocation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			16th July 2013			Mohith H R		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_zWebRetailerAdminRetrieveRetailerLocation]

	--Input parameters	 
	  @RetailID INT	
	, @LowerLimit int  
    , @ScreenName varchar(50)

	--Output variables
	, @MaxCnt INT OUTPUT
	, @NxtPageFlag BIT OUTPUT 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
		--To get the row count for pagination.  
		DECLARE @UpperLimit int   
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
		FROM AppConfiguration   
		WHERE ScreenName = 'RetailAdmin' 
		AND ConfigurationType = 'Pagination'
		AND Active = 1
		
		CREATE TABLE #RetailerLocationList(Row_Num INT IDENTITY(1,1)
								  ,RetailLocationID INT
							      ,StoreIdentification VARCHAR(100)							     
							      ,Address1 VARCHAR(250)
							      ,[State] CHAR(2)							      
							      ,City VARCHAR(50)
							      ,PostalCode VARCHAR(10))
		

		INSERT INTO #RetailerLocationList(RetailLocationID
								,StoreIdentification							      										
								,Address1
								,[State]
								,City
								,PostalCode)
		SELECT      RetailLocationID
				   ,StoreIdentification						   
				   ,Address1
				   ,[State]
				   ,City
				   ,PostalCode								
		FROM RetailLocation				
		WHERE RetailID = @RetailID
		AND (Headquarters=0 AND CorporateAndStore=0)
		ORDER BY Address1 ASC
				
			
		--To capture max row number. 		 
		SELECT @MaxCnt = MAX(Row_Num) FROM #RetailerLocationList
 
		 --This flag is a indicator to enable "More" button in the UI.   
		 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 	
		 
		 SELECT Row_Num row_Num
			   ,RetailLocationID
			   ,StoreIdentification						   
			   ,Address1
			   ,[State]
			   ,City	
			   ,PostalCode
		 FROM #RetailerLocationList								    
		 WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit 
		 
		 SELECT * fROM #RetailerLocationList WHERE Address1 LIKE '1979%'
		 
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetrieveRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
