USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchRestaurantDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchRestaurantDataPorting
Purpose					: To move data from Stage Deal Map table to Production ProductHotDeal Table
Example					: usp_BatchRestaurantDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Dec 2012	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchRestaurantDataPorting]
(
   ----Input variable.
	  @FileName VARCHAR(255)
	  
	--Output Variable ;
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION
	 
		DECLARE @Duplicates INT
		DECLARE @Mandatory INT
		DECLARE @Expired INT
		DECLARE @StateLength INT	
		DECLARE @RowCount INT
					   	    
	    IF NOT EXISTS(SELECT 1 FROM APIRestaurantData WHERE [PARTNER]  = 'Restaurant.com' AND PROVIDER = 'Restaurant.com')
	     BEGIN  
	       RAISERROR ('Present file Data is not related to Restaurant.com', 16,1);
         END
	     ELSE
	     BEGIN
			--Port the deals from the stage area to the hot deal table.
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'Restaurant.com'					
			
			--Insert into Batch Log table			
			INSERT INTO APIBatchLog(APIPartnerID
								  , APIPartnerName
								  , APIRowCount
								  , ExecutionDate
								  , ProcessedFileName)
						SELECT @APIPartnerID
							 , 'Restaurant.com'
							 , COUNT(1)
							 , GETDATE()
							 , @FileName
						FROM APIRestaurantData 
				
			--Delete the duplicate records in the stage area before moving to production.		
			;WITH CTE 
			(
				 PRICE
				,[DISCOUNT AMOUNT] 
				,[SALE PRICE]
				,NAME
				,[SHORT DESCRIPTION]
				,[LONG DESCRIPTION]
				,IMAGEURL
				,[TERMS AND CONDITIONS]
				,URL
				,STARTDATE
				,ENDDATE
				,CATEGORY
				,CITY
				,STATE	
				,RWNO
			)
			AS
			(
				SELECT PRICE
				      ,[DISCOUNT AMOUNT]
				      ,[SALE PRICE]
				      , NAME
				      ,[SHORT DESCRIPTION]
				      ,[LONG DESCRIPTION]
				      ,IMAGEURL
				      ,[TERMS AND CONDITIONS]
				      ,URL
				      ,STARTDATE
				      ,ENDDATE
				      ,CATEGORY
				      ,CITY
				      ,STATE
				      ,ROW_NUMBER() OVER (PARTITION BY NAME, URL ORDER BY NAME) AS RWNO 
				 FROM APIRestaurantData 	
			) 
			DELETE FROM CTE WHERE RWNO > 1						
			SELECT @Duplicates = @@ROWCOUNT 
						
			--Deleting expired hot deals from stage table.			
			DELETE FROM APIRestaurantData WHERE GETDATE()> ISNULL(ENDDATE,GETDATE()+1)
			SELECT @Expired  = @@ROWCOUNT 
			
			--Delete Hot Deals if Name and URL not provided.
			DELETE FROM APIRestaurantData WHERE NAME IS NULL OR URL IS NULL
			SELECT @Mandatory = @@ROWCOUNT 
			
			--To Delete Hot Deals if StateLength > 2, from Stage Table.
			DELETE FROM APIRestaurantData WHERE LEN(State)>2
			SELECT @StateLength = @@ROWCOUNT
			
			
			--To delete the associated RetailLocation before deleting from the hot deals table.
			   SELECT * 
				INTO #Temp
				FROM ProductHotDeal T
				WHERE CAST(T.HotDealEndDate AS DATE) <CAST(GETDATE()AS DATE)
				AND T.[APIPartnerID] = @APIPartnerID
				
				DELETE FROM ProductHotDealRetailLocation
				FROM ProductHotDealRetailLocation PRL
				INNER JOIN #Temp T ON T.ProductHotDealID = PRL.ProductHotDealID 
				
				INSERT INTO [ProductHotDealHistory]
							([ProductHotDealID]					
							,[RetailID]
							,[APIPartnerID]
							,[HotDealProvider]
							,[Price]
							,[SalePrice]
							,[HotDealName]
							,[HotDealShortDescription]
							,[HotDeaLonglDescription]
							,[HotDealImagePath]
							,[HotDealTermsConditions]
							,[HotDealURL]
							,[HotDealStartDate]
							,[HotDealEndDate]
							,[Category]
							,[CreatedDate]
							,[DateModified])
					SELECT [ProductHotDealID]					
							,[RetailID]
							,[APIPartnerID]
							,[HotDealProvider]
							,[Price]
							,[SalePrice]
							,[HotDealName]
							,[HotDealShortDescription]
							,[HotDeaLonglDescription]
							,[HotDealImagePath]
							,[HotDealTermsConditions]
							,[HotDealURL]
							,[HotDealStartDate]
							,[HotDealEndDate]
							,[Category]
							,[CreatedDate]
							,[DateModified]
					FROM #Temp 	
					
				DELETE FROM ProductHotDeal 
				WHERE CAST(HotDealEndDate AS DATE) <CAST(GETDATE()AS DATE)
				AND [APIPartnerID] = @APIPartnerID
				
				
					MERGE ProductHotDeal AS T
					USING APIRestaurantData AS S
					ON T.[APIPartnerID] = @APIPartnerID 
					AND T.HotDealName = S.Name
					AND ISNULL(T.[HotDealShortDescription], 0) = ISNULL(S.[Short Description], 0)
					AND T.HotDealURL = S.URL
					AND S.NAME IS NOT NULL
					
					WHEN NOT MATCHED BY TARGET  --AND T.[APIPartnerID] = @APIPartnerID  
						THEN INSERT([APIPartnerID]
										 ,[HotDealProvider]
										 ,[Price]	
										 ,[HotDealDiscountPct]					
										 ,[SalePrice]
										 ,[HotDealName]
										 ,[HotDealShortDescription]
										 ,[HotDeaLonglDescription]
										 ,[HotDealImagePath]
										 ,[HotDealTermsConditions]
										 ,[HotDealURL]
										 ,[HotDealStartDate]
										 ,[HotDealEndDate]
										 ,[Category]
										 ,[CreatedDate]) 
								VALUES(@APIPartnerID
										,S.[Provider]
										,S.[Price]
										,S.[Discount Amount]
										,S.[Sale Price]
										,S.[Name]
										,S.[Short Description]
										,S.[Long Description]
										,S.[ImageURL]
										,S.[Terms And Conditions]
										,S.[URL]
										,S.[StartDate]
										,S.[Enddate]
										,S.[Category]
										,Getdate())										
					WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID 
						THEN UPDATE SET T.[Price] = S.[Price]
										 ,T.[HotDealDiscountPct] = S.[Discount Amount]
										 ,T.[SalePrice] = S.[Sale Price]
										 ,T.[HotDealName] = S.[Name]
										 ,T.[HotDealShortDescription] = S.[Short Description]
										 ,T.[HotDeaLonglDescription] = S.[Long Description]
										 ,T.[HotDealImagePath] = S.[ImageURL]
										 ,T.[HotDealTermsConditions] = S.[Terms And Conditions]
										 ,T.[HotDealURL] = S.[URL]
										 ,T.[HotDealStartDate] = S.[StartDate]
										 ,T.[HotDealEndDate] = S.[EndDate]
										 ,T.[Category] = S.[Category]
										 ,T.[CreatedDate] = GETDATE()
										 ,T.[DateModified] = GETDATE();
										 
			
		
					--Capture the number of records that got inserted or updated from the current file.
					SELECT @RowCount = @@ROWCOUNT 						
				
				--Update the log with the counts.
				UPDATE APIBatchLog 
				SET DuplicatesCount = @Duplicates
				  , ExpiredCount = @Expired
				  , MandatoryFieldsMissingCount = @Mandatory
				  , InvalidRecordsCount = @StateLength
				  ,	ProcessedRowCount = @RowCount
				WHERE ProcessedFileName = @FileName
				AND CONVERT(DATE, GETDATE()) = CONVERT(DATE, ExecutionDate) 
				
	       
	       END
	       
	       --Confirmation of Success.
		   SELECT @Status = 0
		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		    SELECT ERROR_MESSAGE()
		 
			PRINT 'Error occured in Stored Procedure usp_BatchRestaurantDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
