USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierManageProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebSupplierManageProductSearch  
Purpose     : To display the product Details for the input UPC/ Product Name.  
Example     : EXEC usp_WebSupplierManageProductSearch  
  
History  
Version  Date       Author    Change Description  
-----------------------------------------------------------------------------------   
1.0   14st December 2011    Pavan Sharma K  Initial Version  
-----------------------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebSupplierManageProductSearch]  
(  
  
 --Input Input Parameter(s)--  
     
   @ManufacturerID int  
 , @SearchParameter varchar(1100)  
 , @LowerLimit int  
   
 --Output Variable--     
   
 , @RowCount int output  
    , @NextPageFlag bit output   
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
   
     DECLARE @Config varchar(50)  
  SELECT @Config=ScreenContent  
  FROM AppConfiguration   
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
    
  DECLARE @RowCnt INT  
     DECLARE @MaxCnt INT  
     DECLARE @UpperLimit INT  
       
     --To get the row count for pagination.    
     DECLARE @ScreenContent Varchar(100)  
  SELECT @ScreenContent = ScreenContent     
  FROM AppConfiguration     
  WHERE ScreenName = 'All'   
   AND ConfigurationType = 'Website Pagination'  
   AND Active = 1     
      
     SELECT @UpperLimit = @LowerLimit + @ScreenContent  
       
     SELECT   RowNum = ROW_NUMBER() OVER (ORDER BY productID ASC)  
      ,ProductID  
            , ScanCode  
         , ProductName  
         , ModelNumber  
         , SuggestedRetailPrice  
         , imagePath       
         , longDescription  
         , shortDescription  
         , warranty  
            , CategoryID  
         , category  
  INTO #Temp  
  FROM    
    (SELECT  DISTINCT P.ProductID  
       ,P.ScanCode  
       , P.ProductName  
       , P.ModelNumber  
       , P.SuggestedRetailPrice  
       , imagePath = CASE WHEN WebsiteSourceFlag = 1 AND ProductImagePath IS NOT NULL THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID) + '/'+  ProductImagePath   
         ELSE ProductImagePath END   
      -- , @Config + PM.ProductMediaPath ProductMediaPath  
       , P.ProductLongDescription longDescription  
       , P.ProductShortDescription shortDescription  
       , P.WarrantyServiceInformation warranty  
       , P.ProductExpirationDate  
       --, PA.AttributeName prodAttributeName  
       --, PA.DisplayValue prodDisplayValue  
       , PC.CategoryID  
       , C.ParentCategoryName category  
       FROM Product P  
   --LEFT OUTER JOIN ProductAttributes PA ON P.ProductID = PA.ProductID      
   LEFT OUTER JOIN ProductMedia PM ON P.ProductID = PM.ProductID    
   LEFT OUTER  JOIN ProductCategory PC ON P.ProductID = PC.ProductID   
   LEFT OUTER  JOIN Category C ON PC.CategoryID = C.CategoryID   
    
  WHERE P.ProductID <> 0 
  AND (P.ProductName  LIKE (CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%'   
  ELSE '%' END)                                                             --TO MATCH WITH ALL THE PRODUCTS OF THAT MANUFACTURER  
  OR P.ScanCode = @SearchParameter) AND P.ManufacturerID = @ManufacturerID) Products  
      
         
        SELECT @MaxCnt = MAX(RowNum) FROM #TEMP  
       
      SET @RowCount = @MaxCnt  
       
      SELECT @NextPageFlag = (CASE WHEN (@MaxCnt - @UpperLimit)>0 THEN 1 ELSE 0 END)  
        
       SELECT  RowNum,  
     ProductID productID  
            , ScanCode  
         , ProductName  
         , ModelNumber  
         , SuggestedRetailPrice  
         , imagePath       
         , longDescription  
         , shortDescription  
         , warranty  
            , CategoryID category  
  FROM #Temp   
  WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit    
   
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebSupplierManageProductSearch.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
