USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_PreferredCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_PreferredCategory
Purpose					: 
Example					: usp_PreferredCategory 2, '', '7/4/2011'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_PreferredCategory]
(
	@UserID int
	,@Category varchar(max)
	,@Date datetime
	
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			IF ISNULL(@Category, '') <> ''
			BEGIN
				SELECT Param Category
				INTO #Category
				FROM fn_SplitParam(@Category, ',')
			
				--To capture not existing categories.
				INSERT INTO [UserCategory]
					   ([UserID]
					   ,[CategoryID]
					   ,[DateAdded])
				SELECT @UserID 
					, Category 
					, @Date 
				FROM #Category C
				LEFT JOIN UserCategory UC ON UC.CategoryID = C.Category AND UC.UserID = @UserID 
				WHERE UC.CategoryID IS NULL
				
				--To revome disabled categories.
				DELETE FROM UserCategory WHERE UserID = @UserID AND CategoryID NOT IN (SELECT Category FROM #Category)
			END
			ELSE IF ISNULL(@Category, '') = ''
			BEGIN
				--To revome disabled categories.
				DELETE FROM UserCategory WHERE UserID = @UserID --AND CategoryID NOT IN (ISNULL(@Category, ''))
			END
			
			--Confirmation of Success
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_PreferredCategory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			
			--Confirmation of Failure
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
