USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandEventsCreationAndUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Stored Procedure name	: [usp_WebBandEventsCreationAndUpdation]
Purpose					: To Create and Update Band Event.
Example					: [usp_WebBandEventsCreationAndUpdation]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			04/19/2016     Bindu T A            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandEventsCreationAndUpdation]
(
     
	 --Input variable	  
	  @RetailID Int
	, @EventID Int
	, @EventName Varchar(1000)
	, @ShortDescription Varchar(2000)
    , @LongDescription Varchar(2000) 
	, @ImagePath Varchar(2000)
	, @MoreInformationURL Varchar(2000)
	, @EventCategoryID Varchar(1000)
	, @OngoingEvent bit 
	, @StartDate DATE
	, @StartTime TIME
	, @EndDate date
	, @EndTime time
	, @RecurrencePatternID int
	, @RecurrenceInterval int
	, @EveryWeekday bit
	, @Days varchar(1000)
	, @EndAfter int
	, @DayNumber int
	, @BussinessEvent Bit  
	, @BandRetailID int
	, @LocationID Varchar(max)  -- Comma separated ID's
	, @Address Varchar(2000)
    , @City varchar(200)
    , @State Varchar(100)
    , @PostalCode VARCHAR(10)
    , @Latitude Float
    , @Longitude Float
    , @GeoErrorFlag BIT
	, @EventListingImagePath Varchar(2000)
	, @EventLocationKeyword Varchar(150)
	
	--Output Variable 		
    , @Status int output        
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION  
	 
				
	          
								
			DECLARE @RecurrencePattern varchar(10)

			SELECT @EventID = ISNULL(@EventID,0)

			SELECT @EveryWeekday = ISNULL(@EveryWeekday, 0)

			SELECT @Days = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Days, '1', 'Sunday'), '2', 'Monday'), '3', 'Tuesday'), '4', 'Wednesday'), '5', 'Thursday'), '6', 'Friday'), '7', 'Saturday')

			SELECT @RecurrencePattern = RecurrencePattern FROM HcEventRecurrencePattern WHERE HcEventRecurrencePatternID = @RecurrencePatternID

			IF @EveryWeekday = 1
			BEGIN
				SELECT @Days = 'Monday,Tuesday,Wednesday,Thursday,Friday'				
			END


			IF EXISTS (SELECT 1 FROM HcBandEvents WHERE HcBandEventID = @EventID)
				BEGIN
					--SELECT 'A'
						--Updating BandEvent		
						UPDATE HcBandEvents 
						SET HcBandEventName = LTRIM(RTRIM(@EventName))  
											, ShortDescription = @ShortDescription
											, LongDescription = @LongDescription								
											, ImagePath = @ImagePath
											, EventListingImagePath = @EventListingImagePath
											, BussinessEvent = @BussinessEvent 
											, StartDate = CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
											, EndDate = IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))--EndDate =  CAST(ISNULL(@EndDate, @StartDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, '23:59:00.000') AS DATETIME)
											--, EndDate =  CAST(ISNULL(@EndDate, @StartDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, '23:59:00.000') AS DATETIME)
											--, EndDate = IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
											, DateModified = GETDATE() 
											, MoreInformationURL= @MoreInformationURL
											, OnGoingEvent = @OngoingEvent
											, HcEventRecurrencePatternID = IIF(@OngoingEvent = 1, @RecurrencePatternID, NULL) 
											, RecurrenceInterval = IIF(@OngoingEvent = 1, CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
																			THEN @RecurrenceInterval 
																		ELSE NULL 
																   END, NULL)
											, EventFrequency =  IIF(@OngoingEvent = 1, @EndAfter, NULL)
											, BandID = @RetailID
											, Active = 1
						WHERE HcBandEventID = @EventID	

						UPDATE HcBandEventsAssociation
						SET 
						BandID= @RetailID,
						DateModified= GETDATE(),
						CreatedUserID= 3,
						BandStartDate = CAST(@StartDate AS DATETIME),
						BandEndDate = CAST (@EndDate AS DATETIME),
						BandStartTime = @StartTime,
						BandEndTime = @EndTime
						WHERE HcBandEventID= @EventID

				END
			ELSE 
				BEGIN
					--SELECT 'B'
						--Creating New Band Event		
						INSERT INTO HcBandEvents(HcBandEventName  
											, ShortDescription
											, LongDescription 								
											, ImagePath
											, EventListingImagePath
											, BussinessEvent 
											, StartDate 
											, EndDate 
											, DateCreated  
											, MoreInformationURL
											, OnGoingEvent
											, HcEventRecurrencePatternID
											, RecurrenceInterval
											, EventFrequency
											, BandID
											, Active
											)	
						SELECT LTRIM(RTRIM(@EventName))
							 , @ShortDescription 
							 , @LongDescription 			      
							 , @ImagePath 
							 , @EventListingImagePath
							 , @BussinessEvent
							 , CAST(@StartDate AS DATETIME)+' '+CAST(@StartTime AS DATETIME) 
							 --, @EndDate
							   , CAST(ISNULL(@EndDate, @StartDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, '23:59:00.000') AS DATETIME)
							  -- , IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
							-- , IIF(@OngoingEvent = 0, CAST(ISNULL(@EndDate, @EndDate) AS DATETIME)+' '+CAST(ISNULL(@EndTime, @EndTime) AS DATETIME), CAST(@EndDate AS DATETIME)+' ' + CAST(@EndTime AS DATETIME))
							 , GETDATE()
							 , @MoreInformationURL	
							 , @OngoingEvent
							 , @RecurrencePatternID
							 , CASE WHEN (@RecurrencePattern = 'Daily' AND @EveryWeekday = 0) OR (@RecurrencePattern = 'Weekly') 
										THEN @RecurrenceInterval 
									ELSE NULL 
							   END
							 , @EndAfter
							 , @RetailID
							 , 1
					
						SET @EventID = SCOPE_IDENTITY()
						
						INSERT INTO HcBandEventsAssociation( HcBandEventID,BandID, DateCreated,CreatedUserID,BandStartDate,BandEndDate,BandStartTime,BandEndTime )
						VALUES( @EventID, @RetailID,GETDATE(), 3, @StartDate,@EndDate, @StartTime,@EndTime)
				
				END
			
			--Deleting EventCategory association
			DELETE FROM HcBandEventsCategoryAssociation
			WHERE HcBandEventID = @EventID

			--Associating Categories to Event
			INSERT INTO HcBandEventsCategoryAssociation(HcBandEventCategoryID 
													, HcBandEventID 
													, DateCreated
													, CreatedUserID)	
			SELECT [Param]
			      , @EventID 
			      , GETDATE()
			      , NULL
			FROM dbo.fn_SplitParam(@EventCategoryID, ',') 


			--Delete event Locations if already Existed
			DELETE FROM HcBandEventLocation 
			WHERE HcBandEventID = @EventID 

			delete from HcbandretailerEventsAssociation
			WHERE HcBandEventID = @EventID



			--Insert Event Details to Event Location Table
			IF (@BussinessEvent = 0 OR @BussinessEvent IS NULL)
			BEGIN

						DELETE FROM HcBandEventLocation
						WHERE HcBandEventID = @EventID 

					INSERT INTO HcBandEventLocation(HcBandEventID
												, Address
												, City
												, State
												, PostalCode
												, Latitude
												, Longitude
												, GeoErrorFlag
												, DateCreated, EventLocationKeyword)
					SELECT  @EventID 
							, @Address 
							, @City
							, @State 
							, @PostalCode 
							, @Latitude
							, @Longitude 
							, @GeoErrorFlag
							, GETDATE(), @EventLocationKeyword
			END
	
			--Deleting RetailLocations for given Retailer
			DELETE FROM HcbandretailerEventsAssociation
			WHERE HcBandEventID = @EventID
							 
			IF @BussinessEvent = 1
			BEGIN
						
						INSERT INTO HcbandretailerEventsAssociation(HcbandEventID
																, RetailID
																, RetailLocationID
																, DateCreated)
						SELECT  @EventID
							  , @BandRetailID
							  , [Param]
							  , GETDATE()
						FROM dbo.fn_SplitParam(@LocationID, ',')
					
					
			END
			
			DELETE FROM HcBandEventInterval
			WHERE HcBandEventID = @EventID	
			   	
			--For On Going Events insert the ongoing pattern into the association tables.
			IF @OngoingEvent = 1
			BEGIN
				--If the event is created as Daily with a inteval value then do not insert record into HcEventInterval table.
				--IF ((@RecurrencePattern <> 'Daily' AND @EveryWeekday <> 0) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				IF ((@RecurrencePattern = 'Weekly' OR @RecurrencePattern = 'Monthly' ) OR (@RecurrencePattern = 'Daily' AND @EveryWeekday = 1))
				BEGIN
					DELETE FROM HcBandEventInterval WHERE HcBandEventID = @EventID
					INSERT INTO HcBandEventInterval(HcBandEventID
											  , DayNumber
											  , MonthInterval
											  , DayName
											  , DateCreated)
											  --, CreatedUserID)
									SELECT @EventID
										 , @DayNumber
										 , IIF(@RecurrencePattern ='Monthly', @RecurrenceInterval, NULL)
										 , LTRIM(RTRIM(REPLACE(REPLACE(Param, '[', ''), ']', '')))
										 , GETDATE()
										 --, @UserID
									FROM [HubCitiWeb].fn_SplitParam(@Days, ',') A
				END
			END
				
	       --Confirmation of Success.
		   SELECT @Status = 0

		COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		
		
		--select 'ggg'  as v into v 
			PRINT 'Error occured in Stored Procedure [usp_WebBandEventsCreationAndUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;

			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
