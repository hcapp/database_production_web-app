USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerAddtoShoppingListfromFavorite]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerAddtoShoppingListfromFavorite  
Purpose               : To search for the product.  
Example               : usp_WebConsumerAddtoShoppingListfromFavorite   
 
Version    Date              Author         Change Description  
---------------------------------------------------------------   
1.0        2nd Aug 2013      Dhananjaya TR  Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerAddtoShoppingListfromFavorite]  
(  
   @UserID int  
 , @UserProductID varchar(max)  
 
 --User Tracking Inputs
 , @MainMenuID int
   
 --Output Variable   
 , @ProductExists Bit output
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
  
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
    
   SET @ProductExists=0
   
   -- To fetch Product which already exists for the user (Active and Inactive Products). 
   
		IF EXISTS (Select 1 from UserProduct UP
				   INNER JOIN fn_SplitParam(@UserProductID,',') F ON F.Param =UP.UserProductID   
				   where UserID =@UserID AND TodayListtItem =1)
			BEGIN
			  SET @ProductExists =1
			END    
   
        Update UserProduct SET TodayListtItem =1
        FROM UserProduct UP
        INNER JOIN fn_SplitParam(@UserProductID,',') F ON F.Param =UP.UserProductID  
		WHERE UserID =@UserID
		AND TodayListtItem =0 
	
		
		 --User Tracking Section
		Update ScanSeeReportingDatabase..UserProduct SET TodayListtItem =1
		FROM ScanSeeReportingDatabase..UserProduct UP
        INNER JOIN fn_SplitParam(@UserProductID,',') F ON F.Param =UP.UserProductID   
		WHERE UserID =@UserID
		AND TodayListtItem =0 	  
    
   --Confirmation of Success.  
   SELECT @Status = 0  
  COMMIT TRANSACTION    
   
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerAddtoShoppingListfromFavorite.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;


GO
