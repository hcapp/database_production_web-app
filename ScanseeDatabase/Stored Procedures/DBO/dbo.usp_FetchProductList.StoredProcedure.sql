USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FetchProductList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FetchProductList
Purpose					: To list Products of a category 
Example					: EXEC usp_FetchProductList 35, 2, 0, 50
History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			5/28/2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_FetchProductList]
(
	  @UserID int
	, @CategoryID int
	, @LowerLimit int
	, @UpperLimit int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			;WITH ProductList
			AS
			(
				SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY P.ProductName)
					, P.ProductID 
					, P.ProductName 
					, P.ManufacturerID  
					, P.ModelNumber 
					, P.ProductLongDescription 
					, P.ProductExpirationDate 
					, P.ProductImagePath 
					, P.SuggestedRetailPrice 
					, P.Weight 
					, P.WeightUnits 		
				FROM ProductCategory PC 
					INNER JOIN Product P ON P.ProductID = PC.ProductID
				WHERE PC.CategoryID  = @CategoryID
					AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())
			)
			SELECT Row_Num  rowNumber
					, ProductID productId
					, ProductName  productName
					, ManufName  manufacturersName
					, ModelNumber modelNumber
					, ProductLongDescription productDescription
					, ProductExpirationDate productExpDate
					, ProductImagePath imagePath
					, SuggestedRetailPrice suggestedRetailPrice
					, Weight productWeight
					, WeightUnits weightUnits
			FROM ProductList PL
				INNER JOIN Manufacturer M ON M.ManufacturerID = PL.ManufacturerID 
			WHERE Row_Num BETWEEN (@LowerLimit+1) AND (@UpperLimit)			
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FetchProductList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
