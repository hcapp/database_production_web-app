USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEventsLogInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchEventsLogInsertion]
Purpose					: To insert into batch Events log table when file is not there to process.
Example					: [usp_BatchEventsLogInsertion]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/20/2015       Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEventsLogInsertion]
(
    --Input Variable
	  @EventStatus BIT
	, @Reason VARCHAR(1000)

	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION 

			INSERT INTO HcEventsBatchLog(ExecutionDate
										,Status
										,Reason
										,TotalRowCount
										,ProcessedRowCount
										,DuplicatesCount
										,ExpiredCount
										,MandatoryFieldsMissingCount
										,InvalidRecordsCount
										,ExistingRecordCount
										,ImageMissingCount
										,EventListingImageMissingCount
										)
								VALUES(GETDATE()
									  , @EventStatus
									  , @Reason
									  , 0
									  , 0
									  , 0
									  , 0
									  , 0
									  , 0
									  , 0
									  , 0
									  , 0
									  )

			
		   --Confirmation of Success.
		   SELECT @Status = 0

		COMMIT TRANSACTION 

	END TRY
		
	BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		PRINT 'Error occured in Stored Procedure [usp_BatchEventsLogInsertion].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
		ROLLBACK TRANSACTION 	
		--Confirmation of failure.
		SELECT @Status = 1
	END;
		 
	END CATCH;
END;


GO
