USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerProductMediaDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WishListProductAttributes  
Purpose     : To dispaly Product Attributes in Wish List  
Example     : usp_WishListProductAttributes  
  
History  
Version  Date              Author            Change Description  
---------------------------------------------------------------   
1.0      9th may 2013      Dhananjaya TR     Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerProductMediaDisplay]  
(  
   @ProductID int 
   
 --User Tracking Inputs
 , @MainMenuID int 
 , @ProductListID int 
   
 --Output Variable   
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	BEGIN TRANSACTION
 
   --To get Media Server Configuration. 
		 DECLARE @ManufConfig varchar(50) 
		  
		 SELECT @ManufConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
  -- To capture the existance of the media for the specified product.  
            DECLARE @VideoFlag bit  
            DECLARE @AudioFlag bit  
            DECLARE @FileFlag bit  
          
            SELECT Rownum=identity(int,1,1)
                  , PM.ProductMediaID 
                  , PM.ProductID 
                  ,Video=CASE WHEN PT.ProductMediaType = 'Video Files' AND ProductMediaPath IS NOT NULL AND ProductMediaPath <>'' THEN @ManufConfig + CAST(P.ManufacturerID AS VARCHAR(100)) +'/' + ProductMediaPath END                   
            INTO #Temp
            FROM ProductMedia PM  
            INNER JOIN Product P ON P.ProductID = PM.ProductID
            INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
            WHERE PM.ProductID = @ProductID AND PT.ProductMediaType ='Video Files'
            
            
            SELECT Row=IDENTITY (int,1,1)
                 , PM.ProductMediaID 
                 , PM.ProductID 
                 , Audio=CASE WHEN PT.ProductMediaType = 'Audio Files' AND ProductMediaPath IS NOT NULL AND ProductMediaPath <>'' THEN @ManufConfig + CAST(P.ManufacturerID AS VARCHAR(100)) + '/' + ProductMediaPath END 
            INTO #Temp1
            FROM ProductMedia PM   
            INNER JOIN Product P ON P.ProductID = PM.ProductID
            INNER JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID   
            WHERE PM.ProductID = @ProductID AND PT.ProductMediaType = 'Audio Files' 
   
			SELECT CASE WHEN T.ProductMediaID IS NOT NULL THEN T.ProductMediaID ELSE T1.ProductMediaID END ProductMediaID
			      ,Video videoFilePath
			      ,Audio audioFilePath			
			INTO #Media
			FROM #Temp T
			FULL OUTER JOIN #Temp1 T1 ON T.Rownum =T1.Row 
			WHERE (Video IS NOT NULL OR Audio IS NOT NULL)
			
			--User Tracking
			CREATE TABLE #Temp2(ProductMediaListID INT								
							  , ProductMediaID INT)
			
			INSERT INTO ScanSeeReportingDatabase..ProductMediaList(ProductListID
																	,ProductMediaID
																	,ProductMediaClick
																	,CreatedDate)
			OUTPUT inserted.ProductMediaListID,inserted.ProductMediaID INTO #Temp2(ProductMediaListID,ProductMediaID)
			SELECT @ProductListID 
			      ,ProductMediaID 
			      ,0
			      ,GETDATE()  
			FROM #Media 
																	
			SELECT T.ProductMediaListID
			      ,M.audioFilePath 
			      ,M.videoFilePath 			
			FROM #Media M
			INNER JOIN #Temp2 T ON T.ProductMediaID =M.ProductMediaID 			
			
		--Confirmation of Success.
		SELECT @Status = 0
     COMMIT TRANSACTION
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure <usp_WebConsumerProductAttributes>.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION; 
   --Confirmation of failure.
   SELECT @Status = 1 
  END;  
     
 END CATCH;  
END;


GO
