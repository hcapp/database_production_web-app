USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadRetailerProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadRetailerProducts
Purpose					: To upload product data from stage table UploadManufacturerProducts to production table Products
Example					: usp_WebUploadRetailerProducts

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0		3rd January 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadRetailerProducts]
(
	
	  
	   @UserID int
     , @RetailerID int
     , @FileName varchar(1000)
     , @UploadRetailLocationProductLogID int
 	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			
			Declare @TotalRows int
		    Declare @SuccessRows int
		    Declare @FailureRows int
			
			--To capture execution start date & time
			DECLARE @startDate datetime
			SET @startDate=GETDATE()
			
			------- Processing Product Data --------------------------------------
			--Filter unique products.
			SELECT MIN(URLP.UploadRetailLocationProductID)UploadRetailLocationProductID
			INTO #UniqueRetailLocationProducts
			FROM UploadRetailLocationProduct URLP
			WHERE URLP.RetailerID = @RetailerID
			GROUP BY StoreIdentification, ScanCode
			
			--Fetch the Duplicate records.
			SELECT UploadRetailLocationProductID
			     , StoreIdentification
			     , ScanCode
			     , RetailLocationProductDescription
				 , Quantity
				 , Price
				 , SalePrice
				 , SaleStartDate
				 , SaleEndDate
				 , FileName
				 , UserID
				 , RetailerID
			INTO #DuplicateRecords
			FROM UploadRetailLocationProduct URLP			
			WHERE UploadRetailLocationProductID NOT IN(SELECT UploadRetailLocationProductID FROM #UniqueRetailLocationProducts)
			AND URLP.RetailerID = @RetailerID
			AND URLP.UserID = @UserID
			
			
			--Filter all the valid products and store.
			SELECT UploadRetailLocationProductID
				 , RetailLocationID
                 , ProductID
                 , RetailLocationProductDescription
                 , Quantity
                 , Price
                 , SalePrice
                 , SaleStartDate
                 , SaleEndDate
            INTO #Temp
            FROM  (	SELECT URLP.UploadRetailLocationProductID
                         , RL.RetailLocationID
						 , P.ProductID
						 , URLP.RetailLocationProductDescription
						 , URLP.Quantity
						 , URLP.Price
						 , URLP.SalePrice
						 , URLP.SaleStartDate
						 , URLP.SaleEndDate		                 
					FROM UploadRetailLocationProduct URLP 
					INNER JOIN Product P ON URLP.ScanCode = P.ScanCode
					INNER JOIN RetailLocation RL ON RL.StoreIdentification = URLP.StoreIdentification AND RL.Active = 1
					WHERE URLP.StoreIdentification IS NOT NULL 
					AND URLP.UploadRetailLocationProductID IN (SELECT UploadRetailLocationProductID FROM #UniqueRetailLocationProducts)
					AND URLP.ScanCode IS NOT NULL 
					AND URLP.RetailerID = @RetailerID
					AND RL.RetailID =@RetailerID) RetailLocations
					
			--Update if Already Exists.	
				
			UPDATE RetailLocationProduct SET Price = ExistingProducts.Price
										   , Quantity = ExistingProducts.Quantity
										   , RetailLocationProductDescription = ExistingProducts.RetailLocationProductDescription
										   , SaleStartDate = ExistingProducts.SaleStartDate
										   , SaleEndDate = ExistingProducts.SaleEndDate
			FROM (SELECT T.Price
			           , T.Quantity
			           , T.RetailLocationProductDescription
			           , T.SaleStartDate
			           , T.SaleEndDate
			           , T.ProductID
			           , T.RetailLocationID
				  FROM #Temp T
				  INNER JOIN RetailLocationProduct RLP ON RLP.ProductID = T.ProductID) ExistingProducts
            WHERE ExistingProducts.ProductID = RetailLocationProduct.ProductID
            AND ExistingProducts.RetailLocationID = RetailLocationProduct.RetailLocationID           
            
            --Insert only new products.
            
            INSERT INTO RetailLocationProduct (
												RetailLocationID
											  , ProductID
											  , RetailLocationProductDescription
											  , Quantity
											  , Price							
											  , SalePrice		
											  , SaleStartDate
											  , SaleEndDate		 									
											  , DateCreated							  
											  )	
					SELECT T.RetailLocationID
						 , T.ProductID
						 , T.RetailLocationProductDescription
						 , T.Quantity
						 , T.Price		
						 , T.SalePrice		
						 , T.SaleStartDate
						 , T.SaleEndDate		
						 , GETDATE()
					FROM #Temp T
					LEFT OUTER JOIN RetailLocationProduct RLP ON T.ProductID = RLP.ProductID AND T.RetailLocationID = RLP.RetailLocationID
					WHERE RLP.ProductID IS NULL AND RLP.RetailLocationID IS NULL
					 
					
					
             --Capture Successful Row Count.
            Select @SuccessRows = @@ROWCOUNT 					
					
			--Capture the deals in a Temporary table for furthur processing.	
			SELECT UploadRetailLocationProductID
				 , RetailLocationID
                 , ProductID
                 , RetailLocationProductDescription
                 , Quantity
                 , Price
                 , SalePrice
                 , SaleStartDate
                 , SaleEndDate
            INTO #Deals
			FROM #Temp
			WHERE SalePrice IS NOT NULL OR SalePrice <> ''
			OR SaleStartDate IS NOT NULL OR SaleStartDate <> ''
			OR SaleEndDate IS NOT NULL OR SaleEndDate <> ''
			
			--Update the Deal Information if it already exists.
			UPDATE RetailLocationDeal SET SalePrice = D.SalePrice
										, SaleStartDate = D.SaleStartDate
										, SaleEndDate = D.SaleEndDate
										, RetailLocationProductDescription = D.RetailLocationProductDescription
										, DateUpdated = GETDATE()
			FROM #Deals D
			INNER JOIN RetailLocationDeal RLD ON RLD.RetailLocationID = D.RetailLocationID AND RLD.ProductID = D.ProductID
			
			--Insert a deal if it is a new one.
			INSERT INTO RetailLocationDeal(RetailLocationID
										 , ProductID
										 , RetailLocationProductDescription
										 , Price
										 , SalePrice
										 , SaleStartDate
										 , SaleEndDate
										 , DateCreated
										 , APIPartnerID)
						SELECT D.RetailLocationID
							 , D.ProductID
							 , D.RetailLocationProductDescription
							 , D.Price
							 , D.SalePrice
							 , D.SaleStartDate
							 , D.SaleEndDate
							 , GETDATE()
							 , (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName LIKE 'ScanSee')
						FROM #Deals D 
						LEFT OUTER JOIN RetailLocationDeal RLD ON D.ProductID = RLD.ProductID AND D.RetailLocationID = RLD.RetailLocationID
						WHERE RLD.ProductID IS NULL AND RLD.RetailLocationID IS NULL 
						
						
			--Get the discarded records.			
			SELECT UploadRetailLocationProductID
			     , StoreIdentification
			     , ScanCode
			     , RetailLocationProductDescription
				 , Quantity
				 , Price
				 , SalePrice
				 , SaleStartDate
				 , SaleEndDate
				 , FileName
				 , UserID
				 , RetailerID
				 , ReasonForDiscarding
            INTO #DiscardedRecords
            FROM 
            (SELECT UploadRetailLocationProductID
			     , StoreIdentification
			     , ScanCode
			     , RetailLocationProductDescription
				 , Quantity
				 , Price
				 , SalePrice
				 , SaleStartDate
				 , SaleEndDate
				 , FileName
				 , UserID
				 , RetailerID
				 , 'Duplicate Records.' ReasonForDiscarding
            FROM #DuplicateRecords
            
            UNION
            
            SELECT UploadRetailLocationProductID
			     , StoreIdentification
			     , ScanCode
			     , RetailLocationProductDescription
				 , Quantity
				 , Price
				 , SalePrice
				 , SaleStartDate
				 , SaleEndDate
				 , FileName
				 , UserID
				 , RetailerID
				 , 'Invalid Store or Product.' ReasonForDiscarding
            FROM UploadRetailLocationProduct
            WHERE UploadRetailLocationProductID IN(SELECT UploadRetailLocationProductID FROM #UniqueRetailLocationProducts)
            AND UploadRetailLocationProductID NOT IN(SELECT UploadRetailLocationProductID FROM #Temp)
            AND RetailerID = @RetailerID
            AND UserID = @UserID)DiscardedRecords	     
            
		    --Capture the total count before the discarded rows are inserted while moving from staging to production area.
		    --Formula: Total Records = (Total records in the Staging table) + (Count of records discarded while inserting into the staging area.)
            Select @TotalRows = Count(UploadRetailLocationProductID) From UploadRetailLocationProduct WHERE RetailerID = @RetailerID AND UserID = @UserID
            SELECT @TotalRows + (SELECT COUNT(1) FROM UploadRetailLocationDiscardedProducts WHERE UploadRetailLocationProductLogID = @UploadRetailLocationProductLogID)
     
                      
            --Move the Discarded rows into the Discardrd table.
            INSERT INTO UploadRetailLocationDiscardedProducts(UploadRetailLocationProductLogID															  
															, StoreIdentification
															, ProductName
															, RetailLocationProductDescription
															, Quantity
															, Price
															, SalePrice
															, SaleStartDate
															, SaleEndDate
															, [FileName]
															, UserID
															, RetailerID 
															, ReasonForDiscarding)
		    SELECT @UploadRetailLocationProductLogID
		         , StoreIdentification
			     , ScanCode
			     , RetailLocationProductDescription
				 , Quantity
				 , Price
				 , SalePrice
				 , SaleStartDate
				 , SaleEndDate
				 , FileName
				 , UserID
				 , RetailerID
				 , ReasonForDiscarding
		    FROM #DiscardedRecords
             
            --Capture all the counts.
            Select @FailureRows = Count(UploadRetailLocationDiscardedProductID) From UploadRetailLocationDiscardedProducts WHERE  RetailerID = @RetailerID  AND UserID = @UserID
            
            --Update Log details. 
            UPDATE UploadRetailLocationProductLog SET RetailLocationProductTotalRowsProcessed = @TotalRows 
													, RetailLocationProductSuccessfulRowCount = @SuccessRows
													, RetailLocationProductFailureRowCount = @FailureRows
													, RetailLocationProductFileName = @FileName
													--, ProcessStartDate = @startDate
													, ProcessEndDate = GETDATE()
			WHERE UploadRetailLocationProductLogID = @UploadRetailLocationProductLogID
													   		   
							
	--Confirmation of Success.
		SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadRetailerProducts.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			--DECLARE @ERRORMsg Varchar(2000)
			--SET @ERRORMsg=ERROR_MESSAGE()
			ROLLBACK TRANSACTION;
						
			--To capture Log info.
			INSERT INTO UploadRetailLocationProductLog (             
														  UserID
														, RetailID
														, RetailLocationProductFileName
														, RetailLocationProductTotalRowsProcessed
														, RetailLocationProductSuccessfulRowCount
														, RetailLocationProductFailureRowCount
														, Remarks
														, ProcessDate
														, ProcessStartDate
														, ProcessEndDate														
													   )
											Values (
														@UserID
													  , @RetailerID
													  , @FileName
													  , 0
													  , 0
													  , 0
													  , @ErrorMessage
													  , GETDATE()
													  , GETDATE()
													  , GETDATE()													  
													)
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
