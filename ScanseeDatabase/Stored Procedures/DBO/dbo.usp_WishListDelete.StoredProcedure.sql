USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListDelete]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListDelete
Purpose					: To delete a Wish List item of a user and moving it to the history.
Example					: usp_WishListDelete 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListDelete]
(
	  @UserProductID int
	, @UserID int
	, @WishListRemoveDate datetime
	
	--Output Variable
	, @Result bit output 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--UPDATE UserProduct 
			--SET WishListItem = 0
			--	, WishListRemoveDate = @WishListRemoveDate
			--WHERE UserProductID = @UserProductID 
			--	AND UserID = @UserID 
			
			--SELECT @Result = CASE WHEN COUNT(UserProductID) = 0 THEN 0 ELSE 1 END
			--FROM UserProduct 
			--WHERE UserID = @UserID 
			--AND WishListItem = 1
			
			--After swipe delete the wish list item in user product table must be updated to 0.
			
			UPDATE UserProduct 
			SET WishListItem = 0
				, WishListRemoveDate = @WishListRemoveDate
			WHERE UserProductID = @UserProductID 
					AND UserID = @UserID
					 
		    --After swipe delete moving the product from wish list to History

					INSERT INTO [UserProductHistory]
							   ([UserProductID]
							   ,[UserRetailPreferenceID]
							   ,[UserID]
							   ,[ProductID]
							   ,[MasterListItem]
							   ,[WishListItem]
							   ,[TodayListtItem]
							   ,[ShopCartItem]
							   ,[MasterListAddDate]
							   ,[WishListAddDate]
							   ,[TodayListAddDate]
							   ,[ShopCartAddDate]
							   ,[MasterListRemoveDate]
							   ,[WishListRemoveDate]
							   ,[TodayListRemoveDate]
							   ,[ShopCartRemoveDate])
							   
					SELECT  UserProductID
						   ,UserRetailPreferenceID
						   ,UserID
						   ,ProductID
						   ,MasterListItem
						   ,1
						   ,TodayListtItem
						   ,ShopCartItem
						   ,MasterListAddDate
						   ,@WishListRemoveDate
						   ,TodayListAddDate
						   ,ShopCartAddDate
						   ,MasterListRemoveDate
						   ,NULL
						   ,TodayListRemoveDate
						   ,ShopCartRemoveDate
					FROM UserProduct
					WHERE UserProductID=@userproductid AND UserID=@UserID
		

				SELECT @Result = CASE WHEN COUNT(UserProductID) = 0 THEN 0 ELSE 1 END
				FROM UserProduct 
				WHERE UserID = @UserID 
				AND WishListItem = 1
							
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListDelete.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
