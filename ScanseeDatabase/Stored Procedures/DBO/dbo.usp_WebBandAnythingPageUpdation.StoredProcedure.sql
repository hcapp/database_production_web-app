USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAnythingPageUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandAnythingPageUpdation
Purpose					: To Update the anything page.
Example					: usp_WebBandAnythingPageUpdation

History
Version		Date						Author				Change Description
------------------------------------------------------------------------------- 
1.0			18th Apr 2016				Sagar Byali			[usp_WebRetailerAnythingPageUpdation]
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandAnythingPageUpdation]
(

	--Input Parameter(s)--
	
     @PageID int	
 	--Common
   , @RetailID int	
   , @RetailLocationID varchar(MAX)
   , @AnythingPageTitle varchar(255)
   , @Image varchar(100) 
   
   --Link to Existing.
   , @WebLink varchar(1000)
   , @ImageIconID int
   
   --Make your Own
   , @PageDescription varchar(Max)
   , @PageShortDescription varchar(max)
   , @PageLongDescription varchar(max) 
   , @StartDate date
   , @EndDate date
   , @StartTime time
   , @EndTime time
   , @UploadFileName varchar(500)   
   	
	--Output Variable--	  	 
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
		
			DECLARE @ProductMediaTypeID int
			
			--Create the Anything Page & retrieve the page id for the QR code generation.
			
			UPDATE QRBandCustomPage SET Pagetitle = @AnythingPageTitle
										  , StartDate = CAST(@StartDate AS VARCHAR(10))+' '+CAST(@StartTime AS VARCHAR(12))
										  , EndDate = CAST(@EndDate AS VARCHAR(10))+' '+CAST(@EndTime AS VARCHAR(12))
										  , [Image] = @Image
										  , PageDescription = @PageDescription
										  , ShortDescription = @PageShortDescription
										  , LongDescription = @PageLongDescription
										  , QRBandCustomPageIconID = @ImageIconID
										  , URL = @WebLink
										  , DateUpdated = GETDATE()
						WHERE QRBandCustomPageID = @PageID
						
			--If the Band Locations are not null.						  
			IF (@RetailLocationID IS NOT NULL)
			BEGIN
				--Delete the Existing Association with Location.
				DELETE FROM QRBandCustomPageAssociation WHERE QRBandCustomPageID = @PageID
				
				--Create a fresh association with the Retail Locations.
				INSERT INTO QRBandCustomPageAssociation(QRBandCustomPageID
														  , BandID
														  , RetailLocationID
														  , DateCreated)
													SELECT @PageID
														 , @RetailID
														 , Param
														 , GETDATE()
													FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END
			
			--This will have a value only if the Create own page is selected.
			IF (@UploadFileName IS NOT NULL)
			BEGIN
			
			   --Delete the Existing Media Content.
			   DELETE FROM QRBandCustomPageMedia where QRBandCustomPageID = @PageID
			
			   --Create the fresh set of Media.
			   INSERT INTO QRBandCustomPageMedia(QRBandCustomPageID
													, MediaTypeID
													, MediaPath
													, DateCreated)
										  SELECT @PageID
										       ,(SELECT ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType LIKE 'Other Files')
										       , @UploadFileName
										       , GETDATE()														 
										
			END				  
									  
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandAnythingPageUpdation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
