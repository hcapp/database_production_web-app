USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierManageProductsAddMedia]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebSupplierManageProductsAddMedia  
Purpose     : To add more media files to the existing product.  
Example     : usp_WebSupplierManageProductsAddMedia  
  
History  
Version  Date       Author   Change Description  
-------------------------------------------------------------------------------   
1.0   30th Jan 2012    Pavan Sharma K Initial Version  
-------------------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebSupplierManageProductsAddMedia]  
(  
  
 --Input Parameter(s)--  
   
     @UserId int  
   , @ManufacturerID int  
   , @ProductID int  
   , @Images varchar(max)  
   , @Audio varchar(max)  
   , @Video varchar(max)      
       
 --Output Variable--  
     
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
     
   DECLARE @AudioTypeID int  
   DECLARE @VideoTypeID int  
   DECLARE @ImageTypeID int  
     
   SELECT @ImageTypeID = ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType = 'Image Files'  
   SELECT @AudioTypeID = ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType = 'Audio Files'  
   SELECT @VideoTypeID = ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType = 'Video Files'  
     
   IF (@Images IS NOT NULL)  
   BEGIN  
		IF EXISTS(SELECT ProductImagePath FROM Product WHERE ProductID = @ProductID)  
		BEGIN  
			INSERT INTO ProductMedia(ProductID  
					 , ProductMediaPath  
					 , ProductMediaTypeID             
					 , DateAdded)  
				SELECT @ProductID  
					, [Param]  
					, @ImageTypeID           
					, GETDATE()  
				FROM dbo.fn_SplitParam(@Images, ',')       
           
		END  
    ELSE  
    BEGIN  
		INSERT INTO ProductMedia(ProductID  
								, ProductMediaPath  
							    , ProductMediaTypeID              
								, DateAdded)  
							 SELECT @ProductID  
          , [Param]  
          , @ImageTypeID           
          , GETDATE()  
         FROM dbo.fn_SplitParam(@Images, ',')  
         
         EXCEPT 
          
         SELECT TOP 1 @ProductID  
          , [Param]  
          , @ImageTypeID          
          , GETDATE()  
         FROM dbo.fn_SplitParam(@Images, ',')   
    END  
   END  
     
   IF (@Audio IS NOT NULL)  
   BEGIN  
    INSERT INTO ProductMedia(ProductID  
                           , ProductMediaPath  
                           , ProductMediaTypeID                             
                           , DateAdded)  
                SELECT @ProductID  
                     , [Param]  
                     , @AudioTypeID                      
                     , GETDATE()  
                 FROM dbo.fn_SplitParam(@Audio, ',')  
   END  
     
   IF (@Video IS NOT NULL)  
   BEGIN  
    INSERT INTO ProductMedia(ProductID  
                           , ProductMediaPath  
                           , ProductMediaTypeID                           
                           , DateAdded)  
                SELECT @ProductID  
                     , [Param]  
                     , @VideoTypeID                     
                     , GETDATE()  
                 FROM dbo.fn_SplitParam(@Video, ',')  
   END  
     
     
  --Confirmation of Success.  
    
   SELECT @Status = 0  
  COMMIT TRANSACTION  
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebSupplierManageProductsAddMedia.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;


GO
