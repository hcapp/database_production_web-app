USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGalleryRedeemCoupon]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerGalleryRedeemCoupon
Purpose					: To add Coupon to User Gallery And making used flag as 1 when they tap on redeem button.
Example					: usp_WebConsumerGalleryRedeemCoupon 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd June 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerGalleryRedeemCoupon]
(
      @CouponID varchar(255)
	, @UserID int
	, @UserCouponClaimTypeID tinyint
	, @CouponPayoutMethod varchar(20)
	, @RetailLocationID int
	, @CouponClaimDate datetime
	
	--User Tracking
    , @CouponListID int	
 
	
	--Output Variable 
	, @UsedFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			
			
			
			
				IF  EXISTS (SELECT 1 FROM UserCouponGallery UC
				            INNER JOIN fn_SplitParam(@CouponID,',') P ON P.Param = UC.CouponID 
				            WHERE CouponID = P.Param AND UserID=@UserID AND UsedFlag=0)
				BEGIN
					UPDATE UserCouponGallery
					SET UsedFlag=1
					FROM UserCouponGallery UC
				    INNER JOIN fn_SplitParam(@CouponID,',') P ON P.Param = UC.CouponID 
				    WHERE CouponID = P.Param AND UserID=@UserID AND UsedFlag=0
					
				END
				
				INSERT INTO [UserCouponGallery]
					   ([CouponID]
					   ,[UserID]
					   ,[UserClaimTypeID]
					   ,[RetailLocationID]
					   ,[CouponClaimDate])
				SELECT P.Param 					    
						  ,@UserID
						  ,@UserCouponClaimTypeID
						  ,@RetailLocationID
						  ,@CouponClaimDate
				FROM fn_SplitParam(@CouponID,',') P	 
				WHERE P.Param NOT IN(SELECT CouponID FROM UserCouponGallery where UserID =@UserID) 				
					
				UPDATE UserCouponGallery
				SET UsedFlag=1
				FROM fn_SplitParam(@CouponID,',') P
				Where UserID=@UserID 
				AND CouponID=P.Param 		
			
			--User Tracking
			--Update Used Coupon in Reporting Database
			UPDATE ScanSeeReportingDatabase..CouponList SET CouponUsed =1
			WHERE CouponListID = @CouponListID 
			
			
			--Confirmation of Success.
					SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerGalleryRedeemCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
