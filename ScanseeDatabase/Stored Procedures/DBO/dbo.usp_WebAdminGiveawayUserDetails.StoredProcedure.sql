USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminGiveawayUserDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAdminGiveawayUserDetails
Purpose					: To Display GiveAwayPage Registered User. 
Example					: usp_WebAdminGiveawayUserDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6 May 2013	Span   	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminGiveawayUserDetails]
(
	  
	--Input Variable
	  @QRRetailerCustomPageID int
	, @LowerLimit int 
	    
	
	--Output Variable 
	, @RowCount INT OUTPUT    
	, @NextPageFlag bit output   
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 
		DECLARE @UpperLimit INT    
		DECLARE @MaxCnt INT    
		
		DECLARE @ScreenContent Varchar(100)    
		SELECT @ScreenContent = ScreenContent       
		FROM AppConfiguration       
		WHERE ScreenName = 'All'     
		AND ConfigurationType = 'Website Pagination'    
		AND Active = 1   
		 
		SET @UpperLimit = @LowerLimit + @ScreenContent  
		
		 
		--To Display GiveAwayPage Registered User. 
		SELECT RowNum = IDENTITY(int,1, 1)
		       , U.UserName
			   ,U.Email	
		INTO #Temp	   
		FROM UserGiveawayAssociation UG
		INNER JOIN Users U ON UG.UserID = U.UserID
		WHERE UG.QRRetailerCustomPageID = @QRRetailerCustomPageID
		 
		SELECT @MaxCnt = COUNT(RowNum) FROM #Temp
		
		--Output Total no of Records
		SET @RowCount = @MaxCnt 
		
		SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS 		 
		
		
		SELECT  UserName
			   ,Email	
		FROM #Temp
		WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit	
					
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebAdminGiveawayUserDetails>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
