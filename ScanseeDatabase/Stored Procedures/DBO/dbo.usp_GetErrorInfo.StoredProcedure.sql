USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetErrorInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GetErrorInfo
Purpose					: Retrieve error information
Example					: EXEC usp_GetErrorInfo 

History
Version		Date			Author			Change Description
---------------------------------------------------------------
1.0			12th May 2011	Padmapriya M	Initial Version
--------------------------------------------------------------- 
*/

CREATE PROCEDURE [dbo].[usp_GetErrorInfo]
AS
BEGIN

--To get Error Information
 SELECT ERROR_NUMBER() AS ErrorNumber,
		ERROR_MESSAGE() AS ErrorMessage
		
END

GO
