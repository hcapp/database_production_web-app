USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerDiscountPlanDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebRetailerDiscountPlanDisplay]
(

	--Input Input Parameter(s)--  	   
	  
	  @DiscountCode varchar(10)
	  
	--Output Variable--
	  
	
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		SELECT D.RetailerDiscountPlanID
		     , D.RetailerDiscountPlanCode
			 , D.DiscountPrice discount
			 , D.RetailerDiscountPlanDescription 
		FROM RetailerDiscountPlan D
		WHERE RetailerDiscountPlanCode = @DiscountCode	
	END TRY

		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDiscountPlanRetailerDisplay.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;




GO
