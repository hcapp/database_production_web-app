USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[TestPaging1]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TestPaging1]  
(  
  @PageNumber INT,  
  @PageSize INT  
)  
AS  
DECLARE @OffsetCount INT  
SET @OffsetCount = (@PageNumber-1)*@PageSize  
SELECT  *  
FROM [UserDetail]  
ORDER BY [User_Id]  
OFFSET @OffsetCount ROWS  
FETCH NEXT @PageSize ROWS ONLY  
GO
