USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEventsRefreshStagingTable]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchEventsRefreshStagingTable]
Purpose					: To refersh the Stage Table
Example					: [usp_BatchEventsRefreshStagingTable]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/17/2015       Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEventsRefreshStagingTable]
(
	
	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		TRUNCATE TABLE HcEventsStagingData

		--Confirmation of Status
		SELECT @Status = 0

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_BatchEventsRefreshStagingTable].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			--Confirmation of Failure
			SELECT @Status = 1	
		END;
		 
	END CATCH;
END;



GO
