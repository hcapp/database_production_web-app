USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Stored Procedure name : usp_WebRssFeedNewsDisplay
Purpose : 
Example : usp_WebRssFeedNewsDisplay

History
Version Date Author Change Description
--------------------------------------------------------------- 
1.0 13 Feb 2015 Mohith H R Initial Version
8th Aug 2016 Sagar Byali Modified for breaking news to  be displayed first in resultset
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedNewsDisplay]
( 
		 @NewsType varchar(500)
		, @HcHubCitiID int  
		--Output Variable 
		, @Status bit Output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY  

		 IF @NewsType='top'
		 BEGIN
				--to display all records
		
						SELECT distinct  RssFeedNewsID Id
										,NewsType
										,Title
										,convert(datetime,PublishedDate,105) PubStrtDate
										,ImagePath
										,ShortDescription shortDesc
										,LongDescription description
										,Link
										,PublishedDate
										,videoLink
							FROM RssFeedNews RFN
							WHERE NewsType IN ('breaking', 'top') AND HcHubCitiID = @HcHubCitiID AND RFN.Title IS NOT NULL
						    ORDER BY convert(datetime,PublishedDate,105) desc
			END
			ELSE
			BEGIN
	
						SELECT distinct  RssFeedNewsID Id
										,NewsType
										,Title
										,ImagePath
										,ShortDescription shortDesc
										,LongDescription description
										,Link
										,CASE WHEN LEN(PublishedDate)>15 THEN SUBSTRING(PublishedDate,1,7) + ' , ' +SUBSTRING(PublishedDate,8,4) ELSE PublishedDate END PublishedDate
										,convert(datetime,PublishedDate,105) PubStrtDate
										,Classification
										,Section
										,AdCopy
										,VideoLink videoLink
						 FROM RssFeedNews RFN
						 WHERE HcHubCitiID = @HcHubCitiID  AND (NewsType = @NewsType OR @NewsType IS NULL)
						 AND ((RFN.Title IS NOT NULL AND @NewsType <> 'Classifields')OR (RFN.Title IS NULL AND @NewsType = 'Classifields'))
						 ORDER BY convert(datetime,PublishedDate,105) desc
 
				END 
  
	 SET @Status=0  

	END TRY
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		PRINT 'Error occured in Stored Procedure usp_WebRssFeedNewsDisplay.' 
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		SET @Status=1 
		END;
 
	END CATCH;
END;









GO
