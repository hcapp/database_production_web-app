USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFetchDiscardedRetailLocationProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerFetchDiscardedRetailLocations
Purpose					: To Fetch all the discarded records that have not yet been notified..
Example					: usp_WebRetailerFetchDiscardedRetailLocations

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			25th July 2012   				  Pavan Sharma K	     Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerFetchDiscardedRetailLocationProducts]               
(

	--Input Parameter(s)--
	 
	  @RetailerID int
    , @UserID int
    , @UploadRetailLocationProductLogID int     
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
			  --Fetch all the discarded records that have not yet been notified.
			  SELECT  StoreIdentification storeIdentificationID
					, ProductName scanCode
					, RetailLocationProductDescription longDescription
					, Quantity
					, Price productPrice
					, SalePrice productSalePrice
					, SaleStartDate
					, SaleEndDate
					, [FileName]
					, URL.UserID
					, RetailerID 
					, ReasonForDiscarding errorMessage
			 FROM UploadRetailLocationDiscardedProducts URLP
			 INNER JOIN UploadRetailLocationProductLog URL ON URL.UploadRetailLocationProductLogID = URLP.UploadRetailLocationProductLogID
			 WHERE URL.UploadRetailLocationProductLogID = @UploadRetailLocationProductLogID
			 AND URL.EmailNotificationFlag = 0
			 
			 --Update the Log once Email is sent.
			 UPDATE UploadRetailLocationProductLog
			 SET EmailNotificationFlag = 1
			 WHERE UploadRetailLocationProductLogID = @UploadRetailLocationProductLogID
		    
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealAdd.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
