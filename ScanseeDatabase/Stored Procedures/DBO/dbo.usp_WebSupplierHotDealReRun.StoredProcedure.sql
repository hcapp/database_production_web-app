USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierHotDealReRun]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierHotDealReRun
Purpose					: To Re Run Hot Deal by the Supplier.
Example					: usp_WebSupplierHotDealReRun

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			6th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierHotDealReRun]               
(

	--Input Parameter(s)--
	 
	 --Inputs for ProductHotDeal Table
	  @HotDealName  varchar(255)
    , @RegularPrice money
    , @SalePrice money
    , @HotDealDescription  varchar(1000)
    , @HotDealLongDescription  varchar(3000)
    , @HotDealTerms  varchar(1000)
    , @URL  varchar(1000)
    , @DealStartDate date
    , @DealStartTime time
    , @DealEndDate date
    , @DealEndTime time   
    , @ManufacturerID int    
    , @HotDealTimeZoneID int 
    , @CategoryID int   
    
    --Product HotDealLocation Inputs.
   --Product HotDealLocation Inputs.
    --, @City varchar(max) 
    --, @State varchar(max)			Commented because the Prototype is changed to take the Population Centre ID as input.
    
    , @PopulationCentreID varchar(max)
    
    --Product HotDealRetailLocation Inputs.
    , @RetailID int
    , @RetailLocationID int
     
     --Inputs for HotDealProduct Table   
  
    , @ProductID varchar(max)
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
		
		DECLARE @ProductHotDealID INT
				
		   -- Cerate a record in Product Hot Deal Table.   
		   INSERT INTO ProductHotDeal(
									   HotDealName
									 , Price
									 , SalePrice
									 , HotDealShortDescription
									 , HotDeaLonglDescription
									 , HotDealTermsConditions
									 , HotDealURL
									 , HotDealStartDate
									 , HotDealEndDate  
									 , CreatedDate
									 , ManufacturerID
									 , HotDealTimeZoneID
									 , CategoryID
								  )	
							VALUES(
										@HotDealName
									  , @RegularPrice
									  , @SalePrice
									  , @HotDealDescription
									  , @HotDealLongDescription
									  , @HotDealTerms
									  , @URL
									  , CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'') AS DATETIME)
									  , CAST(@DealEndDate AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME)
									  , GETDATE()	
									  , @ManufacturerID
									  , @HotDealTimeZoneID
									  , @CategoryID
								  )
				
          --Capture the inserted hot deal ID.				
          SET @ProductHotDealID = SCOPE_IDENTITY()       
						
          			     
            
          --If the Hot Deal is associated with the City.  								  
          IF (@PopulationCentreID IS NOT NULL)
          BEGIN
          INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]												 
											  )
										SELECT @ProductHotDealID
										     , City
										     , [State]
										FROM dbo.fn_SplitParam(@PopulationCentreID, ',') P
										INNER JOIN PopulationCenterCities PC ON P.Param = PC.PopulationCenterID
			
		  END						     
		  IF(@ProductID IS NOT NULL)
		  BEGIN
		  INSERT INTO HotDealProduct(
										ProductHotDealID
									  , ProductID
									)
						SELECT @ProductHotDealID
						     , [Param]
						FROM dbo.fn_SplitParam(@ProductID, ',') 
          END
          
          --Below code works when state is multi select and City is in the "State-City" format
			--INSERT INTO ProductHotDealLocation (
			--										ProductHotDealID												 
			--									  , City
			--									  , [State]												 
			--									)
			--				SELECT @ProductHotDealID
			--				     , SUBSTRING([Param], CHARINDEX('-',[Param]) + 1, LEN([Param])-CHARINDEX('-',[Param]))  --Compute the State and City form th State-City Combination.
			--				     , SUBSTRING([Param], 1, CHARINDEX('-',[Param])-1)							    
			--				FROM dbo.fn_SplitParam(@City, ',')  							
			      							
          							     
          	
          
          --If Hot Deal is associated with the Retail Location.
          IF (@RetailID IS NOT NULL AND @RetailLocationID IS NOT NULL)
          BEGIN
			
				INSERT INTO ProductHotDealRetailLocation(
															ProductHotDealID
														  , RetailLocationID
														)
											SELECT @ProductHotDealID
											     , [Param]
											FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			   INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]												 
											  )
										SELECT @ProductHotDealID
										     , City
										     , [State]
										FROM dbo.fn_SplitParam(@PopulationCentreID, ',') P
										INNER JOIN PopulationCenterCities PC ON P.Param = PC.PopulationCenterID		
												
		  END
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierHotDealReRun.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
