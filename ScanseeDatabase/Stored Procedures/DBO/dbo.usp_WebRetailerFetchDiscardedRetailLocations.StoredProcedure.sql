USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFetchDiscardedRetailLocations]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerFetchDiscardedRetailLocations
Purpose					: To get the discarded RetailLocations and notify the User through mail.
Example					: usp_WebRetailerFetchDiscardedRetailLocations

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			7th May 2012				 Pavan Sharma K	     Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerFetchDiscardedRetailLocations]               
(

	--Input Parameter(s)--
	 
	  @RetailerID int
    , @UserID int
   
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
			
			  SELECT UploadRetaillocationsDiscardedID uploadRetaillocationsID
					,StoreIdentification
					,RetailID
					,Headquarters
					,Address1
					,Address2
					,Address3
					,Address4
					,City
					,State
					,PostalCode
					,CountryID
					,RetailLocationLatitude
					,RetailLocationLongitude
					,RetailLocationTimeZone
					,RetailLocationURL
					,PhoneNumber
					,DateCreated
					,DateModified
					,RetailLocationStoreHours
					,URL.RetaillocationFileName
					,URLD.ReasonForDiscarding
					,URLD.RetailKeyword keyword
					,URLD.RetailLocationImagePath GridImgLocationPath														
			FROM UploadRetaillocationsDiscarded URLD
			INNER JOIN UploadRetaillocationsLog URL ON URLD.UploadRetaillocationsLogID = URL.UploadRetaillocationsLogID
			WHERE URLD.RetailID = @RetailerID AND UserID = @UserID
			AND	 EmailNotificationFlag = 0
		    
		    UPDATE UploadRetaillocationsLog
		    SET EmailNotificationFlag = 1
		    FROM UploadRetaillocationsLog URL
		    INNER JOIN UploadRetaillocationsDiscarded URLD ON URLD.UploadRetaillocationsLogID = URL.UploadRetaillocationsLogID
		    WHERE URLD.RetailID = @RetailerID
		    AND UserID = @UserID
		    
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealAdd.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
