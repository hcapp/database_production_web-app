USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAdminRetrieveBandBusinessCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebBandAdminRetrieveBandBusinessCategory
Purpose					: To retrieve list of Band's business categories.
Example					: usp_WebBandAdminRetrieveBandBusinessCategory

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/04/2016			Prakash C		Initial Version
-------------------------------------------------------------------------------
*/


--EXEC [usp_WebBandAdminRetrieveBandBusinessCategory] 1,NULL,NULL,NULL
CREATE PROCEDURE [dbo].[usp_WebBandAdminRetrieveBandBusinessCategory]

	--Input parameters	 
	  @RetailID INT	

	--Output variables 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
	
		


	SELECT DISTINCT A.BandCategoryID categoryId
	, BandCategoryName categoryName
	--, Associated = A.BandCategoryID 
	, filterAssociated = 0
	, subCatAssociated = CASE WHEN F.BandSubCategoryID IS NOT NULL THEN 1 ELSE 0 END
	, Checked = CASE WHEN F.bandid IS NOT NULL THEN 1 ELSE 0 END
	FROM bandcategory A		 
	LEFT JOIN BandCategoryAssociation F ON A.BandCategoryID = F.BandCategoryID AND F.bandid = @RetailID
	--LEFT JOIN BandSubCategoryAssociation B ON B.BandCategoryID = F.BandCategoryID AND F.bandid = 1
	ORDER BY BandCategoryName


	SELECT DISTINCT A.BandSubCategoryID categoryId
	, A.BandSubCategoryName categoryName
	, associated = B.BandCategoryID 
	, Checked = CASE WHEN c.bandid IS NOT NULL THEN 1 ELSE 0 END
	FROM BANDSubCategory A
	LEFT JOIN BandSubCategoryType B ON A.BandSubCategoryTypeID = B.BandSubCategoryTypeID 
	LEFT JOIN BandCategoryAssociation C ON C.BandSubCategoryID = A.BandSubCategoryID AND C.BandID = @RetailID
	ORDER BY A.BandSubCategoryName



		
		--Confirmation of failure.
		SELECT @Status = 0

	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandAdminRetrieveBandBusinessCategory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;



GO
