USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SearchProduct
Purpose					: To get Searched product information
Example					: EXEC usp_SearchProduct 35, '38000391200',  '12347890', -73.99653, 40.750742, 0, '7/22/2011'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			31st May 2011	SPAN Infotech India	Initial Version 
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SearchProduct]
(
	@UserId int
	,@ProdSearch varchar(max)
	
	----Scan History and ProductUserHit Variables
 --   --, @RetailLocationID int  
	----, @ProductID int  
	----, @RebateID int  
	--, @DeviceID varchar(60)  
	--, @ScanLongitude float  
	--, @ScanLatitude float  
	--, @FieldAgentRequest bit  
	--, @Date datetime 
	--, @ScanTypeID tinyint  
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY	
		DECLARE @ProductID int
		DECLARE @RebateID int 
		DECLARE @ScanTypeID tinyint  
	--To get product details
		SELECT ProductID 
			  , ProductName
			  , M.ManufName   
			  , ModelNumber 
			  , ProductLongDescription 
			  , ProductExpirationDate 
			  , ProductImagePath 
			  , SuggestedRetailPrice 
			  , Weight 
			  , WeightUnits 
		FROM Product P
			INNER JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID
		WHERE ( ScanCode LIKE @ProdSearch + '%'
				OR 
			    ProductName LIKE '%' + @ProdSearch + '%'
			  )
			AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())
			AND P.ProductID <> 0 
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SearchProduct.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
