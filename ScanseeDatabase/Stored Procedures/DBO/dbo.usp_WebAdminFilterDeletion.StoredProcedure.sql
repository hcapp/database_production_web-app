USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterDeletion]
Purpose                  : To delete Filter.
Example                  : [usp_WebAdminFilterDeletion]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterDeletion]
(

      --Input Input Parameter(s)--     
	    @FilterID Int	 
      
      
      --Output Variable--	  
	  , @RetailerAssociatedFlag BIT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY             
			
	         -- To delete Filter with association table.
			 IF EXISTS(SELECT 1 FROM RetailerFilterAssociation WHERE AdminFilterID =@FilterID)
			 BEGIN
				SET @RetailerAssociatedFlag=1
			 END

			 ELSE
			 BEGIN
			     SET @RetailerAssociatedFlag=0

				 DELETE FROM AdminFilterCategoryAssociation 
				 WHERE AdminFilterID =@FilterID

				 SELECT AdminFilterValueID 
				 INTO #Temp
				 FROM AdminFilterValueAssociation 
				 WHERE AdminFilterID =@FilterID 

				 DELETE FROM AdminFilterValueAssociation 
				 WHERE AdminFilterID =@FilterID 

				 DELETE FROM AdminFilterValue 
				 FROM AdminFilterValue AF
				 INNER JOIN #Temp T ON T.AdminFilterValueID =AF.AdminFilterValueID 		 
			 
				 DELETE FROM AdminFilter 
				 WHERE AdminFilterID =@FilterID 
			  END 

        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterDeletion.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;





GO
