USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminUserLoginCreationandUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name      : [usp_WebAdminUserLoginCreationandUpdation]
Purpose                    : To Create User Login.
Example                    : [usp_WebAdminUserLoginCreationandUpdation]

History
Version              Date                 Author   Change Description
--------------------------------------------------------------- 
1.0                  29/07/2014        Dhananjaya TR            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminUserLoginCreationandUpdation]
(
    --Input variable
         @RetailID int
       , @Username Varchar(1000) 
       , @Password varchar(1000)
       , @EmailID Varchar(1000)
       , @LoginExist Bit
      -- , @AdminUserID int
                
       --Output Variable
       , @DuplicateEmail bit output   
       , @DuplicateUserName bit output
       , @RetailName Varchar(500) output
       , @Status int output
       , @ErrorNumber int output
       , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

       BEGIN TRY
                     
                     BEGIN TRANSACTION
                     
                     DECLARE @UserID INT
                     DECLARE @UserType INT
                     DECLARE @ContactID INT
                     DECLARE @UserExist INT
                     DECLARE @OldUsername Varchar(500)
                     DECLARE @OldEmail Varchar(500)


					-- Check for login exist, if so capture UserID.
                     SELECT @UserExist=UserID 
                     FROM UserRetailer 
                     WHERE RetailID =@RetailID AND @LoginExist =1 


					 --Capture username who having login.
                     SELECT @OldUsername =UserName                   
                     FROM Users 
                     WHERE UserID =@UserExist

					 --Capture retailer name.
                     SELECT @RetailName=RetailName
                     FROM Retailer 
                     WHERE RetailID=@RetailID AND RetailerActive = 1 

					 --DECLARE @Address1 varchar(1000)
					 --DECLARE @City varchar(500)
					 --DECLARE @State varchar(50)
					 --DECLARE @PostalCode varchar(100)

					 --SELECT @Address1 = Address1
						--   ,@City = City
						--   ,@State = @State
						--   ,@PostalCode = @PostalCode
					 --FROM Retailer
					 --WHERE RetailID = @RetailID

                     IF EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName AND (@UserExist IS NULL OR (@UserExist IS NOT NULL AND @UserExist<> UserID))) 
                     BEGIN
                           
                                  SET @DuplicateUserName = 1
                     END
                     ----Check if the EmailID duplication.
                     --ELSE IF EXISTS(SELECT 1 FROM Users WHERE Email = @EmailID AND (@UserExist IS NULL OR (@UserExist IS NOT NULL AND @UserExist<> UserID)))
                     --BEGIN                       
                     --            SET @DuplicateEmail = 1
                     --END  
                     
                     ELSE
                     BEGIN    
                                         
            --                                    IF NOT EXISTS(SELECT 1 FROM RetailLocation WHERE RetailID =@RetailID AND ((TRY_CONVERT(BIT, Headquarters)>0) OR (TRY_CONVERT(BIT, CorporateAndStore)>0) ))
												----IF NOT EXISTS(SELECT 1 FROM RetailLocation WHERE RetailID =@RetailID and Address1 =@Address1 and city=@City and state=@State and PostalCode=@PostalCode)
            --                                    BEGIN
            --                                                         INSERT INTO RetailLocation(RetailID
            --                                                                                                         ,Headquarters
            --                                                                                                         ,Address1
            --                                                                                                         ,City
            --                                                                                                         ,State
            --                                                                                                         ,PostalCode
            --                                                                                                         ,CountryID
            --                                                                                                         ,DateCreated
            --                                                                                                         ,StoreIdentification
            --                                                                                                         ,CorporateAndStore
            --                                                                                                         --,CreatedUserID
            --                                                                                                         )
            --                                                         SELECT RetailID 
            --                                                                  ,0
            --                                                                  ,Address1 
            --                                                                  ,City 
            --                                                                  ,State 
            --                                                                  ,PostalCode
            --                                                                  ,1
            --                                                                  ,GETDATE()
            --                                                                  ,'9999'
            --                                                                  ,1
            --                                                                 -- ,@AdminUserID
            --                                                         FROM Retailer 
            --                                                         WHERE RetailID =@RetailID 
            --                                    END 
			
			
			--To check if login exists for a respective retailer.
			DECLARE @ExistLogin Bit

			SELECT @ExistLogin = 1
			FROM UserRetailer 
			WHERE RetailID = @RetailID
												
												
			--For Retailers imported from excel sheet, Set CorporateAndStore as '1'
			IF ((SELECT COUNT(DISTINCT RetailLocationID) FROM RetailLocation WHERE Active = 1 AND RetailID = @RetailID) = 1)
			BEGIN   
			                                     
					UPDATE TOP (1) RetailLocation SET CorporateAndStore = 1
					WHERE RetailID = @RetailID AND Headquarters = 0 
			END
												
			--For Retailers having single location:- Set CorporateAndStore as '1' and reset Headquarters to '0' 
			IF ((SELECT COUNT(DISTINCT RetailLocationID) FROM RetailLocation WHERE RetailID =@RetailID AND Active = 1 AND @ExistLogin = 1) =1)
			BEGIN 
												       
					IF EXISTS (SELECT Headquarters = 1 FROM RetailLocation WHERE RetailID = @RetailID)
					BEGIN
					UPDATE RetailLocation 
					SET Headquarters = 0 , CorporateAndStore = 1 
					WHERE RetailID = @RetailID AND Headquarters = 1 AND Active = 1
					END
													  
			END	 
			

			--IF EXISTS(SELECT 1 FROM RetailLocation WHERE RetailID =@RetailID AND (Headquarters <> 1 AND CorporateAndStore <> 1)) 
			--BEGIN                                  
			--	 UPDATE TOP (1) RetailLocation SET CorporateAndStore = 1
			--	 WHERE RetailID = @RetailID 
			--END
			--ELSE IF (SELECT COUNT(1) FROM RetailLocation WHERE RetailID =@RetailID AND (Headquarters=1 AND CorporateAndStore=0))=1
			--BEGIN
			--	 UPDATE TOP (1) RetailLocation SET CorporateAndStore = 1,Headquarters = 0
			--	 WHERE RetailID = @RetailID 												
			--END
											
            IF NOT EXISTS(SELECT 1 FROM UserRetailer WHERE RetailID =@RetailID)
            BEGIN                          
                            --Creating New User
                            INSERT INTO users(CountryID
                                                        ,Password
                                                        ,DateCreated
                                                        ,UserName
														,Email
                                                    -- ,CreatedUserID
														)


                                    SELECT 1
                                            ,@Password 
                                            ,GETDATE()
                                            ,@Username 
											,@EmailID
                                        --  ,@AdminUserID
                                  
                            SET @UserID=SCOPE_IDENTITY()

                            INSERT INTO UserRetailer(RetailID
                                                                    ,UserID) 
                            SELECT @RetailID 
                                        ,@UserID 
            END    
                                         
            ELSE IF EXISTS(SELECT 1 FROM UserRetailer WHERE RetailID =@RetailID)
            BEGIN
			--IF @OldUsername <>@Username
				--BEGIN
					UPDATE Users SET ResetPassword = 0
											,DateModified = GETDATE()
										-- ,ModifiedUserID = @AdminUserID
					WHERE UserID =@UserExist
				--END

					UPDATE Users SET UserName =@Username 
												,Password =@Password 
												,DateModified = GETDATE()
												,Email = @EmailID
										-- ,ModifiedUserID = @AdminUserID
					WHERE UserID =@UserExist 

					UPDATE C SET ContactEmail = @EmailID
					FROM Contact C
					INNER JOIN RetailContact RC ON C.ContactID = RC.ContactID
					INNER JOIN RetailLocation RL ON RC.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
					WHERE RetailID = @RetailID
					AND (CorporateAndStore='1' OR Headquarters ='1')
            END           

            IF EXISTS(SELECT 1 FROM RetailContact RC
                                    INNER JOIN RetailLocation RL ON RC.RetailLocationID =RL.RetailLocationID AND RetailID =@RetailID AND RL.Active = 1 
                                                                                    AND (CorporateAndStore='1' OR Headquarters ='1'))
            BEGIN

            SELECT @OldEmail  =C.ContactEmail  
            FROM CONTACT C 
            INNER JOIN RetailContact RC ON RC.ContactID =C.ContactID 
            INNER JOIN RetailLocation RL ON RL.RetailLocationID =RC.RetailLocationID AND RetailID =@RetailID AND RL.Active = 1
            AND (CorporateAndStore='1' OR Headquarters ='1')

            --IF @OldEmail <>@EmailID
            --BEGIN
                        UPDATE Users SET  ResetPassword =0
                                                    ,DateModified = GETDATE()
                                                    --,ModifiedUserID = @AdminUserID
                        WHERE UserID =@UserExist
            --END

            UPDATE Contact Set ContactEmail =@EmailID
                                            ,DateModified = GETDATE()
                                        --  ,ModifyUserID = @AdminUserID 
            FROM CONTACT C 
            INNER JOIN RetailContact RC ON RC.ContactID =C.ContactID 
            INNER JOIN RetailLocation RL ON RL.RetailLocationID =RC.RetailLocationID AND RetailID =@RetailID AND RL.Active = 1
            AND (CorporateAndStore='1' OR Headquarters ='1')
            END


            IF NOT EXISTS(SELECT 1 FROM RetailContact RC
                                    INNER JOIN RetailLocation RL ON RC.RetailLocationID =RL.RetailLocationID AND RetailID =@RetailID AND RL.Active = 1 
                                                                                    AND (CorporateAndStore='1' OR Headquarters ='1'))
            BEGIN
			INSERT INTO Contact(ContactPhone
										,ContactEmail
										,DateCreated
										,CreateUserID
										)
			SELECT CorporatePhoneNo 
					,@EmailID 
					,GETDATE()
					,ISNULL(@UserID,@UserExist) --@AdminUserID
			FROM Retailer 
			WHERE RetailID =@RetailID AND RetailerActive = 1

			SET @ContactID=SCOPE_IDENTITY()

			INSERT INTO RetailContact(ContactID
													,RetailLocationID
													,ContactEmail)
			SELECT @ContactID 
					,RetailLocationID
					,@EmailID                                
			FROM RetailLocation 
			WHERE RetailID =@RetailID AND (CorporateAndStore='1' OR Headquarters ='1') AND Active = 1
            END    
                                                
            UPDATE Users SET ResetPassword = 0
                                                    ,DateModified = GETDATE()
                                                -- ,ModifiedUserID = @AdminUserID
            WHERE UserID =@UserExist         

            END
                     
			--Confirmation of Success
			SELECT @Status = 0
                     
            COMMIT TRANSACTION
       
       END TRY
              
       BEGIN CATCH
         
              --Check whether the Transaction is uncommitable.
              IF @@ERROR <> 0
              BEGIN         
                  SELECT ERROR_LINE() 
                     PRINT 'Error occured in Stored Procedure [usp_WebAdminUserLoginCreationandUpdation].'              
                     --- Execute retrieval of Error info.
                     EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output                     
                     PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
                     ROLLBACK TRANSACTION;
                     --Confirmation of failure.
                     SELECT @Status = 1
              END;
              
       END CATCH;
END;











GO
