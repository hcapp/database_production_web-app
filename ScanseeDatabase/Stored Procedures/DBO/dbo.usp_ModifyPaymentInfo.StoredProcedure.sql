USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ModifyPaymentInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ModifyPaymentInfo
Purpose					: To store User payment info to UserPayInfo table.
Example					:  
DECLARE @return_value int
EXEC usp_ModifyPaymentInfo 'John@email.com',3,1,0.0,1,1, '#28', '', 'Newyork', 'sc', '500009', '5/19/2011', null, 1, null, null, null, @Result = @return_value OUTPUT
SELECT @return_value

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th May 2011	SPAN Infotech India	Initial Version 
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ModifyPaymentInfo]
(
	 @UserName varchar(100)
	,@PaymentIntervalID int
	,@PaymentTypeID int
	,@DefaultPayout money
	,@PayoutTypeID tinyint
	,@UseDefaultAddress bit
	,@PayAddress1 varchar(30)
	,@PayAddress2 varchar(30)
	,@PayCity varchar(30)
	,@PayState char(2)
	,@PayPostalCode varchar(10)
	,@DateActivated datetime
	,@DateModified datetime
	,@ProvisionalPayoutActive bit
	,@GroupID int
	,@ProvisionalPayoutStartDate date
	,@ProvisionalPayoutEndDate date,

	--Output variable
	@Result int OUTPUT
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @UserID int
			SELECT @UserID = UserID FROM [Users] WHERE Email = @UserName 
			IF EXISTS (SELECT 1 FROM UserPayInfo WHERE UserID = @UserID)
			--If info exists, modify the info.
			BEGIN
				UPDATE UserPayInfo
				SET PaymentIntervalID = @PaymentIntervalID
					,PaymentTypeID = @PaymentTypeID
					,DefaultPayout = @DefaultPayout
					,PayoutTypeID = @PayoutTypeID
					,UseDefaultAddress = @UseDefaultAddress
					,PayAddress1 = @PayAddress1
					,PayAddress2 = @PayAddress2
					,PayCity = @PayCity
					,PayState = @PayState
					,PayPostalCode = @PayPostalCode
					,DateActivated = @DateActivated
					,DateModified = @DateModified
					,ProvisionalPayoutActive = @ProvisionalPayoutActive
					,GroupID = @GroupID
					,ProvisionalPayoutStartDate = @ProvisionalPayoutStartDate
					,ProvisionalPayoutEndDate = @ProvisionalPayoutEndDate
				WHERE UserID = @UserID
			END
			ELSE
			--If info not exists, add the info.
			BEGIN
				INSERT INTO UserPayInfo 
					(UserID
					,PaymentIntervalID
					,PaymentTypeID
					,DefaultPayout
					,PayoutTypeID
					,UseDefaultAddress
					,PayAddress1
					,PayAddress2
					,PayCity
					,PayState
					,PayPostalCode
					,DateActivated
					,DateModified
					,ProvisionalPayoutActive
					,GroupID
					,ProvisionalPayoutStartDate
					,ProvisionalPayoutEndDate)
				VALUES
					(@UserID
					,@PaymentIntervalID
					,@PaymentTypeID
					,@DefaultPayout
					,@PayoutTypeID
					,@UseDefaultAddress
					,@PayAddress1
					,@PayAddress2
					,@PayCity
					,@PayState
					,@PayPostalCode
					,@DateActivated
					,@DateModified
					,@ProvisionalPayoutActive
					,@GroupID
					,@ProvisionalPayoutStartDate
					,@ProvisionalPayoutEndDate)
			END
			
		SELECT @Result = 0 --To indicate successfull execution.
		
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ModifyPaymentInfo.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction.'
			SELECT @Result = 1 --To indicate unsuccessfull execution.
			ROLLBACK TRANSACTION;
		END;
		 
	END CATCH;
END;

GO
