USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryCouponsDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_GalleryCouponDetails]
Purpose					: To display details of coupon related to the user.
Example					: [usp_GalleryCoupons]

History
Version		Date			     Author			Change Description
--------------------------------------------------------------- 
1.0			15th Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryCouponsDetails]
(
	  @Userid int
	, @couponid int
	--Output Variable 
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	   
			SELECT	ISNULL(ProductName,'') productName
					,ISNULL(CP.ProductID,0)  ProductID
					,C.CouponID
					,CouponName
					,CouponLongDescription
					,CouponStartDate
					,CouponExpireDate
					,CouponURL
					,CouponTermsAndConditions termsAndConditions 
					--,UsedFlag=(SELECT CASE WHEN C.CouponID=UC.CouponID THEN 1 ELSE 0 END FROM UserCouponGallery UC WHERE UserID=@Userid AND CouponID=@couponid) 
			FROM Coupon C
			    LEFT JOIN CouponProduct CP ON C.CouponID=CP.CouponID
				LEFT JOIN Product P ON P.ProductID=CP.ProductID
			WHERE  C.CouponID=@couponid 
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_GalleryCoupons].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
