USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ModifyUserRetailerPreference]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ModifyUserRetailerPreference
Purpose					: To store User preferred retailer info to UserRetailPreference table.
Example					:  
DECLARE @return_value int
EXEC usp_ModifyUserRetailerPreference 'John@email.com', '1,2,3', 1, 1, 1, '5/19/2011', @Result = @return_value OUTPUT
SELECT @return_value

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th May 2011	SPAN Infotech India	Initial Version 
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ModifyUserRetailerPreference]
(
	@UserName varchar(100),
	@RetailID varchar(max),
	@Active bit,
	@LoyaltyProgram bit,
	@RetailFavorite bit,
	@DateAdded datetime,

	--Output variable
	@Result int OUTPUT
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To get userid from Username
			DECLARE @UserID int
			SELECT @UserID = UserID FROM [Users] WHERE Email = @UserName 
			--To split retailids.
			SELECT ID = IDENTITY(INT, 1, 1)
					,@UserID AS UserID
					,PARAM AS RetailID
					,@Active AS Active
					,@LoyaltyProgram AS LoyaltyProgram
					,@RetailFavorite AS RetailFavorite
					,@DateAdded AS DateAdded
			INTO #UserRetailPreference
			FROM dbo.fn_SplitParam(@RetailID, ',')
			
			DECLARE @Init int = 1-- Initialization
			DECLARE @Cnt int --Counter
			SELECT @Cnt = COUNT(1) FROM #UserRetailPreference
			
			WHILE @Init <= @Cnt 
			BEGIN 
				DECLARE @UserID_Temp int,
						@RetailID_Temp int,
						@Active_Temp bit,
						@LoyaltyProgram_Temp bit,
						@RetailFavorite_Temp bit,
						@DateAdded_Temp datetime
						
				SELECT @UserID_Temp = UserID,
						@RetailID_Temp = RetailID,
						@Active_Temp = Active,
						@LoyaltyProgram_Temp = LoyaltyProgram,
						@RetailFavorite_Temp = RetailFavorite,
						@DateAdded_Temp = DateAdded
				FROM #UserRetailPreference 
				WHERE ID = @Init
				
				IF EXISTS (SELECT 1 FROM UserRetailPreference WHERE UserID = @UserID_Temp AND RetailID = @RetailID_Temp)
				--If info exists, modify the info.
				BEGIN
					UPDATE UserRetailPreference
					SET UserID = @UserID_Temp
						,RetailID = @RetailID_Temp 
						,Active= @Active_Temp
						,LoyaltyProgram = @LoyaltyProgram_Temp
						,RetailFavorite = @RetailFavorite_Temp
						,DateAdded = @DateAdded_Temp
					WHERE UserID = @UserID_Temp 
						AND RetailID = @RetailID_Temp 
				END
				ELSE
				--If info not exists, add the info.
				BEGIN
					INSERT INTO UserRetailPreference 
						(UserID
						,RetailID
						,Active
						,LoyaltyProgram
						,RetailFavorite
						,DateAdded)
					VALUES
						(@UserID_Temp
						,@RetailID_Temp
						,@Active_Temp
						,@LoyaltyProgram_Temp
						,@RetailFavorite_Temp
						,@DateAdded_Temp)
				END 
			
			SET @Init = @Init + 1 --Incrementing
			END
			
			
			
		SELECT @Result = 0 --To indicate successfull execution.
		
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ModifyUserRetailerPreference.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction.'
			SELECT @Result = 1 --To indicate unsuccessfull execution.
			ROLLBACK TRANSACTION;
		END;
		 
	END CATCH;
END;

GO
