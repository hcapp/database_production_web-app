USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_RebateShare]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RebateShare
Purpose					: 
Example					: usp_RebateShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Jan 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RebateShare]
(
	  @RebateID int
	
	--Output Variable 
	, @RebateExpired bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
		
			
			SELECT RebateName rebateName
				   ,NULL rebateURL 
				   ,RebateStartDate rebateStartDate
				   ,RebateEndDate rebateEndDate
			FROM Rebate 
			WHERE GETDATE() Between RebateStartDate AND RebateEndDate AND RebateID=@RebateID
			
			SELECT @RebateExpired=CASE WHEN GETDATE()>RebateEndDate Then 0 else 1 END 
			FROM Rebate
			WHERE RebateID=@RebateID
		
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_RebateShare.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
