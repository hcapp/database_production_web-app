USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProfileUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerProfileUpdation]
Purpose					: 
Example					: [usp_WebRetailerProfileUpdation]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th DEC 2011	SPAN Infotech 	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerProfileUpdation]
(
	
	 @RetailID int
	,@RetailerName Varchar(100)
	,@Address1 Varchar(100)
	,@Address2 Varchar(50)
	,@State Char(2)
	,@City Varchar(50)
	,@PostalCode Varchar(10)
	,@CorporatePhoneNo Char(10)
	,@IsNonProfitOrganisation bit
    ,@BusinessCategoryIDs varchar(1000)
    ,@RetailerURL varchar(1000)
    ,@RetailerKeyword Varchar(1000)
    ,@RetailerLatitude Float
    ,@RetailerLongitude Float
    --,@NumberOfLocations int
	
	--Input Variables of RetailContact
	,@ContactFirstName Varchar(20)
	,@ContactLastName Varchar(30)
	,@ContactPhone Char(10)
	--Input Variables of Users--
	,@ContactEmail Varchar(100)

	--Input Variables of UserRetailer--
	,@AdminFlag Bit
	,@CorporateAndStore BIT
	
	--For Retailer Filter
	,@AdminFilterID varchar(max)
	,@AdminFilterValueID varchar(max)

	--For Sub-Category
	,@SubCategoryID varchar(max)

	--Output Variable
	, @FindClearCacheURL varchar(1000) output
	, @ResponseRetailID int output 
	, @Status int output
	, @DuplicateRetailer bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			----To fetch Retailer filter Categories
			--DECLARE	@FilterBusinessCategory varchar(1000) 

			--SELECT P.Param
			--INTO #FilterCategory
			--FROM BusinessCategory BC
			--INNER JOIN fn_SplitParam(@BusinessCategoryIDs,',') P ON BC.BusinessCategoryID = P.Param
			--WHERE BusinessCategoryName in ('Bars','Dining') AND @AdminFilterID IS NOT NULL
				
			DECLARE @UserID int
			, @RetailName VARCHAR(100)
			, @RetailLocationID int
			, @ContactID int
			, @RetailAssociatedFlag Int
			, @RetailLocAssociatedFlag Int
			, @NewContact Int
			, @DuplicateRetailerName Bit
				
            SET @DuplicateRetailerName=0

		    SET @RetailAssociatedFlag=0
			SET @RetailLocAssociatedFlag =0
			DECLARE @RetailLocID Int
			
			SELECT @UserID=UserID
			FROM UserRetailer
			WHERE RetailID=@RetailID
		
			IF EXISTS (SELECT 1	FROM Retailer WHERE RetailID <> @RetailID AND RetailName =@RetailerName
						AND Address1 = @Address1 AND CorporatePhoneNo = @CorporatePhoneNo AND City = @City AND [State] = @State) 
			BEGIN
				SET @DuplicateRetailerName =1
			END
			
			--Check if the user is updating Retailer Name			
			IF @DuplicateRetailerName=0
			BEGIN											    
					IF (@PostalCode<>(Select PostalCode From retailer Where RetailID =@RetailID))
					BEGIN
						SET @RetailAssociatedFlag=1
					END		

					UPDATE Retailer
					SET [RetailName]=@RetailerName
						,[Address1]=@Address1
						,[Address2]=@Address2
						,[City]=@City
						,[State]=@State
						,[PostalCode]=@PostalCode
						,[CountryID]=1
						,[DateModified]=GETDATE()
						,[RetailURL] = @RetailerURL
						--,[NumberOfLocations] = @NumberOfLocations
						,[ModifyUserID]=@UserID
						, IsNonProfitOrganisation = @IsNonProfitOrganisation
						,[CorporatePhoneNo]=@CorporatePhoneNo
					WHERE RetailID=@RetailID --AND RetailerActive = 1
					
			--Update RetailerBusinessCategory Table.
						
			DELETE FROM RetailerBusinessCategory WHERE RetailerID = @RetailID    
					
			--Retailer Business and SubCategory Association

			 SELECT @SubCategoryID = REPLACE(@SubCategoryID, 'NULL', '0')  

			--Capture sub-CategoryIDs
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 ,BusCategoryID = Param
			INTO #BusCatIds
			FROM dbo.fn_SplitParamMultiDelimiter(@BusinessCategoryIDs, ',')                     
	
			SELECT RowNum = IDENTITY(INT, 1, 1)
				, SubCategoryID = Param
			INTO #SubCatIds
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCategoryID, '!~~!')  

			SELECT RowNum = IDENTITY(INT, 1, 1)
					,B.BusCategoryID
					,SubcatID =  F.Param 
			INTO #Buscat_SubCat 
			FROM #BusCatIds B
			INNER JOIN #SubCatIds S ON B.RowNum = S.RowNum 
			CROSS APPLY (select Param from fn_SplitParam(S.SubCategoryID,'|') ) F
				

			--Insert into RetailerBusinessCategory table.

			IF CURSOR_STATUS('global','CategoryList')>=-1
			BEGIN
			DEALLOCATE CategoryList
			END
                                                
			DECLARE @BusCategoryID1 int
			DECLARE @SubcatID1 int
			DECLARE CategoryList CURSOR STATIC FOR
			SELECT BusCategoryID
				  ,SubcatID
			FROM #Buscat_SubCat 
                           
			OPEN CategoryList
                           
			FETCH NEXT FROM CategoryList INTO @BusCategoryID1, @SubcatID1 
                           
			CREATE TABLE #TempBusCat(BusinnessCategoryID INT)

			WHILE @@FETCH_STATUS = 0
			BEGIN   
                            
				INSERT INTO #TempBusCat(BusinnessCategoryID) 			                                
				OUTPUT inserted.BusinnessCategoryID INTO #TempBusCat(BusinnessCategoryID)
							SELECT F.[Param]						     
				FROM dbo.fn_SplitParam(@BusCategoryID1, ',') F

				INSERT INTO RetailerBusinessCategory(RetailerID
												, BusinessCategoryID
												, DateCreated
												, BusinessSubCategoryID)
					OUTPUT inserted.BusinessCategoryID INTO #TempBusCat(BusinnessCategoryID)
											SELECT @RetailID
												, IIF(@BusCategoryID1= 0,NULL, @BusCategoryID1)
												, GETDATE()
												, IIF([Param] = 0,NULL,[Param])
											FROM dbo.fn_SplitParam(@SubcatID1, '|')

					
							DECLARE @Cnts INT 
							SELECT @Cnts = 0
							SELECT @Cnts = COUNT(Param)
							FROM dbo.fn_SplitParam(@SubcatID1, '|')
                                   
                               
				TRUNCATE TABLE #TempBusCat
		
				FETCH NEXT FROM CategoryList INTO @BusCategoryID1, @SubcatID1
                                  
			END                        

			CLOSE CategoryList
			DEALLOCATE CategoryList
				    
					--Update the Retail location info.					
					IF (@PostalCode<>(Select PostalCode From RetailLocation  WHERE RetailID=@RetailID AND Active = 1
																			AND (Headquarters = 1 OR CorporateAndStore = 1)))
					BEGIN
						SET @RetailLocAssociatedFlag =1
						SET @RetailLocID=(Select RetailLocationID From RetailLocation  WHERE RetailID=@RetailID AND Active = 1
																			AND (Headquarters = 1 OR CorporateAndStore = 1))
					END	

					UPDATE RetailLocation
					SET [RetailID]=@RetailID			  
						,[Address1]=@Address1
						,[Address2]=@Address2
						,[City]=@City
						,[State]=@State
						,[PostalCode]=@PostalCode
						,[RetailLocationLatitude] = @RetailerLatitude
						,[RetailLocationLongitude] = @RetailerLongitude
						,[CountryID]=1
						,[RetailLocationURL] = @RetailerURL
						,[DateModified]=GETDATE()			   
					WHERE RetailID=@RetailID 
					AND (Headquarters = 1 OR CorporateAndStore = 1)		
					AND Active = 1	
					
					--Capture the RetailLocationID
					
					SELECT @RetailLocationID=RetailLocationID
					FROM RetailLocation
					WHERE RetailID=@RetailID AND Active = 1
					AND (Headquarters = 1 OR CorporateAndStore = 1)
					
					
					--IF Contact is not created then create new contyact information
					IF NOT EXISTS(SELECT 1 FROM RetailContact
									WHERE RetailLocationID =@RetailLocationID)
					BEGIN

						INSERT INTO Contact(ContactFirstName
											,ContactLastname
											,ContactPhone
											,ContactEmail
											,DateCreated
											,CreateUserID)
						SELECT @ContactFirstName 
								,@ContactLastName 
								,@ContactPhone 
								,@ContactEmail 
								,GETDATE()
								,@UserID								

						SET @NewContact =SCOPE_IDENTITY()

						INSERT INTO RetailContact(ContactID
												,RetailLocationID
												,ContactLastName												
												,ContactEmail
												,ContactPhone)
						SELECT @NewContact
								,@RetailLocationID 
								,@ContactLastName							    
								,@ContactEmail 
								,@ContactPhone

					END

					ELSE
					BEGIN						

						SELECT @ContactID=Contactid
						FROM RetailContact RC
						INNER JOIN RetailLocation RL ON RC.RetailLocationID = RL.RetailLocationID
						WHERE RL.RetailLocationID=@RetailLocationID
						AND Active = 1
						AND (Headquarters = 1 OR CorporateAndStore = 1)
						
											
						--Update the Contact Info.
						UPDATE Contact
						SET [ContactFirstName]=@ContactFirstName
						   ,[ContactLastname]=@ContactLastName
						   ,[ContactTitle]=1
						   ,[ContactPhone]=@ContactPhone
						   ,[ContactEmail]=@ContactEmail
						   ,[DateModified]=GETDATE()
						   ,[ModifyUserID]=@UserID						  
						WHERE ContactID = @ContactID	
						
						--Update Email in users table too
						
						UPDATE U SET Email = @ContactEmail
						FROM Users U
						INNER JOIN UserRetailer UR ON U.UserID = UR.UserID
						WHERE RetailID = @RetailID		
					
					END

					--Update The RETAILERKEYWORD TABLE And if Retailer not exist in retailer keywords table then insert
					IF EXISTS(SELECT 1 FROM RetailerKeywords 
									   WHERE RetailID =@RetailID 
								       AND (RetailLocationID = @RetailLocationID OR RetailLocationID IS NULL))
					BEGIN
						UPDATE RetailerKeywords 
						SET [RetailKeyword]=@RetailerKeyword 
							, RetailLocationID = @RetailLocationID 
							,[DateModified]=GETDATE()
						WHERE RetailID =@RetailID  
						AND (RetailLocationID IS NULL OR RetailLocationID = @RetailLocationID)
					END
					ELSE
					BEGIN
						INSERT INTO RetailerKeywords
									(RetailID
									,RetailLocationID
									,RetailKeyword
									,DateCreated )
						VALUES(@RetailID
								,@RetailLocationID 
								,@RetailerKeyword 
								,GETDATE())
					END
			      			
					UPDATE UserRetailer
					SET AdminFlag = @AdminFlag
					WHERE RetailID = @RetailID
			
			----Update RetailerFilterAssociation
			--DELETE FROM RetailerFilterAssociation
			--WHERE RetailID = @RetailID

			----Retailer filter updation
			--IF (SELECT DISTINCT 1 FROM #FilterCategory) > 0
			--BEGIN
			--SELECT @FilterBusinessCategory = COALESCE(@FilterBusinessCategory + '!~~!' , '') + CAST(Param AS varchar(100))
			--FROM  #FilterCategory

			--SELECT @AdminFilterID = REPLACE(@AdminFilterID, 'NULL', '0')  
			--SELECT @AdminFilterValueID = REPLACE(@AdminFilterValueID, 'NULL', '0')

			--	--Capture Category and filter ID's
			--	SELECT RowNum = IDENTITY(INT, 1, 1)
			--			,CategoryID = Param
			--	INTO #CategoryID
			--	FROM dbo.fn_SplitParamMultiDelimiter(@FilterBusinessCategory, '!~~!')                     

			--	SELECT RowNum = IDENTITY(INT, 1, 1)
			--			, FilterId = Param
			--	INTO #FilterID
			--	FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterID, '!~~!')  
					
			--	 SELECT RowNum = IDENTITY(INT, 1, 1)
			--			, FilterValueId = Param
			--	INTO #FilterValueID
			--	FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterValueID, '!~~!')  
	
			--	SELECT RowNum = IDENTITY(INT, 1, 1)
			--			,C.CategoryID
			--			,FilterID =  F.Param 
			--	INTO #cat_filterValues 
			--	FROM #CategoryID C
			--	INNER JOIN #FilterID S ON C.RowNum = S.RowNum 
			--	CROSS APPLY (select Param from fn_SplitParam(S.FilterId,'|') ) F
				
			--	SELECT CategoryID
			--		  ,	FilterID
			--		  , FilterValueId
			--	INTO #Final_filtervalues
			--	FROM #cat_filterValues V
			--	INNER JOIN #FilterValueID I on V.RowNum = I.RowNum
			
			--	--Insert into Retailer Filter Association table.

			--	IF CURSOR_STATUS('global','CategoryList')>=-1
			--	BEGIN
			--	DEALLOCATE CategoryList
			--	END
                                                
			--	DECLARE @CategoryID1 int
			--	DECLARE @FilterID1 int
			--	DECLARE @FilterValueID1 varchar(1000)
			--	DECLARE CategoryList CURSOR STATIC FOR
			--	SELECT CategoryID
			--		  ,FilterId
			--		  ,FilterValueId          
			--	FROM #Final_filtervalues 
                      
			--	OPEN CategoryList
                           
			--	FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                           
			--	CREATE TABLE #TempBiz(BusinnessCategoryID INT)

			--	WHILE @@FETCH_STATUS = 0
			--	BEGIN   
                            
			--			INSERT INTO #TempBiz(BusinnessCategoryID) 			                                
			--			OUTPUT inserted.BusinnessCategoryID INTO #TempBiz(BusinnessCategoryID)
			--						SELECT F.[Param]						     
			--			FROM dbo.fn_SplitParam(@CategoryID1, ',') F


			--			INSERT INTO RetailerFilterAssociation(RetailID
			--									  ,AdminFilterID
			--									  ,BusinessCategoryID
			--									  ,AdminFilterValueID
			--									  ,DateCreated
			--									  ,CreatedUserID)
			--						OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinnessCategoryID)
			--						SELECT @RetailID
			--								,IIF(@FilterID1 = 0,NULL,@FilterID1)
			--								,@CategoryID1
			--								,IIF([Param] = 0,NULL,[Param])
			--								,GETDATE()
			--								,@UserID
			--						FROM dbo.fn_SplitParam(@FilterValueID1, '|')
					
			--						DECLARE @Cnt INT 
			--						SELECT @Cnt = 0
			--						SELECT @Cnt = COUNT(Param)
			--						FROM dbo.fn_SplitParam(@FilterValueID1, '|')
                                   
                               
			--			TRUNCATE TABLE #TempBiz
		
			--			FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                                  
			--	END                        

			--	CLOSE CategoryList
			--	DEALLOCATE CategoryList
		 --  END

					--Confirm No duplicate Retailer.
					SET @DuplicateRetailer = 0

			END		
			ELSE
			BEGIN				
					--Confirm Duplicate Retailer.
					SET @DuplicateRetailer = 1
			END		
					
			 
			--If the User is not updating the Retailer Name
			

				
					
				--Refresh the association w.r.t the Retail location
				----DELETE FROM HcRetailerAssociation 
				----FROM HcRetailerAssociation A
				----INNER JOIN RetailLocation RL ON RL.RetailLocationID = A.RetailLocationID
				----WHERE RL.RetailLocationID = @RetailLocationID
				----AND RL.Headquarters = 0 AND A.Associated = 1

				----Associate the retailer to the Hub Citi.
				--INSERT INTO HcRetailerAssociation(HcHubCitiID
				--							,RetailID
				--							,RetailLocationID
				--							,DateCreated
				--							,CreatedUserID
				--							,Associated) 
				--			SELECT DISTINCT LA.HcHubCitiID 
				--								,@RetailID
				--								,@RetailLocationID
				--								,GETDATE()
				--								,@UserID 
				--								,0
				--			FROM HcLocationAssociation LA
				--			INNER JOIN RetailLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode 
				--			LEFT JOIN HcRetailerAssociation RA ON RA.RetailLocationID = @RetailLocationID AND LA.HcHubCitiID = RA.HcHubCitiID 
				--			WHERE RA.HcRetailerAssociationID IS NULL
				--			AND RL.RetailLocationID = @RetailLocationID
			 --    			AND RL.Headquarters = 0 

			     
			   --      IF (@RetailAssociatedFlag =1)
					 --BEGIN
						--UPDATE HcRetailerAssociation set Associated =0 Where RetailID =@RetailID 					  
					 --END
					  IF (@RetailLocAssociatedFlag  =1)
					 BEGIN
						 UPDATE HcRetailerAssociation set Associated =0 Where RetailLocationID =@RetailLocID  					  
					 END
		
		  -------Find Clear Cache URL---29/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------	
		
			
		--Confirmation of Success.
			SELECT @Status = 0
			set @ResponseRetailID = @RetailID 
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerProfileUpdation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
