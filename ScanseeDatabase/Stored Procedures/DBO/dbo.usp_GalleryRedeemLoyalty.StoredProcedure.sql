USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryRedeemLoyalty]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GalleryRedeemLoyalty
Purpose					: To add Loyalty to User Gallery
Example					: usp_GalleryRedeemLoyalty 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th Dec 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryRedeemLoyalty]
(
	@UserID int
   ,@LoyaltyDealID int
   ,@UserClaimTypeID int
   ,@APIPartnerID int
   ,@LoyaltyDealRedemptionDate datetime
   
   --Output Variable 
    , @UsedFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			IF EXISTS(SELECT 1 FROM UserLoyaltyGallery WHERE LoyaltyDealID = @LoyaltyDealID AND UserID=@UserID AND UsedFlag=1)
				BEGIN
					SET @UsedFlag=1
				END
				
			ELSE
			BEGIN
				SET @UsedFlag=0
				 IF @APIPartnerID IS NULL
				 BEGIN
				     SELECT @APIPartnerID = APIPartnerID FROM LoyaltyDeal WHERE LoyaltyDealID = @LoyaltyDealID
				 END
				 
				 IF EXISTS (SELECT 1 FROM UserLoyaltyGallery WHERE LoyaltyDealID = @LoyaltyDealID AND UserID=@UserID AND UsedFlag=0)
					BEGIN
						UPDATE UserLoyaltyGallery
						SET UsedFlag=1
						WHERE LoyaltyDealID=@LoyaltyDealID AND LoyaltyDealID=@LoyaltyDealID
					END
				 
				 IF NOT EXISTS (SELECT 1 FROM UserLoyaltyGallery WHERE LoyaltyDealID = @LoyaltyDealID AND UserID=@UserID)
					BEGIN
						 INSERT INTO [UserLoyaltyGallery]
							   ([UserID]
							   ,[LoyaltyDealID]
							   ,[UserClaimTypeID]
							   ,[APIPartnerID]
							   ,[LoyaltyDealRedemptionDate])
						 VALUES
							   (@UserID 
							   ,@LoyaltyDealID 
							   ,@UserClaimTypeID
							   ,@APIPartnerID 
							   ,@LoyaltyDealRedemptionDate)
							   
						 UPDATE UserLoyaltyGallery
						 SET UsedFlag=1
						 WHERE LoyaltyDealID=@LoyaltyDealID AND LoyaltyDealID=@LoyaltyDealID
					END
				END
							   
					--Confirmation of Success.
					SELECT @Status = 0
		
					
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GalleryRedeemLoyalty.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
