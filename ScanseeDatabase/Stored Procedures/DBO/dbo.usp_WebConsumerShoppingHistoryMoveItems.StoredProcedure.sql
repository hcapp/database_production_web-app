USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerShoppingHistoryMoveItems]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerShoppingHistoryMoveItems
Purpose					: To move items from History to List and/or Favorites.
Example					: usp_WebConsumerShoppingHistoryMoveItems

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th July 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerShoppingHistoryMoveItems]
(
	 @UserProductID varchar(max)  
	---,@ProductID varchar(max)  
	,@AddTo varchar(20)  
	
	--User Tracking
	, @MainMenuID int
	
	--Output Variable 
	, @ExpiredProducts Varchar(200) Output
	, @ExpiredProductsBit Bit Output
	, @ListFlag int output
	, @FavoritesFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--to split UserProductIDs into table
			SELECT PARAM UserProductID
			INTO #UserProduct
			FROM dbo.fn_SplitParam(@UserProductID, ',')
			
			DECLARE @UserID INT
			
			Set @ExpiredProductsBit=0
			IF Exists (SELECT 1 From Product P
			           INNER  JOIN UserProduct UP ON UP.ProductID =P.ProductID 
			           INNER JOIN fn_SplitParam(@UserProductID,',') F ON F.Param =UP.UserProductID 
			           Where ISNULL(P.ProductExpirationDate,GETDATE()+1) <GETDATE())
				BEGIN
				Set @ExpiredProductsBit=1
				SELECT @ExpiredProducts= COALESCE(@ExpiredProducts + ',', '') + CAST(P.ProductID AS VARCHAR)
				From Product P
			    INNER  JOIN UserProduct UP ON UP.ProductID =P.ProductID 
			    INNER JOIN fn_SplitParam(@UserProductID,',') F ON F.Param =UP.UserProductID 
			    Where ISNULL(P.ProductExpirationDate,GETDATE()+1) <GETDATE()
			END
			
			
			SELECT @UserID = UserID
			FROM UserProduct U
			INNER JOIN dbo.fn_SplitParam(@UserProductID, ',') F ON F.Param = U.UserProductID
	
			--To move item to List
			IF 'List' IN (SELECT LTRIM(RTRIM(PARAM)) FROM fn_SplitParam(@AddTo, ','))
			BEGIN
				--If the item already exists in the list, flag will be enabled to prompt alert to user.
				SELECT @ListFlag = (CASE WHEN COUNT(ProductID) >= 1 THEN 1 ELSE 0 END)
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID 
				WHERE UP.TodayListtItem = 1
				
				--To update list items
				UPDATE UserProduct 
				SET TodayListtItem = 1
					,TodayListAddDate = GETDATE()
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID
				INNER JOIN Product P ON P.ProductID =UP.ProductID 
				WHERE UP.TodayListtItem = 0 AND ISNULL(P.ProductExpirationDate,GETDATE()+1) >=GETDATE()
				
				--User Tracking
				--Capture user shopping list items
				
				UPDATE ScanSeeReportingDatabase..UserProduct 
				SET TodayListtItem = 1
					,TodayListAddDate = GETDATE()
				FROM ScanSeeReportingDatabase..UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID
				INNER JOIN Product P ON P.ProductID =UP.ProductID 
				WHERE UP.TodayListtItem = 0 AND ISNULL(P.ProductExpirationDate,GETDATE()+1) >=GETDATE()						
			
			END
			
			--To move item to Favorites
			IF 'Favorites' IN (SELECT LTRIM(RTRIM(PARAM)) FROM fn_SplitParam(@AddTo, ','))
			BEGIN
				--If the item already exists in the Favorites, flag will be enabled to prompt alert to user.
				SELECT @FavoritesFlag = (CASE WHEN COUNT(ProductID) >= 1 THEN 1 ELSE 0 END)
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID 
				WHERE UP.MasterListItem = 1
				
				--To update list items
				UPDATE UserProduct 
				SET MasterListItem = 1
					,MasterListAddDate = GETDATE()
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID
				INNER JOIN Product P ON P.ProductID =UP.ProductID 
				WHERE ISNULL(P.ProductExpirationDate,GETDATE()+1) >=GETDATE()  
				
				--User Tracking
				--Capture user favorites list items
				
				UPDATE ScanSeeReportingDatabase..UserProduct 
				SET MasterListItem = 1
					,MasterListAddDate = GETDATE()
				FROM ScanSeeReportingDatabase..UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID
				INNER JOIN Product P ON P.ProductID =UP.ProductID 
				WHERE ISNULL(P.ProductExpirationDate,GETDATE()+1) >=GETDATE()
								
			END

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerShoppingHistoryMoveItems.'		
			--- Execute retrieval of Error info.
			EXEC [Version8].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
