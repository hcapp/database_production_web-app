USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerUserTrackingProductShare]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebConsumerUserTrackingProductShare] 
Purpose               : To capure the deails of the prouct share.  
Example               : [usp_WebConsumerUserTrackingProductShare] 
  
History  
Version    Date				Author				Change Description  
---------------------------------------------------------------   
1.0       27th June 2013	    Dhananjaya TR		Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerUserTrackingProductShare]  
(  
  
   @MainMenuID int 
 , @ShareTypeID int
 , @ProductID int 
 , @TargetAddress Varchar(255)   
 --OutPut Variable  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
         --Display Module Details
		 INSERT INTO ScanSeeReportingDatabase..ShareProduct(MainMenuID
														   ,ShareTypeID
														   ,TargetAddress
														   ,ProductID  
														   ,CreatedDate)
		 VALUES	(@MainMenuID
		        ,@ShareTypeID 
		        ,@TargetAddress 
		        ,@ProductID  
		        ,GETDATE())								     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebConsumerUserTrackingProductShare].'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
