USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerPreferredUniversityDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_UserTrackingWebConsumerMainMenuCreation]
Purpose					: To Display the Shopper preferred university.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			27th May 2013				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerPreferredUniversityDisplay]

 --Input Variables
    @UserID int
 --Output variables
  , @Status int OUTPUT

AS
BEGIN

	BEGIN TRY
	
		--Display the user configured university.
		SELECT DISTINCT UU.UserID
			 , UU.UniversityID
			 , U.UniversityName
			 , U.State
		FROM UserUniversity UU
		INNER JOIN University U ON U.UniversityID = UU.UniversityID
		WHERE UU.UserID = @UserID
		
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebShopperDisplayPreferences.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
