USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminAPPVersionDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: [usp_WebAdminAPPVersionDetails]  
Purpose					: Display all Version Details.  
Example					: [usp_WebAdminAPPVersionDetails]  
  
History  
Version  Date				Author			 Change	Description  
---------------------------------------------------------------   
1.0		 5th Sep 2013   	Dhananjaya TR	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebAdminAPPVersionDetails]  
(  
   @VersionID Int 
 --OutPut Variable     
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
  BEGIN TRY  
  
      DECLARE @Config Varchar(1000)
      SELECT @Config=ScreenContent  
      FROM AppConfiguration 
      WHERE ScreenName like 'FTP URL' AND ConfigurationType like 'FTP Server Configuaration'   
        
	  SELECT APPVersionManagementID VersionID
	        ,VersionNumber appVersionNo
			,DatabaseSchemaName SchemaName
			,ServerBuild serverBuildNo
			,((SELECT CONVERT(VARCHAR(20), QAFirstReleaseDate, 105))+ ' '+(SELECT CONVERT(VARCHAR(20), QAFirstReleaseDate, 108))) firstQARelDate
			,((SELECT CONVERT(VARCHAR(20), QALastReleaseDate, 105))+ ' '+(SELECT CONVERT(VARCHAR(20), QALastReleaseDate, 108))) lastQARelDate
			,CASE WHEN QAReleaseNotePath IS NOT NULL THEN @Config+'/'+QAReleaseNotePath ELSE QAReleaseNotePath END relQANotePath
			,((SELECT CONVERT(VARCHAR(20), ProductionFirstReleaseDate, 105))+ ' '+(SELECT CONVERT(VARCHAR(20), ProductionFirstReleaseDate, 108))) firstProdtnRelDate
			,((SELECT CONVERT(VARCHAR(20), ProductionLastReleaseDate, 105))+ ' '+(SELECT CONVERT(VARCHAR(20), ProductionLastReleaseDate, 108))) lastProdtnRelDate
			,CASE WHEN ProductionReleaseNotePath IS NOT NULL THEN @Config+'/'+ProductionReleaseNotePath ELSE ProductionReleaseNotePath END relProdtnNotePath
			,ITunesUploadDate = ((SELECT CONVERT(VARCHAR(20), ITunesUploadDate, 105))+ ' '+(SELECT CONVERT(VARCHAR(20), ITunesUploadDate, 108)))
			,BuildApprovalDate = ((SELECT CONVERT(VARCHAR(20), BuildApprovalDate, 105))+ ' '+(SELECT CONVERT(VARCHAR(20), BuildApprovalDate, 108)))
			--,reltypeID=CASE WHEN BuildApprovalDate IS NOT NULL THEN 3 WHEN ProductionFirstReleaseDate IS NOT NULL THEN 2 ELSE 1 END
			,reltypeID=CASE WHEN ((ITunesUploadDate IS NOT NULL AND ITunesUploadDate>ProductionLastReleaseDate AND ITunesUploadDate>QALastReleaseDate) OR
			      (BuildApprovalDate IS NOT NULL AND BuildApprovalDate>ProductionLastReleaseDate AND BuildApprovalDate>QALastReleaseDate)) THEN 3
			      WHEN ProductionLastReleaseDate IS NOT NULL AND ProductionLastReleaseDate>QALastReleaseDate THEN 2
			      WHEN QALastReleaseDate IS NOT NULL AND ISNULL(ProductionLastReleaseDate,QALastReleaseDate-1)<QALastReleaseDate THEN 1 END			     
			
			,Type AppType
			,itunesFlag=CASE WHEN ITunesUploadDate IS NOT NULL THEN 1 ELSE 0 END
			,QAReleaseNoteFileName QAFileName
			,ProductionReleaseNoteFileName  ProdFileName
	  FROM APPVersionManagement AV
	  INNER JOIN ApplicationType AT ON AT.ApplicationTypeID =AV.ApplicationTypeID 
	  WHERE APPVersionManagementID  = @VersionID      
  END TRY      
  BEGIN CATCH    
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebAdminAPPVersionDetails].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
  END;  
     
 END CATCH;  
END;


GO
