USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayRetailLocationQRURL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayRetailLocationQRURL
Purpose					: To Display the Retailer Custom Page URLs.
Example					: usp_WebDisplayRetailLocationQRURL

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Aug 2012	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebDisplayRetailLocationQRURL]
(
	  
	  @UserID int
	, @RetailID int  
	, @RetailLocationID int  
    
	--Output Variable 
	, @FindClearCacheURL VARCHAR(1000) OUTPUT
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 
		 DECLARE @RetailerConfig varchar(50)
		 		 		
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='QR Code Configuration'
		 
		 
		 
		 DECLARE @QRTypecode varchar(50)
		 
		 SELECT @QRTypecode=QRTypeCode 
		 FROM QRTypes 
		 WHERE QRTypeName ='Main Menu Page'
		 
		 
		SELECT QrUrl = @RetailerConfig + CAST(@QRTypecode AS VARCHAR(10)) + '.htm?retId=' + CAST(@RetailID AS VARCHAR(10)) + '&retlocId=' +CAST(@RetailLocationID AS VARCHAR(10))
		       ,StoreIdentification storeID
		FROM RetailLocation 
		WHERE RetailLocationID =@RetailLocationID AND Active = 1	  
		
		-------Find Clear Cache URL---03/02/2015--------

		DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

	  -----------------------------------------------
	   
			   	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
