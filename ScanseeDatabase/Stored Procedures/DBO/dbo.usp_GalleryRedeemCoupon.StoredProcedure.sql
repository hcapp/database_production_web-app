USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryRedeemCoupon]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GalleryRedeemCoupon
Purpose					: To add Coupon to User Gallery And making used flag as 1 when they tap on redeem button.
Example					: usp_GalleryRedeemCoupon 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th Dec 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryRedeemCoupon]
(
	@CouponID int
	, @UserID int
	, @UserCouponClaimTypeID tinyint
	, @CouponPayoutMethod varchar(20)
	, @RetailLocationID int
	, @CouponClaimDate datetime
	
	--Output Variable 
	, @UsedFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			IF EXISTS(SELECT 1 FROM UserCouponGallery WHERE CouponID = @CouponID AND UserID=@UserID AND UsedFlag=1)
			BEGIN
				SET @UsedFlag=1
			END
			
			ELSE
			BEGIN
				SET @UsedFlag=0
				IF  EXISTS (SELECT 1 FROM UserCouponGallery WHERE CouponID = @CouponID AND UserID=@UserID AND UsedFlag=0)
				BEGIN
					UPDATE UserCouponGallery
					SET UsedFlag=1
					Where UserID=@UserID AND CouponID=@CouponID
				END
				
				IF NOT EXISTS (SELECT 1 FROM UserCouponGallery WHERE CouponID = @CouponID AND UserID=@UserID)
				BEGIN
					INSERT INTO [UserCouponGallery]
					   ([CouponID]
					   ,[UserID]
					   ,[UserClaimTypeID]
					   ,[RetailLocationID]
					   ,[CouponClaimDate])
					VALUES
					   (@CouponID
					   ,@UserID
					   ,@UserCouponClaimTypeID
					   ,@RetailLocationID
					   ,@CouponClaimDate)
					
					UPDATE UserCouponGallery
					SET UsedFlag=1
					Where UserID=@UserID AND CouponID=@CouponID
					   
				END
			END
			
			--Confirmation of Success.
					SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GalleryRedeemCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
