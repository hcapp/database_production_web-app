USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListAddLoyalty]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListAddLoyalty
Purpose					: To add Loyalty to User Gallery
Example					: usp_MasterShoppingListAddLoyalty 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListAddLoyalty]
(
	@UserID int
   ,@LoyaltyDealID int
   ,@LoyaltyDealSource varchar(30)
   ,@LoyaltyDealPayoutMethod varchar(20)
   ,@LoyaltyDealRedemptionDate datetime
   
   --Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--INSERT INTO [UserLoyaltyGallery]
			--   ([UserID]
			--   ,[LoyaltyDealID]
			--   ,[LoyaltyDealSource]
			--   ,[LoyaltyDealPayoutMethod]
			--   ,[LoyaltyDealRedemptionDate])
			--VALUES
			--   (@UserID
			--   ,@LoyaltyDealID
			--   ,'testsource'
			--   ,'testmethod'
			--   ,@LoyaltyDealRedemptionDate)
			
			 DECLARE @APIPartnerID SMALLINT
			 SELECT @APIPartnerID = APIPartnerID FROM LoyaltyDeal WHERE LoyaltyDealID = @LoyaltyDealID
			 IF NOT EXISTS (SELECT 1 FROM UserLoyaltyGallery WHERE LoyaltyDealID = @LoyaltyDealID AND UserID=@UserID)
					 INSERT INTO [UserLoyaltyGallery]
						   ([UserID]
						   ,[LoyaltyDealID]
						   ,[UserClaimTypeID]
						   ,[APIPartnerID]
						   ,[LoyaltyDealRedemptionDate])
					 VALUES
						   (@UserID 
						   ,@LoyaltyDealID 
						   ,NULL
						   ,@APIPartnerID 
						   ,@LoyaltyDealRedemptionDate)
						   
					--Confirmation of Success.
					SELECT @Status = 0
		
					
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListAddLoyalty.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
