USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsInsertion_Today]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsInsertion]
Purpose					: To insert into RssFeedNews from Staging table.
Example					: [usp_WebRssFeedNewsInsertion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16 Feb 2015		Mohith H R	        1.1
1.1         07 Dec 2015     Suganya C           Duplicate news removal
1.2							Sagar Byali			Video deletion changes
1.3			28 Oct 2016						Performance changes
---------------------------------------------------------------
*/

create PROCEDURE [dbo].[usp_WebRssFeedNewsInsertion_Today]
(
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
	DECLARE @ExpiryPublishedDateRockwall DATE =  DATEADD(d,-15,(CONVERT(DATE,GETDATE())))
			,@ExpiryPublishedDateKilleen DATE =  DATEADD(d,-5,(CONVERT(DATE,GETDATE())))
			,@ExpiryPublishedDateMarbelVideos DATE
			,@ExpiryPublishedDateMarbelAllCategoryNews DATE
			,@DoDelete TINYINT = CASE WHEN DATEPART(HH,GETDATE()) = 0 THEN 1 ELSE 0 END
		
	--Added on 26th Sept 2016 to retain 14 days news for 'All' category
	SELECT @ExpiryPublishedDateMarbelAllCategoryNews = DATEADD(d,-14,MAX(CONVERT(DATE,PublishedDate)))
		FROM RssFeedNewsStagingTable 
			WHERE NewsType = 'all' AND HcHubCitiID = 10

	--Added on 22nd Aug 2016 to retain one day videos 
	SELECT @ExpiryPublishedDateMarbelVideos = MAX(CONVERT(DATE,DateCreated)) 
		FROM RssFeedNewsStagingTable 
			WHERE NewsType='videos' AND HcHubCitiID = 10

	--Added on 26/10/2016
	CREATE TABLE #Temp (MaxRssFeedNewsID int
						,NewsType varchar(500)
						,HcHubCitiID int
						,classification varchar(200)
						,Title varchar(5000))

	INSERT INTO #Temp
	SELECT MAX(RssFeedNewsID) as MaxRssFeedNewsID,NewsType,HcHubCitiID, classification, Title
		FROM RssFeedNewsStagingTable
			WHERE NewsType = 'classifields' AND Classification IS NOT NULL  
				GROUP BY NewsType,HcHubCitiID, Classification, Title
   
	INSERT INTO #Temp
	SELECT MAX(RssFeedNewsID) as MaxRssFeedNewsID,NewsType,HcHubCitiID, classification, Title
		FROM RssFeedNewsStagingTable
			WHERE NewsType <> 'classifields' AND Title IS NOT NULL 
			GROUP BY NewsType,HcHubCitiID, Classification, Title

	SELECT  DISTINCT RS.Title
				,RS.ImagePath
				,RS.ShortDescription
				,RS.LongDescription
				,RS.Link
				,RS.PublishedDate
				,RS.NewsType
				,RS.Message
				,RS.HcHubCitiID	
				,RS.Classification
				,RS.Section
				,RS.AdCopy
				,RS.VideoLink
	INTO #InsertValues		
	FROM #Temp T 
	INNER JOIN RssFeedNewsStagingTable RS ON RS.RssFeedNewsID = T.MaxRssFeedNewsID
	AND RS.HcHubCitiID = T.HcHubCitiID AND RS.NewsType = T.NewsType 
	--AND WHERE ((RS.NewsType <> 'Classifields' AND RS.Title = T.Title) OR (RS.NewsType = 'Classifields')) 
	WHERE (RS.Title = T.Title OR RS.NewsType = 'Classifields') 

	BEGIN TRAN

	----To retain 15 days news 
	DELETE FROM RssFeedNewsStagingTable
	WHERE (HcHubCitiID = 28 AND convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateRockwall)
	OR (HcHubCitiID = 10 AND ((convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateRockwall AND NewsType NOT IN ('Videos','all'))
			OR (convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateMarbelAllCategoryNews AND NewsType = 'all')
			OR (convert(date,ISNULL(PublishedDate,DateCreated)) <> @ExpiryPublishedDateMarbelVideos AND NewsType = 'Videos')))
	OR (HcHubCitiID = 52 AND convert(date,ISNULL(PublishedDate,DateCreated)) < @ExpiryPublishedDateKilleen)
	OR (HcHubCitiID NOT IN (28 ,52, 10))
	

	DELETE FROM RssFeedNews
	FROM RssFeedNews RF	INNER JOIN RssFeedNewsStagingTable RS ON RF.NewsType = RS.NewsType
		WHERE ((RS.Title IS NOT NULL AND RS.NewsType <> 'Classifields')	
			OR (RS.NewsType = 'Classifields' AND RS.Classification IS NOT NULL))

	--Insert into RssFeedNews
	INSERT INTO RssFeedNews(Title
						,ImagePath
						,ShortDescription
						,LongDescription
						,Link
						,PublishedDate
						,NewsType
						,Message
						,HcHubCitiID
						,Classification
						,Section
						,AdCopy
						,VideoLink)															
	SELECT * FROM #InsertValues

	COMMIT TRAN 		
				   
	SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
		SET @ErrORNumber = ERROR_NUMBER()
		SET @ErrORMessage = ERROR_MESSAGE()
		SET @Status=1 
	END CATCH;
END;






GO
