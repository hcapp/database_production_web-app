USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondProductCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondProductCreation
Purpose					: To insert the new products in to product table that are coming from wish pond.
Example					: usp_BatchWishPondProductCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04 Jan 2012	  PadmaPriya M   	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondProductCreation]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To Get the ManufacturerID
			DECLARE @ManufacturerID INT
			SELECT @ManufacturerID = ManufacturerID 
			FROM Manufacturer 
			WHERE ManufName = 'WishPond Products'
			
			--To insert the new products in to product table that are coming from wish pond
			
			SELECT  DISTINCT @ManufacturerID ManufacturerID
					,ROW_NUM=IDENTITY(INt,1,1)
					,0 Service
					,WishPondProductName
					,WishPondDealName
					,WishPondDescription
					,WishPondThumbNailImageURL
					,WishPondSalePrice
					,GETDATE() DateCreated
					,GETDATE() DateModified
					,WishPondDealID	
			INTO #Products
			FROM (SELECT WD.WishPondDealID
						,WishPondDealName
						,WishPondProductName
						,WishPondSalePrice
						,WishPondDescription
						,WishPondThumbNailImageURL
						,P.ProductID  
				  FROM APIWishPondDealXRef WD  
					LEFT JOIN Product P ON LTRIM(RTRIM(P.ProductName)) = LTRIM(RTRIM(WD.WishPondProductName))
				  WHERE WishPondProductName IS NOT NULL)Prod
			WHERE  ProductID IS NULL
			
			INSERT INTO PRODUCT (ManufacturerID
								,Service
								,ProductName
								,ProductShortDescription
								,ProductLongDescription
								,ProductImagePath
								,SuggestedRetailPrice
								,ProductAddDate
								,ProductModifyDate
								,WishPondDealID)
			SELECT ManufacturerID
					,Service
					,WishPondProductName
					,WishPondDealName
					,WishPondDescription
					,WishPondThumbNailImageURL
					,WishPondSalePrice
					,DateCreated
					,DateModified
					,WishPondDealID
			FROM #Products
			WHERE ROW_NUM IN (SELECT MIN(Row_Num) FROM #Products GROUP BY WishPondProductName)	
			
			

			--INSERT INTO PRODUCT (ManufacturerID
			--					,Service
			--					,ProductName
			--					,ProductShortDescription
			--					,ProductLongDescription
			--					,ProductImagePath
			--					,SuggestedRetailPrice
			--					,ProductAddDate
			--					,ProductModifyDate
			--					,WishPondDealID)
								
			--SELECT  DISTINCT @ManufacturerID
			--		,0
			--		,WishPondProductName
			--		,WishPondDealName
			--		,WishPondDescription
			--		,WishPondThumbNailImageURL
			--		,WishPondSalePrice
			--		,GETDATE()
			--		,GETDATE()
			--		,WishPondDealID	
			--FROM (SELECT WD.WishPondDealID
			--			,WishPondDealName
			--			,WishPondProductName
			--			,WishPondSalePrice
			--			,WishPondDescription
			--			,WishPondThumbNailImageURL
			--			,P.ProductID  
			--	  FROM APIWishPondDealXRef WD  
			--		LEFT JOIN Product P ON LTRIM(RTRIM(P.ProductName)) = LTRIM(RTRIM(WD.WishPondProductName))
			--	  WHERE WishPondProductName IS NOT NULL)Prod
			--WHERE  ProductID IS NULL

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondProductCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
