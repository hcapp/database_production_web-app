USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerLogdisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadManufacturerLogdisplay
Purpose					: 
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			28th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerLogdisplay]
(

	--Input Input Parameter(s)--	 
	
	--Output Variable--	  

	  @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		SELECT    UploadManufacturerLogID as upoadManufacturerLogID
				, UserID as userID
				, ManufacturerID as manufacturerID
				, ManufacturerName as manufacturerName
				, ProductFileName as productFileName
				, ProductTotalRowsProcessed as productTotalRowsProcessed
				, ProductSuccessfulRowCount as productSuccessfulRowCount
				, ProductFailureRowCount as productFailureRowCount
				, ProductAttributeFileName as productAttributeFileName
				, ProductAttributeTotalRowsProcessed as productAttributeTotalRowsProcessed
				, ProductAttributeSuccessfulRowCount as productAttributeSuccessfulRowCount
				, ProductAttributeFailureRowCount as productAttributeFailureRowCount
				, Remarks as remarks
				, ProcessDate as processDate
				, ProcessStartDate as processStartDate
				, ProcessEndDate as processEndDate
				, EmailNotificationFlag	 as emailNotificationFlag	      
		FROM UploadManufacturerLog 
		WHERE EmailNotificationFlag = 0
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerLogdisplay.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
	END CATCH	 
	
END;


GO
