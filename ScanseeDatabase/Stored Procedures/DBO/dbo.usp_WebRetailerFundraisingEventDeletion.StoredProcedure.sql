USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFundraisingEventDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerFundraisingEventDeletion]
Purpose					: Deleting Selected  Retailer fundraising Event.
Example					: [usp_WebRetailerFundraisingEventDeletion]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			11/11/2014      SPAN             1.0
---------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[usp_WebRetailerFundraisingEventDeletion]
(   
    --Input variable.	  
      @FundraisingID Int
	
  	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
			
			--Logical Delete
			UPDATE HcFundraising SET Active = 0
			WHERE HcFundraisingID = @FundraisingID 


			--Confirmation of Success
			SELECT @Status = 0
			COMMIT TRANSACTION	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerFundraisingEventDeletion].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			ROLLBACK TRANSACTION
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
