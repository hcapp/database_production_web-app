USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingCartDispaly]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*      
Stored Procedure name : usp_ShoppingCartDispaly      
Purpose     : To retrive Shopping Cart Products.      
Example     : usp_ShoppingCartDispaly 14      
      
History      
Version  Date   Author   Change Description      
---------------------------------------------------------------       
1.0   6th June 2011 SPAN Infotech India Initail Version      
---------------------------------------------------------------      
*/      
      
CREATE PROCEDURE [dbo].[usp_ShoppingCartDispaly]      
(      
 @UserID int      
       
       
 --OutPut Variable      
 , @ErrorNumber int output      
 , @ErrorMessage varchar(1000) output      
)      
AS      
BEGIN      
      
 BEGIN TRY      
 
      --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'       
       
  --SELECT Category = 'ShoppingBasket'      
  -- , UP.UserProductID       
  -- , UP.UserID       
  -- , UP.ProductID       
  -- , ISNULL(R.RetailID, 0) retailerId      
  -- , ISNULL(R.RetailName, 'Others') retailerName          
  -- , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END      
  -- , Usage = 'Red'      
  --FROM UserProduct UP      
  --LEFT JOIN Product P ON P.ProductID = UP.ProductID       
  --LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID       
  --LEFT JOIN Retailer R ON R.RetailID = UR.RetailID        
  --WHERE UP.UserID = @UserID       
  --AND ShopCartItem = 1      
  --AND MasterListItem = 1      
  --AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())       
                
          DECLARE @CLRFlag bit      
                
    SELECT Category = 'ShoppingBasket'      
     , UP.UserProductID       
     , UP.UserID       
     , UP.ProductID       
     , ISNULL(R.RetailID, 0) retailerId      
     , ISNULL(R.RetailName, 'Others') retailerName          
     , ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END    
     , ProductImagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END        
     , Coupon.Coupon      
     , Loyalty.Loyalty      
     , Rebate.Rebate      
    INTO #products       
    FROM UserProduct UP      
    LEFT JOIN Product P ON P.ProductID = UP.ProductID       
    LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID       
    LEFT JOIN Retailer R ON R.RetailID = UR.RetailID      
    OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon      
    OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty      
    OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate        
    WHERE UP.UserID = @UserID       
    AND ShopCartItem = 1      
    --AND MasterListItem = 1      
    AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE())       
          
    SELECT Category Category      
     , UserProductID UserProductID      
     , p.UserID UserID      
     , p.ProductID ProductID      
     , retailerId  retailerId      
     , retailerName  retailerName      
     , ProductName  ProductName     
     , ProductImagePath imagePath     
     , coupon = CASE WHEN COUNT(p.Coupon) = 0 THEN 0 ELSE 1 END      
     , loyalty = CASE WHEN COUNT(p.Loyalty) = 0 THEN 0 ELSE 1 END      
     , rebate = CASE WHEN COUNT(p.Rebate) = 0 THEN 0 ELSE 1 END      
     , coupon_Status = CASE WHEN COUNT(p.Coupon) = 0 THEN 'Grey'       
           ELSE CASE WHEN COUNT(uc.CouponID) = 0 THEN 'Red' ELSE 'Green' END      
         END      
     , loyalty_Status = CASE WHEN COUNT(P.Loyalty) = 0 THEN 'Grey'      
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END      
          END      
     , rebate_Status = CASE WHEN COUNT(P.Rebate) = 0 THEN 'Grey'      
           ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END      
         END      
     , CLRFlag = CASE WHEN (COUNT(P.Coupon)+COUNT(P.Loyalty)+COUNT(P.Rebate)) > 0 THEN 1 ELSE 0 END      
     FROM #products p      
      LEFT JOIN UserCouponGallery UC ON UC.UserID = P.UserID AND UC.CouponID = P.Coupon      
      LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = P.UserID AND UL.LoyaltyDealID = P.Loyalty      
      LEFT JOIN UserRebateGallery UR ON UR.UserID = P.UserID AND UR.RebateID = P.Rebate       
       GROUP BY  category      
       ,UserProductID       
       , p.UserID       
       , p.ProductID       
       , retailerId      
       , retailerName        
       , ProductName       
       , ProductImagePath     
        
 END TRY      
       
 BEGIN CATCH      
       
  --Check whether the Transaction is uncommitable.      
  IF @@ERROR <> 0      
  BEGIN      
   PRINT 'Error occured in Stored Procedure usp_ShoppingCartDispaly.'        
   --- Execute retrieval of Error info.      
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output       
  END;      
         
 END CATCH;      
END;

GO
