USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerPageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerPageDisplay
Purpose					: To Display the Retailer Page Details.
Example					: usp_RetailerPageDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st May 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerPageDisplay]
(	  
	 
	  @RetailID int  
	, @PageID int  
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		 DECLARE @RetailerConfig varchar(50)
		
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		SELECT @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + R.RetailerImagePath Logo
			 , QRRCP.Pagetitle
			 , @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + QRRCP.[Image]	[Image]  
			 , QRRCP.PageDescription
			 , QRRCP.ShortDescription
			 , QRRCP.LongDescription
			 , R.RetailName
			 , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
		FROM QRRetailerCustomPage QRRCP
		INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID
		INNER JOIN Retailer R ON QRRCP.RetailID = R.RetailID
		WHERE QRRCP.QRRetailerCustomPageID = @PageID
		AND QRRCP.RetailID = @RetailID
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
