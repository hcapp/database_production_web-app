USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierBillingPlanDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_WebSupplierBillingPlanDisplay]
(

	--Input Parameter(s)--	  
	
	--Output Variable-- 
	  
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--Retrieve all the active plans.			
		SELECT M.ManufacturerBillingPlanID productId
		     , M.ManufacturerBillingPlanName productName
		     , M.ManufacturerBillingPlanDescription description
		     , M.BillingPlanPrice price
		     , M.ManufacturerBillingYearly retailerBillingYearly
		     , M.ManufacturerBillingMonthly retailerBillingMonthly
		FROM ManufacturerBillingPlan M	
	    WHERE ActiveFlag = 1
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierBillingPlanDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
