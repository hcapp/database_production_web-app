USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayCouponDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebDisplayCouponDetails  
Purpose     : To display all the Coupons Created.  
Example     : EXEC usp_WebDisplayCouponDetails  
  
History  
Version  Date       Author    Change Description  
-----------------------------------------------------------------------------------   
1.0   1st December 2011    Pavan Sharma K  Initial Version  
-----------------------------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebDisplayCouponDetails]  
(  
  
 --Input Input Parameter(s)--  
   --@LowerLimit int  
   
   
 --Output Variable--     
   
 --, @NxtPageFlag bit output  
   @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
   
 --DECLARE @UpperLimit INT  
 --SELECT @UpperLimit = 20 + @LowerLimit  
   
 --To get Media Server Configuration.  
  DECLARE @Config varchar(50)    
  SELECT @Config=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Retailer Media Server Configuration'   
   
 DECLARE @MAXCNT INT -- TO get the count of records in the result set  
   
  SELECT ROW_NUM = ROW_NUMBER()OVER(ORDER BY CouponDateAdded DESC)
     , CouponID  
     , CouponName  
     , CouponDiscountType  
     , CouponDiscountAmount  
     , CouponDiscountPct   
     , CouponLongDescription  
     , CouponShortDescription
     , CouponStartDate  
     , CouponDisplayExpirationDate  
     , CouponExpireDate  
     , ExternalCoupon  
     , CouponURL   
     , CouponDateAdded  
     , ViewableOnWeb
      
  INTO #TEMP   
  FROM (SELECT DISTINCT
       C.CouponID  
     , C.CouponName  
     , C.CouponDiscountType  
     , C.CouponDiscountAmount  
     , C.CouponDiscountPct  
     , C.CouponLongDescription  
     , C.CouponShortDescription      
     , C.CouponStartDate  
     , C.CouponDisplayExpirationDate  
     , C.CouponExpireDate  
     , C.ExternalCoupon  
     , C.CouponURL  
     , C.CouponDateAdded  
     , ViewableOnWeb
    FROM Coupon C
    ) Coupons  
    --ORDER BY CouponDateAdded DESC  
    
  --SELECT @MAXCNT = MAX(ROW_NUM) FROM #TEMP      -- Getting row count  
   
  --SET @NxtPageFlag = CASE WHEN (@MAXCNT - @UpperLimit) > 0 THEN 1 ELSE 0 END -- This flag is set if there are still more records to be displayed  
    
  SELECT ROW_NUM 
     , CouponID  
     , CouponName  
     , CouponDiscountType  
     , CouponDiscountAmount  
     , CouponDiscountPct   
     , CouponLongDescription  
     , CouponShortDescription 
     , CouponStartDate  
     , CouponDisplayExpirationDate  
     , CouponExpireDate  
     , ExternalCoupon  
     , CouponURL  
     , ViewableOnWeb
  FROM #TEMP   
  --WHERE ROW_NUM BETWEEN (@LowerLimit + 1) AND @UpperLimit        --Display the records that fall in the limits specified based on the row count  
  ORDER BY ROW_NUM ASC  
     
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebDisplayCouponDetails.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;




GO
