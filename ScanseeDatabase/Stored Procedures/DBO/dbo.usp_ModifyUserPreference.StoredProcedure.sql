USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ModifyUserPreference]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ModifyUserPreference
Purpose					: To Store User preferred info in UserPreference Table.
Example					: 
DECLARE @return_value int
EXEC usp_ModifyUserPreference 'John@email.com', 1, 5, 1, 1, 1, 1, 1, 5, 1, 0, NULL, '5/19/2011', @Result = @return_value OUTPUT
SELECT @return_value return_value

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			19th May 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ModifyUserPreference]
(
	 @UserName varchar(100)
	,@LocaleID int
	,@LocaleRadius int
	,@ScannerSilent bit
	,@DisplayCoupons bit
	,@DisplayRebates bit
	,@DisplayLoyaltyRewards bit
	,@DisplayFieldAgent bit
	,@FieldAgentRadius int
	,@SavingsActivated bit
	,@SleepStatus bit
	,@SleepActivationDate datetime
	,@DateModified datetime
	
	--Output Variable
	,@Result int OUTPUT

)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To get userid from Username
			DECLARE @UserID int
			SELECT @UserID = UserID FROM [Users] WHERE Email = @UserName 
			--To check the existance of the user preferred info.
			IF EXISTS (SELECT 1 FROM UserPreference WHERE UserID = @UserID AND LocaleID = @LocaleID)
			--If info exists, modify the info.
			BEGIN
				UPDATE UserPreference
				SET LocaleRadius = @LocaleRadius
					,ScannerSilent = @ScannerSilent
					,DisplayCoupons = @DisplayCoupons
					,DisplayRebates = @DisplayRebates
					,DisplayLoyaltyRewards =	@DisplayLoyaltyRewards
					,DisplayFieldAgent = @DisplayFieldAgent
					,FieldAgentRadius = @FieldAgentRadius
					,SavingsActivated = @SavingsActivated
					,SleepStatus = @SleepStatus
					,SleepActivationDate = @SleepActivationDate
					,DateModified = @DateModified
				WHERE UserID = @UserID
					AND LocaleID = @LocaleID
			END
			--If info not exists, add the info.
			ELSE
			BEGIN
				INSERT INTO UserPreference
					(UserID
					,LocaleID
					,LocaleRadius
					,ScannerSilent
					,DisplayCoupons
					,DisplayRebates
					,DisplayLoyaltyRewards
					,DisplayFieldAgent
					,FieldAgentRadius
					,SavingsActivated
					,SleepStatus
					,SleepActivationDate
					,DateModified)
				VALUES
					(@UserID
					,@LocaleID
					,@LocaleRadius
					,@ScannerSilent
					,@DisplayCoupons
					,@DisplayRebates
					,@DisplayLoyaltyRewards
					,@DisplayFieldAgent
					,@FieldAgentRadius
					,@SavingsActivated
					,@SleepStatus
					,@SleepActivationDate
					,@DateModified)
			END					 
					
			SELECT @Result = 0 -- To indicate successfull execution.
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ModifyUserPreference.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			SELECT @Result = 1 --To indicate unsuccessfull execution.
			ROLLBACK TRANSACTION;
		END;
		 
	END CATCH;
END;

GO
