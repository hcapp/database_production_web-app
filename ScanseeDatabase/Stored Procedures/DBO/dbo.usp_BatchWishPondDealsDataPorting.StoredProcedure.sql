USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondDealsDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondDealsDataPorting
Purpose					: To port data into RetailLocationDeal
Example					: usp_BatchWishPondDealsDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			12th Oct 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondDealsDataPorting]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @APIPartnerID INT = (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName LIKE 'wishpond')
			
			--To clear old wishpond deals
			
			DELETE FROM RetailLocationDeal WHERE APIPartnerID = @APIPartnerID
			
			-- To move data to RetailLocationDeal table.
			INSERT INTO [RetailLocationDeal]
					   ([RetailLocationID]
					   ,[ProductID]
					   ,[RetailLocationProductDescription]
					   ,[SalePrice]
					   ,[ThumbNailProductImagePath]
					   ,[LargeProductImagePath]
					   ,[DateCreated]
					   , [APIPartnerID])
			SELECT WR.ScanSeeRetailLocID ScanSeeRetailLocID
				  , P.ProductID ProductID
				  , WP.Description Description
				  , WP.SalePrice SalePrice
				  , WD.WishPondThumbNailImageURL 
				  , WD.WishPondLargeImageURL
				  , GETDATE() DateCreated
				  , @APIPartnerID
			FROM APIWishPondProductData WP
				INNER JOIN APIWishpondLocationData WL ON WL.Id = WP.APIWishpondLocId 
				INNER JOIN dbo.APIWishPondDealXRef WD ON WD.WishPondDealID = WP.DealId and WD.WishPondItemID = WP.ItemID and WL.Id = WD.WishPondRetailerID
				INNER JOIN Product P ON P.ProductID = WD.ScanSeeProductID
				INNER JOIN dbo.APIWishPondRetailerXRef WR ON WR.WishPondRetailerID = WD.WishPondRetailerID
			WHERE WR.ScanSeeRetailLocID IS NOT NULL
					AND WD.ScanSeeProductID IS NOT NULL

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondDealsDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
