USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayHotDealDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebDisplayHotDeal
Purpose					: To the detailes of the input Hot Deal ID.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			4th January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayHotDealDetails]				
(

	--Input Input Parameter(s)--
	
	  @HotDealID int
	
	--Output Variable--	  

	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY 
	
	--To get Media Server Configuration.  
	    DECLARE @ManufacturerConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)   
		
		SELECT @ManufacturerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		
		
		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'		
		
		--Display the Details of the input Hot Deal.
		SELECT DISTINCT PHD.ProductHotDealID as productHotDealID
		     , PHD.HotDealName as hotDealName
		     , PHD.HotDealShortDescription as hotDealShortDesc
		     , PHD.HotDeaLonglDescription as hotDealLongDesc
		     , HotDealImagePath = CASE WHEN PHD.HotDealImagePath IS NOT NULL THEN 
								CASE WHEN PHD.WebsiteSourceFlag = 1 THEN 
									CASE WHEN PHD.ManufacturerID IS NOT NULL THEN @ManufacturerConfig +CONVERT(VARCHAR(30),PHD.ManufacturerID)+'/'+ HotDealImagePath
										  ELSE @RetailerConfig +CONVERT(VARCHAR(30),PHD.RetailID)+'/' +  HotDealImagePath
									   END

 									ELSE PHD.HotDealImagePath
								END
					        END
		     , P.ProductName
		     , M.ManufName
		     , P.Weight
		     , P.WeightUnits
		     , PHD.Price as price
		     , PHD.RetailID as retailID
		    
		    
		FROM ProductHotDeal PHD 
		LEFT OUTER JOIN HotDealProduct HDP ON PHD.ProductHotDealID = HDP.ProductHotDealID
		LEFT OUTER JOIN ProductHotDealLocation PHDL ON PHDL.ProductHotDealID = PHD.ProductHotDealID
		LEFT OUTER JOIN ProductHotDealRetailLocation PHDRL ON PHD.ProductHotDealID = PHDRL.ProductHotDealID
		LEFT OUTER JOIN Product P ON HDP.ProductID = P.ProductID
		LEFT OUTER JOIN Manufacturer M ON P.ManufacturerID = M.ManufacturerID
		WHERE PHD.ProductHotDealID = @HotDealID
		
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayHotDeal.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
	END CATCH	 
	
END;


GO
