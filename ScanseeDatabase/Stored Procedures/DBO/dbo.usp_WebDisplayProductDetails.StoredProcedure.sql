USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayProductDetails
Purpose					: To display the details of the selected product.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			12th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayProductDetails]
(

	--Input Input Parameter(s)--
	   
	  
	   @ProductID int
	 , @RetailLocationID int
	 
	 
	
	--Output Variable--
	  
	 , @STATUS INT OUTPUT
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration' 
		
		IF @RetailLocationID IS NOT NULL
		BEGIN
			SELECT DISTINCT P.ProductName
				 , P.ScanCode 
				 , RLP.Price
				 , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 
											THEN @Config +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductImagePath
																							ELSE ProductImagePath END
									  
				 , ProductMediaPath = CASE WHEN PM.ProductMediaPath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																						   THEN @Config
																						   +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																						   +PM.ProductMediaPath 
																						 ELSE @AppConfig+PM.ProductMediaPath 
																						 END   
										  ELSE PM.ProductMediaPath END 
				 , P.ProductShortDescription
				 , P.ProductLongDescription
				 , PA.AttributeName
				 , PA.DisplayValue
				 , P.WarrantyServiceInformation
			FROM Product P 
			INNER JOIN RetailLocationProduct RLP ON P.ProductID = RLP.ProductID
			AND P.ProductID = @ProductID
			AND RLP.RetailLocationID = @RetailLocationID
			LEFT JOIN ProductAttributes PA ON P.ProductID = PA.ProductID
			LEFT JOIN ProductMedia PM ON P.ProductID = PM.ProductID
		END
		ELSE
		BEGIN
			SELECT DISTINCT P.ProductName
					 , P.ScanCode 
					 , P.SuggestedRetailPrice
					 , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductImagePath
												ELSE ProductImagePath END
					 , ProductMediaPath = CASE WHEN ProductMediaPath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductMediaPath
												ELSE ProductMediaPath END
					 , P.ProductShortDescription
					 , P.ProductLongDescription
					 , PA.AttributeName
					 , PA.DisplayValue
					 , P.WarrantyServiceInformation
				FROM Product P 				
				LEFT JOIN ProductAttributes PA ON P.ProductID = PA.ProductID
				LEFT JOIN ProductMedia PM ON P.ProductID = PM.ProductID
				WHERE P.ProductID = @ProductID
		END
		
		
		SET @STATUS = 0
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
