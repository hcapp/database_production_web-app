USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCustomPageDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_QRRetailerCreatedPagesDetails
Purpose					: To Display the Retailer Created Page Details.
Example					: usp_QRRetailerCreatedPagesDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCustomPageDisplay]
(
	  
	  @UserID int
	, @RetailID int 
	, @LowerLimit int	 
    
	--Output Variable 
	, @RowCount int output	
	, @NxtPageFlag bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		DECLARE @Config INT
		DECLARE @UpperLimit int 
		DECLARE @MaxCnt INT 
		
		--Get the Pagination limit
		SELECT @UpperLimit = @LowerLimit + ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType = 'Website Pagination'
		AND Active = 1
		
		SELECT Row_Num = ROW_NUMBER() OVER (ORDER BY pageId DESC)
		     , pageId
			 , pageTitle
			 , qrType
			 , pageStartDate
			 , pageEndDate
		INTO #Temp
		FROM(
			SELECT TOP 100 PERCENT QRRCP.QRRetailerCustomPageID pageId
				 , QRRCP.Pagetitle pageTitle
				 , QRT.QRTypeName qrType
				 , QRRCP.StartDate pageStartDate
				 , QRRCP.EndDate pageEndDate
			FROM QRRetailerCustomPage QRRCP
			INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID
			WHERE QRRCP.RetailID = @RetailID
			ORDER BY QRRCP.DateCreated DESC)RetailerCreatedPages
		
		--Capture Total Number of Records.
		SELECT @MaxCnt = MAX(Row_Num) FROM #Temp
		SET @RowCount = @MaxCnt	
		
		--this flag is a indicator to enable "More" button in the UI. 
		--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
		SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 			
				
		SELECT Row_Num  rowNumber
			 , pageId
			 , pageTitle
			 , qrType
			 , pageStartDate
			 , pageEndDate
		FROM #Temp
		WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
