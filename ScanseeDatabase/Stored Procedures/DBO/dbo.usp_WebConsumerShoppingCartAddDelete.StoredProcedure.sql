USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerShoppingCartAddDelete]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerShoppingCartAddDelete
Purpose					: To Swap Product between Today Shopping list and Shopping Cart
Example					: usp_WebConsumerShoppingCartAddDelete

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th July 2011	Dhananjaya TR	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerShoppingCartAddDelete]
(
	  @UserID int
	, @UserProductID varchar(100)
	
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output

)
AS
BEGIN

	BEGIN TRY
		--Current Date.
		
		SET @UserProductID =REPLACE(@UserProductID,',','')
		DECLARE @Today datetime
		SET @Today = GETDATE()
		BEGIN TRANSACTION
			--To Swap product from Cart to Today List.
			UPDATE UserProduct 
			SET  ShopCartItem=CASE WHEN ShopCartItem = 0 THEN 1 ELSE 0 END
			   , TodayListtItem=CASE WHEN TodayListtItem = 1 THEN 1 ELSE 0 END
			   , ShopCartRemoveDate = CASE WHEN ShopCartItem = 1 THEN @Today ELSE ShopCartRemoveDate END
			   , TodayListRemoveDate =CASE WHEN TodayListtItem = 1 THEN @Today ELSE TodayListRemoveDate END
			   , ShopCartAddDate=CASE WHEN ShopCartItem = 0 THEN @Today ELSE ShopCartAddDate END
			   , TodayListAddDate = CASE WHEN TodayListtItem = 0 THEN @Today ELSE TodayListAddDate END 
			FROM UserProduct UP				
			WHERE UserID = @UserID AND UserProductID =@UserProductID 

			----To Swap product from Today List to Cart.
			--UPDATE UserProduct 
			--SET TodayListtItem = 0
			--	, TodayListRemoveDate = @Today 
			--	, ShopCartItem = 1
			--	, ShopCartAddDate = @Today 
			--FROM UserProduct UP
			--	INNER JOIN fn_SplitParam(@CartUserProductID, ',') P ON P.Param = UP.UserProductID 
			--WHERE UserID = @UserID 
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerShoppingCartAddDelete.'		
			--- Execute retrieval of Error info.
			EXEC dbo.[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
