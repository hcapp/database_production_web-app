USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetrieveRetailLocationSubCategory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetrieveRetailerLocation
Purpose					: To retrieve list of Retailer's RetailLocations business sub category.
Example					: usp_WebRetailerAdminRetrieveRetailerLocation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/03/2014			Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetrieveRetailLocationSubCategory]

	--Input parameters	 
	  @RetailID INT
	, @RetailLocationID INT	

	--Output variables 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
		SELECT DISTINCT A.HcBusinessSubCategoryID categoryId
			 , A.BusinessSubCategoryName categoryName
			 , Associated = CASE WHEN D.HcRetailerSubCategoryID IS NOT NULL THEN 1 ELSE 0 END
		FROM HcBusinessSubCategory A
		INNER JOIN HcBusinessSubCategoryType B ON A.HcBusinessSubCategoryTypeID = B.HcBusinessSubCategoryTypeID 
		INNER JOIN RetailerBusinessCategory C ON C.BusinessCategoryID = B.BusinessCategoryID AND C.RetailerID = @RetailID
		LEFT JOIN HcRetailerSubCategory D ON D.HcBusinessSubCategoryID = A.HcBusinessSubCategoryID AND D.RetailLocationID = @RetailLocationID
		ORDER BY A.BusinessSubCategoryName

		 
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetrieveRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
