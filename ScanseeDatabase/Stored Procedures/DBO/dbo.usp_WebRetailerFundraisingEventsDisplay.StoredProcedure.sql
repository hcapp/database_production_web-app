USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFundraisingEventsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerFundraisingEventsDisplay
Purpose					: To display list of Retailer Fundraising Events.
Example					: usp_WebRetailerFundraisingEventsDisplay

History
Version		   Date			Author		Change Description
--------------------------------------------------------------- 
1.0			11/11/2014	    SPAN		     1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerFundraisingEventsDisplay]
(   
    --Input variable.	  
	  @RetailID int
	, @UserID Int
	, @SearchKey Varchar(1000)
	, @LowerLimit int  
	
	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int

			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit +  ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = 'HubCitiWeb' 
			AND ConfigurationType = 'Pagination'
			AND Active = 1
		
	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To display List of Retailer Fundraising Events

			CREATE TABLE #FundraisingEvents(RowNum Int
											, FundraisingID Int	
											, FundraisingName Varchar(1000)							
											, FundraisingOrganizationName Varchar(1000)
											--, CategoryName Varchar(250)		
											, Address1 Varchar(1000)
											, City Varchar(100)
											, State Varchar(10)
											, PostalCode Varchar(10)																					
											, StartDate Date								
											, EndDate Date
											, StartTime Time
											, EndTime Time
											--, RetailLocationFlag Bit
											)								
								
				INSERT INTO #FundraisingEvents(RowNum 
											, FundraisingID	
											, FundraisingName							
											, FundraisingOrganizationName 
											--, CategoryName
											, Address1
											, City
											, State
											, PostalCode								
											, StartDate								
											, EndDate 
											, StartTime
											, EndTime 
											--, RetailLocationFlag
											)

								 SELECT RowNum 
										, HcFundraisingID	
										, FundraisingName							
										, FundraisingOrganizationName 
										--, FundraisingCategoryName
										, Address1
										, City
										, State
										, PostalCode								
										, StartDate								
										, EndDate 
										, StartTime
										, EndTime 
										--, FundraisingAppsiteFlag
								
							FROM(
							SELECT DISTINCT Rownum = ROW_NUMBER() OVER (ORDER BY E.HcFundraisingID) 
										   , E.HcFundraisingID
										   , FundraisingName 							   
										   , FundraisingOrganizationName 
										  -- , FC.FundraisingCategoryName	
										   , Address1
										   , City
										   , State
										   , PostalCode						   						  							
										   , StartDate =CAST(StartDate AS DATE)
										   , EndDate =CAST(EndDate AS DATE)
										   , StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
										   , EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
										  -- , FundraisingAppsiteFlag				  							  									  											
							FROM HcFundraising E 			
							INNER JOIN HcFundraisingCategoryAssociation EA ON E.HcFundraisingID = EA.HcFundraisingID 
							--INNER JOIN HcFundraisingCategory FC ON EA.HcFundraisingCategoryID = FC.HcFundraisingCategoryID
							LEFT JOIN HcRetailerFundraisingAssociation RF ON E.HcFundraisingID = RF.HcFundraisingID
							LEFT JOIN RetailLocation RL ON RF.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
							WHERE E.RetailID = @RetailID AND E.Active = 1 --AND GETDATE() < ISNULL(E.EndDate, GETDATE()+1)
							AND ((@SearchKey IS NULL AND 1=1) OR ( @SearchKey IS NOT NULL AND FundraisingName LIKE '%'+@SearchKey+'%') )				
							--AND E.CreatedUserID = @UserID 
							) E
		
		               

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #FundraisingEvents
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SELECT RowNum 
					, FundraisingID eventID
					, FundraisingName eventName
					, FundraisingOrganizationName organization 
					--, CategoryName eventCategoryName
					, Address1 address
					, City
					, State
					, PostalCode								
					, StartDate eventStartDate								
					, EndDate eventEndDate
					, StartTime 
					, EndTime 
					--, RetailLocationFlag								  											
			FROM #FundraisingEvents
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    --ORDER BY FundraisingName 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebHcHubCitiFundraisingEventsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;











GO
