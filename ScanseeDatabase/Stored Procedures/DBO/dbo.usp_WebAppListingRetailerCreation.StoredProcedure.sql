USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAppListingRetailerCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebAppListingRetailerCreation]
Purpose					: To register New Retailer. 
Example					: [usp_WebAppListingRetailerCreation]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			31st Jan 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAppListingRetailerCreation]
(

	 @RetailerName Varchar(100)
	,@Address1 Varchar(100)
	,@Address2 Varchar(50)
	,@State Char(2)
	,@City Varchar(50)
	,@PostalCode Varchar(10)
	,@CorporatePhoneNo Char(10)
	,@BusinessCategory varchar(1000)
	,@RetailerURL varchar(1000)
	,@RetailerKeyword Varchar(1000)
	,@RetailerLatitude Float
    ,@RetailerLongitude Float
    ,@ReferralName varchar(500)
    ,@AssociateOrganizations varchar(255)
	--Input Variables of RetailContact
	,@ContactFirstName Varchar(20)
	,@ContactLastName Varchar(30)
	,@ContactPhone Char(10)
	
	--Input Variables of Users--
	,@UserName varchar(100)
	,@Password varchar(60)
	,@ContactEmail Varchar(100)

	--Input Variables of UserRetailer--
	,@AdminFlag Bit	
	,@CorporateAndSore Bit
	,@StoreIdentification Varchar(20)
	
					
	--Output Variable 
	, @ResponseRetailID int output
	, @ResponseUserID int output
	, @DuplicateFlag bit output
	, @DuplicateRetailerFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
		IF NOT EXISTS(SELECT 1 FROM Retailer WHERE RetailName = @RetailerName AND RetailerActive = 1)
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName)
			BEGIN
			
		    --INSERT INTO USERS TABLE
				INSERT INTO Users
							(UserName
							,Password
							,DateCreated)
					VALUES (@UserName
						   ,@Password
						   ,GETDATE())				
					
			DECLARE @UserID int
			SET @UserID=SCOPE_IDENTITY();
		
			--INSERT INTO RETAILER TABLE
		 
			INSERT INTO [Retailer]
					   ([RetailName]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[CountryID]
					   ,[RetailURL]
					   ,[CreateUserID]
					   ,[DateCreated]
					   ,[CorporatePhoneNo]
					   , WebsiteSourceFlag
					   , FreeApplistingReferralName
					   , FreeApplistingRetailer
					   , AssociateOrganizations
					   )
			VALUES( @RetailerName
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   ,1
				   ,@RetailerURL
				   ,@UserID
				   ,GETDATE()
				   ,@CorporatePhoneNo
				   , 1
				   , @ReferralName
				   , 1
				   , @AssociateOrganizations)			   
				   
				   
			DECLARE @RetailID int
			SET @RetailID=SCOPE_IDENTITY(); 
			
			--INSERT INTO RetailCategoty TABLE
		   
		   INSERT INTO RetailerBusinessCategory(RetailerID
		                            , BusinessCategoryID
		                            , DateCreated)
		                       SELECT @RetailID
		                            , [Param]
		                            , GETDATE()
		                            
		                       FROM dbo.fn_SplitParam(@BusinessCategory, ',')
		                      
			--INSERT INTO RETAILLOCATION TABLE
			
			INSERT INTO [RetailLocation]
					   ([RetailID]
					   ,[Headquarters]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[RetailLocationURL]
					   ,[RetailLocationLatitude]
					   ,[RetailLocationLongitude]
					   ,[CountryID]
					   ,[CorporateAndStore]
					   ,[StoreIdentification]
					   ,[DateCreated])  
			VALUES(@RetailID
				   ,CASE WHEN @CorporateAndSore = 1 THEN 0 ELSE 1 END
				   --,0
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   , CASE WHEN @CorporateAndSore = 1 THEN @RetailerURL ELSE NULL END
				   ,@RetailerLatitude 
                   ,@RetailerLongitude 
                   ,1
				   ,@CorporateAndSore	
				   ,@StoreIdentification			   				  
				   ,GETDATE())
				   
			DECLARE @RetailLocationID int
			SET @RetailLocationID=SCOPE_IDENTITY();

			--To update City state changes
			update RetailLocation 
			set HccityID = C.HccityID
			FROM RetailLocation R
			inner join HcCity C on R.City = C.CityName
			where Retaillocationid = @RetailLocationID

            update RetailLocation 
			set StateID = C.StateID
			FROM RetailLocation R
			inner join state C on R.State = C.Stateabbrevation
			where Retaillocationid = @RetailLocationID


			--To update cosine & sine values
			update RetailLocation 
			set	  SINRetailLocationLatitude = SIN(RetailLocationLatitude / 57.2958)
				, COSRetailLocationLatitude = COS(RetailLocationLatitude / 57.2958)
				, COSRetailLocationLongitude = COS(RetailLocationLongitude / 57.2958)
			where Retaillocationid = @RetailLocationID
			
			--INSERT INTO CONTACT TABLE
							   
			INSERT INTO [Contact]
					   ([ContactFirstName]
					   ,[ContactLastname]
					   ,[ContactTitle]					  
					   ,[ContactPhone]
					   ,[ContactEmail]
					   ,[DateCreated]
					   ,[CreateUserID])
			VALUES(@ContactFirstName
				   ,@ContactLastName
				   ,1				 
				   ,@ContactPhone
				   ,@ContactEmail
				   ,GETDATE()
				   ,@UserID)
				   
			DECLARE @ContactID int
			SET @ContactID=SCOPE_IDENTITY();
				   
			--INSERT INTO RETAILCONTACT TABLE				   
			INSERT INTO [RetailContact]
					   ([ContactID]
					   ,[RetailLocationID])
			VALUES(@ContactID
				   ,@RetailLocationID)
				   
			--INSERT INTO USERRETAILER CHILD TABLE				   
			INSERT INTO UserRetailer
					   ([RetailID]
					   ,[UserID]
					   ,[AdminFlag])
			VALUES(@RetailID
				   ,@UserID
				   ,@AdminFlag)
										   
         
           --INSERT INTO RETAILERKEYWORDS TABLE
           INSERT INTO RetailerKeywords 
				   (RetailID
				   , RetailLocationID
				    ,RetailKeyword
					,DateCreated)
		   VALUES(@RetailID
				  ,@RetailLocationID
		          ,@RetailerKeyword 
		          ,GETDATE())       
		          			
         		
			SET @Status = 0					--Confirmation of Success.		
			SET @DuplicateFlag = 0	
			SET @DuplicateRetailerFlag = 0		--CONFIRM NO DUPLICATION
			SET @ResponseRetailID = @RetailID 
			SET @ResponseUserID= @UserID
		END
		ELSE
		BEGIN
			SET @Status = 0					--Confirmation of Success.
			SET @DuplicateFlag = 1	
			SET @DuplicateRetailerFlag = 0		--CONFIRM DUPLICATION
			SET @ResponseRetailID = @RetailID 
			SET @ResponseUserID= @UserID

		END		
	   END
	   ELSE
	   BEGIN
		PRINT 'Retailer Name exists in the system'
		SET @Status = 0		
		SET @DuplicateFlag = 0
		SET @DuplicateRetailerFlag = 1
	   END
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebAppListingRetailerCreation].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
