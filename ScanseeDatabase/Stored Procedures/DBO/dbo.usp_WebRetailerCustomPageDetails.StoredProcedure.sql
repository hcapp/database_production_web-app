USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCustomPageDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCustomPageDetails
Purpose					: To Display the Retailer Created Page Details.
Example					: usp_WebRetailerCustomPageDetails

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCustomPageDetails]
(
	  
	  @UserID int
	, @RetailID int  
	, @PageID int  
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		 DECLARE @RetailerConfig varchar(50)
		 DECLARE @MediaTypes VARCHAR(1000) = ''
		 DECLARE @MediaPath VARCHAR(MAX) = ''
		 DECLARE @ExternalFlag varchar(100) = ''
		 DECLARE @RetailLocations VARCHAR(1000) = ''
		 DECLARE @FileNames VARCHAR(1000) = ''
		
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration'
		 
		 --Get Associated Media Details
		 SELECT  @MediaTypes = COALESCE(@MediaTypes+',','') + CAST(PMT.ProductMediaType AS VARCHAR(10))
			   , @MediaPath = COALESCE(@MediaPath+',','') + CASE WHEN ExternalFlag = 0 THEN  @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + CAST(QR.MediaPath AS VARCHAR(100))
					ELSE CAST(MediaPath AS VARCHAR(100)) END  
			   , @ExternalFlag = COALESCE(@ExternalFlag+',','') + CAST(ExternalFlag AS VARCHAR(10))
		 FROM QRRetailerCustomPageMedia QR
		 INNER JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = QR.MediaTypeID
		 WHERE QRRetailerCustomPageID = @PageID
		 
		 --Get Associated Files.
		 SELECT @FileNames = COALESCE(@FileNames+',','')+CAST(QR.MediaPath AS VARCHAR(1000))
		 FROM QRRetailerCustomPageMedia QR
		 INNER JOIN ProductMediaType PMT ON PMT.ProductMediaTypeID = QR.MediaTypeID
		 WHERE QRRetailerCustomPageID = @PageID
		 AND ExternalFlag = 0
		 
		 --Get Associated RetailLocations Details.
		 SELECT @RetailLocations = COALESCE(@RetailLocations + ',','') + CAST(RetailLocationID AS VARCHAR(1000))
		 FROM QRRetailerCustomPageAssociation
		 WHERE QRRetailerCustomPageID = @PageID
		 
		--Get Page Details 
		SELECT DISTINCT QRRCP.QRRetailerCustomPageID pageID
			 , QRRCP.Pagetitle
			 , QRRCP.PageDescription
			 , QRRCP.ShortDescription
			 , QRRCP.LongDescription
			 , QRRCP.Image ImageName
			 , @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + QRRCP.Image	ImagePath
			 , SUBSTRING(@MediaTypes, 2, LEN(@MediaTypes)) MediaType
			 , SUBSTRING(@MediaPath, 2, LEN(@MediaPath)) MediaPath	
			 , SUBSTRING(@ExternalFlag, 2, LEN(@ExternalFlag)) ExternalFlag 
			 , SUBSTRING(@FileNames, 2, LEN(@FileNames)) [FileName]
			 , QRRCP.RetailID
			 , SUBSTRING(@RetailLocations, 2, LEN(@RetailLocations)) RetailLocationID
			 , CAST(QRRCP.StartDate AS DATE) StartDate
			 , CONVERT(VARCHAR(5),CAST(QRRCP.StartDate AS TIME)) StartTime
			 , CAST(QRRCP.EndDate AS DATE) EndDate
			 , CONVERT(VARCHAR(5), CAST(QRRCP.EndDate AS TIME)) EndTime			 
		FROM QRRetailerCustomPage QRRCP
		INNER JOIN QRTypes QRT ON QRRCP.QRTypeID = QRT.QRTypeID		
		WHERE QRRCP.RetailID = @RetailID
		AND QRRCP.QRRetailerCustomPageID = @PageID
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
		SELECT ERROR_LINE()
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
