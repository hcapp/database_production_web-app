USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebRetailerBannerAdsDisplay]
Purpose                  : To display the Banner ads created by the given Retailer.
Example                  : [usp_WebRetailerBannerAdsDisplay]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               31st July 2012      Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdsDisplay]
(

      --Input Input Parameter(s)--  
      
        @RetailID INT
      , @SearchParameter VARCHAR(1000)
      , @LowerLimit INT
      , @ShowAll BIT --To include expired Ads
      --Output Variable--
	  , @RowCount INT OUTPUT
	  , @NextPageFlag BIT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
			DECLARE @UpperLimit INT    
		    DECLARE @MaxCnt INT  
      
      
			  --To get the row count for pagination.      
			 DECLARE @ScreenContent Varchar(100)    
			 SELECT @ScreenContent = ScreenContent       
			 FROM AppConfiguration       
			 WHERE ScreenName = 'All'     
			 AND ConfigurationType = 'Website Pagination'    
			 AND Active = 1   
		 
		    SET @UpperLimit = @LowerLimit + @ScreenContent   
		    
		    --To Fetch the Retailer Media server Configuaration      
			DECLARE @Config varchar(255)    
			SELECT @Config=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration' AND Active =1  		
			
			DECLARE @Temp TABLE(RowNum INT IDENTITY(1, 1)
								   , retailLocationAdvertisementID INT
								   , advertisementName VARCHAR(255)
								   , strBannerAdImagePath VARCHAR(1000)
								   , advertisementDate DATE
								   , advertisementEndDate DATE
								   , DeleteFlag BIT
								   , ExpireFlag BIT)
			
			
			  --Select the ads that are currently running & those that start in future. This will be common in both the events where @ShowAll is 0 or 1.
			   INSERT INTO @Temp(retailLocationAdvertisementID  
							   , advertisementName  
							   , strBannerAdImagePath 
							   , advertisementDate  
							   , advertisementEndDate  
							   , DeleteFlag  
							   , ExpireFlag)
				
			   SELECT DISTINCT AB.AdvertisementBannerID as retailLocationAdvertisementID
						      ,AB.BannerAdName as advertisementName 
						      ,@Config + CONVERT(VARCHAR(30),RL.RetailID) +'/' + BannerAdImagePath AS strBannerAdImagePath 
						      ,advertisementDate = AB.StartDate   
						      ,advertisementEndDate = AB.EndDate 						 
						      ,DeleteFlag = CASE WHEN AB.StartDate > GETDATE() THEN 1 ELSE 0 END
						      ,ExpireFlag = CASE WHEN (CONVERT(DATE, GETDATE())>=CONVERT(DATE, AB.StartDate) AND CONVERT(DATE, GETDATE())<=CONVERT(DATE, ISNULL(AB.EndDate, GETDATE() + 1))) THEN 1 ELSE 0 END																									      																								      
					FROM AdvertisementBanner  AB 
					INNER JOIN RetailLocationBannerAd  RLB ON AB.AdvertisementBannerID = RLB.AdvertisementBannerID  
					INNER JOIN RetailLocation RL ON RLB.RetailLocationID = RL.RetailLocationID 
					WHERE RL.RetailID = @RetailID AND RL.Active = 1
					AND GETDATE() <= ISNULL(AB.EndDate, GETDATE() + 1)
					AND AB.BannerAdName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END
					
				--Select the expired Ads when @ShowAll is 1
				IF (@ShowAll = 1)
				BEGIN
					
					 INSERT INTO @Temp(retailLocationAdvertisementID  
								   , advertisementName  
								   , strBannerAdImagePath 
								   , advertisementDate  
								   , advertisementEndDate  
								   , DeleteFlag  
								   , ExpireFlag)
					SELECT DISTINCT AB.AdvertisementBannerID as retailLocationAdvertisementID
					  ,AB.BannerAdName as advertisementName 
					  ,@Config + CONVERT(VARCHAR(30),RL.RetailID) +'/' + BannerAdImagePath AS strBannerAdImagePath 
					  ,advertisementDate = CASE WHEN AB.StartDate = '01/01/1900' THEN (SELECT StartDate 
																					   FROM AdvertisementBannerHistory 
																					   WHERE AdvertisementBannerID = AB.AdvertisementBannerID
																                       AND DateCreated = (SELECT MAX(DateCreated) 
																										  FROM AdvertisementBannerHistory
																									      WHERE AdvertisementBannerID = AB.AdvertisementBannerID)) ELSE AB.StartDate END  
					  , advertisementEndDate = CASE WHEN AB.EndDate = '01/01/1900' THEN (SELECT EndDate 
																					   FROM AdvertisementBannerHistory 
																					   WHERE AdvertisementBannerID = AB.AdvertisementBannerID
																                       AND DateCreated = (SELECT MAX(DateCreated) 
																										  FROM AdvertisementBannerHistory
																									      WHERE AdvertisementBannerID = AB.AdvertisementBannerID)) ELSE AB.EndDate END  
					 
				     , DeleteFlag = CASE WHEN AB.StartDate > GETDATE() THEN 1 ELSE 0 END
				     , ExpireFlag = CASE WHEN (CONVERT(DATE, GETDATE())>=CONVERT(DATE, AB.StartDate) AND CONVERT(DATE, GETDATE())<=CONVERT(DATE, ISNULL(AB.EndDate, GETDATE() + 1))) THEN 1 ELSE 0 END																									      																								      
				FROM AdvertisementBanner  AB 
				INNER JOIN RetailLocationBannerAd  RLB ON AB.AdvertisementBannerID = RLB.AdvertisementBannerID  
				INNER JOIN RetailLocation RL ON RLB.RetailLocationID = RL.RetailLocationID 
				WHERE RL.RetailID = @RetailID AND RL.Active = 1
				AND AB.BannerAdName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END
				AND GETDATE() > AB.EndDate
					
			END
					
			SELECT @MaxCnt = COUNT(RowNum) FROM @Temp 
		--Output Total no of Records
		SET @RowCount = @MaxCnt 
		
		--CHECK IF THERE ARE SOME MORE ROWS        
		SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  
		
		--Display the records in the given limit
		    SELECT RowNum 
		         , retailLocationAdvertisementID 
				 , advertisementName  
				 , strBannerAdImagePath  
				 , advertisementDate  
				 , advertisementEndDate
				 , DeleteFlag
				 , ExpireFlag
		    FROM @Temp	
		    WHERE RowNum BETWEEN (@LowerLimit + 1) AND (@UpperLimit)              
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdsDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;




GO
