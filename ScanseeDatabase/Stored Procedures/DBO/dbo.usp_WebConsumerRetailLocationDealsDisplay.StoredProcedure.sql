USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerRetailLocationDealsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebConsumerRetailLocationDealsDisplay
Purpose					: To get the list of Hotdeals available.
Example					: usp_WebConsumerRetailLocationDealsDisplay

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.4			30th May 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerRetailLocationDealsDisplay]
(
	  @UserID INT
	, @RetailID INT
	, @RetailLocationID INT
	, @LowerLimit int
	, @ScreenName varchar(50)
	, @RecordCount INT
	
    --User Tracking inputs.
    , @MainMenuID int
    , @RetailerListID int	
	
	--OutPut Variable	
	, @MaxCnt int output 
	, @NxtPageFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN
	
	BEGIN TRY
	
		BEGIN TRANSACTION
	
			 --To get Server Configuration
			 DECLARE @ManufConfig varchar(50) 
			 DECLARE @RetailerConfig varchar(50) 
			 DECLARE @MediaTypeID Int
			 
			 SELECT @MediaTypeID=ProductMediaTypeID 
			 FROM ProductMediaType 
			 WHERE ProductMediaType ='Image Files'
			   
			 SELECT @ManufConfig=ScreenContent 				   
			 FROM AppConfiguration  
			 WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
			--To get the row count for pagination.
			 DECLARE @UpperLimit int 
			 SELECT @UpperLimit = @LowerLimit + @RecordCount		    
		      
			  SELECT DISTINCT P.ProductID 
					 ,P.ProductName 
					 ,p.ProductShortDescription 
					 ,P.ProductLongDescription 
					 ,RLD.Price 
					 ,RLD.SalePrice 
					 ,imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																					 THEN @ManufConfig+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																						+ProductImagePath ELSE ProductImagePath 
																					  END 
										 ELSE CASE WHEN WebsiteSourceFlag=1 
										           THEN @ManufConfig+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+PM.ProductMediaPath
												   ELSE ProductImagePath END
					                END
					  ,RLD.RetailLocationDealID             	      
			  INTO #Deals
			  FROM RetailLocationDeal RLD
			  INNER JOIN Product P ON P.ProductID=RLD.ProductID 
			  INNER JOIN RetailLocation RL ON RL.RetailLocationID=RLD.RetailLocationID 
			  LEFT JOIN ProductMedia PM ON PM.ProductID =P.ProductID AND PM.ProductMediaTypeID =@MediaTypeID 	
			  Where RL.RetailID = @RetailID 
			  AND RL.RetailLocationID = @RetailLocationID 
			  AND GETDATE() Between ISNULL(RLD.SaleStartDate,GETDATE()-1) AND ISNULL(RLD.SaleEndDate,GETDATE()+1) 
			 -- Group BY P.ProductID,ProductName,ProductShortDescription,ProductLongDescription,Price,SalePrice,RetailLocationDealID
			
			
			  Select Row_Num= IDENTITY(int, 1,1)
					 ,ProductID 
					 ,ProductName 
					 ,ProductShortDescription 
					 ,ProductLongDescription 
					 ,Price 
					 ,SalePrice 
					 ,imagePath  
					 ,RetailLocationDealID           	      
			  INTO #productDeals1
			  FROM #Deals
			 -- WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
			  --ORDER BY Row_Num
			  
			
			---imagePath
			  --To capture max row number.
			  SELECT @MaxCnt = MAX(Row_Num) FROM #productDeals1 
			
			  --this flag is a indicator to enable "More" button in the UI. 
			  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button 
			  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			   Select Row_Num
					 ,ProductID 
					 ,ProductName 
					 ,ProductShortDescription 
					 ,ProductLongDescription 
					 ,Price 
					 ,SalePrice 
					 ,imagePath  
					 ,RetailLocationDealID           	      
			  INTO #productDeals
			  FROM #productDeals1
			  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit	
			  ORDER BY Row_Num
			  
			  --User Tracking section.
			  
			  --Capture the impression of the deals.
			  CREATE TABLE #Temp(SaleListID int
							   , RetailLoactionDealID int)
			  
			  INSERT INTO ScanSeeReportingDatabase..SalesList(RetailerListID
															 ,RetailLoactionDealID
															 ,RetailLocationDealClick
															 ,CreatedDate
															 ,MainMenuID
															 ,SalePrice)
																										
				OUTPUT inserted.SaleListID , inserted.RetailLoactionDealID INTO #Temp(SaleListID, RetailLoactionDealID)
										
				SELECT @RetailerListID 
				      ,RetailLocationDealID
					  , 0
					  , GETDATE()
					  ,@MainMenuID 
					  ,SalePrice 
				FROM #productDeals 		
			
			 --Display the hot deal list along with the primary key of the tracking table.
			Select DIstinct Row_Num
			         ,T.SaleListID  
					 ,ProductID productId
					 ,ProductName 
					 ,ProductShortDescription 
					 ,ProductLongDescription 
					 ,Price 
					 ,SalePrice 
					 ,imagePath  					        	      
			  FROM #productDeals P
			  INNER JOIN #Temp T ON P.RetailLocationDealID =T.RetailLoactionDealID 
			 
			--Confirmation of Success.
			SELECT @Status = 0										  
		COMMIT TRANSACTION		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerRetailLocationDealsDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION; 
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
