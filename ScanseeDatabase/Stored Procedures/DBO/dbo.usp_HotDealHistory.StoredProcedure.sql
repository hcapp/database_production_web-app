USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_HotDealHistory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_HotDealHistory
Purpose					: To move expired Hotdeals from Hotdeal table to HotDeal History Table.
Example					: usp_HotDealHistory 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_HotDealHistory]

AS
BEGIN

	BEGIN TRY
		DECLARE @Date datetime
		SET @Date = GETDATE() - 1
		BEGIN TRANSACTION
				--To fetch expiring Hot Deals.
				SELECT [ProductHotDealID]
				 -- ,[ProductID]
				  ,[RetailID]
				  ,[APIPartnerID]
				  ,[Price]
				  ,[HotDealDiscountType]
				  ,[HotDealDiscountAmount]
				  ,[HotDealDiscountPct]
				  ,[SalePrice]
				  ,[HotDealShortDescription]
				  ,[HotDeaLonglDescription]
				  ,[HotDealImagePath]
				  ,[HotDealTermsConditions]
				  ,[HotDealURL]
				  ,[Expired]
				  ,[HotDealStartDate]
				  ,[HotDealEndDate]
				  ,[HotDealName]
				INTO #ProductHotDeal
				FROM [ProductHotDeal]
				WHERE HotDealEndDate <= @Date 
				
				--To capture in Hot Deal history table.
				INSERT INTO [ProductHotDealHistory]
				   ([ProductHotDealID]
				   --,[ProductID]
				   ,[RetailID]
				   ,[APIPartnerID]
				   ,[Price]
				   ,[HotDealDiscountType]
				   ,[HotDealDiscountAmount]
				   ,[HotDealDiscountPct]
				   ,[SalePrice]
				   ,[HotDealShortDescription]
				   ,[HotDeaLonglDescription]
				   ,[HotDealImagePath]
				   ,[HotDealTermsConditions]
				   ,[HotDealURL]
				   ,[Expired]
				   ,[HotDealStartDate]
				   ,[HotDealEndDate]
				   ,[HotDealName])
				SELECT [ProductHotDealID]
				 -- ,[ProductID]
				  ,[RetailID]
				  ,[APIPartnerID]
				  ,[Price]
				  ,[HotDealDiscountType]
				  ,[HotDealDiscountAmount]
				  ,[HotDealDiscountPct]
				  ,[SalePrice]
				  ,[HotDealShortDescription]
				  ,[HotDeaLonglDescription]
				  ,[HotDealImagePath]
				  ,[HotDealTermsConditions]
				  ,[HotDealURL]
				  ,[Expired]
				  ,[HotDealStartDate]
				  ,[HotDealEndDate]
				  ,[HotDealName]
				FROM #ProductHotDeal 
				
				----To delete from Hot Deals table.
				--DELETE FROM ProductHotDeal 
				--WHERE ProductHotDealID IN (SELECT ProductHotDealID FROM #ProductHotDeal)
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_HotDealHistory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfo]
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
		END;
		 
	END CATCH;
END;

GO
