USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandAdminRetrieveband]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebbandAdminRetrieveband
Purpose					: To retrieve list of bands from the system.
Example					: usp_WebbandAdminRetrieveband

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			26th April 2016		Prakash C		Initial Version
---------------------------------------------------------------
*/

--exec [dbo].[usp_WebBandAdminRetrieveband] null,null,null,0,null,null,null,null,null,null

CREATE PROCEDURE [dbo].[usp_WebBandAdminRetrieveband]

	--Input parameters	 
	  @RetailName VARCHAR(100)
	, @City VARCHAR(50)
	, @State CHAR(2)
	, @LowerLimit int  
    , @ScreenName varchar(50)

	--Output variables
	, @MaxCnt INT OUTPUT
	, @NxtPageFlag BIT OUTPUT 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
	  BEGIN TRANSACTION		
		
		--To get the row count for pagination.  
		DECLARE @UpperLimit int   
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
		FROM AppConfiguration   
		WHERE ScreenName = 'bandAdmin' 
		AND ConfigurationType = 'Pagination'
		AND Active = 1
		
		IF @City =''
		BEGIN
		SET @City=NULL
		END
		
		IF @State  =''
		BEGIN
		SET @State =NULL
		END
		
		IF @RetailName  =''
		BEGIN
		SET @RetailName=NULL
		END
		
		CREATE TABLE #bandList1(bandID INT
							      ,bandName VARCHAR(500)
							      ,NumberOfLocations INT
							      ,Address1 VARCHAR(500)
							      ,[State] CHAR(2)							      
							      ,City VARCHAR(50)
							      ,PostalCode VARCHAR(10)
								  ,bandURL VARCHAR(1000)
								  ,Latitude FLOAT
								  ,Longitude FLOAT
								  ,CorporatePhoneNo VARCHAR(10)
								  ,UserLoginFlag Bit
								  ,IsPaid Bit)
		
		IF (@RetailName IS NULL AND @City IS NULL AND @State IS NULL) 
			BEGIN 
				INSERT INTO #bandList1(bandID
										,bandName							      
										,NumberOfLocations
										,Address1
										,State							      
										,City
										,PostalCode
										,bandURL
										,Latitude
										,Longitude
										,CorporatePhoneNo
										,UserLoginFlag
										,IsPaid)
				SELECT	DISTINCT	R.bandID     
						   ,bandName
						   ,NumberOfLocations = (SELECT COUNT(1) FROM bandLocation RL1 WHERE R.bandID = RL1.bandID AND Headquarters = 0 AND Active = 1)
						   ,R.Address1
						   ,R.[State]
						   ,R.City	
						   ,R.PostalCode	
						   ,R.BandURL
						   ,bandLocationLatitude
						   ,bandLocationLongitude
						   ,R.CorporatePhoneNo	
						   ,UserLoginFlag=IIF(UR.bandID IS NOT NULL,1,0)	
						   ,IsPaid				   					
				FROM band R
				LEFT JOIN Userband UR ON UR.bandID =R.bandID 
				LEFT JOIN bandLocation RL ON R.bandID=RL.bandID 
				AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)	
				AND RL.Active = 1	
				LEFT JOIN bandBillingDetails RB ON R.bandID = RB.bandID			
				WHERE R.bandActive = 1 --AND RL.Active = 1
				GROUP BY R.bandID,bandName,NumberOfLocations,R.Address1,R.[State],R.City,R.PostalCode,R.BandURL,
				RL.bandLocationLatitude,RL.bandLocationLongitude,R.CorporatePhoneNo,RL.Headquarters, CorporateAndStore, RL.bandID	,UR.bandID			
				,RL.bandLocationID,IsPaid
				ORDER BY bandName	

				
			END		
		ELSE
			BEGIN		
				INSERT INTO #bandList1(bandID
										,bandName							      
										,NumberOfLocations
										,Address1
										,[State]
										,City
										,PostalCode
										,bandURL
										,Latitude
										,Longitude
										,CorporatePhoneNo
										,UserLoginFlag
										,IsPaid)										
					SELECT DISTINCT  R.bandID
						   ,bandName
						   ,NumberOfLocations = (SELECT COUNT(1) FROM bandLocation RL1 WHERE R.bandID = RL1.bandID AND Headquarters = 0 AND Active = 1)
						   ,R.Address1
						   ,R.[State]
						   ,R.City	
						   ,R.PostalCode
						   ,R.BandURL
						   ,bandLocationLatitude
						   ,bandLocationLongitude
						   ,R.CorporatePhoneNo
						   ,UserLoginFlag=IIF(UR.bandID IS NOT NULL ,1,0)
						   ,IsPaid							   						
				FROM band R
				LEFT JOIN bandLocation RL ON R.bandID=RL.bandID 
				AND (RL.Headquarters = 1 OR RL.CorporateAndStore = 1)
					AND RL.Active = 1
				LEFT JOIN Userband UR ON UR.bandID =R.bandID 
				LEFT JOIN bandBillingDetails RB ON R.bandID = RB.bandID
				WHERE R.bandActive = 1 AND ((@RetailName IS NOT NULL AND bandName LIKE '%'+@RetailName+'%' AND @State IS NOT NULL AND R.[State] = @State AND @City IS NOT NULL AND R.City = @City)
				OR (@RetailName IS NOT NULL AND bandName LIKE '%'+@RetailName+'%' AND @State IS NOT NULL AND R.[State] = @State AND @City IS NULL)
				OR (@RetailName IS NULL AND @State IS NOT NULL AND R.[State] = @State AND @City IS NULL)
				OR (@RetailName IS NOT NULL AND bandName LIKE '%'+@RetailName+'%' AND @State IS NULL AND @City IS NULL)
				OR (@RetailName IS NULL AND @State IS NOT NULL AND R.[State] = @State AND @City IS NOT NULL AND R.City = @City)) 
				GROUP BY R.bandID,bandName,R.Address1,R.[State],R.City,R.PostalCode,R.BandURL,RL.bandLocationLatitude,RL.bandLocationLongitude,R.CorporatePhoneNo,RL.Headquarters, CorporateAndStore, RL.bandID	,
				UR.bandID	
				,RL.bandLocationID,IsPaid
				ORDER BY bandName					
									
			END

			SELECT DISTINCT bandID	
						   ,bandName
						   ,NumberOfLocations
						   ,Address1
						   ,[State]
						   ,City
						   ,PostalCode
						   ,bandURL retUrl
						   ,Latitude latitude
						   ,Longitude longitude
						   ,CorporatePhoneNo phoneNo
						   ,UserLoginFlag isLoginExist	
						   ,IsPaid isPaid
			 INTO #LoginExist	  		   
			 FROM #bandList1								    
			 WHERE UserLoginFlag =1


			
			 DELETE FROM #bandList1
			 FROM #bandList1 L
			 INNER JOIN #LoginExist LE ON LE.bandName=L.bandName AND L.Address1 =LE.Address1  AND LE.City =L.City AND L.City IS NOT NULL AND LE.State =L.State AND LE.PostalCode =L.PostalCode AND L.bandID <>LE.bandID AND L.UserLoginFlag =0 

			
			 SELECT DISTINCT Row_Num=identity(int, 1,1)
						   ,bandID	
						   ,bandName
						   ,NumberOfLocations
						   ,Address1
						   ,[State]
						   ,City
						   ,PostalCode
						   ,bandURL retUrl
						   ,Latitude latitude
						   ,Longitude longitude
						   ,CorporatePhoneNo phoneNo
						   ,UserLoginFlag isLoginExist	
						   ,IsPaid isPaid
			 INTO #bandList	  		   
			 FROM #bandList1 L
			 ORDER BY bandName
			
					
		 --To capture max row number. 		 
		 SELECT @MaxCnt = MAX(Row_Num) FROM #bandList
 
		 --This flag is a indicator to enable "More" button in the UI.   
		 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 		 		

		 SELECT Row_Num row_Num
			   ,R.bandID	 as RetailID
			   ,bandName as  RetailName
			   ,NumberOfLocations
			   ,R.Address1
			   ,R.[State]
			   ,R.City
			   ,R.PostalCode
			   ,retUrl
			   ,Latitude latitude
			   ,Longitude longitude
			   ,phoneNo
			   ,isLoginExist	
			   ,IIF(isPaid IS NULL,0,isPaid) isPaid		  		   
		 FROM #bandList	R
		 --INNER JOIN bandLocation RL ON R.bandID = RL.bandID							    
		 WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit --AND RL.Active = 1
		 ORDER BY bandName 
		

		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebbandAdminRetrieveband.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
