USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserPostalcodeUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserPostalcodeUpdation
Purpose					: To update the user postal code when the Location Services is off and the user has not entered the Postal code at the time of							  Registration.
Example					: usp_UserPostalcodeUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10th May 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserPostalcodeUpdation]
(
	  @UserID int
	, @PostalCode varchar(20)
	
	--Output Variables
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
			
			UPDATE Users SET PostalCode = @PostalCode
			WHERE UserID = @UserID 			
		
		--Confirmation of Success.
		SET @Status = 0		
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure WebRetailerAdsInsertion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
