USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireLoyaltyDealsProductDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireLoyaltyDealsProductDataPorting
Purpose					: To move Products associated to Loyalty Deals from stage to production.
Example					: usp_BatchCellFireLoyaltyDealsProductDataPorting

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			23rd October 2012   Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireLoyaltyDealsProductDataPorting]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			--Generate the set of Deals that are there in the system.
			SELECT DISTINCT F.LoyaltyProgramID
						  , G.LoyaltyDealID
						  , G.CellFireCouponID
						  , D.UPC
			INTO #Temp
			FROM APICellFireCouponByUPC A
			INNER JOIN APICellFireMerchants B ON A.MerchantID = B.MerchantID
			INNER JOIN APICellFireOfferMerchants C ON A.MerchantID = C.MerchantID
			INNER JOIN APICellFireOfferUPCs D ON A.OfferID = D.OfferID 
			INNER JOIN Retailer E ON E.RetailName = B.MerchantName
			INNER JOIN LoyaltyProgram F ON F.RetailID = E.RetailID AND F.LoyaltyProgramName = B.CardName
			INNER JOIN LoyaltyDeal G ON G.LoyaltyProgramID = F.LoyaltyProgramID AND G.CellFireCouponID = A.CouponID
			WHERE (D.UPC NOT LIKE '' AND D.UPC IS NOT NULL) 
		     
		    --Check if there are any Loyalties
			IF ((SELECT COUNT(1) FROM #Temp)>1)
			BEGIN
				--Delete the existing Product associtions for the Loyalty Deal & make a fresh association.
				DELETE FROM LoyaltyDealProduct
				FROM LoyaltyDealProduct LP
				INNER JOIN LoyaltyDeal LD ON LD.LoyaltyDealID = LP.LoyaltyDealID				
				
				INSERT INTO LoyaltyDealProduct(LoyaltyDealID
											 , ProductID
											 , DateCreated)
										SELECT T.LoyaltyDealID
											 , P.ProductID
											 , GETDATE()
										FROM #Temp T
										INNER JOIN Product P ON T.UPC = P.ScanCode
				
				--Log the Product UPC & the associated deal info when the UPC is not present in the system.
				INSERT INTO APICellFireProductUPC(UPC
												, LoyaltyDealID
												, CellFireCouponID
												, DateCreated)
										SELECT T.UPC
											 , T.LoyaltyDealID
											 , T.CellFireCouponID
											 , GETDATE()
										FROM #Temp T
										LEFT JOIN Product P ON T.UPC = P.ScanCode
										WHERE P.ProductID IS NULL
			END		
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireLoyaltyDealsProductDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
