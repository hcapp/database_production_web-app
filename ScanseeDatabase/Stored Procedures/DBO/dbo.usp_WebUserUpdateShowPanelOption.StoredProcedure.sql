USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUserUpdateShowPanelOption]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUserUpdateShowPanelOption
Purpose					: To Update the Show Panel Option of the user.
Example					: usp_WebUserUpdateShowPanelOption

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			26 April 2012					Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUserUpdateShowPanelOption]
(

	--Input Parameter(s)--
	  @UserID int
	, @ShowPanel bit
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			   UPDATE Users SET ShowPanel = @ShowPanel
		       WHERE UserID = @UserID		
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUserUpdateShowPanelOption.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
