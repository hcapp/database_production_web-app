USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ThislocationCategoryList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ThislocationCategoryList
Purpose					: To fetch the categories with products that have deals/sale price/coupons/loyalty/rebate.
Example					: usp_ThislocationCategoryList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ThislocationCategoryList]
(
	  @RetailID int
	, @Retaillocationid int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			SELECT  RetailLocationID 
				  , ProductID 
			INTO #PROD
			FROM RetailLocationProduct 
			WHERE RetailLocationID = @Retaillocationid

			DECLARE @DiscountProduct TABLE (ProductId int)
			INSERT INTO @DiscountProduct
			SELECT RD.ProductID 
			FROM RetailLocationDeal RD 
			INNER JOIN #PROD P ON P.ProductID = RD.ProductID 
			WHERE RD.RetailLocationID = @RetailLocationID   
			AND GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
			
			INSERT INTO @DiscountProduct
			SELECT CP.ProductID 
			FROM Coupon C
			INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID
			inner join #PROD p on p.ProductID = CP.ProductID 
			WHERE C.RetailID = @RetailID   
				  AND GETDATE() BETWEEN C.CouponStartDate AND C.CouponExpireDate 

			INSERT INTO @DiscountProduct
			SELECT RP.ProductID 
			FROM Rebate R
			INNER JOIN RebateProduct RP on RP.RebateID = R.RebateID 
			INNER JOIN #PROD P ON P.ProductID = RP.ProductID 
			WHERE R.RetailID = @RetailID 
			AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate 


			INSERT INTO @DiscountProduct
			select L.ProductID 
			from LoyaltyDeal L 
			INNER JOIN #PROD P ON P.ProductID = L.ProductID 
			WHERE L.RetailLocationID = @Retaillocationid  
			AND GETDATE() BETWEEN L.LoyaltyDealStartDate AND L.LoyaltyDealExpireDate 


			INSERT INTO @DiscountProduct
			SELECT H.ProductID 
			from HotDealProduct h 
			INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = H.ProductHotDealID 
			INNER JOIN #PROD P ON P.ProductID = H.ProductID 
			WHERE PH.RetailID = @RetailID 
			AND GETDATE() BETWEEN PH.HotDealStartDate AND PH.HotDealEndDate 
			            
			SELECT DISTINCT ProductID 
			INTO #Product
			FROM @DiscountProduct P

			SELECT @RetailID retailID
				  , C.CategoryID categoryID
				  , C.ParentCategoryID parentCategoryID
				, C.ParentCategoryName parentCategoryName
				, C.ParentCategoryDescription  parentCategoryDescription
				, C.SubCategoryID subCategoryID
				, C.SubCategoryName subCategoryName
				, C.SubCategoryDescription  subCategoryDescription
			FROM #Product P
			INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductId 
			INNER JOIN Category C ON C.CategoryID = PC.CategoryID

		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ThislocationCategoryList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
