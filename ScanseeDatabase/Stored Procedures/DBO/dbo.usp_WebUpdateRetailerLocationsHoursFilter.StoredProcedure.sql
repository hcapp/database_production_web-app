USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUpdateRetailerLocationsHoursFilter]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebUpdateRetailerLocationsHoursFilter]
Purpose					: 
Example					: [usp_WebUpdateRetailerLocationsHoursFilter] 
..
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25/11/2016		Bindu T A		Hours - Filters Changes- Day Wise
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUpdateRetailerLocationsHoursFilter]
(
	  @RetailID int 
	, @RetailLocationID int
	, @StartTime VARCHAR(1000)
	, @EndTime VARCHAR(1000)
	, @StartTimeUTC VARCHAR(1000)
	, @EndTimeUTC VARCHAR(1000)
	, @TimeZone VARCHAR(1000)

	--Output Variable 
	, @Status bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 

)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			DECLARE @TimeZoneID INT

			SELECT @TimeZoneID = TimeZoneID FROM TimeZone WHERE TimeZone = @TimeZone

			CREATE TABLE #StartTime ( StartTimes TIME, Num INT IDENTITY(1,1) )
			CREATE TABLE #EndTime ( EndTimes TIME, Num INT IDENTITY(1,1) )
			CREATE TABLE #StartTimeUTC ( StartTimeUTCs TIME, Num INT IDENTITY(1,1) )
			CREATE TABLE #EndTimeUTC ( EndTimeUTCs TIME, Num INT IDENTITY(1,1) )

			INSERT INTO #StartTime (StartTimes )
						SELECT Param
						FROM fn_SplitParam (@StartTime,',')

			INSERT INTO #EndTime (EndTimes)
						SELECT Param
						FROM fn_SplitParam (@EndTime,',')

			INSERT INTO #StartTimeUTC (StartTimeUTCs)
						SELECT Param
						FROM fn_SplitParam (@StartTimeUTC,',')

			INSERT INTO #EndTimeUTC (EndTimeUTCs)
						SELECT Param
						FROM fn_SplitParam (@EndTimeUTC,',')

			IF EXISTS (SELECT 1 FROM HcHoursFilterTime WHERE RetailLocationID IN (SELECT RetailLocationID FROM RetailLocation WHERE RetailLocationID = @RetailLocationID AND RetailID = @RetailID AND Active = 1) )
			BEGIN

				UPDATE HcHoursFilterTime 
							SET StartTime = CASE WHEN S.StartTimes = '00:00' AND E.EndTimes = '00:00' THEN CONVERT(datetime,NULL,121)
												ELSE S.StartTimes END,
								EndTime = CASE WHEN E.EndTimes = '00:00' AND S.StartTimes = '00:00' THEN CONVERT(datetime,NULL,121)
													ELSE E.EndTimes END ,
								StartTimeUTC = CASE WHEN SU.StartTimeUTCs = '00:00' AND EU.EndTimeUTCs = '00:00' THEN CONVERT(datetime,NULL,121)
													ELSE SU.StartTimeUTCs END ,
								EndTimeUTC = CASE WHEN EU.EndTimeUTCs = '00:00' AND SU.StartTimeUTCs = '00:00' THEN CONVERT(datetime,NULL,121)
												ELSE EU.EndTimeUTCs END
							 FROM HcHoursFilterTime H
							 INNER JOIN #StartTime S ON H.HcDaysOfWeekID = S.Num AND H.RetailLocationID= @RetailLocationID 
							 INNER JOIN #EndTime E ON S.Num= E.Num
							 INNER JOIN #StartTimeUTC SU ON SU.Num = S.Num AND SU.Num = S.Num
							 INNER JOIN #EndTimeUTC EU ON EU.Num = SU.Num AND SU.Num = EU.Num
							 WHERE RetailLocationID = @RetailLocationID 

				UPDATE RetailLocation 
				SET TimeZoneID = @TimeZoneID
				WHERE RetailLocationID = @RetailLocationID

			END
			ELSE IF NOT EXISTS (SELECT 1 FROM HcHoursFilterTime WHERE @RetailLocationID IS NOT NULL AND RetailLocationID IN (SELECT RetailLocationID FROM RetailLocation WHERE RetailLocationID = @RetailLocationID AND RetailID = @RetailID AND Active = 1) )
			BEGIN
				INSERT INTO HcHoursFilterTime 
				 (
				 RetailLocationID,
				 HcDaysOfWeekID,
				 StartTime,
				 EndTime,
				 StartTimeUTC,
				 EndTimeUTC,
				 [DateCreated],
				 [DateModified]
				 )
				 SELECT 
				 @RetailLocationID,
				 ROW_NUMBER ()OVER (ORDER BY S.num),
				 CASE WHEN S.StartTimes = '00:00' AND E.EndTimes = '00:00' THEN CONVERT(datetime,NULL,121)
						   ELSE S.StartTimes END, 
				 CASE WHEN E.EndTimes = '00:00' AND S.StartTimes = '00:00' THEN CONVERT(datetime,NULL,121)
							ELSE E.EndTimes END ,
				 CASE WHEN SU.StartTimeUTCs = '00:00' AND EU.EndTimeUTCs = '00:00' THEN CONVERT(datetime,NULL,121)
							ELSE SU.StartTimeUTCs END,
				 CASE WHEN EU.EndTimeUTCs = '00:00' AND SU.StartTimeUTCs = '00:00' THEN CONVERT(datetime,NULL,121)
							ELSE EU.EndTimeUTCs END,
				 GETDATE(),
				 GETDATE()			 			
				 FROM #StartTime S 
				 INNER JOIN #EndTime E ON S.Num= E.Num
				 INNER JOIN #StartTimeUTC SU ON SU.Num = S.Num AND SU.Num = S.Num
				 INNER JOIN #EndTimeUTC EU ON EU.Num = SU.Num AND SU.Num = EU.Num

				 UPDATE RetailLocation 
				 SET TimeZoneID = @TimeZoneID
				 WHERE RetailLocationID = @RetailLocationID

			END


		--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebUpdateRetailerLocationsHoursFilter].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;			
GO
