USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryExpiredCoupons]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_MyGalleryExpiredCoupons   
Purpose     : To list Coupon  
Example     : usp_MyGalleryExpiredCoupons   
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   15th Oct 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_GalleryExpiredCoupons]  
(  
   @UserID int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
   
 BEGIN TRY  
 
  --To get Media Server Configuration.  
  DECLARE @ManufConfig varchar(50)    
  SELECT @ManufConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
   
   --To get the row count for pagination.  
    DECLARE @UpperLimit int   
    SELECT @UpperLimit = @LowerLimit + ScreenContent   
    FROM AppConfiguration   
    WHERE ScreenName = @ScreenName   
     AND ConfigurationType = 'Pagination'  
     AND Active = 1  
    DECLARE @MaxCnt int  
  
   SELECT Row_Num=ROW_NUMBER() over(order by C.couponid)  
    ,C.CouponID   
    ,CouponName  
    ,CouponDiscountType  
    ,CouponDiscountAmount  
    ,CouponDiscountPct  
    ,CouponShortDescription  
    ,CouponLongDescription  
    ,CouponDateAdded  
    ,CouponStartDate  
    ,CouponExpireDate  
    ,CouponURL  
    ,CouponImagePath =CASE WHEN CouponImagePath IS NULL THEN DBO.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																										CASE WHEN WebsiteSourceFlag = 1 
																											THEN @ManufConfig
																											+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																											+CouponImagePath 
																									    ELSE CouponImagePath 
																									    END
																								 END 
					 END  
	,ViewableOnWeb
   INTO #Coupon  
   FROM Coupon C  
   INNER JOIN UserCouponGallery CG ON CG.CouponID = C.CouponID AND UserID = @UserID   
   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID   
   WHERE CouponExpireDate < GETDATE() AND DATEDIFF(DAY,GETDATE(),CouponExpireDate)<=90  
     
   --To capture max row number.  
    SELECT @MaxCnt = MAX(Row_Num) FROM #Coupon  
    --this flag is a indicator to enable "More" button in the UI.   
    --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
    SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
  
   SELECT Row_Num rowNum  
    ,C.CouponID couponId  
    ,C.CouponName couponName  
    ,C.CouponDiscountType   
    ,C.CouponDiscountAmount   
    ,C.CouponDiscountPct   
    ,CouponShortDescription   
    ,CouponLongDescription    
    ,C.CouponDateAdded   
    ,C.CouponStartDate   
    ,C.CouponExpireDate   
    ,CouponURL  
    ,C.CouponImagePath  
    ,C.ViewableOnWeb
   FROM #Coupon C  
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   Order by Row_Num  
    
    
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_MyGalleryExpiredCoupons.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
