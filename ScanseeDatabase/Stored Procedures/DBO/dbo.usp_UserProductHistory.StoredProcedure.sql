USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserProductHistory]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserProductHistory
Purpose					: To move Wish list item to history when it is deleted.
Example					: usp_UserProductHistory

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserProductHistory]
(
	    @UserProductID int
	  , @UserID int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		    SELECT  UserProductID
				   ,UserRetailPreferenceID
				   ,UserID
				   ,ProductID
				   ,MasterListItem
				   ,WishListItem
				   ,TodayListtItem
				   ,ShopCartItem
				   ,UnassignedProductName
				   ,UnassignedProductDescription
				   ,MasterListAddDate
				   ,WishListAddDate
				   ,TodayListAddDate
				   ,ShopCartAddDate
				   ,MasterListRemoveDate
				   ,WishListRemoveDate
				   ,TodayListRemoveDate
				   ,ShopCartRemoveDate
				   ,PushNotifyFlag
			INTO #WishListHistory
			FROM UserProduct
			WHERE UserProductID=@userproductid AND UserID=@UserID
			      AND WishListRemoveDate IS NOT NULL
	    

			INSERT INTO [UserProductHistory]
					   ([UserProductID]
					   ,[UserRetailPreferenceID]
					   ,[UserID]
					   ,[ProductID]
					   ,[MasterListItem]
					   ,[WishListItem]
					   ,[TodayListtItem]
					   ,[ShopCartItem]
					   ,[UnassignedProductName]
					   ,[UnassignedProductDescription]
					   ,[MasterListAddDate]
					   ,[WishListAddDate]
					   ,[TodayListAddDate]
					   ,[ShopCartAddDate]
					   ,[MasterListRemoveDate]
					   ,[WishListRemoveDate]
					   ,[TodayListRemoveDate]
					   ,[ShopCartRemoveDate]
					   ,[PushNotifyFlag])
			SELECT      UserProductID
					   ,UserRetailPreferenceID
					   ,UserID
					   ,ProductID
					   ,MasterListItem
					   ,WishListItem
					   ,TodayListtItem
					   ,ShopCartItem
					   ,UnassignedProductName
					   ,UnassignedProductDescription
					   ,MasterListAddDate
					   ,WishListAddDate
					   ,TodayListAddDate
					   ,ShopCartAddDate
					   ,MasterListRemoveDate
					   ,WishListRemoveDate
					   ,TodayListRemoveDate
					   ,ShopCartRemoveDate
					   ,PushNotifyFlag
			FROM #WishListHistory
		
			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserProductHistory.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			
		END;
		 
	END CATCH;
END;

GO
