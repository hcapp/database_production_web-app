USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerBannerAdDeletion
Purpose					: To delete a Banner Ad.
Example					: usp_WebRetailerBannerAdDeletion

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdDeletion]
(
	--Input Variables
	  @RetailID int
	, @BannerAdID int
	
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 DECLARE @ADStartDate date
			 DECLARE @ADEndDate date
			 
			 SELECT @ADStartDate = StartDate
				  , @ADEndDate = EndDate
			 FROM AdvertisementBanner
			 WHERE AdvertisementBannerID = @BannerAdID
			 
			 --Confirm if the Ad is inactive before deletion.
			 IF NOT EXISTS(SELECT 1 FROM AdvertisementBanner WHERE AdvertisementBannerID = @BannerAdID AND GETDATE() BETWEEN StartDate AND EndDate)
			 BEGIN
				--Move the Ad to archive.
				INSERT INTO AdvertisementBannerArchive(AdvertisementBannerID
													 , BannerAdName
													 , BannerAdDescription
													 , BannerAdImagePath
													 , BannerAdURL
													 , StartDate
													 , EndDate
													 , WebsiteSourceFlag
													 , DateCreated)
										  SELECT AdvertisementBannerID
													 , BannerAdName
													 , BannerAdDescription
													 , BannerAdImagePath
													 , BannerAdURL
													 , StartDate
													 , EndDate
													 , WebsiteSourceFlag
													 , GETDATE()
										  FROM AdvertisementBanner
										  WHERE AdvertisementBannerID = @BannerAdID
					
					--Move the Retail Location association to the Archive.					  
					INSERT INTO RetailLocationBannerAdArchive(RetailLocationID
													 , AdvertisementBannerID
													 , DateCreated)
												SELECT RetailLocationID
													 , AdvertisementBannerID
													 , GETDATE()
												FROM RetailLocationBannerAd
												WHERE AdvertisementBannerID = @BannerAdID
												
					
					--Delete the child table.
					DELETE FROM RetailLocationBannerAd WHERE AdvertisementBannerID = @BannerAdID
					--Delete from the master table.
					DELETE FROM AdvertisementBanner WHERE AdvertisementBannerID = @BannerAdID												
			 END
			
			
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdDeletion.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
