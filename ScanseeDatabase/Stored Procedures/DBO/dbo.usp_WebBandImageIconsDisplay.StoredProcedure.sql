USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandImageIconsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebBandImageIconsDisplay
Purpose     : To Display the default image icons in the Band Anything Page Creation & Special Offer Creation.    
Example     : usp_WebBandImageIconsDisplay  
    
History    
Version  Date			 Author				Change Description    
---------------------------------------------------------------     
1.0   4/26/2016		Bindu T A					Initial Version -usp_WebBandImageIconsDisplay  
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebBandImageIconsDisplay]    
(    
    @PageType varchar(200)
 --Output Variable--    
   , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY    
 
	 --To get Media Server Configuration.  
	  DECLARE @Config varchar(50)    
	  SELECT @Config=	
	    ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Retailer Media Server Configuration'   
	    
	    
	    
	  --Concatenate the image icon name with the Retailer Media Server Configuration.   
      SELECT  QRBandCustomPageIconID  as QRRetailerCustomPageIconID
			, ImagePath = @Config + QRBandCustomPageIconImagePath
      FROM QRBandCustomPageIcons WHERE PageType In (Select QRPageTypeID from QRBandCustomPageType where QRPageTypeName IN(@PageType,'ALL'))
      
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebBandImageIconsDisplay'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       
  END;    
       
 END CATCH;    
END;




GO
