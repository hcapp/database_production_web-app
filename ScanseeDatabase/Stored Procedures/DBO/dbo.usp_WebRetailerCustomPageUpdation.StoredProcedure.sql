USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCustomPageUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCustomPageUpdation
Purpose					: To Update the Retailer Custom Page. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCustomPageUpdation]
(
	   
	  @RetailID int
    , @RetailLocationID varchar(MAX)
	, @PageID int
	, @PageTitle varchar(255)
	, @Image varchar(100)
	, @PageDescription varchar(1000)
	, @ShortDescription varchar(255)
	, @LongDescription varchar(1000)
	, @StartDate datetime
	, @EndDate datetime
	, @MediaPath varchar(max)
    , @ExternalFlag varchar(max)   
	
	--Output Variable 	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		    DECLARE @ProductMediaTypeID int
		 
			SELECT @ProductMediaTypeID = ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType LIKE 'Other Files'
			
		    --Modify the Custom Page Details.
			UPDATE QRRetailerCustomPage
			SET Pagetitle = @PageTitle
			  , [Image] = @Image
			  , PageDescription = @PageDescription			 
			  , ShortDescription = @ShortDescription
			  , LongDescription = @LongDescription
			  , StartDate = @StartDate
			  , EndDate = @EndDate
			 WHERE RetailID = @RetailID
			 AND QRRetailerCustomPageID = @PageID
			 
			 --Update the RetailLocation Association
			 
			 IF @RetailLocationID IS NOT NULL
			 BEGIN
					 DELETE FROM QRRetailerCustomPageAssociation 
					 WHERE QRRetailerCustomPageID = @PageID
					 
					 INSERT INTO QRRetailerCustomPageAssociation(
																	  QRRetailerCustomPageID
																	, RetailID
																	, RetailLocationID
																	, DateUpdated)
														SELECT @PageID
															 , @RetailID
															 , [Param]
															 , GETDATE()
														FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END									
					--Update the Media Association.
					
			IF @MediaPath IS NOT NULL
			BEGIN
					DELETE FROM QRRetailerCustomPageMedia
					WHERE QRRetailerCustomPageID = @PageID
					
					 --Split the Comma Separated Media												
						SELECT Row = IDENTITY(INT, 1, 1)
							 , [Param] Media
						INTO #Media
						FROM dbo.fn_SplitParam(@MediaPath, ',')	
		                
						--Split the Comma Separated External Flags
						SELECT RowNum = IDENTITY(INT, 1, 1)
							 , [Param] ExternalFlag
						INTO #ExternalFlag
						FROM dbo.fn_SplitParam(@ExternalFlag, ',')	
		                
		                												
						--Make Media Association.
						INSERT INTO QRRetailerCustomPageMedia(
																  QRRetailerCustomPageID
																, MediaTypeID
																, MediaPath
																, ExternalFlag
																, DateUpdated
															  )
														SELECT
																 @PageID
															   , @ProductMediaTypeID
															   , M.Media
															   , E.ExternalFlag
															   , GETDATE()
														FROM #Media M
														INNER JOIN #ExternalFlag E ON M.Row = E.RowNum
					   
			  END												
			 
			 --Confirmation of Success.
			 SET @Status = 0		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCustomPageUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

--SELECT * FROM UserRetailer U INNER JOIN Users US ON U.UserID = US.UserID




GO
