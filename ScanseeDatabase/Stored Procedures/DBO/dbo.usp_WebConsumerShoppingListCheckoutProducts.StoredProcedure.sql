USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerShoppingListCheckoutProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerShoppingListCheckoutProducts]
Purpose					: To clear all the product from shopping Cart and move it to History.
Example					: [usp_WebConsumerShoppingListCheckoutProducts]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			2nd Aug 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerShoppingListCheckoutProducts]
(
	  @UserID int	
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY	    
				
		BEGIN TRANSACTION
		
		DECLARE @CartLatitude float
	    DECLARE @PostalCode VArchar(100)
	    DECLARE @CartLongitude float 
		DECLARE @Today datetime
		DECLARE @UserCartID int
		SET @Today = GETDATE()
		
		SELECT @PostalCode =PostalCode  
		FROM Users Where Userid =@UserID 
		
		IF @PostalCode IS NOT NULL
		BEGIN
           SELECT @CartLatitude =Latitude 
                 ,@CartLongitude =Longitude 
           FROM GeoPosition WHERE PostalCode =@PostalCode 
		END
		
			--Moving Shopping Cart data to History
			INSERT INTO [UserShoppingCartHistory]
				   ([UserID]
				   ,[CartLatitude]
				   ,[CartLongitude]
				   ,[CartCreateDate])
			 VALUES
				   (@UserID 
				   ,@CartLatitude
				   ,@CartLongitude
				   ,@Today)
			
			SET @UserCartID = @@IDENTITY 
			
			INSERT INTO [UserShoppingCartHistoryProduct]
				   ([UserCartID]
				   ,[UserProductID]
				   ,[MasterListAddDate]
				   ,[TodayListAddDate]
				   ,[ShopCartAddDate])
			 SELECT @UserCartID
					, UserProductID
					, MasterListAddDate
					, @Today
					, @Today
			 FROM UserProduct 
		     WHERE UserID = @UserID 
					AND ShopCartItem = 1
					
			--To delete product from Shopping Cart
			UPDATE UserProduct 
			SET ShopCartItem = 0
			   ,TodayListtItem =0
				, ShopCartRemoveDate = @Today 
				,TodayListRemoveDate =@Today
			WHERE UserID = @UserID 
				AND ShopCartItem = 1
				
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerShoppingListCheckoutProducts].'		
			--- Execute retrieval of Error info.
			EXEC [Version7].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
