USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFundraisingAssociatedRetailLocationsList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebFundraisingAssociatedRetailLocationsList]
Purpose					: To display List of Associated RetailLocations.
Example					: [usp_WebFundraisingAssociatedRetailLocationsList]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			11/11/2014      SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFundraisingAssociatedRetailLocationsList]
(   
    --Input variable.	  
	  @FundraisingID int
	, @RetailID int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			IF EXISTS (SELECT 1 FROM HcFundraising WHERE HcFundraisingID = @FundraisingID AND RetailID = @RetailID AND FundraisingAppsiteFlag = 1)
			BEGIN
					SELECT retailLocationIds = RL.RetailLocationID
						  , Address1 = RL.Address1
						  , City = RL.City
						  , State = RL.State
						  , PostalCode = RL.PostalCode
						  , StoreIdentification = RL.StoreIdentification
					FROM HcFundraising F
					INNER JOIN HcRetailerFundraisingAssociation RF ON F.HcFundraisingID = RF.HcFundraisingID
					INNER JOIN RetailLocation RL ON RF.RetailLocationID = RL.RetailLocationID 
					WHERE F.HcFundraisingID = @FundraisingID AND F.Active = 1 AND FundraisingAppsiteFlag = 1
					AND F.RetailID = @RetailID AND RL.Active = 1
			END
			ELSE 
			BEGIN
					SELECT retailLocationIds = NULL
							  , Address1 = Address
							  , City = City
							  , State = State
							  , PostalCode = PostalCode
							  , StoreIdentification = NULL
					FROM HcFundraising F
					INNER JOIN HcFundraisingLocation FL ON F.HcFundraisingID = FL.HcFundraisingID
					WHERE F.HcFundraisingID = @FundraisingID AND F.Active = 1 AND FundraisingAppsiteFlag = 0
					AND F.RetailID = @RetailID
			END
			
			
			 
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebFundraisingAssociatedRetailLocationsList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;










GO
