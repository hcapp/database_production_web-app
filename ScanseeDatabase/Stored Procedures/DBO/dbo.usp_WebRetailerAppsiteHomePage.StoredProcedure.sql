USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAppsiteHomePage]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerAppsiteHomePage]
Purpose					: To display Retailer Appsite Home Page.
Example					: [usp_WebRetailerAppsiteHomePage]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			12/01/2015		SPAN           1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAppsiteHomePage]
(   
    --Input variable.	  
	  @RetailID Int
  
	--Output Variable 
	, @Logo bit output
	, @Splash bit output
	, @Banner bit output
	, @AnythingPage bit output
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			SELECT @Logo = 0
			SELECT @Splash = 0
			SELECT @Banner = 0
			SELECT @AnythingPage = 0

			DECLARE @AnythingPageTypeID int = 0
			SELECT @AnythingPageTypeID = QRTypeID
			FROM QRTypes
			WHERE QRTypeName = 'Anything Page'


			SELECT @Logo = 1
			FROM Retailer 
			WHERE RetailID = @RetailID
			AND RetailerImagePath IS NOT NULL

			SELECT @Splash = 1
			FROM AdvertisementSplash
			WHERE RetailID = @RetailID
			AND SplashAdImagePath IS NOT NULL

			SELECT @Banner = 1
			FROM AdvertisementBanner
			WHERE RetailID = @RetailID
			AND BannerAdImagePath IS NOT NULL

			SELECT @AnythingPage = 1
			FROM QRRetailerCustomPage
			WHERE RetailID = @RetailID
			AND QRTypeID = @AnythingPageTypeID

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerAppsiteHomePage].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
