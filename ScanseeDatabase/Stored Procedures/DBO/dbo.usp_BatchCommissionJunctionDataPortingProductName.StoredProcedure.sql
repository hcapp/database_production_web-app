USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCommissionJunctionDataPortingProductName]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CommissionJunctionDataPortingProductName
Purpose					: usp_CommissionJunctionDataPortingProductName
Example					: usp_CommissionJunctionDataPortingProductName

History
Version		Date			Author					Change Description
--------------------------------------------------------------- 
1.0			31st Dec 2012	SPAN Infotech India		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCommissionJunctionDataPortingProductName]
(
	 @FileName VARCHAR(255)
	--Output Variable ;
	,@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		    			
			DECLARE @ExistingProducts2 INT
			DECLARE @APIPartnerID INT
			DECLARE @ProcessedRowCount INT
			DECLARE @Duplicates INT
		
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'CommissionJunction'	
			
			;WITH CTE 
					(
						PROGRAMNAME
						,PROGRAMURL						
						,NAME
						,KEYWORDS
						,[DESCRIPTION]
						,SKU
						,MANUFACTURER						
						,UPC
						,ISBN					
						,SALEPRICE
						,PRICE
						,RETAILPRICE						
						,BUYURL
						,IMPRESSIONURL
						,IMAGEURL						
						,PROMOTIONALTEXT
						,STARTDATE
						,ENDDATE						
						,INSTOCK						
						,WARRANTY
						,STANDARDSHIPPINGCOST
						,Category
						,[Sub Category]						
						,RWNO
					)
					AS
					(
						  SELECT PROGRAMNAME
								,PROGRAMURL								
								,NAME
								,KEYWORDS
								,[DESCRIPTION]
								,SKU
								,MANUFACTURER								
								,UPC
								,ISBN								
								,SALEPRICE
								,PRICE
								,RETAILPRICE								
								,BUYURL
								,IMPRESSIONURL
								,IMAGEURL								
								,PROMOTIONALTEXT
								,STARTDATE
								,ENDDATE								
								,INSTOCK								
								,WARRANTY
								,STANDARDSHIPPINGCOST
								,Category
								,[Sub Category]												
								,ROW_NUMBER() OVER (PARTITION BY NAME ORDER BY NAME) AS RWNO 
						 FROM APICommissionJunctionData
						 WHERE UPC IS NULL
						 AND ISBN IS NULL
						 AND NAME IS NOT NULL
					) 		
				--Delete Products if duplicated.			
				DELETE FROM CTE WHERE RWNO > 1						
				SELECT @Duplicates = @@ROWCOUNT 
			    			
				SELECT @ExistingProducts2 = COUNT(1)
				FROM APICommissionJunctionData ACD
				INNER JOIN Product P ON P.ProductName = ACD.NAME --AND P.ProductLongDescription = ACD.[DESCRIPTION]
				WHERE ACD.NAME IS NOT NULL 
				AND ACD.UPC IS NULL 
				AND ACD.ISBN IS NULL
				AND P.ScanCode IS NULL	
				AND P.ISBN IS NULL	
				AND P.ProductName IS NOT NULL	
					
				--Mapping staging table with the retailer table to get the new retailer and existing retailer.
				SELECT DISTINCT ProgramName
							   ,ProgramURL	
				INTO #NewRetailer		
				FROM APICommissionJunctionData C
				LEFT JOIN Retailer R ON R.RetailName = C.ProgramName
				WHERE RetailID IS NULL 

				SELECT DISTINCT ProgramName
							   ,ProgramURL	
							   ,RetailID
				INTO #ExistingRetailer		
				FROM APICommissionJunctionData C
				INNER JOIN Retailer R ON R.RetailName = C.ProgramName
				
				--If existing retailer then update associated data in the retialer table. 
				IF EXISTS(SELECT 1 FROM #ExistingRetailer)
				BEGIN
					UPDATE Retailer
					SET RetailOnlineURL = ProgramURL
					  , [DateModified] = GETDATE() 
					FROM #ExistingRetailer E
					INNER JOIN Retailer R ON R.RetailID = E.RetailID
				END
				
				--If new reatiler then insert into retailer table.
				IF EXISTS(SELECT 1 FROM #NewRetailer)
				BEGIN
				INSERT INTO [Retailer]
						   ([RetailName]
						   ,[CountryID]
						   ,[DateCreated]
						   ,[DateModified]
						   ,[RetailOnlineURL])
				SELECT  ProgramName
						,1
					   ,GETDATE()
					   ,GETDATE()	
					   ,ProgramURL	
				FROM #NewRetailer
				END

				--If the Retailer is already associated and has onlinestore.
				SELECT distinct R.RetailID 
				INTO #ExistingRetailloc
				FROM Retailer R 
				INNER JOIN APICommissionJunctionData C ON R.RetailName = C.ProgramName
				INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID
				WHERE OnlineStoreFlag=1
				
				--If the Retailer is not associated, then CREATE a new retaillocation for this retailer.
				SELECT DISTINCT R.RetailID
				INTO #NewRetaillocations
				FROM Retailer R 
				INNER JOIN APICommissionJunctionData C ON R.RetailName = C.ProgramName
				LEFT JOIN RetailLocation RL ON R.RetailID=RL.RetailID
				WHERE RetailLocationID is null 

				--if the Retailer is already associated but onlinestoreflag=0 then CREATE a new retaillocation for this retailer.
				SELECT DISTINCT R.RetailID
				INTO #Existingretailerlocation
				FROM Retailer R 
				INNER JOIN APICommissionJunctionData C ON R.RetailName = C.ProgramName
				INNER JOIN RetailLocation RL ON R.RetailID=RL.RetailID
				WHERE OnlineStoreFlag =0

				SELECT  RetailID
				INTO #Retailerlocations
				from #Existingretailerlocation WHERE RetailID not in (SELECT RetailID FROM #ExistingRetailloc)

				IF EXISTS(SELECT 1 FROM #NewRetaillocations)
				BEGIN
					INSERT INTO [RetailLocation]
						   ([RetailID]
						   ,[CountryID]
						   ,[Headquarters]
						   ,[DateCreated]
						   ,[DateModified]
						   ,[OnlineStoreFlag])
					 SELECT DISTINCT RetailID 
							,1
							,0
							,GETDATE()
							,GETDATE()
							,1
					 FROM #NewRetaillocations
				END

				IF EXISTS(SELECt 1 FROM #retailerlocations)
				BEGIN
					INSERT INTO [RetailLocation]
						   ([RetailID]
						   ,[CountryID]
						   ,[Headquarters]
						   ,[DateCreated]
						   ,[DateModified]
						   ,[OnlineStoreFlag])
					 SELECT DISTINCT RetailID 
							,1
							,0
							,GETDATE()
							,GETDATE()
							,1
					 FROM #retailerlocations
				END

				--Check and insert the products based on ProductName
				SELECT RowNum = IDENTITY(INT,1,1), C.*
				INTO #Temp 
				FROM APICommissionJunctionData C
				LEFT OUTER JOIN Product P ON P.ProductName = C.NAME AND P.ScanCode IS NULL AND P.ISBN IS NULL
				WHERE P.ProductID IS NULL
				AND C.UPC IS NULL 
				AND C.ISBN IS NULL

				SELECT * 
				INTO #Products
				FROM #Temp
				WHERE RowNum IN 
				(SELECT MIN(RowNum)
				FROM #Temp
				GROUP BY NAME)
				 
				DECLARE @ManufacturerID INT

				SELECT @ManufacturerID = ManufacturerID
				FROM Manufacturer WHERE ManufName LIKE 'Scansee Commission Junction'

				SELECT ACD.*
				INTO #UpdateProducts
				FROM APICommissionJunctionData ACD
				INNER JOIN Product P ON P.ProductName = ACD.NAME --AND P.ProductLongDescription = ACD.[DESCRIPTION]
				WHERE ACD.NAME IS NOT NULL 
				AND ACD.UPC IS NULL 
				AND ACD.ISBN IS NULL
				AND P.ScanCode IS NULL	
				AND P.ISBN IS NULL	
				AND P.ProductName IS NOT NULL
				
				--Updating Existing Products From Stage table
				UPDATE Product SET [ManufacturerID]=@ManufacturerID 
						   ,[ScanCode]=C.UPC 
						   ,[ProductName]=C.Name 
						   ,ISBN=C.ISBN 
						   ,[ProductShortDescription]=SUBSTRING(C.DESCRIPTION,1,255) 
						   ,[ProductLongDescription]=Description         
						   ,[ProductImagePath]=C.ImageURL 
						   ,[SuggestedRetailPrice]=C.Price           
						   ,[ProductModifyDate]=GETDATE()   
						   ,[WarrantyServiceInformation]=C.Warranty      
						   ,[APIPartnerID]=@APIPartnerID 
				FROM #UpdateProducts C
				INNER JOIN Product P ON P.ProductName = C.NAME 
				WHERE C.NAME IS NOT NULL 
				AND C.UPC IS NULL 
				AND C.ISBN IS NULL
				AND P.ScanCode IS NULL	
				AND P.ISBN IS NULL	
				AND P.ProductName IS NOT NULL
				
				--Insert into Product table
				INSERT INTO  [Product]
						   ([ManufacturerID]
						   ,[ScanCode]
						   ,[ScanTypeID]
						   ,[Service]
						   ,[ProductName]
						   , ISBN
						   ,[ProductShortDescription]
						   ,[ProductLongDescription]          
						   ,[ProductImagePath]
						   ,[SuggestedRetailPrice]          
						   ,[ProductAddDate]
						   ,[ProductModifyDate] 
						   ,[WarrantyServiceInformation]        
						   ,[WebsiteSourceFlag])
				 SELECT @ManufacturerID
					  , P.UPC
					  , 1
					  , 0
					  , P.NAME
					  , P.isbn
					  , SUBSTRING(P.DESCRIPTION,1,255)
					  , P.DESCRIPTION	
					  , P.IMAGEURL
					  , P.PRICE
					  , GETDATE()
					  , GETDATE()
					  , P.Warranty
					  , 0
				FROM #Products P
				
				SELECT @ProcessedRowCount = @@ROWCOUNT
				
				--Update existing  Product SKU from stage table
				UPDATE ProductSKU SET SKUNumber =U.SKU 
				FROM #UpdateProducts U 
				INNER JOIN Product P ON U.Name =P.ProductName  
				INNER JOIN ProductSKU PS ON PS.ProductID =P.ProductID 
				WHERE P.ScanCode IS NOT NULL
				AND U.UPC IS NULL 
				AND U.ISBN IS NULL
				AND P.ScanCode IS NULL	
				AND P.ISBN IS NULL	
				AND P.ProductName IS NOT NULL
				AND U.SKU IS NOT NULL
				 	  
				--Insert into ProductSKU Table.
				INSERT INTO ProductSKU(ProductID
									 , SKUNumber
									 , DateAdded)
				SELECT P.ProductID
					 , PR.SKU
					 , GETDATE()
				FROM Product P
				INNER JOIN #Products Pr ON P.ProductName = Pr.NAME
				AND CONVERT(DATE, P.ProductAddDate ) = CONVERT(DATE, GETDATE())

				--Insert into ProductCategory Table. Associate the category "Others" in the absence of the category association for the product.
				SELECT ProductID
					  ,CategoryID = ISNULL(CategoryID,(SELECT CategoryID  
                                                       FROM Category 
												       WHERE ParentCategoryName LIKE 'Other' 
												       AND SubCategoryName LIKE 'Miscellaneous'))
				      , DateAdded
				INTO #PC
				FROM(
				SELECT P.ProductID, C.CategoryID, GETDATE() DateAdded FROM Product P 
				INNER JOIN #Products TP ON P.ProductName = TP.NAME
				LEFT JOIN Category C ON C.ParentCategoryID = TP.Category AND C.SubCategoryID = TP.[Sub Category]
			    LEFT JOIN ProductCategory PC ON PC.ProductID =P.ProductID AND PC.CategoryID =C.CategoryID 			 
			    WHERE PC.ProductID IS NULL AND CONVERT(DATE,ProductAddDate) = CONVERT(DATE,GETDATE()))P

				--Updating existing Product category from Stage table
				UPDATE ProductCategory SET CategoryID =U.Category 
				FROM #UpdateProducts U
				INNER JOIN Product P ON P.ProductName =U.Name 
				INNER JOIN ProductCategory PC ON PC.ProductID =P.ProductID 
				WHERE P.ScanCode IS NOT NULL
				AND U.UPC IS NULL 
				AND U.ISBN IS NULL
				AND P.ScanCode IS NULL	
				AND P.ISBN IS NULL	
				AND P.ProductName IS NOT NULL 
				AND U.Category IS NOT NULL	
				
				INSERT INTO ProductCategory(ProductID
										  , CategoryID
										  , DateAdded)
							SELECT ProductID
								 , CategoryID
								 , DateAdded
							FROM #PC
				 
				 DECLARE @UserID INT
				 
				 SELECT @UserID = UM.UserID
				 FROM Manufacturer M
				 INNER JOIN UserManufacturer UM ON M.ManufacturerID = UM.ManufacturerID
				 WHERE M.ManufName LIKE 'Scansee Commission Junction'
				 
				 --Updating Productkeywords table from Stage table			 
				 Update ProductKeywords SET ProductKeyword=U.Keywords 
										   ,DateModified=GETDATE()
				 FROM #UpdateProducts U
				 INNER JOIN Product P ON P.ProductName =U.Name  
				 INNER JOIN ProductKeywords K ON K.ProductID =P.ProductID
				 WHERE P.ScanCode IS NOT NULL
				 AND U.UPC IS NULL 
				 AND U.ISBN IS NULL
				 AND P.ScanCode IS NULL	
				 AND P.ISBN IS NULL	
				 AND P.ProductName IS NOT NULL 
				 AND U.Keywords IS NOT NULL  
				  
				 --Insert into ProductKeywords Table 
				 INSERT INTO ProductKeywords(ProductID
											, ProductKeyword
											, DateCreated
											, CreatedUserID)
									SELECT P.ProductID
										 , Pr.KEYWORDS
										 , GETDATE()
										 , @UserID
									FROM #Products Pr
									INNER JOIN Product P ON Pr.NAME = P.ProductName
									WHERE CONVERT(DATE, ProductAddDate) = CONVERT(DATE, GETDATE())
				 
				--RetailLocationProduct
				SELECT RowNum=IDENTITY(int,1,1)
					  ,	* 
				INTO #Products1
				FROM APICommissionJunctionData z
				WHERE UPC IS NULL
				AND ISBN IS NULL
				
				SELECT * 
				INTO #Products2
				FROM #Products1 z
				WHERE RowNum IN 
				(SELECT MIN(RowNum)
				FROM #Products1
				GROUP BY NAME)

				SELECT RP.RetailLocationID
						,RP.ProductID
						,C.DESCRIPTION
						,C.SalePrice
						,ISNULL(C.Price,C.RetailPrice) Price
						,C.STARTDATE
						,C.ENDDATE
						,C.BUYURL
						,c.PROGRAMNAME
						,C.STANDARDSHIPPINGCOST
				INTO #ExistingRetailLocProducts
				FROM #Products2 C
				INNER JOIN Retailer R ON R.RetailName = C.ProgramName
				INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND OnlineStoreFlag=1
				INNER JOIN Product P ON P.ProductName = C.NAME
				INNER JOIN RetailLocationProduct RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=P.ProductID 
				--AND CONVERT(DATE,ProductAddDate) = CONVERT(DATE,GETDATE())
								
				SELECT RL.RetailLocationID
						,P.ProductID
						,C.Description
						,C.SalePrice
						,ISNULL(C.Price,C.RetailPrice) Price
						,C.STARTDATE
						,C.ENDDATE
						,C.ProgramName
						,C.Name
						,C.BUYURL
						,c.STANDARDSHIPPINGCOST
				INTO #NewRetailLocProducts
				FROM #Products2 C
				INNER JOIN Retailer R ON R.RetailName = C.ProgramName
				INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND OnlineStoreFlag=1
				INNER JOIN Product P ON P.ProductName = C.NAME
				LEFT JOIN RetailLocationProduct RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=P.ProductID
				WHERE RP.RetailLocationID IS NULL AND RP.ProductID IS NULL 
				--AND CONVERT(DATE,ProductAddDate) = CONVERT(DATE,GETDATE())

				IF EXISTS(SELECT 1 FROM #ExistingRetailLocProducts)					
				BEGIN
					UPDATE RetailLocationProduct
					SET RetailLocationProductDescription=C.Description
						,SalePrice=C.SalePrice
						,Price=C.Price
						,SaleStartDate=convert(date,convert(varchar(50),C.STARTDATE),103)
						,SaleEndDate=convert(date,convert(varchar(50),C.ENDDATE),103)
						,DateUpdated = GETDATE()
						,BuyURL=C.BuyURL
						,ShipmentDetails = C.STANDARDSHIPPINGCOST
					FROM #ExistingRetailLocProducts C
					INNER JOIN RetailLocationProduct RP ON RP.RetailLocationID=C.RetailLocationID AND RP.ProductID=C.ProductID 
				END

				IF  EXISTS(SELECT 1 FROM #NewRetailLocProducts)
				BEGIN
				INSERT INTO [RetailLocationProduct]
						   ([RetailLocationID]
						   ,[ProductID]
						   ,[RetailLocationProductDescription]
						   ,[Price]
						   ,[SalePrice]
						   ,[SaleStartDate]
						   ,[SaleEndDate]
						   ,[DateCreated]
						   ,[DateUpdated]
						   ,[BuyURL]
						   ,[ShipmentDetails])
				SELECT C.RetailLocationID
								,C.ProductID
								,C.Description
								,C.Price
								,C.SALEPRICE
								,C.STARTDATE
								,C.ENDDATE
								,GETDATE()
								,GETDATE()
								,C.BUYURL
								,C.STANDARDSHIPPINGCOST
							FROM #NewRetailLocProducts C
				END
				 
				--Select Existing products in New products table for update ReatilLocationDeal.
				SELECT RP.RetailLocationID
						,RP.ProductID
						,C.DESCRIPTION
						,C.SalePrice
						,ISNULL(C.Price,C.RetailPrice)Price
						,C.STARTDATE
						,C.ENDDATE
						,@APIPartnerID APIPartnerID
				INTO #ExistingRetailLocDeal
				FROM #Products2 C
				INNER JOIN Retailer R ON R.RetailName = C.ProgramName
				INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND OnlineStoreFlag=1
				INNER JOIN Product P ON P.ProductName = C.NAME
				INNER JOIN RetailLocationDeal RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=P.ProductID 
				WHERE C.SalePrice IS NOT NULL
	            
				--Select new products associated to the RetailLocationDeal table.
				SELECT RL.RetailLocationID
						,P.ProductID
						,C.Description
						,C.SalePrice
						,ISNULL(C.Price,C.RetailPrice) Price
						,C.STARTDATE
						,C.ENDDATE
						,@APIPartnerID APIPartnerID
				INTO #NewRetailLocDeal
				FROM #Products2 C
				INNER JOIN Retailer R ON R.RetailName = C.ProgramName
				INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID AND OnlineStoreFlag=1
				INNER JOIN Product P ON P.ProductName = C.NAME AND P.ScanCode IS NULL AND P.ISBN IS NULL
				LEFT JOIN RetailLocationDeal RP ON RP.RetailLocationID=RL.RetailLocationID AND RP.ProductID=P.ProductID
				WHERE RP.RetailLocationID IS NULL AND RP.ProductID IS NULL  
				AND C.SalePrice IS NOT NULL
							  
				IF EXISTS(SELECT 1 FROM #ExistingRetailLocDeal)			
				BEGIN			
				    --Select products list those sale price is updated.
					SELECT RD.ProductID 
						  ,RD.RetailLocationID 
						  ,RD.RetailLocationDealID 
					INTO #SalesExist         
					FROM RetailLocationDeal RD
					INNER JOIN #ExistingRetailLocDeal E ON RD.RetailLocationDealID =E.RetailLocationID AND RD.ProductID =E.ProductID 
					AND E.SalePrice <> RD.SalePrice 
			    
			    
			        --Checking if any sale price updated products exist
			        IF EXISTS(SELECT 1 FROM #SalesExist)
		            BEGIN		    
						SELECT DISTINCT UserID
							  ,ProductID
							  ,DeviceID
							  ,RetailLocationDealID  	              
						INTO #Sales
						FROM (
						SELECT U.UserID
							  ,UP.ProductID
							  ,UD.DeviceID 
							  ,RLP.RetailLocationDealID     
							  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
							  ,LocaleRadius 
						from #SalesExist T
						INNER JOIN RetailLocationDeal RLP ON T.RetailLocationDealID =RLP.RetailLocationDealID 
						INNER JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID  
						INNER JOIN UserProduct UP ON UP.ProductID =RLP.ProductID AND WishListItem =1	
						INNER JOIN Users U ON U.UserID =UP.UserID 
						INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
						INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
						INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
						)Note
					--	WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
				        
							        
						--User pushnotification already exist then NotificationSent flag updating to 0
						UPDATE UserPushNotification SET NotificationSent=0
													   ,DateModified =GETDATE()			                                      
						FROM UserPushNotification UP
						INNER JOIN #Sales T ON UP.RetailLocationDealID=T.RetailLocationDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0
				       
					   --User pushnotification not exist then insert. 
					   INSERT INTO UserPushNotification(UserID
														,ProductID
														,DeviceID												
														,RetailLocationDealID  												
														,NotificationSent
														,DateCreated)
						SELECT T.UserID 
							  ,T.ProductID 
							  ,T.DeviceID 
							  ,T.RetailLocationDealID   
							  ,0
							  ,GETDATE()
						FROM #Sales T				
						LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.RetailLocationDealID  =UP.RetailLocationDealID AND T.ProductID =UP.ProductID AND DiscountCreated=0
						WHERE UP.UserPushNotificationID IS NULL  
			        END				
				
					UPDATE RetailLocationDeal 
					SET RetailLocationProductDescription=C.Description
						,SalePrice=C.SalePrice
						,Price=C.Price
						,SaleStartDate=convert(date,convert(varchar(50),C.STARTDATE),103)
						,SaleEndDate=convert(date,convert(varchar(50),C.ENDDATE),103)
						,DateUpdated = GETDATE()
						,APIPartnerID = @APIPartnerID 
						FROM #ExistingRetailLocDeal C
						INNER JOIN RetailLocationDeal  RP ON RP.RetailLocationID=C.RetailLocationID AND RP.ProductID=C.ProductID 
				END

				IF  EXISTS(SELECT 1 FROM #NewRetailLocDeal)
				BEGIN
				
				 CREATE TABLE #NewSales(RetailLocationDealID Int,RetailLocationID int,ProductID Int)
				 
				INSERT INTO [RetailLocationDeal]
						   ([RetailLocationID]
						   ,[ProductID]
						   ,[RetailLocationProductDescription]
						   ,[Price]
						   ,[SalePrice]
						   ,[SaleStartDate]
						   ,[SaleEndDate]
						   ,[DateCreated]
						   ,[DateUpdated]
						   ,APIPartnerID)
				
				OUTPUT inserted.RetailLocationDealID,inserted.RetailLocationID,inserted.ProductID INTO #NewSales(RetailLocationDealID,RetailLocationID,ProductID)
				
				SELECT C.RetailLocationID
								,C.ProductID
								,C.Description
								,C.Price
								,C.SALEPRICE
								,C.STARTDATE
								,C.ENDDATE
								,GETDATE()
								,GETDATE()
								,APIPartnerID 
							FROM #NewRetailLocDeal C
				 				
				
				SELECT DISTINCT UserID
						  ,ProductID
						  ,DeviceID
						  ,RetailLocationDealID  	              
					INTO #Temp22
					FROM (
					SELECT U.UserID
						  ,UP.ProductID
						  ,UD.DeviceID 
						  ,RLP.RetailLocationDealID     
						  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
						  ,LocaleRadius 
					from #NewSales RLP
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID  AND RLP.ProductID =RLP.productID
					INNER JOIN UserProduct UP ON UP.ProductID =RLP.ProductID AND WishListItem =1	
					INNER JOIN Users U ON U.UserID =UP.UserID 
					INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
					INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
					)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
			       
						        
					--User pushnotification already exist then NotificationSent flag updating to 0
					UPDATE UserPushNotification SET NotificationSent=0
												   ,DateModified =GETDATE()		
												   ,DiscountCreated=1	                                      
					FROM UserPushNotification UP
					INNER JOIN #Temp22 T ON UP.RetailLocationDealID=T.RetailLocationDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=1
			       
		           --User pushnotification not exist then insert. 
				   INSERT INTO UserPushNotification(UserID
													,ProductID
													,DeviceID												
													,RetailLocationDealID  												
													,NotificationSent
													,DateCreated
													,DiscountCreated)
					SELECT T.UserID 
						  ,T.ProductID 
						  ,T.DeviceID 
						  ,T.RetailLocationDealID   
						  ,0
						  ,GETDATE()
						  ,1
					FROM #Temp22 T				
					LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.RetailLocationDealID =UP.RetailLocationDealID AND T.ProductID =UP.ProductID AND DiscountCreated=1
					WHERE UP.UserPushNotificationID IS NULL					
				END
					
				--Update the log with the counts.
				UPDATE APIBatchLog 
				SET ExistingRecordCount = ExistingRecordCount + @ExistingProducts2
				  , ProcessedRowCount = ProcessedRowCount + @ProcessedRowCount
				  , DuplicatesCount = DuplicatesCount + @Duplicates
				WHERE ProcessedFileName = @FileName
				AND CONVERT(DATE, GETDATE()) = CONVERT(DATE, ExecutionDate)
			 
			 
			               	
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CommissionJunctionDataPortingProductName.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
