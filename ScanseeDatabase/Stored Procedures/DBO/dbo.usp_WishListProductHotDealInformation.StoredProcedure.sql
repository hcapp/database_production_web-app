USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListProductHotDealInformation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListProductHotDealInformation
Purpose					: to get the hot deal product details.
Example					: usp_WishListProductHotDealInformation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04 Sep 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListProductHotDealInformation]
(
	  @UserId int
	, @ProductId int
	, @Latitude decimal(18,6)    
	, @Longitude decimal(18,6)    
	, @ZipCode varchar(10)
	, @Radius int    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--To get Server Configuration
		 DECLARE @Config varchar(50)  
		 SELECT @Config=ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Configuration of server' 
			   
			 --While User search by Zipcode, coordinates are fetched from GeoPosition table.
			     
			   IF (@Latitude IS NULL AND @Longitude IS NULL )    
			      BEGIN    
						SELECT @Latitude = Latitude     
						  , @Longitude = Longitude     
						FROM GeoPosition     
						WHERE PostalCode = @ZipCode     
			       END       
	   
			   --If radius is not passed, getting user preferred radius from UserPreference table.    
			   IF @Radius IS NULL     
				 BEGIN     
						SELECT @Radius = LocaleRadius  
						FROM dbo.UserPreference WHERE UserID = @UserID    
			     END     
					 
				--To get hot deal info for the wish list product.
				IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL) 
				
					BEGIN	
				
							SELECT ProductHotDealID
								   , HotDealName
								   , HotDeaLonglDescription hDLognDescription
								   , HotDealImagePath
								   , Price hDPrice
								   , SalePrice hDSalePrice
								   , Distance
								   , HotDealTermsConditions hDTermsConditions
								   , HotDealStartDate hDStartDate
								   , HotDealEndDate hDEndDate
								   , HotDealURL hdURL
								   , APIPartnerName apiPartnerName
							FROM ( SELECT DISTINCT  p.ProductHotDealID 
										  , p.HotDealName
										  , p.HotDeaLonglDescription 
										  , HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
																						CASE WHEN WebsiteSourceFlag = 1 THEN 
																									CASE WHEN P.ManufacturerID IS NOT NULL THEN @Config  
																									   +'Images/manufacturer/'  
																									   +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
																									   +HotDealImagePath  
																									ELSE @Config  
																									+'Images/retailer/'  
																									+CONVERT(VARCHAR(30),P.RetailID)+'/'  
																									+HotDealImagePath  
																									END  					                         
																						ELSE P.HotDealImagePath  
																						END  
																END  
										  , Price
										  , SalePrice
										  , Distance = (ACOS((SIN(PL.HotDealLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(PL.HotDealLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (PL.HotDealLongitude / 57.2958))))*6371) * 0.6214  
										  , p.HotDealTermsConditions
										  , p.HotDealStartDate
										  , p.HotDealEndDate
										  , p.HotDealURL
										  , A.APIPartnerName 
								  FROM UserProduct up
									INNER JOIN HotDealProduct HP ON up.ProductID=HP.ProductID
									INNER JOIN ProductHotDeal P ON p.ProductHotDealID=hp.ProductHotDealID
									INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HP.ProductHotDealID
									LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID
								  WHERE HP.productid=@productid 
								   AND  UP.UserID =  @userid
								   AND UP.WishListItem = 1
								   AND GETDATE() BETWEEN P.HotDealStartDate AND P.HotDealEndDate) HotDeal
							WHERE Distance <= ISNULL(@Radius, 5)
					  END	
					  
					  IF (@Latitude IS NULL AND @Longitude IS NULL ) AND @ZipCode IS NULL
					  BEGIN
 
						  SELECT ProductHotDealID
							   , HotDealName
							   , HotDeaLonglDescription hDLognDescription
							   , HotDealImagePath
							   , Price hDPrice
							   , SalePrice hDSalePrice
							   , Distance
							   , HotDealTermsConditions hDTermsConditions
							   , HotDealStartDate hDStartDate
							   , HotDealEndDate hDEndDate
							   , HotDealURL hdURL
							   , APIPartnerName
						FROM (SELECT DISTINCT p.ProductHotDealID 
								, p.HotDealName
								, p.HotDeaLonglDescription 
								, HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
																			CASE WHEN WebsiteSourceFlag = 1 THEN 
																						CASE WHEN P.ManufacturerID IS NOT NULL THEN @Config  
																						   +'Images/manufacturer/'  
																						   +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'  
																						   +HotDealImagePath  
																						ELSE @Config  
																						+'Images/retailer/'  
																						+CONVERT(VARCHAR(30),P.RetailID)+'/'  
																						+HotDealImagePath  
																						END  					                         
																			ELSE P.HotDealImagePath  
																			END  
													END  
								, Price
								, SalePrice
								, 0 Distance
								, p.HotDealTermsConditions
								, p.HotDealStartDate
								, p.HotDealEndDate
								, p.HotDealURL
								, A.APIPartnerName 
								FROM UserProduct up
									INNER JOIN HotDealProduct HP ON up.ProductID=HP.ProductID
									INNER JOIN ProductHotDeal P ON p.ProductHotDealID=hp.ProductHotDealID
									--INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HP.ProductHotDealID
									LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID
									WHERE HP.productid=@productid 
									AND  UP.UserID =  @userid
									AND UP.WishListItem = 1
									AND GETDATE() BETWEEN P.HotDealStartDate AND P.HotDealEndDate) HotDeal
						END
											
											
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListProductHotDealInformation'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			
		END;
		 
	END CATCH;
END;




GO
