USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebProductSearch
Purpose					: Used to search the products.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			10th december 2012				SPAN	    Initial Version
-------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[usp_WebProductSearch]
(

	--Input Parameter(s)--	   
	  
	   @SearchKey varchar(255)	 
	
	--Output Variable--
	  
	 , @Status INT OUTPUT
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY	
		
		DECLARE @SearchKey1 VARCHAR(255) = @SearchKey
		--Searches products for the given SearchKey
		SELECT DISTINCT P.ProductID
                , ProductName 
                , ProductShortDescription
                , ScanCode 
        INTO #Temp
        FROM Product P
        WHERE P.ProductID > 0 
        AND (ScanCode LIKE '%' + @SearchKey1 + '%' 
        OR ProductName LIKE '%' + @SearchKey1 + '%')
       
       SELECT T.ProductID
			, ProductName
			, ProductShortDescription
			, ScanCode
			, Category = CASE WHEN PC.CategoryID IS NOT NULL THEN C.ParentCategoryName + ' - ' + C.SubCategoryName ELSE NULL END
       FROM #Temp T
       LEFT JOIN ProductCategory PC ON T.ProductID = PC.ProductID
       LEFT JOIN Category C ON C.CategoryID = PC.CategoryID
        
       

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebProductSearch.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
