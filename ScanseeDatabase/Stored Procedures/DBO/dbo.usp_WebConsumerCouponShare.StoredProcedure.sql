USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerCouponShare]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerCouponShare
Purpose					: 
Example					: usp_WebConsumerCouponShare

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerCouponShare]
(
	 @CouponID int
	
	--Output Variable 
	, @Status int output
	, @CouponExpired bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		 --To get Server Configuration 
			 DECLARE @RetailConfig varchar(50)
		  
			 SELECT @RetailConfig = ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration'
			 
			 --To Get App Media Server Configuration 
		     DECLARE @Config varchar(50)    
		     SELECT @Config=ScreenContent    
		     FROM AppConfiguration     
		     WHERE ConfigurationType='App Media Server Configuration' 
			 
			SELECT  CouponName
				   ,CouponURL
				   ,CouponStartDate
				   ,CouponImagePath = CASE WHEN CouponImagePath IS NULL THEN DBO.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																										CASE WHEN WebsiteSourceFlag = 1 
																											THEN @RetailConfig
																											+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																											+CouponImagePath 
																									    ELSE CouponImagePath 
																									    END
																								 END 
					 END  
				   ,CouponExpireDate
				   ,@Config+'mailer_facebook.png' facebookimg
				   ,@Config+'mailer_scansee.png' scanseeimg
				   ,@Config+'mailer_twitter.png' twitterimg
				   ,@Config+'mailer_arrowscansee.png' arrowscanseeimg
			FROM Coupon c
			LEFT JOIN CouponRetailer cr on c.CouponID = cr.CouponID
			WHERE GETDATE() Between CouponStartDate AND CouponExpireDate AND c.CouponID=@CouponID
			
			SELECT @CouponExpired=CASE WHEN GETDATE()>CouponExpireDate Then 0 else 1 END 
			FROM Coupon
			WHERE CouponID=@CouponID
			
		 --Confirmation of Success.
		 SELECT @Status = 0			
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerCouponShare.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
