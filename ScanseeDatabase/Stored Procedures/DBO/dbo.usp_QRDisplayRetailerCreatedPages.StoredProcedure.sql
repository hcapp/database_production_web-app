USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_QRDisplayRetailerCreatedPages]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_QRDisplayRetailerCreatedPages
Purpose					: To Retrieve the list of Custom pages(Anything Page) associated with the retailer location.
Example					: usp_QRDisplayRetailerCreatedPages

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_QRDisplayRetailerCreatedPages]
(
	  
	  @UserID int
	, @RetailID int
    , @RetailLocationID int
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @Configuration VARCHAR(100)
		DECLARE @QRType INT
		DECLARE @RetailerConfig varchar(50)
		
		 SELECT @RetailerConfig= ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		--Get the Server Configuration.
		SELECT @Configuration = ScreenContent FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'QR Code Configuration'
		AND Active = 1		
		 
		 
		SELECT @QRType = QRTypeID FROM QRTypes WHERE QRTypeName LIKE 'Anything Page' 		 
		
		--Fetch all the Created Pages for the given Retailer Location		
		SELECT DISTINCT QR.QRRetailerCustomPageID pageID
		     , QR.Pagetitle pageTitle
		     , CASE WHEN [Image] IS NULL THEN (SELECT @RetailerConfig + QRRetailerCustomPageIconImagePath FROM QRRetailerCustomPageIcons WHERE QRRetailerCustomPageIconID = QR.QRRetailerCustomPageIconID) 
					ELSE @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + [Image] END pageImage		 
			 , PageLink = CASE WHEN QR.URL IS NULL THEN @Configuration + CAST((SELECT QRTypeCode FROM QRTypes WHERE QRTypeName LIKE 'Anything Page') AS VARCHAR(10)) + '.htm?retId=' + CAST(QRCPA.RetailID AS VARCHAR(10)) + '&retlocId=' + CAST(QRCPA.RetailLocationID AS VARCHAR(10)) + '&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10)) 
						  ELSE QR.URL END
		FROM QRRetailerCustomPage QR
		INNER JOIN QRRetailerCustomPageAssociation QRCPA ON QR.QRRetailerCustomPageID = QRCPA.QRRetailerCustomPageID		
		WHERE QRCPA.RetailID = @RetailID 
		AND QRCPA.RetailLocationID = @RetailLocationID
		AND QR.QRTypeID = @QRType
		AND ((StartDate IS NULL AND EndDate IS NULL) 
		      OR 
		     (GETDATE() BETWEEN StartDate AND EndDate)
		    )
		ORDER BY Pagetitle ASC
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
