USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebManufacturerDataCleanUpAll]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebManufacturerDataCleanUpAll
Purpose					: 
Example					: usp_WebManufacturerDataCleanUpAll

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			2nd Feb 2012	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebManufacturerDataCleanUpAll]
(
	--Output Variable 
	 @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		--All the WebSite Users Manufacturer data is considered for deletion.
		SELECT U.UserID
		INTO #Users
		FROM Users U
		INNER JOIN UserManufacturer UM ON UM.UserID = U.UserID 
		INNER JOIN Manufacturer M ON UM.ManufacturerID = M.ManufacturerID
		WHERE M.ManufName <> 'Scansee Commission Junction'
		AND M.ManufName <> 'Kwikee'
		
		--WHERE BINARY_CHECKSUM(UserName) <> BINARY_CHECKSUM(@UserName)
		
 		SELECT ManufacturerID 
		INTO #Manuf
		FROM UserManufacturer M
		INNER JOIN #Users U ON U.UserID = M.UserID  
		 

		SELECT ProductID 
		INTO #Product
		FROM Product P
		INNER JOIN #MANUF M ON M.ManufacturerID = P.ManufacturerID

		SELECT CouponID
		INTO #Coupon
		FROM Coupon C
		INNER JOIN #MANUF M ON M.ManufacturerID = C.ManufacturerID

		SELECT RebateID 
		INTO #Rebate
		FROM Rebate R
		INNER JOIN #MANUF M ON M.ManufacturerID = R.ManufacturerID

		SELECT ProductHotDealID
		INTO #HotDeal
		FROM ProductHotDeal P
		INNER JOIN #MANUF M ON M.ManufacturerID = P.ManufacturerID

		--Product related tables
		DELETE FROM ProductAttributes
		FROM ProductAttributes PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM ProductCategory
		FROM ProductCategory PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM ProductMedia
		FROM ProductMedia PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM ProductReviews
		FROM ProductReviews PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM  ProductUserHit
		FROM ProductUserHit PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM UserShoppingCartHistoryProduct
		FROM UserShoppingCartHistoryProduct PH
		INNER JOIN UserProduct PA ON PA.UserProductID = PH.UserProductID 
		INNER JOIN #Product P ON P.ProductID = PA.ProductID
		
		DELETE FROM UserProductHistory
		FROM UserProductHistory UPH
		INNER JOIN UserProduct PA ON UPH.UserProductID = PA.UserProductID
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM UserProduct
		FROM UserProduct PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM UserProductShare
		FROM UserProductShare PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID  

		DELETE FROM UserRating
		FROM UserRating PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM RetailLocationProduct 
		FROM RetailLocationProduct PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM RebateProduct 
		FROM RebateProduct PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		DELETE FROM HotDealProduct 
		FROM HotDealProduct PA
		INNER JOIN #Product P ON P.ProductID = PA.ProductID 

		--Coupon retated tables 

		DELETE FROM CouponProduct
		FROM CouponProduct CP
		INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  
		
		DELETE FROM CouponProduct
		FROM #Product CP
		INNER JOIN CouponProduct C ON C.ProductID = CP.ProductID

		DELETE FROM CouponRetailer
		FROM CouponRetailer CP
		INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  

		DELETE FROM CouponUserHit
		FROM CouponUserHit CP
		INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  

		DELETE FROM UserCouponGallery
		FROM UserCouponGallery CP
		INNER JOIN #Coupon C ON C.CouponID = CP.CouponID   

		DELETE FROM UserPayHistory
		FROM UserPayHistory CP
		INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  

		DELETE FROM UserPayout
		FROM UserPayout CP
		INNER JOIN #Coupon C ON C.CouponID = CP.CouponID  

		--Rebate related tables

		DELETE FROM RebateProduct
		FROM RebateProduct RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		DELETE FROM RebateRetailer
		FROM RebateRetailer  RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		DELETE FROM RebateUserHit
		FROM RebateUserHit  RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		DELETE FROM ScanHistory
		FROM ScanHistory  RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		DELETE FROM ScanHistory
		FROM ScanHistory  RP
		INNER JOIN #Product R ON R.ProductID = RP.ProductID
		 
		DELETE FROM UserPayHistory
		FROM UserPayHistory  RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		DELETE FROM UserRebateGallery
		FROM UserRebateGallery  RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		DELETE FROM UserPayout
		FROM UserPayout  RP
		INNER JOIN #Rebate R ON R.RebateID = RP.RebateID 

		--ProductHotDeal related tables.

		DELETE FROM  ProductHotDealRetailLocation
		FROM ProductHotDealRetailLocation P
		INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID  

		DELETE FROM ProductHotDealUserHit
		FROM ProductHotDealUserHit P
		INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 

		DELETE FROM  ProductHotDealLocation
		FROM ProductHotDealLocation P
		INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 

		--Manufacturer related Tables
		DELETE FROM UserManufacturer
		FROM UserManufacturer UM
		INNER JOIN #Manuf M ON M.ManufacturerID = UM.ManufacturerID 


		DELETE FROM ManufacturerContact
		FROM ManufacturerContact UM
		INNER JOIN #Manuf M ON M.ManufacturerID = UM.ManufacturerID 
		
		DELETE FROM  ManufacturerCategory
		FROM ManufacturerCategory UM
		INNER JOIN #Manuf M ON M.ManufacturerID = UM.ManufacturerID 

		--To delete Coupon data
		DELETE FROM Coupon 
		WHERE CouponID IN (SELECT CouponID FROM #Coupon)

		--To delete Rebate data
		DELETE FROM Rebate 
		WHERE RebateID IN (SELECT RebateID FROM #Rebate)

		--To delete HotDeal
		DELETE FROM ProductHotDeal 
		WHERE ProductHotDealID IN (SELECT ProductHotDealID FROM #HotDeal)


DELETE FROM ManufacturerBillingDetails WHERE ManufacturerID IN(SELECT ManufacturerID FROM #Manuf)
		--To delete Product 
		DELETE FROM Product 
		WHERE ProductID IN (SELECT ProductID FROM #Product)

		--To delete Manufacturer
		DELETE FROM Manufacturer 
		WHERE ManufacturerID IN (SELECT ManufacturerID FROM #Manuf)
	
		DELETE FROM UserFirstUseLogin WHERE UserID IN (SELECT UserID FROM #Users)
		--To delete User
		DELETE FROM Users 
		WHERE UserID IN (SELECT UserID FROM #Users)

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebManufacturerDataCleanUpAll.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
