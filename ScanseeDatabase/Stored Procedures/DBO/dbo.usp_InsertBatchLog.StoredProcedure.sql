USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertBatchLog]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_InsertBatchLog]
Purpose					: To update the Log when the URL Execution fails.
Example					: [usp_InsertBatchLog]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			3rd July 2012	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_InsertBatchLog]
(
	  @APIPartnerID INT	
	, @APIStatus BIT
	, @Reason VARCHAR(1000)
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			INSERT INTO APIBatchLog(ExecutionDate
									,Status
									,Reason
									,APIPartnerName
									,APIRowCount
									,ProcessedRowCount
									,DuplicatesCount
									,ExpiredCount
									,MandatoryFieldsMissingCount
									,InvalidRecordsCount
									,ExistingRecordCount
									,APIPartnerID)
						SELECT GETDATE()
						     , @APIStatus
						     , @Reason
						     , (SELECT APIPartnerName FROM APIPartner WHERE APIPartnerID = @APIPartnerID)
						     , 0
						     , 0
						     , 0
						     , 0
						     , 0
						     , 0
						     , 0
						     , @APIPartnerID						       
						

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_InsertBatchLog].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
