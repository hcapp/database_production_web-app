USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MaintenanceIndexReBuild]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MaintenanceIndexReorganise
Purpose					: Used to Rebuild the Indexes whose fragmentation % > 40.
Example					: usp_MaintenanceIndexReorganise

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0		20th August 2016	Prakash Chinasamy 	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MaintenanceIndexReBuild]
(
	  
		
	--Output Variable 
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
	
		DECLARE @objectid int;
		DECLARE @indexid int;
		DECLARE @schemaname nvarchar(130); 
		DECLARE @objectname nvarchar(130); 
		DECLARE @indexname nvarchar(130);
		DECLARE @frag float;
		DECLARE @command nvarchar(4000); 
		
		SELECT object_id AS objectid
		     , index_id AS indexid
		     --, partition_number AS partitionnum
		     , avg_fragmentation_in_percent AS frag
		INTO #work_to_do
		FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, NULL)
		WHERE avg_fragmentation_in_percent > 10.0 AND index_id > 0;
		
		
		-- Declare the cursor for the list of partitions to be processed.
		DECLARE TableCursor CURSOR FOR SELECT * FROM #work_to_do;

		-- Open the cursor.
		OPEN TableCursor;

		-- Loop through the partitions.
	WHILE (1=1)
		BEGIN;
			FETCH NEXT
			   FROM TableCursor
			   INTO @objectid, @indexid, @frag;
			IF @@FETCH_STATUS < 0 BREAK;
			SELECT @objectname = QUOTENAME(o.name) 
			FROM sys.objects AS o
			JOIN sys.schemas as s ON s.schema_id = o.schema_id
			WHERE o.object_id = @objectid;
			SELECT @indexname = QUOTENAME(name)
			FROM sys.indexes
			WHERE  object_id = @objectid AND index_id = @indexid;			

			--ReOrganise the index.
			IF @frag >= 40.0
				SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REBUILD';
				EXEC (@command);
			PRINT N'Executed: ' + @command;
		END;

	-- Close and deallocate the cursor.
	CLOSE TableCursor;
	DEALLOCATE TableCursor;
	
	--Update the statistics once the Rebuilding is done.
	EXEC sp_UpdateStats
		
	END TRY
		
	BEGIN CATCH		
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MaintenanceIndexReorganise.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;		 
	END CATCH;
END;

GO
