USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebEventPatternDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebEventPatternDisplay
Purpose					: To display List of RecurrencePatterns.
Example					: usp_WebEventPatternDisplay

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014      SPAN             1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebEventPatternDisplay]
(   

	--Output Variable 
      @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY

			SELECT HcEventRecurrencePatternID recurrencePatternID
				 , RecurrencePattern recurrencePatternName
			FROM HcEventRecurrencePattern	


			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebEventPatternDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
