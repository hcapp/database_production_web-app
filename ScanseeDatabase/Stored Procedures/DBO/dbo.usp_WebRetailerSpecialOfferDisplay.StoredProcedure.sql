USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerSpecialOfferDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebRetailerSpecialOfferDisplay]
Purpose                  : To display the Welcome pages created by the given Retailer.
Example                  : [usp_WebRetailerSpecialOfferDisplay]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               9th Aug 2012        Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerSpecialOfferDisplay]
(

      --Input Input Parameter(s)--  
      
        @RetailID INT
      , @SearchParameter VARCHAR(1000)
      , @LowerLimit int
      , @RecordCount INT
	  , @SpecialOfferPageID Varchar(MAX) --Comma separated SpecialOfferPageids
      , @SortOrder VARCHAR (255)
      
      --Output Variable--
	  , @RowCount INT OUTPUT
	  , @NextPageFlag BIT OUTPUT
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
		     DECLARE @UpperLimit INT    
			 DECLARE @MaxCnt INT   
			 
			 IF @SortOrder IS NOT NULL
			 BEGIN
					--Store Sort order to temp table
				 CREATE TABLE #Temp(Rownum INT IDENTITY(1,1),SortID varchar(100))
				 INSERT INTO #Temp (SortID)
				 SELECT R.Param 
				 FROM dbo.fn_SplitParam(@SortOrder, ',') R

				 --Store SpecialOfferPageIDs to temp1 table
				 CREATE TABLE #Temp1(Rownum INT IDENTITY(1,1),PageID varchar(100))
				 INSERT INTO #Temp1(PageID)
				 SELECT R.Param 
				 FROM dbo.fn_SplitParam(@SpecialOfferPageID,',') R
             
				 --Store Sort order and SpecialOfferPageIDs together in temp2 table
				 CREATE TABLE #Temp2(Rownum INT IDENTITY(1,1),SortID VARCHAR(255),PageID VARCHAR(100))
				 INSERT INTO #Temp2 (SortID,PageID )
				 SELECT	T1.SortID 
					   ,T2.PageID  
				 FROM #Temp T1 
				 INNER JOIN #Temp1 T2 ON T1.Rownum =T2.Rownum 

		   
				--Update SpecialOfferPage based on sorting.
				UPDATE QRRetailerCustomPage SET SortOrder = T.SortID 
				FROM QRRetailerCustomPage R
				INNER JOIN #temp2 T ON T.PageID =R.QRRetailerCustomPageID  
			 END
			 
			      
      
			  --To get the row count for pagination.      
			 --DECLARE @ScreenContent Varchar(100)    
			 --SELECT @ScreenContent = ScreenContent       
			 --FROM AppConfiguration       
			 --WHERE ScreenName = 'All'     
			 --AND ConfigurationType = 'Website Pagination'    
			 --AND Active = 1   
		 
		    SET @UpperLimit = @LowerLimit + @RecordCount   
		    --To Fetch the Retailer Media server Configuaration      
		
		
		    SELECT RowNum = ROW_NUMBER()OVER (ORDER BY ISNULL(SortOrder, 1000000),QRRetailerCustomPageID ASC)
		         , QRRetailerCustomPageID
		         , Pagetitle 
				 , [Start Date]
				 , [Start Time]  
				 , [End Date] 
				 , [End Time]
				 , SortOrder
			INTO #Temp5
			FROM(    
              
				SELECT DISTINCT QRC.QRRetailerCustomPageID  as QRRetailerCustomPageID
							, QRC.Pagetitle as Pagetitle
							, CONVERT(DATE,QRC.StartDate) as [Start Date]
							, CONVERT(Time,QRC.StartDate) as [Start Time]
							, CONVERT(DATE,QRC.EndDate) as [End Date] 
							, CONVERT(Time,QRC.EndDate) as [End Time]
							, SortOrder	
				FROM QRRetailerCustomPage  QRC
				INNER JOIN QRTypes QRT ON QRC.QRTypeID = QRT.QRTypeID
				WHERE QRT.QRTypeName LIKE 'Special Offer Page' 
				AND QRC.RetailID =@RetailID 
				AND Pagetitle  LIKE CASE WHEN @SearchParameter IS NOT NULL THEN '%'+@SearchParameter+'%' ELSE '%' END) AnythingPage     
				
			SELECT @MaxCnt = COUNT(RowNum) FROM #Temp5 
		    --Output Total no of Records
		    SET @RowCount = @MaxCnt 
		
		    --CHECK IF THERE ARE SOME MORE ROWS        
		    SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  
		
	    	--Display the records in the given limit
		    SELECT RowNum as iRowId
		         , QRRetailerCustomPageID
		         , Pagetitle 
				 , [Start Date] 
				 , [Start Time]
				 , [End Date]
				 , [End Time] 	
				 , SortOrder		
		    FROM #Temp5	
		    WHERE RowNum BETWEEN (@LowerLimit + 1) AND (@UpperLimit)                  
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebRetailerSpecialOfferDisplay.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;






GO
