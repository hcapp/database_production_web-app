USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerWishListSearchAddProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerWishListSearchAddProduct  
Purpose               : To search for the product.  
Example               : usp_WebConsumerWishListSearchAddProduct   
 
Version    Date              Author         Change Description  
---------------------------------------------------------------   
1.0   7th June 2011 Padmapriya M Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerWishListSearchAddProduct]  
(  
   @UserID int  
 , @ProductID varchar(max)  
 
 --User Tracking Inputs
 , @MainMenuID int
   
 --Output Variable   
 , @ProductExists Bit output
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
  
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
    
   SET @ProductExists=0
   
   -- To fetch Product which already exists for the user (Active and Inactive Products). 
   
    IF EXISTS (Select 1 from UserProduct where UserID =@UserID AND ProductID =@ProductID AND WishListItem  =1)
    BEGIN
      SET @ProductExists =1
    END   
   
   IF EXISTS (Select 1 from UserProduct where UserID =@UserID AND ProductID =@ProductID AND WishListItem  =0)
   BEGIN
        Update UserProduct SET WishListItem  =1,WishListAddDate =GETDATE()
		WHERE UserID =@UserID 
		AND ProductID =@ProductID
		AND WishListItem  =0
		
		 --User Tracking Section
		Update ScanSeeReportingDatabase..UserProduct SET WishListItem  =1,WishListAddDate =GETDATE()
		WHERE UserID =@UserID 
		AND ProductID =@ProductID 
		AND WishListItem  =0		
   END
   
   IF NOT EXISTS (Select 1 from UserProduct where UserID =@UserID AND ProductID =@ProductID)
	   BEGIN
			INSERT INTO [UserProduct] ([UserID]  
									  ,[ProductID]  
									  ,[MasterListItem]  
									  ,WishListAddDate   
									  ,[WishListItem]  
									  ,[TodayListtItem]  
									  ,[ShopCartItem])  
			VALUES(@UserID  
				 , @ProductID     
				 , 0  
				 , GETDATE()  
				 , 1  
				 , 0  
				 , 0 ) 
				 
			 --User Tracking Section	 
			 INSERT INTO ScanSeeReportingDatabase..UserProduct([UserID]  
															  ,[ProductID]  
															  ,[MasterListItem]  
															  ,WishListAddDate   
															  ,[WishListItem]  
															  ,[TodayListtItem]  
															  ,[ShopCartItem])
			  VAlUES( @UserID  
					 , @ProductID     
					 , 0  
					 , GETDATE()  
					 , 1  
					 , 0  
					 , 0)	
			  	 
	  END  
   

   
   
   
   --Confirmation of Success.  
   SELECT @Status = 0  
  COMMIT TRANSACTION    
   
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerWishListSearchAddProduct.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;


GO
