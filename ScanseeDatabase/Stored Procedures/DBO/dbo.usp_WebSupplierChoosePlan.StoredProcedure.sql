USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierChoosePlan]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierChoosePlan
Purpose					: To insert the details of the plan choosed by the supplier
Example					: usp_WebSupplierChoosePlan

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Jan 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierChoosePlan]
(
		 @ManufacturerID int
		,@UserID int
		,@BillingPlanManufacturerIDs varchar(1000)                  --Comma Separated values 
		,@Quantitys varchar(1000)                                   --Comma Separated values  
		,@TotalPrice money		
		,@DiscountPlanManufacturerID int
		,@ManufacturerProductFeeFlag bit
			
	--Output Variable 
		, @Status int output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION 
		
		SELECT IDENTITY(INT, 1,1) Row						--To split the comma separated Plans.
		     , BillingPlan
		    INTO #BillingPlans
		    
		FROM (SELECT [Param] BillingPlan
		      FROM  dbo.fn_SplitParam(@BillingPlanManufacturerIDs, ',')) BillingPlans
		      
		 SELECT IDENTITY(INT, 1,1) RowNum					--To split the comma separated Quantity.
		     , Quantity
		    INTO #Quantity
		    
		FROM (SELECT [Param] Quantity
		      FROM  dbo.fn_SplitParam(@Quantitys, ',')) Quantity
		
		--INSERT INT0 THE ManufacturerBillingDetails TABLE.
			INSERT INTO ManufacturerBillingDetails(
													    UserID
													  ,	ManufacturerID
													  , ManufacturerBillingPlanID
													  , Quantity
													  , TotalPrice
													  , ManufacturerProductFeeFlag		
													  , ManufacturerDiscountPlanID											 
												   )
										
					                            SELECT @UserID
					                                 , @ManufacturerID
					                                 , B.BillingPlan
					                                 , CASE WHEN Quantity = 0 THEN 'Waived' ELSE Quantity END          --If the Price is waived then we dont have any quantity.
					                                 , @TotalPrice
					                                 , @ManufacturerProductFeeFlag
					                                 , @DiscountPlanManufacturerID
					                            FROM #BillingPlans B
					                            INNER JOIN #Quantity Q ON B.Row = Q.RowNum
			
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierChoosePlan.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
