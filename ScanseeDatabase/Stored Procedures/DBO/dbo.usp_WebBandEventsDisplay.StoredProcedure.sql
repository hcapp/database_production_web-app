USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandEventsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebBandEventsDisplay]
Purpose					: To display List of Events for given Band.
Example					: [usp_WebBandEventsDisplay]

History
Version		   Date			Author		Change Description
--------------------------------------------------------------- 
1.0			04/20/2016      Bindu T A        1.0 - usp_WebBandEventsDisplay
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandEventsDisplay]
(   
    --Input variable	  
	  @RetailID int
	, @SearchKey Varchar(2000)
	, @LowerLimit int 
	, @Fundraising bit 

	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int
			SET @Fundraising = ISNULL(@Fundraising,0)	

			DECLARE @bandID INT 
			SET @BandID = @RetailID
			  
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit + 
			 ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = 'HubCitiWeb' 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = 
			ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'

			--To Fetch the Band Media server Configuaration
      
			DECLARE @RetailerConfig varchar(255)    
			SELECT @RetailerConfig=
			ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration' AND Active =1  
		

			--To display List fo Events
			CREATE TABLE #BandEvents(Rownum int IDENTITY(1,1)
								,BandEventID int
								,BandEventName varchar(1000)
								,BandImagepath Varchar(1000)
								,ShortDescription varchar(1000)
								,LongDescription varchar(MAX)
								,BussinessEvent INT
								,StartDate DATETIME
								,EndDate DATETIME
								,StartTime DATETIME
								,EndTime DATETIME
								,moreInfoURL varchar(1000)
								,EventLocationKeyword varchar(1000)
								,Address VARCHAR(500)
								,City VARCHAR(50)
								,State VARCHAR(10)
								,PostalCode VARCHAR(10)
								,GeoErrorFlag INT
								)

			INSERT INTO #BandEvents(BandEventID
							    ,BandEventName 
								,BandImagepath
								,ShortDescription
								,LongDescription
								,BussinessEvent
								,StartDate
								,EndDate
								,StartTime
								,EndTime
								,moreInfoURL
								,EventLocationKeyword
								,Address
								,City 
								,State 
								,PostalCode 
								,GeoErrorFlag)
				SELECT DISTINCT E.HcBandEventID 
							   ,HcBandEventName  
							   ,@RetailerConfig + CONVERT(VARCHAR(30),E.BandID) +'/' +ImagePath BandImagepath
							   ,ShortDescription
							   ,LongDescription 								
							   ,BussinessEvent
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   ,EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
							   ,MoreInformationURL moreInfoURL
							   ,EL.EventLocationKeyword
							   ,Address = EL.Address
							   ,City = EL.City
							   ,State = EL.State
							   ,PostalCode = El.PostalCode	
							   ,GeoErrorFlag = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(EL.GeoErrorFlag,0) END
							   								
				FROM HcBandEvents E 
				LEFT JOIN HcBandEventsAssociation RE ON RE.HcBandEventID = E.HcBandEventID
				--LEFT JOIN RetailLocation RL ON RL.RetailLocationID = RE.RetailLocationID AND RL.Active = 1
				LEFT JOIN HcBandEventLocation EL on EL.HcBandEventID = E.HcBandEventID
				WHERE E.BandID = @BandID 
				AND  ((@Fundraising = 1 AND GETDATE() <= ISNULL(EndDate, GETDATE()+1)) OR @Fundraising = 0)
				AND E.Active = 1				
				AND ((@SearchKey IS NOT NULL AND HcBandEventName LIKE '%'+ @SearchKey +'%') OR (@SearchKey IS NULL AND 1=1))
				ORDER BY E.HcBandEventName                

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #BandEvents
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SET @MaxCnt = ISNULL(@MaxCnt,0)

			SELECT Rownum	
			      ,BandEventID AS eventID 
			      ,BandEventName AS eventName 
				  ,BandImagepath AS Imagepath
			      ,ShortDescription
                  ,LongDescription 								
				  ,BussinessEvent
				  ,StartDate eventStartDate
				  ,EndDate eventEndDate
				  ,StartTime eventTime
				  ,EndTime
				  ,EventLocationKeyword
				  ,Address
				  ,City
				  ,State
				  ,PostalCode	
				  ,GeoErrorFlag					
				  ,moreInfoURL	  				  											
			FROM #BandEvents
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY RowNum 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebBandEventsDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	
			INSERT  INTO ScanseeValidationErrors(ERRORCODE,ERRORLINE,ERRORDESCRIPTION,ERRORPROCEDURE)
            VALUES(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())		
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
