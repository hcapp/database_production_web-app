USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCategoriesSubCategories_Script_Old]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [dbo].[UpdateCategoriesSubCategories_Script]
Purpose					: To associate Retailers with Categories/Sub-categories
Example					: [dbo].[UpdateCategoriesSubCategories_Script]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			8 Aug 2016	    Sagar Byali	    1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[UpdateCategoriesSubCategories_Script_Old]
(
	 --OutPut Variable	
	   @Status int output
	 , @ErrorNumber int output  
	 , @ErrorMessage varchar(1000) output  
  
)
AS
BEGIN


	BEGIN TRY

	

------EXEC sp_RENAME '[DataUpload_AddCAT].[category name]', 'category', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[Sub-Category Name]', 'subcategory', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail id]', 'retailid', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail locationid]', 'retaillocationid', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail location id]', 'retaillocationid', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[address]', 'address1', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[postal code]', 'postalcode', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[contact phone]', 'contactphone', 'COLUMN'
------EXEC sp_RENAME '[DataUpload_AddCAT].[retail name]', 'retailname', 'COLUMN'


IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'retail name')
EXEC sp_RENAME '[DataUpload_AddCAT].[retail name]', 'retailname', 'COLUMN'

IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'category name')
EXEC sp_RENAME '[DataUpload_AddCAT].[category name]', 'category', 'COLUMN'

IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'Sub-Category Name')
EXEC sp_RENAME '[DataUpload_AddCAT].[Sub-Category Name]', 'subcategory', 'COLUMN'

IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'retail id')
EXEC sp_RENAME '[DataUpload_AddCAT].[retail id]', 'retailid', 'COLUMN'

IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'retail location id')
EXEC sp_RENAME '[DataUpload_AddCAT].[retail location id]', 'retaillocationid', 'COLUMN'

IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'address')
EXEC sp_RENAME '[DataUpload_AddCAT].[address]', 'address1', 'COLUMN'

IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'postal code')
EXEC sp_RENAME '[DataUpload_AddCAT].[postal code]', 'postalcode', 'COLUMN'


IF  EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS 	WHERE TABLE_NAME = 'DataUpload_AddCAT' AND COLUMN_NAME = 'contact phone')			
EXEC sp_RENAME '[DataUpload_AddCAT].[contact phone]', 'contactphone', 'COLUMN'


IF NOT EXISTS (SELECT * FROM scansee.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'DataUpload_AddCAT' AND COLUMN_NAME like '%phone%')
alter table DataUpload_AddCAT add contactphone varchar(50)

	
--to insert data from excel sheet into Archieve---	
insert into DataUpload_AddRetailerBusinessCategoriesSubCategories_Archieve(Category,
																 SubCategory,
																 RetailName,
																 RetailID,
																 RetailLocationID,
																 Address1,
																 City,
																 State,
																 PostalCode,
																 contactphone,DateCreated
																 )

													select distinct Category,
																	SubCategory,
																	RetailName,
																	RetailID,
																	RetailLocationID,
																	Address1,
																	City,
																	State,
																	PostalCode,
																	contactphone,DateCreated
													from DataUpload_AddRetailerBusinessCategoriesSubCategories

													
--To truncate old data---
truncate  table DataUpload_AddRetailerBusinessCategoriesSubCategories

													
--To insert data into Retailer upload table----														
insert into DataUpload_AddRetailerBusinessCategoriesSubCategories(Category,
																 SubCategory,
																 RetailName,
																 RetailID,
																 RetailLocationID,
																 Address1,
																 City,
																 State,
																 PostalCode
																 ,contactphone
																 )

											select distinct Category,
													SubCategory,
													RetailName,
													RetailID,
													RetailLocationID,
													Address1,
													City,
													State,
													PostalCode,
													contactphone
													from [DataUpload_AddCAT]

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Florist' 
					where Category='Florists'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Landscaping/Lawn Care' 
					where Category='Landscapping/Lawn Care'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Dry Cleaners/Laundromats' 
					where Category='Dry Cleaners/Luandromats'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = null
					where SubCategory='N/A'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = 'Cabins / B&B / Houses' 
					where SubCategory='Cabins/B&B/Houses'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Windshield Installation & Repair' 
					where Category='Windshield Installation/Repair'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = 'Hotel / Motel' 
					where SubCategory='Hotel/Motel'

			
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set SubCategory = 'Campground' 
					where SubCategory='Campgrounds'

			
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Communications & Media' 
					where category='Communication/Media'

		
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Communications & Media' 
					where category='Communications/Media'



					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Construction/Manufacturing' 
					where category='Construction/Manufactoring'


		
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Financial' 
					where category='Financial Services'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'NonProfit' 
					where category='Non-Profit'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'NonProfit' 
					where category='Non Profit'


					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Specialty/Gift Stores' 
					where category='Speicalty/Gift Shops'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Home Decor/Interior Design' 
					where Category='Home Décor/Interior Design'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Computer Sales & Service' 
					where Category='Computer Sales & Services'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Banks/Credit Union' 
					where Category='Banks/Credit Unions'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set Category = 'Automotive Dealers' 
					where Category='Automobile Dealers'



					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Salon/Barbers' 
					where category='Salon/Barber'

					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set category = 'Hospitals/Medical' 
					where category='Hospital/Medical'

							
					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subcategory = 'Hot Dogs' 
					where subcategory='Hotdogs'



					update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subcategory = 'Wine Bar' 
					where subcategory='Wine Bars'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subCategory = 'Resorts / Spas' 
					where subCategory='Resorts/Spas'

						update DataUpload_AddRetailerBusinessCategoriesSubCategories
					set subCategory = 'Campgrounds' 
					where subCategory='Campground' and category ='lodging'

				 


			update DataUpload_AddRetailerBusinessCategoriesSubCategories
			set catid = BusinessCategoryid
			from BusinessCategory B
			INNER JOIN DataUpload_AddRetailerBusinessCategoriesSubCategories Z ON Z.Category = B.BusinessCategoryDisplayValue
			where Subcategory is null

			update DataUpload_AddRetailerBusinessCategoriesSubCategories
			set catid = B.BusinessCategoryid , SubCatID = T.HcBusinessSubCategoryID
			from BusinessCategory B
			INNER JOIN DataUpload_AddRetailerBusinessCategoriesSubCategories Z ON Z.Category = B.BusinessCategoryDisplayValue
			INNER JOIN HcBusinessSubCategoryType TT ON TT.BusinessCategoryID = B.BusinessCategoryID
			INNER JOIN HcBusinessSubCategory T ON T.HcBusinessSubCategoryTypeID = TT.HcBusinessSubCategoryTypeID AND T.BusinessSubCategoryName = Z.Subcategory
			where Subcategory is not null



		

			------------Categories with no subcategories------------------------
			insert into RetailerBusinessCategory 
			select distinct z.retailid,z.catid,getdate(),null,null 
			from DataUpload_AddRetailerBusinessCategoriesSubCategories z
			left join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID
			where cc.RetailerID is null and z.subcatid is null and Z.catid is not null


			------------Categories with subcategories only----------------------------
			insert into RetailerBusinessCategory
			select distinct  z.retailid,z.catid,getdate(),null,z.subcatid
			from DataUpload_AddRetailerBusinessCategoriesSubCategories z
			left join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID 
			and cc.BusinessSubCategoryID = z.subcatid
			where cc.RetailerID is null and z.subcatid is not null

			--------------------------------------------------------------------------------------------------------------------------------------------------

			------------Categories with subcategories only along with retaillocations

			insert into HcRetailerSubCategory(RetailID,RetailLocationID,HcBusinessSubCategoryID,DateCreated,DateModified,CreatedUserID,BusinessCategoryID)
			select distinct z.retailid,z.retaillocationid,z.subcatid,getdate(),null,1,z.catid
			from DataUpload_AddRetailerBusinessCategoriesSubCategories z
			left join HcRetailerSubCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailID = z.RetailID and cc.HcBusinessSubCategoryID = z.subcatid 
			and cc.RetailLocationID = z.retaillocationid
			where cc.RetailID is null and cc.RetailLocationID is null and z.subcatid is not null and cc.BusinessCategoryID is null and cc.HcBusinessSubCategoryID is null


			--drop table #DataUpload_AddRetailerBusinessCategoriesSubCategories
			select * into #DataUpload_AddRetailerBusinessCategoriesSubCategories  from DataUpload_AddRetailerBusinessCategoriesSubCategories

			--deleting inserted category data only
			delete from #DataUpload_AddRetailerBusinessCategoriesSubCategories
			from #DataUpload_AddRetailerBusinessCategoriesSubCategories z
			inner join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID


			--deleting inserted business category & subcategory data 
			delete from #DataUpload_AddRetailerBusinessCategoriesSubCategories
			from #DataUpload_AddRetailerBusinessCategoriesSubCategories z
			inner join RetailerBusinessCategory CC ON CC.BusinessCategoryID = Z.catid and cc.RetailerID = z.RetailID and cc.BusinessSubCategoryID = z.subcatid
			inner join HcRetailerSubCategory C ON C.BusinessCategoryID = Z.catid and c.RetailID = z.RetailID and c.HcBusinessSubCategoryID = z.subcatid and c.RetailLocationID = z.retaillocationid


			select * from #DataUpload_AddRetailerBusinessCategoriesSubCategories

			truncate table DataUpload_AddCAT



			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_HcFilterOptionList].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





















GO
