USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebProductDetails
Purpose					: To display the basic details of the selected product.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			29th May 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebProductDetails]
(

	--Input Input Parameter(s)--	   
	  
	   @ProductID int	 
	
	--Output Variable--
	  
	 , @STATUS INT OUTPUT
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		SELECT    ProductID
				, ProductName
		        , ProductShortDescription
		        , ProductLongDescription
				, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config +CONVERT(VARCHAR(30),ManufacturerID)+'/' + ProductImagePath
								     ELSE ProductImagePath END
				, ModelNumber
			    , WarrantyServiceInformation
			    , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')
		FROM Product 
		WHERE ProductID = @ProductID		
		
		SET @STATUS = 0
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebProductDetails.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
