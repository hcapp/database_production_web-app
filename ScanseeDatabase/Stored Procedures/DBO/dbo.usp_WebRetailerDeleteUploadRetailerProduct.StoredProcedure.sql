USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerDeleteUploadRetailerProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerDeleteUploadRetailerProduct
Purpose					: To clear the data of the retailer from staging area after moving to production..
Example					: usp_WebRetailerDeleteUploadRetailerProduct

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			05 Jan 2012	   Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerDeleteUploadRetailerProduct]
(
	 @RetailID int
	,@UserID int
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
    
	  BEGIN TRY
	  BEGIN TRANSACTION
	  
	    -- To clear the data of the retailer from staging area after moving to production.
		DELETE FROM UploadRetailLocationProduct
		WHERE UserID = @UserID
		AND RetailerID = @RetailID
		
		--Confirmation of Success.
		SET @Status = 0							
									
     COMMIT TRANSACTION								
	 END TRY
	
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerDeleteUploadRetailerProduct.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			set @Status = 1
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;




GO
