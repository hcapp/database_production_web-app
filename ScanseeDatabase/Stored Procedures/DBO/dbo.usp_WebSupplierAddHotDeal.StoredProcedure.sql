USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierAddHotDeal]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierHotDealAdd
Purpose					: To Add a new Hot Deal by the Supplier.
Example					: usp_WebSupplierHotDealAdd

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			6th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierAddHotDeal]               
(

	--Input Parameter(s)--
	 
	 --Inputs for ProductHotDeal Table
	  @ManufacturerID int
	, @HotDealName  varchar(255)
    , @RegularPrice money
    , @SalePrice money
    , @HotDealDescription  varchar(1000)
    , @HotDealLongDescription  varchar(3000)
    , @HotDealTerms  varchar(1000)
    , @URL  varchar(1000)
    , @DealStartDate date
    , @DealStartTime time
    , @DealEndDate date
    , @DealEndTime time
    , @HotDealTimeZoneID int 
    , @CategoryID int
    --Product HotDealLocation Inputs.
   --, @City varchar(max) 
    --, @State varchar(max)			Commented because the Prototype is changed to take the Population Centre ID as input.
    
    , @PopulationCentreID VARCHAR(MAX) 
   -- , @PostalCode varchar(10)
    --, @HotDealLatitude float
    --, @HotDealLongitude float 
    
    --Product HotDealRetailLocation Inputs.
    , @RetailID int
    , @RetailLocationID int
     
     --Inputs for HotDealProduct Table   
  
    , @ProductID varchar(max)
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION		
		
		DECLARE @ProductHotDealID INT
		DECLARE @APIPartnerID INT
		SELECT @APIPartnerID = APIPartnerID FROM APIPartner WHERE APIPartnerName = 'ScanSee'
 
		   -- Cerate a record in Product Hot Deal Table.   
		   INSERT INTO ProductHotDeal( APIPartnerID
									 , HotDealName
									 , Price
									 , SalePrice
									 , HotDealShortDescription
									 , HotDeaLonglDescription
									 , HotDealTermsConditions
									 , HotDealURL
									 , HotDealStartDate
									 , HotDealEndDate  
									 , CreatedDate
									 , ManufacturerID
									 , HotDealTimeZoneID
									 , WebsiteSourceFlag
									 , CategoryID
								  )	
							VALUES(     @APIPartnerID
									  , @HotDealName
									  , @RegularPrice
									  , @SalePrice
									  , @HotDealDescription
									  , @HotDealLongDescription
									  , @HotDealTerms
									  , @URL
									  , CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'') AS DATETIME)
									  , CAST(@DealEndDate AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME)
									  , GETDATE()	
									  , @ManufacturerID
									  , @HotDealTimeZoneID
									  , 1
									  , @CategoryID
								  )
				
          --Capture the inserted hot deal ID.				
          SET @ProductHotDealID = SCOPE_IDENTITY()            			     
            
          --If the Hot Deal is associated with the City.  								  
          IF (@PopulationCentreID IS NOT NULL)
          BEGIN        
          INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]										 
											  )
										SELECT @ProductHotDealID
										     , City
										     , [State]
										FROM dbo.fn_SplitParam(@PopulationCentreID, ',') P
										INNER JOIN PopulationCenterCities PC ON P.Param = PC.PopulationCenterID										
			
		  END
										     
		  IF(@ProductID IS NOT NULL)
		  BEGIN																--Product Association is not mandated if the deal is w.r.t city and state.
		  INSERT INTO HotDealProduct(
										ProductHotDealID
									  , ProductID
									)
						SELECT @ProductHotDealID
						     , [Param]
						FROM dbo.fn_SplitParam(@ProductID, ',') 
          
          END
          --Below code works when state is multi select and City is in the "State-City" format
			--INSERT INTO ProductHotDealLocation (
			--										ProductHotDealID												 
			--									  , City
			--									  , [State]												 
			--									)
			--				SELECT @ProductHotDealID
			--				     , SUBSTRING([Param], CHARINDEX('-',[Param]) + 1, LEN([Param])-CHARINDEX('-',[Param]))  --Compute the State and City form th State-City Combination.
			--				     , SUBSTRING([Param], 1, CHARINDEX('-',[Param])-1)							    
			--				FROM dbo.fn_SplitParam(@City, ',')  							
			      							
          							     
          	
          
          --If Hot Deal is associated with the Retail Location.
          IF (@RetailID IS NOT NULL AND @RetailLocationID IS NOT NULL)
          BEGIN
			
				INSERT INTO ProductHotDealRetailLocation(
															ProductHotDealID
														  , RetailLocationID
														)
											SELECT @ProductHotDealID
											     , [Param]
											FROM dbo.fn_SplitParam(@RetailLocationID, ',')
                INSERT INTO ProductHotDealLocation(ProductHotDealID												 
												  , City
												  , [State])
										SELECT @ProductHotDealID
										     , RL.City
											 , RL.State
										FROM RetailLocation RL 
										WHERE RetailLocationID = @RetailLocationID				
													
		  END
			
			
		SELECT DISTINCT UserID
		              ,ProductID
		              ,DeviceID
		              ,ProductHotDealID		              
		        INTO #Temp
		        FROM (
		        SELECT U.UserID
		              ,UP.ProductID
		              ,UD.DeviceID 
		              ,HD.ProductHotDealID  
		              , Distance= (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(Latitude  / 57.2958) * COS((Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 
		              ,LocaleRadius 
		        from ProductHotDeal PH
		        INNER JOIN HotDealProduct  HD ON HD.ProductHotDealID =PH.ProductHotDealID AND PH.ProductHotDealID=@ProductHotDealID        
		        INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID =HD.ProductHotDealID 
		       -- INNER JOIN RetailLocation RL ON RL.RetailLocationID =PRL.RetailLocationID  
		        INNER JOIN UserProduct UP ON UP.ProductID =HD.ProductID AND WishListItem =1	
		        INNER JOIN Users U ON U.UserID =UP.UserID 
		        INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
		        INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
		        INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
		    )Note
		     --WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		    
		            
		        --User pushnotification already exist then NotificationSent flag updating to 0
		        UPDATE UserPushNotification SET NotificationSent=0
		                                       ,DateModified =GETDATE()	
		                                       ,DiscountCreated=1		                                      
		        FROM UserPushNotification UP
		        INNER JOIN #Temp T ON UP.ProductHotdealID=T.ProductHotDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=1
		       
		       --User pushnotification not exist then insert. 
		       INSERT INTO UserPushNotification(UserID
												,ProductID
												,DeviceID												
												,ProductHotdealID												
												,NotificationSent
												,DateCreated
												,DiscountCreated)
				SELECT T.UserID 
				      ,T.ProductID 
				      ,T.DeviceID 
				      ,T.ProductHotDealID 
				      ,0
				      ,GETDATE()
				      ,1
				FROM #Temp T				
				LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.ProductHotDealID =UP.ProductHotdealID AND T.ProductID =UP.ProductID AND DiscountCreated=1
				WHERE UP.UserPushNotificationID IS NULL  	
			
			
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotDealAdd.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
