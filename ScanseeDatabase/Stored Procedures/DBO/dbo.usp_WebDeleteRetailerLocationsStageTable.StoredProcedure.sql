USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDeleteRetailerLocationsStageTable]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebDeleteRetailerProductStageTable  
Purpose     : To DELETE THE STAGING TABLE by the Retailer.  
Example     : usp_UploadRetailerLocations  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   09 February 2012  Pavan Sharma K Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebDeleteRetailerLocationsStageTable]  
(  
   @RetailerID int  
 , @UserID int  
 --, @RetailLocationFileName Varchar(255)   
   
 --Output Variable   
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION      
     
   DELETE FROM UploadRetaillocations 
   WHERE RetailID = @RetailerID  
   AND UserID = @UserID   
            
 --Confirmation of Success.  
  SELECT @Status = 0  
  COMMIT TRANSACTION  
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebDeleteRetailerProductStageTable.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'     
   ROLLBACK TRANSACTION;      
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;




GO
