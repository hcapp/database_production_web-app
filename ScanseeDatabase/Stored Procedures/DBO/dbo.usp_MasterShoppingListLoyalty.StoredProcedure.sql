USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListLoyalty]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_MasterShoppingListLoyalty  
Purpose     : To dispaly Loyalty.  
Example     : usp_MasterShoppingListLoyalty 1, 1, 13  
  
History  
Version  Date  Author   Change Description  
---------------------------------------------------------------   
1.0   7th June 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_MasterShoppingListLoyalty]  
(  
   @ProductID int  
 , @RetailID int  
 , @UserID int  
 , @LowerLimit int  
 , @ScreenName varchar(50)  
   
 --OutPut Variable  
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
  --To get Media Server Configuration.  
  DECLARE @ManufConfig varchar(50)    
  SELECT @ManufConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'  
  
  --To get Image of the product  
    
   DECLARE @ProductImagePath varchar(1000)  
   SELECT @ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END   
   FROM Product   
   WHERE ProductID = @ProductID   
     
  --To get the row count for pagination.  
    
   DECLARE @UpperLimit int   
   SELECT @UpperLimit = @LowerLimit + ScreenContent   
   FROM AppConfiguration   
   WHERE ScreenName = @ScreenName   
    AND ConfigurationType = 'Pagination'  
    AND Active = 1  
   DECLARE @MaxCnt int  
     
  IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)  
  BEGIN  
   SELECT RetailLocationID   
   INTO #Ret  
   FROM RetailLocation   
   WHERE RetailID = @RetailID  
      
   SELECT Row_Num=ROW_NUMBER() OVER(ORDER BY LoyaltyDealID)  
    ,LoyaltyDealID    
    ,LoyaltyDealName  
    ,LoyaltyDealDiscountType  
    ,LoyaltyDealDiscountAmount  
    ,LoyaltyDealDiscountPct  
    ,LoyaltyDealDescription  
    ,LoyaltyDealDateAdded  
    ,LoyaltyDealStartDate  
    ,LoyaltyDealExpireDate  
    ,@ProductImagePath imagePath  
   INTO #Loyalty  
   FROM LoyaltyDeal L  
   INNER JOIN #Ret R ON R.RetailLocationID = L.RetailLocationID   
   WHERE ProductID = @ProductID   
    AND GETDATE() BETWEEN L.LoyaltyDealStartDate AND L.LoyaltyDealExpireDate  
     
    --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #Loyalty  
   --this flag is a indicator to enable "More" button in the UI.   
   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
  
   SELECT Row_Num rowNum  
       ,C.LoyaltyDealID    
    ,LoyaltyDealName  
    ,LoyaltyDealDiscountType  
    ,LoyaltyDealDiscountAmount  
    ,LoyaltyDealDiscountPct  
    ,LoyaltyDealDescription  
    ,LoyaltyDealDateAdded  
    ,LoyaltyDealStartDate  
    ,LoyaltyDealExpireDate  
    ,ImagePath imagePath  
    ,usage = CASE WHEN CG.LoyaltyDealID IS NULL THEN 'Red' ELSE 'Green' END  
   FROM #Loyalty C  
    LEFT JOIN UserLoyaltyGallery CG ON CG.LoyaltyDealID = C.LoyaltyDealID AND UserID = @UserID   
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   ORDER BY Row_Num  
     
  END  
  IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS NULL OR @RetailID =0)  
  BEGIN  
   SELECT Row_Num=ROW_NUMBER() OVER(ORDER BY LoyaltyDealID)   
    ,LoyaltyDealID    
    ,LoyaltyDealName  
    ,LoyaltyDealDiscountType  
    ,LoyaltyDealDiscountAmount  
    ,LoyaltyDealDiscountPct  
    ,LoyaltyDealDescription  
    ,LoyaltyDealDateAdded  
    ,LoyaltyDealStartDate  
    ,LoyaltyDealExpireDate  
    ,@ProductImagePath imagePath  
   INTO #Loyalty1  
   FROM LoyaltyDeal   
   WHERE ProductID = @ProductID   
    AND GETDATE() BETWEEN LoyaltyDealStartDate AND LoyaltyDealExpireDate  
      
    --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #Loyalty1  
   --this flag is a indicator to enable "More" button in the UI.   
   --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
    
  
  
   SELECT Row_Num rowNum  
    ,C.LoyaltyDealID    
    ,LoyaltyDealName  
    ,LoyaltyDealDiscountType  
    ,LoyaltyDealDiscountAmount  
    ,LoyaltyDealDiscountPct  
    ,LoyaltyDealDescription  
    ,LoyaltyDealDateAdded  
    ,LoyaltyDealStartDate  
    ,LoyaltyDealExpireDate  
    ,ImagePath imagePath  
    ,usage = CASE WHEN CG.LoyaltyDealID IS NULL THEN 'Red' ELSE 'Green' END  
   FROM #Loyalty1 C  
    LEFT JOIN UserLoyaltyGallery CG ON CG.LoyaltyDealID = C.LoyaltyDealID AND UserID = @UserID  
   WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
   ORDER BY Row_Num  
     
  END  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_MasterShoppingListLoyalty.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
