USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerLogUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUploadManufacturerLogUpdation
Purpose					:
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			28th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerLogUpdation]
(

	--Input Input Parameter(s)--	 
	
	  @UploadManufacturerLogID INT
	  
	--Output Variable--
	  
	, @Status INT OUTPUT	
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		   UPDATE UploadManufacturerLog
		   SET EmailNotificationFlag = 1 
		   WHERE UploadManufacturerLogID = @UploadManufacturerLogID		
		   
		   --CONFIRMATION OF SUCCESS
		   SET @Status = 0
	COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUploadManufacturerLogUpdation.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
