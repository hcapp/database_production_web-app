USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierAttributeDisplayList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierAttributeDisplayList
Purpose					: To Display the attributes that are not associated to the product.
Example					: usp_WebSupplierAttributeDisplayList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			03rd Jan 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierAttributeDisplayList]
(
	@Productid int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	--Display only those attributes that are still not associated with the input product.
	BEGIN TRY
		SELECT   AttributeID  prodAttributesID 
		       , AttributeName prodAttributeName	
		FROM Attribute
		WHERE AttributeId NOT IN (SELECT AttributeId FROM ProductAttributes WHERE ProductID = @Productid)
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierAttributeDisplayList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
