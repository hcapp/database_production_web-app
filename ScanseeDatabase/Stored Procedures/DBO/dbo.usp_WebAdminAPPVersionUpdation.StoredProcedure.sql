USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminAPPVersionUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: [usp_WebAdminAPPVersionUpdation]  
Purpose					: Update Given Version Details.  
Example					: [usp_WebAdminAPPVersionUpdation]  
  
History  
Version  Date				Author			 Change	Description  
---------------------------------------------------------------   
1.0		 5th Sep 2013   	Dhananjaya TR	 Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebAdminAPPVersionUpdation]  
(  
   @VersionID Int
 , @AppType Varchar(255)   
 , @VersionNumber Varchar(100)  
 , @DatabaseSchemaName Varchar(200)
 , @ServerBuild Varchar(500)
 , @QAFirstReleaseDate DateTime 
 , @QALastReleaseDate DateTime
 , @QAReleaseNotePath Varchar(500) 
 , @ProductionFirstReleaseDate DateTime   
 , @ProductionLastReleaseDate DateTime 
 , @ProductionReleaseNotePath Varchar(500) 
 , @ITunesUploadDate Datetime   
 , @BuildApprovalDate Datetime
 , @QAReleaseNote Varchar(2000)
 , @ProductionReleaseNote Varchar(2000)
   
 --OutPut Variable   
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
  BEGIN TRY  
 
	BEGIN TRANSACTION
 
     --Updating Yet to Release Version to Given Version Number
     IF @ProductionFirstReleaseDate IS NOT NULL
     BEGIN 
         UPDATE AppConfiguration SET ScreenContent =@VersionNumber 
         WHERE ConfigurationType like 'Yet to Release Version'
     
     END
     
     --Update Current App Version, Latest App Version in the appconfiguaration Table when bulild is approved
     IF @BuildApprovalDate IS NOT NULL
     BEGIN
		 UPDATE AppConfiguration SET ScreenContent =(SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType like 'LatestAppVersion' AND ScreenName LIKE 'LatestAppVersion' ) 
		 WHERE ConfigurationType like 'CurrentAppVersion' AND ScreenName LIKE 'CurrentAppVersion'
	      
		 UPDATE AppConfiguration SET ScreenContent =(SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType like 'Yet to Release Version' AND ScreenName LIKE 'Yet to Release Version' ) 
		 WHERE ConfigurationType like 'LatestAppVersion' AND ScreenName LIKE 'LatestAppVersion'
	      
		 UPDATE AppConfiguration SET ScreenContent =NULL
		 WHERE ConfigurationType like 'Yet to Release Version' AND ScreenName LIKE 'Yet to Release Version'     
     END
 
     --Updating selected App Vrsion details
     UPDATE APPVersionManagement SET --QAFirstReleaseDate=CASE WHEN QAFirstReleaseDate IS NOT NULL THEN QAFirstReleaseDate  WHEN @QAFirstReleaseDate IS NULL THEN QAFirstReleaseDate ELSE @QAFirstReleaseDate END
									 QALastReleaseDate=CASE WHEN @QALastReleaseDate IS NULL THEN QALastReleaseDate ELSE @QALastReleaseDate END
									,QAReleaseNotePath=CASE WHEN @QAReleaseNotePath IS NULL THEN QAReleaseNotePath ELSE @QAReleaseNotePath END
									,ProductionFirstReleaseDate=CASE WHEN ProductionFirstReleaseDate IS NULL THEN @ProductionFirstReleaseDate ELSE ProductionFirstReleaseDate END
									,ProductionLastReleaseDate=CASE WHEN @ProductionLastReleaseDate IS NULL AND ProductionLastReleaseDate IS NULL THEN @ProductionFirstReleaseDate WHEN @ProductionLastReleaseDate IS NOT NULL THEN @ProductionLastReleaseDate ELSE ProductionLastReleaseDate END
									,ProductionReleaseNotePath=CASE WHEN @ProductionReleaseNotePath IS NULL THEN ProductionReleaseNotePath ELSE @ProductionReleaseNotePath END
									,ITunesUploadDate=CASE WHEN @ITunesUploadDate IS NULL THEN ITunesUploadDate ELSE @ITunesUploadDate END
									,BuildApprovalDate=CASE WHEN @BuildApprovalDate IS NULL THEN BuildApprovalDate ELSE @BuildApprovalDate END
                                    ,QAReleaseNoteFileName=CASE WHEN @QAReleaseNote IS NULL THEN QAReleaseNoteFileName ELSE @QAReleaseNote END
                                    ,ProductionReleaseNoteFileName=CASE WHEN @ProductionReleaseNote IS NULL THEN ProductionReleaseNoteFileName ELSE @ProductionReleaseNote END
     WHERE APPVersionManagementID =@VersionID 
	
	--Confirmation of Success.
	SELECT @Status = 0
   COMMIT TRANSACTION
 END TRY  
    
 BEGIN CATCH 
 
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebAdminAPPVersionUpdation].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction' 
    --Confirmation of failure.
   SELECT @Status = 1 
    
   ROLLBACK TRANSACTION;  
  END;  
     
 END CATCH;  
END;


GO
