USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGetUserModuleRadius]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerGetUserModuleRadius]
Purpose					: To get the User preferred radius.
Example					: [usp_WebConsumerGetUserModuleRadius]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th May 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerGetUserModuleRadius]
(
	--  @UserID int  
	--, @ModuleName varchar(50)
	--Output Variable 
	
	  @Radius int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
	--Fetch the default radius for find module.
	--IF @ModuleName = 'Find'
	--BEGIN
	--	IF EXISTS(SELECT 1 FROM UserPreference WHERE UserID = @UserID)
	--	BEGIN
	--		SELECT @Radius = LocaleRadius
	--		FROM UserPreference
	--		WHERE UserID = @UserID
	--	END
		
		
		--If the user has not configured any default radius then consider the global radius
		IF @Radius IS NULL
		BEGIN
			SELECT @Radius = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'WebConsumerDefaultRadius'
			AND ScreenName = 'WebConsumerDefaultRadius'
			AND Active = 1
		END
	
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerGetUserModuleRadius].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
