USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireUnmappedProductDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireLoyaltyDealsProductDataPorting
Purpose					: To move the unmapped products into the Product Table.
Example					: usp_BatchCellFireLoyaltyDealsProductDataPorting

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			23rd October 2012   Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireUnmappedProductDataPorting]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			INSERT INTO Product(ProductID
							  , ManufacturerID
							  , ScanCode
							  , Service
							  , ProductShortDescription
							  , ProductName
							  , ProductLongDescription
							  , SKUNumber
							  , ModelNumber
							  , ProductImagePath
							  , SuggestedRetailPrice
							  , Weight
							  , WeightUnits
							  , ProductExpirationDate
							  , ProductAddDate
							  , ProductModifyDate
							  , WarrantyServiceInformation
							  , ISBN
							  , APIPartnerID)
						SELECT ProductID
							  , ManufacturerID
							  , ScanCode
							  , Service
							  , ProductShortDescription
							  , ProductName
							  , ProductLongDescription
							  , SKUNumber
							  , ModelNumber
							  , ProductImagePath
							  , SuggestedRetailPrice
							  , Weight
							  , WeightUnits
							  , ProductExpirationDate
							  , ProductAddDate
							  , ProductModifyDate
							  , WarrantyServiceInformation
							  , ISBN
							  , (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName LIKE 'CellFire')
						FROM APICellFireUnmappedProducts
						
			--Execute the procedure to move the Loyalty Product Information.						
			EXEC [dbo].[usp_BatchCellFireLoyaltyDealsProductDataPorting] @Status = @Status output , @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output						
			
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireLoyaltyDealsProductDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
