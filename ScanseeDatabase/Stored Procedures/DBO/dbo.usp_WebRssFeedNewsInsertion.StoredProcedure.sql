USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssFeedNewsInsertion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Stored Procedure name	: [usp_WebRssFeedNewsInsertion]
Purpose					: To insert into RssFeedNews from Staging table.
Example					: [usp_WebRssFeedNewsInsertion]

History
Version		Date			Author									Change Description
---------------------------------------------------------------------------------- ---------------------------------------
1.0			16 Feb 2015		Mohith H R								 1.1
1.1         07 Dec 2015     Suganya C								 Duplicate news removal
1.2							Sagar Byali								 Video deletion changes
1.3			28 Oct 2016		Shilpashree & Sagar Byali				 Performance changes
1.4			16 Dec 2016		Sagar Byali								 Procedure re-written with logical changes
---------------------------------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssFeedNewsInsertion]
(
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY		
	
			-- Inserting the latest news feeds into Main table
			INSERT INTO RssFeedNews
						(Title
						,ImagePath
						,ShortDescription
						,LongDescription
						,Link
						,PublishedDate
						,NewsType
						,Message
						,HcHubCitiID	
						,Classification
						,Section
						,AdCopy
						,VideoLink)

			-- To collect latest published news from Staging table
			SELECT DISTINCT T.Title
						,T.ImagePath
						,T.ShortDescription
						,T.LongDescription
						,T.Link
						,T.PublishedDate
						,T.NewsType
						,T.Message
						,T.HcHubCitiID	
						,T.Classification
						,T.Section
						,T.AdCopy
						,T.VideoLink
			FROM RssFeedNewsStagingTable T 
			LEFT JOIN RssFeedNews RS ON RS.Title = T.Title 
									AND T.HcHubCitiID = RS.HcHubCitiID
									AND ISNULL(RS.PublishedDate,' ') = ISNULL(T.PublishedDate,' ') 
									AND ISNULL(RS.ImagePath,' ') = ISNULL(T.ImagePath,' ') 
									AND ISNULL(T.Link,' ') = ISNULL(RS.Link,' ') 
									AND ISNULL(T.ShortDescription,' ') = ISNULL(RS.ShortDescription,' ') 
			WHERE RS.RssFeedNewsID IS NULL 
			AND T.Title IS NOT NULL 
			AND T.HcHubCitiID IS NOT NULL
			AND T.NewsType != 'Classifields' 
			GROUP BY T.Title
					,T.ImagePath
					,T.ShortDescription
					,T.LongDescription
					,T.Link
					,T.PublishedDate
					,T.NewsType
					,T.Message
					,T.HcHubCitiID	
					,T.Classification
					,T.Section
					,T.AdCopy
					,T.VideoLink

			UNION ALL

			SELECT DISTINCT T.Title
						,T.ImagePath
						,T.ShortDescription
						,T.LongDescription
						,T.Link
						,T.PublishedDate
						,T.NewsType
						,T.Message
						,T.HcHubCitiID	
						,T.Classification
						,T.Section
						,T.AdCopy
						,T.VideoLink		
			FROM RssFeedNewsStagingTable T 
			LEFT JOIN RssFeedNews RS ON RS.Classification = T.Classification AND T.HcHubCitiID = RS.HcHubCitiID
			WHERE RS.RssFeedNewsID IS NULL 
			AND T.Classification IS NOT NULL
			AND T.NewsType = 'Classifields'
			GROUP BY T.Title
					,T.ImagePath
					,T.ShortDescription
					,T.LongDescription
					,T.Link
					,T.PublishedDate
					,T.NewsType
					,T.Message
					,T.HcHubCitiID	
					,T.Classification
					,T.Section
					,T.AdCopy
					,T.VideoLink

	--BEGIN TRAN

			--To retain 15 days news in Main table		  
			SELECT	RetainDate = CASE    WHEN HcHubCitiID = 10 AND Newstype NOT IN ('All','videos') THEN MAX(DATEADD(d,-15,(ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE))))) 
										 WHEN HcHubCitiID = 10 AND Newstype = 'All' THEN MAX(DATEADD(d,-14,(ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE))))) 
										 WHEN HcHubCitiID = 10 AND Newstype = 'videos' THEN MAX(DATEADD(d,-1,(ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE))))) 
										 WHEN HcHubCitiID = 52 THEN MAX(DATEADD(d,-5,(ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE))))) 
										 WHEN HcHubCitiID NOT IN (10,52) THEN MAX(DATEADD(d,-15,(ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE))))) 
								 END	  
				   ,Newstype
				   ,HcHubCitiID
			INTO #RetainNews
			FROM RssFeedNews
			GROUP BY NewsType, HcHubCitiID

			--To delete News from Main table (after 15 days)
			DELETE FROM RssFeedNews
			FROM RssFeedNews N
			INNER JOIN #RetainNews FN ON FN.NewsType = N.NewsType AND FN.HcHubCitiID = N.HcHubCitiID
			WHERE ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE)) < RetainDate

			--To delete News from StagingTable table (after 18 days)
			DELETE FROM RssFeedNewsStagingTable
			FROM RssFeedNewsStagingTable N
			INNER JOIN #RetainNews FN ON FN.NewsType = N.NewsType AND FN.HcHubCitiID = N.HcHubCitiID
			WHERE ISNULL(CAST(PublishedDate as DATE),CAST(DateCreated as DATE)) < DATEADD(d,+3,RetainDate)		

	--COMMIT TRAN 		
				   
			SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
		SET @ErrorNumber = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @Status=1 
	END CATCH;
END;






GO
