USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShoppingHistoryMoveItems]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_ShoppingHistoryMoveItems
Purpose					: To move items from History to List and/or Favorites.
Example					: usp_ShoppingHistoryMoveItems

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_ShoppingHistoryMoveItems]
(
	 @UserProductID varchar(max)  
	,@ProductID varchar(max)  
	,@AddTo varchar(20)  
	--Output Variable 
	, @ListFlag int output
	, @FavoritesFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--to split UserProductIDs into table
			SELECT PARAM UserProductID
			INTO #UserProduct
			FROM fn_SplitParam(@UserProductID, ',')
	
			--To move item to List
			IF 'List' IN (SELECT LTRIM(RTRIM(PARAM)) FROM fn_SplitParam(@AddTo, ','))
			BEGIN
				--If the item already exists in the list, flag will be enabled to prompt alert to user.
				SELECT @ListFlag = (CASE WHEN COUNT(ProductID) >= 1 THEN 1 ELSE 0 END)
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID 
				WHERE UP.TodayListtItem = 1
				
				--To update list items
				UPDATE UserProduct 
				SET TodayListtItem = 1
					,TodayListAddDate = GETDATE()
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID
				WHERE UP.TodayListtItem = 0  
			END
			
			--To move item to Favorites
			IF 'Favorites' IN (SELECT LTRIM(RTRIM(PARAM)) FROM fn_SplitParam(@AddTo, ','))
			BEGIN
				--If the item already exists in the Favorites, flag will be enabled to prompt alert to user.
				SELECT @FavoritesFlag = (CASE WHEN COUNT(ProductID) >= 1 THEN 1 ELSE 0 END)
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID 
				WHERE UP.MasterListItem = 1
				
				--To update list items
				UPDATE UserProduct 
				SET MasterListItem = 1
					,MasterListAddDate = GETDATE()
				FROM UserProduct UP
				INNER JOIN #UserProduct U ON U.UserProductID = UP.UserProductID
				WHERE UP.MasterListItem = 0  
			END

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_ShoppingHistoryMoveItems.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
