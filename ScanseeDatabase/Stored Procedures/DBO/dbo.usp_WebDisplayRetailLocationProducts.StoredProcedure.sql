USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayRetailLocationProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebDisplayRetailLocationProducts
Purpose					: To fetch the RetailLocation associated products.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			02 April 2012				  Pavan Sharma K	     Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayRetailLocationProducts]				
(

	--Input Input Parameter(s)--
	
	  @RetailerID  int
	, @RetailLocationID varchar(5000) --CSV RetailLocationIDs
	, @SearchParameter varchar(100)
	, @lowerlimit int
	, @RecordCount INT
	
	--Output Variable--	  
	, @RowCount INT output    
	, @NextPageFlag bit output   
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY 
	
		  DECLARE @UpperLimit INT    
		  DECLARE @MaxCnt INT  
		 
	  --To get the Number of records per page.    
		  --DECLARE @ScreenContent INT    
		  --SELECT @ScreenContent = ScreenContent    
		  --FROM AppConfiguration    
		  --WHERE ConfigurationType = 'Website Pagination'    
		  --AND ScreenName = 'All'    
		  --AND Active = 1    
      
		  SELECT @UpperLimit = @LowerLimit + @RecordCount 
	
	--To get Media Server Configuration.  
	    DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration' 
		 
		SELECT DISTINCT RowNum=IDENTITY(int,1,1)
			   , P.ProductID
		       , P.ScanCode
		        , P.ProductName
		       , P.ProductShortDescription
		       --, P.ProductLongDescription
		       --, RL.RetailLocationID		      
		       --, RLP.Price
		      -- , RLD.SalePrice
			   , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 
											THEN @Config +CONVERT(VARCHAR(30),P.ManufacturerID)+'/' + ProductImagePath
																						ELSE ProductImagePath END
			  --, RLP.RetailLocationProductDescription 
			  --, RLD.SaleStartDate
			 -- , RLD.SaleEndDate
		INTO #Products																					
		FROM RetailLocationProduct RLP
		INNER JOIN RetailLocation RL ON RLP.RetailLocationID = RL.RetailLocationID AND RL.Active = 1	
		INNER JOIN dbo.fn_SplitParam(@RetailLocationID, ',')R ON R.Param = RL.RetailLocationID
		INNER JOIN Product P ON RLP.ProductID = P.ProductID
		LEFT OUTER JOIN RetailLocationDeal RLD ON RLP.RetailLocationID = RLD.RetailLocationID AND RLP.ProductID = RLD.ProductID
		WHERE  RL.RetailID = @RetailerID	
		AND (P.ProductName LIKE CASE WHEN @SearchParameter IS NOT NULL THEN'%'+@SearchParameter+'%' ELSE '%' END
				OR
			   P.ScanCode LIKE @SearchParameter+'%')	
		AND ISNULL(P.ProductExpirationDate,GETDATE()+1)>= GETDATE()	   
			   	
		SELECT @MaxCnt = MAX(RowNum) FROM #Products    
	    SET @RowCount = @MaxCnt    
        SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END 
        
         SELECT   RowNum
			   , ProductID
			   , ScanCode
			   , ProductName
			   , ProductShortDescription as productShortDescription
			   --, ProductLongDescription
			   --, RetailLocationID		      
			  -- , Price
			   --, SalePrice
			   , ProductImagePath	
			  -- , RetailLocationProductDescription as productShortDescription
			   --, SaleStartDate
			   --, SaleEndDate											
		 FROM #Products		
		 WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit  
		 
		  IF @RowCount IS NULL     --TO CHECK IF THE RESULT SET IS EMPTY    
		  SET @RowCount = 0 
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayHotDeal.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
	END CATCH	 
	
END;




GO
