USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchDealMapCategoryXRef]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchDealMapCategoryXRef
Purpose					: To update categoryid
Example					: usp_BatchDealMapCategoryXRef

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st Oct 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchDealMapCategoryXRef]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE ProductHotDeal 
			SET CategoryID = C.CategoryID 
			FROM ProductHotDeal HD
			INNER JOIN APIDealMapData D ON D.ID = HD.SourceID 
			INNER JOIN DealMapCategoryXRef DX ON DX.DealMapCategory = D.Category 
			INNER JOIN Category C ON C.ParentCategoryID = DX.ScanSeeParentCategoryID AND C.SubCategoryID = DX.ScanSeeSubCategoryID 

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchDealMapCategoryXRef.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
