USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGalleryAllCoupons]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebConsumerGalleryAllCoupons]  
Purpose     : To display all the coupons related to the user in user coupon gallery.  
Example     : [usp_WebConsumerGalleryAllCoupons] 
  
History  
Version  Date        Author   Change Description  
---------------------------------------------------------------   
1.0   16th Oct 2011 Naga Sandhya S Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerGalleryAllCoupons]  
(  
   @Userid int  
 , @RetailerID int
 , @SearchKey varchar(255)
 , @CategoryIDs varchar(1000)
 , @LowerLimit int  
 , @RecordCount varchar(50)  
 
 --User Tracking Inputs
 , @MainMenuID int
   
 --OutPut Variable  
 , @MaxCnt int output
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
  --To get Media Server Configuration.  
  DECLARE @RetailConfig varchar(50)    
  SELECT @RetailConfig=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Retailer Media Server Configuration'  
       
   
    --To get the row count for pagination.  
  DECLARE @UpperLimit int   
  SELECT @UpperLimit = @LowerLimit + @RecordCount  
  --DECLARE @MaxCnt int  
  
  CREATE TABLE #Gallery(Row_Num INT IDENTITY(1, 1)
					  , CouponID INT 
					  , CouponName VARCHAR(255)
					  , CouponDiscountType VARCHAR(255)
					  , CouponDiscountAmount MONEY
					  , CouponDiscountPct FLOAT
					  , CouponDescription VARCHAR(1000) 
					  , CouponDateAdded DATETIME
					  , CouponStartDate DATETIME
					  , CouponExpireDate DATETIME
					  , CouponURL VARCHAR(1000)
					  , CouponImagePath VARCHAR(1000)
					  , Fav_Flag BIT
					  , CoupFavFlag int
					  , ViewableOnWeb BIT
					  --, RetailerID int
					  --, RetailName varchar(255)
					  , CategoryID int
					  , CategoryName varchar(100))

--Check if the User has selected any category				  
  IF @CategoryIDs = '0'  
  BEGIN
	 print 'No Category'
  --Check if the User has selected any Retailer				  
	IF @RetailerID = 0 
	BEGIN
		  print 'No Retailer'
		  INSERT INTO #Gallery(CouponID   
							,CouponName  
							,CouponDiscountType  
							,CouponDiscountAmount  
							,CouponDiscountPct  
							,CouponDescription 							
							,CouponDateAdded  
							,CouponStartDate  
							,CouponExpireDate  
							,CouponURL  
							,CouponImagePath  
							,Fav_Flag  
							,CoupFavFlag
							,ViewableOnWeb
							--,RetailerID
							--,RetailName
							,CategoryID
							,CategoryName)
		  
		  SELECT DISTINCT c.CouponID   
			,C.CouponName  
			,CouponDiscountType  
			,CouponDiscountAmount  
			,CouponDiscountPct  
			,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
			,CouponDateAdded  
			,CouponStartDate  
			,CouponExpireDate  
			,CouponURL  
			,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																												CASE WHEN C.WebsiteSourceFlag = 1 
																													THEN @RetailConfig
																													+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																													+CouponImagePath 
																												ELSE CouponImagePath 
																												END
																										 END 
							 END  
			,Fav_Flag=CASE WHEN C.CouponID=UC.CouponID THEN 1 ELSE 0 END
			,CoupFavFlag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
			,ViewableOnWeb
		    --,CR.RetailID
		    --,CASE WHEN CR.RetailID IS NULL THEN 'ScanSee' ELSE R.RetailName END	
		   , CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
		   , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END    	 
		  FROM Coupon c  
		  LEFT JOIN UserCouponGallery UC ON c.couponid=uc.CouponID AND UserID=@Userid    
		  LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
		  LEFT JOIN CouponProduct CP ON CP.CouponID = C.CouponID
		  LEFT JOIN ProductCategory PC ON PC.ProductID = CP.ProductID
		  LEFT JOIN Category Cat ON Cat. CategoryID = PC.CategoryID  
		  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate 
		  AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
		  AND UC.UserCouponGalleryID IS NULL
		  ORDER BY CategoryName, C.CouponName ASC	 
			 
		 
	END
	--If no Retailer Selected
	IF @RetailerID <> 0
	BEGIN
	print 'Retailer'
		 INSERT INTO #Gallery(CouponID   
								,CouponName  
								,CouponDiscountType  
								,CouponDiscountAmount  
								,CouponDiscountPct  
								,CouponDescription 							
								,CouponDateAdded  
								,CouponStartDate  
								,CouponExpireDate  
								,CouponURL  
								,CouponImagePath  
								,Fav_Flag  
								,CoupFavFlag
								,ViewableOnWeb
								--,RetailerID
								--,RetailName
								,CategoryID
								,CategoryName)
			  
			  SELECT DISTINCT c.CouponID   
				,CouponName  
				,CouponDiscountType  
				,CouponDiscountAmount  
				,CouponDiscountPct  
				,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
				,CouponDateAdded  
				,CouponStartDate  
				,CouponExpireDate  
				,CouponURL  
				,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																													CASE WHEN C.WebsiteSourceFlag = 1 
																														THEN @RetailConfig
																														+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																														+CouponImagePath 
																													ELSE CouponImagePath 
																													END
																											 END 
								 END  
				,Fav_Flag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
				,CoupFavFlag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
				,ViewableOnWeb
				--,CR.RetailID
				--,CASE WHEN CR.RetailID IS NULL THEN 'ScanSee' ELSE R.RetailName END	
			   , CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
			   , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END    	 
			  FROM Coupon c  
			  LEFT JOIN UserCouponGallery UC ON c.couponid=uc.CouponID AND UserID=@Userid    
			  LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID
			  LEFT JOIN CouponProduct CP ON CP.CouponID = C.CouponID
			  LEFT JOIN ProductCategory PC ON PC.ProductID = CP.ProductID
			  LEFT JOIN Category Cat ON Cat. CategoryID = PC.CategoryID  
			  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate 
			  AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
			  AND CR.RetailID = @RetailerID
			  AND UC.UserCouponGalleryID IS NULL
			  ORDER BY CategoryName, C.CouponName ASC
	END
		 
    END
					  
     
  IF @CategoryIDs <> '0'  
  BEGIN
	print 'Category'
	IF @RetailerID = 0
	BEGIN
		  print 'No Retailer'
		  INSERT INTO #Gallery(CouponID   
							  ,CouponName  
							  ,CouponDiscountType  
							  ,CouponDiscountAmount  
							  ,CouponDiscountPct  
							  ,CouponDescription 							
							  ,CouponDateAdded  
							  ,CouponStartDate  
							  ,CouponExpireDate  
							  ,CouponURL  
							  ,CouponImagePath  
							  ,Fav_Flag
							  ,CoupFavFlag  
							  ,ViewableOnWeb
							  --,RetailerID
							  --,RetailName
							  ,CategoryID
							  ,CategoryName)
		  
		  SELECT DISTINCT c.CouponID   
			,CouponName  
			,CouponDiscountType  
			,CouponDiscountAmount  
			,CouponDiscountPct  
			,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
			,CouponDateAdded  
			,CouponStartDate  
			,CouponExpireDate  
			,CouponURL  
			,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																												CASE WHEN C.WebsiteSourceFlag = 1 
																													THEN @RetailConfig
																													+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																													+CouponImagePath 
																												ELSE CouponImagePath 
																												END
																										 END 
							 END  
			,Fav_Flag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
			,CoupFavFlag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
			,ViewableOnWeb
		    --,CR.RetailID
		    --,CASE WHEN CR.RetailID IS NULL THEN 'ScanSee' ELSE R.RetailName END	
		    , CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
		    , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END 
		  FROM Coupon c  
		   LEFT JOIN UserCouponGallery UC ON C.couponid=uc.CouponID  
		   INNER JOIN CouponProduct CP ON CP.CouponID = C.CouponID
		   INNER JOIN Product P ON P.ProductID = CP.ProductID
		   INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
		   INNER JOIN Category Cat ON CAT.CategoryID = PC.CategoryID
		   INNER JOIN dbo.fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
		   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
		   --LEFT JOIN Retailer R ON CR.RetailID = R.RetailID
		  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate 
		  AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
		  AND UC.UserCouponGalleryID IS NULL
		  ORDER BY CategoryName, C.CouponName ASC
	 END
	 
	 IF @RetailerID <> 0
	 BEGIN
		 print 'Retailer'
		 SELECT DISTINCT c.CouponID   
			,CouponName  
			,CouponDiscountType  
			,CouponDiscountAmount  
			,CouponDiscountPct  
			,CASE WHEN CouponShortDescription  IS NOT NULL THEN CouponShortDescription ELSE CouponLongDescription END
			,CouponDateAdded  
			,CouponStartDate  
			,CouponExpireDate  
			,CouponURL  
			,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																												CASE WHEN C.WebsiteSourceFlag = 1 
																													THEN @RetailConfig
																													+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																													+CouponImagePath 
																												ELSE CouponImagePath 
																												END
																										 END 
							 END  
			,Fav_Flag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
			,CoupFavFlag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END 
			,ViewableOnWeb
		    --,CR.RetailID
		    --,CASE WHEN CR.RetailID IS NULL THEN 'ScanSee' ELSE R.RetailName END	
		    , CASE WHEN Cat.CategoryID IS NULL THEN 0 ELSE Cat.CategoryID END
		    , CategoryName = CASE WHEN Cat.ParentCategoryName IS NULL THEN 'Others' ELSE Cat.ParentCategoryName +' - '+Cat.SubCategoryName END 
		  FROM Coupon c  
		   LEFT JOIN UserCouponGallery UC ON C.couponid=uc.CouponID  
		   INNER JOIN CouponProduct CP ON CP.CouponID = C.CouponID
		   INNER JOIN Product P ON P.ProductID = CP.ProductID
		   INNER JOIN ProductCategory PC ON PC.ProductID = P.ProductID
		   INNER JOIN Category Cat ON CAT.CategoryID = PC.CategoryID
		   INNER JOIN [dbo].fn_SplitParam(@CategoryIDs, ',') F ON F.Param = PC.CategoryID
		   LEFT JOIN CouponRetailer CR ON CR.CouponID = C.CouponID 
		   --LEFT JOIN Retailer R ON CR.RetailID = R.RetailID
		  WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate 
		  AND C.CouponName LIKE CASE WHEN @SearchKey IS NOT NULL THEN '%'+@SearchKey+'%' ELSE '%' END 
		  AND CR.RetailID = @RetailerID
		  AND UC.UserCouponGalleryID IS NULL
		  ORDER BY CategoryName, C.CouponName ASC
	 END
			  
    END
  --To capture max row number.  
  SELECT @MaxCnt = MAX(Row_Num) FROM #Gallery  
  --this flag is a indicator to enable "More" button in the UI.   
   SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END  
   
   
   
  SELECT  Row_Num   
    ,CouponID   
    ,CouponName  
    ,CouponDiscountType  
    ,CouponDiscountAmount  
    ,CouponDiscountPct  
    ,CouponDescription 
    ,CouponDateAdded   
    ,CouponStartDate 
    ,CouponExpireDate  
    ,CouponURL  
    ,CouponImagePath  
    ,Fav_Flag 
    ,CoupFavFlag  
    ,ViewableOnWeb
    , CategoryID 
    , CategoryName 
  INTO #CouponList
  FROM #Gallery  
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND  @UpperLimit   
  Order by Row_Num ASC
    
        
  ----To get Image of the product  
  -- DECLARE @ProductImagePath TABLE(couponid int,productimagepath varchar(1000))  
  -- INSERT INTO @ProductImagePath(CouponID,ProductImagePath)  
  -- SELECT C.CouponID, ProductImagePath   
  -- FROM Product P  
  -- INNER JOIN Coupon C ON C.ProductID=P.ProductID  
  -- INNER JOIN UserCouponGallery UC ON UC.CouponID=c.CouponID  
  -- WHERE UserID=@Userid  
     
  --SELECT  uc.CouponID   
  --  ,CouponName  
  --  ,CouponDiscountType  
  --  ,CouponDiscountAmount  
  --  ,CouponDiscountPct  
  --  ,CouponShortDescription  
  --  ,CouponLongDescription  
  --  ,CouponDateAdded  
  --  ,CouponStartDate  
  --  ,CouponExpireDate  
  --  ,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN productimagepath ELSE CouponImagePath END  
  --FROM Coupon c  
  -- INNER JOIN UserCouponGallery UC ON c.couponid=uc.CouponID  
  -- INNER JOIN @ProductImagePath P ON P.couponid=UC.CouponID  
  --WHERE  GETDATE() BETWEEN c.CouponStartDate AND c.CouponExpireDate  
  --    AND UserID=@userid  
    
 
 
 --User Tracking
 
 CREATE TABLE #Temp(CouponListID int,CouponID int)
 
 INSERT INTO ScanSeeReportingDatabase..CouponList (MainMenuID
												  ,CouponID
												  ,CreatedDate)
  OUTPUT inserted.CouponListID,inserted.CouponID INTO #Temp(CouponListID,CouponID)												  
  SELECT @MainMenuID 
        ,CouponID 
        ,GETDATE() 
  FROM #CouponList 												  
	
												  
  SELECT   Row_Num
    ,MIN(T.CouponListID)CouponListID
    ,C.CouponID   
    ,MIN(CouponName)CouponName
    ,MIN(CouponDiscountType)  CouponDiscountType
    ,MIN(CouponDiscountAmount) CouponDiscountAmount 
    ,MIN(CouponDiscountPct) CouponDiscountPct 
    ,MIN(CouponDescription) coupDesptn
    ,MIN(CouponDateAdded) coupDateAdded  
    ,MIN(CouponStartDate) coupStartDate
    ,MIN(CouponExpireDate) coupExpireDate 
    ,MIN(CouponURL)  CouponURL
    ,MIN(CouponImagePath) CouponImagePath 
    ,Fav_Flag  
    ,CoupFavFlag
    ,ViewableOnWeb
    ,MIN(CategoryID) cateId
    ,MIN(CategoryName) cateName
  FROM #CouponList C
  INNER JOIN #Temp T ON T.CouponID =C.CouponID
  GROUP BY C.CouponID
		 , C.CoupFavFlag
		 , C.Fav_Flag
		 , C.ViewableOnWeb
		 , Row_Num
  ORDER BY Row_Num ASC
 										  
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebConsumerGalleryAllCoupons].'    
   --- Execute retrieval of Error info.  
   EXEC [Version6].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
