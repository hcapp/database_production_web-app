USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerUploadRetailerLocationsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebRetailerUploadRetailerLocationsDisplay    
Purpose     : To display retailLocation information for a particular manufacturer from the stage table.    
Example     : usp_WebRetailerUploadRetailerLocationsDisplay    
    
History    
Version  Date   Author   Change Description    
---------------------------------------------------------------     
1.0   29th Dec 2011 Naga Sandhya S Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE  [dbo].[usp_WebRetailerUploadRetailerLocationsDisplay]    
(    
  @RetailID int    
 ,@UserID int    
 ,@LowerLimit int  
 --Output Variable 
 , @FindClearCacheURL VARCHAR(1000) output    
 , @NextPageFlag bit output    
 , @TotalRecords int output    
 , @ErrorNumber int output    
 , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 DECLARE @UpperLimit int    
 DECLARE @Config VARCHAR(500)
     
 --To get the row count for pagination.      
  DECLARE @ScreenContent Varchar(100)    
  SELECT @ScreenContent = ScreenContent       
  FROM AppConfiguration       
  WHERE ScreenName = 'All'     
  AND ConfigurationType = 'Website Pagination'    
  AND Active = 1     

   ----To get the Configuration Details.	 
	SELECT @Config = ScreenContent   
	FROM AppConfiguration   
	WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
	AND Active = 1
     
 SELECT @UpperLimit = @ScreenContent + @LowerLimit    
     
 DECLARE  @RowCnt int    
    
 BEGIN TRY    
      SELECT RowNum = ROW_NUMBER() OVER (ORDER BY UploadRetaillocationsID)    
            ,UploadRetaillocationsID    
   ,UserID    
   ,StoreIdentification    
   ,RetailID    
   ,Headquarters    
   ,Address1    
   ,Address2    
   ,Address3    
   ,Address4    
   ,City    
   ,State    
   ,PostalCode    
   ,CountryID    
   ,RetailLocationURL
   ,RetailLocationLatitude    
   ,RetailLocationLongitude    
   ,RetailLocationTimeZone    
   ,DateCreated    
   ,DateModified    
   ,RetailLocationStoreHours    
   ,PhoneNumber     
   ,ContactEmail    
   , ContactTitle    
   , ContactFirstName
   , ContactLastName    
   , ContactMobilePhone  
   , RetailKeyword
   , GridImgLocationPath
  INTO #TEMP     
      
  FROM     
  (SELECT UploadRetaillocationsID    
    ,UserID    
    ,StoreIdentification    
    ,U.RetailID    
    ,Headquarters    
    ,U.Address1    
    ,U.Address2    
    ,U.Address3    
    ,U.Address4    
    ,U.City    
    ,U.State    
    ,U.PostalCode    
    ,U.CountryID 
    ,RetailLocationURL   
    ,RetailLocationLatitude    
    ,RetailLocationLongitude    
    ,RetailLocationTimeZone    
    ,U.DateCreated    
    ,U.DateModified    
    ,RetailLocationStoreHours    
    ,PhoneNumber    
    ,ContactEmail    
    , ContactTitle   
    , ContactFirstName 
    , ContactLastName   
    , ContactMobilePhone 
    , RetailKeyword 
	--, RetailLocationImagePath GridImgLocationPath
	, IIF(RetailLocationImagePath <>'' AND RetailLocationImagePath IS NOT NULL,@Config+CAST(@RetailID AS VARCHAR(10))+'/locationlogo/'+RetailLocationImagePath,
	  IIF(RetailerImagePath IS NOT NULL AND RetailerImagePath <>'',@Config + CONVERT(VARCHAR(30),R.RetailID) +'/' + RetailerImagePath,Null)
	  ) GridImgLocationPath	
  FROM dbo.UploadRetaillocations U   
  INNER JOIN Retailer R ON R.RetailID =U.RetailID AND R.RetailerActive = 1 
  WHERE U.RetailID=@RetailID AND UserID=@UserID)UploadedData    
      
  SELECT @RowCnt = MAX(RowNum) FROM #TEMP    
      
  SET @TotalRecords = @RowCnt    
      
  SELECT @NextPageFlag = (CASE WHEN (@RowCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END)    
      
   SELECT RowNum     
            ,UploadRetaillocationsID    
   ,UserID    
   ,StoreIdentification    
   ,RetailID    
   ,Headquarters    
   ,Address1    
   ,Address2    
   ,Address3    
   ,Address4    
   ,City    
   ,State    
   ,PostalCode    
   ,CountryID    
   ,RetailLocationURL
   ,RetailLocationLatitude as RetailerLocationLatitude   
   ,RetailLocationLongitude  as RetailerLocationLongitude  
   ,RetailLocationTimeZone    
   ,DateCreated    
   ,DateModified    
   ,RetailLocationStoreHours    
   ,PhoneNumber     
   ,ContactEmail    
  , ContactTitle  
  , ContactFirstName  
  , ContactLastName  
  , ContactMobilePhone
  , RetailKeyword as keyword
  , GridImgLocationPath
  FROM #TEMP    
  WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit	      


  -------Find Clear Cache URL---29/2/2015--------

		DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

 -----------------------------------------------
      
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebRetailerUploadRetailerLocationsDisplay.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       
  END;    
       
 END CATCH;    
END;





GO
