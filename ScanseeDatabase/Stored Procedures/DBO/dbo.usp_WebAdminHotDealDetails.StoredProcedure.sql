USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminHotDealDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : [usp_WebAdminHotDealDetails] 
Purpose				  : To get the HotDeal Details  
Example				  : [usp_WebAdminHotDealDetails] 
    
History    
Version     Date           Author    Change Description    
---------------------------------------------------------------     
1.0      30th Aug 2013     SPAN      Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebAdminHotDealDetails] 
(    
     --Input Variables
      @ProductHotDealID int 
	
	 --OutPut Variables
	, @Status int output 
	, @ErrorNumber int output  
	, @ErrorMessage varchar(1000) output    
)    
AS    
BEGIN    

	BEGIN TRY 
		
		--To get Media Server Configuration.  
		DECLARE @ManufConfig varchar(50)    
		DECLARE @RetailerConfig varchar(50)
		DECLARE @Config Varchar(50) 
		DECLARE @Imagepath Varchar(100)
		DECLARE @RetLocAddr Varchar(1000) 
		DECLARE @RetLocID VARCHAR(1000)  
		DECLARE @City VARCHAR(1000)
		DECLARE @State VARCHAR(1000)

		SELECT @ManufConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'		

		SELECT @RetailerConfig = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Retailer Media Server Configuration'
		
		SELECT @Config = ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType ='App Media Server Configuration'
		
		SELECT @RetLocAddr = COALESCE(@RetLocAddr ,'') + RL.Address1 + ', ' +  RL.city + ', ' +  RL.[State] + ', ' +  RL.PostalCode + ' | '
			 , @RetLocID = COALESCE(@RetLocID, '') + cast(RL.RetailLocationID AS VARCHAR(10)) + ' | '
		FROM ProductHotDeal P 
		LEFT JOIN ProductHotDealRetailLocation PHL ON P.ProductHotDealID = PHL.ProductHotDealID 
		LEFT JOIN RetailLocation RL ON PHL.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
		WHERE P.ProductHotDealID = @ProductHotDealID
		
		SELECT DISTINCT PL.City
					  , S.StateName
		INTO #Temp
		FROM ProductHotDeal P
		INNER JOIN ProductHotDealLocation PL ON P.ProductHotDealID = PL.ProductHotDealID
		INNER JOIN State S ON S.Stateabbrevation = PL.State
		WHERE P.ProductHotDealID = @ProductHotDealID
		
		SELECT @City =  COALESCE(@City ,'') + City + ' | '
			 , @State = COALESCE(@State, '') + StateName + ' | '
		FROM #Temp
		
		--To get the HotDeal Details
		SELECT DISTINCT P.ProductHotDealID --hotDealId     
				,HotDealName 
				,R.RetailID 
				,@City City
				,@State State
				,RetailName = CASE WHEN R.RetailID IS NOT NULL THEN R.RetailName END --retName
				,@RetLocAddr RetailLocationAddress
				,@RetLocID retailerLocIds
				,SalePrice
				,Price
				--,CASE WHEN R.RetailID IS NOT NULL THEN RE.Address1 END retAddr
				--,CASE WHEN R.RetailID IS NOT NULL THEN RE.City WHEN R.RetailID IS NULL THEN PHL.City END city
				--,CASE WHEN R.RetailID IS NOT NULL THEN RE.State WHEN R.RetailID IS NULL THEN PHL.State END state
				--,CASE WHEN R.RetailID IS NOT NULL THEN RE.PostalCode END zipcode	   
				,ISNULL(HotDealShortDescription,HotDeaLonglDescription) HotDealShortDescription 
				,HotDeaLonglDescription = CASE WHEN ISNULL(HotDeaLonglDescription,0) = '0' OR  HotDeaLonglDescription = '' THEN     
									 CASE WHEN ISNULL(HotDealShortDescription,0) = '0' OR  HotDealShortDescription = '' THEN HotDealName    
										  ELSE HotDealShortDescription     
									 END    
										 ELSE HotDeaLonglDescription    
									 END    
				,HotDealImagePath = CASE WHEN P.HotDealImagePath IS NOT NULL THEN   
									CASE WHEN P.WebsiteSourceFlag = 1 THEN 
										 CASE WHEN P.ManufacturerID IS NOT NULL THEN @ManufConfig +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+HotDealImagePath  
											  ELSE @RetailerConfig+CONVERT(VARCHAR(30),P.RetailID)+'/'+HotDealImagePath  
										 END  					                         
										 ELSE P.HotDealImagePath  
									END  
								    END  
				, CAST(HotDealStartDate AS DATE) HotDealStartDate
				, CAST(HotDealStartDate AS TIME) DealStartTime
				, CAST(HotDealEndDate AS DATE) HotDealEndDate
				, CAST(HotDealEndDate AS TIME) DealEndTime
				,P.CategoryID
				,Category = C.ParentCategoryName + '-' + C.SubCategoryName   
				,P.HotDealURL --hdURL 
				,A.APIPartnerName --poweredBy
				,@Config + A.APIPartnerImagePath apiPartnerImagePath
		FROM ProductHotDeal P 
		LEFT JOIN Category C ON P.CategoryID = C.CategoryID   
		LEFT JOIN ProductHotDealLocation PHL ON PHL.ProductHotDealID = P.ProductHotDealID
		LEFT JOIN APIPartner A ON A.APIPartnerID=P.APIPartnerID   	   
		LEFT JOIN ProductHotDealRetailLocation RL ON RL.ProductHotDealID =P.ProductHotDealID 
		LEFT JOIN RetailLocation RE ON RL.RetailLocationID =RE.RetailLocationID AND RE.Active=1
		LEFT JOIN Retailer R ON (R.RetailID =P.RetailID OR RE.RetailID =R.RetailID)	AND R.RetailerActive=1  
		WHERE P.ProductHotDealID = @ProductHotDealID
				
		--Confirmation of Success
		SELECT @Status=0
		
	END TRY    
     
	BEGIN CATCH    
		--Check whether the Transaction is uncommitable.    
		IF @@ERROR <> 0    
		BEGIN    
			PRINT 'Error occured in Stored Procedure [usp_WebAdminHotDealDetails].'      
			--- Execute retrieval of Error info.    
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output
			SELECT @Status=1
		END;    
	END CATCH;
   
END;




GO
