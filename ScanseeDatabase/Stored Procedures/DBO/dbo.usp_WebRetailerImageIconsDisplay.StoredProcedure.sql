USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerImageIconsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    
Stored Procedure name : usp_WebRetailerImageIconsDisplay
Purpose     : To Display the default image icons in the Retailer Anything Page Creation & Special Offer Creation.    
Example     : usp_WebRetailerImageIconsDisplay    
    
History    
Version  Date			 Author				Change Description    
---------------------------------------------------------------     
1.0   8t hAugust 2012    Pavan Sharma K		Initial Version    
---------------------------------------------------------------    
*/    
    
CREATE PROCEDURE [dbo].[usp_WebRetailerImageIconsDisplay]    
(    
    @PageType varchar(200)
 --Output Variable--    
   , @ErrorNumber int output    
  , @ErrorMessage varchar(1000) output     
)    
AS    
BEGIN    
    
 BEGIN TRY    
 
	 --To get Media Server Configuration.  
	  DECLARE @Config varchar(50)    
	  SELECT @Config=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Retailer Media Server Configuration'   
	    
	    
	    
	  --Concatenate the image icon name with the Retailer Media Server Configuration.   
      SELECT  QRRetailerCustomPageIconID
			, ImagePath = @Config + QRRetailerCustomPageIconImagePath
      FROM QRRetailerCustomPageIcons WHERE PageType In (Select QRPageTypeID from QRRetailerCustomPageType where QRPageTypeName IN(@PageType,'ALL'))
      
 END TRY    
      
 BEGIN CATCH    
     
  --Check whether the Transaction is uncommitable.    
  IF @@ERROR <> 0    
  BEGIN    
   PRINT 'Error occured in Stored Procedure usp_WebRetailerImageIconsDisplay.'      
   --- Execute retrieval of Error info.    
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output     
       
  END;    
       
 END CATCH;    
END;




GO
