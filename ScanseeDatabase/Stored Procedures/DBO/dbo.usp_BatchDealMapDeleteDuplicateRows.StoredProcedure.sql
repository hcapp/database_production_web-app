USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchDealMapDeleteDuplicateRows]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_BatchDealMapDeleteDuplicateRows]
(
	
	--Output Variable 
	  @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			; WITH DealMap
			AS
			(
			SELECT ID
				  , Ranking  = ROW_NUMBER() OVER(PARTITION BY ID ORDER BY NEWID())
			FROM APIDealMapData
			)
			DELETE FROM DealMap WHERE Ranking > 1
			--To get # Rows affected
			DECLARE @IDDelete int
			SET @IDDelete = @@ROWCOUNT 
			
			;WITH DealMapTitle
			AS
			 (
			 SELECT Title
				  , Ranking  = ROW_NUMBER() OVER(PARTITION BY Title ORDER BY NEWID())
			FROM APIDealMapData
			 )
			DELETE FROM DealMapTitle WHERE Ranking > 1

			--To get # Rows affected
			DECLARE @NameDelete int
			SET @NameDelete = @@ROWCOUNT 
			
			--To get Total # Rows affected
			SELECT @Result = @IDDelete + @NameDelete

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchDealMapDeleteDuplicateRows.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
