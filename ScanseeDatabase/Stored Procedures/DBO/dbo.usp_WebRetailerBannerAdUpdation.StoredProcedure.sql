USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerBannerAdUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerBannerAdUpdation
Purpose					: To edit a Banner Page.
Example					: usp_WebRetailerBannerAdUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerBannerAdUpdation]
(
	--Input Variables
	  @BannerAdID int
	, @RetailID int
	, @RetailLocationIds varchar(max) --CSV
	, @BannerAdName varchar(100)
	, @BannerAdURL varchar(1000)
	, @BannerAdImagePath varchar(255)
	, @StartDate varchar(20)
	, @EndDate varchar(20)
	
	--Output Variable 
	, @InvalidRetailLocations varchar(5000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			--Initialise to empty string
			 SET @InvalidRetailLocations = NULL
			 
			 
			  DECLARE @RetailLocationID INT
			  SELECT @RetailLocationID=RetailLocationID   FROM RetailLocationBannerAd  WHERE AdvertisementBannerID =@BannerAdID
			 
			  DECLARE @ADVertiseidID INT
			  SELECT @ADVertiseidID=AdvertisementBannerID 
			  FROM AdvertisementBanner  where AdvertisementBannerID  in (select AdvertisementBannerID from RetailLocationBannerAd  where RetailLocationID =@RetailLocationID AND AdvertisementBannerID  !=@BannerAdID)
			  
			  CREATE TABLE #Temp(RetailLocationID INT)
			 
			 IF @EndDate IS NULL
			 BEGIN
				INSERT INTO #Temp(RetailLocationID)				
				SELECT RLB.RetailLocationID
				FROM AdvertisementBanner AB
				INNER JOIN RetailLocationBannerAd RLB ON AB.AdvertisementBannerID = RLB.AdvertisementBannerID
				INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLB.RetailLocationID
				WHERE (@StartDate <= (SELECT MAX(StartDate)  FROM AdvertisementBanner WHERE AdvertisementBannerID  =@ADVertiseidID ))
				       OR (@StartDate = (SELECT MAX(EndDate)  FROM AdvertisementBanner WHERE AdvertisementBannerID =@ADVertiseidID ))
				
				     -- OR (@StartDate <= (SELECT MAX(EndDate)  FROM AdvertisementSplash WHERE RetailID  = @RetailID AND RL.Param = RLSA.RetailLocationID AND AdvertisementSplashID !=@WelcomePageID ))
				      OR (EndDate IS NULL AND ((@StartDate >= StartDate OR @StartDate < StartDate)OR (@StartDate = StartDate AND EndDate IS not Null)))
				         
			    AND RLB.AdvertisementBannerID <> @BannerAdID END
			 
			 --Filter all the locations that already has a Banner Ad in the given date range.
			 IF @EndDate IS NOT NULL
			 BEGIN		
				 INSERT INTO #Temp(RetailLocationID)	 
				 SELECT RL.Param
				 FROM AdvertisementBanner AB
				 INNER JOIN RetailLocationBannerAd RLBA ON AB.AdvertisementBannerID = RLBA.AdvertisementBannerID
				 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLBA.RetailLocationID
				 WHERE (@StartDate BETWEEN (SELECT MAX(StartDate)  FROM AdvertisementBanner WHERE RetailID  = @RetailID AND RL.Param = RLBA.RetailLocationID AND AdvertisementBannerID !=@BannerAdID ) AND ISNULL(AB.EndDate, DATEADD(DD, 1, AB.StartDate))
		 				OR
		 				(@EndDate BETWEEN AB.StartDate AND ISNULL(AB.EndDate, DATEADD(DD, 1, AB.StartDate))
		 				 AND (@EndDate >AB.StartDate OR @EndDate = AB.StartDate))
		 			    OR
		 			    (AB.EndDate IS NULL AND @EndDate>AB.StartDate)
		 				OR (@StartDate <AB.StartDate AND @StartDate <AB.EndDate AND @EndDate >AB.StartDate AND @EndDate >AB.EndDate )
		 				)  
		 				
				 AND RLBA.AdvertisementBannerID <> @BannerAdID
			END
			
			SELECT @InvalidRetailLocations = COALESCE(@InvalidRetailLocations+',','') +  CAST(RetailLocationID AS VARCHAR(10))
			FROM #Temp
			GROUP BY RetailLocationID
			 
			--Allow the user to edit the Banner Ad only if no location has a Banner Ad in the given date range.
		    IF (@InvalidRetailLocations IS NULL OR @InvalidRetailLocations LIKE '')
		    BEGIN
				UPDATE AdvertisementBanner
				SET  BannerAdName = @BannerAdName 
				   , BannerAdImagePath = @BannerAdImagePath
				   , BannerAdURL = @BannerAdURL
				   , StartDate = @StartDate
				   , EndDate = @EndDate
				WHERE AdvertisementBannerID = @BannerAdID
				 
				--Delete the existing associations with the retail locations.   
				DELETE FROM RetailLocationBannerAd WHERE AdvertisementBannerID = @BannerAdID				
				--Associate the Retail Locations to the Banner Ad.		
				INSERT INTO RetailLocationBannerAd(RetailLocationID
												 , AdvertisementBannerID
												 , DateCreated)	
										 SELECT [Param]
										      , @BannerAdID
										      , GETDATE()											   
										 FROM dbo.fn_SplitParam(@RetailLocationIds, ',')
				
		    END
										
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerBannerAdUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
