USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerProfileDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerProfileDisplay
Purpose					: To display the profile of the given Retailers.
Example					: usp_WebRetailerProfileDisplay

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			29th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerProfileDisplay]
(
	--Input Parameters
	  @RetailerID  int	 
	
	--Output Parameters	  
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			--To get Media Server Configuration.
			 DECLARE @Config varchar(50)  
			 SELECT @Config=ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration' 

			 DECLARE @RetailLocationID int

			 SELECT @RetailLocationID = RetailLocationID
			 FROM RetailLocation
			 WHERE RetailID = @RetailerID AND Active = 1
			 AND (Headquarters = 1 OR CorporateAndStore = 1)
		 

			 --To get Business CategoryIDs which has subcategories.
			 DECLARE @BusinessSubCategory Varchar(500)
			 DECLARE @SubCategoryIDs Varchar(max)

			 SELECT DISTINCT Row_Num = IDENTITY (int,1,1)
							,RBC.BusinessCategoryID
							,BC.BusinessCategoryName
			INTO #Test
			FROM RetailerBusinessCategory RBC
			INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID 
			WHERE RetailerID = @RetailerID AND BusinessSubCategoryID IS NOT NULL
			ORDER BY BC.BusinessCategoryName

			SELECT @BusinessSubCategory = COALESCE(@BusinessSubCategory + ',','') + CAST(BusinessCategoryID AS VArchar(100))
			FROM #Test


			--To display SubCategory ids
			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
							,RetailerID
							,RBC.BusinessCategoryID
							,SubCategoryID = STUFF(( SELECT ',' + CAST(F.BusinessSubCategoryID AS VARCHAR(1000))
										FROM RetailerBusinessCategory F
										WHERE F.RetailerID = @RetailerID AND F.BusinessCategoryID = RBC.BusinessCategoryID 
										FOR XML PATH ('')
										), 1,1,'')
							,BC.BusinessCategoryName
			INTO #SubCat
			FROM RetailerBusinessCategory RBC
			INNER JOIN BusinessCategory BC ON RBC.BusinessCategoryID = BC.BusinessCategoryID 
			WHERE RBC.RetailerID = @RetailerID AND RBC.BusinessSubCategoryID IS NOT NULL
			ORDER BY BC.BusinessCategoryName


			SELECT DISTINCT @SubCategoryIDs = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(S.SubCategoryID IS NULL,'asd',S.SubCategoryID) AS VARCHAR(MAX))
																	FROM #SubCat S
																	WHERE S.RetailerID = RBC.RetailerID
																	ORDER BY S.Rownum 
																	FOR XML PATH(''))
																	, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
			FROM RetailerBusinessCategory RBC
			WHERE RBC.RetailerID = @RetailerID AND RBC.BusinessSubCategoryID IS NOT NULL


			--To get comma separated Business Category IDs, FilterIds and Filter Value IDs

			DECLARE @BusinessCategoryIDs VARCHAR(1000)
			DECLARE @Filters VARCHAR(1000)
			DECLARE @FilterValues VARCHAR(1000)
			DECLARE @FilterCategory VARCHAR(1000) 

			SELECT  @BusinessCategoryIDs = COALESCE(@BusinessCategoryIDs+',','')+CAST(BusinessCategoryID AS VARCHAR(10)) 
			FROM (SELECT DISTINCT BusinessCategoryID
			FROM RetailerBusinessCategory 
			WHERE RetailerID = @RetailerID)A
		
			SELECT @FilterCategory = COALESCE(@FilterCategory+',','')+CAST(R.BusinessCategoryID AS VARCHAR(10)) 
			FROM BusinessCategory BC
			INNER JOIN  (SELECT DISTINCT BusinessCategoryID
			FROM RetailerBusinessCategory 
			WHERE RetailerID = @RetailerID) R ON R.BusinessCategoryID = Bc.BusinessCategoryID AND BC.BusinessCategoryName IN ('Bars','Dining')
		

			SELECT DISTINCT BusinessCategoryID ,AdminFilterID INTO #Filters
			FROM RetailerFilterAssociation WHERE RetailID = @RetailerID

			SELECT DISTINCT BusinessCategoryID,AdminFilterID,AdminFilterValueID INTO #FilterValues
			FROM RetailerFilterAssociation WHERE RetailID = @RetailerID 


			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
					,RetailID
					,RF.BusinessCategoryID
					,AdminFilterID = STUFF(( SELECT ',' + CAST(F.AdminFilterID AS VARCHAR(1000))
								FROM #Filters F
								WHERE  F.BusinessCategoryID = RF.BusinessCategoryID 
								FOR XML PATH ('')
								), 1,1,'')
			INTO #Temp1
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			--FROM AdminFilterCategoryAssociation CA
			--INNER JOIN RetailerFilterAssociation RF ON  CA.BusinessCategoryID = RF.BusinessCategoryID --AND CA.AdminFilterID = RF.AdminFilterID 
			WHERE RF.RetailID = @RetailerID

		
			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
					,RF.BusinessCategoryID
					,RF.AdminFilterID
					,FilterValueID = STUFF(( SELECT ',' + CAST(V.AdminFilterValueID AS VARCHAR(1000))
										FROM #FilterValues V
										WHERE  V.AdminFilterID = RF.AdminFilterID AND V.BusinessCategoryID = RF.BusinessCategoryID
										FOR XML PATH ('')
										), 1,1,'')
			INTO #Temp2
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			--FROM AdminFilterCategoryAssociation CA
			--INNER JOIN RetailerFilterAssociation RF ON CA.BusinessCategoryID = RF.BusinessCategoryID --AND CA.AdminFilterID = RF.AdminFilterID  
			WHERE RF.RetailID = @RetailerID
			ORDER BY RF.BusinessCategoryID


			SELECT DISTINCT RF.BusinessCategoryID 
						,Filters = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.AdminFilterID IS NULL,'asd',F.AdminFilterID) AS VARCHAR(MAX))
															FROM #Temp1 F
															WHERE F.RetailID = RF.RetailID
															ORDER BY F.Rownum 
															FOR XML PATH(''))
															, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
						,FilterValues = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.FilterValueID IS NULL,'asd',F.FilterValueID) AS VARCHAR(MAX))
															FROM #Temp2 F
															WHERE F.BusinessCategoryID = RF.BusinessCategoryID
															ORDER BY F.Rownum 
															FOR XML PATH(''))
															, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
			INTO #FinalResult
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			--FROM AdminFilterCategoryAssociation C
			--INNER JOIN RetailerFilterAssociation RF ON C.BusinessCategoryID = RF.BusinessCategoryID --AND C.AdminFilterID = RF.AdminFilterID 
			WHERE RF.RetailID = @RetailerID 
			ORDER BY RF.BusinessCategoryID 


			SELECT @Filters = Filters
				   ,@FilterValues = REPLACE(COALESCE(@FilterValues+',','') + CAST(FilterValues AS varchar(max))	, ',' ,'!~~!' )
			FROM #FinalResult

		
			SELECT Distinct R.RetailName AS retailerName
				 , R.Address1 address1
				 , R.Address2 address2
				 , R.State 'state'
				 , retailerImagePath = CASE WHEN RetailerImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),R.RetailID) +'/' + RetailerImagePath ELSE RetailerImagePath END  
										ELSE RetailerImagePath 
							   END
				 , R.City city
				 , R.PostalCode  postalCode
				 , R.CorporatePhoneNo contactPhone
				 , C.ContactFirstName AS contactFName
				 , C.ContactLastname AS contactLName
				 , C.ContactEmail AS contactEmail
				 --, U.Email AS contactEmail
				 , C.ContactPhone AS contactPhoneNo
				 , R.RetailURL AS webUrl
				 , R.NumberOfLocations AS numOfStores
				 , @BusinessCategoryIDs AS bCategory
				 , @Filters AS filters
				 , @FilterValues AS filterValues
				 , @FilterCategory AS filterCategory
				 , @BusinessSubCategory AS subCategory
				 , @SubCategoryIDs AS subCategories
				 , R.IsNonProfitOrganisation AS nonProfit
				 , RL.CorporateAndStore AS islocation
				 , RK.RetailKeyword AS keyword
				 , RL.RetailLocationLatitude AS RetailerLocationLatitude
				 , RL.RetailLocationLongitude AS RetailerLocationLongitude 
			FROM Retailer R 
			INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND RL.Active = 1 AND R.RetailerActive = 1
			--INNER JOIN UserRetailer UR ON R.RetailID = UR.RetailID
			--INNER JOIN Users U ON UR.UserID = U.UserID
			LEFT JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID		
			LEFT JOIN Contact C ON RC.ContactID = C.ContactID
			LEFT OUTER JOIN RetailerKeywords RK ON RK.RetailID =R.RetailID AND (RK.RetailLocationID IS NULL OR RK.RetailLocationID = RL.RetailLocationID)
			WHERE R.RetailID = @RetailerID AND RL.RetailLocationID = @RetailLocationID
			AND (Headquarters = 1 OR CorporateAndStore = 1)
			
		
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerProfileDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;








GO
