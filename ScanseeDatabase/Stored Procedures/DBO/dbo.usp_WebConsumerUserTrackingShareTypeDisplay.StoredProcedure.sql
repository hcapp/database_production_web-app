USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerUserTrackingShareTypeDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebConsumerUserTrackingShareTypeDisplay] 
Purpose               : To capure the deails of the prouct share.  
Example               : [usp_WebConsumerUserTrackingShareTypeDisplay] 
  
History  
Version    Date				Author				Change Description  
---------------------------------------------------------------   
1.0      26th June 2013 	Dhananjaya TR		Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerUserTrackingShareTypeDisplay]  
(    
    
 --OutPut Variable  
   @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
        SELECT ShareTypeID shrTypID
			 , ShareTypeName shrTypNam
        FROM ScanSeeReportingDatabase..ShareType				     

 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebConsumerUserTrackingShareTypeDisplay].'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
