USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerFindDealSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [dbo].[usp_WebConsumerFindDealSearch]
Purpose					: To Search Hot Deals.
Example					: [dbo].[usp_WebConsumerFindDealSearch]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6 May 2013	    Span   	     Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerFindDealSearch]
(
	  
	--Input Variable
	  @UserID int
	, @SearchString varchar(255)
	, @PostalCode int 
    , @LowerLimit int 
    , @RecordCount INT 
	  
    --User Tracking Inputs  
	, @MainMenuID Int
	
	--Output Variable 
	, @RowCount INT OUTPUT    
	, @NextPageFlag bit output 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		 
		 DECLARE @Latitude float
		 DECLARE @Longitude float
		 DECLARE @Radius float
		 DECLARE @UserPostalCode int
		 DECLARE @SearchID Int		
		 DECLARE @MaxCnt INT
		 
		  --To get the row count for pagination.
         DECLARE @UpperLimit int 
		 SELECT @UpperLimit = @LowerLimit + @RecordCount 
		 
		 --To get Default Radius value
		IF @UserID =(Select UserID FROM UserPreference WHERE UserID =@UserID)
		BEGIN
		 SELECT @Radius = LocaleRadius 
		 FROM UserPreference  
		 WHERE UserID = @UserID 
		END
		
		IF (@Radius IS NULL OR @Radius =0)
	    BEGIN			
		 SELECT @Radius = ScreenContent 
		 FROM AppConfiguration 
		 WHERE ConfigurationType = 'HotDeals Default Radius'
		END
		 		 
		 --To get PostalCode using UserID
		 SELECT @UserPostalCode = PostalCode
		 FROM Users
		 WHERE UserID = @UserID
		 
		 --To get Latitude and Longitude for given PostalCode
		 SELECT  @Latitude = Latitude
				,@Longitude = Longitude
		 FROM GeoPosition
		 WHERE PostalCode = @PostalCode OR PostalCode = @UserPostalCode
		 
		 
		 --To get Server Configuration
		 DECLARE @ManufConfig varchar(50) 
		 DECLARE @RetailerConfig varchar(50) 
		   
		 SELECT @ManufConfig =(SELECT ScreenContent  
							 FROM AppConfiguration   
							 WHERE ConfigurationType='Web Manufacturer Media Server Configuration')
			   ,@RetailerConfig =(SELECT ScreenContent  
								 FROM AppConfiguration   
								 WHERE ConfigurationType='Web Retailer Media Server Configuration')
		 FROM AppConfiguration 
		 
		
			
		CREATE TABLE #Deals(ProductHotDealID int
						   ,HotDealName Varchar(1000)
						   ,HotDealShortDescription varchar(2000)
						   ,HotDeaLonglDescription varchar(MAX)
						   ,HotDealStartDate Datetime
						   ,HotDealEndDate Datetime
						   ,HotDealDiscountAmount Money
						   ,HotDealImagePath Varchar(2000)
						   ,Distance Float
						   ,CategoryID int
						   ,Category Varchar(255)
						   ,Price money
						   ,SalePrice money
						   ,HotdealURL Varchar(2000)
						   ,ApipartnerName Varchar(200))
		
		
		
		 IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)    
		 BEGIN  
			
			INSERT INTO #Deals(ProductHotDealID 
						   ,HotDealName 
						   ,HotDealShortDescription 
						   ,HotDeaLonglDescription 
						   ,HotDealStartDate 
						   ,HotDealEndDate 
						   ,HotDealDiscountAmount 
						   ,HotDealImagePath 
						   ,Distance
						   ,CategoryID
						   ,Category
						   ,Price
						   ,SalePrice
						   ,HotdealURL
						   ,ApipartnerName )	
			
			 SELECT  ProductHotDealID
					,HotDealName
					,HotDealShortDescription
					,HotDeaLonglDescription
					,HotDealStartDate
					,HotDealEndDate
					,HotDealDiscountAmount
					,HotDealImagePath
					,Distance
					,CategoryID 
					,Category 
					,Price 
					,SalePrice 	
					,HotDealURL 
					,APIPartnerName 		     
			 FROM  			   
			 (SELECT DISTINCT HD.ProductHotDealID
					,HD.HotDealName
					,HD.HotDealShortDescription
					,HD.HotDeaLonglDescription
					,HD.HotDealStartDate
					,HD.HotDealEndDate
					,HD.HotDealDiscountAmount
					,HotDealImagePath = CASE WHEN WebsiteSourceFlag = 1 THEN
											CASE WHEN HD.ManufacturerID IS NOT NULL 
													THEN @ManufConfig + CAST(HD.ManufacturerID AS varchar(50)) + '/' + HotDealImagePath
											ELSE @RetailerConfig + CAST(HD.RetailID AS varchar(50)) + '/' + HotDealImagePath 
											END
										ELSE HotDealImagePath
										END
				   ,Distance = (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 			   
                   ,HD.CategoryID 
                   ,Category =ISNULL(HD.Category,C.ParentCategoryName)
                   ,Price 
                   ,SalePrice
                   ,HD.HotDealURL  
                   ,AP.APIPartnerName 
			 FROM ProductHotDeal HD
			 INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID=HD.ProductHotDealID
			 LEFT JOIN UserCategory UC ON UC.CategoryID =HD.CategoryID AND UC.UserID =@UserID
			 LEFT JOIN GeoPosition G ON G.PostalCode = PL.PostalCode 
			 LEFT JOIN Category C ON HD.CategoryID =C.CategoryID 
			 LEFT JOIN APIPartner AP ON AP.APIPartnerID =HD.APIPartnerID 
			 WHERE HD.HotDealName LIKE (CASE WHEN @SearchString IS NOT NULL THEN '%'+@SearchString+'%' ELSE '%'END)
			 AND ((1=1 AND UC.UserCategoryID IS NULL) OR (UC.UserID =@UserID AND UC.UserCategoryID IS NOT NULL AND HD.CategoryID =UC.CategoryID))) Deals			  
			 WHERE ISNULL(DISTANCE,0)<=@Radius    
	   
	     END
		 --select All Nationwide hotdeals
		 SELECT DISTINCT PH.ProductHotDealID 
		       ,PH.HotDealName 
		       ,PH.HotDealShortDescription 
		       ,PH.HotDeaLonglDescription
		       ,PH.HotDealStartDate 
		       ,PH.HotDealEndDate 
		       ,PH.HotDealDiscountAmount
		       ,HotDealImagePath = CASE WHEN WebsiteSourceFlag = 1 THEN
										CASE WHEN PH.ManufacturerID IS NOT NULL 
												THEN @ManufConfig + CAST(PH.ManufacturerID AS varchar(50)) + '/' + HotDealImagePath
										ELSE @RetailerConfig + CAST(PH.RetailID AS varchar(50)) + '/' + HotDealImagePath 
										END
									ELSE HotDealImagePath
									END
			   ,Distance = (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN G.Latitude ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN G.Longitude ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 			   
		       ,PH.CategoryID 
		       ,Category =ISNULL(PH.Category,C.ParentCategoryName) 
		       ,Price 
		       ,SalePrice 
		       ,HotDealURL 
		       ,AP.APIPartnerName 
		 INTO #Proucthotdeal       
		 FROM ProductHotDeal PH
		 LEFT JOIN ProductHotDealLocation PL ON PH.ProductHotDealID=PL.ProductHotDealID
		 LEFT JOIN UserCategory UC ON UC.CategoryID =PH.CategoryID AND UC.UserID =@UserID 
		 LEFT JOIN GeoPosition G ON G.PostalCode = PL.PostalCode
		 LEFT JOIN Category C ON C.CategoryID =PH.CategoryID  
		 LEFT JOIN APIPartner AP ON AP.APIPartnerID =PH.APIPartnerID 
		 WHERE PL.ProductHotDealID IS NULL
		 AND ((1=1 AND UC.UserCategoryID IS NULL) OR (UC.UserID =@UserID AND UC.UserCategoryID IS NOT NULL AND PH.CategoryID =UC.CategoryID))
		 AND PH.HotDealName LIKE (CASE WHEN @SearchString IS NOT NULL THEN '%'+@SearchString+'%' ELSE '%'END)
		 AND ISNULL(PH.HotDealStartDate,GETDATE()-1) <=GETDATE()AND ISNULL(PH.HotDealEndDate,GETDATE()+1) >= GETDATE()	
		 ORDER BY PH.HotDealName 		 
		 
			 	 
			 SELECT Rownum=IDENTITY(int,1,1)
			        ,ProductHotDealID
					,HotDealName
					,HotDealShortDescription
					,HotDeaLonglDescription
					,HotDealStartDate
					,HotDealEndDate
					,HotDealDiscountAmount
					,HotDealImagePath
					,Distance
					,CategoryID 
					,Category 
					,Price 
					,SalePrice 
					,HotdealURL
					,APIPartnerName
			 INTO #Hotdeals
			 FROM 
			 (SELECT ProductHotDealID
					,HotDealName
					,HotDealShortDescription
					,HotDeaLonglDescription
					,HotDealStartDate
					,HotDealEndDate
					,HotDealDiscountAmount
					,HotDealImagePath
					,Distance
					,CategoryID 
					,Category 
					,Price 
					,SalePrice 
					,HotdealURL 
					,APIPartnerName
			  FROM #Deals
			  UNION ALL
			  SELECT ProductHotDealID 
					,HotDealName 
					,HotDealShortDescription 
					,HotDeaLonglDescription
					,HotDealStartDate 
					,HotDealEndDate 
					,HotDealDiscountAmount
					,HotDealImagePath
					,Distance
					,CategoryID
					,Category 
					,Price 
					,SalePrice 
					,HotDealURL 
					,APIPartnerName
			  FROM #Proucthotdeal)A		 
		 
			  SELECT @MaxCnt = COUNT(RowNum) FROM #Hotdeals 
			 
			  --Output Total no of Records
			  SET @RowCount = @MaxCnt
			 
			  SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS 
		 
		 
		      --User Tracking Section.
				
				--Check if the search is happening on the Hotdeal name.
				IF @SearchString IS NOT NULL
				BEGIN
				
					INSERT INTO ScanSeeReportingDatabase..HotDealSearch(MainMenuID
																	   ,SearchKeyword
																	   ,CreatedDate)
									
					VALUES(@MainMenuID 
						  ,@SearchString 
						  ,GETDATE())	
					SELECT @SearchID = SCOPE_IDENTITY()      												   
																   
				END
				
				--Capture the impression of the hot deals.
                CREATE Table #Temp1(HotDealListID Int
                                   ,ProductHotDealID Int)
                
               		INSERT INTO ScanSeeReportingDatabase..HotDealList(MainMenuID
																	,HotDealSearchID
																	--,HotDealByCityID
																	,ProductHotDealID                                                                        
																	,CreatedDate)

					OUTPUT inserted.HotDealListID,inserted.ProductHotDealID INTO #Temp1(HotDealListID,ProductHotDealID)
	                
					SELECT @MainMenuID 
						  ,@SearchID 
						  --,@HotDealBycityID 
						  ,ProductHotDealID 
						  ,GETDATE()                          
					FROM #Hotdeals
	                  
					SELECT  Rownum 
					        , T.HotDealListID hotdealLstId
							,D.ProductHotDealID hotDealId
							,HotDealName
							,HotDealShortDescription hDshortDescription
							,HotDeaLonglDescription hDLognDescription
							,HotDealStartDate hDStartDate
							,HotDealEndDate hDEndDate
							,HotDealDiscountAmount
							,HotDealImagePath hotDealImagePath
							,Distance
							,CategoryID hdCatgoryId
							,Category hdCategoryName
							,Price hDPrice
							,SalePrice hDSalePrice
							,HotdealURL hdURL 
							,APIPartnerName apiPartnerName 
					FROM #Hotdeals D
					INNER JOIN #Temp1 T ON D.ProductHotDealID = T.ProductHotDealID
				    WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit	
				    
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <usp_WebConsumerFindDealSearch>.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
