USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchDealMapLocDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchDealMapLocDataPorting
Purpose					: To move Location data from Stage Deal Map table to Production ProductHotDealLocation Table
Example					: usp_BatchDealMapLocDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			29th Sep 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchDealMapLocDataPorting]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			MERGE ProductHotDealLocation AS T
			USING (SELECT AD.ID
						, HD.ProductHotDealID 
						, AD.City
						, AD.State
						, AD.PostalCode
						, AD.Latitude
						, AD.Longitude
				   FROM APIDealMapData AD
						INNER JOIN ProductHotDeal HD ON HD.SourceID = AD.ID ) AS S
			ON (T.ProductHotDealID = S.ProductHotDealID) 
			WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
				THEN INSERT([ProductHotDealID]
						   ,[City]
						   ,[State]
						   ,[PostalCode]
						   ,[HotDealLatitude]
						   ,[HotDealLongitude]) 
					 VALUES(S.[ProductHotDealID]
						   ,S.[City]
						   ,CASE WHEN LEN(S.[State]) <= 2 THEN S.[State] END
						   ,S.[PostalCode]
						   ,S.[Latitude]
						   ,S.[Longitude])
			WHEN MATCHED  
				THEN UPDATE SET T.[City] = S.[City]
						   ,T.[State] = CASE WHEN LEN(S.[State]) <= 2 THEN S.[State] END
						   ,T.[PostalCode] = S.[PostalCode]
						   ,T.[HotDealLatitude] = S.[Latitude]
						   ,T.[HotDealLongitude] = S.[Longitude];
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchDealMapLocDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
