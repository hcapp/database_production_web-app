USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterList]
Purpose                  : To display List of Filters.
Example                  : [usp_WebAdminFilterList]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterList]
(

      --Input Input Parameter(s)--       
        @UserID INT
	  , @FilterName Varchar(500)
	  , @LowerLimit int  
      --, @RecordCount Int
      
      
      --Output Variable--	 
	  , @MaxCnt int  output
      , @NxtPageFlag bit output 
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY

			 --To get the row count for pagination.
			 DECLARE @RecordCount Int
			 set @RecordCount=20		
			 DECLARE @UpperLimit int 
			 SELECT @UpperLimit = @LowerLimit + @RecordCount	
      
	         -- To display List of Filters.
		     SELECT DISTINCT ROW_NUMBER() over (ORDER by A.FilterName  ASC) Rownum
			                ,A.AdminFilterID 
			                ,A.FilterName 
							, CategoryName= REPLACE(STUFF((SELECT ', ' + CAST(B.BusinessCategoryName AS VARCHAR(2000))
                                                                                                     FROM AdminFilterCategoryAssociation F
																									 INNER JOIN BusinessCategory B ON B.BusinessCategoryID =F.BusinessCategoryID 
                                                                                                     WHERE F.AdminFilterID =A.AdminFilterID 
                                                                                                     ORDER BY B.BusinessCategoryName 
                                                                                                     FOR XML PATH(''))
             
			                                                                                        , 1, 2, ''), ' ', '')
			 INTO #Temp
			 FROM AdminFilter A
			 WHERE (@FilterName IS NULL OR (@FilterName IS NOT NULL AND A.FilterName LIKE '%'+@FilterName+'%'))			
			
			 SELECT @MaxCnt = MAX(RowNum) FROM #Temp
             
			 SET @MaxCnt =ISNULL(@MaxCnt,0)             
             --this flag is a indicator to enable "More" button in the UI.   
             --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
             SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END

			 SELECT Rownum
			       ,AdminFilterID filterId
			       ,FilterName filterName
				   ,CategoryName fCategoryName
			 FROM #Temp 
			 WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit  
             ORDER BY RowNum 
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterList.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;



GO
