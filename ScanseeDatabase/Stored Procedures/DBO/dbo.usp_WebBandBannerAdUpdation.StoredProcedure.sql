USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandBannerAdUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandBannerAdUpdation
Purpose					: To edit a Banner Page.
Example					: usp_WebBandBannerAdUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			04/18/2016	Prakash C	Initial Version(usp_WebRetailerBannerAdUpdation)
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandBannerAdUpdation]
(
	--Input Variables
	  @BannerAdID int
	, @RetailID int
	, @RetailLocationIds varchar(max) --CSV
	, @BannerAdName varchar(100)
	, @BannerAdURL varchar(1000)
	, @BannerAdImagePath varchar(255)
	, @StartDate varchar(20)
	, @EndDate varchar(20)
	
	--Output Variable 
	, @InvalidBandLocations varchar(5000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			--Initialise to empty string
			 SET @InvalidBandLocations = NULL
			 
			 
			  DECLARE @RetailLocationID INT
			  SELECT @RetailLocationID=BandLocationID   FROM BandLocationBannerAd  WHERE AdvertisementBannerID =@BannerAdID
			 
			  DECLARE @ADVertiseidID INT
			  SELECT @ADVertiseidID=BandAdvertisementBannerID 
			  FROM BandAdvertisementBanner  where BandAdvertisementBannerID 
			   in (select AdvertisementBannerID from BandLocationBannerAd  where BandLocationID =@RetailLocationID AND AdvertisementBannerID  !=@BannerAdID)
			  
			  CREATE TABLE #Temp(BandLocationID INT)
			 
			 IF @EndDate IS NULL
			 BEGIN
				INSERT INTO #Temp(BandLocationID)				
				SELECT RLB.BandLocationID
				FROM BandAdvertisementBanner AB
				INNER JOIN BandLocationBannerAd RLB ON AB.BandAdvertisementBannerID = RLB.AdvertisementBannerID
				INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLB.BandLocationID
				WHERE (@StartDate <= (SELECT MAX(StartDate)  FROM BandAdvertisementBanner WHERE AdvertisementBannerID  =@ADVertiseidID ))
				       OR (@StartDate = (SELECT MAX(EndDate)  FROM BandAdvertisementBanner WHERE AdvertisementBannerID =@ADVertiseidID ))
				
				     -- OR (@StartDate <= (SELECT MAX(EndDate)  FROM AdvertisementSplash WHERE BandID  = @RetailID AND RL.Param = RLSA.BandLocationID AND AdvertisementSplashID !=@WelcomePageID ))
				      OR (EndDate IS NULL AND ((@StartDate >= StartDate OR @StartDate < StartDate)OR (@StartDate = StartDate AND EndDate IS not Null)))
				         
			    AND RLB.AdvertisementBannerID <> @BannerAdID END
			 
			 --Filter all the locations that already has a Banner Ad in the given date range.
			 IF @EndDate IS NOT NULL
			 BEGIN		
				 INSERT INTO #Temp(BandLocationID)	 
				 SELECT RL.Param
				 FROM BandAdvertisementBanner AB
				 INNER JOIN BandLocationBannerAd RLBA ON AB.BandAdvertisementBannerID = RLBA.AdvertisementBannerID
				 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLBA.BandLocationID
				 WHERE (@StartDate BETWEEN (SELECT MAX(StartDate)  FROM BandAdvertisementBanner WHERE BandID  = @RetailID AND RL.Param = RLBA.BandLocationID AND AdvertisementBannerID !=@BannerAdID ) AND ISNULL(AB.EndDate, DATEADD(DD, 1, AB.StartDate))
		 				OR
		 				(@EndDate BETWEEN AB.StartDate AND ISNULL(AB.EndDate, DATEADD(DD, 1, AB.StartDate))
		 				 AND (@EndDate >AB.StartDate OR @EndDate = AB.StartDate))
		 			    OR
		 			    (AB.EndDate IS NULL AND @EndDate>AB.StartDate)
		 				OR (@StartDate <AB.StartDate AND @StartDate <AB.EndDate AND @EndDate >AB.StartDate AND @EndDate >AB.EndDate )
		 				)  
		 				
				 AND RLBA.AdvertisementBannerID <> @BannerAdID
			END
			
			SELECT @InvalidBandLocations = COALESCE(@InvalidBandLocations+',','') +  CAST(BandLocationID AS VARCHAR(10))
			FROM #Temp
			GROUP BY BandLocationID
			 
			--Allow the user to edit the Banner Ad only if no location has a Banner Ad in the given date range.
		    IF (@InvalidBandLocations IS NULL OR @InvalidBandLocations LIKE '')
		    BEGIN
				UPDATE BandAdvertisementBanner
				SET  BannerAdName = @BannerAdName 
				   , BannerAdImagePath = @BannerAdImagePath
				   , BannerAdURL = @BannerAdURL
				   , StartDate = @StartDate
				   , EndDate = @EndDate
				WHERE BandAdvertisementBannerID = @BannerAdID
				 
				--Delete the existing associations with the Band locations.   
				DELETE FROM BandLocationBannerAd WHERE AdvertisementBannerID = @BannerAdID				
				--Associate the Band Locations to the Banner Ad.		
				INSERT INTO BandLocationBannerAd(BandLocationID
												 , AdvertisementBannerID
												 , DateCreated)	
										 SELECT [Param]
										      , @BannerAdID
										      , GETDATE()											   
										 FROM dbo.fn_SplitParam(@RetailLocationIds, ',')
				
		    END
										
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandBannerAdUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
			   insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
               values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())   
		END;
		 
	END CATCH;
END;



GO
