USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListRetailerList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListRetailerList
Purpose					: To fetch User shopping list Retailer List
Example					: EXEC usp_MasterShoppingList 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListRetailerList]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY 
		SELECT DISTINCT UP.UserID
			, ISNULL(UP.UserRetailPreferenceID, 0) UserRetailPreferenceID 
			, ISNULL(R.RetailID , 0) RetailID
			, ISNULL(R.RetailName, 'Others') RetailName
			--, ISNULL(C.CategoryID, 0) CategoryID
			, ISNULL(C.ParentCategoryID, 0) ParentCategoryID 
			, ISNULL(C.ParentCategoryName, 'Unassigned Products') ParentCategoryName
			, CASE WHEN RetailName <> 'Others' THEN 0 ELSE 1 END
			, CASE WHEN ParentCategoryName <> 'Unassigned Products' THEN 0 ELSE 1 END
			--, C.SubCategoryName  
		FROM UserProduct UP
			LEFT JOIN ProductCategory PC ON PC.ProductID = UP.ProductID 
			LEFT JOIN Category C ON C.CategoryID = PC.CategoryID 
			LEFT JOIN UserRetailPreference UR ON UR.UserRetailPreferenceID = UP.UserRetailPreferenceID 
			LEFT JOIN Retailer R ON R.RetailID = UR.RetailID 
		WHERE UP.UserID = @UserID
			AND UP.MasterListItem = 1 
		ORDER BY CASE WHEN RetailName <> 'Others' THEN 0 ELSE 1 END
			, RetailName
			, CASE WHEN ParentCategoryName <> 'Unassigned Products' THEN 0 ELSE 1 END
			, ParentCategoryName
			--, SubCategoryName
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListRetailerList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
