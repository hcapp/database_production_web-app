USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListRebateList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListRebateList
Purpose					: To display Rebate.
Example					: usp_MasterShoppingListRebateList 1,0,14

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListRebateList]
(
	  @ProductID int
	, @RetailID int
	, @UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
	--To fetch Rebate info of product with Retailer info.
		IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)
		BEGIN
			SELECT R.RebateID
				, R.ManufacturerID 
				, RebateAmount 
				, RebateStartDate 
				, RebateEndDate 
				, RR.RetailID
				, RebateName 
			FROM Rebate R
				INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID 
				INNER JOIN RebateRetailer RR ON RR.RebateID=R.RebateID
			WHERE R.RetailID = @RetailID 
			AND RP.ProductID = @ProductID 
			AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate
			AND R.RebateID NOT IN (SELECT RebateID FROM UserRebateGallery WHERE UserID = @UserID)		
		END
		--To fetch Rebate info of Product from "Others" Category.
		IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS NULL OR @RetailID =0) 
			AND @ProductID IS NOT NULL
		BEGIN
		
			SELECT R.RebateID
				, R.ManufacturerID 
				, RebateAmount 
				, RebateStartDate 
				, RebateEndDate 
				, NULL AS RetailID
				, RebateName 
			FROM Rebate R
				INNER JOIN RebateProduct RP ON RP.RebateID = R.RebateID 
			WHERE RP.ProductID = @ProductID 
			AND GETDATE() BETWEEN R.RebateStartDate AND R.RebateEndDate
			AND R.RebateID NOT IN (SELECT RebateID FROM UserRebateGallery WHERE UserID = @UserID)
		END
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListRebateList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
