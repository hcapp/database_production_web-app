USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandProfileDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebBandProfileDisplay
Purpose					: To display the profile of the given Bands.
Example					: usp_WebBandProfileDisplay

History
Version		Date				Author			Change Description
------------------------------------------------------------------------------- 
1.0			4/25/2016		   Bindu T A		Initial Version  - usp_WebBandProfileDisplay
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandProfileDisplay]
(
	--Input Parameters
	  @RetailerID  int	 
	
	--Output Parameters	  
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @BandID INT
			SET @BandID = @RetailerID

			--To get Media Server Configuration.
			 DECLARE @Config varchar(50)  
			 SELECT @Config=
			 ScreenContent  
			 FROM AppConfiguration   
			 WHERE ConfigurationType='Web Retailer Media Server Configuration' 

			 --DECLARE @RetailLocationID int

			 --SELECT @RetailLocationID = RetailLocationID
			 --FROM RetailLocation
			 --WHERE RetailID = @RetailerID AND Active = 1
			 --AND (Headquarters = 1 OR CorporateAndStore = 1)
		 

			 --To get Business CategoryIDs which has subcategories.
			 DECLARE @BandSubCategory Varchar(500)
			 DECLARE @BandSubCategoryIDs Varchar(max)

			 SELECT DISTINCT Row_Num = IDENTITY (int,1,1)
							,RBC.BandCategoryID
							,BC.BandCategoryName
			INTO #Test
			FROM BandCategoryAssociation RBC
			INNER JOIN BandCategory BC ON RBC.BandCategoryID = BC.BandCategoryID
			WHERE BandID = @BandID AND BandSubCategoryID IS NOT NULL
			ORDER BY BC.BandCategoryName

			SELECT @BandSubCategory = COALESCE(@BandSubCategory + ',','') + CAST(BandCategoryID AS VArchar(100))
			FROM #Test


			--To display BandSubCategory ids
			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
							,BandID
							,RBC.BandCategoryID
							,BandSubCategoryID = STUFF(( SELECT ',' + CAST(F.BandSubCategoryID AS VARCHAR(1000))
										FROM BandCategoryAssociation F
										WHERE F.BandID = @BandID AND F.BandCategoryID = RBC.BandCategoryID 
										FOR XML PATH ('')
										), 1,1,'')
							,BC.BandCategoryName
			INTO #SubCat
			FROM BandCategoryAssociation RBC
			INNER JOIN BandCategory BC ON RBC.BandCategoryID = BC.BandCategoryID 
			WHERE RBC.BandID = @BandID AND RBC.BandSubCategoryID IS NOT NULL
			ORDER BY BC.BandCategoryName


			SELECT DISTINCT @BandSubCategoryIDs = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(S.BandSubCategoryID IS NULL,'asd',S.BandSubCategoryID) AS VARCHAR(MAX))
																	FROM #SubCat S
																	WHERE S.BandID = RBC.BandID
																	ORDER BY S.Rownum 
																	FOR XML PATH(''))
																	, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
			FROM BandCategoryAssociation RBC
			WHERE RBC.BandID = @BandID AND RBC.BandSubCategoryID IS NOT NULL


			--To get comma separated Business Category IDs, FilterIds and Filter Value IDs

			DECLARE @BandCategoryIDs VARCHAR(1000)

			SELECT  @BandCategoryIDs = COALESCE(@BandCategoryIDs+',','')+CAST(BandCategoryID AS VARCHAR(10)) 
			FROM (SELECT DISTINCT BandCategoryID
			FROM BandCategoryAssociation 
			WHERE BandID = @BandID)A
		
		/*  DECLARE @Filters VARCHAR(1000)
			DECLARE @FilterValues VARCHAR(1000)
			DECLARE @FilterCategory VARCHAR(1000) 

			SELECT @FilterCategory = COALESCE(@FilterCategory+',','')+CAST(R.BusinessCategoryID AS VARCHAR(10)) 
			FROM BusinessCategory BC
			INNER JOIN  (SELECT DISTINCT BusinessCategoryID
			FROM RetailerBusinessCategory 
			WHERE RetailerID = @RetailerID) R ON R.BusinessCategoryID = Bc.BusinessCategoryID AND BC.BusinessCategoryName IN ('Bars','Dining')
		

			SELECT DISTINCT BusinessCategoryID ,AdminFilterID INTO #Filters
			FROM RetailerFilterAssociation WHERE RetailID = @RetailerID

			SELECT DISTINCT BusinessCategoryID,AdminFilterID,AdminFilterValueID INTO #FilterValues
			FROM RetailerFilterAssociation WHERE RetailID = @RetailerID 


			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
					,RetailID
					,RF.BusinessCategoryID
					,AdminFilterID = STUFF(( SELECT ',' + CAST(F.AdminFilterID AS VARCHAR(1000))
								FROM #Filters F
								WHERE  F.BusinessCategoryID = RF.BusinessCategoryID 
								FOR XML PATH ('')
								), 1,1,'')
			INTO #Temp1
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			--FROM AdminFilterCategoryAssociation CA
			--INNER JOIN RetailerFilterAssociation RF ON  CA.BusinessCategoryID = RF.BusinessCategoryID --AND CA.AdminFilterID = RF.AdminFilterID 
			WHERE RF.RetailID = @RetailerID

		
			SELECT DISTINCT Rownum = IDENTITY(INT,1,1)
					,RF.BusinessCategoryID
					,RF.AdminFilterID
					,FilterValueID = STUFF(( SELECT ',' + CAST(V.AdminFilterValueID AS VARCHAR(1000))
										FROM #FilterValues V
										WHERE  V.AdminFilterID = RF.AdminFilterID AND V.BusinessCategoryID = RF.BusinessCategoryID
										FOR XML PATH ('')
										), 1,1,'')
			INTO #Temp2
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			--FROM AdminFilterCategoryAssociation CA
			--INNER JOIN RetailerFilterAssociation RF ON CA.BusinessCategoryID = RF.BusinessCategoryID --AND CA.AdminFilterID = RF.AdminFilterID  
			WHERE RF.RetailID = @RetailerID
			ORDER BY RF.BusinessCategoryID


			SELECT DISTINCT RF.BusinessCategoryID 
						,Filters = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.AdminFilterID IS NULL,'asd',F.AdminFilterID) AS VARCHAR(MAX))
															FROM #Temp1 F
															WHERE F.RetailID = RF.RetailID
															ORDER BY F.Rownum 
															FOR XML PATH(''))
															, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
						,FilterValues = (REPLACE((REPLACE((REPLACE(REPLACE(STUFF((SELECT ', ' + CAST(IIF(F.FilterValueID IS NULL,'asd',F.FilterValueID) AS VARCHAR(MAX))
															FROM #Temp2 F
															WHERE F.BusinessCategoryID = RF.BusinessCategoryID
															ORDER BY F.Rownum 
															FOR XML PATH(''))
															, 1, 2, ''), ' ', '|'),'asd','NULL')),',|','!~~!')),',','|')) 
			INTO #FinalResult
			FROM AdminFilterBusinessCategory FBC
			INNER JOIN RetailerFilterAssociation RF ON RF.BusinessCategoryID = FBC.BusinessCategoryID
			LEFT JOIN AdminFilterCategoryAssociation CA ON  RF.BusinessCategoryID = CA.BusinessCategoryID
			--FROM AdminFilterCategoryAssociation C
			--INNER JOIN RetailerFilterAssociation RF ON C.BusinessCategoryID = RF.BusinessCategoryID --AND C.AdminFilterID = RF.AdminFilterID 
			WHERE RF.RetailID = @RetailerID 
			ORDER BY RF.BusinessCategoryID 


			SELECT @Filters = Filters
				   ,@FilterValues = REPLACE(COALESCE(@FilterValues+',','') + CAST(FilterValues AS varchar(max))	, ',' ,'!~~!' )
			FROM #FinalResult

*/		
			SELECT Distinct B.BandName AS retailerName
				 , B.Address1 address1
				 , B.Address2 address2
				 , B.State 'state'
				 , retailerImagePath = CASE WHEN BandImagePath IS NOT NULL THEN CASE WHEN B.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),B.BandID) +'/' + BandImagePath ELSE BandImagePath END  
										ELSE BandImagePath 
							   END
				 , B.City city
				 , B.PostalCode  postalCode
				 , B.CorporatePhoneNo contactPhone
				 --, BL.ContactFirstName AS contactFName
				 --, BL.ContactLastname AS contactLName
				 --, BL.ContactEmail AS contactEmail
				 --, U.Email AS contactEmail
				 --, C.ContactPhone AS contactPhoneNo
				 --, B.BandURL AS webUrl
				 --, BL.NumberOfLocations AS numOfStores
				 , @BandCategoryIDs AS bCategory
				 --, @Filters AS filters
				 --, @FilterValues AS filterValues
				 --, @FilterCategory AS filterCategory
				 , @BandSubCategory AS subCategory
				 , @BandSubCategoryIDs AS subCategories
				 , B.IsNonProfitOrganisation AS nonProfit
				 --, BL.CorporateAndStore AS islocation
				 --, BL.RetailKeyword AS keyword
				 --, BL.RetailLocationLatitude AS RetailerLocationLatitude
				 --, BL.RetailLocationLongitude AS RetailerLocationLongitude 
			FROM Band B 
			--INNER JOIN RetailLocation RL ON R.RetailID = RL.RetailID AND RL.Active = 1 AND R.RetailerActive = 1
			--INNER JOIN UserRetailer UR ON R.RetailID = UR.RetailID
			--INNER JOIN Users U ON UR.UserID = U.UserID
			--LEFT JOIN BandContact BC ON BC.BandLocationID= RC.RetailLocationID		
			--LEFT JOIN Contact C ON RC.ContactID = C.ContactID
			--LEFT OUTER JOIN RetailerKeywords RK ON RK.RetailID =R.RetailID AND (RK.RetailLocationID IS NULL OR RK.RetailLocationID = RL.RetailLocationID)
			WHERE B.bandID = @BandID --AND RL.RetailLocationID = @RetailLocationID
			--AND (Headquarters = 1 OR CorporateAndStore = 1)
			
		
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebBandProfileDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;

GO
