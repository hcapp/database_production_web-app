USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ScanProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_ScanProduct  
Purpose     : To get the details of the scanned product.  
Example     : EXEC usp_ScanProduct 35, '38000391200', NULL, NULL, NULL, 1234, -73.99653, 40.750742, 0, '6/2/2011', NULL  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   2nd June 2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_ScanProduct]  
(  
    @UserId int  
      ,@ScanCode varchar(20)  
        
        
      --Scan History and ProductUserHit Variables  
    --, @RetailLocationID int    
 --, @ProductID int    
 --, @RebateID int    
 , @DeviceID varchar(60)  
 , @ScanType varchar(10)    
 , @ScanLongitude float    
 , @ScanLatitude float    
 , @FieldAgentRequest bit    
 , @Date datetime   
 --, @ScanTypeID tinyint   
   
 --OutPut Variable  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
  --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
	  
  DECLARE @ScanTypeID tinyint  
  SELECT @ScanTypeID = ScanTypeID  
  FROM ProductScanCodeType   
  WHERE ScanType = @ScanType  
  DECLARE @ProductID int  
  DECLARE @RebateID int   
  --To get scanned product details  
  SELECT ProductID    
     , ProductName    
     , M.ManufName      
     , ModelNumber    
     , ProductLongDescription   
     , ProductExpirationDate    
     , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END     
     , SuggestedRetailPrice   
     , Weight    
     , WeightUnits  
  INTO #Product          
  FROM Product P  
   LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID  
  WHERE ScanCode = @ScanCode  
   AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())  
    
  SELECT ProductID productId  
     , ProductName productName  
     , ManufName  manufacturersName  
     , ModelNumber modelNumber  
     , ProductLongDescription   
     , ProductExpirationDate productExpDate  
     , ProductImagePath imagePath  
     , SuggestedRetailPrice   
     , Weight productWeight  
     , WeightUnits        
  FROM #Product   
    
  -- To get the count of product fetched.  
  DECLARE @ROWCOUNT int = @@ROWCOUNT  
  DECLARE @Status int  
  SELECT @ProductID = ProductID FROM #Product     
    
  -- If No Product found, then scan is unsuccessful  
  IF @ROWCOUNT < 1  
  BEGIN  
   EXEC usp_ScanUnSuccessScanHistory @UserID, @DeviceID, @ScanLongitude, @ScanLatitude, @FieldAgentRequest, @Date, @ScanCode, @ScanTypeID, 0, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
  END   
  --If Product found, then scan is successful  
  ELSE IF @ROWCOUNT > 0   
  BEGIN  
   DECLARE @RetailLocationID int     
   -- If GPS is ON and the Scan Latitude and Longitude is passed, fetching Retailer location whose coordinates matches.    
   IF @RetailLocationID IS NULL AND (@ScanLatitude IS NOT NULL AND @ScanLongitude IS NOT NULL)    
   BEGIN    
      SELECT RL.RetailLocationID    
      INTO #RetailLocation    
      FROM RetailLocationProduct RLP     
    INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID     
      WHERE RLP.ProductID = @ProductID     
    AND (RL.RetailLocationLatitude = @ScanLatitude AND RL.RetailLocationLongitude = @ScanLongitude)    
    -- If Single Retail locations matched then capture.    
    DECLARE @RetailLoc int    
    SELECT @RetailLoc = CASE WHEN COUNT(RetailLocationID) = 1 THEN MIN(RetailLocationID) END    
    FROM #RetailLocation  
    END  
   EXEC usp_ScanSuccessScanHistory  @UserID, @ProductID, @RetailLocationID,  @RebateID, @DeviceID, @ScanLongitude, @ScanLatitude, @FieldAgentRequest, @Date, @ScanCode, @ScanTypeID, 1, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
   EXEC usp_UserProductHit @UserID, @ProductID, @RetailLocationID, @Status = @Status output, @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output    
  END  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_ScanProduct.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;

GO
