USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerDeleteProduct]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerDeleteProduct
Purpose					: To delete the product.
Example					: usp_WebRetailerDeleteProduct

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			05 Jan 2012	   Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerDeleteProduct]
(
	 @RetailID int
	,@ProductID int
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
    
	  BEGIN TRY
	  BEGIN TRANSACTION
	  
		DELETE FROM  RetailLocationProduct
		WHERE RetailLocationID IN (SELECT RL.RetailLocationID
									FROM RetailLocationProduct RLP
									INNER JOIN RetailLocation RL ON RLP.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
									AND RL.RetailID = @RetailID 
									AND RLP.ProductID = @ProductID)
									
		AND ProductID =@ProductID
									
     	set @Status = 0								
									
     COMMIT TRANSACTION								
	 END TRY
	
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerDeleteProduct.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			set @Status = 1
			ROLLBACK TRANSACTION
			
		END;
		 
	END CATCH;
END;




GO
