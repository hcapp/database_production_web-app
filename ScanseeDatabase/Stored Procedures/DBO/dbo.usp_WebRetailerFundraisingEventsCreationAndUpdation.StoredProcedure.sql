USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerFundraisingEventsCreationAndUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerFundraisingEventsCreationAndUpdation
Purpose					: Creating/Updating New Retailer Fundraising Events.
Example					: usp_WebRetailerFundraisingEventsCreationAndUpdation

History
Version		  Date			Author	  Change Description
--------------------------------------------------------------- 
1.0			11/11/2014       SPAN		   1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerFundraisingEventsCreationAndUpdation]
(
   
   --Input variable	
      @UserID Int
	, @FundraisingID Int
	, @FundraisingName Varchar(1000)
	, @FundraisingOrganizationName Varchar(1000)
	, @FundraisingOrganizationImagePath Varchar(1000)
	, @FundraisingRetailLocationFlag Bit
	, @RetailLocationID Int
	, @ShortDescription Varchar(1000)
	, @LongDescription Varchar(max)
	, @HcFundraisingCategoryID Varchar(100) 
	--, @HcFundraisingDepartmentID Varchar(100) 
	, @RetailID Int
	, @StartDate Date
	--,@StartTime Time
	, @EndDate Date
	--,@EndTime Time
	, @FundraisingEventFlag Bit
	, @EventID Varchar(max)	
	, @MoreInformationURL Varchar(1000)
	, @PurchaseProductURL Varchar(1000)
	, @FundraisingGoal Varchar(1000)
	, @CurrentLevel Varchar(500) 
	, @Address Varchar(1000)
	, @City Varchar(100)
	, @State Varchar(100)
	, @PostalCode Varchar(20)
	, @Latitude float
	, @Longitude float
	, @GeoErrorFlag bit
	
	--Output Variable 
	, @FundraisingEventID Int Output		
    , @Status Int output        
	, @ErrorNumber Int output
	, @ErrorMessage Varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	 BEGIN TRANSACTION           													

			IF @FundraisingID IS NULL
			BEGIN
				--Creating New Event		
				INSERT INTO HcFundraising(FundraisingName
										, FundraisingOrganizationName
										, FundraisingOrganizationImagePath
										, ShortDescription
										, LongDescription
										, RetailID
										, StartDate
										, EndDate
										, FundraisingGoal
										, CurrentLevel										
										, FundraisingAppsiteFlag
										, FundraisingEventFlag
										, MoreInformationURL
										, PurchaseProductURL
										, CreatedUserID										
										, DateCreated
										, Active)	
																				
								SELECT LTRIM(RTRIM(@FundraisingName))
								     , @FundraisingOrganizationName
									 , @FundraisingOrganizationImagePath
									 , @ShortDescription 
									 , @LongDescription 			      
									 , @RetailID 
									 , CAST(@StartDate AS DATETIME)--+' '+CAST(@StartTime AS DATETIME) 													 								
									 , CAST(@EndDate AS DATETIME)--+' '+CAST(@EndTime AS DATETIME)	
									 , @FundraisingGoal
									 , @CurrentLevel
									 , @FundraisingRetailLocationFlag		
									 , @FundraisingEventFlag					
									 , @MoreInformationURL
									 , @PurchaseProductURL
									 , @UserID
									 , GETDATE()
									 , 1
					
				
				SET @FundraisingEventID = SCOPE_IDENTITY()

				INSERT INTO HcFundraisingCategoryAssociation(HcFundraisingID
															, HcFundraisingCategoryID
															--, HcFundraisingDepartmentID
															, CreatedUserID															
															, DateCreated)
												 
												 SELECT @FundraisingEventID
												       , @HcFundraisingCategoryID
													  -- , @HcFundraisingDepartmentID
													   , @UserID
													   , GETDATE()
															

				--If RetailLocation associated, insert into Fundraising RetailerAssociation table
				IF @FundraisingRetailLocationFlag = 1
				BEGIN

					INSERT INTO HcRetailerFundraisingAssociation(HcFundraisingID
															   , RetailID
															   , RetailLocationID
															   , CreatedUserID
															   , DateCreated)
													 
												SELECT @FundraisingEventID
													  , @RetailID
													  , @RetailLocationID											  
													  , @UserID 
													  , GETDATE()
												
				END


				--If Events associated, insert into Fundraising Event Association table
				IF @FundraisingEventFlag = 1
				BEGIN
					INSERT INTO HcFundraisingEventsAssociation(HcFundraisingID
															 , HcEventID
															 , CreatedUserID															
															 , DateCreated)
																
													SELECT @FundraisingEventID
														  , Param 
														  , @UserID
														  , GETDATE()					  
													FROM dbo.fn_SplitParam(@EventID, ',') 
				END	
				
				--If Fundraising is Organization hosting, insert into HcFundraisingLocation table
				IF @FundraisingRetailLocationFlag = 0
				BEGIN
					INSERT INTO HcFundraisingLocation(HcFundraisingID
													, Address
													, City
													, State
													, PostalCode
													, Latitude
													, Longitude
													, DateCreated
													, CreatedUserID
													, GeoErrorFlag
													)
											SELECT @FundraisingEventID
													, @Address
													, @City
													, @State
													, @PostalCode
													, @Latitude
													, @Longitude
													, GETDATE()
													, @UserID
													, @GeoErrorFlag
				END

			END
			ELSE IF @FundraisingID IS NOT NULL
			BEGIN 
				UPDATE HcFundraising SET FundraisingName = @FundraisingName
										, FundraisingOrganizationName = @FundraisingOrganizationName
										, FundraisingOrganizationImagePath = @FundraisingOrganizationImagePath
										, ShortDescription = @ShortDescription
										, LongDescription = @LongDescription
										, StartDate = CAST(@StartDate AS DATETIME)--+' '+CAST(@StartTime AS DATETIME) 
										, EndDate = CAST(@EndDate AS DATETIME)--+' '+CAST(@EndTime AS DATETIME)
										, FundraisingGoal = @FundraisingGoal
										, CurrentLevel = @CurrentLevel										
										, FundraisingAppsiteFlag = @FundraisingRetailLocationFlag 
										, FundraisingEventFlag = @FundraisingEventFlag
										, MoreInformationURL = @MoreInformationURL
										, PurchaseProductURL = @PurchaseProductURL
										, ModifiedUserID	= @UserID									
										, DateModified = GETDATE()
										, Active = 1
               WHERE HcFundraisingID = @FundraisingID

				DELETE FROM HcFundraisingCategoryAssociation WHERE HcFundraisingID = @FundraisingID 

				INSERT INTO HcFundraisingCategoryAssociation(HcFundraisingID
															, HcFundraisingCategoryID
															--, HcFundraisingDepartmentID
															, CreatedUserID															
															, DateCreated)
												 
													 SELECT @FundraisingID
														   , @HcFundraisingCategoryID
														   --, @HcFundraisingDepartmentID
														   , @UserID
														   , GETDATE()


			   --If RetailLocation associated, insert into Fundraising RetailerAssociation table
			   DELETE FROM HcRetailerFundraisingAssociation WHERE HcFundraisingID = @FundraisingID

				IF @FundraisingRetailLocationFlag = 1
				BEGIN

					INSERT INTO HcRetailerFundraisingAssociation(HcFundraisingID
															   , RetailID
															   , RetailLocationID
															   , CreatedUserID
															   , DateCreated)
													 
														SELECT @FundraisingID
															  , @RetailID
															  , @RetailLocationID											  
															  , @UserID 
															  , GETDATE()
														
				END
				

				--If Events associated, insert into Fundraising Event Association table		
				DELETE FROM HcFundraisingEventsAssociation WHERE HcFundraisingID = @FundraisingID
					
				IF @FundraisingEventFlag = 1
				BEGIN
					
					INSERT INTO HcFundraisingEventsAssociation(HcFundraisingID
																, HcEventID
																, ModifiedUserID
																, DateModified)
																
													SELECT @FundraisingID
														  , Param 
														  , @UserID
														  , GETDATE()					  
													FROM dbo.fn_SplitParam(@EventID, ',') 
				END
				
				--If Fundraising is Organization hosting, insert into HcFundraisingLocation table
			
				DELETE FROM HcFundraisingLocation WHERE HcFundraisingID = @FundraisingID

				IF @FundraisingRetailLocationFlag = 0
				BEGIN
				
					INSERT INTO HcFundraisingLocation(HcFundraisingID
													, Address
													, City
													, State
													, PostalCode
													, Latitude
													, Longitude
													, DateModified
													, ModifiedUserID
													, GeoErrorFlag
													)
											SELECT @FundraisingID
													, @Address
													, @City
													, @State
													, @PostalCode
													, @Latitude
													, @Longitude
													, GETDATE()
													, @UserID
													, @GeoErrorFlag
												
				END		
			END		
			
	       --Confirmation of Success.
		   SELECT @Status = 0
		  COMMIT TRANSACTION
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure usp_WebRetailerFundraisingEventsCreationAndUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
