USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUserPreferences]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUserPreferences
Purpose					: To Capture UserPreferences On Alerts.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUserPreferences]
(

	--Input Input Parameter(s)--		
	  
	  --Inputs for UserPreferences Table
	  @UserID int
	, @EmailAlert Bit
	, @CellPhoneAlert Bit
	
	--Input for UserCategoryTable
	--, @Category varchar(max)  --Category IDs as CSV
	 
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		    
		    --Create a record in Userpreferences Table.
		    
		    IF NOT EXISTS(Select 1 From UserPreference Where UserID = @UserID)
		    Begin
			INSERT INTO UserPreference (
											UserID
										  , EmailAlert
										  , CellPhoneAlert										  
										)
							  Values(
										@UserID
									  , @EmailAlert
									  , @CellPhoneAlert
							         )
			End	 
			
			ELSE			
			
			--Create a record in Userpreferences Table.
			BEGIN
				UPDATE UserPreference SET EmailAlert = @EmailAlert
										, CellPhoneAlert = @CellPhoneAlert
							WHERE UserID = @UserID
			END    
							         
    --      --Create a record in UserCategory Table.	
    --      IF(@Category IS NOT NULL)		
    --      BEGIN				         
    --      DELETE FROM UserCategory WHERE UserID = @UserID
    --      INSERT INTO UserCategory (
				--						UserID
				--					  , CategoryID
				--					  , DateAdded
				--					)
				--		    SELECT @UserID
				--		         , PARAM
				--		         , GETDATE()
				--		    FROM fn_SplitParam(@Category, ',')
		  --END
		  
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUserPreferences.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
