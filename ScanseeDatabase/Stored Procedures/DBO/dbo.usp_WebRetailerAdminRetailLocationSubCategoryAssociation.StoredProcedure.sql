USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetailLocationSubCategoryAssociation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetailLocationSubCategoryAssociation
Purpose					: To associate the RetailLocations to business sub category.
Example					: usp_WebRetailerAdminRetailLocationSubCategoryAssociation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			26/03/2014			Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerAdminRetailLocationSubCategoryAssociation]

	--Input parameters	 
	  @RetailID INT
	, @RetailLocationID INT	
	, @BusinessCategoryIDs VARCHAR(1000)
	, @UserID int
	--Output variables 
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
		DELETE FROM HcRetailerSubCategory WHERE RetailLocationID = @RetailLocationID
		DELETE FROM RetailerBusinessCategory WHERE retailerid = @RetailID and BusinessSubCategoryID is not null

		INSERT INTO HcRetailerSubCategory( RetailID
										 , RetailLocationID
										 , HcBusinessSubCategoryID
										 , DateCreated 
										 , CreatedUserID
										 , BusinessCategoryID)
								SELECT @RetailID
									 , @RetailLocationID
									 , Param
									 , GETDATE()
									 , @UserID
									 , BC.BusinessCategoryID
								FROM dbo.fn_SplitParam(@BusinessCategoryIDs, ',') F
								INNER JOIN HcBusinessSubCategory SC ON F.Param = SC.HcBusinessSubCategoryID
								INNER JOIN HcBusinessSubCategoryType T ON SC.HcBusinessSubCategoryTypeID = T.HcBusinessSubCategoryTypeID
								INNER JOIN BusinessCategory BC ON T.BusinessCategoryID = BC.BusinessCategoryID

select RetailID,BusinessCategoryID,HcBusinessSubCategoryID into #ins  from HcRetailerSubCategory  where RetailID=@RetailID
except
select  retailerid,BusinessCategoryID,BusinessSubCategoryID  from RetailerBusinessCategory where RetailerID=@RetailID

insert  into RetailerBusinessCategory(retailerid,BusinessCategoryID,BusinessSubCategoryID,DateCreated)
select distinct #ins.*,getdate()  from #ins
		
		-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

		------------------------------------------------					 
							 
		 
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetailLocationSubCategoryAssociation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
