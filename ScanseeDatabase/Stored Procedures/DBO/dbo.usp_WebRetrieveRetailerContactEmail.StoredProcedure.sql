USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveRetailerContactEmail]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebRetrieveRetailerContactEmail]  
Purpose     :   To Retrieve the Contact Email of the Retailer to be notified.
Example     : [usp_WebRetrieveRetailerContactEmail]  
  
History  
Version  Date		 Author			Change Description  
---------------------------------------------------------------   
1.0   16thMay2012  Pavan Sharma K Initial	Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebRetrieveRetailerContactEmail]  
(  
    @UserID int
  , @RetailerID int
 
 --Output Variable   
 
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
     SELECT C.ContactID
		  , C.ContactEmail
     FROM RetailLocation RL     
     INNER JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
     INNER JOIN Contact C ON RC.ContactID = C.ContactID 
     WHERE RetailID = @RetailerID
     AND (Headquarters = 1 OR CorporateAndStore = 1) AND RL.Active = 1
    
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebRetrieveRetailerContactEmail].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
     
  END;  
     
 END CATCH;  
END;




GO
