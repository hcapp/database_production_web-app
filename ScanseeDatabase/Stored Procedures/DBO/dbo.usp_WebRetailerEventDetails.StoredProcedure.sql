USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerEventDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerEventDetails]
Purpose					: To display Retailer Event Details.
Example					: [usp_WebRetailerEventDetails]

History
Version		Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014		SPAN           1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerEventDetails]
(   
    --Input variable.	  
	  @EventID Int
  
	--Output Variable 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
			
			DECLARE @EventCategoryID Varchar(2000)
			DECLARE @RetailLocationID Varchar(2000)
			DECLARE @Config VARCHAR(500)
			DECLARE @WeeklyDays VARCHAR(1000)

	        SELECT @Config=ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Web Retailer Media Server Configuration'
			
			SELECT @EventCategoryID = COALESCE(@EventCategoryID+',', '') + CAST(HcEventCategoryID AS VARCHAR)
			FROM HcEventsCategoryAssociation 
			WHERE HcEventID = @EventID 
			
			SELECT @WeeklyDays = COALESCE(@WeeklyDays+',', '') + CAST([DayName] AS VARCHAR)
			FROM HcEventInterval			
			WHERE HcEventID = @EventID
			
			SET @WeeklyDays=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@WeeklyDays, 'Sunday', '1'), 'Monday', '2'), 'Tuesday', '3'), 'Wednesday', '4'), 'Thursday', '5'), 'Friday', '6'), 'Saturday', '7')

			--To display Event Details.
			SELECT DISTINCT E.HcEventID EventID
						   ,HcEventName eventName 
			               ,ShortDescription shortDescription
                           ,LongDescription longDescription 								
						   ,eventImagePath = @Config + CAST(E.RetailID AS VARCHAR(100))+'/'+ ImagePath 
						   ,ImagePath eventImageName
						   ,EvtImagePath = @Config + CAST(E.RetailID AS VARCHAR(100))+'/'+ EventListingImagePath 
						   ,EventListingImagePath EvtImageName
						   ,BussinessEvent bsnsLoc
						   ,eventStartDate =CAST(StartDate AS DATE)
						   ,eventEndDate =ISNULL(CAST(EndDate AS DATE),NULL)
						   ,eventStartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
						   ,eventEndTime =ISNULL(CAST(CAST(EndDate AS Time) AS VARCHAR(5)),NULL)
						    ,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcRetailerEventsAssociation A 
																			  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID AND RL.Active = 1)
																		 ELSE EL.Address 
										 END
							  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID AND RL.Active = 1)
																		 ELSE EL.City 
									  END
							  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID AND RL.Active = 1)
																		 ELSE EL.State 
									  END
							  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcRetailerEventsAssociation A 
																			    INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			    WHERE A.HcEventID = E.HcEventID AND RL.Active = 1)
																		  ELSE EL.PostalCode 
									  END  	
						
						     ,Latitude = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.RetailLocationLatitude FROM HcRetailerEventsAssociation A 
																			    INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			    WHERE A.HcEventID = E.HcEventID)
																		  ELSE EL.Latitude 
									  END  	
							 ,Longitude = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.RetailLocationLongitude FROM HcRetailerEventsAssociation A 
																			    INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			    WHERE A.HcEventID = E.HcEventID AND RL.Active = 1)
																		  ELSE EL.Longuitude 
									  END  		
						   ,@EventCategoryID  EventCategory		
						   ,RetailLocationIDs = REPLACE(SUBSTRING((SELECT ( ', ' + CAST(RetailLocationID AS VARCHAR(10)))
													FROM HcRetailerEventsAssociation REA
													WHERE REA.HcEventID = E.HcEventID
												    FOR XML PATH( '' )
												  ), 3, 1000 ), ' ', '')
						  ,geoError = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(EL.GeoErrorFlag,0) END 		
						  ,MoreInformationURL moreInfoURL
						  ,OnGoingEvent isOnGoing	 	
						  ,E.HcEventRecurrencePatternID recurrencePatternID
						  ,HR.RecurrencePattern recurrencePatternName
						  ,[isWeekDay] = CASE WHEN HR.RecurrencePattern ='Daily' AND RecurrenceInterval IS NULL THEN 1 ELSE 0 END 		
						  ,REPLACE(REPLACE(@WeeklyDays, '[', ''), ']', '') Days	
						  ,ISNULL(RecurrenceInterval,MonthInterval) AS RecurrenceInterval
						  ,ByDayNumber = CASE WHEN HR.RecurrencePattern = 'Monthly' AND DayName IS NULL THEN 1 ELSE 0 END
						  ,DayNumber
						  --,MonthInterval	
						  ,EventFrequency AS EndAfter									
			FROM HcEvents E
			LEFT JOIN HcRetailerEventsAssociation RE ON RE.HcEventID = E.HcEventID
			LEFT JOIN RetailLocation RL ON RE.RetailLocationID = RL.RetailLocationID AND RL.Active = 1
			LEFT JOIN HcEventLocation EL ON E.HcEventID =EL.HcEventID 
			LEFT JOIN HcEventRecurrencePattern HR ON HR.HcEventRecurrencePatternID = E.HcEventRecurrencePatternID
			LEFT JOIN HcEventInterval EI ON EL.HcEventID = EI.HcEventID  OR RE.HcEventID = EI.HcEventID
			WHERE E.HcEventID = @EventID 
			AND E.RetailID IS NOT NULL
			AND E.Active = 1
			
			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerEventDetails].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
