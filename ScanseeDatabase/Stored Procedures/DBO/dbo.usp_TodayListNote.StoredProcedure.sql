USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_TodayListNote]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_TodayListNote  
Purpose     : To Add User Notes.  
Example     : usp_TodayListNote 14, 'Book', '6/20/2011'  
  
History  
Version  Date   Author   Change Description  
---------------------------------------------------------------   
1.0   6th June 2011 SPAN Infotech India Initail Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_TodayListNote]  
(  
 @UserID int  
 , @Note varchar(255)  
 , @NoteAddDate datetime  
   
 --Output Variable   
 , @Status int output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
  IF EXISTS (SELECT 1 FROM UserShoppingCartNote  WHERE [UserID]= @UserID )  
    
     UPDATE [UserShoppingCartNote]  
                 SET [Note] = @Note  
                     ,[Active] = 1,  
                      [NoteModifiyDate] = @NoteAddDate  
                  WHERE  [UserID] = @UserID  
  
     
  ELSE   
  
              INSERT INTO [UserShoppingCartNote]  
      ([UserID]  
      ,[Note]  
      ,[Active]  
      ,[NoteAddDate])  
   VALUES  
      (@UserID  
      ,@Note  
      ,1   
      ,@NoteAddDate)  
                 
   --Confirmation of Success.  
   SELECT @Status = 0  
  COMMIT TRANSACTION  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_TodayListNote.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION;  
   --Confirmation of failure.  
   SELECT @Status = 1  
  END;  
     
 END CATCH;  
END;

GO
