USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerWelcomePageUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerWelcomePageUpdation
Purpose					: To edit a Welcome page.
Example					: usp_WebRetailerWelcomePageUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerWelcomePageUpdation]
(
	--Input Variables
	  @WelcomePageID int
	, @RetailID int
	, @RetailLocationIds varchar(max) --CSV
	, @WelcomePageName varchar(100)
	, @WelcomePageImagePath varchar(255)
	, @StartDate varchar(20)
	, @EndDate varchar(20)
	
	--Output Variable 
	, @InvalidRetailLocations varchar(5000) output
	, @AdExists bit output --This flag is set when the user tries to set the end date as null when thwere are few ads whose start date is greater than that of the current one.
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 --Initialise to empty string
			 SET @InvalidRetailLocations = NULL	
			 SET @AdExists = 0 --Initialise to not exists	
			 DECLARE @RetailLocationID INT
			 SELECT @RetailLocationID=RetailLocationID   FROM RetailLocationSplashAd WHERE AdvertisementSplashID =@WelcomePageID
			 
			  DECLARE @ADVertiseidID INT
			  SELECT @ADVertiseidID=AdvertisementSplashID 
			  FROM AdvertisementSplash where AdvertisementSplashID in (select AdvertisementSplashID from RetailLocationSplashAd where RetailLocationID =@RetailLocationID AND AdvertisementSplashID !=@WelcomePageID)
			  
			 
			 
			 
			 
			 CREATE TABLE #Temp(RetailLocationID INT)
			 
			 IF @EndDate IS NULL
			 BEGIN
				INSERT INTO #Temp(RetailLocationID)				
				SELECT RLSA.RetailLocationID
				FROM AdvertisementSplash ASP
				INNER JOIN RetailLocationSplashAd RLSA ON ASP.AdvertisementSplashID = RLSA.AdvertisementSplashID
				INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLSA.RetailLocationID
				WHERE (@StartDate <= (SELECT MAX(StartDate)  FROM AdvertisementSplash WHERE AdvertisementSplashID =@ADVertiseidID ))
				       OR (@StartDate = (SELECT MAX(EndDate)  FROM AdvertisementSplash WHERE AdvertisementSplashID =@ADVertiseidID ))
				
				     -- OR (@StartDate <= (SELECT MAX(EndDate)  FROM AdvertisementSplash WHERE RetailID  = @RetailID AND RL.Param = RLSA.RetailLocationID AND AdvertisementSplashID !=@WelcomePageID ))
				      OR (EndDate IS NULL AND ((@StartDate >= StartDate OR @StartDate < StartDate)OR (@StartDate = StartDate AND EndDate IS not Null)))
				         
			   AND RLSA.AdvertisementSplashID <> @WelcomePageID
			
			
			 END
			 
			 IF @EndDate IS NOT NULL
			 BEGIN
				 --Filter all the locations that already has a Welcome Page in the given date range.
				 INSERT INTO #Temp(RetailLocationID)
				 SELECT RL.Param
				 FROM AdvertisementSplash A
				 INNER JOIN RetailLocationSplashAd RLSA ON A.AdvertisementSplashID = RLSA.AdvertisementSplashID
				 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLSA.RetailLocationID
				 WHERE (@StartDate BETWEEN A.StartDate AND ISNULL(A.EndDate, DATEADD(DD, 1, A.StartDate))
		 				OR
		 				(@EndDate BETWEEN A.StartDate AND ISNULL(A.EndDate, DATEADD(DD, 1, A.StartDate))
		 				 AND (@EndDate >A.StartDate OR @EndDate = A.StartDate))
		 			    OR
		 			    (A.EndDate IS NULL AND @EndDate>A.StartDate)
		 				
		 				OR
		 				(@StartDate <A.StartDate AND @StartDate <A.EndDate AND @EndDate >A.StartDate AND @EndDate >A.EndDate ))
				 AND RLSA.AdvertisementSplashID <> @WelcomePageID
			 END
			 
			 SELECT @InvalidRetailLocations = COALESCE(@InvalidRetailLocations+',','') +  CAST(RetailLocationID AS VARCHAR(10))
			 FROM #Temp
			 GROUP BY RetailLocationID
			 
			 --Allow the user to create the welcome page only if no location has a Welcome page in the given date range.
			 IF (@InvalidRetailLocations IS NULL OR @InvalidRetailLocations LIKE '')
			 BEGIN
				UPDATE AdvertisementSplash  SET SplashAdName = @WelcomePageName
										      , SplashAdImagePath = @WelcomePageImagePath
										      , StartDate = @StartDate
										      , EndDate = @EndDate
										      , WebsiteSourceFlag = 1
										      , DateModified = GETDATE()
				WHERE AdvertisementSplashID = @WelcomePageID
				
				--Delete the existing associations with the retail locations.   
				DELETE FROM RetailLocationSplashAd WHERE AdvertisementSplashID = @WelcomePageID	 
				  				
				--Associate the Retail Locations to the Welcome Page.		
				INSERT INTO RetailLocationSplashAd(RetailLocationID
												 , AdvertisementSplashID
												 , DateCreated)	
										 SELECT [Param]
										      , @WelcomePageID
										      , GETDATE()											   
										 FROM dbo.fn_SplitParam(@RetailLocationIds, ',')	      
							
				END		
										
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerWelcomePageUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
