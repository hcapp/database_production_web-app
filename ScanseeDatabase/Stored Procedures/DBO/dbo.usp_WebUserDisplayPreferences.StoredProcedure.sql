USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUserDisplayPreferences]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebUserDisplayPreferences]
Purpose					: To Display the Shopper Preferences for the Confirmation.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th December 2011				Pavan Sharma K		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUserDisplayPreferences]

--Input Variables
    @UserID int 

--Output variables
, @Status int OUTPUT

AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		Select FirstName
		      ,Lastname
		      ,Address1
		      ,Address2
		      ,State
		      ,City
		      ,PostalCode
		      ,MobilePhone
		      ,CollegeName
		      ,EmailAlert
		      ,CellPhoneAlert
         into #Temp1
         From		      
		 (Select DISTINCT U.FirstName
		      , U.Lastname
		      , U.Address1
		      , U.Address2
		      , U.State
		      , U.City
		      , U.PostalCode
		      , U.MobilePhone
		      , UP.CollegeName
		      , ISNULL(UP.EmailAlert,0) EmailAlert
		      , ISNULL(UP.CellPhoneAlert,0)	CellPhoneAlert	     
		 From Users	U
		 LEFT JOIN UserPreference UP ON U.UserID = UP.UserID		
		 WHERE U.UserID = @UserID) AboutMe
		 
		 Select UniversityID
			  , UniversityName	     
			  , State
		 Into #Temp3
		 From
		 (
			Select Distinct Uni.UniversityID	
						  , Uni.UniversityName
						  , Uni.State	          
		    From Users	U
		      LEFT OUTER JOIN UserUniversity UU on U.UserID =  UU.UserID
			  INNER JOIN University Uni ON Uni.UniversityID = UU.UniversityID			 
			  AND U.UserID = @UserID
		 )University
		 
		 Select NonProfitPartnerName 
		 Into #Temp4
		 From 
		 (		 
		   Select Distinct NonProfitPartnerName 
		   From Users	U
		   LEFT OUTER JOIN UserNonProfitPartner UN on U.UserID = UN.UserID
		   INNER JOIN NonProfitPartner NP ON NP.NonProfitPartnerID = UN.NonProfitPartnerID
		   AND U.UserID = @UserID
		 )NonprofitPartner
		
		--Display from the User Preferences Table.
		SELECT  FirstName
			  ,	Lastname
		      , Address1
		      , Address2
		      , State
		      , City
		      , PostalCode
		      , MobilePhone
		      , CollegeName
		      , EmailAlert
		      , CellPhoneAlert	
		FROM #Temp1
		
		 SELECT  Row_Num=ROW_NUMBER() over(order by C.categoryid)
			,C.CategoryID categoryID
		    ,C.ParentCategoryID  parentCategoryID
			, C.ParentCategoryName  parentCategoryName
			, C.SubCategoryID  subCategoryID
			, C.SubCategoryName  subCategoryName
			, displayed = CASE WHEN UC.CategoryID IS NOT NULL THEN 1 ELSE 0 END
		FROM Category C
		LEFT JOIN UserCategory UC ON UC.CategoryID = C.CategoryID AND UC.UserID =@UserID 
		--LEFT JOIN UserPreference UP ON UP.UserID =UC.UserID 		
		order by ParentCategoryName, subCategoryName
		
		--Display the University information.
		SELECT UniversityID
			 , UniversityName
			 , State
		FROM #Temp3
		
		
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebShopperDisplayPreferences.'		
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
