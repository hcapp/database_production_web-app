USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_CJProductsInOtherCategoryListDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CJProductsInOtherCategoryListDisplay
Purpose					: Display List of products belongs to Other Category
Example					: usp_CJProductsInOtherCategoryListDisplay

History
Version		Date			Author					Change Description
--------------------------------------------------------------- 
1.0			7th Oct 2013	Dhananjaya TR   		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_CJProductsInOtherCategoryListDisplay]
(
	 	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION	
			
			--Select Commission Junction manufacturerID
			DECLARE @ManufID Int
			SELECT @ManufID= ManufacturerID
			FROM Manufacturer WHERE Manufname LIKE 'Scansee Commission Junction'
			
			--DECLARE @maxRow Int
			
			--Select @maxRow=MAX(Rownumber) from CJListProductsInOterCategory 
			
			-- Insert new products.
			
			SELECT  DISTINCT Top 100000
				  P.ProductID
				  ,P.ProductName 
				  ,P.ScanCode 
				  ,P.ProductShortDescription 
				  ,P.ProductLongDescription 			  
				  ,C.ParentCategoryName 				  
			INTO #TEMP	  
			FROM Category C
			INNER JOIN ProductCategory PC ON C.CategoryID =PC.CategoryID AND C.ParentCategoryName = 'Other'
			INNER JOIN Product  P ON P.ProductID =PC.ProductID AND P.ManufacturerID=@ManufID 
			--INNER JOIN RetailLocationProduct RLP ON RLP.ProductID =P.ProductID 
			--INNER JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID 
			--INNER JOIN Retailer R ON R.RetailID =RL.RetailID  
			LEFT JOIN CJListProductsInOterCategory CJ ON P.ProductiD=CJ.Productid
			WHERE CJ.Productid IS NULL 	
			
			
			INSERT INTO [CJListProductsInOterCategory](RetailID
										  ,RetailLocationID
										  ,RetailName
										  ,Address1
										  ,ProductID
										  ,ProductName
										  ,ScanCode
										  ,ProductShortDescription
										  ,ProductLongDescription
										  ,BuyURL
										  ,ParentCategoryName
										  ,ProductSent)
			SELECT DISTINCT TOP 50000 R.RetailID
				  ,RL.RetailLocationID
				  ,R.RetailName
				  ,RL.Address1 
				  ,P.ProductID
				  ,P.ProductName 
				  ,P.ScanCode 
				  ,P.ProductShortDescription 
				  ,P.ProductLongDescription 
				  ,RLP.BuyURL 
				  ,P.ParentCategoryName 
				  ,0
			FROM #TEMP P
			LEFT JOIN RetailLocationProduct RLP ON RLP.ProductID =P.ProductID 
			LEFT JOIN RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID 
			LEFT JOIN Retailer R ON R.RetailID =RL.RetailID 
			
            --Display inserted new products.
			SELECT Top 50000 RetailID retailId
				  ,RetailLocationID retLocId
				  ,RetailName retName
				  ,Address1 address
				  ,ProductID prodId
				  ,ProductName prodName
				  ,ScanCode scanCode
				  ,ProductShortDescription prodShortDesc
				  ,ProductLongDescription prodLongDesc
				  ,BuyURL buyURL
				  ,ParentCategoryName parentCatName				 								  
			FROM [CJListProductsInOterCategory]
			WHERE ProductSent =0 
			Order BY ProductName
		            	
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CJProductsInOtherCategoryListDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
