USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterValueList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterValueList]
Purpose                  : To display List of Filter Values.
Example                  : [usp_WebAdminFilterValueList]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterValueList]
(

      --Input Input Parameter(s)--       
        @UserID INT
	  , @FilterID INT
      
      
      --Output Variable--	 
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
            IF @FilterID IS NULL
			BEGIN
				 -- To display List of Filter Values.
				 SELECT DISTINCT AdminFilterValueID fValueId
								,FilterValueName fValueName
				 FROM AdminFilterValue
				 WHERE Associated =0
			 END

			 ELSE
			 BEGIN
				
				SELECT DISTINCT AF.AdminFilterValueID fValueId
				               ,AF.FilterValueName fValueName
				FROM AdminFilterValueAssociation AFV 
				INNER JOIN AdminFilterValue AF ON AFV.AdminFilterValueID =AF.AdminFilterValueID 
				WHERE AFV.AdminFilterID =@FilterID 


			 END



      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterValueList.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;



GO
