USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserPreferencesDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserPreferencesDisplay
Purpose					: To fetch User prefernces info.
Example					: usp_UserPreferencesDisplay 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserPreferencesDisplay]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		SELECT UserPreferenceID 
			,LocaleID
			,UPR.UserID
			,LocaleRadius
			,ScannerSilent
			,DisplayCoupons
			,DisplayRebates
			,DisplayLoyaltyRewards
			,SavingsActivated
			,SleepStatus
		FROM UserPreference UPR 
		--FULL OUTER JOIN  UserPayInfo UPA ON UPA.UserID = UPR.UserID 
		WHERE UPR.UserID = @UserID 
		
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserPreferencesDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
