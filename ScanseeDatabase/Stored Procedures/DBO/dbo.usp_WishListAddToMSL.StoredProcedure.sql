USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListAddToMSL]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListAddToMSL
Purpose					: To add product to Master List from Wish List.
Example					: usp_WishListAddToMSL 2, '63'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			28th June 2011	SPAN Infotech India	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListAddToMSL]
(
	 @UserID int
	, @UserProductID varchar(max)
	, @MasterListAddDate datetime
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To Add the products to Master Shopping List
			UPDATE UserProduct
			SET MasterListItem = 1 
				,MasterListAddDate = @MasterListAddDate 				
			FROM UserProduct UP
				INNER JOIN fn_SplitParam(@UserProductID, ',') U ON U.Param = UP.UserProductID 
			WHERE UP.UserID = @UserID
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListAddToMSL.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
