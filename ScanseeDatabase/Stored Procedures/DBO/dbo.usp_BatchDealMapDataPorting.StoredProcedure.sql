USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchDealMapDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchDealMapDataPorting
Purpose					: To move data from Stage Deal Map table to Production ProductHotDeal Table
Example					: usp_BatchDealMapDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th Aug 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchDealMapDataPorting]
(
	
	--Output Variable 
	@Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--DECLARE @APIPartnerID int
			--SELECT @APIPartnerID = APIPartnerID
			--FROM APIPartner 
			--WHERE APIPartnerName = 'DealMap'
			--INSERT INTO [ProductHotDealHistory]
			--	([ProductHotDealID]
			--	,[ProductID]
			--	,[RetailID]
			--	,[APIPartnerID]
			--	,[HotDealProvider]
			--	,[Price]
			--	,[HotDealDiscountType]
			--	,[HotDealDiscountAmount]
			--	,[HotDealDiscountPct]
			--	,[SalePrice]
			--	,[HotDealName]
			--	,[HotDealShortDescription]
			--	,[HotDeaLonglDescription]
			--	,[HotDealImagePath]
			--	,[HotDealTermsConditions]
			--	,[HotDealURL]
			--	,[Expired]
			--	,[HotDealStartDate]
			--	,[HotDealEndDate]
			--	,[APIDeleteFlag]
			--	,[SourceID]
			--	,[Category]
			--	,[CreatedDate]
			--	,[DateModified]
			--	,[CategoryID])
			--SELECT [ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,1
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--		,[DateModified]
			--		,[CategoryID]
			--FROM
			--	(
			--		MERGE ProductHotDeal AS T
			--		USING APIDealMapData AS S
			--		ON (T.SourceID = S.ID) 
			--		WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
			--			THEN INSERT([SourceID]
			--							 ,[APIPartnerID]
			--							 ,[HotDealProvider]
			--							 ,[Price]
			--							 ,[SalePrice]
			--							 ,[HotDealName]
			--							 ,[HotDealShortDescription]
			--							 ,[HotDealImagePath]
			--							 ,[HotDealURL]
			--							 ,[HotDealStartDate]
			--							 ,[HotDealEndDate]
			--							 ,[Category]
			--							 ,[CreatedDate]) 
			--					 VALUES(S.[ID]
			--							,@APIPartnerID
			--							,S.[DealSource]
			--							,S.[FaceValue]
			--							,S.[DiscountedValue]
			--							,S.[Title]
			--							,S.[Description]
			--							,S.[ImageUrl]
			--							,S.[MoreInfoLink]
			--							,S.[EffectiveTime]
			--							,S.[ExpirationTime]
			--							,S.[Category]
			--							,S.[CreatedDate])
			--		WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID
			--			THEN UPDATE SET T.[HotDealProvider] = S.[DealSource]
			--							 ,T.[Price] = S.[FaceValue]
			--							 ,T.[SalePrice] = S.[DiscountedValue]
			--							 ,T.[HotDealName] = S.[Title]
			--							 ,T.[HotDealShortDescription] = S.[Description]
			--							 ,T.[HotDealImagePath] = S.[ImageUrl]
			--							 ,T.[HotDealURL] = S.[MoreInfoLink]
			--							 ,T.[HotDealStartDate] = S.[EffectiveTime]
			--							 ,T.[HotDealEndDate] = S.[ExpirationTime]
			--							 ,T.[Category] = S.[Category]
			--							 ,T.[DateModified] = S.[CreatedDate]
			--		WHEN NOT MATCHED BY SOURCE AND T.[APIPartnerID] = @APIPartnerID
			--			THEN DELETE 
			--		OUTPUT $action, Deleted.*
			--	) AS HotDeal
			--	(
			--		 [Action]
			--		,[ProductHotDealID]
			--		,[ProductID]
			--		,[RetailID]
			--		,[APIPartnerID]
			--		,[HotDealProvider]
			--		,[Price]
			--		,[HotDealDiscountType]
			--		,[HotDealDiscountAmount]
			--		,[HotDealDiscountPct]
			--		,[SalePrice]
			--		,[HotDealName]
			--		,[HotDealShortDescription]
			--		,[HotDeaLonglDescription]
			--		,[HotDealImagePath]
			--		,[HotDealTermsConditions]
			--		,[HotDealURL]
			--		,[Expired]
			--		,[HotDealStartDate]
			--		,[HotDealEndDate]
			--		,[SourceID]
			--		,[Category]
			--		,[CreatedDate]
			--		,[DateModified]
			--		,[CategoryID]
			--	)
			--WHERE [Action] = 'DELETE'
			
			
			DECLARE @APIPartnerID int
			SELECT @APIPartnerID = APIPartnerID
			FROM APIPartner 
			WHERE APIPartnerName = 'DealMap'


			DECLARE @Deals TABLE([ProductHotDealID] int								
								,[RetailID] int
								,[APIPartnerID] int
								,[HotDealProvider] varchar(100)
								,[Price] money
								,[HotDealDiscountType] varchar(20)
								,[HotDealDiscountAmount] money
								,[HotDealDiscountPct] float
								,[SalePrice] money
								,[HotDealName] varchar(300)
								,[HotDealShortDescription] varchar(1000)
								,[HotDeaLonglDescription] varchar(3000)
								,[HotDealImagePath] varchar(1000)
								,[HotDealTermsConditions] varchar(1000)
								,[HotDealURL] varchar(1000)
								,[Expired] bit
								,[HotDealStartDate] datetime
								,[HotDealEndDate] datetime
								,[APIDeleteFlag] bit
								,[SourceID] varchar(100)
								,[Category] varchar(100)
								,[CreatedDate] datetime
								,[DateModified] datetime
								,[CategoryID] int)

			INSERT INTO @Deals
					([ProductHotDealID]					
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID])
			SELECT [ProductHotDealID]				
					,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,1
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM
				(
					MERGE ProductHotDeal AS T
					USING APIDealMapData AS S
					ON (T.SourceID = S.ID) 
					WHEN NOT MATCHED BY TARGET --AND T.[APIPartnerID] = @APIPartnerID  
						THEN INSERT([SourceID]
										 ,[APIPartnerID]
										 ,[HotDealProvider]
										 ,[Price]
										 ,[SalePrice]
										 ,[HotDealName]
										 ,[HotDealShortDescription]
										 ,[HotDealImagePath]
										 ,[HotDealURL]
										 ,[HotDealStartDate]
										 ,[HotDealEndDate]
										 ,[Category]
										 ,[CreatedDate]) 
								 VALUES(S.[ID]
										,@APIPartnerID
										,S.[DealSource]
										,S.[FaceValue]
										,S.[DiscountedValue]
										,S.[Title]
										,S.[Description]
										,S.[ImageUrl]
										,S.[MoreInfoLink]
										,S.[EffectiveTime]
										,S.[ExpirationTime]
										,S.[Category]
										,S.[CreatedDate])
					WHEN MATCHED AND T.[APIPartnerID] = @APIPartnerID
						THEN UPDATE SET T.[HotDealProvider] = S.[DealSource]
										 ,T.[Price] = S.[FaceValue]
										 ,T.[SalePrice] = S.[DiscountedValue]
										 ,T.[HotDealName] = S.[Title]
										 ,T.[HotDealShortDescription] = S.[Description]
										 ,T.[HotDealImagePath] = S.[ImageUrl]
										 ,T.[HotDealURL] = S.[MoreInfoLink]
										 ,T.[HotDealStartDate] = S.[EffectiveTime]
										 ,T.[HotDealEndDate] = S.[ExpirationTime]
										 ,T.[Category] = S.[Category]
										 ,T.[DateModified] = S.[CreatedDate]
					WHEN NOT MATCHED BY SOURCE AND T.[APIPartnerID] = @APIPartnerID
						THEN DELETE 
					OUTPUT $action, Deleted.*
				) AS HotDeal
				(
					 [Action]
					,[ProductHotDealID]
				    ,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
					, ManufacturerID
					, HotDealTimeZoneID
					, WebsiteSourceFlag
				)
			WHERE [Action] = 'DELETE'
			
			INSERT INTO ProductHotDealHistory
					([ProductHotDealID]
				 	,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,[APIDeleteFlag]
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID])
			SELECT [ProductHotDealID]
				 	,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,1
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM @Deals
			SELECT [ProductHotDealID]
				 	,[RetailID]
					,[APIPartnerID]
					,[HotDealProvider]
					,[Price]
					,[HotDealDiscountType]
					,[HotDealDiscountAmount]
					,[HotDealDiscountPct]
					,[SalePrice]
					,[HotDealName]
					,[HotDealShortDescription]
					,[HotDeaLonglDescription]
					,[HotDealImagePath]
					,[HotDealTermsConditions]
					,[HotDealURL]
					,[Expired]
					,[HotDealStartDate]
					,[HotDealEndDate]
					,1
					,[SourceID]
					,[Category]
					,[CreatedDate]
					,[DateModified]
					,[CategoryID]
			FROM @Deals
			
			--To update Scansee categoryID in ProductHotDeal Table
			UPDATE ProductHotDeal 
			SET CategoryID = C.CategoryID 
			FROM APIDealMapData DM
			INNER JOIN ProductHotDeal HD ON HD.SourceID = DM.ID 
			INNER JOIN DealMapCategoryXRef DC ON DC.DealMapCategory = HD.Category 
			INNER JOIN Category C ON C.ParentCategoryID = DC.ScanSeeParentCategoryID  
									AND C.SubCategoryID = DC.ScanSeeSubCategoryID 
									
			--To delete hot deals from hot deal product table when the deals are moved from history
			
			DELETE from HotDealProduct
			from HotDealProduct h
			inner join @Deals d on h.ProductHotDealID=d.ProductHotDealID
			
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchDealMapDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
