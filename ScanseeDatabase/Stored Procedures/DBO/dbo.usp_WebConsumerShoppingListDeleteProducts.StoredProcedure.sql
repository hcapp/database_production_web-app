USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerShoppingListDeleteProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerShoppingListDeleteProducts
Purpose					: To delete product from shopping list 
Example					: usp_WebConsumerShoppingListDeleteProducts 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th July 2013	Dhananjaya TR	Initail Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerShoppingListDeleteProducts]
(
	  @UserID int
	, @UserProductID varchar(max)
	--, @MasterListRemoveDate datetime
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE UserProduct
			SET TodayListtItem = 0	
			   ,ShopCartItem =0			
			  , TodayListRemoveDate = GETDATE()				
			FROM UserProduct UP
			INNER JOIN fn_SplitParam (@UserProductID, ',') P ON P.Param  = UP.UserProductID  
			WHERE UserID = @UserID 	
						
			--After swipe delete moving the product from wish list to History

					INSERT INTO [UserProductHistory]
							   ([UserProductID]
							   ,[UserRetailPreferenceID]
							   ,[UserID]
							   ,[ProductID]
							   ,[MasterListItem]
							   ,[WishListItem]
							   ,[TodayListtItem]
							   ,[ShopCartItem]
							   ,[MasterListAddDate]
							   ,[WishListAddDate]
							   ,[TodayListAddDate]
							   ,[ShopCartAddDate]
							   ,[MasterListRemoveDate]
							   ,[TodayListRemoveDate]
							   ,[ShopCartRemoveDate])
							   
					SELECT  UserProductID
						   ,UserRetailPreferenceID
						   ,UserID
						   ,ProductID
						   ,MasterListItem
						   ,[WishListItem]
						   ,1
						   ,[ShopCartItem]
						   ,[MasterListAddDate]
						   ,[WishListAddDate]
						   ,[TodayListAddDate]
						   ,[ShopCartAddDate]
						   ,[MasterListRemoveDate]
						   ,[TodayListRemoveDate]
						   ,[ShopCartRemoveDate]
					FROM UserProduct UP
					INNER JOIN fn_SplitParam(@userproductid,',') P ON P.Param =UP.UserProductID 
					WHERE UserID=@UserID
			
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerShoppingListDeleteProducts.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
