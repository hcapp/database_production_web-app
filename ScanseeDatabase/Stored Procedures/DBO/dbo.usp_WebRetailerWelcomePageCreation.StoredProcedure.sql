USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerWelcomePageCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerWelcomePageCreation
Purpose					: To create a Welcome page.
Example					: usp_WebRetailerWelcomePageCreation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerWelcomePageCreation]
(
	--Input Variables
	  @RetailID int
	, @RetailLocationIds varchar(max) --CSV
	, @WelcomePageName varchar(100)
	, @WelcomePageImagePath varchar(255)
	, @StartDate varchar(20)
	, @EndDate varchar(20)
	
	
	--Output Variable 
	, @InvalidRetailLocations varchar(5000) output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 --Initialise to empty string
			 SET @InvalidRetailLocations = NULL
			 
			 DECLARE @WelcomePageID INT
			 
			 --DECLARE @RetailLocationID INT
			 --SELECT @RetailLocationID=RetailLocationID   FROM RetailLocationSplashAd WHERE AdvertisementSplashID =@WelcomePageID
			 
			 -- DECLARE @ADVertiseidID INT
			 -- SELECT @ADVertiseidID=AdvertisementSplashID 
			 -- FROM AdvertisementSplash where AdvertisementSplashID in (select AdvertisementSplashID from RetailLocationSplashAd where RetailLocationID =@RetailLocationID)
			  
			 
			 
			 CREATE TABLE #Temp(RetailLocationID INT)
			 
			 --Filter all the locations that already has a Welcome Page in the given date range.
			 SET @InvalidRetailLocations = NULL
			 
			 
			 --Filter all the locations that already has a Welcome Page in the given date range.
			 IF @EndDate IS NOT NULL
			 BEGIN
			 SELECT @InvalidRetailLocations = COALESCE(@InvalidRetailLocations + ',','') + RL.Param
			 FROM AdvertisementSplash A
			 INNER JOIN RetailLocationSplashAd RLSA ON A.AdvertisementSplashID = RLSA.AdvertisementSplashID
			 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLSA.RetailLocationID
			 WHERE (@StartDate BETWEEN A.StartDate AND ISNULL(A.EndDate, DATEADD(DD, 1, A.StartDate))
			 		OR
			 		ISNULL(@EndDate, DATEADD(DD, 1, A.StartDate)) BETWEEN A.StartDate AND ISNULL(A.EndDate, DATEADD(DD, 1, @EndDate)))
			 END
			 
			 IF @EndDate IS NULL
			 BEGIN
			 
			  SELECT @InvalidRetailLocations = COALESCE(@InvalidRetailLocations + ',','') + RL.Param
			 FROM AdvertisementSplash A
			 INNER JOIN RetailLocationSplashAd RLSA ON A.AdvertisementSplashID = RLSA.AdvertisementSplashID
			 INNER JOIN dbo.fn_SplitParam(@RetailLocationIds, ',') RL ON RL.Param = RLSA.RetailLocationID
			 --WHERE @StartDate BETWEEN A.StartDate AND ISNULL(A.EndDate, DATEADD(DD, 1, @StartDate ))
			 where (EndDate IS NULL AND ((@StartDate >= StartDate OR @StartDate < StartDate)OR (@StartDate = StartDate AND EndDate IS not Null)))
			 	OR (@StartDate BETWEEN A.StartDate AND A.EndDate AND EndDate IS not Null)	
			 END
			 
			 
			 
			 --Allow the user to create the welcome page only if no location has a Welcome page in the given date range.
			 IF (@InvalidRetailLocations IS NULL OR @InvalidRetailLocations LIKE '')
			 BEGIN
				INSERT INTO AdvertisementSplash  (SplashAdName
												, SplashAdImagePath
												, StartDate
												, EndDate
												, WebsiteSourceFlag
												, DateCreated
												, RetailID )
												
										VALUES( @WelcomePageName
										      , @WelcomePageImagePath
										      , @StartDate
										      , @EndDate
										      , 1
										      , GETDATE()
										      ,@RetailID)
										      
						--Capture the inserted identity value
						SET @WelcomePageID = SCOPE_IDENTITY()
				
				--Associate the Retail Locations to the Welcome Page.		
				INSERT INTO RetailLocationSplashAd(RetailLocationID
												 , AdvertisementSplashID
												 , DateCreated)	
										 SELECT [Param]
										      , @WelcomePageID
										      , GETDATE()											   
										 FROM dbo.fn_SplitParam(@RetailLocationIds, ',')	      
							
				END		
										
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerWelcomePageCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
