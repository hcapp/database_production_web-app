USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerGiveAwayPageUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebGiveAwayPageUpdation1
Purpose					: To Update a Give Away Page 
Example					: usp_WebGiveAwayPageUpdation1

History
Version		Date						Author				Change Description
------------------------------------------------------------------------------- 
1.0			29th April 2013				SPAN        		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerGiveAwayPageUpdation]
(

	--Input Parameter(s)--
	 @RetailID int	
   , @PageTitle varchar(255)
   , @StartDate varchar(20)
   , @EndDate varchar(20)
   , @Image varchar(1000)
   , @ShortDescription varchar(255)
   , @LongDescription varchar(1000)
   , @Rules varchar(1000)
   , @TermsandConditions varchar(2000)
   , @Quantity int
   , @QRRetailerCustomPageID int
   	
	--Output Variable--	  	 
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @QRTypeID int
			SELECT @QRTypeID=QRTypeID FROM QRTypes WHERE QRTypeName = 'Give Away Page'
		 
			--Create a record in QRRetailerCustomPage
			 Update QRRetailerCustomPage
			      SET QRTypeID = @QRTypeID
					, RetailID = @RetailID
					, Pagetitle = @PageTitle
					, StartDate = @StartDate
					, EndDate = @EndDate
					, [Image] = @Image
					, ShortDescription = @ShortDescription
					, LongDescription = @LongDescription
					, DateUpdated = GETDATE() 
					, Rules = @Rules
					, [Terms and Conditions] = @TermsandConditions
					, Quantity = @Quantity
			  WHERE RetailID = @RetailID 
			  AND QRRetailerCustomPageID = @QRRetailerCustomPageID
		
		--Confirmation of Success.		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAddPage.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
