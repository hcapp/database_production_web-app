USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListUnassignedProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListUnassignedProducts
Purpose					: To insert Unassigned Product info
Example					: usp_MasterShoppingListUnassignedProducts , 14, 'UnassignedProductName', 'Desc', '6/20/2011'

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			20th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListUnassignedProducts]
(
	@UserID int
	, @UnassignedProductName varchar(50)
	, @UnassignedProductDescription varchar(255)
	, @Date DATETIME
	, @ScreenFlag char(1) -- If the flag value is 'F' then the call is from Favorite Screen .
						  -- if the flag value is 'L' then the call is from List Screen.
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To check the existance of the product.
			IF EXISTS (SELECT TOP 1 UserProductID FROM UserProduct WHERE UserID = @UserID AND UnassignedProductName = @UnassignedProductName)
			BEGIN
				UPDATE UserProduct 
				SET MasterListItem = CASE WHEN @ScreenFlag = 'F' THEN 1 ELSE MasterListItem END
					, MasterListAddDate = CASE WHEN @ScreenFlag = 'F' THEN @Date ELSE MasterListAddDate END
					, TodayListtItem = CASE WHEN @ScreenFlag = 'L' THEN 1 ELSE TodayListtItem END
					, TodayListAddDate = CASE WHEN @ScreenFlag = 'L' THEN @Date ELSE TodayListAddDate END
				WHERE UserID = @UserID 
					AND UnassignedProductName = @UnassignedProductName  
			END
			ELSE
			BEGIN
			--To Insert Unassigned Products.
			INSERT INTO [UserProduct]
				   ([UserID]
				   ,[ProductID] 
				   ,[MasterListItem]
				   ,[WishListItem]
				   ,[TodayListtItem]
				   ,[ShopCartItem]
				   ,[UnassignedProductName]
				   ,[UnassignedProductDescription]
				   ,[MasterListAddDate]
				   ,[TodayListAddDate])
			 VALUES
				   (@UserID 
				   ,0
				   ,CASE WHEN @ScreenFlag = 'F' THEN 1 ELSE 0 END
				   ,0 
				   ,CASE WHEN @ScreenFlag = 'L' THEN 1 ELSE 0 END 
				   ,0 
				   ,@UnassignedProductName 
				   ,@UnassignedProductDescription
				   ,CASE WHEN @ScreenFlag = 'F' THEN @Date END
				   ,CASE WHEN @ScreenFlag = 'L' THEN @Date END)
			END
			
			--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
		--To return UserProductID
			SELECT UserProductID 
			FROM UserProduct 
			WHERE UserID = @UserID 
				AND UnassignedProductName = @UnassignedProductName
				AND (MasterListAddDate = @Date OR TodayListAddDate=@Date)
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListUnassignedProducts.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
