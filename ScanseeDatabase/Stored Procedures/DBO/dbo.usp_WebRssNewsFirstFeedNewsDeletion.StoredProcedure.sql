USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRssNewsFirstFeedNewsDeletion]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Stored Procedure name	: [usp_WebRssNewsFirstFeedNewsDeletion]
Purpose					: To delete from RssNewsFirstFeedNewsStagingTable.
Example					: [usp_WebRssNewsFirstFeedNewsDeletion]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17 Nov 2015		SPAN	        1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRssNewsFirstFeedNewsDeletion]
(
	
	--Output Variable 
	  @Status bit Output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRAN

				--Deleting 30 days older News for 'spanqa.regionsapp'.

				set  identity_insert RssNewsFirstFeedNewsStagingTable_archive  on

insert  into RssNewsFirstFeedNewsStagingTable_archive(
RssNewsFirstFeedNewsID,
Title,
ImagePath,
ShortDescription,
LongDescription,
Link,
PublishedDate,
NewsType,
DateCreated,
DateModified,
CreatedUserID,
ModifiedUserID,
HcHubCitiID,
Message,
Classification,
Section,
AdCopy,
thumbnail,
author,
subcategory,
PublishedTime,
VideoLink,
SubCategoryURL)

select RssNewsFirstFeedNewsID,
Title,
ImagePath,
ShortDescription,
LongDescription,
Link,
PublishedDate,
NewsType,
DateCreated,
DateModified,
CreatedUserID,
ModifiedUserID,
HcHubCitiID,
Message,
Classification,
Section,
AdCopy,
thumbnail,
author,
subcategory,
PublishedTime,
VideoLink,
SubCategoryURL from  RssNewsFirstFeedNewsStagingTable

set  identity_insert RssNewsFirstFeedNewsStagingTable_archive  off

 TRUNCATE TABLE RssNewsFirstFeedNewsStagingTable
				
		
				--truncate table RssNewsFirstFeedNewsStagingTable

				 --Deleting all news for other HubCities.
				-- DELETE FROM RssNewsFirstFeedNewsStagingTable 
				-- WHERE HcHubCitiID NOT IN (2070 ,2146)

	   
		COMMIT TRAN						   
		SET @Status=0	
		  					   		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebRssNewsFirstFeedNewsDeletion].'		
			--- Execute retrieval of Error info.
			ROLLBACK TRAN
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			SET @Status=1	
		END;
		 
	END CATCH;
END;







GO
