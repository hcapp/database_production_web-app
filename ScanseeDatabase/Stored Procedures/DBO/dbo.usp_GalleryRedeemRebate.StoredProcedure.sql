USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_GalleryRedeemRebate]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_GalleryRedeemRebate
Purpose					: To add Rebate to User gallery
Example					: usp_GalleryRedeemRebate

History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			19th Dec 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_GalleryRedeemRebate]
(
	@UserID int
   ,@RebateID int
   ,@RetailLocationID int
   ,@RedemptionStatusID tinyint
   ,@PurchaseAmount money
   ,@PurchaseDate datetime
   ,@ProductSerialNumber varchar(20)
   ,@ScanImage Varchar(255)
   
   --Output Variable 
    , @UsedFlag int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		IF EXISTS(SELECT 1 FROM UserRebateGallery WHERE RebateID = @RebateID AND UserID=@UserID AND UsedFlag=1)
			BEGIN
				SET @UsedFlag=1
			END
		ELSE
			BEGIN
				SET @UsedFlag=0
				IF EXISTS (SELECT 1 FROM UserRebateGallery WHERE RebateID = @RebateID AND UserID=@UserID AND UsedFlag=0)
					BEGIN
					
						UPDATE UserRebateGallery
						SET UsedFlag=1
						WHERE UserID=@UserID AND RebateID=@RebateID
						
					END
				
				IF NOT EXISTS (SELECT 1 FROM UserRebateGallery WHERE RebateID = @RebateID AND UserID=@UserID)
					BEGIN
					
						INSERT INTO [UserRebateGallery]
						   ([UserID]
						   ,[RebateID]
						   ,[RetailLocationID]
						   ,[RedemptionStatusID]
						   ,[PurchaseAmount]
						   ,[PurchaseDate]
						   ,[ProductSerialNumber]
						   ,[ScanImagePath])
						VALUES
						   (@UserID  
						   ,@RebateID
						   ,@RetailLocationID  
						   ,0 
						   ,@PurchaseAmount  
						   ,@PurchaseDate  
						   ,@ProductSerialNumber  
						   ,@ScanImage)
						   
						UPDATE UserRebateGallery
						SET UsedFlag=1
						WHERE UserID=@UserID AND RebateID=@RebateID
						
					END
				END
			
				--Confirmation of Success.
				SELECT @Status = 0
	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_GalleryRedeemRebate.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
