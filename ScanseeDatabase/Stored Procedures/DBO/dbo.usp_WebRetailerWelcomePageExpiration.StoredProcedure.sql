USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerWelcomePageExpiration]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerWelcomePageExpiration
Purpose					: To Expire a splash Ad.
Example					: usp_WebRetailerWelcomePageExpiration

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Aug 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerWelcomePageExpiration]
(
	--Input Variables
	  @RetailID int
	, @WelcomePageID int
	
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			
			 
			 DECLARE @ADStartDate date
			 DECLARE @ADEndDate date
			 
			 SELECT @ADStartDate = StartDate
				  , @ADEndDate = EndDate
			 FROM AdvertisementSplash
			 WHERE AdvertisementSplashID = @WelcomePageID
			 
			 --Check if the Ad is active and move the record to the history and then update the date to '01/01/1900'
			 IF (CONVERT(DATE,GETDATE()) >= @ADStartDate AND CONVERT(DATE,GETDATE()) <= ISNULL(@ADEndDate, DATEADD(DD,1,GETDATE())))
			 BEGIN
				INSERT INTO AdvertisementSplashHistory(AdvertisementSplashID
													 , SplashAdName
													 , StartDate
													 , EndDate
													 , DateCreated)
											SELECT AdvertisementSplashID
												 , SplashAdName
												 , StartDate
												 , EndDate
												 , GETDATE()
											FROM AdvertisementSplash
											WHERE AdvertisementSplashID = @WelcomePageID
				
				--Update the date to the starting date in SQL Server.
				UPDATE AdvertisementSplash
				SET StartDate  = '01/01/1900'
				  , EndDate = '01/01/1900'
				WHERE AdvertisementSplashID = @WelcomePageID
			 END
			
		   --Confirmation of Success.
			SELECT @Status = 0
		
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerWelcomePageExpiration.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
