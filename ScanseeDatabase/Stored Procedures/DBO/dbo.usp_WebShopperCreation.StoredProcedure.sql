USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebShopperCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebShopperCreation
Purpose					: To Register a New Supplier/Manufacturer.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			21st December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebShopperCreation]
(

	--Input Input Parameter(s)--
	
	--Input variables for Users table.
	
	  @FirstName varchar(20)
	, @LastName varchar(50)
	, @Address1 varchar(50)
	, @Address2 varchar(50)
	, @State char(2)
	, @City varchar(20)
	, @PostalCode varchar(10)
	, @PhoneNumber varchar(10)
	, @ContactEmail varchar(100)
	, @Password VARCHAR(100)
	, @UserName VARCHAR(100)
	--Output Variable--
	  
	, @Status INT OUTPUT
	, @DuplicateFlag bit OUTPUT 
	, @UserID int OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @ManufacturerID INT		
		
		--INSERT INTO USERS TABLE
		IF NOT EXISTS(SELECT 1 FROM Users WHERE UserName = @ContactEmail OR Email = @ContactEmail)  --CHECK FOR DUPLICATION
		BEGIN
			INSERT INTO Users(
								  FirstName
								, Lastname
								, Address1
								, Address2
								, State								
								, City
								, PostalCode
								, Email
								, UserName
								, Password
								, MobilePhone
								, DateCreated
							  )
					 VALUES (
								@FirstName
							  , @LastName
							  , @Address1
							  , @Address2
							  , @State
							  , @City
							  , @PostalCode
							  , @ContactEmail
							  , @ContactEmail
							  , @Password
							  , @PhoneNumber
							  , GETDATE()
							)
				SET @DuplicateFlag = 0							--CONFIRM NO DUPLICATES
				SET @Status = 0	
				SET @UserID = SCOPE_IDENTITY()								--CONFIRM SUCCESS
				
				
				INSERT INTO UserLocation 
					(
						UserID 
						,PostalCode
						,UserLocationDate
					)
				VALUES
					(
						@UserID 
						,@PostalCode
						,GETDATE() 
					)
		END
		
		ELSE
		BEGIN
			SET @Status = 0									--CONFIRM SUCCESS
			SET @DuplicateFlag = 1							--CONFIRM DUPLICATE EXISTANCE
		END
	COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebShopperCreation.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
