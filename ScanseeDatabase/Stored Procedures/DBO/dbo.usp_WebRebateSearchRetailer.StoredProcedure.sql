USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRebateSearchRetailer]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRebateSearchRetailer
Purpose					: To search for Rebates.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			6th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRebateSearchRetailer]
(

	--Input Input Parameter(s)--
	  
	  @RetailerID int
	--, @RetailerLocationID int 	Commented because thers is no RetailLocation selection in the prototype.
    , @RebateName varchar(1000)
    , @LowerLimit int
	
	--Output Variable--	  
     , @NextPageFlag bit output
	 , @RowCount INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
	    DECLARE @UpperLimit int 																					
		DECLARE @MaxCnt int
		
		--To get the Number of records per page.
		DECLARE @ScreenContent INT
		SELECT @ScreenContent = ScreenContent
		FROM AppConfiguration
		WHERE ConfigurationType = 'Website Pagination'
		AND ScreenName = 'All'
		AND Active = 1		
		
		SELECT @UpperLimit = @LowerLimit + @ScreenContent
		
       SELECT  RowNum = ROW_NUMBER() OVER (ORDER BY retailID ASC) 
             , retailID 
		     , rebateID
		     , rebName 
		     , rebAmount 
		     , rebStartDate
		     , rebEndDate
		  
    INTO #Temp
    FROM (
		  SELECT DISTINCT RR.RetailID retailID 
		     , R.RebateID as rebateID
		     , R.RebateName rebName 
		     , R.RebateAmount rebAmount 
		     , CONVERT(VARCHAR(10),R.RebateStartDate,105) + ' ' + CONVERT(VARCHAR(10),R.RebateStartDate,108) rebStartDate
		     , CONVERT(VARCHAR(10),R.RebateEndDate,105) + ' ' + CONVERT(VARCHAR(10),R.RebateEndDate,108) rebEndDate
		        
		 FROM Rebate R
		INNER JOIN RebateRetailer RR ON R.RebateID = RR.RebateID		
		WHERE R.RebateName LIKE (CASE WHEN @RebateName IS NOT NULL THEN '%'+@RebateName+'%' ELSE '%' END) AND RR.RetailID =  @RetailerID) Products
		
	SELECT @MaxCnt = MAX(RowNum) FROM #TEMP
	SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END  --CHECK IF THERE ARE SOME MORE ROWS
	
	SET @RowCount = @MaxCnt
	
	   SELECT 
	           RowNum 
             , retailID 
		     , rebateID
		     , rebName 
		     , rebAmount 
		     , rebStartDate
		     , rebEndDate
		    
	FROM #TEMP	
	WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRebateSearchRetailer.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
	END CATCH	 
	
END;


GO
