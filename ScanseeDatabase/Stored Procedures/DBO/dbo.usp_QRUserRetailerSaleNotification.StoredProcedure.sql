USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_QRUserRetailerSaleNotification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_QRUserRetailerSaleNotification
Purpose					: To Notify the Retailer when the User taps on the notify button. 
Example					: usp_QRUserRetailerSaleNotification

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			17th May 2012	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_QRUserRetailerSaleNotification]
(
	 @UserID int
   , @RetailID int
   , @RetailLocationID int
	 
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			INSERT INTO QRUserRetailerSaleNotification(
														  UserID
														, RetailID
														, RetailLocationID
														, DateCreated														
													  )
											VALUES(
													@UserID
												  , @RetailID
												  , @RetailLocationID
												  , GETDATE()
												  )
		COMMIT TRANSACTION		
		--Confirmation of Success.		
		SET @Status = 0	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_QRUserRetailerSaleNotification.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
			--Confirmation of Failure.
			SET @Status = 1
			ROLLBACK TRANSACTION
		END;
		 
	END CATCH;
END;

GO
