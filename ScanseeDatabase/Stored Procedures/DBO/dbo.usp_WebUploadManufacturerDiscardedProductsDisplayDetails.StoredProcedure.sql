USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUploadManufacturerDiscardedProductsDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebUploadManufacturerDiscardedProductsDisplayDetails]
Purpose					: To Register a New Supplier/Manufacturer.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			28th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUploadManufacturerDiscardedProductsDisplayDetails]
(

	--Input Input Parameter(s)--	 
	
	  @ManufacturerID INT
	, @UserID INT
	, @UploadManufacturerLogID int 
	  
	--Output Variable--
	
	, @ContactEmail VARCHAR(1000) OUTPUT
	, @FirstName VARCHAR(255) OUTPUT
	, @Status INT OUTPUT	
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			SELECT   UploadManufacturerDiscardedProductID
					,UML.UploadManufacturerLogID
					,ProductName
					,ScanCode
					,ScanType
					,CAST(ProductExpirationDate AS DATE) as prodExpirationDate
					,ProductImagePath
					,Category as categoryName
					,ProductShortDescription as shortDescription
					,ProductLongDescription as longDescription
					,ModelNumber as	modelNumber
					,SuggestedRetailPrice as strSuggestedRetailPrice
					,[Weight] as strweight	
					,WeightUnits as weightUnits
					,ManufacturerProdcutUrl as manufProuductURL
					,Audio as audioMediaPath
					,Video as videoMediaPath
					,[Image] as imagePath
					,UM.ManufacturerId
					,UM.UserID
					,DateCreated
					,OtherFiles
					,ReasonForDiscarding as discardReason	
			FROM UploadManufacturerDiscardedProducts UM
			INNER JOIN UploadManufacturerLog UML ON UM.UploadManufacturerLogID = UML.UploadManufacturerLogID
			WHERE UM.ManufacturerId = @ManufacturerID
			AND UM.UserID = @UserID
			AND UML.UploadManufacturerLogID = @UploadManufacturerLogID
			AND UML.EmailNotificationFlag = 0
			
			--To update the Email Sent Flag.
			UPDATE UploadManufacturerLog
			SET EmailNotificationFlag = 1
			WHERE UploadManufacturerLogID = @UploadManufacturerLogID
			
		    --Fetch the ContactEmailID
			SELECT @ContactEmail = C.ContactEmail
			     , @FirstName = C.ContactFirstName
			FROM ManufacturerContact MC
			INNER JOIN Contact C ON C.ContactID = MC.ContactID
			WHERE ManufacturerID = @ManufacturerID	
			
			--Confirmation of Success.			
			SET @Status = 0	
				
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebUploadManufacturerDiscardedProductsDisplayDetails].'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			SET @Status = 1
			ROLLBACK TRANSACTION;
			
		END;
		 
	END CATCH;
END;


GO
