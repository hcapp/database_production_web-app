USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebFetchCouponAssociatedProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebFetchCouponAssociatedProducts
Purpose					: To Display products associated with Coupon.
Example					: usp_WebFetchCouponAssociatedProducts

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			8thJune2012 	Pavan sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebFetchCouponAssociatedProducts]
(
	--Input Variable
	  @CouponID int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		DECLARE @ProductNames VARCHAR(MAX)
		DECLARE @ProductIDs VARCHAR(MAX)
		
		--Get the Products associated with the coupon in comma separated format.
		SELECT   @ProductIDs = COALESCE(@ProductIDs + ',', '') + CAST(P.ProductID AS VARCHAR(50))
			   , @ProductNames  = COALESCE(@ProductNames + ',', '') + CAST(P.ProductName AS VARCHAR(50))
		FROM Coupon C
		INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID 
		INNER JOIN Product P ON P.ProductID = CP.ProductID
		WHERE C.CouponID = @CouponID
		
		--Display the comma separated products.		
		SELECT @ProductIDs ProductIDs
		      ,@ProductNames ProductNames
			  ,CouponID 
		      ,RetailID 
		      ,ManufacturerID 
		      ,APIPartnerID 
		      ,CouponName 
		      ,CouponDiscountType 
		      ,CouponDiscountAmount 
		      ,CouponDiscountPct 
		      ,CouponShortDescription 
		      ,CouponLongDescription 
		      ,CouponImagePath 
		      ,CouponStartDate = CASE WHEN CAST(C.CouponStartDate AS DATE) = '01/01/1900' THEN CAST((SELECT MAX(CouponStartDate) FROM CouponHistory WHERE CouponID = C.CouponID) AS DATE) ELSE CAST(C.CouponStartDate AS DATE) END 
		      ,CouponDisplayExpirationDate 
		      ,CouponExpireDate = CASE WHEN CAST(C.CouponExpireDate AS DATE) = '01/01/1900' THEN CAST((SELECT MAX(CouponExpireDate) FROM CouponHistory WHERE CouponID = C.CouponID) AS DATE) ELSE CAST(C.CouponExpireDate AS DATE) END 
		      ,CouponURL 
		      ,ExternalCoupon 
		      ,NoOfCouponsToIssue 
		      ,CouponTermsAndConditions 
		      ,CouponTimeZoneID 
		      ,WebsiteSourceFlag 
		      ,POSIntegrate 
		      ,ViewableOnWeb 
		      ,CreatedUserID 
		     
		FROM Coupon C
		WHERE CouponID =@CouponID     
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebFetchCouponAssociatedProducts.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
