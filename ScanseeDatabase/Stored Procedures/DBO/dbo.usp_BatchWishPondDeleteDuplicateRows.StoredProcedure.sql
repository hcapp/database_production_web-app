USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchWishPondDeleteDuplicateRows]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchWishPondDeleteDuplicateRows
Purpose					: To delete duplicate rows.
Example					: usp_BatchWishPondDeleteDuplicateRows

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10th Nov 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchWishPondDeleteDuplicateRows]
(
	
	--Output Variable 
	  @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			--To delete duplicate location data
			DELETE FROM APIWishpondLocationData
			WHERE LTRIM(RTRIM(ISNULL(City,''))) <> SUBSTRING(LTRIM(RTRIM(sourceCity)),1,LEN(LTRIM(RTRIM(sourceCity)))-3)
			OR LTRIM(RTRIM(ISNULL(State,''))) <> RIGHT(LTRIM(RTRIM(sourceCity)),2)
			
			--To delete duplicate product deal data
			; WITH APIWishpondLocationData
			AS
			(
			SELECT Ranking  = ROW_NUMBER() 
						OVER(PARTITION BY 
							DealID  
							, ItemID  
							, APIWishpondLocId  
							ORDER BY NEWID()) 
					, DealID  
					, ItemID  
					, APIWishpondLocId 
			FROM APIWishPondProductData 
			)
			DELETE FROM APIWishpondLocationData WHERE Ranking > 1
			
			
			
			
						
			--To get # Rows affected
			SELECT @Result = @@ROWCOUNT

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchWishPondDeleteDuplicateRows.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
