USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_DisplayRetailLocationInfo]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_DisplayRetailLocationInfo
Purpose					: To Display the basic infomration about the Retail Location.
Example					: usp_DisplayRetailLocationInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd May 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_DisplayRetailLocationInfo]
(
	  
	  @UserID int
	, @RetailID int
    , @RetailLocationID int
    , @Latitude float
    , @Longitude float
    , @PostalCode char(10)
    
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		
		
	    DECLARE @RetailerConfig varchar(50)
	    DECLARE @RetailLocationAdvertisement VARCHAR(1000)--WelcomePage/SplashAd/BannerAd(previously)
	    DECLARE @RibbonAdImagePath varchar(1000)--BannerAd
	    DECLARE @RibbonAdURL VARCHAR(1000)--BannerAdURL
	    DECLARE @RetailerPageQRURL VARCHAR(1000)
	    DECLARE @Config VARCHAR(100)	 	
	    
	    --Retrieve the server configuration.
		SELECT @Config = ScreenContent 
		FROM AppConfiguration 
		WHERE ConfigurationType LIKE 'QR Code Configuration'
		AND Active = 1 
		
		--To get server Configuration.
		SELECT @RetailerConfig= ScreenContent  
		FROM AppConfiguration   
		WHERE ConfigurationType='Web Retailer Media Server Configuration'		
		
		--Get the associated Splash Ad/ Welcome Page Details.
		SELECT @RetailLocationAdvertisement =  @RetailerConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.SplashAdImagePath
		FROM AdvertisementSplash RLA
		INNER JOIN RetailLocationSplashAd RL ON RL.RetailLocationID = RL.RetailLocationID
		WHERE RL.RetailLocationID = @RetailLocationID
		AND RLA.RetailID = @RetailID
		AND GETDATE() BETWEEN StartDate AND EndDate
		AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementSplash ASP 
							 INNER JOIN  RetailLocationSplashAd RLS ON ASP.AdvertisementSplashID = RLS.AdvertisementSplashID
							 WHERE RetailLocationID = @RetailLocationID)
							 
		--Get the associated Banner Ad Details.
		SELECT @RibbonAdImagePath = @RetailerConfig + CAST(@RetailID AS VARCHAR(10)) + '/' + RLA.BannerAdImagePath
			 , @RibbonAdURL = RLA.BannerAdURL
		FROM AdvertisementBanner RLA
		INNER JOIN RetailLocationBannerAd RL ON RL.RetailLocationID = RL.RetailLocationID
		WHERE RL.RetailLocationID = @RetailLocationID
		AND RLA.RetailID = @RetailID
		AND GETDATE() BETWEEN StartDate AND EndDate
		AND RLA.StartDate = (SELECT MAX(StartDate) FROM AdvertisementBanner ASP 
							 INNER JOIN  RetailLocationBannerAd RLS ON ASP.AdvertisementBannerID = RLS.AdvertisementBannerID
							 WHERE RetailLocationID = @RetailLocationID)
		
		--Get the associated QRPage URL.
		SELECT @RetailerPageQRURL = CASE WHEN QR.QRRetailerCustomPageID IS NOT NULL THEN 
					@Config  + CAST(QRTypeCode AS VARCHAR(10)) + '.htm?retId=' + CAST(@RetailID AS VARCHAR(10)) + '&retlocId=0&pageId=' + CAST(QR.QRRetailerCustomPageID AS VARCHAR(10)) 
				    ELSE NULL END			
		FROM QRRetailerCustomPage QR
		INNER JOIN QRRetailerCustomPageAssociation QRRCPA ON QR.QRRetailerCustomPageID = QRRCPA.QRRetailerCustomPageID
		INNER JOIN QRTypes Q ON QR.QRTypeID = Q.QRTypeID
		WHERE QR.RetailID = @RetailID
		AND QRRCPA.RetailLocationID = @RetailLocationID
		AND Q.QRTypeName = 'Main Menu Page'
		
		--Find if there is any sale or discounts associated with the Retail Location.
		select distinct Retailid , RetailLocationid
		into #RetailItemsonSale
		from 
		(select b.RetailID, a.RetailLocationID 
		from RetailLocationDeal a 
		inner join RetailLocation b on a.RetailLocationID = b.RetailLocationID 
		inner join RetailLocationProduct c on a.RetailLocationID = c.RetailLocationID
										   and a.ProductID = c.ProductID 
										   AND A.RetailLocationID = @RetailLocationID
										   and GETDATE() between isnull(a.SaleStartDate, GETDATE()-1) and isnull(a.SaleEndDate, GETDATE()+1)
		union 
		select  CR.RetailID, RetailLocationID  as RetaillocationID 
		from Coupon C 
		INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID
		where GETDATE() BETWEEN CouponStartDate AND CouponExpireDate
		AND CR.RetailLocationID = @RetailLocationID
		union 
		select  RR.RetailID, RR.RetailLocationID  
		from Rebate R 
		inner join RebateRetailer RR ON R.RebateID=RR.RebateID
		where RR.RetailLocationID = @RetailLocationID
		AND GETDATE() BETWEEN RebateStartDate AND RebateEndDate 
		union 
		select  c.retailid, a.RetailLocationID 
		from  LoyaltyDeal a
		inner join RetailLocationProduct b on a.RetailLocationID = b.RetailLocationID 
									and a.ProductID = b.ProductID 
		inner join RetailLocation c on a.RetailLocationID = b.RetailLocationID 
		Where GETDATE() BETWEEN LoyaltyDealStartDate AND LoyaltyDealExpireDate
		AND A.RetailLocationID = @RetailLocationID
		union
		select rl.RetailID, rl.RetailLocationID
		from ProductHotDeal p
		inner join ProductHotDealRetailLocation pr on pr.ProductHotDealID = p.ProductHotDealID 
		inner join HotDealProduct hp on hp.ProductHotDealID = p.ProductHotDealID 
		inner join RetailLocation rl on rl.RetailLocationID = pr.RetailLocationID
		WHERE GETDATE() between HotDealStartDate and HotDealEndDate
		AND rl.RetailLocationID = @RetailLocationID 
		union
		select q.RetailID, qa.RetailLocationID
		from QRRetailerCustomPage q
		inner join QRRetailerCustomPageAssociation qa on qa.QRRetailerCustomPageID = q.QRRetailerCustomPageID
		inner join QRTypes qt on qt.QRTypeID = q.QRTypeID and qt.QRTypeName = 'Special Offer Page'
		where qa.RetailLocationID = @RetailLocationID 
		and GETDATE() BETWEEN isnull(q.startdate,'1/1/1900') and isnull(q.enddate,GETDATE()+1)) a			 
		
		--Check if the both Lat, Long and ZipCode is not sent to the procedure.
		IF @Latitude IS NULL AND @Longitude IS NULL AND @PostalCode IS NULL
		BEGIN
			SELECT @Latitude = G.Latitude, @Longitude = G.Longitude
			FROM Users U
			INNER JOIN GeoPosition G ON G.PostalCode = U.PostalCode
			WHERE UserID = @UserID
		END
		
		--Check if the Lat, Long is not sent but ZipCode not sent to the procedure.
		IF @Latitude IS NULL AND @Longitude IS NULL AND @PostalCode IS NOT NULL
		BEGIN
			SELECT @Latitude = Latitude
				 , @Longitude = Longitude
			FROM GeoPosition
			WHERE PostalCode = @PostalCode
		END
				 
		--Display the Basic details of the Retail location.		
		SELECT DISTINCT R.RetailID retailerId
		     , R.RetailName retailerName
		     , RL.RetailLocationID retailLocationID
			 , RL.Address1 retaileraddress1
			 , ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) retLatitude
			 , ISNULL(RL.RetailLocationLongitude, (SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) retLongitude
			 , Distance = ROUND((ACOS((SIN(ISNULL(RL.RetailLocationLatitude,(SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) / 57.2958) * SIN(@Latitude / 57.2958) + COS(ISNULL(RL.RetailLocationLatitude, (SELECT Latitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (ISNULL(RL.RetailLocationLongitude,(SELECT Longitude FROM GeoPosition WHERE PostalCode = RL.PostalCode)) / 57.2958))))*6371) * 0.6214,1,1)   
		     , C.ContactPhone contactPhone
		     , CASE WHEN RL.RetailLocationURL IS NULL THEN R.RetailURL ELSE RL.RetailLocationURL END retailerURL
		     , @RetailLocationAdvertisement bannerAdImagePath
		     , @RibbonAdImagePath ribbonAdImagePath
		     , @RibbonAdURL ribbonAdURL
		     , @RetailerConfig + CAST(@RetailID AS VARCHAR(10))+ '/' + R.RetailerImagePath image
		     , @RetailerPageQRURL retailerPageQRURL 
		     , SaleFlag = CASE WHEN (SELECT COUNT(1) FROM #RetailItemsonSale)>0 THEN 1 ELSE 0 END
		     , AppDownloadLink = (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'App Download Link')			
		FROM  RetailLocation RL 
		INNER JOIN Retailer R ON R.RetailID = RL.RetailID			
		LEFT JOIN RetailContact RC ON RC.RetailLocationID = RL.RetailLocationID
		LEFT JOIN Contact C ON C.ContactID = RC.ContactID		
		WHERE RL.RetailID = @RetailID 
		AND RL.RetailLocationID = @RetailLocationID				 
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure <>.'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
