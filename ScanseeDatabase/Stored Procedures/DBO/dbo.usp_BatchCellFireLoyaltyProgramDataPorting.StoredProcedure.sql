USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchCellFireLoyaltyProgramDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchCellFireLoyaltyProgramDataPorting
Purpose					: To move Loyalty Program from stage to production.
Example					: usp_BatchCellFireLoyaltyProgramDataPorting

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			23rd October 2012   Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchCellFireLoyaltyProgramDataPorting]
(
	
	--Output Variable 
	  @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
				--Update the Loyalty if it was already existing with the same card name(Program Name)
				UPDATE LoyaltyProgram
				SET CardSupportURL = A.CardSupportURL
				  , CardSupportPhone = A.CardSupportPhone
				  , CardSupportMessage = A.CardSupportMessage
				  , WaitTimeInMins = A.WaitTimeInMins
				  , LoyaltyLargeImage = A.LoyaltyLargeImage
				  , LoyaltySmallImage = A.LoyaltySmallImage
				  , APIPartnerID = (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName = 'CellFire')
				  , LoyaltyProgramModifyDate = GETDATE()
				FROM Retailer R 
				INNER JOIN APICellFireMerchants A ON LTRIM(RTRIM(A.MerchantName)) = LTRIM(RTRIM(R.RetailName))
				INNER JOIN LoyaltyProgram LP ON LP.RetailID = R.RetailID AND LTRIM(RTRIM(LP.LoyaltyProgramName)) = LTRIM(RTRIM(A.CardName))
				
				--Create the Loyalty program if not existing.
				INSERT INTO LoyaltyProgram(RetailID
										 , LoyaltyProgramName
										 , LoyaltyProgramActive
										 , LoyaltyProgramAddDate
										 , CardSupportURL
										 , CardSupportPhone
										 , CardSupportMessage
										 , WaitTimeInMins
										 , LoyaltyLargeImage
										 , LoyaltySmallImage
										 , APIPartnerID)
				SELECT R.RetailID
					 , A.CardName
					 , 1
					 , GETDATE()
					 , A.CardSupportURL
					 , A.CardSupportPhone
					 , A.CardSupportMessage
					 , A.WaitTimeInMins
					 , A.LoyaltyLargeImage
					 , A.LoyaltySmallImage
					 , (SELECT APIPartnerID FROM APIPartner WHERE APIPartnerName = 'CellFire')
				FROM Retailer R 
				INNER JOIN APICellFireMerchants A ON LTRIM(RTRIM(A.MerchantName)) = LTRIM(RTRIM(R.RetailName))
				LEFT JOIN LoyaltyProgram LP ON LP.RetailID = R.RetailID AND LTRIM(RTRIM(LP.LoyaltyProgramName)) = LTRIM(RTRIM(A.CardName))	
				WHERE LP.LoyaltyProgramID IS NULL									 
			
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchCellFireLoyaltyProgramDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
