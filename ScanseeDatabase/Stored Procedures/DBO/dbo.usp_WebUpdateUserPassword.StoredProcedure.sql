USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUpdateUserPassword]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : [usp_WebUpdateUserPassword]  
Purpose     :   To Reset the Password and Retrieve the EmailID of the user on which he/she should be notified.
Example     : [usp_WebUpdateUserPassword]  
  
History  
Version  Date		 Author			Change Description   
---------------------------------------------------------------   
1.0   16thMay2012  Pavan Sharma K Initial	Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebUpdateUserPassword]  
(  
    @UserName varchar(100)
  , @Password varchar(100)
 
 --Output Variable   
 , @Status int output
 , @Success bit output
 , @LoginName varchar(100) output 
 , @FirstName varchar(100) output 
 , @ContactEmail varchar(100) output 
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY   
	BEGIN TRAN
		DECLARE @UserID INT
		
		--Check if the user exists in the System.
		IF EXISTS(SELECT 1 FROM Users U INNER JOIN  UserRetailer UR ON U.UserID = UR.UserID INNER JOIN Retailer R ON UR.RetailID = R.RetailID
					WHERE R.RetailerActive = 1 AND  BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName))
		BEGIN			
			SELECT @UserID = UserID
			FROM Users 
			WHERE UserName = @UserName			 
			
			SET @LoginName = @UserName
			--Reset the Password.
			UPDATE Users 
			SET Password = @Password
			  , ResetPassword = 0
			WHERE UserID = @UserID
			
			SET @Success = 1
			
			--Check if the User is a Retailer and retrieve the Contact Email.
			IF EXISTS(SELECT 1 FROM UserRetailer WHERE UserID = @UserID)
			BEGIN
				SELECT @ContactEmail = C.ContactEmail
					 , @FirstName = C.ContactFirstName
				 FROM RetailLocation RL     
				 INNER JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID AND RL.Active = 1
				 INNER JOIN Contact C ON RC.ContactID = C.ContactID
				 INNER JOIN UserRetailer UR ON UR.RetailID = RL.RetailID
				 WHERE UR.UserID = @UserID  				  
				 AND (Headquarters = 1 OR CorporateAndStore = 1)
			END
			
			--Check if the User is a Manufacturer and Retrieve the Contact Email.
			IF EXISTS(SELECT 1 FROM UserManufacturer WHERE UserID = @UserID)
			BEGIN
				SELECT @ContactEmail = C.ContactEmail
					 , @FirstName = C.ContactFirstName
				FROM UserManufacturer UM
				INNER JOIN ManufacturerContact MC ON UM.ManufacturerID = MC.ManufacturerID
				INNER JOIN Contact C ON C.ContactID = MC.ContactID
				WHERE UserID = @UserID
			END
			
			--If the user is a shopper retrieve directly from the Users Table.
			IF NOT EXISTS(SELECT 1 FROM UserRetailer WHERE UserID = @UserID) AND NOT EXISTS (SELECT 1 FROM UserManufacturer WHERE UserID = @UserID)
			BEGIN		
				SELECT @ContactEmail = Email
					 , @FirstName = FirstName
				FROM Users
				WHERE UserID = @UserID
			END			
		END	
		
		--If the user did not exist in the system notify.
		ELSE
		BEGIN
			SET @Success = 0
		END
		
		SET @Status = 0
    COMMIT TRAN
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure [usp_WebUpdateUserPassword].'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   SET @Status = 1  
   ROLLBACK TRAN
  END;  
     
 END CATCH;  
END;




GO
