USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAppListingAboutUsPageCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebAppListingAboutUsPageCreation
Purpose					: To Create Anything Page (About Us) by the Retailer.
Example					: usp_WebAppListingAboutUsPageCreation

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			06th February 2013			SPAN	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAppListingAboutUsPageCreation]
(

	--Input Parameter(s)--
	
	--Common
	 @RetailID int	
   , @RetailLocationID varchar(MAX)
   , @AnythingPageTitle varchar(255)
   , @Image varchar(100)      
   
   --Make your Own
   , @PageDescription varchar(1000)
      	
	--Output Variable--	  	 
	, @PageID int output
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @QRRetailerCustomPageID INT
			DECLARE @ProductMediaTypeID int
			
			--Create the Anything Page & retrieve the page id for the QR code generation.
			INSERT INTO QRRetailerCustomPage(QRTypeID
											,RetailID
											,Pagetitle											
											,Image
											,PageDescription
											,ShortDescription																			
											,DateCreated)
											
				                 SELECT (SELECT QRTypeID FROM QRTypes WHERE QRTypeName LIKE 'Anything Page')
									  , @RetailID
									  , @AnythingPageTitle
									  , @Image
									  , @PageDescription
									  , @PageDescription									
									  , GETDATE()
									  
			SELECT @QRRetailerCustomPageID = SCOPE_IDENTITY()
			SELECT @PageID = @QRRetailerCustomPageID
			
			--If the Retailer Locations are not null.						  
			IF (@RetailLocationID IS NOT NULL)
			BEGIN
				INSERT INTO QRRetailerCustomPageAssociation(QRRetailerCustomPageID
														  , RetailID
														  , RetailLocationID
														  , DateCreated)
													SELECT @QRRetailerCustomPageID
														 , @RetailID
														 , Param
														 , GETDATE()
													FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END									  
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebAppListingAboutUsPageCreation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
