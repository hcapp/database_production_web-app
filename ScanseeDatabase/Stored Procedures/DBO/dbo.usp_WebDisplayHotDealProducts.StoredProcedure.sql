USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayHotDealProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDisplayHotDealProducts
Purpose					: To Display the product(s) associated to the Hot Deal.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			13th January 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayHotDealProducts]
(

	--Input Input Parameter(s)--	   
	  
	  @ProductHotDealID int		 
	
	--Output Variable--  

	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN	
		
	BEGIN TRY
	
	--To get Media Server Configuration.
	 DECLARE @Config varchar(50)  
	 SELECT @Config=ScreenContent  
	 FROM AppConfiguration   
	 WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
		
		SELECT HDP.ProductID
		     , P.ScanCode
		     , P.ProductName
		     , ProductImagePath = CASE WHEN WebsiteSourceFlag = 1 AND ProductImagePath IS NOT NULL THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID) +'/'+  ProductImagePath ELSE ProductImagePath END 
		     , P.ProductShortDescription
		FROM HotDealProduct HDP
		INNER JOIN Product P ON HDP.ProductID = P.ProductID
		WHERE HDP.ProductHotDealID = @ProductHotDealID
		
	END TRY	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDisplayHotDealProducts.'		
		-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
