USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_APIWishPondInputCity]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_APIWishPondInputCity
Purpose					: To Pass city as the input for APIWishPondURL.
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_APIWishPondInputCity]
(
	
	--Output Variable 

	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
			SELECT AL.City
				 , AL.State 
			FROM APIUsageLocation AL 
			INNER JOIN APIPartner A ON A.APIPartnerID = AL.APIPartnerID 
			WHERE A.APIPartnerName = 'WishPond' and AL.Status=1


			----shyam  sdgdfgdfgdfg
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_APIWishPondInputCity.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		
		END;
		 
	END CATCH;
END;

GO
