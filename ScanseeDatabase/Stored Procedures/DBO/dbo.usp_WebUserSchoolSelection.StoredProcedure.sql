USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebUserSchoolSelection]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebUserSchoolSelection
Purpose					: To Capture UserPreferences On College.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebUserSchoolSelection]
(

	--Input Input Parameter(s)--		
	  
	  --Inputs for UserPreferences Table
	  @UserID Int
	, @CollegeName Varchar(255)
	
	--Input For UserUniversity Table
	, @UniversityIDs Varchar(1000) --Comma Separated University IDs
	
	--Input For UserNonProfitPartner Table
	, @NonProfitPartnerIDs Varchar(1000) --Comma Separated NonProfitPartner IDs
	
		 
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		    
		    --Create a record in Userpreferences Table.
		    
		    IF NOT EXISTS(SELECT 1 FROM UserPreference WHERE UserID= @UserID)
		    BEGIN
			INSERT INTO UserPreference (
											UserID
										  , CollegeName									  
										)
							  Values(
										@UserID
									  , @CollegeName 
							         )
           END
           
           ELSE 
           BEGIN
					UPDATE UserPreference SET CollegeName = @CollegeName
					WHERE UserID = @UserID
           END							         
          --Create a record in UserUniversity Table if exists.		
          
          IF @UniversityIDs IS NOT NULL		
          BEGIN
          --If already existing delete and insert.          
          DELETE FROM UserUniversity WHERE UserID = @UserID          			         
			  INSERT INTO UserUniversity (
											UserID
										  , UniversityID
										  , DateCreated
										)
								SELECT @UserID
									 , PARAM
									 , GETDATE()
								FROM fn_SplitParam(@UniversityIDs, ',')
         END 								
						    
        --Create a record in UserNonProfitPartner Table.
        
        --Delete the record if already existing and insert.
        DELETE FROM UserNonProfitPartner WHERE UserID = @UserID
        
        IF @NonProfitPartnerIDs IS NOT NULL
        BEGIN
           INSERT INTO UserNonProfitPartner (
												UserID
											  , NonProfitPartnerID
											  , DateCreated
											)
						    SELECT @UserID
						         , PARAM
						         , GETDATE()
						    FROM fn_SplitParam(@NonProfitPartnerIDs, ',')
		END			       			   
		
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebUserSchoolSelection.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
