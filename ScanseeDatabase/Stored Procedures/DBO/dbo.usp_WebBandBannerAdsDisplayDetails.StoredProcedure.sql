USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebBandBannerAdsDisplayDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebBandBannerAdsDisplayDetails]
Purpose                  : To display the Banner ads created by the given Band.
Example                  : [usp_WebBandBannerAdsDisplayDetails]

History
Version           Date                Author          Change Description
------------------------------------------------------------------------------- 
1.0               04/18/2016        Prakash C   Initial Version (usp_WebRetailerBannerAdsDisplayDetails)                                      
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebBandBannerAdsDisplayDetails]
(

      --Input Input Parameter(s)--  
      
        @AdvertisementBannerID INT
      --Output Variable--
	  
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
      
      --To Fetch the Band Media server Configuaration
      
			DECLARE @Config varchar(255)    
			SELECT @Config=
			ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration' AND Active =1  
		
		
       --Display The Banner page Details
       
				SELECT AB.BandAdvertisementBannerID as retailLocationAdvertisementID
				      ,AB.BannerAdName as advertisementName 
					  ,@Config + CONVERT(VARCHAR(30),AB.BandID) +'/' + BannerAdImagePath as strBannerAdImagePath
					  ,AB.BannerAdURL as ribbonXAdURL
					  ,advertisementDate = CASE WHEN AB.StartDate = '01/01/1900' THEN (SELECT StartDate
																					  FROM BandAdvertisementBannerHistory 
																					  WHERE BandAdvertisementBannerID = @AdvertisementBannerID
																					  AND DateCreated = (SELECT MAX(DateCreated)
																										 FROM BandAdvertisementBannerHistory
																										 WHERE BandAdvertisementBannerID = @AdvertisementBannerID)) ELSE AB.StartDate END
				      ,advertisementEndDate = CASE WHEN AB.EndDate = '01/01/1900' THEN (SELECT EndDate
																					  FROM BandAdvertisementBannerHistory 
																					  WHERE BandAdvertisementBannerID = @AdvertisementBannerID
																					  AND DateCreated = (SELECT MAX(DateCreated)
																										 FROM BandAdvertisementBannerHistory
																										 WHERE BandAdvertisementBannerID = @AdvertisementBannerID)) ELSE AB.EndDate END
				FROM BandAdvertisementBanner AB 
				WHERE AB.BandAdvertisementBannerID = @AdvertisementBannerID                   
                  
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebBandBannerAdsDisplayDetails.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
				   insert  into ScanseeValidationErrors(ErrorCode,ErrorLine,ErrorDescription,ErrorProcedure)
               values(ERROR_NUMBER(),  ERROR_LINE(),ERROR_MESSAGE(),ERROR_PROCEDURE())    
                               
                  
            END;
            
      END CATCH;
END;





GO
