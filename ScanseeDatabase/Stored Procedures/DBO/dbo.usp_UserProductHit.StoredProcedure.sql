USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserProductHit]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserProductHit
Purpose					: To capture User product Hit.
Example					: usp_UserProductHit

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			09th Aug 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserProductHit]
(
	@UserID int    
	, @ProductID int
	, @RetailLocationID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION  
		--To capture User Product Hit info.  
		INSERT INTO [ProductUserHit]  
			([ProductID]  
			,[UserID]  
			,[RetailLocationID]  
			,[UserHitDate])  
		 VALUES  
			(@ProductID   
			,@UserID   
			,@RetailLocationID   
			,GETDATE())
			
		--Confirmation of Success.  
		 SELECT @Status = 0  
		COMMIT TRANSACTION   
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserProductHit.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
