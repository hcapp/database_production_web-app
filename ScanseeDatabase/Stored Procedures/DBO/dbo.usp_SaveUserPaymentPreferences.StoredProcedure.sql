USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveUserPaymentPreferences]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserPreferences
Purpose					: To Save User Preferences.
Example					: usp_UserPreferences

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SaveUserPaymentPreferences]
(
	 @UserID int
	,@PaymentIntervalID int
	,@PaymentTypeID int
	,@PayoutTypeID int
	,@DefaultPayout money
	--,@UseDefaultAddress bit
	--,@PayAddress1 varchar(30)
	--,@PayAddress2 varchar(30)
	--,@PayCity varchar(30)
	--,@PayState char(2)
	--,@PayPostalCode varchar(10)
	,@ProvisionalPayoutActive bit
	,@ProvisionalPayoutStartDate date
	,@ProvisionalPayoutEndDate date
	,@DateModified datetime
	
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
				
			--To check the existance of the user Payment info.
			IF EXISTS (SELECT 1 FROM UserPayInfo WHERE UserID = @UserID)
			--If info exists, modify the info.
			BEGIN
				UPDATE UserPayInfo 
				SET [PaymentIntervalID] = @PaymentIntervalID   
				  ,[PaymentTypeID] =  @PaymentTypeID 
				  ,[DefaultPayout] =  @DefaultPayout 
				  ,[PayoutTypeID] =  @PayoutTypeID 
				  --,[UseDefaultAddress] =  @UseDefaultAddress 
				  --,[PayAddress1] =  @PayAddress1 
				  --,[PayAddress2] =  @PayAddress2 
				  --,[PayCity] =  @PayCity 
				  --,[PayState] =  @PayState 
				  --,[PayPostalCode] = @PayPostalCode 
				  ,[DateModified] = @DateModified 
				  ,[ProvisionalPayoutActive] = @ProvisionalPayoutActive 
				  ,[ProvisionalPayoutStartDate] = @ProvisionalPayoutStartDate 
				  ,[ProvisionalPayoutEndDate] = @ProvisionalPayoutEndDate 
				WHERE UserID = @UserID
			END
			--If info not exists, add the info.
			ELSE
			BEGIN
				INSERT INTO [UserPayInfo]
					   ([UserID]
					   ,[PaymentIntervalID]
					   ,[PaymentTypeID]
					   ,[DefaultPayout]
					   ,[PayoutTypeID]
					   --,[UseDefaultAddress]
					   --,[PayAddress1]
					   --,[PayAddress2]
					   --,[PayCity]
					   --,[PayState]
					   --,[PayPostalCode]
					   ,[DateActivated]
					   ,[ProvisionalPayoutActive]
					   ,[ProvisionalPayoutStartDate]
					   ,[ProvisionalPayoutEndDate])
				 VALUES
					   (@UserID 
					   ,@PaymentIntervalID 
					   ,@PaymentTypeID 
					   ,@DefaultPayout 
					   ,@PayoutTypeID 
					   --,@UseDefaultAddress 
					   --,@PayAddress1 
					   --,@PayAddress2 
					   --,@PayCity 
					   --,@PayState 
					   --,@PayPostalCode 
					   ,@DateModified 
					   ,@ProvisionalPayoutActive 
					   ,@ProvisionalPayoutStartDate 
					   ,@ProvisionalPayoutEndDate)
			END
			
			--Confirmation of Success
			SELECT @Status = 0 
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserPreferences.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1 
		END;
		 
	END CATCH;
END;

GO
