USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserEmailUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserEmailUpdation
Purpose					: To update the email if he is not having it in the users table
Example					: usp_UserEmailUpdation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			16-02-2012	 SPAN Infotech India	     Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserEmailUpdation]
(
	@UserID int
	,@Email Varchar(100)
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			UPDATE Users 
			SET Email=@Email
			WHERE UserID=@UserID AND Email IS NULL
			
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserEmailUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
