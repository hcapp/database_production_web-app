USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserPreferences]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UserPreferences
Purpose					: To set User Preferences.
Example					: usp_UserPreferences

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			30th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserPreferences]
(
	 @UserID int
	,@LocaleRadius int
	,@ScannerSilent bit
	,@DisplayCoupons bit
	,@DisplayRebates bit
	,@DisplayLoyaltyRewards bit
	,@SavingsActivated bit
	,@SleepStatus bit
	,@DateModified datetime
	
	--OutPut Variable
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			--To fetch LocaleID of the user from UserLocation
			DECLARE @LocaleID INT
			SELECT @LocaleID = LocaleID FROM UserLocation WHERE UserID = @UserID 
			
			--To check the existance of the user preferred info.
			IF EXISTS (SELECT 1 FROM UserPreference WHERE UserID = @UserID AND ISNULL(LocaleID,0) = ISNULL(@LocaleID,0))
			--If info exists, modify the info.
			BEGIN
				UPDATE UserPreference
				SET LocaleRadius = @LocaleRadius
					--,ScannerSilent = @ScannerSilent
					--,DisplayCoupons = @DisplayCoupons
					--,DisplayRebates = @DisplayRebates
					--,DisplayLoyaltyRewards =	@DisplayLoyaltyRewards
					--,DisplayFieldAgent = 0
					--,FieldAgentRadius = 0
					--,SavingsActivated = @SavingsActivated 
					--,SleepStatus = @SleepStatus
					,DateModified = @DateModified
				WHERE UserID = @UserID
					AND LocaleID = @LocaleID
			END
			--If info not exists, add the info.
			IF NOT EXISTS (SELECT 1 FROM UserPreference WHERE UserID = @UserID AND ISNULL(LocaleID,0) = ISNULL(@LocaleID,0))
			BEGIN
				INSERT INTO UserPreference
					(UserID
					,LocaleID
					,LocaleRadius
					--,ScannerSilent
					--,DisplayCoupons
					--,DisplayRebates
					--,DisplayLoyaltyRewards
					--,DisplayFieldAgent
					--,FieldAgentRadius
					--,SavingsActivated
					--,SleepStatus
					,DateModified)
				VALUES
					(@UserID
					,@LocaleID
					,@LocaleRadius
					--,@ScannerSilent
					--,@DisplayCoupons
					--,@DisplayRebates
					--,@DisplayLoyaltyRewards
					--,0
					--,0
					--,@SavingsActivated 
					--,@SleepStatus
					,@DateModified)
					
			END
			
						
			--Confirmation of Success
			SELECT @Status = 0 
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UserPreferences.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of Failure
			SELECT @Status = 1 
		END;
		 
	END CATCH;
END;

GO
