USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerTodayShoppingListProducts]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_WebConsumerTodayShoppingListProducts  
Purpose               : To fetch User's Favorite Items.  
Example               : EXEC usp_WebConsumerTodayShoppingListProducts  
  
History  
Version  Date            Author        Change Description  
---------------------------------------------------------------   
1.0      29thJuly2013    Dhananjaya TR Initial Version  
---------------------------------------------------------------   
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerTodayShoppingListProducts]  
(  
   @UserID int  
 , @LowerLimit int   
 , @RecordCount int
   
 --OutPut Variable 
 , @MaxCnt int  output
 , @MaxShopCartCnt int output
 , @NxtPageFlag bit output 
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
  
     --To get Media Server Configuration.  
	  DECLARE @ManufConfig varchar(50)    
	  SELECT @ManufConfig=ScreenContent    
	  FROM AppConfiguration     
	  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
	  
	  --To get the row count for pagination.  
	  DECLARE @UpperLimit int  		           
	  SELECT @UpperLimit = @LowerLimit + @RecordCount   
	  
  
  --To get categories, Products and CLR info of a User's Favorite list.  
  SELECT UP.UserProductID  
   , UP.UserID  
   , UP.ProductID  
   , Case when P.ProductName='Unassigned Products' THEN UP.UnassignedProductName ELSE P.ProductName END ProductName 
   , ProductShortDescription  
   , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																							THEN @ManufConfig
																							+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																							+ProductImagePath ELSE ProductImagePath 
																					  END   
                          ELSE ProductImagePath END  
   , ISNULL(C.CategoryID ,0) CategoryID  
   , ISNULL(C.ParentCategoryID, 0) ParentCategoryID  
   , ISNULL(C.ParentCategoryName, 'Unassigned Products') ParentCategoryName     
   , Coupon.Coupon   
   , Loyalty.Loyalty  
   , Rebate.Rebate      
   , UP.ShopCartItem
   , FavProductFlag=CASE WHEN MasterListItem=1 THEN 1 ELSE 0 END
  INTO #CLR      
  FROM UserProduct UP  
   INNER JOIN Product P ON P.ProductID = UP.ProductID  
   LEFT JOIN ProductCategory PC ON PC.ProductID = P.ProductID  
   LEFT JOIN Category C ON C.CategoryID = PC.CategoryID    
   LEFT JOIN UserRetailPreference UR ON UR.UserID = UP.UserID AND UR.UserRetailPreferenceID = UP.UserRetailPreferenceID   
   LEFT JOIN Retailer R ON R.RetailID = UR.RetailID       
   OUTER APPLY fn_CouponDetails(UP.ProductID, R.RetailID) Coupon  
   OUTER APPLY fn_LoyaltyDetails(UP.ProductID, R.RetailID) Loyalty  
   OUTER APPLY fn_RebateDetails(UP.ProductID, R.RetailID) Rebate  
  WHERE UP.UserID = @UserID  
   AND (UP.TodayListtItem = 1 OR UP.ShopCartItem = 1)
   AND (P.ProductExpirationDate IS  NULL OR P.ProductExpirationDate  >= GETDATE()) 
     ORDER BY CASE WHEN C.CategoryID IS NULL THEN 'Unassigned Products' ELSE C.ParentCategoryName END  
     ,CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END 
  
  CREATE TABLE #TodaysShoppingList(Row_Num INT IDENTITY(1,1) 
							   , UserProductID int 
							   , UserID int  
							   , ProductID int 
							   , ProductName varchar(500)
							   , ProductShortDescription varchar(2000)							   
							   , ProductImagePath varchar(1000)  
							   , CategoryID int
							   , ParentCategoryID int
							   , ParentCategoryName varchar(255)							  
							   , CLRFlag bit
							   , Coupon_Status varchar(250)
							   , Loyalty_Status varchar(250)
							   , Rebate_Status varchar(250)
							   , ShopCartItem BIT
							   ,FavProductFlag BIT)
							   
  INSERT INTO #TodaysShoppingList(UserProductID 
							   , UserID 
							   , ProductID 
							   , ProductName 
							   , ProductShortDescription 							   
							   , ProductImagePath 
							   , CategoryID 
							   , ParentCategoryID 
							   , ParentCategoryName 							  
							   , CLRFlag 
							   , Coupon_Status 
							   , Loyalty_Status 
							   , Rebate_Status
							   , ShopCartItem
							   ,FavProductFlag)							   
  SELECT UserProductID userProductID  
   , CLR.UserID  
   , CLR.ProductID  
   , CLR.ProductName
   , ProductShortDescription  
   , CLR.ProductImagePath   
   , CategoryID categoryID  
   , ParentCategoryID parentCategoryID  
   , ParentCategoryName parentCategoryName  
   , CLRFlag = CASE WHEN (COUNT(CLR.Coupon)+COUNT(CLR.Loyalty)+COUNT(CLR.Rebate)) > 0 THEN 1 ELSE 0 END  
   , coupon_Status = CASE WHEN COUNT(CLR.Coupon) = 0 THEN 'Grey'   
           ELSE CASE WHEN COUNT(UC.CouponID) = 0 THEN 'Red' ELSE 'Green' END  
         END  
   , loyalty_Status = CASE WHEN COUNT(CLR.Loyalty) = 0 THEN 'Grey'  
         ELSE CASE WHEN COUNT(UL.LoyaltyDealID) = 0 THEN 'Red' ELSE 'Green' END  
          END  
   , rebate_Status = CASE WHEN COUNT(CLR.Rebate) = 0 THEN 'Grey'  
           ELSE CASE WHEN COUNT(UR.UserRebateGalleryID) = 0 THEN 'Red' ELSE 'Green' END  
         END 
   , ShopCartItem
   ,FavProductFlag
  FROM #CLR CLR  
   LEFT JOIN UserCouponGallery UC ON UC.UserID = CLR.UserID AND UC.CouponID = CLR.Coupon  
   LEFT JOIN UserLoyaltyGallery UL ON UL.UserID = CLR.UserID AND UL.LoyaltyDealID = CLR.Loyalty  
   LEFT JOIN UserRebateGallery UR ON UR.UserID = CLR.UserID AND UR.RebateID = CLR.Rebate   
  WHERE CLR.UserID = @UserID   
  GROUP BY UserProductID  
   , CLR.UserID  
   , CategoryID   
   , ParentCategoryID   
   , ParentCategoryName      
   , CLR.ProductID  
   , CLR.ProductName  
   , CLR.ProductImagePath  
   , ProductShortDescription  
   , ShopCartItem
   , FavProductFlag
  ORDER BY  ParentCategoryName, ProductName  
  
   --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #TodaysShoppingList
   
   SELECT @MaxShopCartCnt = COUNT(1) 
   FROM #TodaysShoppingList 
   WHERE ShopCartItem = 1 
   AND Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit
 
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 
  
  SELECT Row_Num row_Num 
   , UserProductID userProductID  
   , UserID  
   , ProductID  
   , ProductName
   , ProductShortDescription   
   , ProductImagePath   
   , CategoryID categoryID  
   , ParentCategoryID parentCategoryID  
   , ParentCategoryName parentCategoryName     
   , CLRFlag
   , coupon_Status    
   , loyalty_Status    
   , rebate_Status 
   , ShopCartItem
   , FavProductFlag
  FROM #TodaysShoppingList
  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit   
  
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerTodayShoppingListProducts.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
