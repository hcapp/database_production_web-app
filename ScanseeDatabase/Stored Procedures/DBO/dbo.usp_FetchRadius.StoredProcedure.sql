USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FetchRadius]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FetchRadius
Purpose					: To list the Distances
Example					:  
History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			5/28/2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_FetchRadius]
(
	   
	--OutPut Variable
	  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
			SELECT Radius = CAST(D.DistanceValue AS VARCHAR(10)) + ' ' + CASE WHEN  DistanceValue = 1 THEN CAST(U.UnitTypeName AS VARCHAR(4)) ELSE CAST(U.UnitTypeName AS VARCHAR(10)) END
				 , D.DistanceInMiles distInMiles
			FROM Distance D
			INNER JOIN UnitType U ON D.UnitTypeID = U.UnitTypeID 	
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FetchRadius.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
