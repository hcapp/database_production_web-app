USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListCouponGallery]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_MasterShoppingListCouponGallery]
Purpose					: To display the coupons of user coupon gallery
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListCouponGallery]
(
	  @Userid int
	 ,@Productid int
	 ,@Retailid int
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		--To Fetch Coupon Information from user coupon gallery for the product with retailer info.
		IF ISNULL(@Retailid,0)!=0 --(@RetailID IS NOT NULL and @RetailID != 0)
			BEGIN
				SELECT	 uc.CouponID 
						,CouponName
						,CouponDiscountType
						,CouponDiscountAmount
						,CouponDiscountPct
						,CouponShortDescription
						,CouponLongDescription
						,CouponDateAdded
						,CouponStartDate
						,CouponExpireDate
						,ViewableOnWeb
				FROM Coupon c
						INNER JOIN CouponProduct CP ON CP.CouponID=C.CouponID
						INNER JOIN UserCouponGallery UC ON c.couponid=uc.CouponID
				WHERE CP.ProductID = @productid
						AND RetailID = @retailid
						AND CouponExpireDate >= GETDATE()
						AND UserID=@userid
			END
			
		IF ISNULL(@Retailid,0)=0 --(@RetailID IS  NULL and @RetailID = 0)		
			BEGIN
				SELECT uc.CouponID 
					,CouponName
					,CouponDiscountType
					,CouponDiscountAmount
					,CouponDiscountPct
					,CouponShortDescription
					,CouponLongDescription
					,CouponDateAdded
					,CouponStartDate
					,CouponExpireDate
					,ViewableOnWeb
				FROM Coupon c
						INNER JOIN CouponProduct CP ON CP.CouponID=C.CouponID
						INNER JOIN UserCouponGallery UC ON c.couponid=uc.CouponID
				WHERE CP.ProductID = @productid
					AND CouponExpireDate >= GETDATE()
					AND UserID=@userid
			END
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListCouponGallery.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
