USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebHotdealCityStateRetailerList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebHotdealCityStateRetailerList
Purpose					: to get list of states,city,retailers.
Example					: usp_WebHotdealCityStateRetailerList

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			07 Dec 2011	   Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebHotdealCityStateRetailerList]
(
	@Search Varchar(10)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		IF(@Search='City')
			BEGIN
	
				SELECT CityState
				FROM(SELECT 'All'  CityState  
					 UNION ALL 
					 SELECT Distinct City  +' - '+  State CityState
					 FROM GeoPosition
					) City
			END
			
		IF(@Search='Retail')
			BEGIN
				SELECT RetailName
				FROM(SELECT 'All' RetailName
					 UNION ALL 
				     SELECT Distinct RetailName
				     FROM Retailer WHERE RetailerActive = 1
				     ) Retailer 	
			 END
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebHotdealCityStateRetailerList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;




GO
