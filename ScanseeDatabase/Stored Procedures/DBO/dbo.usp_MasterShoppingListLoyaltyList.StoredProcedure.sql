USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_MasterShoppingListLoyaltyList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_MasterShoppingListLoyaltyList
Purpose					: To dispaly Loyalty.
Example					: usp_MasterShoppingListLoyaltyList 1, 1, 13

History
Version		Date		Author			Change Description
--------------------------------------------------------------- 
1.0			7th June 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_MasterShoppingListLoyaltyList]
(
	  @ProductID int
	, @RetailID int
	, @UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		IF ISNULL(@RetailID, 0) != 0 --(@RetailID IS NOT NULL and @RetailID != 0)
		BEGIN
			SELECT RetailLocationID 
			INTO #Ret
			FROM RetailLocation 
			WHERE RetailID = @RetailID
			 
			SELECT LoyaltyDealID  
				,LoyaltyDealName
				,LoyaltyDealDiscountType
				,LoyaltyDealDiscountAmount
				,LoyaltyDealDiscountPct
				,LoyaltyDealDescription
				,LoyaltyDealDateAdded
				,LoyaltyDealStartDate
				,LoyaltyDealExpireDate
			INTO #Loyalty
			FROM LoyaltyDeal L
			LEFT JOIN #Ret R ON R.RetailLocationID = L.RetailLocationID 
			WHERE ProductID = @ProductID 
			AND LoyaltyDealExpireDate >= GETDATE()
			AND LoyaltyDealID NOT IN (SELECT LoyaltyDealID FROM UserLoyaltyGallery WHERE UserID = @UserID)
		END
		IF ISNULL(@RetailID, 0) = 0 --(@RetailID IS NULL OR @RetailID =0)
		BEGIN
			SELECT LoyaltyDealID  
				,LoyaltyDealName
				,LoyaltyDealDiscountType
				,LoyaltyDealDiscountAmount
				,LoyaltyDealDiscountPct
				,LoyaltyDealDescription
				,LoyaltyDealDateAdded
				,LoyaltyDealStartDate
				,LoyaltyDealExpireDate
			FROM LoyaltyDeal 
			WHERE ProductID = @ProductID 
			AND LoyaltyDealExpireDate >= GETDATE()
			AND LoyaltyDealID NOT IN (SELECT LoyaltyDealID FROM UserLoyaltyGallery WHERE UserID = @UserID)
		END
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_MasterShoppingListLoyaltyList.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;

GO
