USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserFaceBookLogIn]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FaceBookLogIn
Purpose					: For User log in through facebook.
Example					: usp_FaceBookLogIn
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd Nov 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UserFaceBookLogIn]
(
	  @UserName varchar(100)
	, @DateCreated datetime
	, @FaceBookAuthenticatedUser Bit
	
	--UserLocation Variables.
	, @DeviceID varchar(60)
	, @UserLongitude float
	, @UserLatitude float
	
	--Output Variable 
	, @FirstUserID int output
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			-- Check for existance of the Email Address. 
			--If already found, return -1
			DECLARE @UserID int
			SELECT @UserID = UserID 
			FROM Users 
			WHERE BINARY_CHECKSUM(UserName) = BINARY_CHECKSUM(@UserName)  
			      AND FaceBookAuthenticatedUser=@FaceBookAuthenticatedUser
			
			--If not found, insert into Users table. ie,allow user to register.
			IF ISNULL(@UserID, '') = ''			
			BEGIN
				INSERT INTO Users 
					(
					  UserName
					 ,PushNotify
					 ,FirstUseComplete
					 ,FieldAgent
					 ,DeviceID
					 ,DateCreated
					 ,FaceBookAuthenticatedUser
					)
				VALUES
					(
					  @UserName 
					 ,0
					 ,0
					 ,0
					 ,@DeviceID
					 ,@DateCreated
					 ,@FaceBookAuthenticatedUser
					)
				
				SET @UserID = SCOPE_IDENTITY()
				
				INSERT INTO UserLocation 
					(
						UserID 
						,DeviceID
						,UserLongitude
						,UserLatitude
						,UserLocationDate
					)
				VALUES
					(
						@UserID 
						,@DeviceID 
						,@UserLongitude 
						,@UserLatitude 
						,@DateCreated 
					)
			-- To capture User First Use Login.
			INSERT INTO [UserFirstUseLogin]
				   ([UserID]
				   ,[FirstUseLoginDate])
			 VALUES
				   (@UserID 
				   ,GETDATE())
			END	   
			--To Pass generated UserID.
			SELECT @FirstUserID = @UserID
			
			--Confirmation of Success.
			SELECT @Status = 0		
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FaceBookLogIn.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
