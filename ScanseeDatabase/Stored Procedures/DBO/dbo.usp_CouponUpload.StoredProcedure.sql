USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_CouponUpload]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_CouponUploads
Purpose					: To Upload Coupons.
Example					: usp_CouponUploads

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21 Feb 2012	 SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_CouponUpload]
(
	
	  @CouponFileName varchar(500)
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		
		DECLARE @ManufID int
		SELECT @ManufID = ManufacturerID FROM Manufacturer WHERE ManufName = 'Coupon Product Manufacturer'

			SELECT UPC , COUNT(1) CNT
			INTO #COUPON 
			FROM CouponAdmin 
			GROUP BY UPC 
			HAVING COUNT(1) > 1

			SELECT C.UPC, [Coupon Name] 
			, COUNT(1) CNT
			INTO #DiscardCoupon
			FROM #COUPON C
			INNER JOIN CouponAdmin CA ON CA.UPC = C.UPC
			GROUP BY C.UPC, [Coupon Name] 
			HAVING COUNT(1) = 1

			SELECT UPC , COUNT(1) CNT
			INTO #ValidCoupon
			FROM CouponAdmin 
			GROUP BY UPC 
			HAVING COUNT(1) = 1

			SELECT UPC 
			INTO #PROD
			FROM 
			(
			SELECT UPC FROM #COUPON WHERE UPC not in (SELECT UPC FROM #DiscardCoupon)
			UNION
			SELECT UPC FROM #ValidCoupon 
			)A

			INSERT INTO Product(
						ManufacturerID
						,ScanCode
						,ProductName
						,ProductShortDescription
						,ProductAddDate
						,ProductModifyDate)
			SELECT @ManufID
			, CA.UPC 
			, CA.[Coupon Name]
			, MIN (CA.[Coupon Description])
			, GETDATE()
			, GETDATE()
			FROM #PROD P
			LEFT JOIN Product D ON D.ScanCode = P.UPC  
			LEFT JOIN CouponAdmin CA ON CA.UPC = P.UPC 
			WHERE D.ProductID IS NULL 
			GROUP BY CA.UPC 
			, CA.[Coupon Name] 
			 


			INSERT INTO [Coupon]
					   ([CouponName] 
					   ,[CouponDiscountAmount]					   
					   ,[CouponShortDescription]  
					   ,[CouponDateAdded]
					   ,[CouponStartDate] 
					   ,[CouponExpireDate]
					   ,[CouponURL]
					   )
			SELECT [Coupon Name]
					,[Coupon Face Value]
					,[Coupon Description]
					,GETDATE()
					,ISNULL([Coupon Start],GETDATE())
					,[Coupon Expires]
					,[Coupon URL]
			FROM #PROD P
			INNER JOIN CouponAdmin CA ON CA.UPC = P.UPC 
			WHERE CA.[Coupon Expires] IS NOT NULL

			INSERT INTO [CouponProduct]
								   ([CouponID]
								   ,[ProductID]
								   ,[DateAdded]) 
			SELECT DISTINCT C.CouponID, P.ProductID, GETDATE()
			FROM #PROD D
			INNER JOIN CouponAdmin CA ON CA.UPC = D.UPC 
			INNER JOIN Product P ON P.ScanCode = CA.UPC 
			INNER JOIN Coupon C ON C.CouponName = CA.[Coupon Name] 
			ORDER BY C.CouponID 

			INSERT INTO DiscaredCoupons (UPC
										,[Coupon UPC]
										,[Coupon Name]
										,[Coupon Face Value]
										,[Coupon Description]
										,[Coupon Start]
										,[Coupon Expires]
										,[Coupon URL]
										,F9
										,[Discount Type]
										,[External]
										,RevShare
										,ScanSee
										,Source
										,CouponsFileName)
		    SELECT UPC
				 , [Coupon UPC]
				 , [Coupon Name]
				 , [Coupon Face Value]
				 , [Coupon Description]
				 , [Coupon Start]
				 , [Coupon Expires]
				 , [Coupon URL]
				 , F9
				 , [Discount Type]
				 , [External]
				 , RevShare
				 , ScanSee
				 , Source
				 , @CouponFileName 
		    FROM
			(SELECT CA.* 
			FROM #DiscardCoupon D
			INNER JOIN CouponAdmin CA on CA.UPC = D.UPC AND CA.[Coupon Name] = D.[Coupon Name] 
			UNION 
			SELECT * FROM CouponAdmin WHERE [Coupon Expires] IS NULL)DiscardedCoupons
			SET @Status = 0
		COMMIT TRANSACTION
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_CouponUpload.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			ROLLBACK TRANSACTION
			SET @Status = 1
		END;
		 
	END CATCH;
END;

GO
