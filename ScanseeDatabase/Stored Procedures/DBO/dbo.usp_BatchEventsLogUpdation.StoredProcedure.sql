USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchEventsLogUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_BatchEventsLogUpdation]
Purpose					: To update batch Events log table.
Example					: [usp_BatchEventsLogUpdation]

History
Version		  Date			Author		Change Description
--------------------------------------------------------------- 
1.0			7/20/2015       Span            1.1
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchEventsLogUpdation]
(
     --Input Variable
	  @FileName VARCHAR(1000)
	, @EventStatus BIT
	, @Reason VARCHAR(1000)
	, @ImageMissingCount int
	, @ListingImageMissingCount int

	--Output Variable
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
		BEGIN TRANSACTION 

			DECLARE @Date DATETIME
			SELECT @Date = MAX(ExecutionDate)
			FROM HcEventsBatchLog 
			WHERE ProcessedFileName = @FileName 
		
			--To update batch Events log table after data porting 
			UPDATE HcEventsBatchLog 
			SET Status = @EventStatus
			  , Reason = @Reason	
			  , ImageMissingCount = @ImageMissingCount
			  , EventListingImageMissingCount = @ListingImageMissingCount	 
			WHERE ProcessedFileName = @FileName
			AND ExecutionDate = @Date

		   --Confirmation of Success.
		   SELECT @Status = 0

		COMMIT TRANSACTION 

	END TRY
		
	BEGIN CATCH
	  
	--Check whether the Transaction is uncommitable.
	IF @@ERROR <> 0
	BEGIN
		PRINT 'Error occured in Stored Procedure [usp_BatchEventsLogUpdation].'		
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		ROLLBACK TRANSACTION 	
		--Confirmation of failure.
		SELECT @Status = 1
	END;
		 
	END CATCH;
END;


GO
