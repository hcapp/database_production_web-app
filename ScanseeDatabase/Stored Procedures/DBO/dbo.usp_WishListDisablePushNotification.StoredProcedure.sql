USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WishListDisablePushNotification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WishListDisablePushNotification
Purpose					: To disable Pushnotification
Example					: usp_WishListDisablePushNotification

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10th Oct 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WishListDisablePushNotification]
(
	--@UserProductID int
	@UserId int
	,@ProductId int
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		--To disable Pushnotification.(if user views the details of Sale then the pushnotification should be disabled.)
			UPDATE UserProduct 
			SET PushNotifyFlag = 0
			--WHERE UserProductID = @UserProductID
			WHERE UserID = @UserId and ProductID= @ProductId
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WishListDisablePushNotification.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
