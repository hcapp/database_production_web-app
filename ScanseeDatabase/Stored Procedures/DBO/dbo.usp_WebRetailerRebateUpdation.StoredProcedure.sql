USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerRebateUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerRebateUpdation
Purpose					: To Update the Existing Rebate Details by the Retailer.
Example					: 

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			10th January 2012 				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerRebateUpdation]
(

	--Input Input Parameter(s)--
	 
	  @RebateID int
	, @RebateName varchar(100)
	, @RebateAmount money
	, @RebateStartDate varchar(100)
	, @RebateEndDate varchar(100)
	, @RebateStartTime varchar(100)
	, @RebateEndTime varchar(100)
	, @RetailerID int
	, @RetailerLocationID varchar(max)
	, @RebateShortDescription Varchar(255)
	, @RebateLongDescription Varchar(1000)
	, @RebateTermsAndCondition Varchar(1000)
	, @RebateTimeZoneID int
	
	
	--Output Variable--
	  
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
				
				
				--Update the Rebates Table.
				UPDATE Rebate SET RebateName = @RebateName
				                , RebateAmount = @RebateAmount
				                --, RebateStartDate = CAST(@RebateStartDate AS DATETIME) + CAST(@RebateStartTime AS TIME)
				                --, RebateEndDate = CAST(@RebateEndDate AS DATETIME)+ CAST(@RebateEndTime AS TIME)
								 ,RebateStartDate= CONVERT(DATETIME, CONVERT(CHAR(8), @RebateStartDate, 112) + ' ' + CONVERT(CHAR(8), @RebateStartTime , 108))
								 ,RebateEndDate=CONVERT(DATETIME, CONVERT(CHAR(8), @RebateEndDate, 112) + ' ' + CONVERT(CHAR(8), @RebateEndDate , 108))
							  
							    , RebateShortDescription=@RebateShortDescription
				                , RebateLongDescription=@RebateLongDescription
				                , RebateTermsConditions=@RebateTermsAndCondition
				                , RebateTimeZoneID = @RebateTimeZoneID
				WHERE RebateID = @RebateID
				
				--Update the RebateRetailers Table.
				
				IF(@RetailerID IS NOT NULL AND @RetailerLocationID IS NOT NULL)			--LOOK IN HERE WHEN THE RETAILERS ALSO BECOMES MULTI-SELECT.	
				BEGIN
				DELETE FROM RebateRetailer WHERE RebateID = @RebateID
				
				INSERT INTO RebateRetailer (RebateID
				                          , RetailID
				                          , RetailLocationID)
				                SELECT @RebateID
				                     , @RetailerID
				                     , PARAM
				                FROM dbo.fn_SplitParam(@RetailerLocationID, ',')
					                        
			     END
		
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerRebateUpdation.'		
			-- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
