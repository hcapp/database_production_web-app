USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateBatchLog]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_UpdateBatchLog
Purpose					: To update status of batch processing.
Example					: usp_UpdateBatchLog

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10th Nov 2011	Padmapriya M	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_UpdateBatchLog]
(
	@APIPartnerID INT
	, @APIStatus BIT
	, @Reason VARCHAR(1000)
	
	--Output Variable 
	, @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @APIPartnerName varchar(100)
			SELECT @APIPartnerName = APIPartnerName 
			FROM APIPartner 
			WHERE APIPartnerID = @APIPartnerID 

			--To get the count of processed rows.
			DECLARE @Count INT
			IF @APIPartnerName = 'Yipit' OR @APIPartnerName = 'SimpleRelevance' OR @APIPartnerName = 'Plum District' OR @APIPartnerName='Living Social' 
			BEGIN
				SELECT @Count = COUNT(1)
				FROM ProductHotDeal HD 
				WHERE APIPartnerID = @APIPartnerID
				AND (CONVERT(DATE,CreatedDate) = CONVERT(DATE,GETDATE()) 
					OR
					CONVERT(DATE,DateModified) = CONVERT(DATE,GETDATE())) 
			END
			ELSE IF @APIPartnerName = 'wishpond'
			BEGIN
			SELECT  @Count = COUNT(1)
			FROM RetailLocationDeal 
			WHERE APIPartnerID = @APIPartnerID 
			AND CONVERT(DATE,DateCreated) = CONVERT(DATE,GETDATE())
			END
			--Check if Cellfire and capture the count & set the status of process as stopped in the AppConfiguration table.
			ELSE IF @APIPartnerName = 'CellFire'
			BEGIN
				SELECT @Count = COUNT(1)
				FROM LoyaltyDeal
				WHERE (CONVERT(DATE, LoyaltyDealDateAdded) = CONVERT(DATE,GETDATE())
					  OR
					  CONVERT(DATE, LoyaltyDealModifyDate) = CONVERT(DATE,GETDATE()))
				AND APIPartnerID = @APIPartnerID
				
				--Update the status of the Batch as stopped i.e 0
				UPDATE AppConfiguration
				SET ScreenContent = '0'
				WHERE ConfigurationType = 'CellFire Batch Process Running'
			END
			--Check if CommissionJunction and capture the count.
			ELSE IF @APIPartnerName = 'CommissionJunction'
			BEGIN
				SELECT @Count = COUNT(1)
				FROM Product
				WHERE (CONVERT(DATE, ProductAddDate) = CONVERT(DATE,GETDATE())
					  OR
					  CONVERT(DATE, ProductModifyDate) = CONVERT(DATE,GETDATE()))
				AND APIPartnerID = @APIPartnerID
			END	
		
			--To get the row of APIRowCount which was inserted at the time of deleting duplicates.
			DECLARE @Date DATETIME
			SELECT @Date = MAX(ExecutionDate)
			FROM APIBatchLog 
			WHERE APIPartnerID = @APIPartnerID 
			AND CONVERT(DATE,ExecutionDate) = CONVERT(DATE,GETDATE())

			UPDATE APIBatchLog 
			SET Status = @APIStatus
			, Reason = @Reason
			, ProcessedRowCount = @Count
			WHERE APIPartnerID = @APIPartnerID 
			AND ExecutionDate = @Date

		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_UpdateBatchLog.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
