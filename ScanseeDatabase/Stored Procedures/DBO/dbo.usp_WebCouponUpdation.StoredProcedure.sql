USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCouponUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebCouponUpdation
Purpose					: To Update the details of the existing Coupon.
Example					: usp_WebCouponUpdation

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th November 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebCouponUpdation]
(

	--Input Parameter(s)--
	
	--Inputs for the Coupon table
		
	  @CouponID int
	, @UserID int 
	, @CouponName varchar(100)	 
	, @CouponDiscountType varchar(20)
	, @CouponDiscountAmount money
	, @CouponDiscountPct float
	, @CouponShortDescription varchar(200)
	, @CouponLongDescription varchar(1000)	 
	, @CouponStartDate datetime
	, @CouponExpireDate datetime
	, @CouponURL varchar(1000)
	, @ExternalCoupon bit
	, @ViewableOnWeb bit
	
	--Inputs the CouponProduct table
	
	, @ProductIds varchar(MAX)
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		    
		    --Select Coupon Current Discount VAlue
		    DECLARE @CouponOldFaceValue Money
			SELECT @CouponOldFaceValue=CouponDiscountAmount  
		    FROM Coupon  
		    WHERE CouponID =@CouponID   
			
			 IF (@CouponDiscountAmount<>@CouponOldFaceValue)
		       BEGIN	    
		    
					SELECT DISTINCT UserID
						  ,ProductID
						  ,DeviceID
						  ,CouponID  	              
					INTO #Temp
					FROM (
					SELECT  U.UserID
						  ,UP.ProductID
						  ,UD.DeviceID 
						  ,CD.CouponID   
						  ,Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(Latitude / 57.2958) * COS((Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
						  ,LocaleRadius 
					from fn_SplitParam(@ProductIDs,',') F
					INNER JOIN CouponProduct CD ON CD.ProductID =F.Param AND CD.CouponID =@CouponID         
					INNER JOIN CouponRetailer CR ON CR.CouponID =CD.CouponID 
					INNER JOIN RetailLocation RL ON RL.RetailLocationID =CR.RetailLocationID AND RL.Active = 1
					INNER JOIN UserProduct UP ON UP.ProductID =CD.ProductID AND WishListItem =1	
					INNER JOIN Users U ON U.UserID =UP.UserID 
					INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
					INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
					INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
					)Note
					--WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
			        
			        			       					        
					--User pushnotification already exist then NotificationSent flag updating to 0
					UPDATE UserPushNotification SET NotificationSent=0
												   ,DateModified =GETDATE()			                                      
					FROM UserPushNotification UP
					INNER JOIN #Temp T ON UP.CouponID=T.CouponID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0 
			       
		           --User pushnotification not exist then insert. 
				   INSERT INTO UserPushNotification(UserID
													,ProductID
													,DeviceID												
													,CouponID 												
													,NotificationSent
													,DateCreated)
					SELECT T.UserID 
						  ,T.ProductID 
						  ,T.DeviceID 
						  ,T.CouponID  
						  ,0
						  ,GETDATE()
					FROM #Temp T				
					LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.CouponID = UP.CouponID AND T.ProductID = UP.ProductID AND DiscountCreated = 0
					WHERE UP.UserPushNotificationID IS NULL  
			  END				
		
		   --Updating the Coupon Table
			UPDATE Coupon SET				 
				CouponName = @CouponName,	
				CouponDiscountType = @CouponDiscountType ,
				CouponDiscountAmount = @CouponDiscountAmount, 
				CouponDiscountPct = @CouponDiscountPct ,
				CouponShortDescription = @CouponShortDescription ,
				CouponLongDescription = @CouponLongDescription ,				 				 
				CouponStartDate = @CouponStartDate ,
				CouponExpireDate = @CouponExpireDate ,
				CouponURL = @CouponURL ,
				ExternalCoupon = @ExternalCoupon,
				ViewableOnWeb = @ViewableOnWeb
			WHERE CouponID = @CouponID;		
			
			--If new asoociation is made.
			IF(@ProductIds IS NOT NULL AND @ProductIds NOT LIKE '')
			BEGIN
				DELETE FROM CouponProduct WHERE CouponID = @CouponID	 --Delete the existing records in the CouponProduct table against the input CouponID and insert new set of Products
				
				INSERT INTO CouponProduct 
				SELECT @CouponID
					 , Param 
					 , GETDATE()
				FROM dbo.fn_SplitParam(@ProductIds, ',') P
		    END
		    
		    --If Association is deleted.
		    IF @ProductIds IS NULL
		    BEGIN
				DELETE FROM CouponProduct WHERE CouponID = @CouponID
		    END
		    
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebCouponDelete.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
