USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerEventsDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebRetailerEventsDisplay]
Purpose					: To display List of Events for given Retailer.
Example					: [usp_WebRetailerEventsDisplay]

History
Version		   Date			Author		Change Description
--------------------------------------------------------------- 
1.0			07/25/2014	    SPAN            1.0
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerEventsDisplay]
(   
    --Input variable	  
	  @RetailID int
	, @SearchKey Varchar(2000)
	, @LowerLimit int 
	, @Fundraising bit 

	--Output Variable 
	, @MaxCnt int  output
	, @NxtPageFlag bit output 
    , @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	        
			DECLARE @UpperLimit int
			SET @Fundraising = ISNULL(@Fundraising,0)	
			  
			--To get the row count for pagination.	 
			SELECT @UpperLimit = @LowerLimit +  ScreenContent   
			FROM AppConfiguration   
			WHERE ScreenName = 'HubCitiWeb' 
			AND ConfigurationType = 'Pagination'
			AND Active = 1

	        DECLARE @Config VARCHAR(500)
	        SELECT @Config = ScreenContent
			FROM AppConfiguration
			WHERE ConfigurationType = 'Hubciti Media Server Configuration'
			
			--To display List fo Events
			CREATE TABLE #Events(Rownum int IDENTITY(1,1)
								,HcEventID int
								,HcEventName varchar(1000)
								,ShortDescription varchar(1000)
								,LongDescription varchar(MAX)
								,BussinessEvent INT
								,StartDate DATETIME
								,EndDate DATETIME
								,StartTime DATETIME
								,EndTime DATETIME
								,moreInfoURL varchar(1000)
								,Address VARCHAR(500)
								,City VARCHAR(50)
								,State VARCHAR(10)
								,PostalCode VARCHAR(10)
								,GeoErrorFlag INT
								)

			INSERT INTO #Events(HcEventID
							    ,HcEventName 
								,ShortDescription
								,LongDescription
								,BussinessEvent
								,StartDate
								,EndDate
								,StartTime
								,EndTime
								,moreInfoURL
								,Address
								,City 
								,State 
								,PostalCode 
								,GeoErrorFlag)
				SELECT DISTINCT E.HcEventID 
							   ,HcEventName  
							   ,ShortDescription
							   ,LongDescription 								
							   ,BussinessEvent
							   ,StartDate =CAST(StartDate AS DATE)
							   ,EndDate =CAST(EndDate AS DATE)
							   ,StartTime =CAST(CAST(StartDate AS Time) AS VARCHAR(5))
							   ,EndTime =CAST(CAST(EndDate AS Time)	AS VARCHAR(5))
							   ,MoreInformationURL moreInfoURL
							  ,Address = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.Address1 FROM HcRetailerEventsAssociation A 
																			  INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			  WHERE A.HcEventID = E.HcEventID)
																		 ELSE EL.Address 
										 END
							  ,City = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.City FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID)
																		 ELSE EL.City 
									  END
							  ,State = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.State FROM HcRetailerEventsAssociation A 
																			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			WHERE A.HcEventID = E.HcEventID)
																		 ELSE EL.State 
									  END
							  ,PostalCode = CASE WHEN E.BussinessEvent = 1 THEN (SELECT TOP 1 RL.PostalCode FROM HcRetailerEventsAssociation A 
																			    INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
																			    WHERE A.HcEventID = E.HcEventID)
																		  ELSE EL.PostalCode 
									  END  	
							  ,GeoErrorFlag = CASE WHEN E.BussinessEvent = 1 THEN 0 ELSE ISNULL(EL.GeoErrorFlag,0) END
							   								
				FROM HcEvents E 
				LEFT JOIN HcRetailerEventsAssociation RE ON RE.HcEventID = E.HcEventID
				LEFT JOIN RetailLocation RL ON RL.RetailLocationID = RE.RetailLocationID AND RL.Active = 1
				LEFT JOIN HcEventLocation EL on EL.HcEventID = E.HcEventID
				WHERE E.RetailID = @RetailID 
				AND  ((@Fundraising = 1 AND GETDATE() <= ISNULL(EndDate, GETDATE()+1)) OR @Fundraising = 0)
				AND E.Active = 1				
				AND ((@SearchKey IS NOT NULL AND HcEventName LIKE '%'+ @SearchKey +'%') OR (@SearchKey IS NULL AND 1=1))
				ORDER BY E.HcEventName                

			--To capture max row number.  
			SELECT @MaxCnt = MAX(RowNum) FROM #Events
				 
			--this flag is a indicator to enable "More" button in the UI.   
			--If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
			SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 

			SET @MaxCnt = ISNULL(@MaxCnt,0)

			SELECT Rownum	
			      ,HcEventID AS eventID 
			      ,HcEventName AS eventName 
			      ,ShortDescription
                  ,LongDescription 								
				  ,BussinessEvent
				  ,StartDate eventStartDate
				  ,EndDate eventEndDate
				  ,StartTime eventTime
				  ,EndTime
				  ,Address
				  ,City
				  ,State
				  ,PostalCode	
				  ,GeoErrorFlag					
				  ,moreInfoURL	  				  											
			FROM #Events
			WHERE RowNum BETWEEN (@LowerLimit+1) AND @UpperLimit 	
		    ORDER BY RowNum 

			--Confirmation of Success
			SELECT @Status = 0	      
	
	END TRY
		
	BEGIN CATCH
	  
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN		 
			PRINT 'Error occured in Stored Procedure [usp_WebRetailerEventsDisplay].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;





GO
