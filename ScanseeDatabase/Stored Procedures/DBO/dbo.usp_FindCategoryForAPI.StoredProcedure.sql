USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FindCategoryForAPI]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_FindCategoryForAPI
Purpose					: To display the Categories under a selected Category.
Example					: 

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			13th December 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_FindCategoryForAPI]
(
	   @ApiPartnerName Varchar(50)
	 , @CategoryName varchar(100)
	--Output Variable 
	,  @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		
			SELECT  BusinessCategoryID 
					,CategoryName CatName
			FROM  FindSourceCategory F
			INNER JOIN APIPartner A ON A.APIPartnerID=F.APIPartnerID
			WHERE ActiveFlag=1 And APIPartnerName=@ApiPartnerName
			AND CategoryDisplayName = @CategoryName
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_FindCategoryForAPI.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
