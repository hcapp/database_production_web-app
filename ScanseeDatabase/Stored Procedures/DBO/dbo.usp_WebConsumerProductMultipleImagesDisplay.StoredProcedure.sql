USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerProductMultipleImagesDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	 : usp_WebConsumerProductMultipleImagesDisplay  
Purpose					 : To dispaly Product Details 
Example					 : usp_WebConsumerProductMultipleImagesDisplay 
  
History  
Version  Date				Author		    Change Description  
---------------------------------------------------------------   
1.0		 15th July      	Dhananjaya TR	Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerProductMultipleImagesDisplay]  
(  

 @ProductID int  
 
 --User Tracking
  , @ProductListID int
  , @SaleListID int      
  , @MainMenuID int
 
   
 --Output Variable   
 , @Status int output
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
	BEGIN TRANSACTION
 
   --To get Media Server Configuration. 
	DECLARE @ManufConfig varchar(50) 
	DECLARE @Config varchar(50)
			  
	SELECT @ManufConfig = ScreenContent  
	FROM AppConfiguration   
	WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
   
    DECLARE @productSKU Varchar(200) 
    
      
    
    SELECT @productSKU =SKUNumber FROM ProductSKU WHERE ProductID =@ProductID 
   
    DECLARE @MediaTypeID INT
    SELECT @MediaTypeID = ProductMediaTypeID 
	FROM ProductMediaType
	WHERE ProductMediaType = 'Image Files'

        SELECT @Config = ScreenContent    
		FROM AppConfiguration     
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'

	DECLARE @VideoFlag bit  
	DECLARE @AudioFlag bit 

	SELECT DISTINCT P.ProductID productId
		   , P.ProductName productName 
		   , ModelNumber modelNumber
		   , CASE WHEN @VideoFlag > 0 THEN 'Available' ELSE 'N/A' END  videoFlag
		   , CASE WHEN @AudioFlag > 0 THEN 'Available' ELSE 'N/A' END  audioFlag
		   , P.ProductShortDescription productShortDescription
		   , CASE WHEN P.ProductLongDescription = P.ProductShortDescription THEN NULL ELSE P.ProductLongDescription END productLongDescription	 
		   , ISNULL(AVG(Rating),0) prodRatingCount
		   , COUNT(PR.ProductID) prodReviewCount
		   , productImagePath =ISNULL(CASE WHEN WebsiteSourceFlag = 1 
								THEN @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + ProductImagePath
							ELSE ProductImagePath END, '') + ', '
							 + ISNULL(STUFF((SELECT + ', ' +CASE WHEN WebsiteSourceFlag = 1 
											                    THEN  @Config + CAST(P.ManufacturerID AS VARCHAR(10)) + '/' + B.ProductMediaPath
														    ELSE B.ProductMediaPath END  
							FROM ProductMedia b 
							WHERE b.ProductID = P.ProductID
							AND B.ProductMediaTypeID = @MediaTypeID
							FOR XML PATH('')), 1, 2, ''), '')
				 			
			----,productImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN WebsiteSourceFlag = 1     
   --                    THEN @ManufConfig    
   --                    +CONVERT(VARCHAR(30),P.ManufacturerID)+'/'    
   --                    +ProductImagePath ELSE ProductImagePath     
   --                    END       
   --                       ELSE ProductImagePath END      				
		   , @productSKU skuCode 
		   , ISNULL(P.ScanCode,0) scanCode
	INTO #Prod       	
	FROM Product P  	
	LEFT JOIN ProductMedia PM ON P.ProductID = PM.ProductID
	LEFT JOIN ProductMediaType PT ON PT.ProductMediaTypeID = PM.ProductMediaTypeID 
	LEFT JOIN UserRating UR ON P.ProductID = UR.ProductID
	LEFT JOIN ProductReviews PR ON P.ProductID = PR.ProductID
	WHERE P.ProductID  = @ProductID
	GROUP BY P.ProductID,P.ProductName,ModelNumber,P.ProductShortDescription,P.ProductLongDescription,ProductImagePath,WebsiteSourceFlag,P.ManufacturerID,ScanCode
	
	
	SELECT productId
		   , productName 
		   , modelNumber
		   , videoFlag
		   , audioFlag
		   , productShortDescription
		   , productLongDescription	 
		   , prodRatingCount
		   , prodReviewCount
		   , productImagePath = CASE WHEN RIGHT(RTRIM(productImagePath), 1) = ',' THEN LEFT(productImagePath, LEN(productImagePath)-1) ELSE productImagePath END		   
		   , skuCode 
		   , scanCode
	FROM #Prod
	
	--User Tracking
	
	UPDATE ScanSeeReportingDatabase..SalesList 
	SET RetailLocationDealClick = 1
	WHERE SaleListID = @SaleListID
	
	
	Update ScanSeeReportingDatabase..ProductList
	SET ProductClick =1 
	WHERE ProductListID =@ProductListID 
	
	
	--Confirmation of Success.
	SELECT @Status = 0
   COMMIT TRANSACTION
 END TRY  
    
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerProductMultipleImagesDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
   PRINT 'The Transaction is uncommittable. Rolling Back Transaction'  
   ROLLBACK TRANSACTION; 
   --Confirmation of failure.
   SELECT @Status = 1 
  END;  
     
 END CATCH;  
END;


GO
