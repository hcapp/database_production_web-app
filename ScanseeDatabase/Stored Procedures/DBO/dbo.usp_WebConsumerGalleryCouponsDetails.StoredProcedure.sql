USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerGalleryCouponsDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebConsumerGalleryCouponsDetails]
Purpose					: To display details of coupon related to the user.
Example					: [usp_WebConsumerGalleryCouponsDetails]

History
Version		Date			     Author			Change Description
--------------------------------------------------------------- 
1.0			3rd June 2013        Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerGalleryCouponsDetails]
(
	  @Userid int
	, @couponid int
	
	--Inputs for User Tracking. 
	, @CouponListID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		BEGIN TRANSACTION
		
			--To get Media Server Configuration.  
			DECLARE @RetailConfig varchar(50)    
			SELECT @RetailConfig=ScreenContent    
			FROM AppConfiguration     
			WHERE ConfigurationType='Web Retailer Media Server Configuration'
			  
			SELECT	ISNULL(ProductName,'') productName
					,ISNULL(CP.ProductID,0)  ProductID
					,RetailName					
					,C.CouponID
					,CouponName
					,CouponShortDescription
					,CouponLongDescription
					,CouponStartDate
					,CouponExpireDate
					,CouponURL
					,CouponTermsAndConditions termsAndConditions 
					,UsedFlag=CASE WHEN C.CouponID=UC.CouponID AND UC.UsedFlag =1 THEN 2 WHEN C.CouponID=UC.CouponID AND UC.UsedFlag=0 THEN 1 ELSE 0 END
					,CASE WHEN C.CouponExpireDate<GETDATE() THEN 1 ELSE 0 END expFlag
					,C.CouponDiscountAmount couponDiscountAmount
					,R.Address1 address
					,R.City city
					,R.State state
					,R.PostalCode postalCode
					,CouponImagePath=CASE WHEN CouponImagePath IS NULL THEN dbo.fn_CouponImage(C.CouponID) ELSE CASE WHEN CouponImagePath IS NOT NULL THEN 
																												CASE WHEN C.WebsiteSourceFlag = 1 
																													THEN @RetailConfig
																													+CONVERT(VARCHAR(30),CR.RetailID)+'/'
																													+CouponImagePath 
																												ELSE CouponImagePath 
																												END
																										 END 
							 END  
				     ,CouponProductExist=Case When CP.CouponProductId IS Not NULL THEN 1 ELSE 0 END			  
			FROM Coupon C
		    LEFT JOIN CouponProduct CP ON C.CouponID=CP.CouponID
			LEFT JOIN Product P ON P.ProductID=CP.ProductID
			LEFT JOIN UserCouponGallery UC ON UC.CouponID = C.CouponID AND UC.UserID = @Userid
			LEFT JOIN CouponRetailer CR ON C.CouponID = CR.CouponID
			LEFT JOIN Retailer R ON R.RetailID = CR.RetailID			
			WHERE  C.CouponID=@couponid 
 
			
			--User Tracking section.
			
			--Capture the Click
			UPDATE ScanSeeReportingDatabase..CouponList SET CouponClick = 1
			WHERE CouponListID = @CouponListID
			
		 --Confirmation of Success.
		SELECT @Status = 0	
		COMMIT TRANSACTION	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebConsumerGalleryCouponsDetails].'		
			--- Execute retrieval of Error info.
			EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;	
			--Confirmation of failure.
			SELECT @Status = 1		
		END;
		 
	END CATCH;
END;


GO
