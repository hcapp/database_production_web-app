USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetrieveTimeZone]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetrieveTimeZone
Purpose					: To display the set of standard timezones
Example					: Exec usp_WebRetrieveTimeZone

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27 Jan 2012 	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetrieveTimeZone]
(
	
	--Output Variable 
	 @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		SELECT TimeZoneID timeZoneId
			  ,TimeZone timeZoneName
		FROM TimeZone
		Order BY TimeZone
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetrieveTimeZone.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;


GO
