USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_SalesAppRetailerCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SalesAppRetailerCreation
Purpose					: To register New Retailer. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			18th Dec 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SalesAppRetailerCreation]
(

	 @RetailerName Varchar(100)
	,@Address1 Varchar(100)
	,@Address2 Varchar(50)
	,@State Char(2)
	,@City Varchar(50)
	,@PostalCode Varchar(10)
	,@CorporatePhoneNo Char(10)
	,@BusinessCategory varchar(1000)
	,@RetailerURL varchar(1000)
	,@NumberOfLocations int
	,@RetailerLatitude Float
    ,@RetailerLongitude Float
	--Input Variables of RetailContact
	,@ContactFirstName Varchar(20)
	,@ContactLastName Varchar(30)
	,@ContactPhone Char(10)
	--Input Variables of Users--
	
	,@UserName varchar(100)
	,@ContactEmail Varchar(100)
	,@Password Varchar(60)
	--Input Variables of UserRetailer--
	,@CorporateAndSore BIT
	,@StoreIdentification VARCHAR(20)
	
	--Input Variables of Salesperson
	,@SalesPersonID int
	,@Latitude float
	,@Longitude float
	,@DeviceID varchar(100)
	,@StartDate datetime
	
	--Output Variable 
	, @ResponseRetailID int output
	, @DuplicateFlag bit output
	, @DuplicateRetailerFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
		IF NOT EXISTS(SELECT 1 FROM Retailer WHERE RetailName LIKE @RetailerName)
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName)
			BEGIN
		
		    --INSERT INTO USERS TABLE
			INSERT INTO Users
						(UserName
						,Password
						,DateCreated)
			VALUES( @UserName
					,@Password
					,GETDATE())
					
			DECLARE @UserID int
			SET @UserID=SCOPE_IDENTITY();
		
			--INSERT INTO RETAILER TABLE
		 
			INSERT INTO [Retailer]
					   ([RetailName]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[CountryID]
					   ,[RetailURL]
					   ,[NumberOfLocations]
					   ,[CreateUserID]
					   ,[DateCreated]
					   ,[CorporatePhoneNo]
					   , WebsiteSourceFlag)
			VALUES( @RetailerName
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   ,1
				   ,@RetailerURL
				   ,@NumberOfLocations
				   ,@UserID
				   ,GETDATE()
				   ,@CorporatePhoneNo
				   , 1)			   
				   
				   
			DECLARE @RetailID int
			SET @RetailID=SCOPE_IDENTITY(); 
			
			--To update City state changes
			UPDATE Retailer 
			SET HccityID = C.HccityID
			FROM Retailer R
			INNER JOIN HcCity C on R.City = C.CityName
			WHERE Retailid = @RetailID

            UPDATE Retailer 
			SET StateID = C.StateID
			FROM Retailer R
			INNER JOIN state C on R.State = C.Stateabbrevation
			WHERE Retailid = @RetailID

			--INSERT INTO RetailCategoty TABLE
		   
		   INSERT INTO RetailerBusinessCategory(RetailerID
		                            , BusinessCategoryID
		                            , DateCreated)
		                       SELECT @RetailID
		                            , [Param]
		                            , GETDATE()
		                            
		                       FROM dbo.fn_SplitParam(@BusinessCategory, ',')
		                      
			--INSERT INTO RETAILLOCATION TABLE
			
			INSERT INTO [RetailLocation]
					   ([RetailID]
					   ,[Headquarters]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[RetailLocationLatitude]
					   ,[RetailLocationLongitude]
					   ,[CountryID]
					   ,[RetailLocationURL]
					   ,[CorporateAndStore]
					   ,[StoreIdentification]
					   ,[DateCreated])  
			VALUES(@RetailID
				   ,CASE WHEN @CorporateAndSore = 1 THEN 0 ELSE 1 END
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   ,@RetailerLatitude 
                   ,@RetailerLongitude 
                   ,1
				   , CASE WHEN @CorporateAndSore = 1 THEN @RetailerURL ELSE NULL END
				   ,@CorporateAndSore
				   ,@StoreIdentification
				   ,GETDATE())
				   
			DECLARE @RetailLocationID int
			SET @RetailLocationID=SCOPE_IDENTITY();

			--To update City state changes
			UPDATE RetailLocation 
			SET HcCityID = C.HccityID
			FROM RetailLocation R
			INNER JOIN HcCity C on R.City = C.CityName
			WHERE Retaillocationid = @RetailLocationID

            UPDATE RetailLocation 
			SET StateID = C.StateID
			FROM RetailLocation R
			INNER JOIN state C on R.State = C.Stateabbrevation
			WHERE Retaillocationid = @RetailLocationID
			
			--INSERT INTO CONTACT TABLE
							   
			INSERT INTO [Contact]
					   ([ContactFirstName]
					   ,[ContactLastname]
					   ,[ContactTitle]					  
					   ,[ContactPhone]
					   ,[ContactEmail]
					   ,[DateCreated]
					   ,[CreateUserID])
			VALUES(@ContactFirstName
				   ,@ContactLastName
				   ,1				 
				   ,@ContactPhone
				   ,@ContactEmail
				   ,GETDATE()
				   ,@UserID)
				   
			DECLARE @ContactID int
			SET @ContactID=SCOPE_IDENTITY();
				   
			--INSERT INTO RETAILCONTACT TABLE				   
			INSERT INTO [RetailContact]
					   ([ContactID]
					   ,[RetailLocationID])
			VALUES(@ContactID
				   ,@RetailLocationID)
				   
			--INSERT INTO USERRETAILER CHILD TABLE				   
			INSERT INTO UserRetailer
					   ([RetailID]
					   ,[UserID])
			VALUES(@RetailID
				   ,@UserID)          
		          
			--INSERT INTO RETAILERBILLINGDETAILS TABLE	
			INSERT INTO RetailerBillingDetails( UserID
												,RetailerID
												,SalesPersonID
												,DeviceID
												,StartDate
												,Latitude
												,Longitude
												,DateCreated)
									VALUES(@UserID
										 ,@RetailID
										 ,@SalesPersonID
										 ,@DeviceID
										 ,@StartDate
										 ,@Latitude
										 ,@Longitude
										 ,GETDATE())			          
		          			
         		
			SET @Status = 0					--Confirmation of Success.		
			SET @DuplicateFlag = 0	
			SET @DuplicateRetailerFlag = 0		--CONFIRM NO DUPLICATION
			SET @ResponseRetailID = @RetailID 
		END
		ELSE
		BEGIN
			SET @Status = 0					--Confirmation of Success.
			SET @DuplicateFlag = 1	
			SET @DuplicateRetailerFlag = 0		--CONFIRM DUPLICATION
			SET @ResponseRetailID = @RetailID 
		END		
	   END
	   ELSE
	   BEGIN
		PRINT 'Retailer Name exists in the system'
		SET @Status = 0		
		SET @DuplicateFlag = 0
		SET @DuplicateRetailerFlag = 1
	   END
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SalesAppRetailerCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
