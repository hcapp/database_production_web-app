USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerHotDealModification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerHotDealModification
Purpose					: To Update the details of the existing Hot Deal BY THE RETAILER.
Example					: usp_WebRetailerHotDealModification

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			28th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerHotDealModification]
(

	--Input Parameter(s)--
	
	--Inputs to ProductHotdeal Table
	  @HotDealID int
	, @HotDealName  varchar(255)
	, @RegularPrice money
    , @SalePrice money    
    , @HotDealShortDescription varchar(100)
    , @HotDealLongDescription varchar(1000)
    , @HotDealTerms varchar(500)
    , @DealStartDate DATE
    , @DealStartTime Time
    , @DealEndDate DATE
    , @DealEndTime TIME
    , @HotDealTimeZoneID int
    , @CategoryID int
    , @CouponCode Varchar(100)
    , @Quantity int  --No of HotDeals
    , @ExpirationDate DATE
    , @ExpirationTime TIME
    , @HotDealImagePath varchar(1000)  
    
    --, @City varchar(MAX)			Commented because the Prototype is changed to take the Population Centre ID as input.
    --, @State varchar(MAX)
    
    , @PopulationCentreID VARCHAR(MAX)
    
    , @RetailerID VARCHAR(MAX)
    , @RetailerLocationID VARCHAR(MAX)
    , @ProductIDs varchar(MAX)
     
	--Output Variable--
	, @FindClearCacheURL varchar(1000) output
	, @PercentageFlag bit output  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		DECLARE @Percentage float  -- To calculate Hot Deal percentage
		DECLARE @Difference float
		DECLARE @HDOldSalePice Money
		SET @Difference = (@RegularPrice - @SalePrice)
		SET @Percentage = (@Difference*100)/@RegularPrice

		IF @ProductIDs=''
		BEGIN
		SET @ProductIDs=null
		END

		
		IF (@Percentage >= 50)
		BEGIN
		    	    
		    SELECT @HDOldSalePice=SalePrice 
		    FROM ProductHotDeal 
		    WHERE ProductHotDealID =@HotDealID  
		     
		    print 'a'
		    --Update ProductHotDeal table
			UPDATE ProductHotDeal SET HotDealName = @HotDealName
			                        , Price = @RegularPrice
			                        , HotDealDiscountAmount = @Difference
									, HotDealDiscountPct = @Percentage
			                        , SalePrice = @SalePrice
			                        , HotDealShortDescription = @HotDealShortDescription
			                        , HotDeaLonglDescription = @HotDealLongDescription
			                        , HotDealTermsConditions = @HotDealTerms
			                        , HotDealStartDate = CASE WHEN (CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'')AS DATETIME))='1900-01-01 00:00:00.000' 
											   THEN NULL 
											 ELSE (CAST(@DealStartDate AS DATETIME)+CAST(ISNULL(@DealStartTime,'') AS DATETIME))
										END
			                        , HotDealEndDate = CASE WHEN (CAST(@DealEndDate AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME))='1900-01-01 00:00:00.000'
											    THEN NULL 
											 ELSE (CAST(@DealEndDate AS DATETIME)+CAST(ISNULL(@DealEndTime,'') AS DATETIME))
										END	
			                        , HotDealTimeZoneID=@HotDealTimeZoneID		
			                        , CategoryID = @CategoryID  
			                        , DateModified = GETDATE() 
			                        , HotDealCode = @CouponCode
			                        , NoOfHotDealsToIssue = @Quantity
			                        , HotDealExpirationDate =  CASE WHEN ((CAST(@ExpirationDate AS DATETIME)+CAST(ISNULL(@ExpirationTime,'') AS DATETIME))='1900-01-01 00:00:00.000' OR ((CAST(@ExpirationDate AS DATETIME)+CAST(ISNULL(@ExpirationTime,'') AS DATETIME)) IS NULL))	
												THEN NULL 
											 ELSE (CAST(@ExpirationDate AS DATETIME)+CAST(ISNULL(@ExpirationTime,'') AS DATETIME))
										END	 
			                        , HotDealImagePath = @HotDealImagePath                   
			             WHERE ProductHotDealID = @HotDealID

						
		  
			             
                        
		  IF (@PopulationCentreID IS NOT NULL)
          BEGIN
          
          DELETE FROM ProductHotDealRetailLocation WHERE ProductHotDealID = @HotDealID
          
          DELETE FROM ProductHotDealLocation WHERE ProductHotDealID = @HotDealID
          
          INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]												 
											  )
										SELECT @HotDealID
										     , City
										     , [State]
										FROM dbo.fn_SplitParam(@PopulationCentreID, ',') P
										INNER JOIN PopulationCenterCities PC ON P.Param = PC.PopulationCenterID										
			
		  END		
			   
			IF(@ProductIDs IS NOT NULL)				--Product Association is not mandated if the deal is w.r.t City & State.
			BEGIN
				DELETE FROM HotDealProduct WHERE ProductHotDealID = @HotDealID
					
				INSERT INTO HotDealProduct (ProductHotDealID
					                          , ProductID)
					                   SELECT @HotDealID
					                        , PARAM
					                        FROM dbo.fn_SplitParam(@ProductIDs, ',')
			END
			
			
			IF(@RetailerLocationID IS NOT NULL)
			BEGIN
				DELETE FROM ProductHotDealLocation WHERE ProductHotDealID = @HotDealID
				
				DELETE FROM ProductHotDealRetailLocation WHERE ProductHotDealID = @HotDealID
				
				INSERT INTO ProductHotDealRetailLocation(
															ProductHotDealID
														  , RetailLocationID
														)
											SELECT @HotDealID							--For now the Retailer is single select and Retaillocation is multi select
											     , [Param]
											FROM dbo.fn_SplitParam(@RetailerLocationID, ',')	
				INSERT INTO ProductHotDealLocation (
													ProductHotDealID												 
												  , City
												  , [State]	
												  											 
											  )
										SELECT @HotDealID
										     , City
										     , [State]
										FROM RetailLocation RL 
										WHERE RetailLocationID = @RetailerLocationID AND RL.Active = 1														
											
				
			END
              SET @PercentageFlag = 0
		END	
		
		
		
		ELSE 
		BEGIN
			SET @PercentageFlag = 1
		END 
		
		 IF (@SalePrice<>@HDOldSalePice)
		 BEGIN	
				SELECT DISTINCT UserID
		              ,ProductID
		              ,DeviceID
		              ,ProductHotDealID		              
		        INTO #Temp
		        FROM (
		        SELECT U.UserID
		              ,UP.ProductID
		              ,UD.DeviceID 
		              ,HD.ProductHotDealID  
		              , Distance= (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(Latitude  / 57.2958) * COS((Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 
		              ,LocaleRadius 
		        from fn_SplitParam(@ProductIDs,',') F
		        INNER JOIN HotDealProduct  HD ON HD.ProductID =F.Param AND HD.ProductHotDealID =@HotDealID 	        
		        INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID =HD.ProductHotDealID 
		       -- INNER JOIN RetailLocation RL ON RL.RetailLocationID =PRL.RetailLocationID  
		        INNER JOIN UserProduct UP ON UP.ProductID =HD.ProductID AND WishListItem =1	
		        INNER JOIN Users U ON U.UserID =UP.UserID 
		        INNER JOIN UserDeviceAppVersion UD ON UD.UserID =U.UserID AND UD.PrimaryDevice =1
		        INNER JOIN UserPreference UPE ON UPE.UserID =U.UserID 
		        INNER JOIN GeoPosition G ON G.PostalCode =U.PostalCode  	       
		    )Note
		     --WHERE Distance <= ISNULL(LocaleRadius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenName = 'WebConsumerDefaultRadius'))  
		    
		    
		        --UPDATE UserPushNotification SET CouponID=NULL,RetailLocationDealID=Null 
		        --FROM UserPushNotification UP
		        --INNER JOIN #Temp T ON T.UserID =UP.UserID AND T.ProductID =UP.ProductID 
		        --WHERE NotificationSent =1 AND (T.ProductHotDealID =UP.ProductHotdealID OR UP.ProductHotdealID IS NULL)
		        
		        --User pushnotification already exist then NotificationSent flag updating to 0
		        UPDATE UserPushNotification SET NotificationSent=0
		                                       ,DateModified =GETDATE()			                                      
		        FROM UserPushNotification UP
		        INNER JOIN #Temp T ON UP.ProductHotdealID=T.ProductHotDealID AND T.UserID =UP.UserID AND T.ProductID =UP.ProductID AND DiscountCreated=0
		       
		       --User pushnotification not exist then insert. 
		       INSERT INTO UserPushNotification(UserID
												,ProductID
												,DeviceID												
												,ProductHotdealID												
												,NotificationSent
												,DateCreated)
				SELECT T.UserID 
				      ,T.ProductID 
				      ,T.DeviceID 
				      ,T.ProductHotDealID 
				      ,0
				      ,GETDATE()
				FROM #Temp T				
				LEFT JOIN UserPushNotification UP ON UP.UserID =T.UserID AND T.ProductHotDealID =UP.ProductHotdealID AND T.ProductID =UP.ProductID AND DiscountCreated=0  
				WHERE UP.UserPushNotificationID IS NULL 				
			END

			-------Find Clear Cache URL---29/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)--,@FindClearCacheURL varchar(1000)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			  --select @CurrentURL,@SupportURL,@YetToReleaseURL

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			 -- select @FindClearCacheURL

			-----------------------------------------------	
		
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerHotDealModification.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
