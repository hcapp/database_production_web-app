USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerChoosePlan]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerChoosePlan
Purpose					: To insert the details of the plan choosed by the Retailer
Example					: usp_WebRetailerChoosePlan

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st January 2012 Pavan Sharma K 	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerChoosePlan]
(
		 @RetailerID int
		,@UserID int
		,@BillingPlanRetailerIDs varchar(1000)                      --Comma Separated values 
		,@Quantitys varchar(1000)                                   --Comma Separated values  
		,@TotalPrice money		
		,@DiscountPlanRetailerID int
		,@RetailerProductFeeFlag bit
			
	--Output Variable 
		, @Status int output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION 
		
		SELECT IDENTITY(INT, 1,1) Row						--To split the comma separated Plans.
		     , BillingPlan
		    INTO #BillingPlans
		    
		FROM (SELECT [Param] BillingPlan
		      FROM  dbo.fn_SplitParam(@BillingPlanRetailerIDs, ',')) BillingPlans
		      
		 SELECT IDENTITY(INT, 1,1) RowNum					--To split the comma separated Quantity.
		     , Quantity
		    INTO #Quantity
		    
		FROM (SELECT [Param] Quantity
		      FROM  dbo.fn_SplitParam(@Quantitys, ',')) Quantity
		
		--INSERT INT0 THE RetailerBillingDetails TABLE.
			INSERT INTO RetailerBillingDetails(
													    UserID
													  ,	RetailerID
													  , RetailerBillingPlanID
													  , Quantity
													  , TotalPrice
													  , RetailerProductFeeFlag		
													  , RetailerDiscountPlanID											 
												   )
										
					                            SELECT @UserID
					                                 , @RetailerID
					                                 , B.BillingPlan
					                                 , CASE WHEN Quantity = 0 THEN 'Waived' ELSE Quantity END          --If the Price is waived then we dont have any quantity.
					                                 , @TotalPrice
					                                 , @RetailerProductFeeFlag
					                                 , @DiscountPlanRetailerID
					                            FROM #BillingPlans B
					                            INNER JOIN #Quantity Q ON B.Row = Q.RowNum
					
			
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerChoosePlan.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
