USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerRebateAddProductSearch]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerRebateAddProductSearch
Purpose					: To Search for the Product and Add Rebate on it by the Retailer.
Example					: usp_WebRetailerRebateAddProductSearch

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			6th January 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerRebateAddProductSearch]
(	
	 
	  @RetailerID int
	, @Search Varchar(1000)
	--Output Variable 
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
		
		 DECLARE @AppConfig varchar(50)
		 SELECT @AppConfig = ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='App Media Server Configuration'
				--RETRIEVE THE PRODUCT DETAILS
				SELECT DISTINCT P.ProductID as productID
					   ,P.ProductName as productName					 
					   , P.ProductShortDescription as shortDescription
					   , P.ScanCode as scanCode
					   , P.ProductLongDescription as longDescription
					   , RL.RetailID
					   , RL.RetailLocationID as retailerLocID
					   , P.ProductExpirationDate as prodExpirationDate
					   , productImagePath = CASE WHEN ProductImagePath IS NOT NULL AND WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),P.ManufacturerID)+'/'+  ProductImagePath
										ELSE ProductImagePath END
				FROM 				
				Product P 
				INNER JOIN RetailLocationProduct RLP ON RLP.ProductID = P.ProductID
				INNER JOIN RetailLocation RL ON RL.RetailLocationID = RLP.RetailLocationID			
				WHERE P.ProductID <> 0
				AND RL.RetailID = @RetailerID AND RL.Active = 1
				AND ( P.ProductName LIKE ( CASE WHEN @Search IS NOT NULL THEN '%'+@Search+'%' ELSE '%' END )
					  OR
					  P.ScanCode = @Search
					)
				
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerRebateAddProductSearch.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
		END;
		 
	END CATCH;
END;




GO
