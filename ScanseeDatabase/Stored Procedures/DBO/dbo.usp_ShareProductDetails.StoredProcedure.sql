USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_ShareProductDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name   : usp_ShareProductDetails  
Purpose                             : To fetch Product Details.  
Example                             : usp_ShareProductDetails 2,1   
  
History  
Version           Date              Author                  Change Description  
---------------------------------------------------------------   
1.0               26th July 2011    SPAN Infotech India      Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_ShareProductDetails]  
(  
        @ProductID int 
      --OutPut Variable  
      , @ErrorNumber int output  
      , @ErrorMessage varchar(1000) output   
)  
AS  
BEGIN  
  
      BEGIN TRY  
      
		  --To get Media Server Configuration.  
		  DECLARE @ManufConfig varchar(50)    
		  SELECT @ManufConfig=ScreenContent    
		  FROM AppConfiguration     
		  WHERE ConfigurationType='Web Manufacturer Media Server Configuration' 
      
       SELECT p.ProductID
       , p.ProductName shareProdName  
       , p.ManufacturerID
       , p.ModelNumber
       , p.ProductShortDescription
       , p.ProductLongDescription shareProdLongDesc
       , p.ProductExpirationDate productExpDate
       , p.ProductAddDate 
       , imagePath =  CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																THEN @ManufConfig
																+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																+ProductImagePath ELSE ProductImagePath 
														  END   
                          ELSE ProductImagePath END 
       , p.SuggestedRetailPrice
       , p.SKUNumber skuCode 
       , p.Weight productWeight
       , p.ScanCode barCode
       , p.WeightUnits
       ,M.ManufName manufacturersName 
       FROM Product p 
       LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID 
       WHERE ProductID = @ProductID

      END TRY  
        
      BEGIN CATCH  
        
            --Check whether the Transaction is uncommitable.  
            IF @@ERROR <> 0  
            BEGIN  
                  PRINT 'Error occured in Stored Procedure usp_ShareProductDetails.'           
                  --- Execute retrieval of Error info.  
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
            END;  
              
      END CATCH;  
END;

GO
