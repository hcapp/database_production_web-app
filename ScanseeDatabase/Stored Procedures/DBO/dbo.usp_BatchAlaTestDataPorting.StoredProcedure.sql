USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchAlaTestDataPorting]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_APIAlaTestDataPorting
Purpose					: To move data from stage table api ala test data to product reviews table.
Example					: usp_APIAlaTestDataPorting

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			10th OCT 2011	Naga Sandhys S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchAlaTestDataPorting]
(
	
	--Output Variable 
	  @Result int output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
				Declare @ApipartnerID INT
				SELECT @ApipartnerID= APIPartnerID from APIPartner where APIPartnerName='AlaTest'
		
		
				IF EXISTS (SELECT TOP 1 A.alaid FROM APIAlaTestData A INNER JOIN ProductReviews P ON P.sourceid=A.Alaid)
				BEGIN
							DELETE FROM ProductReviews 
							FROM ProductReviews P
							INNER JOIN APIAlaTestData A
							ON A.alaid=P.sourceid 
			    END
		
				IF NOT EXISTS(SELECT TOP 1 A.alaid FROM APIAlaTestData A INNER JOIN ProductReviews P ON P.sourceid=A.Alaid)
				BEGIN
				PRINT '1'
						INSERT INTO [ProductReviews]
								   ([ProductID]
								   ,[ReviewURL]
								   ,[ReviewComments]
								   ,[DateCreated]
								   ,[Datemodified]
								   ,[APIPartnerID]
								   ,[SourceID])
				        
						SELECT    [Productid]
								, [ReviewURL]
								, [TestSummary]
								, GETDATE()
								, NULL
								, @ApipartnerID 
								, AlaId
						FROM APIalatestdata A
						INNER JOIN Product P ON P.ScanCode = A.ScanCode OR P.ProductName=A.ProductName
						WHERE ReviewURL IS NOT NULL AND TestSummary IS NOT NULL
               END
 
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_APIAlaTestDataPorting.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;

GO
