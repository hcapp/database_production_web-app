USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerSpecialOfferCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerSpecialOfferCreation
Purpose					: To Create a Retailer Special Offers(Special Offer Page).
Example					: usp_WebRetailerSpecialOfferCreation

History
Version		Date						Author			Change Description
------------------------------------------------------------------------------- 
1.0			17th May 2012				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerSpecialOfferCreation]
(

	--Input Parameter(s)--
	
	--Common
	 @RetailID int	
   , @RetailLocationID varchar(MAX)
   , @SpecialOfferPageTitle varchar(255)
   , @Image varchar(100) 
   
   --Link to Existing.
   , @WebLink varchar(1000)
   , @ImageIconID int
   
   --Make your Own
   , @PageDescription varchar(Max)
   , @PageShortDescription varchar(max)
   , @PageLongDescription varchar(max) 
   , @StartDate varchar(10)
   , @EndDate varchar(10)
   , @StartTime varchar(5)
   , @EndTime varchar(5)
   , @UploadFileName varchar(500)   
   	
	--Output Variable--	  
	, @FindClearCacheURL varchar(1000) output	 
	, @PageID int output
	, @Status int output
    , @ErrorNumber int output
    , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION				
			
			DECLARE @QRRetailerCustomPageID INT
			DECLARE @ProductMediaTypeID int
			
			--Create the Special Offer Page & retrieve the page id for the QR code generation.
			INSERT INTO QRRetailerCustomPage(QRTypeID
											,RetailID
											,Pagetitle
											,StartDate
											,EndDate
											,Image
											,PageDescription
											,ShortDescription
											,LongDescription
											,QRRetailerCustomPageIconID
											,URL
											,DateCreated)
											
				                 SELECT (SELECT QRTypeID FROM QRTypes WHERE QRTypeName LIKE 'Special Offer Page')
									  , @RetailID
									  , @SpecialOfferPageTitle 
									  , @StartDate + ' '+ @StartTime 
									  , @EndDate+' '+@EndTime 
									  , @Image
									  , @PageDescription
									  , @PageShortDescription
									  , @PageLongDescription
									  , @ImageIconID
									  , @WebLink
									  , GETDATE()
									  
			SELECT @QRRetailerCustomPageID = SCOPE_IDENTITY()
			SELECT @PageID = @QRRetailerCustomPageID
			
			--If the Retailer Locations are not null.						  
			IF (@RetailLocationID IS NOT NULL)
			BEGIN
				INSERT INTO QRRetailerCustomPageAssociation(QRRetailerCustomPageID
														  , RetailID
														  , RetailLocationID
														  , DateCreated)
													SELECT @QRRetailerCustomPageID
														 , @RetailID
														 , Param
														 , GETDATE()
													FROM dbo.fn_SplitParam(@RetailLocationID, ',')
			END
			
			--This will have a value only if the Create own page is selected.
			IF (@UploadFileName IS NOT NULL)
			BEGIN
				INSERT INTO QRRetailerCustomPageMedia(QRRetailerCustomPageID
													, MediaTypeID
													, MediaPath
													, DateCreated)
										  SELECT @QRRetailerCustomPageID
										       ,(SELECT ProductMediaTypeID FROM ProductMediaType WHERE ProductMediaType LIKE 'Other Files')
										       , @UploadFileName
										       , GETDATE()														 
										
			END				  
			
			-------Find Clear Cache URL---29/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			-----------------------------------------------						  
		   
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerSpecialOfferCreation.'		
			--Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
