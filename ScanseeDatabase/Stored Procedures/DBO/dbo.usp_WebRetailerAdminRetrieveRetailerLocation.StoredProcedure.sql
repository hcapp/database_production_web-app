USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetrieveRetailerLocation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminRetrieveRetailerLocation
Purpose					: To retrieve list of Retailer's RetailLocations from the system.
Example					: usp_WebRetailerAdminRetrieveRetailerLocation

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			16th July 2013			Mohith H R		Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetrieveRetailerLocation]

	--Input parameters	 
	  @RetailID INT	
	, @LowerLimit int  
    , @ScreenName varchar(50)
    , @StoreIdentification VARCHAR(100)							     
	, @Address1 VARCHAR(250)

	--Output variables
	, @MaxCnt INT OUTPUT
	, @NxtPageFlag BIT OUTPUT 
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION	
		
		--To get the row count for pagination.  
		DECLARE @UpperLimit int   
		SELECT @UpperLimit = @LowerLimit + ScreenContent   
		FROM AppConfiguration   
		WHERE ScreenName = 'RetailAdmin' 
		AND ConfigurationType = 'Pagination'
		AND Active = 1
		
		 DECLARE @Config varchar(200)                     
         DECLARE @RetailConfig varchar(200)

		SELECT @Config=ScreenContent
        FROM AppConfiguration 
        WHERE ConfigurationType='App Media Server Configuration'

        SELECT @RetailConfig=ScreenContent
        FROM AppConfiguration 
        WHERE ConfigurationType='Web Retailer Media Server Configuration'


		IF @StoreIdentification =''
		BEGIN
		SET @StoreIdentification=NULL
		END
		
		IF @Address1  =''
		BEGIN
		SET @Address1 =NULL
		END
		
		CREATE TABLE #RetailerLocationList(Row_Num INT IDENTITY(1,1)
								  ,RetailLocationID INT
							      ,StoreIdentification VARCHAR(100)							     
							      ,Address1 VARCHAR(250)
							      ,[State] CHAR(2)							      
							      ,City VARCHAR(50)
							      ,PostalCode VARCHAR(10)
								  ,Latitude float
								  ,Longitude float
								  ,Phone varchar(50)
								  ,WebsiteURL varchar(1000)
								  ,RetailerImagePath Varchar(2000))
		

		IF (@StoreIdentification IS NOT NULL OR @Address1 IS NOT NULL)
		BEGIN
			INSERT INTO #RetailerLocationList(RetailLocationID
									,StoreIdentification							      										
									,Address1
									,[State]
									,City
									,PostalCode
									,Latitude  
									,Longitude  
									,Phone  
									,WebsiteURL
									,RetailerImagePath)
			 SELECT   DISTINCT   RL.RetailLocationID
					   ,StoreIdentification						   
					   ,RL.Address1
					   ,RL.[State]
					   ,RL.City
					   ,RL.PostalCode	
					   ,RetailLocationLatitude
					   ,RetailLocationLongitude
					   ,C.ContactPhone
					   ,RL.RetailLocationURL
					   , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
		
		
			FROM RetailLocation	RL
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID AND R.RetailerActive = 1	
			LEFT JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
			LEFT JOIN Contact C ON C.ContactID = RC.ContactID
			WHERE ((RL.RetailID = @RetailID AND @StoreIdentification IS NOT NULL AND StoreIdentification = @StoreIdentification AND @Address1 IS NOT NULL AND RL.Address1 LIKE '%'+@Address1+'%')		
			OR (RL.RetailID = @RetailID AND @StoreIdentification IS NOT NULL AND StoreIdentification = @StoreIdentification AND @Address1 IS NULL)
			OR (RL.RetailID = @RetailID AND @StoreIdentification IS NULL AND @Address1 IS NOT NULL AND RL.Address1 LIKE '%'+@Address1+'%'))
			--OR (RL.RetailID = @RetailID AND @StoreIdentification IS NULL AND @Address1 IS NULL))
			AND Headquarters = 0 AND RL.Active = 1
			ORDER BY StoreIdentification
		END
		ELSE
		BEGIN 
		
			INSERT INTO #RetailerLocationList(RetailLocationID
									,StoreIdentification							      										
									,Address1
									,[State]
									,City
									,PostalCode
									,Latitude  
									,Longitude  
									,Phone  
									,WebsiteURL
									,RetailerImagePath)
			 SELECT   DISTINCT   RL.RetailLocationID
					   ,StoreIdentification						   
					   ,RL.Address1
					   ,RL.[State]
					   ,RL.City
					   ,RL.PostalCode	
					   ,RetailLocationLatitude
					   ,RetailLocationLongitude
					   ,C.ContactPhone
					   ,RL.RetailLocationURL
					   , RetailerImagePath = IIF(RetailLocationImagePath IS NULL OR RetailLocationImagePath LIKE '',(IIF(RetailerImagePath IS NOT NULL,(IIF(R.WebsiteSourceFlag = 1,(@RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+RetailerImagePath),@Config+RetailerImagePath)),null)), @RetailConfig+CONVERT(VARCHAR(30),R.RetailID)+'/'+'locationlogo'+'/'+RetailLocationImagePath)						
		
		
			FROM RetailLocation	RL
			INNER JOIN Retailer R ON R.RetailID =RL.RetailID AND RetailerActive = 1	
			LEFT JOIN RetailContact RC ON RL.RetailLocationID = RC.RetailLocationID
			LEFT JOIN Contact C ON C.ContactID = RC.ContactID
			WHERE (RL.RetailID = @RetailID AND Headquarters = 0) AND RL.Active = 1
			ORDER BY StoreIdentification
		END
							
		--To capture max row number. 		 
		SELECT @MaxCnt = MAX(Row_Num) FROM #RetailerLocationList
 
		 --This flag is a indicator to enable "More" button in the UI.   
		 --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
		 SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END 	
		 
		 SELECT Row_Num row_num
			   ,RetailLocationID
			   ,StoreIdentification						   
			   ,Address1
			   ,[State]
			   ,City	
			   ,PostalCode
			   ,Latitude  
			   ,Longitude  
			   ,Phone  
			   ,WebsiteURL = IIF(WebsiteURL = '', NULL, WebsiteURL)
			   ,RetailerImagePath gridImgLocationPath
		 FROM #RetailerLocationList								    
		 WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit 
		 ORDER BY StoreIdentification
		 
		SELECT @Status = 0
		COMMIT TRANSACTION
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetrieveRetailerLocation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
