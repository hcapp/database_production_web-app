USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDeleteRetailerPage]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebDeleteretailerPage
Purpose					: 
Example					: usp_WebDeleteretailerPage

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			8th June 2012 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/
 
CREATE PROCEDURE [dbo].[usp_WebDeleteRetailerPage]
(
	
	--Input Variables.
	
	  @PageID int
	  
	--Output Variable 
	
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			--Delete from child tables.
			DELETE FROM QRRetailerCustomPageAssociation
			WHERE QRRetailerCustomPageID = @PageID
		
			DELETE FROM QRRetailerCustomPageMedia
			WHERE QRRetailerCustomPageID = @PageID
			
			--Delete from Parent Table.
			DELETE FROM QRRetailerCustomPage
			WHERE QRRetailerCustomPageID = @PageID
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebDeleteretailerPage.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;




GO
