USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebDisplayRetailerLocationsHoursFilter]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_WebDisplayRetailerLocationsHoursFilter]
Purpose					: 
Example					: [usp_WebDisplayRetailerLocationsHoursFilter] 
..
History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25/11/2016		Bindu T A		Hours - Filters Changes- Day Wise
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebDisplayRetailerLocationsHoursFilter]
(
	 @RetailLocationID int

	--Output Variable 
	, @TimeZone int output
	, @Status bit output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 

)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			SELECT @TimeZone = TimeZoneID FROM RetailLocation WHERE RetailLocationId = @RetailLocationID
			
			IF EXISTS (SELECT 1 FROM HcHoursFilterTime WHERE RetailLocationID = @RetailLocationID)
			BEGIN
				SELECT W.HcDaysOfWeekName,
				CASE WHEN CAST(H.StartTime AS TIME) IS NULL THEN '00:00'
					ELSE CAST(H.StartTime AS TIME) END StartTime,
				CASE WHEN CAST(H.EndTime AS TIME) IS NULL THEN '00:00'
					ELSE CAST(H.EndTime AS TIME) END EndTime
				FROM HcHoursFilterTime H
				INNER JOIN HcDaysOfWeek W ON W.HcDaysOfWeekID = H.HcDaysOfWeekID
				WHERE RetailLocationID= @RetailLocationID
			END
			ELSE 
			BEGIN 
				SELECT HcDaysOfWeekName,
				'00:00' StartTime,
				'00:00' EndTime
				FROM HcDaysOfWeek
			END


		--Confirmation of Success.
			SELECT @Status = 0
			
		COMMIT TRANSACTION

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_WebDisplayRetailerLocationsHoursFilter].'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;		
GO
