USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAds]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerAds
Purpose					: 
Example					: usp_WebRetailerAds

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAds]
(
	  @RetailID int
	 ,@AdNameSearch Varchar(100)
     ,@LowerLimit int

	
	--Output Variable--
	 , @RowCount INT output
	 , @NextPageFlag bit output	
	 , @ErrorNumber int output
	 , @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	
		--To get Media Server Configuration.
		 DECLARE @Config varchar(50)  
		 SELECT @Config=ScreenContent  
		 FROM AppConfiguration   
		 WHERE ConfigurationType='Web Retailer Media Server Configuration' 
		 
		 
		DECLARE @LATITUDE FLOAT
		DECLARE @LONGITUDE FLOAT
		DECLARE @UpperLimit INT
		DECLARE @MaxCnt INT
		
		--To get the row count for pagination.  
		 DECLARE @ScreenContent Varchar(100)
		 SELECT @ScreenContent = ScreenContent   
		 FROM AppConfiguration   
		 WHERE ScreenName = 'All' 
			AND ConfigurationType = 'Website Pagination'
			AND Active = 1  
   
    SELECT @UpperLimit = @LowerLimit + @ScreenContent
		
		SELECT  RowNum = ROW_NUMBER() OVER (ORDER BY advertisementName ASC)
		      , RetailLocationAdvertisementID
		      , advertisementName
			  , BannerAdImagePath
			  , RibbonAdImagePath
			  , RibbonAdURL
			  , advertisementDate
			  , advertisementEndDate
			  ,CreatedBy
		INTO #Temp
		FROM 
		(SELECT 
		        AdName as advertisementName
		       ,R.RetailLocationAdvertisementID
			   ,BannerAdImagePath = CASE WHEN BannerAdImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),RL.RetailID) +'/'+ BannerAdImagePath ELSE BannerAdImagePath END  
							        ELSE BannerAdImagePath
						            END
			   ,RibbonAdImagePath = CASE WHEN RibbonAdImagePath IS NOT NULL THEN CASE WHEN R.WebsiteSourceFlag = 1 THEN @Config + CONVERT(VARCHAR(30),RL.RetailID) +'/'+RibbonAdImagePath ELSE RibbonAdImagePath END  
							        ELSE BannerAdImagePath
						            END
			   ,RibbonAdURL
			   ,CONVERT(VARCHAR(10), AdStartDate, 105) as advertisementDate
			   ,CONVERT(VARCHAR(10), AdEndDate, 105) as advertisementEndDate
			   ,CreatedBy=FirstName+' '+Lastname
		FROM RetailLocationAdvertisement R
		INNER JOIN RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID AND RL.Active = 1
		INNER JOIN UserRetailer UR ON UR.RetailID=RL.RetailID
		INNER JOIN Users U ON U.UserID=UR.UserID
		WHERE RL.RetailID=@RetailID
				AND AdName LIKE (CASE WHEN ISNULL(@AdNameSearch,'')='' THEN '%' ELSE '%'+@AdNameSearch+'%' END))Ads
				
	    --Find out the total number of records in the result set.
	SELECT @MaxCnt = MAX(RowNum) FROM #TEMP
	SET @RowCount = @MaxCnt
	SELECT @NextPageFlag = CASE WHEN (@MaxCnt - @UpperLimit)> 0 THEN 1 ELSE 0 END    --CHECK IF THERE ARE STILL MORE RECORDS
	
	SELECT RowNum
	     , RetailLocationAdvertisementID
	     , advertisementName
		 , BannerAdImagePath strBannerAdImagePath
		 , RibbonAdImagePath strRibbonAdImagePath
		 , RibbonAdURL
		 , advertisementDate = CAST(advertisementDate AS VARCHAR(100)) + ' 00:00:00'
		 , advertisementEndDate = CAST(advertisementEndDate AS VARCHAR(100)) + ' 00:00:00'
		 , CreatedBy
	FROM #Temp
	WHERE RowNum BETWEEN (@LowerLimit + 1) AND @UpperLimit
	
	IF @RowCount IS NULL									--TO CHECK IF THE RESULT SET IS EMPTY
				SET @RowCount = 0	 
	
	
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAds.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;




GO
