USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierRebateReRun]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierRebateReRun
Purpose					: To Re Run a Rebate by the Supplier.
Example					: usp_WebSupplierRebateReRun

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			30th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierRebateReRun]
(

	--Input Parameter(s)--
	
	--Insert into Rebates table	
	  
	  @RebateID int
	, @SupplierID int  
	, @RebateName varchar(100)
    , @RebateAmount money
    , @RebateDescription varchar(255)	
    , @RebateTermsAndCondition varchar(1000)
    , @RebateStartDate varchar(100)
    , @RebateEndDate varchar(100)
    , @RebateStartTime varchar(100)
    , @RebateEndTime varchar(100)
    , @RebateTimeZoneID int 
    
    -- Insert into RebateRetailer Table
    , @RetailerID int
    , @RetailerLocationID int
    
    --Insert into RebateProduct Table
    , @ProductID int
	
	--Output Variable--
	  
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
		
		--Insert into Rebates Table
		
		INSERT INTO Rebate (
							  RebateName
							, RebateAmount
							, RebateShortDescription
							, RebateTermsConditions
							, RebateStartDate
							, RebateEndDate							
							, RebateCreatorID
							, RebateTimeZoneID
							)
					VALUES (
							  @RebateName
							, @RebateAmount
							, @RebateDescription
							, @RebateTermsAndCondition
							--, CAST(@RebateStartDate AS DATETIME) + CAST(@RebateStartTime AS TIME)  -- Time and Date are casted and concatenated to insert into a datetime column
							--, CAST(@RebateEndDate AS DATETIME) +  CAST(@RebateEndTime AS TIME)	   -- Time and Date are casted and concatenated to insert into a datetime column					
							,CONVERT(DATETIME, CONVERT(CHAR(8), @RebateStartDate, 112) + ' ' + CONVERT(CHAR(8), @RebateStartTime , 108))
							,CONVERT(DATETIME, CONVERT(CHAR(8), @RebateEndDate, 112) + ' ' + CONVERT(CHAR(8), @RebateEndDate , 108))
							, @SupplierID
							, @RebateTimeZoneID
						   )
						   


		DECLARE @NewRebateID INT		
								   
       --Insert into Rebate Retailer Table		
       
       SET @NewRebateID = SCOPE_IDENTITY()
       
       INSERT INTO RebateRetailer (
									RebateID
								  , RetailID
								  , RetailLocationID
								  , DateCreated								  	
                                  )
                         VALUES
								(
									@NewRebateID
								  , @RetailerID
								  , @RetailerLocationID
								  , GETDATE()	
								)		
								
       --Insert into RebateProduct table
       
       INSERT INTO RebateProduct(
									RebateID
								  , ProductID
								  , DateAdded
								)			
				   SELECT @NewRebateID
				        , ProductID
				        , GETDATE()
				   FROM RebateProduct
				   WHERE RebateID = @RebateID						      							   
			
		--Confirmation of Success.
		
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebCouponDelete.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
