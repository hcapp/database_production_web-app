USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_FetchRetailerList]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name : usp_FetchRetailerList  
Purpose     : To list retailers with in the specified radius  
Example     : EXEC usp_FetchRetailerList 2, 12.947150, 77.578880, 560102, 10  
  
History  
Version  Date  Author   Change Description  
---------------------------------------------------------------   
1.0   5/28/2011 SPAN Infotech India Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_FetchRetailerList]  
(  
   @UserID int  
 , @Latitude decimal(18,6)  
 , @Longitude decimal(18,6)  
 , @ZipCode varchar(10)  
 , @Radius int  
 
 , @LowerLimit int
 , @UpperLimit int
 
 --OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
        DECLARE @Config varchar(50)
		SELECT @Config=ScreenContent
		FROM AppConfiguration 
		WHERE ConfigurationType='Configuration of server'
		
   --While User search by Zipcode, coordinates are fetched from GeoPosition table.  
   IF (@Latitude IS NULL AND @Longitude IS NULL )  
   BEGIN  
    SELECT @Latitude = Latitude   
      , @Longitude = Longitude   
    FROM GeoPosition   
    WHERE PostalCode = @ZipCode   
   END  
   --If radius is not passed, getting user preferred radius from UserPreference table.  
   IF @Radius IS NULL   
   BEGIN   
    SELECT @Radius = LocaleRadius  
    FROM dbo.UserPreference WHERE UserID = @UserID  
   END   
   --If Coordinates exists fetching retailer list in the specified radius.  
   IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL)  
   BEGIN  
   ; WITH Retail
   AS
   (
		SELECT Row_Num = ROW_NUMBER() OVER(ORDER BY RetailName)
		  , RetailID  
		  , RetailName  
		  , RetailerImagePath  
		  , Distance    
		  , BannerAdImagePath  
		  , RibbonAdImagePath  
		  , RibbonAdURL  
		  , RetailLocationAdvertisementID 
		FROM   
		 (SELECT R.RetailID   
		   , R.RetailName   
		   , @Config + R.RetailerImagePath  RetailerImagePath 
		   , Distance = (ACOS((SIN(RL.RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RL.RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RL.RetailLocationLongitude / 57.2958))))*6371) * 0.6214  
		   , @Config + RA.BannerAdImagePath  BannerAdImagePath
		   , @Config + RA.RibbonAdImagePath  RibbonAdImagePath
		   , RA.RibbonAdURL  
		   , RA.RetailLocationAdvertisementID  
		  FROM Retailer R  
		   INNER JOIN RetailLocation RL ON RL.RetailID = R.RetailID  
		   -- added below join to get the Retailer advertisement (Manju)  
		   LEFT OUTER JOIN RetailLocationAdvertisement RA ON RA.RetailLocationID = RL.RetailLocationID 
															AND GETDATE() BETWEEN  RA.AdStartDate AND RA.AdEndDate 
		 )Retailer  
		WHERE Distance <= @Radius  
	)
	SELECT Row_Num
		  , RetailID retailerId  
		  , RetailName retailerName  
		  , RetailerImagePath imagePath   
		  , Distance    
		  , BannerAdImagePath bannerAdImagePath  
		  , RibbonAdImagePath ribbonAdImagePath  
		  , RibbonAdURL ribbonAdURL  
		  , RetailLocationAdvertisementID advertisementID   
	FROM Retail
	WHERE Row_Num BETWEEN (@LowerLimit+1) AND (@UpperLimit)	
	ORDER BY RetailName
	
   END  
     
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
		PRINT 'Error occured in Stored Procedure usp_FetchRetailerList.'    
		--- Execute retrieval of Error info.
		EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
  END;  
     
 END CATCH;  
END;

GO
