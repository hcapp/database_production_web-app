USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierProfileDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierProfileDisplay
Purpose					: To Update the profile of the Existing Suppliers/ Manufacturers.
Example					: usp_WebSupplierProfileDisplay

History
Version		Date							Author			Change Description
------------------------------------------------------------------------------- 
1.0			15th December 2011				Pavan Sharma K	Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierProfileDisplay]
(

	--Input Parameter(s)--
	  
	  @ManufacturerID  int	 
    
	  
	
	--Output Variable--
	  
	
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
	DECLARE @BusinessCategoryIDs VARCHAR(1000)
	
	SELECT @BusinessCategoryIDs = COALESCE(@BusinessCategoryIDs+',','')+CAST(CategoryID AS VARCHAR(10)) FROM ManufacturerCategory WHERE ManufacturerID = @ManufacturerID
		
		SELECT M.ManufName as companyName
		     , M.Address1 as corporateAddress
		     , M.Address2 as 'address'
		     , M.State as 'state'
		     , M.City as city
		     , M.PostalCode as postalCode
		     , M.CorporatePhoneNo as phone
		     , C.ContactFirstName as contactFName
		     , C.ContactLastname as contactLName
		     , C.ContactEmail as email
		     , C.ContactPhone as contactPhone
		     , @BusinessCategoryIDs as bCategory
		     , M.IsNonProfitOrganisation as nonProfit
		FROM Manufacturer M 
		INNER JOIN ManufacturerContact MC ON M.ManufacturerID = MC.ManufacturerID
		INNER JOIN Contact C ON MC.ContactID = C.ContactID		
		WHERE M.ManufacturerID = @ManufacturerID
		
	
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierProfileDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 			
		END;
		 
	END CATCH;
END;


GO
