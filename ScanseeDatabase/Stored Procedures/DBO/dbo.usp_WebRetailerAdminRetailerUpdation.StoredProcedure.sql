USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminRetailerUpdation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerAdminRetailerUpdation
Purpose					: 
Example					: usp_WebRetailerAdminRetailerUpdation

History
Version		Date				Author			Change Description
--------------------------------------------------------------- 
1.0			26th March 2014		Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerAdminRetailerUpdation]
(
	--Input parameters
      @UserID int
	, @RetailID int    
    , @RetailName varchar(100)
	, @RetailURL varchar(1000)
	, @Address varchar(1000)
	, @City varchar(50)
	, @State varchar(50)
	, @PosatalCode varchar(5)
	, @Latitude float
	, @Longitude float
	, @CorporatePhoneNo varchar(10)
	
	--Output Variable 	
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			
			DECLARE @RetailLocationID INT

			SELECT @RetailLocationID = RetailLocationID
			FROM RetailLocation
			WHERE RetailID = @RetailID
			AND CorporateAndStore = 1 AND Active = 1

			DECLARE @RetailAssociatedFlag Int
			DECLARE @RetailLocAssociatedFlag Int
				

		    SET @RetailAssociatedFlag=0
			SET @RetailLocAssociatedFlag =0
			DECLARE @RetailLocID Int

			--To update retailer information.
			UPDATE Retailer SET RetailName = @RetailName
							  , RetailURL = @RetailURL
							  , Address1 = @Address
							  , City = @City
							  , State = @State
							  , PostalCode = @PosatalCode
							  , CorporatePhoneNo = @CorporatePhoneNo
							  , ModifyUserID = @UserID
							  , DateModified = GETDATE()
			WHERE RetailID = @RetailID AND RetailerActive = 1

			--To update retail location information.
			IF (@PosatalCode<>(Select PostalCode From RetailLocation  WHERE RetailID=@RetailID AND Active = 1
					                                                          AND (Headquarters = 1 OR CorporateAndStore = 1)))
					  BEGIN
					      SET @RetailLocAssociatedFlag =1
						  SET @RetailLocID=(Select RetailLocationID From RetailLocation  WHERE RetailID=@RetailID AND Active = 1 
					                                                          AND (Headquarters = 1 OR CorporateAndStore = 1))
					  END	

			UPDATE RetailLocation SET Address1 = @Address
									, City = @City
									, State = @State
									, PostalCode = @PosatalCode
									, RetailLocationLatitude = @Latitude
									, RetailLocationLongitude = @Longitude
									, DateModified = GETDATE()
			WHERE RetailID = @RetailID AND (CorporateAndStore = 1 OR Headquarters = 1) AND Active = 1

			--Refresh the association w.r.t the Retail location
				--DELETE FROM HcRetailerAssociation 
				--FROM HcRetailerAssociation A
				--INNER JOIN RetailLocation RL ON RL.RetailLocationID = A.RetailLocationID
				--WHERE RL.RetailLocationID = @RetailLocationID
				--AND RL.Headquarters = 0 AND A.Associated = 1

				--Associate the retailer to the Hub Citi.
				--INSERT INTO HcRetailerAssociation(HcHubCitiID
				--							,RetailID
				--							,RetailLocationID
				--							,DateCreated
				--							,CreatedUserID
				--							,Associated) 
				--			SELECT DISTINCT LA.HcHubCitiID 
				--								,@RetailID
				--								,@RetailLocationID
				--								,GETDATE()
				--								,@UserID 
				--								,0
				--			FROM HcLocationAssociation LA
				--			INNER JOIN RetailLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode 
				--			LEFT JOIN HcRetailerAssociation RA ON RA.RetailLocationID = @RetailLocationID AND LA.HcHubCitiID = RA.HcHubCitiID 
				--			WHERE RA.HcRetailerAssociationID IS NULL
				--			AND RL.RetailLocationID = @RetailLocationID
			 --    			AND RL.Headquarters = 0 
		
		      IF (@RetailLocAssociatedFlag  =1)
			  BEGIN
					UPDATE HcRetailerAssociation set Associated =0 Where RetailLocationID =@RetailLocID  					  
			  END

			-------Find Clear Cache URL---4/4/2016--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

			SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
			SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
			SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersion'

			SELECT @FindClearCacheURL= @YetToReleaseURL+screencontent+','+@CurrentURL+Screencontent +','+@SupportURL+Screencontent
			FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'
					  
			------------------------------------------------
		    
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminRetailerUpdation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;






GO
