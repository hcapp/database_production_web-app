USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_BatchRssFeedEmailNotification]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_BatchRssFeedEmailNotification
Purpose					: To send email notification if categories are without any news in it. 
Example					: usp_BatchRssFeedPushNotification 

Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			15 July 2015	Mohith H R		Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_BatchRssFeedEmailNotification]
(
	  @HcHubCitiID varchar(100)

	--Output Variable 		
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			BEGIN
				
				SELECT Param HubCitiIDs
				INTO #HubCitiIDs
				FROM fn_SplitParam (@HcHubCitiID,',')

				SELECT DISTINCT Id = IDENTITY (int,1,1)
							   ,FSC.CategoryName feedType
							   ,IIF(Message IS NULL,'Currently no news for '+ FSC.CategoryName +' category.',Message) Message
							   ,H.HubCitiName
				INTO #NoNews
				FROM RssFeedStaticCategory FSC
				LEFT JOIN RssFeedNewsStagingTable FN ON FSC.CategoryName = FN.NewsType
				LEFT JOIN HcHubCiti H ON FSC.HcHubCitiID = H.HcHubCitiID
				WHERE(FN.RssFeedNewsID IS NULL OR Message IS NOT NULL) AND ((@HcHubCitiID IS NULL AND 1=1) OR (H.HcHubCitiID IN (SELECT HubCitiIDs FROM #HubCitiIDs)))						 
				ORDER BY HubCitiName
				
				SELECT * 
				FROM #NoNews
				ORDER BY Id
						
				--Confirmation of Success.
				SELECT @Status = 0
			END
			
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_BatchRssFeedEmailNotification'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
