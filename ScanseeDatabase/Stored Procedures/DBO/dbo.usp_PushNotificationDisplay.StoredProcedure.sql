USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_PushNotificationDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_PushNotificationDisplay
Purpose					: Whether to show Push Notification to user.
Example					: usp_PushNotificationDisplay 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			1st July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_PushNotificationDisplay]
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		-- If PushNotify is null then Push Notification is not shown to user, so returning 1.
		SELECT PushNotifyDisplay = CASE WHEN PushNotify IS NULL THEN 1 ELSE 0 END
		FROM Users 
		WHERE UserID = @UserID 
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_PushNotificationDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			
		END;
		 
	END CATCH;
END;

GO
