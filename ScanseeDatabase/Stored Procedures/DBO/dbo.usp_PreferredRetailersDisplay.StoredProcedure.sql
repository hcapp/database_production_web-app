USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_PreferredRetailersDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_PreferredRetailersDisplay
Purpose					: To display User'd Preferred retailers.
Example					: usp_PreferredRetailersDisplay 2

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			4th July 2011	SPAN Infotech India	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_PreferredRetailersDisplay] 
(
	@UserID int
	
	--OutPut Variable
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		--To fetch Retailer's Info 
		SELECT R.RetailID retailerId
			, R.RetailName retailerName
			
			, displayed = CASE WHEN UR.RetailID IS NOT NULL THEN 1 ELSE 0 END
		FROM Retailer R
			LEFT JOIN UserRetailPreference UR ON UR.RetailID = R.RetailID AND UR.UserID = @UserID AND UR.Active = 1
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_PreferredRetailersDisplay.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 	 
		END;
		 
	END CATCH;
END;

GO
