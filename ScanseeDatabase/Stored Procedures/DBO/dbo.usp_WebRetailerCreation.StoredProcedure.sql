USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerCreation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebRetailerCreation
Purpose					: To register New Retailer. 
Example					: 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			13th Dec 2011	Naga Sandhya S	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebRetailerCreation]
(

	 @RetailerName Varchar(100)
	,@Address1 Varchar(100)
	,@Address2 Varchar(50)
	,@State Char(2)
	,@City Varchar(50)
	,@PostalCode Varchar(10)
	,@CorporatePhoneNo Char(10)
	,@BusinessCategory varchar(1000)
	,@NonProfitStatus bit
	,@RetailerURL varchar(1000)
	,@NumberOfLocations int
	,@RetailerKeyword Varchar(1000)
	,@RetailerLatitude Float
    ,@RetailerLongitude Float

	--Input Variables of RetailContact
	,@ContactFirstName Varchar(20)
	,@ContactLastName Varchar(30)
	,@ContactPhone Char(10)

	--Input Variables of Users
	,@UserName varchar(100)
	,@ContactEmail Varchar(100)
	,@Password Varchar(60)

	--Input Variables of UserRetailer
	,@AdminFlag Bit
	,@CorporateAndSore BIT
	,@StoreIdentification VARCHAR(20)
	
	--For Duplicate Retailer
	,@DuplicateRetailerID int	
	,@DuplicateRetailerLocationID int
	,@FlagRetailerLocationIDs varchar(max)	

	--For Retailer Filter
	,@AdminFilterID varchar(max)
	,@AdminFilterValueID varchar(max)

	--For Sub-Category
	,@SubCategoryID varchar(max)
	
	--Output Variable 
	, @ResponseRetailID int output
	, @DuplicateFlag bit output
	, @DuplicateRetailerFlag bit output
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output 
)
AS
BEGIN
      
	BEGIN TRY
		BEGIN TRANSACTION
		
		----To fetch Retailer filter Categories
		--DECLARE	@FilterBusinessCategory varchar(1000) 

		--SELECT P.Param
		--INTO #FilterCategory
		--FROM BusinessCategory BC
		--INNER JOIN fn_SplitParam(@BusinessCategory,',') P ON BC.BusinessCategoryID = P.Param
		--WHERE BusinessCategoryName in ('Bars','Dining') AND @AdminFilterID IS NOT NULL

		IF NOT EXISTS(SELECT 1 FROM Retailer R
						  INNER JOIN UserRetailer UR ON R.RetailID = UR.RetailID
						  --INNER JOIN RetailerBillingDetails RB ON R.RetailID = RB.RetailerID
						  WHERE RetailName = @RetailerName AND R.Address1 = @Address1  
						  AND CorporatePhoneNo = @CorporatePhoneNo AND R.City = @City AND R.[State] = @State
						  AND R.RetailerActive = 1)
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM Users WHERE UserName = @UserName)
			BEGIN
		
		    --INSERT INTO USERS TABLE
			INSERT INTO Users
						(UserName
						,Password
						,DateCreated
						,UsertypeID)
			VALUES( @UserName
					,@Password
					,GETDATE()
					,(Select UserTypeID From Usertype WHERE UserType like 'ScanSee Retailer'))
					
			DECLARE @UserID int
			SET @UserID=SCOPE_IDENTITY();
		
			--INSERT INTO RETAILER TABLE
		 
			INSERT INTO [Retailer]
					   ([RetailName]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[CountryID]
					   ,[RetailURL]
					   ,[NumberOfLocations]
					   ,[CreateUserID]
					   ,[DateCreated]
					   ,[CorporatePhoneNo]
					   ,[IsNonProfitOrganisation]
					   , WebsiteSourceFlag)
			VALUES( @RetailerName
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   ,1
				   ,@RetailerURL
				   ,@NumberOfLocations
				   ,@UserID
				   ,GETDATE()
				   ,@CorporatePhoneNo
				   ,@NonProfitStatus
				   , 1)			   
				   
				   
			DECLARE @RetailID int
			SET @RetailID=SCOPE_IDENTITY(); 

			--To update City state changes
			update Retailer 
			set HccityID = C.HccityID
			FROM Retailer R
			inner join HcCity C on R.City = C.CityName
			where Retailid = @RetailID

            update Retailer 
			set StateID = C.StateID
			FROM Retailer R
			inner join state C on R.State = C.Stateabbrevation
			where Retailid = @RetailID
			
		   --Retailer Business and SubCategory Association
		   --INSERT INTO RetailerBusinessCategory(RetailerID
		   --                         , BusinessCategoryID
		   --                         , DateCreated)
		   --                    SELECT @RetailID
		   --                         , [Param]
		   --                         , GETDATE()
		                            
		   --                    FROM dbo.fn_SplitParam(@BusinessCategory, ',')

			 SELECT @SubCategoryID = REPLACE(@SubCategoryID, 'NULL', '0')  

			--Capture sub-CategoryIDs
			SELECT RowNum = IDENTITY(INT, 1, 1)
				 ,BusCategoryID = Param
			INTO #BusCatIds
			FROM dbo.fn_SplitParamMultiDelimiter(@BusinessCategory, ',')                     
	
			SELECT RowNum = IDENTITY(INT, 1, 1)
				, SubCategoryID = Param
			INTO #SubCatIds
			FROM dbo.fn_SplitParamMultiDelimiter(@SubCategoryID, '!~~!')  

			SELECT RowNum = IDENTITY(INT, 1, 1)
					,B.BusCategoryID
					,SubcatID =  F.Param 
			INTO #Buscat_SubCat 
			FROM #BusCatIds B
			INNER JOIN #SubCatIds S ON B.RowNum = S.RowNum 
			CROSS APPLY (select Param from fn_SplitParam(S.SubCategoryID,'|') ) F
				

			--Insert into RetailerBusinessCategory table.

			IF CURSOR_STATUS('global','CategoryList')>=-1
			BEGIN
			DEALLOCATE CategoryList
			END
                                                
			DECLARE @BusCategoryID1 int
			DECLARE @SubcatID1 int
			DECLARE CategoryList CURSOR STATIC FOR
			SELECT BusCategoryID
				  ,SubcatID
			FROM #Buscat_SubCat 
                           
			OPEN CategoryList
                           
			FETCH NEXT FROM CategoryList INTO @BusCategoryID1, @SubcatID1 
                           
			CREATE TABLE #TempBusCat(BusinnessCategoryID INT)

			WHILE @@FETCH_STATUS = 0
			BEGIN   
                            
				INSERT INTO #TempBusCat(BusinnessCategoryID) 			                                
				OUTPUT inserted.BusinnessCategoryID INTO #TempBusCat(BusinnessCategoryID)
							SELECT F.[Param]						     
				FROM dbo.fn_SplitParam(@BusCategoryID1, ',') F

				INSERT INTO RetailerBusinessCategory(RetailerID
												, BusinessCategoryID
												, DateCreated
												, BusinessSubCategoryID)
					OUTPUT inserted.BusinessCategoryID INTO #TempBusCat(BusinnessCategoryID)
											SELECT @RetailID
												, IIF(@BusCategoryID1= 0,NULL, @BusCategoryID1)
												, GETDATE()
												, IIF([Param] = 0,NULL,[Param])
											FROM dbo.fn_SplitParam(@SubcatID1, '|')

					
							DECLARE @Cnts INT 
							SELECT @Cnts = 0
							SELECT @Cnts = COUNT(Param)
							FROM dbo.fn_SplitParam(@SubcatID1, '|')
                                   
                               
				TRUNCATE TABLE #TempBusCat
		
				FETCH NEXT FROM CategoryList INTO @BusCategoryID1, @SubcatID1
                                  
			END                        

			CLOSE CategoryList
			DEALLOCATE CategoryList

		                      
			--INSERT INTO RETAILLOCATION TABLE
			
			INSERT INTO [RetailLocation]
					   ([RetailID]
					   ,[Headquarters]
					   ,[Address1]
					   ,[Address2]
					   ,[City]
					   ,[State]
					   ,[PostalCode]
					   ,[RetailLocationURL]
					   ,[RetailLocationLatitude]
					   ,[RetailLocationLongitude]
					   ,[CountryID]
					   ,[CorporateAndStore]
					   ,[StoreIdentification]
					   ,[DateCreated])  
			VALUES(@RetailID
				   ,CASE WHEN @CorporateAndSore = 1 THEN 0 ELSE 1 END
				   ,@Address1
				   ,@Address2
				   ,@City
				   ,@State
				   ,@PostalCode
				   , CASE WHEN @CorporateAndSore = 1 THEN @RetailerURL ELSE NULL END
				   , CASE WHEN @CorporateAndSore = 1 AND @RetailerLatitude = 0 OR @RetailerLatitude IS NULL THEN (SELECT Latitude FROM GeoPosition WHERE PostalCode = @PostalCode) ELSE @RetailerLatitude END
                   , CASE WHEN @CorporateAndSore = 1 AND @RetailerLongitude = 0 OR @RetailerLongitude IS NULL THEN (SELECT Longitude FROM GeoPosition WHERE PostalCode = @PostalCode) ELSE @RetailerLongitude END
                   ,1
				   ,@CorporateAndSore
				   ,@StoreIdentification
				   ,GETDATE())
				   
			DECLARE @RetailLocationID int
			SET @RetailLocationID=SCOPE_IDENTITY();


			
			--To update City state changes
			UPDATE RetailLocation 
			SET HccityID = C.HccityID
			FROM RetailLocation R
			INNER JOIN HcCity C on R.City = C.CityName
			WHERE Retaillocationid = @RetailLocationID

            UPDATE RetailLocation 
			SET StateID = C.StateID
			FROM RetailLocation R
			INNER JOIN state C on R.State = C.Stateabbrevation
			WHERE Retaillocationid = @RetailLocationID

			
			--INSERT INTO CONTACT TABLE
							   
			INSERT INTO [Contact]
					   ([ContactFirstName]
					   ,[ContactLastname]
					   ,[ContactTitle]					  
					   ,[ContactPhone]
					   ,[ContactEmail]
					   ,[DateCreated]
					   ,[CreateUserID])
			VALUES(@ContactFirstName
				   ,@ContactLastName
				   ,1				 
				   ,@ContactPhone
				   ,@ContactEmail
				   ,GETDATE()
				   ,@UserID)
				   
			DECLARE @ContactID int
			SET @ContactID=SCOPE_IDENTITY();
				   
			--INSERT INTO RETAILCONTACT TABLE				   
			INSERT INTO [RetailContact]
					   ([ContactID]
					   ,[RetailLocationID])
			VALUES(@ContactID
				   ,@RetailLocationID)
				   
			--INSERT INTO USERRETAILER CHILD TABLE				   
			INSERT INTO UserRetailer
					   ([RetailID]
					   ,[UserID]
					   ,[AdminFlag])
			VALUES(@RetailID
				   ,@UserID
				   ,@AdminFlag)
										   
         
           --INSERT INTO RETAILERKEYWORDS TABLE
           INSERT INTO RetailerKeywords 
				   (RetailID
				   , RetailLocationID
				    ,RetailKeyword
					,DateCreated)
		   VALUES(@RetailID
				  ,@RetailLocationID
		          ,@RetailerKeyword 
		          ,GETDATE())
		          
		          
			UPDATE Retailer SET DuplicateRetailerID=@DuplicateRetailerID,DuplicateRetailLocationID=@DuplicateRetailerLocationID
			WHERE RetailID=@RetailID AND RetailerActive = 1

			UPDATE RetailLocation SET Active = 0 WHERE RetailLocationID = @DuplicateRetailerLocationID AND Active = 1
			
			--UPDATE RetailLocation SET DuplicateFlag = 1,FreeAppRetailerID=@RetailID
			--WHERE RetailLocationID=@DuplicateRetailerLocationID
			
			UPDATE RetailLocation SET DuplicateFlag = 1,FreeAppRetailerID=@RetailID
			FROM RetailLocation RL
			INNER JOIN dbo.fn_SplitParam(@FlagRetailerLocationIDs,',') F ON RL.RetailLocationID = F.Param AND RL.Active = 1	
			
			--Insert into HcRetailerAssociation table.			
			INSERT INTO HcRetailerAssociation(HcHubCitiID
												,RetailID
												,RetailLocationID
												,DateCreated
												,CreatedUserID
												,Associated) 
								SELECT DISTINCT HcHubcitiID 
												  ,@RetailID
												  ,@RetailLocationID
												  ,GETDATE()
												  ,@UserID 
												  ,0
								FROM HcLocationAssociation LA
								INNER JOIN RetailLocation RL ON LA.HcCityID = RL.HcCityID AND LA.StateID = RL.StateID AND LA.PostalCode = RL.PostalCode 
								WHERE RL.RetailLocationID = @RetailLocationID
			     				AND RL.Headquarters = 0 AND RL.Active = 1
			
			----Retailer filter changes
			--IF (SELECT DISTINCT 1 FROM #FilterCategory) > 0
			--BEGIN

			--SELECT @FilterBusinessCategory = COALESCE(@FilterBusinessCategory + '!~~!' , '') + CAST(Param AS varchar(100))
			--FROM  #FilterCategory

			--SELECT @AdminFilterID = REPLACE(@AdminFilterID, 'NULL', '0')  
			--SELECT @AdminFilterValueID = REPLACE(@AdminFilterValueID, 'NULL', '0')

			----Capture Category and filter ID's
			--SELECT RowNum = IDENTITY(INT, 1, 1)
			--		,CategoryID = Param
			--INTO #CategoryID
			--FROM dbo.fn_SplitParamMultiDelimiter(@FilterBusinessCategory, '!~~!')                     
	
			--SELECT RowNum = IDENTITY(INT, 1, 1)
			--		, FilterId = Param
			--INTO #FilterID
			--FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterID, '!~~!')  
				
			-- SELECT RowNum = IDENTITY(INT, 1, 1)
			--		, FilterValueId = Param
			--INTO #FilterValueID
			--FROM dbo.fn_SplitParamMultiDelimiter(@AdminFilterValueID, '!~~!')  
				  	
			--SELECT RowNum = IDENTITY(INT, 1, 1)
			--		,C.CategoryID
			--		,FilterID =  F.Param 
			--INTO #cat_filterValues 
			--FROM #CategoryID C
			--INNER JOIN #FilterID S ON C.RowNum = S.RowNum 
			--CROSS APPLY (select Param from fn_SplitParam(S.FilterId,'|') ) F
						
			--SELECT CategoryID
			--	  ,	FilterID
			--	  , FilterValueId
			--INTO #Final_filtervalues
			--FROM #cat_filterValues V
			--INNER JOIN #FilterValueID I on V.RowNum = I.RowNum

			----Insert into Retailer Filter Association table.

			--	IF CURSOR_STATUS('global','CategoryList')>=-1
			--	BEGIN
			--	DEALLOCATE CategoryList
			--	END
                                                
			--	DECLARE @CategoryID1 int
			--	DECLARE @FilterID1 int
			--	DECLARE @FilterValueID1 varchar(1000)
			--	DECLARE CategoryList CURSOR STATIC FOR
			--	SELECT CategoryID
			--		  ,FilterId
			--		  ,FilterValueId          
			--	FROM #Final_filtervalues 
                           
			--	OPEN CategoryList
                           
			--	FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                           
			--	CREATE TABLE #TempBiz(BusinnessCategoryID INT)

			--	WHILE @@FETCH_STATUS = 0
			--	BEGIN   
                            
			--			INSERT INTO #TempBiz(BusinnessCategoryID) 			                                
			--			OUTPUT inserted.BusinnessCategoryID INTO #TempBiz(BusinnessCategoryID)
			--						SELECT F.[Param]						     
			--			FROM dbo.fn_SplitParam(@CategoryID1, ',') F


			--			INSERT INTO RetailerFilterAssociation(RetailID
			--												  ,AdminFilterID
			--												  ,BusinessCategoryID
			--												  ,AdminFilterValueID
			--												  ,DateCreated
			--												  ,CreatedUserID)
			--						OUTPUT inserted.BusinessCategoryID INTO #TempBiz(BusinnessCategoryID)
			--						SELECT @RetailID
			--								,IIF(@FilterID1 = 0,NULL, @FilterID1)
			--								,@CategoryID1
			--								,IIF([Param] = 0,NULL,[Param])
			--								,GETDATE()
			--								,@UserID
			--						FROM dbo.fn_SplitParam(@FilterValueID1, '|')
					
			--						DECLARE @Cnt INT 
			--						SELECT @Cnt = 0
			--						SELECT @Cnt = COUNT(Param)
			--						FROM dbo.fn_SplitParam(@FilterValueID1, '|')
                                   
                               
			--			TRUNCATE TABLE #TempBiz
		
			--			FETCH NEXT FROM CategoryList INTO @CategoryID1, @FilterID1, @FilterValueID1
                                  
			--	END                        

			--	CLOSE CategoryList
			--	DEALLOCATE CategoryList
			--END

         		
			SET @Status = 0					--Confirmation of Success.		
			SET @DuplicateFlag = 0	
			SET @DuplicateRetailerFlag = 0		--CONFIRM NO DUPLICATION
			SET @ResponseRetailID = @RetailID 
		END
		ELSE
		BEGIN
			SET @Status = 0					--Confirmation of Success.
			SET @DuplicateFlag = 1	
			SET @DuplicateRetailerFlag = 0		--CONFIRM DUPLICATION
			SET @ResponseRetailID = @RetailID 
		END		
	   END
	   ELSE
	   BEGIN
		PRINT 'Retailer Name exists in the system'
		SET @Status = 0		
		SET @DuplicateFlag = 0
		SET @DuplicateRetailerFlag = 1
	   END
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerCreation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;









GO
