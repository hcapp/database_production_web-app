USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebRetailerAdminDeleteRetailer]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Stored Procedure name	: usp_WebRetailerAdminDeleteRetailer
Purpose					: To delete single/list of Retailers from the system.
Example					: usp_WebRetailerAdminDeleteRetailer

History
Version		Date					Author			Change Description
------------------------------------------------------------------------------- 
1.0			16th July 2013			SPAN			Initial Version
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE  [dbo].[usp_WebRetailerAdminDeleteRetailer]

	--Input parameters	
	  @RetailID VARCHAR(MAX)	
	, @UserID INT

	--Output variables	
	, @FindClearCacheURL VARCHAR(500) OUTPUT
	, @Status INT OUTPUT
	, @ErrorNumber INT OUTPUT
	, @ErrorMessage VARCHAR(1000) OUTPUT 
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION			

			--Update statement is added for the Logical delete implementaion 
			--and rest of the delete statements are commented.
			UPDATE Retailer SET RetailerActive=0
			FROM Retailer R 
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') RD ON R.RetailID=RD.Param;
			
			UPDATE RetailLocation SET Active=0
			FROM RetailLocation RL 
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') RLD ON RL.RetailID=RLD.Param;
			
			
			--The below delete statements are commented with /* */ symbols.
			/*SELECT Param RetailID
			INTO #Retailer
			FROM dbo.fn_SplitParam(@RetailID,',')			
			
			INSERT INTO RetailerDeletionHistory(RetailID
											,RetailName
											,Address1
											,Address2
											,Address3
											,Address4
											,City
											,State
											,PostalCode
											,CountryID
											,RetailerImagePath
											,UserID
											,DateCreated
											,ModifyUserID
											,DateModified
											,ProgramParticipant
											,WishPondRetailID
											,POSIntegrated
											,RetailURL
											,LegalAuthorityFirstName
											,LegalAuthorityLastName
											,CorporatePhoneNo
											,IsNonProfitOrganisation
											,WebsiteSourceFlag
											,RetailOnlineURL
											,NumberOfLocations											
											,CellFireMerchantID
											,CellFireMerchantSmallImage
											,CellFireMerchantLargeImage
											,FreeApplistingReferralName
											,FreeApplistingRetailer
											,AssociateOrganizations
											,DuplicateRetailerID
											,DuplicateRetailLocationID
											)
											
			SELECT 							RetailID
											,RetailName
											,Address1
											,Address2
											,Address3
											,Address4
											,City
											,State
											,PostalCode
											,CountryID
											,RetailerImagePath
											,@UserID 
											,GETDATE()
											,ModifyUserID
											,DateModified
											,ProgramParticipant
											,WishPondRetailID
											,POSIntegrated
											,RetailURL
											,LegalAuthorityFirstName
											,LegalAuthorityLastName
											,CorporatePhoneNo
											,IsNonProfitOrganisation
											,WebsiteSourceFlag
											,RetailOnlineURL
											,NumberOfLocations											
											,CellFireMerchantID
											,CellFireMerchantSmallImage
											,CellFireMerchantLargeImage
											,FreeApplistingReferralName
											,FreeApplistingRetailer
											,AssociateOrganizations
											,DuplicateRetailerID
											,DuplicateRetailLocationID
												
			FROM Retailer R	
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') F ON R.Retailid = F.Param	
			
			INSERT INTO RetailLocationDeletionHistory(RetailLocationID
												,RetailID
												,UserID
												,Headquarters
												,Address1
												,Address2
												,Address3
												,Address4
												,City
												,State
												,PostalCode
												,CountryID
												,RetailLocationLatitude
												,RetailLocationLongitude
												,RetailLocationTimeZone
												,DateCreated
												,DateModified
												,RetailLocationStoreHours
												,StoreIdentification
												,WishPondRetailID
												,OnlineStoreFlag
												,CorporateAndStore
												,RetailLocationURL
												,DuplicateFlag
												,FreeAppRetailerID)		
			SELECT								RetailLocationID
												,RetailID
												,@UserID
												,Headquarters
												,Address1
												,Address2
												,Address3
												,Address4
												,City
												,State
												,PostalCode
												,CountryID
												,RetailLocationLatitude
												,RetailLocationLongitude
												,RetailLocationTimeZone
												,GETDATE()
												,DateModified
												,RetailLocationStoreHours
												,StoreIdentification
												,WishPondRetailID
												,OnlineStoreFlag
												,CorporateAndStore
												,RetailLocationURL
												,DuplicateFlag
												,FreeAppRetailerID																			
			FROM RetailLocation RL	
			INNER JOIN dbo.fn_SplitParam(@RetailID,',') F ON RL.RetailID = F.Param											

			SELECT C.CouponID
			INTO #Coupon
			FROM Coupon C		
			INNER JOIN #Retailer R ON  R.RetailID= C.RetailID						

			SELECT RE.RebateID 
			INTO #Rebate
			FROM Rebate RE
			INNER JOIN RebateRetailer RR ON RE.RebateID=RR.RebateID 
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=RR.RetailID

			SELECT ProductHotDealID
			INTO #HotDeal
			FROM ProductHotDeal P
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=P.RetailID
			
			SELECT AdvertisementBannerID
			INTO #AdvertisementBanner
			FROM AdvertisementBanner A
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=A.RetailID

			SELECT LoyaltyProgramID
			INTO #LoyaltyProgram
			FROM LoyaltyProgram A
			INNER JOIN dbo.fn_SplitParam(@RetailID, ',') F ON F.Param=A.RetailID

			--Add's			
			
			DELETE FROM UserAdvertisementHit
			FROM UserAdvertisementHit RA
			INNER JOIN RetailLocationAdvertisement AD ON AD.RetailLocationAdvertisementID =RA.RetailLocationAdvertisementID 
			INNER JOIN RetailLocation RL ON AD.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
            print'RetailLocationAdvertisement'
			DELETE FROM RetailLocationAdvertisement
			FROM RetailLocationAdvertisement RA
			INNER JOIN RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
			print'RetailLocationBannerAd'
			DELETE FROM RetailLocationBannerAd
			FROM RetailLocationBannerAd RA
			INNER JOIN RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
			print'AdvertisementBanner'
			DELETE FROM AdvertisementBanner
			FROM AdvertisementBanner RA
			INNER JOIN #Retailer R ON R.RetailID=RA.RetailID
			
			print'RetailLocationSplashAd'
			DELETE FROM RetailLocationSplashAd
			FROM RetailLocationSplashAd RA
			INNER JOIN RetailLocation RL ON RA.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
			print'AdvertisementSplash'
			DELETE FROM AdvertisementSplash
			FROM AdvertisementSplash RA
			INNER JOIN #Retailer R ON R.RetailID=RA.RetailID
			
			print'CellFireUserCardsHistory'
			DELETE FROM CellFireUserCardsHistory
			FROM CellFireUserCardsHistory RA
			INNER JOIN #Retailer R ON R.RetailID=RA.RetailID
			
			print'CellFireUserCards'
			DELETE FROM CellFireUserCards
			FROM CellFireUserCards RA
			INNER JOIN #Retailer R ON R.RetailID=RA.RetailID
			

			--Retailer data deletion
		
			print'RetailerKeywords'
			DELETE FROM RetailerKeywords
			FROM RetailerKeywords RK
			INNER JOIN #Retailer RR ON RK.RetailID=RR.RetailID

			print'RetailCategory'
			DELETE FROM RetailCategory
			FROM RetailCategory R
			INNER JOIN #Retailer RR ON R.RetailID=RR.RetailID

			print'ProductReviews'
			DELETE FROM ProductReviews
			FROM ProductReviews P
			INNER JOIN #Retailer RR ON P.RetailID=RR.RetailID

			print'UserRetailPreference'
			DELETE FROM UserRetailPreference
			FROM UserRetailPreference U
			INNER JOIN #Retailer R ON U.RetailID=R.RetailID
			
			print'RetailerBillingDetails'
			DELETE FROM RetailerBillingDetails
			FROM RetailerBillingDetails RB
			INNER JOIN #Retailer R ON R.RetailID=RB.RetailerID			
			
			print'RetailerBankInformation'
			DELETE FROM RetailerBankInformation
			from RetailerBankInformation RB
			INNER JOIN #Retailer R ON R.RetailID=RB.RetailerID

			print'UserPushNotification'
			DELETE FROM UserPushNotification
			FROM UserPushNotification PA
			INNER JOIN RetailLocationDeal RD ON PA.RetailLocationDealID = RD.RetailLocationDealID
			INNER JOIN RetailLocation RL ON RD.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=rl.RetailID
			
			print'RetailLocationDeal'
			DELETE FROM RetailLocationDeal
			FROM RetailLocationDeal PA
			INNER JOIN RetailLocation RL ON PA.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=rl.RetailID
								
			print'RetailLocationProduct'
			DELETE FROM RetailLocationProduct
			FROM RetailLocationProduct PA
			INNER JOIN RetailLocation RL ON PA.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=rl.RetailID
			
			
			SELECT ContactID 
			INTO #RetailContact
			FROM RetailContact R
			INNER JOIN RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID
				
			print'RetailContact'
			DELETE FROM RetailContact
			FROM RetailContact R
			INNER JOIN RetailLocation RL ON R.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID			
				
			print'Contact'
			DELETE FROM Contact 
			FROM Contact C
			INNER JOIN #RetailContact RC ON C.ContactID=RC.ContactID	    	
				
						
		
			----Product related tables

			print'RebateProduct'
			DELETE FROM RebateProduct
			FROM RebateProduct PA
			INNER JOIN Rebate RR ON PA.RebateID =RR.RebateID 
			--INNER JOIN RebateRetailer RR ON PA.RebateID=RR.RebateID
			inner join #Retailer R ON R.RetailID=RR.RetailID
			
			print'HotDealProduct'
			DELETE FROM HotDealProduct
			FROM HotDealProduct PA
			INNER JOIN ProductHotDeal P on p.ProductHotDealID=PA.ProductHotDealID
			INNER JOIN #Retailer R ON  R.RetailID=P.RetailID

			DELETE FROM UserPushNotification
			FROM UserPushNotification PA			
			INNER JOIN ProductHotDeal P on p.ProductHotDealID=PA.ProductHotDealID
			INNER JOIN #Retailer R ON  R.RetailID=P.RetailID	
			
			
			print'ProductUserHit'
			DELETE FROM ProductUserHit
			FROM ProductUserHit PA
			INNER JOIN RetailLocation RL ON rl.RetailLocationID=PA.RetailLocationID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID

			----Coupon related tables 

			print'CouponHistory'
			DELETE FROM CouponHistory
			FROM CouponHistory CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID	
					
			print'CouponProduct'
			DELETE FROM CouponProduct
			FROM CouponProduct CP
			INNER JOIN #Coupon CR ON CP.CouponID = CR.CouponID			
			
			print'CouponRetailer'
			DELETE FROM CouponRetailer  where CouponID IN (SELECT CouponID from #Coupon)
			--FROM CouponRetailer CP where couponid in (select couponid from #coupon)
			--INNER JOIN #Coupon C ON C.CouponID = CP.CouponID AND C.CouponID IS NOT NULL 			
			
			print'UserCouponGallery'
			DELETE FROM UserCouponGallery
			FROM UserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 

			DELETE FROM HcUserCouponGallery
			FROM HcUserCouponGallery CP
			INNER JOIN #Coupon C ON C.CouponID = CP.CouponID 
			
			print'UserPushNotification'
			DELETE FROM UserPushNotification
			FROM UserPushNotification PA			
			INNER JOIN #Coupon C ON C.CouponID = PA.CouponID	
			
			print'Coupon'
			DELETE FROM Coupon
			FROM Coupon CP
			INNER JOIN #Retailer C ON C.RetailID = CP.RetailID
			
			print'UserLoyaltyProgram'
			DELETE FROM UserLoyaltyProgram
			FROM UserLoyaltyProgram RA
			INNER JOIN LoyaltyProgram RL ON RA.LoyaltyProgramID=RL.LoyaltyProgramID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
			
			print'UserPayHistory'
			DELETE FROM UserPayHistory
			FROM UserPayHistory RA
			INNER JOIN LoyaltyProgram RL ON RA.LoyaltyProgramID=RL.LoyaltyProgramID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID
			
			
			print'LoyaltyDeal'
			DELETE FROM LoyaltyDeal
			FROM LoyaltyDeal RA
			INNER JOIN LoyaltyProgram RL ON RA.LoyaltyProgramID=RL.LoyaltyProgramID
			INNER JOIN #Retailer R ON R.RetailID=RL.RetailID			
			
			print'LoyaltyProgram'
			DELETE FROM LoyaltyProgram
			FROM LoyaltyProgram RB
			INNER JOIN #Retailer R ON R.RetailID=RB.RetailID

			----Rebate related tables
			print'RebateRetailer'
			DELETE FROM RebateRetailer
			FROM RebateRetailer  RP			
			INNER JOIN #Retailer RR ON RP.RetailID=RR.RetailID
			 
			
			print'ScanHistory'
			DELETE FROM ScanHistory
			FROM ScanHistory  RP			
			INNER JOIN RetailLocation RL ON RP.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer RT ON RT.RetailID=RL.RetailID
			
			print'UserRebateGallery'
			DELETE FROM UserRebateGallery
			FROM UserRebateGallery  RP		
			INNER JOIN Rebate RL ON RL.RebateID =RP.RebateID
			INNER JOIN #Retailer RR ON RR.RetailID=RL.RetailID
			
			
			print'RebateHistory'
			DELETE FROM RebateHistory
			FROM RebateHistory RH
			INNER JOIN Rebate R ON R.RebateID = RH.RebateID
			INNER JOIN #Retailer RR ON RR.RetailID=R.RetailID
			
			
			print'RebateUserHit'
			DELETE FROM RebateUserHit
			FROM RebateUserHit RH
			INNER JOIN Rebate R ON R.RebateID = RH.RebateID
			INNER JOIN #Retailer RR ON RR.RetailID=R.RetailID
			
			print'Rebate'
			DELETE FROM Rebate
			FROM Rebate R
			INNER JOIN #Retailer RR ON RR.RetailID=R.RetailID

			----ProductHotDeal related tables.
			print'ProductHotDealRetailLocation'
			DELETE FROM ProductHotDealRetailLocation
			FROM ProductHotDealRetailLocation P
			INNER JOIN #HotDeal D ON D.ProductHotDealID = P.ProductHotDealID 
			
			print'QRRetailerCustomPageAssociation'
			DELETE FROM QRRetailerCustomPageAssociation 
			From QRRetailerCustomPageAssociation QR
			INNER JOin #Retailer R ON R.RetailID =QR.RetailID 
			
			print'QRRetailerCustomPageMedia'
			DELETE FROM QRRetailerCustomPageMedia
			From QRRetailerCustomPageMedia QR
			INNER JOIN QRRetailerCustomPage QC ON QR.QRRetailerCustomPageID=QC.QRRetailerCustomPageID
			INNER JOIN #Retailer R ON R.RetailID =QC.RetailID 
			
			print'UserGiveawayAssociation'
			DELETE FROM UserGiveawayAssociation
			From UserGiveawayAssociation QR
			INNER JOIN QRRetailerCustomPage QC ON QR.QRRetailerCustomPageID=QC.QRRetailerCustomPageID
			INNER JOIN #Retailer R ON R.RetailID =QC.RetailID
			
			print'QRRetailerCustomPage'
			DELETE FROM QRRetailerCustomPage
			From QRRetailerCustomPage QC
			INNER JOin #Retailer R ON R.RetailID =QC.RetailID
			
			print'ProductKeywordsRetailer'
			DELETE FROM ProductKeywordsRetailer
			From ProductKeywordsRetailer PR
			INNER JOin #Retailer R ON R.RetailID =PR.RetailID
			
			--print'UserHotDealGallery'
			--DELETE FROM UserHotDealGallery
			--FROM UserHotDealGallery UH
			--INNER JOIN RetailLocation RL ON UH.RetailLocationID=RL.RetailLocationID
			--INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID

			--DELETE FROM HcUserHotDealGallery
			--FROM HcUserHotDealGallery UH
			--INNER JOIN RetailLocation RL ON UH.RetailLocationID=RL.RetailLocationID
			--INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID
			
			print'AffiliateRetailer'
			DELETE FROM AffiliateRetailer
			FROM AffiliateRetailer RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID
			
			print'GroupRetailer'
			DELETE FROM GroupRetailer
			FROM GroupRetailer RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID
			
			print'ProductUserHit'
			DELETE FROM ProductUserHit
			FROM ProductUserHit RL
			INNER JOIN RetailLocation R ON R.RetailLocationID=RL.RetailLocationID
			INNER JOIN #Retailer RR ON R.RetailID=RR.RetailID
			
			print'QRUserRetailerSaleNotification'
			DELETE FROM QRUserRetailerSaleNotification
			FROM QRUserRetailerSaleNotification RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID			
			
			print'UserRetailPreference'
			DELETE FROM UserRetailPreference
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)
				
			print'UserHotDealGallery'
			DELETE FROM UserHotDealGallery
			WHERE HotDealID IN (SELECT ProducthotdealID FROM #HotDeal)	

			DELETE FROM HcUserHotDealGallery
			WHERE HotDealID IN (SELECT ProducthotdealID FROM #HotDeal)
			
			----Delete Data from Hubciti related Tables

			-- Delte from Appsite related data from Hubciti
			SELECT DISTINCT RL.RetailLocationID 
			INTO #RetailLocation
			FROM #Retailer R
			INNER JOIN RetailLocation RL ON RL.RetailID =R.RetailID AND R.RetailID IS NOT NULL


			SELECT DISTINCT HcAppSiteID 
			INTO #Appsites
			FROM HcAppSite A
			INNER JOIN #RetailLocation R ON R.RetailLocationID =A.RetailLocationID      						
			
			SELECT E.HcEventID 
			INTO #Events 
			FROM HcEventAppsite E
			INNER JOIN #Appsites A ON A.HcAppSiteID =E.HcAppsiteID

			SELECT DISTINCT R1.HcEventID
			INTO #Events1
			FROM HcEvents R1
			INNER JOIN #Retailer R ON R1.RetailID = R.RetailID
			

			DELETE FROM HcRetailerEventsAssociation
			FROM HcRetailerEventsAssociation HR
			INNER JOIN #RetailLocation RL ON HR.RetailLocationID = RL.RetailLocationID

			DELETE FROM HcEventAppsite 
			FROM HcEventAppsite E
			INNER JOIN #Appsites A ON A.HcAppSiteID =E.HcAppsiteID			

			DELETE FROM HcEventInterval 
			FROM HcEventInterval I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			

			DELETE FROM HcEventLocation
			FROM HcEventLocation I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			

			DELETE FROM HcEventPackage 
			FROM HcEventPackage I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			

			DELETE FROM HcEventsCategoryAssociation  
			FROM HcEventsCategoryAssociation I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID 
			
			DELETE FROM HcFundraisingEventsAssociation  
			FROM HcFundraisingEventsAssociation I
			INNER JOIN #Events1 E ON E.HcEventID =I.HcEventID

			--DELETE FROM HcAppSite  
			--FROM HcAppSite C
			--INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HcEvents
			FROM HcEvents E
			INNER JOIN #Retailer R on E.RetailID = R.RetailID


			--Delete from HcCityExperienceRetailLocation		

			DELETE FROM HcEventPackage 
			FROM HcEventPackage C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HcCityExperienceRetailLocation
			FROM HcCityExperienceRetailLocation C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

            
			DELETE FROM HcFilterRetailLocation 
			FROM HcFilterRetailLocation C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HCProductUserHit  
			FROM HCProductUserHit C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HcRetailerAssociation   
			FROM HcRetailerAssociation C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			DELETE FROM HcRetailerSubCategory    
			FROM HcRetailerSubCategory C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			--DELETE FROM HcUserCouponGallery    
			--FROM HcUserCouponGallery C
			--INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID
			

			--print 'HcFundraisingAppsiteAssociation'
			--DELETE FROM HcFundraisingAppsiteAssociation
			--FROM HcFundraisingAppsiteAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.RetailID = @RetailID

			--print 'HcFundraisingCategoryAssociation'
			--DELETE FROM HcFundraisingCategoryAssociation
			--FROM HcFundraisingCategoryAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.RetailID = @RetailID

			--print 'HcFundraisingBottomButtonAssociation'
			--DELETE FROM HcFundraisingBottomButtonAssociation
			--FROM HcFundraisingBottomButtonAssociation A
			--INNER JOIN HcFundraisingDepartments D ON A.HcFundraisingDepartmentID = D.HcFundraisingDepartmentID
			--WHERE D.RetailID = @RetailID

			--print 'HcFundraisingDepartments'
			--DELETE FROM HcFundraisingDepartments WHERE RetailID = @RetailID

			--print 'HcFundraisingEventsAssociation'
			--DELETE FROM HcFundraisingEventsAssociation
			--FROM HcFundraisingEventsAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.RetailID = @RetailID

			--print 'HcRetailerFundraisingAssociation'
			--DELETE FROM HcRetailerFundraisingAssociation
			--FROM HcRetailerFundraisingAssociation A
			--INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			--WHERE F.RetailID = @RetailID

			--print 'HcFundraising'
			--DELETE FROM HcFundraising WHERE RetailID = @RetailID
			
			print 'HcFundraisingAppsiteAssociation'
			DELETE FROM HcFundraisingAppsiteAssociation
			FROM HcFundraisingAppsiteAssociation FA 
			INNER JOIN HcAppSite A ON FA.HcAppsiteID = A.HcAppsiteID
			INNER JOIN RetailLocation RL ON A.RetailLocationID = RL.RetailLocationID
			INNER JOIN #Retailer R ON RL.RetailID = R.RetailID

			print 'HcFundraisingAppsiteAssociation'
			DELETE FROM HcFundraisingAppsiteAssociation
			FROM HcFundraisingAppsiteAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Retailer R ON F.RetailID = R.RetailID
			
			print 'HcAppSite'
			DELETE FROM HcAppSite  
			FROM HcAppSite C
			INNER JOIN #RetailLocation R ON R.RetailLocationID=C.RetailLocationID

			print 'HcFundraisingCategoryAssociation'
			DELETE FROM HcFundraisingCategoryAssociation
			FROM HcFundraisingCategoryAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Retailer R ON F.RetailID = R.RetailID

			print 'HcFundraisingBottomButtonAssociation'
			DELETE FROM HcFundraisingBottomButtonAssociation
			FROM HcFundraisingBottomButtonAssociation A
			INNER JOIN HcFundraisingDepartments D ON A.HcFundraisingDepartmentID = D.HcFundraisingDepartmentID
			INNER JOIN #Retailer R ON D.RetailID = R.RetailID

			print 'HcFundraisingDepartments'
			DELETE FROM HcFundraisingDepartments 
			FROM HcFundraisingDepartments F 
			INNER JOIN #Retailer R ON F.RetailID = R.RetailID

			print 'HcFundraisingEventsAssociation'
			DELETE FROM HcFundraisingEventsAssociation
			FROM HcFundraisingEventsAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Retailer R ON F.RetailID = R.RetailID
			
			print 'HcRetailerFundraisingAssociation'
			DELETE FROM HcRetailerFundraisingAssociation
			FROM HcRetailerFundraisingAssociation A
			INNER JOIN HcFundraising F ON A.HcFundraisingID = F.HcFundraisingID
			INNER JOIN #Retailer R ON F.RetailID = R.RetailID

			print 'HcFundraising'
			DELETE FROM HcFundraising 
			FROM HcFundraising F
			INNER JOIN #Retailer R ON F.RetailID = R.RetailID			
			
			print'RetailLocation'
			DELETE FROM RetailLocation
			FROM RetailLocation RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID
			
			print'ProductHotDeal'
			DELETE FROM ProductHotDeal
			FROM ProductHotDeal RL
			INNER JOIN #Retailer RR ON RL.RetailID=RR.RetailID			

            SELECT UserID 
            INTO #UserID
            FROM UserRetailer 
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)
			
			print'UserRetailer'
			DELETE FROM UserRetailer 
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)

			print'RetailerFilterAssociation'
			DELETE FROM RetailerFilterAssociation
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)

			--print'RetailCategory_BAK'
			--DELETE FROM RetailCategory_BAK   
			--WHERE RetailID IN (SELECT RetailID FROM #Retailer)
			
			print'RetailerBusinessCategory'
            DELETE FROM RetailerBusinessCategory 
			WHERE RetailerID IN (SELECT RetailID FROM #Retailer)
			
			
			print'Retailer'
			DELETE FROM Retailer 
			WHERE RetailID IN (SELECT RetailID FROM #Retailer)
			
			print'Users'
			DELETE FROM Users 
			FROM Users U
			INNER JOIN #UserID US ON US.UserID =U.UserID */


			-------Find Clear Cache URL---26/2/2015--------

			DECLARE @CurrentURL VARCHAR(500),@SupportURL VARCHAR(500), @YetToReleaseURL VARCHAR(500)

              SELECT @CurrentURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QACurrentVersionURL'
              SELECT @SupportURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QASupportVersionURL'
              SELECT @YetToReleaseURL = Screencontent FROM AppConfiguration WHERE ConfigurationType = 'QAYetToReleaseVersionURL'

              SELECT @FindClearCacheURL= @CurrentURL+Screencontent +','+@SupportURL+Screencontent+','+@YetToReleaseURL+Screencontent
              FROM AppConfiguration WHERE ConfigurationType ='FindClearCacheURL'

			------------------------------------------------
			
		--Confirmation of Success.  
		SELECT @Status = 0
		COMMIT TRANSACTION
		
	END	TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebRetailerAdminDeleteRetailer.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;







GO
