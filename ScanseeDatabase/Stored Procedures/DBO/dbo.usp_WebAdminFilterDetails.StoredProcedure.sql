USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebAdminFilterDetails]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DRL Template


/*
Stored Procedure name   :  [usp_WebAdminFilterDetails]
Purpose                  : To display details of the Filter.
Example                  : [usp_WebAdminFilterDetails]

History
Version           Date                 Author          Change Description
------------------------------------------------------------------------------- 
1.0               1st Dec 2014         Dhananjaya TR   Initial Version                                        
-------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebAdminFilterDetails]
(

      --Input Input Parameter(s)--       
        @UserID INT
	  , @FilterID Int      
      
      --Output Variable--	
	  
      , @ErrorNumber INT OUTPUT
      , @ErrorMessage VARCHAR(1000) OUTPUT 
)
AS
BEGIN

      BEGIN TRY
	         
			 DECLARE @FilterValueFlag Bit
			 SET @FilterValueFlag =0

			 IF EXISTS(SELECT 1 FROM AdminFilterValueAssociation 
			           WHERE AdminFilterID =@FilterID)
			 BEGIN
				 SET @FilterValueFlag =1
			 END
			       
	         -- To display details of the Filter
		     SELECT DISTINCT ROW_NUMBER() over (ORDER by A.FilterName  ASC) Rownum
			                ,A.AdminFilterID filterId
			                ,A.FilterName 
							,fCategory= REPLACE(STUFF((SELECT ', ' + CAST(B.BusinessCategoryID AS VARCHAR(2000))
                                                                                                     FROM AdminFilterCategoryAssociation F
																									 INNER JOIN BusinessCategory B ON B.BusinessCategoryID =F.BusinessCategoryID 
                                                                                                     WHERE F.AdminFilterID =A.AdminFilterID 
                                                                                                     ORDER BY B.BusinessCategoryName 
                                                                                                     FOR XML PATH(''))
             
			                                                                                        , 1, 2, ''), ' ', '')
							
							,CategoryName= REPLACE(STUFF((SELECT ', ' + CAST(B.BusinessCategoryName AS VARCHAR(2000))
                                                                                                     FROM AdminFilterCategoryAssociation F
																									 INNER JOIN BusinessCategory B ON B.BusinessCategoryID =F.BusinessCategoryID 
                                                                                                     WHERE F.AdminFilterID =A.AdminFilterID 
                                                                                                     ORDER BY B.BusinessCategoryName 
                                                                                                     FOR XML PATH(''))
             
			                                                                                        , 1, 2, ''), ' ', '')
							--,FilterValue= REPLACE(STUFF((SELECT ', ' + CAST(B.FilterValueName AS VARCHAR(2000))
       --                                                                                              FROM AdminFilterValueAssociation F
							--																		 INNER JOIN AdminFilterValue B ON B.AdminFilterValueID  =F.AdminFilterValueID  
       --                                                                                              WHERE F.AdminFilterID =A.AdminFilterID 
       --                                                                                              ORDER BY B.FilterValueName  
       --                                                                                              FOR XML PATH(''))
             
			    --                                                                                    , 1, 2, ''), ' ', '')

				       ,@FilterValueFlag fvalueflag
			 
			 FROM AdminFilter A
			 WHERE AdminFilterID =@FilterID 
        
      END TRY
            
      BEGIN CATCH 
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_WebAdminFilterDetails.'           
                  -- Execute retrieval of Error info.
                  EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
                               
                  
            END;
            
      END CATCH;
END;



GO
