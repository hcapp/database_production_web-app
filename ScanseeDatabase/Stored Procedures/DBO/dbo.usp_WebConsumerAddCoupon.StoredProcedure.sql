USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerAddCoupon]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebConsumerAddCoupon
Purpose					: To add Coupon to User Gallery
Example					: usp_WebConsumerAddCoupon 

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			22nd Aug 2013	Dhananjaya TR	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebConsumerAddCoupon]
(
	  @CouponID int
	, @UserID int
	--, @UserCouponClaimTypeID tinyint
	----, @CouponPayoutMethod varchar(20)
	--, @RetailLocationID int
	--, @CouponClaimDate datetime
	
	--User Trcking Inputs
	, @CouponListID int
	
	--Output Variable 
	, @Status int output
	, @ErrorNumber int output
	, @ErrorMessage varchar(1000) output
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			IF NOT EXISTS (SELECT 1 FROM UserCouponGallery WHERE CouponID = @CouponID AND UserID=@UserID)
			BEGIN
				INSERT INTO [UserCouponGallery]
				   ([CouponID]
				   ,[UserID]				  
				   ,[CouponClaimDate])
				VALUES
				   (@CouponID
				   ,@UserID				  
				   ,GETDATE())
				   
			END
			
			--User Tracking
			
			--Updating when user clip the coupon in the couponlist table
			Update ScanSeeReportingDatabase..CouponList SET CouponClipped=1, CouponClick = 1
			WHERE CouponListID =@CouponListID 
				   
		    --Confirmation of Success.
			SELECT @Status = 0
			
			   	
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebConsumerAddCoupon.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
