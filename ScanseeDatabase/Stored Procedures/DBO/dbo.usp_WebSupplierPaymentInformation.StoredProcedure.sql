USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebSupplierPaymentInformation]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_WebSupplierPaymentInformation
Purpose					: To insert the details of the plan choosed by the supplier
Example					: usp_WebSupplierPaymentInformation

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			25th Jan 2012	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_WebSupplierPaymentInformation]
(
		  @ManufacturerID int
		, @UserID int
		
		--ACH Information.
		, @ACHorBankInformationFlag bit
		, @BankName varchar(255)
		, @AccountTypeID int
		, @AccountNumber int
		, @AccountHolderName varchar(255)
		
		
		--Credit Card Information.
		, @CreditCardFlag BIT
		, @CreditCardNumber INT
		, @ExpirationDate DATETIME
		, @CVV INT
		, @CardholderName VARCHAR(1000)
		
		--AlternatePayment.
		
		, @AlternatePaymentFlag bit
		, @AccessCode varchar(1000)
		
		
		--General Information.
		, @BillingAddress varchar(1000)
		, @City varchar(255) 
		, @State char(2)
		, @Zip char(10)
		, @PhoneNumber char(10)	
			
	--Output Variable 
		, @Status int output
		, @ErrorNumber int output
		, @ErrorMessage varchar(1000) output 
)
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION 
			
			--If the selected tab is Bank Account.
			IF (@ACHorBankInformationFlag = 1)
			BEGIN
		    INSERT INTO ManufacturerBankInformation(
														UserID
													  , ManufacturerID
													  , ACHorBankInformationFlag
													  , BankName 
													  , AccountTypeID
													  , AccountNumber 
													  , AccountHolderName 
													  , BillingAddress 
													  , City
													  , [State] 
													  , Zip 
													  , PhoneNumber 
												
													)
										VALUES(
												 @ManufacturerID 
											   , @UserID 
											   , @ACHorBankInformationFlag 
											   , @BankName 
											   , @AccountTypeID 
											   , @AccountNumber 
											   , @AccountHolderName 
											   , @BillingAddress
											   , @City 
											   , @State 
											   , @Zip 
											   , @PhoneNumber
											   )	
			END		
			
			--If the selected tab is Credit Card.
			IF (@CreditCardFlag = 1)
			BEGIN
				INSERT INTO ManufacturerBankInformation(
														UserID
													  , ManufacturerID
													  , CreditCardFlag
													  , CreditCardNumber 
													  , ExpirationDate
													  , CardholderName 
													  , CVV 
													  , CreditCardBillingAddress 
													  , City
													  , [State] 
													  , Zip 
													  , PhoneNumber 
												
													)
										VALUES(
												 @ManufacturerID 
											   , @UserID 
											   , @CreditCardFlag 
											   , @CreditCardNumber 
											   , (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@ExpirationDate)+1,0)))
											   , @CardholderName 
											   , @CVV 
											   , @BillingAddress
											   , @City 
											   , @State 
											   , @Zip 
											   , @PhoneNumber
											   )
			END
			
			IF(@AlternatePaymentFlag = 1)
			BEGIN
				INSERT INTO ManufacturerBankInformation(UserID
													  , ManufacturerID
													  , AlternatePaymentFlag
													  , AccessCode)
										VALUES(
												 @ManufacturerID 
											   , @UserID 
											   , @AlternatePaymentFlag
											   , @AccessCode
											  )
			END
		
		--Confirmation of Success.
			SELECT @Status = 0
		COMMIT TRANSACTION
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_WebSupplierPaymentInformation.'		
			--- Execute retrieval of Error info.
			EXEC [usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output 
			PRINT 'The Transaction is uncommittable. Rolling Back Transaction'
			ROLLBACK TRANSACTION;
			--Confirmation of failure.
			SELECT @Status = 1
		END;
		 
	END CATCH;
END;


GO
