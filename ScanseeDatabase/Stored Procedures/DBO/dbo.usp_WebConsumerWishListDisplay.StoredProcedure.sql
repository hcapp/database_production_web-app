USE [scansee]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebConsumerWishListDisplay]    Script Date: 4/6/2017 1:08:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
Stored Procedure name	: usp_WebConsumerWishListDisplay 
Purpose					: To dispaly User's Wish List.  
Example					: usp_WebConsumerWishListDisplay   
  
History  
Version		Date				Author		Change Description  
---------------------------------------------------------------   
1.0			06th August 2013	SPAN		Initial Version  
---------------------------------------------------------------  
*/  
  
CREATE PROCEDURE [dbo].[usp_WebConsumerWishListDisplay]  
(  
   @UserID int   
 --, @Latitude decimal(18,6)  
 --, @Longitude decimal(18,6)  
 --, @ZipCode varchar(10) 
 , @LowerLimit int  
 , @RecordCount int   
 
 --User Tracking Inputs
 , @MainMenuID int 
 
 --OutPut Variable  
 , @MaxCnt int  output
 , @NxtPageFlag bit output  
 , @ErrorNumber int output  
 , @ErrorMessage varchar(1000) output  
)  
AS  
BEGIN  
  
 BEGIN TRY  
 
  --To get Media Server Configuration.  
  DECLARE @Config varchar(50)   
  DECLARE @Radius FLOAT 
  SELECT @Config=ScreenContent    
  FROM AppConfiguration     
  WHERE ConfigurationType='Web Manufacturer Media Server Configuration'
  
   DECLARE @Latitude decimal(18,6)    
   DECLARE @Longitude decimal(18,6)  
   DECLARE @ZipCode VARCHAR(10)
 
   --Get the user configured postal code.
   SELECT @ZipCode = PostalCode
   FROM Users 
   WHERE UserID = @UserId 
 
   --Get the radius of the user.
   IF ISNULL(@Radius, '') = ''
   BEGIN
	  SELECT @Radius = LocaleRadius
	  FROM UserPreference 
	  WHERE UserID = @UserId
   END
  
  --To get the row count for pagination.  
	  DECLARE @UpperLimit int   	
	  SELECT @UpperLimit = @LowerLimit + @RecordCount   
	  

    CREATE TABLE #Wishlist(Row_Num INT IDENTITY(1,1)
						  ,UserProductID int
						  ,UserID int
						  ,ProductID int 
						  ,WishListAddDate datetime   
						  ,ProductName  varchar(2000)
						  ,productimagepath varchar(2000)
						  ,ProductPrice money 
						  ,SalePrice money
						  ,RetailFlag int
						  ,CouponSaleFlag int  
						  ,HotDealFlag int  
						  ,PushNotifyFlag bit
						  ,ProductDescription varchar(2000))
						  
	 CREATE TABLE #WishlistList(Row_Num INT IDENTITY(1,1)
						  ,UserProductID int
						  ,UserID int
						  ,ProductID int 
						  ,WishListAddDate datetime   
						  ,ProductName  varchar(2000)
						  ,productimagepath varchar(2000)
						  ,ProductPrice money 
						  ,SalePrice money
						  ,RetailFlag int
						  ,CouponSaleFlag int  
						  ,HotDealFlag int  
						  ,PushNotifyFlag bit
						  ,ProductDescription varchar(2000))

 --While User search by Zipcode, coordinates are fetched from GeoPosition table.      
   
  SELECT @Latitude = Latitude       
       , @Longitude = Longitude       
  FROM GeoPosition       
  WHERE PostalCode = @ZipCode       
   
 
 --Select the user preferred radius for the wish list module.
 SELECT @Radius = LocaleRadius
 FROM UserPreference
 WHERE UserID = @UserID
 
IF (@Latitude IS NOT NULL AND @Longitude IS NOT NULL )     
BEGIN  
  
	 --To get user's locationwise retail store infomation  
	 SELECT ProductID  
		 , RetailID   
		 , RetailLocationID  
		 , Distance  
		 --, SalePrice  
		 --, Price  
	 INTO #Details  
	 FROM (    
	   SELECT UP.ProductID  
		, RL.RetailID   
		, RL.RetailLocationID  
		, Distance = (ACOS((SIN(RetailLocationLatitude / 57.2958) * SIN(@Latitude / 57.2958) + COS(RetailLocationLatitude / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (RetailLocationLongitude / 57.2958))))*6371) * 0.6214       
		--, MIN(RD.SalePrice) SalePrice   
		--, MIN(RD.Price) Price  
	   FROM UserProduct UP  
		INNER JOIN RetailLocationDeal  RP ON RP.ProductID = UP.ProductID   
		INNER JOIN RetailLocation RL ON RL.RetailLocationID = RP.RetailLocationID   
		--LEFT JOIN RetailLocationDeal RD ON RD.RetailLocationID = RP.RetailLocationID AND RP.ProductID=RD.ProductID  
	   WHERE UP.UserID = @UserID   
		AND UP.WishListItem = 1  
	   GROUP BY UP.ProductID  
		, RL.RetailID   
		, RL.RetailLocationID  
		, RetailLocationLatitude  
		, RetailLocationLongitude  
	   ) D  
	WHERE Distance <= ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WebConsumerDefaultRadius' AND ScreenContent = 'WebConsumerDefaultRadius')) 
	

	 -- To get Deals
	 SELECT RD.RetailLocationID 
	 , RD.ProductID 
	 , RD.SalePrice 
	 , RD.Price 
	 INTO #Deal
	 FROM RetailLocationDeal RD
	 INNER JOIN #Details D ON D.RetailLocationID = RD.RetailLocationID AND D.ProductID = RD.ProductID 
	 WHERE GETDATE() BETWEEN ISNULL(RD.SaleStartDate, GETDATE() - 1) AND ISNULL(RD.SaleEndDate, GETDATE() + 1)
	 
	  
	 --To get Coupons available for User's Wish List Products     
	 SELECT C.CouponID   
	       ,CP.ProductID   
	 INTO #Coupon    
	 FROM Coupon C  
	 INNER JOIN CouponProduct CP ON C.CouponID=CP.CouponID 
	 INNER JOIN CouponRetailer CR ON C.CouponID=CR.CouponID  
	 INNER JOIN #Details D ON D.RetailID = CR.RetailID AND D.RetailLocationID = CR.RetailLocationID AND D.ProductID = CP.ProductID
	 LEFT JOIN UserCouponGallery UCG ON C.CouponID = UCG.CouponID 
	 WHERE ISNULL(CouponExpireDate,GETDATE()+1) >= GETDATE()    
	 GROUP BY C.CouponID
				,NoOfCouponsToIssue
				,CP.ProductID 
	 HAVING  CASE WHEN NoOfCouponsToIssue IS NOT NULL THEN NoOfCouponsToIssue
			ELSE ISNULL(COUNT(UserCouponGalleryID),0) + 1 END > ISNULL(COUNT(UserCouponGalleryID),0)     

  
	 --To get Locationwise Hotdeals   
	 SELECT DISTINCT  ProductHotDealID
					, ProductID
					, NoOfHotDealsToIssue 
	 INTO #HotDeal1  
	 FROM (SELECT P.ProductHotDealID  
				, UP.ProductID
				, NoOfHotDealsToIssue
				, Distance = CASE WHEN @Latitude IS NOT NULL AND @Longitude IS NOT NULL THEN (ACOS((SIN(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * SIN(@Latitude / 57.2958) + COS(CASE WHEN PL.HotDealLatitude IS NULL THEN (SELECT TOP 1 Latitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLatitude END / 57.2958) * COS(@Latitude / 57.2958) * COS((@Longitude / 57.2958) - (CASE WHEN PL.HotDealLongitude IS NULL THEN (SELECT TOP 1 Longitude FROM GeoPosition WHERE State=PL.State AND City =PL.City) ELSE PL.HotDealLongitude END/ 57.2958))))*6371) * 0.6214 END
		FROM UserProduct UP  
	   INNER JOIN HotDealProduct HP ON HP.ProductID = UP.ProductID   
	   INNER JOIN ProductHotDeal P ON P.ProductHotDealID = HP.ProductHotDealID   
	   INNER JOIN ProductHotDealLocation PL ON PL.ProductHotDealID = HP.ProductHotDealID 	   
	  -- LEFT JOIN GeoPosition G ON PL.City = G.City AND PL.State = G.State
		WHERE UP.UserID =  @UserID  
	   AND UP.WishListItem = 1  
	   AND (GETDATE() >= ISNULL(HotDealStartDate,GETDATE()-1) AND GETDATE()<=ISNULL(HotDealEndDate,GETDATE()+1)))HotDeal  
	 WHERE Distance <= ISNULL(@Radius, (SELECT ScreenContent FROM AppConfiguration WHERE ConfigurationType = 'WishList Default Radius' AND ScreenName = 'WishList Default Radius'))  
	 
	    	   
	 SELECT DISTINCT  ProductHotDealID
					, ProductID 
	INTO #HotDeal
	FROM #HotDeal1 H
	LEFT JOIN UserHotDealGallery UHG ON H.ProductHotDealID = UHG.HotDealID
	Group by ProductHotDealID,NoOfHotDealsToIssue,ProductID
	HAVING  CASE WHEN NoOfHotDealsToIssue IS NOT NULL THEN NoOfHotDealsToIssue
				 ELSE ISNULL(COUNT(UserHotDealGalleryID),0) + 1 END > ISNULL(COUNT(UserHotDealGalleryID),0)
	
	
		 
	 INSERT INTO #Wishlist(UserProductID 
						  ,UserID 
						  ,ProductID  
						  ,WishListAddDate    
						  ,ProductName  
						  ,productimagepath
						  ,ProductPrice
						  ,SalePrice
						  ,RetailFlag
						  ,CouponSaleFlag 
						  ,HotDealFlag
						  ,PushNotifyFlag
						  ,ProductDescription) 
	 SELECT UserProductID 
		  ,UserID 
		  ,ProductID  
		  ,WishListAddDate    
		  ,ProductName  
		  ,productimagepath
		  ,ProductPrice
		  ,SalePrice
		  ,RetailFlag
		  ,CouponSaleFlag 
		  ,HotDealFlag
		  ,PushNotifyFlag
		  ,ProductDescription
	 FROM(						  
	 
	 SELECT UserProductID   
	  , UserID   
	  , ProductID  
	  , WishListAddDate   
	  , ProductName  
	  , productimagepath   
	  , ProductPrice   
	  , SalePrice  
	  , RetailFlag = CASE WHEN ISNULL(SalePrice, 0) = 0 THEN 0 ELSE 1 END  
	  , CouponSaleFlag =(SELECT COUNT(CouponID) FROM #Coupon C WHERE C.ProductID = SaleProd.ProductID)  --(SELECT CASE WHEN COUNT(CouponID) = 0 THEN 0 ELSE 1 END FROM #Coupon C WHERE C.ProductID = SaleProd.ProductID)  
	  , HotDealFlag =(SELECT COUNT(DISTINCT(ProductHotDealID)) FROM #HotDeal H WHERE H.ProductID = SaleProd.ProductID) --(SELECT CASE WHEN COUNT(ProductID) = 0 THEN 0 ELSE 1 END FROM #HotDeal H WHERE H.ProductID = SaleProd.ProductID)  
	  , PushNotifyFlag 	
	  , ProductDescription 
	 FROM        
	  (    
	   SELECT UP.UserProductID   
		, UP.UserID   
		, ISNULL(UP.ProductID, 0) ProductID  
		, UP.WishListAddDate   
		, ProductName = CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
		, ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																								THEN @Config																							
																								+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																								+ProductImagePath ELSE ProductImagePath 
																						  END   
							  ELSE ProductImagePath END   
		, MIN(D.Price) ProductPrice   
		, MIN(D.saleprice) SalePrice  
		, UP.PushNotifyFlag   
		, ProductDescription = CASE WHEN P.ProductShortDescription IS NULL THEN P.ProductLongDescription ELSE P.ProductShortDescription END 
	       
	   FROM UserProduct UP  
		LEFT JOIN #Deal D ON D.ProductID = UP.ProductID   
		LEFT JOIN Product P ON P.ProductID = UP.ProductID   
	   WHERE UP.UserID = @UserID   
		AND UP.WishListItem = 1    
	   GROUP BY UP.UserID   
		, UP.UserProductID      
		, UP.ProductID  
		, UP.WishListAddDate  
		, ProductImagePath  
		, UP.PushNotifyFlag  
		, CASE WHEN UP.ProductID = 0 THEN UP.UnassignedProductName ELSE P.ProductName END  
		, p.ManufacturerID
		, P.WebsiteSourceFlag  	
		, CASE WHEN P.ProductShortDescription IS NULL THEN P.ProductLongDescription ELSE P.ProductShortDescription END	  
	  ) SaleProd  
	 )Wish
	 ORDER BY CASE WHEN RetailFlag = 1 THEN 1
					WHEN CouponSaleFlag = 1 THEN 1
					WHEN HotDealFlag = 1 THEN 1
					ELSE 0 END DESC
	, WishListAddDate DESC
	, ProductName ASC
	  --ORDER BY WishListAddDate, ProductName  
	  
    
 END  
 
 IF(@Latitude IS NULL AND @Longitude IS NULL AND @ZipCode IS NULL)  
   
    INSERT INTO #Wishlist(UserProductID 
						  ,UserID 
						  ,ProductID  
						  ,WishListAddDate    
						  ,ProductName  
						  ,productimagepath
						  ,ProductPrice
						  ,SalePrice
						  ,RetailFlag
						  ,CouponSaleFlag 
						  ,HotDealFlag
						  ,PushNotifyFlag
						  ,ProductDescription) 
	SELECT UserProductID 
		  ,UserID 
		  ,ProductID  
		  ,WishListAddDate    
		  ,ProductName  
		  ,productimagepath
		  ,ProductPrice
		  ,SalePrice
		  ,RetailFlag
		  ,CouponSaleFlag 
		  ,HotDealFlag
		  ,PushNotifyFlag
		  ,ProductDescription
	FROM(						  
		SELECT UserProductID	
		 , UserID   
		 , ProductID  
		 , WishListAddDate   
		 , ProductName   
		 , ProductImagePath  
		 , ProductPrice  
		 , SalePrice = (select MIN(SalePrice) from RetailLocationDeal rld where rld.ProductID = Prod.ProductID AND GETDATE() BETWEEN ISNULL(rld.SaleStartDate, GETDATE() - 1) AND ISNULL(rld.SaleEndDate, GETDATE() + 1))
		 , RetailFlag  = (select (case when COUNT(1)>0 then 1 else 0 end) from RetailLocationDeal rld where rld.ProductID = Prod.ProductID AND GETDATE() BETWEEN ISNULL(rld.SaleStartDate, GETDATE() - 1) AND ISNULL(rld.SaleEndDate, GETDATE() + 1))
		 , CouponSaleFlag = (SELECT COUNT(C.CouponID) FROM Coupon C INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID AND cp.ProductID=Prod.ProductID  
			  WHERE  CouponExpireDate >= GETDATE() )  
			  --(SELECT CASE WHEN COUNT(C.CouponID) = 0 THEN 0 ELSE 1 END FROM Coupon C INNER JOIN CouponProduct CP ON C.CouponID = CP.CouponID AND cp.ProductID=Prod.ProductID  
			  --WHERE  CouponExpireDate >= GETDATE() )  
		 , HotDealFlag = (SELECT COUNT(DISTINCT(HP.ProductHotDealID)) FROM HotDealProduct HP INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = HP.ProductHotDealID AND Prod.ProductID=hp.ProductID  
			  WHERE  (GETDATE() >= ISNULL(HotDealStartDate,GETDATE()-1) AND GETDATE()<=ISNULL(HotDealEndDate,GETDATE()+1)))  
			 --(SELECT CASE WHEN COUNT(HP.ProductID) = 0 THEN 0 ELSE 1 END FROM HotDealProduct HP INNER JOIN ProductHotDeal PH ON PH.ProductHotDealID = HP.ProductHotDealID AND Prod.ProductID=hp.ProductID  
			 -- WHERE  GETDATE() BETWEEN HotDealStartDate AND HotDealEndDate)  
		 , PushNotifyFlag     
		 , ProductDescription   
		 FROM (SELECT U.UserProductID   
			 , U.UserID   
			 , ISNULL(U.ProductID, 0) ProductID  
			 , U.WishListAddDate   
			 , ProductName = CASE WHEN U.ProductID = 0 THEN U.UnassignedProductName ELSE P.ProductName END  
			 , ProductImagePath = CASE WHEN ProductImagePath IS NOT NULL THEN CASE WHEN P.WebsiteSourceFlag = 1 
																								THEN @Config
																								+CONVERT(VARCHAR(30),P.ManufacturerID)+'/'
																								+ProductImagePath ELSE ProductImagePath 
																						  END   
							  ELSE ProductImagePath END  
			 , NULL ProductPrice  
			 , NULL SalePrice  
			 , NULL RetailFlag  
			 , U.PushNotifyFlag  
			 , ProductDescription = CASE WHEN P.ProductShortDescription IS NULL THEN P.ProductLongDescription ELSE P.ProductShortDescription END 
			FROM UserProduct U  
			INNER JOIN Product P ON U.ProductID=p.ProductID  
			WHERE UserID=@UserID AND WishListItem=1
		) Prod 
		)WishProd
		ORDER BY CASE WHEN RetailFlag = 1 THEN 1
					WHEN CouponSaleFlag = 1 THEN 1
					WHEN HotDealFlag = 1 THEN 1
					ELSE 0 END DESC
	  , WishListAddDate DESC
	  , ProductName ASC  
    
	
	 
	 SELECT UserProductID 
		   ,UserID 
		   ,ProductID  
		   ,WishListAddDate    
		  ,ProductName  
		  ,productimagepath
		  ,ProductPrice
		  ,SalePrice
		  ,RetailFlag=CASE WHEN RetailFlag>0 THEN 0 ELSE 0 END
		  ,CouponSaleFlag 
		  ,HotDealFlag =CASE WHEN HotDealFlag>0 THEN 0 ELSE 0 END
		  ,PushNotifyFlag
		  ,ProductDescription 
	 INTO #Coupons
	 FROM #Wishlist 
	 Where ( CouponSaleFlag >0 AND (RetailFlag >0 OR HotDealFlag >0))
	  
	 SELECT UserProductID 
		  ,UserID 
		  ,ProductID  
		  ,WishListAddDate    
		  ,ProductName  
		  ,productimagepath
		  ,ProductPrice
		  ,SalePrice
		  ,RetailFlag=CASE WHEN RetailFlag>0 THEN 0 ELSE 0 END
		  ,CouponSaleFlag =CASE WHEN CouponSaleFlag>0 THEN 0 ELSE 0 END
		  ,HotDealFlag
		  ,PushNotifyFlag
		  ,ProductDescription 
	 INTO #Hotdeals
	 FROM #Wishlist 
	 Where ( HotDealFlag >0 AND (RetailFlag >0 OR CouponSaleFlag >0))
	 
	 SELECT UserProductID 
		  ,UserID 
		  ,ProductID  
		  ,WishListAddDate    
		  ,ProductName  
		  ,productimagepath
		  ,ProductPrice
		  ,SalePrice
		  ,RetailFlag
		  ,CouponSaleFlag =CASE WHEN CouponSaleFlag>0 THEN 0 ELSE 0 END
		  ,HotDealFlag=CASE WHEN HotDealFlag>0 THEN 0 ELSE 0 END
		  ,PushNotifyFlag
		  ,ProductDescription 
	 INTO #RetailFlag
	 FROM #Wishlist 
	 Where ( RetailFlag >0 AND (HotDealFlag >0 OR CouponSaleFlag >0))
	 
	 DELETE FROM #Wishlist 
	 Where ((CouponSaleFlag >0 AND (RetailFlag >0 OR HotDealFlag >0)) OR ( HotDealFlag >0 AND (RetailFlag >0 OR CouponSaleFlag >0)) OR ( RetailFlag >0 AND (HotDealFlag >0 OR CouponSaleFlag >0)))
	 
	 
	
     
     --Inserting to final wishlist
     INSERT INTO #WishlistList(UserProductID 
						  ,UserID 
						  ,ProductID  
						  ,WishListAddDate    
						  ,ProductName  
						  ,productimagepath
						  ,ProductPrice
						  ,SalePrice
						  ,RetailFlag
						  ,CouponSaleFlag 
						  ,HotDealFlag
						  ,PushNotifyFlag
						  ,ProductDescription) 
	 SELECT UserProductID 
		  ,UserID 
		  ,ProductID  
		  ,WishListAddDate    
		  ,ProductName  
		  ,productimagepath
		  ,ProductPrice
		  ,SalePrice
		  ,RetailFlag
		  ,CouponSaleFlag 
		  ,HotDealFlag
		  ,PushNotifyFlag
		  ,ProductDescription
	 FROM(						  
	 
	 SELECT UserProductID   
	  , UserID   
	  , ProductID  
	  , WishListAddDate   
	  , ProductName  
	  , productimagepath   
	  , ProductPrice   
	  , SalePrice  
	  , RetailFlag
	  , CouponSaleFlag
	  , HotDealFlag
	  , PushNotifyFlag 	
	  , ProductDescription 
	 FROM #RetailFlag       
	  
	 UNION 
	 	 
	  SELECT UserProductID   
	  , UserID   
	  , ProductID  
	  , WishListAddDate   
	  , ProductName  
	  , productimagepath   
	  , ProductPrice   
	  , SalePrice  
	  , RetailFlag
	  , CouponSaleFlag
	  , HotDealFlag
	  , PushNotifyFlag 	
	  , ProductDescription 
	 FROM #Coupons 
	 
	 Union 
	 	 
	  SELECT UserProductID   
	  , UserID   
	  , ProductID  
	  , WishListAddDate   
	  , ProductName  
	  , productimagepath   
	  , ProductPrice   
	  , SalePrice  
	  , RetailFlag
	  , CouponSaleFlag
	  , HotDealFlag
	  , PushNotifyFlag 	
	  , ProductDescription 
	 FROM #Hotdeals 
	 
	 UNION 
	 
	  SELECT UserProductID   
	  , UserID   
	  , ProductID  
	  , WishListAddDate   
	  , ProductName  
	  , productimagepath   
	  , ProductPrice   
	  , SalePrice  
	  , RetailFlag
	  , CouponSaleFlag
	  , HotDealFlag
	  , PushNotifyFlag 	
	  , ProductDescription 
	 FROM #Wishlist )Wish1
	 ORDER BY CASE WHEN RetailFlag >0 THEN 1
					WHEN CouponSaleFlag >0 THEN 1
					WHEN HotDealFlag >0 THEN 1
					ELSE 0 END DESC
	, WishListAddDate DESC
	, ProductName ASC  	
      
        --To capture max row number.  
   SELECT @MaxCnt = MAX(Row_Num) FROM #WishlistList
 
  --this flag is a indicator to enable "More" button in the UI.   
  --If the flag is "1" then enable "More" button, If the flag is "0" then disable "More" button   
  SELECT @NxtPageFlag = CASE WHEN (@MaxCnt - @UpperLimit) > 0 THEN 1 ELSE 0 END      
      
      
      
      --User Tracking
      
      --Capture the alerted items when the user has configured Postal code or GPS is on.
      CREATE TABLE #Temp(AlertedProductID int,UserProductID int, RN int identity(1, 1))
     
      	  	  INSERT INTO ScanSeeReportingDatabase..AlertedProducts(MainMenuID
																  ,	UserProductID
		   														  ,	CreatedDate)
			   OUTPUT inserted.AlertedProductID,inserted.UserProductID INTO #Temp(AlertedProductID,UserProductID)													  
			   SELECT @MainMenuID 
					 ,UserProductID 
					 ,GETDATE()
			   FROM #WishlistList
			   WHERE (RetailFlag = 1 OR CouponSaleFlag = 1 OR HotDealFlag = 1)   
			   
			   INSERT INTO ScanSeeReportingDatabase..SalesList(CreatedDate
																	,MainMenuID
																	,SalePrice)
																	
			   SELECT GETDATE()
			         ,@MainMenuID 
			         ,SalePrice 
			   FROM #WishlistList 
			   WHERE RetailFlag =1  			   
			   
			   SELECT Row_Num row_Num
					  ,T.AlertedProductID alertProdID
					  , W.UserProductID   					  
					  , ProductID  
					  , WishListAddDate   
					  , ProductName  
					  , productimagepath   					  
					  , SalePrice  
					  , RetailFlag
					  , CouponSaleFlag couponSaleFlag
					  , HotDealFlag	
					  , ProductDescription				  
					  FROM #WishlistList W
					  LEFT JOIN #Temp T ON ((W.UserProductID =T.UserProductID AND T.RN = W.Row_Num))
					  WHERE Row_Num BETWEEN (@LowerLimit+1) AND @UpperLimit  
					  ORDER BY W.Row_Num ASC 
       --               GROUP BY  row_Num
					  --,T.AlertedProductID 
					  --, W.UserProductID   					  
					  --, ProductID  
					  --, WishListAddDate   
					  --, ProductName  
					  --, productimagepath   					  
					  --, SalePrice  
					  --, RetailFlag
					  --, CouponSaleFlag 
					  --, HotDealFlag	
					  --, ProductDescription
             
 END TRY  
   
 BEGIN CATCH  
   
  --Check whether the Transaction is uncommitable.  
  IF @@ERROR <> 0  
  BEGIN  
   PRINT 'Error occured in Stored Procedure usp_WebConsumerWishListDisplay.'    
   --- Execute retrieval of Error info.  
   EXEC [dbo].[usp_GetErrorInfoOutPut] @ErrorNumber = @ErrorNumber output, @ErrorMessage = @ErrorMessage output   
  END;  
     
 END CATCH;  
END;


GO
