USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierDemographicStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SupplierDemographicStatistics
Purpose					: To get the product demographic statistics used accross modules.
Example					: usp_SupplierDemographicStatistics

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st March 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierDemographicStatistics]
(
	  @SupplierID int
	, @StartDate datetime
	, @EndDate datetime	 
	, @Module int

)
AS
BEGIN

	BEGIN TRY		
	
		DECLARE @ModuleID INT
				
		CREATE TABLE #DemoStats(Gender varchar(10), Counts INT)
		
		--Check if the User selected the QR module.
		IF @Module = 1
		BEGIN			
			INSERT INTO #DemoStats(Gender, Counts)
			SELECT Gender
				 , Counts  
			FROM
			(SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
				  , COUNT(DISTINCT(UD.UserID)) Counts 
			FROM ScanSeeReportingDatabase..Scan S
			INNER JOIN scansee..Product P ON P.ProductID = S.ID
			INNER JOIN ScanSeeReportingDatabase..ScanType ST ON ST.ScanTypeID = S.ScanTypeID
			INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = S.MainMenuID
			INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
			WHERE P.ManufacturerID = @SupplierID
			AND ST.ScanType = ('Product QR')
			AND CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
			GROUP BY UD.Gender) QR
		END
		
		
		--Check if the User selected Find in the drop down
		IF @Module = 2
		BEGIN
			
			SELECT @ModuleID = ModuleID
			FROM ScanSeeReportingDatabase..Module 
			WHERE ModuleName = 'Find'
			
			
			INSERT INTO #DemoStats(Gender, Counts)
			SELECT Gender
				 ,SUM(Counts) 
			FROM(
					SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
						 , COUNT(DISTINCT(UD.UserID)) Counts 
					FROM ScanSeeReportingDatabase..ProductList PL
					INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID AND PL.ProductClick = 1
					INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID
					INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
					WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
					AND P.ManufacturerID = @SupplierID
					AND M.ModuleID = @ModuleID
					GROUP BY UD.Gender		
				
				    UNION ALL
				    
				    SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
					 , COUNT(DISTINCT(UD.UserID)) Counts
					FROM ScanSeeReportingDatabase..SalesList S
					INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
					INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
					INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID                               
					INNER JOIN scansee..UserDemographic UD ON UD.UserID=M.UserID
					WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
					AND P.ManufacturerID = @SupplierID
					AND M.ModuleID = @ModuleID
					GROUP BY UD.Gender) Find
					GROUP BY Gender 
			
		END
		
		--Check if the User selected Hot Deals in the drop down
		IF @Module = 3
		BEGIN
			
			SELECT @ModuleID = ModuleID
			FROM ScanSeeReportingDatabase..Module 
			WHERE ModuleName = 'Hot Deals'
			
			INSERT INTO #DemoStats(Gender, Counts)
			SELECT Gender
				 , Counts 
			FROM(
				SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
					 , COUNT(DISTINCT(UD.UserID)) Counts
				FROM ScanSeeReportingDatabase..HotDealList HL
				INNER JOIN scansee..ProductHotDeal HD ON HD.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
				INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID
				INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
				WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				AND HD.ManufacturerID = @SupplierID
				GROUP BY UD.Gender) HotDeals
			
		END
		
		
		--Check if the User selected This Location in the drop down		
		IF @Module = 4
		BEGIN
			SELECT @ModuleID = ModuleID
			FROM ScanSeeReportingDatabase..Module 
			WHERE ModuleName = 'This Location'
		
			INSERT INTO #DemoStats(Gender, Counts)
			SELECT Gender
				 , SUM(Counts)
			FROM (			
				            
				--Capture the Products viewed
				SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
					 , COUNT(DISTINCT(UD.UserID)) Counts 
				FROM ScanSeeReportingDatabase..ProductList PL 
				INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
				INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID				
				INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID				
				WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				AND P.ManufacturerID = @SupplierID
				AND M.ModuleID = @ModuleID
				GROUP BY UD.Gender
				
				UNION ALL
				
				--SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
				--	 , COUNT(DISTINCT(UD.UserID)) Counts
    --            FROM ScanSeeReportingDatabase..HotDealList HL
    --            INNER JOIN scansee..ProductHotDeal HD ON HL.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
    --            INNER JOIN ScanSeeReportingDatabase..MainMenu M ON HL.MainMenuID = M.MainMenuID
    --            INNER JOIN scansee..UserDemographic UD ON UD.UserID=M.UserID 
    --            WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
    --            AND HD.ManufacturerID = @SupplierID 
    --            AND M.ModuleID =@ModuleID 
    --            GROUP BY UD.Gender 
                
    --            UNION ALL 
                
                SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
					 , COUNT(DISTINCT(UD.UserID)) Counts
				FROM ScanSeeReportingDatabase..SalesList S
				INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
				INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
				INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID                 
                INNER JOIN scansee..UserDemographic UD ON UD.UserID=M.UserID
				WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				AND P.ManufacturerID = @SupplierID
				AND M.ModuleID = @ModuleID
				GROUP BY UD.Gender) Counts
                GROUP BY Gender 
				
			END	
			--Check if the User selected Scanned/Searched Products in the drop down
			IF @Module = 5
			BEGIN				
				
				INSERT INTO #DemoStats(Gender, Counts)
				SELECT Gender
					 , SUM(Counts)
				FROM (						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts 
						FROM scansee..ScanHistory S
						INNER JOIN scansee..Product P ON P.ProductID = S.ProductID  
						INNER JOIN scansee..UserDemographic UD ON UD.UserID = S.UserID				
						WHERE P.ManufacturerID = @SupplierID
						AND CAST(S.ScanDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.ScanDate AS DATE) <= CAST(@EndDate AS DATE)
						GROUP BY UD.Gender
						
						UNION ALL
						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts 
						FROM ScanSeeReportingDatabase..ProductList PL 
						INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
						INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID = M.ModuleID
						INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID						
						WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND Mo.ModuleName IN ('Scan Now', 'Shopping List', 'Find', 'Wish List')
						GROUP BY UD.Gender
						
						UNION ALL 
						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts
						FROM ScanSeeReportingDatabase..SalesList S
						INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
						INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID
						INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID =M.ModuleID 
						INNER JOIN scansee..UserDemographic UD ON UD.UserID=M.UserID
						WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND Mo.ModuleName IN ('Scan Now', 'Shopping List', 'Find', 'Wish List')
						GROUP BY UD.Gender)Scans
					GROUP BY Gender
				
		    END
		    
		    --Check if the User selected Shopping List in the drop down
			IF @Module = 6
			BEGIN
				
				SELECT @ModuleID = ModuleID
				FROM ScanSeeReportingDatabase..Module 
				WHERE ModuleName = 'Shopping List'
				
				INSERT INTO #DemoStats(Gender, Counts)
				SELECT Gender
					 , SUM(Counts)
				FROM (						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts				
						FROM ScanSeeReportingDatabase..UserProduct UP
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = UP.TodayShoppingListMainMenuID AND UP.TodayListtItem = 1
						INNER JOIN scansee..Product PR ON PR.ProductID= UP.ProductID
						INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
						WHERE Up.TodayListAddDate >= @StartDate 
						AND PR.ManufacturerID = @SupplierID
						GROUP BY UD.Gender
						
						UNION ALL
						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts
						FROM ScanSeeReportingDatabase..ProductList PL 
						INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
						INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
						WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND M.ModuleID = @ModuleID
						GROUP BY UD.Gender
						
						UNION ALL
						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts
						FROM ScanSeeReportingDatabase..SalesList S
						INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
						INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID
						INNER JOIN scansee..UserDemographic UD ON UD.UserID=M.UserID
						WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND M.ModuleID = @ModuleID
						GROUP BY UD.Gender) Shopping	
					GROUP BY Gender			
			END
			
			--Check if the User selected Shopping List in the drop down
			IF @Module = 7
			BEGIN
				
				SELECT @ModuleID = ModuleID
				FROM ScanSeeReportingDatabase..Module 
				WHERE ModuleName = 'Wish List'
				
				INSERT INTO #DemoStats(Gender, Counts)
				SELECT Gender
					 , SUM(Counts)
				FROM (						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts		
						FROM ScanSeeReportingDatabase..UserProduct UP
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = UP.WishListMainMenuID AND UP.WishListItem = 1
						INNER JOIN scansee..Product PR ON PR.ProductID= UP.ProductID
						INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
						WHERE UP.WishListAddDate >= @StartDate 
						AND PR.ManufacturerID = @SupplierID
						GROUP BY UD.Gender
						
						UNION ALL
							
						--Capture the products in the product list (from SLR associations.)
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts	
						FROM ScanSeeReportingDatabase..ProductList PL 
						INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
						INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
						WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND M.ModuleID = @ModuleID
						GROUP BY UD.Gender
						
						UNION ALL
						
						SELECT Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
							 , COUNT(DISTINCT(UD.UserID)) Counts
						FROM ScanSeeReportingDatabase..SalesList S
						INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
						INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID                          
						INNER JOIN scansee..UserDemographic UD ON UD.UserID=M.UserID
						WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND M.ModuleID = @ModuleID
						GROUP BY UD.Gender) Wish
					GROUP BY Gender				
			END			 
            
            --Dispaly the demographics.
			SELECT Gender
				 , Counts
			FROM #DemoStats
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SupplierDemographicStatistics.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;

GO
