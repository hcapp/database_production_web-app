USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierThisLocationGeographicalStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_SupplierThisLocationGeographicalStatistics
Purpose                 : To get the geographical statistics of the product.
Example                 : usp_SupplierThisLocationGeographicalStatistics

History
Version           Date              Author               Change Description
------------------------------------------------------------------------------ 
1.0               29th March 2013   Pavan Sharma K           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierThisLocationGeographicalStatistics]
(
        @SupplierID int
      , @ProductID int
      , @StartDate datetime
      , @EndDate datetime              
)
AS
BEGIN

      BEGIN TRY
			 
			DECLARE @ModuleID INT
			
			SELECT @ModuleID = ModuleID
			FROM Module 
			WHERE ModuleName = 'This Location'
			
            
            SELECT City
				 , State
				 , Latitude
				 , Longitude
				 , Counts
				 , GEOGRAPHY::STPointFromText('POINT (' + CAST(Longitude AS VARCHAR(20)) + ' ' + CAST(Latitude AS VARCHAR(20)) + ')', 4326) AS GeoRef
			FROM
			(
			SELECT ISNULL(MAX(G.City), MAX(G1.City))City
				 , ISNULL(MAX(G.State), MAX(G1.State))State
				 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
				 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
				 , COUNT(DISTINCT(SL.SaleListID))Counts
            FROM ScanSeeReportingDatabase..SalesList SL
            INNER JOIN scansee..RetailLocationDeal RLD ON RLD.RetailLocationDealID = SL.RetailLoactionDealID AND RetailLocationDealClick = 1
            INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
            INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = SL.MainMenuID
            LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
            LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
            WHERE P.ProductID = @ProductID
            AND P.ManufacturerID = @SupplierID
            AND M.ModuleID = @ModuleID
            AND CAST(SL.CreatedDate AS DATE) >= @StartDate AND CAST(SL.CreatedDate AS DATE) <= @EndDate
            GROUP BY G.City, G1.City, G.State, G1.State) ThisLoc
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierThisLocationGeographicalStatistics'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
