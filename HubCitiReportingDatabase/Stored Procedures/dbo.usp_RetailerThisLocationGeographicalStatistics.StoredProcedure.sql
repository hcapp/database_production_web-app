USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerThisLocationGeographicalStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerThisLocationGeographicalStatistics
Purpose                 : To get the geographical statistics of the product.
Example                 : usp_RetailerThisLocationGeographicalStatistics

History
Version           Date              Author               Change Description
------------------------------------------------------------------------------ 
1.0               5th April         Dhananjaya TR        Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerThisLocationGeographicalStatistics]
(
        @RetailID int
      , @ProductID int
      , @StartDate datetime
      , @EndDate datetime              
)
AS
BEGIN

      BEGIN TRY
			 
			DECLARE @ModuleID INT
			
			SELECT @ModuleID = ModuleID
			FROM Module 
			WHERE ModuleName = 'This Location'
			
            
           
			SELECT ISNULL(MAX(G.City), MAX(G1.City))City
				 , ISNULL(MAX(G.State), MAX(G1.State))State
				 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
				 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
				 , COUNT(DISTINCT(PL.ProductListID))Counts
            INTO #ScanseeData
			FROM ScanSeeReportingDatabase..ProductList PL
            INNER JOIN Scansee..Product P ON PL.ProductID = P.ProductID AND PL.ProductClick = 1
            INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
            INNER JOIN Scansee..RetailLocationProduct RLP ON RLP.ProductID =P.ProductID 
            INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID 
            LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
            LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
            WHERE PL.ProductID = @ProductID
            AND RL.RetailID = @RetailID
            AND M.ModuleID = @ModuleID
            AND PL.CreatedDate >= @StartDate AND PL.CreatedDate <= @EndDate
            GROUP BY G.City, G1.City, G.State, G1.State
      

			SELECT ISNULL(MAX(G.City), MAX(G1.City))City
				 , ISNULL(MAX(G.State), MAX(G1.State))State
				 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
				 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
				 , COUNT(DISTINCT(PL.ProductListID))Counts
            INTO #HubcitiData
			FROM HubCitiReportingDatabase..ProductList PL
            INNER JOIN Scansee..Product P ON PL.ProductID = P.ProductID AND PL.ProductClick = 1
            INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
			INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
			INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID AND L.LinkTypeName LIKE 'Whats NearBy'
            INNER JOIN Scansee..RetailLocationProduct RLP ON RLP.ProductID =P.ProductID 
            INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID 
            LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
            LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
            WHERE PL.ProductID = @ProductID
            AND RL.RetailID = @RetailID
           -- AND M.ModuleID = @ModuleID
            AND PL.DateCreated  >= @StartDate AND PL.DateCreated <= @EndDate
            GROUP BY G.City, G1.City, G.State, G1.State
	  


			--SELECT City
			--	 , State
			--	 , Latitude
			--	 , Longitude
			--	 , Counts
			--	 , GEOGRAPHY::STPointFromText('POINT (' + CAST(Longitude AS VARCHAR(20)) + ' ' + CAST(Latitude AS VARCHAR(20)) + ')', 4326) AS GeoRef
			--FROM
			--	(

	        
			 SELECT City=ISNULL(S.City,H.City)
				 , State=ISNULL(S.State,H.State)
				 , Latitude=ISNULL(S.Latitude,H.Latitude)
				 , Longitude=ISNULL(S.Longitude,H.Longitude)
				 , Counts=CASE WHEN S.City =H.City AND S.State =H.State THEN S.Counts + H.Counts 
				               WHEN S.Counts IS NOT NULL AND H.Counts IS NULL THEN S.Counts 
							   WHEN S.Counts IS NULL AND H.Counts IS NOT NULL THEN H.Counts END
				 , GEOGRAPHY::STPointFromText('POINT (' + CAST(ISNULL(S.Longitude,H.Longitude) AS VARCHAR(20)) + ' ' + CAST(ISNULL(S.Latitude,H.Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
			FROM #ScanseeData S
			FULL JOIN #HubcitiData H ON S.City =H.City AND S.State =H.State 
				  
	  END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_RetailerThisLocationGeographicalStatistics'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;






GO
