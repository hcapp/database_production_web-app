USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerThisLocationDemographicsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerThisLocationDemographicsStatistics
Purpose                 : To get the demographic statistics of the product.
Example                 : usp_RetailerThisLocationDemographicsStatistics

History
Version           Date              Author               Change Description
------------------------------------------------------------------------------ 
1.0               5th April 2013    Dhananjaya TR        Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerThisLocationDemographicsStatistics]
(
        @RetailID int
      , @ProductID int
      , @StartDate datetime
      , @EndDate datetime              
)
AS
BEGIN

      BEGIN TRY
			 
			DECLARE @ModuleID INT
			
			SELECT @ModuleID = ModuleID
			FROM Module 
			WHERE ModuleName = 'This Location'	  
            
			SELECT  Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
				  , Counts = COUNT(DISTINCT(UD.UserID))
            INTO #ScanseeUsers
			FROM ScanSeeReportingDatabase..ProductList PL
            INNER JOIN Scansee..Product P ON PL.ProductID = P.ProductID AND PL.ProductClick = 1
            INNER JOIN Scansee..RetailLocationProduct RLP ON RLP.ProductID =P.ProductID 
            INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID 
            INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
            INNER JOIN Scansee..UserDemographic UD ON UD.UserID = M.UserID
            WHERE PL.ProductID = @ProductID
            AND RL.RetailID = @RetailID 
            AND M.ModuleID = @ModuleID
            AND PL.CreatedDate >= @StartDate AND PL.CreatedDate <= @EndDate
            GROUP BY UD.Gender
            
			--select * from #ScanseeUsers
			

			SELECT  Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
				  , Counts = COUNT(DISTINCT(UD.HcUserID))
			INTO #HubcitiUsers
            FROM HubCitiReportingDatabase..ProductList PL
            INNER JOIN Scansee..Product P ON PL.ProductID = P.ProductID AND PL.ProductClick = 1
            INNER JOIN Scansee..RetailLocationProduct RLP ON RLP.ProductID =P.ProductID 
            INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID =RLP.RetailLocationID 
            INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
			INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID
			INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID AND L.LinkTypeName ='Whats NearBy' 
            INNER JOIN Scansee..HcUser UD ON UD.HcUserID = M.HcUserID
            WHERE PL.ProductID = @ProductID
            AND RL.RetailID = @RetailID            
            AND PL.DateCreated >= @StartDate AND PL.DateCreated <= @EndDate
            GROUP BY UD.Gender	
			
			SELECT ISNULL(S.Gender,H.Gender )
					,Counts=CASE WHEN S.Gender IS NOT NULL AND H.Gender IS NOT NULL THEN S.Counts + H.Counts 
							     WHEN S.Gender IS NOT NULL AND H.Gender IS NULL THEN S.Counts
						         WHEN S.Gender IS NULL AND H.Gender IS NOT NULL THEN H.Counts END 
			FROM #ScanseeUsers S
			FULL JOIN #HubcitiUsers H ON H.Gender =S.Gender 	
			
			            
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_RetailerThisLocationDemographicsStatistics'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;







GO
