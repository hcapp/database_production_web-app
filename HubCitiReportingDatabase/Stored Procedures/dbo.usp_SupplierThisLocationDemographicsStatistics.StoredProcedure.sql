USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierThisLocationDemographicsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_SupplierThisLocationDemographicsStatistics
Purpose                 : To get the demographic statistics of the product.
Example                 : usp_SupplierThisLocationDemographicsStatistics

History
Version           Date              Author               Change Description
------------------------------------------------------------------------------ 
1.0               29th March 2013   Pavan Sharma K          Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierThisLocationDemographicsStatistics]
(
        @SupplierID int
      , @ProductID int
      , @StartDate datetime
      , @EndDate datetime              
)
AS
BEGIN

      BEGIN TRY
			 
			DECLARE @ModuleID INT
			
			SELECT @ModuleID = ModuleID
			FROM Module 
			WHERE ModuleName = 'This Location'			
            
            
			SELECT  Gender = CASE WHEN UD.Gender = 0 THEN 'Women' ELSE 'Men' END
				  , Counts = COUNT(DISTINCT(UD.UserID))
            FROM ScanSeeReportingDatabase..SalesList SL
            INNER JOIN scansee..RetailLocationDeal RLD ON RLD.RetailLocationDealID = SL.RetailLoactionDealID AND RetailLocationDealClick = 1
            INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
            INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = SL.MainMenuID
            INNER JOIN scansee..UserDemographic UD ON UD.UserID = M.UserID
            WHERE RLD.ProductID = @ProductID
            AND P.ManufacturerID = @SupplierID
            AND M.ModuleID = @ModuleID
            AND CAST(SL.CreatedDate AS DATE) >= @StartDate AND CAST(SL.CreatedDate AS DATE) <= @EndDate
            GROUP BY UD.Gender
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierThisLocationDemographicsStatistics'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
