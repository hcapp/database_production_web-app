USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierProductUsageInfo]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_SupplierProductUsageInfo
Purpose                 : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                 : usp_SupplierProductUsageInfo

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               27th March 2013   Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierProductUsageInfo]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime   
      
      
)
AS
BEGIN

      BEGIN TRY
      
            DECLARE @TotalProduct INT=0
            DECLARE @TotalProduct1 INT=0
            DECLARE @ShoppingPercentage float=0
            DECLARE @WishListPercentage float  =0          
            DECLARE @LastShoppingPercentage float=0
            DECLARE @LastWishListPercentage float=0
            DECLARE @percentageChange float =0
            DECLARE @ChangeInWishlist Float =0      
      
            DECLARE @TotalClickCount INT=0
            DECLARE @ShoppingListCount INT=0
            DECLARE @WishListCount INT=0
           -- DECLARE @percentageChange INT            
            DECLARE @LastStartdate datetime
            DECLARE @LastEnddate datetime
            DECLARE @Difference int=Datediff(DAY,@StartDate,@EndDate)
            SET @LastStartdate =@StartDate-(@Difference+1)
            SET @LastEnddate =@EndDate-(@Difference+1) 
            
            SELECT MIN(UserProductID) userproductid
            INTO #UserProduct
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON P.ProductID = U.ProductID 
            WHERE P.ManufacturerID =@SupplierID
            AND (CAST(U.TodayListAddDate AS DATE)>= CAST(@StartDate AS DATE) AND CAST(U.TodayListAddDate AS DATE)<= CAST(@EndDate AS DATE))
            AND TodayListtItem=1
            GROUP BY U.ProductID           
       
            SELECT MIN(UserProductID) userproductid
            INTO #UserWishListProduct
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON P.ProductID = U.ProductID 
            WHERE P.ManufacturerID =@SupplierID
            AND (CAST(U.WishListAddDate AS DATE)>= CAST(@StartDate AS DATE) AND CAST(U.WishListAddDate AS DATE)<= CAST(@EndDate AS DATE)) 
            AND WishListItem =1
            GROUP BY U.ProductID 
                             
            SELECT @TotalProduct=COUNT(1) FROM scansee..Product 
            WHERE ManufacturerID =@SupplierID 
            AND ProductAddDate IS NOT NULL
            AND CAST(ProductAddDate AS DATE) <=CAST(@EndDate AS DATE)                                    
                              
                                   
            SELECT @ShoppingListCount=COUNT(CASE WHEN U.TodayListtItem  =1 AND  CAST(U.TodayListAddDate AS DATE)>= CAST(@StartDate AS DATE) AND CAST(U.TodayListAddDate AS DATE)<= CAST(@EndDate AS DATE) THEN 1 END)                 
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON U.ProductID =P.ProductID 
            INNER JOIN #UserProduct UP ON U.UserProductID =UP.userproductid             
            WHERE P.ManufacturerID =@SupplierID            
            
            SELECT @WishListCount =COUNT(CASE WHEN U.WishListItem =1 AND CAST(U.WishListAddDate AS DATE)>= CAST(@StartDate AS DATE) AND CAST(U.WishListAddDate AS DATE)<= CAST(@EndDate AS DATE) THEN 1 END) 
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON U.ProductID =P.ProductID 
            INNER JOIN #UserWishListProduct  UP ON U.UserProductID =UP.userproductid             
            WHERE P.ManufacturerID =@SupplierID                     
                       
            IF(@TotalProduct>0)
            BEGIN
				SET @ShoppingPercentage =ROUND((CAST(@ShoppingListCount*100 AS FLOAT)/@TotalProduct),2)
				SET @WishListPercentage =ROUND((CAST((@WishListCount*100) AS FLOAT)/@TotalProduct),2)
            END
                                      
            SELECT @TotalProduct1=COUNT(1) FROM scansee..Product 
            WHERE ManufacturerID =@SupplierID 
            AND ProductAddDate IS NOT NULL  
            AND CAST(ProductAddDate AS DATE) <=CAST(@LastEnddate AS DATE)  
            
            
            SELECT MIN(UserProductID) userproductid
            INTO #UserWishProduct1
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON P.ProductID = U.ProductID 
            WHERE P.ManufacturerID =@SupplierID
            AND (CAST(U.WishListAddDate AS DATE)>= CAST(@LastStartdate AS DATE) AND CAST(U.WishListAddDate AS DATE)<= CAST(@LastEnddate AS DATE))
            AND WishListItem =1
            GROUP BY U.ProductID
            
            SET  @ShoppingListCount =0
            SET @WishListCount=0
             
            SELECT MIN(UserProductID) userproductid
            INTO #UserProduct1
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON P.ProductID = U.ProductID 
            WHERE P.ManufacturerID =@SupplierID
            AND (CAST(U.TodayListAddDate AS DATE)>= CAST(@LastStartdate AS DATE) AND CAST(U.TodayListAddDate AS DATE)<= CAST(@LastEnddate AS DATE))
            AND TodayListtItem =1
            GROUP BY U.ProductID          
             
             
                      
            SELECT @ShoppingListCount=COUNT(CASE WHEN U.TodayListtItem =1 AND CAST(U.TodayListAddDate AS DATE)>= CAST(@LastStartdate AS DATE) AND CAST(U.TodayListAddDate AS DATE)<= CAST(@LastEnddate AS DATE) THEN 1 END)                  
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON U.ProductID =P.ProductID 
            INNER JOIN #UserProduct1 UP ON UP.userproductid =U.UserProductID 
            WHERE P.ManufacturerID =@SupplierID  
           
            SELECT @WishListCount =COUNT(CASE WHEN U.WishListItem =1 AND CAST(U.WishListAddDate AS DATE)>= CAST(@LastStartdate AS DATE) AND CAST(U.WishListAddDate AS DATE)<= CAST(@LastEnddate AS DATE) THEN 1 END) 
            FROM ScanSeeReportingDatabase..UserProduct U
            INNER JOIN scansee..Product P ON U.ProductID =P.ProductID 
            INNER JOIN #UserWishProduct1 UP ON UP.userproductid =U.UserProductID 
            WHERE P.ManufacturerID =@SupplierID
           
                      
            IF(@TotalProduct1>0)
            BEGIN
				SET @LastShoppingPercentage =ROUND((CAST(@ShoppingListCount AS FLOAT)*100/@TotalProduct1),2)
				SET @LastWishListPercentage =ROUND((CAST(@WishListCount AS FLOAT)*100/@TotalProduct1),2)
			END
			
			           
            SET @percentageChange =@ShoppingPercentage-ISNULL(@LastShoppingPercentage,0) 
            SET @ChangeInWishlist =@WishListPercentage-ISNULL(@LastWishListPercentage,0) 
            
            CREATE TABLE #Temp(Typess varchar(1000), Percentageuse float,PercentageChange float)  
            INSERT INTO #Temp(Typess,Percentageuse,PercentageChange)
            SELECT 'Shopping List',@ShoppingPercentage,(@ShoppingPercentage-@LastShoppingPercentage)
            UNION ALL
            SELECT 'Wish List',@WishListPercentage,(@WishListPercentage-@LastWishListPercentage)
        
        SELECT Typess Ty
              ,Percentageuse Percentageuse
              ,PercentageChange Change
        FROM #Temp 
        
            
                      
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierAppsiteTrackingInfo.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
