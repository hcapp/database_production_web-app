USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierCategoryProductsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                             : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                             : usp_SupplierCategoryProductsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               2nd April 2013    Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierCategoryProductsStatistics]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime   
      
      
)
AS
BEGIN

      BEGIN TRY     
                        
            CREATE TABLE #Temp(productid int,impressions int,Uniqueusers int, ProductName varchar(255))

			INSERT INTO #Temp(productid ,impressions ,Uniqueusers, ProductName)
			SELECT DISTINCT L.ProductID			      
			       ,COUNT(1) impression			       
			       ,COUNT(DISTINCT UserID)  
			       , P.ProductName      
			FROM ProductList L
			INNER JOIN scansee..Product P ON P.ProductID=L.ProductID 
			INNER JOIN MainMenu M ON M.MainMenuID =L.MainMenuID 
			WHERE ManufacturerID = @SupplierID  
			AND CAST(L.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(L.CreatedDate AS Date)<=CAST(@EndDate AS Date)
			GROUP BY L.ProductID, P.ProductName
			ORDER BY COUNT(1)  DESC
			
			
			CREATE TABLE #Temp1(productid int,impressions int,Uniqueusers int,  ProductName varchar(255))

			INSERT INTO #Temp1(productid ,impressions ,Uniqueusers,  ProductName)
			SELECT DISTINCT P.ProductID			      
			       ,COUNT(1) impression			       
			       ,COUNT(DISTINCT UserID)
			       , P.ProductName              
			FROM scansee..Product P 
            INNER JOIN Scansee..RetailLocationDeal RD ON RD.ProductID =P.ProductID 
            INNER JOIN SalesList S ON S.RetailLoactionDealID =RD.RetailLocationDealID   
            INNER JOIN MainMenu M ON M.MainMenuID = S.MainMenuID            
            WHERE ManufacturerID =@SupplierID 
            AND CAST(S.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(S.CreatedDate AS Date)<=CAST(@EndDate AS Date)
			GROUP BY P.ProductID, P.ProductName      
			ORDER BY COUNT(1)  DESC 
			
			SELECT (CASE WHEN T.productid =TT.productid THEN T.productid WHEN T.productid IS NULL THEN TT.productid ELSE T.productid END) productid
			      , (CASE WHEN T.productid =TT.productid THEN t. ProductName WHEN T.productid IS NULL THEN TT.ProductName ELSE T.ProductName END) ProductName
			      ,impressions = (CASE WHEN T.productid =TT.productid THEN T.impressions +TT.impressions  WHEN T.impressions  IS NULL THEN TT.impressions  ELSE T.impressions END)
			      ,Uniqueusers = (CASE WHEN T.productid =TT.productid THEN T.Uniqueusers + TT.Uniqueusers  WHEN T.Uniqueusers  IS NULL THEN TT.Uniqueusers  ELSE T.Uniqueusers END)
			FROM #Temp T
		    FULL OUTER JOIN #Temp1 TT ON T.productid = TT.productid
			
			
			
			
			
            --SELECT T.productid 
            --      ,P.ProductName 
            --      ,impressions 
            --      ,Uniqueusers             
            --FROM #Temp1 T
            --INNER JOIN scansee..Product P ON P.ProductID=T.ProductID 
            --ORDER BY impressions DESC,ProductName             
                 
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierCategoryProductsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
