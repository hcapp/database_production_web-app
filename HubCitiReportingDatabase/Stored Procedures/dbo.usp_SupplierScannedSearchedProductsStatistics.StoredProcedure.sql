USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierScannedSearchedProductsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                             : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                             : usp_SupplierScannedSearchedProductsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               29th March 2013   Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierScannedSearchedProductsStatistics]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime      
)
AS
BEGIN

      BEGIN TRY
      
            DECLARE @TotalProduct INT=0     
            DECLARE @TotalClickCount INT
            DECLARE @ShoppingListCount INT
            DECLARE @WishListCount INT
            -- DECLARE @percentageChange INT            
            DECLARE @LastStartdate datetime
            DECLARE @LastEnddate datetime
            DECLARE @Difference int=Datediff(DAY,@StartDate,@EndDate)
            SET @LastStartdate =@StartDate-(@Difference+1)
            SET @LastEnddate =@EndDate-(@Difference+1) 
            
 
            --Capture the total product for a selected supplier 
            CREATE TABLE #Temp(Totalproduct int,productid int)
            INSERT INTO #Temp (Totalproduct,productid)               
            SELECT COUNT(1)
                   ,H.ProductID  
            FROM scansee..Product P
            INNER JOIN scansee..ScanHistory H ON P.ProductID =H.ProductID  
            WHERE ManufacturerID =@SupplierID 
            AND CAST(H.ScanDate AS Date)>=CAST(@StartDate AS Date) AND CAST(H.ScanDate AS Date)<=CAST(@EndDate AS Date)
            GROUP BY H.ProductID
            ORDER BY COUNT(1)
            
            
            --Capture number of times product seached.
            SELECT COUNT(1) Totalproduct
                  ,P.ProductID productid
            INTO #Searched 
            FROM ProductList L
            INNER JOIN scansee..Product P ON P.ProductID=L.ProductID
            INNER JOIN MainMenu M ON M.MainMenuID =L.MainMenuID 
            INNER JOIN Module MO ON MO.ModuleID =M.ModuleID 
            WHERE ManufacturerID =@SupplierID 
            AND L.ProductID IS NOT NULL
            AND MO.ModuleName IN ('Scan Now', 'Shopping List', 'Wish List', 'Find')
            AND CAST(L.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(L.CreatedDate AS Date)<=CAST(@EndDate AS Date)
            GROUP BY P.ProductID 
            ORDER BY COUNT(1)
            
            SELECT SUM(a.Totalproduct) Totalproduct
                  ,a.productid 
            INTO #TotalCount
            FROM
            (SELECT Totalproduct 
                  ,productid             
            FROM #Temp 
            UNION ALL
            SELECT Totalproduct 
                  ,productid             
            FROM #Searched)a
            GROUP BY a.productid 
           -- ORDER BY a.Totalproduct DESC
            
          
            
            
            CREATE TABLE #Temp1(Totalproduct int,productid int)
            INSERT INTO #Temp1 (Totalproduct,productid)   
            SELECT COUNT(1)
                  ,H.ProductID  
            FROM scansee..Product P
            INNER JOIN scansee..ScanHistory H ON P.ProductID =H.ProductID  
            WHERE ManufacturerID =@SupplierID 
            AND CAST(H.ScanDate AS Date)>=CAST(@LastStartdate AS Date) AND CAST(H.ScanDate AS Date)<=CAST(@LastEnddate AS Date)
            AND H.ProductID IS NOT NULL
            GROUP BY H.ProductID 
            ORDER BY COUNT(1)
         
            SELECT COUNT(1)  Totalproduct
                  ,P.ProductID productid
            INTO #PreSearched 
            FROM ProductList L
            INNER JOIN scansee..Product P ON P.ProductID=L.ProductID
            INNER JOIN MainMenu M ON M.MainMenuID =L.MainMenuID 
            INNER JOIN Module MO ON MO.ModuleID =M.ModuleID 
            WHERE ManufacturerID =@SupplierID 
            AND L.ProductID IS NOT NULL
            AND MO.ModuleName IN ('Scan Now', 'Shopping List', 'Wish List', 'Find') 
            AND CAST(L.CreatedDate AS Date)>=CAST(@LastStartdate AS Date) AND CAST(L.CreatedDate AS Date)<=CAST(@LastEnddate AS Date)
            GROUP BY P.ProductID 
            ORDER BY COUNT(1)
            
                      
            
            
            SELECT SUM(b.Totalproduct)Totalproduct
                  ,b.productid 
            INTO #TotalPreCount
            FROM 
            (SELECT Totalproduct 
                  ,productid 
            FROM #Temp1 
            UNION ALL
            SELECT Totalproduct 
                  ,productid 
            FROM #PreSearched)b
            GROUP BY b.productid 
          --  ORDER BY b.Totalproduct DESC
            
           -- select * from #TotalPreCount
            
            SELECT P.ProductName 
                  ,T.productid 
                  ,T.Totalproduct   
                  ,(CASE WHEN T.Totalproduct IS NOT NULL AND TP.Totalproduct IS NOT NULL AND T.Totalproduct>=TP.Totalproduct  THEN (((T.Totalproduct-TP.Totalproduct)*100)/T.Totalproduct) WHEN T.Totalproduct IS NOT NULL AND TP.Totalproduct IS NOT NULL AND T.Totalproduct<TP.Totalproduct  THEN (((T.Totalproduct-TP.Totalproduct)*100)/TP.Totalproduct) WHEN T.Totalproduct IS NOT NULL THEN 100 ELSE -100 END) change              
            FROM #TotalCount T 
            INNER JOIN scansee..Product P ON P.ProductID =T.productid 
            LEFT JOIN #TotalPreCount TP ON TP.productid =P.ProductID
            ORDER BY T.Totalproduct DESC        
            
            
            --SELECT ISNULL(T.productid,ISNULL(S.ProductID,ISNULL(T1.productid,ISNULL(PS.ProductID,0))))
            --      ,P.ProductName ProductName
            --      ,(ISNULL(T.Totalproduct,0)+ISNULL(S.Impressions,0)) Totalproduct
            --      ,((((ISNULL(T.Totalproduct,0)+ISNULL(S.Impressions,0)) -(ISNULL(T1.Totalproduct,0)+ISNULL(PS.Impressions,0)))*100)/T1.Totalproduct) Change
            --      --,((ISNULL(T.Totalproduct,0)*100)/T1.Totalproduct) Change
            --FROM scansee..Product P /
            --LEFT JOIN #Temp T ON P.ProductID =T.productid 
            --LEFT JOIN #Searched S ON S.ProductID =P.ProductID 
            --LEFT JOIN #Temp1 T1 ON T.productid =T1.productid
            --LEFT JOIN #PreSearched PS ON P.ProductID =PS.ProductID  
            --WHERE (T.productid=P.ProductID OR S.ProductID =P.ProductID OR T1.productid=P.ProductID OR PS.ProductID =P.ProductID) AND P.ProductID IS NOT NULL
      
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierScannedSearchedProductsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
