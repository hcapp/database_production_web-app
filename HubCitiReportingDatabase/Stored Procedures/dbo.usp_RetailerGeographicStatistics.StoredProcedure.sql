USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerGeographicStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name               : usp_RetailerGeographicStatistics
Purpose                             : To get the product geographical statistics from different modules.
Example                             : usp_RetailerGeographicStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               8th April 2013    SPAN           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerGeographicStatistics]
(
        @RetailID int
      , @Module int
      , @StartDate datetime
      , @EndDate datetime     

)
AS
BEGIN

      BEGIN TRY         
      
            DECLARE @ModuleID INT
            CREATE TABLE #GeoStats(Counts INT, City VARCHAR(100), State VARCHAR(10), GeoRef Geography)
           
           --Check if the User selected the QR module.
			IF @Module = 1
			BEGIN			
				INSERT INTO #GeoStats(Counts, State, GeoRef)
				SELECT SUM(Counts)
					 --, City
					 , State
					 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
				FROM
				(SELECT COUNT(DISTINCT(S.ScanID)) Counts
					 , ISNULL(G.City, G1.City) City
					 , ISNULL(G.State, G1.State) [State]
					 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
					 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
				FROM ScanSeeReportingDatabase..Scan S
				INNER JOIN ScanSeeReportingDatabase..ScanType ST ON ST.ScanTypeID = S.ScanTypeID 
				INNER JOIN ScanSeeReportingDatabase..RetailerList RRL ON RRL.RetailLocationID = S.ID
				--LEFT OUTER JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID =S.ID AND QRTypeID IN (SELECT QRTypeID FROM Scansee..QRTypes WHERE QRTypeName  in('Anything Page','Special Offer Page')) AND Q.RetailID =@RetailID 
				INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID = RRL.RetailLocationID AND RRL.MainMenuID = S.MainMenuID
				INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = S.MainMenuID
				LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
				LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
				WHERE ST.ScanType IN ('Retailer Anything Page QR Code','Retailer Special Offer QR Code','Retailer Summary QR Code')
				AND RL.RetailID = @RetailID
				AND CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				GROUP BY G.City, G1.City, G.State, G1.State
				
				UNION ALL

				--Hubciti DashBoard Implementation
				SELECT COUNT(DISTINCT(S.ScanID)) Counts
					 , ISNULL(G.City, G1.City) City
					 , ISNULL(G.State, G1.State) [State]
					 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
					 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
				FROM HubCitiReportingDatabase..Scan S
				INNER JOIN HubCitiReportingDatabase..ScanType ST ON ST.ScanTypeID = S.ScanTypeID 
				INNER JOIN HubCitiReportingDatabase..RetailerList RRL ON RRL.RetailLocationID = S.ID
				--LEFT OUTER JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID =S.ID AND QRTypeID IN (SELECT QRTypeID FROM Scansee..QRTypes WHERE QRTypeName  in('Anything Page','Special Offer Page')) AND Q.RetailID =@RetailID 
				INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID = RRL.RetailLocationID AND RRL.MainMenuID = S.MainMenuID
				INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = S.MainMenuID
				LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
				LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
				WHERE ST.ScanType IN ('Retailer Anything Page QR Code','Retailer Special Offer QR Code','Retailer Summary QR Code')
				AND RL.RetailID = @RetailID
				AND CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				GROUP BY G.City, G1.City, G.State, G1.State) QR

				GROUP BY State

			END         
            
            --Check if the User selected Hot Deals in the drop down 
            IF @Module = 3
            BEGIN
                  
                  SELECT @ModuleID = ModuleID
                  FROM ScanSeeReportingDatabase..Module 
                  WHERE ModuleName = 'Hot Deals'
                  
                  INSERT INTO #GeoStats(Counts, State, GeoRef)
                  SELECT SUM(Counts)
                        --, City
                        , State
                        , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
                  FROM (
                  SELECT COUNT(DISTINCT(HL.HotDealListID)) Counts
                        , ISNULL(G.City, G1.City) City
                        , ISNULL(G.State, G1.State) [State]
                        , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                        , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                  FROM ScanSeeReportingDatabase..HotDealList HL
                  INNER JOIN Scansee..ProductHotDeal HD ON HD.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
                  INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID                 
                  LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                  LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
                  WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                  AND HD.RetailID = @RetailID
                  GROUP BY G.City, G1.City, G.State, G1.State
				  
				  UNION ALL
					
				  --Hubciti DashBoard Implementation
				  SELECT COUNT(DISTINCT(HL.HotDealListID)) Counts
                        , ISNULL(G.City, G1.City) City
                        , ISNULL(G.State, G1.State) [State]
                        , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                        , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                  FROM HubCitiReportingDatabase..HotDealList HL
                  INNER JOIN Scansee..ProductHotDeal HD ON HD.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
                  INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID                 
                  LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                  LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
                  WHERE CAST(HL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
                  AND HD.RetailID = @RetailID
                  GROUP BY G.City, G1.City, G.State, G1.State

				  
				  
				  ) HotDeals
                  GROUP BY State
                  
            END
            
            
            --Check if the User selected This Location or find in the drop down
            IF (@Module = 2 OR @Module = 4)
            BEGIN
					  IF @Module = 2
					  BEGIN 
						  SELECT @ModuleID = ModuleID
						  FROM ScanSeeReportingDatabase..Module 
						  WHERE ModuleName ='Find'

						  INSERT INTO #GeoStats(Counts, State, GeoRef)
						  SELECT SUM(Counts)
								  , [State]
								  , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
							FROM (
                  
							--Get the counts from the Retailer viewed.
							SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
								  --, ISNULL(G.City, G1.City) City
								  , ISNULL(G.State, G1.State) [State]
								  , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								  , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM ScanSeeReportingDatabase..RetailerList RL              
							INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick =1 
							INNER JOIN ScanSeeReportingDatabase..Module MO ON MO.ModuleID =M.ModuleID     
							LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID  = @RetailID
							AND M.ModuleID = @ModuleID
							GROUP BY G.City, G1.City, G.State, G1.State
                        
							UNION ALL
                        
							 SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
								  --, ISNULL(G.City, G1.City) City
								  , ISNULL(G.State, G1.State) [State]
								  , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								  , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM HubCitiReportingDatabase..RetailerList RL              
							INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick =1 
							 INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
							INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID AND L.LinkTypeName ='Find'     
							LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID  = @RetailID                      
							GROUP BY G.City, G1.City, G.State, G1.State
                        

							UNION ALL
                        
							SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
								--, ISNULL(G.City, G1.City) City
								, ISNULL(G.State, G1.State) [State]
								, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM HubCitiReportingDatabase..RetailerList RL              
							INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick =1 
							INNER JOIN Scansee..HcBottomButton BB ON BB.HcBottomButtonID=M.BottomButtonID
							INNER JOIN Scansee..HcBottomButtonLinkType L ON L.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID  AND L.BottomButtonLinkTypeName ='Find'     
							LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID  = @RetailID                      
							GROUP BY G.City, G1.City, G.State, G1.State


							) Counts
							GROUP BY State
					  END

					  ELSE
					  BEGIN
					
							 SELECT @ModuleID = ModuleID
							 FROM ScanSeeReportingDatabase..Module 
							 WHERE ModuleName ='This Location'  
					  
							 INSERT INTO #GeoStats(Counts, State, GeoRef)
							 SELECT SUM(Counts)
								   , [State]
								   , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
							FROM (
                  
							--Get the counts from the Retailer viewed.
							SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
								  --, ISNULL(G.City, G1.City) City
								  , ISNULL(G.State, G1.State) [State]
								  , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								  , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM ScanSeeReportingDatabase..RetailerList RL              
							INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick =1 
							INNER JOIN ScanSeeReportingDatabase..Module MO ON MO.ModuleID =M.ModuleID     
							LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID  = @RetailID
							AND M.ModuleID = @ModuleID
							GROUP BY G.City, G1.City, G.State, G1.State
                        
							UNION ALL
                        
							 SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
								  --, ISNULL(G.City, G1.City) City
								  , ISNULL(G.State, G1.State) [State]
								  , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								  , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM HubCitiReportingDatabase..RetailerList RL              
							INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick =1 
							 INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
							INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID AND L.LinkTypeName ='Whats NearBy'     
							LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID  = @RetailID                      
							GROUP BY G.City, G1.City, G.State, G1.State

							UNION ALL
                        
							 SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
								  --, ISNULL(G.City, G1.City) City
								  , ISNULL(G.State, G1.State) [State]
								  , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								  , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM HubCitiReportingDatabase..RetailerList RL              
							INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick =1 
							 INNER JOIN Scansee..HcBottomButton BB ON BB.HcBottomButtonID =M.BottomButtonID  
							INNER JOIN Scansee..HcBottomButtonLinkType  L ON L.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID  AND L.BottomButtonLinkTypeName  ='Whats NearBy'     
							LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID  = @RetailID                      
							GROUP BY G.City, G1.City, G.State, G1.State
                        
							) Counts
							GROUP BY State				  
					       
						   select * from #GeoStats
						             
					  END
                 
                  
            END               
            
                  --Check if the User selected Scanned/Searched Products in the drop down
                  IF @Module = 5
                  BEGIN                   
                        
                        INSERT INTO #GeoStats(Counts, State, GeoRef)
                        
                        SELECT SUM(Counts)
                              --, City
                              , State
                              , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
                        FROM (                          
								SELECT COUNT(DISTINCT (RL.RetailerListID)) Counts
									  , ISNULL(G.City, G1.City)City
									  , ISNULL(G.State, G1.State)State
									  , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
									  , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
								FROM ScanSeeReportingDatabase..RetailerList  RL 
								INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
								INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID = M.ModuleID
								INNER JOIN ScanSeeReportingDatabase..ProductList PL ON PL.MainMenuID = M.MainMenuID
								LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
								LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
								WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
								AND RL.RetailID = @RetailID 
								AND RL.RetailLocationClick = 1
								AND Mo.ModuleName IN ('Scan Now', 'Shopping List', 'Wish List', 'Find')
								GROUP BY G.City, G1.City, G.State, G1.State
								
								UNION ALL
								
								SELECT COUNT(DISTINCT (RL.RetailerListID)) Counts
									  , ISNULL(G.City, G1.City)City
									  , ISNULL(G.State, G1.State)State
									  , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
									  , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
								FROM HubCitiReportingDatabase..RetailerList  RL 
								INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
								INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID
								INNER JOIN HubCitiReportingDatabase..ProductList PL ON PL.MainMenuID = M.MainMenuID
								LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
								LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
								WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
								AND RL.RetailID = @RetailID 
								AND RL.RetailLocationClick = 1
								AND L.LinkTypeName IN ('Scan Now','Find')
								GROUP BY G.City, G1.City, G.State, G1.State   
                        
								UNION ALL
								
								SELECT COUNT(DISTINCT (RL.RetailerListID)) Counts
									  , ISNULL(G.City, G1.City)City
									  , ISNULL(G.State, G1.State)State
									  , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
									  , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
								FROM HubCitiReportingDatabase..RetailerList  RL 
								INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..HcBottomButton BB ON BB.HcBottomButtonID=M.BottomButtonID
								INNER JOIN Scansee..HcBottomButtonLinkType L ON L.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
								INNER JOIN HubCitiReportingDatabase..ProductList PL ON PL.MainMenuID = M.MainMenuID
								LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
								LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(G.Longitude AS VARCHAR(15)), 1, 5)
								WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
								AND RL.RetailID = @RetailID 
								AND RL.RetailLocationClick = 1
								AND L.BottomButtonLinkTypeName IN ('Scan Now','Find')
								GROUP BY G.City, G1.City, G.State, G1.State 
								
								)Scans
								GROUP BY State
                        
                END
                
                --Check if the User selected Shopping List in the drop down
                  IF @Module = 6
                  BEGIN
                        
                        SELECT @ModuleID = ModuleID
                        FROM ScanSeeReportingDatabase..Module 
                        WHERE ModuleName = 'Shopping List'
                        
                        INSERT INTO #GeoStats(Counts, State, GeoRef)
                        SELECT SUM(Counts)
                              --, City
                              , State
                              , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
                        FROM(
                        SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
                              --, ISNULL(G.City, G1.City) City
                              , ISNULL(G.State, G1.State) [State]
                              , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                              , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                        FROM ScanSeeReportingDatabase..RetailerList  RL                   
                        INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
                        LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                        LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
                        WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE) 
                        AND M.ModuleID = @ModuleID
                        AND RL.RetailID = @RetailID
                        AND RetailLocationClick =1
                        GROUP BY G.City, G1.City, G.State, G1.State
                         
                        --UNION ALL   
                        
                        --SELECT COUNT(DISTINCT(CL.CouponListID)) Counts
                        --, ISNULL(G.City, G1.City) City
                        --, ISNULL(G.State, G1.State) [State]
                        --, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                        --, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                        --FROM ScanSeeReportingDatabase..CouponList CL
                        --INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.CouponClick =1
                        --INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
                        --LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                        --LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
                        --WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                        --AND RS.RetailID = @RetailID
                        --AND M.ModuleID = @ModuleID
                        --GROUP BY G.City, G1.City, G.State, G1.State
                        
                        --UNION ALL
                        
                        --SELECT COUNT(DISTINCT(CL.LoyaltyListID)) Counts
                        --, ISNULL(G.City, G1.City) City
                        --, ISNULL(G.State, G1.State) [State]
                        --, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                        --, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                        --FROM ScanSeeReportingDatabase..LoyaltyList CL
                        --INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.LoyaltyClick =1
                        --INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
                        --INNER JOIN ScanSeeReportingDatabase..Module Mo ON M.ModuleID =Mo.ModuleID 
                        --LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                        --LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
                        --WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                        --AND RS.RetailID = @RetailID
                        --AND M.ModuleID = @ModuleID
                        --GROUP BY G.City, G1.City, G.State, G1.State                       
                        
                        --UNION ALL
                        
                        --SELECT COUNT(DISTINCT(RL.RebateListID )) Counts
                        --, ISNULL(G.City, G1.City) City
                        --, ISNULL(G.State, G1.State) [State]
                        --, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                        --, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                        --FROM ScanSeeReportingDatabase..RebateList RL
                        --INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =RL.RetailerListID AND RL.RebateClick =1
                        --INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
                        --LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                        --LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
                        --WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                        --AND RS.RetailID = @RetailID
                        --AND M.ModuleID = @ModuleID
                        --GROUP BY G.City, G1.City, G.State, G1.State
                        ) Shopping  
                        GROUP BY State             
                  END
                  
                  --Check if the User selected wish List in the drop down
                    IF @Module = 7
                    BEGIN
                   
                              
                        SELECT @ModuleID = ModuleID
                        FROM ScanSeeReportingDatabase..Module 
                        WHERE ModuleName = 'Wish List'
                         
                        INSERT INTO #GeoStats(Counts, State, GeoRef)
                        SELECT SUM(Counts)
                              --, City
                              , State
                              , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
                        FROM(
                        SELECT COUNT(DISTINCT(RL.RetailerListID)) Counts
                              , ISNULL(G.City, G1.City) City
                              , ISNULL(G.State, G1.State) [State]
                              , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                              , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
                        FROM ScanSeeReportingDatabase..RetailerList  RL
                        INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1
                        LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                        LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
                        WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)  
                        AND RL.RetailID = @RetailID 
                        AND M.ModuleID = @ModuleID
                        GROUP BY G.City, G1.City, G.State, G1.State
                         
      --                  UNION ALL   
                        
						--SELECT COUNT(DISTINCT(CL.CouponListID)) Counts
						--, ISNULL(G.City, G1.City) City
						--, ISNULL(G.State, G1.State) [State]
						--, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
						--, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
						--FROM ScanSeeReportingDatabase..CouponList CL
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.CouponClick =1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
						--INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID = M.ModuleID
						--LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						--LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						--WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND RS.RetailID = @RetailID
						--AND Mo.ModuleName IN ('Wish List')
						--GROUP BY G.City, G1.City, G.State, G1.State
                        
						--UNION ALL
                        
						--SELECT COUNT(DISTINCT(CL.LoyaltyListID)) Counts
						--, ISNULL(G.City, G1.City) City
						--, ISNULL(G.State, G1.State) [State]
						--, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
						--, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
						--FROM ScanSeeReportingDatabase..LoyaltyList CL
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.LoyaltyClick =1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
						--INNER JOIN ScanSeeReportingDatabase..Module Mo ON M.ModuleID =Mo.ModuleID 
						--LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						--LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						--WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND RS.RetailID = @RetailID
						--AND Mo.ModuleName IN ('Wish List')
						--AND M.ModuleID = @ModuleID
						--GROUP BY G.City, G1.City, G.State, G1.State                       
                        
						--UNION ALL
                        
						--SELECT COUNT(DISTINCT(RL.RebateListID )) Counts
						--, ISNULL(G.City, G1.City) City
						--, ISNULL(G.State, G1.State) [State]
						--, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
						--, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
						--FROM ScanSeeReportingDatabase..RebateList RL
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =RL.RetailerListID AND RL.RebateClick =1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
						--INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID =M.ModuleID 
						--LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						--LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						--WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND RS.RetailID = @RetailID
						--AND Mo.ModuleName IN ('Wish List')
						--AND M.ModuleID = @ModuleID
						--GROUP BY G.City, G1.City, G.State, G1.State    
                      
      --                  UNION ALL 
                                
      --                  SELECT COUNT(DISTINCT(SL.SaleListID)) Counts
      --                  , ISNULL(G.City, G1.City) City
      --                  , ISNULL(G.State, G1.State) [State]
      --                  , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
      --                  , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
      --                  FROM ScanSeeReportingDatabase..SalesList SL
      --                  INNER JOIN Scansee..RetailLocationDeal RL ON RL.RetailLocationDealID =SL.RetailLoactionDealID AND Sl.RetailLocationDealClick =1
      --                  INNER JOIN Scansee..RetailLocation R ON R.RetailLocationID =RL.RetailLocationID 
      --                  INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = SL.MainMenuID
      --                  INNER JOIN Scansee..Module Mo ON Mo.ModuleID=M.ModuleID  
      --                  LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
      --                  LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
      --                  WHERE CAST(SL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(SL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
      --                  AND Mo.ModuleName IN ('Wish List')
      --                  AND R.RetailID = @RetailID
      --                  AND M.ModuleID = @ModuleID
      --                  GROUP BY G.City, G1.City, G.State, G1.State      
                              
      --                  UNION ALL                        
                       
						--SELECT COUNT(DISTINCT(HL.HotDealListID)) Counts
						--, ISNULL(G.City, G1.City) City
						--, ISNULL(G.State, G1.State) [State]
						--, ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
						--, ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
						--FROM ScanSeeReportingDatabase..HotDealList HL
						--INNER JOIN Scansee..ProductHotDeal HD ON HL.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID                 
						--LEFT JOIN Scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						--LEFT JOIN Scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						--WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND HD.RetailID = @RetailID
						--GROUP BY G.City, G1.City, G.State, G1.State
						) Wish 
						GROUP BY State 
						                        
                    END
                        
                        --Display the final statistics
                        SELECT Counts
                              , City
                              , State
                              , GeoRef
                        FROM #GeoStats
                  
                  
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_RetailerGeographicStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;





GO
