USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierWishListStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SupplierWishListStatistics
Purpose					: To get the product statistics from Wish List.
Example					: usp_SupplierWishListStatistics

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st March 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierWishListStatistics]
(
	  @SupplierID int
	, @StartDate datetime
	, @EndDate datetime	 

)
AS
BEGIN

	BEGIN TRY
			
		--Capture the statisics of the products from Wish list.
		BEGIN
			
			SELECT ProductID
				 , ProductName
				 , NumberInWishlist = MAX(NumberInWishlist)
				 , Location = MAX(MaxCity)+', '+MAX(MaxState)
				 , Amount = COUNT(CASE WHEN City = MaxCity AND State = MaxState THEN 1 END)
			FROM 
			(
			SELECT DISTINCT Pr.ProductID
				 , Pr.ProductName
				 , NumberInWishlist = COUNT(CASE WHEN UP.WishListItem = 1 THEN 1 END)
				 , MaxCity = MAX(CASE WHEN UP.WishListItem = 1 THEN G.City END)
				 , MaxState = MAX(CASE WHEN UP.WishListItem = 1 THEN G.State END)
				 , City
				 , State
			FROM ScanSeeReportingDatabase..UserProduct UP
			INNER JOIN scansee..Product Pr ON UP.ProductID = Pr.ProductID
			INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = UP.WishListMainMenuID			
			LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode-- AND M.PostalCode IS NOT NULL) OR (M.PostalCode IS NULL AND M.Latitude =G.Latitude AND M.Longitude =G.Longitude))
			WHERE Pr.ManufacturerID = @SupplierID
			AND CAST(UP.WishListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.WishListAddDate AS DATE) <=CAST(@EndDate AS DATE)
			GROUP BY Pr.ProductID, Pr.ProductName, G.City, G.State)Prod
			GROUP BY ProductID, ProductName
		END
		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SupplierWishListStatistics.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;

GO
