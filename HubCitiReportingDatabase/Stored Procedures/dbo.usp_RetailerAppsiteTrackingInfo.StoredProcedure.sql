USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerAppsiteTrackingInfo]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerAppsiteTrackingInfo
Purpose					: To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example					: usp_RetailerAppsiteTrackingInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st March 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerAppsiteTrackingInfo]
(
	  @RetailerID int
	, @StartDate datetime
	, @EndDate datetime	 

)
AS
BEGIN

	BEGIN TRY
	    
	    DECLARE @QRTypeID INT
		DECLARE @TotalClickCount INT
		DECLARE @HCTotalAppsiteClickCount INT
		DECLARE @HcTotalClickCountANythingpagelist INT
		
		SELECT @QRTypeID=QRTypeID FROM Scansee..QRTypes WHERE QRTypeName = 'Anything Page'
		
		--Get the clicks for all the Anything pages of all the retailers in the system.
		SELECT @TotalClickCount = COUNT(1)
		FROM AnythingPageList
		WHERE AnythingPageClick = 1
		AND CAST(@EndDate AS DATE) >= CAST(CreatedDate  AS DATE)
		
		--Get the clicks for all the Anything pages of all the retailers in the system.
		SELECT @HcTotalClickCountANythingpagelist = COUNT(1)
		FROM HubCitiReportingDatabase..AnythingPageList
		WHERE AnythingPageClick = 1
		AND CAST(@EndDate AS DATE) >= CAST(DateCreated  AS DATE)


        --Hubciti Related Appsites Counts for all retailers
		Select @HCTotalAppsiteClickCount=COUNT(1)
		From HubCitiReportingDatabase..AppsiteList 
		WHERE AppsiteClick =1
		AND CAST(@EndDate AS DATE) >= CAST(DateCreated  AS DATE)


		--RowNum = ROW_NUMBER() OVER (ORDER BY COUNT(AnythingPageID) DESC, Q.Pagetitle ASC)
		
		SELECT RowNum = ROW_NUMBER() OVER (ORDER BY COUNT(Counts) DESC, Pagetitle ASC)
		      ,QRRetailerCustomPageID
			  ,Pagetitle
			  ,Impression
			  ,Clicks
			  ,TotalClickCount
			  ,UniqueUserCount
		FROM (		
		--Get the Statistics of the Anything pages of the retailer in context.
		
		SELECT DISTINCT COUNT(Counts) Counts
			 , QRRetailerCustomPageID
			 , Pagetitle
			 , SUM(Impression) Impression
			 , SUM(Clicks) Clicks
			 , SUM(TotalClickCount) TotalClickCount
			 , SUM(UniqueUserCount) UniqueUserCount
		
	    FROM
		
		( 	
		
		SELECT COUNT(AnythingPageID) Counts
			 , Q.QRRetailerCustomPageID
			 , Q.Pagetitle
			 , COUNT(AnythingPageID) AS 'Impression'
			 , COUNT(CASE WHEN AnythingPageClick = 1 AND Q.RetailID = @RetailerID THEN 1 END) AS 'Clicks'
			 , @TotalClickCount TotalClickCount
			 , COUNT(DISTINCT(M.UserID)) UniqueUserCount
		FROM AnythingPageList A
		LEFT JOIN RetailerList RL ON RL.RetailerListID = A.RetailerListID
		LEFT JOIN MainMenu M ON M.MainMenuID = A.MainMenuID		
		LEFT JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID = A.AnythingPageID		 
		WHERE Q.QRTypeID = @QRTypeID
		AND Q.RetailID = @RetailerID
		AND CAST(A.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
		GROUP BY Q.QRRetailerCustomPageID, Q.Pagetitle
		
		UNION ALL

		SELECT COUNT(AnythingPageID) Counts
			 , Q.QRRetailerCustomPageID
			 , Q.Pagetitle
			 , COUNT(AnythingPageID) AS 'Impression'
			 , COUNT(CASE WHEN AnythingPageClick = 1 AND Q.RetailID = @RetailerID THEN 1 END) AS 'Clicks'
			 , @HcTotalClickCountANythingpagelist TotalClickCount
			 , COUNT(DISTINCT(M.HcUserID)) UniqueUserCount
		
		FROM HubCitiReportingDatabase..AnythingPageList A
		LEFT JOIN HubCitiReportingDatabase..RetailerList RL ON RL.RetailerListID = A.RetailerListID
		LEFT JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = A.MainMenuID		
		LEFT JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID = A.AnythingPageID		 
		WHERE Q.QRTypeID = @QRTypeID
		AND Q.RetailID = @RetailerID
		AND CAST(A.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
		GROUP BY Q.QRRetailerCustomPageID, Q.Pagetitle
		)A
		GROUP BY QRRetailerCustomPageID, Pagetitle

		UNION ALL

	    --Fetch Hubcitu Releted Data		
		SELECT COUNT(AL.AppsiteID) Counts
			 , A.HcAppSiteID 
			 , A.HcAppSiteName 
			 , COUNT(AL.AppsiteID) AS 'Impression'
			 , COUNT(CASE WHEN AppsiteClick = 1 AND RL.RetailID = @RetailerID THEN 1 END) AS 'Clicks'
			 , @HCTotalAppsiteClickCount TotalClickCount
			 , COUNT(DISTINCT(M.HcUserID)) UniqueUserCount
				
		FROM HubCitiReportingDatabase..AppsiteList AL
		INNER JOIN HubCitiReportingDatabase..MainMenu M ON AL.MainMenuID = M.MainMenuID
		INNER JOIN Scansee..HcAppSite A ON A.HcAppSiteID =AL.AppsiteID	
		INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID = A .RetailLocationID 			 
		WHERE RL.RetailID =@RetailerID 
		AND CAST(AL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(AL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
		GROUP BY A.HcAppSiteID , A.HcAppSiteName)A 
		GROUP BY QRRetailerCustomPageID,Pagetitle,Impression,Clicks,TotalClickCount,UniqueUserCount,Counts		
		ORDER BY Counts DESC,Pagetitle ASC


		







	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_RetailerAppsiteTrackingInfo.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;





GO
