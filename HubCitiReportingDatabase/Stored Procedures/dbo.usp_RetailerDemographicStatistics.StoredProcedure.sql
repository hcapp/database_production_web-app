USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerDemographicStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name               : usp_RetailerDemographicStatistics
Purpose                             : To get the Retailer geographical statistics from different modules.
Example                             : usp_RetailerDemographicStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               8th April 2013    SPAN           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerDemographicStatistics]
(
        @RetailID int
      , @Module int
      , @StartDate datetime
      , @EndDate datetime     

)
AS
BEGIN

      BEGIN TRY         
      
            DECLARE @ModuleID INT
            
            CREATE TABLE #DemoStats(Gender varchar(10), Counts INT)
                       
            --Check if the User selected the QR module in the drop down list.
			IF @Module = 1
			BEGIN			
				 SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				             , COUNT(DISTINCT(D.UserID)) Counts
							 
						INTO #ScanseeUsers
						FROM ScanSeeReportingDatabase..Scan S
						INNER JOIN ScanSeeReportingDatabase..ScanType ST ON ST.ScanTypeID = S.ScanTypeID 
						INNER JOIN ScanSeeReportingDatabase..RetailerList RRL ON RRL.RetailLocationID = S.ID
						--LEFT OUTER JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID =S.ID AND QRTypeID IN (SELECT QRTypeID FROM Scansee..QRTypes WHERE QRTypeName  in('Anything Page','Special Offer Page')) AND Q.RetailID =@RetailID 
						INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID = RRL.RetailLocationID AND RRL.MainMenuID = S.MainMenuID
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = S.MainMenuID
						INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
						WHERE ST.ScanType IN ('Retailer Anything Page QR Code','Retailer Special Offer QR Code','Retailer Summary QR Code')
						AND RL.RetailID = @RetailID
						AND CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						GROUP BY D.Gender
						
						 


						--Hubciti DashBoard Implementation
						SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				             , COUNT(DISTINCT(D.HcUserID)) Counts
							
						INTO #HcUsers
						FROM HubCitiReportingDatabase..Scan S
						INNER JOIN HubCitiReportingDatabase..ScanType ST ON ST.ScanTypeID = S.ScanTypeID 
						INNER JOIN HubCitiReportingDatabase..RetailerList RRL ON RRL.RetailLocationID = S.ID
						--LEFT OUTER JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID =S.ID AND QRTypeID IN (SELECT QRTypeID FROM Scansee..QRTypes WHERE QRTypeName  in('Anything Page','Special Offer Page')) AND Q.RetailID =@RetailID 
						INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID = RRL.RetailLocationID AND RRL.MainMenuID = S.MainMenuID
						INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = S.MainMenuID
						INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID 
						WHERE ST.ScanType IN ('Retailer Anything Page QR Code','Retailer Special Offer QR Code','Retailer Summary QR Code')
						AND RL.RetailID = @RetailID
						AND CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						GROUP BY D.Gender 

						INSERT INTO #DemoStats(Gender,Counts)
						SELECT ISNULL(S.Gender,H.Gender )
						      ,Counts=CASE WHEN S.Gender IS NOT NULL AND H.Gender IS NOT NULL THEN S.Counts + H.Counts 
							               WHEN S.Gender IS NOT NULL AND H.Gender IS NULL THEN S.Counts
						             	   WHEN S.Gender IS NULL AND H.Gender IS NOT NULL THEN H.Counts END
						FROM #ScanseeUsers S
						FULL JOIN #HcUsers H ON H.Gender =S.Gender 
						

			END              
           
            --Check if the User selected Hot Deals in the drop down
            IF @Module = 3
            BEGIN
                  
                  SELECT @ModuleID = ModuleID
                  FROM ScanSeeReportingDatabase..Module 
                  WHERE ModuleName = 'Hot Deals'
                           

                           INSERT INTO #DemoStats(Gender, Counts)
						   SELECT Gender 
						        , COUNT(Counts)
						   FROM
						   (
                 
                          SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				               , COUNT(DISTINCT(D.UserID)) Counts
						  --INTO #ScanseeHD
						  FROM ScanSeeReportingDatabase..HotDealList HL
						  INNER JOIN Scansee..ProductHotDeal HD ON HL.ProductHotDealID = HD.ProductHotDealID AND HL.ProductHotDealClick = 1
						  INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID                 
						  INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
						  WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						  AND HD.RetailID = @RetailID AND 1=2
						  GROUP BY D.Gender
						  
						 UNION ALL

						  ----Hubciti DashBoard Implementation
						  SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				               , COUNT(DISTINCT(D.HcUserID)) Counts
						  --INTO #HubcitiHD
						  FROM HubCitiReportingDatabase..HotDealList HL
						  INNER JOIN Scansee..ProductHotDeal HD ON HL.ProductHotDealID = HD.ProductHotDealID AND HL.ProductHotDealClick = 1
						  INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID                 
						  INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID 
						  WHERE CAST(HL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
						  AND HD.RetailID = @RetailID
						  GROUP BY D.Gender)A
						  GROUP BY Gender
						  

						  --INSERT INTO #DemoStats(Gender, Counts)
						  --SELECT ISNULL(S.Gender,H.Gender )
						  --       ,Counts=CASE WHEN S.Gender IS NOT NULL AND H.Gender IS NOT NULL THEN S.Counts + H.Counts 
						 	--                  WHEN S.Gender IS NOT NULL AND H.Gender IS NULL THEN S.Counts
						  --           	      WHEN S.Gender IS NULL AND H.Gender IS NOT NULL THEN H.Counts END
						  --FROM #ScanseeHD S
						  --FULL JOIN #HubcitiHD H ON H.Gender =S.Gender 

                  
            END
            
            
            --Check if the User selected This Location or find in the drop down
            IF (@Module = 2 OR @Module = 4)
            BEGIN
                  IF @Module =2
                  BEGIN 
					  SELECT @ModuleID = ModuleID
					  FROM ScanSeeReportingDatabase..Module 
					  WHERE ModuleName ='Find'
                 
				        INSERT INTO #DemoStats(Gender, Counts)
						SELECT Gender 
						    , COUNT(Counts)
						FROM
						(
						                
                        SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				              ,COUNT(DISTINCT(D.UserID)) Counts
                       -- INTO #ScanseeFind
						FROM ScanSeeReportingDatabase..RetailerList RL              
                        INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1 
                        INNER JOIN ScanSeeReportingDatabase..Module MO ON MO.ModuleID =M.ModuleID     
                        INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
                        WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                        AND RL.RetailID  = @RetailID   
                        AND M.ModuleID = @ModuleID                    
                        GROUP BY D.Gender
                        
						UNION ALL

						---Hubciti DashBoard Implementation
						SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				              ,COUNT(DISTINCT(D.HcUserID)) Counts
						--INTO #HubcitiFind
                        FROM HubCitiReportingDatabase..RetailerList RL              
                        INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1 
                        INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
						INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID AND L.LinkTypeName ='Find'   
                        INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID 
                        WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
                        AND RL.RetailID  = @RetailID                                        
                        GROUP BY D.Gender
						
						UNION ALL

						SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				              ,COUNT(DISTINCT(D.HcUserID)) Counts
						--INTO #HubcitiFind
                        FROM HubCitiReportingDatabase..RetailerList RL              
                        INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1 
                        INNER JOIN Scansee..HcBottomButton BB ON BB.HcBottomButtonID =M.BottomButtonID  
						INNER JOIN Scansee..HcBottomButtonLinkType L ON L.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID AND L.BottomButtonLinkTypeName ='Find'   
                        INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID 
                        WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
                        AND RL.RetailID  = @RetailID                                        
                        GROUP BY D.Gender
						
						)A
						GROUP BY Gender									 
				 
				  END
                  ELSE
                  BEGIN

					  SELECT @ModuleID = ModuleID
					  FROM ScanSeeReportingDatabase..Module 
					  WHERE ModuleName ='This Location'                 
                
					    INSERT INTO #DemoStats(Gender, Counts)
						SELECT Gender 
						    , COUNT(Counts)
						FROM
						(              
                        SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				              ,COUNT(DISTINCT(D.UserID)) Counts
						--INTO #ScanseeThisLoc
                        FROM ScanSeeReportingDatabase..RetailerList RL              
                        INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1 
                        INNER JOIN ScanSeeReportingDatabase..Module MO ON MO.ModuleID =M.ModuleID     
                        INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
                        WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                        AND RL.RetailID  = @RetailID   
                        AND M.ModuleID = @ModuleID                    
                        GROUP BY D.Gender
                        
						UNION ALL
						
						---Hubciti DashBoard Implementation
						SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				              ,COUNT(DISTINCT(D.HcUserID)) Counts
						--INTO #HubcitiThisLoc
                        FROM HubCitiReportingDatabase..RetailerList RL              
                        INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1 
                        INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
						INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID AND L.LinkTypeName ='Whats NearBy'  
                        INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID 
                        WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
                        AND RL.RetailID  = @RetailID                                        
                        GROUP BY D.Gender
						
						UNION ALL
						
						---Hubciti DashBoard Implementation
						SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				              ,COUNT(DISTINCT(D.HcUserID)) Counts
						--INTO #HubcitiThisLoc
                        FROM HubCitiReportingDatabase..RetailerList RL              
                        INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1 
                        INNER JOIN Scansee..HcBottomButton BB ON BB.HcBottomButtonID =M.BottomButtonID
						INNER JOIN Scansee..HcBottomButtonLinkType L ON L.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID AND L.BottomButtonLinkTypeName ='Whats NearBy'  
                        INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID 
                        WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
                        AND RL.RetailID  = @RetailID                                        
                        GROUP BY D.Gender
						
						)A
						GROUP BY Gender 
				
				  END
                  
                  END               
            
                  --Check if the User selected Scanned/Searched Products in the drop down
                  IF @Module = 5
                  BEGIN                   
                        
                       
                        
                                INSERT INTO #DemoStats(Gender, Counts)
								SELECT Gender 
									, COUNT(Counts)
								FROM
								(                 
								SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				                     , COUNT(DISTINCT(D.UserID)) Counts
								--INTO #ScanseeScan
								FROM ScanSeeReportingDatabase..RetailerList  RL 
								INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
								INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID = M.ModuleID
								INNER JOIN ScanSeeReportingDatabase..ProductList PL ON PL.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
								WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
								AND RL.RetailID = @RetailID 
								AND RL.RetailLocationClick =1
								AND Mo.ModuleName IN ('Scan Now', 'Shopping List', 'Wish List', 'Find')
								GROUP BY D.Gender 

								
								UNION ALL

								--Hubciti DashBoard Implementation

								SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				                     , COUNT(DISTINCT(D.HcUserID)) Counts
								--INTO #HubcitiScan
								FROM HubCitiReportingDatabase..RetailerList  RL 
								INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..HcMenuItem MI ON MI.HcMenuItemID =M.MenuItemID 
								INNER JOIN Scansee..HcLinkType L ON L.HcLinkTypeID =MI.HcLinkTypeID 
								INNER JOIN HubCitiReportingDatabase..ProductList PL ON PL.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID  
								WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
								AND RL.RetailID = @RetailID 
								AND RL.RetailLocationClick =1
								AND L.LinkTypeName IN ('Scan Now','Find')
								GROUP BY D.Gender 
								
								UNION ALL

								SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				                     , COUNT(DISTINCT(D.HcUserID)) Counts
								--INTO #HubcitiScan
								FROM HubCitiReportingDatabase..RetailerList  RL 
								INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..HcBottomButton BB ON BB.HcBottomButtonID  =M.BottomButtonID 
								INNER JOIN Scansee..HcBottomButtonLinkType L ON L.HcBottomButtonLinkTypeID =BB.BottomButtonLinkTypeID 
								INNER JOIN HubCitiReportingDatabase..ProductList PL ON PL.MainMenuID = RL.MainMenuID
								INNER JOIN Scansee..HcUser D ON D.HcUserID =M.HcUserID  
								WHERE CAST(RL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
								AND RL.RetailID = @RetailID 
								AND RL.RetailLocationClick =1
								AND L.BottomButtonLinkTypeName IN ('Scan Now','Find')
								GROUP BY D.Gender
								
								)A
								GROUP BY Gender

								--INSERT INTO #DemoStats (Gender,Counts )
								--SELECT ISNULL(S.Gender,H.Gender )
								--	  ,Counts=CASE WHEN S.Gender IS NOT NULL AND H.Gender IS NOT NULL THEN S.Counts + H.Counts 
						 	--                       WHEN S.Gender IS NOT NULL AND H.Gender IS NULL THEN S.Counts
						  --           	           WHEN S.Gender IS NULL AND H.Gender IS NOT NULL THEN H.Counts END
								--FROM #ScanseeScan S
								--FULL JOIN #HubcitiScan H ON H.Gender =S.Gender

                END
                
                --Check if the User selected Shopping List in the drop down
                  IF @Module = 6
                  BEGIN
                        
                        SELECT @ModuleID = ModuleID
                        FROM ScanSeeReportingDatabase..Module 
                        WHERE ModuleName = 'Shopping List'
                        
                        INSERT INTO #DemoStats (Gender,Counts )
                        SELECT Gender
                              ,Counts 
                        FROM(
							SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
								 , COUNT(DISTINCT(D.UserID)) Counts
							FROM ScanSeeReportingDatabase..RetailerList  RL                   
							INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
							INNER JOIN Scansee..UserDemographic D ON D.UserID=M.UserID 
							WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
							AND RL.RetailID = @RetailID
							AND M.ModuleID = @ModuleID
							AND RetailLocationClick =1
							GROUP BY D.Gender 
                         
                   --     UNION ALL   
                        
                   --     SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				               --, COUNT(DISTINCT(D.UserID)) Counts
                   --     FROM ScanSeeReportingDatabase..CouponList CL
                   --     INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.CouponClick =1
                   --     INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
                   --     INNER JOIN Scansee..UserDemographic D ON D.UserID=M.UserID 
                   --     WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                   --     AND RS.RetailID = @RetailID
                   --     AND M.ModuleID = @ModuleID
                   --     GROUP BY D.Gender 
                        
                   --     UNION ALL
                        
                   --    SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				               --, COUNT(DISTINCT(D.UserID)) Counts
                   --     FROM ScanSeeReportingDatabase..LoyaltyList CL
                   --     INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.LoyaltyClick =1
                   --     INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
                   --     INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
                   --     WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                   --     AND RS.RetailID = @RetailID
                   --     AND M.ModuleID = @ModuleID
                   --     GROUP BY D.Gender                    
                        
                   --     UNION ALL
                        
                   --     SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				               --, COUNT(DISTINCT(D.UserID)) Counts
                   --     FROM ScanSeeReportingDatabase..RebateList RL
                   --     INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =RL.RetailerListID AND RL.RebateClick =1
                   --     INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
                   --     INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
                   --     WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
                   --     AND RS.RetailID = @RetailID
                   --     AND M.ModuleID = @ModuleID
                   --     GROUP BY D.Gender
                   ) Shopping                    
                  END
                  
                  --Check if the User selected Shopping List in the drop down
                    IF @Module = 7
                    BEGIN
                              
                        SELECT @ModuleID = ModuleID
                        FROM ScanSeeReportingDatabase..Module 
                        WHERE ModuleName = 'Wish List'
                         
                        INSERT INTO #DemoStats (Gender,Counts)
                        SELECT Gender 
                              ,Counts 
                        FROM(
								SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
									   , COUNT(DISTINCT(D.UserID)) Counts
								FROM ScanSeeReportingDatabase..RetailerList  RL
								INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID AND RL.RetailLocationClick = 1
								INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
								WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE) 
								AND RL.RetailID = @RetailID 
								AND M.ModuleID = @ModuleID
								GROUP BY D.Gender 
                         
      --                  UNION ALL   
                        
						--SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				  --             , COUNT(DISTINCT(D.UserID)) Counts
						--FROM ScanSeeReportingDatabase..CouponList CL
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.CouponClick =1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
						--INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID = M.ModuleID
						--INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
						--WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND RS.RetailID = @RetailID
						--AND Mo.ModuleName IN ('Wish List')
						--GROUP BY D.Gender 
                        
						--UNION ALL
                        
						--SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				  --             , COUNT(DISTINCT(D.UserID)) Counts
						--FROM ScanSeeReportingDatabase..LoyaltyList CL
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =CL.RetailerListID AND CL.LoyaltyClick =1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = CL.MainMenuID
						--INNER JOIN ScanSeeReportingDatabase..Module Mo ON M.ModuleID =Mo.ModuleID 
						--INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
						--WHERE CAST(CL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(CL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND RS.RetailID = @RetailID
						--AND Mo.ModuleName IN ('Wish List')
						--AND M.ModuleID = @ModuleID
						--GROUP BY D.Gender                  
                        
						--UNION ALL
                        
						--SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				  --             , COUNT(DISTINCT(D.UserID)) Counts
						--FROM ScanSeeReportingDatabase..RebateList RL
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RS ON Rs.RetailerListID =RL.RetailerListID AND RL.RebateClick =1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = RL.MainMenuID
						--INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID =M.ModuleID 
						--INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
						--WHERE CAST(RL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(RL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND RS.RetailID = @RetailID
						--AND Mo.ModuleName IN ('Wish List')
						--AND M.ModuleID = @ModuleID
						--GROUP BY D.Gender   
                      
      --                  UNION ALL 
                                
      --                  SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				  --             , COUNT(DISTINCT(D.UserID)) Counts
      --                  FROM ScanSeeReportingDatabase..SalesList SL
      --                  INNER JOIN Scansee..RetailLocationDeal RL ON RL.RetailLocationDealID =SL.RetailLoactionDealID AND Sl.RetailLocationDealClick =1
      --                  INNER JOIN Scansee..RetailLocation R ON R.RetailLocationID =RL.RetailLocationID 
      --                  INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = SL.MainMenuID
      --                  INNER JOIN Scansee..Module Mo ON Mo.ModuleID=M.ModuleID  
      --                  INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
      --                  WHERE CAST(SL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(SL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
      --                  AND Mo.ModuleName IN ('Wish List')
      --                  AND R.RetailID = @RetailID
      --                  AND M.ModuleID = @ModuleID
      --                  GROUP BY D.Gender 
                              
      --                  UNION ALL                        
                       
						--SELECT Gender = CASE WHEN D.Gender = 0 THEN 'Women' ELSE 'Men' END
				  --             , COUNT(DISTINCT(D.UserID)) Counts
						--FROM ScanSeeReportingDatabase..HotDealList HL
						--INNER JOIN Scansee..ProductHotDeal HD ON HL.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
						--INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID                 
						--INNER JOIN Scansee..UserDemographic D ON D.UserID =M.UserID 
						--WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						--AND HD.RetailID = @RetailID
						--GROUP BY D.Gender 
						) Wish 
						                        
                    END
                        
                        --Display the final statistics
                        SELECT Gender 
                              ,Counts 
                        FROM #DemoStats          
                  
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_RetailerDemographicStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;





GO
