USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierGeographicStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SupplierGeographicStatistics
Purpose					: To get the product geographical statistics from different modules.
Example					: usp_SupplierGeographicStatistics

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st March 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierGeographicStatistics]
(
	  @SupplierID int
	, @Module int
	, @StartDate datetime
	, @EndDate datetime	 

)
AS
BEGIN

	BEGIN TRY		
	
		DECLARE @ModuleID INT
		CREATE TABLE #GeoStats(Counts INT, City VARCHAR(100), State VARCHAR(10), GeoRef Geography)
		
		--Check if the User selected the QR module.
		IF @Module = 1
		BEGIN			
			INSERT INTO #GeoStats(Counts, State, GeoRef)
			SELECT Sum(Counts)
				 --, City
				 , State
				 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
			FROM
			(SELECT COUNT(DISTINCT(S.ScanID)) Counts
				 , ISNULL(G.City, G1.City) City
				 , ISNULL(G.State, G1.State) [State]
				 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
				 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
			FROM ScanSeeReportingDatabase..Scan S
			INNER JOIN scansee..Product P ON P.ProductID = S.ID
			INNER JOIN ScanSeeReportingDatabase..ScanType ST ON ST.ScanTypeID = S.ScanTypeID
			INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = S.MainMenuID
			LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
			LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
			WHERE P.ManufacturerID = @SupplierID
			AND ST.ScanType = ('Product QR')
			AND CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
			GROUP BY G.City, G1.City, G.State, G1.State) QR
			GROUP BY State 
		END
		
		
		--Check if the User selected Find in the drop down
		IF @Module = 2
		BEGIN
			
			SELECT @ModuleID = ModuleID
			FROM ScanSeeReportingDatabase..Module 
			WHERE ModuleName = 'Find'
			
			INSERT INTO #GeoStats(Counts, State, GeoRef)
			SELECT SUM(Counts)
				 --, City
				 , State
				 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
			FROM(
					SELECT COUNT(DISTINCT(PL.ProductListID)) Counts
						 , ISNULL(G.City, G1.City) City
						 , ISNULL(G.State, G1.State) [State]
						 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
						 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
					FROM ScanSeeReportingDatabase..ProductList PL
					INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID AND PL.ProductClick = 1
					INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID
					LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
					LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
					WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
					AND P.ManufacturerID = @SupplierID
					AND M.ModuleID = @ModuleID
					GROUP BY G.City, G1.City, G.State, G1.State
					
					UNION ALL
					
					SELECT COUNT(DISTINCT(S.SaleListID)) Counts
						 , ISNULL(G.City, G1.City) City
						 , ISNULL(G.State, G1.State) [State]
						 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
						 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
					FROM ScanSeeReportingDatabase..SalesList S
					INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
					INNER JOIN ScanSeeReportingDatabase..RetailerList RL ON RL.RetailerListID = S.RetailerListID
					INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID					
					INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID
					LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
					LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
					WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
					AND P.ManufacturerID = @SupplierID
					AND M.ModuleID = @ModuleID
					GROUP BY G.City, G1.City, G.State, G1.State) Find
			        GROUP BY State
		END
		
		--Check if the User selected Hot Deals in the drop down
		IF @Module = 3
		BEGIN
			
			SELECT @ModuleID = ModuleID
			FROM ScanSeeReportingDatabase..Module 
			WHERE ModuleName = 'Hot Deals'
			
			INSERT INTO #GeoStats(Counts, State, GeoRef)
			SELECT SUM(Counts)
				 --, City
				 , State
				 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
			FROM (
			SELECT COUNT(DISTINCT(HL.HotDealListID)) Counts
				 , ISNULL(G.City, G1.City) City
				 , ISNULL(G.State, G1.State) [State]
				 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
				 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
			FROM ScanSeeReportingDatabase..HotDealList HL
			INNER JOIN scansee..ProductHotDeal HD ON HD.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
			INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = HL.MainMenuID
			LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
			LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
			WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
			AND HD.ManufacturerID = @SupplierID
			GROUP BY G.City, G1.City, G.State, G1.State
			
			) HotDeals
			GROUP BY State
			
		END
		
		
		--Check if the User selected This Location in the drop down
		IF @Module = 4
		BEGIN
			SELECT @ModuleID = ModuleID
			FROM ScanSeeReportingDatabase..Module 
			WHERE ModuleName = 'This Location'
			
			INSERT INTO #GeoStats(Counts, State, GeoRef)
			SELECT SUM(Counts)
				 --, City
				 , [State]
				 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
			FROM (
			
				--Get the counts from the Sales viewed.
				SELECT COUNT(DISTINCT(S.SaleListID)) Counts
					 , ISNULL(G.City, G1.City) City
					 , ISNULL(G.State, G1.State) [State]
					 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
					 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
				FROM ScanSeeReportingDatabase..SalesList S
				INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
				INNER JOIN ScanSeeReportingDatabase..RetailerList RL ON RL.RetailerListID = S.RetailerListID
				INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
				INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID
				LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
				LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
				WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				AND P.ManufacturerID = @SupplierID
				AND M.ModuleID = @ModuleID
				GROUP BY G.City, G1.City, G.State, G1.State
				
				--UNION ALL
				
				--SELECT COUNT(DISTINCT(HL.HotDealListID)) Counts
    --                    , ISNULL(G.City, G1.City) City
    --                    , ISNULL(G.State, G1.State) [State]
    --                    , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
    --                    , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
    --            FROM ScanSeeReportingDatabase..HotDealList HL
    --            INNER JOIN scansee..ProductHotDeal HD ON HL.ProductHotDealID = HL.ProductHotDealID AND HL.ProductHotDealClick = 1
    --            LEFT JOIN ScanSeeReportingDatabase..RetailerList RRL ON RRL.RetailerListID = HL.RetailerListID                                
    --            LEFT JOIN ScanSeeReportingDatabase..MainMenu M ON ((HL.MainMenuID = M.MainMenuID AND HL.MainMenuID IS NOT NULL) OR (HL.RetailerListID = RRL.RetailerListID AND HL.RetailerListID IS NOT NULL))                                                               
    --            LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
    --            LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
    --            WHERE CAST(HL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(HL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
    --            AND HD.ManufacturerID = @SupplierID 
    --            AND M.ModuleID = @ModuleID
    --            GROUP BY G.City, G1.City, G.State, G1.State
                
                UNION ALL
                
                SELECT COUNT(DISTINCT(PL.ProductListID)) Counts
                     , ISNULL(G.City, G1.City) City
                     , ISNULL(G.State, G1.State) [State]
                     , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
                     , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude 
				FROM ScanSeeReportingDatabase..ProductList PL 
				INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
				INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID				
				LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
                LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)				
				WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
				AND P.ManufacturerID = @SupplierID
				AND M.ModuleID = @ModuleID
				GROUP BY G.City, G1.City, G.State, G1.State) Counts
                GROUP BY State 
				
			END
			
			--Check if the User selected Scanned/Searched Products in the drop down
			IF @Module = 5
			BEGIN				
				
				INSERT INTO #GeoStats(Counts, State, GeoRef)
				
				SELECT SUM(Counts)
					 --, City
					 , State
					 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
				FROM (								
						SELECT COUNT(DISTINCT (S.ScanHistoryID)) Counts
							 , G1.City
							 , G1.State
							 , MAX(G1.Latitude) Latitude
							 , MAX(G1.Longitude) Longitude
						FROM scansee..ScanHistory S
						INNER JOIN scansee..Product P ON P.ProductID = S.ProductID  
						INNER JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(S.ScanLatitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(S.ScanLongitude AS VARCHAR(15)), 1, 5)
						WHERE P.ManufacturerID = @SupplierID
						AND CAST(S.ScanDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.ScanDate AS DATE) <= CAST(@EndDate AS DATE)
						GROUP BY G1.City, G1.State
						
						UNION ALL
						
						SELECT COUNT(DISTINCT (PL.ProductListID)) Counts
							 , ISNULL(G.City, G1.City)City
							 , ISNULL(G.State, G1.State)State
							 , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
							 , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
						FROM ScanSeeReportingDatabase..ProductList PL 
						INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
						INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID = M.ModuleID
						LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND Mo.ModuleName IN ('Scan Now', 'Shopping List','Find', 'Wish List')
						GROUP BY G.City, G1.City, G.State, G1.State
						
						UNION ALL
						
						SELECT COUNT(DISTINCT(S.SaleListID)) Counts
							 , ISNULL(G.City, G1.City) City
							 , ISNULL(G.State, G1.State) [State]
							 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
							 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
						FROM ScanSeeReportingDatabase..SalesList S
						INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
						--INNER JOIN ScanSeeReportingDatabase..RetailerList RL ON RL.RetailerListID = S.RetailerListID
						INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID  
						INNER JOIN ScanSeeReportingDatabase..Module Mo ON Mo.ModuleID =M.ModuleID 
						LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND Mo.ModuleName IN ('Scan Now', 'Shopping List','Find', 'Wish List')
						GROUP BY G.City, G1.City, G.State, G1.State)Scans
				  GROUP BY  State
			
				
		    END
		    
		    --Check if the User selected Shopping List in the drop down
			IF @Module = 6
			BEGIN
				
				SELECT @ModuleID = ModuleID
				FROM ScanSeeReportingDatabase..Module 
				WHERE ModuleName = 'Shopping List'
				
				INSERT INTO #GeoStats(Counts, State, GeoRef)
				SELECT SUM(Counts)
					 --, City
					 , State
					 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAX(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
				FROM(
						SELECT COUNT(DISTINCT(UP.UserProductID)) Counts
							 , ISNULL(G.City, G1.City)City
							 , ISNULL(G.State, G1.State)State
							 , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
							 , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
						FROM ScanSeeReportingDatabase..UserProduct UP
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = UP.TodayShoppingListMainMenuID AND UP.TodayListtItem = 1
						INNER JOIN scansee..Product PR ON PR.ProductID= UP.ProductID
						LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						LEFT JOIN scansee..UserProduct U ON U.ProductID =UP.ProductID  
						WHERE UP.TodayListAddDate >= @StartDate 
						AND PR.ManufacturerID = @SupplierID
						AND ((U.ProductID IS NOT NULL AND U.UserID IS NOT NULL AND U.ProductID =UP.ProductID AND UP.UserID =U.UserID) OR (U.ProductID IS NULL AND U.UserID IS NULL AND UP.ProductID IS NOT NULL AND UP.UserID IS NOT NULL AND CAST(UP.TodayListAddDate AS DATE)>=CAST(@StartDate AS DATE) AND CAST(UP.TodayListAddDate AS DATE)<=CAST(@EndDate AS DATE)))
						GROUP BY G.City, G1.City, G.State, G1.State
						
						UNION ALL
						--Capture the products in the product list (from CLR associations.)
						SELECT COUNT(DISTINCT (PL.ProductListID)) Counts
							 , ISNULL(G.City, G1.City)City
							 , ISNULL(G.State, G1.State)State
							 , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
							 , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
						FROM ScanSeeReportingDatabase..ProductList PL 
						INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
						LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND M.ModuleID = @ModuleID
						GROUP BY G.City, G1.City, G.State, G1.State
						
						UNION ALL
						
						SELECT COUNT(DISTINCT(S.SaleListID)) Counts
							 , ISNULL(G.City, G1.City) City
							 , ISNULL(G.State, G1.State) [State]
							 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
							 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
						FROM ScanSeeReportingDatabase..SalesList S
						INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
						INNER JOIN ScanSeeReportingDatabase..RetailerList RL ON RL.RetailerListID = S.RetailerListID
						INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
						INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID
						LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
						LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
						WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
						AND P.ManufacturerID = @SupplierID
						AND M.ModuleID = @ModuleID
						GROUP BY G.City, G1.City, G.State, G1.State) Shopping
					GROUP BY State				
			END
			
			 --Check if the User selected Wish List in the drop down
				IF @Module = 7
				BEGIN
					--Capture the p[roducts in the wish list
					SELECT @ModuleID = ModuleID
					FROM ScanSeeReportingDatabase..Module 
					WHERE ModuleName = 'Wish List'
					
					INSERT INTO #GeoStats(Counts,State, GeoRef)
					SELECT SUM(Counts)
						 --, City
						 , State
						 , GEOGRAPHY::STPointFromText('POINT (' + CAST(MAx(Longitude) AS VARCHAR(20)) + ' ' + CAST(MAX(Latitude) AS VARCHAR(20)) + ')', 4326) AS GeoRef
					FROM(
							SELECT COUNT(DISTINCT(UP.UserProductID)) Counts
								, ISNULL(G.City, G1.City)City
								, ISNULL(G.State, G1.State)State
								, MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
								, MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
							FROM ScanSeeReportingDatabase..UserProduct UP
							INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = UP.WishListMainMenuID AND UP.WishListItem = 1
							INNER JOIN scansee..Product PR ON PR.ProductID= UP.ProductID
							LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
							LEFT JOIN scansee..UserProduct U ON U.ProductID =UP.ProductID 
							WHERE UP.WishListAddDate >= @StartDate 
							AND PR.ManufacturerID = @SupplierID
							AND ((U.ProductID IS NOT NULL AND U.UserID IS NOT NULL AND U.ProductID =UP.ProductID AND UP.UserID =U.UserID) OR (U.ProductID IS NULL AND U.UserID IS NULL AND UP.ProductID IS NOT NULL AND UP.UserID IS NOT NULL AND CAST(UP.WishListAddDate AS DATE)>=CAST(@StartDate AS DATE) AND CAST(UP.WishListAddDate AS DATE)<=CAST(@EndDate AS DATE)))
							GROUP BY G.City, G1.City, G.State, G1.State
							
							UNION ALL
							
							--Capture the products in the product list (from SLR associations.)
							SELECT COUNT(DISTINCT (PL.ProductListID)) Counts
								 , ISNULL(G.City, G1.City)City
								 , ISNULL(G.State, G1.State)State
								 , MAX(ISNULL(G.Latitude, G1.Latitude)) Latitude
								 , MAX(ISNULL(G.Longitude, G1.Longitude)) Longitude
							FROM ScanSeeReportingDatabase..ProductList PL 
							INNER JOIN scansee..Product P ON P.ProductID = PL.ProductID AND PL.ProductClick = 1
							INNER JOIN ScanSeeReportingDatabase..MainMenu M ON M.MainMenuID = PL.MainMenuID
							LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(PL.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(PL.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
							AND P.ManufacturerID = @SupplierID
							AND M.ModuleID = @ModuleID
							GROUP BY G.City, G1.City, G.State, G1.State
							
							UNION ALL
							
							SELECT COUNT(DISTINCT(S.SaleListID)) Counts
								 , ISNULL(G.City, G1.City) City
								 , ISNULL(G.State, G1.State) [State]
								 , ISNULL(MAX(G.Latitude), MAX(G1.Latitude))Latitude
								 , ISNULL(MAX(G.Longitude), MAX(G1.Longitude))Longitude
							FROM ScanSeeReportingDatabase..SalesList S
							INNER JOIN scansee..RetailLocationDeal RLD ON S.RetailLoactionDealID = RLD.RetailLocationDealID AND S.RetailLocationDealClick = 1
							INNER JOIN ScanSeeReportingDatabase..RetailerList RL ON RL.RetailerListID = S.RetailerListID
							INNER JOIN scansee..Product P ON P.ProductID = RLD.ProductID
							INNER JOIN ScanSeeReportingDatabase..MainMenu M ON S.MainMenuID = M.MainMenuID 
							LEFT JOIN scansee..GeoPosition G ON G.PostalCode = M.PostalCode
							LEFT JOIN scansee..GeoPosition G1 ON SUBSTRING(CAST(G1.Latitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Latitude AS VARCHAR(15)), 1, 5) AND SUBSTRING(CAST(G1.Longitude AS VARCHAR(15)), 1, 5) = SUBSTRING(CAST(M.Longitude AS VARCHAR(15)), 1, 5)
							WHERE CAST(S.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(S.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
							AND P.ManufacturerID = @SupplierID
							AND M.ModuleID = @ModuleID
							GROUP BY G.City, G1.City, G.State, G1.State
							) Wish
						GROUP BY State					
				END
				
				--Display the final statistics
				SELECT Counts
					 --, City
					 , State
					 , GeoRef
				FROM #GeoStats
	            
			
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SupplierGeographicStatistics.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;

GO
