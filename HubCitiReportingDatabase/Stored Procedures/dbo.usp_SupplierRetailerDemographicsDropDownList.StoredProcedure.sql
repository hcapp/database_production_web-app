USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierRetailerDemographicsDropDownList]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: [usp_SupplierRetailerDemographicsDropDownList]
Purpose					: [usp_SupplierRetailerDemographicsDropDownList]
Example					: [usp_SupplierRetailerDemographicsDropDownList]

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			08th April 2013	Mohith H R	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierRetailerDemographicsDropDownList]
AS
BEGIN

	BEGIN TRY
			
		SELECT 1 ID, 'QR' Name
		UNION ALL
		SELECT 2, 'Find'
		UNION ALL
		SELECT 3, 'Hot Deals'
		UNION ALL
		SELECT 4, 'This Location'
		UNION ALL
		SELECT 5, 'Scanned/Searched Products'
		UNION ALL
		SELECT 6, 'Shopping List'
		UNION ALL
		SELECT 7, 'Wish List'

	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure [usp_SupplierRetailerDemographicsDropDownList].'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;

GO
