USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerMonthwiseTrackingSummary]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerMonthwiseTrackingSummary
Purpose					: To get the counts on the Appsites on monthly basis.
Example					: usp_RetailerMonthwiseTrackingSummary

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			27th March 2013	Pavan Sharma K	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerMonthwiseTrackingSummary]
(
	  @RetailerID int
	, @StartDate datetime
	, @EndDate datetime	 

)
AS
BEGIN

	BEGIN TRY	
	
		DECLARE @AppsitesCount INT
		DECLARE @QRTypeID INT 
		--DECLARE @Counts INT
		
		SELECT @QRTypeID=QRTypeID FROM Scansee..QRTypes WHERE QRTypeName = 'Anything Page'
		
		SELECT SUM(AppsitesCount) AppsitesCount
		      ,[Month]
			  ,[MM]
			  ,[YYYY]
		INTO #AppsitesCount
		FROM 
		(
		--Get the Appsite count for the selected date range
		SELECT AppsitesCount = COUNT(1) 
			 , [Month] = DATENAME(MONTH, A.DateCreated)
			 , [MM] = DATEPART(MM, A.DateCreated)  
			 , [YYYY] = DATEPART(YYYY, A.DateCreated)
		--INTO #AppsitesCount
		FROM Scansee..QRRetailerCustomPage A
		WHERE RetailID = @RetailerID
		AND QRTypeID = @QRTypeID
		AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
		AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate
		GROUP BY DATENAME(MONTH, A.DateCreated), DATEPART(YYYY, A.DateCreated), DATEPART(MM, A.DateCreated)  
		
		--select * from #AppsitesCount
		UNION ALL

		--Hubciti Related Appsite Count

		SELECT AppsitesCount = COUNT(DISTINCT A.HcAppSiteID) 
			 , [Month] = DATENAME(MONTH, A.DateCreated)
			 , [MM] = DATEPART(MM, A.DateCreated)  
			 , [YYYY] = DATEPART(YYYY, A.DateCreated)
		--INTO #HubcitiAppsitesCount
		FROM Scansee..HcAppSite A
		INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID =A.RetailLocationID 
		WHERE RetailID = @RetailerID
		AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)		
		GROUP BY DATENAME(MONTH, A.DateCreated), DATEPART(YYYY, A.DateCreated), DATEPART(MM, A.DateCreated))A
		GROUP BY [Month],[MM],[YYYY]








		SELECT SUM(ImpressionsCount) ImpressionsCount
		      ,SUM(UniqueVisitorCount) UniqueVisitorCount
			  ,[Month]
			  ,[MM]  
			  ,[YYYY] 
		INTO #Counts
		FROM
		(	
		--Get the statistics on the appsites.
		SELECT ImpressionsCount = COUNT(DISTINCT A.AnythingPageListlID) 
			 --, @AppsitesCount AppsitesCount 
			 , UniqueVisitorCount = COUNT(DISTINCT M.UserID) 
			 , [Month] = DATENAME(MONTH, A.CreatedDate)
			 , [MM] = DATEPART(MM, A.CreatedDate)  
			 , [YYYY] = DATEPART(YYYY, A.CreatedDate) 
		
		FROM AnythingPageList A
		LEFT JOIN RetailerList RL ON RL.RetailerListID = A.RetailerListID
		LEFT JOIN MainMenu M ON M.MainMenuID = A.MainMenuID	AND UserID IS NOT NULL	
		LEFT JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID = A.AnythingPageID
		WHERE Q.QRTypeID = @QRTypeID
	    AND Q.RetailID = @RetailerID 
		AND CAST(A.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
		AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate	
		GROUP BY DATENAME(MONTH, A.CreatedDate), DATEPART(YYYY, A.CreatedDate), DATEPART(MM, A.CreatedDate)  
		--ORDER BY DATEPART(MM, A.CreatedDate) ASC	
		
		UNION ALL
		--Hubciti Anything page impressions and clicks
			
		SELECT ImpressionsCount = COUNT(DISTINCT A.AnythingPageListID) 
			 --, @AppsitesCount AppsitesCount 
			 , UniqueVisitorCount = COUNT(DISTINCT M.HcUserID) 
			 , [Month] = DATENAME(MONTH, A.DateCreated)
			 , [MM] = DATEPART(MM, A.DateCreated )  
			 , [YYYY] = DATEPART(YYYY, A.DateCreated) 
		
		FROM HubCitiReportingDatabase..AnythingPageList A
		LEFT JOIN HubCitiReportingDatabase..RetailerList RL ON RL.RetailerListID = A.RetailerListID
		LEFT JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = A.MainMenuID	AND HcUserID IS NOT NULL	
		LEFT JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID = A.AnythingPageID
		WHERE Q.QRTypeID = @QRTypeID
	    AND Q.RetailID = @RetailerID 
		AND CAST(A.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
		AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate	
		GROUP BY DATENAME(MONTH, A.DateCreated), DATEPART(YYYY, A.DateCreated), DATEPART(MM, A.DateCreated)  
		
		UNION ALL

		SELECT ImpressionsCount = COUNT(DISTINCT AL.AppsiteListID) 
			 --, @AppsitesCount AppsitesCount 
			 , UniqueVisitorCount = COUNT(DISTINCT M.HcUserID) 
			 , [Month] = DATENAME(MONTH, AL.DateCreated)
			 , [MM] = DATEPART(MM, AL.DateCreated )  
			 , [YYYY] = DATEPART(YYYY, AL.DateCreated) 
		FROM HubCitiReportingDatabase..AppsiteList AL
		INNER JOIN Scansee..HcAppSite A ON A.HcAppSiteID =AL.AppsiteID 
		INNER JOIN Scansee..RetailLocation RL ON Rl.RetailLocationID =A.RetailLocationID 
		INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID =AL.MainMenuID 
		WHERE RetailID =@RetailerID 
		AND CAST(AL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(AL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
		GROUP BY DATENAME(MONTH, AL.DateCreated), DATEPART(YYYY, AL.DateCreated), DATEPART(MM, AL.DateCreated)  
		)A
		GROUP BY  [Month]
			  ,[MM]  
			  ,[YYYY] 	
		
	
	 	
				
		SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
			 , ISNULL(C.ImpressionsCount, 0)ImpressionsCount
			 , ISNULL(A.AppsitesCount, 0)AppsitesCount
			 , ISNULL(C.UniqueVisitorCount, 0)UniqueVisitorCount
			 , ISNULL(C.[Month], A.[Month])[Month]
			 , ISNULL(C.MM, A.MM)MM
			 , ISNULL(C.YYYY, A.YYYY)YYYY
			 , ChangeYearFlag=CASE WHEN DATEPART(YYYY,@StartDate)=DATEPART(YYYY,@EndDate) THEN 0 ELSE 1 END
		INTO #Temp
		FROM #Counts C
		FULL JOIN #AppsitesCount A ON A.MM = C.MM AND A.YYYY = C.YYYY
		ORDER BY MM ASC 
		
		

		--Hubciti DashBoard Implementation

		
		
				
		----Get the statistics on the appsites.
		--SELECT ImpressionsCount = COUNT(DISTINCT A.AppsiteListID) 
		--	 , @AppsitesCount AppsitesCount 
		--	 , UniqueVisitorCount = COUNT(DISTINCT M.HcUserID) 
		--	 , [Month] = DATENAME(MONTH, A.DateCreated)
		--	 , [MM] = DATEPART(MM, A.DateCreated)  
		--	 , [YYYY] = DATEPART(YYYY, A.DateCreated) 
		--INTO #Counts1
		--FROM HubCitiReportingDatabase..AppsiteList A
		--INNER JOIN Scansee..HcAppSite AP ON AP.HcAppSiteID =A.AppsiteID 
		--LEFT JOIN Scansee..RetailLocation RL ON RL.RetailLocationID  = AP.RetailLocationID 
		--LEFT JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = A.MainMenuID AND HcUserID IS NOT NULL	
		--WHERE -- Q.QRTypeID = @QRTypeID
	 --    RL.RetailID = @RetailerID 
		--AND CAST(A.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)
		----AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate	
		--GROUP BY DATENAME(MONTH, A.DateCreated), DATEPART(YYYY, A.DateCreated), DATEPART(MM, A.DateCreated)  
		----ORDER BY DATEPART(MM, A.CreatedDate) ASC	
		
						
		--SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
		--	 , ISNULL(C.ImpressionsCount, 0)ImpressionsCount
		--	 , ISNULL(A.AppsitesCount, 0)AppsitesCount
		--	 , ISNULL(C.UniqueVisitorCount, 0)UniqueVisitorCount
		--	 , ISNULL(C.[Month], A.[Month])[Month]
		--	 , ISNULL(C.MM, A.MM)MM
		--	 , ISNULL(C.YYYY, A.YYYY)YYYY
		--	 , ChangeYearFlag=CASE WHEN DATEPART(YYYY,@StartDate)=DATEPART(YYYY,@EndDate) THEN 0 ELSE 1 END
		--INTO #Temp1
		--FROM #Counts1 C
		--FULL JOIN #AppsitesCount A ON A.MM = C.MM AND A.YYYY = C.YYYY
		--ORDER BY MM ASC 	
		
		
		--SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
		--      , ImpressionsCount=CASE WHEN TT.Month =T.Month AND TT.MM=T.MM AND TT.YYYY =T.YYYY THEN T.ImpressionsCount +TT.ImpressionsCount 
		--	                          WHEN T.ImpressionsCount IS NOT NULL AND TT.ImpressionsCount IS Null THEN T.ImpressionsCount 
		--							  WHEN T.ImpressionsCount IS NULL AND TT.ImpressionsCount IS NOT NULL THEN TT.ImpressionsCount END

		--	  ,AppsitesCount=CASE WHEN T.AppsitesCount IS NOT NULL AND TT.AppsitesCount IS NOT NULL THEN T.AppsitesCount +TT.AppsitesCount 
		--	                          WHEN T.AppsitesCount IS NOT NULL AND TT.AppsitesCount IS Null THEN T.AppsitesCount 
		--							  WHEN T.AppsitesCount IS NULL AND TT.AppsitesCount IS NOT NULL THEN TT.AppsitesCount END

		--	  ,UniqueVisitorCount=CASE WHEN T.UniqueVisitorCount IS NOT NULL AND TT.UniqueVisitorCount IS NOT NULL THEN T.UniqueVisitorCount +TT.UniqueVisitorCount 
		--	                          WHEN T.UniqueVisitorCount IS NOT NULL AND TT.UniqueVisitorCount IS Null THEN T.UniqueVisitorCount 
		--							  WHEN T.UniqueVisitorCount IS NULL AND TT.UniqueVisitorCount IS NOT NULL THEN TT.UniqueVisitorCount END
		--	  ,[Month]=ISNULL(T.Month,TT.Month)
		--	  ,MM=ISNULL(T.MM,TT.MM)
		--	  ,YYYY=ISNULL(T.YYYY,TT.YYYY)
		--	  , ChangeYearFlag=CASE WHEN DATEPART(YYYY,@StartDate)=DATEPART(YYYY,@EndDate) THEN 0 ELSE 1 END                              
  
  --      INTO #Temp3
		--FROM #Temp T
		--FULL JOIN #Counts1 TT ON TT.Month =T.Month AND TT.MM=T.MM AND TT.YYYY =T.YYYY





		-- END Hubciti Implementation
				
		SELECT A.ImpressionsCount
			 , SUM(B.AppsitesCount)AppsitesCount			
			 , A.UniqueVisitorCount
			 , A.[Month]
			 , A.MM
			 , A.YYYY
		FROM #Temp  A
		CROSS JOIN #Temp B
		WHERE (A.RowNum <= B.RowNum)  
		AND ((A.MM BETWEEN DATEPART(MM, @StartDate)AND DATEPART(MM, @EndDate)) 
		OR (A.MM >=DATEPART(MM, @StartDate) OR A.MM<=DATEPART(MM, @EndDate) AND A.ChangeYearFlag =1))
		GROUP BY A.ImpressionsCount, A.UniqueVisitorCount,A.AppsitesCount,A.YYYY, A.[Month], A.MM
		ORDER BY A.YYYY,A.MM ASC


		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_RetailerMonthwiseTrackingSummary.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;





GO
