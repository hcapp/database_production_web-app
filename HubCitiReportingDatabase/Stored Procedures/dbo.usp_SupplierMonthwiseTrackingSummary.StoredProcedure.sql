USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierMonthwiseTrackingSummary]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                             : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                             : usp_SupplierCategoryProductsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               2nd April 2013    Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierMonthwiseTrackingSummary]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime   
      
      
)
AS
BEGIN

      BEGIN TRY
      
            DECLARE @TotalProduct INT=0
            DECLARE @TotalProduct1 INT=0
            DECLARE @UniqueUsers INT=0
            DECLARE @TotalImpressionCount INT
            
            SELECT @TotalProduct=COUNT(1) 
            FROM scansee..Product 
            WHERE ManufacturerID =@SupplierID 
            AND CAST(ProductAddDate AS Date)<=@EndDate 
           
            SELECT TotalProduct = COUNT(1) 
				 , [Month] = DATENAME(MONTH, A.ProductAddDate)
				 , [MM] = DATEPART(MM, A.ProductAddDate)  
				 , [YYYY] = DATEPART(YYYY, A.ProductAddDate)
			INTO #ProductsCount
			FROM scansee..Product A
			WHERE ManufacturerID = @SupplierID 			
			AND CAST(A.ProductAddDate AS DATE) <= CAST(@EndDate AS DATE)	
			--AND CAST(A.ProductAddDate AS DATE)>CAST(@StartDate AS DATE)	
			GROUP BY DATENAME(MONTH, A.ProductAddDate), DATEPART(YYYY, A.ProductAddDate), DATEPART(MM, A.ProductAddDate) 
                      
            SELECT SUM(TotalImpressionCount) TotalImpressionCount
                  ,SUM(UniqueUsers) UniqueUsers
                  ,[Month]
                  ,[MM]
                  ,[YYYY]
                --,MAX(CreatedDate)                  
            INTO #Counts
            FROM(                      
				SELECT TotalImpressionCount = COUNT(1)              
					  ,UniqueUsers=COUNT(Distinct M.UserID) 
					  ,[Month] = DATENAME(MONTH, PL.CreatedDate)
					  ,[MM] = DATEPART(MM, PL.CreatedDate)  
					  ,[YYYY] = DATEPART(YYYY, PL.CreatedDate)  
					  --,PL.CreatedDate CreatedDate
				FROM scansee..Product P  
				INNER JOIN ProductList PL ON PL.ProductID =P.ProductID 
				INNER JOIN MainMenu M ON M.MainMenuID=PL.MainMenuID 
				WHERE ManufacturerID =@SupplierID 
				AND CAST(PL.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(PL.CreatedDate AS Date)<=CAST(@EndDate AS Date)
				GROUP BY DATENAME(MONTH, PL.CreatedDate), DATEPART(YYYY, PL.CreatedDate), DATEPART(MM, PL.CreatedDate)  
	    		--ORDER BY DATEPART(MM, PL.CreatedDate) ASC    

				UNION ALL
	            			
				SELECT TotalImpressionCount = COUNT(1)	                     
					  ,UniqueUsers=COUNT(Distinct M.UserID) 
					  ,[Month] = DATENAME(MONTH, S.CreatedDate)
					  ,[MM] = DATEPART(MM, S.CreatedDate)  
					  ,[YYYY] = DATEPART(YYYY, S.CreatedDate) 
					  --,S.CreatedDate CreatedDate           
				FROM scansee..Product P 
				INNER JOIN Scansee..RetailLocationDeal RD ON RD.ProductID =P.ProductID 
				INNER JOIN SalesList S ON S.RetailLoactionDealID =RD.RetailLocationDealID   
				INNER JOIN MainMenu M ON M.MainMenuID = S.MainMenuID            
				WHERE ManufacturerID =@SupplierID 
				AND CAST(S.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(S.CreatedDate AS Date)<=CAST(@EndDate AS Date)			     
				GROUP BY DATENAME(MONTH, S.CreatedDate), DATEPART(YYYY, S.CreatedDate), DATEPART(MM, S.CreatedDate)  
	    		--ORDER BY DATEPART(MM, S.CreatedDate) ASC           
            )A 
            GROUP BY [Month],[YYYY] ,[MM]
            
            SELECT DISTINCT RowNum = IDENTITY(INT, 1, 1)
				 , ISNULL(C.TotalImpressionCount, 0)TotalImpressionCount
				 , ISNULL(A.TotalProduct, 0)TotalProduct
				 , ISNULL(C.UniqueUsers, 0)UniqueUsers
				 , ISNULL(C.[Month], A.[Month])[Month]
				 , ISNULL(C.MM, A.MM)MM
				 , ISNULL(C.YYYY, A.YYYY)YYYY
			INTO #Temp
			FROM #Counts  C
			FULL JOIN #ProductsCount A ON A.MM = C.MM AND A.YYYY = C.YYYY
			ORDER BY MM ASC           
            
            SELECT A.TotalImpressionCount
				 , SUM(B.TotalProduct)TotalProduct
				 , A.UniqueUsers
				 , A.[Month]
				 , A.MM
				 , A.YYYY
			FROM #Temp A
			CROSS JOIN #Temp B
			WHERE (B.RowNum <= A.RowNum)  
			AND A.MM BETWEEN DATEPART(MM, @StartDate) AND DATEPART(MM, @EndDate)
			GROUP BY A.TotalImpressionCount, A.UniqueUsers, A.[Month], A.MM, A.YYYY
			ORDER BY A.MM ASC
                 
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierCategoryProductsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
