USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierCategoryStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                             : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                             : usp_SupplierCategoryProductsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               2nd April 2013    Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierCategoryStatistics]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime   
      
      
)
AS
BEGIN

      BEGIN TRY
      
            DECLARE @TotalProduct INT=0
            DECLARE @UniqueUsers INT=0
            DECLARE @TotalImpressionCount INT=0
            
            DECLARE @UniqueUsersCount INT=0
            DECLARE @TotalImpressionCounts INT=0           
            
            
            DECLARE @UniqueUsers1 INT=0
            DECLARE @TotalImpressionCount1 INT=0
            
            SELECT @TotalProduct=COUNT(1) 
            FROM scansee..Product 
            WHERE ManufacturerID =@SupplierID 
            AND CAST(ProductAddDate AS Date)<=CAST(@EndDate AS Date)            
                   
            --SELECT @TotalImpressionCount = COUNT(1) 
            --      ,@UniqueUsers=COUNT(Distinct M.UserID)                    
            --FROM scansee..Category C
            --INNER JOIN scansee..ProductCategory PC ON C.CategoryID=PC.CategoryID
            --INNER JOIN scansee..Product P ON P.ProductID =PC.ProductID 
            --INNER JOIN ProductList PL ON PL.ProductID =P.ProductID 
            --INNER JOIN MainMenu M ON M.MainMenuID=PL.MainMenuID 
            --WHERE ManufacturerID =@SupplierID 
            --AND CAST(PL.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(PL.CreatedDate AS Date)<=CAST(@EndDate AS Date)
            
            
            SELECT @TotalImpressionCount = COUNT(1) 
                  ,@UniqueUsers=COUNT(Distinct M.UserID)                    
            FROM scansee..Product P 
            INNER JOIN ProductList PL ON PL.ProductID =P.ProductID 
            INNER JOIN MainMenu M ON M.MainMenuID=PL.MainMenuID 
            WHERE ManufacturerID =@SupplierID 
            AND CAST(PL.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(PL.CreatedDate AS Date)<=CAST(@EndDate AS Date)
            
            
            SELECT @TotalImpressionCounts = COUNT(1) 
                  ,@UniqueUsersCount =COUNT(Distinct M.UserID)                    
            FROM scansee..Product P 
            INNER JOIN Scansee..RetailLocationDeal RD ON RD.ProductID =P.ProductID 
            INNER JOIN SalesList S ON S.RetailLoactionDealID =RD.RetailLocationDealID   
            INNER JOIN MainMenu M ON M.MainMenuID =S.MainMenuID            
            WHERE ManufacturerID =@SupplierID 
            AND CAST(S.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(S.CreatedDate AS Date)<=CAST(@EndDate AS Date)
         
                       
            --SELECT @TotalImpressionCount1 = COUNT(1) 
            --      ,@UniqueUsers1=COUNT(Distinct M.UserID)                    
            --FROM ScanSeeReportingDatabase..SalesList SL 
            --INNER JOIN scansee..RetailLocationDeal RD ON RD.RetailLocationDealID =SL.RetailLoactionDealID 
            --INNER JOIN scansee..Product P ON P.ProductID =RD.ProductID  
            --INNER JOIN MainMenu M ON M.MainMenuID=SL.MainMenuID 
            --WHERE ManufacturerID =@SupplierID 
            --AND CAST(SL.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(SL.CreatedDate AS Date)<=CAST(@EndDate AS Date)
            
            
            
            CREATE TABLE #Temp(Category Varchar(255),[Total Count] int)
            
            INSERT INTO #Temp(Category,[Total Count])
            SELECT 'Total Number of Impressions',@TotalImpressionCount+@TotalImpressionCounts
            UNION ALL 
            SELECT 'Total Number of Product' ,@TotalProduct 
            UNION ALL
            SELECT 'Total Number of Unique Visitors',@UniqueUsers+@UniqueUsersCount
            
			SELECT Category
			      ,[Total Count]
			FROM #Temp
          
                 
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierCategoryProductsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
