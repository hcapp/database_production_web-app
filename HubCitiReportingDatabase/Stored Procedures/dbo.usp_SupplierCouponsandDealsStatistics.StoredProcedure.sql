USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierCouponsandDealsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                             : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                             : usp_SupplierCouponsandDealsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               29th March 2013   Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierCouponsandDealsStatistics]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime   
      
      
)
AS
BEGIN

      BEGIN TRY         
           
           --Capture the Coupon Impressions and Clicks
           CREATE TABLE #Temp(couponid int,impressions int,clicks int,saves varchar(255),Percentage as (clicks*100/impressions))
           
           INSERT INTO #Temp (couponid,impressions,clicks,saves) 
           SELECT L.CouponID 
                  ,COUNT(1) impressions
                  ,COUNT(CASE WHEN L.CouponClick =1 THEN 1 END) Clicks
                  ,COUNT(CASE WHEN L.CouponClipped =1 THEN 1 END) Used                               
           FROM CouponList L
           INNER JOIN scansee..Coupon C ON L.CouponID =C.CouponID 
           INNER JOIN Scansee..CouponProduct CP ON CP.CouponID =C.CouponID 
           INNER JOIN Scansee..Product P ON P.ProductID =CP.ProductID 
           WHERE P.ManufacturerID =@SupplierID  
           AND CAST(L.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(L.CreatedDate AS Date)<=CAST(@EndDate AS Date) 
           GROUP BY L.CouponID 
           ORDER BY COUNT(1)
           
           CREATE TABLE #Temp1(couponid int,impressions int,clicks int,saves Varchar(255),Percentage as (clicks*100/impressions))
           
           INSERT INTO #Temp1 (couponid,impressions,clicks,saves) 
           SELECT H.ProductHotDealID  
                  ,COUNT(1) impressions
                  ,COUNT(CASE WHEN H.ProductHotDealClick =1 THEN 1 END) Clicks
                  ,'' Used                               
           FROM HotDealList H
           INNER JOIN scansee..ProductHotDeal PH ON PH.ProductHotDealID=H.ProductHotDealID  
           WHERE PH.ManufacturerID =@SupplierID  
           AND CAST(H.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(H.CreatedDate AS Date)<=CAST(@EndDate AS Date)
           GROUP BY H.ProductHotDealID 
           ORDER BY COUNT(1)           
            
          SELECT couponid
                ,name
                ,impressions 
                ,clicks 
                ,saves 
                ,CouponStartDate 
                ,CouponExpireDate 
                ,Percentage 
                ,Savebit 
          INTO #COUPON
          FROM
			  (SELECT T.couponid 
					 ,C.CouponName name
					 ,impressions 
					 ,clicks 
					 ,saves                 
					 ,CAST(C.CouponStartDate AS DATE ) CouponStartDate
					 ,CAST(C.CouponExpireDate AS DATE)  CouponExpireDate 
					 ,T.Percentage 
					 , Savebit=(CASE WHEN T.CouponID IS NOT NULL THEN 1 END)      
			   FROM #Temp T
			   INNER JOIN scansee..Coupon C ON C.CouponID =T.couponid
			   UNION ALL
			   SELECT T.couponid 
					 ,H.HotDealName  
					 ,impressions 
					 ,clicks 
					 ,''                  
					 ,CAST(H.HotDealStartDate AS DATE ) CouponStartDate
					 ,CAST(H.HotDealEndDate AS DATE)  CouponExpireDate  
					 ,T.Percentage  
					 ,Savebit=(CASE WHEN T.couponid IS NOT NULL THEN 0 END)         
			   FROM #Temp1 T
			   INNER JOIN scansee..ProductHotDeal H ON T.couponid =H.ProductHotDealID)a
           ORDER BY Percentage DESC,impressions DESC,clicks DESC
           
           SELECT couponid
                ,name
                ,impressions 
                ,clicks 
                ,(CASE WHEN Savebit =1 THEN saves ELSE 'N/A'END) saves
                ,CouponStartDate 
                ,CouponExpireDate 
                ,Percentage 
          FROM #COUPON
              
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierCouponsandDealsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
