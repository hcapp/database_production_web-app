USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierTop15ProductsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                             : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                             : usp_SupplierTop15ProductsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               27th March 2013   Dhananjaya TR           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierTop15ProductsStatistics]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime      
      
)
AS
BEGIN

      BEGIN TRY
      
                      
            DECLARE @LastStartdate datetime
            DECLARE @LastEnddate datetime
            DECLARE @Difference int=Datediff(DAY,@StartDate,@EndDate)
            --SET @Difference=@Difference
            SET @LastStartdate =@StartDate-(@Difference +1)
            SET @LastEnddate =@EndDate-(@Difference+1) 
            
           -- drop table #temp
            CREATE TABLE #Temp(productid int,impressions float,clicks float,percentage as (clicks*100/impressions))

			INSERT INTO #Temp(productid ,impressions ,clicks )
			SELECT DISTINCT TOP 15 L.ProductID
			      
			       ,COUNT(1) impression
			       ,COUNT(CASE WHEN ProductClick =1 THEN 1 END) Click
			      	        
			FROM ProductList L
			INNER JOIN scansee..Product P ON P.ProductID=L.ProductID 
			WHERE ManufacturerID =@SupplierID  
			AND CAST(CreatedDate AS DATE) >= @StartDate AND CAST(CreatedDate AS DATE) <=CAST(@EndDate AS DATE) 
			GROUP BY L.ProductID
			ORDER BY COUNT(1)  DESC 
			
			--Select Product impression and clicks from salelist
			CREATE TABLE #Sale(productid int,impressions float,clicks float,percentage as (clicks*100/impressions))

			INSERT INTO #Sale(productid ,impressions ,clicks )
			SELECT DISTINCT P.ProductID			      
			       ,COUNT(1) impression		       
			       , COUNT(CASE WHEN RetailLocationDealClick =1 THEN 1 END) Click              
			FROM scansee..Product P 
            INNER JOIN Scansee..RetailLocationDeal RD ON RD.ProductID =P.ProductID 
            INNER JOIN SalesList S ON S.RetailLoactionDealID =RD.RetailLocationDealID   
            INNER JOIN MainMenu M ON M.MainMenuID = S.MainMenuID            
            WHERE ManufacturerID =@SupplierID 
            AND CAST(S.CreatedDate AS Date)>=CAST(@StartDate AS Date) AND CAST(S.CreatedDate AS Date)<=CAST(@EndDate AS Date)
			GROUP BY P.ProductID, P.ProductName      
			ORDER BY COUNT(1)  DESC		
			
			
			
			
			CREATE TABLE #CurrentList(productid int,impressions float,clicks float,percentage as (clicks*100/impressions))

			INSERT INTO #CurrentList(productid ,impressions ,clicks )	
			
			SELECT CASE WHEN T.productid =S.productid THEN S.productid WHEN T.productid IS NULL THEN S.productid  ELSE T.productid  END productid
			      ,CASE WHEN T.productid =S.productid THEN ISNULL(T.impressions,0) + ISNULL(S.impressions,0) WHEN T.productid IS NULL THEN ISNULL(S.impressions,0) ELSE T.impressions END impressions
			      ,CASE WHEN T.productid =S.productid THEN ISNULL(T.clicks,0) + ISNULL(S.clicks,0) WHEN T.productid IS NULL THEN ISNULL(S.clicks,0) ELSE ISNULL(T.clicks,0) END	clicks     		
			
			FROM #Temp T
			FULL OUTER JOIN #Sale S ON T.productid =S.productid  
			ORDER By impressions 
						
			--Creting temp table for capture impression and clicks.
			CREATE TABLE #Temp1(productid int,impressions float,clicks float,percentage as (clicks*100/impressions))
			
			INSERT INTO #Temp1(productid ,impressions ,clicks )
			SELECT DISTINCT L.ProductID			      
			       ,COUNT(1) impression
			       ,COUNT(CASE WHEN ProductClick =1 THEN 1 END) Click
			FROM ProductList L
			INNER JOIN #Temp  T ON T.ProductID=L.ProductID 
			INNER JOIN scansee..Product P ON P.ProductID =T.productid 
			WHERE ManufacturerID =@SupplierID  
			AND CAST(L.CreatedDate AS DATE) >= CAST(@LastStartdate AS DATE) AND CAST(L.CreatedDate AS DATE) <= CAST(@LastEnddate AS DATE)  
			GROUP BY L.ProductID
			ORDER BY COUNT(1)  DESC
			 
			 
			--Select Product impression and clicks from salelist
			CREATE TABLE #PreSale(productid int,impressions float,clicks float,percentage as (clicks*100/impressions))

			INSERT INTO #PreSale(productid ,impressions ,clicks )
			SELECT DISTINCT P.ProductID			      
			       ,COUNT(1) impression		       
			       , COUNT(CASE WHEN RetailLocationDealClick =1 THEN 1 END) Click              
			FROM scansee..Product P 
            INNER JOIN Scansee..RetailLocationDeal RD ON RD.ProductID =P.ProductID 
            INNER JOIN SalesList S ON S.RetailLoactionDealID =RD.RetailLocationDealID   
            INNER JOIN MainMenu M ON M.MainMenuID = S.MainMenuID            
            WHERE ManufacturerID =@SupplierID 
            AND CAST(S.CreatedDate AS Date)>=CAST(@LastStartdate AS Date) AND CAST(S.CreatedDate AS Date)<=CAST(@LastEnddate AS Date)
			GROUP BY P.ProductID, P.ProductName      
			ORDER BY COUNT(1)  DESC	 
			 
			
			CREATE TABLE #PreList(productid int,impressions float,clicks float,percentage as (clicks*100/impressions))

			INSERT INTO #PreList(productid ,impressions ,clicks )
			
			SELECT CASE WHEN T.productid =S.productid THEN S.productid WHEN T.productid IS NULL THEN S.productid  ELSE T.productid  END productid
			      ,CASE WHEN T.productid =S.productid THEN ISNULL(T.impressions,0) + ISNULL(S.impressions,0) WHEN T.productid IS NULL THEN ISNULL(S.impressions,0) ELSE T.impressions END impressions
			      ,CASE WHEN T.productid =S.productid THEN ISNULL(T.clicks,0) + ISNULL(S.clicks,0) WHEN T.productid IS NULL THEN ISNULL(S.clicks,0) ELSE ISNULL(T.clicks,0) END	clicks     		
			FROM #Temp1 T
			FULL OUTER JOIN #PreSale S ON T.productid =S.productid
			ORDER BY impressions  			
						 
			--Track the impression of the user SL, WL & Fav
			--INSERT INTO #Temp1(productid ,impressions ,clicks)
			--SELECT DISTINCT P.ProductID
			--			   ,COUNT(1) impression
			--			   ,1					
			--FROM ScanSeeReportingDatabase..UserProduct UP
			--INNER JOIN scansee..Product P ON UP.ProductID = P.ProductID
			--WHERE (UP.WishListAddDate IS NOT NULL AND UP.WishListAddDate >= @StartDate AND UP.WishListAddDate <= @EndDate)
			--		OR(UP.TodayListAddDate IS NOT NULL AND UP.TodayListAddDate >= @StartDate AND UP.TodayListAddDate <= @EndDate)
			--		OR (UP.MasterListAddDate IS NOT NULL AND UP.MasterListAddDate >= @StartDate AND UP.MasterListAddDate <= @EndDate) 
			--AND P.ManufacturerID =@SupplierID 		
			--GROUP BY p.ProductID 	
		 --   ORDER BY impression DESC
			      	        
						
			
			--Creting temp table for capture impression and clicks.
			CREATE TABLE #List(ProductID int,ProductName Varchar(255),impressions float,clicks float,Change float,percentage as (clicks*100/impressions))
			INSERT INTO #List(ProductID,ProductName ,impressions ,clicks,Change  )
			SELECT T.productid 
			      ,P.ProductName name
			      ,T.impressions impressions
			      ,T.clicks clicks
			      ,CASE WHEN T1.percentage IS NOT NULL THEN (T.percentage-T1.percentage ) ELSE T.percentage END Change 
			FROM #CurrentList  T
			INNER JOIN scansee..Product P ON P.ProductID =T.productid 
			LEFT JOIN #PreList T1 ON T.productid =T1.productid    
			
		
			SELECT ProductID 
			      ,ProductName name
			      ,CAST(impressions AS INT) impressions
			      ,CAST(clicks AS INT) clicks
			      ,ROUND(Change,2)Change		
			FROM #List 
			Order BY Change desc,impressions desc,clicks desc
			
                  
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierTop15ProductsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
