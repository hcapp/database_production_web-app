USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerCouponsandDealsStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_RetailerAppsiteTrackingInfo
Purpose                 : To get the impressions and clicks on the given Retailer coupons and hot deals.
Example                 : usp_SupplierCouponsandDealsStatistics

History
Version           Date              Author                  Change Description
------------------------------------------------------------------------------ 
1.0               28th June 2013   Pavan Sharma K           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerCouponsandDealsStatistics]
(
        @RetailerID int
      , @StartDate datetime
      , @EndDate datetime   
      
      
)
AS
BEGIN

      BEGIN TRY         
           
           --Capture the Coupon Impressions and Clicks
           CREATE TABLE #Temp(ID int, Name varchar(255), StartDate DATE, EndDate DATE, Impressions int,
		   Clicks int,Saves varchar(255),Percentage as (Clicks*100/(CASE WHEN Impressions=0 THEN 1 ELSE Impressions END)), CouponFlag bit)
           
		   
		   		  	   
		   --Get the Coupon statistics			  	
		   		   
		   SELECT C.CouponID
				, C.CouponName
				, C.CouponStartDate
				, C.CouponExpireDate
				, COUNT(DISTINCT CouponListID) Impressions
				, COUNT(DISTINCT CASE WHEN CL.CouponClick = 1 THEN CL.CouponListID END) Clicks
				, COUNT(DISTINCT UC.UserCouponGalleryID) Saves
				, 1 CouponFlag
		   INTO #ScanseeCouponList
		   FROM Scansee..Coupon C
		   LEFT JOIN Scansee..UserCouponGallery UC ON C.CouponID = UC.CouponID				
		   LEFT JOIN ScanSeeReportingDatabase..CouponList CL ON CL.CouponID = C.CouponID
		   WHERE C.RetailID = @RetailerID															
		   AND CAST(CL.CreatedDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
		   GROUP BY C.CouponID																	
				  , C.CouponName
				  , C.CouponStartDate
				  , C.CouponExpireDate
          
         
		 --HubCiti Related User Tracking
		 SELECT C.CouponID 
		      , C.CouponName 
			  , C.CouponStartDate
			  , C.CouponExpireDate 
			  , COUNT(DISTINCT CL.CouponsListID ) Impressions
			  , COUNT(DISTINCT CASE WHEN CL.CouponClick =1 THEN CL.CouponsListID END) Clicks
			  , COUNT(DISTINCT UG.HcUserCouponGalleryID) Saves
			  , 1 CouponFlag
		 INTO #HubCitiCouponList
		 FROM Scansee..Coupon C
		 LEFT JOIN Scansee..HcUserCouponGallery UG ON C.CouponID =UG.CouponID 
		 LEFT JOIN HubCitiReportingDatabase..CouponsList CL ON CL.CouponID =C.CouponID			
		 WHERE C.RetailID =@RetailerID 
		 AND CAST(CL.DateCreated AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
		 GROUP BY C.CouponID
				  , C.CouponName
				  , C.CouponStartDate
				  , C.CouponExpireDate 
		 
				
          --Get the hot deal statistics.
          SELECT PHD.ProductHotDealID
			   , PHD.HotDealName
			   , PHD.HotDealStartDate
			   , PHD.HotDealEndDate
			   , COUNT(DISTINCT HotDealListID) Impressions
			   , COUNT(DISTINCT CASE WHEN HL.ProductHotDealClick = 1 THEN HL.HotDealListID END) Clicks
			   , COUNT(DISTINCT UH.UserHotDealGalleryID) Saves
			   , 0 CouponFlag
		  INTO #ScanseeHotDealList
          FROM Scansee..ProductHotDeal PHD 
          LEFT JOIN Scansee..UserHotDealGallery UH ON UH.HotDealID = PHD.ProductHotDealID
          LEFT JOIN ScanSeeReportingDatabase..HotDealList HL ON HL.ProductHotDealID = PHD.ProductHotDealID
          WHERE PHD.RetailID = @RetailerID
          AND CAST(HL.CreatedDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)  
          GROUP BY PHD.ProductHotDealID
			     , PHD.HotDealName
			     , PHD.HotDealStartDate
			     , PHD.HotDealEndDate
          
      
		  --HubCiti Related User Tracking
		  SELECT PHD.ProductHotDealID
			   , PHD.HotDealName
			   , PHD.HotDealStartDate
			   , PHD.HotDealEndDate
			   , COUNT(DISTINCT HotDealListID) Impressions
			   , COUNT(DISTINCT CASE WHEN HL.ProductHotDealClick = 1 THEN HL.HotDealListID END) Clicks
			   , COUNT(DISTINCT UH.HcUserHotDealGalleryID) Saves
			   , 0 CouponFlag
          INTO #HubcitiHotDealList
		  FROM Scansee..ProductHotDeal PHD 
          LEFT JOIN Scansee..HcUserHotDealGallery UH ON UH.HotDealID = PHD.ProductHotDealID
          LEFT JOIN HubCitiReportingDatabase..HotDealList HL ON HL.ProductHotDealID = PHD.ProductHotDealID
          WHERE PHD.RetailID = @RetailerID
          AND CAST(HL.DateCreated AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)  
          GROUP BY PHD.ProductHotDealID
			     , PHD.HotDealName
			     , PHD.HotDealStartDate
			     , PHD.HotDealEndDate
		  		  
		 --Newly Added to Get the Special Offers Statistics

		 --drop table #ScanseeSpecialsList
		 --drop table #HubcitiSpecialsList
		 
		 SELECT QR.QRRetailerCustomPageID,
				QR.Pagetitle,
				QR.StartDate,
				QR.EndDate,
				COUNT(DISTINCT SL.SpecialsListID ) Impressions,
			    COUNT(DISTINCT CASE WHEN SL.SpecialOfferClick =1 THEN SL.SpecialsListID END) Clicks,
			    0 Saves,
			    0 CouponFlag
		 INTO #ScanseeSpecialsList
		 FROM Scansee..QRRetailerCustomPage QR
		 INNER JOIN Scansee..QRTypes QT ON QT.QRTypeID=QR.QRTypeID AND QT.QRTypeName='Special Offer Page'
		 LEFT JOIN ScanSeeReportingDatabase..SpecialsList SL ON SL.SpecialOfferID= QR.QRRetailerCustomPageID 
		 WHERE QR.RetailID = @RetailerID
		 AND CAST(SL.CreatedDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE) 	 		
		 GROUP BY QR.QRRetailerCustomPageID,
				  QR.Pagetitle,
				  QR.StartDate,
				  QR.EndDate

				  --select * from #ScanseeSpecialsList

		--Newly Added to Hubciti related Special Offer Tracking
				 
		 SELECT QR.QRRetailerCustomPageID,
				QR.Pagetitle,
				QR.StartDate,
				QR.EndDate,
				COUNT(DISTINCT SL.SpecialsListID ) Impressions,
			    COUNT(DISTINCT CASE WHEN SL.SpecialOfferClick =1 THEN SL.SpecialsListID END) Clicks,
			    0 Saves,
			    0 CouponFlag
		 INTO #HubcitiSpecialsList
		 FROM Scansee..QRRetailerCustomPage QR
		 INNER JOIN Scansee..QRTypes QT ON QT.QRTypeID=QR.QRTypeID AND QT.QRTypeName='Special Offer Page'
		 LEFT JOIN HubCitiReportingDatabase..SpecialsList SL ON SL.SpecialOfferID= QR.QRRetailerCustomPageID 
		 WHERE QR.RetailID = @RetailerID	
		 AND CAST(SL.CreatedDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)	 		
		 GROUP BY QR.QRRetailerCustomPageID,
				  QR.Pagetitle,
				  QR.StartDate,
				  QR.EndDate


				  --select * From #ScanseeSpecialsList

				  --select * From #HubcitiSpecialsList

		  INSERT INTO #Temp(ID, Name, StartDate, EndDate, Impressions, Clicks, Saves, CouponFlag)

		  SELECT ID=ISNULL(S.CouponID,H.CouponID)
		        ,Name=ISNULL(S.CouponName,H.CouponName)
				,StartDate=ISNULL(S.CouponStartDate,H.CouponStartDate)
				,EndDate=ISNULL(S.CouponExpireDate,H.CouponExpireDate)
				,Impressions=CASE WHEN S.CouponID IS NOT NULL AND H.CouponID IS NOT NULL THEN S.Impressions + H.Impressions
				                  WHEN S.CouponID IS NOT NULL AND H.CouponID IS NULL THEN S.Impressions
								  WHEN S.CouponID IS NULL AND H.CouponID IS NOT NULL THEN H.Impressions END
			    ,Clicks=CASE WHEN S.CouponID IS NOT NULL AND H.CouponID IS NOT NULL THEN S.Clicks + H.Clicks
				             WHEN S.CouponID IS NOT NULL AND H.CouponID IS NULL THEN S.Clicks
							 WHEN S.CouponID IS NULL AND H.CouponID IS NOT NULL THEN H.Clicks END
				,Saves=CASE WHEN S.CouponID IS NOT NULL AND H.CouponID IS NOT NULL THEN S.Saves + H.Saves
				             WHEN S.CouponID IS NOT NULL AND H.CouponID IS NULL THEN S.Saves
							 WHEN S.CouponID IS NULL AND H.CouponID IS NOT NULL THEN H.Saves END
				,1 
				
		  FROM #ScanseeCouponList S
		  FULL JOIN #HubCitiCouponList H ON H.CouponID =S.CouponID 

		  UNION ALL


		  SELECT ID=ISNULL(S.ProductHotDealID,H.ProductHotDealID)
		        ,Name=ISNULL(S.HotDealName,H.HotDealName)
				,HotDealStartDate=ISNULL(S.HotDealStartDate,H.HotDealStartDate)
				,HotDealEndDate=ISNULL(S.HotDealEndDate,H.HotDealEndDate)
				,Impressions=CASE WHEN S.ProductHotDealID IS NOT NULL AND H.ProductHotDealID IS NOT NULL THEN S.Impressions + H.Impressions
				                  WHEN S.ProductHotDealID IS NOT NULL AND H.ProductHotDealID IS NULL THEN S.Impressions
								  WHEN S.ProductHotDealID IS NULL AND H.ProductHotDealID IS NOT NULL THEN H.Impressions END
			    ,Clicks=CASE WHEN S.ProductHotDealID IS NOT NULL AND H.ProductHotDealID IS NOT NULL THEN S.Clicks + H.Clicks
				             WHEN S.ProductHotDealID IS NOT NULL AND H.ProductHotDealID IS NULL THEN S.Clicks
							 WHEN S.ProductHotDealID IS NULL AND H.ProductHotDealID IS NOT NULL THEN H.Clicks END
				,Saves=CASE WHEN S.ProductHotDealID IS NOT NULL AND H.ProductHotDealID IS NOT NULL THEN S.Saves + H.Saves
				            WHEN S.ProductHotDealID IS NOT NULL AND H.ProductHotDealID IS NULL THEN S.Saves
							WHEN S.ProductHotDealID IS NULL AND H.ProductHotDealID IS NOT NULL THEN H.Saves END
				,0
		  FROM #ScanseeHotDealList S
		  FULL JOIN #HubcitiHotDealList H ON S.ProductHotDealID =H.ProductHotDealID 

		  UNION ALL




		   SELECT ID=ISNULL(S.QRRetailerCustomPageID,H.QRRetailerCustomPageID)
		        ,Name=ISNULL(S.Pagetitle,H.Pagetitle)
				,HotDealStartDate=ISNULL(S.StartDate,H.StartDate)
				,HotDealEndDate=ISNULL(S.EndDate,H.EndDate)
				,Impressions=CASE WHEN S.QRRetailerCustomPageID IS NOT NULL AND H.QRRetailerCustomPageID IS NOT NULL THEN S.Impressions + H.Impressions
				                  WHEN S.QRRetailerCustomPageID IS NOT NULL AND H.QRRetailerCustomPageID IS NULL THEN S.Impressions
								  WHEN S.QRRetailerCustomPageID IS NULL AND H.QRRetailerCustomPageID IS NOT NULL THEN H.Impressions END
			    ,Clicks=CASE WHEN S.QRRetailerCustomPageID IS NOT NULL AND H.QRRetailerCustomPageID IS NOT NULL THEN S.Clicks + H.Clicks
				             WHEN S.QRRetailerCustomPageID IS NOT NULL AND H.QRRetailerCustomPageID IS NULL THEN S.Clicks
							 WHEN S.QRRetailerCustomPageID IS NULL AND H.QRRetailerCustomPageID IS NOT NULL THEN H.Clicks END
				,0
				,0
		  FROM #ScanseeSpecialsList S
		  FULL JOIN #HubcitiSpecialsList H ON S.QRRetailerCustomPageID=H.QRRetailerCustomPageID
		  
		  
		   --Display the Coupon and Hot deals statistics sorted by name.
           SELECT ID
                , Name
                , Impressions 
                , Clicks 
                , Saves
                , StartDate 
                , EndDate
                , Percentage 
                , CouponFlag
          FROM #Temp
          ORDER BY Name ASC
              
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_RetailerCouponsandDealsStatistics.'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;




GO
