USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierShoppingListStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_SupplierWishListStatistics
Purpose					: To get the product statistics from Shoppin List & Favourites List.
Example					: usp_SupplierWishListStatistics

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			21st March 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierShoppingListStatistics]
(
	  @SupplierID int
	, @StartDate datetime
	, @EndDate datetime	 

)
AS
BEGIN

	BEGIN TRY
		--Capture the Shopping list statistics of the supplier products.
		BEGIN
			--Get the total user count.
			DECLARE @UserCount INT 
			SELECT @UserCount = COUNT(DISTINCT U.UserID)
			FROM scansee..Users U
			INNER JOIN scansee..UserDeviceAppVersion UD ON U.UserID = UD.UserID
			WHERE U.DateCreated <= @EndDate				
			
			--Get the statistics on Shopping list & Favourites.	
			SELECT P.ProductID
				 , P.ProductName
				 , 'Number on Shopping List' = COUNT(CASE WHEN UP.TodayListtItem = 1 AND CAST(UP.TodayListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.TodayListAddDate AS DATE) <= CAST(@EndDate AS DATE) THEN UP.UserProductID END)
				 , '% of Total Users' = ROUND((CAST(COUNT(CASE WHEN UP.TodayListtItem = 1 AND CAST(UP.TodayListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.TodayListAddDate AS DATE) <= CAST(@EndDate AS DATE) THEN UP.UserProductID END) AS FLOAT)/@UserCount)*100, 2)
				 , 'Number in Favourites' = COUNT(CASE WHEN UP.MasterListItem = 1 AND CAST(UP.MasterListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.MasterListAddDate AS DATE) <= CAST(@EndDate AS DATE) THEN UP.UserProductID  END)
			FROM scansee..Product P
			INNER JOIN ScanSeeReportingDatabase..UserProduct UP ON P.ProductID = UP.ProductID
			WHERE P.ManufacturerID = @SupplierID
			AND ((CAST(UP.TodayListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.TodayListAddDate AS DATE) <= CAST(@EndDate AS DATE)) 
			OR (CAST(UP.TodayListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.TodayListAddDate AS DATE) <= CAST(@EndDate AS DATE)) 
			OR (CAST(UP.MasterListAddDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(UP.MasterListAddDate AS DATE) <= CAST(@EndDate AS DATE)))
			GROUP BY P.ProductID, P.ProductName			
		END
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_SupplierWishListStatistics.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;
		
GO
