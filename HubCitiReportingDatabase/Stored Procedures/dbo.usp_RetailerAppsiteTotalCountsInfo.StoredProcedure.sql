USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_RetailerAppsiteTotalCountsInfo]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name	: usp_RetailerAppsiteTotalCountsInfo
Purpose					: To get the counts on the .
Example					: usp_RetailerAppsiteTotalCountsInfo

History
Version		Date			Author			Change Description
--------------------------------------------------------------- 
1.0			26th March 2013	SPAN	Initial Version
---------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_RetailerAppsiteTotalCountsInfo]
(
	  @RetailerID int
	, @StartDate datetime
	, @EndDate datetime	 

)
AS
BEGIN

	BEGIN TRY	
		DECLARE @QRTypeID INT
		DECLARE @ImpressionsCount INT
		DECLARE @AppsitesCount INT
		DECLARE @UniqueVisitorCount INT
		DECLARE @HCAppsiteCount INT
		DECLARE @HCUniqueVisitors INT
		DECLARE @HcAppsiteImpression INT
		DECLARE @HCAnythingLists INT
		DECLARE @HCImpressionsCount INT
		DECLARE @HCUniqueVisitorCount INT

		
		SELECT @QRTypeID=QRTypeID FROM Scansee..QRTypes WHERE QRTypeName = 'Anything Page'
		
		--Get the Appsite count for the selected date range
		SELECT @AppsitesCount = COUNT(1)
		FROM Scansee..QRRetailerCustomPage
		WHERE RetailID = @RetailerID
		AND QRTypeID = @QRTypeID
		AND CAST(DateCreated AS DATE) <= CAST(@EndDate AS DATE)		
		AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate 	
		
		--HuBciti Related Appsite Count

		SELECT @HCAppsiteCount=COUNT(DISTINCT A.HcAppSiteID)
		FROM Scansee..HcAppSite A
		INNER JOIN HubCitiReportingDatabase..AppsiteList AL ON A.HcAppSiteID =AL.AppsiteID 
		INNER JOIN Scansee..RetailLocation RL ON RL.RetailLocationID =A.RetailLocationID 
		WHERE RL.RetailID =@RetailerID 
		AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)	
		
		--Hubciti related appsite Impressions Count

		SELECT @HcAppsiteImpression=COUNT(DISTINCT AL.AppsiteListID)
		      ,@HCUniqueVisitors =COUNT(DISTINCT M.HcUserID)
		FROM HubCitiReportingDatabase..AppsiteList AL
		INNER JOIN Scansee..HcAppSite A ON A.HcAppSiteID =AL.AppsiteID 
		INNER JOIN Scansee..RetailLocation RL ON Rl.RetailLocationID =A.RetailLocationID 
		INNER JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID =AL.MainMenuID 
		WHERE RetailID =@RetailerID 
		AND CAST(AL.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(AL.DateCreated AS DATE) <= CAST(@EndDate AS DATE)

		--Capture Retailer Created Anything Page In Hubciti
		SELECT @HCImpressionsCount = COUNT(DISTINCT A.AnythingPageListID) 
			 , @HCUniqueVisitorCount = COUNT(DISTINCT M.HcUserID) 
		--INTO #Temp
		FROM HubCitiReportingDatabase..AnythingPageList A
		INNER JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID = A.AnythingPageID
		LEFT JOIN HubCitiReportingDatabase..RetailerList RL ON RL.RetailerListID = A.RetailerListID
		LEFT JOIN HubCitiReportingDatabase..MainMenu M ON M.MainMenuID = A.MainMenuID
		WHERE Q.QRTypeID = @QRTypeID
		AND Q.RetailID = @RetailerID	
		AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate	
		AND CAST(A.DateCreated AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.DateCreated AS DATE) <= CAST(@EndDate AS DATE)




		--Get the Statistics of the Anything pages of the retailer in context.
		SELECT @ImpressionsCount = COUNT(DISTINCT A.AnythingPageListlID) 
			 , @UniqueVisitorCount = COUNT(DISTINCT M.UserID) 
		--INTO #Temp
		FROM AnythingPageList A
		INNER JOIN Scansee..QRRetailerCustomPage Q ON Q.QRRetailerCustomPageID = A.AnythingPageID
		LEFT JOIN RetailerList RL ON RL.RetailerListID = A.RetailerListID
		LEFT JOIN MainMenu M ON M.MainMenuID = A.MainMenuID
		WHERE Q.QRTypeID = @QRTypeID
		AND Q.RetailID = @RetailerID	
		AND ISNULL(EndDate, DATEADD(DD, 1, @EndDate)) >= @EndDate	
		AND CAST(A.CreatedDate AS DATE) >= CAST(@StartDate AS DATE) AND CAST(A.CreatedDate AS DATE) <= CAST(@EndDate AS DATE)
		
		
		CREATE TABLE #Appsite(Category varchar(1000), TotalCount int)
		INSERT INTO #Appsite(Category, TotalCount)
		SELECT 'Total Number of Impressions', @ImpressionsCount	+ @HcAppsiteImpression + @HCImpressionsCount
		UNION ALL
		SELECT 'Total Number of App Sites', @AppsitesCount + @HCAppsiteCount
		UNION ALL
		SELECT 'Total Number of Unique Visitors', @UniqueVisitorCount + @HCUniqueVisitors +@HCUniqueVisitorCount
		
		SELECT Category
			 , TotalCount
		FROM #Appsite		
		
	END TRY
		
	BEGIN CATCH
	
		--Check whether the Transaction is uncommitable.
		IF @@ERROR <> 0
		BEGIN
			PRINT 'Error occured in Stored Procedure usp_RetailerAppsiteTotalCountsInfo.'		
			--- Execute retrieval of Error info.
			SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
			
		END;
		 
	END CATCH;
END;





GO
