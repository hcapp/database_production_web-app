USE [ScanSeeReportingDatabase]
GO
/****** Object:  StoredProcedure [dbo].[usp_SupplierThisLocationStatistics]    Script Date: 4/6/2017 5:42:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Stored Procedure name   : usp_SupplierThisLocationStatistics
Purpose                 : To get the impressions and clicks on the given Retailer Appsite(Anything Page).
Example                 : usp_SupplierThisLocationStatistics

History
Version           Date              Author               Change Description
------------------------------------------------------------------------------ 
1.0               29th March 2013   Mohith H R           Initial Version
------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[usp_SupplierThisLocationStatistics]
(
        @SupplierID int
      , @StartDate datetime
      , @EndDate datetime              
)
AS
BEGIN

      BEGIN TRY
            DECLARE @ModuleID int
            
            SELECT @ModuleID =ModuleID  
            FROM Module 
            WHERE ModuleName='This Location'
                                   
            CREATE TABLE #Temp(ProductID int,Impressions int,Clicks int,Percentage as (Clicks*100/Impressions))

			INSERT INTO #Temp(ProductID,Impressions,Clicks)
			SELECT  P.ProductID		
			       ,COUNT(1) Impression
			       ,COUNT(CASE WHEN RetailLocationDealClick =1 THEN 1 END) Click			      	        
			FROM ScanSeeReportingDatabase..SalesList L
			INNER JOIN scansee..RetailLocationDeal RD ON RD.RetailLocationDealID =L.RetailLoactionDealID 
			INNER JOIN scansee..Product P ON P.ProductID = RD.ProductID 
			INNER JOIN MainMenu M ON M.MainMenuID =L.MainMenuID 
			WHERE ManufacturerID = @SupplierID  
			AND M.ModuleID =@ModuleID 
			AND CAST(L.CreatedDate AS DATE)>=CAST(@StartDate AS DATE) AND CAST(L.CreatedDate AS DATE)<= CAST(@EndDate AS DATE)   
			GROUP BY P.ProductID
			
			
			SELECT DISTINCT P.ProductID 
			      ,P.ProductName Name			      
			      ,T.Impressions Impressions
			      ,RL.SalePrice			    
			      ,T.Clicks Clicks	
			      ,Percentage 		     
			FROM #Temp T
			INNER JOIN scansee..Product P ON P.ProductID = T.ProductID  
			INNER JOIN scansee..RetailLocationProduct RL ON P.ProductID = RL.ProductID  
            ORDER BY T.Percentage,Impressions  DESC       
      END TRY
            
      BEGIN CATCH
      
            --Check whether the Transaction is uncommitable.
            IF @@ERROR <> 0
            BEGIN
                  PRINT 'Error occured in Stored Procedure usp_SupplierThisLocationStatistics'            
                  --- Execute retrieval of Error info.
                  SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE()
                  
            END;
            
      END CATCH;
END;

GO
